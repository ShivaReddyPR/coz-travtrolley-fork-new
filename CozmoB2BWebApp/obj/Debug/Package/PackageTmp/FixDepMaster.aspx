﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FixDepMasterGUI" Title="Fixed Departure"
    ValidateRequest="false" EnableEventValidation="false" Codebehind="FixDepMaster.aspx.cs" %>
 <%@ MasterType VirtualPath="~/TransactionBE.master"   %>
<%@ Import Namespace="CT.CMS" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script language="JavaScript" type="text/javascript" src="Scripts/jsBE/Utils.js"></script>

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="ash.js"></script>

    <script type="text/javascript" src="Scripts/Common/Common.js"></script>

    <script language="javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js" type="text/javascript"></script>

    <script type="text/javascript" language="javascript">
        var Ajax;
        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var region;
        var country;
        var city;

        var regions=[];
        var countries=[];
        var cities=[];
        var rc;

        function LoadRegions(id) {
           // alert(id +" Loading");
            
            region = id;
            var url = "RegionsAjax";
            var paramList = 'id=' + region;

            // Ajax.onreadystatechange = ShowRegionList;
           
            Ajax.open('POST', url, false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
            ShowRegionList(Ajax.responseText);

           // alert(id + " Loaded");
                   
            //new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: ShowRegionList });
        }
        function ShowRegionList(response) {
            
            if (Ajax.readyState == 4) {
               
                if (Ajax.status == 200) {
                    
                    if (Ajax.responseText.length > 0) {
                        
                        region = Ajax.responseText.split('#')[0];
                        var ddl = document.getElementById(region);
                        if (ddl != null) {
                            
                            for (var i = 0; i < ddl.length; i++) {
                                ddl.remove(i);
                            }
                            var values = Ajax.responseText.split('#')[1].split(',');
                            var el = document.createElement("option");
                            el.textContent = "Select Region";
                            el.value = "0";
                            ddl.add(el, 0);
                           
                            for (var i = 0; i < values.length; i++) {
                               
                                var opt = values[i];
                                var el = document.createElement("option");
                                el.textContent = opt.split('|')[1];
                                el.value = opt.split('|')[0];
                                ddl.appendChild(el);
                                
                            }
                            //Set the Region selected and load its Countries
                            if (regions.length > 0 && regions[0].trim().length > 0) {
                                
                                var id = eval(region.split('-')[1]);
                                if (regions.length >= id) {
                                    
                                    id = eval(id - 1);
                                    ddl.value = regions[id];
                                   
                                    LoadCountries(region);
                                }

                            }
                        }
                    }
                }
            }
        }

        function LoadCountries(id) {
            if (id == "ctl00_cphTransaction_RegionsText1") {
                country = 'ctl00_cphTransaction_CountryText1';
            }
            else {
                country = 'CountryText-' + id.split('-')[1];
            }
            var url = "CountryAjax";
            var paramList = 'Region=' + document.getElementById(id).value + '&id=' + country;
           // Ajax.onreadystatechange = ShowCountriesList;
            Ajax.open('POST', url,false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
            ShowCountriesList(Ajax.responseText)
            
//            if (id == "ctl00_cphTransaction_RegionsText1") {
//                country = 'ctl00_cphTransaction_CountryText1';
//            }
//            else {
//                country = 'CountryText-' + id.split('-')[1];
//            }
//            var url = "CountryAjax.aspx";
//            var paramList = 'Region=' + document.getElementById(id).value + '&id=' + country;
//            new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: ShowCountriesList });
        }
        function ShowCountriesList(response) {

            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0 || Ajax.responseText != null) {
                        country = Ajax.responseText.split('#')[0];
                        var ddl = document.getElementById(country);
                        
                        if (ddl != null) {
                            ddl.options.length = 0;
                            //                for (var i = 0; i <= ddl.length; i++) {
                            //                    ddl.remove(i);
                            //                }

                            var el = document.createElement("option");
                            ddl.options.length = 0;
                            el.textContent = "Select Country";
                            el.value = "0";
                            ddl.add(el, 0);
                            var values = Ajax.responseText.split('#')[1].split(',');

                            for (var i = 0; i < values.length; i++) {
                                var opt = values[i];
                                if (opt.length > 0 && opt.indexOf('|') > 0) {
                                    var el = document.createElement("option");
                                    el.textContent = opt.split('|')[0];
                                    el.value = opt.split('|')[1];
                                    ddl.appendChild(el);
                                }
                            }
                            
                            //Set the Country and load its cities
                             if (countries.length > 0 && countries[0].trim().length > 0) {
                                var id = eval(country.split('-')[1]);
                                if (id == undefined) {
                                    ddl.value = countries[0];
                                    if (ddl.value == "") {
                                        ddl.value = ddl.options[0].value;
                                    }
                                    LoadCities(country);
                                }
                                else {
                                    if (countries.length >= id) {
                                        id = eval(id - 1);
                                        ddl.value = countries[id];
                                        if (ddl.value == "") {
                                            ddl.value = ddl.options[0].value;
                                        }
                                        LoadCities(country);
                                    }
                                }
                            }
                            else {
                                ddl.value = ddl.options[0].value;
                            }
                        }
                        //LoadCities(country);
                    }

                }
            }
        }
            
       


        function LoadCities(id) {
            //assiging selcted values to hidden filds to retain the value
            if (id == "ctl00_cphTransaction_CountryText1") {
                city = 'ctl00_cphTransaction_CityText1';


                var sel = document.getElementById('<%=hdfCountries.ClientID %>');

                if (sel.value.indexOf(document.getElementById(id).value) < 0 && sel.value != "0") {
                    if (sel.value.trim().length > 0) {
                        sel.value += "," + document.getElementById(id).value;
                    }
                    else {
                        sel.value = document.getElementById(id).value;
                    }
                }
            }
            else {
                city = 'CityText-' + id.split('-')[1];
            }
            var url = "CityAjax";
            var paramList = 'requestSource=getCountryIdByCities' + '&Country=' + document.getElementById(id).value + '&id=' + city;
           // Ajax.onreadystatechange = ShowCitiesList;
            Ajax.open('POST', url,false);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
            ShowCitiesList(Ajax.responseText);
        }
        function ShowCitiesList(response) {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        city = Ajax.responseText.split('#')[0];
                        var ddl = document.getElementById(city);
                        if (document.getElementById('ctl00_cphTransaction_CountryText1').value != "0") {
                            //assiging selcted values to hidden filds to retain the value
                            document.getElementById('<%=hdfSelCountry.ClientID %>').value = document.getElementById('ctl00_cphTransaction_CountryText1').value;
                            //alert(document.getElementById('<%=hdfSelCountry.ClientID %>').value);
                        }
                        if (document.getElementById('ctl00_cphTransaction_CityText1').value != "0") {
                            document.getElementById('<%=hdfSelCity.ClientID %>').value = document.getElementById('ctl00_cphTransaction_CityText1').value;
                        }
                        if (ddl != null) {
                            ddl.options.length = 0;
                            //                for (var i = 0; i <= ddl.length; i++) {
                            //                    ddl.remove(i);
                            //                }

                            var el = document.createElement("option");
                            el.textContent = "Select City";
                            el.value = "0";
                            ddl.add(el, 0);
                            var values = Ajax.responseText.split('#')[1].split(',');

                            for (var i = 0; i < values.length; i++) {
                                var opt = values[i];
                                if (opt.length > 0 && opt.indexOf('|') > 0) {
                                    var el = document.createElement("option");
                                    el.textContent = opt.split('|')[0];
                                    el.value = opt.split('|')[1];
                                    ddl.appendChild(el);
                                }
                            }

                            if (cities.length > 0 && cities[0].trim().length > 0) {
                                var id = eval(city.split('-')[1]);
                                if (id == undefined) {
                                    id = 0;
                                    ddl.value = cities[id];
                                    if (ddl.value == "") {
                                        ddl.value = ddl.options[0].value;
                                    }
                                }
                                else {
                                    if (cities.length >= id) {
                                        id = eval(id - 1);
                                        ddl.value = cities[id];
                                        if (ddl.value == "") {
                                            ddl.value = ddl.options[0].value;
                                        }
                                    }

                                }

                            }
                        }
                    }
                }
            }
        }
                        
                       


        function SetCity(id) {
           
            if (id != "ctl00_cphTransaction_CityText1") {
                var sel = document.getElementById('<%=hdfCities.ClientID %>');


                if (sel.value.indexOf(document.getElementById(id).value) < 0 && sel.value != "0") {
                    if (sel.value.trim().length > 0) {
                        sel.value += "," + document.getElementById(id).value;
                    }
                    else {
                        sel.value = document.getElementById(id).value;
                    }
                    //alert(sel.value);
                }
            } else {
                //assiging selcted values to hidden filds to retain the value
                if (document.getElementById('ctl00_cphTransaction_CityText1').value != "0") {
                    document.getElementById('<%=hdfSelCity.ClientID %>').value = document.getElementById('ctl00_cphTransaction_CityText1').value;
                }
               //alert(document.getElementById('<%=hdfSelCity.ClientID %>').value);
            }
        }
        
        function ShowItinerary() {

            var ddl = document.getElementById('<%=ddlDays.ClientID %>').value;

            for (var i = 2; i <= 31; i++) {
                document.getElementById('tbl' + i).style.display = 'none';                
            }

            for (var i = 2; i <= ddl; i++) {
                document.getElementById('tbl' + i).style.display = 'block';                
            }
        }

        tinyMCE.init({
            mode: "textareas",
            theme: "advanced",
            width: '640px',
            theme_advanced_buttons1_add: "bullist,numlist,outdent,indent,undo,redo,fontselect,fontsizeselect",
            theme_advanced_buttons2: "",
            theme_advanced_toolbar_location: "top",
            theme_advanced_disable: "styleselect,anchor,formatselect,justifyfull,help,cleanup",
            convert_fonts_to_spans: true,
            content_css: "/style.css"
        });
        //    function init() {

        //        //    showReturn();
        //        var today = new Date();
        //        // For making dual Calendar use CalendarGroup  for single Month use Calendar     
        //        cal1 = new YAHOO.widget.Calendar("cal1", "container1");
        //        cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
        //        cal1.cfg.setProperty("title", "Select From Date");
        //        cal1.cfg.setProperty("close", true);
        //        cal1.selectEvent.subscribe(setDate1);
        //        cal1.render();

        //        cal2 = new YAHOO.widget.Calendar("cal2", "container2");
        //        cal2.cfg.setProperty("title", "Select To Date");
        //        cal2.selectEvent.subscribe(setDate2);
        //        cal2.cfg.setProperty("close", true);
        //        cal2.render();
        //    }
        function init1() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal3 = new YAHOO.widget.Calendar("cal3", "container3");
            cal3.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal3.cfg.setProperty("title", "Select From Date");
            cal3.cfg.setProperty("close", true);
            cal3.selectEvent.subscribe(setDate3);
            cal3.render();

            cal4 = new YAHOO.widget.Calendar("cal4", "container4");
            cal4.cfg.setProperty("title", "Select To Date");
            cal4.selectEvent.subscribe(setDate4);
            cal4.cfg.setProperty("close", true);
            cal4.render();
        }
        function init2() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal5 = new YAHOO.widget.Calendar("cal5", "container5");
            cal5.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal5.cfg.setProperty("title", "Select From Date");
            cal5.cfg.setProperty("close", true);
            cal5.selectEvent.subscribe(setDate5);
            cal5.render();

            cal6 = new YAHOO.widget.Calendar("cal6", "container6");
            cal6.cfg.setProperty("title", "Select To Date");
            cal6.selectEvent.subscribe(setDate6);
            cal6.cfg.setProperty("close", true);
            cal6.render();
        }
        function init3() {

            //    showReturn();

            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar
            cal7 = new YAHOO.widget.Calendar("cal7", "container7");
            cal7.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal7.cfg.setProperty("title", "Select From Date");
            cal7.cfg.setProperty("close", true);
            cal7.selectEvent.subscribe(setDate7);
            cal7.render();

            cal8 = new YAHOO.widget.Calendar("cal8", "container8");
            cal8.cfg.setProperty("title", "Select To Date");
            cal8.selectEvent.subscribe(setDate8);
            cal8.cfg.setProperty("close", true);
            cal8.render();
        }
        function init4() {
            cal1 = new YAHOO.widget.Calendar("cal1", "callContainer");
            cal1.selectEvent.subscribe(setDate9);
            cal1.cfg.setProperty("close", true);
            cal1.render();
        }

        function init5() {
            var today = new Date();
            cal10 = new YAHOO.widget.Calendar("cal10", "cal2Container");
            cal10.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal10.selectEvent.subscribe(setDate10);
            cal10.cfg.setProperty("close", true);
            cal10.render();
        }
        var calendarId;
        function setDate9() {
            var date1 = cal1.getSelectedDates()[0];
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            var availDateFrom = new Date();
            var availDateTo = new Date();
            var unAvDate = new Date();
            availDateFrom = document.getElementById('<% = txtFrom.ClientID %>').value;
            availDateTo = document.getElementById('<% = txtTo.ClientID %>').value;
            unAvDate = day + "/" + (month) + "/" + date1.getFullYear(); //document.getElementById('<% = Date.ClientID %>').value;
            var d1 = availDateFrom.split("/");
            var d2 = availDateTo.split("/");
            var c = unAvDate.split("/");

            var from = new Date(d1[2], d1[1] - 1, d1[0]);  // -1 because months are from 0 to 11
            var to = new Date(d2[2], d2[1] - 1, d2[0]);
            var check = new Date(c[2], c[1] - 1, c[0]);
            if (check < from || check > to) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "UnAvail Dates should be in between from Date and To Date ";
                return false;
            }
            else {
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
            }


            document.getElementById('<% = Date.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();


            var counter = document.getElementById("<%=hdfUnavailDateCounter.ClientID%>").value;
            counter = counter - 1;

            document.getElementById('txtUnAvailDatesText-' + counter).value = document.getElementById('<% = Date.ClientID %>').value

            document.getElementById('callContainer').style.display = "none";
        }


        function setDate10() {
            var date1 = cal10.getSelectedDates()[0];            
            
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            var availDateFrom = new Date();
            
            var unAvDate = new Date();
            availDateFrom = document.getElementById(calendarId).value;
            
            unAvDate = day + "/" + (month) + "/" + date1.getFullYear(); //document.getElementById('<% = Date.ClientID %>').value;
            var d1 = availDateFrom.split("/");
            
            var c = unAvDate.split("/");

            var from = new Date(d1[2], d1[1]-1, d1[0]);  // -1 because months are from 0 to 11
            
            var check = new Date(c[2], c[1]-1, c[0]);
            if (check < from ) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date should be greater than Today";
                return false;
            }
            else {
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
            }
            document.getElementById(calendarId).value = day + "/" + (month) + "/" + date1.getFullYear();

            document.getElementById('cal2Container').style.display = "none";
            
        }

        function ShowCalender(container) {
            var containerId = container;
            if (document.getElementById(containerId).style.display == "none") {
                document.getElementById(containerId).style.display = "block"
            }
        }
        //    function showCalendar1() {
        //        document.getElementById('container2').style.display = "none";
        //        document.getElementById('container1').style.display = "block";
        //        document.getElementById('container2').style.display = "none";
        //        document.getElementById('container1').style.display = "block"

        //    }
        function showCalendar3() {
            //        document.getElementById('container4').style.display = "none";
            //        document.getElementById('container3').style.display = "block";
            document.getElementById('container4').style.display = "none";
            document.getElementById('container3').style.display = "block";
        }
        function showCalendar5() {

            document.getElementById('container6').style.display = "none";
            document.getElementById('container5').style.display = "block";
            //        document.getElementById('container6').style.display = "none";
            //        document.getElementById('container5').style.display = "block";

        }
        function showCalendar7() {
            //        document.getElementById('container8').style.display = "none";
            //        document.getElementById('container7').style.display = "block";
            document.getElementById('container8').style.display = "none";
            document.getElementById('container7').style.display = "block";
        }
        var departureDate = new Date();
        function showCalendar2() {

            //vij document.getElementById('container1').style.display = "none";
            document.getElementById('container1').style.display = "none";
            cal1.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function showCalendar4() {

            //vij document.getElementById('container3').style.display = "none";
            document.getElementById('container3').style.display = "none";
            cal3.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal4.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal4.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal4.render();
            }
            document.getElementById('container4').style.display = "block";
        }

        function showCalendar8() {
            //document.getElementById('container7').style.display = "none";
            document.getElementById('container7').style.display = "none";
            cal5.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById("<%=txtPickUpHr.ClientID%>").value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal8.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal8.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal8.render();
            }
            document.getElementById('container8').style.display = "block";
        }

        YAHOO.util.Event.addListener(window, "load", init5);
        function showCalendar10(id) {
            calendarId = id;
            var bodyRect = document.body.getBoundingClientRect();
            var elemRect = document.getElementById(calendarId).getBoundingClientRect();
            var offset = elemRect.top - bodyRect.top;

            document.getElementById('cal2Container').style.top = eval(offset - 150);
            document.getElementById('cal2Container').style.display = "block";
        }

        function setDate3() {
            var date1 = cal3.getSelectedDates()[0];
            //alert(date1);
            document.getElementById('IShimFrame').style.display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();
            var thisTime = this.today.getHours();
            var thisMinutes = this.today.getMinutes();
            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                return false;
            }
            departureDate = cal3.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();
            var time = "";
            var timePM = "";

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }
            if (thisMinutes < 10) {
                thisMinutes = "0" + thisMinutes
            }
            if (thisTime > 11) {
                timePM = ("PM")
            } else {
                timePM = ("AM")
            }
            time = (thisTime + ":" + thisMinutes + timePM + " ");
            document.getElementById("<%=txtFrom.ClientID%>").value = day + "/" + (month) + "/" + date1.getFullYear();
            document.getElementById("<%=txtFromTime.ClientID%>").value = time;

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal3.hide();

        }
        function setDate4() {
            var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select from date.";
                return false;
            }

            var date2 = cal4.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid To Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            this.today = depdate;
            var thisTime = this.today.getHours();
            var thisMinutes = this.today.getMinutes();
            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "To Date should be greater than  or equal to date of From (" + date1 + ")";
                return false;
            }

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }
            if (thisMinutes < 10) {
                thisMinutes = "0" + thisMinutes
            }
            if (thisTime > 11) {
                timePM = ("PM")
            } else {
                timePM = ("AM")
            }
            time = (thisTime + ":" + thisMinutes + timePM + " ");

            document.getElementById("<%=txtTo.ClientID%>").value = day + "/" + month + "/" + date2.getFullYear();
            document.getElementById("<%=txtToTime.ClientID%>").value = time;

            cal4.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init1);

        function setDate5() {


        }
        function setDate6() {
        }
        YAHOO.util.Event.addListener(window, "load", init2);

        function setDate7() {
            var date1 = cal7.getSelectedDates()[0];

            document.getElementById('IShimFrame').style.display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();
            var thisTime = this.today.getHours();
            var thisMinutes = this.today.getMinutes();
            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                return false;
            }
            departureDate = cal7.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();
            var time = "";
            var timePM = "";

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }
            if (thisMinutes < 10) {
                thisMinutes = "0" + thisMinutes
            }
            if (thisTime > 11) {
                timePM = ("PM")
            } else {
                timePM = ("AM")
            }
            time = (thisTime + ":" + thisMinutes + timePM + " ");
            document.getElementById("<%=txtPickUpHr.ClientID%>").value = day + "/" + (month) + "/" + date1.getFullYear();
            document.getElementById("<%=txtPickUpMin.ClientID%>").value = time;

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal7.hide();

        }
        function setDate8() {

            var date1 = document.getElementById("<%=txtPickUpHr.ClientID%>").value;

            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select from date.";
                return false;
            }

            var date2 = cal8.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid To Date";
                return false;
            }

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            var thisTime = this.today.getHours();
            var thisMinutes = this.today.getMinutes();
            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "To Date should be greater than  or equal to date of From (" + date1 + ")";
                return false;
            }

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }
            if (thisMinutes < 10) {
                thisMinutes = "0" + thisMinutes
            }
            if (thisTime > 11) {
                timePM = ("PM")
            } else {
                timePM = ("AM")
            }
            time = (thisTime + ":" + thisMinutes + timePM + " ");

            document.getElementById("<%=txtDropUpHr.ClientID%>").value = day + "/" + month + "/" + date2.getFullYear();
            document.getElementById("<%=txtDropUpMin.ClientID%>").value = time;

            cal8.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init3);

        YAHOO.util.Event.addListener(window, "load", init4);

        function validateDtlAddUpdate(action, id) {
            try {

                var msg = '';
                var rowId = id.substring(0, id.lastIndexOf('_'));
                if (action == 'I') {

                    if (document.getElementById(rowId + '_FTtxtLabel').value == '') msg += '\n Label cannot be blank!';
                    if (document.getElementById(rowId + '_FTddlControl').selectedIndex <= 0) msg += '\n Please select a Control! ';
                    if (document.getElementById(rowId + '_FTddlControl').value == 'L') {
                        if (document.getElementById(rowId + '_FTtxtSqlQuery').value == '') msg += 'SqlQuery cannot be blank!';
                    }
                    if (document.getElementById(rowId + '_FTddlDatatype').selectedIndex <= 0) msg += '\n Please select a DataType! ';
                    if (document.getElementById(rowId + '_FTddlMandatory').selectedIndex <= 0) msg += '\n Please select a MandatoryStatus!';
                    if (document.getElementById(rowId + '_FTtxtOrder').value == '') msg += '\n Order cannot be blank!';

                }
                else if (action == 'U')//Update
                {
                    if (document.getElementById(rowId + '_EITtxtLabel').value == '') msg += 'Label cannot be blank!';
                    //if (document.getElementById(rowId + '_EITddlControl').selectedIndex <= 0) msg += '\n Please select a Control! ';  
                    if (document.getElementById(rowId + '_EITddlControl').value == 'L') {
                        if (document.getElementById(rowId + '_EITtxtSqlQuery').value == '') msg += 'SqlQuery cannot be blank!';
                    }
                    //if (document.getElementById(rowId + '_EITddlDatatype').selectedIndex <= 0) msg += '\n Please select a DataType! ';
                    //if (document.getElementById(rowId + '_EITddlMandatory').selectedIndex <= 0) msg += '\n Please select a MandatoryStatus!';
                    if (document.getElementById(rowId + '_EITtxtOrder').value == '') msg += '\n Order cannot be blank!';
                }
                if (msg != '') {
                    //alert(msg);
                    return false;
                }
                return true;
            }
            catch (e) {
                return true;
            }
        }
        function validateDtlAddUpdate1(action, id) {
            try {

                var msg = '';
                var rowId = id.substring(0, id.lastIndexOf('_'));
                if (action == 'I') {
                    if (document.getElementById(rowId + '_FTddlPaxType').selectedIndex <= 0) msg += '\n Please select a PaxType! ';
                    if (document.getElementById(rowId + '_FTtxtLabel').value == '') msg += '\n Label cannot be Blank!';
                    if (document.getElementById(rowId + '_FTtxtStockInHand').value == '') msg += '\n StockInHand cannot be Blank!';
                    if (document.getElementById(rowId + '_FTtxtSupplierCost').value == '') msg += '\n Supplier Cost cannot be Blank!';
                    if (document.getElementById(rowId + '_FTtxtTax').value == '') msg += '\n Tax cannot be Blank!';
                    if (document.getElementById(rowId + '_FTtxtMarkup').value == '') msg += '\n Markup cannot be Blank!';
                    var date = document.getElementById(rowId + '_FTtxtDate').value;
                    var DateArray = date.split('/');
                    if (date.length > 0) {
                        if (!CheckValidDate(DateArray[0], DateArray[1], DateArray[2])) msg += '\n Enter Valid Date!';
                    }
                    else {
                        msg += '\n Select Valid Date!';
                    }
                    
                    
                }
                else if (action == 'U')//Update
                {
                    if (document.getElementById(rowId + '_EITddlPaxType').selectedIndex <= 0) msg += '\n Please select a PaxType! ';
                    if (document.getElementById(rowId + '_EITtxtLabel').value == '') msg += '\n Label cannot be Blank!';
                    if (document.getElementById(rowId + '_EITtxtStockInHand').value == '') msg += '\n StockInHand cannot be Blank!';
                    if (document.getElementById(rowId + '_EITtxtSupplierCost').value == '') msg += '\n Supplier Cost cannot be Blank!';
                    if (document.getElementById(rowId + '_EITtxtTax').value == '') msg += '\n Tax cannot be Blank!';
                    if (document.getElementById(rowId + '_EITtxtMarkup').value == '') msg += '\n Markup cannot be Blank!';
                    var date = document.getElementById(rowId + '_EITtxtDate').value;
                    var DateArray = date.split('/');
                    if (date.length > 0) {
                        if (!CheckValidDate(DateArray[0], DateArray[1], DateArray[2])) msg += '\n Enter Valid Date!';
                    }
                    else {
                        msg += '\n Select Valid Date!';
                    }
                }
                if (msg != '') {
                    //alert(msg);
                    return false;
                }
                return true;
            }
            catch (e) {
                return true;
            }
        }
        function CheckValidDate(Day, Mn, Yr) {
            var DateVal = Mn + "/" + Day + "/" + Yr;
            var dt = new Date(DateVal);

            if (dt.getDate() != Day) {
                return false;
            }
            else if (dt.getMonth() != Mn - 1) {
                //this is for the purpose JavaScript starts the month from 0
                return false;
            }
            else if (dt.getFullYear() != Yr) {
                return false;
            }
            return (true);
        }
        
        
        function trim(stringToTrim) {
            return stringToTrim.replace(/^\s+|\s+$/g, "");
        }

        function Save() {



            document.getElementById('errMess').style.display = "none";
            if (document.getElementById("<%=txtActivityName.ClientID%>").value == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "ActivityName cannot be blank!"
                return false;
            }


            if (document.getElementById("<%=txtStockInHand.ClientID%>").value == "" && false) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Stock in Hand cannot be Blank !";
                return false;
            }

            if (document.getElementById("<%=txtStartingFrom.ClientID%>").value == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "StartingFrom cannot be blank!";
                return false;
            }


           var regValue = eval(document.getElementById('RegionsCurrent').value); // its value is +1 the current
           if (Trim(document.getElementById('<%=RegionsText1.ClientID %>').value) == "0") {

               document.getElementById('errorMessage').innerHTML = "Please select Region for the Fixed Departure.";
               document.getElementById('errMess').style.display = "block";
               return false;
           } else if (Trim(document.getElementById('<%=CountryText1.ClientID %>').value) == "0") {

               document.getElementById('errorMessage').innerHTML = "Please select Country for the Fixed Departure.";
               document.getElementById('errMess').style.display = "block";
               return false;
           }
           else if (Trim(document.getElementById('<%=CityText1.ClientID %>').value) == "0") {

               document.getElementById('errorMessage').innerHTML = "Please select City for the Fixed Departure.";
               document.getElementById('errMess').style.display = "block";
               return false;
           }

           for (var i = 2; i < regValue; i++) {
               //alert(i);
               if (Trim(document.getElementById('RegionsText-' + i).value) == "0") {
                   if (i == 1) {
                       document.getElementById('errorMessage').innerHTML = "Please select Region for the Fixed Departure.";
                       document.getElementById('errMess').style.display = "block";
                       return false;
                   }
                   else {
                       document.getElementById('errorMessage').innerHTML = "Please select all Regions for the Fixed Departure.";
                       document.getElementById('errMess').style.display = "block";
                       return false;
                   }
               } else if (Trim(document.getElementById('CountryText-' + i).value) == "0") {
                   if (i == 1) {
                       document.getElementById('errorMessage').innerHTML = "Please select Country for the Fixed Departure.";
                       document.getElementById('errMess').style.display = "block";
                       return false;
                   }
                   else {
                       document.getElementById('errorMessage').innerHTML = "Please select all Countries for the Fixed Departure.";
                       document.getElementById('errMess').style.display = "block";
                       return false;
                   }
               }
               else if (Trim(document.getElementById('CityText-' + i).value) == "0") {
                   if (i == 1) {
                       document.getElementById('errorMessage').innerHTML = "Please select City for the Fixed Departure.";
                       document.getElementById('errMess').style.display = "block";
                       return false;
                   }
                   else {
                       document.getElementById('errorMessage').innerHTML = "Please select all Cities for the Fixed Departure.";
                       document.getElementById('errMess').style.display = "block";
                       return false;
                   }
               }
           }



            if (document.getElementById("<%=hdfUploadYNImage1.ClientID%>").value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Image 1 should be uploaded !";
                return false;
            }

            if (document.getElementById("<%=hdfUploadYNImage2.ClientID%>").value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Image 2 should be uploaded !";
                return false;
            }

            var chklist = document.getElementById('<%= chkTheme.ClientID %>');
            var chkListinputs = chklist.getElementsByTagName("input");
            var count = 0;
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    count = count + 1;
                }
            }
            if (count == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Atleast one theme should be selected !";
                return false;
            }


            var regexStr = /<\S[^><]*>/g;
            var regexStr1 = /&nbsp;/g;
            var regexStr2 = /<BR>/g;
            var changed = window.frames.mce_editor_0.document.body.innerHTML.replace(regexStr, '');
            var changed = changed.replace(regexStr1, '');
            var changed = changed.replace(regexStr2, '');
            if (trim(changed) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter Intoduction.";
                return false;
            }

            changed = window.frames.mce_editor_1.document.body.innerHTML.replace(regexStr, '');
            var changed = changed.replace(regexStr1, '');
            var changed = changed.replace(regexStr2, '');

            if (trim(changed) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter OverView.";
                return false;
            }


            //          if (document.getElementById("<%=hdfUploadYNImage2.ClientID%>").value =="0") {
            //            document.getElementById('errMess').style.display = "block";
            //            document.getElementById('errorMessage').innerHTML = "Image 2 should be uploaded !";
            //            return false;
            //        }
            //        
            //        
            //          if (document.getElementById("<%=hdfUploadYNImage2.ClientID%>").value =="0") {
            //            document.getElementById('errMess').style.display = "block";
            //            document.getElementById('errorMessage').innerHTML = "Image 2 should be uploaded !";
            //            return false;
            //        }

            /*if (document.getElementById("<%=txtStartFromHr.ClientID%>").value =="") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Start from Hour cannot be Blank !";
            return false;
            }
        
        if (document.getElementById("<%=txtStartTimeMin.ClientID%>").value =="") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Start from Minut cannot be Blank !";
            return false;
            }
        
        if (document.getElementById("<%=txtEndToHr.ClientID%>").value =="") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "End to Hour cannot be Blank !";
            return false;
            }
        
        if (document.getElementById("<%=txtEndTimeMin.ClientID%>").value =="") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "End to Minut cannot be Blank !";
            return false;
            }
        
     
            var startHr= parseInt(document.getElementById("<%=txtStartFromHr.ClientID%>").value);
            //alert(startHr);     
            var startMin= parseInt(document.getElementById("<%=txtStartTimeMin.ClientID%>").value); 
            var endHr= parseInt(document.getElementById("<%=txtEndToHr.ClientID%>").value); 
            var endMin= parseInt(document.getElementById("<%=txtEndTimeMin.ClientID%>").value);
    

     if(startHr >23 || endHr >23)
            {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Invalid Time!";
            document.getElementById("<%=txtDuration.ClientID%>").value = "Invalid Time!";
            return false;
            }
            else if(startMin >59 || endMin >59)
            {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Invalid Time!";
            document.getElementById("<%=txtDuration.ClientID%>").value = "Invalid Time!";
            return false;
            }
            else if(startHr > endHr)
            {    
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Start Time can not be later than End Time!";
            document.getElementById("<%=txtDuration.ClientID%>").value = "Start Time can not be later than End Time!";
            return false;
            }
            else if(startHr==endHr && startMin > endMin)
            {    
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Start Time can not be later than End Time!";
            document.getElementById("<%=txtDuration.ClientID%>").value = "Start Time can not be later than End Time!";
            return false;
            }
        
    



            if (!ValidDate(document.getElementById('<%= txtFrom.ClientID %>').value)) {

                document.getElementById('errorMessage').innerHTML = "Please fill valid Available Start To date";
                document.getElementById('errMess').style.display = "block";
                return false;
            }


            if (document.getElementById("<%=txtTo.ClientID%>").value == "" || document.getElementById("<%=txtFromTime.ClientID%>").value == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Invalid Available Start From !";
                return false;
            } */
               var len=0;
             changed = window.frames.mce_editor_0.document.body.innerHTML.replace(regexStr, '');
            var changed = changed.replace(regexStr1, '');
            var changed = changed.replace(regexStr2, '');
          //alert(trim(changed).length);
            if (trim(changed).length >18) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Intoduction cannot be exceeded 500 charctors .";
                return false;
            }

             changed = window.frames.mce_editor_1.document.body.innerHTML.replace(regexStr, '');
            var changed = changed.replace(regexStr1, '');
            var changed = changed.replace(regexStr2, '');
            
           if (trim(changed).length >4000) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Tour Highlights cannot be exceeded 4000 charctors .";
                return false;
            }

            changed = window.frames.mce_editor_2.document.body.innerHTML.replace(regexStr, '');
            var changed = changed.replace(regexStr1, '');
            var changed = changed.replace(regexStr2, '');

            if (trim(changed) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter Air details.";
                return false;
            }
            else  if (trim(changed).length >4000) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Air Details cannot be exceeded 4000 charctors .";
                return false;
            }
            
            changed = window.frames.mce_editor_3.document.body.innerHTML.replace(regexStr, '');             
            var changed = changed.replace(regexStr1, '');
            var changed = changed.replace(regexStr2, '');

            if (trim(changed) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter Hotel details.";
                return false;
            }
             else  if (trim(changed).length >4000) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Hotel Details cannot be exceeded 4000 charctors .";
                return false;
            }

            changed = window.frames.mce_editor_4.document.body.innerHTML.replace(regexStr, '');             
            var changed = changed.replace(regexStr1, '');
            var changed = changed.replace(regexStr2, '');

            if (trim(changed) == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter Itinerary details.";
                return false;
            }
              else  if (trim(changed).length >4000) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Itinerary Details cannot be exceeded 4000 charctors .";
                return false;
            }
            
            //Itinerary
            var days = document.getElementById('<%=ddlDays.ClientID%>').value;

            if (Trim(document.getElementById('<%=txtItineraryName1.ClientID %>').value) == "") {
                document.getElementById('errorMessage').innerHTML = "Please enter Itinerary Name 1 for the Fixed Departure.";
                document.getElementById('errMess').style.display = "block";
                return false;
            } else if (Trim(document.getElementById('<%=txtMeals1.ClientID %>').value) == "") {
                document.getElementById('errorMessage').innerHTML = "Please enter Meals 1 details for the Fixed Departure.";
                document.getElementById('errMess').style.display = "block";
                return false;
            }
            for (var i = 2; i <= days; i++) {
                //alert(document.getElementById('ctl00_cphTransaction_txtItinerary' + i).value);
                if (Trim(document.getElementById('ctl00_cphTransaction_txtItineraryName' + i).value) == "") {
                    document.getElementById('errorMessage').innerHTML = "Please enter Itinerary Name " + i + " for the Fixed Departure.";
                    document.getElementById('errMess').style.display = "block";
                    return false;
                } else if (Trim(document.getElementById('ctl00_cphTransaction_txtMeals' + i).value) == "") {
                    document.getElementById('errorMessage').innerHTML = "Please enter Meals " + i + " details for the Fixed Departure.";
                    document.getElementById('errMess').style.display = "block";
                    return false;
                } 
//else   if (trim(changed) == "") {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "Please enter Itinerary details.";
//                return false;
            
                
            }
            

            var exclValue = eval(document.getElementById('ExclusionsCurrent').value); // its value is +1 the current


            for (var i = 1; i < exclValue; i++) {
                //alert(i);
                if (Trim(document.getElementById('ExclusionsText-' + i).value) == "") {
                    if (i == 1) {
                        document.getElementById('errorMessage').innerHTML = "Please enter Exclusions for the Activity.";
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                    else {
                        document.getElementById('errorMessage').innerHTML = "Please fill all Exclusions for the Activity.";
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                }
            }


            var incValue = eval(document.getElementById('InclusionsCurrent').value); // its value is +1 the current

            for (var i = 1; i < incValue; i++) {

                if (Trim(document.getElementById('InclusionsText-' + i).value) == "") {
                    if (i == 1) {
                        document.getElementById('errorMessage').innerHTML = "Please enter Inclusions for Activity.";
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                    else {
                        document.getElementById('errorMessage').innerHTML = "Please fill all Inclusions for Activity.";
                        document.getElementById('errMess').style.display = "block";
                        return false;
                    }
                }
            }

            if (document.getElementById("<%=hdfAFCDetails.ClientID%>").value != 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Activity Flex Details in Edit Mode!";
                return false;
            }

            if (document.getElementById("<%=hdfPriceCount.ClientID%>").value == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Price Details cannot be blank!";
                return false;
            }

            if (document.getElementById("<%=hdfPrice.ClientID%>").value != 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Price Details in Edit Mode!";
                return false;
            }

            document.getElementById('savePage').value = "true";
            document.forms[0].submit();

        }

        //    function Save() {
        //        if (getElement('txtActivityName').value == '') addMessage('ActivityName cannot be blank!', '');
        //        if (getElement('txtStartingFrom').value == '') addMessage('StartingFrom cannot be blank!', '');

        //        if (getElement('hdfAFCDetails').value != "0") addMessage('ACF Details in Edit Mode!', '');
        //        if (getElement('hdfPrice').value != "0") addMessage('ACF Price in Edit Mode!', '');

        //        if (getMessage() != '') {
        //            //alert(getMessage());
        //            clearMessage(); return false;
        //        }
        //    }

        //    function OnTimeExit(dcId) {
        //        var rowId = dcId.substring(0, dcId.lastIndexOf('_'));
        //        var svcRowId = rowId.substring(0, rowId.lastIndexOf('_'));
        //        var id = rowId.split("_");
        //        var inDate = "";
        //        var outDate = "";
        //        var unit = "";

        //        if (id[4] == '_dcEndTo') {
        //            inDate = GetDateTimeObject(svcRowId + '_dcStartFrom');
        //            outDate = GetDateTimeObject(rowId);
        //            unit = outDate - inDate;
        //            var noOfDays = ((outDate.getTime() - inDate.getTime()) / (1000 * 60 * 60 * 24));
        //            unit = Math.floor(noOfDays / 24);
        //            document.getElementById(svcRowId + '_txtDuration').value = unit;
        //        }

        //    }
        // 

        function AddTextBox(divName) {
            //alert(divName);
            var idCount = eval(document.getElementById(divName + 'Counter').value);
            var idCurrent = eval(document.getElementById(divName + 'Current').value);
            //alert(idCount);
            if (document.getElementById(divName + "-" + idCurrent)) {
                document.getElementById(divName + "-" + idCurrent).style.display = "block";
                document.getElementById(divName + "Img-" + idCurrent).style.display = "block";
                Remove(divName + "Img-" + eval(idCurrent - 1), 'img');
                idCurrent++;
                document.getElementById(divName + 'Current').value = idCurrent;
            }
            else {

                var stringHTML = "";
                var newdiv = document.createElement('div');
                if (divName == "Inclusions") {
                    stringHTML += "<p class=\"fleft width-100 padding-top-5\" style=\"padding-left:160px;\" id=\"" + divName + "-" + idCount + "\">";
                }
                if (divName == "CancellPolicy" || divName == "ThingsToBring" || divName == "Exclusions") {
                    stringHTML += "<p class=\"fleft width-100 padding-top-5\" style=\"padding-left:160px;\" id=\"" + divName + "-" + idCount + "\">";
                }
                if (divName == "Regions") {
                    
                    stringHTML += "<p class=\"fleft width-100 padding-top-5\" style=\"padding-left:160px;\" id=\"" + divName + "-" + idCount + "\">";
                    stringHTML += "<span class=\"fleft width-300\"><select id=\"" + divName + "Text-" + idCount + "\" name=\"" + divName + "Text-" + idCount + "\" class=\"width-300 fleft\" onchange=\"LoadCountries('" + divName + "Text-" + idCount + "')\"></select>";
                    stringHTML += "<select id=\"CountryText-" + idCount + "\" name=\"CountryText-" + idCount + "\" class=\"width-300 fleft\" onchange=\"LoadCities('CountryText-" + idCount + "')\"><option selected='selected' value='0'>Select Country</option></select>";
                    stringHTML += "<select id=\"CityText-" + idCount + "\" name=\"CityText-" + idCount + "\" class=\"width-300 fleft\" onchange=\"document.getElementById('<%=hdfSelCity.ClientID %>').value = document.getElementById('ctl00_cphTransaction_CityText1').value;\" ><option selected='selected' value='0'>Select City</option></select></span>";
                    stringHTML += "<i class=\"fleft margin-left-10\" id=\"" + divName + "Img-" + idCount + "\"><img src=\"Images/delete.gif\" alt=\"Remove\" onclick=\"javascript:Remove('" + divName + "-" + idCount + "','text')\"/></i>";
                    stringHTML += "</p>"
                }
                else {
                    stringHTML += "<span class=\"fleft width-300\"><input type=\"text\" id=\"" + divName + "Text-" + idCount + "\" name=\"" + divName + "Text-" + idCount + "\" class=\"width-300 fleft\" /></span>";
                    stringHTML += "<i class=\"fleft margin-left-10\" id=\"" + divName + "Img-" + idCount + "\"><img src=\"Images/delete.gif\" alt=\"Remove\" onclick=\"javascript:Remove('" + divName + "-" + idCount + "','text')\"/></i>";
                    stringHTML += "</p>";
                }
                newdiv.innerHTML = stringHTML;
                document.getElementById(divName + "Div").appendChild(newdiv)
                //document.getElementById(divName+"Div").innerHTML += stringHTML;
                if (divName == "Regions") {
                    LoadRegions(divName + "Text-" + idCount);
                }
                Remove(divName + "Img-" + eval(idCount - 1), 'img');
                
                idCount++;
                document.getElementById(divName + 'Counter').value = idCount;
                idCurrent++;
                document.getElementById(divName + 'Current').value = idCurrent;

                if (divName == "Inclusions") {
                    document.getElementById("<%=hdfinclCounter.ClientID%>").value = idCount;
                }
                else if (divName == "Exclusions") {
                    document.getElementById("<%=hdfExclCounter.ClientID%>").value = idCount;
                }
                else if (divName == "CancellPolicy") {
                    document.getElementById("<%=hdfCanPolCounter.ClientID%>").value = idCount;
                }
                else if (divName == "ThingsToBring") {
                    document.getElementById("<%=hdfThingsCounter.ClientID%>").value = idCount;
                }
                else if (divName == "Regions") {
                    document.getElementById("<%=hdfRegionsCounter.ClientID %>").value = idCount;
                }

                
                //        //alert(document.getElementById('InclusionsCounter').value);
            }
        }

        function AddDateMore(divName) {
            var idCount = eval(document.getElementById(divName + 'Counter').value);
            var idCurrent = eval(document.getElementById(divName + 'Current').value);
            if (document.getElementById(divName + "-" + idCurrent)) {
                document.getElementById(divName + "-" + idCurrent).style.display = "block";
                document.getElementById(divName + "Img-" + idCurrent).style.display = "block";
                Remove(divName + "Img-" + eval(idCurrent - 1), 'img');
                idCurrent++;
                document.getElementById(divName + 'Current').value = idCurrent;
            }
            else {

                var stringHTML = "";
                var newdiv = document.createElement('div');
                if (divName == "UnAvailDates") {
                    stringHTML += "<p class=\"fleft width-100 padding-top-5\" style=\"padding-left:160px;\" id=\"" + divName + "-" + idCount + "\">";
                }
                stringHTML += "<span class=\"fleft width-300\"><input type=\"text\" id=\"txt" + divName + "Text-" + idCount + "\" name=\"txt" + divName + "Text-" + idCount + "\" class=\"width-300 fleft\" /></span>";
                stringHTML += "<i class=\"fleft margin-left-10\" id=\"" + divName + "Img-" + idCount + "\"><img src=\"Images/delete.gif\" alt=\"Remove\" onclick=\"javascript:Remove('" + divName + "-" + idCount + "','text')\"/></i>";
                stringHTML += "</p>";
                newdiv.innerHTML = stringHTML;
                document.getElementById(divName + "Div").appendChild(newdiv)
                //document.getElementById(divName+"Div").innerHTML += stringHTML;       
                Remove(divName + "Img-" + eval(idCount - 1), 'img');

                idCount++;
                document.getElementById(divName + 'Counter').value = idCount;
                idCurrent++;
                document.getElementById(divName + 'Current').value = idCurrent;
                if (divName == "UnAvailDates") {
                    document.getElementById("<%=hdfUnavailDateCounter.ClientID%>").value = idCount;
                }

            }
        }

        //   function RemoveAll(id, count)
        //  {
        //  //alert('hi');
        //alert(id);
        //alert(count);
        //    for(var i=1; i<=count;i++)
        //    {
        //         document.getElementById(id+i).style.display="block";   
        //    }
        //        
        //  }
        function Remove(id, val) {

            if (id.split('-')[1] != 1) {
                //alert(document.getElementById(id));
                document.getElementById(id).value = "";
                document.getElementById(id).style.display = "none";
                //alert(id);
                if (val == "text") {

                    var count = eval(id.split('-')[1]);
                    var idSplit = id.split('-')[0];
                    //alert(id);

                    if (eval(count - 1) != 1) {
                        document.getElementById(idSplit + "Img-" + eval(count - 1)).style.display = "block";

                    }
                    var idCurrent = eval(document.getElementById(id.split('-')[0] + 'Current').value);
                    idCurrent--;
                    document.getElementById(id.split('-')[0] + 'Current').value = idCurrent;

                    if (idSplit == "Inclusions") {
                        document.getElementById("<%=hdfinclCounter.ClientID%>").value = idCurrent;
                    }
                    else if (idSplit == "Exclusions") {
                        document.getElementById("<%=hdfExclCounter.ClientID%>").value = idCurrent;
                    }
                    else if (idSplit == "CancellPolicy") {
                        document.getElementById("<%=hdfCanPolCounter.ClientID%>").value = idCurrent;
                    }
                    else if (idSplit == "ThingsToBring") {
                        document.getElementById("<%=hdfThingsCounter.ClientID%>").value = idCurrent;
                    }
                    else if (idSplit == "UnAvailDates") {
                        document.getElementById("<%=hdfUnavailDateCounter.ClientID%>").value = idCurrent;
                    }
                    else if (idSplit == "Regions") {
                    document.getElementById("<%=hdfRegionsCounter.ClientID %>").value = idCurrent;
                    }

                }
            }
        }

        function CalcDuration(hrOrMin) {

            var startHr = parseInt(document.getElementById("<%=txtStartFromHr.ClientID%>").value);
            //alert(startHr);     
            var startMin = parseInt(document.getElementById("<%=txtStartTimeMin.ClientID%>").value);
            var endHr = parseInt(document.getElementById("<%=txtEndToHr.ClientID%>").value);
            var endMin = parseInt(document.getElementById("<%=txtEndTimeMin.ClientID%>").value);


            if (startHr > 23 || endHr > 23) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Invalid Time!";
                document.getElementById("<%=txtDuration.ClientID%>").value = "Invalid Time!";
                return false;
            }
            else if (startMin > 59 || endMin > 59) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Invalid Time!";
                document.getElementById("<%=txtDuration.ClientID%>").value = "Invalid Time!";
                return false;
            }
            else if (startHr > endHr) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Start Time can not be later than End Time!";
                document.getElementById("<%=txtDuration.ClientID%>").value = "Start Time can not be later than End Time!";
                return false;
            }
            else if (startHr == endHr && startMin > endMin) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Start Time can not be later than End Time!";
                document.getElementById("<%=txtDuration.ClientID%>").value = "Start Time can not be later than End Time!";
                return false;
            }
            else {
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
            }

            //alert(parseInt(endHr)+parseInt(startHr));
            var durationHr = endHr - startHr;
            var durationMin = endMin - startMin;
            if (durationMin < 0) {
                durationHr = durationHr - 1
                durationMin = 60 + durationMin;
            }
            var duration = durationHr + 'Hrs ' + durationMin + 'Min';
            //alert(duration);   
            document.getElementById("<%=txtDuration.ClientID%>").value = duration;
            //document.getElementById("<%=txtDuration.ClientID%>").disabled=true;
        }

        function ValidateTime(hrOrMin, value, id) {
            if (hrOrMin == 'H' && value > 23) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Invalid Time!";
                document.getElementById(id).value = "";
                return false;
            }
            else if (hrOrMin == 'M' && value > 59) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Invalid Time!";
                document.getElementById(id).value = "";
                return false;
            }
            else {
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
            }
        }

        function getTotalAmount(rowId, action) {
            var id = rowId.substring(0, rowId.lastIndexOf('_') + 1);
            //var quantity = parseFloat(isNaN(document.getElementById(id+'FTtxtQty').value));// document.getElementById(id+'FTtxtQty').value;
            var supplierCost = 0;
            var tax = 0;
            var markup = 0;
            var totalAmount = 0;
            if (action == "I") {
                supplierCost = parseFloat((isNaN(document.getElementById(id + 'FTtxtSupplierCost').value) || document.getElementById(id + 'FTtxtSupplierCost').value == '') ? '0' : document.getElementById(id + 'FTtxtSupplierCost').value);
                tax = parseFloat((isNaN(document.getElementById(id + 'FTtxtTax').value) || document.getElementById(id + 'FTtxtTax').value == '') ? '0' : document.getElementById(id + 'FTtxtTax').value);
                markup = parseFloat((isNaN(document.getElementById(id + 'FTtxtMarkup').value) || document.getElementById(id + 'FTtxtMarkup').value == '') ? '0' : document.getElementById(id + 'FTtxtMarkup').value);

                totalAmount = supplierCost + tax + markup;
                document.getElementById(id + 'FTtxtAmount').value = totalAmount;
                document.getElementById(id + 'FTtxtAmount').value = totalAmount.toFixed(2);
                //alert(document.getElementById(id+'FTtxtRate').value);
            }
            else if (action == "U") {

                supplierCost = parseFloat((isNaN(document.getElementById(id + 'EITtxtSupplierCost').value) || document.getElementById(id + 'EITtxtSupplierCost').value == '') ? '0' : document.getElementById(id + 'EITtxtSupplierCost').value);
                tax = parseFloat((isNaN(document.getElementById(id + 'EITtxtTax').value) || document.getElementById(id + 'EITtxtTax').value == '') ? '0' : document.getElementById(id + 'EITtxtTax').value);
                markup = parseFloat((isNaN(document.getElementById(id + 'EITtxtMarkup').value) || document.getElementById(id + 'EITtxtMarkup').value == '') ? '0' : document.getElementById(id + 'EITtxtMarkup').value);

                totalAmount = supplierCost + tax + markup;
                document.getElementById(id + 'EITtxtAmount').value = totalAmount;
                document.getElementById(id + 'EITtxtAmount').value = totalAmount.toFixed(2);


            }


        }

        function restrictNumeric(fieldId, kind,e) {
            try {
                return (maskNumeric(fieldId, (kind == '3' || kind == '4' ? 'false' : 'true'), (kind == '1' || kind == '3' ? 'true' : 'false'),e))
            }
            catch (err) {
                showError(err, 'Validation', 'restrictNumeric'); return (false)
            } 
        }
        function maskNumeric(fieldId, ignoreNegative, IgnoreDecimal,e) {
            var key; var keychar
            if (ignoreNegative == null) ignoreNegative = 'true'
            if (IgnoreDecimal == null) IgnoreDecimal = 'true'
//            if (window.event) {
//                if (navigator.appName.substring(0, 1) == 'M') key = window.event.keyCode
//                else key = window.event.charCode
//            }
//            else if (event) key = event.which
            //            else return true
            if (!e) var e = window.event
            if (e.keyCode) key = e.keyCode;
            else if (e.which) key = e.which;
            keychar = String.fromCharCode(key)
            if ((key == null) || (key == 0) || (key == 8) || (key == 9) || (key == 13) || (key == 27)) return true
            var strSet = "0123456789" + (ignoreNegative == 'true' ? '' : '-') + (IgnoreDecimal == 'true' ? '' : '.')
            if ((strSet.indexOf(keychar) > -1)) {
                var inputbox = document.getElementById(fieldId)
                if (ignoreNegative == 'false' && key == 45) {
                    if (inputbox.value.indexOf('-') == -1) inputbox.value = '-' + inputbox.value
                    return (false)
                }
                if (IgnoreDecimal == 'false' && inputbox.value.indexOf('.') > -1 && key == 46) return (false)
                return true
            }
            return (false)
        }


        function ValidDate(text) {
            var textArray = text.split('/')
            if (textArray.length != 3 || textArray.length != 3) {
                return false;
            }
            if (!CheckValidDate(textArray[0], textArray[1], textArray[2])) {
                return false;
            }
            return true;
        }
        function showimag1(id, imgid, hdfid, hideid) {
            //    var div = document.getElementById(id);
            //    if (div.style.display == "block") {
            //        div.style.display = "none";
            //    }
            //    else {
            //        div.style.display = "block";;
            //    }
            //    document.getElementById(imgid).src = document.getElementById(hdfid).value;
            window.open(document.getElementById(hdfid).value);
            return false;
        }

 function showImage(stat,img)
        {       
         
            if(stat=="S")
            {   
                         
               //alert(document.getElementById('<%=pnlImages.ClientID %>'));
                document.getElementById('<%=pnlImages.ClientID %>').style.display="block";
                
                if(img=="1")  
                {                
                document.getElementById('<%=img1PreView.ClientID %>').style.display="block";
                document.getElementById('<%=img2PreView.ClientID %>').style.display="none";
                document.getElementById('<%=img3PreView.ClientID %>').style.display="none";
                }
                else if(img=="2")
                {
                document.getElementById('<%=img1PreView.ClientID %>').style.display="none";
                document.getElementById('<%=img2PreView.ClientID %>').style.display="block";
                document.getElementById('<%=img3PreView.ClientID %>').style.display="none";
                }
                else 
                {
                document.getElementById('<%=img1PreView.ClientID %>').style.display="none";
                document.getElementById('<%=img2PreView.ClientID %>').style.display="none";
                document.getElementById('<%=img3PreView.ClientID %>').style.display="block";
                }
            }
            else
            {
                document.getElementById('<%=pnlImages.ClientID %>').style.display="none";
                document.getElementById('<%=img1PreView.ClientID %>').style.display="none";
                document.getElementById('<%=img2PreView.ClientID %>').style.display="none";
                document.getElementById('<%=img3PreView.ClientID %>').style.display="none";
                return false;
            }
            
        }
 
    </script>
    
    <style> 
    
    .box28 { border: solid 1px #ccc; padding: 4px 10px 4px 10px; }
    
    .heading28 { background:#275eae; color:#fff; padding: 4px 10px 4px 10px; font-weight:bold  }
    
   .width-60p { width:60%; }
   
   
   
   .GridPager { font-family:Arial; font-size:13px;    }
   
   .GridPager th { font-family:Arial; font-weight:bold; background:#f5f4f5;  padding: 4px 4px 0px 5px;   border-color:#ccc;  }
   
      .GridPager td { font-family:Arial;  padding: 4px 4px 0px 5px;  border-color:#ccc; }
   
    
    
    
    </style>

    <link href="Styles/Default.css" rel="stylesheet" type="text/css" />
    <asp:HiddenField runat="server" ID="hdfImage1" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage2" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage3" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage1Extn" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage2Extn" Value="0" />
    <asp:HiddenField runat="server" ID="hdfImage3Extn" Value="0" />
    <asp:HiddenField runat="server" ID="hdfUploadYNImage1" Value="0" />
    <asp:HiddenField runat="server" ID="hdfUploadYNImage2" Value="0" />
    <asp:HiddenField runat="server" ID="hdfUploadYNImage3" Value="0" />
    <input type="hidden" id="savePage" name="savePage" value="false" />
    <asp:Label runat="server" Style="color: Red" ID="lblError" Visible="false" Text=""></asp:Label>
    <%--<div id="errMess" class="error_module" style="display:none;"> <div id="errorMessage" style="float:left; color:Red;" class="padding-5 yellow-back width-100 center margin-top-5"> </div></div>--%>
    <div class="clear" style="margin-left: 30px">
        <div id="container1" style="position: absolute; top: 220px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container3" style="position: absolute; top: 292px;  left: 430px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container5" style="position: absolute; top: 220px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container7" style="position: absolute; top: 220px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 252px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container4" style="position: absolute; top: 292px; left: 700px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container6" style="position: absolute; top: 252px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container8" style="position: absolute; top: 252px; left: 215px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="callContainer" style="position: absolute; top: 380px; left: 360px; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="cal2Container" style="position: absolute; top: 200px; left: 370px; display: none;">
        </div>
    </div>
    <center>
        <div class="heading28 margin-top-10"> Fixed Departure </div>
                
                
                
                <div class="box28">
                      <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        
                        <tr>
                            <td>
                            </td>
                            <td align="left" style="width: 521px">
                                <asp:Label ID="lblMessage" runat="server" Font-Bold="true" Width="250"></asp:Label>
                            </td>
                        </tr>
                        
                        <tr>
                       <td height="23px" align="right">
                                <asp:Label ID="lblAgent" runat="server" Text="Agent Name:"> </asp:Label><sup>*</sup>
                            </td>
                        <td><asp:DropDownList ID="ddlAgent" CssClass="width-300 fleft" runat="server" Width="150px">
                                             <asp:ListItem Value="0" Text="Select Agent"></asp:ListItem>
                                             </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td height="23px" align="right">
                                <asp:Label ID="lblActivityName" runat="server" Text="Tour Name:"> </asp:Label><sup>*</sup>
                            </td>
                            <td align="left" style="width: 521px">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:TextBox ID="txtActivityName" CssClass="inp_22_1" runat="server" Width="200px"></asp:TextBox>
                                        </td>
                                        <td>
                                        </td>
                                        <td height="23px" align="right">
                                            <asp:Label ID="lblDays" runat="server" Text="Days:"> </asp:Label>
                                        </td>
                                        <td align="left">
                                            <asp:DropDownList ID="ddlDays" CssClass="inputDdlEnabled" runat="server" Width="50px"
                                                onchange="ShowItinerary()">
                                                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                            </asp:DropDownList>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr style="display:none">
                            <td height="23px" align="right">
                                <asp:Label ID="lblStockInHand" runat="server" Text="StockInHand:"> </asp:Label><sup>*</sup>
                            </td>
                            <td align="left" style="width: 521px">
                                <asp:TextBox ID="txtStockInHand" CssClass="inp_22_1" runat="server" Width="200px"
                                    onkeypress="return restrictNumeric(this.id,'0',event);"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td height="23px" align="right">
                                <asp:Label ID="lblStartingFrom" runat="server" Text="Starting From:"> </asp:Label><sup>*</sup>
                            </td>
                            <td align="left" style="width: 521px">
                                <asp:TextBox ID="txtStartingFrom" CssClass="inp_22_1" runat="server" Width="200px"
                                    onkeypress="return restrictNumeric(this.id,'2',event);"></asp:TextBox>
                            </td>
                        </tr>
                        
                        </table>
                  <%--  <div style="display: none;">
                                    <input type="hidden" id="Hidden1" value="<%=activityVariables["RegionsCount"] %>" />
                                    <input type="hidden" id="Hidden2" name="RegionsCurrent" value="<%=activityVariables["RegionsCount"] %>" />
                                </div>    
                --%>
                 </div>
    
    
    
                 <div class="heading28 margin-top-10"> Regions </div>
                  <div class="box28">
                  
                        <div style="display: none;">
                                    <input type="hidden" id="RegionsCounter" value="<%=activityVariables["RegionsCount"] %>" />
                                    <input type="hidden" id="RegionsCurrent" name="RegionsCurrent" value="<%=activityVariables["RegionsCount"] %>" />
                                </div>
                             <div style=" margin-left:170px;">
                               
                                 <div id="RegionsDiv">
                                    <%-- <p><b class="fleft" style="width: 160px">Regions:<sup>*</sup></b></p>--%>
                                      <%for (int i = 1; i < Convert.ToInt32(activityVariables["RegionsCount"]); i++)
                                        {
                                            

                                            if (i == 1)
                                            { %>
                                  
                                     <p class="fleft width-60p padding-top-5" style="padding-left: 160px">
                                         <span style="width: 470px" class="fleft">
                                             <asp:DropDownList ID="RegionsText1" CssClass="width-300 fleft" runat="server" Width="300px" AppendDataBoundItems="true" onchange="LoadCountries(this.id)">
                                             <asp:ListItem Value="0" Text="Select Region"></asp:ListItem>
                                             </asp:DropDownList>
                                                    
                                         </span>
                                     
                                         <span style="width: 470px" class="fleft">
                                         
                                             
                                                    <asp:DropDownList ID="CountryText1" CssClass="width-300 fleft" runat="server" Width="300px"  onchange="LoadCities(this.id)">
                                                    <asp:ListItem Selected="True" Value="0" Text="Select Country"></asp:ListItem>
                                             </asp:DropDownList>
                                         </span>
                                     
                                     <span style="width: 470px" class="fleft">
                                     
                                     
                                                    <asp:DropDownList ID="CityText1" CssClass="width-300 fleft" Width="300px" runat="server" onchange="SetCity(this.id)">
                                                    <asp:ListItem Selected="True" Value="0" Text="Select City"></asp:ListItem>
                                             </asp:DropDownList>
                                     </span>
                                     </p>
                                     
                                     <%}
                                                else
                                                {%>
                                                <div class="fleft width-100 padding-top-5" style="padding-left: 160px" id="Regions-<%=i %>">
                                                <p class="fleft">
                                         <span style="width: 470px" class="fleft">
                                         
                                             <select id="RegionsText-<%=i %>" name="RegionsText-<%=i %>" 
                                                    class="width-300 fleft" onchange="LoadCountries(this.id)">
                                                    
                                                    </select>
                                                    
                                         </span>
                                     </p>
                                     <p class="fleft">
                                         <span style="width: 470px" class="fleft">
                                         
                                             <select  id="CountryText-<%=i %>" name="CountryText-<%=i %>"
                                                    class="width-300 fleft" onchange="LoadCities(this.id)" >
                                                    <option value="0">Select Country</option>
                                                    </select>
                                         </span>
                                     </p>
                                     <p class="fleft">
                                     <span style="width: 470px" class="fleft">
                                     
                                     <select id="CityText-<%=i %>" name="CityText-<%=i %>"
                                                    class="width-300 fleft"  onchange="SetCity(this.id)">
                                                    <option value="0" >Select City</option>
                                                    </select>
                                     </span>
                                     </p>
                                     <%if (i < Convert.ToInt32(activityVariables["RegionsCount"]) - 1)
                                       {%>
                                                <i style="display: none;" class="fleft margin-left-10" id="RegionsImg-<%=i %>">
                                                <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Regions-<%=i %>','text')" /></i>
                                                  
                                            <%}
                                       else
                                       { %>
                                       <i class="fleft margin-left-10" id="RegionsImg-<%=i %>">
                                                <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Regions-<%=i %>','text')" /></i>
                                               
                                                <%} %>
                                             </div>
                                            <%}%>
                                            <%}%>
                                 </div>
                                
                                 
                             </div>
                             
                              <div class="clear"></div>
                             <div> 
                             
                                 
                                 <input type="button" value="Add" onclick="javascript:AddTextBox('Regions')" />
                                 
                                 </div>
                            
                    
                  </div>
                  
                      
                     <div class="heading28 margin-top-10"> Upload Images </div>
       
                
                <div class="box28">
                <table border="0" >
                <tr align="center">
                <td style="width:200px"></td>
                <td>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    
                   <tr align="Center">
                            <td height="28" align="right">
                                Image1(145X120):<sup>*</sup>
                            </td>
                            <td align="left" style="width: 380px">
                                <table >
                                <tr></tr>
                                    <tr>
                                        <td>
                                              <asp:UpdatePanel ID="UpdatePanel1_fuImage1" runat="server">
                                               <ContentTemplate>
                                              <asp:FileUpload ID="fuImage1" runat="server" Width="200px"/>   
                                              </ContentTemplate>
                                              <Triggers>
                                               <asp:PostBackTrigger ControlID="btnUpload1" />
                                               </Triggers>
                                              </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnUpload1" runat="server" Text="Upload" OnClick="btnUpload1_Click" />
                                        </td>
                                        <td>
                                        <asp:UpdatePanel ID="UpdatePanel_lbUploadMsg" runat="server">
                                               <ContentTemplate>
                                             <asp:LinkButton ID="lbUploadMsg" runat="server" OnClick="lbUploadMsg_Click"></asp:LinkButton>
                                              </ContentTemplate>
                                              <Triggers>
                                               <asp:PostBackTrigger ControlID="lbUploadMsg" />
                                               </Triggers>
                                              </asp:UpdatePanel>
                                            
                                            <asp:Label ID="lblUpload1" runat="server" Visible="false"></asp:Label>
                                           <%-- <div id="dUpload1Msg" style="display: none;">
                                                <img id="imgupload1" height="50px" width="50px" />
                                            </div>--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="28" align="right">
                                Image2(625X250):<sup>*</sup>
                            </td>
                            <td align="left" style="width: 380px">
                                <table>
                                    <tr>
                                        <td>
                                            <asp:UpdatePanel ID="UpdatePanel2_fuImage2" runat="server">
                                            <ContentTemplate>
                                            <asp:FileUpload ID="fuImage2" runat="server" Width="200px"/>
                                            </ContentTemplate>
                                           <Triggers>
                                              
                                               <asp:PostBackTrigger ControlID="btnUpload2" />
                                               
                                              </Triggers>
                                            </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnUpload2" runat="server" Text="Upload" OnClick="btnUpload2_Click" />
                                        </td>
                                        <td>
                                        <asp:UpdatePanel ID="UpdatePanel_lbUpload2Msg" runat="server">
                                        <ContentTemplate>
                                        <asp:LinkButton ID="lbUpload2Msg" runat="server" OnClick="lbUpload2Msg_Click"></asp:LinkButton>
                                        </ContentTemplate>
                                        <Triggers>
                                        <asp:PostBackTrigger ControlID="lbUpload2Msg" />
                                        </Triggers>
                                        </asp:UpdatePanel>
                                            
                                            <asp:Label ID="lblUplaod2" runat="server" Visible="false"></asp:Label>
                                            <%--<div id="dUpload2Msg" style="display: none;">
                                                <img id="imgupload2" height="50px" width="50px" />
                                            </div>--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="28" align="right">
                                Image3(145X120):
                            </td>
                            <td align="left" style="width: 380px">
                                <table border="0">
                                    <tr>
                                        <td>
                                           
                                             <asp:UpdatePanel ID="UpdatePanel_fuImage3" runat="server">
                                                  <ContentTemplate>
                                                  <asp:FileUpload ID="fuImage3" runat="server" Width="200px"/>
                                                  </ContentTemplate>
                                                  <Triggers>
                                               
                                               <asp:PostBackTrigger ControlID="btnUpload3" />
                                               
                                              </Triggers>
                                                  </asp:UpdatePanel>
                                        </td>
                                        <td>
                                            <asp:Button ID="btnUpload3" runat="server" Text="Upload" OnClick="btnUpload3_Click" />
                                        </td>
                                        <td>
                                        <asp:UpdatePanel ID="UpdatePanel_lbUpload3Msg" runat="server">
                                                  <ContentTemplate>
                                                  <asp:LinkButton ID="lbUpload3Msg" runat="server" OnClick="lbUpload3Msg_Click"></asp:LinkButton>
                                                  </ContentTemplate>
                                                  <Triggers>
                                               
                                               <asp:PostBackTrigger ControlID="lbUpload3Msg" />
                                               
                                              </Triggers>
                                                  </asp:UpdatePanel>
                                            
                                            <asp:Label ID="lblUpload3" runat="server" Visible="false"></asp:Label>
                                           <%-- <div id="dUpload3Msg" style="display: none;">
                                                <img id="imgupload3" height="50px" width="50px" />
                                            </div>--%>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                    </td>
                   <td style="width:100px">
                                <div id="pnlImages" runat="server" style="display:none">
                                <%--<asp:Panel id="pnlImages" runat="server"  style="position:fixed;right:250px;display:none" >--%>
                                           <div class="thepet pdac ">
                                                <table border="0" style="color:Blue" ><tr><td>
                                            <h3 style="background-color:Gray;" >
                                                Preview </h3></td></tr>
                                            <tr><td>
                                                        <asp:Image  runat="server"  style="height:50px;width:80px;display:block" id="img1PreView"  />
                                                        <asp:Image  runat="server" style="height:50px;width:80px;display:block" id="img2PreView"  />
                                                        <asp:Image  runat="server" style="height:50px;width:80px;display:block" id="img3PreView"  />
                                                        </td>
                                                        </tr>
                                                        <tr><td align="right">
                                                         <asp:UpdatePanel ID="UpdatePanel1" runat="server">
                                                  <ContentTemplate>
                                                 <asp:Button OnClientClick="showImage('H','')" runat="server" id="btnClose" Text="Close" style="color:Blue;background-color:Gray" CssClass="button" /> 
                                                  </ContentTemplate>
                                                  <Triggers>
                                               
                                               <asp:PostBackTrigger ControlID="btnClose" />
                                               
                                              </Triggers>
                                                  </asp:UpdatePanel>
                                                         
                                                         </td>
                                                         </tr>
                                                </table>
                                            </div>
                                            <%--</asp:Panel>--%>
                                            </div></td>
                    </tr>
                    </table>
                    
                        </div>
                 
                 
                      <div class="heading28 margin-top-10"><asp:Label ID="lblTheme" runat="server" Text="Select Theme"></asp:Label> </div>
                
                
                
                <div class="box28">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <asp:CheckBoxList ID="chkTheme" CssClass="padding-10" runat="server" RepeatDirection="Horizontal">
                                </asp:CheckBoxList>
                    
                     </table>
                    
                        </div>
    
    
    <div class="heading28 margin-top-10"><asp:Label ID="Label64" runat="server" Text="Select Inclusions"></asp:Label> </div>
    <div class="box28">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <asp:CheckBoxList ID="chkInclusions" CssClass="padding-10" runat="server" RepeatDirection="Horizontal">
                    <asp:ListItem Value="A" Text="AirFare"></asp:ListItem>
                    <asp:ListItem Value="H" Text="Hotel Accomodation"></asp:ListItem>
                    <asp:ListItem Value="T" Text="Travel Insurance"></asp:ListItem>
                    <asp:ListItem Value="V" Text="Visa Facilitation"></asp:ListItem>
                    <asp:ListItem Value="S" Text="Sight Seeing"></asp:ListItem>
                    <asp:ListItem Value="R" Text="Airport Transfers"></asp:ListItem>
                    <asp:ListItem Value="C" Text="Cruise Ride"></asp:ListItem>
                    <asp:ListItem Value="M" Text="Meals"></asp:ListItem>
                    <asp:ListItem Value="P" Text="Park Entry Fees"></asp:ListItem>
                                </asp:CheckBoxList>
                    
                     </table>
    </div>
    
       <div class="heading28 margin-top-10"> <asp:Label ID="lblIntroduction" runat="server" Text="Introduction"></asp:Label> </div>

                <div class="box28">
                
                <asp:TextBox ID="txtIntroduction" CssClass="inp_22_1" runat="server" Width="250px"
                                    TextMode="MultiLine" Height="100"></asp:TextBox>
               
                  </div>
                        

                    
       <div class="heading28 margin-top-10"> <asp:Label ID="lblOverView" runat="server" Text="Tour Highlights"></asp:Label> </div>
 
                
                <div class="box28">
                <asp:TextBox ID="txtOverView" CssClass="inp_22_1" runat="server" Width="250px" TextMode="MultiLine"
                                    Height="100"></asp:TextBox>
                
                       </div>
    
       <div class="heading28 margin-top-10"> <asp:Label ID="lblAir" runat="server" Text="Air"></asp:Label> </div>
 
                
                <div class="box28">
                <asp:TextBox ID="txtAir" CssClass="inp_22_1" runat="server" Width="250px" TextMode="MultiLine"
                                    Height="100"></asp:TextBox>
                
                       </div>
                       
       <div class="heading28 margin-top-10"> <asp:Label ID="lblHotel" runat="server" Text="Hotel"></asp:Label> </div>
 
                
                <div class="box28">
                <asp:TextBox ID="txtHotel" CssClass="inp_22_1" runat="server" Width="250px" TextMode="MultiLine"
                                    Height="100"></asp:TextBox>
                
                       </div>
    
    <div class="heading28 margin-top-10" style="display:none">Available Dates </div>
    
    
    <div class="box28" style="display:none">
     <div>             <table width="100%" border="0" cellspacing="0" cellpadding="0">
    
    <tr style="display: none">
                            <td height="23px" align="right">
                                <label>
                                    StartFrom :<sup>*</sup></label>
                            </td>
                            <td align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <p class="fleft car-search">
                                                <span class="fleft margin-top-3 margin-left-5">
                                                    <asp:TextBox ID="txtStartFromHr" runat="server" Width="30" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0',event);"
                                                        onchange=" CalcDuration()"> </asp:TextBox>
                                                    <asp:TextBox ID="txtStartTimeMin" runat="server" Width="30" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0',event);"
                                                        onchange="CalcDuration()"></asp:TextBox>
                                                    <%--<input  name="FromDate" type="text" value="DD/MM/YYYY" id="FromDate" />--%></span>
                                                <%--<a href="javascript:void()" onclick="showCalendar1()"><img src="images/cal.gif" class="margin-top-8 margin-left-5" alt="Pick Date" /></a>--%>
                                            </p>
                                        </td>
                                        <td>
                                            <td height="23px" align="right">
                                                <label>
                                                    EndTo :<sup>*</sup></label>
                                            </td>
                                        </td>
                                        <td>
                                            <p class="fleft car-search">
                                                <span class="fleft margin-top-3 margin-left-5">
                                                    <asp:TextBox ID="txtEndToHr" runat="server" Width="30" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0',event);"
                                                        onchange="CalcDuration()"></asp:TextBox>
                                                    <asp:TextBox ID="txtEndTimeMin" runat="server" Width="30" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0',event);"
                                                        onchange="CalcDuration()"></asp:TextBox>
                                                    <%--<input  name="FromDate" type="text" value="DD/MM/YYYY" id="FromDate" />--%></span>
                                                <%--<a href="javascript:void()" onclick="showCalendar2()"><img src="images/cal.gif" class="margin-top-8 margin-left-5" alt="Pick Date" /></a>--%>
                                            </p>
                                        </td>
                                        <td height="23px" align="right">
                                            <label>
                                                HH:mm 24 Hrs &nbsp&nbsp&nbsp&nbsp</label>
                                        </td>
                                        <td>
                                            <asp:Label ID="Label2" runat="server" Text="Activity Duration:"></asp:Label>
                                        </td>
                                        <td>
                                            <asp:TextBox ID="txtDuration" EnableViewState="true" runat="server"></asp:TextBox>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
     
     </table>
     </div>
     
     <div> 
     <center>                   
                    <table  border="0" cellspacing="0" cellpadding="0">
                    
                    
                    <tr>
                            <td height="23px" align="right">
                                <label>
                                    From :<sup>*</sup></label>
                            </td>
                            <td align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <p class="fleft car-search">
                                                <span class="fleft margin-top-3 margin-left-5">
                                                    <asp:TextBox ID="txtFrom" runat="server" Width="80"></asp:TextBox>
                                                    <asp:TextBox ID="txtFromTime" runat="server" Width="50"></asp:TextBox>
                                                    <%--<input  name="FromDate" type="text" value="DD/MM/YYYY" id="FromDate" />--%></span>
                                                <a href="javascript:void()" onclick="showCalendar3()">
                                                    <img src="images/cal.gif" class="margin-top-8 margin-left-5" alt="Pick Date" /></a>
                                            </p>
                                        </td>
                                        <td>
                                            <td height="23px" align="right">
                                                <label>
                                                    To:<sup>*</sup></label>
                                            </td>
                                        </td>
                                        <td>
                                            <p class="fleft car-search">
                                                <span class="fleft margin-top-3 margin-left-5">
                                                    <asp:TextBox ID="txtTo" runat="server" Width="80" Height="20"></asp:TextBox>
                                                    <asp:TextBox ID="txtToTime" runat="server" Width="50"></asp:TextBox>
                                                    <%--<input  name="FromDate" type="text" value="DD/MM/YYYY" id="FromDate" />--%></span>
                                                <a href="javascript:void()" onclick="showCalendar4()">
                                                    <img src="images/cal.gif" class="margin-top-8 margin-left-5" alt="Pick Date" /></a>
                                                <%--<div class="hand fright" style="padding-right:2px;"><img src="images/cal.gif" onclick="showCalendar4()" class="margin-top-8 margin-left-5" alt="Pick Date" /></div>--%>
                                            </p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    
                    
                     </table>
                     
              </center>

                     
     
     </div>
     
     
                    
                        </div>
     
     
     
     
        <div class="heading28 margin-top-10">Itinerary</div>
              
              
                 <div class="box28">
                
                  
                  
                   <div>
                   
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <span style="display: none" class="fleft">UnAvail &nbsp Date </span>
                                            <div id="DateBox" style="display: none" class="fleft padding-top-bottom">
                                                <div class="fleft gray-border">
                                                    <div class="fleft">
                                                        <asp:TextBox ID="Date" runat="server" CssClass="text width-70 border-none"></asp:TextBox></div>
                                                    <div class="hand fright" style="padding-right: 2px;">
                                                        <img src="images/cal.gif" alt="cal" onclick="ShowCalender('callContainer')" />
                                                        <%-- <a href="javascript:void()" onclick="ShowCalender('callContainer')"><img src="images/cal.gif" class="margin-top-8 margin-left-5" alt="Unavail Date" /></a>--%>
                                                    </div>
                                                </div>
                                                <%--  <div class="clear" style="position: absolute;"><div id="callContainer" style="display:none;"></div></div>--%>
                                            </div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="23px" align="right">
                                            <div style="display: none;">
                                                <input type="hidden" id="UnAvailDatesCounter" value="<%=activityVariables["UnAvailDatesCount"] %>" />
                                                <input type="hidden" id="UnAvailDatesCurrent" name="UnAvailDatesCurrent" value="<%=activityVariables["UnAvailDatesCount"] %>" />
                                            </div>
                                            <div class="fleft " style="border: solid 1px #ccc; display: none; width: 740px; margin-top: 25px;
                                                padding: 10px 10px 10px 10px;">
                                                <div id="UnAvailDatesDiv">
                                                    <b class="fleft" style="width: 160px">UnAvailDates :</b>
                                                    <%for (int i = 1; i < Convert.ToInt32(activityVariables["UnAvailDatesCount"]); i++)
                                                      {
                                                          if (i == 1)
                                                          { %>
                                                    <p class="fleft">
                                                        <%--<span style=" width:470px" class="fleft"><input style="width:460px" type="text"  id="txtUnAvailDatesText-<%=i %>"  value='<%=Page.Request["txtUnAvailDatesText-"+i] %>' name="txtUnAvailDatesText-<%=i %>" class="width-300 fleft" value="<%=activityVariablesArray["UnAvailDatesValue"][i - 1] %>" /></span>--%>
                                                        <span style="width: 470px" class="fleft">
                                                            <input style="width: 460px" type="text" id="txtUnAvailDatesText-<%=i %>" name="txtUnAvailDatesText-<%=i %>"
                                                                class="width-300 fleft" value="<%=activityVariablesArray["UnAvailDatesValue"][i - 1] %>" /></span>
                                                    </p>
                                                    <%}
                                              else
                                              { %>
                                                    <%if (i < Convert.ToInt32(activityVariables["UnAvailDatesCount"]) - 1)
                                                      { %>
                                                    <p class="fleft width-100 padding-top-5" style="padding-left: 160px" id="UnAvailDates-<%=i %>">
                                                        <span class="fleft">
                                                            <input id="txtUnAvailDatesText-<%=i %>" name="txtUnAvailDatesText-<%=i %>" class="width-300 fleft"
                                                                type="text" value="<%=activityVariablesArray["UnAvailDatesValue"][i - 1] %>" /></span>
                                                        <%--<span class="fleft"><input id="Text9" name="txtUnAvailDatesText-<%=i %>" value='<%=Page.Request["txtUnAvailDatesText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                                        <i style="display: none;" class="fleft margin-left-10" id="UnAvailDatesImg-<%=i %>">
                                                            <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('UnAvailDates-<%=i %>','text')" /></i>
                                                    </p>
                                                    <%}
                                                      else
                                                      { %>
                                                    <p class="fleft width-100 padding-top-5" style="padding-left: 160px" id="UnAvailDates-<%=i %>">
                                                        <span class="fleft width-300">
                                                            <input id="txtUnAvailDatesText-<%=i %>" name="txtUnAvailDatesText-<%=i %>" class="width-300 fleft"
                                                                type="text" value="<%=activityVariablesArray["UnAvailDatesValue"][i - 1] %>" /></span>
                                                        <%--<span class="fleft width-300"><input  id="txtUnAvailDatesText-<%=i %>"  name="txtUnAvailDatesText-<%=i %>"  value='<%=Page.Request["txtUnAvailDatesText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                                        <i class="fleft margin-left-10" id="UnAvailDatesImg-<%=i %>">
                                                            <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('UnAvailDates-<%=i %>','text')" /></i>
                                                    </p>
                                                    <%} %>
                                                    <%} %>
                                                    <%} %>
                                                </div>
                                                <div class="fleft" style="width: 100px; text-align: right">
                                                    <input type="button" value="add More" onclick="javascript:AddDateMore('UnAvailDates')" />
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table></div>
                     
                     
                     
     
     
     <div style=" margin-top:-30px">                
                   
<table id="Table1" style="display: block;">

  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>

<tr>
<td height="24" align="right">
<asp:Label ID="Label4" runat="server" Text="Itinerary Name 1:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName1" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>

<tr>
<td height="24" align="right">
<asp:Label ID="Label5" runat="server" Text="Meals 1:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals1" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>

<tr>
<td height="24px" align="right">
<asp:Label ID="lblItinerary1" runat="server" Text="Itinerary 1:"></asp:Label><sup>*</sup>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary1" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>


</table>

</div>

<table id="tbl2" style="display: none;">

  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label8" runat="server" Text="Itinerary Name 2:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName2" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label9" runat="server" Text="Meals 2:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals2" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary2" runat="server" Text="Itinerary 2:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary2" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>



<table  id="tbl3" style="display: none;"   border="0">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label1" runat="server" Text="Itinerary Name 3:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName3" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label3" runat="server" Text="Meals 3:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals3" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary3" runat="server" Text="Itinerary 3:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary3" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl4" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>

<tr>
<td height="24" align="right">
<asp:Label ID="Label6" runat="server" Text="Itinerary Name 4:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName4" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label7" runat="server" Text="Meals 4:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals4" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary4" runat="server" Text="Itinerary 4:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary4" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>





<table id="tbl5" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label10" runat="server" Text="Itinerary Name 5:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName5" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label11" runat="server" Text="Meals 5:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals5" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary5" runat="server" Text="Itinerary 5:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary5" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>



<table id="tbl6" style="display: none">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label12" runat="server" Text="Itinerary Name 6:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName6" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label13" runat="server" Text="Meals 6:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals6" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary6" runat="server" Text="Itinerary 6:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary6" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>



<table id="tbl7" style="display: none">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label14" runat="server" Text="Itinerary Name 7:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName7" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label15" runat="server" Text="Meals 7:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals7" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary7" runat="server" Text="Itinerary 7:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary7" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>



<table id="tbl8" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label16" runat="server" Text="Itinerary Name 8:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName8" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label17" runat="server" Text="Meals 8:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals8" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary8" runat="server" Text="Itinerary 8:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary8" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl9" style="display: none">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label18" runat="server" Text="Itinerary Name 9:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName9" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label19" runat="server" Text="Meals 9:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals9" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary9" runat="server" Text="Itinerary 9:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary9" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>




<table id="tbl10" style="display: none">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label20" runat="server" Text="Itinerary Name 10:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName10" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label21" runat="server" Text="Meals 10:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals10" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary10" runat="server" Text="Itinerary 10:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary10" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl11" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label22" runat="server" Text="Itinerary Name 11:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName11" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label23" runat="server" Text="Meals 11:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals11" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary11" runat="server" Text="Itinerary 11:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary11" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl12" style="display: none;">

  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label24" runat="server" Text="Itinerary Name 12:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName12" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label25" runat="server" Text="Meals 12:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals12" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary12" runat="server" Text="Itinerary 12:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary12" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl13" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label26" runat="server" Text="Itinerary Name 13:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName13" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label27" runat="server" Text="Meals 13:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals13" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary13" runat="server" Text="Itinerary 13:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary13" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>          


<table id="tbl14" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label28" runat="server" Text="Itinerary Name 14:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName14" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label29" runat="server" Text="Meals 14:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals14" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary14" runat="server" Text="Itinerary 14:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary14" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>





<table id="tbl15" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label30" runat="server" Text="Itinerary Name 15:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName15" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label31" runat="server" Text="Meals 15:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals15" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary15" runat="server" Text="Itinerary 15:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary15" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl16" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label32" runat="server" Text="Itinerary Name 16:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName16" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label33" runat="server" Text="Meals 16:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals16" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary16" runat="server" Text="Itinerary 16:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary16" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>

<table id="tbl17" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label34" runat="server" Text="Itinerary Name 17:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName17" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label35" runat="server" Text="Meals 17:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals17" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary17" runat="server" Text="Itinerary 17:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary17" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl18" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label36" runat="server" Text="Itinerary Name 18:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName18" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label37" runat="server" Text="Meals 18:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals18" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary18" runat="server" Text="Itinerary 18:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary18" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl19" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label38" runat="server" Text="Itinerary Name 19:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName19" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label39" runat="server" Text="Meals 19:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals19" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary19" runat="server" Text="Itinerary 19:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary19" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl20" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label40" runat="server" Text="Itinerary Name 20:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName20" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label41" runat="server" Text="Meals 20:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals20" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary20" runat="server" Text="Itinerary 20:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary20" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl21" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label42" runat="server" Text="Itinerary Name 21:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName21" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label43" runat="server" Text="Meals 21:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals21" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary21" runat="server" Text="Itinerary 21:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary21" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl22" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label44" runat="server" Text="Itinerary Name 22:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName22" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label45" runat="server" Text="Meals 22:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals22" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary22" runat="server" Text="Itinerary 22:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary22" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl23" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label46" runat="server" Text="Itinerary Name 23:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName23" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label47" runat="server" Text="Meals 23:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals23" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary23" runat="server" Text="Itinerary 23:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary23" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl24" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label48" runat="server" Text="Itinerary Name 24:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName24" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label49" runat="server" Text="Meals 24:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals24" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary24" runat="server" Text="Itinerary 24:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary24" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>

<table id="tbl25" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label50" runat="server" Text="Itinerary Name 25:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName25" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label51" runat="server" Text="Meals 25:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals25" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary25" runat="server" Text="Itinerary 25:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary25" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>


<table id="tbl26" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label52" runat="server" Text="Itinerary Name 26:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName26" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label53" runat="server" Text="Meals 26:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals26" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary26" runat="server" Text="Itinerary 26:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary26" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>



<table id="tbl27" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label54" runat="server" Text="Itinerary Name 27:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName27" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label55" runat="server" Text="Meals 27:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals27" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary27" runat="server" Text="Itinerary 27:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary27" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>

<table id="tbl28" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label56" runat="server" Text="Itinerary Name 28:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName28" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label57" runat="server" Text="Meals 28:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals28" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary28" runat="server" Text="Itinerary 28:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary28" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>

<table id="tbl29" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label58" runat="server" Text="Itinerary Name 29:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName29" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label59" runat="server" Text="Meals 29:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals29" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary29" runat="server" Text="Itinerary 29:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary29" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>                   

<table id="tbl30" style="display: none;">
  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label60" runat="server" Text="Itinerary Name 30:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName30" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label61" runat="server" Text="Meals 30:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals30" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="lblItinerary30" runat="server" Text="Itinerary 30:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary30" CssClass="inp_22_1" runat="server" Width="250px"
TextMode="MultiLine" Height="100px"></asp:TextBox>
</td>
</tr>
</table>                


<table id="tbl31" style="display: none;">

  <tr>
    <td width="20%">&nbsp;</td>
    <td width="80%">&nbsp;</td>
  </tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label62" runat="server" Text="Itinerary Name 31:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItineraryName31" CssClass="inp_22_1" runat="server" MaxLength="200"
Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="24" align="right">
<asp:Label ID="Label63" runat="server" Text="Meals 31:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtMeals31" CssClass="inp_22_1" runat="server" MaxLength="100" Width="250px"></asp:TextBox>
</td>
</tr>
<tr>
<td height="23px" align="right">
<asp:Label ID="lblItinerary31" runat="server" Text="Itinerary 31:"></asp:Label>
</td>
<td align="left" style="width: 521px">
<asp:TextBox ID="txtItinerary31" runat="server" Width="250px" TextMode="MultiLine"
Height="100"></asp:TextBox>
</td>
</tr>
</table>     


         

                   
                    
                    
                     
                     
                    
                        </div>
          
            
            
                                 
          <div class="heading28 margin-top-10"><asp:Label ID="lblDetails"  runat="server" Text="Details"></asp:Label></div>
                
                
                
                <div class="box28">
<asp:TextBox ID="txtDetails" CssClass="inp_22_1" runat="server" Width="250px" TextMode="MultiLine"
                                    Height="100"></asp:TextBox>
                    
                        </div>              
    
    
    
    
    
             <div class="box28 margin-top-10">
                
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    
                        <tr>
                            <td height="23px" align="right">
                                Supplier Name:
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtSupplierName" CssClass="inp_22_1" runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td height="28" align="right">
                                Supplier E-mail:
                            </td>
                            <td align="left" style="width: 521px">
                                <asp:TextBox ID="txtSupplierEmail" CssClass="inp_22_1" runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td height="23px" align="right">
                                <asp:Label ID="lblMealsInclude" runat="server" Text="Meals Include:"> </asp:Label>
                            </td>
                            <td align="left" style="width: 521px">
                                <asp:TextBox ID="txtMealsInclude" CssClass="inp_22_1" runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td height="23px" align="right">
                                <asp:Label ID="lblTrasferInclude" runat="server" Text="Trasfer Include:"> </asp:Label>
                            </td>
                            <td align="left" style="width: 521px">
                                <asp:TextBox ID="txtTrasferInclude" CssClass="inp_22_1" runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td height="23px" align="right">
                                <asp:Label ID="lblPickLocation" runat="server" Text="Tour Starts in:"> </asp:Label>
                            </td>
                            <td align="left" style="width: 521px">
                                <asp:TextBox ID="txtPickLocation" CssClass="inp_22_1" runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
                        <tr style="display: none">
                            <td height="23px" align="right">
                                <label>
                                    Pick Up Date/Time :</label>
                            </td>
                            <td align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <p class="fleft car-search">
                                                <span class="fleft margin-top-3 margin-left-5">
                                                    <asp:TextBox ID="txtPickUpHr" runat="server" Width="30" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0',event);"
                                                        onchange="return ValidateTime('H',this.value,this.id)"></asp:TextBox>
                                                    <asp:TextBox ID="txtPickUpMin" runat="server" Width="30" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0',event);"
                                                        onchange="return ValidateTime('M',this.value,this.id)"></asp:TextBox>
                                                    <%--<input  name="FromDate" type="text" value="DD/MM/YYYY" id="FromDate" />--%></span>
                                                <%--<a href="javascript:void()" onclick="showCalendar7()"><img src="images/cal.gif" class="margin-top-8 margin-left-5" alt="Pick Date" /></a>
--%></p>
                                        </td>
                                        <td height="23px" align="right">
                                            <label>
                                                &nbsp HH:mm 24 Hrs</label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="23px" align="right">
                                <asp:Label ID="lblDropOffLocation" runat="server" Text="Tour Ends in:"> </asp:Label>
                            </td>
                            <td align="left" style="width: 521px">
                                <asp:TextBox ID="txtDropOffLocation" CssClass="inp_22_1" runat="server" Width="200px"></asp:TextBox>
                            </td>
                        </tr>
    
    </table>
    
    </div>
    
    
     <div class="box28 margin-top-10">
     
     
     <table>
     
          <tr style="display: none">
                            <td height="23px" align="right">
                                <asp:Label ID="lblDropOffDate" runat="server" Text="Drop off Date/Time:"></asp:Label>
                            </td>
                            <td align="left">
                                <table>
                                    <tr>
                                        <td>
                                            <p class="fleft car-search">
                                                <span class="fleft margin-top-3 margin-left-5">
                                                    <asp:TextBox ID="txtDropUpHr" runat="server" Width="30" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0',event);"
                                                        onchange="return ValidateTime('H',this.value,this.id)"></asp:TextBox>
                                                    <asp:TextBox ID="txtDropUpMin" runat="server" Width="30" MaxLength="2" onkeypress="return restrictNumeric(this.id,'0',event);"
                                                        onchange="return ValidateTime('M',this.value,this.id)"></asp:TextBox>
                                                    <%--<input  name="FromDate" type="text" value="DD/MM/YYYY" id="FromDate" />--%></span>
                                                <%--<a href="javascript:void()" onclick="showCalendar8()"><img src="images/cal.gif" class="margin-top-8 margin-left-5" alt="Pick Date" /></a>--%>
                                            </p>
                                        </td>
                                        <td height="23px" align="right">
                                            <label>
                                                &nbsp HH:mm 24 Hrs</label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 21px">
                                <div style="display: none;">
                                    <input type="hidden" id="ExclusionsCounter" value="<%=activityVariables["exclusionsCount"] %>" />
                                    <input type="hidden" id="ExclusionsCurrent" name="ExclusionsCurrent" value="<%=activityVariables["exclusionsCount"] %>" />
                                </div>
                                <div class="fleft " style="border: solid 1px #ccc; width: 740px; margin-top: 0px;
                                    padding: 10px 10px 10px 10px;">
                                    <div id="ExclusionsDiv">
                                        <b class="fleft" style="width: 160px">Exclusions:<sup>*</sup></b>
                                        <%for (int i = 1; i < Convert.ToInt32(activityVariables["exclusionsCount"]); i++)
                                          {
                                              if (i == 1)
                                              { %>
                                        <p class="fleft">
                                            <%-- <span style=" width:470px" class="fleft"><input style="width:460px" type="text" id="ExclusionsText-<%=i %>" value='<%=Page.Request["ExclusionsText-"+i] %>' name="ExclusionsText-<%=i %>" class="width-300 fleft" value="<%=activityVariablesArray["exclusionsValue"][i - 1] %>" /></span>--%>
                                            <span style="width: 470px" class="fleft">
                                                <input style="width: 460px" type="text" id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>"
                                                    class="width-300 fleft" value="<%=activityVariablesArray["exclusionsValue"][i - 1] %>" /></span>
                                        </p>
                                        <%}
                                  else
                                  { %>
                                        <%if (i < Convert.ToInt32(activityVariables["exclusionsCount"]) - 1)
                                          { %>
                                        <p class="fleft width-100 padding-top-5" style="padding-left: 160px" id="Exclusions-<%=i %>">
                                            <span class="fleft">
                                                <input id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>" class="width-300 fleft"
                                                    type="text" value="<%=activityVariablesArray["exclusionsValue"][i - 1] %>" /></span>
                                            <%--<span class="fleft width-300""><input id="Text1" name="ExclusionsText-<%=i %>" value='<%=Page.Request["ExclusionsText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                            <i style="display: none;" class="fleft margin-left-10" id="ExclusionsImg-<%=i %>">
                                                <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Exclusions-<%=i %>','text')" /></i>
                                        </p>
                                        <%}
                                          else
                                          { %>
                                        <p class="fleft width-100 padding-top-5" style="padding-left: 160px" id="Exclusions-<%=i %>">
                                            <span class="fleft width-300">
                                                <input id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>" class="width-300 fleft"
                                                    type="text" value="<%=activityVariablesArray["exclusionsValue"][i - 1] %>" /></span>
                                            <%--<span class="fleft width-300"><input id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>"  value='<%=Page.Request["ExclusionsText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                            <i class="fleft margin-left-10" id="ExclusionsImg-<%=i %>">
                                                <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Exclusions-<%=i %>','text')" /></i>
                                        </p>
                                        <%} %>
                                        <%} %>
                                        <%} %>
                                    </div>
                                    <div class="fleft" style="width: 100px; text-align: right">
                                        <input type="button" value="add More" onclick="javascript:AddTextBox('Exclusions')" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 21px">
                                <asp:HiddenField runat="server" ID="hdfinclCounter" Value="2" />
                                <asp:HiddenField runat="server" ID="hdfExclCounter" Value="2" />
                                <asp:HiddenField runat="server" ID="hdfCanPolCounter" Value="2" />
                                <asp:HiddenField runat="server" ID="hdfThingsCounter" Value="2" />
                                <asp:HiddenField runat="server" ID="hdfUnavailDateCounter" Value="2" />
                                <asp:HiddenField runat="server" ID="hdfinclusion" Value="" />
                                <asp:HiddenField runat="server" ID="hdfExclusions" Value="" />
                                <asp:HiddenField runat="server" ID="hdfCanPolicy" Value="" />
                                <asp:HiddenField runat="server" ID="hdfThings" Value="" />
                                <asp:HiddenField runat="server" ID="hdfUnavailDates" Value="" />
                                <asp:HiddenField runat="server" ID="hdfRegionsCounter" Value="1" />
                                <asp:HiddenField runat="server" ID="hdfRegions" Value="" />
                                <asp:HiddenField runat="server" ID="hdfCountries" Value="" />
                                <asp:HiddenField runat="server" ID="hdfCities" Value="" />
                                <asp:HiddenField runat="server" ID="hdfSelCountry" Value="" />
                                <asp:HiddenField runat="server" ID="hdfSelCity" Value="" />
                                <%--<td align="left"><asp:TextBox ID="txtInclusion" runat="server"></asp:TextBox></td>
    <td><asp:Button ID="btnInclusion" Text ="Add" runat="server" Width="68"
            onclick="btnInclusion_Click" /></td>--%>
                                <%-- <input name="tst" type="text" value='<%=Page.Request["tst"] %>' />--%>
                                <div style="display: none;">
                                    <input type="hidden" id="InclusionsCounter" name="hdInclusionsCounter" value="<%=activityVariables["inclusionsCount"] %>" />
                                    <input type="hidden" id="InclusionsCurrent" name="InclusionsCurrent" value="<%=activityVariables["inclusionsCount"] %>" />
                                </div>
                                <div class="fleft " style="border: solid 1px #ccc; width: 740px; margin-top: 25px;
                                    padding: 10px 10px 10px 10px;">
                                    <div id="InclusionsDiv">
                                        <b class="fleft" style="width: 160px">Inclusions :<sup>*</sup></b>
                                        <%for (int i = 1; i < Convert.ToInt32(activityVariables["inclusionsCount"]); i++)
                                          {
                                              if (i == 1)
                                              { %>
                                        <p class="fleft">
                                            <span style="width: 470px" class="fleft">
                                                <input style="width: 460px" type="text" id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>"
                                                    class="width-300 fleft" value="<%=activityVariablesArray["inclusionsValue"][i - 1] %>" /></span>
                                        </p>
                                        <%}
                                  else
                                  { %>
                                        <%if (i < Convert.ToInt32(activityVariables["inclusionsCount"]) - 1)
                                          { %>
                                        <p class="fleft width-100 padding-top-5" style="padding-left: 160px" id="Inclusions-<%=i %>">
                                            <%--<span class="fleft"><input id="Text1" name="InclusionsText-<%=i %>" value='<%=Page.Request["InclusionsText-"+i] %>' class="width-300 fleft" type="text" value="<%=activityVariablesArray["inclusionsValue"][i - 1] %>" /></span>--%>
                                            <span class="fleft width-300"">
                                                <input id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" value="<%=activityVariablesArray["inclusionsValue"][i - 1] %>"
                                                    class="width-300 fleft" type="text" /></span> <i style="display: none;" class="fleft margin-left-10"
                                                        id="InclusionsImg-<%=i %>">
                                                        <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Inclusions-<%=i %>','text')" /></i>
                                        </p>
                                        <%}
                                          else
                                          { %>
                                        <p class="fleft width-100 padding-top-5" style="padding-left: 160px" id="Inclusions-<%=i %>">
                                            <%--<span class="fleft width-300"><input id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" value='<%=Page.Request["InclusionsText-"+i] %>' class="width-300 fleft" type="text" value="<%=activityVariablesArray["inclusionsValue"][i - 1] %>" /></span>--%>
                                            <span class="fleft width-300">
                                                <input id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" value="<%=activityVariablesArray["inclusionsValue"][i - 1] %>"
                                                    class="width-300 fleft" type="text" /></span> <i class="fleft margin-left-10" id="InclusionsImg-<%=i %>">
                                                        <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Inclusions-<%=i %>','text')" /></i>
                                        </p>
                                        <%} %>
                                        <%} %>
                                        <%} %>
                                    </div>
                                    <div class="fleft" style="width: 100px; text-align: right">
                                        <input type="button" value="add More" onclick="javascript:AddTextBox('Inclusions')" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 21px">
                                <div style="display: none;">
                                    <input type="hidden" id="CancellPolicyCounter" value="<%=Convert.ToInt32(activityVariables["CancellPolicyCount"])%>" />
                                    <input type="hidden" id="CancellPolicyCurrent" name="CancellPolicyCurrent" value="<%=Convert.ToInt32(activityVariables["CancellPolicyCount"])%>" />
                                </div>
                                <div class="fleft " style="border: solid 1px #aaa; width: 740px; margin-top: 25px;
                                    padding: 10px 10px 10px 10px;">
                                    <div id="CancellPolicyDiv">
                                        <b class="fleft" style="width: 160px;">Cancellation Policy :</b>
                                        <%for (int i = 1; i < Convert.ToInt32(activityVariables["CancellPolicyCount"]); i++)
                                          {
                                              if (i == 1)
                                              { %>
                                        <p class="fleft">
                                            <%-- <span style=" width:470px;" class="fleft"><input style=" width:460px" type="text" id="CancellPolicyText-<%=i %>" value='<%=Page.Request["CancellPolicyText-"+i] %>' name="CancellPolicyText-<%=i %>"  class="width-300 fleft" value="<%=activityVariablesArray["CancellPolicyValue"][i - 1] %>" /></span>--%>
                                            <span style="width: 470px;" class="fleft">
                                                <input style="width: 460px" type="text" id="Text4" name="CancellPolicyText-<%=i %>"
                                                    class="width-300 fleft" value="<%=activityVariablesArray["CancellPolicyValue"][i - 1] %>" /></span>
                                        </p>
                                        <%}
                                  else
                                  { %>
                                        <%if (i < Convert.ToInt32(activityVariables["CancellPolicyCount"]) - 1)
                                          { %>
                                        <p class="fleft width-100 padding-top-5" style="padding-left: 160px;" id="CancellPolicy-<%=i %>">
                                            <span class="fleft width-300">
                                                <input id="Text2" name="CancellPolicyText-<%=i %>" class="width-300 fleft" type="text"
                                                    value="<%=activityVariablesArray["CancellPolicyValue"][i - 1] %>" /></span>
                                            <%-- <span class="fleft width-300"><input id="Text8" name="CancellPolicyText-<%=i %>" value='<%=Page.Request["CancellPolicyText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                            <i style="display: none;" class="fleft margin-left-10" id="CancellPolicyImg-<%=i %>">
                                                <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('CancellPolicy-<%=i %>','text')" /></i>
                                        </p>
                                        <%}
                                          else
                                          { %>
                                        <p class="fleft width-100 padding-top-5" style="padding-left: 160px;" id="CancellPolicy-<%=i %>">
                                            <span class="fleft width-300">
                                                <input id="CancellPolicyText-<%=i %>" name="CancellPolicyText-<%=i %>" class="width-300 fleft"
                                                    type="text" value="<%=activityVariablesArray["CancellPolicyValue"][i - 1] %>" /></span>
                                            <%--<span class="fleft width-300"><input id="Text7" name="CancellPolicyText-<%=i %>" value='<%=Page.Request["CancellPolicyText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
                                            <i class="fleft margin-left-10" id="CancellPolicyImg-<%=i %>">
                                                <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('CancellPolicy-<%=i %>','text')" /></i>
                                        </p>
                                        <%} %>
                                        <%} %>
                                        <%} %>
                                    </div>
                                    <div class="fleft" style="width: 100px; text-align: right">
                                        <input type="button" value="add More" onclick="javascript:AddTextBox('CancellPolicy')" />
                                    </div>
                                </div>
                            </td>
                        </tr>
                       
                       
                       
                       
                      <%-- below tr i m putting display none as it is not required--%>
                        <tr style=" display:none">
                            <td height="24" align="right">
                                <asp:Label ID="lblUnavailableDays" runat="server" Text="Unavailable Days :"> </asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtUnavailableDays" CssClass="inp_22_1" runat="server" Width="400px" Text="0"></asp:TextBox>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" style="height: 21px">
                                <div style="display: none;">
                                    <input type="hidden" id="ThingsToBringCounter" value="<%=Convert.ToInt32(activityVariables["ThingsToBringCount"])%>" />
                                    <input type="hidden" id="ThingsToBringCurrent" name="ThingsToBringCurrent" value="<%=Convert.ToInt32(activityVariables["ThingsToBringCount"])%>" />
                                </div>
                                <div class="fleft " style="border: solid 1px #aaa; width: 740px; margin-top: 25px;
                                    padding: 10px 10px 10px 10px;">
                                    <div id="ThingsToBringDiv">
                                        <b class="fleft" style="width: 160px;">Payment Policy :</b>
                                        <%for (int i = 1; i < Convert.ToInt32(activityVariables["ThingsToBringCount"]); i++)
                                          {
                                              if (i == 1)
                                              { %>
                                        <p class="fleft">
                                            <%-- <span style=" width:470px;" class="fleft"><input style=" width:460px" type="text" id="Text3" name="ThingsToBringText-<%=i %>" value='<%=Page.Request["ThingsToBringText-"+i] %>' class="width-300 fleft" value="<%=activityVariablesArray["ThingsToBringValue"][i - 1] %>" /></span>--%>
                                            <span style="width: 470px;" class="fleft">
                                                <input style="width: 460px" type="text" id="Text3" name="ThingsToBringText-<%=i %>"
                                                    class="width-300 fleft" value="<%=activityVariablesArray["ThingsToBringValue"][i - 1] %>" /></span>
                                        </p>
                                        <%}
                                  else
                                  { %>
                                        <%if (i < Convert.ToInt32(activityVariables["ThingsToBringCount"]) - 1)
                                          { %>
                                        <p class="fleft width-100 padding-top-5" style="padding-left: 160px;" id="ThingsToBring-<%=i %>">
                                            <span class="fleft width-300">
                                                <input id="Text2" name="ThingsToBringText-<%=i %>" class="width-300 fleft" type="text"
                                                    value="<%=activityVariablesArray["ThingsToBringValue"][i - 1] %>" /></span>
                                            <%--<span class="fleft width-300"><input id="Text2" name="ThingsToBringText-<%=i %>" value='<%=Page.Request["ThingsToBringText-"+i] %>' class="width-300 fleft" type="text" /></span>--%>
                                            <i style="display: none;" class="fleft margin-left-10" id="ThingsToBringImg-<%=i %>">
                                                <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('ThingsToBring-<%=i %>','text')" /></i>
                                        </p>
                                        <%}
                                          else
                                          { %>
                                        <p class="fleft width-100 padding-top-5" style="padding-left: 160px;" id="ThingsToBring-<%=i %>">
                                            <span class="fleft width-300">
                                                <input id="ThingsToBringText-<%=i %>" name="ThingsToBringText-<%=i %>" class="width-300 fleft"
                                                    type="text" value="<%=activityVariablesArray["ThingsToBringValue"][i - 1] %>" /></span>
                                            <%--<span class="fleft width-300"><input id="ThingsToBringText-<%=i %>" name="ThingsToBringText-<%=i %>"value='<%=Page.Request["ThingsToBringText-"+i] %>'  class="width-300 fleft" type="text"  /></span>--%>
                                            <i class="fleft margin-left-10" id="ThingsToBringImg-<%=i %>">
                                                <img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('ThingsToBring-<%=i %>','text')" /></i>
                                        </p>
                                        <%} %>
                                        <%} %>
                                        <%} %>
                                    </div>
                                    <div class="fleft" style="width: 100px; text-align: right">
                                        <input type="button" value="add More" onclick="javascript:AddTextBox('ThingsToBring')" />
                                    </div>
                                </div>
                            </td>
                        </tr>
     
     </table>
     
     
     </div>
    
    
    
    
     <div class="box28 margin-top-10">
     
                        <table>
                        <tr>
                            <td height="23px" align="right">
                                <asp:Label ID="lblBookingCutOff" runat="server" Text="Booking CutOff:"> </asp:Label>
                            </td>
                            <td align="left">
                                <asp:TextBox ID="txtBookingCutOff" CssClass="inp_22_1" runat="server" Width="200px"
                                    onkeypress="return restrictNumeric(this.id,'2',event);"></asp:TextBox>
                            </td>
                        </tr>
                    </table>
     
     
     </div>
     
     
     
     
      <div class="heading28 margin-top-10">ActivityFlexDetails </div>
                
                
                
                <div class="box28">
<div id="div1" style="height:250px; width:100%; margin-top: -0.5px; border-top: none;overflow:scroll" >
                                   
                                    <asp:HiddenField ID="hdfAFCDetails" runat="server"></asp:HiddenField>
                                   <asp:HiddenField runat="server" ID="hdfAFCCount" Value="0" />
                                    <asp:GridView CssClass="GridPager" ID="gvAFCDetails" runat="server" AutoGenerateColumns="False" AllowPaging="true"
                                        PageSize="10" DataKeyNames="flexId" CellPadding="0" ShowFooter="true" CellSpacing="0"
                                        GridLines="Both" OnRowCommand="gvAFCDetails_RowCommand" Width="100%" OnRowEditing="gvAFCDetails_RowEditing"
                                        OnRowDeleting="gvAFCDetails_RowDeleting" OnRowUpdating="gvAFCDetails_RowUpdating"
                                        OnRowCancelingEdit="gvAFCDetails_RowCancelingEdit" OnPageIndexChanging="gvAFCDetails_PageIndexChanging">
                                        <Columns>
                                            <asp:TemplateField>
                                                <HeaderStyle />
                                                <HeaderTemplate>
                                                    <label style="color: Black">
                                                        Label</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ITLabel" runat="server" Text='<%#Eval("flexLabel") %>' CssClass="labelDtl grdof"
                                                        ToolTip='<%#Eval("flexLabel") %>' Width="100px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="EITtxtLabel" runat="server" Enabled="true" Text='<%#Eval("flexLabel") %>'
                                                        Width="100px" CssClass="inputDdlEnabled"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="FTtxtLabel" runat="server" Enabled="true" CssClass="inputEnabled"
                                                        Width="100px"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle/>
                                                <HeaderTemplate>
                                                    <label style="color: Black;">
                                                        <span class="mdt"></span>Control</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%-- <asp:Label ID="ITlblControl" runat="server" Text='<%# Eval("flexControlDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexControlDESC") %>' Width="110px"></asp:Label>--%>
                                                    <asp:Label ID="ITlblControl" runat="server" Text='<%# Eval("flexControlDESC") %>'
                                                        CssClass="labelDtl grdof" ToolTip='<%# Eval("flexControlDESC") %>' Width="110px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="EITddlControl" runat="server" Enabled="true" Width="120px">
                                                        <asp:ListItem Text="TextBox" Value="T"></asp:ListItem>
                                                        <asp:ListItem Text="DateControl" Value="D"></asp:ListItem>
                                                        <asp:ListItem Text="DropDownList" Value="L"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:HiddenField ID="EIThdfControl" runat="server" Value='<%# Eval("flexControl") %>'>
                                                    </asp:HiddenField>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="FTddlControl" runat="server" Enabled="true" Maxlength="50"
                                                        Width="120px">
                                                        <asp:ListItem Text="-SelectControl-" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="TextBox" Value="T"></asp:ListItem>
                                                        <asp:ListItem Text="DateControl" Value="D"></asp:ListItem>
                                                        <asp:ListItem Text="DropDownList" Value="L"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle />
                                                <HeaderTemplate>
                                                    <label style="color: Black">
                                                        Sql Query</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ITSqlQuery" runat="server" Text='<%#Eval("flexSqlQuery") %>' CssClass="labelDtl grdof"
                                                        ToolTip='<%#Eval("flexSqlQuery") %>' Width="100px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="EITtxtSqlQuery" runat="server" Enabled="true" Text='<%#Eval("flexSqlQuery") %>'
                                                        Width="100px" CssClass="inputDdlEnabled"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="FTtxtSqlQuery" runat="server" Enabled="true" CssClass="inputEnabled"
                                                        Width="100px"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle />
                                                <HeaderTemplate>
                                                    <label style="color: Black;">
                                                        <span class="mdt"></span>DataType</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%--<asp:Label ID="ITlblDataType" runat="server" Text='<%# Eval("flexDataTypeDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexDataTypeDESC") %>' Width="110px"></asp:Label>--%>
                                                    <asp:Label ID="ITlblDataType" runat="server" Text='<%# Eval("flexDataTypeDESC") %>'
                                                        CssClass="labelDtl grdof" ToolTip='<%# Eval("flexDataTypeDESC") %>' Width="110px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="EITddlDataType" runat="server" Enabled="true" Width="120px">
                                                        <asp:ListItem Text="AlphaNumeric" Value="A"></asp:ListItem>
                                                        <asp:ListItem Text="Numeric" Value="N"></asp:ListItem>
                                                        <asp:ListItem Text="TextOnly" Value="T"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:HiddenField ID="EIThdfDataType" runat="server" Value='<%# Eval("flexDataType") %>'>
                                                    </asp:HiddenField>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="FTddlDatatype" runat="server" Enabled="true" Width="120px">
                                                        <asp:ListItem Text="-SelectDataType-" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="AlphaNumeric" Value="A"></asp:ListItem>
                                                        <asp:ListItem Text="Numeric" Value="N"></asp:ListItem>
                                                        <asp:ListItem Text="TextOnly" Value="T"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle />
                                                <HeaderTemplate>
                                                    <label style="color: Black;">
                                                        <span class="mdt"></span>Mandatory</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <%-- <asp:Label ID="ITlblMandatory" runat="server" Text='<%# Eval("flexStatusDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexStatusDESC") %>' Width="110px"></asp:Label>--%>
                                                    <asp:Label ID="ITlblMandatory" runat="server" Text='<%# Eval("flexStatusDESC") %>'
                                                        CssClass="labelDtl grdof" ToolTip='<%# Eval("flexStatusDESC") %>' Width="110px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="EITddlMandatory" runat="server" Enabled="true" Width="120px">
                                                        <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                        <asp:ListItem Text="NO" Value="N"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:HiddenField ID="EIThdfStatus" runat="server" Value='<%# Eval("flexMandatoryStatus") %>'>
                                                    </asp:HiddenField>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="FTddlMandatory" runat="server" Enabled="true" Width="120px">
                                                        <asp:ListItem Text="-SelectStatus-" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                                                        <asp:ListItem Text="NO" Value="N"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <HeaderTemplate>
                                                    <label style="color: Black">
                                                        Order</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ITOrder" runat="server" Text='<%#Eval("flexOrder") %>' CssClass="labelDtl grdof"
                                                        ToolTip='<%#Eval("flexOrder") %>' Width="100px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="EITtxtOrder" runat="server" Enabled="true" Text='<%#Eval("flexOrder") %>'
                                                        Width="100px" CssClass="inputDdlEnabled" onkeypress="return restrictNumeric(this.id,'2',event);"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="FTtxtOrder" runat="server" Enabled="true" CssClass="inputEnabled"
                                                        onkeypress="return restrictNumeric(this.id,'2',event);" Width="100px"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label runat="server" ID="id" Width="100px"></asp:Label></HeaderTemplate>
                                                <ItemTemplate>
                                                <asp:UpdatePanel ID="updatepanel3_lnkEdit" runat="server">
                                                <ContentTemplate>
                                                  <asp:LinkButton ID="lnkEdit_1" runat="server" Text="Edit" CommandName="Edit" Width="50px">
                                                    </asp:LinkButton>
                                                
                                                </ContentTemplate>
                                                <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkEdit_1"/>
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                   
                                                   <asp:UpdatePanel ID="updatepanel4_ibtnDelete" runat="server">
                                                <ContentTemplate>
                                                 <asp:ImageButton ID="ibtnDelete_1" runat="server" ImageUrl="~/Images/delete.gif"
                                                        CausesValidation="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the user?');">
                                                    </asp:ImageButton>
                                               
                                                </ContentTemplate>
                                                 <Triggers>
                                                <asp:PostBackTrigger ControlID="ibtnDelete_1"/>
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                   
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                 <asp:UpdatePanel ID="updatepanel4_lnkUpdate" runat="server">
                                                <ContentTemplate>
                                                 <asp:LinkButton ID="lnkUpdate_1" runat="server" Text="Update" Width="50px" CommandName="Update"
                                                        OnClientClick="return validateDtlAddUpdate('U',this.id)"></asp:LinkButton>
                                                
                                                </ContentTemplate>
                                                <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkUpdate_1"/>
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                    
                                                    <asp:UpdatePanel ID="updatepanel5_lnkCancel" runat="server">
                                                <ContentTemplate>
                                                 <asp:LinkButton ID="lnkCancel_1" runat="server" Text="Cancel" Width="50px" CommandName="Cancel"></asp:LinkButton>
                                               
                                                </ContentTemplate>
                                                 <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkCancel_1"/>
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                    
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                  <asp:UpdatePanel ID="updatepanel5_lnkAdd" runat="server">
                                                <ContentTemplate>
                                                 <asp:LinkButton ID="lnkAdd_1" CommandName="Add" runat="server" Text="Add" Width="50px"
                                                        OnClientClick="return validateDtlAddUpdate('I',this.id)"></asp:LinkButton>
                                                
                                                </ContentTemplate>
                                                <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkAdd_1"/>
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                    
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="grid011" />
                                        <RowStyle />
                                        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                    </asp:GridView>
                                </div>
                    
                        </div>
     
     
     <div class="heading28 margin-top-10"> PriceDetails </div>
     
    <div class="box28">
     
     <div id="divPODetails"  style="height:250px; width:100%; margin-top: -0.5px; border-top: none;overflow:scroll" >
                                    <asp:HiddenField ID="hdfPrice" runat="server"></asp:HiddenField>
                                    <asp:HiddenField runat="server" ID="hdfPriceCount" Value="0" />
                                    
                                    <asp:GridView  CssClass="GridPager" ID="GvPrice" runat="server" AutoGenerateColumns="False" AllowPaging="false"
                                        PageSize="100" DataKeyNames="priceId" CellPadding="0" ShowFooter="true" CellSpacing="0"
                                        Width="500" GridLines="Both" OnRowCommand="GvPrice_RowCommand" OnRowDeleting="GvPrice_RowDeleting"
                                        OnRowEditing="GvPrice_RowEditing" OnRowUpdating="GvPrice_RowUpdating" OnRowCancelingEdit="GvPrice_RowCancelingEdit"
                                        OnPageIndexChanging="GvPrice_PageIndexChanging">
                                        <Columns>
                                        <asp:TemplateField>
                                                <HeaderStyle />
                                                <HeaderTemplate>
                                                    <label style="color: Black;">
                                                        <span class="mdt"></span>PaxType</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ITlblPaxType" runat="server" Text='<%# Eval("PaxType") %>'
                                                        CssClass="labelDtl grdof" ToolTip='<%# Eval("PaxType") %>' Width="110px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:DropDownList ID="EITddlPaxType" runat="server" Enabled="true" Width="120px">
                                                    <asp:ListItem Text="-SelectPax-" Value="0"></asp:ListItem>
                                                       <asp:ListItem Text="Adult" Value="Adult"></asp:ListItem>
                                                        <asp:ListItem Text="Child" Value="Child"></asp:ListItem>
                                                        <asp:ListItem Text="Infant" Value="Infant"></asp:ListItem>
                                                    </asp:DropDownList>
                                                    <asp:HiddenField ID="EIThdfPaxType" runat="server" Value='<%# Eval("PaxType") %>'>
                                                    </asp:HiddenField>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:DropDownList ID="FTddlPaxType" runat="server" Enabled="true" Width="120px">
                                                        <asp:ListItem Text="-SelectPax-" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="Adult" Value="Adult"></asp:ListItem>
                                                        <asp:ListItem Text="Child" Value="Child"></asp:ListItem>
                                                        <asp:ListItem Text="Infant" Value="Infant"></asp:ListItem>
                                                    </asp:DropDownList>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <HeaderTemplate>
                                                    <label style="color: Black">
                                                        Label</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ITLabel" runat="server" Text='<%#Eval("Label") %>' CssClass="labelDtl grdof"
                                                        ToolTip='<%#Eval("Label") %>' Width="150px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="EITtxtLabel" runat="server" Enabled="true" Text='<%#Eval("Label") %>'
                                                        Width="150px" CssClass="inputDdlEnabled"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="FTtxtLabel" runat="server" Enabled="true" CssClass="inputEnabled"
                                                        Width="150px"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            
                                                      <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <HeaderTemplate>
                                                    <label style="color: Black">
                                                        StockInHand</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ITStockInHand" runat="server" Text='<%#Eval("StockInHand") %>' CssClass="labelDtl grdof"
                                                        ToolTip='<%#Eval("StockInHand") %>' Width="80px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="EITtxtStockInHand" runat="server" Enabled="true" Text='<%#Eval("StockInHand") %>'
                                                        Width="80px" CssClass="inputDdlEnabled" onkeypress="return restrictNumeric(this.id,'2',event);"
                                                       ></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="FTtxtStockInHand" runat="server" Enabled="true" CssClass="inputEnabled"
                                                        onkeypress="return restrictNumeric(this.id,'2',event);" Width="80px"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            
                                             <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <HeaderTemplate>
                                                    <label style="color: Black">
                                                        StockUsed</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ITStockUsed" runat="server" Text='<%#Eval("StockUsed") %>' CssClass="labelDtl grdof"
                                                        ToolTip='<%#Eval("StockUsed") %>' Width="70px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="EITtxtStockUsed" Enabled="false" runat="server" Text='<%#Eval("StockUsed") %>'
                                                        Width="70px" CssClass="inputDdlEnabled" onkeypress="return restrictNumeric(this.id,'2',event);"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="FTtxtStockUsed" Enabled="false" runat="server" CssClass="inputEnabled"
                                                        onkeypress="return restrictNumeric(this.id,'2',event);" Width="70px" Text="0"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            
                                            <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <HeaderTemplate>
                                                    <label style="color: Black">
                                                        Date</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ITDate" runat="server" Text='<%# (Eval("PriceDate") == DBNull.Value) ? String.Empty : Convert.ToDateTime(Eval("PriceDate")).ToString("dd/MM/yyyy") %>' CssClass="labelDtl grdof"
                                                        ToolTip='<%#Eval("PriceDate") %>' Width="100px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="EITtxtDate" runat="server" Enabled="true" Text='<%#Convert.ToDateTime(Eval("PriceDate")).ToString("dd/MM/yyyy") %>'
                                                        Width="100px" CssClass="inputDdlEnabled" onfocus="showCalendar10(this.id)"></asp:TextBox>
                                                        
                                                        <%--<a href="javascript:void()" onclick="showCalendar10('<%=EITtxtDate.ClientID %>')">
                                                    <img src="images/cal.gif" class="margin-top-8 margin-left-5" alt="Pick Date" /></a>--%>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="FTtxtDate" runat="server" Enabled="true" CssClass="inputEnabled"
                                                        Width="100px"  onfocus="showCalendar10(this.id)"></asp:TextBox>
                                                    <%--     <a href="javascript:void()" onclick="showCalendar10(this.id)">
                                                    <img src="images/cal.gif" class="margin-top-8 margin-left-5" alt="Pick Date" /></a>--%>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <HeaderTemplate>
                                                    <label style="color: Black">
                                                        Supplier Cost</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ITSupplierCost" runat="server" Text='<%#Eval("SupplierCost") %>' CssClass="labelDtl grdof"
                                                        ToolTip='<%#Eval("SupplierCost") %>' Width="120px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="EITtxtSupplierCost" runat="server" Enabled="true" Text='<%#Eval("SupplierCost") %>'
                                                        Width="80px" CssClass="inputDdlEnabled" onkeypress="return restrictNumeric(this.id,'2',event);"
                                                        onChange="getTotalAmount(this.id,'U')"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="FTtxtSupplierCost" runat="server" Enabled="true" CssClass="inputEnabled"
                                                        onChange="getTotalAmount(this.id,'I')" onkeypress="return restrictNumeric(this.id,'2',event);"
                                                        Width="80px"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <HeaderTemplate>
                                                    <label style="color: Black">
                                                        Tax</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ITTax" runat="server" Text='<%#Eval("tax") %>' CssClass="labelDtl grdof"
                                                        ToolTip='<%#Eval("tax") %>' Width="80px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="EITtxtTax" runat="server" Enabled="true" Text='<%#Eval("tax") %>'
                                                        Width="80px" CssClass="inputDdlEnabled" onkeypress="return restrictNumeric(this.id,'2',event);"
                                                        onChange="getTotalAmount(this.id,'U')"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="FTtxtTax" runat="server" Enabled="true" CssClass="inputEnabled"
                                                        onkeypress="return restrictNumeric(this.id,'2',event);" onChange="getTotalAmount(this.id,'I')"
                                                        Width="80px"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <HeaderTemplate>
                                                    <label style="color: Black">
                                                        Markup</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ITMarkup" runat="server" Text='<%#Eval("markup") %>' CssClass="labelDtl grdof"
                                                        ToolTip='<%#Eval("markup") %>' Width="80px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="EITtxtMarkup" runat="server" Enabled="true" Text='<%#Eval("markup") %>'
                                                        Width="80px" CssClass="inputDdlEnabled" onkeypress="return restrictNumeric(this.id,'2',event);"
                                                        onChange="getTotalAmount(this.id,'U')"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="FTtxtMarkup" runat="server" Enabled="true" CssClass="inputEnabled"
                                                        onkeypress="return restrictNumeric(this.id,'2',event);" onChange="getTotalAmount(this.id,'I')"
                                                        Width="80px"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <asp:TemplateField>
                                                <HeaderStyle HorizontalAlign="Left" />
                                                <HeaderTemplate>
                                                    <label style="color: Black">
                                                        Amount</label>
                                                </HeaderTemplate>
                                                <ItemTemplate>
                                                    <asp:Label ID="ITAmount" runat="server" Text='<%#Eval("amount") %>' CssClass="labelDtl grdof"
                                                        ToolTip='<%#Eval("amount") %>' Width="70px"></asp:Label>
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                    <asp:TextBox ID="EITtxtAmount" Enabled="false" runat="server" Text='<%#Eval("amount") %>'
                                                        Width="70px" CssClass="inputDdlEnabled" onkeypress="return restrictNumeric(this.id,'2',event);"></asp:TextBox>
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                    <asp:TextBox ID="FTtxtAmount" Enabled="false" runat="server" CssClass="inputEnabled"
                                                        onkeypress="return restrictNumeric(this.id,'2',event);" Width="70px"></asp:TextBox>
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                            <%--<asp:TemplateField Visible="false" >
                                                            <HeaderStyle HorizontalAlign="left" />
                                                            <HeaderTemplate>
                                                                <label style="color: Black;">
                                                                    <span class="mdt"></span>Type</label>
                                                            </HeaderTemplate>
                                                        <ItemTemplate>
                                                           <asp:Label ID="ITlblType" runat="server" Text='<%# Eval("flexTypeDESC") %>' CssClass="labelDtl grdof"
                                                                    ToolTip='<%# Eval("flexTypeDESC") %>' Width="10px"></asp:Label>
                                                            </ItemTemplate>
                                                            <EditItemTemplate>
                                                              <asp:DropDownList ID="EITddlType" runat="server" Enabled="true" Maxlength="200" Width="10px">
                                                          
                                                            <asp:ListItem Text="SupplierCost" Value="S"></asp:ListItem>
                                                            <asp:ListItem Text="Tax" Value="T"></asp:ListItem>
                                                            <asp:ListItem Text="Mark Up" Value="M"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            
                                                             <asp:HiddenField id="EIThdfType" runat="server" value='<%# Eval("type") %>'></asp:HiddenField>
                                                         </EditItemTemplate>
                                                           <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                            <FooterTemplate>
                                                                 <asp:DropDownList ID="FTddltype" runat="server" Enabled="true" Maxlength="200" Width="10px">
                                                              <asp:ListItem Text="-SelectType-" Value="0"></asp:ListItem>
                                                             <asp:ListItem Text="SupplierCost" Value="S"></asp:ListItem>
                                                            <asp:ListItem Text="Tax" Value="T"></asp:ListItem>
                                                            <asp:ListItem Text="Mark Up" Value="M"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            </FooterTemplate>
                                                        </asp:TemplateField>--%>
                                            <asp:TemplateField>
                                                <HeaderTemplate>
                                                    <asp:Label runat="server" ID="id" Width="150px"></asp:Label></HeaderTemplate>
                                                <ItemTemplate>
                                                <asp:UpdatePanel ID="updatepanel_lnkEdit" runat="server">
                                                <ContentTemplate>
                                                <asp:LinkButton ID="lnkEdit" runat="server" Text="Edit" CommandName="Edit" Width="50px">  </asp:LinkButton>
                                               
                                                </ContentTemplate>
                                                 <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkEdit"/>
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                    
                                                  
                                                    
                                                     <asp:UpdatePanel ID="updatepanel2" runat="server">
                                                <ContentTemplate>
                                                 <asp:ImageButton ID="ibtnDelete" runat="server" ImageUrl="~/Images/delete.gif"
                                                        CausesValidation="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the user?');">
                                                    </asp:ImageButton>
                                                
                                                </ContentTemplate>
                                                <Triggers>
                                                <asp:PostBackTrigger ControlID="ibtnDelete"/>
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                   
                                                </ItemTemplate>
                                                <EditItemTemplate>
                                                  <asp:UpdatePanel ID="updatepanel2" runat="server">
                                                <ContentTemplate>
                                                 <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" Width="50px" CommandName="Update"
                                                        OnClientClick="return validateDtlAddUpdate1('U',this.id)"></asp:LinkButton>
                                                
                                                </ContentTemplate>
                                                <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkUpdate"/>
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                    
                                                     <asp:UpdatePanel ID="updatepanel3" runat="server">
                                                <ContentTemplate>
                                                 <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" Width="50px" CommandName="Cancel"></asp:LinkButton>
                                               
                                                </ContentTemplate>
                                                 <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkCancel"/>
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                    
                                                </EditItemTemplate>
                                                <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                                <FooterTemplate>
                                                   <asp:UpdatePanel ID="updatepanel3" runat="server">
                                                <ContentTemplate>
                                                 <asp:LinkButton ID="lnkAdd" CommandName="Add" runat="server" Text="Add" Width="50px"
                                                        OnClientClick="return validateDtlAddUpdate1('I',this.id)"></asp:LinkButton>
                                                
                                                </ContentTemplate>
                                                <Triggers>
                                                <asp:PostBackTrigger ControlID="lnkAdd"/>
                                                </Triggers>
                                                </asp:UpdatePanel>
                                                    
                                                </FooterTemplate>
                                            </asp:TemplateField>
                                        </Columns>
                                        <HeaderStyle CssClass="grid011" />
                                        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                    </asp:GridView>
                                </div>
     
     </div>
     
     
     
     
     <div class=" padding-10">
     
     <table cellpadding="0" width="300px" cellspacing="0" class="label" border="0">
                        
                        
                        
                        
                        <tr>
                            <td>
                                <div id="errMess" class="error_module" style="display: none;">
                                    <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                                    </div>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td style="padding-top: 5px" align="right">
                                <asp:Label ID="lblTotal" Text="Total:" runat="server" CssClass="label"></asp:Label>
                                <asp:TextBox ID="txtTotal" runat="server" Enabled="false" CssClass="inputDisabled"
                                    Width="100px"></asp:TextBox>
                                <input class="fright" type="button" id="SaveButton" value=" Save" onclick="javascript:Save(); return false;" />
                                <%--        <asp:Button ID="btnSubmit" Text="Save" runat="server"  
                                                CssClass="butt01" OnClick="btnSubmit_Click" OnClientClick="return Save();"></asp:Button>--%>
                                <asp:UpdatePanel ID="updatePanel_btnClear" runat="server">
                                <ContentTemplate>
                                <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="butt01" OnClick="btnClear_Click">
                                </asp:Button>
                               
                                </ContentTemplate>
                                 <Triggers>
                                <asp:PostBackTrigger ControlID="btnClear"/>
                                </Triggers>
                                </asp:UpdatePanel>
                                
                            </td>
                        </tr>
                    </table>
     
     </div>
    
        
    </center>
    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
    </iframe>
    
     <%if (CurrentObject == null)
      { %>
    <script>
        if (document.getElementById('savePage') != "true") {
            var regionsCount = eval(document.getElementById('RegionsCounter').value);
            regions = document.getElementById('<%=hdfRegions.ClientID %>').value.split(',');
            countries = document.getElementById('<%=hdfCountries.ClientID %>').value.split(',');
            cities = document.getElementById('<%=hdfCities.ClientID %>').value.split(',');

            var first = document.getElementById('<%=RegionsText1.ClientID%>');

            if (first.value != "0") {
                LoadCountries('<%=RegionsText1.ClientID%>');
            }

            for (rc = 1; rc < regionsCount - 1; rc++) {
                LoadRegions('RegionsText-' + eval(rc + 1));
            }
        }

        ShowItinerary();
    </script>
    <%}else if (CurrentObject != null)
      { %>
    <script>
        //Load additional Regions list added, internally respective countries and its will be loaded.
        var regionsCount = eval(document.getElementById('RegionsCounter').value);
        regions = document.getElementById('<%=hdfRegions.ClientID %>').value.split(',');
        countries = document.getElementById('<%=hdfCountries.ClientID %>').value.split(',');
        cities = document.getElementById('<%=hdfCities.ClientID %>').value.split(',');


        for (rc = 1; rc < regionsCount - 1; rc++) {
            LoadRegions('RegionsText-' + eval(rc + 1));
        }
        ShowItinerary();
    </script>
    <%} %>
    
    
   
</asp:Content>
