﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="InsuranceListGUI" Title="Insurance List" EnableEventValidation="false" CodeBehind="InsuranceList.aspx.cs" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<asp:HiddenField runat="server" id="hdfParam" value="1"></asp:HiddenField>
      <table cellpadding="0" cellspacing="0" class="label">
            <tr>
                <td style="width:100px" align="right"><a style="cursor:Hand;font-weight:bold;font-size:8pt;color:Black" id="ancParam" onclick="return ShowHide('divParam');">Hide Parameter</a> </td>
                <td width="800px" align="right">
        </td>
          </tr>
         </table>
         
         <div class="grdScrlTrans" style="margin-top:-1px;">
    <div class="paramcon" title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true" >
            <div class="col-md-12 padding-0 marbot_10"> 

<div class="col-md-2"><asp:Label ID="lblFromDate" Text="From Date:" runat="server"></asp:Label> </div>

<div class="col-md-2"> <uc1:DateControl Id="dcFromDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></div>

<div class="col-md-2"><asp:Label ID="lblToDate" Text="To Date:" runat="server"></asp:Label> </div>

<div class="col-md-2"><uc1:DateControl Id="dcToDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl> </div>

<div class="col-md-2"><asp:Label ID="lblAgent" runat="server" Visible="true"  Text="Agent:"></asp:Label> </div>

<div class="col-md-2"> <asp:DropDownList ID="ddlAgent"  CssClass="inputDdlEnabled form-control" Visible="true"  runat="server" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></div>

<div class="clearfix"> </div>
</div>



<div class="col-md-12 padding-0 marbot_10"> 
 <div class="col-md-2"> <asp:Label ID="lblB2BAgent" runat="server" Visible="true"  Text="B2BAgent:"></asp:Label></div>
    
    <div class="col-md-2"> <asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="inputDdlEnabled form-control" OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></div>
    
     <div class="col-md-2"> <asp:Label ID="lblB2B2BAgent" runat="server" Visible="true"  Text="B2B2BAgent:"></asp:Label></div>
       <div class="col-md-2"> <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control" OnSelectedIndexChanged="ddlB2B2BAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></div>
<div class="col-md-2"><asp:Label ID="lblLocation" runat="server" Text="Location:"></asp:Label> </div>

<div class="col-md-2"><asp:DropDownList ID="ddlLocation"  CssClass="inputDdlEnabled form-control"  runat="server"></asp:DropDownList> </div>

<div class="clearfix"> </div>
</div>



<div class="col-md-12 padding-0 marbot_10"> 
<div class="col-md-2"><asp:Label ID="lblAcctStatus" Text="Accounted Status:" runat="server"></asp:Label> </div>

<div class="col-md-2"> <asp:DropDownList CssClass="form-control" ID="ddlAcctStatus" runat="server">

                           <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                           <asp:ListItem  Value="1" Text="YES"></asp:ListItem>
                           <asp:ListItem  Value="0" Text="NO"></asp:ListItem>
                       </asp:DropDownList></div>
<div class="col-md-2"> <asp:Label ID="lblTransType" runat="server" Text="TransType:" Visible="false"></asp:Label></div>

<div class="col-md-2"> <asp:DropDownList CssClass="form-control" ID="ddlTransType" runat="server" Visible="false">
                    <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                           <asp:ListItem  Value="B2B" Text="B2B"></asp:ListItem>
                           <asp:ListItem  Value="B2C" Text="B2C"></asp:ListItem>
                    </asp:DropDownList></div>
<div class="clearfix"> </div>
</div>
<div class="col-md-12 padding-0 marbot_10"> 
<div class="col-md-12"><asp:Button runat="server" ID="btnSearch" Text="Search" OnClientClick="return ValidateParam();" CssClass="btn but_b pull-right" OnClick="btnSearch_Click" /> </div>
</div>
        </asp:Panel>
    </div>
    <table width="100%"  id="tabSearch" border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td>
        <asp:GridView ID="gvInsuranceList" Width="100%" runat="server" AllowPaging="true"
            DataKeyNames="PlanId" EmptyDataText="No Insurance List!" AutoGenerateColumns="false"
            PageSize="29" GridLines="none" CssClass="grdTable" CellPadding="4" CellSpacing="0" 
            OnPageIndexChanging="gvInsuranceList_PageIndexChanging">
            <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left"></HeaderStyle>
            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
            <Columns>
            <asp:TemplateField>
                <ItemTemplate>
                   <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" CssClass="label" Checked='<%# Eval("isAccounted").ToString()!= "N"%>'
                        Enabled='<%# Eval("isAccounted").ToString()!="U"%>'></asp:CheckBox>
                    <asp:HiddenField ID="IThdfInsDetId" runat="server" Value='<%# Bind("PlanId") %>'></asp:HiddenField>
                </ItemTemplate>
            </asp:TemplateField>
                <asp:TemplateField>
                    <ItemStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' CssClass="label"
                            ToolTip='<%# Container.DataItemIndex+1 %>' Width="20px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtPlanType" HeaderText="Plan Type" CssClass="inputEnabled" Width="120px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblPlanType" runat="server" Text='<%# Eval("PlanTitle") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("PlanTitle") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtPlanCode" HeaderText="Plan Code" CssClass="inputEnabled" Width="120px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblPlanCode" runat="server" Text='<%# Eval("InsPlanCode") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("InsPlanCode") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td>
                                    <uc1:DateControl ID="HTtxtIsuueDate" runat="server" DateOnly="true" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="HTbtnIsuueDate" runat="server" ImageUrl="~/Images/wg_filter.GIF"
                                        ImageAlign="AbsMiddle" OnClick="Filter_Click" />
                                </td>
                            </tr>
                        </table>
                        <label class="filterHeaderText">
                            Issue.&nbsp;Date</label>
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblIsuueDate" Width="120px" runat="server" Text='<%# CTDateTimeFormat(Eval("PolicyPurchasedDate")) %>'
                            CssClass="label grdof" ToolTip='<%# Eval("PolicyPurchasedDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td>
                                    <uc1:DateControl ID="HTtxtDepatureDate" runat="server" DateOnly="true" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="HTbtnDepatureDate" runat="server" ImageUrl="~/Images/wg_filter.GIF"
                                        ImageAlign="AbsMiddle" OnClick="Filter_Click" />
                                </td>
                            </tr>
                        </table>
                        <label class="filterHeaderText">
                            Depature.&nbsp;Date</label>
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblDepatureDate" Width="120px" runat="server" Text='<%# CTDateTimeFormat(Eval("DepartureDate")) %>'
                            CssClass="label grdof" ToolTip='<%# Eval("DepartureDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td>
                                    <uc1:DateControl ID="HTtxtArrivalDate" runat="server" DateOnly="true" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="HTbtnArrivalDate" runat="server" ImageUrl="~/Images/wg_filter.GIF"
                                        ImageAlign="AbsMiddle" OnClick="Filter_Click" />
                                </td>
                            </tr>
                        </table>
                        <label class="filterHeaderText">
                            Return.&nbsp;Date</label>
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblArrivalDate" Width="120px" runat="server" Text='<%# CTDateTimeFormat(Eval("ReturnDate")) %>'
                            CssClass="label grdof" ToolTip='<%# Eval("ReturnDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtTotalDays" HeaderText="Total Days" CssClass="inputEnabled" Width="60px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblTotalDays" runat="server" Text='<%# Eval("TotalDays") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("TotalDays") %>' Width="60px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtFromCity" HeaderText="From City" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle/>
                    <ItemTemplate>
                        <asp:Label ID="ITlblFromCity" runat="server" Text='<%# Eval("FromCity") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("FromCity") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtToCity" HeaderText="To City" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblToCity" runat="server" Text='<%# Eval("ToCity") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("ToCity") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAirlinePNR" HeaderText="Airline PNR" CssClass="inputEnabled"
                            Width="120px" OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblAirlinePNR" runat="server" Text='<%# Eval("PNR") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("PNR") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxCertificateNo" HeaderText="Certificate No" Width="120px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblCertificateNo" runat="server" Text='<%# Eval("PolicyNo") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("PolicyNo") %>' Width="150px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtPaxType" HeaderText="PaxCount" Width="200px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblPaxType" runat="server" Text='<%# Eval("PaxCount") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("PaxCount") %>' Width="200px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtCurrency" HeaderText="Currency" CssClass="inputEnabled"
                            Width="100px" OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblCurrency" runat="server" Text='<%# Eval("Currency") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Currency") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtNetAmount" HeaderText="PremiumAmount" CssClass="inputEnabled"
                            Width="100px" OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblNetAmount" runat="server" Text='<%# CTCurrencyFormat(Eval("NetAmount"), Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# Eval("NetAmount") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtMarkup" HeaderText="Markup" CssClass="inputEnabled"
                            Width="100px" OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblMarkup" runat="server" Text='<%# CTCurrencyFormat(Eval("Markup"), Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Markup") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtDiscount" HeaderText="Discount" CssClass="inputEnabled"
                            Width="100px" OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblDiscount" runat="server" Text='<%# CTCurrencyFormat(Eval("Discount"), Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Discount") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtChildren" HeaderText="Childs" Width="50px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblChildren" runat="server" Text='<%# Eval("Childs") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Childs") %>' Width="50px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtInfants" HeaderText="Infants" Width="50px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblInfants" runat="server" Text='<%# Eval("Infants") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Infants") %>' Width="50px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
               <%-- <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtTotalPax" HeaderText="Total Pax" Width="70px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblTotalPax" runat="server" Text='<%# Eval("TotalPax") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("TotalPax") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtTotalAmount" HeaderText="Total Amount" Width="120px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblTotalAmount" runat="server" Text='<%# CTCurrencyFormat(Eval("TotalAmount"), Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# Eval("TotalAmount") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtChargeType" HeaderText="Premium Charge Type" Width="120px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblChargeType" runat="server" Text='<%# Eval("PremiumChargeType") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("PremiumChargeType") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtPassenger" HeaderText="Passenger Name" Width="120px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblPassenger" runat="server" Text='<%# Eval("PassengerName") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("PassengerName") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtProduct" HeaderText="Product" Width="80px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblProduct" runat="server" Text='<%# Eval("Product") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Product") %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAgent" HeaderText="Agent" Width="120px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgent" runat="server" Text='<%# Eval("agent_name") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("agent_name") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtLocation" HeaderText="Location" Width="120px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblLocation" runat="server" Text='<%# Eval("location_name") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("Location_name") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtUser" HeaderText="User" Width="120px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblUSer" runat="server" Text='<%# Eval("USER_FULL_NAME") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("USER_FULL_NAME") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAcctStatus" HeaderText="Accounted Status" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblAcctStatus" runat="server" Text='<%# Eval("AccountedStatus") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("AccountedStatus") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left"/>
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtStatus" HeaderText="Status" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle/>
                    <ItemTemplate>
                        <asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("status") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("status") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtInvoiceNumber" HeaderText="Invoice Number" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblInvoiceNo" runat="server" Text='<%# Eval("Invoice_no") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("Invoice_no") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtMessage" HeaderText="Message" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblMessage" runat="server" Text='<%# Eval("Message") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("Message") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </td></tr>
   
    </table>
             <table>
                 <tr>
                     <td width="120px">
                         <asp:Button OnClick="btnUpdateStatus_Click" runat="server" ID="btnUpdateStatus" Text="Update Status"
                             CssClass="button" />
                     </td>
                     <td width="20px">
                     </td>
                     <td width="80px">
                         <asp:Button OnClick="btnExport_Click" runat="server" ID="btnExport" Text="Export To Excel"
                             CssClass="button" />
                     </td>
                     <td>
                     </td>
                 </tr>
             </table>
  </div>
  <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
<asp:Label style="COLOR: #dd1f10" id="lblError" runat="server"></asp:Label> 
<div>
    <asp:DataGrid ID="dgInsuranceList" runat="server" AutoGenerateColumns="false" >
    <Columns>
    <asp:BoundColumn HeaderText="Agent" DataField="agent_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Plan Type" DataField="PlanTitle" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Plan Code" DataField="InsPlanCode" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Certificate No" DataField="PolicyNo" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Issue Date" DataField="PolicyPurchasedDate" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Depature Date" DataField="DepartureDate" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Arrival Date" DataField="ReturnDate" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Total Days" DataField="TotalDays" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="From City" DataField="FromCity" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="To City" DataField="ToCity" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Airline PNR" DataField="PNR" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Pax Name" DataField="PassengerName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="PaxType" DataField="PaxType" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Premium Charge Type" DataField="PremiumChargeType" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Product" DataField="Product" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Currency" DataField="Currency" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Premium Amount" DataField="NetAmount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Markup" DataField="Markup" HeaderStyle-Font-Bold="true"></asp:BoundColumn> 
    <asp:BoundColumn HeaderText="Discount" DataField="Discount" HeaderStyle-Font-Bold="true"></asp:BoundColumn> 
    <asp:BoundColumn HeaderText="Total Amount" DataField="TotalAmount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="Location" DataField="location_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="User" DataField="USER_FULL_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="AcctStatus" DataField="AccountedStatus" HeaderStyle-Font-Bold="true"></asp:BoundColumn>

      <%--  <%--Added by Somasekhar on 28/11/2018-- --%>
         <asp:BoundColumn HeaderText="Routing" DataField="Routing" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
         <asp:BoundColumn HeaderText="User Code" DataField="User_Code" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
         <asp:BoundColumn HeaderText="Map Code" DataField="Map_Code" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
         <asp:BoundColumn HeaderText="Cust code & Corp Code" DataField="CUST_CD_CORPORATE_CODE" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
         
         <%--<asp:BoundColumn HeaderText="Travel Date" DataField="TRV_DATE" HeaderStyle-Font-Bold="true"></asp:BoundColumn>--%>
        <asp:BoundColumn HeaderText="Travel Date" DataField="TRV_DATE" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        
        <asp:BoundColumn HeaderText="Ticket Date" DataField="TKT_DATE" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Adult Count" DataField="adultCount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Child Count" DataField="childCount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Infant Count" DataField="infantCount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="status" DataField="status" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Invoice Number" DataField="Invoice_no" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
        <asp:BoundColumn HeaderText="Message" DataField="Message" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
       
        <asp:BoundColumn HeaderText="Ref No" DataField="agetREfNo" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
       
        <%--============================================--
--%>

    </Columns>
    </asp:DataGrid>
    </div>
<script  type="text/javascript">

function ShowHide(div)
{
    if(getElement('hdfParam').value=='1')
        {
            document.getElementById('ancParam').innerHTML='Show Param'
            document.getElementById(div).style.display='none';
            getElement('hdfParam').value='0';
        }
        else
        {
            document.getElementById('ancParam').innerHTML='Hide Param'
           document.getElementById('ancParam').value='Hide Param'
            document.getElementById(div).style.display='block';
            getElement('hdfParam').value='1';
        }
}
function ValidateParam()
{
     clearMessage();
    var fromDate=GetDateTimeObject('ctl00_cphTransaction_dcFromDate');
    var fromTime=getElement('dcFromDate_Time');
    var toDate=GetDateTimeObject('ctl00_cphTransaction_dcToDate');
    var toTime=getElement('dcToDate_Time');
    if(fromDate==null) addMessage('Please select From Date !','');
    //alert(fromTime);
    if(fromTime.value=='') addMessage('Please select From Time!','');
    if(toDate==null) addMessage('Please select To Date !','');
    if(toTime.value=='') addMessage('Please select To Time!','');
    if((fromDate!=null && toDate!=null) && fromDate>toDate) addMessage('From Date should not be later than To Date!','');
    if(getMessage()!=''){ 
    alert(getMessage()); 
    clearMessage(); return false; }
}

 
 function getElement(id)
 {
    return document.getElementById('ctl00_cphTransaction_'+id);
 }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

