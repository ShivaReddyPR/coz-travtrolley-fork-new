﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FixDepTerms" Title="Rules & Restrictions" Codebehind="FixDepTerms.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<%--<script type="text/javascript">

    function submitEnq(ActName) {
        window.open('PopupActSubmitEnq.aspx?ActivityName=' + ActName, 'ActivityWindow', 'menubar=no,toolbar=no,height=300,width=500,scrollbars=no')
    }
    </script>--%>


<style> 

.terms-tble { font-size:14px; font-family:Arial, Helvetica, sans-serif; line-height:20px; color:#000;  }


.terms-tble h3 {  color:#000!important }

.terms-tble span{ color:#999  }

.termschild-tble td { padding:5px; }

 

</style>



<div style="margin-top:3px; margin-bottom:3px;" id="bottom-container">



<div class="wrapper">


<div style="padding:10px 10px 0px 10px">

<table width="100%" border="0" cellpadding="0" cellspacing="0">
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
        <td><h2>BOOKING CONDITIONS</h2></td>
      </tr>
      <tr>
        <td height="14"></td>
      </tr>
    </table></td>
  </tr>
  
  
  <tr>
  
  <td>
  
  <table class="terms-tble" width="100%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td><h3> 1. Definitions</h3>

The following shall bear meaning in these booking conditions:-
“<span> Cozmo Travel & Tours </span>”, “<span>The Company</span>”, “<span>us</span>”, “<span>our</span>” refers to a travel company registered in Sharjah , P.O. Box 3393, U.A.E.<br /><br />


“<span>Force Majeure</span>” – any unusual circumstance beyond our control including but without limitation war or threat of war, political unrest, public demonstrations, terrorist activity, adverse weather conditions , industrial dispute, closure of airport or technical difficulties.<br /><br />


“<span>Adult</span>”- above 12 years of age as per passport at the time of travel including the return date.<br /><br />


“<span>Child</span> “- below 12 years of age as per passport at the time of travel including the return date.<br /><br />


“<span>Holiday</span>” – the tour and services booked by you or on your behalf with us.<br /><br />


“<span>Lead Name</span>”  or “<span> You</span>”– the first named customer over 18 years of age on the booking form responsible and acting on behalf of all the other passengers booked in the booking form that understands and accepts all the booking conditions.
<br /><br />


<h3> 2. The Contract</h3>
These conditions and other information in all our printed, public or promotional material form a contract between you and us when you duly sign the booking form .All Holidays are subject to availability and are sold to you. The company has the right at any time or for any reason to terminate this contract after acceptance of deposit but prior to the commencement of tour without assigning any reason whatsoever. No person other than the company, in writing has the authority to vary, add, amplify or waive any stipulations, representations, booking conditions in the printed or promotional material.  <br /><br />





<h3> 3. Holiday Price</h3>
All prices are quoted in AED, (unless otherwise specified). The prices quoted in any printed or promotional material have been calculated at the rate prevailing at the time of printing & publishing. The company reserves the right to amend the price published depending on the situations like fluctuating currency exchange/fuel surcharge/price hike in the airline/peak season /charge from the supplier before the departure & so on.<br /><br />


<h3> 4. Payment </h3>
We accept cash, cheques (as per management discretion) & Bank transfer. No payment reminders will be sent , delayed payments will result in the booking being auto cancelled and fresh booking will have to be made , where the previous booking fee or any other payments made will be forfeited and the tour availability and price is subject to change at the time of booking.  <br /><br />





Returned cheques incur an administration fee of 350AED per transaction payable by you to us apart from the respective bank charges.<br />
<br /></td>
  </tr>
  <tr>
  <td style="text-decoration:underline"> Partial Payment Policy: </td>
  </tr>
  <tr>
    <td><table class="termschild-tble" width="100%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td width="20%">Booking Deposit </td>
        <td width="80%">AED 500 per person </td>
      </tr>
      <tr>
        <td>2nd Payment</td>
        <td>50% of the balance amount (45 days before departure or upon visa submission whichever comes first )</td>
      </tr>
      <tr>
        <td>Final Payment </td>
        <td>Full payment (30 days before departure)</td>
      </tr>
      <tr>
        <td>Optional tours / Extras </td>
        <td>Full Payment at the time of booking</td>
      </tr>
      
    </table>
    
    
   
<br />
<h3>6. Holiday Confirmation  </h3>
Confirmed travel documents will be handed over upon full payment and based on the entire group securing required visa / travel documents before the departure of your holiday. When you book an escorted tour, we handle and take care of all your paper work so you have a hassle free holiday. Your tour leader will hand over your room keys and tour tickets on the day of the tour as per the itinerary. On receipt of your completed booking form & the applicable payment, we will issue a copy of payment receipt; it is at this stage that a binding contract comes into existence between you and us. A pre or post holiday will only be requested by us once your booking form, together with a deposit has been received. Your confirmation documents, in this instance, will indicate your requested package cost & you will be advised of any accommodation, flights etc. which are still on request & not confirmed at the time the confirmation document is issued. It is your responsibility to check the confirmation receipt carefully & to let us know immediately in the event of any error.<br />
<br />




<h3> 7.  Changes or Cancellation </h3>
i) <u>Change or Cancellation by us: </u> We will ensure our utmost attempts to operate the holiday as booked. As we plan the arrangements many months in advance we may occasionally have to change or cancel the services booked by you, without assigning any reason. Such changes or cancellation may be due to circumstances beyond our control. <br />
<br />
In such cases where there has been no Force Majeure , the lead name will be notified in writing as soon as possible and  you would have the option of  either one of the below. 
-accepting the changed arrangement.
<br />
<br />


- transferring to another holiday offered by us and paying or receiving a refund for any price difference.<br />

-travelling as an individual traveler, this will incur an extra cost as group rates would not be applicable and certain services may not be provided to individuals. <br />

-cancelling your holiday and receiving a refund of all monies paid to us (excluding visa fees, taxes, travel insurance and non refundable services booked where applicable).<br />
 We are not liable for any expenses uncured by you.<br />

Significant changes are limited to the changes involving:<br />

- Your departure airport, time <br />

- Your destination area<br />

-a change in accommodation to a lower star grading<br />

-absence of tour manager on certain holidays due to unavoidable circumstances <br /><br />



ii) <u>Change or Cancellation by you: </u> If you wish to change your booking after the first payment has been paid, the Lead name should send us a request in writing, and we will try to meet the request. If we are able to make the change requested, you agree to pay a non –refundable amendment fee of AED 200 per person per change if the change is made 15 days or more prior to departure in addition to any applicable ticket change fees or increase in holiday costs such as upgrade of service or increase in passengers. Changes made within 14 days of departure will be bear 100% cancellation charges. Pls. Note: Many suppliers such as cruise, hotel, rail, safari operators, theme parks, entertainment shows, and some activity providers do not permit us to change the names, ages, no. of pax, nationality, and dates and impose full cancellation charges .In such a case, the costs will be passed onto you in addition to any operational costs we may incur. 




    
    <br /></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
  <td style="text-decoration:underline"> Cancellation Policy- Applicable for partial payment : </td>
  </tr>
  <tr>
    <td><table class="termschild-tble" width="100%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" bgcolor="#16ace7"><strong>Cancellation</strong></td>
        <td width="70%" bgcolor="#16ace7"><strong>Charges</strong></td>
      </tr>
      <tr><td >Booking Deposit </td><td >Non Refundable  </td></tr>
                                        <tr><td>44 days -31days to departure</td><td>10% of the tour price paid will be refunded</td></tr>
                                        <tr><td>30 days -15 days to departure</td><td>50% of the tour price paid will be refunded</td></tr>
                                        <tr><td>14 days & less</td><td>Non Refundable </td></tr>
                                        <tr><td>Optional Tours / Extras</td><td>Non Refundable</td></tr>
      
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
   <tr>
  <td style="text-decoration:underline"> Cancellation Policy- Applicable for full payment : </td>
  </tr>
  <tr>
    <td><table class="termschild-tble" width="100%" border="1" cellspacing="0" cellpadding="0">
      <tr>
        <td width="30%" bgcolor="#16ace7"><strong>Cancellation</strong></td>
        <td width="70%" bgcolor="#16ace7"><strong>Charges</strong></td>
      </tr>
      <tr><td >Booking Deposit </td><td >Non Refundable  </td></tr>
                                        <tr><td>44 days -31days to departure</td><td>50 % of the tour price paid will be refunded</td></tr>
                                        <tr><td>30 days -15 days to departure</td><td>25 % of the tour price paid  will be refunded </td></tr>
                                        <tr><td>14 days & less</td><td>Non Refundable </td></tr>
                                        <tr><td>Optional Tours / Extras</td><td>Non Refundable</td></tr>
      
      
    </table></td>
  </tr>
  <tr>
    <td>&nbsp;</td>
  </tr>
  <tr>
    <td>
    
    
    <h3>8.    Cancellations due to visa rejection </h3>
It is convenient & safe to have the visa application made to the concerned consulates/authorities through the company much in advance to avoid cancellation. Granting or rejecting visas & immigration clearance is the sole prerogative of the concerned sovereign governments; the company shall neither be responsible in cases of non-granting of such documents nor liable for any delay, denial or other related act/omission or for any loss, expense damage or cost resulting therein. You should ensure that you submit the relevant documents & photographs within the stipulated time as mentioned at the time of booking of the tour. The cost of processing visas is not included in your price unless provided otherwise.<br />
<br />

The visa fee when prescribed includes the actual visa charges (Subject to embassy rules as some only accept payment from the passenger at the time of appointment / directly), cost of processing fee, the professional charges of the company & overheads. Even if visas are rejected, the stipulated fees of the Company shall be payable by you. There would be no refund, if anyone is unable to travel due to the said reasons. No claim whatsoever shall be made for the same & the cancellation schedule shall be applied in addition as applicable .If the passports are required to be mailed for the visas to different cities, the Company will mail the passports by a reputed courier at extra costs which will be borne by you. In case of loss or delay of the passports arising out of such transmissions, the Company would not be responsible to compensate the holder for any loss whatsoever. The position in respect of cancellation of holiday by you due to non-availability of travel documents would not change only by virtue of you having applied for such documents through the Company. In the event you are unable to travel on the holiday originally booked by you, due to rejection of visas by the concerned embassy, the cancellation schedule shall apply.
<br />
<br />

<h3>9. Hotels/ Cabins/ Accommodation </h3>

 We have carefully inspected and selected hotels which are comfortable & convenient.
We will do our best to book you in a hotel at or close to the city center, subject to availability & travelling time. The hotels will either be those mentioned in the itinerary or similar category. However there will be cases wherein the hotel will be located outside the city .We cannot guarantee the availability of adjoining rooms/ interconnecting rooms/non smoking room/rooms on the same floor etc. If you seek a change in room while on holiday, the same will be subject to availability & you will need to pay any additional charges if applicable. Due to prevailing weather conditions in Europe and some other countries, most of the hotels do not have air-conditioners or fans. We will not be responsible for any loss or theft of any personal items & we recommend you to take care of your own personal belongings. You need to be very careful of pick pocketing. Cozmo Travel & Tours is not responsible for any damages made by you to the hotel property however please take care when you use the hotel lockers/refrigerated items/ telephones/ televisions, restaurants, etc. Usage of certain services may be chargeable.
<br />
<br />


<h3>10. Check in – Check out </h3>
Generally the check in time is 3 pm & check out is at 12 noon. (This may vary depending on hotel policy). Early check- in & check- out, will be subject to availability & will not be guaranteed. Please note the rooms in Europe and some other countries are very small & the hotel would not permit more than 3 passengers in 1 room. Your accommodation will be based on twin & triple sharing basis. If a passenger wishes to travel alone then there will be an additional surcharge. A family of 2 Adults and 2 children will have to book 2 rooms at adult rates unless specified by us.<br />
<br />



<h3>11. Pre – Post Holidays </h3>
You can opt for your Pre/Post holidays from us at the time of booking.  Separate rates will be applicable if you decide to do so while on holiday. Feel free to contact your tour manager.<br />
<br />


<h3> 12. Airline tickets from Cozmo Travel & Tours</h3>
Once the tickets are issued, any voluntary cancellation made before departure will incur an airline cancellation fee plus our service fee. Please note cancellation fees vary from one airline to another. If cancellation is done after departure the ticket will be strictly NON-REFUNDABLE. We shall also under no circumstances whatsoever, be liable to you or any person travelling with you, for loss of baggage by the airline, failure to provide meal of your choice by the airline, denied boarding or down-gradation due to overbooking or any other reason, failure on the part of the airline to accommodate you despite having confirmed tickets, failure to provide standard quality or quantity of meals offered by the airline, flight delays or rescheduling, flight cancellation, changes of flight or routing & change of airline mentioned at the time of booking. In such circumstances, we will not be liable for the injury, loss or inconvenience suffered by you, but you will be free to pursue the concerned airline. We highly recommend you to purchase an appropriate Travel insurance to protect you and your family. Contact us to purchase a suitable one.<br />
<br />


<h3>13. Baggage </h3>
This varies from airline to airline . Europe airlines restriction on a baggage weight  may be 1 piece of 23 KGS for an economy class & not exceeding 7kgs hand except for USA & Canada. We will inform you on the allowance according to the airline ticket issued to you.  We recommend you to take care of your own baggage, and adhere to the laws of carrying prohibited items; we would not be responsible for any loss/ delay due of such nature. We strongly recommend you to use and keep your valuables in the locker and personally attend to it at all times. <br />
<br />


<h3> 14. Punctuality & Personal conduct </h3>
 We request you to be punctual on your holiday in order for us to operate the holiday in a timely and professional manner so we may be able to cover all the inclusions mentioned in your holiday. We will not be responsible if you miss any service due to delays and as such you will not be entitled to claim a refund. If delays occur the itinerary will be adjusted according to our discretion. We request you to maintain a positive personal conduct at all times during your holiday. We reserve the right to withdraw any passenger whose behavior is determined likely to affect smooth operation of the holiday or adversely affect the enjoyment or safety of other passengers.<br />
<br />


<h3>15. Meals  </h3>
We have carefully selected restaurants that will provide you with delicious and nutritious meals .You may choose from Veg. and Non- Vegetarian options at the time of booking.  Changes while on tour can be requested to the tour manager in advance but cannot be guaranteed.  Take away and extras are not included. A- La- Carte items will be paid by you directly to the restaurant/ tour manager. There is a pre-set menu for meals depending on the holiday. Packed meals are served at some places. We recommend you to carry baby food if you have infants with you. The company reserves the right to change the menu & arrangement of the meals without assigning any reasons.<br />
<br />
<h3> 16. Tips, Gratuities and Government Tax </h3>
Tipping is mandatory in all parts of the world for services rendered (e.g. porters, coach, driver, tour leader, guides etc.)The amount would depend on the country of travel. Gratuities will be informed to you at the time of booking and will be payable directly. Some countries like Europe levy a government tax for the number of nights per person and this amount varies, thus we will inform you at the time of confirmation, such taxes have to be paid directly to the hotel/service.<br />
<br />

<h3> 17. Your health and insurance </h3>
Any medical history that may affect your ability to enjoy the holiday must be informed to us at the time of booking. In the event of a medical condition having not been disclosed, we shall not be liable to provide any assistance or refund the money. It is mandatory that you be covered by a certified & registered overseas travel insurance company that covers risk of life, limb & property during the entire duration of the holiday. We will not be responsible for any loss of life or property in any manner. Contact us for an appropriate insurance policy.<br />
<br />



<h3>18. Promotion / Offer/ Schemes </h3>
Bookings made through Special Offers/promotions/scheme’s will comply with the same booking conditions. Any promotion which is not availed by you cannot be compensated in any manner whatsoever. You may have to adhere to payment terms & schedules in order to be eligible. If you fail to comply with payment & other deadlines you will not be entitled to avail the benefits. All Promotion/Offers/Scheme’s will be counted valid for the booked holiday. In the event of cancellation/curtailment of the holiday, the tour scheme will be void.<br />
<br />


<h3>19. Minimum Participation  </h3>
All holidays specified in our range of holidays are subject to a minimum participation of paying adult participants. If the minimum requirements to operate a holiday are not met, we reserve the right to amalgamate/vary/ amend/alter /cancel without incurring liability to compensate you in any manner. In such cases Section 7 will apply.<br />
<br />


<h3>20. Refunds </h3>
Subject to approval, all refunds will be processed within 21 days of our confirmation of your request in writing and providing us with the required valid documents. Refunds will be processed in the same payment method used to make the booking initially and to the same payee and will be governed by the respective bank / credit card company rules. Any services on request basis that we are unable to confirm will be eligible for a refund  after deducting any operational costs involved by us .No refund will be given unless original travel documents are returned to us .No refund for unused / missed services . Any claims must be made by you within 30 days of the program. We will refund you taking in consideration several aspect of the tour based on factors like the number of participants, the cancellation policies of suppliers like the hotelier, Airline, Embassy, coach operators. 
All refunds have to be collected within a maximum of 45 days from the refund processed; any claims not made post the same will be terminated, unless notified.
<br />
<br />


<h3> 21. Your privacy</h3>
We treat all information furnished by you as confidential & will share only the necessary information. However we may be considered to disclose information furnished by you if such disclosure is required by law & by order of a court.<br />
<br />





<h3>22. Scope of activity  </h3>
Cozmo Travel & Tours is engaged in the business of travel & tours & does not own, control or operate any airline, ships, coach, rail, insurance, hotel, transport, restaurant, kitchen or any other facility on the tour. We shall not be liable for any damages caused to you due to reasons beyond the control of Cozmo Travel & Tours (Force Majeure). Any delays/overstay expenses which occur due to Force Majeure shall be borne entirely by you.<br />
<br />



</td>
  </tr>
</table>
  
  </td>
  </tr>
</table>

<br />
</div>

<div class="clear"></div>



    </div>
 </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

