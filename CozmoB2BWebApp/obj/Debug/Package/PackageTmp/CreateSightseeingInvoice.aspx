﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="CreateSightseeingInvoiceGUI" Codebehind="CreateSightseeingInvoice.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Sightseeing Invoice</title>
     <link rel="stylesheet" href="css/main-style.css" />
      <script type="text/javascript">
var Ajax;

if (window.XMLHttpRequest) {
    Ajax = new XMLHttpRequest();
}
else {
    Ajax = new ActiveXObject('Microsoft.XMLHTTP');
}
    function printPage() {
        document.getElementById('btnPrint').style.display = "none";
        window.print();
//        var divToPrint = document.getElementById('middle-container');
//           var popupWin = window.open('', 'HotelInvoice', 'width=850,height=500');
//           popupWin.document.open();
        //           popupWin.document.write('<html><head><style media=\"screen\">.hotlinvoice td { line-height:22px; font-size:13px; }.hotlinvoice h3 {  background:url(images/cozmovisa-sprite.jpg) repeat-x; height:20px;margin-top:10px; color:#fff; padding-left:10px; }</style></head><body onload="window.print()">' + divToPrint.innerHTML + '</html>');
//            popupWin.document.close(); 
        setTimeout('showButtons()', 1000);
    }
    function showButtons() {
        document.getElementById('btnPrint').style.display = "block";
    }

    

    </script>
    
    
    
       
              <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- manual css -->
    <link href="css/override.css" rel="stylesheet">


<!-- Bootstrap Core JavaScript -->
<script src="Scripts/bootstrap.min.js"></script>

</head>
<body>
<div>
    <form id="form1" runat="server">
    

<div class="col-md-12">

<input type="hidden" id="invoiceNo" value="<%=invoice.InvoiceNumber%>" /> 



<div>
<div style="position:absolute; right:700px; top:530px; display:none"  id="emailBlock">
                    
                    
                    <div style="border: solid 4px #ccc; width:200px" class="ShowMsgDiv"> 
                    <div class="showMsgHeading"> Enter Email Address <a style=" float:right; padding-right:10px" class="closex" href="javascript:HideEmailDiv()">X</a></div>
                    
                           <div style=" padding:10px">
                                    <input style=" width:100%; border: solid 1px #7F9DB9" id="addressBox" name="" type="text" /></div>
                               <div style=" padding-left:10px; padding-bottom:10px">
                                    <input onclick="SendMail()" type="button" value="Send mail" />
                                    
                                    </div>
                                    
                                    
                                    
                    </div>
                    

</div>

</div>




<table id="tblInvoice" width="100%" border="0" cellspacing="0" cellpadding="0">
   
  <tr>
    <td>
    
    
    
   <div class="col-md-12 padding-0 mar-top-10">                                      
   
 <div class="col-md-4"><asp:Image ID="imgLogo" Height="59px" runat="server" AlternateText="AgentLogo" ImageUrl="" /> </div>
   
    <div class="col-md-4"> <center> <h1 style=" font-size:26px">Invoice</h1>
          <div class="small">
             <%if (location.CountryCode == "IN")
                 { %>
             GSTIN:100019554300003
             <%}
    else {%>
             TRN:100019554300003
             <%} %>
        </div>
                           </center> </div>
   
    <div class="col-md-4"><input style="width: 100px;" id="btnPrint" onclick="return printPage();" type="button"
                        value="Print Invoice" /> </div>
     



        
        
        
        
        
        
        
         

<div class="clearfix"></div>
    </div>
        
        </div>
    
         <div class="col-md-12 padding0 marbot_10">                                      
   
 <div class="col-md-6">
  <table class="hotlinvoice" width="100%" style="font-size: 12px;font-family: 'Arial'">
                                    <tbody>
                                        <tr>
                                            <td height="30px" colspan="2">
                                                <h3 style="font-size: 16px;font-family: 'Arial';">Client Details</h3>                                            </td>
                                        </tr>
                                     <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Agency Name: </b></td>
                                            <td style="vertical-align: top;"> <% = agency.Name %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Agency Code: </b></td>
                                            <td style="vertical-align: top;"> <% = agency.Code %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Agency Address: </b></td>
                                            <td style="vertical-align: top;"><% = agencyAddress %></td>
                                        </tr>
                                            <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>TelePhone No: </b></td>
                                                <td style="vertical-align: top;">
                                                    <%  if (agency.Phone1 != null && agency.Phone1.Length != 0)
                                                        {%>
                                                    Phone:
                                                    <% = agency.Phone1%>
                                                    <%  }
                                                        else if (agency.Phone2 != null && agency.Phone2.Length != 0)
                                                        { %>
                                                    Phone:
                                                    <% = agency.Phone2%>
                                                    <%  } %>
                                                    <%  if (agency.Fax != null && agency.Fax.Length > 0)
                                                        { %>
                                                    Fax:
                                                    <% = agency.Fax %>
                                                    <%  } %>                                                </td>
                                        </tr>
                                         <%if (location.CountryCode == "IN")
                                            { %>
                                        <tr>
                                            <td style="vertical-align: middle; width: 120px;"><b>GST Number: </b></td>

                                            <td style="vertical-align: middle;"><% = location.GstNumber %></td>
                                        </tr>
                                        <%}
                                        else if (agency.Telex != null && agency.Telex.Length > 0)
                                        {%>
                                        <tr>
                                            <td style="vertical-align: middle; width: 120px;"><b>TRN Number: </b></td>

                                            <td style="vertical-align: middle;"><% = agency.Telex %></td>
                                        </tr>
                                        <%} %>
                                    </tbody>
                                </table>  
     
       <table class="hotlinvoice" width="100%" style="font-size: 12px;font-family: 'Arial'">
                              <tbody>
                                <tr>
                                  <td colspan="4" style="vertical-align: top;"><h3 style="font-size: 16px;font-family: 'Arial';">Booking Details</h3></td>
                                </tr>
                                <tr>
                                  <td style="vertical-align: top;width: 91px;"><b>Booking Date: </b></td>
                                  <td style="vertical-align: top;"><%=itinerary.CreatedOn.ToString("dd MMM yy") %></td>
                                </tr>
                                <tr>
                                  <td style="vertical-align: top;width: 91px;"><b>Tour Date: </b></td>
                                  <td style="vertical-align: top;"><%=itinerary.TourDate.ToString("dd MMM yy") %></td>
                                  
                                </tr>
                             
                              </tbody>
                            </table>    
            
              <table class="hotlinvoice" width="100%" style="font-size: 12px; font-family: 'Arial'">
                                                    <tbody>
                                                        <tr>
                                                            <td colspan="4">
                                                                <h3 style="font-size: 16px; font-family: 'Arial';">
                                                                    Lead Passenger Details</h3>
                                                            </td>
                                                            </tr>
                                        
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Pax Name: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.PaxNames[0] %> 
                                            </td>
                                             </tr>
                                              <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>No. Of Adult: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.AdultCount %> 
                                            </td>
                                             </tr>
                                              <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>No. Of Child: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.ChildCount %> 
                                            </td>
                                             </tr>
                                             <%if(agency.AddnlPaxDetails=="Y" )
                                             {%>
                                       <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Email ID: </b></td>
                                            <td style="vertical-align: top;"> <% = itinerary.Email%> 
                                            </td>
                                             </tr>
                                              <%} %>
                                             </tbody>
                                             </table>   
  </div>
   
   
   
   
    <div class="col-md-6"> 
    
      <table class="hotlinvoice" width="100%" align="right" style="font-size: 12px;font-family: 'Arial'">
                      <tbody>
                                        <tr>
                                            <td height="30px" colspan="2">
                                                <h3 style="font-size: 16px; font-family: 'Arial';">Invoice Details</h3>                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Invoice No: </b></td>
                                            <td style="vertical-align: top;"><%=invoice.CompleteInvoiceNumber%></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Invoice Date: </b></td>
                                            <td style="vertical-align: top;"><% = invoice.CreatedOn.ToString("dd MMMM yyyy") %></td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Confirmation No: </b></td>
                                            <td style="vertical-align: top;"><% = confNo %></td>
                                        </tr>
                                            <tr>
                                             <%  
                                                 int bookingID = 0;
                                                 bookingID = BookingDetail.GetBookingIdByProductId(sightseeingId, ProductType.SightSeeing); 
                                            %>
                                            <td style="vertical-align: top;width: 120px;"><b>CozmoVocher No: </b></td>
                                            <td style="vertical-align: top;">SS-CT-<%=bookingID%></td>
                                        </tr>
                                    </tbody>
                                </table>   
          
          
           <table class="hotlinvoice" width="100%" align="right" style="font-size: 12px;font-family: 'Arial'">
      <tbody>
                                        <tr>
                                            <td style="vertical-align: top;" colspan="2">
                                                <h3 style="font-size: 16px;font-family: 'Arial';">Tour Details</h3>                                            </td>
                                        </tr>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Tour Name: </b></td>
                                            <td style="vertical-align: top;"> <%=itinerary.ItemName%></td>
                                        </tr>
                                        <%if (itinerary.Duration != null && itinerary.Duration.Length > 0)
                                          { %>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Duration: </b></td>
                                            <td style="vertical-align: top;">  <%=itinerary.Duration%></td>
                                        </tr>
                                        <%} %>
                                        <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>Address: </b></td>
                                            <td style="vertical-align: top;"> <%=itinerary.Address%></td>
                                        </tr>
                                           <tr>
                                            <td style="vertical-align: top;width: 120px;"><b>City: </b></td>
                                            <td style="vertical-align: top;">  <%=itinerary.CityName %></td>
                                        </tr>
                                  </tbody>
                                </table> 
             
             
             
              <table class="hotlinvoice" border="0" width="100%" align="right" style="font-size: 12px;font-family: 'Arial'">
                            <tbody>
                              <% decimal totalPrice = 0;

                                  PriceAccounts price = new PriceAccounts();
                                  price = itinerary.Price;
                                  decimal rateofExchange = price.RateOfExchange;
                                  decimal markup = 0, addlMarkup=0, b2cMarkup=0;

                                  string symbol = Util.GetCurrencySymbol(price.Currency);
                                  decimal CGST = 0, SGST = 0, IGST = 0;
                                  int CGSTPer = 0, IGSTPer = 0, SGSTPer = 0;
                                  decimal outPutVat = 0 ,inPutVat = 0;
                                  if (itinerary.Price.AccPriceType == PriceType.NetFare)
                                  {
                                      totalPrice = totalPrice + (itinerary.Price.NetFare);

                                      markup += itinerary.Price.Markup;
                                      addlMarkup = itinerary.Price.AsvAmount;
                                      b2cMarkup = itinerary.Price.B2CMarkup;
                                      if (location.CountryCode == "IN")//If Booking Login Country IN is there Need to load GST
                                      {
                                          List<GSTTaxDetail> taxDetList = GSTTaxDetail.LoadGSTDetailsByPriceId(itinerary.Price.PriceId);
                                          if (taxDetList != null && taxDetList.Count > 0)
                                          {
                                              CGST = taxDetList.Where(p => p.TaxCode == "CGST").Sum(p => p.TaxAmount);
                                              SGST = taxDetList.Where(p => p.TaxCode == "SGST").Sum(p => p.TaxAmount);
                                              IGST = taxDetList.Where(p => p.TaxCode == "IGST").Sum(p => p.TaxAmount);
                                              GSTTaxDetail objTaxDet = taxDetList.Where(p => p.TaxCode == "CGST").FirstOrDefault();
                                              if (objTaxDet != null)
                                              {
                                                  CGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                                              }
                                              objTaxDet = taxDetList.Where(p => p.TaxCode == "SGST").FirstOrDefault();
                                              if (objTaxDet != null)
                                              {
                                                  SGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                                              }
                                              objTaxDet = taxDetList.Where(p => p.TaxCode == "IGST").FirstOrDefault();
                                              if (objTaxDet != null)
                                              {
                                                  IGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                                              }
                                          }
                                      }
                                      else
                                      {
                                          outPutVat = itinerary.Price.OutputVATAmount;
                                          inPutVat = itinerary.Price.InputVATAmount;
                                      }
                                  }
                                  if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                                  {
                                      totalPrice -= inPutVat;
                                  }
                                  else
                                  {
                                      inPutVat = 0;
                                  }

                                  %>
                              <tr>
                                <td style="vertical-align: top; width:50%;" colspan="2"><h3 style="font-size: 16px;font-family: 'Arial';">Rate Details</h3></td>
                              </tr>
                                <tr>
                                    <td style="vertical-align: top; width: 120px;">
                                        <b>Net Amount:</b>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <%=price.Currency  %>
                                        <% if (Settings.LoginInfo.AgentId <= 1)
                                           {
                                           //Use ceiling instead of Round, Changed on 05082016

                                               %>
                                           <%=(Math.Ceiling(totalPrice).ToString("N" + agency.DecimalValue))%>
                                           <%} 
                                           else{%>
                                           <%=(Math.Ceiling(totalPrice + markup + b2cMarkup).ToString("N" + agency.DecimalValue))%>
                                           <%} %>
                                        
                                    </td>
                                </tr>
                                <tr>
                                <%if (Settings.LoginInfo.AgentId == 1)
                                  { %>
                                    <td style="vertical-align: top; width: 120px;">
                                        <b>Markup: </b>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <%=price.Currency %>
                                        <%=((markup+b2cMarkup) ).ToString("N"+agency.DecimalValue)%>
                                    </td>
                                    <%} %>
                                </tr>
                               
                                <%if (addlMarkup > 0)
                                  {%>
                                <tr>
                                    <td style="vertical-align: top; width: 120px;">
                                        <b>Addl Markup: </b>
                                    </td>
                                    <td style="vertical-align: top;">
                                        <%=price.Currency  %>
                                        <%=(addlMarkup).ToString("N"+agency.DecimalValue)%>
                                    </td>
                                </tr>
                                <%} %>
                              <tr>
                                <td style="vertical-align: top;width: 120px;"><b>Gross Amount:  </b></td>
                                <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(Math.Ceiling(totalPrice + markup + b2cMarkup) + addlMarkup).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                                 <%if (location.CountryCode == "IN")
                                     { %> 
                                <%if (IGST > 0)
    { %>    
                                    <tr>
                                <td style="vertical-align: top;width: 120px;"><b>IGST(<%=IGSTPer %>) %: </b></td>
                                <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(IGST).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                                <%} %>
                                   <%if (CGST > 0)
    { %>    
                                    <tr>
                                <td style="vertical-align: top;width: 120px;"><b>CGST(<%=CGSTPer %>) %: </b></td>
                                <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(CGST).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                                <%} %>
                                   <%if (SGST > 0)
    { %>    
                                    <tr>
                                <td style="vertical-align: top;width: 120px;"><b>SGST(<%=SGSTPer %>) %: </b></td>
                                <td style="vertical-align: top;"><%=price.Currency  %> <%=Convert.ToDouble(SGST).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                                <%}
    }
    else if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
    { %>
                                 <tr>
                                <td style="vertical-align: top;width: 120px;"><b>In Vat:</b></td>
                                <td style="vertical-align: top;"><%=price.Currency  %> <%=Math.Ceiling(inPutVat).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                                  <tr>
                                <td style="vertical-align: top;width: 120px;"><b>outPutVat: </b></td>
                                <td style="vertical-align: top;"><%=price.Currency  %> <%=Math.Ceiling(outPutVat).ToString("N" + agency.DecimalValue)%></td>
                              </tr>
                                <%} %>
                              <tr>
                                <td style="vertical-align: top;width: 120px;"><b>Total Amount: </b></td>
                                <td style="vertical-align: top;"><%=price.Currency%> <%=(Math.Ceiling(totalPrice + markup+b2cMarkup+inPutVat)+addlMarkup+Math.Ceiling(outPutVat)+Math.Ceiling(IGST+CGST+SGST)).ToString("N"+agency.DecimalValue)%></td>
                              </tr>
                            </tbody>
                          </table>                     
    </div>


<div class="clearfix"></div>
    </div>
    
    
    
    
    <div class="col-md-12  marbot_10">  
    
     <table border="0" class="hotlinvoice" width="30%" style="font-family: 'Arial'">
<tr>
                 <% UserMaster member = new UserMaster(invoice.CreatedBy); %>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Invoiced By: </b>
                 </td>
                 <td style="vertical-align: top;"  align="left">
                     Cozmo Travel
                 </td>
             </tr>
             <tr>
                 <td style="vertical-align: top; width: 100px;">
                     <b>Created By: </b>
                 </td>
                 <td style="vertical-align: top;" align="left">
                     <%= member.FirstName + " " + member.LastName%>
                 </td>
             </tr>
             <tr>
             <td style="vertical-align: top; width: 100px;">
             <b>Location: </b>
             </td>
             <td style="vertical-align: top;" align="left">
             <%=location.Name %>
             </td>
             </tr>
      
             <tr>
             <td style="width:130px">
             <div class="width-100 fleft text-right" style="display:none"><img src="images/Email1.gif"  />
           <a href="javascript:ShowEmailDivD()">Email Invoice</a>
                                     
        
                       
          </div>
          </td>
          <td style="width:160px">
          <span id="LowerEmailSpan" class="fleft" style="margin-left: 15px;"></span> 
              </td>
                    <td>
                        <div class="email-message-parent" style="display:none;" id="emailSent">
                    <span  style="float:left;width:100%;margin:auto;text-align:center;">
                        <span class="email-message-child" id="messageText" style="background-color:Yellow";>Email sent successfully</span>
                    </span>
                    
           </div>
             </td>
             </tr>
             </table>
    
    </div>
    
    

    </td>
  </tr>

                                
         </table>
    
</div>
    </form>
    </div>
</body>
</html>
