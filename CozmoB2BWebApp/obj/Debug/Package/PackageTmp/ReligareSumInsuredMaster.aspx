﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="ReligareSumInsuredMaster.aspx.cs" Inherits="CozmoB2BWebApp.ReligareSumInsuredMaster" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
 <%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <div><h3>Religare Sum Insured Master</h3></div>
         <div class="body_container" style="height:200px">
        <div class="col-md-12 padding-0 marbot_10">

            <div class="col-md-2">
                <asp:Label ID="lblProductTypeId" runat="server" Text="Product Type:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:DropDownList CssClass="form-control" ID="ddlProductTypeId" runat="server" OnSelectedIndexChanged="ddlProductTypeId_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
            </div>

            <div class="col-md-2">
                <asp:Label ID="lblProductId" runat="server" Text="Product  Name:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:DropDownList CssClass="form-control" ID="ddlProductId" runat="server" AutoPostBack="true"></asp:DropDownList>
            </div>

            <div class="col-md-2">
                <asp:Label ID="lblRangId" runat="server" Text="Range Id:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtRangId" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>
            </div>
            <div class="clearfix"></div>
            </div>
         <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                <asp:Label ID="lblRangeDescription" runat="server" Text="Range Description:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtRangeDescription" runat="server"></asp:TextBox>
            </div>

            <div class="col-md-2">
                <asp:Label ID="lblCoverType" runat="server" Text="Cover Type:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtCoverType" runat="server"></asp:TextBox>
            </div>
             <div class="clearfix"></div>
            </div>

        <div class="col-md-12">
            <label style=" padding-right:5px" class="f_R"><asp:Button  ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();" CssClass="btn but_b"  OnClick ="btnSave_Click" ></asp:Button></label>
                 
                 
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b"   OnClick="btnClear_Click"></asp:Button></label>
                    
                    
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="btn but_b"   OnClick="btnSearch_Click"></asp:Button></label>
            </div>

        
        <div class="col-md-10">
            <asp:Label ID="lblSuccessMsg" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdfMeid" Value="0" />
    <script type="text/javascript">
        function Save() {
            if (getElement('ddlProductTypeId').value == -1) addMessage('Product Type Id cannot be blank!', '');
            if (getElement('ddlProductId').value == -1) addMessage('Product Id cannot be blank!', '');
            if (getElement('txtRangId').value == '') addMessage('Range Id cannot be blank!', '');
            if (getElement('txtRangeDescription').value == '') addMessage('Range Description cannot be blank!', '');
            if (getElement('txtCoverType').value == '') addMessage('Cover Type cannot be blank!', '');

        if (getMessage() != '') {
            //alert(getMessage());
            alert(getMessage()); clearMessage(); 
            return false;
           }
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
                return false;
            }
            return true;
        }
    </script>
</asp:Content>

<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server"> 

     <asp:GridView ID="gvSearch" Width="100%"  runat="server" AllowPaging="true" DataKeyNames="SUM_INSURED_ID"
      emptydatalist="No Location List!" AutoGenerateColumns="false" PageSize="15" GridLines="None"
     CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4"
     CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">

     <Columns>
         <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
         <asp:TemplateField HeaderText ="SL.NO">
        <ItemTemplate>
             <%#Container.DataItemIndex+1 %>
        </ItemTemplate>
    </asp:TemplateField>
         <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlblProductName" runat="server" Text='<%# Eval("PRODUCT_TYPE") %>' CssClass="label grdof" ToolTip='<%# Eval("PRODUCT_TYPE") %>'></asp:Label>
   
    </ItemTemplate>   
             <HeaderTemplate>
                 Product Type
             </HeaderTemplate>
         </asp:TemplateField>
          <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlblProductName" runat="server" Text='<%# Eval("PRODUCT_NAME") %>' CssClass="label grdof" ToolTip='<%# Eval("PRODUCT_NAME") %>'></asp:Label>
   
    </ItemTemplate>   
             <HeaderTemplate>
                 Product Name
             </HeaderTemplate>
         </asp:TemplateField>
          <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlblQuestioncode" runat="server" Text='<%# Eval("RANGE_ID") %>' CssClass="label grdof" ToolTip='<%# Eval("RANGE_ID") %>' ></asp:Label>
   
    </ItemTemplate>  
             <HeaderTemplate>
                 Range Id
             </HeaderTemplate>
       </asp:TemplateField> 
         <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlblRangeDescription" runat="server" Text='<%# Eval("RANGE_DESCRIPTION") %>' CssClass="label grdof" ToolTip='<%# Eval("RANGE_DESCRIPTION") %>'></asp:Label>
   
    </ItemTemplate>   
             <HeaderTemplate>
                 Range Description
             </HeaderTemplate>
         </asp:TemplateField>

              
             

     </Columns>

     </asp:GridView>
     

    </asp:Content>
