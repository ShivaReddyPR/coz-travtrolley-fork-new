﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FixedDepartureAvailabilityGUI" Title="FixedDeparture Availability" Codebehind="FixedDepartureAvailability.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript" src="Scripts/Jquery/ajax_tabs_m.js"></script>


<!-- ----------------------For Calender Control--------------------------- -->
<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js" ></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js" ></script>
     
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
<script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
<!-- ----------------------End Calender Control--------------------------- -->
 <!------------------------Departure Date Calendar Script------------->
<script language="javascript" type="text/javascript">
    var day_of_week = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
    var month_of_year = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

    //  DECLARE AND INITIALIZE VARIABLES
    var Calendar = new Date();

    var years = '<%=departureYears %>';
    var year; //Calendar.getFullYear();     // Returns year
    var months = '<%=departureMonths %>'; //'0,1,2,3,4,5,6,7,8,9,10,11';
    var month;  // Calendar.getMonth();    // Returns month (0-11)
    var today = '<%=departureDates %>';    // Returns day (1-31) Set the dates for Selection here eg: [1],[15] etc
    var weekday = Calendar.getDay();    // Returns day (1-31)

    var DAYS_OF_WEEK = 7;    // "constant" for number of days in a week
    var DAYS_OF_MONTH = 31;    // "constant" for number of days in a month
    var cal = '';    // Used for printing





    /* VARIABLES FOR FORMATTING
    NOTE: You can format the 'BORDER', 'BGCOLOR', 'CELLPADDING', 'BORDERCOLOR'
    tags to customize your caledanr's look. */

    var TR_start = '<TR>';
    var TR_end = '</TR>';
    var highlight_start = '<TD WIDTH="30" HEIGHT="30"><TABLE CELLSPACING=0 BORDER=1 BGCOLOR=DEDEFF BORDERCOLOR=CCCCCC><TR><TD WIDTH=30 HEIGHT=30><B><CENTER>';
    var highlight_end = '</CENTER></TD></TR></TABLE></B>';
    var TD_start = '<TD WIDTH="30" HEIGHT="30"><CENTER>';
    var TD_end = '</CENTER></TD>';

    /* BEGIN CODE FOR CALENDAR
    NOTE: You can format the 'BORDER', 'BGCOLOR', 'CELLPADDING', 'BORDERCOLOR'
    tags to customize your calendar's look.*/
    cal += '<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=0 BORDERCOLOR=BBBBBB STYLE=FONT-SIZE:14px;>';
    //loop thru all the months in a year
    for (var j = 0; j < years.split(',').length; j++) {
        year = years.split(',')[j];
        cal += "<TR>";
        for (var i = 0; i < months.split(',').length; i++) {

            //Insert new row after displaying 4 months in a row
            //            if (i == 4 || i == 8 || i == 12) {
            //                cal += "<TR>";
            //            }

            month = months.split(',')[i];
            if (month.split('-')[0].indexOf(year) == 0) {
                month = month.split('-')[1];
                Calendar.setDate(1);    // Start the calendar day at '1'
                Calendar.setMonth(month);    // Start the calendar month at now                
                cal += '<TD VALIGN=TOP><TABLE BORDER=1 CELLSPACING=0 CELLPADDING=0 BORDERCOLOR=BBBBBB STYLE=FONT-SIZE:14px;><TR><TD>';
                cal += '<TABLE BORDER=0 CELLSPACING=5 CELLPADDING=5>' + TR_start;
                cal += '<TD COLSPAN="' + DAYS_OF_WEEK + '" BGCOLOR="#EFEFEF"><CENTER><B STYLE=FONT-SIZE:16px;>';
                cal += month_of_year[month] + '   ' + year + '</B>' + TD_end + TR_end;
                cal += TR_start;

                //   DO NOT EDIT BELOW THIS POINT  //

                // LOOPS FOR EACH DAY OF WEEK
                for (index = 0; index < DAYS_OF_WEEK; index++) {

                    // BOLD TODAY'S DAY OF WEEK
                    //        if (weekday == index)
                    //            cal += TD_start + '<B>' + day_of_week[index] + '</B>' + TD_end;

                    // PRINTS DAY
                    //else
                    cal += TD_start + day_of_week[index] + TD_end;
                }

                cal += TD_end + TR_end;
                cal += TR_start;

                // FILL IN BLANK GAPS UNTIL TODAY'S DAY
                for (index = 0; index < Calendar.getDay(); index++)
                    cal += TD_start + '  ' + TD_end;

                // LOOPS FOR EACH DAY IN CALENDAR
                for (index = 0; index < DAYS_OF_MONTH; index++) {
                    if (Calendar.getDate() > index) {
                        // RETURNS THE NEXT DAY TO PRINT
                        week_day = Calendar.getDay();

                        // START NEW ROW FOR FIRST DAY OF WEEK
                        if (week_day == 0)
                            cal += TR_start;

                        if (week_day != DAYS_OF_WEEK) {

                            // SET VARIABLE INSIDE LOOP FOR INCREMENTING PURPOSES
                            var day = Calendar.getDate();

                            // HIGHLIGHT TODAY'S DATE
                            /////////////////////////////////////////////////////////////////
                            //////////////////// Highlighting the Dates /////////////////////
                            if (today.indexOf('[' + year + '-' + month + '-' + Calendar.getDate() + ']') >= 0)
                                cal += highlight_start + day + highlight_end + TD_end;
                            // PRINTS DAY                
                            /////////////////////////////////////////////////////////////////
                            else
                                cal += TD_start + day + TD_end;
                        }

                        // END ROW FOR LAST DAY OF WEEK
                        if (week_day == DAYS_OF_WEEK)
                            cal += TR_end;
                    }

                    // INCREMENTS UNTIL END OF THE MONTH
                    Calendar.setDate(Calendar.getDate() + 1);

                } // end for loop

                cal += '</TD></TR></TABLE></TABLE></TD>';

                //Close the row after displaying 4 months in a row
                //            if (i == 7 || i == 11 || i == 12) {
                //                cal += "</TR>";
                //            }
            }
        }
        cal += "</TR>";
    }
    cal += "</TR></TABLE>";
    //  PRINT CALENDAR
    //document.write(cal);

    //  End -->
        </script>
<!----------------------End Departure Date Calendar----------------------------------------->

 

    <!-- Add jQuery library -->

    
    <!-- Add fancyBox main JS and CSS files -->

    <script type="text/javascript" src="Scripts/Jquery/jquery.fancybox.js"></script>
<script type="text/javascript">

    var cal1;
    var cal2;
    function init() {
        var dt = new Date();
        cal1 = new YAHOO.widget.CalendarGroup("cal1", "container1");
        cal1.cfg.setProperty("minDate", dt.getMonth() + 1 + "/" + dt.getDate() + "/" + dt.getUTCFullYear());
        cal1.cfg.setProperty("pagedate", dt.getMonth() + 1 + "/" + dt.getUTCFullYear());
//        cal1.cfg.setProperty("title", "Select your desired checkin date:");
        cal1.cfg.setProperty("close", true);
        cal1.cfg.setProperty("iframe", true);
        cal1.selectEvent.subscribe(setDate1);
        cal1.render();        
    }

    function showCalendar1() {
        
        document.getElementById('container1').style.display = "block";
        document.getElementById('Outcontainer1').style.display = "block";        
    }

    var departureDate = new Date();
    
    function setDate1() {
        var date1 = cal1.getSelectedDates()[0];
        var dt = new Date();
        this.today = new Date(dt.getUTCFullYear(), dt.getMonth() - 1, dt.getDate());
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());
        if (difference < 0) {
            document.getElementById('errMessHotel').style.visibility = "visible";
            document.getElementById('errMessHotel').innerHTML = "Please select correct checkin date. ";
            return false;
        }
        departureDate = cal1.getSelectedDates()[0];
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        document.getElementById('<%=txtDepDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
        cal1.hide();
        document.getElementById('Outcontainer1').style.display = "none";
    }
    YAHOO.util.Event.addListener(window, "load", init);
    YAHOO.util.Event.addListener(window, "click", init);
    
    

    
    </script>
 <script type="text/javascript">

//     function Validate() {
//         var msg = true;
//         var date = document.getElementById('<%=txtDepDate.ClientID %>').value;
//         if (date.length <= 0 || date =='DD/MM/YYYY') {
//             document.getElementById('errMessHotel').style.display = 'block';
//             document.getElementById('errMessHotel').innerHTML = 'Enter Valid Date';
//             msg = false;
//         }
//         else {
//             document.getElementById('errMessHotel').style.display = "none";
//             msg = true;
//         }
//         return msg;
//     }
     function Validate() {
         var msg = true;
         if (document.getElementById('<%=ddlDepDate.ClientID %>').selectedIndex <= 0) {
             document.getElementById('errMessHotel').style.display = 'block';
             document.getElementById('errMessHotel').innerHTML = 'Enter Valid Date';
             msg = false;
         }
         else {
             document.getElementById('errMessHotel').style.display = "none";
             msg = true;
         }
         return msg;
     }
     function ShowDiv(name) {
         if (name == "home") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('Inclusions').style.display = "none";
             document.getElementById('Itinerary').style.display = "none";
             document.getElementById('RoomRates').style.display = "none";
             document.getElementById('Terms').style.display = "none";
             return false;
         }
         if (name == "Inclusions") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('home').style.display = "none";
             document.getElementById('Itinerary').style.display = "none";
             document.getElementById('RoomRates').style.display = "none";
             document.getElementById('Terms').style.display = "none";
             return false;
         }
         if (name == "Itinerary") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('home').style.display = "none";
             document.getElementById('Inclusions').style.display = "none";
             document.getElementById('RoomRates').style.display = "none";
             document.getElementById('Terms').style.display = "none";
             return false;
         }
         if (name == "RoomRates") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('home').style.display = "none";
             document.getElementById('Inclusions').style.display = "none";
             document.getElementById('Itinerary').style.display = "none";
             document.getElementById('Terms').style.display = "none";
             return false;
         }
         if (name == "Terms") {
             var div = document.getElementById(name);
             div.style.display = "block";
             document.getElementById('home').style.display = "none";
             document.getElementById('Inclusions').style.display = "none";
             document.getElementById('Itinerary').style.display = "none";
             document.getElementById('RoomRates').style.display = "none";
             return false;
         }
     }

     
    </script>
    
    
      <script type="text/javascript" src="Scripts/Jquery/jquery.min.js"></script>
<script type="text/javascript" src="Scripts/Jquery/ajax_tabs_m.js"></script>
<link rel="stylesheet" href="css/ajax_tab_css.css" />
   
  
    
     <script type="text/javascript">


         $(function() {
             $('.tabs').tabs()
         });

     $('.tabs').bind('change', function(e) {
         var nowtab = e.target // activated tab
         var divid = $(nowtab).attr('href').substr(1);
         if (divid == "ajax") {
             $.getJSON('<%=Request.Url.Scheme%>://1.upstatic.sinaapp.com/api.php').success(function(data) {
                 $("#" + divid).text(data.msg);
             });
         }


     });
  
    </script>
   
            <div style=" padding-top:10px">
                <div class="ns-h3">
                        <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></div>
                
                <div class="wraps0ppp">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="70%" class="bg_shadow">
                                           <asp:Image ID="imgActivity" runat="server" Width="625px" Height="250px"/>
                                        </td>
                                        <td width="30%" align="center" valign="top">
                                            <div id="BookingDetails">
                                            </div>
                                            <div id="plztryagain">
                                            <div class="error_msg" style="display:none;" id="errMessHotel"> </div>
                                                <table style="font-size: 13px; margin-left:10px " width="100%" border="0" cellspacing="0" cellpadding="0">
                                                    <tr>
                                                        <td height="20" align="left" valign="top">
                                                            <strong>Select Dates</strong>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="20" valign="top">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="36%">
                                                                            Excursion Date :
                                                                        </td>
                                                                        <td width="29%">
                                                                             <asp:DropDownList ID="ddlDepDate" runat="server"></asp:DropDownList>
                                                                            <asp:TextBox ID="txtDepDate" runat="server" CssClass="inp_00" Text="DD/MM/YYYY" Visible="false"></asp:TextBox>
                                                                        </td>
                                                                        <td width="13%" valign="bottom">
                                                                           <%-- <a href="javascript:void(null)" onclick="showCalendar1()"/><img src="images/call-cozmo.png"></a>--%>
                                                                        </td>
                                                                        <td width="22%">
                                                                            &nbsp;
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td height="21px">
                                                            <hr class="b_bot_1 ">
                                                        </td>
                                                    </tr>
                                                    <tr style="display:none;">
                                                        <td height="20" align="left" valign="top">
                                                            <strong>Select No. of Passengers</strong>
                                                        </td>
                                                    </tr>
                                                    <tr style="display:none;">
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0" border="0" width="160">
                                                                <tbody>
                                                                    <tr>
                                                                        <td align="left">
                                                                           
                                                                            Adults:</td>
                                                                                                    <td>
                                                                                                        Childs</td>
                                                                                                    <td>
                                                                                                        Infants</td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="left">
                                                                        &nbsp;<asp:DropDownList ID="ddlAdult" CssClass="inp_01" runat="server">                                                                            
                                                                            <asp:ListItem Selected="True" Value="1" Text="1"></asp:ListItem>                                                                                
                                                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                                                                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                                                                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="left">
                                                                        &nbsp;<asp:DropDownList ID="ddlChild"  CssClass="inp_01" runat="server">                                                                            
                                                                                <asp:ListItem Selected="True" value="0" Text="0"></asp:ListItem>
                                                                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>                                                                                
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="left">
                                                                        &nbsp;<asp:DropDownList ID="ddlInfant" CssClass="inp_01" runat="server">                                                                            
                                                                                <asp:ListItem Selected="True" Value="0" Text="0"></asp:ListItem>
                                                                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>                                                                                
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr style="display:none;">
                                                        <td height="21px">
                                                            <hr class="b_bot_1 ">
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td align="left">
                                                                                                                            
                                                            <%--<asp:ImageButton ID="imgCheck" ImageUrl="images/check-availibility.gif" 
                                                                runat="server" onclick="imgCheck_Click" OnClientClick="return Validate();" />--%>
                                                                
                                                                <asp:LinkButton ID="imgCheck" CssClass="button-new"  Text="Check Availability"
                                                                runat="server" onclick="imgCheck_Click" OnClientClick="return Validate();" />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="5" cellpadding="0">
                                                                <tr>
                                                                    <td width="11%">                                                                        
                                                                        <asp:Image ID="Image1" ImageUrl="images/error.gif" Width="29" Height="24" Visible="false" runat="server" />
                                                                    </td>
                                                                    <td width="89%">
                                                                        
                                                                            <asp:Label ID="lblError" runat="server" Visible="false" ForeColor="#9a0406" Text="Selected tour is not available on the specified dates. Please change your search
                                                                            parameters."></asp:Label>
                                                                            
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div class="container_iframe">
                                <ul class="tabs">
                                    <li class="active"><a href="#home" >Overview</a></li><%--<asp:LinkButton ID="lnkHome" Text="Overview" OnClientClick="return ShowDiv('home')"></asp:LinkButton>--%>
                                    <%--<li><asp:LinkButton ID="LinkButton1" Text="Inclusions & Exclusions" OnClientClick="return ShowDiv('home')"></asp:LinkButton>--%>
                                    <li><a href="#Inclusions">Inclusions & Exclusions</a></li>
                                    <li><a href="#Itinerary">Itinerary</a></li>
                                    <li><a href="#RoomRates">Price</a></li>
                                    <li><a href="#DepartureDates">Departure Dates</a></li>
                                    <li><a href="#Terms">Terms & Conditions</a></li>
                                    <li><a href="#Air">Airline</a></li>
                                    <li><a href="#Hotel">Hotel</a></li>
                                </ul>
                                <div class="pill-content">
                                    <div class="active" id="home">
                                        <%=(activity != null ? activity.Overview : "") %>
                                    </div>
                                    <div id="Inclusions">
                                        <label>
                                        <h3>Inclusions:</h3>
                                        <ul></ul>
                                        <%if (activity != null && activity.Inclusions != null && activity.Exclusions != null)
                                          { %>
                                            <%foreach (string item in activity.Inclusions)
                                              { %>
                                              <li style=" list-style-type:circle; margin-left:20px;"><%=item%></li>
                                            <%} %></label>
                                        <label>
                                        <h3>Exclusions:</h3>
                                            <%foreach (string item in activity.Exclusions)
                                              { %>
                                            <li style=" list-style-type:circle; margin-left:20px;"><%=item%></li>
                                            <%}
                                          }%></label>
                                    </div>
                                    <div id="Itinerary">
                                    <div style="height:350px; font-size:14px; overflow-y:auto;">
                                          
                                            <asp:DataList ID="dlFDItinerary" runat="server" CellPadding="4" DataKeyField="Id"
                                                Width="900px">
                                                <ItemTemplate>
                                                        <table style=" border: solid 1px #ccc; margin-bottom:10px;" class="" width="100%">
                                                           <tr style="height: 25px;">
                                                                <td>

                                                                        <table style="margin-bottom:14px; margin-top:10px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                        <td width="14%"> <strong style=" font-size:14px">Day <%# Eval("Day")%> </strong></td>
                                                                        <td width="86%"></td>
                                                                        </tr>

                                                                        </table>
                                                   
                                                                   
                                                                    
                                                                    
                                                                    
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr style="height: 25px;">
                                                                <td>
                                                                  
                                                                  
                                                                  
                                                                  <table style="margin-bottom:16px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="14%"><strong>ItineraryName: </strong></td>
    <td width="86%"><%# Eval("ItineraryName")%></td>
  </tr>
  
</table>
                                                                  
                                                                    
                                                                    
                                                                    
                                                                    
                                                                </td>
                                                            </tr>
                                                                    <tr style="height: 25px;">
                                                                <td>
                                                                
                                 <table style="margin-bottom:10px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="14%"><strong>  Meals:</strong> </td>
    <td width="86%"><%# Eval("Meals")%></td>
  </tr>
  
</table>
                                                                
                                                                    
                                                                </td>
                                                            </tr>
                                                             <tr style="height: 25px;">
                                                                <td>
                                                                   
                                                                   
                                                                   
  <table style="margin-bottom:14px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" width="14%"> <strong>Itinerary: </strong></td>
    <td width="86%"><%# Eval("Itinerary")%></td>
  </tr>
  
</table>
                                                                   
                                                                   
                                                                   
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                        <%--<label>
                                            <%=(activity != null ? activity.Itinerary1 : "") %></label>
                                        <label>
                                            <%=(activity != null ? activity.Itinerary2 : "") %></label>
                                            <%=(activity != null ? activity.Details : "") %></label>
                                            <h3>Transfer included</h3>
                                           <li style=" list-style-type:circle; margin-left:20px;"> <%=(activity != null ? activity.TransferIncluded : "") %></li>
                                            <h3>Meals Included</h3>
                                            <ul>
                                            <%if(activity != null && activity.MealsIncluded != null){ %>
                                            <%string[] mealItems = activity.MealsIncluded.Split(','); %>
                                            <%foreach (string meal in mealItems)
                                              { %>
                                              <li style=" list-style-type:circle; margin-left:20px;"><%=meal %></li>
                                            <%} %>
                                            <%} %>
                                            </ul>--%>
                                    </div>
                                    <div id="RoomRates">
                                    <label>
                                    <h3>Room Rates</h3>
                                    <table style=" font-weight:bold" class="grid-tble" id="tblPriceDates" width="100%" border="0" runat="server"></table>
                                            <%--<asp:DataList ID="dlRoomRates" runat="server" Width="100%">
                                                <ItemTemplate>
                                                    <table width="100%" border="1">
                                                  
                                                        <tr>
                                                            <td height="25px" width="30%">
                                                        <%#(Eval("PriceDate")==DBNull.Value)?string.Empty: Convert.ToDateTime(Eval("PriceDate")).ToString("dd-MMM-yyyy")%>
                                                        </td>
                                                            <td height="25px" width="30%">
                                                                <%#Eval("Label") %>
                                                            </td>
                                                           
                                                            <td width="70%">
                                                            <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>
                                                              <%#CTCurrencyFormat(Eval("Total"))%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>--%>
                                            </label>
                                    </div>
                                    <div id="DepartureDates">
                                    <span id="DepDate">                                    
                                    </span>
                                    </div>
                                 <div id="Terms">
                                    
                                     <%--<ul>
                                        <%if (activity != null && activity.CancelPolicy != null)
                                          { %>
                                        <%string[] cancelP = activity.CancelPolicy.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                        <%foreach (string cancel in cancelP)
                                          { %>
                                          <li style=" list-style-type:circle; margin-left:20px;"><%=cancel %></li>
                                        <%} %>
                                        <%} %>
                                        </ul>
                                        
                                        
                                        <h3>Things to bring</h3>
                                        <ul>
                                        <%if (activity != null && activity.ThingsToBring != null)
                                          { %>
                                        <%string[] things = activity.ThingsToBring.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                        <%foreach (string thing in things)
                                          { %>
                                          <li style=" list-style-type:circle; margin-left:20px;"><%=thing %></li>
                                        <%} %>
                                        <%} %>
                                        </ul>--%>
                                        <h3 style="background-color:#18407B;padding-left: 10px; color: #FFFFFF; font-size: 12px;">Partial Payment Policy</h3>
                                        <ul>
                                        <li>
                                         <table width="100%" border="1" cellpadding="0" cellspacing="0">
                                        <tr><td width="25%">Booking Deposit </td><td width="75%">AED 500 per person </td></tr>
                                        <tr><td>2nd Payment</td><td>50% of the balance amount (45 days before departure or with visa submission whichever comes first  )</td></tr>
                                        <tr><td>Final Payment</td><td>Full payment (30 days before departure )</td></tr>
                                        <tr><td>Optional tours / Extras</td><td>Full Payment at the time of booking</td></tr>
                                        </table>
                                        </li>
                                        </ul>
                                        <h3 style="background-color:#18407B;padding-left: 10px; color: #FFFFFF; font-size: 12px;">Cancellation Policy - Applicable for partial payment </h3>
                                        <ul>
                                        <li>
                                       <table  width="100%" border="1" cellpadding="0" cellspacing="0">
                                        <tr><td width="25%">Booking Deposit </td><td width="75%">Non Refundable  </td></tr>
                                        <tr><td>44 days -31days to departure</td><td>10% of the tour price paid will be refunded</td></tr>
                                        <tr><td>30 days -15 days to departure</td><td>50% of the tour price paid will be refunded</td></tr>
                                        <tr><td>14 days & less</td><td>Non Refundable </td></tr>
                                        <tr><td>Optional Tours / Extras</td><td>Non Refundable</td></tr>
                                        </table>
                                        </li>
                                        </ul>
                                        <h3 style="background-color:#18407B;padding-left: 10px; color: #FFFFFF; font-size: 12px;">Cancellation Policy - Applicable for full payment  </h3>
                                        <ul>
                                        <li>
                                       <table  width="100%" border="1" cellpadding="0" cellspacing="0">
                                        <tr><td width="25%">Booking Deposit </td><td width="75%">Non Refundable  </td></tr>
                                        <tr><td>44 days -31days to departure</td><td>50 % of the tour price paid will be refunded</td></tr>
                                        <tr><td>30 days -15 days to departure</td><td>25 % of the tour price paid  will be refunded </td></tr>
                                        <tr><td>14 days & less</td><td>Non Refundable </td></tr>
                                        <tr><td>Optional Tours / Extras</td><td>Non Refundable</td></tr>
                                        </table>
                                        </li>
                                        </ul>
                                        <h3 style="display:none">Payment Policy</h3>
                                        <ul style="display:none">
                                        
                                        </ul>
                                        <br />
                                        <h3 style="display:none">Cancellation Policy</h3>
                                     <ul style="display:none">
                                        
                                        </ul>
                                        
                                        On behalf of the persons booked, I/We have read, understood & accepted the booking <a href="<%=Request.Url.Scheme%>://www.cozmotravel.com/FixDepTerms.aspx" target=_blank>Terms & Conditions</a>. I /We being duly authorized by the said person do hereby agree & accept the same for self & on behalf of the said persons.
                                    </div>
                                     <div id="Air">
                                      <h3>Airline:</h3>
                                        <asp:Label ID="lblAir" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div id="Hotel">
                                    <h3>Hotel Accommodation:</h3>
                                        <asp:Label ID="lblHotel" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clear">
                </div>
            </div>
          
    
    <div id="Outcontainer1" style="position:absolute;display: none; right:60px; top:164px;  border:solid 0px #ccc;  z-index:200;  " >

    <a onclick="document.getElementById('Outcontainer1').style.display = 'none'">
        <img style="cursor:pointer; position:absolute; z-index:300;  top:-12px; right:-10px;" src="images/cross_icon.png" />

</a>

<div id="container1" style="border:1px solid #ccc;">
</div>

</div>
<script>
    document.getElementById('DepDate').innerHTML = cal;
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

