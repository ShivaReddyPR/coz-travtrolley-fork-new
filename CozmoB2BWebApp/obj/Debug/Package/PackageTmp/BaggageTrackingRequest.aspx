﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="BaggageTrackingRequest.aspx.cs" Inherits="CozmoB2BWebApp.BaggageTrackingRequest" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <link href="DropzoneJs_scripts/dropzone.css" rel="stylesheet" />
    <script src="DropzoneJs_scripts/dropzone.js"></script>
    <script>
        var dropZonePIR;
        var dropZoneBP;
        var dropZonePIRObj;
        var dropZoneBoardPassObj;
        var pirFiles;
        var boardpassFiles;

        function InitDropZone() {
            $(".closepnldocs").on('click', function () {
                $(".pnldocs").hide();
                $(".pnldrop").hide();
                if (document.getElementById('lnkRemove') != null) {
                    document.getElementById('lnkRemove').click();

                }
            });

            Dropzone.autoDiscover = false;
            Dropzone.prototype.defaultOptions.acceptedFiles = "image/jpeg,application/pdf";
            Dropzone.prototype.defaultOptions.maxFiles = "1";
            // for PIR files Upload
            dropZonePIRObj = {

                url: "hn_BaggageTrackingFileUploader.ashx",
                maxFiles: 5,
                addRemoveLinks: true,
                success: function (file, response) {
                    pirFiles = $('#ctl00_cphTransaction_dzPIRFiles').val() + file.name + "|";
                    
                    $('#ctl00_cphTransaction_dzPIRFiles').val(pirFiles);
                    var imgName = response;
                    file.previewElement.classList.add("dz-success");
                    console.log("Successfully uploaded :" + imgName);
                    $('#dzPIRFileserror').html("");
                    $('#dzBoardPassFileserror').html("");

                },
                removedfile: function (file) {
                    var ref;
                    if (file.previewElement) {
                        if ((_ref = file.previewElement) != null) {
                            _ref.parentNode.removeChild(file.previewElement);
                        }
                    }
                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "BaggageTrackingRequest.aspx/RemoveFile",
                        // data: {},
                        data: "{'response':'" + file.name + "'}",
                        dataType: "json",
                    });
                    return this._updateMaxFilesReachedClass();

                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                }
            }
            dropZonePIR = new Dropzone('#ctl00_cphTransaction_dZUploadpir', dropZonePIRObj);
            // for Boarding Pass files Uploads
            dropZoneBoardPassObj = {
                url: "hn_BaggageTrackingFileUploader.ashx",
                maxFiles: 5,
                addRemoveLinks: true,
                success: function (file, response) {
                    boardpassFiles = $('#ctl00_cphTransaction_dzBoardPassFiles').val() + file.name + "|";
                    
                    $('#ctl00_cphTransaction_dzBoardPassFiles').val(boardpassFiles);
                    var imgName = response;
                    file.previewElement.classList.add("dz-success");
                    console.log("Successfully uploaded :" + imgName);
                },
                removedfile: function (file) {
                    var ref;
                    if (file.previewElement)
                    {
                        if ((_ref = file.previewElement) != null) {
                            _ref.parentNode.removeChild(file.previewElement);
                        }
                    }

                    $.ajax({
                        type: "POST",
                        contentType: "application/json; charset=utf-8",
                        url: "BaggageTrackingRequest.aspx/RemoveFile",
                        // data: {},
                        data: "{'response':'" + file.name + "'}",
                        dataType: "json",

                       
                    });
                    return this._updateMaxFilesReachedClass();
                 

                     // file must be added manually

                   // $('#ctl00_cphTransaction_dzBoardPassFiles').val(files);
                    //file.previewElement.remove();
                    
                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                }
            }
            dropZoneBP = new Dropzone('#ctl00_cphTransaction_dZUpload', dropZoneBoardPassObj);
        }

        $(document).ready(function () {
            $(".ssr-info").click(function () {
                $(".ssr-infodiv").toggle();
                change();
            });
            $(".booking-info").click(function () {
                $(".booking-infodiv").toggle();
            });
            InitDropZone();
        });
        function showpopup() {
            $('#FareDiff').modal('show');
            InitDropZone();
           
        }
        function change() {
            if (document.getElementById('ssrDiv').style.display == "none") {
                document.getElementById('ssrDiv').style.display = "block";
                document.getElementById('ssr').innerHTML = "Hide SSR Information";
            }
            else {
                document.getElementById('ssrDiv').style.display = "none";
                document.getElementById('ssr').innerHTML = "Show SSR Information";
            }
        }

    </script>
    <script type="text/javascript">
        $(document).ready(function () {
           

        });


        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function validateEmail(emailField) {
            var reg = /\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*/;
            
            if (reg.test(emailField) == false) {
                //alert('Invalid Email Address');
                $('#emailerror').html("Invalid Email ID.");
                return false;
            }
            return true;
        }

        function isAlphaNumeric(e) {

            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 9 || keyCode == 8 || keyCode == 11);
            return ret;
        }


        function BaggageValidation() {
            var status = true;
            var txtpnr = $("#<%=txtpnr.ClientID%>").val().trim();
            if (txtpnr == "") {
                $('#pnrerror').html("Enter CTW PNR");
                status = false;
            }
            else { $('#pnrerror').html(""); }

            var txtpaxname = $("#<%=txtpaxname.ClientID%>").val().trim();
            if (txtpaxname == "") {
                $('#paxerror').html("Enter Passenger Name.");
                status = false;
            } else { $('#paxerror').html(""); }

            var txtmobileNo = $("#<%=txtmobileNo.ClientID%>").val().trim();
            if (txtmobileNo == "") {
                $('#mobileNoerror').html("Enter Mobile Number.");
                status = false;
            } else { $('#mobileNoerror').html(""); }

            var txtemail = $("#<%=txtemail.ClientID%>").val().trim();
            if (txtemail == "") {
                $('#emailerror').html("Enter Email ID.");
                status = false;
            } else { $('#emailerror').html(""); }

            status = validateEmail(txtemail);
           
                var ddlbags = $("#<%=ddlbags.ClientID%>").val().trim();
                if (ddlbags == "-1") {
                    $('#ddlbagserror').html("Select Undelivered bags .");
                     status = false;
                } else { $('#ddlbagserror').html(""); }

                var cPIRObj = dropZonePIR.getAcceptedFiles();
                var filesCountPIRObj = dropZonePIR.files.length;//cPIRObj.length;
                if (filesCountPIRObj < 1) {
                    $('#dzPIRFileserror').html("Select PIR / BIR files.");
                     status = false;
                } else {
                    $('#dzPIRFileserror').html("");
                }

                var cBoardPassObj = dropZoneBP.getAcceptedFiles();
                var filesCountBoardPassObj = dropZoneBP.files.length;//cBoardPassObj.length;
                if (filesCountBoardPassObj < 1) {
                    $('#dzBoardPassFileserror').html("Select Boarding Pass files.");
                    status = false;
                } else {
                    $('#dzBoardPassFileserror').html("");
                }
            

            if (status == true) {
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "BaggageTrackingRequest.aspx/getPassengerDetails",
                    // data: {},
                    data: "{'paxPnr':'" + txtpnr + "','paxName':'" + txtpaxname + "','mobeleNo':'" + txtmobileNo + "','email':'" + txtemail + "'}",
                    dataType: "json",
                    success: function (success) {
                        if (success.d) {
                            status = true; 
                        }
                        else {
                            status = false;
                        }
                    },
                });
            }
            return status;
        }
        function ClearBaggageSelectedValues() {
            $("#<%=txtpnr.ClientID%>").val("");
            $("#<%=txtpaxname.ClientID%>").val("");
            $("#<%=txtmobileNo.ClientID%>").val("");
            $("#<%=txtemail.ClientID%>").val("");
            $("#<%=ddlbags.ClientID%>").val("-1");
            location.reload();
            //dropZonePIR = new Dropzone('#ctl00_cphTransaction_dZUploadpir', dropZonePIRObj);
            //dropZoneBP = new Dropzone('#ctl00_cphTransaction_dZUpload', dropZoneBoardPassObj);
        }

       

    </script>
    <style type="text/css">
        .footer_big, .bggray, .register_man {
            display: none;
        }
    </style>
    <link href="css/bs4-utilities.css" rel="stylesheet" type="text/css" />

    <div class="cz-container">
        <div class="body_container">

            <h3 class="py-4">Cozmo Travel World Baggage Tracking Request(BTR)</h3>

            <p>
                Before reporting baggage mishandled to Cozmo Travel World, passenger must first notify the airline that the baggage did not arrive at the destination of your flight. The airline will have provided you with the tracking reference number that you will need to file the baggage tracking request to Cozmo Travel World.

Please complete the fields exactly as they appear on your claim report with the airline.
            </p>

            <p class="py-4"><strong>Baggage Tracking Request Form:</strong></p>

            <div class="CorpTrvl-tabbed-panel CorpTrvl-page">

                <div style="background-color: #eeeeee; padding: 15px;">
                    <div style="padding: 15px; background-color: #fff;">
                        <div class="row">
                            <div class="col-xs-12 pb-3"><strong>Please enter your Cozmo Travel World PNR details and information of your airline:</strong></div>
                            <div class="col-sm-4 col-md-3">
                                <div class="form-group">
                                    <label>CTW PNR <span class="req-star">*</span>:</label>
                                    <asp:TextBox ID="txtpnr" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event)" ondrop="return false;" onpaste="return true;" MaxLength="20" />
                                    <span id="pnrerror" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-3">
                                <div class="form-group">
                                    <label>Passenger Name <span class="req-star">*</span>:</label>
                                    <asp:TextBox ID="txtpaxname" runat="server" CssClass="form-control"  onkeypress="return isAlphaNumeric(event)" ondrop="return false;" onpaste="return true;" MaxLength="50" />
                                    <span id="paxerror" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-3">
                                <div class="form-group">
                                    <label>Mobile Number <span class="req-star">*</span>:</label>
                                    <asp:TextBox ID="txtmobileNo" runat="server" CssClass="form-control" onKeyPress="return isNumber(event)" MaxLength="15" />
                                    <span id="mobileNoerror" style="color: red;"></span>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-3">
                                <div class="form-group">
                                    <label>Email Id <span class="req-star">*</span>:</label>
                                    <asp:TextBox ID="txtemail" runat="server" CssClass="form-control" />
                                    <span id="emailerror" style="color: red;"></span>
                                    <%--<input type="text" class="form-control" placeholder="" />--%>
                                </div>
                            </div>
                            
                        </div>
                        <div class="row">
                            <div class="col-xs-12 pb-3"><strong>When you filed a lost baggage report with the airline you received a reference number, please enter details here:</strong></div>
                            <div class="col-sm-4 col-md-3">
                                <div class="form-group">
                                    <label>Undelivered bags <span class="req-star">*</span>:</label>
                                    <asp:DropDownList ID="ddlbags" runat="server" CssClass="form-control">
                                        <asp:ListItem Value="-1">--Select--</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                    </asp:DropDownList>
                                    <span id="ddlbagserror" style="color: red;"></span>
                                    <%--   <select class="form-control">
                                        <option>1</option>
                                        <option>2</option>
                                    </select>--%>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-3">
                                <div class="form-group">
                                    <div class="padding-0">
                                        <asp:Panel class="pnldrop" ID="Panel1" runat="server" Style="display: block; width: 100%;background-color: White; border-color: blue">

                                            <div id="dZUploadpir" class="dropzone" runat="server" style="width:100%;" accept="image/jpeg,application/pdf">
                                                <div class="dz-default dz-message">
                                                    Upload or Drag Drop PIR / BIR copy*: (jpeg / pdf files only).
                                                </div>
                                            </div>
                                            <%--                                            <asp:Button ID="btnUploadFiles" runat="server" Text="Upload Files" CssClass="button-normal" accept="image/jpeg,application/pdf" />--%>
                                            <%--                                            <asp:Panel ID="pnlUploadedFiles" runat="server" Width="400px" Height="200px" class="pnldrop" BorderStyle="Solid" BorderWidth="1px" BorderColor="Gray" Style="overflow: auto;">--%>
                                            <%--<div class="ns-h3">
                                                <span>View Files</span>
                                            </div>--%>
                                        </asp:Panel>
                                        <input type="text" id="dzPIRFiles" style="display: none" runat="server" />
                                        <span id="dzPIRFileserror" style="color: red;"></span>

                                    </div>
                                    <%--<label>PIR / BIR copy*: (jpeg / pdf files only) <span class="req-star">*</span>:</label>
                                    <input type="file" runat="server" class="form-control" id="PirBirfiles" accept="image/jpeg,application/pdf" placeholder=" " />
                                    <span id="PirBirfileserror" style="color: red;"></span>--%>
                                </div>
                            </div>
                            <div class="col-sm-4 col-md-3">
                                <div class="form-group">
                                    <div class="padding-0">
                                        <asp:Panel class="pnldrop" ID="pnlDragandDrop" runat="server" Style="display: block; width: 100%; background-color: White; border-color: blue">

                                            <div id="dZUpload" class="dropzone" runat="server" style="width:100%" accept="image/jpeg,application/pdf">
                                                <div class="dz-default dz-message">
                                                    Upload or Drag Drop Boarding Pass copy: (jpeg / pdf files only) here.
                                                </div>
                                            </div>
                                            <%--                                            <asp:Button ID="btnUploadFiles" runat="server" Text="Upload Files" CssClass="button-normal" accept="image/jpeg,application/pdf" />--%>
                                            <%--                                            <asp:Panel ID="pnlUploadedFiles" runat="server" Width="400px" Height="200px" class="pnldrop" BorderStyle="Solid" BorderWidth="1px" BorderColor="Gray" Style="overflow: auto;">--%>
                                            <%--<div class="ns-h3">
                                                <span>View Files</span>
                                            </div>--%>
                                        </asp:Panel>
                                        <input type="text" id="dzBoardPassFiles" style="display: none" runat="server" />
                                        <span id="dzBoardPassFileserror" style="color: red;"></span>
                                    </div>
                                    <%--  <label>Boarding Pass copy: (jpeg / pdf files only) <span class="req-star">*</span>:</label>
                                    <input type="file" runat="server" class="form-control" id="boardingPass" accept="image/jpeg,application/pdf" placeholder="" />
                                <span id="boardingPasserror" style="color:red;"></span>--%>
                                </div>
                            </div>
                        </div>
                        <div class="row top-buffer">

                            <div class="col-xs-12">

                                <!--OnClientClick="return Save();" -->
                                <asp:Button ID="btnsubmit" Text="Submit" runat="server" class="btn btn-primary ml-0" OnClientClick="javascript:return BaggageValidation();" OnClick="btnsubmit_Click" />
                                <asp:Button ID="btncancel" Text="Cancel" runat="server" class="btn btn-default" OnClientClick="javascript:ClearBaggageSelectedValues();" />

                            </div>
                             <div class="col-xs-12" id="resltmsg" style="display: none; color: green;" runat="server">Successfully Updated.</div>
                        </div>
                    </div>
                </div>
                <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">

                        <button type="button" data-dismiss="modal" class="close mt-0" style="color: #fff; opacity: 1;">
                            <span aria-hidden="true">×</span>
                        </button>

                        <h4 class="modal-title" id="rtgrt">Tracking Status</h4>
                    </div>
                    <span id="array_disp"></span>
                    <div class="modal-body p-0">
                        <div id="popupData" runat="server">
						  Tracking Number :<%=trackNumber %> 
                    </div>
                </div>
            </div>
        </div>
                    </div>

                <p class="pt-5 pb-3">Please read terms and conditions:-</p>

                <p class="pt-3 pb-3">
                    If your bag was mishandled by an airline that only issues property irregularity reports (PIR) by the airline or through the airport, or a baggage irregularity report (BIR) via the airline. Please also send a copy of your PIR, or BIR to helpdesk@cozmotravel.com and put your Cozmo Travel World PNR number in the subject line, after filing your baggage tracking request with us. You must intimation Cozmo Travel World of mishandled baggage within 10 hours for International and 3 hours for domestic of your flight arrival destination and send a copy of PIR or BIR report within 10 hours for International and 3 hours for domestic of your flight landing for your report to be considered valid. If a copy of the report is not provided within 10 hours for International and 3 hours for domestic of your flight landing, Cozmo Travel World will deny your report immediately upon expiration of the 10 hours for International and 3 hours for domestic deadline. Once the bag referenced in your file is delivered to the airport on record, your Cozmo Travel World lost baggage tracking request will be closed immediately, and your bags will be considered returned under our terms and conditions. Any bag within this category will be considered "returned" per our terms and conditions once it arrives at the airport on record with the flight itinerary or forwarding information provided to the airline by the customer when filing your lost baggage claim with the airline.
                </p>

                <p class="pt-3 pb-3">If your bag was mishandled by an airline that only issues property irregularity reports (PIR) through the airport (lost and found), or a baggage irregularity report (BIR) via the airline, Cozmo Travel World will only issue our Tracking Lapse Commitment for a bag mishandled for more than 99 hours.Cozmo Travel World will assist in the tracking of your undelivered luggage as normal for the first 99 hours of the bags being mishandled.</p>

                <p class="pt-3 pb-3">If the airline has not provided you with a valid file reference number, you will need to contact the airline and collect it from them before filling your request with Cozmo Travel World. Cozmo Travel World will not retrieve file reference numbers from the airline. It is the passenger’s responsibility to collect this information from the airline before filling their baggage tracking request with us.</p>

                <p class="pt-3 pb-3">If a delivery address was not provided to the airline when filing an airline lost baggage claim, Cozmo Travel World will close your case once the bag arrives at the designated airport.</p>

                <p class="pt-3 pb-3">If the passenger indicates to the airline with which they have a claim, or to Cozmo Travel World, at any point, that their bag will be picked up as opposed to delivered, the baggage report status will be set to closed, and all bags will be considered returned, as soon as the bag arrives at the airport.</p>
            </div>

        </div>
    </div>

    <div style="display: none" id="footer">
            <span class="frright01">
                <div id="ctl00_upnlGrandTotal">
                </div>
            </span>
            <span class="frright">Powered by <a href="#">
                <img src="images/czit.png" alt="#" /></a></span>

        </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>
