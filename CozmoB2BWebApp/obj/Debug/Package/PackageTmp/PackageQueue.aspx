﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PackageQueueGUI" MasterPageFile="~/TransactionBE.master" Title="Package Queue" Codebehind="PackageQueue.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%--<%@ Import Namespace="CT.HolidayDeals" %>--%>
<%@ Import Namespace="System.IO" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">

    <link href="css/TicketQuee.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <script>
        var cal1;
        var cal2;

        function CancelAmendBooking(cnf) {
            var val = document.getElementById('ctl00_cphTransaction_dlBookingQueue_ctl0' + cnf + '_txtRemarks').value;
            if (document.getElementById('ctl00_cphTransaction_dlBookingQueue_ctl0' + cnf + '_ddlBooking').value == "Select") {
                document.getElementById('ctl00_cphTransaction_dlBookingQueue_ctl0' + cnf + '_errRemarks').innerHTML = "Please Select Request";
                return false;
            }
            else if (val.length <= 0 || val == "Enter Remarks here") {
                
                document.getElementById('ctl00_cphTransaction_dlBookingQueue_ctl0' + cnf + '_errRemarks').innerHTML = "Enter remarks";
                return false;
            }
            else {
               
                document.getElementById('ctl00_cphTransaction_dlBookingQueue_ctl0' + cnf + '_errRemarks').innerHTML = "";
                return true;
            }
        }

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
            cal2.hide();
        }


        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());


            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

           
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);
        var Ajax;
        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        function ViewPaymentInfo(id, orderId, promoTranxId) {

            pblockId = id;
            var url = "PaymentInfoAjax";
            var paramList = 'isPackagePaymentInfo=true';
            paramList += '&orderId=' + orderId;
            paramList += '&blockId=' + pblockId;
            paramList += '&promoTranxId=' + promoTranxId;
            document.getElementById('PaymentInfo-' + pblockId).style.display = "block";
            document.getElementById('PaymentInfo-' + pblockId).innerHTML = "Loading...";

            Ajax.onreadystatechange = ShowPaymentInfoPopUp;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }



        function ShowPaymentInfoPopUp() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        var numberOfRecord = eval('<%=recordsPerPage%>');
                        for (var i = 0; i < numberOfRecord; i++) {
                            if (document.getElementById('PaymentInfo-' + i) != null)
                                document.getElementById('PaymentInfo-' + i).style.display = "none";

                        }
                        document.getElementById('PaymentInfo-' + pblockId).style.display = "block";
                        document.getElementById('PaymentInfo-' + pblockId).innerHTML = Ajax.responseText;

                    }
                }
            }
        }
    </script>
    <script type="text/javascript">

        function ViewBookingforPackage(tranxId) {
            var finalurl = "ViewBookingforPackage?tranxId=" + tranxId;
            window.location = finalurl;
        }
        function ViewInvoiceforPackage(tranxId) {
            window.open("ViewInvoiceforPackage?tranxId=" + tranxId, 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
        }
    </script>

    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0"></iframe>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; left: 250px; display: none; z-index: 9999">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 120px; left: 500px; display: none; z-index: 9999">
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdfParam" Value="1"></asp:HiddenField>
    <table cellpadding="0" cellspacing="0" class="label">
        <tr>
            <td style="width: 700px" align="left">
                <a style="cursor: pointer; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
                    onclick="return ShowHide('divParam');">Hide Parameter</a>
            </td>
        </tr>
    </table>
    <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true">
            <div class="paramcon">
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        From Date:
                    </div>
                    <div class="col-md-2">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputEnabled form-control"
                                        Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal1()">
                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">
                        To Date:
                    </div>
                    <div class="col-md-2">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal2()">
                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>

                    <div class="col-md-2">  Agent: </div> 
<div class="col-md-2"> <asp:DropDownList ID="ddlAgents" CssClass="form-control" runat="server" AppendDataBoundItems="true">
                                        <%--<asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>--%>
                                    </asp:DropDownList> </div> 

                    <div class="clearfix">
                    </div>
                </div>

                <div class="col-md-12 padding-0 marbot_10" style="display:none">


                    <%--<div class="col-md-2">
                        Pax Name:
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtPaxName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>--%>
                    <div class="col-md-2">
                        PNR No.
                    </div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtPNR" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                  <asp:Button Text="Search" CssClass="button" runat="server" ID="btnSearch" OnClick="btnSearch_Click"/>

                    </div>


            </div>
        </asp:Panel>
    </div>

    <div id="Paging" runat="server" visible="false" style="float:right;">
        
        <table>
  <tr>
    <td style="padding:5px">
       
        <asp:Button ID="btnfirst"  runat="server" Font-Bold="true" Text="<<" 
                     onclick="btnfirst_Click" /></td>
        <td style="padding:5px">
            <asp:Button  ID="btnprevious" runat="server" Font-Bold="true" Text="<" 
                    onclick="btnprevious_Click" /></td>
            <td style="padding:5px">
                <asp:Button  ID="btnnext" runat="server" Font-Bold="true" Text=">"  
                    onclick="btnnext_Click" /></td>
                <td style="padding:5px">
                    <asp:Button  ID="btnlast" runat="server" Font-Bold="true" Text=">>" 
                    onclick="btnlast_Click" /></td>
    </tr>
   </table>


    </div>
    <div>
        <asp:DataList ID="dlBookingQueue" runat="server" Width="100%"  OnItemDataBound="dlBookingQueue_ItemDataBound"
            OnItemCommand="dlBookingQueue_ItemCommand">
            <ItemTemplate>
                <div class="bg_white pad_10 bor_gray marbot_10">                  
                    <div class="col-md-12 martop_10">
                        <div class="col-md-4">
                            Supplier :<b><asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Supplier") %>' runat="server" ID="lblSupplier"></asp:Label>
                        </b> 
                            
                        </div>
                        <div class="col-md-4">
                            Status : <b style="color: #009933">
                                <asp:Label runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "BookingStatus") %>' ID="lblStatus"></asp:Label>
                            </b>
                        </div>

                        <div class="col-md-4">
                            Confirmation No:<b><asp:Label  Text='<%# DataBinder.Eval(Container.DataItem, "TripId") %>'  runat="server" ID="lblConfirmationNo"></asp:Label>
                        </b></div>

                        <div class="clearfix"></div>
                    </div>
         
                    <div class="col-md-12 martop_10">

                        <div class="col-md-4">
                            Package Start Date. :

                                <b><asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Package_Start_Date") %>' runat="server" ID="lblPackageStartDate"></asp:Label>
                       </b> </div>

                        <div class="col-md-4">
                            Location : 
                               <b><asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Location") %>' runat="server" ID="lblLocation"></asp:Label>

                       </b> </div>

                       
                        <div class="col-md-4" id="supplierRefDiv" runat="server">
                            <asp:Label runat="server" ID="lblSupplierRefTxt" Text="Supplier Ref :"></asp:Label>
                            
                               <b> <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Supplier_Ref") %>' runat="server" ID="lblSupplierRef"></asp:Label>
                          </b>  
                        </div>
                       
                        



                        <div class="clearfix"></div>
                    </div>

                    <div class="col-md-12 martop_10">

                        <div class="col-md-4">
                            Lead Pax Name :
                            <b>
                                <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Lead_Pax_Name") %>' runat="server" ID="lblLeadPaxName"></asp:Label>
                                <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Lead_Pax_EMAIL") %>' runat="server" ID="lblLeadPaxEmail" style="display:none"></asp:Label>
                            </b>
                            
                        </div>
                        <div class="col-md-4">
                            No. Of Rooms : 
                            <b>
                                <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "RoomCount") %>' runat="server" ID="lblRoomsCount"></asp:Label>
                            </b>
                        </div>
                        <div class="col-md-4">
                            Total Price : 
                            <b>
                                <asp:Label runat="server" ID="lblTotalPrice"></asp:Label>
                                </b>
                            <br />
                            <asp:Label runat="server" ID="lblSupplierPrice" Text="Supplier Price:"></asp:Label>
                            <b>
                                <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Total_Package_Price") %>' runat="server" ID="lblTotalPackageSupplierPrice"></asp:Label>
                                </b>
                        </div>


                        <div class="clearfix"></div>
                    </div>
               
                    <div class="col-md-12 martop_10">

                        <div class="col-md-4">
                            Booked On :
                            <b>
                                <asp:Label  Text='<%# DataBinder.Eval(Container.DataItem, "TransactionDate") %>' runat="server" ID="lblBookedOn"></asp:Label>
                            </b>
                            
                        </div>
                        <div class="col-md-4">
                            No. Of Guests :
                            <b>
                                <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Guests") %>' runat="server" ID="lblGuests"></asp:Label>
                            </b>
                        </div>

                        <div class="col-md-4">
                            Booked by :
                            <b>
                                <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Booked_by") %>' runat="server" ID="lblBookedBy"></asp:Label>

                            </b>
                        </div>


                        <div class="clearfix"></div>
                    </div>

                      <div class="col-md-12 martop_10">

                        <div class="col-md-4">
                            Package Name:
                            <b><asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "PackageName") %>' runat="server" ID="lblPackageName"></asp:Label>
                        </b>
                            
                        </div>


                          <div class="col-md-4">                          
                            <asp:Label runat="server" ID="lblPromoCode" Text="Applied Promo Code:"></asp:Label>
                            <b>
                                <asp:Label Text='<%# DataBinder.Eval(Container.DataItem, "Promo_Code") %>' runat="server" ID="lblPromoCodeValue"></asp:Label>
                                </b>
                        </div>


                        

                        <div class="clearfix"></div>
                    </div>


                    <div class="col-md-12 padding-0 marbot_10">
                        <div class="col-md-8">
                            <label>
                              
                            </label> 



                            </div>
                        <div class="col-md-4">
                              <asp:Label ID="lblViewPackageBooking" runat="server" Text=""></asp:Label>
                            <asp:Label ID="lblViewPackageInvoice" runat="server" Text=""></asp:Label>
                            </div>

                        


                        </div>

                    <div class="col-md-12 martop_10">
                        <table width="100%">
                            <tbody>
                                <tr>
                                    <td>




                                        <div class="martop_10 paramcon">

                                            <div class="col-md-2 marbot_10"><asp:Label ID="lblRequestChange" runat="server" Text="Request Change :"></asp:Label></div>
                                            <div class="col-md-2 marbot_10">

                                                <asp:DropDownList CssClass="form-control" ID="ddlBooking" runat="server">
                                                            <asp:ListItem Selected ="True" Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="Cancel Booking" Value="Cancel Booking"></asp:ListItem>
                                                               
                                                            </asp:DropDownList>

                                              

                                                 <b id="errRemarks" style="color:Red" runat="server"></b>
                                            </div>
                                            <div class="col-md-3 marbot_10">

                                            
                                                     <asp:TextBox CssClass="form-control" ID="txtRemarks" TextMode="MultiLine" Rows="1" Text="Enter Remarks here"
                                                                runat="server" onfocus="this.style.color='#000000'; if( this.value=='Enter Remarks here' ) { this.value=''; }"></asp:TextBox>
                                

                                            </div>



                                            <div class="col-md-3 marbot_10">



                                                <asp:Button CommandName="SubmitRequest" CommandArgument='<%#Eval("TranxHeaderId") %>'  ID="btnRequest" runat="server"
                                                    Text="Submit Request"  CssClass="button" />
                                                <asp:Label ID="lblRequestStatus" BackColor="#9AF1A2" ForeColor="Black" runat="server"
                                    Font-Italic="True" Font-Names="Arial" Font-Size="10pt"></asp:Label>


                                            </div>


                                            <div class="clearfix"></div>
                                        </div>






                                    </td>
                                </tr>
                            </tbody>
                        </table>
<br />
                                            <div>
                                                <div style="position: relative">
                        <asp:LinkButton ID="lnkPayment" runat="server" Visible="false" Text="Payment Information" href="#"></asp:LinkButton>
                        <div id="PaymentInfo-<%#Container.ItemIndex %>" class="visa_PaymentInfo_pop" style="position: absolute;
                                                    display: none; bottom: -70px; left: 234px; height: auto!important; z-index: 9999;">
                        </div>
                    </div>
                                            </div>

                        <div class="clearfix"></div>

                    </div>



                   <hr style="border:1px solid #fff" />

                </div>
            </ItemTemplate>
            <FooterTemplate>
                <asp:Label Visible='<%#bool.Parse((dlBookingQueue.Items.Count==0).ToString())%>'
                    runat="server" ID="lblNoRecord" Text="No Record Found!"></asp:Label>
            </FooterTemplate>
        </asp:DataList>
    </div>
    <script type="text/javascript">
        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }
    </script>
</asp:Content>
