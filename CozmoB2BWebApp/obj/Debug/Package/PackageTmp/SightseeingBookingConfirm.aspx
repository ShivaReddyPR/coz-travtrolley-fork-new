﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="SightseeingBookingConfirm" Title="Sightseeing Booking Confirmation" Codebehind="SightseeingBookingConfirm.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="System.Collections.Generic" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript">
    if (history.length > 1) {
        history.forward(1);
    }
    function validateTerms() {
        if (document.getElementById('chkTerms').checked == true) {
            document.getElementById('validate').style.display = "none";
            <%if (!Settings.LoginInfo.AgentBlock) //checking Agent able to book or not Added By brahmam
    {%>
            if (document.getElementById('<%=hdnWarning.ClientID %>').value.length <= 0) {
                document.getElementById('MainDiv').style.display = "none";
                document.getElementById('PreLoader').style.display = "block";
            }
            history.forward(1);
            return true;
            <%}
    else
    {%>
            alert('Please contact Administrator'); //We are Showing Alert Message
            return false;
            <%}%>
        }
        else {
            document.getElementById('validate').style.display = "block";
            document.getElementById('MainDiv').style.display = "block";
            document.getElementById('PreLoader').style.display = "none";
            return false;
        }
    }
</script>
<asp:HiddenField ID="hdnWarning" runat="server" />
 <div id="MainDiv" class="mar-top-10">
        <div class="col-md-3 bg_white padding-0">
       
            <div class="">
                <div class="ns-h3">
                    Pricing Details</div>
                <div class="pad-botom-5">
                    <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="59%">
                                Sightseeing Cost
                            </td>
                            <td width="41%">
                                <%=(itinerary.Currency)%>
                                <%=Math.Ceiling(total)%>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                (Inclusive of all Taxes &amp; Fees)
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Total</strong>
                            </td>
                            <td>
                                <strong>
                                    <%=(itinerary.Currency)%>
                                    <%=Math.Ceiling(total).ToString("N" + Settings.LoginInfo.DecimalValue)%></strong>
                            </td>
                        </tr>
                        
                        <tr>
                            <td>
                                <strong class="spnred">Total Inc. Markup</strong>
                            </td>
                            <td>
                                <strong>
                                    <%=(itinerary.Currency)%>
                                    <%=(markup > 0 ? (markup).ToString("N" + Settings.LoginInfo.DecimalValue) : (total).ToString("N" + Settings.LoginInfo.DecimalValue))%></strong>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="ns-h3">
                    Sightseeing Details</div>
                <div class="pad-botom-5">
                    <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td width="42%">
                                <strong>Sightseeing name :</strong>
                            </td>
                            <td width="58%">
                                <%=itinerary.ItemName%>
                            </td>
                        </tr>
                      
                        <tr>
                            <td>
                                <strong>City Name : </strong>
                                <br />
                            </td>
                            <td>
                                <%=itinerary.CityName %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Language:
                                    <br />
                                </strong>
                            </td>
                            <td>
                                <%=itinerary.Language.Split('#')[0]%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Tour Date:
                                    <br />
                                </strong>
                            </td>
                            <td>
                                <%=itinerary.TourDate.ToString("dd-MMM-yyyy") %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Duration:
                                    <br />
                                </strong>
                            </td>
                            <td>
                                <%=itinerary.Duration == "" ? "N/A" : itinerary.Duration%>
                            </td>
                        </tr>
                        <%if (itinerary.DepPointInfo != null && itinerary.DepPointInfo.Length > 0)
                          { %>
                        <tr>
                            <td>
                                <strong>Departure Point: </strong>
                            </td>
                            <td>
                                  <%=itinerary.DepPointInfo.Split(',')[0]%>
                                 <%=itinerary.DepPointInfo.Split(',')[1]%>
                            </td>
                        </tr>
                        <%} %>
                        
                         <%int adults = 0, childs = 0;
                          adults += itinerary.AdultCount;
                          childs += itinerary.ChildCount;
                        %>
                       
                        <tr>
                            <td>
                                <strong>Passengers :<br />
                                </strong>
                            </td>
                            <td>
                                Adult
                                <%=adults%>, Child
                                <%=childs %>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <strong>Price :</strong>
                            </td>
                            <td>
                                <strong>
                                    <%=(itinerary.Currency)%>
                                    <%=Math.Ceiling(total).ToString("N" + agency.DecimalValue)%></strong>
                            </td>
                        </tr>
                    </table>
                </div>
                
                <div class="clear">
                </div>
            </div>
            
            <div class="clear">
            </div>
        </div>
        
        
        <div class="col-md-9 pad_right0 pad_xs0">
            <div class="ns-h3">
                Payment Confirmation</div>
            <div>
                <div>
                    <asp:MultiView ID="PaymentMultiView" runat="server" ActiveViewIndex="0">
                        <asp:View ID="NormalView" runat="server">
                         
                         <div class="bg_white pad_10 bor_gray"> 
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                           
                                           
                                           <tr> 
                                           <td colspan="2"> <asp:Label ID="lblMessage" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label></td>
                                           
                                           
                                           </tr>
                                           
                                           
                                            
                                            <% int count = 0;
                                               foreach (string paxDetails in itinerary.PaxNames)
                                               {
                                            %>
                                            <%if (count == 0)
                                              { %>
                                            <tr>
                                                <td  height="24" class="red_span" colspan="2">
                                                    <strong>Adult 1(Lead Guest)</strong>
                                                </td>
                                                <td>
                                                    &nbsp;
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="24" width="21%">
                                                    <strong>Name :<br />
                                                    </strong>
                                                </td>
                                                <td width="79%">
                                                    <%=paxDetails.Split('|')[0]%><br />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="24">
                                                    <strong>E-mail ID:</strong>
                                                </td>
                                                <td>
                                                    <%=itinerary.Email%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="24">
                                                    <strong>Phone No.:</strong>
                                                </td>
                                                <td>
                                                    <%=itinerary.PhNo%>
                                                </td>
                                            </tr>
                                            <%
                                                }
                                              else 
                                              { %>
                                            <tr>
                                                <td height="24" colspan="2" class="red_span">
                                                <%if (paxDetails.Contains("|"))
                                                  { %>
                                                    <strong><%=paxDetails.Split('|')[1]%></strong>
                                                    <%} else{%>
                                                    <strong>Other Guest</strong>
                                                    <%}%>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="24" width="21%">
                                                    <strong>Name :<br />
                                                    </strong>
                                                </td>
                                                <td width="79%">
                                                    <%=paxDetails.Split('|')[0]%><br />
                                                </td>
                                            </tr>
                                            <%}
                                              count++;

                                                               }%>
                                        </table>
                
                </td> </tr>
               
                               
        
              
              
              
                <tr>
                    <td height="21">
                       
                    </td>
                </tr>
              
                <tr>
                    
                </tr>
                </table> 
                
                </div>
                
                
                
                
                <div> 
                
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                
                
                            <tr>
                                <td colspan="2">
                                    <div style="padding: 10px; overflow: auto">
                                        <div>
                                            <span style="font-weight: bold;">Cancellation &amp; Charges</span>
                                        </div>
                                        <ul style="float: left; font-family: Calibri; font-size: 14px;">                                          
                                             <%if (string.IsNullOrEmpty(itinerary.CancellationPolicy))
                                            { %>
                                    <%string[] cdata = cancelData.Split('|');
                                        foreach (string data in cdata)
                                        { %>
                                       <li><%=data%></li>                                           
                                          <%}%>
                                       <%} else { %>
                                       <li><%=itinerary.CancellationPolicy%></li> 
                                       <%} %>
                                        </ul>
                                    </div>
                                    <%if (!string.IsNullOrEmpty(amendmentData) || amendmentData!="")
                                       { %>
                                    <div style="padding: 10px; overflow: auto">
                                        <div>
                                            <span style="font-weight: bold; float: left">Amendment Policies</span>
                                        </div>
                                        <br />
                                        <ul style="float: left; font-family: Calibri; font-size: 14px;">
                                            <%string[] aData = amendmentData.Split('|');
                                              foreach (string amendData in aData)
                                              {%>
                                            <li>
                                                <%=amendData%></li>
                                            <%}
                                            %>
                                        </ul>
                                    </div>
                                     <%} %>
                                    <%if (itinerary.Note != null && itinerary.Note.Length > 0)
                                      {%>
                                    <div style="padding: 10px; overflow: auto">
                                        <div>
                                            <span style="font-weight: bold; float: left">Notes &amp; Additional Information</span>
                                        </div>
                                        <br />
                                        <ul style="float: left; font-family: Calibri; font-size: 14px;">
                                            <li>
                                                <%=itinerary.Note.Split('#')[0]%></li>
                                           <%string[] addInfo = itinerary.Note.Split('#')[1].Split('|');
                                             foreach (string ad in addInfo)
                                             {
                                          %>
                                            <li>
                                                <%=ad%></li>
                                                <%} %>
                                        </ul>
                                    </div>
                                    <%
                                          }%>

                                     <%if (itinerary.DepPointInfo != null && itinerary.DepPointInfo.Length > 1)
                                          { %>
                                         <div style="padding: 10px;overflow:auto">
                                        
                                            <div>
                                                <span style="font-weight:bold; float: left">Pcikup Location &amp; Pcikup Date Time</span>
                                            </div><br />
                                             <ul style="float: left; font-family: Calibri; font-size: 14px;">
                                            
                                                 <%string[] aData = itinerary.DepPointInfo.Split(',');
                                       foreach (string depoint in aData)
                                       {%>
                                                <li>
                                                    <%=depoint%></li>
                                                <%}
                                                %>
                                                     
                                             </ul>
                                        </div>
                                         <%
                                          }%>
                                    <%if(!string.IsNullOrEmpty(itinerary.PickupPoint))
                                            { %>
                                     <div style="padding: 10px;overflow:auto">
                                        
                                            <div>
                                                <span style="font-weight:bold; float: left">Guest PickUp Point</span>
                                            </div><br />
                                             <ul style="float: left; font-family: Calibri; font-size: 14px;">
                                              <li>
                                                    <%=itinerary.PickupPoint%></li>
                                             </ul>
                                        </div>
                                         <%
                                          }%>
                                </td>
                            </tr>
                        
                <tr>
                    <td>
                        <div style="color: Red; font-size: 14px">
                            <%=warningMsg %>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td height="21">
                        <hr class="b_bot_1 " />
                    </td>
                </tr>
                <tr>
                    <td colspan="2" style="text-align: right">
                        <table width="100%">
                            <tr class="nA-h4">
                                <td>
                                    Available Balance :
                                </td>
                                <td style="font-weight: bold">
                                    <%=agency.CurrentBalance.ToString("N" + agency.DecimalValue)%>
                                </td>
                            </tr>
                            <tr class="nA-h4">
                                <td>
                                    Total Price :
                                </td>
                                <td style="font-weight: bold">
                                    <%=Math.Ceiling(total).ToString("N" + agency.DecimalValue)%>
                                </td>
                            </tr>
                            <tr class="nA-h4">
                                <td>
                                    <%if (location.CountryCode == "IN")
                                        { %>
                                         
                                                Total GST :
                                        <%}
                                    else
                                    {%>
                                         
                                                Vat :
                                            
                                        <%}%>
                                </td>
                                            <td style="font-weight: bold">
                                                <%=outPutVat.ToString("N" + agency.DecimalValue)%>
                                            </td>
                                        </tr>
                            <tr class="nA-h4">
                                <td>
                                    Amount to be Booked :
                                </td>
                                <td class="spnred" style="font-weight: bold">
                                    <%=(Math.Ceiling(total) + Math.Ceiling(outPutVat)).ToString("N"+agency.DecimalValue)%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="21">
                        <hr class="b_bot_1 ">
                    </td>
                </tr>
                <tr>
                    <td colspan="2">
                    
                    
                    
                    <div> 
                       <div class="col-md-9">  
                       <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="97%" align="left">
                                    <label>
                                        <input type="checkbox" name="chkTerms" id="chkTerms"  />
                                        I have reviewed and agreed on the rates and commision offered for this booking
                                    </label>
                                    
                                </td>
                            </tr>
                            
                            
                            
                            
                            <tr> 
                            
                            <td> &nbsp;
                            
                            <div id="validate" style="display: none; color: Red">
                                        <span>Please check the Terms checkbox to continue</span>
                                    </div>
                            </td>
                            
                            </tr>
                            
                     
                            
                            
                            
                            
                        </table>
                       
                       
                        </div>
                       
                       
                    
                    <div class="col-md-3"> 
                    
                    <asp:Button ID="imgMakePayment" runat="server" CssClass="btn but_b pull-right" Text="Confirm"
                                OnClick="imgMakePayment_Click" OnClientClick="return validateTerms();" />
                    
                    </div>
                    
                    
                    <div class="clearfix"> </div>
                    </div>
                        
                    </td>
                </tr>
                </table>
                
                
                </div>
                
                
                </asp:View>
                <asp:View ID="ErrorView" runat="server">
                    <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                    <br />
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Sightseeing.aspx">Go to Search Page</asp:HyperLink>
                    <br />
                    <%--<asp:ImageButton ID="ImageButton1" runat="server"  
                            ImageUrl="~/images/continue.jpg" onclick="ImageButton1_Click"/>--%>
                </asp:View>
                </asp:MultiView>
            </div>
            <div class="clear">
            </div>
        </div>
        <div class="clear">
        </div>
    </div>
   
    <div class="clear">
    </div>
    </div> 
    <div id="PreLoader" style="display: none">
        <div class="loadingDiv">
            <div>
                <img src="images/preloader11.gif" /></div>
            <div style="font-size: 16px; color: #999999">
                <strong>Awaiting Sightseeing Booking confirmation from</strong>
            </div>
            <div style="font-size: 21px;" >
                <strong class="primary-color"><span id="searchCity">
                    <%=itinerary.ItemName %></span> </strong>
                <br />
                <strong style="font-size: 14px; color: black">do not click the refresh or back button
                    as the transaction may be interrupted or terminated.</strong>
            </div>
        </div>
        <div class="parameterDiv">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%">
                       
                            <strong>Tour Date:
                        <span class="primary-color" id="fromDate">
                            <%=itinerary.TourDate.ToString("dd/MM/yyyy") %>
                            
                            </span></strong>
                    </td>
                    <td width="50%">
                        <%--<label class="parameterLabel">
                            <strong>Check out:</strong></label>
                        <span id="toDate"><%=itinerary.EndDate.ToString("dd/MM/yyyy") %></span>--%>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
