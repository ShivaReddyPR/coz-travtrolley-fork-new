﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="AddVisaCity" Title="Cozmo Travels" ValidateRequest="false" Codebehind="AddVisaCity.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
        
        
           <div> 
    <div class="col-md-6"> <h4> 
   
                 <% if (UpdateCity > 0)
                           {%>
                               <span>Update Visa City</span>

                          <% }
                           else
                           {  %>
                               Add Visa City 
                          <% }
                               %>
    
    </h4> </div>
    
    <div class="col-md-6">
    
    
    <asp:HyperLink class="fcol_blue pull-right" ID="HyperLink1" runat="server" NavigateUrl="~/VisaCityList.aspx">Go to Visa City List</asp:HyperLink>
   

 </div>     
    
    <div class="clearfix"> </div> 
    </div>
    
    <div>
                  <asp:Label class="font-red" runat="server" ID="lblErrorMessage" Text=""></asp:Label>
            </div>
            
            
       <div class="paramcon"> 
    
                <div> 
    <div class="col-md-2"><label>
                            Country<sup style="color:Red">*</sup></label> </div>
    
    <div class="col-md-2"> <p>
                        
                        <span>
                            <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control">
                            </asp:DropDownList>
                        </span><asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please select country "
                            ControlToValidate="ddlCountry" InitialValue="Select"></asp:RequiredFieldValidator>
                    </p></div>   
 
    <div class="col-md-2"><label>
                            City Name<sup style="color:Red">*</sup></label> </div>
    
    <div class="col-md-2"> <p>
                        
                        <span>
                            <asp:TextBox ID="txtCityName" CssClass="form-control" runat="server"></asp:TextBox>&nbsp;</span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Please fill city name "
                         ControlToValidate="txtCityName"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="rgcityName" runat="server" ErrorMessage="Please enter only character"
                         ValidationExpression="[a-zA-Z\s]+" ControlToValidate="txtCityName" Display="Dynamic"></asp:RegularExpressionValidator>
                         <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Please enter maximum 50 character"
                         ValidationExpression="[\S\s]{0,50}" ControlToValidate="txtCityName" Display="Dynamic"></asp:RegularExpressionValidator>
                         
                         </p></div>  
                    <div class="col-md-2">
                        <label>Arabic City Name<sup style="color:Red">*</sup></label>
                    </div> 
                    <div class="col-md-2">
                         <span>
                            <asp:TextBox ID="txtARCityName" CssClass="form-control" onkeypress="return CheckArabicCharactersOnly(event);" runat="server"></asp:TextBox>&nbsp;</span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" Display="Dynamic" ErrorMessage="Please fill Arabic city name "
                         ControlToValidate="txtARCityName"></asp:RequiredFieldValidator>
                        <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ErrorMessage="Please enter maximum 50 character"
                         ValidationExpression="[\S\s]{0,50}" ControlToValidate="txtARCityName" Display="Dynamic"></asp:RegularExpressionValidator>
                    </div> 

    <div class="col-md-2"><label>
                            City Code<sup style="color:Red">*</sup></label> </div>
        <div class="col-md-2"><p>
                        
                        <span>
                            <asp:TextBox ID="txtCityCode" CssClass="form-control" runat="server"></asp:TextBox>&nbsp;</span>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ErrorMessage="Please fill city code "
                            ControlToValidate="txtCityCode"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic" ErrorMessage="Please enter maximum three character"
                                    ValidationExpression="^[a-z,A-Z]{2,3}$" ControlToValidate="txtCityCode"></asp:RegularExpressionValidator> </p> </div>
    
        <div class="clearfix"> </div> 

    </div>
        
        
      
      
      <div class="col-md-12"> 
      
      
      <asp:Button ID="btnSave" CssClass="but but_b pull-right"  runat="server" Text=" Save "
                                    OnClick="btnSave_Click" />
      
      </div>  
        
        </div>     
        <script  type="text/javascript">
// Allow Arabic Characters only
function CheckArabicCharactersOnly(e) {
var unicode = e.charCode ? e.charCode : e.keyCode
if (unicode != 8) { //if the key isn't the backspace key (which we should allow)
if (unicode == 32)
return true;
else {
if ((unicode < 0x0600 || unicode > 0x06FF)) //if not a number or arabic
return false; //disable key press
}
}
}
</script>
</asp:Content>
