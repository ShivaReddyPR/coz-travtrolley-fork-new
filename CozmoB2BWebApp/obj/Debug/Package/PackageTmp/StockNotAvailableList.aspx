﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="StockNotAvailableListGUI" Title="Stock Not Available List" Codebehind="StockNotAvailableList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript" src="Scripts/Jquery/jquery.min.js"></script>
<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js" ></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js" ></script>
     
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="JSLib/prototype.js"></script>
<script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
<script type="text/javascript" language="javascript">
function init() {

        //    showReturn();
        var today = new Date();
        // For making dual Calendar use CalendarGroup  for single Month use Calendar     
        cal1 = new YAHOO.widget.Calendar("cal1", "container1");
        //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
        cal1.cfg.setProperty("title", "Select From Date");
        cal1.cfg.setProperty("close", true);
        cal1.selectEvent.subscribe(setDate1);
        cal1.render();

        cal2 = new YAHOO.widget.Calendar("cal2", "container2");
        cal2.cfg.setProperty("title", "Select To Date");
        cal2.selectEvent.subscribe(setDate2);
        cal2.cfg.setProperty("close", true);
        cal2.render();
    }

    function showCalendar1() {
       document.getElementById('container2').style.display = "none";
       document.getElementById('container1').style.display = "block";

    }

    function showCalendar2() {
        document.getElementById('container1').style.display = "none";
        cal1.hide();
        // setting Calender2 min date acoording to calendar1 selected date
        var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
        //var date1=new Date(tempDate.getDate()+1);

        if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            var depDateArray = date1.split('/');

          //  var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

            //cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
            //cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
            cal2.render();
        }
        document.getElementById('container2').style.display = "block";
    }


    function setDate1() {
        var date1 = cal1.getSelectedDates()[0];

        document.getElementById('IShimFrame').style.display = "none";
        this.today = new Date();
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var thisTime = this.today.getHours();
        var thisMinutes = this.today.getMinutes();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());
         
        departureDate = cal1.getSelectedDates()[0];
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";
        //			
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        var time = "";
        var timePM = "";

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }
        if (thisMinutes < 10) {
            thisMinutes = "0" + thisMinutes
        }
        if (thisTime > 11) {
            timePM = ("PM")
        } else {
            timePM = ("AM")
        }
        time = (thisTime + ":" + thisMinutes + timePM + " ");

        document.getElementById("<%=txtFrom.ClientID%>").value = day + "/" + (month) + "/" + date1.getFullYear();
 

        //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
        //cal2.render();

        cal1.hide();

    }
    function setDate2() {
        var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
        if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "First select from date.";
            return false;
        }

        var date2 = cal2.getSelectedDates()[0];

        var depDateArray = date1.split('/');

        // checking if date1 is valid		    
        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = " Invalid To Date";
            return false;
        }
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        // Note: Date()	for javascript take months from 0 to 11
        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
        var difference = returndate.getTime() - depdate.getTime();
        //var thisTime = this.today.getHours();
        //var thisMinutes = this.today.getMinutes();
        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "To Date should be greater than  or equal to date of From (" + date1 + ")";
            return false;
        }

        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        var month = date2.getMonth() + 1;
        var day = date2.getDate();

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }

        document.getElementById("<%=txtTo.ClientID%>").value = day + "/" + month + "/" + date2.getFullYear();


        cal2.hide();
    }
    YAHOO.util.Event.addListener(window, "load", init);
    YAHOO.util.Event.addListener(window, "click", init);
     
    </script>
    
 <div class=" mar-top-10">
<div class="ns-h3"> 
<table>
<tr>
<td align="left" style="width:650px;">Stock Not Available                               </td>
<td align="right" style="width:350px;  padding-right:10px">  
                            <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click">First</asp:LinkButton>
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" />
                            <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" />
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click">Last</asp:LinkButton></td>
</tr>
</table>
</div>
<div id="errMess" class="error_module" style="display:none;"> <div id="errorMessage" style="float:left; color:Red;" class="padding-5 yellow-back width-100 center margin-top-5"> </div></div>
           
              <div id="container1" style="position: absolute;top:140px;left:80px; display: none;">
                                            </div>
                                                 <div class="clear" style="margin-left:30px">
                                            <div id="container2" style="position: absolute;top:140px;left:255px; display: none;">
                                            </div>
                                        </div>
         <div class=" bor-1">
    <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true" >
        <table cellpadding="0" cellspacing="0" class="label">
            <tr>
                <td style="width:125px"></td>
                <td style="width:150px"></td>
                <td style="width:75px"></td>
                <td style="width:150px"></td>
                <td style="width:75px"></td>
                  <td style="width:75px"></td>
                <td style="width:150px"></td>
                <td style="width:75px"></td>
                <td style="width:150px"></td>
            </tr> 
            <tr>
                <td colspan="4"></td>
            </tr> 
            <tr>
                 <td align="right"><asp:Label ID="lblFromDate" Text="From Date:" runat="server" CssClass="label" ></asp:Label></td>
                  <td>          <p class="fleft car-search">
                                <span class="fleft margin-top-3 margin-left-5">
                                <asp:TextBox ID="txtFrom" runat="server" Width="80"></asp:TextBox>
                                 </span>
                                 <a href="javascript:void()" onclick="showCalendar1()"><img src="images/call-cozmo.png" class="margin-top-8 margin-left-5" alt="Pick Date" /></a>
</p>
                                 </td>
                                
                 <td align="right"><asp:Label ID="lblToDate" Text="To Date:" runat="server" CssClass="label" ></asp:Label></td>
                 <td> <p class="fleft car-search">
            <span class="fleft margin-top-3 margin-left-5">
                  <asp:TextBox ID="txtTo" runat="server" Width="80"></asp:TextBox>
            </span>
            <a href="javascript:void()" onclick="showCalendar2()"><img src="images/call-cozmo.png" class="margin-top-8 margin-left-5" alt="Pick Date" /></a>
</p>
            </td> 
                 
                <td align="right"><asp:Label ID="lblAgent" runat="server" Text="Agent"></asp:Label></td>
            <td><asp:DropDownList ID="ddlAgent" runat="server" class="auto-list width98p" Width="120px"></asp:DropDownList> </td>
            <td></td>
             <td><asp:Button runat="server" ID="btnSearch" CssClass="button" Text="Search" OnClick="btnSearch_OnClick"
                         /></td>
            </tr>
        </table>
        
        </asp:Panel>


    </div>
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
    <tr>
    <td>
     <div id="Div1"  runat="server"  class="one" style="width: 1900px; overflow:auto; height: 420px; color: #000000; background-color: #FFFFFF;">
   <asp:HiddenField ID="hdnEnquiryId" runat="server" />
    <asp:DataList ID="dlEnquiry" runat="server" AutoGenerateColumns="false" GridLines="Both"
                BorderWidth="0" CellPadding="0" CellSpacing="0"
                Width="1900px" ForeColor="" ShowHeader="true" DataKeyField="EnquiryId">
                <HeaderStyle BackColor="#333333" Font-Bold="true" ForeColor="White"  HorizontalAlign="Left"/>
      <HeaderTemplate >
      <table class="tblpax">
                                        <tr height="30px">
                                             
                                            <td width="180px" class="heading">
                                                Agent Name
                                            </td>
                                            <td width="240px" class="heading" >
                                               Product Name
                                            </td>
                                            <td width="240px" class="heading" >
                                                Email
                                            </td>
                                            <td width="120px" class="heading" >
                                                Phone
                                            </td>
                                             <td width="240px" class="heading">
                                                Enquiry Msg
                                            </td>
                                             <td width="200px" class="heading">
                                                Departure Date
                                            </td>
                                             <td width="150px" class="heading">
                                                Adults
                                            </td>
                                             <td width="100px" class="heading">
                                               Childs
                                            </td>
                                             <td width="100px" class="heading">
                                                Infants
                                            </td>
                                             <td width="200px" class="heading">
                                                Created On
                                            </td>
                                            <td width="200px" class="heading"></td>
                                          
                                        </tr>
                                    </table>
    </HeaderTemplate>
    <ItemTemplate>
    <table class="tblpax">
                                        <tr height="30px">
                                            <td width="180px" align="left">
                                                <%#Eval("agent_name")%>
                                            </td>
                                            <td width="240px" align="left">
                                                <%#Eval("ProductName")%>
                                            </td>
                                            <td width="240px" align="left">
                                                <%#Eval("Email")%>
                                            </td>
                                            <td width="120px" align="left">
                                                <%#Eval("Phone")%>
                                            </td>
                                             <td width="240px">
                                                <%#Eval("EnquiryMsg") %>
                                            </td>
                                             <td width="200px">
                                                <%#Eval("DepartureDate")%>
                                            </td>
                                            <td width="150px">
                                                <%#Eval("Adult")%>
                                            </td>
                                            <td width="100px">
                                                <%#Eval("Child")%>
                                            </td>
                                               <td width="100px">
                                                <%#Eval("Infant")%>
                                            </td>
                                            <td width="200px">
                                                <%#Eval("CreatedOn")%>
                                            </td>
                                             <td width="200px">
                                                <a href="FixedDepartureDetails.aspx?Id=<%#Eval("ProductId")%>">Continue</a>
                                            </td>
                                        </tr>
                                    </table>
    </ItemTemplate>
     </asp:DataList>
      <asp:Label ID="lblMessage" runat="server"></asp:Label>
     </div>
     </td>
    </tr>
    </table>
 </div>
 </div>
   <iframe id="IShimFrame" style="position:absolute; display:none;" frameborder="0"></iframe>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

