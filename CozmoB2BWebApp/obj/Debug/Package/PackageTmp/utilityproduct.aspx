﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="utilityproduct.aspx.cs" Inherits="CozmoB2BWebApp.utilityproduct" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
    <%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <%-- <style>
    
  .GridPager a, .GridPager span
    {
        display: block;
        height: 15px;
        width: 15px;
        font-weight: bold;
        text-align: center;
        text-decoration: none; margin:4px;
    }
    .GridPager a
    {
        background-color: #f5f5f5;
        color: #969696;
        border: 1px solid #969696;
    }
    .GridPager span
    {
        background-color: #A1DCF2;
        color: #000;
        border: 1px solid #3AC0F2;
    }  
    
    
    
  .label { line-height:26px; font-size:12px!important; font-family:Arial;}  
    
.firstPopupDivOuter
        {
            position: absolute;
            top: 50px;
            left: 100px;
            width: 400px;
            height: 300px;
            display: none;
            background-color:#2BA9D9;
            color:#FFF;
        }
        #firstPopupDivInner
        {
            width: 378px;
            height: 268px;
            background: url(images/thankuwindow.png) no-repeat;
            vertical-align: middle;
            margin: 160px auto;
        }
        
        
        
        
        a.paging_list { background:#fff; border: solid 1px #ccc; padding:4px; color:Green; display:block }
        
   .gvHeader2 { background:#1c498a; font-size:12px!important; font-family:Arial; color:#fff; height:26px;   }
      
   
  .hiddencol
  {
    display: none;
  }

</style>--%>
    
    <div class="body_container">
      
  
        <h3>Agent Data List Master Page</h3>

      
      
       <div class="search_container p-3">
       	<div class=" row">
            <div class="col-md-3">
                 <span>  Agent Name:</span>
                <span class="red_span">*</span>
                <div class="form-group">
              
              <asp:DropDownList ID="ddlAgent" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">Select Agent</asp:ListItem>
                </asp:DropDownList>
                <b class="red_span" style="display: none" id="Agent"></b>
            </div>
               </div>
           
           
               <div class="col-md-3">
                     <span>Code</span>
                   <span class="red_span">*</span>
                   <div class="form-group">
              
            <asp:TextBox ID="txtCode" runat="server" CssClass="form-control" onchange="SearchCode();return false"></asp:TextBox>
                 <span class="red_span" style="display: none" id="Code"></span>
            </div>
                   
                </div>
             <div id="CodeText" class="" style="overflow: auto; padding-top: 20px;text-align: center"> </div>
          </div>	
           <div class="row">

           
            <div class="col-md-3">
               <div>
                Value:
                <span class="red_span">*</span>
            </div>
           
                <asp:TextBox ID="txtValue" runat="server" CssClass="form-control"></asp:TextBox>
                 <span class="red_span" style="display: none" id="Value"></span>
            </div>
            <div class="col-md-3 mb-2">               
				<div>
					Description:
					<span class="red_span">*</span>
				</div> 
                <asp:TextBox ID="txtDescription" runat="server" CssClass="form-control"></asp:TextBox>
                <span class="red_span" style="display: none" id="Description"></span>
            </div>
            <div class="col-md-3  mb-2">           
				<div>
					Sequence:
					<span class="red_span">*</span>
				</div>
            	<asp:TextBox ID="txtSequence" runat="server" CssClass="form-control" onKeyPress="return isNumbers(event)"></asp:TextBox>
                <span class="red_span" style="display: none" id="Sequence"></span>
            </div>
            <div class="col-md-3  mb-2">
               <div>
					Status:
				</div>
           	   <asp:CheckBox ID="chkStatus" runat="server" Checked="true"/>
            </div>            
            <div class="col-md-12  mb-3">
               
				   <div class="float-right w-100 mr-4">
					<asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn but_b button pull-right" OnClick="btnDelete_Click"/>
				 <asp:Button ID="btnClear1" runat="server" Text="Clear" CssClass="btn but_b button pull-right" OnClick="btnClear1_Click" />
				<asp:Button ID="btnAdd" runat="server" Text="Add"  CssClass="btn but_b button  pull-right" OnClick="btnAdd_Click" OnClientClick="return AddSave();"/></div>
            </div>
              
        </div>
          
		</div>  
   <br />
                
       <div class="row mt-3">
       		<div class="col-lg-6">
       			    <div>
    
    <asp:GridView ID="gridData" runat="server" CssClass="table table-bordered b2b-corp-table" AutoGenerateColumns="false" AllowPaging="true" ShowHeaderWhenEmpty="true"
     DataKeyNames="LST_ID" EmptyDataText="No Record Available"  OnSelectedIndexChanged="gridData_SelectedIndexChanged" 
        OnPageIndexChanging="gridData_PageIndexChanging" PageSize="10"> 
            <HeaderStyle CssClass="mydatagrid themecol1" HorizontalAlign="Left"></HeaderStyle>
            <Columns>
                 <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
                
                <asp:BoundField DataField="LST_VALUE" HeaderText="Value" ItemStyle-Width="120" />  
                <asp:BoundField DataField="LST_DESCP" HeaderText="Description" ItemStyle-Width="120" /> 
                <asp:BoundField DataField="LST_SEQ"  HeaderText="Sequence"     ItemStyle-Width="120" />
                <asp:BoundField DataField="LST_STATUS"  HeaderText="Status"     ItemStyle-Width="120" ReadOnly="true" />
                 </Columns> 
            </asp:GridView> 
        <span class="red_span" style="display: none" id="gridMsg"></span>
     </div>
       		</div>       	
       </div>         
      
         <%-- update gridview start --%>
     <div class="row mt-3">
       		<div class="col-lg-6">
    <div>
    <asp:GridView ID="gridparent" runat="server" CssClass="table table-bordered b2b-corp-table" DataKeyNames="LST_ID" OnSelectedIndexChanged="gridparent_SelectedIndexChanged" 
         OnPageIndexChanging="gridparent_PageIndexChanging" AllowPaging="true" PageSize="10">
         <HeaderStyle CssClass="mydatagrid themecol1" HorizontalAlign="Left"></HeaderStyle>
        <Columns>
              <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
             </Columns>
    </asp:GridView>
        </div>
        </div>
         </div>
        <%--update gridview End --%>
   
       
     <asp:Label ID="lblSuccessMsg" runat="server"></asp:Label>
    <asp:HiddenField ID="hidGrid" runat="server" />
    <asp:HiddenField ID="hidparent" runat="server" Value="0" />
        <%--Add grid Start --%>

        <%--Add grid End--%>
   
         </div>
        <div class="col-md-12">
            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn but_b button pull-right" OnClick="btnClear_Click"/>
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn but_b button pull-right" OnClientClick="return Search();" OnClick="btnSearch_Click" />
            <%if(gridData.Rows.Count>0 || (gridparent.Rows.Count>0 )){%>
            <asp:Button ID="btnSave" runat="server" Text="Save"  CssClass="btn but_b button  pull-right" OnClientClick="return MainSave();" OnClick="btnSave_Click" />
             <%} %> 
         </div>
       
       

    
     <script type="text/javascript">
         
         function MainSave() {
             var isValid = true;
             var RowCount = <%=gridData.Rows.Count%>;

             if (document.getElementById('<%=ddlAgent.ClientID%>').value ==-1) {
                document.getElementById('Agent').style.display = 'block';
                document.getElementById('Agent').innerHTML = "Please Select Agent";
                isValid = false;
             }

           if (document.getElementById('<%=txtCode.ClientID%>').value.trim().length <= 0) {
                document.getElementById('Code').style.display = 'block';
                document.getElementById('Code').innerHTML = "Please Enter Code";
                isValid = false;
             }

                 
             if (RowCount < 0) {
                 document.getElementById('gridMsg').style.display = 'block';
                 document.getElementById('gridMsg').innerHTML = 'Please Enter gridview Details';
                 isValid = false;

             }

             
           
             if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function isNumbers(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
                return false;
            }
            return true;
         }
         function AddSave() {
             var isValid = true;

             if (document.getElementById('<%=txtValue.ClientID%>').value.trim().length <= 0) {
                document.getElementById('Value').style.display = 'block';
                document.getElementById('Value').innerHTML = "Please Enter Value";
                isValid = false;
            }
             if (document.getElementById('<%=txtDescription.ClientID%>').value.trim().length <=0) {
                document.getElementById('Description').style.display = 'block';
                document.getElementById('Description').innerHTML = "Please Enter Description";
                isValid = false;

            }
            if (document.getElementById('<%=txtSequence.ClientID%>').value.trim().length <=0) {
                document.getElementById('Sequence').style.display = 'block';
                document.getElementById('Sequence').innerHTML = "Please Enter Sequence Number";
                isValid = false;

             }
             if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
         }

         function SearchCode() {
             var Agentid, code;
             Agentid =$('#<%=ddlAgent.ClientID %>').val();
             code = document.getElementById('<%=txtCode.ClientID %>').value;

             var Ajaxurl ='utilityproduct.aspx/GetCode';
             var Inputdata = "{'code':'" + code + "', 'agentid':'" + Agentid + "'}"

             $.ajax({
                type: "POST",
                url: Ajaxurl,
                contentType: "application/json; charset=utf-8",
                data: Inputdata,
                dataType: "json",
                 async: false,
                success: function (data) {                    
                    //alert(data.d);
                    var message = data.d;
                    if (message.length > 0) {
                        //document.getElementById("CodeText").innerHTML = message;
                        $('#CodeText').html(message);
                    }
                    else {
                        $('#CodeText').html('');
                    }
                 },
                 
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
        });
             
         }

         function Search() {
             var isValid = true;
              if (document.getElementById('<%=ddlAgent.ClientID%>').value ==-1) {
                document.getElementById('Agent').style.display = 'block';
                document.getElementById('Agent').innerHTML = "Please Select Agent";
                isValid = false;
             }

             if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
         }
        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
     <asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="LST_ID" 
    EmptyDataText="No AgencyDataListMaster List!" AutoGenerateColumns="false" PageSize="5" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging" >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
    
     <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc1:Filter   ID="HTtxtAgent" Width="70px" HeaderText="Agent Name" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblAgent" runat="server" Text='<%# Eval("agent_name") %>' CssClass="label grdof" ToolTip='<%# Eval("agent_name") %>' ></asp:Label>
    </ItemTemplate>    
    
    </asp:TemplateField>      
      
      <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtCode"  Width="100px" CssClass="inputEnabled" HeaderText="Code" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblCode" runat="server" Text='<%# Eval("LST_CODE") %>' CssClass="label grdof"  ToolTip='<%# Eval("LST_CODE") %>'></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
     <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtValue"  Width="100px" CssClass="inputEnabled" HeaderText="Value" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblValue" runat="server" Text='<%# Eval("LST_VALUE") %>' CssClass="label grdof"  ToolTip='<%# Eval("LST_VALUE") %>'></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    </Columns>           
    </asp:GridView>
</asp:Content>
