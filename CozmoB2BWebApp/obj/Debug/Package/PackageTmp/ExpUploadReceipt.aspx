﻿<%@ Page Title="Upload Receipt" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpUploadReceipt.aspx.cs" Inherits="CozmoB2BWebApp.ExpUploadReceipt" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="System.Security.AccessControl" %>
<%@ Import Namespace="System.IO" %>

<%--<asp:Content ID="Content2" ContentPlaceHolderID="cphHeader" runat="server">--%>

   
 <%--</asp:Content>--%>
 
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <link href="DropzoneJs_scripts/dropzone.css" rel="stylesheet" />
    <link href="css/toastr.css" rel="stylesheet" type="text/css" />

    <script src="DropzoneJs_scripts/dropzone.js"></script>    
    <script src="scripts/Common/Common.js"></script>    
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="scripts/Common/ExpenseObjects.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>

    <script type="text/javascript">

        var dropZonePIR = null;
        var dropZonePIRObj;
        var files = [];

        function InitDropZone() {

            $(".closepnldocs").on('click', function () {

                $(".pnldocs").hide();
                $(".pnldrop").hide();

                if (document.getElementById('lnkRemove') != null) 
                    document.getElementById('lnkRemove').click();
                
            });

            // Dropzone.autoDiscover = false;
            Dropzone.prototype.defaultOptions.acceptedFiles = ".JPG,.JPEG,.PNG,.PDF,.DOC,.DOCX,.XLS,.XLSX";
            Dropzone.prototype.defaultOptions.maxFiles = "1";
            var filesize = 1024 * 5000;
            Dropzone.maxFilesize = 10;

            // for Receipt files Upload          
            dropZonePIRObj = {

                url: "hn_ExpenseReceiptUploader.ashx",
                maxFiles: 10,
                addRemoveLinks: true,
                previewsContainer: '#tpl-container',
                thumbnailHeight: 120,
                thumbnailWidth: 120,
                init: function () {

                    this.on("addedfile", function (file) {

                        if (this.files.length) {

                            var _i, _len;

                            //Reoving the File when exceed the File Limit.
                            if (file.size > filesize) {

                                this.removeFile(file);

                                if (!files.includes(file.name))
                                    files.push(file.name);

                                var msg = '';

                                for (var i = 0; i < files.length; i++)
                                    msg += files[i] + (i != files.length - 1 ? ',' : '');

                                $('#errorfiles').html("<b>" + msg + "</b> Failed to Upload the files exceeded file Limits.");
                            }

                            //Removing the duplicate file having same size ,name and modification date.
                            for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) // -1 to exclude current file
                            {
                                if (this.files[_i].name === file.name && this.files[_i].size === file.size && this.files[_i].lastModifiedDate.toString() === file.lastModifiedDate.toString()) {
                                    this.removeFile(file);
                                }
                            }
                        }
                    });
                },
                success: function (file, response) {

                },
                error: function (file, response) {
                    file.previewElement.classList.add("dz-error");
                }
            }

            $('.dropzone').each(function () {
                let dropzoneControl = $(this)[0].dropzone;
                if (dropzoneControl) {
                    dropzoneControl.destroy();
                }
            });

            dropZonePIR = new Dropzone('#expense-dropzone', dropZonePIRObj);
            
            $('.dz-message').append('<div class="use-camera bg-primary"  onclick="showCamera()">USE CAMERA<i class="icon-camera pl-2"></i></div><div class="selectfromGallery "><i class="icon icon-image pr-2 "></i>Select from Gallery</div>');
        }

    </script>
    
    <script type="text/javascript">

        /* Global variables */
        var apiHost = '', bearer = '', cntrllerPath = 'api/expTransactions';
        var apiAgentInfo = {},employeeProfiles = {},expReceiptsDetails = {};
        var selectedProfile = 0;
        var selectedReceipts = [], expReportDetails = {}, selectedReports = [];
        var fileUploadPath = '<%=ConfigurationManager.AppSettings["ExpenseReceiptsFilePath"]%>';

        /* Page Load */
        $(document).ready(function () {

            var pathError = '';

            if (IsEmpty(fileUploadPath)) 
                pathError = 'Please configure path to upload files.';

            if (IsEmpty(pathError) && CompFields('<%= Directory.Exists(ConfigurationManager.AppSettings["ExpenseReceiptsFilePath"])%>', 'False'))
                pathError = 'File upload path not found to upload files.';

            if (IsEmpty(pathError) && CompFields('<%= GenericStatic.DirectoryHasPermission(ConfigurationManager.AppSettings["ExpenseReceiptsFilePath"], FileSystemRights.Write)%>', 'False'))
                pathError = 'Access denied to path to upload files.';

            if (!IsEmpty(pathError)) {

                ShowError(pathError);
                $('#divReceiptDetails').hide();
                $('#btnSave').hide();
                $('#btnClear').hide();
                return false;
            }

            /* Initialize drop zone control */
            InitDropZone();

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {

                ShowError('Session expired, please login once again.');
                return;
            }

            /* Getting Expense Receipts,Profiles Data */
            GetExpenseData(apiAgentInfo.LoginUserCorpId, 'SL'); 

            $("#ctl00_upProgress").hide();
        });

        /* To get expense Receipts info and corp profiles */
        function GetExpenseData(ProfileId, mode) {

            if (IsEmpty(ProfileId) || Math.ceil(ProfileId) <= 0) {

                BindExpenseReceipts([]);
                return false;
            }

            var reqData = { AgentInfo: apiAgentInfo, ProfileId: ProfileId, Mode: mode };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getExpenseReceipts';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);                        
        }

         /* To bind expense info and corp profiles */
        function BindScreenData(screenData) {

            employeeProfiles = {};

            if (!IsEmpty(screenData.dtProfiles) && screenData.dtProfiles.length > 0) {

                employeeProfiles = screenData.dtProfiles;
                DdlBind('ddlProfiles', employeeProfiles, 'profileId', 'employeeId-surName name', '-', mt, '--Select Employee--', '');

                if (employeeProfiles.length == 1) {

                    $('#ddlProfiles').attr('disabled', 'disabled');
                    $("#ddlProfiles").select2("val", employeeProfiles[0].profileId);
                    $("#ddlProfiles").val(employeeProfiles[0].profileId);
                    selectedProfile = employeeProfiles[0].profileId;
                }
            }

            if (!IsEmpty(screenData.receipts) && screenData.receipts.length > 0)
                BindExpenseReceipts(screenData.receipts);
           

            if (!IsEmpty(screenData.dtExpenses) && screenData.dtExpenses.length > 0)
                BindExpenseReports(screenData.dtExpenses);
        }

        /* To Bind Expense receips to grid */
        function BindExpenseReceipts(receipts) {
            
            expReceiptsDetails = receipts;

            var gridProperties = GetGridPropsEntity();
            gridProperties.headerColumns = ('Receipt Date|Receipt Name|Ref.No').split('|');
            gridProperties.displayColumns = ('rC_CreatedDate|rC_Name|rC_Id').split('|');
            gridProperties.pKColumnNames = ('rC_Name').split('|');
            gridProperties.dataEntity = receipts;
            gridProperties.divGridId = 'divExpReceiptsGrid';
            gridProperties.headerPaging = false;
            gridProperties.gridSelectedRows = selectedReceipts;
            gridProperties.selectType = 'M';
            gridProperties.recordsperpage = 10;

            EnablePagingGrid(gridProperties);
            $('#divTrips').show();

            receipts.length > 0 ? $('#divReceiptDetails').show() : $('#divReceiptDetails').hide();
        }

         /* To Bind The Expense Reports based on Profile to grid */
        function BindExpenseReports(reports) {   

            expReportDetails = reports;

            var agentDecimal = <%=Settings.LoginInfo.DecimalValue%>;
            var rows = ''; 
            
            for (var i = 0; i < reports.length; i++) {

                var rbtnhtml = '<input type="radio" name="radios" id="rbReport' + reports[i].eD_Id + '" />';
                var tr = '<tr><td>' + rbtnhtml + '</td><td>' + GetDateFormat(reports[i].eD_TransDate, '/', 'ddmmyyyy') + '</td><td>' + reports[i].eT_Desc
                    + '</td><td>' + FixedDecimalRound(reports[i].eD_TotalAmount, agentDecimal) + '</td></tr>';
                rows += tr;
            }

            $("#tblExpReports > tbody").html(rows == '' ? "No expense reporta found to link" : rows);
        }

         /* To clear trips details and hide the div */
        function ClearTrips() {

            RemoveGrid();
            $('#divTrips').hide();
        }

         /* To get agent and login info */
        function GetAgentInfo() {

            try {

                var loginInfo = '<%=Settings.LoginInfo == null%>';

                if (loginInfo == true)
                    return apiAgentInfo;
                                
                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {

            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';

            if (IsEmpty(apiHost))
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";

            return apiHost;
        }

        /* To Save The Expense Receipt Details */
        function saveUploadReceipt() {

            var files = $('.dz-filename');
            var filenames = [];
            var expReceipts = [];         

            var IsValid = CheckReqData('ddlProfiles', 'NM', '1000', 'select', '0');
            IsValid = CheckReqData('txtReceiptName', 'AN', '50', 'Receipt Name', '') && IsValid;
            
            if (files.length < 1) return ShowError('Please upload The Receipts.');
            if (!IsValid) return false;

            for (var i = 0; i < files.length; i++) {

                filenames.push(files[i].innerText);

                expReceipts.push(
                    GetExpReceiptObj(0, 0, $('#ddlProfiles').val(), $("#txtReceiptName").val().trim(),
                        files[i].innerText, fileUploadPath, true, apiAgentInfo.LoginUserId, 'S')
                );
            }

            var reqData = { AgentInfo: apiAgentInfo, expenseReceipts: expReceipts };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/SaveExpenseReceipts';
            WebApiReq(apiUrl, 'POST', reqData, '', receiptResponse, null, null);
        }

        /* To Upload the receipt files Based on response */
        function receiptResponse(apiData) {

            if (apiData != null && apiData != []) {

                var fileNames = {};

                 for (var i = 0; i < apiData.length; i++) {

                    fileNames[apiData[i].rC_Id] = apiData[i].rC_FileName;
                }               
                
                var resp = AjaxCall('ExpUploadReceipt.aspx/saveReceiptFiles', "{'response':'" + JSON.stringify(fileNames) + "', 'spath':'" + fileUploadPath + "'}");

                if (resp == 'Success') {

                    alert('Receipts uploaded successfully.');
                    pageReload();
                } else 
                    ShowError(resp);
            }            
        }

         /* To Show the Expense Receipts based on Profile */
        function showReports() {

            var ProfileId = $('#ddlProfiles').val();

            if (IsEmpty(ProfileId) || Math.ceil(ProfileId) <= 0) {

                ShowError('Please select profile.');
                return false;
            }                

            selectedReceipts = GetSetSelectedRows();

            if (IsEmpty(selectedReceipts) || selectedReceipts.length == 0) {

                ShowError('Please select receipt to link report.');
                return false;
            }                

            GetExpenseData(ProfileId, 'RP');
            $('#modalExpenseReports').modal();
        }

        var actionDone = '';

         /* To prepare and  Linking the Selected Report to Receips */
        function linkorDelete(action) {

            actionDone = action;
            var id = 0;

            if (action != 'D') {
                var selReport = $('input[id*=rbReport]:checked').attr('id');
                id = (!IsEmpty(selReport) ? selReport.replace('rbReport', '') : id);

                if (action == 'U' && IsEmpty(id) || id == 0) {
                    ShowError('Please select report to link.');
                    return false;
                }
            }            

            selectedReceipts = GetSetSelectedRows();

            if (IsEmpty(selectedReceipts) || selectedReceipts.length == 0) {

                ShowError('Please select receipt.');
                return false;
            } 
            var expenseReceipts = [];
            $.each(selectedReceipts, function (key, selrc) {

                expenseReceipts.push(GetExpReceiptObj(0, id, $('#ddlProfiles').val(), selrc, '', '', true, apiAgentInfo.LoginUserId, action));   
                expReceiptsDetails = expReceiptsDetails.filter(x => x.rC_Name !== selrc);
            });   

            var reqData = { AgentInfo: apiAgentInfo, ExpenseReceipts: expenseReceipts };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/SaveExpenseReceipts';
            WebApiReq(apiUrl, 'POST', reqData, '', linkorDeleteSuccess, null, null);
        }

        /* response from link to Receipt and Delete Receipt  */
        function linkorDeleteSuccess(response) {

            if (response.errors != null && response.errors.length > 0) {

                ShowError(response.errors);
                return false;
            }
                
            pageReload();

            alert('Receipt(s) ' + (actionDone == 'D' ? 'deleted successfully' : 'linked to selected report.'))
        }

        /* Reloading the Page */
        function pageReload(){

            window.location.href = 'ExpUploadreceipt.aspx';
        }
       
    </script>
    <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row">
                <div class="col-12 col-xl-10">
                    <h3 class="m-0 title py-3 px-4 float-md-left">Upload Receipt </h3>
                </div>
                <div class="col-12 col-xl-2 d-flex align-items-end justify-content-end mt-2 mt-xl-0">
                    <a id="btnClear" class="btn btn-info mr-2" onclick="pageReload();">Clear <i class="icon icon-play_arrow"></i></a>
                    <a id="btnSave" class="btn btn-primary mr-2" onclick="return saveUploadReceipt()">Save <i class="icon icon-play_arrow"></i></a>
                </div>
            </div>
        </div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="row no-gutters">
                    <div class="col-lg-8">
                        <div class="exp-content-block">
                            <div class="form-row">
                                <div class="form-group col-md-4">
                                    <label>Select Profile</label>
                                    <select id="ddlProfiles" class="form-control" onchange="GetExpenseData(event.val, 'RC')" ></select>
                                </div>                                                           
                                <div class="form-group col-md-4">
                                    <label>Receipt Name</label>
                                    <input ID="txtReceiptName" class="form-control" placeholder="Enter Receipt Name" onKeyPress="return onKeyPressVal(event, 'TS', 50)" />                              
                                </div>                                
                            </div>
                            <div class=" exp-file-upload-wrap flex-wrap flex-md-nowrap">
                                <div class="dropzone" id="expense-dropzone" style="overflow-x:auto;overflow-y:auto"></div>
                                <div id="tpl-container" class="dropzone-previews">
                                </div>
                            </div>
                         <span id="errorfiles" style="color: red" />
                        </div>
                    </div>
                    <div class="col-lg-4">
                        <div class="exp-content-block " id="divReceiptDetails" style="display:none">
                            <h5 class="mb-3">Receipt details</h5>
                            <div class="button-controls text-right">
                                <a id="btnLinkReports" class="btn btn-info mr-2" onclick="showReports();">Link to Report <i class="icon icon-plus"></i></a>
                                <a id="btnDelete" class="btn btn-danger" onclick="linkorDelete('D')">DELETE  <i class="icon icon-delete"></i></a>
                            </div>
                            <div class="table-responsive">
                               
                                
                                  <div id="divExpReceiptsGrid"></div>
                                
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="clear "></div>
    </div>
   <div class="modal fade" id="modalExpenseReports" role="dialog">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header" style="color: white">
                            <span ID="title" Style="color: white; text-align: center; margin-top: 15px">Expense Reports</span>
                            <button type="button" class="close" data-dismiss="modal" style="height: 10px; width: 10px" aria-label="Close">
                                <span aria-hidden="true">X</span>
                            </button>
                        </div>
                        <div class="modal-body" id="modalReports"> 
                                <div id="divExpReportsGrid">
                                    <table class="table" id="tblExpReports">
                                      <thead>  <tr style="font-weight:bold"><th>Select</th><th>Date</th><th>Description</th><th>Amount</th></tr></thead>
                                        <tbody></tbody>
                                    </table>
                                </div>
                        </div>
                        <div class="modal-footer">
                             <a class="btn btn-danger mr-2" data-dismiss="modal" >Close <i class="icon icon-close"></i></a>
                             <a class="btn btn-info mr-2" onclick="return linkorDelete('U')">Link to Receipt <i class="icon icon-plus"></i></a>
                            </div>
                    </div>
                </div>
            </div>

</asp:Content>

 <%--<asp:Content ID="Content3" ContentPlaceHolderID="cphFooter" runat="server">--%>
 
<%-- </asp:Content>--%>
