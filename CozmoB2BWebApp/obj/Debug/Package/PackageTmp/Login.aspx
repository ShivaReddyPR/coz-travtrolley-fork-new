﻿<%@ Page Language="C#" MasterPageFile="~/Transaction.master" AutoEventWireup="true" Inherits="LoginUI" Title="CT B2B" Codebehind="Login.aspx.cs" %>

<%@ MasterType VirtualPath="~/Transaction.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">



    <script language="javascript" type="text/javascript">

        function setFocus() {
            document.getElementById('ctl00_cphTransaction_txtLoginName').focus();
        }
        setTimeout('setFocus()', 1000);

        function ShowPopUp(id) {
            document.getElementById('<%=txtEmailId.ClientID %>').value = "";
            document.getElementById('err').style.display = 'none';
            document.getElementById('forgotPwd').style.display = "block";
            var positions = getRelativePositions(document.getElementById(id));
            //            document.getElementById('forgotPwd').style.left = (positions[0] + 130) + 'px';
            //            document.getElementById('forgotPwd').style.top = (positions[1] - 200) + 'px';
            return false;
        }
        function HidePopUp() {
            document.getElementById('forgotPwd').style.display = "none";
        }
        function getRelativePositions(obj) {
            var curLeft = 0;
            var curTop = 0;
            if (obj.offsetParent) {
                do {
                    curLeft += obj.offsetLeft;
                    curTop += obj.offsetTop;
                } while (obj = obj.offsetParent);

            }
            return [curLeft, curTop];
        }
        function Validate() {

            var isValid = true;
            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
            document.getElementById('err').style.display = 'none';
            if (document.getElementById('<%=txtEmailId.ClientID %>').value.length <= 0) {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Email address";
                isValid = false;
            }
            else if (reg.test(document.getElementById('<%=txtEmailId.ClientID %>').value)) {
                document.getElementById('err').style.display = 'none';
            }
            else {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Correct Email";
                isValid = false;
            }
            if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
        }
            
       
     </script>


 


<script>
    $(document).ready(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
</script>


        <asp:UpdatePanel ID="upnlLogin" runat="server" UpdateMode="conditional">
        <ContentTemplate>
        




<div class="login_container col-md-3 col-xs-12"> 
  
  <h3> Agent Login  </h3> 
  
<div class="martop_xs10">

<div class="login_div"> 


 <asp:TextBox ID="txtLoginName" class="login_text" autocomplete="off" placeholder="User Name" runat="server"></asp:TextBox>
</div> 


 
  </div> 
  

<div>

<div class="password_div"> 

<asp:TextBox ID="txtPassword" class="password_text"  autocomplete="off" placeholder="Password"  runat="server" TextMode="password"></asp:TextBox>



</div> 
 
  </div> 


<div class="martop_10">

<table width="100%" border="0" cellspacing="0" cellpadding="0">

  <tr>
    <td>
    
    <a style="display:none" class="forgot_link fcol_fff" id="forgotpassword" href="#"  onclick="return ShowPopUp(this.id);"> Forgot password?</a></td>
    
    <td><label class="pull-right"> 
    <asp:Button ID="btnLogin" Width="100" class="login_lock btn" runat="server" Text="Login" OnClick="btnLogin_Click" OnClientClick=" Validate()" />
 </label></td>
  </tr>
  
  <tr><td colspan="2"> <asp:Label ID="lblForgotPwd" runat="server" Text="" style="color:Red; font-size:14px;"></asp:Label>
  
  <asp:Label style="color:Red; font-size:14px;" runat="server" ID="lblError" CssClass="lblError"></asp:Label></span>
  
  </td> </tr>

</table>


 </div> 
 
  
  
  
  
  
  </div>
                 



<div  class="modal" id="forgotPwd">
           
			
			
			<div class="modal-content forgot_div">
      <em class="close close_popup" style=" position:absolute;  right:10px; top:10px; cursor:pointer">
					  
						<i><img align="right" alt="Close" onclick="HidePopUp()" src="images/close-itimes.gif"></i>
					</em>

					
					
           
    <div><center> <h4>Forgot password? </h4></center> </div>
                                        
      <div>  
      
      <asp:TextBox ID="txtEmailId" CssClass="form-control margin_top10" placeholder="Enter Your Email Address" runat="server" CausesValidation="True" ></asp:TextBox>
      
      
      
       </div>
            
         <div class="martop_14">
                
               
                
                
                
                <asp:Button ID="btnGetPassword" CssClass="chosencol btn" runat="server" Text="Get Password" OnClientClick="return Validate()"
                            OnClick="btnGetPassword_Click" />
                          
                          
                           <b style="display: none; color:Red" id="err"></b>
                
               
                
                </div>   
           
           
            
           
    </div>


		
					
					         

              
                   
               
           </div>
       

    

        </ContentTemplate>
        </asp:UpdatePanel>


                            
          
   
      

      <link rel="stylesheet" href="css/mosaic.css" type="text/css" media="screen" />
		
		<script type="text/javascript" src="Scripts/mosaic.1.0.1.js"></script>
		
		<script type="text/javascript">

		    jQuery(function($) {
		        $('.cover').mosaic({
		            animation: 'slide', //fade or slide
		            hover_x: '400px'		//Horizontal position on hover
		        });
		    });
		    
		</script>
	
	
	
	<div> 
 
 

<!-- Half Page Image Background Carousel Header -->

<header id="carouselSlider" class="carousel slide hidden-xs"  data-ride="carousel">
    
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#carouselSlider" data-slide-to="0" class="active"></li>
            <li data-target="#carouselSlider" data-slide-to="1"></li> 
            <li data-target="#carouselSlider" data-slide-to="2"></li>  
           
        </ol>

        <!-- Wrapper for Slides -->
        <div class="carousel-inner">
           
            
           

 <div class="item bg bg4 active"> <div class="carousel-caption"></div> </div>
  
  <div class="item bg bg5"><div class="carousel-caption"></div></div>

<div class="item bg bg6"> <div class="carousel-caption"></div></div>
            
                     
        </div>
             
        <!-- Controls -->
    <!--    <a class="left carousel-control" href="#carouselSlider" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#carouselSlider" data-slide="next">
            <span class="icon-next"></span>
        </a>
-->
    </header>
    
<!-- Half Page Image Background Carousel Header ends -->
           
  
  
  
  
  <div class="product_container hidden-xs"> 

<div class="marbot_20"> 

<div class="col-md-2 "> 

<div class="p110"> <div class="p100 p_flight"> </div> </div> 

<div class="p_title">  Flights </div> 

</div>
<div class="col-md-2 "> 

<div class="p110"> <div class="p100 p_visa"> </div> </div> 

<div class="p_title">  Visa </div> 

</div>
<div class="col-md-2 "> 

<div class="p110"> <div class="p100 p_hotel"> </div> </div> 

<div class="p_title">  Hotels </div> 

</div>
<div class="col-md-2 "> 

<div class="p110"> <div class="p100 p_cruise"> </div> </div> 

<div class="p_title">  Cruise </div> 

</div>
<div class="clearfix"> </div> 
</div>

<div class="marbot_20"> 

<div class="col-md-2 "> 

<div class="p110"> <div class="p100 p_activities"> </div> </div> 

<div class="p_title">  Activities </div> 

</div>
<div class="col-md-2 "> 

<div class="p110"> <div class="p100 p_group-tours"> </div> </div> 

<div class="p_title">  Group tours </div> 

</div>
<div class="col-md-2 "> 

<div class="p110"> <div class="p100 p_holidays"> </div> </div> 

<div class="p_title">  Holidays </div> 

</div>
<div class="col-md-2"> 

<div class="p110"> <div class="p100 p_insurance"> </div> </div> 

<div class="p_title">  Insurance  </div> 

</div>
<div class="clearfix"> </div> 
</div>



</div>  


<div class="clearfix"> </div>
</div>	

	
     
     <div class="loginbody"> 
      
      <div class="col-md-12">

<div class="col-md-2"> 

<div class="product_block"> 
<center> 

<div class="api_support"> </div>

<div> <h3> API Support</h3> </div>


<div> Our feature-rich API (Flights and Visa) enables you to seamlessly integrate...</div>


<div> <a href="ContAPISupport.aspx"> Read more</a>   </div> 

</center>

</div>
 </div>


<div class="col-md-2"> 

<div class="product_block"> 
<center> 

<div class="white_label"> </div>

<div> <h3> White Label </h3> </div>




<div> Grow your business with our innovative and robust White Label solutions... </div>


<div> <a href="ContWhiteLabel.aspx"> Read more</a>   </div> 

</center>

</div>

</div>
<div class="col-md-2"> 

<div class="product_block"> 
<center> 

<div class="supplier"> </div>

<div> <h3>Supplier Inventory</h3> 
  </div>

<div>If you are a Tour operators or a Travel Agent having your...   </div>

<div style=" margin-top:-2px;"> <a href="ContSuppInv.aspx"> Read more</a>   </div> 

</center>

</div>

</div>
<div class="col-md-2"> 

<div class="product_block"> 
<center> 

<div class="payment_gateway"> </div>

<div> <h3> Payment Gateway</h3> </div>

<div>
Our White Labeled solutions comes with integrated payment... </div>

<div style=" margin-top:-2px;"> <a href="ContPG.aspx"> Read more</a>   </div> 

</center>

</div>

</div>
<div class="col-md-2"> 

<div class="product_block"> 
<center> 

<div class="extranet"> </div>

<div> <h3> Hotel Extranet</h3> </div>

<div>Our advanced Hotel Extranet tool helps you to load your private inventories... </div>

<div> <a href="ContExtraNet.aspx"> Read more</a>   </div> 

</center>

</div>

</div>
<div class="col-md-2"> 

<div class="product_block"> 
<center> 

<div class="online_packages"> </div>

<div style=" margin-top:-3px;"> <h3>Holiday Packages</h3> </div>

<div>You can create your own Holiday Package through our Holiday...</div>

<div> <a href="ContHolidays.aspx"> Read more</a>   </div> 

</center>

</div>

</div>

<div class="clearfix"> </div>
 </div>


           



 <!-- news -->    
    <div class="col-md-12 hidden-xs">
            
          <div class="col-md-12"> <h3> News </h3>  </div>
            
            
            </div>
    
    <div style=" padding:30px"> 




<div style=" z-index:-1" id="carouselSlider2" class="carousel slide hidden-xs" data-ride="carousel">
  


<table> 

<tr> 
<td valign="top" style=" padding-right:10px;"> <img src="images/newslist.png" />   </td>

<td>  <div style="height:100px;" class="carousel-inner">
           
            


           

  <div style="background:#fff" class="item active">
  
 
  
  
   <div class=""><strong> Airarabia City Check-in</strong><br />
Avoid long queues at the airport by availing the city check-in service spread across Sharjah, Dubai and Ras Al Khaima. Check-in your luggage and collect your boarding pass. 
   
    </div> 
    
    </div>
    
  <div style="background:#fff" class="item"> <div class=""><strong>BOOK AIR ARABIA COACH SERVICE:</strong><br />
Are you in Dubai, Abudhabi or in Al Ain. Is it really difficult for you to reach Sharjah Airport to catch Air Arabia flights. Air Arabia is pleased to inform, Now you can conveniently book Air Arabia's Coach Service from Abudhabi, Al Ain and Dubai to Sharjah Airport. Air Arabia's exclusive coaches are branded for easy recognition, fully air-conditioned and conveniently equipped with baggage trolleys - all that, so you can begin and end your journey in total comfort.  </div> </div>  
   
   
  <div style="background:#fff" class="item"> <div class="">  <strong>OK to board messages:</strong><br />
Passengers traveling with photocopy of visa may require “OK to Board” approval in order to be accepted for travel. This requirement is varying from travelling sectors and airline. Normally travelling from Indian 
sub-continent “OK to Board” is a must.  </div> </div>
   
   
   
<%--     <div style="background:#fff" class="item"> <div class=""> <strong>Air India Express 30Kg Baggage:</strong><br />
Free Baggage allowance in Air India express is increased to 30 Kg to India from Gulf. </div> </div>



     <div style="background:#fff" class="item"> <div class=""><strong>Happy News: </strong><br />
Happy News to all Indian Expats in Gulf. Air India Express has increased the baggage allowance to 30Kg from Gulf to India till 31st December 2015. </div> </div>--%>


            
                     
        </div></td>


</tr>


</table>
            
            
            

        <!-- Wrapper for Slides -->
       
             
        <!-- Controls -->
    <!--    <a class="left carousel-control" href="#carouselSlider" data-slide="prev">
            <span class="icon-prev"></span>
        </a>
        <a class="right carousel-control" href="#carouselSlider" data-slide="next">
            <span class="icon-next"></span>
        </a>
-->
    </div>

</div>
<!-- news ends -->


<div class="bggray pad20 hidden-xs"> 


<div class="boxlogi pull-left">

<table  border="1" cellspacing="0">
  <tr>
    <td width="138"> <img src="images/contactus.jpg" width="128" height="96" alt="#"></td>
    <td valign="top">
    <h3> Contact Us </h3> 
    Landline: +971 6 5074592 <br>
    For any Support Contact:Helpdesk@cozmotravel.com<br>
For Sales	:B2bsales@cozmotravel.com 
    
    
    
    </td>
  </tr>
</table>


 </div>


<div class="boxlogi pull-right">

<table  border="1" cellspacing="0">
  <tr>
    <td width="138"> <img src="images/subscribe.jpg" width="128" height="96" alt="#"></td>
    <td valign="top">
    <h3> Newsletter </h3> 
    
    <div>Get Latest articles in your inbox for free. </div> 
    
    <div class="subscribe"> 
    <input  placeholder="Enter your email id" class="subsinpt" name="" type="text"> 
    
    <input class="subsbut" value="Submit" name="" type="button">
    
    
    </div> 
    
    
    
    </td>
  </tr>
</table>


 </div>


<div class="clearfix"> </div>
</div> 



</div>







<!--body container ends-->

 
 <div  style="display: none"> 

      <table cellpadding="0" cellspacing="0" class="label">
        <tr>
            <td style="width: 150px">
            </td>
            <td style="width: 150px">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
            </td>
            <td>
            </td>
        </tr>
        <tr>
            <td align="right">
                <asp:Label Visible="false" ID="lblCompanyName" runat="server" Text="Company:"></asp:Label>
            </td>
            <td>
                <asp:DropDownList runat="server" Visible="false" ID="ddlCompany" CssClass="inputDdlEnabled"
                    Width="154px">
                </asp:DropDownList>
            </td>
        </tr>
        <tr style="height: 15px">
            <td>
            </td>
        </tr>
        <tr>
            <td>
            </td>
            <td align="right">
            </td>
        </tr>
        <tr>
            <td colspan="3">
                <asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError"></asp:Label>
            </td>
        </tr>
    </table>
  
  
  </div>
  
  
     
           
</asp:Content>
