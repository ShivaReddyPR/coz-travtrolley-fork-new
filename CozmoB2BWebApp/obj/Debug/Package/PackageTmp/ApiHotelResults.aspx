﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ApiHotelResults.aspx.cs" Inherits="CozmoB2BWebApp.ApiHotelResults" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <style>
        .error {
            border: 1px solid #D8000C;
        }    
        .disabled { color:lightgray; pointer-events:none; }
    </style>       
<%--     <link href="App_Themes/Default/BookingStyle.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/cozmovisa-style.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/main-style.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/style.css" type="text/css" rel="stylesheet" />--%>

    <!-- Required styles for Hotel Listing-->
    <link href="build/css/royalslider.css" rel="stylesheet" type="text/css" />
    <link href="build/css/rs-default.css" rel="stylesheet" type="text/css" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />

<%--    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>--%>
    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>
    <style>
        .error {
            border: 1px solid #D8000C;
        }    
        .disabled { color:lightgray; pointer-events:none; }
       .ui-listing-wrapper li.item.isPackage{
           position:relative;
       }
      
    </style>
    <script type="text/javascript">

        var Dataofobject;
        var SearchList;
        var SessionId;
        var request;
        var TotalRecords;
        var FilterList = [];
        var HotelNames = [];
        var ModifyCity = [];
        var NoofPages; var PageIndex = 1; var sort = [];
        var decimalvalue;
        var Modify = false;//To handle slider loading for no result page
        var Rating = 0;
        var AgentType;
        var ActiveSource = 0;
        var isSHOWPFLINK = false;
        var GimmonixSuppliersCount = 0;
        var cityRestrict = '';
        var bearer ='';
        var countryRestrict = '';
//Scroll Binding Data
        function ScorllLoader() {
                if (!$('#TotalCount').is(':empty')) {
                
                    var TotalCount = $('#TotalCount').text().match(/\d/g),
                        totalElements = $('#list-group>li.item').length;
                    TotalCount = TotalCount.join("");
                    if (totalElements === parseInt(TotalCount)) {
                        $('#lazyLoader').hide();
                        if ($('#list-group>li.item').length > 10) {
                            $('#FinishedLoading').show();
                        }
                    }
                }
            }
            function ScrollBindingData() {
                var Results
                if (sort.length != 0) {
                    Results = sort;
                }
                else if (FilterList.length != 0) {
                    Results = FilterList;
                }
                else {
                    Results = SearchList;
                }
                PageIndex++;
                if (PageIndex <= NoofPages) {
                    LoadHotels(Results, PageIndex);                   
                }
            }

            $(window).scroll(function () {
                if (!$('body').hasClass('htl-enable-map-view')) {
                    if ($('#list-group>li.item').length > 9) {                        
                        $('#lazyLoader').show();
                        $('#FinishedLoading').hide();
                    } else {
                        $('#lazyLoader').hide();
                    }
                   
                    if ($(window).scrollTop() + $(window).height() > $(document).height() - 10) {
                        ScrollBindingData()
                    }
                    ScorllLoader()
                }
            });
        $(window).load(function () {
            initAutocomplete();
                $('#list-group-wrap').scroll(function () {
                    if ($('#list-group>li.item').length > 9) {
                        $('#lazyLoader').show();
                        $('#FinishedLoading').hide();
                    } else {
                        $('#lazyLoader').hide();
                    }
                    var scrollHeight = document.getElementById("list-group-wrap").scrollHeight,
                        scrollTop = $('#list-group-wrap').scrollTop(),
                        maxSrolled = scrollHeight - $('#list-group-wrap').innerHeight();
                    if (scrollTop > maxSrolled - 10) {
                        ScrollBindingData()
                    }
                    ScorllLoader()
                });
            });
        

            //page load
        $(document).ready(function () {
                
            NProgress.start();
            isSHOWPFLINK = '<%= liAppconfig != null && liAppconfig.Where(x => x.ProductID == 2 &&( (x.AppKey.ToUpper() == "SHOWPFLINK" && x.AppValue.ToLower() == "true")||(x.AppKey.ToUpper() == "RESTRICTEDCITIES")||(x.AppKey.ToUpper() == "RESTRICTEDCOUNTRIES"))).Count() > 0 %>';
           // isSHOWPFLINK='<%= CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId ==2785 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId ==2786 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId ==2787 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId ==2788 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId ==2826 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId ==2827 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId ==2828 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId ==2931 %>';
            cityRestrict='<%= (liAppconfig != null) ?liAppconfig.Where(x => x.ProductID == 2 && x.AppKey.ToUpper() == "RESTRICTEDCITIES" ).Select(y=>y.AppValue).FirstOrDefault():""%>';
            countryRestrict='<%= (liAppconfig != null) ?liAppconfig.Where(x => x.ProductID == 2 && x.AppKey.ToUpper() == "RESTRICTEDCOUNTRIES" ).Select(y=>y.AppValue).FirstOrDefault():""%>';
            request = JSON.parse(document.getElementById('<%=hdnobj.ClientID %>').value);
            AgentType = JSON.parse(document.getElementById('<%=hdnAgentType.ClientID %>').value);
            Rating = request.MinRating;
            if (Rating > 0) {

                $("#rating-" + Rating).prop("checked", true);
                    $("#rating-" + Rating).prop("disabled", true);
                $('#spanhover').removeClass('star-cb-group withhover');
                    $('#spanhover').addClass('star-cb-group');
                $(".Allrating").hide();
                $('.star-rating :input').attr('disabled', true);
            }
             //AT FIRST TIME AUTHENTICATE THE USER AND GET THE TOKENID
             var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';
                
                if (apiUrl == null || apiUrl == '') {
                    apiUrl = DynamicAPIURL();<%--'<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';--%>
                }   
             var user ={
            grant_type:'password',
            username:'cozmo',
            password:'cozmo'
        };
            $.ajax({
             
            type: "POST",
           url: apiUrl+'/token',
            data:user,
            contentType: "application/x-www-form-urlencoded",
                dataType: "json",
                async:false,
            success: function (data) {
                bearer = JSON.parse(JSON.stringify(data));
                bearer = bearer.access_token;
               // AjaxCall("ApiHotelResults.aspx/UpdateTokenId", JSON.stringify({ bearerToken: bearer }));
                AjaxCall('ApiHotelResults.aspx/UpdateTokenId', "{'bearerToken':'" + bearer + "'}");

            },
            failure: function (response) {
                alert(response.responseText);
            },
            error: function (response) {
                alert(response.responseText);
            }
        });

            LoadModifySearch();            

            /* Check if the hotel results are there in seesion data */
            if (document.getElementById('<%=hdnHotelResults.ClientID %>').value == '') 
                SearchHotels();
            else {

                SearchList = JSON.parse(document.getElementById('<%=hdnHotelResults.ClientID %>').value);
                decimalvalue = JSON.parse(document.getElementById('<%=hdndecimal.ClientID %>').value);
                SessionId = JSON.parse(document.getElementById('<%=hdnSession.ClientID %>').value);
                BindResults();
                NProgress.done();
            }

            $('input[type=radio][name=rating]').change(function () {

                $('#Chkrating').prop("checked", false);
                $(this).prop("checked", true);

                AllSort();
            });

                //autocomplete for HotelName and Modify city
                Hotelautocomplete();
                //$("#txtCity").autocomplete({
                //    source: ModifyCity,
                //    minLength: 3,
                //    open: function (event, ui) {
                //        $("#txtCity").autocomplete("widget").css("width", "243px");
                //    }
                //});
                $('#Chkrating').change(function () {
                    if ($(this).is(":checked")) {
                        $("#rating-5").prop("checked", true);
                        AllSort();
                    }
                    else {
                        $("#rating-0").prop("checked", true);
                        AllSort();
                    }
                });
                $("#btnFilterLocation").click(function () {
                    AllSort();
                });

                if (document.getElementById('<%=hdnCorpInfo.ClientID %>').value != '')
                    BindCorpInfo();

                $('.hotel-pax-dropdown select').change(function () {
                    hotelPaxCount();
                });

                /* Apply UAPI hotel config */
                if (request.Sources.indexOf('UAH') > -1)
                    ApplyUAHConfig();
            });
             function Hotelautocomplete() {//Hotel Names AutoComplete in filters
                      $("#txtHotelNameSearch").autocomplete({
                    source: HotelNames,
                    minLength: 3,
                    open: function (event, ui) {
                        $("#txtHotelNameSearch").autocomplete("widget").css("width", "285px");
                    },
                    select: function (event, ui) {
                        $("#txtHotelNameSearch").val(ui.item.value);
                        AllSort();
                    }
                });
                }
            function ConvertDate(selector) { // To matach with Main search Date Format
                var from = selector.split("/");
                var Date = from[1] + "/" +from[0]  + "/" + from[2];
                return Date;
}
            //modify Search and updating request obj Session  through web Method and 
            //again search for hotels with modified request
            function ModifySearch() {
                Modify = true;
                var StartDate = ConvertDate($('#txtFromDate').val());
                var EndDate = ConvertDate($('#txtToDate').val());
                var Nationality = $('#ModifyNationality').val();
                var ddlTravReason = $('#ddlTravelReasons').val();
                var ddlTraveler = $('#ddlTravelers').val();
                var rooms = $('#roomCount').val();
                var ModifyHotelName = $("#txtHotelName").val();
                var CityName; var CountryName; var Longitude; var Latitude; var Radius; var Supplierid = []; var MinRating; var MaxRating;
                var AirPortCode;
                var MSource = $('#dvCheckBoxListControl').find('input[type="checkbox"]:checked').map(function() {
                 return this.value;
                }).get().join(',');
                MSource = MSource.replace(/^0+/, '').trim();
            if ($("#modifyrating").val() == 0) {
                MinRating = 0;
                MaxRating = 5;
            }
            else {
                    MinRating =$("#modifyrating").val();
                MaxRating =$("#modifyrating").val();
            }

            if (request.Longtitude == 0 && request.Latitude == 0) {

                var City = $('#txtCity').val();
                 AirPortCode = City.split('-')[1];
                City = City.split('-')[0];
                //for checking city having multiple names or not
                if (City.split(',').length > 2) {   
                    CountryName = City.split(',')[City.split(',').length-1];
                }
                else {
                    CountryName = City.split(',')[1];
                }
                CityName = City.split(',')[0];
            }
            else {
                AirPortCode = '';
                    Longitude = $('#modifyLongtitude').val();
                    Latitude = $('#modifyLatitude').val();
                Radius = ($('#Radius').val() * 1000);
                CountryName = request.CountryName;// $("#PointOfInterest").val();
            }
             
                var paxCount = 0; var AdultCount = 0; var ChildCOunt = 0; var MultipleRooms = false; var guestInfo = [];

                if (StartDate == "" || StartDate == null) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Start Date";
                    return false;
                }
                else if (EndDate == "" || EndDate == null) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select End Date";
                    return false;
                }
                else if (!checkRooms()) {
                    return false;
                }
                else if (!hotelSourceValidate()) {
                    return false;
                }
                   else  if (CityName == "" || CityName == null || CityName.length <= "3") {
                    if (request.Longtitude == 0 && request.Latitude == 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Select City Name";
                        return false;
                    }
                    else {
                        if ((Longitude == "" && Latitude == "") || $("#PointOfInterest").val() == "") {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Please Select POI";
                            return false;
                        }
                        else if( $("#Radius").val()<=0){
                             document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select Radius";
                    return false;
                        }
                          else {
                    document.getElementById('errMess').style.display = "none";
                    document.getElementById('errorMessage').innerHTML = "";
                }
                    }
                       
                }
                else if (document.getElementById('<%=hdnCorpInfo.ClientID %>').value != '' && ddlTravReason == '') {

                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Travel Reason";
                    return false;               
                }
                else if (document.getElementById('<%=hdnCorpInfo.ClientID %>').value != '' && ddlTraveler == '') {

                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Traveller";
                    return false;               
                }
                else {
                    document.getElementById('errMess').style.display = "none";
                    document.getElementById('errorMessage').innerHTML = "";
                }
                //Starts Loader
                $('#LocationFilter').val("");
                $('#txtHotelNameSearch').val("");
                $("#rating-0").prop("checked", true);
                $("input[name='rating']:checked").val(0);
                $("#Chkrating").prop("checked", false);
                $('#list-group').children().remove();
                $("#sortbyPrice").select2("val", "Lowest");
                $("#Noresults").hide();
                $('#htlmapView').show();
                FilterList = []; sort = [];
                $(".ui-listing-page").addClass("loading-progress");
                $('#StaticView').children().css('display', 'block');
                NProgress.start();
                PageIndex = 1;
                
                if (rooms > 1) {
                    MultipleRooms = true;
                }
                for (var i = 0; i < rooms; i++) {
                    var noOfAdults = $("#adtRoom-" + (i + 1)).val();
                    paxCount += parseInt(noOfAdults);
                    AdultCount += parseInt(noOfAdults);
                    if ($('#chdRoom-' + (i + 1)).val() > 0) {
                        var noOfChild = $('#chdRoom-' + (i + 1)).val();
                        paxCount += parseInt(noOfChild);
                        ChildCOunt += parseInt(noOfChild);
                        var ChildAge = [];
                        for (var j = 1; j <= noOfChild; j++) {
                            numChild = $("#ChildBlock-" + (i + 1) + "-ChildAge-" + j).val();
                            ChildAge.push(numChild);
                        }

                    }
                    else {
                        var noOfChild = 0;
                        var ChildAge = [];
                    }
                    guestInfo.push({ ChildAge, noOfAdults, noOfChild });
                }
                if (AgentType == 3 || AgentType == 4) {
                    Supplierid = null;
                }
                else {
                        var $chkbox = $('#tblSources tr').find('input[type="checkbox"]');
                        if ($chkbox.length) {
                            for (var i = 1; i < $chkbox.length; i++) {
                                if (document.getElementById('chklistitem' + i).checked == true && $chkbox[i].value == "GIMMONIX") {
                                    if (document.getElementById('chkChdlistitem0').checked == true) {
                                        Supplierid = null;
                                    }
                                    else { 
                                    var $chdchkbox = $('#tblChildSources tr').find('input[type="checkbox"]:checked');
                                    if ($chdchkbox.length) {
                                        for (var k = 0; k < $chdchkbox.length; k++) {
                                            Supplierid.push($chdchkbox[k].value);
                                        }
                                        }
                                        }
                                }
                            }
                        }
                }

            if (request.Longtitude == 0 && request.Latitude == 0) {

                Object.assign(request, {
                    Corptravelreason: ddlTravReason,
                    Corptraveler: ddlTraveler,
                    PassengerNationality: Nationality,
                    StartDate: StartDate,
                    EndDate: EndDate,
                    CityName: CityName,
                    CountryName: CountryName,
                    NoOfRooms: rooms,
                    IsMultiRoom: MultipleRooms,
                    RoomGuest: guestInfo,
                    MaxRating: MaxRating,
                    MinRating:MinRating,
                    HotelName:ModifyHotelName ,
                    SupplierIds: Supplierid,
                    Sources: AgentType != 3 && AgentType != 4 ? MSource.split(',').filter(v=>v!='') : request.Sources,
                    AirPortCityCode: AirPortCode
                });
            }
            else {

                Object.assign(request, {
                    Corptravelreason: ddlTravReason,
                    Corptraveler: ddlTraveler,
                    PassengerNationality: Nationality,
                    StartDate: StartDate,
                        EndDate: EndDate,
                        CountryName: CountryName,
                    Longtitude: Longitude,
                        Latitude: Latitude,
                    Radius:Radius,
                    NoOfRooms: rooms,
                    IsMultiRoom: MultipleRooms,
                    RoomGuest: guestInfo,
                        MaxRating: MaxRating,
                        MinRating:MinRating,
                        HotelName: ModifyHotelName,
                    SupplierIds: Supplierid,
                    Sources:AgentType != 3 && AgentType != 4 ? MSource.split(',') : request.Sources
                });
            }

            Rating = request.MinRating;

            if (Rating > 0) {

                $("#rating-" + Rating).prop("checked", true);
                $("#rating-" + Rating).prop("disabled", true);
                $('#spanhover').removeClass('star-cb-group withhover');
                $('#spanhover').addClass('star-cb-group');
                $(".Allrating").hide();
                $('.star-rating :input').attr('disabled', true);
            }
            else {

                $("#rating-" + Rating).prop("checked", true);
                $("#rating-" + Rating).prop("disabled", false);
                $('#spanhover').addClass('star-cb-group withhover');
                $(".Allrating").show();
                $('.star-rating :input').attr('disabled', false);
            }

            document.getElementById('<%=hdnobj.ClientID %>').value = request;
                AjaxCall("ApiHotelResults.aspx/ModifySearch", "{'data':'" + JSON.stringify(request) + "' }");
            
            if ($('#htlPriceRangeSlider').text().length != 0) {
                $("#htlPriceRangeSlider").slider("destroy");
            }
            $('#list-group').children().remove();
            HotelNames = [];
            SearchHotels();
        }

            //Check for Room(s) Details (i.e Adult,Childs and Ages are empty or not)
             function checkRooms() {
            var submit = true;
            var rooms = document.getElementById('roomCount');
            var room = parseInt(rooms.options[rooms.selectedIndex].value);
            if (room > 0) {
                for (var k = 1; k <= room; k++) {
                    if (submit == false) {
                        break;
                    }
                    if (document.getElementById("adtRoom-" + k).value == 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Enter Room " + k + "Details!!";
                        submit = false;
                    }

                    if (document.getElementById("chdRoom-" + k).value > 0) {
                        for (var j = 1; j <= document.getElementById("chdRoom-" + k).value; j++) {
                            var str = "ChildBlock-" + k + "-ChildAge-" + j;
                            if (document.getElementById(str).value < 0) {
                                document.getElementById('errMess').style.display = "block";
                                document.getElementById('errorMessage').innerHTML = "Please Enter Child-" + (parseInt(j)) + "  Age Details in Room " + (parseInt(k)) + "!!";
                                submit = false;
                                break;
                            }

                        } //for inner for
                    } //end if
                }  //end for
            }
            else {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select atleast 1 room";
                submit = false;
            }

            return submit;
        }
            /* To apply configurations related to UAPI hotel */
            function ApplyUAHConfig() {

                document.getElementById('divNationality').style.display = document.getElementById('chdRoom-1').style.display = 
                    document.getElementById('lblRm1Child').style.display = 'none';
                                
                $('#roomCount').addClass('disabled');
                document.getElementById('spnPetroFacLink').style.display = (isSHOWPFLINK != 'False' && request.Sources.indexOf('UAH') > -1 && ( cityRestrict.toLowerCase().includes(request.CityName.toLowerCase()) || countryRestrict.toUpperCase().includes(request.CountryCode.toUpperCase()))) ?'block' : 'none';
        }

        function LoadModifySearch() {

            var options = '';
            var data = AjaxCall("ApiHotelResults.aspx/LoadModifyData", "");

            $.each(data, function (index, item) {
                options += "<option value='" + index + "'>" + item + "</option>";
            });

            $('#ModifyNationality').append(options);
            $("#ModifyNationality").select2("val", request.PassengerNationality);

            if (request.Latitude == 0 && request.Longtitude == 0) {

                $("#PointOfInterest").hide();
                $("#divRadius").hide();
                LoadModifyCity();
            }
            else {

                $("#txtCity").hide();
                $('#cityIcon').attr('class', 'icon icon-map-marked-alt');
                    $('#modifyLongtitude').val(request.Longtitude);
                $('#modifyLatitude').val(request.Latitude);
                     $("#PointOfInterest").val(request.CityName + "," + request.CountryName);
                $("#Radius").val((request.Radius/1000));
                }
               
                //Assign values to Modify search control
                //Selected Rooms adults and childs
                  PersonInfo = request.RoomGuest;
                var htlAdultCount = 0; var htlChildCount = 0;
                $('#roomCount').val(PersonInfo.length);
                for (var i = 0; i < PersonInfo.length; i++) {
                    htlAdultCount += PersonInfo[i].noOfAdults;
                    htlChildCount += PersonInfo[i].noOfChild;
                    document.getElementById('room-' + (i + 1)).style.display = 'flex';
                    $("#adtRoom-" + (i + 1)).val(PersonInfo[i].noOfAdults);
                    $('#chdRoom-' + (i + 1)).val(PersonInfo[i].noOfChild);
                    for (var j = 1; j <= PersonInfo[i].noOfChild; j++) {
                        document.getElementById('ChildBlock-' + (i + 1)).style.display = 'block';
                        document.getElementById('ChildBlock-' + (i + 1) + '-Child-' + j).style.display = 'block';
                        document.getElementById('ChildBlock-' + (i + 1) + '-ChildAge-' + j).value = PersonInfo[i].childAge[j - 1];
                        document.getElementById('PrevChildCount-' + (i + 1)).value = PersonInfo[i].noOfChild;
                    }
                }
                var HotelTotalRooms = 'Room ' + PersonInfo.length;

                var HotelTotalPax = htlAdultCount;
                HotelTotalPax += ' Adult ';
                if (htlChildCount > 0) {
                    HotelTotalPax += ', ' + htlChildCount + ' Child';
                }
                $('#htlTotalRooms').html(HotelTotalRooms);
                $('#htlTotalPax').html(HotelTotalPax);
                document.getElementById('PrevNoOfRooms').value = PersonInfo.length;
                //Date Controls
                FromDate = new Date(request.StartDate);
                $("#txtFromDate").datepicker(
                    {
                        minDate: 0,
                        numberOfMonths: [1, 2],
                        dateFormat: 'dd/mm/yy',
                        onSelect: function (dateText, inst) {
                            var selectedDate = new Date(ConvertDate(dateText));
                            selectedDate.setDate(selectedDate.getDate() + 1);
                            $("#txtToDate").datepicker("option", "minDate", selectedDate);
                            var fromDate = new Date($("#txtFromDate").datepicker("getDate"));
                            var toDate = new Date($("#txtToDate").datepicker("getDate"));
                            if (fromDate > toDate) {
                                $("#txtToDate").datepicker("setDate", selectedDate);
                                // $("#txtToDate").val(selectedDate);

                            }
                        MinNights();
                        },
                        onchange: function (dateText, inst) {


                        }
                    }
                ).datepicker("setDate", FromDate);
                FromDate.setDate(FromDate.getDate() + 1);
                $("#txtToDate").datepicker(
                    {
                        numberOfMonths: [1, 2],
                        minDate: FromDate,
                         dateFormat: 'dd/mm/yy'
                    }).datepicker("setDate", new Date(request.EndDate));
            MinNights();
            $("#txtHotelName").val(request.HotelName);
                $("#modifyrating").select2("val", request.MinRating);

            if (AgentType == 1 || AgentType == 2) {// For Base and Agent
                $("#divSupplier").show();
                BindSuppliers();
            }
        }

        function BindSuppliers() {

            var items = JSON.parse(AjaxCall("ApiHotelResults.aspx/BindSuppliers", ""));
            ActiveSource = items.Suppliers.length;
            printSuppliers(items.Suppliers);
            if (items.ChildSuppliers!=null && items.ChildSuppliers.length > 0) { 
            GimmonixSuppliersCount = items.ChildSuppliers.length;
            printChildSuppliers(items.ChildSuppliers);
        }
        }

        function printSuppliers(data) {

            var table = $('<table width="100%" id="tblSources" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" border="0" cellspacing="0" cellpadding="0"></table>');
            var counter = 1;

            var allSupp = 0;// IsEmpty(request.SupplierIds) || request.SupplierIds.length == 0 || request.SupplierIds.length == data.length;

            table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({          
                type: 'checkbox', checked: (allSupp || (request.Sources.length) == data.length), name: 'chklistitem', value: "0", id: 'chklistitem0', onclick: 'setCheck("chklistitem0")'
            })).append(
                $('<label>').attr({
                    for: 'chklistitem0'
                }).text("ALL")))
            );

            $(data).each(function () {
                                
                table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({                
                   type: 'checkbox', checked: (allSupp || (request.Sources.indexOf(this.Name)) != -1), name: 'chklistitem', value: this.Name, id: 'chklistitem' + counter,onclick:'setCheck("chklistitem' + counter+'")'
                })).append(
                        $('<label>').attr({
                            for: 'chklistitem' + counter++
                 }).text(this.Name))));
            });               
 
            $('#dvCheckBoxListControl').append(table);
            if ((request.Sources.indexOf(this.Name)) != "GIMMONIX")
            {
                 hideChildSuppliers();
            }
        }

        function printChildSuppliers(data) {
            $('#divSuppliers').show();
            var table = $('<table width="100%" id="tblChildSources" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" border="0" cellspacing="0" cellpadding="0"></table>');
            var counter = 1;

            var allSupp = IsEmpty(request.SupplierIds) || request.SupplierIds.length == 0 || request.SupplierIds.length == data.length;

            table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({
                type: 'checkbox', checked: allSupp, name: 'chkChdlistitem', value: "0", id: 'chkChdlistitem0',onclick:'setCheck("chkChdlistitem0")'
            })).append(
                $('<label>').attr({
                    for: 'chkChdlistitem0'
                }).text("ALL")))
            );

            $(data).each(function () {
                                
                table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({
                
                    type: 'checkbox', checked: (allSupp || request.SupplierIds.indexOf(this.gimmonixsuppid) != -1), name: 'chkChdlistitem', value: this.gimmonixsuppid, id: 'chkChdlistitem' + counter,onclick:'setCheck("chkChdlistitem' + counter+'")'
                    })).append(
                        $('<label>').attr({
                            for: 'chkChdlistitem' + counter++
                 }).text(this.Name))));
            });               
 
            $('#dvChkbChildListControl').append(table);
        }

        function hotelSourceValidate() {

            if (AgentType == 3 || AgentType == 4)
                return true;
            
            if ($('#dvCheckBoxListControl input[type=checkbox]:checked').length == 0) {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select atleast 1 Supplier";
                return false;
            }          
            return true;
        }

        function setCheck(ctrl) {

            var CheckLen = ctrl != 'chklistitem0' ? $('#dvCheckBoxListControl input[type=checkbox]:checked').length : 0;
            var CheckChdLen = ctrl != 'chkChdlistitem0' ? $('#dvChkbChildListControl input[type=checkbox]:checked').length : 0;            
            var selected = document.getElementById(ctrl).checked;

            if (ctrl == 'chklistitem0') {

                for (i = 1; i <= ActiveSource; i++) {

                    document.getElementById('chklistitem' + i).checked = selected;
                }
            }
            else if (ctrl == 'chkChdlistitem0') {
                for (i = 1; i <= GimmonixSuppliersCount; i++) {

                    document.getElementById('chkChdlistitem' + i).checked = selected;
                }
            }
            else {
                if (ctrl.includes('chklistitem'))
                    document.getElementById('chklistitem0').checked = selected && (CheckLen == ActiveSource)
                if (ctrl.includes('chkChdlistitem'))
                    document.getElementById('chkChdlistitem0').checked = selected && (CheckChdLen == GimmonixSuppliersCount)
            }
              hideChildSuppliers();
        }
        function hideChildSuppliers() {

            var isGimmonix = false;
            var $chkbox = $('#tblSources tr').find('input[type="checkbox"]');
            if ($chkbox.length) {
                for (var i = 1; i < $chkbox.length; i++) {
                    if (document.getElementById('chklistitem' + i).checked == true && $chkbox[i].value == "GIMMONIX") {
                        isGimmonix = true;
                    }
                }
            }            
            if (isGimmonix) {
             
                $('#divChildSuppliers').show();
            } else {
                $('#divChildSuppliers').hide();
            }
        }
        function LoadModifyCity() {
            
            //var data = AjaxCall("ApiHotelResults.aspx/LoadCity", "");

           //$.each(data, function (index, item) {
             //    ModifyCity.push(item);            
            //});               

            $('#txtCity').val(request.CityName + "," + request.CountryName);
        }

        function DynamicAPIURL() {

            var url;
               
                var Secure ='<%=Request.IsSecureConnection%>';
                if (Secure == 'True') {
                    url = '<%=Request.Url.Scheme%>'+"://";
            }
            else {
                   //url = '<%=Request.Url.Scheme%>' + "://";
                      url = 'https://';
                }

             url=url+'<%=Request.Url.Host%>' + "/HotelWebApi";

                return url;
            }

            //Search Hotels via API
            function SearchHotels() {
                if(isSHOWPFLINK != 'False' && request.Sources.indexOf('UAH') > -1 && ( cityRestrict.toLowerCase().includes(request.CityName.toLowerCase())|| countryRestrict.toUpperCase().includes(request.CountryCode.toUpperCase())))
                {
                    hideResults();
                    return;
                }
                else {
                    document.getElementById('spnPetroFacLink').style.display = 'none';
                }
                var AgentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
                var UserId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
                var BehalfLocation = JSON.parse(document.getElementById('<%=hdnBehalfLocation.ClientID %>').value);
                var data = { request: request, AgentId: AgentId, UserId: UserId, BehalfLocation: BehalfLocation };
                var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';
                
                if (apiUrl == null || apiUrl == '') {
                    apiUrl = DynamicAPIURL();<%--'<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';--%>
                }
                              //  alert(apiUrl);
                $.ajax({
                    url: apiUrl+'/api/Hotel/GetSearchResult',
                    type: 'POST',
                    ContentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', "Bearer " + bearer);
                    },
                    data: data,
                    success: function (data) {
                        Dataofobject = JSON.parse(data);
                        SearchList = Dataofobject.Result;
                        SessionId = Dataofobject.SessionId;
                        decimalvalue = Dataofobject.decimalValue;
                        if (SearchList.length == 0) {
                            $('#StaticView').children().css('display', 'none');
                             $('#Heading').hide();
                             $("#Noresults").show();
                             $('#htlmapView').hide();
                             $(".ui-listing-page").removeClass("loading-progress");
                        }
                        else {
                            $('#list-group-wrap').show();
                            BindResults();
                        }
                        
                                              
                    },
                    complete: function () {
                        NProgress.done();
                    },
                    error: function (xhr, textStatus, errorThrown) {
                        alert('Error in Operation');
                    }
                });
            }
            function BindResults() {
                  TotalRecords = SearchList.length;        
                if(TotalRecords!=0) {
                    for (var i = 0; i < SearchList.length; i++) {
                        HotelNames.push(SearchList[i].HotelName);
                       
                    }
                    Hotelautocomplete();
                    NoofPages = Math.ceil(TotalRecords / 10);//To display intial number of hotels
                    $('#price-from-range').text(Math.ceil(SearchList[0].TotalPrice - SearchList[0].Price.Discount));  //.toFixed(decimalvalue)
                    var last = SearchList[SearchList.length - 1];
                    $('#price-to-range').text(Math.ceil(last.TotalPrice -last.Price.Discount ) );
                    $('#fromrange').text(Math.ceil(SearchList[0].TotalPrice - SearchList[0].Price.Discount) );
                    $('#torange').text(Math.ceil(last.TotalPrice -last.Price.Discount) ); 

                    $('#fromrangeCurrency').text(SearchList[0].Currency);
                    $('#torangeCurrency').text(SearchList[0].Currency);                    

                    priceSlider();
                    Modify = false;
                    document.getElementById('<%=hdnHotelResults.ClientID %>').value = SearchList;
                    LoadHotels(SearchList, 1);
                    $(".ui-listing-page").removeClass("loading-progress");
                }
            }
            //Slider LOading
            //function LoadSlider() {   
            //}
            $(function () {
                var uiMoreButtonClass = $('.ui-list-btn');
                $('.results-list-wrapper').on('click', '.ui-list-btn', function () {
                    $('.ui-collapsible-panel').hide();
                    $(this).closest('li.item').find('.ui-collapsible-panel').addClass('animated fadeIn').show();
                    $('.ui-list-features li.active').removeClass('active');
                    $(this).closest('li').addClass('active');
                    var elemIndex = $(this).attr('href');
                    $('.hotel-listing-tab a[href="' + elemIndex + '"]').tab('show');
                    $('.royalSlider').royalSlider('updateSliderSize', true);
                    setTimeout(function () {
                        $('.royalSlider').royalSlider('updateSliderSize', true);
                    }, 200);
                })               
                $('.results-list-wrapper').on('click', 'a[data-toggle="pill"].nav-link', function () {
                    $('.ui-list-features li.active').removeClass('active');
                    var elemID = $(this).attr('href');                     
                    $(".ui-list-features a[href='" + elemID + "']").closest('li').addClass('active');
                    $('.royalSlider').royalSlider('updateSliderSize', true);
                    setTimeout(function () {
                        $('.royalSlider').royalSlider('updateSliderSize', true);
                    }, 200);
                })
                //Close Expanded Panel
                $('.results-list-wrapper').on('click', '.ui-close-button', function (e) {              
                    $(this).closest('.ui-collapsible-panel').hide();
                    $('.ui-list-features').find('li.active').removeClass('active')
                })
            })
            function priceSlider() {
                var sliderMin =  $('#fromrange').text();
                var sliderMax = $('#torange').text();
            
                $("#htlPriceRangeSlider").slider({
                    range: true,
                    min: Number(sliderMin),
                    max: Number(sliderMax),
                    values: [sliderMin, sliderMax],
                    slide: function (event, ui) {
                        var fromRange = ui.values[0],
                            toRange = ui.values[1];
                        $('#price-from-range').text(fromRange);
                        $('#price-to-range').text(toRange);
                    },
                    change: function (event, ui) {
                        
                        if (Modify == false) {
                            AllSort();
                        }
                        
                    }

                });
                $('#price-from-range').text(sliderMin);
                $('#price-to-range').text(sliderMax);
            }

            //Binding Hotels
            function LoadHotels(HotelList, page) {
                if (HotelList.length == 0) {
                    $('#TotalCount').text('');
                      $('#h2HotelinCity').text('');
                      $('#Heading').show();
                    $('#TotalCount').text(HotelList.length + ' found');
                    if (request.Longitude != 0 && request.Latitude != 0) {
                            $('#h2HotelinCity').append('Hotels in <strong>' + request.CountryName + '</strong>');
                    } else {
                            $('#h2HotelinCity').append('Hotels in <strong>' + request.CityName + '</strong>');
                    }
                
                    //alert("No Results Found");
                }
                else {
                    var showFrom = ((Math.ceil(page) - 1) * 10);
                    var showTo = Math.ceil(showFrom) + 9;
                    //document.getElementById("Heading").style.display = "block";
                  $('#TotalCount').text('');
                      $('#h2HotelinCity').text('');
                      $('#Heading').show();
                    $('#TotalCount').text(HotelList.length + ' found');
                     if (request.Longitude != 0 && request.Latitude != 0) {
                            $('#h2HotelinCity').append('Hotels in <strong>' + request.CountryName + '</strong>');
                    } else {
                            $('#h2HotelinCity').append('Hotels in <strong>' + request.CityName + '</strong>');
                    }
                    var orgqueueTemplate = $('#templateView').html();
                   
                    // Temp Preferred Hotel- Hard coding Hotel Ids
                    //var hotelArray = ["4021216", "7255217", "6442036", "4510480", "5680274", "5695629", "4476026"];// Test
                    var hotelArray = ["10797889", "4021583", "6637874", "4021242", "4306205", "11285695"];
                    

                    for (var i = Math.ceil(showFrom); i <= Math.ceil(showTo); i++) {
                        if (i < HotelList.length) {
                            var rating;
                            if (HotelList[i].Rating == 1) {
                                rating = 'one-star-hotel';
                            }
                            else if (HotelList[i].Rating == 2) {
                                rating = 'two-star-hotel';
                            }
                            else if (HotelList[i].Rating == 3) {
                                rating = 'three-star-hotel';
                            }
                            else if (HotelList[i].Rating == 4) {
                                rating = 'four-star-hotel';
                            }
                            else if (HotelList[i].Rating == 5) {
                                rating = 'five-star-hotel';
                            }
                            $('#list-group').append('<li id="List' + i + '" class="item">' + orgqueueTemplate + '</li>');
                            $('#h4HotelName').attr('id', 'h4HotelName-' + i);
                            $('#h4HotelName-' + i).text(HotelList[i].HotelName);
                            $('#isPackage').attr('id', 'isPackage-' + i);
                            //if (HotelList[i].SourceName=='EPS') {
                            //    // alert('source is ='+HotelList[i].SourceName);
                            //    $('#List' + i).addClass('isPackage').append('<div class="isPackage-label">Package</div>');
                               
                            //}
                        
                            $('#pAddress').attr('id', 'pAddress-' + i);
                            $('#pAddress-' + i).text(!IsEmpty(HotelList[i].HotelAddress) ? HotelList[i].HotelAddress : '');
                            $('#divrating').attr('id', 'divrating-' + i);
                            $('#divrating-' + i).addClass(rating);
                            $('#spanRating').attr('id', 'spanRating-' + i);
                            $('#spanRating-' + i).text(HotelList[i].Rating);
                            $('#HotelImage').attr('id', 'HotelImage-' + i);
                            if (HotelList[i].HotelPicture != null && HotelList[i].HotelPicture != '') {
                                    $('#HotelImage-' + i).attr("src", HotelList[i].HotelPicture);
                            }
                            else 
                            $('#HotelImage-' + i).attr("src", '/images/HotelNA.jpg');
                            $('#Discount').attr('id', 'Discount-' + i);
                            var discountAmount = parseFloat(HotelList[i].TotalPrice) - parseFloat(HotelList[i].Price.Discount);
                            $('#Discount-' + i).html('<em class="currency">' + HotelList[i].Currency + ' </em>' + ' ' + Math.ceil(discountAmount).toFixed(decimalvalue));
                            $('#TotalPrice').attr('id', 'TotalPrice-' + i);
                            $('#TotalPrice-' + i).html('<del><em class="currency">' + HotelList[i].Currency + ' </em>' + ' ' + Math.ceil(HotelList[i].TotalPrice).toFixed(decimalvalue)) + '</del>';
                            if (discountAmount.toFixed(decimalvalue) == HotelList[i].TotalPrice.toFixed(decimalvalue)) {
                                $('#TotalPrice-' + i).children().hide();
                            }
                            else {
                                $('#TotalPrice-' + i).children().show();
                            }
                             $('#btnSelectRoom').attr('id', 'btnSelectRoom-' + i);
                            $('#btnSelectRoom-' + i).attr("onClick", "SelectRoom('" + HotelList[i].HotelCode + "','" + SessionId + "','" +HotelList[i].BookingSource+"')");
                          // Temp Preferred Hotel- Hard coding Hotel Ids
                            $('#htlimgPromotion').attr('id', 'htlimgPromotion' + i);  
                          //  if (HotelList[i].HotelCode == '4021216' || HotelList[i].HotelCode == '7255217' || HotelList[i].HotelCode == '6442036' || HotelList[i].hotelCode == '4510480' || HotelList[i].HotelCode == '5695629' || HotelList[i].HotelCode == '4476026') {
                            if (hotelArray.includes(HotelList[i].HotelCode)) {  
                                $('#htlimgPromotion' + i).attr("src", "images/special-deal-gift.jpg");
                                $('#htlimgPromotion' + i).css({'width':'22px','height':'22px'});
                                $('#htlimgPromotion' + i).attr("alt", "Preferred Hotel");
                                  $('#htlimgPromotion' + i).attr('title', "Preferred Hotel");
                            }
 // End
                            $('#HotelDetailsview').attr('id', 'HotelDetailsview-' + i);
                            $('#pills-gallery-tab').attr('id', 'pills-gallery-tab-' + i);
                            $('#pills-location-tab').attr('id', 'pills-location-tab-' + i);
                            $('#pills-overview-tab').attr('id', 'pills-overview-tab-' + i);
                            $('#btnimage').attr('id', 'btnimage-' + i);

                            //$('#btnimage-' + i).attr("onClick", "GetHotelDetails('"+HotelList[i].HotelCode+ "','" + SessionId + "')");
                            $('#btnimage-' + i).prop("href", "#pills-gallery-tab-" + i);
                            $('#btnlocation').attr('id', 'btnlocation-' + i);
                            $('#btnlocation-' + i).prop("href", "#pills-location-tab-" + i);
                            $('#btnDes').attr('id', 'btnDes-' + i);
                            $('#btnDes-' + i).prop("href", "#pills-overview-tab-" + i);
                            $('#btnDetaliGallery').attr('id', 'btnDetaliGallery-' + i);
                            $('#btnDetalilocation').attr('id', 'btnDetalilocation-' + i);
                            $('#btnDetaliOverview').attr('id', 'btnDetaliOverview-' + i);
                            $('#btnDetaliGallery-' + i).prop("href", "#pills-gallery-tab-" + i);
                            $('#btnDetalilocation-' + i).prop("href", "#pills-location-tab-" + i);
                            $('#btnDetaliOverview-' + i).prop("href", "#pills-overview-tab-" + i);
                            $('#HotelDes').attr('id', 'HotelDes-' + i);

                            $('#List' + i).find('#Gallery').attr('id', 'Gallery-' + i);
                            $('#List' + i).find('#Amenities').attr('id', 'Amenities-' + i);

                            BindDetails(i);

                            if (HotelList[i].HotelMap != null) {
                                $('#List' + i).find('#map_canvas').attr('id', 'map_canvas-' + i);                                
                                 initialize(HotelList[i].HotelMap, i);
                                //Addded for MapView Listing
                                var mapLatLong =   HotelList[i].HotelMap.split('||');
                                 $('#HotelDes-' + i).closest('li.item')
                                      .attr({
                                          'data-lat': mapLatLong[0],
                                          'data-long': mapLatLong[1],
                                          'data-title': HotelList[i].HotelName,
                                          'data-imageurl':  HotelList[i].HotelPicture,
                                          'data-totalprice': HotelList[i].Currency + " " + Math.ceil(discountAmount).toFixed(decimalvalue),
                                          'data-hotelcode': HotelList[i].HotelCode,
                                          'data-sessionid': SessionId
                                      })
                                //END:MapView Listing
                            }                            

                            if (SearchList[i].BookingSource == '23') {
                                if(SearchList[i].BookingSource == '23')
                                $('#spnPerNight').attr('id', 'spnPerNight-' + i);$('#spnPerNight-' + i).attr('style', '');
                                $('#btnimage-' + i).attr('onclick', 'GetHotelDetails("' + i + '")');
                                $('#btnlocation-' + i).attr('onclick', 'GetHotelDetails("' + i + '")');
                                $('#btnDes-' + i).attr('onclick', 'GetHotelDetails("' + i + '")');
                            }                                

                            
                        }
                    }
                    //LoadSlider();
                }
                $('#StaticView').children().css('display', 'none');
                //Addded for MapView Listing
                if ($('body').hasClass('htl-enable-map-view')) {
                    mainMap();
                }
                if ($('#list-group-wrap.ui-listing-wrapper ul>li.item').length > 9) {
                    $('#lazyLoader').show();
                    $('#FinishedLoading').hide();
                } else {
                    $('#lazyLoader').hide();
                    $('#FinishedLoading').hide();
                }
            }

            /* Bind hotel details like images, description and amenities */
            function BindDetails(id) {

                var images = "";

                if (!IsEmpty(SearchList[id].HotelImages)) {
                                        
                    $('#Gallery-' + id).children().remove();

                    for (var j = 0; j < SearchList[id].HotelImages.length; j++) {

                        images += '<a class="rsImg" data-rsbigimg="build" href="' + SearchList[id].HotelImages[j] + '" data-rsw="700" data-rsh="300"><img width="96" height="72" class="rsTmb" src="' + SearchList[id].HotelImages[j] + '" /></a>';
                    }						
                 
					$('#Gallery-' + id).append('<div class="royalSlider rsUni" />')
					$('#Gallery-' + id+ ' .royalSlider').html(images);					
					var sliderOptions = {
						controlNavigation: 'thumbnails',
						thumbs: {
							orientation: 'vertical',
							paddingBottom: 4,
							appendSpan: true
						},
						transitionType: 'fade',
						autoScaleSlider: true,
						autoScaleSliderWidth: 960,
						autoScaleSliderHeight: 530,
						loop: true,
						arrowsNav: true,
						keyboardNavEnabled: true,
						imageScaleMode: 'fill'
					}		
					$('#Gallery-' + id+ ' .royalSlider').royalSlider(sliderOptions);

                    //LoadSlider();
                }
                          
                if (!IsEmpty(SearchList[id].HotelFacilities)) {

                    for (var j = 0; j < SearchList[id].HotelFacilities.length; j++) {
                            
                        var amenity = SearchList[id].HotelFacilities[j].trim().toLowerCase();
                        amenity = amenity.replace(" ", "-");
                        $('#Amenities-' + id).append(' <li><span class="icon icon-' + amenity + '"></span><span class="text">' + SearchList[id].HotelFacilities[j] + '</span></li>');;
                    }
                }

                if (!IsEmpty(SearchList[id].HotelDescription))
                    $('#HotelDes-' + id).append('<p>' + SearchList[id].HotelDescription + '</p>');        
            }

            /* Get hotel details like images, description and amenities */
            var Dtlslist = [];
            function GetHotelDetails(id, Isimages) {

                try {

                    if ((SearchList[id].BookingSource == '23') && Dtlslist.indexOf(id) < 0) {

                        $("#ctl00_upProgress").show();
                        var userId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
                        var agentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
                        var behalfLocation = JSON.parse(document.getElementById('<%=hdnBehalfLocation.ClientID %>').value);
                        request.IsGetHotelDtls = true;

                        var Inputdata = { request: request, userId: userId, agentId: agentId, behalfLocation: behalfLocation, hotelCode: SearchList[id].HotelCode, sessionId: SessionId };

                        var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';

                        apiUrl = !IsEmpty(apiUrl) ? apiUrl : DynamicAPIURL();

                        var Webresult = WebAPICall(apiUrl + '/api/HotelRooms/GetRoomsResult', Inputdata, 'POST');

                        if (!IsEmpty(Webresult)) {

                            var DtlsRsp = JSON.parse(Webresult);
                            SearchList[id] = !IsEmpty(DtlsRsp.ResultObj) ? DtlsRsp.ResultObj : SearchList[id];
                            BindDetails(id, Isimages);
                            Dtlslist.push(id);
                            
                                initialize(SearchList[id].HotelMap, id);
                        }                                       
                        
                        request.IsGetHotelDtls = false;
                        $("#ctl00_upProgress").hide();
                    }
                }
                catch (exception) {

                    $("#ctl00_upProgress").hide();
                    var ex = exception;
                }
            }

            //when we click on SelectRoom it redirect to ApiRoomDetails page
        function SelectRoom(hotelCode, sessionId, bookingSource) {
            AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'ApiRoomDetails', 'sessionData':'" + JSON.stringify({ hotelCode: hotelCode, sessionId: sessionId, sourceId: bookingSource }) + "', 'action':'set'}");
            AsyncAjaxCall("ApiHotelResults.aspx/UpdateResults", JSON.stringify({ data: SearchList, session: SessionId }));
            window.location.href = "ApiRoomDetails.aspx";
            //window.location.href = "ApiRoomDetails.aspx?hotelCode=" + hotelCode + "&sessionId=" + sessionId + "&sourceId=" + bookingSource;
        }

        //Filters
        function AllFilters() {
            if ($('#txtHotelNameSearch').val() != "" && $("input[name='rating']:checked").val() != 0 && $('#LocationFilter').val() != "") {
                var Location = new RegExp($('#LocationFilter').val(), 'gi');
                FilterList = SearchList.filter(function (item) {
                    return item.HotelName == $('#txtHotelNameSearch').val() && item.Rating == $("input[name='rating']:checked").val() && item.HotelAddress.match(Location) && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                });
            }
            else if ( $("input[name='rating']:checked").val() != 0 && $('#LocationFilter').val() != "") {
                var Location = new RegExp($('#LocationFilter').val(), 'gi');
                FilterList = SearchList.filter(function (item) {
                    return  item.Rating == $("input[name='rating']:checked").val() && item.HotelAddress.match(Location) && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                });
            }
            else if ($('#txtHotelNameSearch').val() != "" && $("input[name='rating']:checked").val() != 0) {
                var Location = new RegExp($('#LocationFilter').val(), 'gi');
                FilterList = SearchList.filter(function (item) {
                    return item.HotelName == $('#txtHotelNameSearch').val() && item.Rating == $("input[name='rating']:checked").val() && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                });
            }
            else if ($('#txtHotelNameSearch').val() != "" && $('#LocationFilter').val() != "") {
                var Location = new RegExp($('#LocationFilter').val(), 'gi');
                FilterList = SearchList.filter(function (item) {
                    return item.HotelName == $('#txtHotelNameSearch').val() && item.HotelAddress.match(Location) && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                });
            }
            else if ($('#txtHotelNameSearch').val() != "") {
                FilterList = SearchList.filter(function (item) {
                    return item.HotelName == $('#txtHotelNameSearch').val() && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice -item.Price.Discount) <= $('#price-to-range').text());
                });
            }
            else if ($('#LocationFilter').val() != "") {
                   
                    var Location = new RegExp($('#LocationFilter').val().toLowerCase(), 'gi');
                    FilterList = SearchList.filter(function (item) {
                    if (!IsEmpty( item.HotelAddress))
                        return item.HotelAddress.toLowerCase().match(Location) && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                    });
                }
                else if ($("input[name='rating']:checked").val() != 0) {
                    FilterList = SearchList.filter(function (item) {
                        return item.Rating == $("input[name='rating']:checked").val() && (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                    });

                }
                else if ($('#price-from-range').text() != "") {
                    FilterList = SearchList.filter(function (item) {
                        return (Math.ceil(item.TotalPrice) >= $('#price-from-range').text() && Math.ceil(item.TotalPrice-item.Price.Discount) <= $('#price-to-range').text());
                    });
                }
                else {
                    FilterList = SearchList;
                }
            }
            //Sort by Price
            function AllSort() {
                sort = [];
                AllFilters();
                if ($('#sortbyPrice').val() == "Highest") {

                    sort = Object.assign([], FilterList);
                    sort = sort.reverse();
                    $('#list-group').children().remove();
                    PageIndex = 1;
                    NoofPages = Math.ceil(sort.length / 10);
                    LoadHotels(sort, 1);
                }
                else {
                    $('#list-group').children().remove();
                    PageIndex = 1;
                    NoofPages = Math.ceil(FilterList.length / 10);
                    LoadHotels(FilterList, 1);
                }
            }
            //clear Filters
            function ClearFilters() {    
                $('#LocationFilter').val("");
                $('#txtHotelNameSearch').val("");
                $("#rating-0").prop("checked", true);
				 $("input[name='rating']:checked").val(0);
                 $("#Chkrating").prop("checked", false);
                $('#list-group').children().remove();
                $("#sortbyPrice").select2("val", "Lowest");
                if (Rating > 0) {
                    $("#rating-" + Rating).prop("checked", true);
                    $("#rating-" + Rating).prop("disabled", true);

                    $(".Allrating").hide();
                    $('.star-rating :input').attr('disabled', true);
                }
                 $('#price-from-range').text(Math.ceil(SearchList[0].TotalPrice - SearchList[0].Price.Discount));  //.toFixed(decimalvalue)
                    var last = SearchList[SearchList.length - 1];
                    $('#price-to-range').text(Math.ceil(last.TotalPrice - last.Price.Discount));
                    $('#fromrange').text(Math.ceil(SearchList[0].TotalPrice - SearchList[0].Price.Discount));
                    $('#torange').text(Math.ceil(last.TotalPrice - last.Price.Discount));

                    $('#fromrangeCurrency').text(SearchList[0].Currency);
                    $('#torangeCurrency').text(SearchList[0].Currency);
                AllSort();
                // $("#htlPriceRangeSlider").slider("destroy");
                if ($('#htlPriceRangeSlider').text().length != 0) {
                    $("#htlPriceRangeSlider").slider("destroy");
                }
                priceSlider();
            }
            //map
            function initialize(Map, i) {

                try {
                    var data = Map.split('||');
                    myCenter = new google.maps.LatLng(data[0], data[1]);
                    var mapProp = {
                        center: myCenter,
                        zoom: 18,
                        panControl: true,
                        zoomControl: true,
                        mapTypeControl: true,
                        scaleControl: true,
                        streetViewControl: true,
                        overviewMapControl: true,
                        rotateControl: true,
                        mapTypeId: google.maps.MapTypeId.ROADMAP
                    };
                    var map = new google.maps.Map(document.getElementById("map_canvas-" + i), mapProp);


                    marker = new google.maps.Marker({
                        position: myCenter,
                        animation: google.maps.Animation.BOUNCE
                    });

                    marker.setMap(map);
                }                
                catch (exception) {
                }
            }
           

            $(function () {

                $('#normalSearchFields').on('click', function () {
                    var htlHddDiv = $('#advancedSearchFields');
                    if (htlHddDiv.css('display') == 'none') {
                        htlHddDiv.fadeIn();
                    } 

                    if ($('#divCorpFields').css('display') == 'none' && document.getElementById('<%=hdnCorpInfo.ClientID %>').value != '') {
                        $('#divCorpFields').fadeIn();
                    }                     
                })
            })
            //POINT OF INTEREST
            //PointOfInterst
        var autocomplete;
        var geolocation;
  function initAutocomplete() {
  // Create the autocomplete object, restricting the search predictions to
  // geographical location types.
      try {

                autocomplete = new google.maps.places.Autocomplete(
                                /** @type {HTMLInputElement} */(document.getElementById('PointOfInterest')),
                    { types: ['geocode'] });
                google.maps.event.addListener(autocomplete, 'place_changed', function () {
                    var place = autocomplete.getPlace();
                    $('#modifyLongtitude').val(place.geometry.location.lng());
                    $('#modifyLatitude').val(place.geometry.location.lat());
                    var sCntryInfo = GetCountry(place).split('|');
                    request.CountryName = sCntryInfo[0];
                    request.CountryCode = sCntryInfo[1];
                    request.CityName = GetCityNamePOI(place).split('|')[0];
                    /* Show/Hide petrofac link */
                    document.getElementById('spnPetroFacLink').style.display = (isSHOWPFLINK != 'False' && request.Sources.indexOf('UAH') > -1 && (cityRestrict.toLowerCase().includes(request.CityName.toLowerCase())|| countryRestrict.toUpperCase().includes(request.CountryCode.toUpperCase()))) ? 'block' : 'none';                    
                    if (document.getElementById('spnPetroFacLink').style.display == 'block') {
                        hideResults();
                        $('#list-group-wrap').hide();
                    }
                });
            }
            catch (exception) {
            }
        }

        function geolocate() {
  if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function(position) {
     geolocation = {
        lat: position.coords.latitude,
        lng: position.coords.longitude
        };
      var circle = new google.maps.Circle(
          {center: geolocation, radius: position.coords.accuracy});
      autocomplete.setBounds(circle.getBounds());
    });
  }
            }

         /* To hide resultslist when spnPetroFacLink is enabled*/
        function hideResults() {

             document.getElementById('spnPetroFacLink').style.display='block';
             $('#StaticView').children().css('display', 'none');
             $('#Heading').hide();
             $("#Noresults").show();
             $('#htlmapView').hide();
             $(".ui-listing-page").removeClass("loading-progress");
             $('#lazyLoader').hide();
             $('#FinishedLoading').hide();
             NProgress.done();
        }
    </script>
    <script>

        /* To bind corporate ddl fields */
        function BindCorpInfo()
        {
            var CorpInfo = JSON.parse(document.getElementById('<%=hdnCorpInfo.ClientID %>').value);
            var sReason = '<option value="">Select Reason for Travel</option>';

            $.each(CorpInfo.Table1, function (key, col) {

                sReason += '<option value="' + col.ReasonId + '">' + col.Description + '</option>';
            });

            document.getElementById('ddlTravelReasons').innerHTML = sReason;

            var sTraveler = '<option value="">Select Traveller</option>';

            $.each(CorpInfo.Table2, function (key, col) {

                sTraveler += '<option value="' + col.Profile + '">' + col.ProfileName + '</option>';
            });

            document.getElementById('ddlTravelers').innerHTML = sTraveler;

            Setddlval('ddlTravelReasons', request.Corptravelreason, '');
            Setddlval('ddlTravelers', request.Corptraveler, '')
        }
           
            function ShowRoomDetails() {

                //var f=document.getElementById;

                var rooms = document.getElementById('roomCount');
                if (rooms != null) {
                    var count = eval(rooms.options[rooms.selectedIndex].value);
                    if (document.getElementById('PrevNoOfRooms') != null) {
                        var prevCount = eval(document.getElementById('PrevNoOfRooms').value);

                        if (count > prevCount) {
                            for (var i = (prevCount + 1); i <= count; i++) {

                                document.getElementById('room-' + i).style.display = 'flex';
                                document.getElementById('adtRoom-' + i).value = '1';
                                document.getElementById('chdRoom-' + i).value = '0';
                                document.getElementById('PrevChildCount-' + i).value = '0';

                            }

                    }
                    else if (count < prevCount) {

                            for (var i = prevCount; i > count; i--) {
                                document.getElementById('room-' + i).style.display = 'none';
                                document.getElementById('adtRoom-' + i).value = '1';

                                var childcount = document.getElementById('chdRoom-' + i).value
                                document.getElementById('ChildBlock-' + i).style.display = 'none';
                                for (var j = 1; j <= childcount; j++) {
                                    document.getElementById('ChildBlock-' + i + '-ChildAge-' + j).value = '-1';
                                    document.getElementById('ChildBlock-' + i + '-ChildAge-' + j).style.display = 'none';
                                    document.getElementById('ChildBlock-' + i + '-Child-' + j).style.display = 'none';
                                }
                                document.getElementById('chdRoom-' + i).value = '0';
                            }
                        }
                    }
                    document.getElementById('PrevNoOfRooms').value = count;
                }
            }
            /*function to show number of childrens    */



            function ShowChildAge(number) {
                var childCount = eval(document.getElementById('chdRoom-' + number).value);
                var PrevChildCount = eval(document.getElementById('PrevChildCount-' + number).value);
                if (eval(document.getElementById('chdRoom-1').value) > 0 || eval(document.getElementById('chdRoom-2').value) > 0 || eval(document.getElementById('chdRoom-3').value) > 0 || eval(document.getElementById('chdRoom-4').value) > 0) {
                    document.getElementById('childDetails').style.display = 'block';
                }
                else {
                    document.getElementById('childDetails').style.display = 'none';
                }
                if (childCount > PrevChildCount) {
                    document.getElementById('ChildBlock-' + number).style.display = 'block';
                    for (var i = (PrevChildCount + 1); i <= childCount; i++) {
                        document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'block';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).style.display = 'block';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                        $('#ChildBlock-' + number + '-ChildAge-' + i).select2('destroy');
                    }
                }
                else if (childCount < PrevChildCount) {
                    if (childCount == 0) {
                        document.getElementById('ChildBlock-' + number).style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-1').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-2').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-3').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-4').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-5').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-6').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-Child-1').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-2').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-3').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-4').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-5').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-6').style.display = 'none';

                    }







                    else {
                        for (var i = PrevChildCount; i > childCount; i--) {
                            if (i != 0) {
                                document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'none';
                                document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                            }
                        }
                    }
                }
                document.getElementById('PrevChildCount-' + number).value = childCount;
            }
            function childInfo(id) {
                var index = parseInt(id);
                var se = $("selectChild-" + index);
                if (parseInt(se.value) > 0) {


                    var str = "";

                    str += "  <div style='float:left; width:200px;' id='roomChild" + index + "'>";
                    for (var i = 0; i < se.value; i++) {

                        str += "<span style='float:left; width:95px; padding-bottom:3px;'>";
                        str += "  <label style='float:left; width:40px; padding-top:4px; font-size:11px;'> Child " + (parseInt(i) + 1) + "</label>";
                        str += "         <em style='float:left;'>";
                        str += "            <select style='width:50px;font-size:10px;font-family:Arial' class='room_guests' name='child" + index + (parseInt(i) + 1) + "' id='child" + index + (parseInt(i) + 1) + "'>";
                        str += "                    <option selected='selected' value='-1'>Age?</option>";

                        str += "                    <option value='0'><1</option>";
                        str += "                      <option value='1' >1</option>";
                        str += "                      <option value='2'>2</option>";
                        str += "                    <option value='3'>3</option>";
                        str += "                        <option value='4'>4</option>";
                        str += "                        <option value='5'>5</option>";
                        str += "                        <option value='6'>6</option>";
                        str += "                     <option value='7'>7</option> ";
                        str += "                     <option value='8'>8</option>";
                        str += "                      <option value='9'>9</option>"
                        str += "                      <option value='10'>10</option>";
                        str += "                      <option value='11'>11</option>";
                        str += "                   <option value='12'>12</option>";
                        str += "                    <option value='13'>13</option> ";
                        str += "                      <option value='14'>14</option>";
                        str += "                      <option value='15'>15</option>";
                        str += "                     <option value='16'>16</option> ";
                        str += "                    <option value='17'>17</option>";
                        str += "                      <option value='18'>18</option>";

                        str += "               </select>";
                        str += "       </em>";
                        str += "        </span>";
                    }
                    str += "  </div>";

                    $("childInfo-" + index).innerHTML = str;
                    $("childInfo-" + index).style.display = "block";
                }
                else {
                    $("roomChild" + index).style.display = "none";

                }


            }
            function roomInfo(id) {

                if (parseInt(id) == 0) {
                    var se = $('roomCount');
                }
                else {
                    var se = $('norooms');
                }
                if (parseInt(se.value) > 1) {
                    $('guestsInfo').style.display = "none";
                }
                else {
                    $('guestsInfo').style.display = "none";
                    return;

                }
                var str = "";
                for (var i = 2; i <= se.value; i++) {
                    str += "   <div style='float:left; width:220px;' id='noOfPax-" + i + "'>";
                    str += "<b style='float:left; padding:8px 5px 0 0;'>Room " + parseInt(i) + "</b>";
                    str += "      <div class='no_of_guests'>";
                    str += "     <select class='guests'  name='adultCount-" + parseInt(i) + "' id='adultCount-" + parseInt(i) + "'>";
                    str += "               <option selected='selected' value='1' >1</option>";
                    str += "               <option value='2' >2</option>";
                    str += "               <option value='3'>3</option>";
                    str += "               <option value='4'>4</option>";
                    str += "        </select>";
                    str += "       <span>Adults</span>";
                    str += "       <b>(18+ yrs)</b>";
                    str += "   </div>";
                    str += "   <div class='no_of_guests' >";
                    str += "       <select class='guests' id='selectChild-" + parseInt(i) + "' name='childCount-" + parseInt(i) + "' onchange='javascript:childInfo(" + parseInt(i) + ")'>";
                    str += "               <option selected='selected' value='0'>none</option>";
                    str += "               <option value='1'>1</option>";
                    str += "               <option value='2'>2</option>";
                    str += "           </select>";
                    str += "       <span>Children</span>";
                    str += "       <b>(Till 18 yrs)</b>";
                    str += "   </div>";
                    str += "   </div>";
                    str += "<div id='childInfo-" + i + "' style='display:none'></div>"
                }
                $('guestsInfo').innerHTML = str;
                $('guestsInfo').style.display = "block";

        }
        </script>
    <script>
        function getStates_Hotel(sQuery) {

            var paramList = 'searchKey=' + sQuery;   
            paramList += '&requestSource=' + "HotelSearchDomesticForGimmonix";//Added by Harish ;
            var url = "CityAjax";
            var arrayStates = "";
            var faltoo = getFile(url, paramList);
            arrayStates = faltoo.split('/');

            if (arrayStates[0] != "") {

                for (var i = 0; i < arrayStates.length; i++) {

                    {
                        arrayStates[i] = [arrayStates[i], arrayStates[i]];

                    }
                    return arrayStates;
                }
            }
            else return (false);
        }
        //for added helper in source destination lookup
        function autoCompInitHotelCity() {

            // Instantiate third data source        
            oACDSHC = new YAHOO.widget.DS_JSFunction(getStates_Hotel); // temporarily autosearch deactivated
            // Instantiate third auto complete        
            oAutoCompHC = new YAHOO.widget.AutoComplete('txtCity', 'statescontainer3', oACDSHC);
            oAutoCompHC.prehighlightClassName = "yui-ac-prehighlight";
            oAutoCompHC.queryDelay = 0;
            oAutoCompHC.minQueryLength = 3;
            oAutoCompHC.useIFrame = true;
            oAutoCompHC.useShadow = true;

            oAutoCompHC.formatResult = function (oResultItem, sQuery) {
                document.getElementById('statescontainer3').style.display = "block";
                var toShow;
                if (oResultItem[1] != null && oResultItem[1].length > 1) {
                    toShow = oResultItem[1].split('-')[0].split(',');
                }
                else {
                        toShow = oResultItem.split('-')[0].split(',');
                }
                
                var sMarkup;
                     
                sMarkup = toShow.join(',');
            
                var aMarkup = [sMarkup];
                return (aMarkup.join(""));
            };
            oAutoCompHC.itemSelectEvent.subscribe(itemSelectHandlerHC);
        }

        var itemSelectHandlerHC = function (sType2, aArgs2) {

            YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
            var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
            if (oMyAcInstance2[1]!=null && oMyAcInstance2[1].length > 1) {
                var city = oMyAcInstance2[1].split('-')[0].split(',');                              
            }
            else {
                var city = oMyAcInstance2.split('-')[0].split(',');
                }
            document.getElementById('txtCity').value = city.join(',');
            document.getElementById('statescontainer3').style.display = "none";
            var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
            var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource 
        };
        YAHOO.util.Event.addListener(this, 'load', autoCompInitHotelCity); //temporarily commented
    </script>
    <script type="text/javascript">
    
        function hotelPaxCount() {

            var roomCount = $('#PrevNoOfRooms').val();
            var htlAdultCount = 0, htlChildCount = 0;

            $('#hotelRoomSelectDropDown select.adult-pax').each(function (index) {
                count = index + 1;
                if (count > roomCount) {
                    return false;
                }
                htlAdultCount += parseInt($(this).val());
            });

            $('#hotelRoomSelectDropDown select.child-pax').each(function (index) {
                count = index + 1;
                if (count > roomCount) {
                    return false;
                }
                htlChildCount += parseInt($(this).val());
            });

		    var HotelTotalRooms = 'Room ' + roomCount;
				
                var HotelTotalPax = htlAdultCount;
                HotelTotalPax += ' Adult ';
                if (htlChildCount > 0) 
                    HotelTotalPax += ', ' + htlChildCount + ' Child';
               
                $('#htlTotalRooms').html(HotelTotalRooms);
                $('#htlTotalPax').html(HotelTotalPax);
            }
           

        </script>

    <script>
        /* To bind Days Calculation between chekin and checkout Dates */
        function MinNights() {
            
            var fromDate = new Date($("#txtFromDate").datepicker("getDate"));
            var toDate = new Date($("#txtToDate").datepicker("getDate"));
            var checkoutDt = new Date(toDate.getFullYear(), toDate.getMonth(), toDate.getDate());
            var checkInDt = new Date(fromDate.getFullYear(), fromDate.getMonth(), fromDate.getDate());
            var timeDiff = (checkoutDt.getTime() - checkInDt.getTime());
            var MinNights = Math.ceil(timeDiff / (1000 * 3600 * 24));
            document.getElementById('txtMinNights').value = MinNights;
            if (MinNights > 0 && MinNights != "")
            {
                DateChange();
            }
        }

        /* To bind checkout date based on MinNights */
        function DateChange() {
            var mn = document.getElementById('txtMinNights').value;
           
            var date1 = new Date($("#txtFromDate").datepicker("getDate"));
            if (date1 == "" || date1 == null)
            {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Start Date";
                    return false;
            }
            if (mn != "" && mn > 0)
            {
                var arrMinDate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate() + eval(document.getElementById('txtMinNights').value));
                var month = arrMinDate.getMonth() + 1;
                var day = arrMinDate.getDate();

                if (month.toString().length == 1) {
                    month = "0" + month;
                }
                if (day.toString().length == 1) {
                    day = "0" + day;
                }
            document.getElementById('txtToDate').value = day + "/" + month + "/" + arrMinDate.getFullYear();
            }
            else {
                alert("No Of Nights not allowed Zero or empty");
            }
        }

        function allowNumerics(evt) {
             var charCode = (evt.which) ? evt.which : evt.keyCode;
             if (charCode != 46 && charCode > 31
                 && (charCode < 48 || charCode > 57))
                 return false;

             return true;
        }
    </script>


        <input type="hidden" id="hdnobj" runat="server" />
        <input type="hidden" id="hdnUserId" runat="server" />
        <input type="hidden" id="hdnAgentId" runat="server" />
        <input type="hidden" id="hdnBehalfLocation" runat="server" />
        <input type="hidden" id="hdnHotelResults" runat="server" />
       <input type="hidden" id="hdnSession" runat="server" />
        <input type="hidden" id="hdndecimal" runat="server" />
        <input type="hidden" id="hdnGuestInfo" runat="server" />
     <input type="hidden" id="hdnAgentType" runat="server" />
        <input type="hidden" id="hdnCorpInfo" runat="server" />
        <div class="ui-listing-page loading-progress">
            <!--Modify Panel-->
            <div class="ui-modify-wrapper">
                  <div id="errMess" class="error_module" style="display: none;">
                    <div id="errorMessage" class="alert-messages alert-caution">
                    </div>
                </div>

                <div class="ui-hotel-modify-search">



                    <div class="row custom-gutter">
                        <div class="col-md-10">
                            <div class="row custom-gutter">
                                <div class="col-12" id="normalSearchFields">
                                    <div class="row custom-gutter">
                                        <div class="col-md-6 col-lg-6 col-xl-5 mb-2 mb-xl-0">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon icon-location" id="cityIcon"></span>
                                                </div>
                                                <input class="form-control" type="text"  id="txtCity">
                                                <div id="statescontainer3" style="width:95%; line-height:30px; color:#000; position:absolute; display:none; z-index: 103;    top: 38px;" ></div>
                                                  <input type="text" class="form-control" placeholder="Point of Interest" id="PointOfInterest" onFocus="geolocate()" name="location"/>
                                               

                                                 <input type="hidden" id="modifyLongtitude" />
                                                 <input type="hidden" id="modifyLatitude" />

                                                <div class="inputTypeAddkm-wrap ml-2 float-left" id="divRadius" style="width: 120px;">                                              
                                                     <input type="number" class="form-control small inputTypeAddkm" id="Radius"  value="0"/>                                                                           
                                                </div>  

                                            </div>
                                        </div>
                                    
                                <div class="col-md-3 col-lg-3 col-xl-2 mb-2 mb-xl-0">

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="icon icon-calendar"></span>
                                        </div>

                                        <input class="form-control" type="text" id="txtFromDate">
                                    </div>

                                </div>

                                    <div class="col-md-2 col-xl-2 mb-2 mb-md-0 mt-md-0">
                                    <div class="input-group">
                                         <div class="input-group-prepend">
                                            <span class="icon icon-calendar"></span>
                                        </div>
                                         <input type="text" placeholder="No Of Nights" Class="form-control" id="txtMinNights" onkeypress="return allowNumerics(event);" onKeyUp="DateChange();"/>
                                     </div> 
                                    </div>

                                <div class="col-md-3 col-lg-3 col-xl-2 mb-2 mb-xl-0">

                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="icon icon-calendar"></span>
                                        </div>

                                        <input class="form-control" type="text" id="txtToDate" onchange="MinNights();">
                                    </div>

                                </div>
                        
                                    </div>
                                </div>

                                <div class="col-12" id="advancedSearchFields" style="display:none">
                                    <div class="row custom-gutter">
                                <div class="col-md-6 mb-2 col-xl-3 mb-md-0" id="divNationality">
                                    <div class="input-group">
                                        <div class="input-group-prepend">
                                            <span class="icon icon-language"></span>
                                        </div>

                                        <select class="form-control" id="ModifyNationality">
                                        </select>
                                    </div>
                                </div>

                                        <div class="col-md-6 col-xl-3 mb-2 mb-md-0 mt-md-2">
                                            <div class="custom-dropdown-wrapper">
                                    <a href="jaavscript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#hotelRoomSelectDropDown">
                                        <div class="icon-holder">
 	                                         <span class="icon-group"></span>
                                        </div>
                                        <span class="form-control-text">                                        	
											<em id="htlTotalRooms" class="room-label">Room 1</em>
											<em class="pax-label" id="htlTotalPax"> 1 Adult</em>    
                                        </span>           
                                    </a>
                                    <div class="dropdown-content d-none p-4 hotel-pax-dropdown" id="hotelRoomSelectDropDown">     
                                        <div class="row custom-gutter">
                                            <div class="col-4">
                                                ROOM 
                                            </div>
                                            <div class="col-6">
                                                  <input type="hidden" value="1" name="PrevNoOfRooms" id="PrevNoOfRooms" />
                                                  <div class="form-control-holder">
                                                     <select class="form-control small no-select2 htl-room-select" id="roomCount" name="roomCount" onchange="ShowRoomDetails();">
                                                        <option selected="selected">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <%--<option value="5">5</option>
                                                        <option value="6">6</option>--%>
                                                    </select>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row custom-gutter room-wrapper" id="room-1">                                           
                                            <div class="col-12"> <label class="room-label">Room 1</label></div>
                                            <div class="col-4">
                                                 <div class="form-group">  
                                                       <label>Adults</label> 
                                                    <select class="form-control no-select2 adult-pax" name="adtRoom-1" id="adtRoom-1">
                                                                        <option selected="selected" value="1">1</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                    </select>
                                                                    <input type="hidden" value="0" id="PrevChildCount-1" name="PrevChildCount-1" />  
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                 <div class="form-group">                                                     
                                                     <label id="lblRm1Child"> Child(2-12 yrs)</label> 
                                                     <select onchange="javascript:ShowChildAge('1')" name="chdRoom-1" 
                                                            id="chdRoom-1" class="form-control no-select2  child-pax">
                                                            <option selected="selected" value="0">none</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                        </select>
                                                 </div>
                                                
                                            </div>
                                            <div class="col-12" id="ChildBlock-1" name="ChildBlock-1" style="display: none; width: 100%;">
                                                 <div class="row no-gutters child-age-wrapper">                                                     
                                                    <div class="col-4" id="ChildBlock-1-Child-1" name="ChildBlock-1-Child-1" style="display: none;">
                                                        <div class="form-group">                                                     
                                                             <label>Child 1</label> 
                                                             <select class="form-control no-select2" id="ChildBlock-1-ChildAge-1" name="ChildBlock-1-ChildAge-1">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option> 
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                     <div class="col-4" id="ChildBlock-1-Child-2" name="ChildBlock-1-Child-2" style="display: none;">
                                                        <div class="form-group">                                                     
                                                             <label>Child 2</label> 
                                                             <select class="form-control no-select2" id="ChildBlock-1-ChildAge-2" name="ChildBlock-1-ChildAge-2"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-3" name="ChildBlock-1-Child-3" style="display: none;">
                                                        <div class="form-group">
                                                            <label>Child 3</label> 
                                                            <select class="form-control no-select2" id="ChildBlock-1-ChildAge-3" name="ChildBlock-1-ChildAge-3"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-4" name="ChildBlock-1-Child-4" style="display: none;">
                                                        <div class="form-group">
                                                            <label>Child 4</label> 
                                                            <select class="form-control no-select2" id="ChildBlock-1-ChildAge-4" name="ChildBlock-1-ChildAge-4"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>                                                           
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-5" name="ChildBlock-1-Child-5" style="display: none;" >
                                                        <div class="form-group">
                                                            <label>Child 5</label> 
                                                            <select class="form-control no-select2" id="ChildBlock-1-ChildAge-5" name="ChildBlock-1-ChildAge-5"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-6" name="ChildBlock-1-Child-6" style="display: none;">
                                                        <div class="form-group">
                                                            <label>Child 6</label> 
                                                            <select class="form-control no-select2" id="ChildBlock-1-ChildAge-6" name="ChildBlock-1-ChildAge-6"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                     <div class="col-12" id="childDetails" style="display: none;">
                                                         <!--<p>Child Details</p>-->
                                                     </div>



                                                 </div>
                                            </div>


                                        </div>     
                                        
                                        <div class="row custom-gutter room-wrapper" id="room-2" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 2</label></div>
                                            <div class="col-4">
                                                 <div class="form-group">  
                                                       <label>Adults</label> 
                                                         <select name="adtRoom-2" id="adtRoom-2" class="form-control no-select2 adult-pax">
                                                                <option selected="selected" value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                        </select>
                                                       <input type="hidden" value="0" id="PrevChildCount-2" name="PrevChildCount-2" />
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                      <label> Child (2-12 yrs)</label> 
                                                      <select onchange="javascript:ShowChildAge('2')" name="chdRoom-2" id="chdRoom-2" class="form-control no-select2  child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                     </select>     
                                                </div>
                                            </div>
                                            <div class="col-12"  id="ChildBlock-2" name="ChildBlock-2" style="display: none;">
                                              <div class="row no-gutters child-age-wrapper">  
                                                <div class="col-4"  id="ChildBlock-2-Child-1" name="ChildBlock-2-Child-1">
                                                   <div class="form-group">  
                                                    <label>Child 1</label>
                                                    <select class="form-control no-select2" id="ChildBlock-2-ChildAge-1" name="ChildBlock-2-ChildAge-1">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-2" name="ChildBlock-2-Child-2" style="display: none;">
                                                   <div class="form-group">  
                                                    <label>Child 2</label>
                                                    <select class="form-control no-select2" id="ChildBlock-2-ChildAge-2" name="ChildBlock-2-ChildAge-2">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-3" name="ChildBlock-2-Child-3" style="display: none;">
                                                    <div class="form-group">
                                                    <label>Child 3</label>
                                                    <select class="form-control no-select2" id="ChildBlock-2-ChildAge-3" name="ChildBlock-2-ChildAge-3">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-4" name="ChildBlock-2-Child-4" style="display: none;">
                                                    <div class="form-group">
                                                    <label>Child 4</label>
                                                    <select class="form-control no-select2" id="ChildBlock-2-ChildAge-4" name="ChildBlock-2-ChildAge-4">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-5" name="ChildBlock-2-Child-5" style="display: none;">
                                                    <div class="form-group">
                                                    <label>Child 5</label>
                                                    <select class="form-control no-select2" id="ChildBlock-2-ChildAge-5" name="ChildBlock-2-ChildAge-5">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-6" name="ChildBlock-2-Child-6" style="display: none;">
                                                    <div class="form-group">
                                                    <label>Child 6</label>
                                                    <select class="form-control no-select2" id="ChildBlock-2-ChildAge-6" name="ChildBlock-2-ChildAge-6">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                              </div>
                                            </div>
                                        </div>

                                        <div class="row custom-gutter room-wrapper" id="room-3" style="display: none;">  
                                                <div class="col-12"> <label class="room-label">Room 3</label></div>
                                                    <div class="col-4" >
                                                        <div class="form-group">
                                                            <label>Adults</label> 
                                                            <select name="adtRoom-3" id="adtRoom-3" class="form-control no-select2 adult-pax">
                                                                <option selected="selected" value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                             </select>
                                                             <input type="hidden" value="0" id="PrevChildCount-3" name="PrevChildCount-3" />
                                                        </div>
                                                    </div>
                                                    <div class="col-4" >
                                                        <div class="form-group">
                                                            <label> Children (2-12 yrs)</label> 
                                                            <select onchange="javascript:ShowChildAge('3')" name="chdRoom-3" id="chdRoom-3" class="form-control  no-select2  child-pax">
                                                                <option selected="selected" value="0">none</option>
                                                                <option value="1">1</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12"  id="ChildBlock-3" name="ChildBlock-3" style="display: none;">
                                                        <div class="row no-gutters child-age-wrapper">     
                                                                <div class="col-4"  id="ChildBlock-3-Child-1" name="ChildBlock-3-Child-1" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 1</label>
                                                                    <select class="form-control no-select2" id="ChildBlock-3-ChildAge-1" name="ChildBlock-3-ChildAge-1">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-2" name="ChildBlock-3-Child-2" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 2</label>
                                                                    <select class="form-control no-select2" id="ChildBlock-3-ChildAge-2" name="ChildBlock-3-ChildAge-2"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-3" name="ChildBlock-3-Child-3" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 3</label>
                                                                    <select class="form-control no-select2" id="ChildBlock-3-ChildAge-3" name="ChildBlock-3-ChildAge-3"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-4" name="ChildBlock-3-Child-4" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 4</label>
                                                                    <select class="form-control no-select2" id="ChildBlock-3-ChildAge-4" name="ChildBlock-3-ChildAge-4"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-5" name="ChildBlock-3-Child-5" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 5</label>
                                                                    <select class="form-control no-select2" id="ChildBlock-3-ChildAge-5" name="ChildBlock-3-ChildAge-5"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>
                                                                <div class="col-4"  id="ChildBlock-3-Child-6" name="ChildBlock-3-Child-6" style="display: none;">
                                                                   <div class="form-group">  
                                                                    <label>Child 6</label>
                                                                    <select class="form-control no-select2" id="ChildBlock-3-ChildAge-6" name="ChildBlock-3-ChildAge-6"
                                                                        onchange="javascript:ShowChildAge('1')">
                                                                        <option value="-1" selected="selected">Age?</option>
                                                                        <option value="2">2</option>
                                                                        <option value="3">3</option>
                                                                        <option value="4">4</option>
                                                                        <option value="5">5</option>
                                                                        <option value="6">6</option>
                                                                        <option value="7">7</option>
                                                                        <option value="8">8</option>
                                                                        <option value="9">9</option>
                                                                        <option value="10">10</option>
                                                                        <option value="11">11</option>
                                                                        <option value="12">12</option>
                                                                    </select>
                                                                   </div> 
                                                                </div>      
                                                        </div>
                                                    </div>

                                        </div>

                                        <div class="row custom-gutter room-wrapper" id="room-4" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 4</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                          <select name="adtRoom-4" id="adtRoom-4" class="form-control no-select2 adult-pax">
                                                            <option selected="selected" value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                        </select>
                                                        <input type="hidden" value="0" id="PrevChildCount-4" name="PrevChildCount-4" /> 
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>  Child (2-12 yrs)</label> 
                                                    <select onchange="javascript:ShowChildAge('4')" name="chdRoom-4" id="chdRoom-4" class="form-control no-select2 child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                    </select>  

                                                </div>
                                            </div>                                            
                                            <div class="col-12"  id="ChildBlock-4" name="ChildBlock-4" style="display: none;">
                                                <div class="row no-gutters child-age-wrapper">   
                                                    <div class="col-4" id="ChildBlock-4-Child-1" name="ChildBlock-4-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 1</label>
                                                        <select class="form-control no-select2" id="ChildBlock-4-ChildAge-1" name="ChildBlock-4-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-2" name="ChildBlock-4-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 2</label>
                                                        <select class="form-control no-select2" id="ChildBlock-4-ChildAge-2" name="ChildBlock-4-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-3" name="ChildBlock-4-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 3</label>
                                                        <select class="form-control no-select2" id="ChildBlock-4-ChildAge-3" name="ChildBlock-4-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-4" name="ChildBlock-4-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 4</label>
                                                        <select class="form-control no-select2" id="ChildBlock-4-ChildAge-4" name="ChildBlock-4-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-5" name="ChildBlock-4-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 5</label>
                                                        <select class="form-control no-select2" id="ChildBlock-4-ChildAge-5" name="ChildBlock-4-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-6" name="ChildBlock-4-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 6</label>
                                                        <select class="form-control no-select2" id="ChildBlock-4-ChildAge-6" name="ChildBlock-4-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>

                                        <div class="row custom-gutter room-wrapper" id="room-5" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 5</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                       <select name="adtRoom-5" id="adtRoom-5" class="form-control adult-pax">
                                                        <option selected="selected" value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                    </select>
                                                    <input type="hidden" value="0" id="PrevChildCount-5" name="PrevChildCount-5" />    
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label> Child (2-12 yrs)</label> 
                                                     <select onchange="javascript:ShowChildAge('5')" name="chdRoom-5" id="chdRoom-5" class="form-control  child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                    </select>
                                                </div>
                                            </div>                                            
                                            <div class="col-12"  id="ChildBlock-5" name="ChildBlock-5" style="display: none;">                                             
                                                    <div class="row no-gutters child-age-wrapper">
                                                        <div class="col-4" id="ChildBlock-5-Child-1" name="ChildBlock-5-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 1</label>
                                                            <select class="form-control no-select2" id="ChildBlock-5-ChildAge-1" name="ChildBlock-5-ChildAge-1">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-2" name="ChildBlock-5-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 2</label>
                                                            <select class="form-control no-select2" id="ChildBlock-5-ChildAge-2" name="ChildBlock-5-ChildAge-2"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-3" name="ChildBlock-5-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 3</label>
                                                            <select class="form-control no-select2" id="ChildBlock-5-ChildAge-3" name="ChildBlock-5-ChildAge-3"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-4" name="ChildBlock-5-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 4</label>
                                                            <select class="form-control no-select2" id="ChildBlock-5-ChildAge-4" name="ChildBlock-5-ChildAge-4"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-5" name="ChildBlock-5-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 5</label>
                                                            <select class="form-control no-select2" id="ChildBlock-5-ChildAge-5" name="ChildBlock-5-ChildAge-5"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-6" name="ChildBlock-5-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                          <label>Child 6</label>
                                                            <select class="form-control no-select2" id="ChildBlock-5-ChildAge-6" name="ChildBlock-5-ChildAge-6"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                    </div>  
                                            </div>
                                        </div>

                                        <div class="row custom-gutter room-wrapper" id="room-6" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 6</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                     <select name="adtRoom-6" id="adtRoom-6" class="form-control adult-pax">
                                                        <option selected="selected" value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                    <input type="hidden" value="0" id="PrevChildCount-6" name="PrevChildCount-6" />
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label> Child (2-12 yrs)</label> 
                                                          <select onchange="javascript:ShowChildAge('6')" name="chdRoom-6" id="chdRoom-6" class="form-control child-pax">
                                                            <option selected="selected" value="0">none</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                        </select>  
                                                </div>
                                            </div>                                            
                                            <div class="col-12" id="ChildBlock-6" name="ChildBlock-6" style="display: none;">
                                                 <div class="row no-gutters child-age-wrapper">
                                                    <div class="col-4" id="ChildBlock-6-Child-1" name="ChildBlock-6-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 16</label>
                                                        <select class="form-control no-select2" id="ChildBlock-6-ChildAge-1" name="ChildBlock-6-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-2" name="ChildBlock-6-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 26</label>
                                                        <select class="form-control no-select2" id="ChildBlock-6-ChildAge-2" name="ChildBlock-6-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-3" name="ChildBlock-6-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 36</label>
                                                        <select class="form-control no-select2" id="ChildBlock-6-ChildAge-3" name="ChildBlock-6-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-4" name="ChildBlock-6-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 46</label>
                                                        <select class="form-control no-select2" id="ChildBlock-6-ChildAge-4" name="ChildBlock-6-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-5" name="ChildBlock-6-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 56</label>
                                                        <select class="form-control no-select2" id="ChildBlock-6-ChildAge-5" name="ChildBlock-6-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-6" name="ChildBlock-6-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 6</label>
                                                        <select class="form-control no-select2" id="ChildBlock-6-ChildAge-6" name="ChildBlock-6-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>
                                                                                                             
                              
  
                                  
                                                </div>
                                          </div>
                                        </div>

                                         <div class="col-md-4 col-xl-3 mb-2 mb-md-0 mt-md-2">

                                              <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon icon-star"></span>
                                                </div>

                                                <select  id="modifyrating" Class="form-control">
                                                    <option  value="0">Show All</option>
                                                    <option Value="1">1 rating</option>
                                                    <option Value="2">2 rating</option>
                                                    <option Value="3">3 rating</option>
                                                    <option Value="4">4 rating</option>
                                                    <option Value="5">5 rating</option>
                                                </select> 
                                            </div>

                                    
                                         </div>
                                         <div class="col-md-4 col-xl-3 mb-2 mb-md-0 mt-0 mt-md-2" id="divSupplier" style="display:none;">
                                  <div class="custom-dropdown-wrapper">
                                    <a href="jaavscript:void(0);" class=" form-control-element with-custom-dropdown" data-dropdown="#HtlSupplierDropdown">
                                        <div class="form-control-holder mb-0 ">
                                            <div class="icon-holder">                                       
                                                 <span class="icon-global-settings"></span>                                     
                                            </div>   
                                            <span class="form-control-text mt-2" id="htlSelectedSuppliers">Search Suppliers</span>                                  
                                        </div>  
                                    </a>
                                    <div class="dropdown-content d-none p-4" id="HtlSupplierDropdown">                         
                                        <div class="row no-gutters">                                            
                                            <div class="col-md-12">
                                                <strong> Search Suppliers : </strong>
                                            </div>     
                                            <div class="col-md-12" id="dvCheckBoxListControl">                               
<%--                                                <table width="100%" id="tblSources" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" enableviewstate="true" border="0" cellspacing="0" cellpadding="0"></table>  --%>
                                                <a href="javascript:void(0);" class="btn btn-primary btn-sm float-right dd-done-btn">DONE</a>
                                            </div>                                             
                                        </div>
                                    </div>
                              </div>
                                         </div>
                                    </div>
                                <div>
                                <div class="col-12" id="divCorpFields" style="display:none">
                                    <div class="row custom-gutter">
                                        <div class="col-md-6 mb-2 col-xl-3 mb-md-0">
                                            <div class="input-group">
                                                <select class="form-control" id="ddlTravelReasons"></select>
                                            </div>
                                        </div>
                                        <div class="col-md-6 mb-2 col-xl-3 mb-md-0">
                                            <div class="input-group">
                                                <select class="form-control" id="ddlTravelers"></select>
                                            </div>
                                        </div>
                                    </div>                                                                    
                                </div>

                                      <div class="col-md-4 col-xl-3 mb-2 mb-md-0 mt-md-2">
                                             <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon icon-bed"></span>
                                                </div>

                                                <input type="text" placeholder="Search By Hotel Name" Class="form-control" id="txtHotelName" /> 
                                            </div>
                                         </div>
                                <div  id="divSuppliers" style="display:none">

                                     <div class="col-md-4 col-xl-3 mb-2 mb-md-0 mt-0 mt-md-2" id="divChildSuppliers" >
                                  <div class="custom-dropdown-wrapper">
                                    <a href="jaavscript:void(0);" class=" form-control-element with-custom-dropdown" data-dropdown="#HtlChdSupplierDropdown">
                                        <div class="form-control-holder mb-0 ">
                                            <div class="icon-holder">                                       
                                                 <span class="icon-global-settings"></span>                                     
                                            </div>   
                                            <span class="form-control-text mt-2" id="htlChdSelectedSuppliers">Gimmonix Suppliers</span>                                  
                                        </div>  
                                    </a>
                                    <div class="dropdown-content d-none p-4" id="HtlChdSupplierDropdown">                         
                                        <div class="row no-gutters">                                            
                                            <div class="col-md-12">
                                                <strong> Gimmonix Suppliers : </strong>
                                            </div>     
                                            <div class="col-md-12" id="dvChkbChildListControl">                               
                                                <a href="javascript:void(0);" class="btn btn-primary btn-sm float-right dd-done-btn">DONE</a>
                                            </div>                                             
                                        </div>
                                    </div>
                              </div>
                                         </div>


                                </div>
                                </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2 d-flex align-content-center align-items-center justify-content-center">
                            <input type="button" class="btn btn-primary font-weight-bold w-100" style="height: 38px;" value="MODIFY" id="btnModify" onclick="ModifySearch();"/>
                        </div>
                    </div>



                </div>
            </div>
            <div class="results-list-wrapper pt-3">
 <%-- Below code is for Petrofac (Only for UAE City Search) --%>
  <span id="spnPetroFacLink" class="mb-2" style="margin-top: -10px;color: #000;font-size: 20px;margin-left: 300px;font-weight: 600;display:none">
                    Book Petrofac Guesthouses in the UAE
                    <a style="font-size: 14px;    color: #dc1818; text-decoration:underline" target="_blank" href="https://petrofac.cloudbeds.com"> https://petrofac.cloudbeds.com</a>
                </span>
                <div class="row custom-gutter">
<!--
                    <div class="col-12 listing-progress">	
				<h6 class="text-center">Please wait while we search for the best hotels</h6>				
				<div class="progress">							
					<div class="determinate" style="width: 70%"></div>
				 </div>					
			</div>
-->
                    <div class="col-12 col-lg-3 d-none d-lg-block" id="filter-panel">
                        <a href="javascript:void(0);" class="filter-done-mobile close-btn d-block d-lg-none "><span class="icon icon-close"></span></a>
                        <div class="row">
                            <div class="col-md-12 filter-text">
                                <span>FILTER </span><a id="ClearFilter" href="javascript:void(0);" onclick="ClearFilters();" >Clear all</a>
                            </div>
                        </div>
                        <div class="ui-left-panel">

                            <div class="row">

                                <div class="col-md-12 mb-1 left-panel-heading">HOTEL NAME </div>

                                <div class="col-md-12">

                                    <div class="input-group">

                                        <input class="form-control  ui-serch-by-hotel" placeholder="Search by Hotel Name" id="txtHotelNameSearch" onkeydown="return (event.keyCode!=13);">
                                       <%-- <span class="input-group-append">                
			 <button  class="btn btn-outline-primary border-left-0 border" type="button" id="btnFilterHotelName">                 
				<span class="icon icon-search"></span> </button>
							</span>--%>
                                    </div>

                                </div>

                                <div class="col-md-12 left-panel-heading mb-1 mt-4">STAR RATING </div>

                                <div class="col-md-12 filter-category">
									<div class="custom-checkbox-style">
										<input type="checkbox" id="Chkrating" name="rating" value="0" class="Allrating" />
										<label for="rating-0" class="Allrating">All Stars</label>
									</div>
                                    <div class="star-rating">
                                        <fieldset>
                                            <span id="spanhover" class="star-cb-group withhover">
                                                <input type="radio" id="rating-5" name="rating" value="5" /><label for="rating-5" class="lblratingclass-5">5</label>
                                                <input type="radio" id="rating-4" name="rating" value="4" /><label for="rating-4" class="lblratingclass-4">4</label>
                                                <input type="radio" id="rating-3" name="rating" value="3" /><label for="rating-3" class="lblratingclass-3">3</label>
                                                <input type="radio" id="rating-2" name="rating" value="2" /><label for="rating-2" class="lblratingclass-2">2</label>
                                                <input type="radio" id="rating-1" name="rating" value="1" /><label for="rating-1" class="lblratingclass-2">1</label> 
                          <input type="radio" id="rating-0" name="rating" value="0" class="star-cb-clear" checked="checked"/><label for="rating-0">0</label>     												
                                            </span>
                                        </fieldset>
                                    </div>

                                </div>

                                <div class="col-md-12 left-panel-heading mb-1 mt-4">PRICE </div>

                                <div class="col-md-12">
                                    <div class="filter-item price-slider">

                                        <div class="price-slider-text">
                                            <div class="float-left">
                                                <small id="fromrangeCurrency"></small> 
                                                <span class="range" id="price-from-range"></span>
                                                <span style="display:none;" id="fromrange"></span>
                                            </div>
                                            <div class="float-right">

                                                <small id="torangeCurrency"></small>   
                                                <span class="range" id="price-to-range"></span>
                                                 <span style="display:none;" id="torange"></span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="htlPriceRangeSlider" ></div>

                                    </div>

                                </div>

                                <div class="col-md-12 left-panel-heading mb-1 mt-4">LOCATION </div>

                                <div class="col-md-12">
                                     <div class="input-group">
                                <input type="text" id="LocationFilter" class="form-control custom-select" placeholder="Enter Location" onkeydown="return (event.keyCode!=13);"/>
                                     <span class="input-group-append">                
			 <button  class="btn btn-outline-primary border-left-0 border" type="button" id="btnFilterLocation">                 
				<span class="icon icon-search"></span> </button>
							</span>
</div>
                                </div>



                                <div class="col-md-12 filter-category" style="display:none">

                                    <div class="custom-checkbox-style mt-4">
                                        <input type="checkbox" />
                                        <label class="lbl-select-all">HOTEL CHAIN </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input name="foo" type="checkbox" />
                                        <label>Accor - Le Club Accor </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input name="foo" type="checkbox" />
                                        <label>Choice - Choice Privileges </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input name="foo" type="checkbox" />
                                        <label>Hyatt - World of Hyatt </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input name="foo" type="checkbox" />
                                        <label>Jumeirah Hotel and Resorts </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input name="foo" type="checkbox" />
                                        <label>Hilton - Hilton Honors </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input name="foo" type="checkbox" />
                                        <label>Emaar Hospitality Group </label>
                                    </div>

                                </div>


                                <div class="col-md-12 filter-category"  style="display:none">
                                    <div class="custom-checkbox-style mt-4">
                                        <input type="checkbox" />
                                        <label class="lbl-select-all">PROPERTY TYPE </label>
                                    </div>
                                    <div class="custom-checkbox-style">
                                        <input type="checkbox" />
                                        <label>Hotels </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input type="checkbox" />
                                        <label>B&B and Inns </label>
                                    </div>

                                    <div class="custom-checkbox-style">
                                        <input type="checkbox" />
                                        <label>Specialty Lodging </label>
                                    </div>

                                </div>

                            </div>

                        </div>
                        <button type="button" id="filterDoneMobile" class="filter-done-mobile pl-3 d-block d-lg-none btn-block btn-primary btn mt-3 font-weight-bold">
                            APPLY
                        </button>
                    </div>
                    <div class="col-12 col-lg-9" id="hotel-results-panel">
                        <div class="ui-result-header">
                            <div class="float-left mb-3" id="Heading" style="display:none;">
                                <h2 class="float-left" id="h2HotelinCity"></h2>
                                <span class="primary-bgcolor ui-bg-highlighted float-left" id="TotalCount"></span>
                            </div>
                         

                            <div class="float-left float-sm-right mb-3 mb-md-0">

                                <div class="ui-list-control-btn-group mt-2 float-left">
                                    <ul>
                                        <li class="normal-text">SORT BY</li>
                                        <li>
                                            <div class="btn-group" id="sortDropdown">
                                                <select id="sortbyPrice" class="form-control" onchange="AllSort();">
                                                    <option value="Lowest">Lowest Price</option>
                                                    <option value="Highest">Highest Price</option>
                                                </select>
                                            </div>
                                        </li>
                                        <li id="htlstandardView" class="active" title="Detail View">
                                            <a href="javascript:void(0);">
                                                   <span class="icon icon-list-view"></span>                              
                                            </a>
                                        </li>
                                        <li id="htllistView" title="List View">
                                            <a href="javascript:void(0);">
                                              <span class="icon icon-list-view2"></span>
                                            </a>
                                        </li>    
                                        <li id="htlgridView" title="Grid View"><a href="javascript:void(0);">
                                            <span class="icon icon-grid-view"></span>

                                       </a></li>                                    
                                        <li id="htlmapView" title="Map View">
                                            <a href="javascript:void(0);">
                                                <img src="https://travtrolley.com/build/img/map-view.svg" alt="Hotel Listing Map View">
                                            </a>
                                        </li>

                                        <li id="filterBtnWrap">
                                            <a href="javascript:void(0);" id="filterBtnMobile">
                                                <span class="icon icon-filter"></span>
                                            </a>
                                         </li>
                                    </ul>
                                </div>

                            </div>
                        </div>



                        <div class="clearfix"></div>

           	<ul class="ui-listing-wrapper list-unstyled" id="StaticView">
							<li class="item">
								        <div>
            <div class="row no-gutters">
                <div class="col-2 col-sm image-wrapper">
                    <%--<img src="123" alt="Hotel Name" class=" h-100">--%>
                </div>
                <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column text-content-wrapper">
                    <h4 ></h4>
                    <p class="address mb-0" ></p>
                    <div class="mb-auto mt-2">
                        <div ></div>
                        <div class="review-rating float-left mt-2" style="display:none">
                            <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                            <span class="rate primary-bgcolor" ></span>
                        </div>
                    </div>
                    <ul class="ui-list-features list-unstyled">
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" >
                                <i class="icon icon-images"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false">
                                <i class="icon icon-map-marked-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false">
                                <i class="icon icon-more"></i>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator price-wrapper">
                    <div class="ui-list-price text-center">
                        <%--<span class="price strikeout d-inline-block d-sm-block"><del><em class="currency">AED </em>1177</del></span>--%>
                        <span class="price d-inline-block d-sm-block"</span>
                        <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0">SELECT ROOM</button>
                    </div>
                </div>
            </div>
        </div>  
							</li>	
				
															<li class="item">
								        <div >
            <div class="row no-gutters">
                <div class="col-2 col-sm image-wrapper">
                    <img src="build/img/hotel-full-demo.jpg" alt="Hotel Name" class=" h-100">
                </div>
                <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column text-content-wrapper">
                    <h4 ></h4>
                    <p class="address mb-0" ></p>
                    <div class="mb-auto mt-2">
                        <div ></div>
                        <div class="review-rating float-left mt-2" style="display:none">
                            <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                            <span class="rate primary-bgcolor" ></span>
                        </div>
                    </div>
                    <ul class="ui-list-features list-unstyled">
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" >
                                <i class="icon icon-images"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false">
                                <i class="icon icon-map-marked-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false">
                                <i class="icon icon-more"></i>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator price-wrapper">
                    <div class="ui-list-price text-center">
                        <%--<span class="price strikeout d-inline-block d-sm-block"><del><em class="currency">AED </em>1177</del></span>--%>
                        <span class="price d-inline-block d-sm-block"></span>
                        <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0" >SELECT ROOM</button>
                    </div>
                </div>
            </div>
        </div>  
							</li>		
                   							<li class="item">		
                                                    <div >
            <div class="row no-gutters">
                <div class="col-2 col-sm image-wrapper">
                    <img src="build/img/hotel-full-demo.jpg" alt="Hotel Name" class=" h-100">
                </div>
                <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column text-content-wrapper">
                    <h4 ></h4>
                    <p class="address mb-0" ></p>
                    <div class="mb-auto mt-2">
                        <div ></div>
                        <div class="review-rating float-left mt-2" style="display:none">
                            <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                            <span class="rate primary-bgcolor" ></span>
                        </div>
                    </div>
                    <ul class="ui-list-features list-unstyled">
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" >
                                <i class="icon icon-images"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false">
                                <i class="icon icon-map-marked-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false">
                                <i class="icon icon-more"></i>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator price-wrapper">
                    <div class="ui-list-price text-center">
                        <%--<span class="price strikeout d-inline-block d-sm-block"><del><em class="currency">AED </em>1177</del></span>--%>
                        <span class="price d-inline-block d-sm-block"></span>
                        <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0">SELECT ROOM</button>
                    </div>
                </div>
            </div>
        </div>  
							</li>	
															<li class="item">
   <div >
            <div class="row no-gutters">
                <div class="col-2 col-sm image-wrapper">
                    <img src="build/img/hotel-full-demo.jpg" alt="Hotel Name" class=" h-100">
                </div>
                <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column text-content-wrapper">
                    <h4 ></h4>
                    <p class="address mb-0" ></p>
                    <div class="mb-auto mt-2">
                        <div></div>
                        <div class="review-rating float-left mt-2" style="display:none">
                            <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                            <span class="rate primary-bgcolor" ></span>
                        </div>
                    </div>
                    <ul class="ui-list-features list-unstyled">
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" >
                                <i class="icon icon-images"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false">
                                <i class="icon icon-map-marked-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false">
                                <i class="icon icon-more"></i>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator price-wrapper">
                    <div class="ui-list-price text-center">
                        <%--<span class="price strikeout d-inline-block d-sm-block"><del><em class="currency">AED </em>1177</del></span>--%>
                        <span class="price d-inline-block d-sm-block"></span>
                        <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0">SELECT ROOM</button>
                    </div>
                </div>
            </div>
        </div>  
								       
							</li>	
				
						</ul>
                         <div class="inn_wrap01" id="Noresults" style=" display:none;">
                    <div class="ns-h3">
                        No Hotels found
                    </div>
                    <div style="text-align: center; font-weight: bold">
                        Sorry we didn't find any Hotels, please contact our Support Team.
                    </div>
                    <div class="clear">
                    </div>
                </div>
                        <div class="ui-listing-wrapper" id="list-group-wrap">
                            <ul class="list-unstyled" id="list-group">
                            </ul>
                            <div class="lds-ellipsis" id="lazyLoader" style="display:none"><div></div><div></div><div></div><div></div></div>
                            <div id="FinishedLoading" style="display:none">Finished Loading</div>
                        </div>
                        <div id="map-view-div">
                              <div class="map-container column-map right-pos-map fix-map hid-mob-map">
                                <div id="map-main" style="width: 100%;height:720px;"></div>
                                <ul class="mapnavigation">
                                    <li><a href="#" class="prevmap-nav"><i class="icon icon-navigate_before"></i> Prev</a></li>
                                    <li><a href="#" class="nextmap-nav">Next <i class="icon icon-navigate_next"></i></a></li>
                                </ul>
                                <div class="map-close"><i class="fas fa-times"></i></div>
                                <%--<input id="pac-input" name="pacint" class="controls fl-wrap controls-mapwn" type="text" placeholder="What Nearby?">--%>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
            
         <div id="templateView" style="display:none;">
            <div class="row no-gutters">
                <div class="col-2 col-sm image-wrapper">
                    <img src="build/img/hotel-full-demo.jpg" alt="Hotel Name" class=" h-100" id="HotelImage">
                </div>
                <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column text-content-wrapper">
                    <h4 id="h4HotelName"></h4>
                    <p class="address mb-0" id="pAddress"></p>
                    <div class="mb-auto mt-2">
                        <div id="divrating" class="star-rating-list float-left"></div>
                        <div class="review-rating float-left mt-2" style="display:none">
                            <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                            <span class="rate primary-bgcolor" id="spanRating"></span>
                        </div>
                    </div>
                    <ul class="ui-list-features list-unstyled">
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" id="btnimage">
                                <i class="icon icon-images"></i>
                            </a>
                        </li>
                        <li id="goToMapDetailBtn">
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false" id="btnlocation">
                                <i class="icon icon-map-marked-alt"></i>
                            </a>
                        </li>
                        <li id="goToMapBtn">
                            <a class="map-item">
                                 <i class="icon icon-map-marked-alt"></i>
                            </a>
                        </li>
                        <li>
                            <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false" id="btnDes">
                                <i class="icon icon-more"></i>
                            </a>
                        </li>
                    </ul>                    
                    <image id="htlimgPromotion" class="htl-img-promotion"/>
                </div>
                <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator price-wrapper">
                    <div class="ui-list-price text-center">
                        <span class="price d-inline-block d-sm-block strikeout" id="TotalPrice"></span>
                      <span class="price d-inline-block d-sm-block " id="Discount"></span>
                        <span class="currency " id="isPackage"></span>
                      <span class="currency" id="spnPerNight" style="display:none">(per night,Excl.Taxes)</span>  <br />
                        <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0" id="btnSelectRoom">SELECT ROOM</button>
                    </div>
                </div>
            </div>
            <div class="" id="">				 				 
			</div>
           
            <div class="ui-collapsible-panel exapanded-view" id="HotelDetailsview">
                <div class="ui-list-collapse-content" id="hotel-listing-collapse-1">
                    <a href="javascript:void(0);" class="ui-close-button"><span class="icon icon-close"></span></a>
                    <div class="card card-body">
                        <ul class="nav nav-pills mb-3 hotel-listing-tab" id="hotel-listing-tab-2" role="tablist">
                            <li class="nav-item active">
                                <a class="nav-link active" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-2" aria-selected="true" id="btnDetaliGallery">Photo Gallery</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-2" aria-selected="false" id="btnDetalilocation">Location</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-2" aria-selected="false" id="btnDetaliOverview">Overview</a>
                            </li>
                        </ul>
                        <div class="tab-content hotel-listing-tabContent" id="hotel-listing-tabContent-2">
                            <div class="tab-pane fade show active in" id="pills-gallery-tab" role="tabpanel" aria-labelledby="pills-gallery-tab">

                                <!--Gallery-->

                                <div class="" id="Gallery">

                                </div>
                                 
                            </div>
                            <div class="tab-pane fade" id="pills-location-tab" role="tabpanel" aria-labelledby="pills-location-tab-2">
                               
                                <div id="map_canvas" style="width:100%;height:380px;"></div>

                            </div>
                            <div class="tab-pane fade" id="pills-overview-tab" role="tabpanel" aria-labelledby="pills-overview-tab-2">

                                <h6 class="pl-3 mt-3 mb-0 font-weight-bold">HOTEL AMENITIES</h6>
                                <ul class="amenities-list" id="Amenities">
                                </ul>
                                <div class="clearfix"></div>
                                <h6 class="my-3 px-3 font-weight-bold">HOTEL OVERVIEW</h6>
                                <div class="hotel-description px-3" id="HotelDes">
                                  
                                </div>



                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    <script src="build/js/jquery.royalslider.min.js"></script>
    <script src="build/js/jquery-ui.js"></script>
    <script src="build/js/hotel-listing.js"></script> 
    <script src="build/js/nprogress.js"></script>

    <%--MAP/LIST/GRID VIEW SCRIPTS--%>
    <script src="build/js/maps.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0mr4cq7HrZgJb9TZMLYWk-yh-_Ahg9Ro&libraries=places&callback=initAutocomplete" async defer></script>

   <%--     </span>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
