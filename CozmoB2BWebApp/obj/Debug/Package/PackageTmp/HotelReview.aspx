<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="HotelReview" Title="Hotel Review" Codebehind="HotelReview.aspx.cs" %>
    
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <asp:HiddenField ID="hdnWarning" EnableViewState="true" runat="server" />
    
    <div class="ns-h3 mar-top-10"> Hotel Review</div>
    
   <div class="bg_white pad_10 bor_gray">
   <div class="col-md-2"> 
       <%if (!string.IsNullOrEmpty(result.HotelPicture)) { 
               result.HotelPicture= result.HotelPicture.Replace("http:", "https:");
               %>
       
                                            <%if (itinerary.Source == HotelBookingSource.RezLive || itinerary.Source == HotelBookingSource.LOH || itinerary.Source == HotelBookingSource.HotelBeds || itinerary.Source == HotelBookingSource.GTA || itinerary.Source == HotelBookingSource.TBOHotel || itinerary.Source == HotelBookingSource.JAC || itinerary.Source == HotelBookingSource.EET || itinerary.Source == HotelBookingSource.Agoda||itinerary.Source == HotelBookingSource.GRN) //Added by brahmam 26.09.2014
                                                {%>
                                                <img src="<%=result.HotelPicture %>" width="87" height="64" alt="Hotel Image" />
                                                <%}
                                                    else if (itinerary.Source == HotelBookingSource.HotelConnect)
                                                    { %>
                                                <img src="<%= CT.Configuration.ConfigurationSystem.HotelConnectConfig["imgPathForServer"] + result.HotelPicture.Split(',')[0] %>" width="87" height="64" alt="HotelImage" />
                                                <%} else if (itinerary.Source == HotelBookingSource.DOTW) {%>
                                                <img src="<%= result.HotelPicture.Split(',')[0] %>" width="87" height="64" alt="HotelImage" />
                                                <%}
                                                    else if (itinerary.Source == HotelBookingSource.WST) {%>
                                              <img src ="<%=result.HotelPicture %>"width="87" height="64" alt="HotelImage" />
                                              <%} else if (itinerary.Source == HotelBookingSource.Miki) {  %>
                                              <img src="<%= result.HotelPicture.Split(',')[0] %>" width="87" height="64" alt="HotelImage" />
                                              
                                              <%} }%>
   
   </div>  
   
    <div class="col-md-5">
    
    
    <div><b><%=itinerary.HotelName %></b> </div>
    
    <div>  Add :-<%=itinerary.HotelAddress1 %></div>
    
    <div> Tel :- <strong><%=itinerary.HotelAddress2 %></strong> </div>
    
    
    
    
    
    
     </div>    
    
     <div class="col-md-5">
     
       <%  int adults = 0, child = 0;
           //string roomTypes = "";

           for (int i = 0; i < itinerary.Roomtype.Length; i++)
           {
               HotelRoom room = itinerary.Roomtype[i];
               if (room != null)
               {
                   adults += room.AdultCount;
                   child += room.ChildCount;


               }
           }%>
     
     
     <div>
                                                <strong class="spnred">No. of Rooms </strong>:
                                                <%=request.NoOfRooms %><br />
                                            </td>
                                          
                                   </div>
      
      
      <div> 
                            
   <strong class="spnred">No. of People </strong>:  <%=adults %> Adult(s), <%=child %> Child(s)
                                        
      
      </div>
      
      
      <div>    <strong class="spnred">Check In :</strong> <%=request.StartDate.ToString("dd/MM/yyyy") %>
      
</div>

<div>   <strong class="spnred">Check Out :</strong><%=request.EndDate.ToString("dd/MM/yyyy") %></div>
     
     
     
      </div>    
   
   
   
   <div class="clearfix"> </div>
   
   </div>
   
  
  
  
<div class="martop_20"> 
    
    
                    <% decimal total = 0, gtotal = 0, tax = 0, disc = 0; //int j = 1;
                        string roomNames = "";

                      %>
                      
                      
                      <h4> Payment Details</h4>

                      <div  class="table-responsive bg_white"> 
                        <table width="100%" class="table">
                        <tr>
                            <td>
                                    <b>Room no.</b>
                                </td>
                                <td>
                                    <b>Room Type </b>
                                </td>
                                 <td>
                                    <b>No. of Guests</b>
                                </td>
                                <td>
                                    <b>Total Room Price </b>
                                </td>
                          
                                <td>
                                    <b>Total (<%=result.Currency %>)</b>
                                </td>
                            </tr>
                        <% int guests = 1;
                            foreach (CT.BookingEngine.HotelRoom room in itinerary.Roomtype)
                            {
                                if (room != null)
                                {
                                    if (itinerary.Source == HotelBookingSource.RezLive)
                                    {
                                        roomNames = room.RoomName.Split('|')[0] + " - " + room.MealPlanDesc;
                                    }
                                    else
                                    {
                                        //if (itinerary.Source == HotelBookingSource.DOTW)
                                        {
                                            roomNames = room.RoomName + " - " + room.MealPlanDesc;
                                        }
                                        //else
                                        //{
                                        //    roomNames = room.RoomName + " - " + room.RoomTypeCode;
                                        //}
                                    }
                                    //if (room.CancelRestricted)
                                    //{
                                    //    roomNames += "</br><p style='color:red'>";

                                    //    foreach (string data in cancelData.Split('|'))
                                    //    {
                                    //        if (data.Contains("Restricted"))
                                    //        {
                                    //            roomNames += data + "</p>";
                                    //        }
                                    //    }
                                    //}
                                    disc = room.Price.Discount;
                                    tax += (room.Price.Tax);
                                    //if (hotelInfo[index].BookingSource == HotelBookingSource.LOH)
                                    //{
                                    //    if (hotelInfo[index].RoomDetails.Length == request.NoOfRooms)
                                    //    {
                                    //        total = (priceType == CT.BookingEngine.PriceType.PublishedFare ? ((room.Price.PublishedFare + room.Price.Tax)) : ((room.Price.NetFare + room.Price.Tax)));
                                    //    }
                                    //    else
                                    //    {
                                    //        total = (priceType == CT.BookingEngine.PriceType.PublishedFare ? ((room.Price.PublishedFare + room.Price.Tax)) : ((room.Price.NetFare + room.Price.Tax)));
                                    //    }
                                    //}
                                    //else if (hotelInfo[index].BookingSource == HotelBookingSource.RezLive)
                                    //{
                                    //    if (hotelInfo[index].RoomDetails.Length == request.NoOfRooms)
                                    //    {
                                    //        total = (priceType == CT.BookingEngine.PriceType.PublishedFare ? ((room.Price.PublishedFare + room.Price.Tax)) : ((room.Price.NetFare + room.Price.Tax)));
                                    //    }
                                    //    else
                                    //    {
                                    //        total = (priceType == CT.BookingEngine.PriceType.PublishedFare ? ((room.Price.PublishedFare + room.Price.Tax)) : ((room.Price.NetFare + room.Price.Tax)));
                                    //    }
                                    //}
                                    //else
                                    {
                                        if (result.RoomDetails.Length == request.NoOfRooms)
                                        {
                                            total = (priceType == CT.BookingEngine.PriceType.PublishedFare ? ((room.Price.PublishedFare)) : ((room.Price.NetFare + room.Price.Markup)));
                                        }
                                        else
                                        {
                                            total = (priceType == CT.BookingEngine.PriceType.PublishedFare ? ((room.Price.PublishedFare)) : ((room.Price.NetFare + room.Price.Markup)));
                                        }
                                    }

                                    //total = (priceType == CT.BookingEngine.PriceType.PublishedFare ? ((room.Price.PublishedFare + room.Price.Tax) * itinerary.NoOfRooms) : ((room.Price.NetFare + room.Price.Markup + room.Price.Tax) * itinerary.NoOfRooms));
                                    gtotal += total;
                                }%>
                            
                            <tr>
                            <td>
                                    <%=guests %>
                                </td>
                                <td>
                                    <%=roomNames %>
                                </td>
                                  <td>
                                    <% =(room != null ? room.AdultCount + room.ChildCount : 1)%>                                    
                                    (<%= (room != null ? room.AdultCount : 1)%> Adult(s)
                                    <%= (room != null ? room.ChildCount : 0) %> Child(s)) 
                                    <%guests++; %>
                                </td>
                                <td>                                    
                                <%=result.Currency %>
                                    <%=Math.Round(total, decimalValue).ToString("N" + decimalValue.ToString())%>
                                </td>
                                <%--<td>
                                    AED <%=tax.ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                                </td>--%>
                                
                                <td>
                                    
                                    <% %>
                                    
                                    <%=Math.Round(total, decimalValue).ToString("N" + decimalValue.ToString())%>
                                     
                                </td>
                            </tr>
                              <% if (discountType.Length > 0)
                                      {
                                          if (discountType == "P")
                                          {
                                              disc = Math.Ceiling(gtotal) * disc;
                                          }
                                      }
                                      if (room != null)
                                      {
                                          room.Price.Discount = disc;
                                      }
                                  }%>
                                                
                        <%if (disc > 0) {
                                foreach (HotelRoom room in itinerary.Roomtype)
                                {
                                    if (room != null)
                                    {
                                        room.Price.Discount = disc;
                                    }
                                }  %>
                        
                        <tr>
                                <td colspan="4">
                                    <b>Discount </b>
                                
                                </td>
                              
                                <td>
                                    <b class="spnred">
                                    
                                        <%=(disc).ToString("N" + decimalValue.ToString())%></b>
                                     
                                </td>
                            </tr>
                            <%} %>
                           <tr>
                                <td colspan="4">
                                    <b>Grand Total </b>
                                
                                </td>
                              
                                <td>
                                    <b class="spnred">
                                    <% 
                                        gtotal = gtotal - disc; %>
                                        <%=Math.Ceiling(gtotal).ToString("N" + decimalValue.ToString())%></b>
                                     
                                </td>
                            </tr>
                           
                        </table>
                        
                        
                        <div class="clearfix"> </div>
                        </div>    
        
        
        <div class="clearfix"></div>
        </div>
           <div class="martop_20"> 
        
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                
       
       
                
            
                <tr>
                    <td>
                   <%                     //itinerary.HotelCancelPolicy = cancelData;
//itinerary.HotelPolicyDetails = remarks;
                                                 %>
                        <table>
                        <%if (remarks.Length > 0)
                            //modified by brahmam Req...HotelBed norms displaying red colour  19/09/2015(vinay said)
                            { %>
                            <tr>
                                <td>
                                  
                                  <h4> Hotel Norms</h4>
                                  
                                    <ul>
                                         <%string[] norms = remarks.Split('|'); %>
                                    <%foreach (string remark in norms)
                                        {
                                            if (itinerary.Source == HotelBookingSource.HotelBeds)
                                            {
                                           %>
                                        <li style="color:#FF0000;" ><%=remark.Replace("#&#", "")%></li> 
                                                                                  
                                           <%
                                               }
                                               else
                                               {%>
                                               <li ><%=remark.Replace("#&#", "")%></li> 

                                         <% }
                                             } %>
                                    </ul>
                                </td>
                            </tr>
                            <%} %>
                            
                            <%if (itinerary.Source == HotelBookingSource.GTA || itinerary.Source == HotelBookingSource.JAC || itinerary.Source == HotelBookingSource.Agoda||itinerary.Source == HotelBookingSource.GRN ||itinerary.Source == HotelBookingSource.OYO) //showing EssentialInformation Added by brahmam 20.09.2016
                                { %>
                              <tr> 
                               <td>
                                   <%if (!string.IsNullOrEmpty(itinerary.Roomtype[0].EssentialInformation))
                                       { %>
                                    <b>Essential Information</b><br />
                                    <ul style="float: left;">
                                     <%string[] essentialinf = itinerary.Roomtype[0].EssentialInformation.Split('|'); %>
                                    <%foreach (string essential in essentialinf)
                                        {
                                            if (!string.IsNullOrEmpty(essential))
                                            {%>
                                       <li><%=essential%></li>                                           
                                          <%}
                                              } %>
                                    </ul>
                                    <%} %>
                                </td>
                              </tr>
                              <%} %>
                            <%if (itinerary.Source == HotelBookingSource.Agoda)
                                { %>
                            <tr>
                                <td>
                                    <%if (!string.IsNullOrEmpty(itinerary.HotelPolicyDetails))
                                        { %>
                                    <b>Hotel Norms</b><br />
                                    <ul style="float: left;">
                                        <li style="color: #FF0000;"><%=itinerary.HotelPolicyDetails%></li>
                                    </ul>
                                    <%} %>
                                </td>
                            </tr>
                            <%}%>
                           
                             <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                           
                            <tr>
                            
                                <td>
                                  <h4> Cancellation & Charges</h4>
                                  
                                  
                                    <ul style="float: left;">
                                     <%string[] cdata = cancelData.Split('|'); %>
                                    <%foreach (string data in cdata)
                                      { %>
                                       <li><%=data%></li>                                           
                                          <%} %>
                                    </ul>
                                   
                                    <%--<div style="float: left; width: 100%;">
                                        <img src="Images/spacer.gif" alt="spacer" height="10px" /></div>
                                    <p style="width: 98%; float: left; padding: 10px 0 0 0;">
                                        <b>Amendment Charges:</b></p>
                                    <div class="norms1" style="float: left; width: 700px; font-weight: normal;">
                                        <ul style="float: left; width: 98%; font-weight: normal;">
                                            
                                        </ul>
                                        </div>--%>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="color:Red;font-size:14px">
                        <%=warningMsg %>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td class="pad_10" align="right"> 
                     <%if(!string.IsNullOrEmpty(itinerary.HotelCancelPolicy)){ %>                      
                        <asp:Button ID="imgContinue" CssClass=" but but_b" runat="server" Text="Continue"  OnClick="imgContinue_Click" OnClientClick="return confirmBox()"/>
                        <%} %>
                    </td>
                </tr>
            </table>
        </div>

    <script>
        //Added by brahmam Req...HotelBeds purpose  19/09/2015(vinay said)
        function confirmBox() {
            var source = '<%=itinerary.Source%>';
            if (source == "HotelBeds" || source == "Agoda") {
                var msg = confirm("Review the hotel Norms & Canellation Policies strictly, check if the selected booking Price is applied for the Pax Nationality.Cozmo Travel will not take any responsibilty incase of any distputes for which information is already available in Hotel Norms and Cancellation Policies and additional info.");
                if (msg == true) {
                    return true;
                }
                else {
                    return false;
                }
            }
            return true;
        }
    </script>
</asp:Content>
