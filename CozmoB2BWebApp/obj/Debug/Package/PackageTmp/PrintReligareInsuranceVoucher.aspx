﻿<%@ Page Title="" Language="C#" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="PrintReligareInsuranceVoucher.aspx.cs" Inherits="CozmoB2BWebApp.PrintReligareInsuranceVoucher" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />

    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <!-- manual css -->
    <link href="css/override.css" rel="stylesheet" />



</head>
<body>
    <form id="form1" runat="server">
<!--
        <link href="css/steps.css" rel="stylesheet" type="text/css" />

        <div class="wizard-steps">
            <div class="completed-step">
                <a href="#step-one"><span>1</span>Select Policy</a>
            </div>
            <div class="completed-step">
                <a href="#step-two"><span>2</span> PAX Details</a>
            </div>
            <div class="completed-step">
                <a href="#"><span>3</span> Secure Payments</a>
            </div>
            <div class="active-step">
                <a href="#"><span>4</span> Confirmation</a>
            </div>
            <div class="clear">
            </div>
        </div>
-->
        <br />

        <div style="text-align: right">
            <%--        <asp:LinkButton ID="lnkVoucher" CssClass="btn btn-info btn-primary" OnClientClick="return viewVoucher();" runat="server" Text="Show Voucher" Style="float: right; border-radius: 5px; margin-right: 10px;"></asp:LinkButton>--%>

            <asp:HiddenField ID="hdnpolicyNo" runat="server" />
            <asp:HiddenField ID="hdnpolicyData" runat="server" />

        </div>
        <div class="col-md-12">
            <div id="emailBlock" class="showmsg" style="position: absolute; display: none; left: 400px; top: 200px; width: 300px;">
                <div class="showMsgHeading">Enter Your Email Address</div>

                <a style="position: absolute; right: 5px; top: 3px;" onclick="return HidePopUp();" href="#" class="closex">X</a>
                <div class="padding-5">
                    <div style="background: #fff">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td height="40" align="center">
                                    <b style="display: none; color: Red" id="err"></b>
                                    <asp:TextBox Style="border: solid 1px #ccc; width: 90%; padding: 2px;" ID="txtEmailId" runat="server" CausesValidation="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>

                                <td height="40" align="center">
                                    <asp:Button CssClass="button-normal" ID="btnEmailVoucher" runat="server" Text="Send Email" OnClientClick="return Validate()" OnClick="btnEmailVoucher_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>



                </div>



            </div>
            <div id="printableArea" runat="server">

                <div class="col-md-12">
                    <asp:Image ID="imgHeaderLogo" runat="server" Width="162" Height="51px" AlternateText="AgentLogo" ImageUrl="" />
                    <label class="pull-right mar10">
                        <asp:Button ID="btnPrint" runat="server" Text="Print Voucher" OnClientClick="return printPage();" /></label>

                    <label class="pull-right mar10">
                        <asp:Button ID="btnEmail" runat="server" Text="Email Voucher" OnClientClick="return ShowPopUp(this.id);" /></label>

                </div>
                <div class="font_med marbot_20">
                    <center>  <strong> Insurance Voucher</strong></center>
                </div>
                <div class="container" style="margin-top: 80px">

                    <% if (header != null && header.ReligarePassengers != null && header.ReligarePassengers.Count > 0)
                        {
                            for (int i = 0; i < header.ReligarePassengers.Count; i++)
                            {%>
                    <input id="hdnPaxCount" type="hidden" value='<%= header.ReligarePassengers.Count %>' />
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a style="display: block; width: 100%" href="#collapseOne<%=i %>" data-parent="#accordion" data-toggle="collapse">
                                        <span class="glyphicon pull-right font_bold glyphicon-minus"></span>
                                        <%=header.ReligarePassengers[i].FirstName + " " + header.ReligarePassengers[i].LastName%>
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne<%=i %>" class="collapse in">
                                <table width="100%" border="1" class="table" cellpadding="0" cellspacing="0">
                                    <tr class="font_bold">
                                        <td>Plan Title</td>
                                        <td>Plan Code</td>
                                        <td >Policy No</td>                                       
                                        <td id="tdPolicy<%=i %>">Policy Link</td>
                                    </tr>
                                    <tr>
                                        <td><%=header.ProductName %></td>
                                        <td><%=header.Produ_id %></td>
                                        <td><%=header.Policy_no %></td>
                                          <td id="tdViewLink<%=i %>"  style="display: block;"><a target='_blank' class="certificate" href='#'>View Certificate</a></td>

                                    </tr>
                                </table>
                            </div>
                        </div>

                    </div>
                    <%}
                        }%>
                </div>
            </div>

        </div>
        <div class="col-md-12 marbot_10">
            <label style="color: Red">
                Note:</label>This voucher is only for cozmo internal reference 
        </div>

    </form>
</body>

<script src="https://code.jquery.com/jquery-1.12.4.js"></script>
<script type="text/javascript">
    function printPage() {
         document.getElementById('btnPrint').style.display = "none";
        document.getElementById('btnEmail').style.display = "none";
      //  $('.tdlink').hide();
     
         
        $('.certificate').hide();        
        for (var i = 0; i < document.getElementById('hdnPaxCount').value; i++) {
            
                document.getElementById('tdViewLink' + i).style.display = "none";
             document.getElementById('tdPolicy' + i).style.display = "none";
        }
        //    for (var k = 0; k < document.getElementById('hdnPlansCount').value; k++) {
        //        if (document.getElementById('tdViewLink' + i + k) != null) {
        //            document.getElementById('tdViewLink' + i + k).style.display = "none";
        //        }
        //    }
        //}
        window.print();
        setTimeout('showButtons()', 1000);
        return false;
    }
    function showButtons() {
        document.getElementById('btnPrint').style.display = "block";
        document.getElementById('btnEmail').style.display = "block";
        $('.tdlink').show();
        $('.certificate').show();
        for (var i = 0; i < document.getElementById('hdnPaxCount').value; i++) {            
                document.getElementById('tdPolicy' + i).style.display = "block";
             document.getElementById('tdViewLink' + i).style.display = "block";
        }
        //    for (var k = 0; k < document.getElementById('hdnPlansCount').value; k++) {
        //        if (document.getElementById('tdViewLink' + i + k) != null) {
        //            document.getElementById('tdViewLink' + i + k).style.display = "block";
        //        }
        //    }
        //}
    }
    function HideButtons() {
    document.getElementById('btnPrint').style.display = "none";
        document.getElementById('btnEmail').style.display = "none";
        
        debugger;
         
        $('.certificate').hide();        
        for (var i = 0; i < document.getElementById('hdnPaxCount').value; i++) {
            
                document.getElementById('tdViewLink' + i).style.display = "none";
             document.getElementById('tdPolicy' + i).style.display = "none";
        }
    }

    function Validate() {
        var isValid = true;
        var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
        document.getElementById('err').style.display = 'none';
        if (document.getElementById('<%=txtEmailId.ClientID %>').value.length <= 0) {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Email address";
                isValid = false;
            }
            else if (reg.test(document.getElementById('<%=txtEmailId.ClientID %>').value)) {
            document.getElementById('err').style.display = 'none';
        }
        else {
            document.getElementById('err').style.display = 'block';
            document.getElementById('err').innerHTML = "Enter Correct Email";
            isValid = false;
        }
        if (isValid == true) {
            return true;
        }
        else {
            return false;
        }
    }
    function ShowPopUp(id) {
        document.getElementById('txtEmailId').value = "";
        document.getElementById('err').style.display = 'none';
        document.getElementById('emailBlock').style.display = "block";
        return false;
    }
    function HidePopUp() {
        document.getElementById('emailBlock').style.display = "none";
    }

    $(document).ready(function () {
        $(".certificate").click(function () {
            debugger;
            var policyNo =<%=header.Policy_no%>;//$("#ctl00_cphTransaction_hdnpolicyNo").val();
            $.ajax({
                type: "POST",
                contentType: "application/json; charset=utf-8",
                url: "PrintReligareInsuranceVoucher.aspx/GetPolicyPDFData",
                data: "{'policyNo':'" + policyNo + "'}",
                dataType: "json",
                success: function (data) {
                    if (data.d == "")
                        alert('Statement Not Generated For Given Pnumber');
                    else {
                        var pdfWindow = window.open("");
                        pdfWindow.document.write("<iframe width='100%' height='100%' src='data:application/pdf;base64, " + data.d + "'></iframe>");
                    }
                },
                error: function (result) { }

            });
        });
    });
</script>
</html>




