﻿<%@ Page Language="C#" MasterPageFile="~/Transaction.master" AutoEventWireup="true" Inherits="ContWhiteLabelUI" Title="" Codebehind="ContWhiteLabel.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
 
 
 
  
 <style> 
 
 .body_container { line-height:24px; font-size:14px; }

 </style>
 
 
 <div class="body_container">
 
 <div class="col-md-12"> 
 <h4 class=" paddingtop_10 paddingbot_10">WHITE LABEL</h4>
 


Grow your business with our innovative and robust White Label solutions. We take care of your technology and operations constraints while you take care of your customers. Our White Label solution offers complete suite of travel products including Flights, Hotels, Visa, Holiday Packages, Activities and Insurance. We have competitive inventory (content)where you can mark-up your margin and enjoy your profit by selling the products to your customers.You can any time log on to your administrative dashboard and see your business dashboard all in one place through our innovative back-end module and you can manage all your all online booking, sales and commissions of their sub agents.  <br /> <br /> 

You can even integrate your own content through XML interface on our White Labeled solution. You can avail our integrated payment gateway system or you can use your own payment gateway system for payments & processing.  <br /> <br /> 


<strong> Benefits of using our White Label Solution:</strong><br /> <br /> 

a) Build your Brand Online <br /> 

We help you building your brand online using our innovative white labeled solution. You can create fascinating design for your website with your own company name, logo and branding.  <br /> <br /> 


b) Fully Customizable Solution <br />

We can customize the white labeled solution as per your requirements and specification. Our solution is flexible so that you can implement your own design and look & feel on our solution.  <br /> <br /> 

c)	Increase your Sales <br />

with improved customer journey and ese of use of our white labeled solution, your customer will be able to purchase more frequently our your website.
d)	Focus on your business while we focus on the technology. <br /> <br /> 

Using our white labeled solution, you can focus on your business and operation, whilst we provide the best-in-class technology solution for your business  <br /> <br /> 


e)	Saves Time and Money <br />

By using our white labeled solution, you can save time and money by not investing on technology, people and process which is not your core expertise. It will take more time and effort for you to build the same technology which we are providing through our white labeled solution.




 <br /> <br /> 

 
 </div>
 
 <div class="clearfix"> </div>
 </div>

 
 
 
 
 
 



 
</asp:Content>

