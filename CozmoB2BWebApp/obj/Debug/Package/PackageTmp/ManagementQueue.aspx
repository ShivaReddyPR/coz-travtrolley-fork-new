﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true"
    Inherits="ManagementQueue" Title="Untitled Page" Codebehind="ManagementQueue.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <%----------------------------------the files for magnific popup-------------------------------------------------%>
    <%--<script src="Scripts/magnificpop/jquery.js" type="text/javascript"></script>--%>

    <script src="Scripts/magnificpop/jquery.magnific-popup.js" type="text/javascript"></script>

    <link rel="stylesheet" href="css/magnific-popup.css" type="text/css">
    <%-----------------------------------------------------------------------------------%>
    <link href="css/TicketQuee.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "fromContainer1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "toContainer2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1_checker() {
            $('toContainer2').context.styleSheets[0].display = "none";
            $('fromContainer1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
            cal2.hide();
        }


        var departureDate = new Date();
        function showCal2_checker() {
            $('fromContainer1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtFromDate1.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('toContainer2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('Iframe1').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());


            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtFromDate1.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtFromDate1.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtToDate1.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);
    </script>

    <script type="text/javascript">
        function show(id) {
            document.getElementById(id).style.visibility = "visible";
        }
        function hide(id) {
            document.getElementById(id).style.visibility = "hidden";
        }


        function showStuff(id) {
            document.getElementById(id).style.display = 'block';
        }


        function hidestuff(boxid) {
            document.getElementById(boxid).style.display = "none";
        }

        function showAgent(id) {

            document.getElementById('DisplayAgent' + id).style.display = 'block';
        }
        function Validation() {
            var fromDate = document.getElementById('<%=txtFromDate1.ClientID %>').value;
            var toDate = document.getElementById('<%=txtToDate1.ClientID %>').value;
            var currentDate = new Date();
            if (fromDate.length > 0) {
                if (toDate.length > 0) {
                    if (fromDate.length > 0 && toDate.length > 0) {
                        //                        if (fromDate > toDate) {
                        //                            alert("From Date should be less than ToDate");
                        //                            return false;
                        //                        }
                        if (fromDate > currentDate) {
                            alert("FromDate should be less than CurrentDate");
                            return false;
                        }
                        if (toDate > currentDate) {
                            alert("ToDate Should be less than CurrentDate");
                            return false;
                        }
                    }
                }
                else {
                    alert("please selectt ToDate");
                    return false;
                }
            }
            else {
                alert("please select FromDate");
                return false;
            }
            return true;
        }



        function enableGridControls1(chkId) {
            var Id = chkId.substring(0, chkId.lastIndexOf('_') + 1)
            if (document.getElementById(chkId) != null) {
                document.getElementById(Id + 'ddlStatus1').disabled = !document.getElementById(chkId).checked;
                document.getElementById(Id + 'ddlStatus1').className = document.getElementById(chkId).checked ? 'inputEnabled' : 'inputDisabled'

            }
            getManagementIdStatus1();
        }

        function getManagementIdStatus1() {
            var count = eval('<%=rowsCount %>');
            document.getElementById('<%= hdnIds_CheckerIds1.ClientID %>').value = null;
            for (var i = 2; i <= count + 2; i++) {
                if (document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl0' + i + '_ITchkStatus1')) {
                    if (document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl0' + i + '_ITchkStatus1').checked) {
                        var id = document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl0' + i + '_ManageId1').value;
                        var status = document.getElementById('ctl00_cphTransaction_gvCheckerQueue_ctl0' + i + '_ddlStatus1').value;
                        if (document.getElementById('<%= hdnIds_CheckerIds1.ClientID %>').value != null) {
                            document.getElementById('<%= hdnIds_CheckerIds1.ClientID %>').value += "," + id + "-" + status;
                        }
                        else {
                            document.getElementById('<%= hdnIds_CheckerIds1.ClientID %>').value = id + "-" + status;
                        }
                    }
                }
            }
            var hdnIds = document.getElementById('<%= hdnIds_CheckerIds1.ClientID %>').value;
        }

        function bindStatus_Checker() {
            getManagementIdStatus1();
        }


        function download() {
            var path = getElement('hdfPath').value;
            var type = getElement('hdfType').value;
            var docName = getElement('hdfDocName').value;

            // getElement('imgPreview').ImageUrl = "~/images/Common/Preview.png";
            var open = window.open('DownloadDoc.aspx?path=' + path + '&type=' + type + '&docName=' + docName);
            //getElement('hdfDownload').value = "download";
            //document.forms[0].submit();
        }
        function saveClickDetails( id,chequeId) {
            PageMethods.lnkDocuments_Click(id, chequeId, onComplete);
        }
        function onComplete() { 
        
        }

    </script>

    <iframe id="Iframe1" style="position: absolute; display: none;" frameborder="0">
    </iframe>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <div class="clear" style="margin-left: 25px">
        <div id="fromContainer1" style="position: absolute; top: 120px; left: 160px; display: none;
            z-index: 9999">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="toContainer2" style="position: absolute; top: 120px; left: 325px; display: none;
            z-index: 9999">
        </div>
    </div>
    <asp:Label ID="lblErrorMessage1" runat="server" Visible="false" Style="float: left;
        color: Red;" class="padding-5 yellow-back width-100 center margin-top-5"></asp:Label>
    <div title="Param" id="div5">
        <asp:Panel runat="server" ID="Panel1" Visible="true" CssClass="paneldocs-datePickerControl">
            <div class="paramcon">
                <div class="col-md-12 padding-0 marbot_10">
                    <table>
                        <tr>
                            <%--<td style="padding-right: 5px;">
                                <div>
                                    From Date:
                                </div>
                            </td>--%>
                            <%--   <td>
                                <div>
                                    <table>
                                        <tr>
                                            <td>
                                               
                                            </td>
                                            <td>
                                                <a href="javascript:void(null)" onclick="showCal1_checker()">
                                                    <img id="Img2" src="images/call-cozmo.png" alt="Pick Date" />
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>--%>
                            <td>
                                <div class="form-group">
                                    <label>
                                        From Date:</label>
                                    <div class="input-group" onclick="showCal1_checker()">
                                        <asp:TextBox ID="txtFromDate1" runat="server" CssClass="inputEnabled form-control"
                                            Width="100px"></asp:TextBox>
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                    </div>
                                </div>
                            </td>
                            <td style="padding-left: 20px; padding-right: 5px;">
                                <div>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <table>
                                        <tr>
                                            <td>
                                                <div class="form-group">
                                                    <label>
                                                        To Date:</label>
                                                    <div class="input-group" onclick="showCal2_checker()">
                                                        <asp:TextBox ID="txtToDate1" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                                        <div class="input-group-addon">
                                                            <span class="glyphicon glyphicon-calendar"></span>
                                                        </div>
                                                    </div>
                                                </div>
                                                <%--                                                <a href="javascript:void(null)" onclick="showCal2_checker()">
                                                    <img id="Img3" src="images/call-cozmo.png" alt="Pick Date" />
                                                </a>--%>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <asp:Label ID="lblCountryName1" runat="server" Text="Country Name :"></asp:Label>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <asp:DropDownList ID="ddlCountryName1" runat="server">
                                        <asp:ListItem Text="--Select--" Value="-1"></asp:ListItem>
                                        <asp:ListItem Text="INDIA" Value="INDIA"></asp:ListItem>
                                        <asp:ListItem Text="UAE" Value="UAE"></asp:ListItem>
                                        <asp:ListItem Text="QATAR" Value="QATAR"></asp:ListItem>
                                        <asp:ListItem Text="KSA" Value="KSA"></asp:ListItem>
                                        <asp:ListItem Text="KUWAIT" Value="KUWAIT"></asp:ListItem>
                                        <asp:ListItem Text="BAHRAIN" Value="BAHRAIN"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </td>
                            <td style="padding-left: 20px; padding-right: 5px; padding-top: 5px;">
                                <div class="form-group">
                                    <asp:Label ID="lblCompanyName1" runat="server" Text="Company Name"></asp:Label>
                                    <asp:TextBox ID="tbCompanyName1" runat="server" CssClass="form-control"></asp:TextBox>
                                </div>
                            </td>
                            <td>
                                <div>
                                </div>
                            </td>
                            <td>
                                <div>
                                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="Mangt-SubmitButton btn btn-primary"
                                        OnClick="btnSearch_OnClick" OnClientClick="return Validation();" />
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </asp:Panel>
    </div>
    <asp:HiddenField ID="hdnIds_CheckerIds1" runat="server" />
    <asp:HiddenField ID="hdnIds_Beneficiary" runat="server" />
    <asp:HiddenField ID="hdnBeneficiaryName" runat="server"></asp:HiddenField>
    <asp:GridView ID="gvManagementQueue" CssClass="gvManagementQueue tblvms table" runat="server"
        AllowPaging="false" AutoGenerateColumns="false" OnRowDataBound="OnRowDataBound">
        <Columns>
            <asp:TemplateField HeaderText="" Visible="false">
                <ItemTemplate>
                    <asp:CheckBox ID="ITchkStatus1" runat="server" OnClick="enableGridControls1(this.id);"
                        Width="20px" CssClass="label"></asp:CheckBox>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Id" HeaderText="Id" Visible="false" />
            <asp:BoundField DataField="DocDate" HeaderText="Document&nbsp;Date" DataFormatString="{0:dd/MM/yyyy}" />
            <asp:BoundField DataField="CountryName" HeaderText="CountryName" />
            <asp:BoundField DataField="CompanyName" HeaderText="Company Name" />
            <asp:BoundField DataField="BankName" HeaderText="BankName" />
            <asp:BoundField DataField="DebitAcNo" HeaderText="DebitAcNo" />
            <asp:TemplateField HeaderText="Beneficiary Name">
                <ItemTemplate>
                    <asp:LinkButton ID="lbtnBeneficiaryName" runat="server" Text='<%# Eval("BeneficiaryName") %>'
                        OnClick="lbtnBeneficiaryName_OnClick" />
                    <asp:HiddenField ID="ManageId1" runat="server" Value='<%# Eval("Id")%>' />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Currency" HeaderText="Currency" />
            <asp:BoundField DataField="Amount" HeaderText="Amount" />
            <asp:BoundField DataField="TransferType" HeaderText="TransferType" />
            <asp:BoundField DataField="Remarks" HeaderText="Remarks" />
            <asp:BoundField DataField="VoucherNo" HeaderText="VoucherNo" />
        </Columns>
    </asp:GridView>
    <br />
    <br />
    <asp:HiddenField runat="server" ID="hdfPath"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfType"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfDocName"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfPreviewPath"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfDownload"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfChequeId" />
    <asp:HiddenField ID="hdnRowsCount" runat="server" />
    <asp:Panel class="pnldocs ManageQueuePanelDocs" ID="pnlDocuments" runat="server"
        Visible="false" EnableViewState="true">
        <div class="modal-backdrop fade in">
        </div>
        <div class="PanelDocs-inner-wrapper" style="text-align: right">
            <div style="text-align: right">
                <span id="closepnldocs" onclick='hidepaneldoc();' class="close-btn-paneldocs glyphicon glyphicon-remove">
                </span>
            </div>
            <div class="modal-header" style="text-align: left; margin-left: 10px">
                <h4 class="modal-title">
                    Support Documents</h4>
            </div>
            <div id="ViewFiles" class="paneldocs-contents" runat="server" style="z-index: 9999;
                text-align: left; margin-left: 10px">
                <%-- <asp:HiddenField Visible="false" ID="IThdfDocId" runat="server"></asp:HiddenField>
                <asp:HiddenField Visible="false" ID="IThdfDocPath" runat="server"></asp:HiddenField>
                <asp:HiddenField Visible="false" ID="IThdfDocType" runat="server"></asp:HiddenField>
                <asp:HiddenField ID="IThdfDocName" runat="server"></asp:HiddenField>--%>
                <%--<asp:HiddenField ID="hdnBeneficiaryName" runat="server"></asp:HiddenField>--%>
            </div>
            <asp:LinkButton ID="lnlDownloadAll" runat="server" Text="Download All" CssClass="lnkEnabled downloadAll"
                OnClick="lnlDownloadAll_Click"></asp:LinkButton>
        </div>
    </asp:Panel>

    <script type="text/javascript">
        function hidepaneldoc() {
            document.getElementById("ctl00_cphTransaction_pnlDocuments").style.display = "none";
        }
        $(document).ready(function() {
            console.log("Hello");
            $("html").on('click', function() {
                $("#pnlDocuments").hide();
            })
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
