﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="CreateTransferInvoice.aspx.cs" Inherits="CozmoB2BWebApp.CreateTransferInvoice" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background:#f3f3f3!important">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>Transfer Invoice</title>
    <style>
        @media only screen {
            html {
                min-height: 100%;
                background: #f3f3f3
            }
        }

        @media only screen and (max-width:716px) {
            .small-float-center {
                margin: 0 auto !important;
                float: none !important;
                text-align: center !important
            }
        }

        @media only screen and (max-width:716px) {
            table.body img {
                width: auto;
                height: auto
            }

            table.body center {
                min-width: 0 !important
            }

            table.body .container {
                width: 95% !important
            }

            table.body .columns {
                height: auto !important;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                padding-left: 16px !important;
                padding-right: 16px !important
            }

                table.body .columns .columns {
                    padding-left: 0 !important;
                    padding-right: 0 !important
                }

            table.body .collapse .columns {
                padding-left: 0 !important;
                padding-right: 0 !important
            }

            th.small-6 {
                display: inline-block !important;
                width: 50% !important
            }

            th.small-12 {
                display: inline-block !important;
                width: 100% !important
            }

            .columns th.small-12 {
                display: block !important;
                width: 100% !important
            }

            table.menu {
                width: 100% !important
            }

                table.menu td,
                table.menu th {
                    width: auto !important;
                    display: inline-block !important
                }

                table.menu.vertical td,
                table.menu.vertical th {
                    display: block !important
                }

                table.menu[align=center] {
                    width: auto !important
                }
        }

        @media print {
            * {
                -webkit-print-color-adjust: exact !important;
                color-adjust: exact !important
            }
        }
        #loading {
                  width: 100%;
                  height: 100%;
                  top: 0;
                  left: 0;
                  position: fixed;
                  display: block;
                  opacity: 0.7;
                  background-color: #fff;
                  z-index: 99;
                  text-align: center;
                }

        #loading-image {
            position: absolute;
            top: 100px;
            left: 240px;
            z-index: 100;
        }
    </style>
    <script type="text/javascript" src="Scripts/Common/Common.js" ></script>
    <script type="text/javascript" src="Scripts/Menu/query2.js"></script>
</head>
<body>
    
    <form id="form1" runat="server">
        <div id="loading">
            <div class="lds-loader-wrap">
                    <div class="lds-loader-inner">
                <div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>
                        </div>
            </div>
          <img id="loading-image" style="width: 16px; height: 16px" src="Images/Home/Null.gif" alt="Loading..." />
        </div>
        <div id="divProcess" style="display:none" class="lds-loader-wrap"><div class="lds-loader-inner"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div></div>
            <div style="position:absolute; right:700px; top:485px; display:none" id="emailBlock">
                <div style="border: solid 4px #ccc;width:400px;height: 130px;"> 
                    <div style="font-size:16px;font-family:Arial;background-color:#60c231;padding: 5px 0px 5px 0px;color:#fff;font-weight: bold;"> 
                        Enter Email Address <a style="float:right;padding-right:10px;color: #fff;" href="javascript:HideEmailDiv()">X</a>
                    </div>                    
                    <div style=" padding:10px">
                        <input name="txtEmail" type="text" id="txtEmail" onchange="ValidateEmail(this.id, 'lblError')" style="width:100%;border: solid 1px #7F9DB9;height: 20px;" />
                        <label id="lblError" style="display:none;color:red;font-family:Arial;"></label>
                    </div>
                    <div style=" padding-left:10px; padding-bottom:10px">
                        <input type="submit" name="btnEmailInvoice" value="Send Email" onclick="return Validate();" id="btnEmailInvoice" style="font-size: 13px;font-family:Arial;background-color: #60c231;padding: 5px 5px 5px 5px;color: #fff;font-weight: bold;"/>
                    </div>          
                </div>
            </div>
            <div id="divInvoice" style=" padding:10px;">
                <table id="tblInvoice" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tbody><tr> 
                        <td> 
                            <div> 
                                <center> 
                                    <img id="imgLogo" src="https://ctb2cstage.cozmotravel.com:443/images/logo.jpg" alt="AgentLogo" style="border-width:0px;max-width: 159px;max-height: 51px;" /> </center> </div>
                            <div> 
                                <table width="100%" style=" font-family:Arial;"> 
                                    <tbody><tr> 
                                        <td> 
                                            <center>  
                                                <h1 style=" font-size:26px">Invoice</h1>                                                
                                                 TRN: <span id="invoiceNo"></span>                                                
                                            </center>
                                            <%--<input name="btnEmail" type="button" id="btnEmail" style=" float:right; margin-top:-26px;" onclick="return ShowPopUp();" value="Email Invoice" />--%>
                                            <input name="btnPrint" type="button" id="btnPrint" style=" float:right; margin-top:-26px;margin-right: 150px;" onclick="return printPage();" value="Print Invoice" />
                                        </td>  
                                    </tr>  
                                </tbody></table>  
                            </div>  
                            <table width="100%"> 
                                <tbody><tr> 
                                    <td width="50%" valign="top"> 
                                        <h3 style="font-size:16px; font-family:Arial; background-color:#60c231; padding: 7px 10px 7px 10px; color:#fff;">Client Details</h3>
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tbody>                                        
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Agency Name: </b></td>
                                                    <td style="vertical-align: top;" id="AgencyName"></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Agency Code: </b></td>
                                                    <td style="vertical-align: top;" id="AgenctCode"> </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Agency Address: </b></td>
                                                    <td style="vertical-align: top;" id="AgencyAddress"></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>TelePhone No: </b></td>
                                                    <td style="vertical-align: top;" >
                                                            
                                                         <span></span>   Phone:
                                                         <span id="AgencyNo"></span>  
                                                            
                                                         <span> Fax:</span>
                                                         <span id="AgencyFax">6454788</span>                                                                         
                                                    </td>
                                                </tr>
                                                
                                                <tr>
                                                    <td style="vertical-align: middle; width: 120px;"><b>TRN Number: </b></td>
                                                    <td style="vertical-align: middle;" id="TrnNo"></td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="50%" valign="top"> 
                                        <h3 style="font-size:16px; font-family:Arial; background-color:#60c231; padding: 7px 10px 7px 10px; color:#fff;">Invoice Details</h3>
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tbody>                                        
                                                <tr>
                                                    <td style="vertical-align: top; width: 120px;"><b>Invoice No: </b></td>
                                                    <td style="vertical-align: top;" id="InvoiceNo"></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Invoice Date: </b></td>
                                                    <td style="vertical-align: top;" id="invoiceDate"></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Confirmation No: </b></td>
                                                    <td style="vertical-align: top;" id="ConfirmationNo"></td>
                                                </tr>
                                                <tr>
                                                     
                                                    <td style="vertical-align: top;width: 240px;"><b><span id="AgenctTitle"></span> Vocher No: </b></td>
                                                    <td style="vertical-align: top;" id="VoucherNo"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr> 
                                    <td width="50%" valign="top"> 
                                        <h3 style="font-size:16px; font-family:Arial; background-color:#60c231; padding: 7px 10px 7px 10px; color:#fff;"> Booking Details</h3>
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;" id="passInfoTable"> 
                                            <tbody>                                
                                                <tr>
                                                    <td style="vertical-align: top;"><b>Booking Date: </b></td>
                                                    <td style="vertical-align: top;" id="BookingDate"></td>
                                                </tr>                             
                                                <tr>
                                                    <td style="vertical-align: top;"><b>Transfer In: </b></td>
                                                    <td style="vertical-align: top;" id="TransferDate"></td>                                 
                                                    
                                                </tr>                                
                                                <tr>
                                                    
                                                    <td style="vertical-align: top;"><b>No. of Passengers(s): </b></td>
                                                    <td style="vertical-align: top;" id="NoofPassengers"></td>
                                                    
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="50%" valign="top"> 
                                        <h3 style="font-size:16px; font-family:Arial; background-color:#60c231; padding: 7px 10px 7px 10px; color:#fff;"> Transfer Details </h3>
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tbody>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Vehicle Name: </b></td>
                                                    <td style="vertical-align: top;" id="VehicleName"> </td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>PickUp Place: </b></td>
                                                    <td style="vertical-align: top;" id="PickUpPlace"></td>
                                                </tr>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Drop Off Place: </b></td>
                                                    <td style="vertical-align: top;" id="DropOffPlace"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr> 
                                    <td width="50%" valign="top"> 
                                        <h3 style="font-size:16px; font-family:Arial; background-color:#60c231; padding: 7px 10px 7px 10px; color:#fff;"> Lead Passenger Details </h3>
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tbody>
                                                <tr>
                                                    <td style="vertical-align: top;width: 120px;"><b>Pax Name: </b></td>
                                                    <td style="vertical-align: top;" id="PaxName"> </td>
                                                </tr>
                                                
                                            </tbody>
                                        </table>
                                    </td>
                                    <td width="50%" valign="top"> 
                                        <h3 style="font-size:16px; font-family:Arial; background-color:#60c231; padding: 7px 10px 7px 10px; color:#fff;">Rate Details </h3>
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tbody>
                                                                                                                                  
                                                <tr>
                                                    <td style="vertical-align: top; width: 120px;"><b>Net Amount:</b></td>
                                                    <td style="vertical-align: top;">
                                                       <span class="currency">AED</span> 
                                                        <span id="NetAmount"></span>
                                                        
                                        
                                                    </td>
                                                </tr>    
                                                <%--<tr>
                                                    
                                                    <td style="vertical-align: top; width: 120px;"><b>Markup: </b></td>
                                                    <td style="vertical-align: top;">
                                                        <span class="currency">AED</span> 
                                                        <span id="markUp"></span>
                                                    </td>
                                                    
                                                </tr>--%>
                                                <tr class="discountRow">
                                                    
                                                    <td style="vertical-align: top; width: 120px;"><b>Discount: </b></td>
                                                    <td style="vertical-align: top;">
                                                        <span class="currency">AED</span> 
                                                        <span id="discount"></span>
                                                    </td>
                                                    
                                                </tr>
                                                                    
                                               
                                                <tr  class="isgst">
                                                    <td style="vertical-align: top;width: 120px;"><b>GST: </b></td>
                                                    <td style="vertical-align: top;">
                                                        <span class="currency">AED</span> 
                                                        <span id="cgstAmount">0</span>
                                                    </td>
                                                </tr>    
                                               
                                                <tr  class="isvat">
                                                    <td style="vertical-align: top;width: 120px;"><b>Vat: </b></td>
                                                    <td style="vertical-align: top;">
                                                        <span class="currency">AED</span> 
                                                        <span id="OutputVatAmount">0</span>
                                                    </td>
                                                </tr>    
                                                <tr >
                                                    <td style="vertical-align: top;width: 120px;"><b>Gross Amount:  </b></td>
                                                    <td style="vertical-align: top;">
                                                        <span class="currency">AED</span> 
                                                        <span id="grossAmount"></span>
                                                    </td>
                                                </tr>  
                                                
                                                                     
                                              <tr>
                                                <td style="vertical-align: top;width: 120px;"><b>Total Amount: </b></td>
                                                <td style="vertical-align: top;" >
                                                   <span class="currency">AED</span> 
                                                   <span id="TotalAmount"></span>
                                                </td>
                                              </tr>                      
                                            </tbody>
                                          </table>
                                    </td>
                                </tr>
                                <tr> 
                                    <td width="50%" valign="top"> 
                                        <table width="100%" style=" font-family:Arial; font-size:12px; line-height:24px;"> 
                                            <tbody><tr>
                                                
                                                <td style="vertical-align: top; width: 120px;"><b>invoiced By: </b></td>
                                                 <td style="vertical-align: top;" align="left" id="InvoiceByAgent"></td>
                                            </tr>   
                                            <tr>
                                                <td style="vertical-align: top; width: 120px;"><b>Created By: </b></td>
                                                <td style="vertical-align: top;" align="left" id="CreatedBy"></td>
                                            </tr>
                                            <tr>
                                                <td style="vertical-align: top; width: 120px;"><b>Location: </b></td>
                                                <td style="vertical-align: top;" align="left" id="locationName"></td>
                                            </tr>
                                            <tr>
                                                <td style="width:120px">
                                                    <div style="display:none"><img src="images/Email1.gif" />
                                                        <a href="javascript:ShowEmailDivD()">Email Invoice</a>
                                                    </div>
                                                </td>
                                                <td style="width:160px"><span id="LowerEmailSpan" style="margin-left: 15px;"></span> </td>
                                                <td>
                                                    <div style="display:none;" id="emailSent">
                                                        <span style="float:left;width:100%;margin:auto;text-align:center;">
                                                            <span id="messageText" style="background-color:Yellow" ;="">Email sent successfully</span>
                                                        </span>
                                                    </div>
                                                </td>
                                            </tr>
                                        </tbody></table>
                                    </td>
                                    <td width="50%" valign="top"></td>
                                </tr>
                            </tbody></table>
                        </td>
                    </tr>
                </tbody>

                </table>
            </div>
    </form>
    <input type="hidden" id="hdnUserId" runat="server" />
    <input type="hidden" id="hdnAgentId" runat="server" />
    <input type="hidden" id="hdnBehalfLocation" runat="server" />        
    <input type="hidden" id="hdnSession" runat="server" />
    <script type="text/javascript">
        
        var AgentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
        var UserId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
        var BehalfLocation = JSON.parse(document.getElementById('<%=hdnBehalfLocation.ClientID %>').value);
        var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["TransferWebApiUrl"]%>';
        var agentImage = "/"+'<%=System.Configuration.ConfigurationManager.AppSettings["AgentImage"].ToString().Replace("\\","/")%>';
   
    </script>
    <script type="text/javascript" src="scripts/Transfers/TransferInvoiceCreation.js"></script>
</body>
</html>
