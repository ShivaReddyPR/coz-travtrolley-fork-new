<%@ Page Language="C#" AutoEventWireup="true" Inherits="RosterUploadCtrlUI" Codebehind="RosterUploadCtrl.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme %>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme %>://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
<link href="css/Default.css" rel="stylesheet" type="text/css" />
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
<table style=" font-family:Arial; font-size:12px;">
<tr>
<td align="right" style="display:none"><asp:Label ID="lblAppliedThru" runat="server" Text="Applied Thru:"></asp:Label></td>
                <td style="display:none"><asp:DropDownList ID="ddlAppliedThrue" height="20px"  runat="server" Width="153px">
                <%--<asp:ListItem Text="--Select Applied Thru--" value="N"></asp:ListItem>--%>
                <asp:ListItem Text="shj_img" value="shj_img"></asp:ListItem>
                <asp:ListItem Text="dxb_img" value="dxb_img"></asp:ListItem>
                </asp:DropDownList></td>
<td>
<span style="color:Red; font-size:12px; font-family:arial">*</span>Attach Excel file:
</td>
<td>
<asp:FileUpload ID="fileuploadExcel" runat="server" />
</td>
 <td>
 <div id="divPlsWait" runat="server" style="top:10px;right:80px"  align="center" class="divPleaseWait">
                        <asp:Label ID="lblPlsWait" style="font-size:16px;font-weight:bold" runat="server" Text="Please wait........."></asp:Label>
                    </div>
    <asp:Button ID="btnSend" OnClientClick="return Validate();" runat="server" Text="Export" onclick="btnSend_Click"  />
    </td>
    <td>
    <asp:Button ID="btnClear" OnClientClick="return Clear();" runat="server" Text="Clear"  />
    </td>
    <td><asp:TextBox ID="txtDocNumber" runat="server" Text=""></asp:TextBox></td>
    <td><asp:HiddenField ID="hdfDocNumber" runat="server" Value=""></asp:HiddenField></td>
</tr>

</table>
    </form>
    <script type="text/javascript" >
        function Validate() {
            //alert(document.getElementById('fileuploadExcel'));
            //alert(document.getElementById('divPlsWait'));
            if(document.getElementById('fileuploadExcel').value=='')
            {
                alert('Attach Excel File cannot be Blank ! ');
                return false;
            }
            document.getElementById('divPlsWait').style.display = 'block';
            //return true;
        }

        function Clear() {
           
           document.getElementById('txtDocNumber').value='';
            document.getElementById('hdfDocNumber').value='';
                return false;
            }
           
    </script>
</body>

</html>
