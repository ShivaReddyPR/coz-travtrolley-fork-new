<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CreateUserUI" Title="Create User" Codebehind="CreateUser.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
 <style>
     .select2-container-multi.no-select2{display:none!important}
 </style>
                   
     
     
     <div class="body_container">    
                   
                        <asp:HiddenField runat="server" id="hdfMode" value="0" ></asp:HiddenField> 
                        <asp:HiddenField runat="server" id="hdfEncriPwd" value="" ></asp:HiddenField>  
                        <asp:HiddenField runat="server" id="hdfAgentCode" value="" ></asp:HiddenField>  
                        <asp:HiddenField runat="server" id="hdfLoginName" value="" ></asp:HiddenField>  
                        
                        
                        
                        <div class="paramcon"> 
                             <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblFirstName" runat="server" Text="First Name:"></asp:Label></div>
    <div class="col-md-2"> <asp:TextBox ID="txtFirstName" CssClass="inputEnabled form-control" runat="server"></asp:TextBox></div>
    <div class="col-md-2"> <asp:Label ID="lblLastName" runat="server" Text="Last Name:"></asp:Label></div>
     <div class="col-md-2"><asp:TextBox ID="txtLastName" CssClass="inputEnabled form-control" runat="server"></asp:TextBox> </div>
   
     <div class="col-md-2"><asp:Label ID="lblEmailId" runat="server" Text="Email ID:"></asp:Label> </div>
         <div class="col-md-2"> <asp:TextBox ID="txtEmailID" CssClass="inputEnabled form-control" runat="server"></asp:TextBox></div>

<div class="clearfix"></div>
    </div>




     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"><asp:Label ID="lblLoginName" runat="server" Text="Login Name:"></asp:Label> </div>
   
    <div class="col-md-2">
    
    <table> 
    
    <tr> 
    
    <td><asp:TextBox ID="txtLoginName"  CssClass="inputEnabled form-control" runat="server"></asp:TextBox> </td>
     <td><asp:TextBox ID="txtLoginSuffix" Enabled="false" CssClass="inputDisabled form-control" runat="server" Width="50px"></asp:TextBox> </td>
    
    </tr>
    
    </table>
    
    
    
    
    
     </div>
  
    <div class="col-md-2"> <asp:Label ID="lblPassword" runat="server" Text="Password:"></asp:Label></div>
    
     <div class="col-md-2"> <asp:TextBox ID="txtPassword" CssClass="inputEnabled form-control" EnableViewState="true" runat="server" TextMode="password"></asp:TextBox></div>
     
     
     <div class="col-md-2"><asp:Label ID="lblConfirmPassword" runat="server" Text="Confirm Password:"></asp:Label> </div>
     
         <div class="col-md-2"><asp:TextBox ID="txtConfirmPassword" CssClass="inputEnabled form-control" Onchange="return CheckPassword();" runat="server" Width="150px" TextMode="password"></asp:TextBox> </div>

<div class="clearfix"></div>
    </div>


     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblMemberType" runat="server" Text="Member Type:"></asp:Label></div>
    <div class="col-md-2"><asp:DropDownList ID="ddlMemberType"  CssClass="inputDdlEnabled form-control" runat="server"></asp:DropDownList> </div>
     <div class="col-md-2"><asp:Label ID="lblAgent" runat="server" Text="Agent:"></asp:Label> </div>
         <div class="col-md-2"><asp:DropDownList ID="ddlAgent"  CssClass="inputDdlEnabled form-control" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged"  AutoPostBack="true" onchange="setLoginSuffix(this.id);" runat="server"></asp:DropDownList> </div>
         <div class="col-md-2"><asp:Label ID="lblLocation" runat="server" Text="Location:"></asp:Label> </div>
     <div class="col-md-2"> <asp:DropDownList ID="ddlLocation"  CssClass="inputDdlEnabled form-control" runat="server"></asp:DropDownList></div>

<div class="clearfix"></div>
    </div>



     



     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblSubAgent" runat="server" Text="Sub Agent:" Enabled=true></asp:Label></div>
 
    <div class="col-md-2"> <asp:DropDownList CssClass="form-control" ID="ddlSubAgent" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSubAgent_SelectedIndexChanged" Enabled=true>
                         
                         </asp:DropDownList></div>
                         
                         
                         
    <div class="col-md-2"> <asp:Label ID="lblTransType" runat="server" Text="TransType:"></asp:Label></div>
    
     <div class="col-md-2"><asp:DropDownList ID="ddlTransType" runat="server" CssClass="inputEnabled form-control">
                     <asp:ListItem Text="B2B" Value="B2B" Selected="True"></asp:ListItem>
                     <asp:ListItem Text="B2C" Value="B2C"></asp:ListItem>
                     <asp:ListItem Text="BOTH" Value="B2B,B2C"></asp:ListItem>
                     </asp:DropDownList> </div>
                     
     <div class="col-md-2"> <asp:Label ID="lblMobNo" runat="server" Text="Mobile No:" Enabled=true></asp:Label></div>
         <div class="col-md-2"> <asp:TextBox ID="txtMobNo" CssClass="inputEnabled form-control" runat="server"></asp:TextBox></div>

<div class="clearfix"></div>
    </div>


<div class="col-md-12 padding-0 marbot_8">                                      
   
 <div class="col-md-2"><asp:Label ID="lblAddress" runat="server" Text="Address:"></asp:Label> </div>
    <div class="col-md-6"> <asp:TextBox ID="txtAddress"  runat="server" TextMode="multiline" Height="45" CssClass="inputEnabled" ></asp:TextBox>

    </div>

    
    <div class="col-md-2"><asp:Label ID="Label1" runat="server" Text="Special Access:"></asp:Label> </div>
    <div class="col-md-2">
        <asp:ListBox ID="lstUserAccess" CssClass="no-select2"  runat="server" SelectionMode="Multiple"></asp:ListBox>
        
        </div>


   <div class="clearfix"></div>
    



                        
     </div>   
							
							<div class="col-md-12 padding-0 marbot_8">
								 <div class="col-md-2"> <asp:Label ID="lblUserCode" runat="server" Text="User Code:" Enabled=true></asp:Label></div>
         <div class="col-md-2"> <asp:TextBox ID="txtCUserCode" CssClass="inputEnabled form-control" runat="server"></asp:TextBox></div>
								<div class="clearfix"></div>
								</div>
                        
                                         
                         
              <center> 
                 
                         
 <table cellpadding="0" style="border:solid 0px;" border="0" cellspacing="0" class="content2 label">
                          
                       
                    
           <tr> 
           
           
           <td> 
           
           
           <div style="height:250px;width:265px; border:solid 1px" class="grdScrlTrans" >
             
          <asp:GridView ID="gvUserRoleDetails" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="Role_id" 
            EmptyDataText="No Role Details!" AutoGenerateColumns="false" PageSize="10" GridLines="none"  CssClass="grdTable"
            CellPadding="1" CellSpacing="0"
            OnPageIndexChanging="gvUserRoleDetails_PageIndexChanging" >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label style="color:Black">Select</label>
    <asp:CheckBox runat="server" id="HTchkSelectAll" AutoPostbACK="true" OnCheckedChanged="HTchkSelectAll_CheckedChanged"  ></asp:CheckBox>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" AutoPostbACK="true"   OnCheckedChanged="ITchkSelect_CheckedChanged" CssClass="InputEnabled" Checked='<%# Eval("urd_status").ToString()=="A"%>' ></asp:CheckBox>
    <%--<asp:Label ID="ITlblRemarks" runat="server" Text='<%# Eval("vs_remarks") %>' CssClass="label grdof"  ToolTip='<%# Eval("vs_remarks") %>' Width="150px"></asp:Label>--%>
    </ItemTemplate>    
    </asp:TemplateField>   
    
    
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    
    <label style=" float:left"><cc1:Filter ID="HTtxtRoleName" Width="150px" HeaderText="Role Name" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />   </label>              
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblRolName" runat="server" Text='<%# Eval("Role_name") %>' CssClass="label grdof" ToolTip='<%# Eval("Role_name") %>' Width="150px"></asp:Label>
    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfPaxId" runat="server" Value='<%# Bind("pax_id") %>'></asp:HiddenField>--%>
    </ItemTemplate>    
    
    </asp:TemplateField>  
    
   <%-- <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc1:Filter ID="HTtxtParentMenu" Width="150px" HeaderText="ParentMenu" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblParentMenu" runat="server" Text='<%# Eval("ParentMenu") %>' CssClass="label grdof" ToolTip='<%# Eval("ParentMenu") %>' Width="150px"></asp:Label>
    </ItemTemplate>    
    
    </asp:TemplateField>--%>  

          
    </Columns>           
    </asp:GridView>
    
    
                </div>
                <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
           </td>
           
           </tr>                  
   
                       
                            
                     
      
       
                         </table> 
   
   </center>
   
        <center> 
   
   
   <asp:Button ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();"
                              CssClass="but but_b"  OnClick ="btnSave_Click" ></asp:Button>
                              
                    <asp:Button ID="btnCancel" Text="Clear" runat="server" CssClass="but but_b"  OnClick="btnCancel_Click"></asp:Button>
                    
                    <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="but but_b"   OnClick="btnSearch_Click"></asp:Button>
   </center>           
                         
                         
                         </div>                                 
         <script src="scripts/Jquery/bootstrap-multiselect.js"></script>
         <link href="css/bootstrap-multiselect.css" rel="stylesheet" />
                     
<script type="text/javascript">
    var apiStatus = false;
function CheckPassword()
{
    
      if(getElement('txtPassword').value != getElement('txtConfirmPassword').value)  addMessage('Password Mismatch!','');
      if(getMessage()!=''){ 
    alert(getMessage()); clearMessage(); return false;}
}


function Save()
    { 
           // ShowMessageDialog('testing', 'testing details','Severe')
    

    if(getElement('txtFirstName').value =='') addMessage('First Name cannot be blank!','');
    if(getElement('txtLastName').value =='') addMessage('Last Name cannot be blank!','');
    if(getElement('txtEmailID').value =='') addMessage('Email Id cannot be blank!','');
    else if(!checkEmail(getElement('txtEmailID').value)) addMessage('Email is not valid!','');
    if(getElement('txtLoginName').value =='') addMessage('Login Name cannot be blank!','');
    if(getElement('hdfMode').value =='0') 
    {
        if(getElement('txtPassword').value =='') addMessage('Password Name cannot be blank!','');
        if(getElement('txtConfirmPassword').value =='') addMessage('Confirm Password Name cannot be blank!','');
    }
    if(getElement('ddlMemberType').selectedIndex <=0 ) addMessage('Please select Member Type from the List!','');
    if(getElement('ddlLocation').selectedIndex <=0 ) addMessage('Please select Location from the List!','');
    if(getElement('ddlAgent').selectedIndex <=0 ) addMessage('Please select Agent from the List!','');
   
    if (getElement('ddlMemberType').value == "VMSUSER" && getElement('hdfMode').value == '0')  {
        if (getMessage() == '')
        {
            if (APISave())                
                return true                            
            else  //if the call response is failure or error ,  no server cals
                return false    
        }
    }
   
    if(getMessage()!=''){ 
    alert(getMessage()); clearMessage(); return false;}
    }
    
     function setLoginSuffix(id)
    {   
        var ddlAgent=document.getElementById(id);
        if(ddlAgent.selectedIndex>0 && getElement('hdfMode').value =='0' )
        {
            var suffix=ddlAgent.options[ddlAgent.selectedIndex].value;
            //alert(suffix);
            getElement('txtLoginSuffix').value=suffix;
        }
    }
   
    //function GetEncriPwd(value) {
    //    PageMethods.EncriPWD(value, OnSuccess, OnFailure);
    //}
    //function OnSuccess(result) {
    //    alert(result)
    //    getElement('hdfEncriPwd').value = result;

    //}

      function APISave() {
             
            return true;
    }
//    function checkEmail(inputvalue){	
//    var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
//    if(pattern.test(inputvalue)){         
//		return true;
//    }else{   
//		return false;
//    }
//}


</script>
<script type="text/javascript">

    $(document).ready(function () {
        $('#ctl00_cphTransaction_lstUserAccess').multiselect({
                includeSelectAllOption: true
            });
    });
    var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            $('.no-select2').select('destroy');
            $('#ctl00_cphTransaction_lstUserAccess').multiselect({
                includeSelectAllOption: true
            });
            $('.no-select2').select('destroy');
        });
</script>
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
<asp:GridView ID="gvUsers" runat="server" AutoGenerateColumns="False" Width="100%"
        AllowPaging="true" PageSize="13" DataKeyNames="USER_ID" CellPadding="4" 
        CellSpacing="0" GridLines="none" OnPageIndexChanging="gvUsers_PageIndexChanging" 
        OnRowDeleting="gvUsers_RowDeleting"  OnSelectedIndexChanged="gvUsers_SelectedIndexChanged"  >
        
         <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
        <Columns>
        <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
           <%-- <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtID" runat="server"  OnClick="FilterSearch_Click" HeaderText="User ID"
                        Width="70px"  />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblID" CssClass="label grdof" runat="server" Text='<%# Eval("USER_ID") %>' Width="70px"></asp:Label>
                </ItemTemplate>                                            
            </asp:TemplateField>--%>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtFirstName" CssClass="inputenabled" runat="server" OnClick="FilterSearch_Click" HeaderText="First Name"
                        Width="120px" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblFirstName" CssClass="label grdof" ToolTip='<%# Eval("USER_FIRST_NAME") %>'  runat="server" Text='<%# Eval("USER_FIRST_NAME") %>' Width="120px"></asp:Label>
                </ItemTemplate>                                            
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtLastName" runat="server" OnClick="FilterSearch_Click" HeaderText="Last Name"
                        Width="120px" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblLastName" runat="server" Text='<%# Eval("USER_LAST_NAME") %>' CssClass="label grdof"  ToolTip='<%# Eval("USER_FIRST_NAME") %>'  Width="120px"></asp:Label>
                </ItemTemplate>                                            
            </asp:TemplateField>
           
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtLoginName" runat="server" OnClick="FilterSearch_Click" HeaderText="Login Name"
                        Width="120px" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblLoginName" runat="server" Text='<%# Eval("USER_LOGIN_NAME") %>' CssClass="label grdof" ToolTip='<%# Eval("USER_LOGIN_NAME") %>'  Width="120px"></asp:Label>
                </ItemTemplate>                                            
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtEmail" runat="server" OnClick="FilterSearch_Click" HeaderText="Email Id"
                        Width="120px" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblEmail"  runat="server" Text='<%# Eval("USER_EMAIL") %>' CssClass="label grdof" ToolTip='<%# Eval("USER_EMAIL") %>'    Width="120px"></asp:Label>
                </ItemTemplate>                                            
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtMemberType" runat="server" OnClick="FilterSearch_Click" HeaderText="Member Type"
                        Width="120px" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblMemberType" runat="server" Text='<%# Eval("USER_MEMBER_TYPE_NAME") %>' CssClass="label grdof" ToolTip='<%# Eval("USER_MEMBER_TYPE_NAME") %>'  Width="120px"></asp:Label>
                </ItemTemplate>                                            
            </asp:TemplateField>
             <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtLocation" runat="server" OnClick="FilterSearch_Click" HeaderText="Location"
                        Width="120px" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblLocation" runat="server" Text='<%# Eval("LOCATION_NAME") %>'  CssClass="label grdof" ToolTip='<%# Eval("LOCATION_NAME") %>'  Width="120px"></asp:Label>
                </ItemTemplate>                                            
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtAgent" runat="server" OnClick="FilterSearch_Click" HeaderText="Company"
                        Width="120px" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblAgent" runat="server" Text='<%# Eval("AGENT_NAME") %>'  CssClass="label grdof" ToolTip='<%# Eval("AGENT_NAME") %>'  Width="120px"></asp:Label>
                </ItemTemplate>                                            
            </asp:TemplateField>
             <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtAddress" runat="server" OnClick="FilterSearch_Click" HeaderText="Address"
                        Width="120px" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblAddress" runat="server" Text='<%# Eval("USER_ADDRESS") %>' CssClass="label grdof" ToolTip='<%# Eval("USER_ADDRESS") %>'  Width="120px"></asp:Label>
                </ItemTemplate>                                            
            </asp:TemplateField>  
            
              <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtStatus" runat="server" OnClick="FilterSearch_Click" HeaderText="Status"
                        Width="120px" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("USER_STATUS") %>' CssClass="label grdof" ToolTip='<%# Eval("USER_STATUS") %>'  Width="120px"></asp:Label>
                </ItemTemplate>                                            
            </asp:TemplateField>  
			 <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtUserCode" runat="server" OnClick="FilterSearch_Click" HeaderText="USerCode"
                        Width="120px" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblUserCode" runat="server" Text='<%# Eval("user_code") %>' CssClass="label grdof" ToolTip='<%# Eval("user_code") %>'  Width="120px"></asp:Label>
                </ItemTemplate>                                            
            </asp:TemplateField>
            <asp:TemplateField>
            <HeaderTemplate><asp:Label runat=server ID="id" width="90px" ></asp:Label></HeaderTemplate>
            <ItemTemplate>
                <%--<asp:LinkButton ID="lnkEdit" Syle="font-size:20px;" runat="server" Text="Edit" Width="60px" >
                </asp:LinkButton>--%>
                <%--<asp:LinkButton ID="lnkDelete" runat="server" Width="60px" OnClientClick="return confirm('Do you want to delete the user?');" Text="Delete" CommandName="Delete" ></asp:LinkButton>--%>
                <asp:ImageButton ID = "ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the user?');" ></asp:ImageButton>
            </ItemTemplate>                                        
            
            </asp:TemplateField>
        </Columns>
        <HeaderStyle CssClass="gvHeader"></HeaderStyle>
        <RowStyle CssClass="gvRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvAlternateRow" />                                    
    </asp:GridView>
</asp:Content>


