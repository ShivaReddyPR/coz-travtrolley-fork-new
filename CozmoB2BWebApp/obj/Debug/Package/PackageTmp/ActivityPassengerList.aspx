<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ActivityPassengerListGUI" Title="Activity Passengers List" Codebehind="ActivityPassengerList.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="js/VisaDefault.js"></script>

    <script type="text/javascript" src="js/VisaPassengerDetail.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    
   

    <script type="text/javascript">
        var cal1;
        var cal2;
        function init() {
            var dt = new Date();
            cal1 = new YAHOO.widget.CalendarGroup("cal1", "container1");
            cal1.cfg.setProperty("minDate", dt.getMonth() + 1 + "/" + dt.getDate() + "/" + dt.getUTCFullYear());
            cal1.cfg.setProperty("pagedate", dt.getMonth() + 1 + "/" + dt.getUTCFullYear());
           // cal1.cfg.setProperty("title", "Select your desired checkin date:");
            cal1.cfg.setProperty("close", true);
            cal1.cfg.setProperty("iframe", true);
            cal1.selectEvent.subscribe(setDate1);
            cal1.render();
            //            cal2 = new YAHOO.widget.CalendarGroup("cal2", "container2");
            //            cal2.cfg.setProperty("title", "Select your desired checkout date:");
            //            cal2.selectEvent.subscribe(setDate2);
            //            cal2.cfg.setProperty("close", true);
            //            cal2.render();
            //load();
        }

        function showCalendar1() {
            //cal2.hide();
            document.getElementById('container1').style.display = "block";


            document.getElementById('Outcontainer1').style.display = "block";
            //document.getElementById('Outcontainer2').style.display = "none";
        }

        //        var departureDate = new Date();
        //        function showCalendar2() {
        //            cal1.hide();
        //            var date1 = document.getElementById('ctl00$BookingBlock$dlFlexList$ctl00$dcFlexField1').value;

        //            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
        //                var depDateArray = date1.split('/');
        //                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);
        //                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
        //                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
        //                cal2.render();
        //            }
        //            document.getElementById('container2').style.display = "block";


        //            document.getElementById('Outcontainer2').style.display = "block";
        //            document.getElementById('Outcontainer1').style.display = "none";
        //        }
        function setDate1() {
            var date1 = cal1.getSelectedDates()[0];
            var dt = new Date();
            this.today = new Date(dt.getUTCFullYear(), dt.getMonth() - 1, dt.getDate());
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();
            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());
            if (difference < 0) {
                document.getElementById('errMessHotel').style.visibility = "visible";
                document.getElementById('errMessHotel').innerHTML = "Please select correct checkin date. ";
                return false;
            }
            var month = date1.getMonth() + 1;
            var day = date1.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=dateControlId %>').value = day + "/" + (month) + "/" + date1.getFullYear();
            cal1.hide();
            document.getElementById('Outcontainer1').style.display = "none";
        }


        //        function setDate2() {
        //            var date1 = document.getElementById('checkInDate').value;
        //            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
        //                document.getElementById('errMessHotel').style.visibility = "visible";
        //                document.getElementById('errMessHotel').innerHTML = "First select checkin date.";
        //                return false;
        //            }

        //            var date2 = cal2.getSelectedDates()[0];
        //            var depDateArray = date1.split('/');

        //            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
        //                document.getElementById('errMessHotel').style.visibility = "visible";
        //                document.getElementById('errMessHotel').innerHTML = " Invalid checkin date";
        //                return false;
        //            }

        //            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        //            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
        //            var difference = returndate.getTime() - depdate.getTime();

        //            if (difference < 1) {
        //                document.getElementById('errMessHotel').style.visibility = "visible";
        //                document.getElementById('errMessHotel').innerHTML = "Checkout date should be greater than or equal to checkin date (" + date1 + ")";
        //                return false;
        //            }

        //            var month = date2.getMonth() + 1;
        //            var day = date2.getDate();

        //            if (month.toString().length == 1) {
        //                month = "0" + month;
        //            }

        //            if (day.toString().length == 1) {
        //                day = "0" + day;
        //            }

        //            document.getElementById('checkOutDate').value = day + "/" + month + "/" + date2.getFullYear();
        //            cal2.hide();
        //            document.getElementById('Outcontainer2').style.display = "none";

        //        }
        YAHOO.util.Event.addListener(window, "load", init);
    
    </script>

    
    <div class="padding_top_10"> 
    
    <div class="ns-h3">
                    <asp:Label ID="lblName" runat="server" Text=""></asp:Label></div>
            <div class="wraps0ppp">
            
            
            <table> 
            
            <td> <asp:Image ID="imgActivity" runat="server" style="width:120px; height:70px"  /></td>
            
            <td><p class=" pad_left10">
                                    <strong>Location :</strong>
                                    <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label><br>
                                    <strong>Duration :</strong>
                                    <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label>
                                    <strong>Booked for Date :</strong>
                                    <asp:Label ID="lblBookingDate" runat="server" Text=""></asp:Label></p> </td>
            
            
            </table>
            
            
            
            
                
            </div>
    
    </div>
    
    
    
    <div> 
    
    
    <asp:DataList ID="dlPaxPrice" runat="server" Width="100%">
                                <HeaderTemplate>
                                    <table class="datagrid fcol_fff" width="100%" border="1" cellspacing="0" cellpadding="5">
                                        <tr>
                                            <td class="themecol1" width="19%">
                                                <strong> Category</strong>
                                            </td>
                                            <td class="themecol1" width="12%">
                                               <strong>  Pax</strong>
                                            </td>
                                            <td class="themecol1" width="25%">
                                               <strong>  Rate</strong>
                                            </td>
                                            <td class="themecol1" width="19%" >
                                               <strong>  Total</strong>
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table class="datagrid" width="100%" border="1" cellspacing="0" cellpadding="5">
                                        <tr>
                                            <td class="bg_white" width="19%">
                                                <%#Eval("Label") %>
                                            </td>
                                            <td class="bg_white" width="12%">
                                                <%#Eval("Adults") %>
                                            </td>
                                            <td class="bg_white" width="25%">
                                            <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>
                                                <%#Eval("Price") %>
                                            </td>
                                            <td  class="bg_white" width="19%">
                                                <strong> <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>
                                                    <%#Eval("Amount") %></strong>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
    <table width="100%">
                                <tr>
                                   
                                <td width="19%">
                                        <b> <label style="font-size:small;">  &nbsp;</label></b>
                                    </td>
                                    
                                   <td width="12%">&nbsp; </td>
                                    
                                    
                                    <td width="25%" align="right"> &nbsp; <b>Total:</b> &nbsp;  </td>
                                     
                                  <td width="19%">
                                        <b> <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>&nbsp;</b><b><asp:Label ID="lblSumTotal" runat="server" style="font-size:small;"></asp:Label></b>
                                    </td>
                                    
                                </tr>
                            </table>
    </div>
    
    
    
    
    
    
    
        <div style=" position:relative; padding-top:10px">
        <div id="Outcontainer1" style="position: absolute; display: none; left: 263px; top: 281px;
        border: solid 1px #ccc; z-index: 200;">
        <a onclick="document.getElementById('Outcontainer1').style.display = 'none'">
            <img style="cursor: pointer; position: absolute; z-index: 300; top: -12px; right: -10px;"
                src="images/cross_icon.png" />
        </a>
        <div id="container1" style="border: 1px solid #ccc;">
        </div>
    </div>
         
        </div>
        
        
        
        <div class="ns-h3">Passenger Details</div>
                
                
                
        
        <div class="tour-border">
            
            <div style="padding: 0px 10px 0px 10px">
                <asp:DataList ID="dlPassengers" runat="server" Width="100%" OnItemDataBound="dlPassengers_DataBound">
                    <ItemTemplate>
                     <div class="bggray padding-5">
                            
                            <strong> <%#Eval("PaxType") %></strong>
                        </div>
                      
                      
                      <table> 
                      <tr> 
                      
                      <td>   First Name :<span class="red-color">*</span> </td>
                       <td> <label>
                                                    <asp:TextBox ID="txtFirstName" CssClass="inp_file" Width="180px" runat="server" CausesValidation="true"
                                                        ValidationGroup="flex" TabIndex="1"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Firstname"
                                                        ControlToValidate="txtFirstName" SetFocusOnError="true" InitialValue="" ValidationGroup="flex"></asp:RequiredFieldValidator>
                                                </label></td>
                       
                      
                        <td>   Last Name :<span class="red-color">*</span> </td>
                        
                        
                        
                         <td> <label>
                                                    <asp:TextBox ID="txtLastName" CssClass="inp_file" Width="180px" runat="server" CausesValidation="true"
                                                        ValidationGroup="flex" TabIndex="2"></asp:TextBox>
                                                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter Lastname"
                                                        ControlToValidate="txtLastName" SetFocusOnError="true" ValidationGroup="flex"></asp:RequiredFieldValidator>
                                                </label></td>
                                                
                                                
                                                
                                    <td>  E-mail Address :<span class="red-color">*</span> </td>
                       <td><asp:TextBox ID="txtEmail" CssClass="inp_file" Width="180px" runat="server" CausesValidation="true"
                                                    ValidationGroup="flex" TabIndex="3"></asp:TextBox>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Email"
                                                    ControlToValidate="txtEmail" SetFocusOnError="true" ValidationGroup="flex"></asp:RequiredFieldValidator>
                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email address"
                                                    ControlToValidate="txtEmail" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"
                                                    ValidationGroup="flex" SetFocusOnError="true"></asp:RegularExpressionValidator> </td>                           
                                                
                      
                      </tr>
                      
                      
                      
                      
                      <tr> 
                      
       
                       
                       
                       
                        <td> <asp:Label ID="Label1" runat="server" Text="Nationality :"></asp:Label><span class="red-color">*</span></td>
                      
                        <td> 
                        
                        <asp:DropDownList ID="ddlNationality" Width="180px" AppendDataBoundItems="true" CausesValidation="true"
                                                    ValidationGroup="flex" runat="server" TabIndex="4">
                                                    <asp:ListItem Selected="True" Text="Select Nationality" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Value="1" Text="India"></asp:ListItem>
                                                </asp:DropDownList>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Select Nationality"
                                                    ControlToValidate="ddlNationality" InitialValue="-1" SetFocusOnError="true" ValidationGroup="flex"></asp:RequiredFieldValidator>
                        
                        </td>
                         
                                     
                      <td> Phone Number :<span class="red-color">*</span> </td>
                       <td>  <asp:TextBox ID="txtCountryCode" CssClass="inp_file" Width="40px" CausesValidation="true"
                                                    ValidationGroup="flex" runat="server" MaxLength="5" TabIndex="5" onkeypress="return restrictNumeric(this.id,'1');"></asp:TextBox>
                                                <asp:TextBox ID="txtMobileNo" CssClass="inp_file" Width="135px" CausesValidation="true"
                                                    ValidationGroup="flex" runat="server" MaxLength="10" TabIndex="6" onkeypress="return restrictNumeric(this.id,'1');"></asp:TextBox><br />
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Code (91)"
                                                    ControlToValidate="txtCountryCode" SetFocusOnError="true" ValidationGroup="flex"
                                                    InitialValue=""></asp:RequiredFieldValidator>
                                                <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage=" Mobile Number"
                                                    ControlToValidate="txtMobileNo" SetFocusOnError="true" ValidationGroup="flex"
                                                    InitialValue=""></asp:RequiredFieldValidator></td>
                      
                      </tr>
                      
                      
                      
                      
                      
                      
                      
                      
                      
                      </table>
                      
                      
                      
                      
                      
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td width="53%">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td width="100">
                                             
                                            </td>
                                            <td>
                                                
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                               
                                            </td>
                                            <td>
                                                
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                                <td width="47%">
                                    
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                </asp:DataList>
            </div>
            <div class="clear">
            </div>
        </div>
        <div class="tour-border">
            <div class=" ns-h3">
                Additional Details</div>
            <div style="padding: 0px 10px 0px 10px">
                <asp:HiddenField ID="hdnDateControlId" runat="server" />
                <%--<asp:DataList ID="dlFlexList" runat="server" OnItemDataBound="dlFlexList_DataBound"
                    Width="100%">
                    <ItemTemplate>--%>
                        <table id="tblFlexFields" runat="server" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <%--<tr>
                                <td width="50%">
                                    <asp:HiddenField ID="hdnFlexId" runat="server" Value='<%#Eval("flexId") %>' />
                                    <strong>
                                        <asp:Label ID="lblFlexField1" runat="server" Text='<%#Eval("flexLabel") %>'></asp:Label></strong>
                                </td>
                                <td height="30" width="50%">
                                    <label>
                                        <asp:TextBox ID="txtFlexField1" runat="server" CssClass="inp_file" CausesValidation="true"
                                            ValidationGroup="flex" Visible="false" Width="180px"></asp:TextBox>
                                        <asp:TextBox ID="dcFlexField1" runat="server" Visible="false"></asp:TextBox>
                                        <%--<a href="javascript:void(null)" onclick="showCalendar1()"/><img src="images/call-cozmo.png"></a>
                                        <asp:HyperLink ID="hlCal1" NavigateUrl="javascript:void(null)" onclick="showCalendar1()"
                                            Visible="false" runat="server"><img src="images/call-cozmo.png"></asp:HyperLink>
                                        <asp:DropDownList ID="ddlFlexField1" AppendDataBoundItems="true" runat="server" Visible="false"
                                            Width="180px" CausesValidation="true" ValidationGroup="flex">
                                            <asp:ListItem Selected="True" Value="-1" Text="Select"></asp:ListItem>
                                        </asp:DropDownList>
                                        <asp:RequiredFieldValidator ID="rfvFlex1" runat="server" SetFocusOnError="true" ValidationGroup="flex"
                                            InitialValue=""></asp:RequiredFieldValidator>
                                    </label>
                                </td>
                            </tr>--%>
                        </table>
                   <%-- </ItemTemplate>
                </asp:DataList>--%>
            </div>
            <div class="clear">
            </div>
        </div>
        <div class=" f_R">
            <%--<asp:ImageButton ID="imgSubmit" ImageUrl="images/submit-gif.gif" runat="server" Width="88"
                Height="30" CausesValidation="true" ValidationGroup="flex" OnClick="imgSubmit_Click" />--%>
                
                <asp:LinkButton ID="imgSubmit" Text="Submit" CssClass="btn but_b"  runat="server" 
                 CausesValidation="true" ValidationGroup="flex" OnClick="imgSubmit_Click" />
                
                
        </div>
     
    <%--</form>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

