﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorpGVAcknowledgementGUI" Codebehind="CorpGVAcknowledgement.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

    <%if (visaSale != null)
        { %>
<div class="body_container"> 

<div class="col-md-6 col-md-offset-3">
<div class="well bg_white text-center"> 

<h2 class="text-success"> Thank You for the details !!</h2>

<h2 class=" martop_10">Your Reference is:- <span class="text-danger"> <%=visaSale.DocNumber %></span></h2>

<div class="martop_10">   Our Visa experts will review your details and get in touch with you in shortly.</div>

</div>

</div>

<div class="clearfix"> </div>
 </div>
    <%} %>

</asp:Content>



<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

