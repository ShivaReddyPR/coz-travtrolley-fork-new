﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpCategoriesMaster.aspx.cs" Inherits="CozmoB2BWebApp.ExpCategoriesMaster" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <link href="css/toastr.css" rel="stylesheet" type="text/css" />
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>

    <script>

        /* Global variables */
        var apiHost = ''; var cntrllerPath = 'api/expenseMasters';  var cntrlrPath = 'api/commonMasters';
        var apiAgentInfo = {}; var expRepId = 0; var selectedRows = [];
        var costCenterData = {};
        var flag = '';
        var EC_Id = 0;
        var agentdata = {}; var selectedProfile = 0;

        /* Page Load */
        $(document).ready(function () {
            /* Check query string to see if existing report needs to open */
            expRepId = '<%=Request.QueryString["ExpRepId"] == null ? "0" : Request.QueryString["ExpRepId"]%>';

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {
                alert('Session expired, please login once again.');
                return;
            }

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            /* to get screen dropdown list data */
            GetScreenData();

        });

        /* To get agent and login info */
        function GetAgentInfo() {
            try {
                var loginInfo = '<%=Settings.LoginInfo == null%>';
                if (loginInfo == true)
                    return apiAgentInfo;

                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {
            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';
            if (IsEmpty(apiHost))
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";
            return apiHost;
        }

        /* To get agent and cost center */
        function GetScreenData() {
            var reqData = { AgentInfo: apiAgentInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCategoriesScreenLoadData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);
        }

        /* To bind card master agent and cost center */
        function BindScreenData(screenData) {
            agentdata = {};
            if (!IsEmpty(screenData.dtAgents)) {

                agentdata = screenData.dtAgents;
                var options = agentdata.length != 1 ? GetddlOption('', '--Select Agent--') : '';
                $.each(agentdata, function (key, col) {

                    options += GetddlOption(col.agenT_ID, col.agenT_NAME);
                });
                $('#ddlAgentId').empty();
                $('#ddlAgentId').append(options);
                $("#ddlAgentId").select2("val", '');
                if (agentdata.length == 1) {

                    $('#ddlAgentId').attr('disabled', 'disabled');
                    $("#ddlAgentId").select2("val", agentdata[0].agenT_ID);
                }
            }
            if (!IsEmpty(screenData.dtCostCenter))
                BindCostCenterData(screenData.dtCostCenter);
        }

        /* To bind cost center data to costcenter control */
        function BindCostCenterData(ccData) {
            costCenterData = {};
            if (IsEmpty(ccData)) {
                $('#ddlCostCenter').empty();
                $("#ddlCostCenter").select2("val", '');
                return;
            }
            costCenterData = ccData;
            var options = costCenterData.length != 1 ? GetddlOption('', '--Select Cost Center--') : '';
            $.each(costCenterData, function (key, col) {

                options += GetddlOption(col.setupId, col.name);
            });
            $('#ddlCostCenter').empty();
            $('#ddlCostCenter').append(options);
            $("#ddlCostCenter").select2("val", '');
            if (costCenterData.length == 1) {
                $("#ddlCostCenter").select2("val", costCenterData[0].setupId);
            }
        }

        /* To set agent name and cost center on agent change */
        function AgentChange(agentId) {
            selectedProfile = agentId;
            if (selectedProfile > 0) {

                var reqData = { AgentInfo: apiAgentInfo, AgentId: parseInt(selectedProfile) };
                var apiUrl = apiHost.trimRight('/') + '/' + cntrlrPath.trimRight('/') + '/getCostCenterData';
                WebApiReq(apiUrl, 'POST', reqData, '', BindCostCenterData, null, null);
            }
            ClearInfo();
        }

        /* To prepare and get the categories entity list for selected rows */
        function GetSelectedRows() {
            selectedRows = GetSetSelectedRows();
            if (IsEmpty(selectedRows) || selectedRows.length == 0)
                return [];
            var expcvSelected = [];
            $.each(selectedRows, function (key, col) {
                
                var cInfo = ecInfoDetails.find(item => item.eC_ID == col);
                cInfo.eC_Status = false;
                cInfo.eC_ModifiedBy = apiAgentInfo.LoginUserId;
                expcvSelected = expcvSelected.concat(cInfo);
            });
            return expcvSelected;
        }
        
        /* To prepare and set categories info entity */
        function RefreshGrid(resp) {
            $.each(selectedRows, function (key, col) {

                var ecid = parseInt(col);
                ecInfoDetails = ecInfoDetails.filter(item => (item.eC_ID) !== ecid);
            });
            if (ecInfoDetails.length == 0) {
                BindCategoriesInfo(ecInfoDetails);
                return;
            }
            selectedRows = [];
            GetSetSelectedRows('set', selectedRows);
            GetSetDataEntity('set', ecInfoDetails);
            LoadData(GetSetCurrentPageNo('get', ''));
        }

        /* To Load expense categories info */
        function LoadCategoriesInfo() {
            var eC_AgentId = $("#ddlAgentId").val();
            if (eC_AgentId == "" || eC_AgentId == "--Select Agent--") {
                toastr.error('Please select agent');
                $('#ddlAgentId').parent().addClass('form-text-error');
                return false;
            }
            var categoriesInfo = { EC_AgentId: eC_AgentId }
            var reqData = {AgentInfo: apiAgentInfo, CategoriesInfo: categoriesInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCategories';
            WebApiReq(apiUrl, 'POST', reqData, '', BindCategoriesInfo, null, null);
        }

        /* To bind categories info to grid */
        function BindCategoriesInfo(ecInfo) {
            if (ecInfo.length == 0) {

                ShowError('no data available for selected agent.');
                ClearInfo();
                return;
            }
            ecInfoDetails = ecInfo;
            var gridProperties = GetGridPropsEntity();
            gridProperties.headerColumns = ('Code|Desc|Order|GLAccount').split('|');
            gridProperties.displayColumns = ('eC_Code|eC_Desc|eC_Order|eC_GLAccount').split('|');
            gridProperties.pKColumnNames = ('eC_ID').split('|');
            gridProperties.dataEntity = ecInfo;
            gridProperties.divGridId = 'CategoriesList';
            gridProperties.headerPaging = true;
            gridProperties.gridSelectedRows = selectedRows;
            gridProperties.selectType = 'M';
            gridProperties.recordsperpage = 5;

            EnablePagingGrid(gridProperties);
            $('#CList').show();
        }

        /* To save categories info */
        function Save() {
            var eC_AgentId = $("#ddlAgentId").val();
            var eC_CostCenter = $("#ddlCostCenter").val();
            var eC_Code = $("#EC_Code").val();
            var eC_Desc = $("#EC_Desc").val();
            var eC_Order = $("#EC_Order").val();            
            var eC_GLAccount = $("#EC_GLAccount").val();
            
            if ((eC_AgentId == "" || eC_AgentId == "--Select Agent--") || (eC_CostCenter == "" || eC_CostCenter == "--Select CostCenter--" || eC_CostCenter == null)|| (eC_Code == "" || eC_Code == "Enetr Code") || (eC_Desc == "" || eC_Desc == "Enter Desc"))
            {
                if (eC_AgentId == "" || eC_AgentId == "--Select Agent--") {
                    toastr.error('Please Select Agent');
                    $('#ddlAgentId').parent().addClass('form-text-error');
                }
                if (eC_CostCenter == "" || eC_CostCenter == "--Select CostCenter--"|| eC_CostCenter == null) {
                    toastr.error('Please Select CostCenter');
                    $('#ddlCostCenter').parent().addClass('form-text-error');
                }
                if (eC_Code == "" || eC_Code == "Enter Code") {
                    toastr.error('Please Enter Code');
                    $("#EC_Code").addClass('form-text-error');
                }
                if (eC_Desc == "" || eC_Desc == "Enter Desc") {
                    toastr.error('Please Enter Desc');
                    $("#EC_Desc").addClass('form-text-error');
                }                
                return false;
            }            

            var categoriesInfo = { EC_ID: EC_Id, EC_AgentId: eC_AgentId, EC_CostCenter: eC_CostCenter, EC_Code: eC_Code, EC_Desc: eC_Desc, EC_Order: eC_Order, EC_GLAccount: eC_GLAccount, EC_Status: true, EC_CreatedBy: apiAgentInfo.LoginUserId, EC_ModifiedBy: apiAgentInfo.LoginUserId }           
            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expRepId, CategoriesInfo: categoriesInfo, flag: "INSERT" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveCategories';
            WebApiReq(apiUrl, 'POST', reqData, '', StatusConfirm, null, null);            
        }

        /* To bind status of categories info */
        function StatusConfirm() {
           if (EC_Id > 0) {
                alert('record updated successfully!');             
            }
            else if (EC_Id == 0) {
                alert('record added successfully!');               
            }          
            ClearInfo();
            LoadCategoriesInfo();
        }

        /* To update selected categories info from the grid and update the status in data base */
        function EditCategoriesInfo() {
            var delCInfo = GetSelectedRows();
            if (IsEmpty(delCInfo) || delCInfo.length == 0) {
                ShowError('Please check any one of the row in below table');
                return;
            }
            if (IsEmpty(delCInfo) || delCInfo.length > 1) {
                ShowError('Please check one of the row in below table');
                return;
            }
            var ecDetails = ecInfoDetails.find(item => item.eC_ID == selectedRows[0]);
            EC_Id = ecDetails.eC_ID;
            $("#ddlAgentId").select2('val', ecDetails.eC_AgentId);
            $("#EC_Code").val(ecDetails.eC_Code);
            $("#EC_Desc").val(ecDetails.eC_Desc);
            $("#EC_Order").val(ecDetails.eC_Order);
            $("#ddlCostCenter").select2('val', ecDetails.eC_CostCenter);
            $("#EC_GLAccount").val(ecDetails.eC_GLAccount);
        }

         /* To delete selected rows from the grid and update the status in data base */
        function Delete() {
            var delCInfo = GetSelectedRows();
            if (IsEmpty(delCInfo) || delCInfo.length == 0) {

                ShowError('Please select record to delete');
                return;
            }          
            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expRepId, delCInfo, flag: "DELETE" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveCategories';
            WebApiReq(apiUrl, 'POST', reqData, '', RefreshGrid, null, null);
        }

        /* To clear categories details and hide the div */
        function ClearInfo() {
            selectedRows = [];
            selectedProfile = 0;
            flag = ''
            EC_Id = 0;
            $("#EC_Code").val('');
            $("#EC_Order").val('');
            $("#EC_Desc").val('');
            $("#ddlCostCenter").select2('val', '');
            $("#EC_GLAccount").val('');

            $("#ddlAgentId").parent().removeClass('form-text-error');
            $("#EC_Code").removeClass('form-text-error');
            $("#EC_Order").removeClass('form-text-error');
            $("#EC_Desc").removeClass('form-text-error');
            $("#ddlCostCenter").parent().removeClass('form-text-error');
            $("#EC_GLAccount").removeClass('form-text-error');
            RemoveGrid();
            $('#CList').hide();
        }

        /* textbox allows only alphanumeric and @-_$,.:; */
        function check(e) {
            var keynum;
            var keychar;

            // For Internet Explorer
            if (window.event) {
                keynum = e.keyCode;
            }
            // For Netscape/Firefox/Opera
            else if (e.which) {
                keynum = e.which;
            }
            keychar = String.fromCharCode(keynum);
            //List of special characters you want to restrict
            if (keychar == "'" || keychar == "`" || keychar == "!" || keychar == "#" || keychar == "%" || keychar == "^" || keychar == "*" || keychar == "(" || keychar == ")" || keychar == "+" || keychar == "=" || keychar == "/" || keychar == "~" || keychar == "<" || keychar == ">" || keychar == "|" || keychar == "?" || keychar == "{" || keychar == "}" || keychar == "[" || keychar == "]" || keychar == "¬" || keychar == "£" || keychar == '"' || keychar == "\\") {
                return false;
            } else {
                return true;
            }
        }

        /* textbox allows only alphanumeric */
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (keyCode == 32));
            return ret;
        }

        /* textbox allows only numeric */
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>

    <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">
                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left">Categories Master</h3>
                    </div>
                </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <button class="btn btn-info mr-2" id="showCategoriesList" type="button" data-toggle="collapse" data-target="#CList" aria-expanded="false" aria-controls="CateList" onclick="LoadCategoriesInfo()">VIEW ALL <i class="icon icon-search "></i></button>
                    <button type="button" class="btn btn-gray mr-2" onclick="ClearInfo();">CLEAR <i class="icon icon-close "></i></button>
                    <button type="button" class="btn btn-primary" onclick="Save();">SUBMIT <i class="icon icon-play_arrow "></i></button>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">
                        <input type="hidden" id="EC_ID" name="EC_ID" />
                        <div class="form-group col-md-3">
                            <label>Agent</label><span style="color: red;">*</span>
                            <select name="Agent" id="ddlAgentId" class="form-control" onchange="AgentChange(this.value)"></select>                           
                        </div>
                        <div class="form-group col-md-3">
                            <label>Cost Center</label><span style="color: red;">*</span>
                            <select name="CostCenter" id="ddlCostCenter" class="form-control"></select>                           
                        </div>
                        <div class="form-group col-md-3">
                            <label>Code</label><span style="color: red;">*</span>
                            <input type="text" id="EC_Code" class="form-control" maxlength="10" placeholder="Enter Code" onkeypress="return IsAlphaNumeric(event);" />                            
                        </div>
                        <div class="form-group col-md-3">
                            <label>Desc</label><span style="color: red;">*</span>
                            <input type="text" class="form-control" id="EC_Desc" maxlength="50" placeholder="Enter Desc" onkeypress="return check(event)" />                            
                        </div>
                        <div class="form-group col-md-3">
                            <label>Order</label>
                            <input type="text" class="form-control" id="EC_Order" maxlength="50" placeholder="Enter Order" onkeypress="return isNumberKey(event)" />                            
                        </div>                        
                        <div class="form-group col-md-3">
                            <label>GLAccount</label>
                            <input type="text" class="form-control" id="EC_GLAccount" maxlength="20" placeholder="Enter GLAccount" onkeypress="return IsAlphaNumeric(event);" />                            
                        </div>
                    </div>
                </div>

                <div class="exp-content-block collapse" id="CList" style="display: none;">
                    <h5 class="mb-3 float-left">All Categories</h5>
                    <div class="button-controls text-right">
                        <button class="btn btn-edit" type="button" onclick="EditCategoriesInfo();">EDIT  <i class="icon icon-edit"></i></button>
                        <button class="btn btn-danger" type="button" onclick="Delete();">DELETE  <i class="icon icon-delete"></i></button>
                    </div>
                    <div class="clear"></div>
                    <div class="table-responsive" id="CategoriesList"></div>
                </div>

            </div>
        </div>
        <div class="clear"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
