﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ViewEnquiryGUI" Codebehind="ViewEnquiry.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme %>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme %>://www.w3.org/1999/xhtml">
<head runat="server">
    <title>VIEW ENQUIRY</title>

</head>
<body>
    <form id="form1" runat="server">
           <script type="test/css">
    .tblpax {font-size:13px; border: solid 1px #ccc; }
    .heading {background:#DADADA; font-size:13px; margin-left:10px;  font-weight:bold;}
    </script>
    <div>
    <table>
    <tr>
    <td>
        <asp:DataList ID="dlViewEnquiry" runat="server" AutoGenerateColumns="false" GridLines="Both"
            BorderWidth="0" CellPadding="0" CellSpacing="0" Width="100%" ForeColor="" ShowHeader="true"
            DataKeyField="Id">
            <HeaderStyle BackColor="#333333" Font-Bold="true" ForeColor="White" HorizontalAlign="Left" />
            <HeaderTemplate>
            <table class="tblpax">
             <tr height="30px">
            <td width="380px" class="heading">
            Remarks
            </td>
            <td width="180px" class="heading">Date</td>
            </tr>
            </table>
            </HeaderTemplate>
            <ItemTemplate>
                <table  class="tblpax">
            <tr>
            <td width="380px" align="left">
            <%#Eval("Remarks")%>
            </td>
            <td width="180px" align="left"><%#Eval("CreatedOn")%></td>
            </tr>
            </table>
            
            </ItemTemplate>
        </asp:DataList>
          <asp:Label ID="lblMessage" runat="server"></asp:Label>
    </td>
    </tr>
    </table>
    
    </div>
    </form>
</body>
</html>
