﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FleetGuestDetails" Title="Fleet GuestDetails" Codebehind="FleetGuestDetails.aspx.cs" %>

<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script src="DropzoneJs_scripts/dropzone.js"></script>

    <link href="DropzoneJs_scripts/dropzone.css" rel="stylesheet" />

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <script src="Scripts/select2.min.js" type="text/javascript"></script>
    
    
    

    <style>
        #AgencyCustomerList li
        {
            padding-bottom: 4px;
        }
        .error
        {
            color: Red;
            font-weight: bold;
            font-size: 14px;
        }
    </style>

    <script>

        var call1;
        function init1() {
            var today = new Date();
            // Rendering Cal1
            call1 = new YAHOO.widget.CalendarGroup("call1", "container1");
            call1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            //           call1.cfg.setProperty("title", "Select your desired departure date:");
            call1.cfg.setProperty("close", true);
            call1.selectEvent.subscribe(setDate1);
            call1.render();
        }
        function showCalendar1() {
            init1();

            document.getElementById('container1').style.display = "block";
            document.getElementById('Outcontainer1').style.display = "block";
            //document.getElementById('Outcontainer2').style.display = "none";
        }

        function setDate1() {
            var date1 = call1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                return false;
            }
            departureDate = call1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtLicenseExpiry.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
            call1.hide();
            document.getElementById('Outcontainer1').style.display = "none";

        }

        YAHOO.util.Event.addListener(window, "load", init1);

        var ValidEmail = /^[_a-zA-Z0-9-]+(\.[_a-zA-Z0-9-]+)*@[a-zA-Z0-9-]+(\.[a-zA-Z0-9-]+)*(\.[a-zA-Z][a-zA-Z]+)$/;

        function ValidateGuestDetails() {
            var isValid = true;
            document.getElementById('title').innerHTML = '';
            document.getElementById('firstError').innerHTML = '';
            document.getElementById('lastError').innerHTML = '';
            document.getElementById('EmailError').innerHTML = '';
            document.getElementById("PickupAddressError").innerHTML = '';
            document.getElementById("LandMarkError").innerHTML = '';
            document.getElementById('mobno').innerHTML = '';
            document.getElementById('mobno2').innerHTML = '';
            document.getElementById('licenseNumber').innerHTML = '';
            document.getElementById('LicenseExpiry').innerHTML = '';
            document.getElementById('LicenseIssuePlace').innerHTML = '';
            document.getElementById('LicenseUpload').innerHTML = '';

            if (document.getElementById('<%=ddlTitle.ClientID%>').value == "0") {
                document.getElementById('title').style.display = "block";
                document.getElementById('title').innerHTML = "Please Select Title";
                isValid = false;
            }
            if (document.getElementById('<%=txtFirstName.ClientID%>').value.length == 0) {
                document.getElementById("firstError").style.display = "block";
                document.getElementById('firstError').innerHTML = "Please enter First Name";
                document.getElementById('<%=txtFirstName.ClientID%>').focus();
                isValid = false;
            }
            if (document.getElementById('<%=txtLastName.ClientID%>').value.length == 0) {
                document.getElementById("lastError").style.display = "block";
                document.getElementById("lastError").innerHTML = "Please enter Last Name";
                document.getElementById('<%=txtLastName.ClientID%>').focus();
                isValid = false;
            }
            if (document.getElementById('<%=txtEmail.ClientID%>').value.length == 0) {
                document.getElementById("EmailError").style.display = "block";
                document.getElementById("EmailError").innerHTML = "Please enter Email address";
                document.getElementById('<%=txtEmail.ClientID%>').focus();
                isValid = false;
            }
            if (!ValidEmail.test(document.getElementById('<%=txtEmail.ClientID%>').value)) {
                document.getElementById("EmailError").style.display = "block";
                document.getElementById("EmailError").innerHTML = "Please enter valid Email address";
                document.getElementById('<%=txtEmail.ClientID%>').focus();
                isValid = false;
            }

            if (document.getElementById('<%=txtPickupAddress.ClientID%>').value.length == 0) {
                document.getElementById("PickupAddressError").style.display = "block";
                document.getElementById("PickupAddressError").innerHTML = "Please enter Pickup Address";
                document.getElementById('<%=txtPickupAddress.ClientID%>').focus();
                isValid = false;
            }
            if (document.getElementById('<%=txtLandMark.ClientID%>').value.length == 0) {
                document.getElementById("LandMarkError").style.display = "block";
                document.getElementById("LandMarkError").innerHTML = "Please enter Landmark";
                document.getElementById('<%=txtLandMark.ClientID%>').focus();
                isValid = false;
            }
//            if (document.getElementById('<%=txtMobileNo2.ClientID%>').value.length == 0) {
//                document.getElementById("mobno2").style.display = "block";
//                document.getElementById("mobno2").innerHTML = "Please enter Mobile2 Number";
//                document.getElementById('<%=txtMobileNo2.ClientID%>').focus();
//                isValid = false;
//            }

            if (document.getElementById('<%=txtMobileNo.ClientID%>').value.length == 0) {
                document.getElementById("mobno").style.display = "block";
                document.getElementById("mobno").innerHTML = "Please enter Mobile1 Number";
                document.getElementById('<%=txtMobileNo.ClientID%>').focus();
                isValid = false;
            }
            else if (document.getElementById('<%=txtMobileNo.ClientID%>').value.length < 10) {
                document.getElementById("mobno").style.display = "block";
                document.getElementById("mobno").innerHTML = "Mobile No1 should be required minimum 10 digits ";
                document.getElementById('<%=txtMobileNo.ClientID%>').focus();
                isValid = false;
            }
            if (document.getElementById('<%=txtMobileNo2.ClientID%>').value.length > 0) {
                if (document.getElementById('<%=txtMobileNo2.ClientID%>').value.length < 10) {
                    document.getElementById("mobno2").style.display = "block";
                    document.getElementById("mobno2").innerHTML = "Mobile No2 should be required minimum 10 digits";
                    document.getElementById('<%=txtMobileNo2.ClientID%>').focus();
                    isValid = false;
                }
            }
            if (document.getElementById('uploadDocuments').checked) {
                $('#uploadDocumentsForm').show();
                if (document.getElementById('<%=txtLicenseNumber.ClientID%>').value.length == 0) {
                    document.getElementById("licenseNumber").style.display = "block";
                    document.getElementById("licenseNumber").innerHTML = "Please enter License Number";
                    document.getElementById('<%=txtLicenseNumber.ClientID%>').focus();
                    isValid = false;
                }
                var date1 = document.getElementById('<%=hdnToDate.ClientID %>').value;
                var date2 = document.getElementById('<%=txtLicenseExpiry.ClientID %>').value;
                var depDateArray = date1.split('/');
                var retDateArray = date2.split('/');
                var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                var difference = returndate.getTime() - depdate.getTime();
                if (date2.length == 0 || date2 == "DD/MM/YYYY") {
                    document.getElementById("LicenseExpiry").style.display = "block";
                    document.getElementById("LicenseExpiry").innerHTML = "First select License Expiry date.";
                    document.getElementById('<%=txtLicenseExpiry.ClientID%>').focus();
                    isValid = false;
                }
                else if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                    document.getElementById('LicenseExpiry').style.display = "block";
                    document.getElementById('LicenseExpiry').innerHTML = " Invalid return date";
                    isValid = false;
                }
                else if (difference <= 0) {
                    document.getElementById('LicenseExpiry').style.display = "block";
                    document.getElementById('LicenseExpiry').innerHTML = "License Expiry date should be greater than to Dropup date";
                    isValid = false;
                }
                if (document.getElementById('<%=txtLicenseIssuePlace.ClientID%>').value.length == 0) {
                    document.getElementById("LicenseIssuePlace").style.display = "block";
                    document.getElementById("LicenseIssuePlace").innerHTML = "Please enter License IssuePlace";
                    document.getElementById('<%=txtLicenseIssuePlace.ClientID%>').focus();
                    isValid = false;
                }
                var uploadLength = 0;
                for (var n = 0; n < document.getElementById('dZUpload').dropzone.files.length; n++) {
                    if (document.getElementById('dZUpload').dropzone.files[n].accepted) {
                        uploadLength = parseInt(uploadLength) + 1;
                    }
                }
                
                if ($("#uploadedFilesDiv")[0].innerText.trim() == '') {
                    if (document.getElementById('dZUpload').innerText.trim() == "Drop image here.") {
                        document.getElementById("LicenseUpload").style.display = "block";
                        document.getElementById("LicenseUpload").innerHTML = "Please Upload Documents";
                        document.getElementById('dZUpload').focus();
                        isValid = false;
                    }
                    else if (parseInt(uploadLength) <= 3) { //Upload images Count
                        document.getElementById("LicenseUpload").style.display = "block";
                        document.getElementById("LicenseUpload").innerHTML = "Please Upload minimum 4 documents";
                        document.getElementById('dZUpload').focus();
                        isValid = false;
                    }
                }  ////Upload images Count +previous uplaod count
                else if ((parseInt(document.getElementById('<%=hdnFilesCount.ClientID %>').value) + parseInt(uploadLength)) <= 3) {
                    document.getElementById("LicenseUpload").style.display = "block";
                    document.getElementById("LicenseUpload").innerHTML = "Please Upload minimum 4 documents";
                    document.getElementById('dZUpload').focus();
                    isValid = false;
                }
            }
            if (isValid) {
                document.getElementById('<%=action.ClientID %>').value = "BookFleet";
                return true;
            }
            else {
                return false;
            }



            //return true;
        }



        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }

        function numbersonly() {
            var data = document.getElementById('ctl00_cphTransaction_txtMarkup').value;
            var filter = /^\d+(?:\.\d{1,2})?$/;
            if (filter.test(data) == false) {
                return false;
            } else
                return true;
        }

        function validateName(oSrc, args) {
            args.IsValid = (args.Value.length >= 2);
        }

        function calculateMarkup() {
            var dp = eval('<%=Settings.LoginInfo.DecimalValue%>');
            var markup = !isNumber(document.getElementById('<%=txtMarkup.ClientID %>').value) ? 0 : document.getElementById('<%=txtMarkup.ClientID %>').value;
            if (markup == '') {
                markup = 0;
            }
            document.getElementById('<%=txtMarkup.ClientID %>').value = parseFloat(markup).toFixed(dp);
            var percent = document.getElementById('<%=rbtnPercent.ClientID %>').checked;
            var fixed = document.getElementById('<%=rbtnFixed.ClientID %>').checked;
            var total;

            if (window.navigator.appCodeName != "Mozilla") {
                total = document.getElementById('<%=lblCost.ClientID %>').innerText;
            } else {
                total = document.getElementById('<%=lblCost.ClientID %>').textContent;
            }
            total = total.replace(',', '');
            var temp = 0;
            var totCalu = 0;
            total = parseFloat(total).toFixed(dp);
            if (markup.length > 0) {
                if (percent == true) {
                    totCalu = parseFloat((total * (markup / 100)));
                    temp = eval(parseFloat(total)) + eval(parseFloat((total * (markup / 100))).toFixed(dp));

                } else if (fixed == true) {
                    totCalu = parseFloat(markup);
                    temp = eval(parseFloat(total)) + eval(parseFloat(markup).toFixed(dp));
                }
                temp = parseFloat(temp).toFixed(dp);
                // 	page level markup limit not more than 15%
                var calucatePer = parseFloat((total * (15 / 100)));
                if (totCalu > calucatePer) {
                    alert('Markup cannot exceed 15% of Rental Charges !')
                    temp = 0;
                    document.getElementById('<%=txtMarkup.ClientID %>').value = parseFloat(temp).toFixed(dp);
                }
                if (temp == 0) {
                    temp = total;
                }
                if (window.navigator.appCodeName != "Mozilla") {
                    document.getElementById('<%=lblTotal.ClientID %>').innerText = temp;
                } else {
                    document.getElementById('<%=lblTotal.ClientID %>').textContent = temp;
                }
            } else {
                if (window.navigator.appCodeName != "Mozilla") {
                    document.getElementById('<%=lblTotal.ClientID %>').innerText = (total);
                } else {
                    document.getElementById('<%=lblTotal.ClientID %>').textContent = (total);
                }
            }
        }


        function AddServices(id) {
            var total = 0;
            var price = 0;
            var dp = eval('<%=Settings.LoginInfo.DecimalValue%>');
            total = parseFloat(eval(document.getElementById("<%=hdnTotalAmount.ClientID%>").value.replace(',', '')));
            if (document.getElementById("ctl00_cphTransaction_chkVRF") != null && document.getElementById("ctl00_cphTransaction_chkVRF").checked) {
                price = parseFloat(eval(document.getElementById("ctl00_cphTransaction_lblVRF").innerHTML.replace(',', '')));
                document.getElementById("ctl00_cphTransaction_lblVRFValue").innerHTML = price.toFixed(dp);
                total = total + price;
            }
            else if (document.getElementById("ctl00_cphTransaction_chkVRF") != null) {
                document.getElementById("ctl00_cphTransaction_lblVRFValue").innerHTML = (0).toFixed(dp);
            }
            if (document.getElementById("ctl00_cphTransaction_chkSCDW") != null && document.getElementById("ctl00_cphTransaction_chkSCDW").checked) {
                price = parseFloat(eval(document.getElementById("ctl00_cphTransaction_lblSCDW").innerHTML.replace(',', '')));
                document.getElementById("ctl00_cphTransaction_lblSCDWValue").innerHTML = price.toFixed(dp);
                total = total + price;
            }
            else if (document.getElementById("ctl00_cphTransaction_chkSCDW") != null) {
                document.getElementById("ctl00_cphTransaction_lblSCDWValue").innerHTML = (0).toFixed(dp);
            }
            if (document.getElementById("ctl00_cphTransaction_chkCDW") != null && document.getElementById("ctl00_cphTransaction_chkCDW").checked) {
                price = parseFloat(eval(document.getElementById("ctl00_cphTransaction_lblCDW").innerHTML.replace(',', '')));
                document.getElementById("ctl00_cphTransaction_lblCDWValue").innerHTML = price.toFixed(dp);
                total = total + price;
            }
            else if (document.getElementById("ctl00_cphTransaction_chkCDW") != null) {
                document.getElementById("ctl00_cphTransaction_lblCDWValue").innerHTML = (0).toFixed(dp);
            }
            if (document.getElementById("ctl00_cphTransaction_chkPAI") != null && document.getElementById("ctl00_cphTransaction_chkPAI").checked) {
                price = parseFloat(eval(document.getElementById("ctl00_cphTransaction_lblPAI").innerHTML.replace(',', '')));
                document.getElementById("ctl00_cphTransaction_lblPAIValue").innerHTML = price.toFixed(dp);
                total = total + price;
            }
            else if (document.getElementById("ctl00_cphTransaction_chkPAI") != null) {
                document.getElementById("ctl00_cphTransaction_lblPAIValue").innerHTML = (0).toFixed(dp);
            }
            if (document.getElementById("ctl00_cphTransaction_chkCSEAT") != null && document.getElementById("ctl00_cphTransaction_chkCSEAT").checked) {
                price = parseFloat(eval(document.getElementById("ctl00_cphTransaction_lblCSEAT").innerHTML.replace(',', '')));
                document.getElementById("ctl00_cphTransaction_lblCSEATValue").innerHTML = price.toFixed(dp);
                total = total + price;
            }
            else if (document.getElementById("ctl00_cphTransaction_chkCSEAT") != null) {
                document.getElementById("ctl00_cphTransaction_lblCSEATValue").innerHTML = (0).toFixed(dp);
            }
            if (document.getElementById("ctl00_cphTransaction_chkVMD") != null && document.getElementById("ctl00_cphTransaction_chkVMD").checked) {
                price = parseFloat(eval(document.getElementById("ctl00_cphTransaction_lblVMD").innerHTML.replace(',', '')));
                document.getElementById("ctl00_cphTransaction_lblVMDValue").innerHTML = price.toFixed(dp);
                total = total + price;
            }
            else if (document.getElementById("ctl00_cphTransaction_chkVMD") != null) {
                document.getElementById("ctl00_cphTransaction_lblVMDValue").innerHTML = (0).toFixed(dp);
            }
            if (document.getElementById("ctl00_cphTransaction_chkGPS") != null && document.getElementById("ctl00_cphTransaction_chkGPS").checked) {
                price = parseFloat(eval(document.getElementById("ctl00_cphTransaction_lblGPS").innerHTML.replace(',', '')));
                document.getElementById("ctl00_cphTransaction_lblGPSValue").innerHTML = price.toFixed(dp);
                total = total + price;
            }
            else if (document.getElementById("ctl00_cphTransaction_chkGPS") != null) {
                document.getElementById("ctl00_cphTransaction_lblGPSValue").innerHTML = (0).toFixed(dp);
            }
            if (document.getElementById("ctl00_cphTransaction_chkChauffer") != null && document.getElementById("ctl00_cphTransaction_chkChauffer").checked) {
                price = parseFloat(eval(document.getElementById("ctl00_cphTransaction_lblChauffer").innerHTML.replace(',', '')));
                document.getElementById("ctl00_cphTransaction_lblChaufferValue").innerHTML = price.toFixed(dp);
                total = total + price;
            }
            else if (document.getElementById("ctl00_cphTransaction_chkChauffer") != null) {
                document.getElementById("ctl00_cphTransaction_lblChaufferValue").innerHTML = (0).toFixed(dp);
            }
            document.getElementById("<%=lblGrandTotal.ClientID%>").innerHTML = total.toFixed(dp);
            document.getElementById("<%=lblPrice.ClientID%>").innerHTML = total.toFixed(dp);
        }


        function isValidateMaxLength(textBox) {
            var percent = document.getElementById('<%=rbtnPercent.ClientID %>').checked;
            if (percent) {
                if (textBox.value.length > 3) {
                    textBox.value = textBox.value.substr(0, 3);
                }
            }

        }

       
    </script>

    <script type="text/javascript">
                var specialKeys = new Array();
                specialKeys.push(8); //Backspace
                specialKeys.push(9); //Tab
                specialKeys.push(46); //Delete
                specialKeys.push(36); //Home
                specialKeys.push(35); //End
                specialKeys.push(37); //Left
                specialKeys.push(39); //Right
                
                
                function deleteFile(id) {
          
            var deleteFileDiv = "divFiles" + id;
            document.getElementById(deleteFileDiv).style.display = "none";
            if(parseInt(document.getElementById('<%=hdnFilesCount.ClientID %>').value) > 0)
            {
            document.getElementById('<%=hdnFilesCount.ClientID %>').value=parseInt(document.getElementById('<%=hdnFilesCount.ClientID %>').value)-1;
            }
            if (document.getElementById('<%=hdnDeletedFilesList.ClientID %>').value == "0") {
                document.getElementById('<%=hdnDeletedFilesList.ClientID %>').value = id;
            }
            else {
                document.getElementById('<%=hdnDeletedFilesList.ClientID %>').value = document.getElementById('<%=hdnDeletedFilesList.ClientID %>').value + '|' + id;
            }

            
        }
                
                
                function IsAlphaNumeric(e) {
                    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                    var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
                    return ret;
                }

                function OpenPopUp() {
                    document.getElementById('SearchBox').value = "";

                    document.getElementById('SearchBox').value = "";

                    //IShimPop('SearchCustomerPop', 'IShimFrame');
                }

                function IShimPop(divId, iframeId) {
                    if (document.getElementById('AgencyCustomerList') != null) {
                        document.getElementById('AgencyCustomerList').innerHTML = '';
                    }
                    var DivRef = document.getElementById(divId);
                    var IfrRef = document.getElementById(iframeId);

                    if (DivRef.style.display == "none") {
                        //DivRef.style.display = "block";
                        // IfrRef.style.width = "100px";
                        //IfrRef.style.height = "100px";
                        // IfrRef.style.display = "block";
                    } else {
                        DivRef.style.display = "none";
                        IfrRef.style.display = "none";
                        $('#SearchCustomerPop').modal('hide');
                    }
                }

                var Ajax;

                function SearchAgentCustomer() {
                    if ((document.getElementById('SearchBox').value).length == 0) {
                        document.getElementById('AgencyCustomerList').innerHTML = "<div class='alert alert-danger text-left'>Please enter some text in search Box</div>";
                        return false;
                    }
                    var paramList = 'requestFrom=GetFleetPax&customerOf=' + <%=agencyId%> + '&boxText=' + document.getElementById('SearchBox').value;
                    var url = "CustomerListAjax";
                    if (window.XMLHttpRequest) {
                        Ajax = new window.XMLHttpRequest();
                    } else {
                        Ajax = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    Ajax.onreadystatechange = ShowAgentDetail;
                    Ajax.open('POST', url);
                    Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    Ajax.send(paramList);
                }

                function ShowAgentDetail() {
                    if (Ajax.readyState == 4) {
                        if (Ajax.status == 200) {
                            if (Ajax.responseText.length > 0) {
                                var text = Ajax.responseText;
                                document.getElementById('AgencyCustomerList').innerHTML = text;
                            }
                        }
                    }
                }

                function FillDataThroughPopUp(paxId) {
                    var paramList = 'paxId=' + paxId;
                    paramList += '&requestFrom=' + "PassengerDetail";

                    var url = "CustomerListAjax";
                    if (window.XMLHttpRequest) {
                        Ajax = new window.XMLHttpRequest();
                    } else {
                        Ajax = new ActiveXObject("Microsoft.XMLHTTP");
                    }
                    Ajax.onreadystatechange = ShowingDetailsforPopup;
                    Ajax.open('POST', url);
                    Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    Ajax.send(paramList);
                    $('#SearchCustomerPop').modal('hide');
                }

                function ShowingDetailsforPopup() {
                    if (Ajax.readyState == 4) {
                        if (Ajax.status == 200) {
                            if (Ajax.responseText.length > 0) {
                                var text = Ajax.responseText;
                                if (document.getElementById('AgencyCustomerList') != null) {
                                    document.getElementById('AgencyCustomerList').innerHTML = '';
                                }
                                IShimPop('IShimFrame', 'SearchCustomerPop');
                                FillData(text);
                            }
                        }
                    }
                }
                
                
                
                

                function FillData(response) {
             
               
                
                    if (response.responseText != "Error") {
                        var responseArray = response.split(';*;');
                        if (responseArray[0] != null && responseArray[0] != "") {
                            document.getElementById('<%=ddlTitle.ClientID %>').value = responseArray[0]; //Title       
                        } else {
                            document.getElementById('<%=ddlTitle.ClientID %>').value = "-1"; //Title       
                        }
                        if (responseArray[1] != null && responseArray[1] != "") {
                            document.getElementById('<%=txtFirstName.ClientID %>').value = responseArray[1]; //FirstName       
                        } else {
                            document.getElementById('<%=txtFirstName.ClientID %>').value = ""; //FirstName       
                        }
                        if (responseArray[2] != null && responseArray[2] != "") {
                            document.getElementById('<%=txtLastName.ClientID %>').value = responseArray[2]; //LastName
                        } else {
                            document.getElementById('<%=txtLastName.ClientID %>').value = ""; //LastName
                        }
                        if (responseArray[3] != null && responseArray[3] != "") {
                            document.getElementById('<%=txtEmail.ClientID %>').value = responseArray[3]; //LastName
                        } else {
                            document.getElementById('<%=txtEmail.ClientID %>').value = ""; //LastName
                        }

                        if (responseArray[4] != null && responseArray[4] != "") {
                            document.getElementById('<%=hdnClient.ClientID %>').value = responseArray[4]; //LastName
                        } else {
                            document.getElementById('<%=hdnClient.ClientID %>').value = ""; //LastName
                        }
                        if (responseArray[5] != null && responseArray[5] != "") {
                            document.getElementById('<%=txtMobileNo.ClientID %>').value = responseArray[5]; //LastName
                        } else {
                            document.getElementById('<%=txtMobileNo.ClientID %>').value = ""; //LastName
                        }
                        if (responseArray[6] != null && responseArray[6] != "") {
                        
                            document.getElementById('<%=txtLicenseNumber.ClientID %>').value = responseArray[6]; //License Number
                        } else {
                            document.getElementById('<%=txtLicenseNumber.ClientID %>').value = ""; //License Number
                        }
                        if (responseArray[7] != null && responseArray[7] != "") {
                            var licenseDate=responseArray[7].split(" ")[0].split('/');
                             var month = licenseDate[0];
                             var day = licenseDate[1];

                             if (month.toString().length == 1) {
                                 month = "0" + month;
                                 }

                             if (day.toString().length == 1) {
                                day = "0" + day;
                                }
                            document.getElementById('<%=txtLicenseExpiry.ClientID %>').value = day+'/'+month+'/'+licenseDate[2]; //License Expiry date
                        } else {
                            document.getElementById('<%=txtLicenseExpiry.ClientID %>').value = ""; //License Expiry date
                        }
                        if (responseArray[8] != null && responseArray[8] != "") {
                            document.getElementById('<%=txtLicenseIssuePlace.ClientID %>').value = responseArray[8]; //License Issue Place
                        } else {
                            document.getElementById('<%=txtLicenseIssuePlace.ClientID %>').value = ""; //License Issue Place
                        }
                         
                          if (responseArray[9] != null && responseArray[9] != "") {
                            document.getElementById('<%=txtPickupAddress.ClientID %>').value = responseArray[9]; //PickupLocation
                        } else {
                            document.getElementById('<%=txtPickupAddress.ClientID %>').value = ""; //PickupLocation
                        }
                         if (responseArray[10] != null && responseArray[10] != "") {
                            document.getElementById('<%=txtLandMark.ClientID %>').value = responseArray[10]; //LandMark
                        } else {
                            document.getElementById('<%=txtLandMark.ClientID %>').value = ""; //LandMark
                        }
                         if (responseArray[11] != null && responseArray[11] != "") {
                            document.getElementById('<%=txtMobileNo2.ClientID %>').value = responseArray[11]; //MobileNo2
                        } else {
                            document.getElementById('<%=txtMobileNo2.ClientID %>').value = ""; ////MobileNo2
                        }
                         if (responseArray[12] != null && responseArray[12] != "") {
                            document.getElementById('<%=hdnFilesCount.ClientID %>').value = responseArray[12]; 
                        } else {
                            document.getElementById('<%=hdnFilesCount.ClientID %>').value = ""; 
                        }
                         
                        if (responseArray[13] != null && responseArray[13] != "") {
                        document.getElementById('<%=hdnDeletedFilesList.ClientID %>').value = "0";
                             $('#uploadDocumentsForm').show();
                             document.getElementById('uploadDocuments').checked = true;
                             console.log(responseArray[13]);
                             document.getElementById("uploadedFilesDiv").style.display ="block";
                             $("#uploadedFilesDiv").html(responseArray[13]);
                            // document.getElementById("uploadedFilesDiv").innerHTML(responseArray[9]);                         
                        } 
                        else
                        {
                             $('#uploadDocumentsForm').hide();
                             document.getElementById('uploadDocuments').checked = false;
                             document.getElementById("uploadedFilesDiv").style.display ="none";
                        }
                        
                    }
                }



    function Download(path,docName) {
            var open = window.open("DownloadeDoc.aspx?path=" + path + "&docName=" + docName);
            return false;
        }
                
                
                
    </script>

<asp:HiddenField ID="hdnDeletedFilesList" runat="server"  Value="0"/>
<asp:HiddenField ID="hdnFilesCount" runat="server" Value="0" />
    <div id="Outcontainer1" style="position: absolute; top: 730px; left: 45%; display: none;
        z-index: 300">
        <div id="container1" style="border: 0px solid #ccc;">
        </div>
    </div>
    <asp:HiddenField ID="action" runat="server" />
    <asp:HiddenField ID="hdnClient" runat="server" Value="0" />
    <asp:HiddenField ID="hdnTotalAmount" runat="server" Value="0" />
    <asp:HiddenField ID="hdnToDate" runat="server" Value="" />
    <div id="errorProfileRegister" style="display: none; float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
    </div>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <div class="col-md-3 padding-0 bg_white">
        <div class="">
            <div class="ns-h3">
                Pricing Details</div>
            <div style="padding-bottom: 5px;">
                <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td class="gray-smlheading">
                            <strong>Product</strong>
                        </td>
                        <td class="gray-smlheading">
                            <strong>Price</strong>
                        </td>
                    </tr>
                    <tr>
                        <td>
                           Car Rental Charges
 
                        </td>
                        <td>
                            <%=(itinerary.Currency) %>
                            <asp:Label ID="lblCost" runat="server" Text=""></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Total</strong>
                        </td>
                        <td>
                            <strong>
                                <%=(itinerary.Currency)%>
                                <asp:Label ID="lblTotal" runat="server"></asp:Label></strong>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="ns-h3">
                Markup</div>
            <div style="padding: 5px;">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="69%">
                          <table> 
                                
                                <tr> 
                                 <td> <strong>Add Markup &nbsp; </strong></td>
                             
                                 <td> 
                                    <asp:TextBox ID="txtMarkup" CssClass="form-control" Width="70px" MaxLength="5"
                                        runat="server" onblur="calculateMarkup();" onKeyUp="return isValidateMaxLength(this);"
                                        onChange="return isValidateMaxLength(this);" onkeypress="return isNumber(event);"
                                        onpaste="return false;" ondrop="return false;" Text="0"></asp:TextBox>
                               </td>
                               </tr>
                               </table>
                        </td>
                        <td width="15%">
                            P
                            <label>
                                <asp:RadioButton ID="rbtnPercent" runat="server" Checked="True" GroupName="markup"
                                    onclick="calculateMarkup();" />
                            </label>
                        </td>
                        <td width="16%">
                            F
                            <label>
                                <asp:RadioButton ID="rbtnFixed" runat="server" GroupName="markup" onclick="calculateMarkup();" />
                            </label>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="ns-h3">
                Fleet Details</div>
            <div style="padding-bottom: 5px;">
                <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="42%">
                            <strong>Fleet name :</strong>
                        </td>
                        <td width="58%" style="font-weight: bold">
                            <%=itinerary.FleetName %>
                        </td>
                    </tr>
                    <tr>
                        <td width="42%">
                            <strong>Pickup Details</strong>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td width="42%">
                            <strong>Pickup City :</strong>
                        </td>
                        <td width="58%" style="font-weight: bold">
                            <%=itinerary.FromCity%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Pickup Location : </strong>
                            <br />
                        </td>
                        <td>
                            <%=itinerary.FromLocation %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Pickup Date: </strong>
                        </td>
                        <td>
                            <%=itinerary.FromDate.ToString("dd-MMM-yyyy HH:mm") %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Drop Details </strong>
                        </td>
                        <td>
                        </td>
                    </tr>
                    <%if (itinerary.ReturnStatus == "Y")
                      { %>
                    <tr>
                        <td width="42%">
                            <strong>Drop City :</strong>
                        </td>
                        <td width="58%" style="font-weight: bold">
                            <%=itinerary.ToCity %>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Drop Location : </strong>
                            <br />
                        </td>
                        <td>
                            <%=itinerary.ToLocation %>
                        </td>
                    </tr>
                    <%} %>
                    <tr>
                        <td>
                            <strong>Drop Date: </strong>
                        </td>
                        <td>
                            <%=itinerary.ToDate.ToString("dd-MMM-yyyy HH:mm")%>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <strong>Price :</strong>
                        </td>
                        <td>
                            <strong>
                                <%=(itinerary.Currency)%>
                                <asp:Label ID="lblPrice" runat="server" Text=""></asp:Label>
                            </strong>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clear">
            </div>
        </div>
    </div>
    <!--<div id="SearchCustomerPop" style="float: left; display: none; width: 480px; position: absolute; left: 347px;
                top: 40px; background-color: Green; z-index: 100">
                <div class="fleft border-bottom-black padding-5 search-popup-child" style="width: 270px;">
                    <span class="font-16 fleft margin-top-2">Search</span> <span class="margin-left-10 fleft margin-top-2" style="width: 118px;">
                        <input type="text" id="SearchBox" class="text" style="width: 115px" />
                    </span><span class="fleft margin-left-5">
                        <input type="button" name="AgentCustomerSearch" id="AgentCustomerSearch" value="Search"
                            onclick="javascript:SearchAgentCustomer();" />
                    </span><span class="hand fright margin-left-5" onclick="javascript:IShimPop('SearchCustomerPop','IShimFrame');">
                        <img alt="close" src="Images/close.gif" /></span></div>
                <div id="AgencyCustomerList" class="fleft margin-top-5 padding-5" style="height: 130px;
                    overflow: auto;">
                </div>
            </div>-->
    <div class="modal fade" id="SearchCustomerPop" tabindex="-1" role="dialog" aria-labelledby="SearchCustomerPopLabel">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" onclick="javascript:IShimPop('SearchCustomerPop','IShimFrame');">
                        <span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">
                        Select Passenger</h4>
                </div>
                <div class="modal-body">
                    <div class="form-horizontal">
                        <div class="form-group">
                            <label class="col-sm-4 control-label">
                                Passenger Name</label>
                            <div class="col-sm-8">
                                <input type="text" id="SearchBox" class="form-control" />
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-sm-8">
                                <input type="button" class="btn btn-primary but_b" name="AgentCustomerSearch" id="AgentCustomerSearch"
                                    value="Search" onclick="javascript:SearchAgentCustomer();" />
                            </div>
                        </div>
                    </div>
                    <div id="AgencyCustomerList" class="" style="overflow: auto; padding-top: 20px">
                        <ul>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="IShimFrame" style="position: absolute; display: none;">
    </div>
    <div class="col-md-9 pad_right0 paramcon">
        <div class="ns-h3">
            Please enter the details of the guest.
        </div>
        <div class="bg_white" style="border: solid 1px #ccc; padding: 10px; padding-top: 0px;">
            <h6>
                <a id="SelectPassenger" href="javascript:OpenPopUp()" class="select-passenger" data-toggle="modal"
                    data-target="#SearchCustomerPop">Select Passenger from Customer List</a></h6>
            <div>
                <div class="subgray-header">
                    <b>
                        <%=(itinerary.FleetName)%>
                    </b>
                </div>
                <div class="col-md-12 padding-0" style="padding-top: 20px;">
                    <div class="col-md-3  form-group">
                        <strong>Title<br />
                        </strong>
                    </div>
                    <div class="col-md-3  form-group">
                        <asp:DropDownList ID="ddlTitle" runat="server" CssClass="form-control">
                            <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                            <asp:ListItem>Mr.</asp:ListItem>
                            <asp:ListItem>Ms.</asp:ListItem>
                        </asp:DropDownList>
                        <span id='title' class="error" style="display: none;"></span>
                        <%-- <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="ddlTitle" ErrorMessage="Choose Title" InitialValue="Select" SetFocusOnError="True" ValidationGroup="pax"></asp:RequiredFieldValidator>--%>
                    </div>
                    <div class="col-md-3  form-group">
                        <strong>First Name*<br />
                        </strong>
                    </div>
                    <div class="col-md-3  form-group">
                        <asp:TextBox ID="txtFirstName" runat="server" CssClass="form-control" onkeypress="return IsAlphaNumeric(event);"
                            ondrop="return false;" MaxLength="20"></asp:TextBox>
                        <span class="error" style="display: none" id="firstError"></span>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ControlToValidate="txtFirstName" ErrorMessage="Enter First Name" SetFocusOnError="True" ValidationGroup="pax"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="CustomValidator1" runat="server" ControlToValidate="txtFirstName" ClientValidationFunction="validateName" SetFocusOnError="True" ValidationGroup="pax" ErrorMessage="Minimum 2 alphabets required"></asp:CustomValidator>--%>
                    </div>
                    <div class=" clearfix">
                    </div>
                </div>
                <div>
                    <div class="col-md-3  form-group">
                        <strong>Last Name*<br />
                        </strong>
                    </div>
                    <div class="col-md-3  form-group">
                        <asp:TextBox ID="txtLastName" runat="server" CssClass="form-control" onkeypress="return IsAlphaNumeric(event);"
                            ondrop="return false;" MaxLength="20"></asp:TextBox>
                        <span class="error" style="display: none" id="lastError"></span>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter Last Name" ControlToValidate="txtLastName" SetFocusOnError="true" ValidationGroup="pax" onkeypress="return IsAlphaNumeric(event);" ondrop="return false;"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtLastName" ClientValidationFunction="validateName" SetFocusOnError="True" ValidationGroup="pax" ErrorMessage="Minimum 2 alphabets required"></asp:CustomValidator>--%>
                    </div>
                    <div class="col-md-3  form-group">
                        <strong>E-mail ID*<br />
                        </strong>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="50"
                            ondrop="return false;"></asp:TextBox>
                        <span class="error" style="display: none" id="EmailError"></span>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter Email" SetFocusOnError="true" ControlToValidate="txtEmail" ValidationGroup="pax"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email" SetFocusOnError="true" ControlToValidate="txtEmail" ValidationGroup="pax" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>--%>
                    </div>
                    <div class=" clearfix">
                    </div>
                </div>
                <div>
                    <div class="col-md-3  form-group">
                        <strong>Pickup Address*<br />
                        </strong>
                    </div>
                    <div class="col-md-3  form-group">
                        <asp:TextBox ID="txtPickupAddress" runat="server" CssClass="form-control"
                             MaxLength="500" TextMode="MultiLine" Height="60px"></asp:TextBox>
                        <span class="error" style="display: none" id="PickupAddressError"></span>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Enter Last Name" ControlToValidate="txtLastName" SetFocusOnError="true" ValidationGroup="pax" onkeypress="return IsAlphaNumeric(event);" ondrop="return false;"></asp:RequiredFieldValidator>
                                <asp:CustomValidator ID="CustomValidator2" runat="server" ControlToValidate="txtLastName" ClientValidationFunction="validateName" SetFocusOnError="True" ValidationGroup="pax" ErrorMessage="Minimum 2 alphabets required"></asp:CustomValidator>--%>
                    </div>
                    <div class="col-md-3  form-group">
                        <strong>Land mark*<br />
                        </strong>
                    </div>
                    <div class="col-md-3">
                        <asp:TextBox ID="txtLandMark" runat="server" CssClass="form-control" MaxLength="200" TextMode="MultiLine" Height="60px"
                            ></asp:TextBox>
                        <span class="error" style="display: none" id="LandMarkError"></span>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ErrorMessage="Enter Email" SetFocusOnError="true" ControlToValidate="txtEmail" ValidationGroup="pax"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ErrorMessage="Invalid Email" SetFocusOnError="true" ControlToValidate="txtEmail" ValidationGroup="pax" ValidationExpression="\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>--%>
                    </div>
                    <div class=" clearfix">
                    </div>
                </div>
              
                <div>
                    <div class="col-md-3  form-group">
                        <strong>Mobile No1*<br />
                        </strong>
                    </div>
                    <div class="col-md-3  form-group">
                        <asp:TextBox ID="txtMobileNo" runat="server" CssClass="form-control" onkeypress="return isNumber(event)" placeHolder="Residence Mobile No"
                            MaxLength="20"></asp:TextBox>
                        <span class="error" style="display: none" id="mobno"></span>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Enter Mobile No" ControlToValidate="txtMobileNo" SetFocusOnError="true" ValidationGroup="pax" onkeypress="return isNumber(event);" ondrop="return false;"></asp:RequiredFieldValidator>--%>
                    </div>
                    
                     <div class="col-md-3  form-group">
                        <strong>Mobile No2<br />
                        </strong>
                    </div>
                    <div class="col-md-3  form-group">
                        <asp:TextBox ID="txtMobileNo2" runat="server" CssClass="form-control" onkeypress="return isNumber(event)" placeHolder="Travelling Mobile No"
                            MaxLength="20"></asp:TextBox>
                        <span class="error" style="display: none" id="mobno2"></span>
                        <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Enter Mobile No" ControlToValidate="txtMobileNo" SetFocusOnError="true" ValidationGroup="pax" onkeypress="return isNumber(event);" ondrop="return false;"></asp:RequiredFieldValidator>--%>
                    </div>
                   
                    <div class=" clearfix">
                    </div>
                </div>
            </div>
        </div>
        <div>
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
            <td colspan="2">
            <strong>&nbsp;Note :Agent must provide the Home country Mobile Number and also the International Number.</strong> 
            </td>
            </tr>
                <tr>
                    <td colspan="2">
                        <div style="padding: 0px; overflow: auto">
                            <div class="alert alert-warning" role="alert">
                            <h5 style="color:Red;"><b>Mandatory information which has to be declared to customer.</b></h5>
                             <div style='padding: 0px 10px 0px 10px'>
                              <li> Rental to be charged in advance. </li>
                              <li>Please inform the customer there will be a refundable security deposit of AED 1500, which will be blocked on his credit card at the time of delivery or customer has an option to       pay the deposit in Cash. </li>
                              <li>The security deposit will be refunded after 15 working days from the date of return of car. </li>
                              <li>For international customers, security deposit will be refunded after 30 working dates from the date of return of car. </li>
                              <%if (searchResult.Ins_Excess > 0)
                              { %>
                              <li>The hirer is liable to pay the applicable INSURANCE EXCESS of  <%=searchResult.Ins_Excess.ToString("N0") %>
                                    <%=itinerary.Currency %> in case of an accident where the hirer is at mistake or the Third party is unknown. (Not applicable       if CDW is opted)</li>
                              <%} %>
                             </div>
                             </br>
                               <h5 style="color:Red;"><b>>Kindly note that below countries driving licenses allow you to drive in Dubai as long as you are on visit visa, and please note that you can only drive a rented car or a first degree relative car.</b</h5>
                                <div style='padding: 0px 10px 0px 10px'>
                                    <li><b>GCC Countries:</b> Kuwait – Saudi Arabia – Bahrain – Oman – Qatar </li>
                                    <li><b>European states:</b> Austria – Belgium – Spain – Germany – France – Ireland –Netherland
                                        – Italy –UK – Turkey – Greece – Switzerland – Norway – Denmark – Sweden –Romania
                                        – Poland – Finland Portugal - Luxembourg - Slovakia </li>
                                    <li><b>American states</b></li>
                                    <li>Canada – USA </li>
                                    <li><b>East Asia: </b></li>
                                    <li><b>South Korea</b> – New Zealand – Hong Kong – Japan – Australia – Singapore- China.</li>
                                    <li><b>African States:</b> South Africa. </li>
                                </div>
  
 

                            </div>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="col-xs-12">
            <div class="checkbox custom-checkbox">
                <label style="font-size:medium">
                    <input type="checkbox" id="uploadDocuments" checked="checked" disabled="disabled">
                   <strong>Upload documents (Driving license details, expiry, place of issue and photo, Visa Page, Passport Copy first page & last page) </strong> <span class="input-check-icon"></span>
                </label>
            </div>
        </div>
        <div class="upload-documents-form" id="uploadDocumentsForm" style="display: block;">
            <div class="col-md-4">
                <div class="form-group">
                    <label for="LoginInputEmail">
                       <strong> License Number</strong></label>
                    <asp:TextBox runat="server" ID="txtLicenseNumber" class='form-control'></asp:TextBox>
                    <span class="error" style="display: none" id="licenseNumber"></span>
                    <%--<input type="text" class="form-control" id="txtLicenseNumber" placeholder="License Number">--%>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <table border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td colspan="2">
                                <label>
                                   <strong> License Expiry</strong></label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtLicenseExpiry" runat="server" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td>
                                <a href="javascript:void(null)" onclick="showCalendar1()">
                                    <img id="dateLink1" src="images/call-cozmo.png" />
                                </a>
                            </td>
                        </tr>
                    </table>
                    <span class="error" style="display: none" id="LicenseExpiry"></span>
                </div>
            </div>
            <div class="col-md-4">
                <div class="form-group">
                    <label for="LoginInputEmail">
                        <strong>Place of issue</strong></label>
                    <asp:TextBox runat="server" ID="txtLicenseIssuePlace" class='form-control'></asp:TextBox>
                    <span class="error" style="display: none" id="LicenseIssuePlace"></span>
                    <%--<input type="text" class="form-control" id="LoginInputEmail" placeholder="Place of issue">--%>
                </div>
            </div>
            <div class="form-group">
                <label for="LoginInputEmail">
                     <strong>Upload Documents</strong></label>
                <div id="dZUpload" class="dropzone">
                    <div class="dz-default dz-message">
                        Drop image here.
                    </div>
                </div>
                <span class="error" style="display: none" id="LicenseUpload"></span>
            </div>
        </div>
        <div class="col-md-12">
            <div id="uploadedFilesDiv" style="display: none;">
            </div>
        </div>
        
        <div>
            <table class="table table-bordered">
                <tr>
                    <td>
                    </td>
                    <td>
                        Car Rental Charges
                    </td>
                    <td>
                       
                    </td>
                    <td> <asp:Label ID="lblFleetAmount" runat="server" Text="0"></asp:Label></td>
                </tr>
                <tr>
                    <td>
                        <input type="checkbox" checked="checked" disabled="disabled" id="chkVRF" />
                    </td>
                    <td>
                        <span class="title">VRF:</span> <span class="sub-title">Vehicle Registration Fee (Mandatory
                            Charges)</span>
                    </td>
                    <td>
                        <asp:Label ID="lblVRF" runat="server" Text="0.00"></asp:Label>
                    </td>
                     <td>
                        <asp:Label ID="lblVRFValue" runat="server" Text="0.00"></asp:Label>
                    </td>
                </tr>
                <tr style="display:none">
                    <td>
                        <asp:CheckBox ID="chkSCDW" runat="server" onclick="AddServices('SCDW')" />
                    </td>
                    <td>
                        <span class="title">SCDW:</span> <span class="sub-title">Super Collision Damage Waiver
                            AED: 0 deductible</span>
                    </td>
                    <td>
                        <asp:Label ID="lblSCDW" runat="server"></asp:Label>
                    </td>
                     <td>
                        <asp:Label ID="lblSCDWValue" runat="server" Text="0.00"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox ID="chkCDW" runat="server" onclick="AddServices('CDW')" />
                    </td>
                    <td>
                        <span class="title">CDW:</span> <span class="sub-title">Collision Damage Waiver 100% deductible</span>
                    </td>
                    <td>
                        <asp:Label ID="lblCDW" runat="server"></asp:Label>
                    </td>
                      <td>
                        <asp:Label ID="lblCDWValue" runat="server" Text="0.00"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:CheckBox ID="chkPAI" runat="server" onclick="AddServices('PAI')" />
                    </td>
                    <td>
                        <span class="title">Personal Accident Insurance:</span> <span class="sub-title">Personal
                            Accident Insurance Covers driver and passengers in case of serious personal injury.</span>
                    </td>
                    <td>
                        <asp:Label ID="lblPAI" runat="server"></asp:Label>
                    </td>
                     <td>
                        <asp:Label ID="lblPAIValue" runat="server" Text="0.00"></asp:Label>
                    </td>
                </tr>
                <%if (searchResult.CSEAT > 0)
                  { %>
                <tr>
                    <td>
                        <asp:CheckBox ID="chkCSEAT" runat="server" onclick="AddServices('CSEAT')" />
                    </td>
                    <td>
                        <span class="title">Child Safety Seat:</span> <span class="sub-title">Safety Seat for
                            Childs</span>
                    </td>
                    <td>
                        <asp:Label ID="lblCSEAT" runat="server"></asp:Label>
                    </td>
                     <td>
                        <asp:Label ID="lblCSEATValue" runat="server" Text="0.00"></asp:Label>
                    </td>
                </tr>
                <%}
                  if (searchResult.VMD > 0)
                  { %>
                <tr>
                    <td>
                        <asp:CheckBox ID="chkVMD" runat="server" onclick="AddServices('VMD')" />
                    </td>
                    <td>
                        <span class="title">VMD:</span> <span class="sub-title">Vehicle Monitoring Device charges</span>
                    </td>
                    <td>
                        <asp:Label ID="lblVMD" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblVMDValue" runat="server" Text="0.00"></asp:Label>
                    </td>
                </tr>
                <%} if (searchResult.GPS > 0)
                  {%>
                     <tr>
                    <td>
                        <asp:CheckBox ID="chkGPS" runat="server" onclick="AddServices('GPS')" />
                    </td>
                    <td>
                        <span class="title">GPS</span>
                    </td>
                    <td>
                        <asp:Label ID="lblGPS" runat="server"></asp:Label>
                    </td>
                     <td>
                        <asp:Label ID="lblGPSValue" runat="server" Text="0.00"></asp:Label>
                    </td>
                </tr>
                 <%}
                  if (searchResult.Chauffer > 0)
                  {%>
                  <tr>
                    <td>
                        <asp:CheckBox ID="chkChauffer" runat="server" onclick="AddServices('Chauffer')" />
                    </td>
                    <td>
                        <span class="title">Chauffer:</span><span class="sub-title">Additional driver</span>
                    </td>
                    <td>
                        <asp:Label ID="lblChauffer" runat="server"></asp:Label>
                    </td>
                     <td>
                        <asp:Label ID="lblChaufferValue" runat="server" Text="0.00"></asp:Label>
                    </td>
                </tr>
                   <%}
                     if (searchResult.LinkCharges > 0)
                  { %>
                <tr>
                    <td>
                        <input type="checkbox" checked="checked" disabled="disabled" id="chkLink" />
                    </td>
                    <td>
                        <span class="title">Link Charges:</span> <span class="sub-title">Charges for pickup from
                            Other City</span>
                    </td>
                    <td>
                        <asp:Label ID="lblLinkCharges" runat="server"></asp:Label>
                    </td>
                     <td>
                        <asp:Label ID="lblLinkChargesValue" runat="server" Text="0.00"></asp:Label>
                    </td>
                </tr>
                <%} if (AirportCharges > 0)
                  { %>
                <tr>
                    <td>
                        <input type="checkbox" checked="checked" disabled="disabled" id="Checkbox1" />
                    </td>
                    <td>
                        <span class="title">AirPort Charges:</span> 
                        <span class="sub-title">
                      <%--  //Temporary purpose only Hard coded Location codes  ZIYAD--%>
                        <%if (itinerary.FromLocationCode.ToUpper() == "DXBA" || itinerary.FromLocationCode.ToUpper() == "AUHA" || itinerary.FromLocationCode.ToUpper() == "MAKT" )
                          { %>
                           Charge for Delivery and Parking Fee for one hour.
                          <%}
                          else if (itinerary.FromLocationCode.ToUpper() == "SHJA")
                          {%>
                          Sharjah Airport .
                          <%}
                          else if (itinerary.ReturnStatus == "Y" && !string.IsNullOrEmpty(itinerary.ToLocationCode))
                          {
                              if (itinerary.ToLocationCode.ToUpper() == "DXBA" || itinerary.ToLocationCode.ToUpper() == "AUHA" || itinerary.ToLocationCode.ToUpper() == "MAKT")
                              {%>
                                  Charge for Delivery and Parking Fee for one hour.
                               <%}
                              else if (itinerary.ToLocationCode.ToUpper() == "SHJA")
                              {%>
                           Sharjah Airport .
                          <%}
                              else
                              {%>
                                Charge for pickup from Other City
                               <%}
                          }
                          else
                          { %>
                          Charge for pickup from Other City
                          <%} %></span>
                    </td>
                    <td>
                        <asp:Label ID="lblAirPortCharges" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblAirPortChargesValue" runat="server" Text="0.00"></asp:Label>
                    </td>
                </tr>
                <%} %>
                <tr>
                    <td>
                    </td>
                      <td>
                    </td>
                    <td align="right">
                        <label style="color: Red;">
                            Total Amount</label>
                    </td>
                    <td>
                        <label style="font-weight: bold;">
                            <%=itinerary.Currency %>
                            <asp:Label ID="lblGrandTotal" runat="server"></asp:Label>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td colspan="4" align="right">
                        <label style="padding-right: 10px">
                            <asp:Button ID="imgContinue" runat="server" CssClass="but but_b" Text="Continue"
                                OnClientClick="return ValidateGuestDetails();" OnClick="imgContinue_Click" />
                        </label>
                    </td>
                </tr>
            </table>
        </div>
    </div>

    <script type="text/javascript">
        $(document).ready(function() {

            Dropzone.autoDiscover = false;

            $("#dZUpload").dropzone({
                url: "hn_FleetFileUploader.ashx",
                maxFiles: 10, //maximum 5 files can be uploaded.
                maxFilesize: 1,
                // acceptedFiles: 'image/*', // accept only png and jpeg format image files
                addRemoveLinks: true,
                success: function(file, response) {
                    var imgName = response;
                    file.previewElement.classList.add("dz-success");
                },
                error: function(file, response) {
                    file.previewElement.classList.add("dz-error");
                },
                removedfile: function(file) {
                    var _ref;
                    if (file.previewElement) {
                        if ((_ref = file.previewElement) != null) {
                            _ref.parentNode.removeChild(file.previewElement);
                        }
                    }
                    $(document).ready(function() {
                        $.ajax({
                            type: "POST",
                            url: "FleetGuestDetails.aspx/RemoveSession",
                             data: "{response: '" + file.name + "'}",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                        });
                    });
                    return this._updateMaxFilesReachedClass();
                }
            });
            $('#uploadDocuments').change(function() {
                if ($(this).is(':checked')) {
                    $('#uploadDocumentsForm').show().addClass('animated fadeIn');
                } else {
                    $('#uploadDocumentsForm').hide().removeClass('fadeIn');
                }
            });




        });
        
        
        
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
