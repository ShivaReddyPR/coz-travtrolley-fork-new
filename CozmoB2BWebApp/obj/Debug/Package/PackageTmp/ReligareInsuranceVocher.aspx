﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="ReligareInsuranceVocher.aspx.cs" Inherits="CozmoB2BWebApp.ReligareInsuranceVocher" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
<!--    <link href="css/steps.css" rel="stylesheet" type="text/css" />-->

<!--
    <div class="wizard-steps">
        <div class="completed-step">
            <a href="#step-one"><span>1</span>Select Policy</a>
        </div>
        <div class="completed-step">
            <a href="#step-two"><span>2</span> PAX Details</a>
        </div>
        <div class="completed-step">
            <a href="#"><span>3</span> Secure Payments</a>
        </div>
        <div class="active-step">
            <a href="#"><span>4</span> Confirmation</a>
        </div>
        <div class="clear">
        </div>
    </div>-->
<!--    <br />-->

    <div style="text-align: right">
        <asp:LinkButton ID="lnkVoucher" CssClass="btn btn-info btn-primary" OnClientClick="return viewVoucher();" runat="server" Text="Show Voucher" Style="float: right; border-radius: 5px; margin-right: 10px;"></asp:LinkButton>

        <asp:HiddenField ID="hdnpolicyNo" runat="server" />
        <asp:HiddenField ID="hdnpolicyData" runat="server" />

    </div>

    <div id="printableArea" runat="server">

        <div class="col-md-12">
            <asp:Image ID="imgHeaderLogo" runat="server" Width="162" Height="51px" AlternateText="AgentLogo" ImageUrl="" />
        </div>
        <div class="font_med marbot_20">
            <center>  <strong> Insurance Voucher</strong></center>
        </div>
        <div class="container" style="margin-top: 80px">

            <% if (header != null && header.ReligarePassengers != null && header.ReligarePassengers.Count > 0)
                {
                    for (int i = 0; i < header.ReligarePassengers.Count; i++)
                    {%>
            <div class="panel-group" id="accordion">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4 class="panel-title">
                            <a style="display: block; width: 100%" href="#collapseOne<%=i %>" data-parent="#accordion" data-toggle="collapse">
                                <span class="glyphicon pull-right font_bold glyphicon-minus"></span>
                                <%=header.ReligarePassengers[i].FirstName + " " + header.ReligarePassengers[i].LastName%>
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne<%=i %>" class="collapse in">
                        <table width="100%" border="1" class="table" cellpadding="0" cellspacing="0">
                            <tr class="font_bold">
                                <td>Plan Title</td>
                                <td>Plan Code</td>
                                <td>Policy No</td>
                                <td id="tdPolicy<%=i %>">Policy Link</td>

                            </tr>
                            <tr>
                                <td><%=header.ProductName %></td>
                                <td><%=header.Produ_id %></td>
                                <td><%=header.Policy_no %></td>
                                <td id="tdViewLink<%=i %>" style="display: block;"><a target='_blank' class="certificate" href='#'>View Certificate</a></td>
                            </tr>
                        </table>
                    </div>
                </div>

            </div>
            <%}
                }%>
        </div>
    </div>

     
    <div class="col-md-12 marbot_10">
        <label style="color: Red">
            Note:</label>This voucher is only for cozmo internal reference 
    </div>



    <script type="text/javascript">
        function viewVoucher() { 
            window.open('PrintReligareInsuranceVoucher.aspx?InsId=<%= header.Header_id %>', 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes');
            return false;
        }
        $(document).ready(function () {
            $(".certificate").click(function () {
                var policyNo = $("#ctl00_cphTransaction_hdnpolicyNo").val();
                $.ajax({
                    type: "POST",
                    contentType: "application/json; charset=utf-8",
                    url: "ReligareInsuranceVocher.aspx/GetPolicyPDFData",
                    data: "{'policyNo':'" + policyNo + "'}",
                    dataType: "json",
                    success: function (data) {
                        if (data.d == "")
                            alert('Statement Not Generated For Given Pnumber');
                        else {
                            var pdfWindow = window.open("");
                            pdfWindow.document.write("<iframe width='100%' height='100%' src='data:application/pdf;base64, " + data.d + "'></iframe>");
                        }
                    },
                    error: function (result) { }
                });
            });
        });
    </script>


    <script>


        $('.collapse').on('shown.bs.collapse', function () {
            $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hidden.bs.collapse', function () {
            $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });

    </script>

</asp:Content>


