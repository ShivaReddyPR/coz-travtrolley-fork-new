﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpReport.aspx.cs" Inherits="CozmoB2BWebApp.ExpReport" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />    
    <link href="css/toastr.css" rel="stylesheet" type="text/css" />

    <script src="scripts/jquery-ui.js"></script>
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="scripts/Common/ExpenseObjects.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>

    <script type="text/javascript">

        /* Global variables */
        var apiHost = mt; var bearer = mt; var cntrllerPath = 'api/expTransactions'; 
        var selectedTrips = [];
        var apiAgentInfo = {}; var employeeProfiles = {}; var expTripDetails = {}; var expenseReport = {};
        var expRepId = 0; var selectedProfile = 0; var empCostCentre = 0; var selExpDtlId = 0;
        var displayMode = mt;

        /* Page Load */
        $(document).ready(function () {

            /* Enable calender for date field */
            $("#txtRepDate").datepicker({ dateFormat: 'dd-mm-yy', minDate: 0 });   

            /* Check query string to see if existing report needs to open */
            <%--expRepId = '<%=Request.QueryString["ExpRepId"] == null ? "0" : Request.QueryString["ExpRepId"]%>';
            selExpDtlId = '<%=Request.QueryString["ExpDtlId"] == null ? "0" : Request.QueryString["ExpDtlId"]%>';--%>

            /* Check session to see if existing report needs to open */
            var pageParams = AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpReport', 'sessionData':'', 'action':'get'}");
            expRepId = selExpDtlId = 0;
            if (!IsEmpty(pageParams)) {

                var objParams = JSON.parse(pageParams);
                expRepId = !IsEmpty(objParams.ExpRepId) ? objParams.ExpRepId : 0;
                selExpDtlId = !IsEmpty(objParams.ExpDtlId) ? objParams.ExpDtlId : 0;
            }

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {

                alert('Session expired, please login once again.');
                return;
            }

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            /* Load screen data */
            GetScreenData();              
        });

        /* To get expense info and corp profiles */
        function GetScreenData() {

            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expRepId };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getExpReportScreenData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);                        
        }

        /* To bind expense info and corp profiles */
        function BindScreenData(screenData) {

            employeeProfiles = {};

            if (!IsEmpty(screenData.dtProfiles)) {

                employeeProfiles = screenData.dtProfiles;
                DdlBind('ddlProfiles', employeeProfiles, 'profileId', 'employeeId-surName name', '-', mt, '--Select Employee--', '');

                if (employeeProfiles.length == 1 || expRepId > 0) {

                    $('#ddlProfiles').attr('disabled', 'disabled');
                    $("#ddlProfiles").select2("val", employeeProfiles[0].profileId);
                    $("#ddlProfiles").val(employeeProfiles[0].profileId);
                    selectedProfile = employeeProfiles[0].profileId;
                    EmployeeChange(selectedProfile);
                }
            }

            if (!IsEmpty(screenData.expenseReport))
                BindExpenseReportData(screenData.expenseReport);

            if (expRepId > 0 && IsEmpty(screenData.expenseReport))
                ShowError('Expense report data not found.');

            if (displayMode == 'V') {

                $('#btnViewExp').show();
                $('#btnAddExp').hide();
                $('#btnIncludeTrips').hide();
                $('#btnDeleteTrips').hide();
            }
        }

        /* To bind expense report data to screen controls */
        function BindExpenseReportData(expRepData) {

            expenseReport = {};

            if (IsEmpty(expRepData))
                return;

            expenseReport = expRepData;
            
            $('#txtRepDate').val(GetDateFormat(expenseReport.eR_Date, '-', 'ddmmyyyy'));
            $('#txtTitle').val(expenseReport.eR_Title);
            $('#txtPurpose').val(expenseReport.eR_Purpose);
            $('#txtCmpVisited').val(expenseReport.eR_CompVisited);
            $('#txtComments').val(expenseReport.eR_Comments);
            $("#ddlProfiles").select2("val", expenseReport.eR_ProfileId);
            EmployeeChange(expenseReport.eR_ProfileId);
            selectedProfile = expenseReport.eR_ProfileId;

            if (!IsEmpty(expenseReport.expenseReportTrips) && expenseReport.expenseReportTrips.length > 0) {

                $.each(expenseReport.expenseReportTrips, function (key, col) {

                    selectedTrips.push(col.erT_REF_Id + '|' + (col.erT_ProductId == '1' ? 'FLIGHT' : 'HOTEL'));
                });
            }

            displayMode = expenseReport.eR_Aprroved == 'S' ? 'V' : mt;

            if (displayMode == 'V') 
                GetAvailableTrips();
        }

        /* To set employee name and cost center on employee change */
        function EmployeeChange(profileId) {

            selectedProfile = profileId;
            if (selectedProfile > 0) {

                var selProfileData = employeeProfiles.find(e => e.profileId == selectedProfile);

                if (!IsEmpty(selProfileData) && !IsEmpty(selProfileData.surName)) {

                    $('#divEmpName').html(selProfileData.surName + ' ' + selProfileData.name);
                    $('#divEmpCostCentre').html(selProfileData.costCentreName);
                    empCostCentre = selProfileData.costCentre;
                }
            }
            else {

                $('#divEmpName').html('');
                $('#divEmpCostCentre').html('');
            }            
            ClearTrips();
            ValDuplicateExp('PC');
        }

        /* To get available flight/hotel trips */
        function GetAvailableTrips() {

            if (selectedProfile == 0) {

                ShowError('Please select employee');
                return;
            }
                
            var reqData = { AgentInfo: apiAgentInfo, CorpProfileId: selectedProfile, ExpRepId: expRepId, Mode: displayMode };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getTripsToLinkExpense';
            WebApiReq(apiUrl, 'POST', reqData, '', BindTripsInfo, null, null);                        
        }

        /* To Bind flight/hotel trips to grid */
        function BindTripsInfo(tripsInfo) {
            
            if (tripsInfo.length == 0) {

                if (displayMode != 'V') {
                    
                    ShowError('No trip records found.');
                    ClearTrips();
                }
                return;
            }

            expTripDetails = tripsInfo;

            var gridProperties = GetGridPropsEntity();
            gridProperties.headerColumns = ('Date of Request|Reference No|Travel Date|Origin|Destination|Trip Purpose|Trip Type').split('|');
            gridProperties.displayColumns = ('dateOfRequest|referenceNo|travelDate|origin|destination|travelReason|product').split('|');
            gridProperties.pKColumnNames = ('prodRefId|product').split('|');
            gridProperties.dataEntity = tripsInfo;
            gridProperties.divGridId = 'divGridInfo';
            gridProperties.gridSelectedRows = selectedTrips;
            gridProperties.selectType = 'M';
            gridProperties.recordsperpage = 10;

            EnablePagingGrid(gridProperties);
            $('#divTrips').show();
        }

        /* To delete selected trips from the grid and update the status in data base */
        function DeleteTrips() {

            var expRepTripsSelected = GetSelectedTrips();
            if (IsEmpty(expRepTripsSelected) || expRepTripsSelected.length == 0) {

                ShowError('Please select trips to delete');
                return;
            }

            var expRepTripsDel = [];
            $.each(selectedTrips, function (key, col) {

                var selval = col.split('|');
                expRepTripsDel.push(GetExpTripEntity(expRepId, (selval[1] == 'FLIGHT' ? 1 : 2), selval[0], 1, apiAgentInfo.LoginUserId));
            });

            var reqData = { AgentInfo: apiAgentInfo, expenseReportTrips: expRepTripsDel, Status: 'D' };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveTripsExpenseStatus';
            WebApiReq(apiUrl, 'POST', reqData, '', RefreshGrid, null, null);                        
        }

        /* To prepare and set expense report trip entity */
        function RefreshGrid(resp) {

            $.each(selectedTrips, function (key, col) {

                expTripDetails = expTripDetails.filter(item => (item.prodRefId + '|' + item.product) !== col);
            });

            if (expTripDetails.length == 0) {

                BindTripsInfo(expTripDetails);
                return;
            }

            selectedTrips = [];
            GetSetSelectedRows('set', selectedTrips);
            GetSetDataEntity('set', expTripDetails);
            LoadData(GetSetCurrentPageNo('get', ''));
        }

        /* To validate and save expense report data */
        function SaveExpenseReport() {

            var expRepTripsSelected = GetSelectedTrips();
            var expRefNo = !IsEmpty(expenseReport) && !IsEmpty(expenseReport.eR_RefNo) ? expenseReport.eR_RefNo : '';
            var expRepDate = GetJsonDate($('#txtRepDate').val(), '-', 'ddmmyyyy');
            //expRepDate = new Date(expRepDate).toISOString();
            //console.log(JSON.stringify(expRepDate));
            //console.log(new Date(expRepDate).toISOString());
            //expRepDate = '2020-09-15T00:30:00.000';
            
            

            if (expRepTripsSelected.length == 0 && !IsEmpty(expenseReport) && !IsEmpty(expenseReport.expenseReportTrips) && expenseReport.expenseReportTrips.length > 0) {

                expenseReport.expenseReportTrips[0].erT_Status = false;
                expRepTripsSelected = expenseReport.expenseReportTrips;
            }

            var reqData = {
                AgentInfo: apiAgentInfo,
                expenseReport: GetExpReportEntity(expRepId, expRefNo, expRepDate, $('#txtTitle').val(), $('#txtPurpose').val(), $('#txtCmpVisited').val(),
                                    $('#txtComments').val(), selectedProfile, empCostCentre, 'N', true, apiAgentInfo.LoginUserId, expRepTripsSelected)
            };

            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveExpenseReport';
            WebApiReq(apiUrl, 'POST', reqData, '', SaveSuccess, null, null);                        
            
        }

        /* To validate expense report mandatory data */
        function ValidateSave() {

            var IsValid = CheckReqData('ddlProfiles', 'NM', '10', 'Employee', mt);
            IsValid = CheckReqData('txtRepDate', 'DT', '10', 'Date', mt) && IsValid;
            IsValid = CheckReqData('txtTitle', 'TS', '250', 'Report title', mt) && IsValid;
            IsValid = CheckReqData('txtPurpose', 'TS', '250', 'Business purpose', mt) && IsValid;

            const RepDate = new Date(new Date(GetJsonDate($('#txtRepDate').val(), '-', 'ddmmyyyy')).toLocaleDateString().slice(0, 10));
            const CurrDate = new Date(new Date().toLocaleDateString().slice(0, 10));
            if (RepDate < CurrDate) {

                IsValid = false;
                ShowError('Report date should be greater than/equal to current date.');
            }

            return IsValid;
        }

        /* To check save response and redirect to expense creation screen */
        function SaveSuccess(resp) {

            if (IsEmpty(resp) || IsEmpty(resp.eR_ID) || resp.eR_ID <= 0) {

                ShowError('Failed to save expense report, please contact admin.');
                return false;
            }
            CreateExpenses(resp.eR_ID);
        }

        /* To redirect to expense creation screen */
        function CreateExpenses(eRID) {

            if ((IsEmpty(eRID) || eRID <= 0) && (IsEmpty(expenseReport) || IsEmpty(expenseReport.eR_ID))) {

                ShowError('Invalid exprense report reference, please contact admin.');
                return false;
            }

            $("#ctl00_upProgress").show();
            AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpCreate', 'sessionData':'" + JSON.stringify({ ExpRepId: (eRID == 0 ? expenseReport.eR_ID : eRID) }) + "', 'action':'set'}");
            window.location.href = "ExpCreate.aspx";
        }

        /* To prepare and get the expense trips entity list for selected trips */
        function GetSelectedTrips() {

            selectedTrips = expTripDetails.length > 0 ? GetSetSelectedRows('get', '') : selectedTrips;
            if (IsEmpty(selectedTrips) || selectedTrips.length == 0)                 
                return [];

            var expRepTripsSelected = [];
            $.each(selectedTrips, function (key, col) {

                var selval = col.split('|');
                expRepTripsSelected.push(GetExpTripEntity(expRepId, (selval[1] == 'FLIGHT' ? 1 : 2), selval[0], true, apiAgentInfo.LoginUserId));
            });
            return expRepTripsSelected;
        }

        /* To clear trips details and hide the div */
        function ClearTrips() {

            RemoveGrid();
            $('#divTrips').hide();
        }

        /* To check report date */
        function CheckDateTitle(dataCntrlId, type, length, fieldName) {

            if (!validateData(dataCntrlId, type, length, fieldName))
                return false;

            ValDuplicateExp('DT');
        }

        /* To validate duplicate records */
        function ValDuplicateExp(action) {

            if (action == 'S' && !ValidateSave())
                return false;

            if (IsEmpty($('#txtRepDate').val()) || IsEmpty($('#txtTitle').val()) || IsEmpty($('#ddlProfiles').val())) 
                return true;

            dcAction = action;

            var pkeys = ('@ER_Date|@ER_Title|@ER_ProfileId|@ER_ID').split('|');
            var pvalues = ($('#txtRepDate').val() + '|' + $('#txtTitle').val() + '|' + selectedProfile + '|' + expRepId).split('|');

            GetDupRecords(apiAgentInfo, apiHost, 'CT_T_EXP_REPORT', pkeys, pvalues, DuplicateResp);            
        }     

        /* To read duplciate records validation response */
        function DuplicateResp(resp) {

            if (IsEmpty(resp) || resp.length == 0) {

                if (dcAction == 'S')
                    SaveExpenseReport();
                return;
            }
            
            var msg = ('Report date: ' + $('#txtRepDate').val() + ', Report title: ' + $('#txtTitle').val() +
                ' combination already exist for profile ' + GetddlSelText('ddlProfiles') + '.');
            ShowError(msg);
            return false;
        }

        /* To get agent and login info */
        function GetAgentInfo() {

            try {

                var loginInfo = '<%=Settings.LoginInfo == null%>';

                if (loginInfo == true)
                    return apiAgentInfo;

                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {

            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';

            if (IsEmpty(apiHost))                                 
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";

            return apiHost;
        }

    </script>

    <div class="c-exp-title-wrapper">
        <div class="row custom-gutter">
            <div class="col-12 col-md-9">
                <div class="d-flex flex-wrap header-blocks">
                    <!-- <h3 class="m-0 title pt-2 float-md-left with-separator"> Expense Report </h3> -->
                    <div class="ml-4 pr-5 item">
                        <span class="icon icon-id-card"></span>
                        <label>Employee ID</label>
                        <select id="ddlProfiles" class="form-control" onchange="EmployeeChange(this.value);"></select>
                    </div>
                    <div class="pr-5 item">
                        <span class="icon icon-user2"></span>
                        <label>Employee Name</label>
                        <div id="divEmpName" class="font-weight-bold"></div>
                    </div>
                    <div class="pr-5 item">
                        <span class="icon icon-budget"></span>
                        <label>Cost Centre</label>
                        <div id="divEmpCostCentre" class="font-weight-bold"></div>
                    </div>
                </div>

            </div>
            <div class="col-12 col-xl-3 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                <a id="btnViewExp" style="display:none" onclick="CreateExpenses(0);" class="btn btn-primary mr-2">VIEW EXPENSES<i class="icon icon-double_arrow"></i><i class="icon icon-keyboard_arrow_right" style="margin-left: -10px;"></i></a>
                <a id="btnAddExp" onclick="return ValDuplicateExp('S');" class="btn btn-primary mr-2">ADD EXPENSE<i class="icon icon-double_arrow"></i><i class="icon icon-keyboard_arrow_right" style="margin-left: -10px;"></i></a>
            </div>
        </div>
    </div>
    <div class="clear"></div>
    <div class="c-exp-main-wrapper">
        <div class="c-exp-left-block" id="createExp-left-block">
            <div class="exp-content-block">
                <div class="form-row">
                    <div class="form-group col-md-2">
                        <span class="red_span">*</span><label>Report Date</label>
                        <input type="text" id="txtRepDate" autocomplete="off" placeholder="Enter date" class="form-control" onchange="return CheckDateTitle(this.id, 'DT', '10', 'Report date');">
                    </div>
                    <div class="form-group col-md-4">
                        <span class="red_span">*</span><label>Report Title</label>
                        <input type="text" id="txtTitle" placeholder="Enter title" class="form-control" 
                            onkeypress="return onKeyPressVal(event, 'TS', '250');" onchange="return CheckDateTitle(this.id, 'TS', '250', 'Report title');">
                    </div>
                    <div class="form-group col-md-2">
                        <span class="red_span">*</span><label>Business Purpose</label>
                        <input type="text" id="txtPurpose" placeholder="Enter business purpose" class="form-control" 
                            onkeypress="return onKeyPressVal(this.id, 'TS', '250');" onchange="return validateData(this.id, 'TS', '250', 'Business purpose');">
                    </div>
                    <div class="form-group col-md-2">
                        <label>Company Visited</label>
                        <input type="text" id="txtCmpVisited" placeholder="Enter company visited" class="form-control" 
                            onkeypress="return onKeyPressVal(this.id, 'TS', '200');" onchange="return validateData(this.id, 'TS', '200', 'company Visited');">
                    </div>
                </div>
                <div class="form-row">
                    <div class="form-group col-md-4">
                        <label>Comments</label>
                        <textarea class="form-control" id="txtComments" placeholder="Enter comments" 
                            onkeypress="return onKeyPressVal(this.id, 'TS', '2000');" onchange="return validateData(this.id, 'TS', '2000', 'Comments');"></textarea>
                    </div>
                    <div class="form-group col-md-8">
                        <div class="button-controls text-right mt-5">
                            <a id="btnIncludeTrips" class="btn btn-primary" onclick="GetAvailableTrips();">Include Trip Requests <i class="icon icon-add"></i></a>
                        </div>
                    </div>
                </div>

            </div>

            <div id="divTrips" class="exp-content-block" style="display:none">
                <h5 class="mb-3">Trip Details</h5>
                <div class="button-controls text-right">
                    <a id="btnDeleteTrips" class="btn btn-danger" onclick="DeleteTrips();">DELETE  <i class="icon icon-delete"></i></a>
                </div>
                <div id="divGridInfo"></div>
            </div>
        </div>
    </div>
       
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
