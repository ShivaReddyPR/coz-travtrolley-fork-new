<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ROSEmployeeReceiptQueueUI" Title="ROS Employee Queue"  ValidateRequest="false" EnableEventValidation="false" Codebehind="ROSEmployeeReceiptQueue.aspx.cs" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%--<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc2" %>--%>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphtransaction" Runat="Server">
<%--<div id="divSearchbar" class="content" style="height:456px;width:100%;" >--%>

<asp:HiddenField runat="server" id="hdfParam" value="1"></asp:HiddenField>
<asp:HiddenField runat="server" id="hdfSelectCount" value="0"></asp:HiddenField>
<asp:HiddenField runat="server" id="hdfCurrentYear" value="0"></asp:HiddenField>
    <table cellpadding="0" cellspacing="0" class="label" >
            <tr>
                    <td style="width:100px" align="right"><a style="cursor:Hand;font-weight:bold;font-size:8pt;color:Black;" id="ancParam" onclick="return ShowHide('divParam');">Hide Parameter</a></td>
          </tr>
         </table>


         <div> 
         
         
             <div class="paramcon" title="Param" id="divParam">


            
 <asp:Panel runat="server" ID="pnlParam" Visible="true">


             
              <div class="marbot_10"> 
             
             <div class="col-md-2"><asp:Label ID="lblFromDate" Text="From Date:" runat="server"  ></asp:Label> </div>

             <div class="col-md-2"> <uc1:DateControl Id="dcFromDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></div>

             <div class="col-md-2"> <asp:Label ID="lblToDate" Text="To Date:" runat="server"></asp:Label></div>

             <div class="col-md-2"> <uc1:DateControl Id="dcToDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></div>

             <div class="col-md-2"><asp:Label ID="lblEmployee" runat="server" Text="Staff:"></asp:Label> </div>

             <div class="col-md-2"><asp:DropDownList ID="ddlStaff"  CssClass="inputDdlEnabled form-control" runat="server"></asp:DropDownList> </div>
             
             <div class="clearfix"></div>
             </div>








                  <div class="marbot_10"> 
             
             <div class="col-md-2"><asp:Label ID="lblCompany" runat="server" Text="Company:"></asp:Label> </div>
             <div class="col-md-2"> <asp:DropDownList ID="ddlCompany" CssClass="inputDdlEnabled form-control" runat="server"></asp:DropDownList></div>
            
             <div class="col-md-2"><asp:Label ID="lblSvcStatus" runat="server" Text="Service Status:"></asp:Label> </div>
             <div class="col-md-2"> 
             
             
             <asp:DropDownList ID="ddlSvcStatus" CssClass="inputDdlEnabled form-control" runat="server" >
            <asp:ListItem Text="All" Value="-1"></asp:ListItem>
            <asp:ListItem Text="Active" Value="A"></asp:ListItem>
            <asp:ListItem Text="Terminated" Value="T"></asp:ListItem>
            </asp:DropDownList>
             
             </div>
             <div class="col-md-2"><asp:Label  ID="lblPaidStatus" runat="server" Text="Paid Status:"></asp:Label> </div>
             <div class="col-md-2"> 
             
             
             <asp:DropDownList ID="ddlPaidStatus"  CssClass="inputDdlEnabled form-control"  runat="server">
            <asp:ListItem Text="All" Value="-1"></asp:ListItem>
            <asp:ListItem Text="Paid" Value="P"></asp:ListItem>
            <asp:ListItem Text="UnPaid" Value="U"></asp:ListItem>
            </asp:DropDownList>
             
             </div>
             
             <div class="clearfix"></div>
             </div>



             <div> 
             

             <div class="col-md-12">

             <asp:Button runat="server" ID="btnSearch" Text="Search"  OnClientClick="return ValidateParam();" CssClass="btn but_b pull-right" OnClick="btnSearch_Click" />
             
             </div>

             </div>


       
        
        
        </asp:Panel>


    </div>
         
         </div>


    <div class="grdScrlTrans" >


    <table id="tabSearch" border="0" cellpadding="0" cellspacing="0">
    <%--<tr>    
    <td align="left" style="width:100%" class="titlebarSearch"><asp:Label ID="lblTitleSearch" runat="server" Text="Search"></asp:Label></td>
    </tr>
    <tr>--%>
    <tr>
    <td>
     <div title="Delete" id="divDelete" style="position:absolute;display:none;">
        <table style="border:solid 1px skyblue;background-color:White" border="0" >
        <tr >
            <td valign="top" align="right"><asp:Label ID="lblRemarks" Text="Remarks:" runat="server" CssClass="label" ></asp:Label></td>
            <td><asp:TextBox ID="txtRemarks" TextMode="multiline" Height="45" width="150px" runat="server" CssClass="inputEnabled" ></asp:TextBox> </td>
        </tr>
         </table>
    </div>
   
    <asp:GridView ID="gvEmployee" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="EMP_ID" 
    EmptyDataText="No Employee List!" AutoGenerateColumns="false" PageSize="21" GridLines="none"  CssClass="table"
     CellPadding="4" CellSpacing="0" PagerSettings-Mode="NumericFirstLast"
    OnPageIndexChanging="gvEmployee_PageIndexChanging" >
    
     <HeaderStyle CssClass=" ns-h3 fcol_fff" HorizontalAlign="Left">
     </HeaderStyle>
     <RowStyle CssClass="gvRow01" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvRow02" />    
    <Columns> 
    
    <%-- <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    
    <HeaderTemplate>
   
    </HeaderTemplate>
    <ItemStyle  />
   
    <ItemTemplate>
    <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" CssClass="label"  Checked='<%# Eval("RECEIPT_ACCOUNTED_STATUS").ToString()=="Y"%>' Enabled='<%# Eval("RECEIPT_ACCOUNTED_UPDATED").ToString()!="U" && Eval("RECEIPT_STATUS").ToString()=="A" %>'></asp:CheckBox>
    </ItemTemplate>    
    </asp:TemplateField>   --%>
     <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' CssClass="label grdof" ToolTip='<%# Container.DataItemIndex %>' Width="40px"></asp:Label>
    </ItemTemplate>    
    
    </asp:TemplateField>  
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="left" />
   <HeaderTemplate>
    <label>Select</label>
    <%--<asp:CheckBox runat="server" id="HTchkSelectAll" AutoPostbACK="true" OnCheckedChanged="ITchkSelect_CheckedChanged"></asp:CheckBox>--%>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:CheckBox ID="ITchkSelect"  OnClick="return SelectRow(this.id);" runat="server" Width="20px" Checked='<%# Eval("checked_status").ToString()=="true"%>' ></asp:CheckBox>
    <%--<asp:Label ID="ITlblRemarks" runat="server" Text='<%# Eval("vs_remarks") %>' CssClass="label grdof"  ToolTip='<%# Eval("vs_remarks") %>' Width="150px"></asp:Label>--%>
    </ItemTemplate>    
    </asp:TemplateField> 
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <label style=" float:left"> Employee Name
    </label>             
    </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblEmpName" runat="server" Text='<%# Eval("empName") %>' CssClass="label grdof" ToolTip='<%# Eval("empName") %>' Width="150px"></asp:Label>
    <asp:HiddenField id="IThdfEmpId" runat="server" Value='<%# Bind("emp_Id") %>'></asp:HiddenField>   
    </ItemTemplate>    
    
    </asp:TemplateField>  
    
    <%--<asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
    <table>    
    <tr>
    
    <td>
    <uc1:DateControl ID="HTtxtReceiptDate" runat="server" DateOnly="true" />
    </td>
    <td>
    <asp:ImageButton ID="HTbtnReceiptDate" runat="server" ImageUrl="~/Images/wg_filter.GIF" ImageAlign="AbsMiddle" OnClick="Filter_Click" />
    </td>
    <td>
    <asp:ImageButton ID="HTbtnChequeReturnDateAdvance"  runat="server" ImageUrl="~/Images/wg_filterAdvance.GIF"  ImageAlign="AbsMiddle"  OnClientClick="return validateAdvanceFilter(this.id,'A');" Alt="Advance Filter" /></td>
    </tr></table>
    <label style="width:140px; float:left" class="">Date</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblReceiptDate" Width="120px" runat="server" Text='<%# IDDateTimeFormat(Eval("receipt_date")) %>' CssClass="label grdof"  ToolTip='<%# Eval("receipt_date") %>' ></asp:Label>                
    </ItemTemplate>                
    </asp:TemplateField>  --%>
   
   <asp:TemplateField><HeaderTemplate>
    <label style=" float:left">Staff Id</label>                
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblStaffId" runat="server" Text='<%# Eval("staff_id") %>' CssClass="label grdof"  ToolTip='<%# Eval("staff_id") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField><HeaderTemplate>
    <label style=" float:left"><%--<cc2:Filter ID="HTtxtServiceStatus" HeaderText="Staff Id" Width="120px" OnClick="Filter_Click" runat="server" />--%> 
    Location
    </label>                
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblLocation" runat="server" Text='<%# Eval("loc_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("loc_name") %>' Width="150px"></asp:Label>                
    <asp:HiddenField id="IThdfCompany" runat="server" Value='<%# Bind("company") %>'></asp:HiddenField>   
    </ItemTemplate>    
    </asp:TemplateField>
    
     <asp:TemplateField><HeaderTemplate>
    <label style=" float:left">Tariff Code </label>                
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblTariffCode" runat="server" Text='<%# Eval("TRF_CODE") %>' CssClass="label grdof"  ToolTip='<%# Eval("TRF_CODE") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
         <asp:TemplateField><HeaderTemplate>
    <label style=" float:left">Tariff Amount</label>                
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblTariffAmount" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("emp_trf_amount")) %>' CssClass="label grdof"  ToolTip='<%# Eval("emp_trf_amount") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
     <label style="width:140px; float:left" class="">Last Rcpt Date</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblReceiptDate" Width="120px" runat="server" Text='<%# IDDateTimeFormat(Eval("emp_last_recpt_date")) %>' CssClass="label grdof"  ToolTip='<%# Eval("emp_last_recpt_date") %>' ></asp:Label>                
    </ItemTemplate>                
    </asp:TemplateField> 
    
      <asp:TemplateField><HeaderTemplate>
    <label style=" float:left">Last Receipt No </label>                
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblStaffNo" runat="server" Text='<%# Eval("emp_last_recpt_no") %>' CssClass="label grdof"  ToolTip='<%# Eval("emp_last_recpt_no") %>' Width="150px"></asp:Label>                
    <asp:HiddenField id="IThdfLstReceiptId" runat="server" Value='<%# Bind("emp_last_recpt_id") %>'></asp:HiddenField>   
    </ItemTemplate>    
    </asp:TemplateField>
    
    
     <asp:TemplateField><HeaderTemplate>
    <label style=" float:left">Last Receipt Amount </label>                
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblRcptAmount" runat="server" Text='<%# CT.TicketReceipt.Common.Formatter.ToCurrency(Eval("emp_last_recpt_Amount")) %>' CssClass="label grdof"  ToolTip='<%# Eval("emp_last_recpt_Amount") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    
      <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
     <label style="width:140px; float:left" class="">Period From</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblReceiptFromDate" Width="120px" runat="server" Text='<%# IDDateTimeFormat(Eval("emp_recpt_from")) %>' CssClass="label grdof"  ToolTip='<%# Eval("emp_recpt_from") %>' ></asp:Label>                
    </ItemTemplate>                
    </asp:TemplateField> 
    
     <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
     <label style="width:140px; float:left" class="">Period To</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblReceiptToDate" Width="120px" runat="server" Text='<%# IDDateTimeFormat(Eval("emp_recpt_to")) %>' CssClass="label grdof"  ToolTip='<%# Eval("emp_recpt_to") %>' ></asp:Label>                
    </ItemTemplate>                
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>
     <label style="width:140px; float:left" class="">Month</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:DropDownList ID="ITddlMonth" Width="120px" runat="server" CssClass="label grdof"  onChange="return ValidateInvPeriod(this.id);" disabled="true" ToolTip='<%# Eval("emp_recpt_to") %>' >
    <asp:ListItem Text="--Select Month--" Value="0"></asp:ListItem>
    <asp:ListItem Text="JAN" Value="1"></asp:ListItem>
    <asp:ListItem Text="FEB" Value="2"></asp:ListItem>
    <asp:ListItem Text="MAR" Value="3"></asp:ListItem>
    <asp:ListItem Text="APR" Value="4"></asp:ListItem>
    <asp:ListItem Text="MAY" Value="5"></asp:ListItem>
    <asp:ListItem Text="JUN" Value="6"></asp:ListItem>
    <asp:ListItem Text="JUL" Value="7"></asp:ListItem>
    <asp:ListItem Text="AUG" Value="8"></asp:ListItem>
    <asp:ListItem Text="SEP" Value="9"></asp:ListItem>
    <asp:ListItem Text="OCT" Value="10"></asp:ListItem>
    <asp:ListItem Text="NOV" Value="11"></asp:ListItem>
    <asp:ListItem Text="DEC" Value="12"></asp:ListItem>
    </asp:DropDownList>                
    </ItemTemplate>                
    </asp:TemplateField>
     
      <asp:TemplateField><HeaderTemplate>
    <label style=" float:left"><%--<cc2:Filter ID="HTtxtServiceStatus" HeaderText="Staff Id" Width="120px" OnClick="Filter_Click" runat="server" />--%> 
    Service Status
    </label>                
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblServiceStatus" runat="server" Text='<%# Eval("emp_service_status_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("emp_service_status_name") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField><HeaderTemplate>
    <label style=" float:left"><%--<cc2:Filter ID="HTtxtServiceStatus" HeaderText="Staff Id" Width="120px" OnClick="Filter_Click" runat="server" />--%> 
    Last Inv Month
    </label>                
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblLastInvMonth" runat="server" Text='<%# Eval("LastInvPeriond") %>' CssClass="label grdof"  ToolTip='<%# Eval("LastInvPeriond") %>' Width="150px"></asp:Label>                
    <asp:HiddenField id="IThdfLastInvMonth" runat="server" Value='<%# Bind("lastInvMonth") %>'></asp:HiddenField>   
    <asp:HiddenField id="IThdfLastInvYear" runat="server" Value='<%# Bind("lastInvYear") %>'></asp:HiddenField>   
    </ItemTemplate>    
    </asp:TemplateField>
    
     <asp:TemplateField><HeaderTemplate>
    <label style=" float:left"><%--<cc2:Filter ID="HTtxtServiceStatus" HeaderText="Staff Id" Width="120px" OnClick="Filter_Click" runat="server" />--%> 
    Paid Status
    </label>                
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblPaidStatus" runat="server" Text='<%# Eval("emp_paid_status_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("emp_paid_status_name") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    
   
    
    <asp:TemplateField>
    <HeaderStyle  width="100px"/>
    <HeaderTemplate>
    <label style="width:140px; float:left" class="">Print</label>    
    
    <%--<cc2:Filter ID="HTtxtStatus" TextBoxWidth="60px" OnClick="Filter_Click" runat="server" />                 --%>
    </HeaderTemplate>
    <ItemStyle />
    <ItemTemplate>
    <asp:LinkButton ID="ITlnkPrint" runat="Server"  onClick="ITlnkPrint_Click" Text="Print" width="70px"   Visible='<%# (bool)StatusVisible(Eval("emp_last_recpt_id"))  %>'    ></asp:LinkButton>
    <%--<asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("receipt_status_name") %>' ToolTip='<%# Eval("receipt_status_name") %>' Width="60px"></asp:Label>--%>
    </ItemTemplate>    
    </asp:TemplateField>     
     <asp:TemplateField>
    <HeaderStyle  />
    <HeaderTemplate>   
    <label style="width:140px; float:left" class="">Generate Invoice</label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Button ID="ITbtnGenerateInvoice" runat="server" Text="GenerateInvoice"  OnClientClick="return ValidateSave(this.id);" disabled="true" OnClick= "ITbtnGenerateInvoice_Click" CssClass="button"  Width="120px"></asp:Button>
    </ItemTemplate>                
    </asp:TemplateField> 
    
   
    </Columns>           
    </asp:GridView>
    </td></tr>
    <tr>
        <td align="left">
        <table>
        <tr>
        <td width="80px"><asp:Button OnClick="btnExport_Click"  Visible="false" runat="server" id="btnExport" Text="Export To Excel"  CssClass="button" /> </td>
        <td>
        </td>
        
        </tr>
        </table>
         </td>
    </tr>
    </table>
    
   
    
    
    </div>
    <%--<asp:Label Cssclass="grandTotal" id="lblGrandText" runat="server" text="Grand Total Of Total Fare:">
    </asp:Label><asp:Label Cssclass="grandTotal" id="lblGrandTotal" runat="server" text=""></asp:Label>--%>
    
  <%--  </div>--%>

 <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
<asp:Label style="COLOR: #dd1f10" id="lblError" runat="server"></asp:Label> 
<div>
    <asp:DataGrid ID="dgReceiptList" runat="server" AutoGenerateColumns="false" >
    <Columns>
    <asp:BoundColumn HeaderText="Receipt Number" DataField="receipt_number" HeaderStyle-Font-Bold="true" ></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Date" DataField="receipt_date" HeaderStyle-Font-Bold="true"></asp:BoundColumn>   
     
     
    <asp:BoundColumn HeaderText="Pax Name" DataField="receipt_pax_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>  
    <asp:BoundColumn HeaderText="Adults" DataField="receipt_adults" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Children" DataField="receipt_children" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="Infant" DataField="receipt_Infants" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Fare" DataField="receipt_fare" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    
    <asp:BoundColumn HeaderText="Total Fare" DataField="receipt_total_fare" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    
    <asp:BoundColumn HeaderText="Coll.Mode" DataField="receipt_settlement_mode_description" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Currency" DataField="receipt_currency_code" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="CASH" DataField="cash_base_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="CREDIT" DataField="credit_base_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="CARD" DataField="card_base_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="EMPLOYEE" DataField="employee_base_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="OTHERS" DataField="others_base_amount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Mode Remarks" DataField="mode_remarks" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Destination" DataField="receipt_destination" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    
    
    
    <asp:BoundColumn HeaderText="User" DataField="receipt_created_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Location" DataField="receipt_location_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Remarks" DataField="receipt_remarks" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Status" DataField="receipt_status" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    </Columns>
    </asp:DataGrid>
    </div>

<script  type="text/javascript">


function validateAdvanceFilter(id,mode)
 {

        if(mode=='A')
        {       
             
             getElement('pnlAdvanceFilterDocDate').style.display="block";  
             getElement('divAdvanceFilterDocDate').style.display="block";
             var positions=getRelativePositions(document.getElementById(id));                        
             getElement('divAdvanceFilterDocDate').style.left= positions[0]+'px';
             getElement('divAdvanceFilterDocDate').style.top=(positions[1]+20)+'px';   
               
        }
        else
        {
             getElement('pnlAdvanceFilterDocDate').style.display="none";  
             getElement('divAdvanceFilterDocDate').style.display="none"; 
             getElement('dcDocDateTo_Date').value=''; 
             getElement('dcDocDateFrom_Date').value=''; 
         
        }
        return false;
    
 }
 function getRelativePositions(obj)
    {
        var curLeft = 0;
        var curTop = 0;
        if(obj.offsetParent)
        {
            do
            {
                curLeft+=obj.offsetLeft;
                curTop+=obj.offsetTop;                
            }while(obj=obj.offsetParent);
            
        }
        return [curLeft, curTop];
    }
 function getElement(id)
 {
    return document.getElementById('ctl00_cphTransaction_'+id);
 }
 
 function ValidateDate(id)
 {
  var fromDate =GetDateObject('ctl00_cphTransaction_dcDocDateFrom');
  var toDate =GetDateObject('ctl00_cphTransaction_dcDocDateTo');   
  if(fromDate!=null && toDate!= null && (fromDate>toDate)) addMessage('To-Date should be later than or equal to From-Date !','');
  if(getMessage()!=''){ 
  alert(getMessage()); clearMessage(); return false; }
 }
function ShowHide(div)
{
    if(getElement('hdfParam').value=='1')
        {
            //alert(document.getElementById(div));
            //alert(document.getElementById(div).innerHtml);
            //getElement(div).value;
            document.getElementById('ancParam').innerHTML='Show Param'
            document.getElementById(div).style.display='none';
            getElement('hdfParam').value='0';
        }
        else
        {
            document.getElementById('ancParam').innerHTML='Hide Param'
           document.getElementById('ancParam').value='Hide Param'
            document.getElementById(div).style.display='block';
            getElement('hdfParam').value='1';
        }
}
function ValidateParam()
{

    clearMessage();
    
    var fromDate = GetDateObject('ctl00_cphTransaction_dcFromDate');
    var toDate=GetDateObject('ctl00_cphTransaction_dcToDate');   
    if(fromDate==null) addMessage('Please select From Date !','');   
    if(toDate==null) addMessage('Please select To Date !','');   
    if((fromDate!=null && toDate!=null) && fromDate>toDate) addMessage('From Date should not be later than To Date!','');
    if(getMessage()!=''){ 
    alert(getMessage());    
    clearMessage(); return false; }
}
function SelectRow(chkId) {
    var Id = chkId.substring(0, chkId.lastIndexOf('_') + 1)
    if (document.getElementById(chkId).checked == true) {
        if (getElement('hdfSelectCount').value == '1') {
            alert('Multiple selection is not allowed ');
            return false;
        }
        else {
            getElement('hdfSelectCount').value = "1";
            document.getElementById(Id + 'ITbtnGenerateInvoice').disabled = false;
            document.getElementById(Id + 'ITddlMonth').disabled = false;
            
        }
    }
    else {
        getElement('hdfSelectCount').value = "0";
        document.getElementById(Id + 'ITbtnGenerateInvoice').disabled = true;
        document.getElementById(Id + 'ITddlMonth').disabled = true;
    }
    return true;
}
function ValidateInvPeriod(chkId) {
    var Id = chkId.substring(0, chkId.lastIndexOf('_') + 1)
    
    var currentTime = new Date()
    var curYear = currentTime.getFullYear();
    var slectedMonth = parseInt(document.getElementById(Id + 'ITddlMonth').value);
    var lastinvMonth = parseInt(document.getElementById(Id + 'IThdfLastInvMonth').value);
    var lastInvYear = document.getElementById(Id + 'IThdfLastInvYear').value;
    if (curYear == lastInvYear) {
       
        if (slectedMonth <= lastinvMonth) {
            addMessage('Invoice is already generated for the selected Month,please select another month !')
            document.getElementById(Id + 'ITddlMonth').selectedIndex = 0;
        }
        
    }

    if (getMessage() != '') {
        alert(getMessage());
    //ShowMessageDialog('Receipt List',getMessage(),'Information');
        clearMessage(); return false;
}
}
function ValidateSave(chkId) {
    var Id = chkId.substring(0, chkId.lastIndexOf('_') + 1)
    if (document.getElementById(Id + 'ITddlMonth').selectedIndex == 0) addMessage('Please select the Month for the Invoice generation !')

    if (getMessage() != '') {
        alert(getMessage());
        //ShowMessageDialog('Receipt List',getMessage(),'Information');
        clearMessage(); return false;
    }
}

</script>
</asp:Content>

