<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="AddTermsAndCondition" Title="Cozmo Travels"
    ValidateRequest="false" Codebehind="AddTermsAndCondition.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<link rel="stylesheet" type="text/css" href="css/style.css" />
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <style type="text/css">
        /*margin and padding on body element
  can introduce errors in determining
  element position and are not recommended;
  we turn them off as a foundation for yui
  CSS treatments. */body
        {
            margin: 0;
            padding: 0;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="yui/build2/menu/assets/skins/sam/menu.css" />
    <link rel="stylesheet" type="text/css" href="yui/build2/button/assets/skins/sam/button.css" />
    <link rel="stylesheet" type="text/css" href="yui/build2/fonts/fonts-min.css" />
    <link rel="stylesheet" type="text/css" href="yui/build2/container/assets/skins/sam/container.css" />
    <link rel="stylesheet" type="text/css" href="yui/build2/editor/assets/skins/sam/editor.css" />

    <script type="text/javascript" src="yui/build2/yahoo-dom-event/yahoo-dom-event.js"></script>

    <script type="text/javascript" src="yui/build2/element/element-min.js"></script>

    <script type="text/javascript" src="yui/build2/container/container-min.js"></script>

    <script type="text/javascript" src="yui/build2/menu/menu-min.js"></script>

    <script type="text/javascript" src="yui/build2/button/button-min.js"></script>

    <script type="text/javascript" src="yui/build2/editor/editor-min.js"></script>
    
    
    
            <div> 
    <div class="col-md-6"> <h4> <asp:Label runat="server" ID="lblStatus" Text="Add Visa Terms And Conditions"></asp:Label></h4> </div>
    
    <div class="col-md-6">
    
    <a class="fcol_blue pull-right" href="VisaTermsAndConditionList.aspx"> Go Back</a>
    
    

 </div>     
    
    <div class="clearfix"> </div> 
    </div>




<div class="paramcon"> 


         <div class="margin-bottom-10"> 
    <div class="col-md-2"><label>
                        Agent<sup>*</sup>
                    </label> </div>
    
    <div class="col-md-2"> <asp:DropDownList ID="ddlAgent" CssClass="form-control" runat="server">
                        </asp:DropDownList>
                        
            <asp:RequiredFieldValidator ID="RequiredFieldValidator10" Display="Dynamic" runat="server" ErrorMessage="Please select a Agent "
                        ControlToValidate="ddlAgent" InitialValue="Select Agent"></asp:RequiredFieldValidator>            
                        
                        
                        </div>     
    
    <div class="clearfix"> </div> 
    </div>
    
  <div class="margin-bottom-10"> 
    <div class="col-md-2"><label>
                        Country<sup style="color: Red">*</sup></label> </div>
    
    <div class="col-md-2"> 
    

 
                  
                        <asp:DropDownList ID="ddlCountry" class="form-control" runat="server">
                        </asp:DropDownList>
                  
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" Display="Dynamic" runat="server" ErrorMessage="Please select country"
                        ControlToValidate="ddlCountry" InitialValue="Select"></asp:RequiredFieldValidator>
              
    
    
    </div>     
    
    <div class="clearfix"> </div> 
    </div>
   
      <div class="margin-bottom-10"> 
    <div class="col-md-2"> <label>
                        Description</label></div>
    
    <div class="col-md-4"><asp:TextBox ID="editor" runat="server" TextMode="MultiLine"
                            Height="200" Width="100%"></asp:TextBox> </div>     
    
    <div class="clearfix"> </div> 
    </div>



     <div> 
    <div class="col-md-2"> </div>
    
    <div class="col-md-2"> <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn but_b"
                                OnClick="btnSave_Click" /></div>     
    
    <div class="clearfix"> </div> 
    </div>
    
    

</div>

    
    

    
    
    
    
</asp:Content>
