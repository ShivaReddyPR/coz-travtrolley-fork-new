﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpExchangeMaster.aspx.cs" Inherits="CozmoB2BWebApp.ExpExchangeMaster" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <link href="css/toastr.css" rel="stylesheet" type="text/css" />
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>

    <script>

        /* Global variables */
        var apiHost = ''; var cntrllerPath = 'api/expenseMasters';
        var apiAgentInfo = {}; var expRepId = 0; var selectedRows = [];
        var countryData = {}; var baseCurrencyData = {}; var currencyData = {};       
        var EM_Id = 0;
        var agentdata = {}; var selectedProfile = 0;

        /* Page Load */
        $(document).ready(function () {
            /* Check query string to see if existing report needs to open */
            expRepId = '<%=Request.QueryString["ExpRepId"] == null ? "0" : Request.QueryString["ExpRepId"]%>';

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {
                alert('Session expired, please login once again.');
                return;
            }

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            /* to get screen dropdown list data */
            GetScreenData();

        });

        var cal1;

        function init() {
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());            
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();
        }
        function showCal1() {
            $('container1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
        }

        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('EM_Date').value = day + "/" + (month) + "/" + date1.getFullYear();
            cal1.hide();
        }

        YAHOO.util.Event.addListener(window, "load", init);

        /* To get agent and login info */
        function GetAgentInfo() {
            try {
                var loginInfo = '<%=Settings.LoginInfo == null%>';
                if (loginInfo == true)
                    return apiAgentInfo;

                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {
            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';
            if (IsEmpty(apiHost))
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";
            return apiHost;
        }

        /* To get agent and currency */
        function GetScreenData() {
            var reqData = { AgentInfo: apiAgentInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getExchangeMasterScreenLoadData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);
        }

        /* To bind exchnage master agent, Currency */
        function BindScreenData(screenData) {
            agentdata = {};
            if (!IsEmpty(screenData.dtAgents)) {

                agentdata = screenData.dtAgents;
                var options = agentdata.length != 1 ? GetddlOption('', '--Select Agent--') : '';
                $.each(agentdata, function (key, col) {

                    options += GetddlOption(col.agenT_ID, col.agenT_NAME);
                });
                $('#ddlAgentId').empty();
                $('#ddlAgentId').append(options);
                $("#ddlAgentId").select2("val", '');
                if (agentdata.length == 1) {

                    $('#ddlAgentId').attr('disabled', 'disabled');
                    $("#ddlAgentId").select2("val", agentdata[0].agenT_ID);
                }
            }
            if (!IsEmpty(screenData.dtCurrency))
                BindBaseCurrencyData(screenData.dtCurrency);
            if (!IsEmpty(screenData.dtCurrency))
                BindCurrencyData(screenData.dtCurrency);
        }

        /* To bind baseCurrency data to baseCurrency control */
        function BindBaseCurrencyData(cData) {
            baseCurrencyData = {};
            if (IsEmpty(cData)) {
                $('#ddlBaseCurrency').empty();
                $("#ddlBaseCurrency").select2("val", '');
                return;
            }
            baseCurrencyData = cData;
            var options = baseCurrencyData.length != 1 ? GetddlOption('', '--Select BaseCurrency--') : '';
            $.each(baseCurrencyData, function (key, col) {

                options += GetddlOption(col.currency_code, col.currency_code);
            });
            $('#ddlBaseCurrency').empty();
            $('#ddlBaseCurrency').append(options);
            $("#ddlBaseCurrency").select2("val", '');
            if (baseCurrencyData.length == 1) {
                $("#ddlBaseCurrency").select2("val", baseCurrencyData[0].currency_code);
            }
        }

        /* To bind currency data to currency control */
        function BindCurrencyData(curData) {
            currencydata = {};
            if (IsEmpty(curData)) {
                $('#ddlCurrency').empty();
                $("#ddlCurrency").select2("val", '');
                return;
            }
            currencydata = curData;
            var options = GetddlOption('', '--Select Currency--');
            $.each(currencydata, function (key, col) {

                options += GetddlOption(col.currency_code, col.currency_code);
            });

            $('#ddlCurrency').empty();
            $('#ddlCurrency').append(options);
            $("#ddlCurrency").select2("val", '');
        }

        /* To prepare and get the exchange entity list for selected rows */
        function GetSelectedRows() {
            selectedRows = GetSetSelectedRows();
            if (IsEmpty(selectedRows) || selectedRows.length == 0)
                return [];
            var expMSelected = [];
            $.each(selectedRows, function (key, col) {

                var eMInfo = ecInfoDetails.find(item => item.eM_ID == col);
                eMInfo.eM_Status = false;
                eMInfo.eM_CreatedBy = apiAgentInfo.LoginUserId;
                expMSelected = expMSelected.concat(eMInfo);
            });
            return expMSelected;
        }

        /* To prepare and set exchnage info entity */
        function RefreshGrid(resp) {
            $.each(selectedRows, function (key, col) {

                var emid = parseInt(col);
                ecInfoDetails = ecInfoDetails.filter(item => (item.eM_ID) !== emid);
            });
            if (ecInfoDetails.length == 0) {
                BindExchangeInfo(ecInfoDetails);
                return;
            }
            selectedRows = [];
            GetSetSelectedRows('set', selectedRows);
            GetSetDataEntity('set', ecInfoDetails);
            LoadData(GetSetCurrentPageNo('get', ''));
        }

        /* To Load expense exchange info */
        function LoadExchangeInfo() {
            ClearControls();
            var eM_AgentId = $("#ddlAgentId").val();
            if (eM_AgentId == "" || eM_AgentId == "--Select Agent--") {
                toastr.error('Please select agent');
                $('#ddlAgentId').parent().addClass('form-text-error');
                return false;
            }
            var reqData = { AgentInfo: apiAgentInfo, AgentId: eM_AgentId };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getExchangeData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindExchangeInfo, null, null);
        }

        /* To bind exchange info to grid */
        function BindExchangeInfo(ecInfo) {
            if (ecInfo.length == 0) {

                ShowError('no data available for selected agent.');
                ClearInfo();
                return;
            }
            ecInfoDetails = ecInfo;
            var gridProperties = GetGridPropsEntity();
            gridProperties.headerColumns = ('Base Currency|Currency|Rate|Date|Remarks').split('|');
            gridProperties.displayColumns = ('eM_BaseCurrency|eM_Currency|eM_Rate|eM_Date|eM_Remarks').split('|');
            gridProperties.pKColumnNames = ('eM_ID').split('|');
            gridProperties.dataEntity = ecInfo;
            gridProperties.divGridId = 'ExchangeList';
            gridProperties.headerPaging = true;
            gridProperties.gridSelectedRows = selectedRows;
            gridProperties.selectType = 'M';
            gridProperties.recordsperpage = 5;

            EnablePagingGrid(gridProperties);
            $('#EList').show();
        }

        /* To save exchange info */
        function Save() {
            var eM_AgentId = $("#ddlAgentId").val();
            var eM_BaseCurrency = $("#ddlBaseCurrency").val();
            var eM_Currency = $("#ddlCurrency").val();
            var eM_Rate = $("#EM_Rate").val();
            var eM_Date = $("#EM_Date").val();
            var eM_Remarks = $("#EM_Remarks").val();

            if ((eM_AgentId == "" || eM_AgentId == "--Select Agent--") || (eM_BaseCurrency == "" || eM_BaseCurrency == "--Select BaseCurrency--" || eM_BaseCurrency == null) || (eM_Currency == "" || eM_Currency == "--Select Currency--" || eM_Currency == null) || (eM_Rate == "" || eM_Rate == "Enetr Rate")) {
                if (eM_AgentId == "" || eM_AgentId == "--Select Agent--") {
                    toastr.error('Please Select Agent');
                    $('#ddlAgentId').parent().addClass('form-text-error');
                }
                if (eM_BaseCurrency == "" || eM_BaseCurrency == "--Select BasceCurrency--" || eM_BaseCurrency == null) {
                    toastr.error('Please Select BaseCurrency');
                    $('#ddlBaseCurrency').parent().addClass('form-text-error');
                }
                if (eM_Currency == "" || eM_Currency == "--Select Currency--" || eM_Currency == null) {
                    toastr.error('Please Select Currency');
                    $('#ddlCurrency').parent().addClass('form-text-error');
                }
                if (eM_Rate == "" || eM_Rate == "Enter Rate") {
                    toastr.error('Please Enter Rate');
                    $("#EM_Rate").addClass('form-text-error');
                }
                return false;
            }

            var exchangeInfo = { EM_ID: EM_Id, EM_AgentId: eM_AgentId, EM_BaseCurrency: eM_BaseCurrency, EM_Currency: eM_Currency, EM_Rate: eM_Rate, EM_Date: eM_Date, EM_Remarks: eM_Remarks, EM_Status: true, EM_CreatedBy: apiAgentInfo.LoginUserId, EM_ModifiedBy: apiAgentInfo.LoginUserId }
            var reqData = { AgentInfo: apiAgentInfo, ExchangeInfo: exchangeInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveExchangeInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', StatusConfirm, null, null);
        }

        /* To bind status of exchange info */
        function StatusConfirm() {
            if (EM_Id > 0) {
                alert('record updated successfully!');
            }
            else if (EM_Id == 0) {
                alert('record added successfully!');
            }
            ClearInfo();
            LoadExchangeInfo();
        }

        /* To update selected exchange info from the grid and update the status in data base */
        function EditExchangeInfo() {
            var delEInfo = GetSelectedRows();
            if (IsEmpty(delEInfo) || delEInfo.length == 0) {
                ShowError('Please check any one of the row in below table');
                return;
            }
            if (IsEmpty(delEInfo) || delEInfo.length > 1) {
                ShowError('Please check one of the row in below table');
                return;
            }
            var ecDetails = ecInfoDetails.find(item => item.eM_ID == selectedRows[0]);
            EM_Id = ecDetails.eM_ID;
            $("#ddlAgentId").select2('val', ecDetails.eM_AgentId);
            $("#ddlBaseCurrency").select2('val', ecDetails.eM_BaseCurrency);
            $("#ddlCurrency").select2('val', ecDetails.eM_Currency);
            $("#EM_Rate").val(parseFloat(ecDetails.eM_Rate).toFixed(2));
            $("#EM_Date").val(ecDetails.eM_Date);
            $("#EM_Remarks").val(ecDetails.eM_Remarks);
        }

        /* To delete selected rows from the grid and update the status in data base */
        function Delete() {
            var delEInfo = GetSelectedRows();
            if (IsEmpty(delEInfo) || delEInfo.length == 0) {

                ShowError('Please select record to delete');
                return;
            }
            var reqData = { AgentInfo: apiAgentInfo, delEInfo, Status: "DELETE" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveExchangeInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', RefreshGrid, null, null);
        }

        /* To clear exchange details and hide the div */
        function ClearInfo() {
            selectedRows = [];
            selectedProfile = 0;            
            EM_Id = 0;
            ClearControls();
            RemoveGrid();
            $('#EList').hide();
        }

        /* To clear control details */
        function ClearControls() {
            $("#EM_Rate").val('');
            $("#EM_Remarks").val('');
            $("#EM_Date").val('');
            $("#ddlBaseCurrency").select2('val', '');
            $("#ddlCurrency").select2('val', '');

            $("#ddlAgentId").parent().removeClass('form-text-error');
            $("#EM_Rate").removeClass('form-text-error');
            $("#EM_Remarks").removeClass('form-text-error');
            $("#EM_Date").removeClass('form-text-error');
            $("#ddlCurrency").parent().removeClass('form-text-error');
            $("#ddlBaseCurrency").parent().removeClass('form-text-error');
        }

        /* textbox allows only numeric */
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31 && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

    </script>

    <iframe id="IShimFrame" style="position: absolute; display: none;"></iframe>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; left: 250px; display: none; z-index: 9999">
        </div>
    </div>
    <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">
                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left">Exchange Master</h3>
                    </div>
                </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <button class="btn btn-info mr-2" id="showExchangeList" type="button" data-toggle="collapse" data-target="#EList" aria-expanded="false" aria-controls="ExList" onclick="LoadExchangeInfo()">VIEW ALL <i class="icon icon-search "></i></button>
                    <button type="button" class="btn btn-gray mr-2" onclick="ClearInfo();">CLEAR <i class="icon icon-close "></i></button>
                    <button type="button" class="btn btn-primary" onclick="Save();">SUBMIT <i class="icon icon-play_arrow "></i></button>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">
                        <div class="form-group col-md-3">
                            <label>Agent</label><span style="color: red;">*</span>
                            <select name="Agent" id="ddlAgentId" class="form-control"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>BaseCurrency</label><span style="color: red;">*</span>
                            <select name="BaseCurrency" id="ddlBaseCurrency" class="form-control"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Currency</label><span style="color: red;">*</span>
                            <select name="Currency" id="ddlCurrency" class="form-control"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Rate</label><span style="color: red;">*</span>
                            <input type="text" id="EM_Rate" class="form-control" placeholder="Enter Rate" onkeypress="return isNumberKey(event)" />
                        </div>
                        <div class="form-group col-md-3">
                            <label>Date</label>
                            <div class="form-group">
                                <div class="input-group date fromDate">
                                    <input type="text" id="EM_Date" placeholder="DD/MM/YYYY" class="inputEnabled form-control" />
                                    <label class="input-group-addon" onclick="showCal1()">
                                        <img src="images/call-cozmo.png" alt="Pick Date" style="margin-left: -13px;" /></label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Remarks</label>
                            <input type="text" class="form-control" id="EM_Remarks" maxlength="500" placeholder="Enter Remarks" />
                        </div>
                    </div>
                </div>
                <div class="exp-content-block collapse" id="EList" style="display: none;">
                    <h5 class="mb-3 float-left">All Exchanges List</h5>
                    <div class="button-controls text-right">
                        <button class="btn btn-edit" type="button" onclick="EditExchangeInfo();">EDIT  <i class="icon icon-edit"></i></button>
                        <button class="btn btn-danger" type="button" onclick="Delete();">DELETE  <i class="icon icon-delete"></i></button>
                    </div>
                    <div class="clear"></div>
                    <div class="table-responsive" id="ExchangeList"></div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
