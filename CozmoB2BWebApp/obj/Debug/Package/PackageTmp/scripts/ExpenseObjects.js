﻿/**
 * JS file to get all expense table related objects for API purpose
 */

/* To set and get CT_T_EXP_REPORT table related entity */
function GetExpReportEntity(eRId, eRRefNo, eRDate, eRTitle, eRPurpose, eRCompVisited,
    eRComments, eRProfileId, eRProfileCC, eRApproved, eRStatus, userId, eRTrips) {

    return expRepEntity = {

        eR_ID: eRId,
        eR_RefNo: eRRefNo,
        eR_Date: eRDate,
        eR_Title: eRTitle,
        eR_Purpose: eRPurpose,
        eR_CompVisited: eRCompVisited,
        eR_Comments: eRComments,
        eR_ProfileId: eRProfileId,
        eR_ProfileCC: eRProfileCC,
        eR_Aprroved: eRApproved,
        eR_Status: eRStatus,
        eR_CreatedBy: userId,
        expenseReportTrips: eRTrips
    };
}

/* To set and get CT_T_EXP_REPORTTRIPS table related entity */
function GetExpTripEntity(expRepId, prodId, tripRefId, status, userId) {

    return expTripEntity = {

        erT_ER_Id: expRepId,
        erT_ProductId: prodId,
        erT_REF_Id: tripRefId,
        erT_Status: status,
        erT_CreatedBy: userId
    };
}

/* To set and get CT_M_EXP_ATTENDEEMASTER table related entity */
function GetAttendeeMasterObj(EAMID, EAMAgentId, EAMTitle, EAMName, EAMType, EAMCVId, EAMStatus, userid) {

    return {

        EAM_ID: EAMID,
        EAM_AgentId: EAMAgentId,
        EAM_Title: EAMTitle,
        EAM_Name: EAMName,
        EAM_Type: EAMType,
        EAM_CV_Id: EAMCVId,
        EAM_Status: EAMStatus,
        EAM_CreatedBy: userid
    };
}

/* To set and get CT_T_EXP_ATTENDEES table related entity */
function GetExpAttendeeObj(eaid, eaedid, eaeamid, eaamount, eastatus, userid) {

    return {

        EA_Id: eaid,
        EA_ED_Id: eaedid,
        EA_EAM_Id: eaeamid,
        EA_Amount: eaamount,
        EA_Status: eastatus,
        EA_CreatedBy: userid
    };
}

/* To set and get CT_T_EXP_FLEXDATA table related entity */
function GetExpFlexObj(EFDId, EFDEDId, EFDEFId, EFDLabel, EFDData, EFDType, EFDStatus, userid) {

    return {

        EFD_Id: EFDId,
        EFD_ED_Id: EFDEDId,
        EFD_EF_Id: EFDEFId,
        EFD_Label: EFDLabel,
        EFD_Data: EFDData,
        EFD_Type: EFDType,
        EFD_Status: EFDStatus,
        EFD_CreatedBy: userid
    };
}

/* To set and get CT_T_EXP_REPORTDATA table related entity */
function GetExpDataObj(EDId, EDERId, EDETId, EDTransDate, EDPTId, EDCMId, EDReceiptStatus, EDECTId, EDUOM, EDUnits,
    EDUnitPrice, EDCurrency, EDExcRate, EDExcAmount, EDTax, EDTotalAmount, EDIsBillable, EDCVId, EDClaimExpense, EDComment,
    EDREIMBURSESTATUS, EDAPPROVALSTATUS, EDStatus, userid, ExpFlexDatas, ExpAttendees, ExpUplFiles, ExpMessages) {

    return {

        ED_Id: EDId,
        ED_ER_Id: EDERId,
        ED_ET_Id: EDETId,
        ED_TransDate: EDTransDate,
        ED_PT_Id: EDPTId,
        ED_CM_Id: EDCMId,
        ED_ReceiptStatus: EDReceiptStatus,
        ED_ECT_Id: EDECTId,
        ED_UOM: EDUOM,
        ED_Units: EDUnits,
        ED_UnitPrice: EDUnitPrice,
        ED_Currency: EDCurrency,
        ED_ExcRate: EDExcRate,
        ED_ExcAmount: EDExcAmount,
        ED_Tax: EDTax,
        ED_TotalAmount: EDTotalAmount,
        ED_IsBillable: EDIsBillable,
        ED_CV_Id: EDCVId,
        ED_ClaimExpense: EDClaimExpense,
        ED_Comment: EDComment,
        ED_REIMBURSESTATUS: EDREIMBURSESTATUS,
        ED_APPROVALSTATUS: EDAPPROVALSTATUS,
        ED_Status: EDStatus,
        ED_CreatedBy: userid,
        expFlexDatas: ExpFlexDatas,
        expAttendees: ExpAttendees,
        expReceipts: ExpUplFiles,
        expMessages: ExpMessages
    };
}

/* To set and get CT_T_EXP_REIPTS table related entity */
function GetExpReceiptObj(RCId, RCEDId, RCProfileId, RCName, RCFileName, RCPath, RCStatus, User, RCMode) {

    return {

        RC_Id: RCId,
        RC_ED_Id: RCEDId,
        RC_ProfileId: RCProfileId,
        RC_Name: RCName,
        RC_FileName: RCFileName,
        RC_Path: RCPath,
        RC_Status: RCStatus,
        RC_CreatedBy: User,
        RC_Mode: RCMode
    }
}

/* To set and get  CT_T_EXP_REPMESSAGES table related entity */
function GetExpRepMsgObj(ERMId, ERMEDId, ERMMessage, ERMStatus, userid) {

    return {

        ERM_Id: ERMId,
        ERM_ED_Id: ERMEDId,
        ERM_Message: ERMMessage,
        ERM_Status: ERMStatus,
        ERM_CreatedBy: userid
    };
}

function GetExpApproverQueueObj(FromDate, ToDate, AgentId, ProfileId, CostCenter, ReceiptStatus, PaymentType, ReportTitle, ReportReferenceNo, ApproverStatus) {
    return {
        FromDate: FromDate,
        ToDate: ToDate,
        AgentId: AgentId,
        ProfileId: ProfileId,
        CostCenter: CostCenter,
        ReceiptStatus: ReceiptStatus,
        PaymentType: PaymentType,
        ReportTitle: ReportTitle,
        ReportReferenceNo: ReportReferenceNo,
        ApproverStatus: ApproverStatus
    };
}
