﻿
var Messages = '';
var PREFIX_MASTER = 'ctl00_cphTransaction_';
var PREFIX_MASTER_PAXGRID = 'ctl00_cphTransaction_dlAdditionalPax_ctl0';

function getElement(id){
    return (document.getElementById(PREFIX_MASTER + id));
}

function getAddlElement(id,counter) {
    return (document.getElementById(PREFIX_MASTER_PAXGRID + counter + '_' + id));
}

function getMessage(){
return(Messages)}
function clearMessage(){
Messages=''}
function addMessage(msg,fieldId){
try{
Messages+='* '+msg+'\r\n'}
catch(err){}}

function isNumeric(fieldId,allowNegative){
try{
var object=document.getElementById(fieldId)
if(object==null)return(true)
var type=object.type
if(type=='text' ||type=='textarea'||type=='password')
if(object.value.length==0)return(false)
var RegExp
if(allowNegative=='false')RegExp=/^[+]?(\d*)(\.?)(\d*)$/;else RegExp=/^[-+]?(\d*)(\.?)(\d*)$/
return(object.value.match(RegExp)!=null)}
catch(err){
showError(err,'Validation','isNumeric')}}



function restrictNumeric(fieldId,kind){
try{
return(maskNumeric(fieldId,(kind=='3'||kind=='4'?'false':'true'),(kind=='1'||kind=='3'?'true':'false')))}
catch(err){
showError(err,'Validation','restrictNumeric');return(false)}}
function maskNumeric(fieldId,ignoreNegative,IgnoreDecimal){
var key;var keychar
if(ignoreNegative==null)ignoreNegative='true'
if(IgnoreDecimal==null)IgnoreDecimal='true'
if(window.event){
if(navigator.appName.substring(0,1)=='M') key=window.event.keyCode
else key=window.event.charCode }
else if(event) key=event.which
else return true
keychar=String.fromCharCode(key)
if((key==null)||(key==0)||(key==8)||(key==9)||(key==13)||(key==27))return true
var strSet="0123456789"+(ignoreNegative=='true'?'':'-')+(IgnoreDecimal=='true'?'':'.')
if((strSet.indexOf(keychar)>-1)){
var inputbox=document.getElementById(fieldId)
if(ignoreNegative=='false'&&key==45){
if(inputbox.value.indexOf('-')==-1)inputbox.value='-'+inputbox.value
return(false)}
if(IgnoreDecimal=='false'&&inputbox.value.indexOf('.')>-1&&key==46)return(false)
return true}
return(false)}

//TO Fix Decimal Points
   function setToFixed(id)
   {
   //alert(getElement(id).value)
        var point=4;
       if(!isNaN(getElement(id).value) && getElement(id).value!='')
       {
            var value=parseFloat(getElement(id).value);
            getElement(id).value=value.toFixed(point);
        }
        else
        {
             var defValue=parseFloat('0');
             getElement(id).value= defValue.toFixed(point);
        }
   }
   function setToFixedThis(id)
   {
   //alert(document.getElementById(id).value)
       var point=7;
       if(!isNaN(document.getElementById(id).value) && document.getElementById(id).value!='')
       {
            var value=parseFloat(document.getElementById(id).value);
            document.getElementById(id).value=value.toFixed(point);
        }
        else
        {
             var defValue=parseFloat('0');
             document.getElementById(id).value= defValue.toFixed(point);
        }
   }
   //Checking Valid Email Pattern
   function checkEmail(inputvalue){	
    var pattern=/^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
    if(pattern.test(inputvalue)){         
		return true;
    }else{   
		return false;
    }
}

var regexAlpha = /^([a-zA-Z ])/; var regexNum = /^([0-9])/; var regexSpChar = /^([a-zA-Z0-9_.@:,; &-])/;
var regexDecimal = /^([0-9.])/; var regexDate = /^\d{1,2}\/\d{1,2}\/\d{4}$/; var hash = '#'; var space = ' ';

/* To validate data value on key press */
function onKeyPressVal(event, type, length) {

    return checkDataFormat(event.key, type, length);
}

/* To validate data value on key press */
function checkDataFormat(dataInput, type, length) {    

    if (type == 'AL' && !regexAlpha.test(dataInput)) // only alphabets
        return false;

    if (type == 'NM' && !regexNum.test(dataInput)) // only numerics
        return false;

    if (type == 'DC' && !regexDecimal.test(dataInput)) // only numerics with decimals
        return false;

    if (type == 'AN' && !(regexAlpha.test(dataInput) || !regexNum.test(dataInput))) // only alpha numerics
        return false;

    if (type == 'TS' && !(regexAlpha.test(dataInput) || regexNum.test(dataInput) || regexSpChar.test(dataInput))) // alpha numerics with few special chars
        return false;

    //if (type == 'DT' && !regexDate.test(dataInput))
    //    return false;

    return IsEmpty(dataInput) || type == 'NM' || (type !== 'NM' && dataInput.length <= length);
}

/* To validate data value on key press */
function validateData(dataCntrlId, type, length, fieldName) {

    if (document.getElementById(dataCntrlId) == null || IsEmpty(document.getElementById(dataCntrlId).value))
        return true;

    if (fieldName !== 'select' && !checkDataFormat(document.getElementById(dataCntrlId).value, type, length)) {

        var dataType = type == 'DC' ? 'numeric with decimals' : type == 'AL' ? 'alphabetic' : type == 'NM' ? 'numeric' : type == 'AN' ? 'alpha numeric' : type == 'DT' ? 'in valid date format' : 'alpha numeric with (' + regexSpChar + ')';
        var msg = fieldName + ' should be ' + dataType + (type == 'DT' ? '' : ' and not more than ' + length + ' characters length.');
        $(hash + dataCntrlId).val('');
        $(hash + dataCntrlId).addClass('form-text-error');
        ShowError(msg);
        return false;
    }  

    return true;
}

/* To validate mandatory data and data format */
function CheckReqData(dataCntrlId, type, length, fieldName, invalidValues) {

    if (IsEmpty(document.getElementById(dataCntrlId)) || $(hash + dataCntrlId).attr('disabled') == 'disabled')
        return true;

    var dataInput = document.getElementById(dataCntrlId).value;

    if (IsEmpty(dataInput)) {
                
        ShowError(($(hash + dataCntrlId)[0].type == 'select-one' ? 'Select ' : 'Enter ') + fieldName);
        $(hash + dataCntrlId).addClass('form-text-error');
        return false;
    }

    var dataInput = document.getElementById(dataCntrlId).value;

    var IsValid = true;

    if (!IsEmpty(invalidValues)) {

        var errorValues = invalidValues.split('|');

        if (type == 'DC' || type == 'NM')
            $.each(errorValues, function (key, col) { IsValid = Math.ceil(dataInput) != Math.ceil(col) && IsValid });
        else
            $.each(errorValues, function (key, col) { IsValid = dataInput != col && IsValid });
    }

    if (!IsValid) {

        ShowError('Enter valid ' + fieldName);
        $(hash + dataCntrlId).addClass('form-text-error');
        return false;
    }

    return IsValid && validateData(dataCntrlId, type, length, ($(hash + dataCntrlId)[0].type == 'select-one' ? 'select' : fieldName));
}

/* To validate Alpha numeric values of text box */
function IsAlphaNum(e) {

    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    return ((keyCode >= 46 && keyCode <= 58) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122)
        || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
}

/* To validate Alpha values of text box */
function IsAlpha(e) {
    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    return ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122)
        || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
}

/* To validate numeric values of text box */
function IsNumeric(e) {
    var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
    return ((keyCode >= 46 && keyCode <= 58) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
}

/* To call ajax web methods */
function AjaxCall(Ajaxurl, Inputdata) {

    var obj = '';

    $.ajax({
        type: "POST",
        url: Ajaxurl,
        contentType: "application/json; charset=utf-8",
        data: Inputdata,// "{'sOrigin':'" + origin + "','sDestination':'" + destination + "','sPaxnames':'" + PaxNames.toString() + "'}",
        dataType: "json",
        async: false,
        success: function (data) {
            obj = (data == null || data.d == null || data.d == 'undefined' || data.d == '') ? '' : data.d;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });

    return obj;
}

/* To call asynchronous ajax web methods */
function AsyncAjaxCall(Ajaxurl, Inputdata) {

    var obj = '';

    $.ajax({
        type: "POST",
        url: Ajaxurl,
        contentType: "application/json; charset=utf-8",
        data: Inputdata,
        dataType: "json",
        success: function (data) {
            obj = (data == null || data.d == null || data.d == 'undefined' || data.d == '') ? '' : data.d;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });

    return obj;
}

/* To set text box value */
function Settextval(id, val) {

    document.getElementById(id).value = val != null ? val : '';
}

/* To set normal drop down value based on display text/stored value */
function Setdropval(id, val, type) {

    val = IsEmpty(val) ? '' : val.toString().trim();
    if (type == 'text')
        document.getElementById(id).value = $("#" + id + " option").filter(function () { return this.text == val.trim(); }).val();
    else
        document.getElementById(id).value = val.trim();

    if (document.getElementById(id).value == '')
        document.getElementById(id).value = $("#" + id + " option:first").val();
}

/* To set bootstrap drop down value based on display text/stored value */
function Setddlval(id, val, type) {

    val = IsEmpty(val) ? '' : val.toString().trim();
    if (type == 'text')
        $("#s2id_" + id).select2('val', $("#" + id + " option").filter(function () { return this.text == val.trim(); }).val());
    else
        $("#s2id_" + id).select2('val', val.trim());

    if (document.getElementById(id).value == '')
        $("#s2id_" + id).select2('val', $("#" + id + " option:first").val());
}

/* To display error message in JS */
function ShowError(errmsg) {

    var errmsgs = $('.toast-message'); var display = true;
    if (errmsgs != null && errmsgs.length > 0) {

        $.each(errmsgs, function (key, msgs) {

            if (errmsg == msgs.innerText) {
                display = false;
                return false;
            }
        });
    }
    if (display)
        toastr.error(errmsg);
}

/* To check the empty/blank values of a field */
function IsEmpty(val) {

    return (val == null || val == 'undefined' || val == '');
}

/* To call Web API methods */
function WebAPICall(Ajaxurl, Inputdata, Method) {

    var obj = '';

    $.ajax({
        type: Method,
        url: Ajaxurl,
        ContentType: "application/json; charset=utf-8",
        data: Inputdata,
        dataType: "json",
        async: false,
        success: function (data) {
            obj = IsEmpty(data) ? '' : data;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });

    return obj;
}

/* To get country code and name from google maps result */
function GetCountry(place) {

    var sCountry = '';
    if (place != null && place.address_components != null && place.address_components.length > 0)
    {
        $.each(place.address_components, function (key, col) {

            if (col.types != null && col.types.length > 0 && col.types.indexOf('country') > -1)
                sCountry = col.long_name + '|' + col.short_name;
        });
    }
    return IsEmpty(sCountry) ? place.formatted_address : sCountry;
}

/* To get country code and name from google maps result */
function GetCityNamePOI(place) {

    var sCityName = '';

    if (place != null && place.address_components != null && place.address_components.length > 0) {

        $.each(place.address_components, function (key, col) {

            if (col.types != null && col.types.length > 1 && col.types.indexOf('locality') > -1 && col.types.indexOf('political') > -1)
                sCityName = col.long_name + '|' + col.short_name;
            if (IsEmpty(sCityName) && col.types != null && col.types.length > 0 && col.types.indexOf('postal_town') > -1)
                sCityName = col.long_name + '|' + col.short_name;
        });
    }

    return IsEmpty(sCityName) ? place.formatted_address : sCityName;
}

/* To enable or disable all input controls of a given selector */
function EnableDisableCntrls(selctor, disable, ignoreelements, ignoreIds) {

    Cntrls = $('.' + selctor + ' :input');

    if (IsEmpty(Cntrls))
        return;

    var ignreelememts = !IsEmpty(ignoreelements) ? ignoreelements.split('|') : [];
    var ignreIds = !IsEmpty(ignoreIds) ? ignoreIds.split('|') : [];

    $.each(Cntrls, function (key, col) {

        if (ignreelememts.length > 0 && ignreelememts.indexOf(col.type) != -1)
            return;

        if (ignreIds.length > 0 && ignreIds.indexOf(col.id) != -1)
            return;

        if (disable)
            $(col).attr('disabled', '');
        else
            $(col).removeAttr('disabled', '');
    });
}

/* To log error into audit and send email */
function LogError(sException, sEvent) {

    try {

        AjaxCall('ApiGuestDetails/LogError', "{'sException':'" + sException + "', 'sEvent':'" + sEvent + "'}");
    }
    catch (exception) {
        var exp = exception;
    }
}

/* To get agent users */
var ddlUsersId = ''; var Ajax;
function LoadAgentUsers(id, AgentId) {

    ddlUsersId = id;
    var paramList = 'requestSource=getAgentUsersByAgentId' + '&AgentId=' + AgentId + '&id=' + id;
    Ajax = window.XMLHttpRequest ? new XMLHttpRequest() : new ActiveXObject("Microsoft.XMLHTTP");
    Ajax.onreadystatechange = BindAgentUsers;
    Ajax.open('POST', "CityAjax");
    Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
    Ajax.send(paramList);
}

/* To bind users to drop down */
function BindAgentUsers() {

    var ddl = document.getElementById(ddlUsersId);

    if (ddl == null)
        return;

    ddl.options.length = 0;
    var el = document.createElement("option");
    el.textContent = "Select User";
    el.value = "-1";
    ddl.add(el, 0);
    $(hash + ddl.id).select2('val', '-1');

    if (Ajax.readyState == 4) {

        if (Ajax.status == 200) {

            if (Ajax.responseText.length > 0) {

                var values = Ajax.responseText.split(',');

                for (var i = 0; i < values.length; i++) {

                    var opt = values[i];
                    if (opt.length > 0 && opt.indexOf('|') > 0) {

                        var el = document.createElement("option");
                        el.textContent = opt.split('|')[0];
                        el.value = opt.split('|')[1];
                        ddl.appendChild(el);
                    }
                }
            }
        }
    }
}

/* Common function to make web api call */
function WebApiReq(apiUrl, apiMethod, reqData, apiToken, fnSuccess, fnFailure, fnComplete) {

    fnFailure = IsEmpty(fnFailure) ? WebApiReqError : fnFailure;
    fnComplete = IsEmpty(fnComplete) ? WebApiReqComplete : fnComplete;

    setTimeout(function () { $("#ctl00_upProgress").show(); }, 0);

    try {

        $.ajax({
            url: apiUrl,
            type: apiMethod,
            ContentType: 'application/json; charset=utf-8',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Authorization', "Bearer " + apiToken);
            },
            data: reqData,
            success: function (apiResponse) {

                if (apiResponse.result) 
                    fnSuccess(apiResponse.result);
                else
                    alert(apiResponse.errors[0].value);
                $("#ctl00_upProgress").hide();
            },
            complete: fnComplete,
            error: fnFailure
        });

    }
    catch (excp) {
        var exception = excp;
    }
}

/* To catch web api request failure error */
function WebApiReqError(error) {

    ShowError('Request got failed, please contact admin.');
    $("#ctl00_upProgress").hide();
}

/* Common function to handle web api req call complete action */
function WebApiReqComplete() {
    $("#ctl00_upProgress").hide();
}

/* To prepare agent object for api input */
function BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress) {

    return { AgentId: agentId, LoginUserId: loginUser, OnBelahfAgentLoc: behalfLocation, IPAddress: ipAddress, LoginUserCorpId: loginUserCorpProfile }
}

/* To bind drop down html with value and text */
function DdlBind(ddlId, dataEntity, valColNames, textColNames, separator, selval, seltext, defSelVal) {

    if (IsEmpty(ddlId) || IsEmpty(dataEntity) || IsEmpty(valColNames) || IsEmpty(textColNames))
        return;

    var options = (!IsEmpty(seltext)) ? GetddlOption(selval, seltext) : mt;

    for (var i = 0; i < dataEntity.length; i++) {

        var stVal = mt; var dispval = mt;
        
        if (!IsEmpty(separator)) {

            var arrValColNames = valColNames.split(separator);
            $.each(arrValColNames, function (key, colName) { stVal += dataEntity[i][colName] + separator });
            stVal = stVal.slice(0, stVal.length - 1);

            var arrtextColNames = textColNames.split(separator);
            $.each(arrtextColNames, function (key, colName) {

                var multicols = colName.split(space);
                if (multicols.length > 1) {

                    $.each(multicols, function (key, mColName) { dispval += space + dataEntity[i][mColName] });
                    dispval += separator; 
                }
                else
                    dispval += space + dataEntity[i][colName] + space + separator;
            });
            dispval = dispval.slice(0, dispval.length - 1).trim();
        }
        else {

            stVal = dataEntity[i][valColNames];
            dispval = dataEntity[i][textColNames];
        }

        options += GetddlOption(stVal, dispval);
    }

    $(hash + ddlId).empty();
    $(hash + ddlId).append(options);
    $(hash + ddlId).select2("val", defSelVal);
}

/* To get drop down option html with value and text */
function GetddlOption(val, text) {
    return '<option value="' + val + '">' + text + '</option>';
}

/* To prepare date for Json object */
function GetJsonDate(val, seperator, format) {

    if (IsEmpty(val))
        return '';

    var date = val.split(seperator);
    var monthindex = format == 'mmddyyyy' ? 0 : 1;
    var dateindex = format == 'mmddyyyy' ? 1 : 0;
        
    //return date[2] + '-' + date[monthindex] + '-' + date[dateindex] + 'T00:00:00.0000000+05:30';
    return date[2] + '-' + date[monthindex] + '-' + date[dateindex] + 'T00:00:00.0000000+00:00';
}

/* To prepare date for Json object */
function GetDateFormat(val, seperator, format) {

    if (IsEmpty(val))
        return '';

    //const date = new Date(val).toLocaleDateString().slice(0, 10);
    const date = new Date(val).toLocaleDateString('en-US');
    const d = new Date(date);
    const ye = new Intl.DateTimeFormat('en', { year: 'numeric' }).format(d);
    const mo = new Intl.DateTimeFormat('en', { month: '2-digit' }).format(d);
    const da = new Intl.DateTimeFormat('en', { day: '2-digit' }).format(d);

    var outdate = format == 'mmddyyyy' ? (mo + seperator + da + seperator + ye) :
        format == 'yyyymmdd' ? (ye + seperator + mm + seperator + da) : (da + seperator + mo + seperator + ye);
    return outdate;
}

/* To get text box value in numeric */
function GetTextBoxDecVal(txtId) {

    if (IsEmpty(txtId) || document.getElementById(txtId) == null || IsEmpty(document.getElementById(txtId).value))
        return 0;

    return eval(document.getElementById(txtId).value.replace(/,/g, mt));
}

/* To compare fields data in uppercase */
function CompFields(leftval, rightval) {

    return !IsEmpty(leftval) && !IsEmpty(rightval) && leftval.toUpperCase() == rightval.toUpperCase();
}

/* To get ddl selected text */
function GetddlSelText(ddlid) {

    if (IsEmpty(ddlid) || IsEmpty($("#" + ddlid)) || IsEmpty($("#" + ddlid)[0].selectedOptions) || $("#" + ddlid)[0].selectedOptions.length == 0)
        return '';

    return $("#" + ddlid)[0].selectedOptions[0].innerHTML;
}

/* To get ddl selected text */
function removeComma(val) {

    if (IsEmpty(val))
        return '';

    return eval(val.toString().replace(/,/g, ''));
}

/* To round off agent decimal value */
function DecimalRound(val, decimal, culture) {

    /* val = IsEmpty(val) ? 0 : val;
    return parseFloat(val).toLocaleString(('en-' + culture), { minimumFractionDigits: eval(decimal) }); */
    return FixedDecimalRound(val, decimal);
}

/* To round off agent decimal value */
function FixedDecimalRound(val, decimal) {

    val = IsEmpty(val) ? 0 : val;
    return parseFloat(val).toFixed(decimal);
}

/* To get split value at given index */
function GetControlSplitVal(cntrlId, separator, index) {

    if ($('#' + cntrlId) == null || IsEmpty($('#' + cntrlId).val()))
        return '';

    return GetSplitVal($('#' + cntrlId).val(), separator, index);
}

/* To get split value at given index */
function GetSplitVal(val, separator, index) {

    if (IsEmpty(val))
        return '';

    return val.split(separator).length > Math.ceil(index) ? val.split(separator)[index] : mt;
}

var files = [];

/* To initialize drop zone control for file uploads */
function SetDropZoneControl(divDropZone, fileUploadHandler, previrecntrnrId, fileformats, maxAllowedFiles) {

    if (IsEmpty(divDropZone) || IsEmpty(fileUploadHandler) || IsEmpty(previrecntrnrId))
        return;

    $(".closepnldocs").on('click', function () {

        $(".pnldocs").hide();
        $(".pnldrop").hide();

        if (document.getElementById('lnkRemove') != null)
            document.getElementById('lnkRemove').click();

    });

    // Dropzone.autoDiscover = false;
    Dropzone.prototype.defaultOptions.acceptedFiles = IsEmpty(fileformats) ? ".JPG,.JPEG,.PNG,.PDF,.DOC,.DOCX,.XLS,.XLSX" : fileformats;
    Dropzone.prototype.defaultOptions.maxFiles = IsEmpty(maxAllowedFiles) || maxAllowedFiles == 0 ? "1" : maxAllowedFiles;
    var filesize = 1024 * 5000;
    Dropzone.maxFilesize = 5;

    // for PIR files Upload          
    var dropZonePIRObj = {

        url: fileUploadHandler,
        maxFiles: 10,
        addRemoveLinks: true,
        previewsContainer: '#' + previrecntrnrId,
        thumbnailHeight: 120,
        thumbnailWidth: 120,
        init: function () {

            this.on("addedfile", function (file) {

                if (this.files.length) {

                    var _i, _len;

                    //Reoving the File when exceed the File Limit.
                    if (file.size > filesize) {

                        this.removeFile(file);

                        if (!files.includes(file.name))
                            files.push(file.name);

                        var msg = '';

                        for (var i = 0; i < files.length; i++)
                            msg += files[i] + (i != files.length - 1 ? ',' : '');

                        $('#errorMsg').remove();
                        $('#' + divDropZone).append('<span id="errorMsg" style="color: red"><b>' + msg + '</b> Failed to Upload the files, exceeded file Limits.</span>');
                        setTimeout(function () { $('#errorMsg').fadeIn(400); }, 5000);
                    }

                    //Removing the duplicate file having same size ,name and modification date.
                    //for (_i = 0, _len = this.files.length; _i < _len - 1; _i++) // -1 to exclude current file
                    //{
                    //    if (this.files[_i].name === file.name && this.files[_i].size === file.size && this.files[_i].lastModifiedDate.toString() === file.lastModifiedDate.toString()) {
                    //        this.removeFile(file);
                    //    }
                    //}
                }
            });
        },
        success: function (file, response) {

        },
        error: function (file, response) {
            file.previewElement.classList.add("dz-error");
        }
    }

    $('.dropzone').each(function () {

        let dropzoneControl = $(this)[0].dropzone;
        if (dropzoneControl)
            dropzoneControl.destroy();
    });

    var dropZonePIR = new Dropzone('#' + divDropZone, dropZonePIRObj);

    $('.dz-message').append('<div class="use-camera bg-primary"  onclick="showCamera()">USE CAMERA<i class="icon-camera pl-2"></i></div><div class="selectfromGallery "><i class="icon icon-image pr-2 "></i>Select from Gallery</div>');
}

/* To enable and show alert message in popup */
function ShowAlertPopUp(popId, message, okfn) {

    if (document.getElementById(popId) == null || IsEmpty(message))
        return true;

    if (document.getElementById('divAlertPopUp') == null) {


        var html = '<div class="modal fade" id="divAlertPopUp" role="dialog"><div class="modal-dialog"><div class="modal-content">' +
            '<div class="modal-header" style="color: white"><span ID="title" Style="color: white; text-align: center; margin-top: 15px">Alert</span>' +
            '<button type="button" class="close" data-dismiss="modal" style="height: 5px; width: 10px" aria-label="Close">' +
            '<span aria-hidden="true">X</span></button></div><div class="modal-body" style="height: 200px" id="divAlertBody"></div></div></div></div>';
        $('#' + popId).append(html);
    }
    
    document.getElementById('divAlertBody').innerHTML = message;
    $('#divAlertPopUp').modal('show');
}

/* To get ddl text/val */
function GetddlTextOrVal(ddlid, val, type, opttype) {

    if (IsEmpty(ddlid) || document.getElementById(ddlid) == null)
        return mt;

    var allOptions = $('#' + ddlid + ' option');
    var optInfo = mt;

    if (opttype == 'T')
        $.each(allOptions, function (key, colName) { if (!IsEmpty(colName.text) && colName.text == val) { optInfo = type == 'V' ? colName.value : colName.text; return; } });
    else
        $.each(allOptions, function (key, colName) { if (!IsEmpty(colName.value) && colName.value == val) { optInfo = type == 'V' ? colName.value : colName.text; return; } });

    return optInfo;
}

/* To prepare and get keys list for duplicate validation */
function GetDupKeysList(keys, values) {

    if (IsEmpty(keys) || IsEmpty(values) || keys.length == 0 || values.length == 0 || keys.length != values.length)
        return [];

    var keysList = [];

    for (var i = 0; i < values.length; i++) {

        keysList.push(keys[i] + '^' + values[i].replace(/^/g, ''));
    }

    return keysList;
}

/* To get and validate duplicate records of the given keys */
function GetDupRecords(apiAgentInfo, apiHost, VLKey, keys, values, callBackfn) {

    if (IsEmpty(apiAgentInfo) || IsEmpty(apiHost) || IsEmpty(VLKey) || IsEmpty(keys) || IsEmpty(values) || IsEmpty(callBackfn))
        return;

    var ukKeysList = GetDupKeysList(keys, values);

    if (IsEmpty(ukKeysList) || ukKeysList.length == 0)
        return;

    var reqData = { AgentInfo: apiAgentInfo, KeyValue: VLKey, PKValues: ukKeysList };
    var apiUrl = apiHost.trimRight('/') + '/api/commonMasters/getDuplicates';
    WebApiReq(apiUrl, 'POST', reqData, '', callBackfn, null, null);     
}
