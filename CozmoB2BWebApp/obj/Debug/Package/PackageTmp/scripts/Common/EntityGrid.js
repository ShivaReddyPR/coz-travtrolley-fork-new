﻿var mt = ''; var gridId = 'list-group'; var hdrPagingId = 'HeaderPagination'; var set = 'set';
var selectAllRows = false; var tbodyRowsId = 'tbRows'; var chkSelectAllId = 'chkGridSelectAll';
var gridProps = {}; var currentPageAllPKValues = []; var currentPageSelectedRows = [];
var totalPages = 1; var gridColumnProp = {};

/* To send the default grid properties entity */
function GetGridPropsEntity() {

    selectAllRows = false;
    currentPageAllPKValues = [];
    totalPages = 1;
    currentPageSelectedRows = [];
    return gridProps = {

        currentPageNo: 1, // Current displayed page no in the grid
        recordsperpage: 10, // No of records per page in the grid
        pagenodisplay: 5, // No of page nos to display in the pagination tab both header and footer
        totalRecords: 0, // Total no of records in the grid
        dataEntity: {}, // Grid data entity to bind the data in grid
        divNoRecords: mt, // No records div Id to show if there is no data in the grid
        divGridId: mt, // Grid div id to bind the grid html
        headerPaging: true, // Header pagination tab id
        headerColumns: [], // Grid header columns text
        displayColumns: [], // Actual column names of grid data entity
        pKColumnNames: [], // Primary key column names to append primary key column values on grid row selection
        gridSelectedRows: [], // Get selected rows primary key values
        fnSelectAll: 'SelectAll', // Give select all check box on click function name
        fnRowSelect: mt, // Five row select check box/radio button click function name
        selectType: 'N' ,// Mention type of row selection, single select / multi select, if selection not needed this should be N
        columnproperties: GetGridColumnProperties(), // get the column properties. 
        selectColProps: null // get the sel check box properties. 
    };

}

/* initializing the common grid control columns proprties. */
function GetGridColumnProperties()
{
    return gridColumnProp = {
        columnName: '', // column name.
        columnEvents: {}, // column control events
        columnAttributes: {}, // custom declared control attributes
        columnControl: '', // column control types.
        columnEventParams : {} //declaring the event required parameters here.
    };
}

/* To get row condition proprties. */
function GetGridRowProp() {

    return gridColumnProp = {

        columnName: mt, 
        colValue: mt, 
        colCondition: mt
    };
}

/* To set all paging grid properties and enable grid with pagination for given data*/
function EnablePagingGrid(gridProperties) {

    gridProps = gridProperties;

    if (IsEmpty(gridProps) || IsEmpty(gridProps.divGridId) || IsEmpty(gridProps.headerColumns) || IsEmpty(gridProps.displayColumns))
        return;

    if (IsEmpty(gridProps.dataEntity) || gridProps.dataEntity.length == 0)
        return;

    if ((gridProps.selectType == 'M' || gridProps.selectType == 'S') && IsEmpty(gridProps.pKColumnNames))
        return;

    RemoveGrid();
    $('#' + gridProps.divNoRecords).show();
    $('.panel-footer').remove();

    gridProps.totalRecords = gridProps.dataEntity.length;
    totalPages = Math.ceil(gridProps.totalRecords / gridProps.recordsperpage);
    gridProps.fnRowSelect = !IsEmpty(gridProps.fnRowSelect) ? gridProps.fnRowSelect : gridProps.selectType == 'M' ? 'SelectRowCheckBox' : 'SelectRowRadioButton';

    if (gridProps.headerPaging == true)
        $('#' + gridProps.divGridId).append('<div id="' + hdrPagingId + '" class="pagmargin"> </div>');

    $('#' + gridProps.divGridId).append('<div class="EntityGridWrapper"><div id="' + gridId + '" class="table-responsive w-100"> </div><div class="clear"></div></div>');

    if (gridProps.totalRecords > 0) {

        $('#' + gridProps.divNoRecords).hide();
        if (gridProps.totalRecords > gridProps.recordsperpage) {

            $('#' + gridId).paginathing({
                limitPagination: (gridProps.totalRecords / gridProps.recordsperpage) > gridProps.pagenodisplay ? gridProps.pagenodisplay : totalPages,
                containerClass: 'panel-footer',
                pageNumbers: true,
                prevNext: (gridProps.totalRecords / gridProps.recordsperpage) > gridProps.pagenodisplay,
                firstLast: (gridProps.totalRecords / gridProps.recordsperpage) > gridProps.pagenodisplay,
                totalRecords: gridProps.totalRecords,
                perPage: gridProps.recordsperpage,
                currPage: gridProps.currentPageNo
            });
        }
        CreateGridHeader();
        LoadData(gridProps.currentPageNo);
        $('#' + gridProps.divNoRecords).hide();
    }
}

/* To create table with grid header and append the html to grid div */
function CreateGridHeader() {

    if (IsEmpty(gridProps.headerColumns))
        return;

    var tableHTML = '<table class="table small mt-2 exp-sidetable"><thead><tr>';

    if (gridProps.selectType == 'M') {

        tableHTML += '<th><div class="custom-checkbox-style dark">';
        tableHTML += '<input type="checkbox" id="' + chkSelectAllId + '" onclick="' + gridProps.fnSelectAll + '(this)" class="form-control"><label></label></div></th>';
    }

    if (gridProps.selectType == 'S')
        tableHTML += '<th><label>Select</label></div></th>';

    $.each(gridProps.headerColumns, function (key, colName) { tableHTML += '<th>' + colName + '</th>' });

    tableHTML += '</tr></thead> <tbody id="' + tbodyRowsId + '"></tbody></table > ';
    $('#' + gridId).append(tableHTML);
}

/* To load selected page data in grid */
function LoadData(page) {

    gridProps.currentPageNo = page;
    $('#' + tbodyRowsId).children().remove();

    var totRecords = Math.ceil(gridProps.totalRecords) - 1;
    var showFrom = ((Math.ceil(page) - 1) * gridProps.recordsperpage);
    var showTo = Math.ceil(showFrom) + (Math.ceil(gridProps.recordsperpage) - 1);
    showTo = showTo >= totRecords ? totRecords : showTo;
    var NewTotalPages = Math.ceil(gridProps.totalRecords / gridProps.recordsperpage);
    
    if (showFrom > totRecords || NewTotalPages < totalPages) {

        gridProps.currentPageNo = showFrom > totRecords ? Math.ceil(gridProps.currentPageNo) - 1 : gridProps.currentPageNo;
        EntityGridRefresh();
    }        
    else
        BindPageRows(showFrom, showTo);
}

/* To bind current page rows data html to grid */
function BindPageRows(showFrom, showTo) {

    currentPageSelectedRows = [];

    for (var i = Math.ceil(showFrom); i <= Math.ceil(showTo); i++) {

        var pkValue = mt; var fnselchange = mt;
        var tableRows = '<tr>';
        var gridRow = gridProps.dataEntity[i];

        if ((gridProps.selectType == 'M' || gridProps.selectType == 'S')) {
            
            var BindSelect = 'Y';

            if (!IsEmpty(gridProps.selectColProps) && !IsEmpty(gridProps.selectColProps.columnName) &&
                !IsEmpty(gridProps.selectColProps.colValue) && !IsEmpty(gridProps.selectColProps.colCondition)) {

                if (gridProps.selectColProps.colCondition == '=')
                    BindSelect = gridProps.dataEntity[i][gridProps.selectColProps.columnName] == gridProps.selectColProps.colValue ? BindSelect : 'N';
            }

            if (BindSelect == 'Y') {

                $.each(gridProps.pKColumnNames, function (key, colName) { pkValue += gridProps.dataEntity[i][colName] + '|' });
                pkValue = pkValue.slice(0, pkValue.length - 1);
                currentPageAllPKValues.push(pkValue);
                fnselchange = gridProps.fnRowSelect + "(this,'" + pkValue + "')";
            }

            if (gridProps.selectType == 'M') {

                tableRows += '<td><div class="custom-checkbox-style dark">';

                if (BindSelect == 'Y') {

                    var isSelected = gridProps.gridSelectedRows.find(item => item == pkValue);
                    if (!IsEmpty(isSelected))
                        currentPageSelectedRows.push(pkValue);

                    tableRows += '<input type="checkbox" id="chkRow' + i + '" ' + (!IsEmpty(isSelected) ? 'checked="checked" ' : mt)
                    tableRows += 'onclick="' + fnselchange + '" class="form-control"><label></label>';
                }

                tableRows += '</div></td>';
            }

            if (gridProps.selectType == 'S') {

                tableRows += '<td><div class="custom-checkbox-style dark">';

                if (BindSelect == 'Y') {

                    var isSelected = gridProps.gridSelectedRows.find(item => item == pkValue);

                    tableRows += '<input type="radio" name="radios" id="rbtnRow' + i + '" ' + (!IsEmpty(isSelected) ? 'checked="checked" ' : mt)
                    tableRows += 'onclick="' + fnselchange + '" class="form-control"><label></label>';
                }

                tableRows += '</div></td>';
            }
        }

        $.each(gridProps.displayColumns, function (key, colName) {
           
            var val = IsEmpty(gridRow[colName]) ? mt : gridRow[colName];   
            var isEvent = false;
            // Binding the dynamic controls to grid using column attributes
            if (!IsEmpty(gridProps.columnproperties[colName])) {

                var props = '';
                for (var prop in gridProps.columnproperties[colName].columnAttributes) {
                    props += " " + prop + "='" + gridProps.columnproperties[colName].columnAttributes[prop] + "'";
                }
              
                for (var attr in gridProps.columnproperties[colName].columnEvents) {

                    var eventparams = '';
                    if (!IsEmpty(gridProps.columnproperties[colName].columnEventParams[attr])) {
                        if (attr.toLowerCase() == 'href' || attr.toLowerCase() == 'id') {

                            var linkparams = gridProps.columnproperties[colName].columnEventParams[attr].split('|');
                            var tempParams = gridProps.columnproperties[colName].columnEvents[attr];
                            for (var j = 0; j < linkparams.length; j++) {
                                tempParams = tempParams.replace(linkparams[j], gridRow[linkparams[j]]);
                            }

                            props += " " + attr + "='" + tempParams+ "'";
                            isEvent = false;
                        } else {

                            $.each(gridProps.columnproperties[colName].columnEventParams[attr].split('|'), function (key, colName) {
                                eventparams += !IsEmpty(gridRow[colName]) ? gridRow[colName] + ',' : '';
                            });

                            eventparams = eventparams.substr(0, eventparams.length - 1);
                            eventparams = eventparams.slice(0, props.length - 1);
                            isEvent = true;
                        }
                    }
                    if (isEvent)
                        props += " " + attr + "='" + gridProps.columnproperties[colName].columnEvents[attr] + "(" + eventparams + ");'";
                   
                }
                if (gridProps.columnproperties[colName].columnControl.toLowerCase() == 'link') {
                    tableRows += '<td> <a ' + props + '>' + val + '</a></td>';
                }
                else if (gridProps.columnproperties[colName].columnControl.toLowerCase() == 'button') {
                    tableRows += '<td> <button ' + props + '>' + val + '</button></td>';
                }
                else if (gridProps.columnproperties[colName].columnControl.toLowerCase() == 'radio') {
                    tableRows += '<td> <input type="radio" ' + props + '/>' + val + '</td>';
                }
                else if (gridProps.columnproperties[colName].columnControl.toLowerCase() == 'checkbox') {
                    tableRows += '<td> <input type="checkbox" ' + props + '/>' + val + '</td>';
                }
                else if (gridProps.columnproperties[colName].columnControl.toLowerCase() == 'textbox') {
                    tableRows += '<td> <input type="text" ' + props + '/></td>';
                }
                else if (gridProps.columnproperties[colName].columnControl.toLowerCase() == 'textarea') {
                    tableRows += '<td> <textarea ' + props + '/></td>';
                }
                else {
                    tableRows += '<td>' + val + '</td>';
                }
            }
            else {
                tableRows += '<td>' + val + '</td>';
            }
        });

        tableRows += '</tr>';
        $('#' + tbodyRowsId).append(tableRows);

        selectAllRows = currentPageSelectedRows.length == gridProps.recordsperpage;
        $('#' + chkSelectAllId).prop('checked', selectAllRows);
    }
}

/* To show grid selected page as active and load the page data */
function showselpage(event, pageno) {

    event.parentNode.className = 'page active';
    LoadData(pageno);
}

/* To bind pagination tab on top of the grid */
function BindHeaderPagination() {

    if (gridProps.headerPaging == false)
        return;

    $('#' + hdrPagingId).children().remove();
    $('#' + hdrPagingId).append($('.panel-footer')[0] != null ? $('.panel-footer')[0].innerHTML : mt);
    var nodes = $('#' + hdrPagingId)[0].childNodes[1].childNodes;
    $.each(nodes, function (key, node) {
        if (node.childNodes[0].attributes.length > 0)
            node.childNodes[0].attributes[0].nodeValue = 'Hdrpagingclick(this,' + key + ')';
    });
}

/* To call actual pagination click from header pagination container */
function Hdrpagingclick(event, id) {

    var linodes = $('.panel-footer')[0].childNodes[0].childNodes;
    var ancclickfn = mt, liclickfn = mt;
    $.each(linodes, function (nodid, linode) {

        if (nodid == id) { ancclickfn = linode.childNodes[0]; liclickfn = linode; }
    });
    ancclickfn.onclick();
    liclickfn.click();
}

/* To set selected row value on grid row check box */
function SelectRowCheckBox(event, selectedval) {
        
    if (event.checked == true) {

        currentPageSelectedRows.push(selectedval);
        gridProps.gridSelectedRows.push(selectedval);
    }
    else {

        currentPageSelectedRows = currentPageSelectedRows.filter(item => item !== selectedval);
        gridProps.gridSelectedRows = gridProps.gridSelectedRows.filter(item => item !== selectedval);
    }        

    selectAllRows = currentPageSelectedRows.length == gridProps.recordsperpage;
    $('#' + chkSelectAllId).prop('checked', selectAllRows);
}

/* To set selected row value on grid row radio button */
function SelectRowRadioButton(event, selectedval) {

    gridProps.gridSelectedRows = [];
    gridProps.gridSelectedRows.push(selectedval);
}

/* To select all rows in the grid */
function SelectAll(event) {

    selectAllRows = event.checked;
    $("#" + tbodyRowsId).find('input[type=checkbox]').each(function () { this.checked = selectAllRows; });

    if (event.checked == true) {

        currentPageSelectedRows = currentPageAllPKValues;
        gridProps.gridSelectedRows = gridProps.gridSelectedRows.concat(currentPageAllPKValues);
    }
    else {

        currentPageSelectedRows = [];
        $.each(currentPageAllPKValues, function (key, col) {

            gridProps.gridSelectedRows = gridProps.gridSelectedRows.filter(item => item !== col);
        });
    }        
}

/* To get/set entity data object */
function GetSetDataEntity(action, val) {

    if (action == set) {

        gridProps.dataEntity = val;
        GetSetTotalRecords('set', gridProps.dataEntity.length);
    }
    return gridProps.dataEntity;
}

/* To get/set grid selected rows data */
function GetSetSelectedRows(action, val) {

    if (action == set)
        gridProps.gridSelectedRows = val;
    return gridProps.gridSelectedRows;
}

/* To get/set grid selected rows data */
function GetSetTotalRecords(action, val) {

    if (action == set)
        gridProps.totalRecords = val;
    return gridProps.totalRecords;
}

/* To get/set grid current selected page no */
function GetSetCurrentPageNo(action, val) {

    if (action == set)
        gridProps.currentPageNo = val;
    return gridProps.currentPageNo;
}

/* To get/set grid current selected page no */
function GetSetGridAllProps(action, val) {

    if (action == set)
        gridProps = val;
    return gridProps;
}

/* To refresh the grid with current props */
function EntityGridRefresh() {

    EnablePagingGrid(gridProps);
}

/* To remove grid and related html */
function RemoveGrid() {

    $('#' + gridProps.divGridId).children().remove();
}
