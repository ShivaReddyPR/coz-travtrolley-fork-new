﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExpReportPrint.aspx.cs" Inherits="CozmoB2BWebApp.ExpReportPrint" %>

<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Expense Report</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <link href="css/main-style.css" rel="stylesheet" type="text/css" />
    <link href="build/css/main.min.css?V1" rel="stylesheet" type="text/css" />
    <link href="css/override.css" rel="stylesheet" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />    
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="#" rel="shortcut icon" />

    <style>
        @media print {
            .receipt-wrapper {
                color: #000 !important
            }
        }

        @media print {
            .receipt-visitor-info th {
                color: #000 !important
            }
        }

        @media print {
            .receipt-wrapper {
                font-family: Arial,Helvetica,sans-serif;
                width: 100%
            }

            .receipt-visitor-info th {
                color: #fff;
                text-shadow: 0 0 0 #cfffcc
            }
        }

        @media print and (-webkit-min-device-pixel-ratio:0) {
            .receipt-visitor-info th {
                color: #fff;
                -webkit-print-color-adjust: exact
            }
        }

        #outerContainer #mainContainer div.toolbar {
          display: none !important; /* hide PDF viewer toolbar */
        }
        #outerContainer #mainContainer #viewerContainer {
          top: 0 !important; /* move doc up into empty bar space */
        }
        .receipt-canvas {
            max-width: 100%;
	        border: 2px solid rgba(0,0,0,.2);
	        margin: 0;
        }
        .receipt-wrapper .image-wrap{
            border: 2px solid rgba(0,0,0,.2);
        }
        .receipt-canvas .page-class{
            position:relative;
        }
        .receipt-canvas .page-class canvas{
            border:1px solid #ccc;
        }
        .receipt-canvas .page-class a,.receipt-wrapper .image-wrap a{
            position: absolute;
            right: 10px;
            top: 20px;
            opacity: .5;
        }
        .download-icon{
            background-image: url("data:image/svg+xml,%3C%3Fxml version='1.0' encoding='iso-8859-1'%3F%3E%3C!-- Generator: Adobe Illustrator 19.0.0, SVG Export Plug-In . SVG Version: 6.00 Build 0) --%3E%3Csvg version='1.1' id='Capa_1' xmlns='http://www.w3.org/2000/svg' xmlns:xlink='http://www.w3.org/1999/xlink' x='0px' y='0px' viewBox='0 0 512 512' style='enable-background:new 0 0 512 512;' xml:space='preserve'%3E%3Cg%3E%3Cg%3E%3Cpath d='M484.078,27.923C466.072,9.918,442.131,0.002,416.667,0.002L95.334,0C42.766,0.003,0,42.77,0.001,95.334L0,416.668 C0.001,469.234,42.768,512,95.335,512h321.332C469.234,512,512,469.234,512,416.667V95.335 C512,69.87,502.084,45.929,484.078,27.923z M482,416.667C482,452.692,452.692,482,416.667,482H95.335 C59.31,482,30.001,452.691,30,416.667l0.001-321.334C30,59.31,59.309,30.002,95.335,30l321.332,0.002 c17.451,0,33.858,6.795,46.198,19.134C475.205,61.476,482,77.883,482,95.335V416.667z'/%3E%3C/g%3E%3C/g%3E%3Cg%3E%3Cg%3E%3Cpath d='M346.131,217.492c-5.857-5.857-15.355-5.858-21.213,0L271,271.412L270.999,111.4c0-8.284-6.716-15-15-15s-15,6.716-15,15 L241,271.415l-53.922-53.921c-5.857-5.858-15.355-5.858-21.213,0c-5.858,5.858-5.858,15.355,0,21.213l79.528,79.526 c2.813,2.814,6.628,4.394,10.606,4.394s7.794-1.581,10.607-4.394l79.526-79.528C351.989,232.847,351.989,223.35,346.131,217.492z' /%3E%3C/g%3E%3C/g%3E%3Cg%3E%3Cg%3E%3Cpath d='M400.602,385.601h-289.2c-8.284,0-15,6.716-15,15s6.716,15,15,15h289.2c8.284,0,15-6.716,15-15 C415.602,392.316,408.886,385.601,400.602,385.601z'/%3E%3C/g%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3Cg%3E%3C/g%3E%3C/svg%3E%0A");
            width: 22px;
            height: 22px;
            float: left;
            background-repeat: no-repeat;
        }
       @media print{    
        .receipt-canvas .page-class a,.receipt-wrapper .image-wrap a,#btnPrint{
            display: none !important;
        }
       }
    </style>
</head>
<body>
    <script src="scripts/jquery.js"></script>
    <script src="scripts/Common/Common.js" type="text/javascript"></script>
    <%--<script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>--%>
    <%--<script type="text/javascript" src="Scripts/Menu/query2.js"></script>--%>
    <script src="scripts/jquery-ui.js"></script>
    <script type="text/javascript" src="Scripts/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdf.js/2.8.335/pdf.min.js" integrity="sha512-SG4yH2eYtAR5eK4/VL0bhqOsIb6AZSWAJjHOCmfhcaqTkDviJFoar/VYdG96iY7ouGhKQpAg3CMJ22BrZvhOUA==" crossorigin="anonymous"></script>
    <script>
       
         //Converting all PDF's to display in Canvas
        function renderPDF(url, canvasContainer, pageName) {

            function renderPage(page) {
                let viewport = page.getViewport({scale: 1})
                const DPI = 72;
                const PRINT_OUTPUT_SCALE = DPI/72; 
                const scale = canvasContainer.clientWidth / viewport.width;
                const canvas = document.createElement('canvas')
                const ctx = canvas.getContext('2d')
                viewport = page.getViewport({scale})

                canvas.width = Math.floor(viewport.width * PRINT_OUTPUT_SCALE);
                canvas.height = Math.floor(viewport.height * PRINT_OUTPUT_SCALE);
                canvas.style.width = '100%';

                canvas.style.transform = 'scale(1,1)';
                canvas.style.transformOrigin = '0% 0%';

                const canvasWrapper = document.createElement('div');
                canvasWrapper.classList.add("page-class", "page-number-" + page._pageIndex);
                canvasWrapper.appendChild(canvas);                            

                const renderContext = {
                    canvasContext: ctx,
                    viewport,
                }                 

                canvasContainer.appendChild(canvasWrapper)
                //page.render(renderContext)
                var task = page.render(renderContext);
                task.promise.then(function () {
                    image = canvas.toDataURL();
                    tmpLink = document.createElement('a');
                    tmpLink.download = pageName + '-' + page._pageIndex + '.png';
                    tmpLink.href = image;
                    tmpLink.innerHTML = '<span class="download-icon"></span>';
                    canvasWrapper.append(tmpLink)
                });


            }

            function renderPages(pdfDoc) {
                for (let num = 1; num <= pdfDoc.numPages; num += 1)
                    pdfDoc.getPage(num).then(renderPage)
            }

            pdfjsLib.disableWorker = true
            pdfjsLib.getDocument(url).promise.then(renderPages)
        }
            
   </script>
    <script type="text/javascript">

     

        var expDetailId = '', apiHost = '', cntrllerPath = 'api/expTransactions', apiAgentInfo = {};

        // Page load event.
        $(document).ready(function () {

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            /* Getting the expense detail id.*/
            //expDetailId = new URLSearchParams(window.location.search).get('expDetailId');

            var pageParams = JSON.parse(AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpReportPrint', 'sessionData':'', 'action':'get'}"));
            expDetailId = !IsEmpty(pageParams) && !IsEmpty(pageParams.expDetailId) ? pageParams.expDetailId : 0;

            if (parseInt(expDetailId) > 0)
                ShowReferenceDetails(expDetailId);
            else
                return ShowError('Expense Report  Details not found.');
        });
 
        /* To get expense api host url */
        function GetExpenseApiHost() {

            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';

            if (IsEmpty(apiHost))
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";

            return apiHost;
        }

        /* Getting the Report reference Datails */
        function ShowReferenceDetails(expDetailId) {

            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expDetailId };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getExpReportDetails';
            WebApiReq(apiUrl, 'POST', reqData, '', BindReportData, null, null);
        }

        /* Binding the Report reference Datails */
        function BindReportData(response) {

            if (!IsEmpty(response.reportBody)) {

                $('#spaneR_Date').text(response.reportHeader[0].eR_Date);
                $('#spaneR_Title').text(response.reportHeader[0].eR_Title);
                $('#spaneR_RefNo').text(response.reportHeader[0].eR_RefNo);
                $('#spaneR_Purpose').text(IsEmpty(response.reportHeader[0].eR_Purpose) ? '' : response.reportHeader[0].eR_Purpose);
                $('#spaneR_CompVisited').text(IsEmpty(response.reportHeader[0].eR_CompVisited) ? '' : response.reportHeader[0].eR_CompVisited);
                $('#spanemployeeId').text(response.reportHeader[0].employeeId);
                $('#spanemployeeName').text(response.reportHeader[0].employeeName);
                $('#spancostCentre').text(response.reportHeader[0].costCentre);
                $('#spanerM_ReimbursementAmount').text(response.reportHeader[0].erM_ReimbursementAmount);
                $('#spanapproverName').text(response.reportHeader[0].approverName);
                $('#spanapprovalStatus').text(response.reportHeader[0].approvalStatus);

                var tdval = '<span class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:100%">';
                var header = '<span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:100%">Approver Name</span>';
                var approverNames = response.reportHeader[0].approverName;
                var approverNamesHtml = "<table><tr><td>" + header + "</td><td>" + tdval + approverNames.replace(/\|/g, '</span></td></tr><td>' + header + '<td>' + tdval) + "</tr></table>";
                $('#spanapproverName').html(approverNamesHtml);


                var tbodytrs = [], employeeClaimAmount = '', companyClaimAmount = '', currency = '', td = '<td style="border-top:1px solid #cecece;padding:8px 10px">';
                var tdMsgs = '<td colspan="6" style="border-top:0;color:red;font-size:11px;padding:0 10px 5px 10px">';
                var decimalValue = '<%=Settings.LoginInfo.DecimalValue%>';

                // Report Expenses
                for (var j = 0; j < response.reportBody.length; j++) {

                    employeeClaimAmount = parseFloat((IsEmpty(response.reportBody[j]["employeeDisbursement"]) ? 0 : response.reportBody[0]["employeeDisbursement"]));
                    companyClaimAmount = parseFloat((IsEmpty(response.reportBody[j]["companyDisbursement"]) ? 0 : response.reportBody[0]["companyDisbursement"]));
                    currency = (IsEmpty(response.reportBody[j]["eD_Currency"]) ? 0 : response.reportBody[0]["eD_Currency"]);
                    var tr = '<tr>';
                    tr += td + (IsEmpty(response.reportBody[j]["eD_TransDate"]) ? "" : response.reportBody[j]["eD_TransDate"]) + '</td>';
                    tr += td + (IsEmpty(response.reportBody[j]["eT_Desc"]) ? "" : response.reportBody[j]["eT_Desc"]) + '</td>';
                    tr += td + (IsEmpty(response.reportBody[j]["eD_City"]) ? "" : response.reportBody[j]["eD_City"]) + '</td>';
                    tr += td + (IsEmpty(response.reportBody[j]["pT_Desc"]) ? "" : response.reportBody[j]["pT_Desc"]) + '</td>';
                    tr += td + (IsEmpty(response.reportBody[j]["eD_TotalAmount"]) ? "" : FixedDecimalRound(response.reportBody[j]["eD_TotalAmount"], decimalValue)) + '</td>';
                    tr += td + (IsEmpty(response.reportBody[j]["reportApprovalStatus"]) ? "" : response.reportBody[j]["reportApprovalStatus"]) + '</td>';
                    tr += td + (IsEmpty(response.reportBody[j]["eD_ReimRemarks"]) ? "" : response.reportBody[j]["eD_ReimRemarks"]) + '</td>';
                    tr += '</tr>';
                    tbodytrs.push(tr);

                    if (!IsEmpty(response.reportBody[j]["expMessages"])) {

                        var messages = response.reportBody[j]["expMessages"].split('|');
                        $.each(messages, function (key, msg) {

                            tr = '<tr class="no-border">' + tdMsgs + msg + '</td></tr>';
                            tbodytrs.push(tr);
                        });
                    }
                }

                $('#tblExpenseReportsBody').html(tbodytrs.toString());
                $('#spaneD_Currency').text("Amount (" + '<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency%>' + ")");
                $('#spanemployeeDisbursement').text('<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency%>' + " " + FixedDecimalRound(employeeClaimAmount, decimalValue));
                $('#spancompanyDisbursement').text('<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency%>' + " " + FixedDecimalRound(companyClaimAmount, decimalValue));

                if (!IsEmpty(response.reportReceipts) && response.reportReceipts.length > 0) {
                                        
                    var receipts = '<tr><td><h4 class="pl-3">Receipts</h4></td></tr>';
                    $.each(response.reportReceipts, function (key, col) {                       
                        var imagePath = col.rC_Path.replaceAll('/', '\\');
                        imagePath = imagePath.substring(imagePath.indexOf('\\Upload\\'), imagePath.Length);
                        var ext = col.rC_FileName.split('.').reverse()[0].toLowerCase();

                        receipts += '<tr>';

                        if (ext == "jpg" || ext == "jpeg" || ext == "png") {
                            receipts += '<td><div class="position-relative image-wrap"><img src="' + imagePath + '" class="mx-auto my-2 d-block" style="max-width: 100%;" ><a download="' + col.rC_FileName + '" href="' + imagePath + '"><span class="download-icon"></span></a></div></td>';
                        }
                        else if (ext === "pdf") {
                            var fileName = col.rC_FileName.split('.').slice(0, -1).join('.')
                            receipts += '<td><div class="receipt-canvas" data-file-name="' + fileName + '" data-path="' + imagePath + '" id="receipt-canvas-' + key + '"></div</td>';
                          
                        }
                        else {
                            receipts += '<td><div class="position-relative image-wrap"><img src="' + imagePath + '" class="mx-auto my-2 d-block" style="max-width: 100%;" ><a download="' + col.rC_FileName + '" href="' + imagePath + '"><span class="download-icon"></span></a></div></td>';

                        }
                        
                        receipts += '</tr>';
                    });

                    $('#tbExpReceipts').append(receipts);
                    $('#tbExpReceipts').show();
                    $('.toolbar').hide();
                }
            }
            else {
                return ShowError('Expense Report Details not found.');
            } 

            //All PDF's in Canvas
            $('.receipt-canvas').each(function () {
                var elm = $(this).attr('id'),
                    path = $(this).attr('data-path'),
                    pageName = $(this).attr('data-file-name');
                renderPDF(path, document.getElementById(elm), pageName);
            })

        }


        /* To get agent and login info */
        function GetAgentInfo() {

            try {

                var loginInfo = '<%=Settings.LoginInfo == null%>';

                if (loginInfo == true)
                    return apiAgentInfo;

                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* Print the expense report. */
        function printReport() {
            document.getElementById('btnPrint').style.display = "none";
            window.print();
            document.getElementById('btnPrint').style.display = "block";
        }
     

    </script>   

    <form id="expReport" runat="server">
        <span class="preheader"></span>

        <table class="body mx-auto">
            <tr>
                <td class="center" align="center" valign="top">
                    <center data-parsed><table width="100%" border="0" cellspacing="0" cellpadding="0" class="receipt-wrapper float-center" align="center" style="-webkit-print-color-adjust:exact;border:1px solid #cacbcb;color:#373737;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:normal;margin:20px auto 0 auto;padding:10px;position:relative;width:650px"><tbody><tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="receipt-header"><tbody><tr><td><img src="<%=serverPath %>" alt width="130px"></td><td class="receipt" style="font-weight:700;line-height:20px;text-align:left"><div style="font-size:16px;padding-top: 10px;padding-left: 10px;">Expense Report :<span id="spaneR_Title"> </span></div><button style="float:right" class="btn btn-primary mr-2" id="btnPrint" onclick="return printReport()">Print Report</button></td></tr></tbody></table></td></tr><tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0" class="receipt-general-info" style="padding-bottom:10px;padding-top:10px"><tbody><tr><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:1px solid #e6e6e6;border-top:0;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%"><span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Approval Status</span> <span id="spanapprovalStatus" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%"></span></td><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;border-top:0;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%"></td></tr><tr><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:1px solid #e6e6e6;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%"><span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Date</span> <span id="spaneR_Date" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%"></span></td><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%"><span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Report Ref:</span> <span id="spaneR_RefNo" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%"></span></td></tr><tr><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:1px solid #e6e6e6;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%"><span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Employee ID</span> <span id="spanemployeeId" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%"></span></td><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%"><span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Employee Name</span> <span id="spanemployeeName" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%"></span></td></tr><tr><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:1px solid #e6e6e6;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%"><span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Cost Centre</span> <span id="spancostCentre" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%"></span></td><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%"><span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Business Purpose</span> <span id= "spaneR_Purpose" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%"></span></td></tr><tr><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:1px solid #e6e6e6;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%"><span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Company Visited</span> <span id="spaneR_CompVisited" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%"></span></td><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%"> <span id="spanapproverName" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:70%"></span></td></tr><tr><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:1px solid #e6e6e6;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%"><span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Reimbursed Amount</span> <span id="spanerM_ReimbursementAmount" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%"></span></td><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%"></td></tr></tbody></table></td></tr><tr><td><table id="tblExpenseReports" width="100%" border="0" cellspacing="0" cellpadding="0" class="receipt-visitor-info" style="margin-bottom:0"><thead><tr><th style="box-shadow:inset 0 0 0 1000px #f5f5f5;color:#000;padding:7px 10px;text-align:left">Date</th><th style="box-shadow:inset 0 0 0 1000px #f5f5f5;color:#000;padding:7px 10px;text-align:left">Expense Type</th><th style="box-shadow:inset 0 0 0 1000px #f5f5f5;color:#000;padding:7px 10px;text-align:left">City Of Purchse</th><th style="box-shadow:inset 0 0 0 1000px #f5f5f5;color:#000;padding:7px 10px;text-align:left">Payment Type</th><th style="box-shadow:inset 0 0 0 1000px #f5f5f5;color:#000;padding:7px 10px;text-align:left"><span id="spaneD_Currency"></span></th><th style="box-shadow:inset 0 0 0 1000px #f5f5f5;color:#000;padding:7px 10px;text-align:left">Status</th><th style="box-shadow:inset 0 0 0 1000px #f5f5f5;color:#000;padding:7px 10px;text-align:left">Remarks</th></tr></thead><tbody id="tblExpenseReportsBody"></tbody></table></td></tr><tr><td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px" class="receipt-fee-info"><tbody><tr class="last"><td style="border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:1px solid #e6e6e6;font-weight:700;padding:4px 8px;text-align:right">Company Disbursements<br><span style="color:#797777">To be paid by Company</span></td><td  style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;box-shadow:inset 0 0 0 1000px #f5f5f5;font-weight:700;padding:4px 8px;text-align:right"><span id="spancompanyDisbursement"></span></td></tr><tr class="last"><td style="border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:1px solid #e6e6e6;font-weight:700;padding:4px 8px;text-align:right">Employee Disbursements<br><span style="color:#797777">To be paid by Employee</span></td><td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;box-shadow:inset 0 0 0 1000px #f5f5f5;font-weight:700;padding:4px 8px;text-align:right"><span  id="spanemployeeDisbursement"></span></td></tr></tbody></table></td></tr></tbody></table></center>
                </td>
            </tr>
        </table>
        <table id="tbExpReceipts" width="100%" border="0" cellspacing="0" cellpadding="0" class="receipt-wrapper float-center" align="center" style="display:none;-webkit-print-color-adjust:exact;border:1px solid #cacbcb;color:#373737;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:normal;margin:20px auto 0 auto;padding:10px;position:relative;width:650px">
            
        </table>
        <!-- prevent Gmail on iOS font size manipulation -->
        <div style="display: none; white-space: nowrap; font: 15px courier; line-height: 0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>

    </form>
</body>
</html>

