﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="ReligareProductDetailsMaster.aspx.cs" Inherits="CozmoB2BWebApp.ReligareProductDetailsMaster" %>

<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
 <%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
  
        <div><h3>Religare Product Details Master</h3></div>
         <div class="body_container" style="height:200px">
        <div class="col-md-12 padding-0 marbot_10">

            <div class="col-md-2">
                <asp:Label ID="lblProductTypeId" runat="server" Text="Product Type:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:DropDownList CssClass="form-control" ID="ddlProductTypeId" runat="server"  AutoPostBack="true"></asp:DropDownList>
            </div>

            <div class="col-md-2">
                <asp:Label ID="lblProductId" runat="server" Text="Product  Id:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtproductId" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>
            </div>
           
            <div class="col-md-2">
                <asp:Label ID="lblProductName" runat="server" Text="Product Name:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtProductName" runat="server" onKeyPress="return blockSpecialChar(event)" ></asp:TextBox>
            </div>
             <div class="clearfix"></div>
            </div>
    <div class="col-md-12 padding-0 marbot_10">
            <div>
                <div class="col-md-2">
                Select TripType
                    </div>
                <div class="col-md-2">
                <asp:DropDownList ID="ddlTripType" CssClass="form-control" Style="width: 200px;"
                runat="server">
                <asp:ListItem Value="0">-- Select TripType --</asp:ListItem>
                <asp:ListItem Value="SINGLE">SINGLE</asp:ListItem>
                <asp:ListItem Value="MULTI">MULTI</asp:ListItem>
            </asp:DropDownList>
                    </div>
            </div>
        </div>
        <div class="col-md-12">
            <label style=" padding-right:5px" class="f_R"><asp:Button  ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();" CssClass="btn but_b"  OnClick ="btnSave_Click" ></asp:Button></label>
                 
                 
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b"   OnClick="btnClear_Click"></asp:Button></label>
                    
                    
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="btn but_b"   OnClick="btnSearch_Click"></asp:Button></label>
            </div>

            <div class="col-md-10">
            <asp:Label ID="lblSuccessMsg" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
        </div>
            
        </div>
   

    <script type="text/javascript">
        function Save() {
            if (getElement('ddlProductTypeId').value == -1) addMessage('Product Type Id cannot be blank!', '');
            if (getElement('txtproductId').value == '') addMessage('Plan Id cannot be blank!', '');
            if (getElement('txtProductName').value == '') addMessage('Plan Name cannot be blank!', '');
            if (getElement('ddlTripType').value == 0) addMessage('Trip Type cannot be blank!', '');
        if (getMessage() != '') {
            //alert(getMessage());
            alert(getMessage()); clearMessage(); 
            return false;
           }
        }
         function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
        
    </script>
   <asp:HiddenField runat="server" ID="hdfEMId" Value="0" />
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server"> 

 <asp:GridView ID="gvSearch" Width="100%" runat="server" visible="true" AllowPaging="true" DataKeyNames="DETAIL_ID"
      emptydatalist="No Location List!" AutoGenerateColumns="false" PageSize="15" GridLines="None"
     CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4"
     CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">

      

     <Columns>
         <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
         <asp:TemplateField HeaderText ="SL.NO">
        <ItemTemplate>
             <%#Container.DataItemIndex+1 %>
        </ItemTemplate>
    </asp:TemplateField>
         <asp:TemplateField>
            
        <ItemTemplate >
    <asp:Label ID="ITlblproductId" runat="server" Text='<%# Eval("PRODUCTNAME") %>' CssClass="label grdof" ToolTip='<%# Eval("PRODUCTNAME") %>'  ></asp:Label>
   
    </ItemTemplate>  
             <HeaderTemplate>
                 Product Name
             </HeaderTemplate>
         </asp:TemplateField>
         <asp:TemplateField>
            
        <ItemTemplate >
    <asp:Label ID="ITlblproductId" runat="server" Text='<%# Eval("PRODUCT_ID") %>' CssClass="label grdof" ToolTip='<%# Eval("PRODUCT_ID") %>'  ></asp:Label>
   
    </ItemTemplate>  
             <HeaderTemplate>
                 Product Id
             </HeaderTemplate>
         </asp:TemplateField>
          <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlblProductName" runat="server" Text='<%# Eval("PRODUCTTYPE_NAME") %>' CssClass="label grdof" ToolTip='<%# Eval("PRODUCTTYPE_NAME") %>' ></asp:Label>
   
    </ItemTemplate>  
              <HeaderTemplate>
                 Product Type
             </HeaderTemplate>
         </asp:TemplateField>
     </Columns>

     </asp:GridView>
    </asp:Content>

