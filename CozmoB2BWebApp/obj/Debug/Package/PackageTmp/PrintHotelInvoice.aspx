<%@ Page Language="C#" AutoEventWireup="true" Inherits="PrintHotelInvoice" Codebehind="PrintHotelInvoice.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace = "CT.BookingEngine" %>
<%@ Import Namespace = "CT.Core" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Print Hotel Invoice</title>
    <link href="style.css" rel="stylesheet" />
    <script type="text/javascript" src="CT.js"></script>
    </head>
<script language="JavaScript" type="text/javascript">
function ClosePop()
{            
    window.close();
}
</script>
<script language="JavaScript" type="text/javascript">
<!--

/* The actual print function */

function prePrint()
{
    document.getElementById('Print').style.display="none";
	window.print();	
	document.getElementById('Print').style.display="block";
}

// -->
</script>
<body>
    <form id="form1" runat="server">
    <%if(Request["isSave"]!="true")
      { %>
    <div>
    <div class="invoice-parent-block fleft">
        <div class="fleft border-bottom-black width-100">
            <div class="padding-10 fleft">
                <div class="fleft font-16 bold width-240">
                    <span>Invoice No-</span><span class="margin-left-15"><%=invoice.CompleteInvoiceNumber%></span>
                </div>
                <div class="fleft" style="margin-left: 50px;">
                   Invoice Date :   <b><% = invoice.CreatedOn.ToString("dd MMMM yyyy") %></b>
                </div>
                <div class="fright bold width-240 right-align">
                    Confirmation No: <% = confNo %></div>
            </div>
        </div>
        <div class="padding-10 fleft" id="InvoicePP" bordercolor="AliceBlue" borderstyle="Solid">
                <div class="fleft">       
                <div class="fleft">
                <%if (Request["fromAgent"] == null && invoice.AgencyId == 1)
                  { %>
                <div class="font-11 padding-top-5" style="width:200px">
                <div class="fleft bold">
                    Billing Address
                </div>
                <div class="fleft">
                    <%--<textarea id="BillingAddress" rows="3" cols="40" style="overflow:auto"></textarea>--%>
               <asp:TextBox ID="BillingAddress" Columns="0" Width="310px" Rows="3" runat="server" TextMode="MultiLine" BorderColor="white" CssClass="noScroll"></asp:TextBox>
                </div>
                </div>
                <%}
                  else
                  {
                      AgentMaster selfAgency = new AgentMaster(1);
                      %>
                    <div class="font-11 padding-top-5 width-240">
                    <div><%=selfAgency.Name %></div>
                    <div><%=selfAgency.Address%> 
                        
                        <%if (selfAgency.City.Length > 0)
                          { %>
                        <%=selfAgency.City%>
                          <%}
                          else
                          {%>
                             <% =RegCity.GetCity(cityId).CityName%>
                          <%}%>
                    </div>
                    <div>
                        <%  if (selfAgency.Phone1 != null && selfAgency.Phone1.Length != 0)
                            {%>
                        Phone:
                        <% = agency.Phone1%>
                        <%  }
                            else if (selfAgency.Phone2 != null && selfAgency.Phone2.Length != 0)
                            { %>
                        Phone:
                        <% = selfAgency.Phone2%>
                        <%  } %>
                        <%  if (selfAgency.Fax != null && selfAgency.Fax.Length > 0)
                            { %>
                        Fax:
                        <% = selfAgency.Fax%>
                        <%  } %>
                    </div>
                     
                  </div>
                  <%} %>
                </div>
                <div class="fleft bold font-size-15" style="margin-left: 45px;">
                    INVOICE
                </div>
                    <div class="font-11 padding-top-5 fright width-240 right-align">
                    <div class="bold">
                        <% = agency.Name %>
                    </div>
                    <div>
                        <% = agencyAddress %>
                        
                        <%  %>
                    </div>
                    <div>
                        <%  if (agency.Phone1 != null && agency.Phone1.Length != 0)
                            {%>
                        Phone:
                        <% = agency.Phone1%>
                        <%  }
                            else if(agency.Phone2 != null && agency.Phone2.Length != 0)
                            { %>
                        Phone:
                        <% = agency.Phone2%>
                        <%  } %>
                        <%  if(agency.Fax!=null&&agency.Fax.Length > 0)
                            { %>
                        Fax:
                        <% = agency.Fax %>
                        <%  } %>
                        <br />
                        <b> Service Tax Reg. No : <%=ConfigurationManager.AppSettings["ServiceTaxRegNo"].ToString() %></b>
                    </div>
                </div>
            </div>
            <div class="fleft margin-top-15 font-11 border-y" style="width: 742px; padding: 5px;">
                <div class="fleft width-100 bold">
                    <div class="fleft width-20">
                        <img alt="spacer" src="images/spacer.gif" /></div>
                    <div class="fleft margin-left-3 width-110 center">
                        Hotel Name</div>
                    <div class="fleft margin-left-10 width-130 center">
                        Room Type</div>
                    <div class="fleft margin-left-10 width-100px center">
                        PAX Name</div>
                    <div class="fleft margin-left-10 width-40 center">
                        Rooms</div>
                   <div class="fleft margin-left-10 width-50 center">
                        Nights</div>
                   <div class="fleft margin-left-10 width-80 center">
                       Rate</div>
                    <%if(itinerary.IsDomestic){ %>
                    <div class="fleft margin-left-10 width-60 center">
                        Tax</div>
                          <div class="fleft margin-left-10 width-50 center">
                        O/C</div>
                        <%}else{ %>
                           <div class="fleft margin-left-10 width-60 center">
                        ROE</div>
                          <div class="fleft margin-left-10 width-50 center">
                        Currency</div>
                        <%} %>
                  
                </div>
                <%  int i = 0;
                    float plbInPercent = 0;
                    System.TimeSpan diffResult = itinerary.EndDate.Subtract(itinerary.StartDate);
                    %>
                <%  foreach (HotelRoom hotelRm in hRoomList)
                    {
                        string bookingClass = string.Empty;     
                        if (hotelRm.Price.PublishedFare != 0 && hotelRm.Price.PublishedFare - hotelRm.Price.OurCommission != 0)
                        {
                            plbInPercent = Convert.ToSingle((hotelRm.Price.AgentPLB * 100) / (hotelRm.Price.PublishedFare - hotelRm.Price.OurCommission));
                        }
                       
                        i++;
                %>
                <div class="fleft width-100 margin-top-8">
                    <div class="fleft width-20">
                        <img alt="spacer" src="images/spacer.gif" /></div>
                    <div class="fleft margin-left-3 width-110 center">
                     <%=itinerary.HotelName%></div>
                    <div class="fleft margin-left-10 width-130 center">
                        <% = hotelRm.RoomName %></div>
                    <div class="fleft margin-left-10 width-100px center">
                        <% = itinerary.HotelPassenger.Firstname %> <%= itinerary.HotelPassenger.Lastname %></div>
                    <div class="fleft margin-left-10 width-40 center">
                        <% = hotelRm.NoOfUnits %></div>
                    <div class="fleft margin-left-10 width-50 center">
                       <%= diffResult.Days %></div>
                       
                       
                       
                       
                     <%if(hotelRm.Price.AccPriceType==PriceType.PublishedFare){ %>
                    <div class="fleft margin-left-10 width-80 right-align">
                        <% = (hotelRm.Price.PublishedFare* hotelRm.Price.RateOfExchange).ToString("N2")%></div>
                        <%}else if(hotelRm.Price.AccPriceType==PriceType.NetFare){%>
                        <div class="fleft margin-left-10 width-80 right-align">
                        <% = ((hotelRm.Price.NetFare + hotelRm.Price.Markup - hotelRm.Price.Discount) * hotelRm.Price.RateOfExchange).ToString("N2")%></div>
                        <%} %>
                     <%if(itinerary.IsDomestic) { %>
                    <div class="fleft margin-left-10 width-60 right-align">
                        <% = (hotelRm.Price.Tax* hotelRm.Price.RateOfExchange).ToString("N2") %></div>
                        <div class="fleft margin-left-10 width-50 right-align">
                        <% = (hotelRm.Price.OtherCharges * hotelRm.Price.RateOfExchange).ToString("N2")%>
                    </div>
                        <%}else { %>
                        <div class="fleft margin-left-10 width-60 right-align">
                        <% = hotelRm.Price.RateOfExchange.ToString("N2") %></div>
                        <div class="fleft margin-left-10 width-50 right-align">
                        <%=CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"]  %>
                    </div>
                        <%} %>
                    
                </div>
                <%  } %>              
            <div class="fleft width-100 margin-top-10 margin-right-30">
              <div class="fleft width-100 margin-top-5">
                    <b>City:</b>
                    <%=itinerary.CityRef %>
                </div>
                <div class="fleft width-100 margin-top-5">
                    <b>Check In:</b>
                    <%=itinerary.StartDate.ToString("dd MMM yy") %>
                </div>
                <div class="fleft width-100 margin-top-5">
                    <b>Check Out:</b> 
                    <%=itinerary.EndDate.ToString("dd MMM yy") %>
                </div>
            </div><br />
                <%--<div class="fleft width-100 margin-top-5 bold">
                    <a href="#">Save</a></div>--%>
            </div>
            <%if(itinerary.HotelPolicyDetails!=null && itinerary.HotelPolicyDetails.Length>0){ %>
            <div class="fleft width-300 margin-top-10 margin-right-20">
                <div class="fleft width-100 margin-top-5 bold">
                    Remarks:</div>
                    
                <%--<div class="fleft width-100 margin-top-5">
                <%if (Request["plbString"] == "0")
                  {%>
                    <%= remarks%>
                    Voidation Rs 500
                    <%}
                       else
                       { %>
                    <%= remarks%>
                    ***<%=Request["plbString"]%>*** Voidation Rs 500
                    <%} %></div>--%>
                    
                <div class="clear">
                 <ul>
                  <%  string[] tempStr = itinerary.HotelPolicyDetails.Split('|');
                            for (int j = 0; j < tempStr.Length - 1; j++)
                            { %>
              <li>  <%=tempStr[j]%> </li>
                    <%} %>
                      </ul>
                </div>
                    
            </div>
            <%} %>
            <div class="fright width-300 margin-top-10">
                <div class="fleft width-100 margin-top-5 bold">
                    
                    <div class="fleft width-140 padding-left-50">Gross:</div>
                    <div class="width-80 fleft text-right">
                    <%decimal totalPrice=0;
                      decimal handlingCharge=0;
                      decimal serviceTax=0;
                      decimal tdsCommission=0;
                      decimal tdsPLB=0;
                      decimal plb = 0;
                      decimal transactionFee = 0;
                      PriceAccounts price = new PriceAccounts();
                      price = hRoomList[0].Price;
                      decimal rateofExchange = price.RateOfExchange;

                      string symbol = Util.GetCurrencySymbol(price.Currency);
                      if (hRoomList[0].Price.AccPriceType == PriceType.PublishedFare)
                      {
                          for (int k = 0; k < hRoomList.Length; k++)
                          {
                              totalPrice = totalPrice + (hRoomList[k].Price.PublishedFare + hRoomList[k].Price.OtherCharges-hRoomList[k].Price.Discount);
                              //handlingCharge = handlingCharge + (hRoomList[k].Price.AgentCommission);
                              //serviceTax = serviceTax + (hRoomList[k].Price.SeviceTax);
                              //tdsCommission = tdsCommission + hRoomList[k].Price.TdsCommission;
                              //plb = plb + hRoomList[k].Price.AgentPLB;
                              //tdsPLB = tdsPLB + hRoomList[k].Price.TDSPLB;
                              //transactionFee = transactionFee + hRoomList[k].Price.TransactionFee;

                          }
                      }
                      else if (hRoomList[0].Price.AccPriceType == PriceType.NetFare)
                      {
                          for (int k = 0; k < hRoomList.Length; k++)
                          {
                              totalPrice = totalPrice + (hRoomList[k].Price.NetFare + hRoomList[k].Price.Markup + hRoomList[k].Price.OtherCharges-hRoomList[k].Price.Discount);
                              //serviceTax = serviceTax + (hRoomList[k].Price.SeviceTax);
                              //tdsCommission = tdsCommission + hRoomList[k].Price.TdsCommission;
                              //plb = plb + hRoomList[k].Price.AgentPLB;
                              //tdsPLB = tdsPLB + hRoomList[k].Price.TDSPLB;
                              //transactionFee = transactionFee + hRoomList[k].Price.TransactionFee;
                          }
                      }
                      //totalPrice=totalPrice*rateOfExc;
          %>
                        
                          <%=CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"]  %> <%=(totalPrice * rateofExchange).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) %></div>
                </div>
                <input type="hidden" id="MaxHandling" value='<%=(handlingCharge+plb)*rateofExchange%>' />
                <%if(itinerary.IsDomestic){ %>
                <div class="fleft width-100 margin-top-5">
                    <div class="fleft width-50 italic">
                        Less
                    </div>
                    <div class="fleft width-140">
                        Handling Charges</div>
                        <%if (Request["fromAgent"] == null && invoice.AgencyId == 1)
                          { %>
                    <div class="width-80 fleft text-right" >
                    <%=(handlingCharge * rateofExchange).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                    
                        </div>
                        <%} 
                          else
                          {%>
                          <div class="width-80 fleft text-right" >
                            <%=((handlingCharge + plb) * rateofExchange).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%>
                        </div>
                          <%} %>
                    <div class="clear" id="Errordiv" style="display:none;color:Red;">ss
                    </div>
                </div>
                <div class="fleft width-100 margin-top-5">
                    <div class="fleft width-50 italic">
                        Add
                    </div>
                    <div class="fleft width-140">
                        Service Tax</div>
                    <div class="width-80 fleft text-right">
                        <%=(serviceTax * rateofExchange).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%></div>
                </div>
                <div class="fleft width-100 margin-top-5">
                    <div class="fleft width-50 italic">
                        Add
                    </div>
                    <div class="fleft width-140">
                        Tra Fee</div>
                    <div class="width-80 fleft text-right">
                        <%=(transactionFee * rateofExchange).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%></div>
                </div>
                <div class="fleft width-100 margin-top-5 bottom-border">
                    <div class="fleft width-50 italic">
                        Add
                    </div>
                    <div class="fleft width-140">
                        TDS Deducted</div>
                    <div class="width-80 text-right fleft" id="TDSHandling">
                        <%=((tdsCommission + tdsPLB) * rateofExchange).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%></div>
                </div>
                <%} %>
                <div class="fleft width-100 margin-top-5">
                    
                    <div class="fleft width-140 padding-left-50">Net Amount</div>
                    <div class="width-80 fleft text-right" id="NetAmount">
                              <%=CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"]  %> <%=Convert.ToDouble((totalPrice - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee) * rateofExchange).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%></div>
                </div>
                <div class="fleft width-280 margin-top-5">
                    
                    <div class="fleft width-140 padding-left-50">Net Receivable</div>
                    <div class="width-80 fleft text-right" id="NetRecievable" >
                             <%=CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"]  %>  <%=Convert.ToDouble((totalPrice - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee) * rateofExchange).ToString(CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"])%></div>
                </div>
                <%if(!itinerary.IsDomestic && itinerary.Source!=HotelBookingSource.IAN){ %>
                <div class="fleft width-280 margin-top-5">
                    
                    <div class="fleft width-140 padding-left-50">Total Amount</div>
                     
                    <div class="width-80 fleft text-right" id="Div1" >
                       <%=CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"]  %> <%=Math.Round((totalPrice-handlingCharge-plb+serviceTax+tdsCommission+tdsPLB+transactionFee)*rateofExchange,Convert.ToInt32(CT.Configuration.ConfigurationSystem.LocaleConfig["RoundPrecision"] ))%></div>
                </div>
                   <div class="fleft width-140 margin-top-5">
                    
                <%} %>
             
                </div>
            </div>
            <br />
            <div class="fleft width-100" style="width: 500px;">
                
                <% UserMaster member = new UserMaster(invoice.CreatedBy); %>
                <div class="width-100">
                    Billed by : <%= CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"]  %>

                </div>
                <div class="width-100">
                    Issued by :
                    <%= member.FirstName + " " + member.LastName%>
                </div>
                
            </div>
            
         
        </div>
        <div class="text-right width-100 fright">
            <input id="Print" onclick="prePrint()" type="button" value="Print" />
        </div>
    </div>
        
    </div>
    <%} %>
    </form>
</body>
</html>
