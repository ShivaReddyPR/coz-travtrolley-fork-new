﻿<%@ Page  Language="C#" AutoEventWireup="true" Inherits="ViewInvoiceforPackage" Codebehind="ViewInvoiceforPackage.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme %>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<html xmlns="<%=Request.Url.Scheme %>://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>ViewInvoice For Package</title>
      <link rel="stylesheet" href="css/main-style.css" />
    <link href="css/cozmovisa-style.css" rel="stylesheet" />
    <link href="css/override.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />

    <script type="text/javascript">
        function printPage() {
            document.getElementById('btnPrint').style.display = "none";
            window.print();
            setTimeout('showButtons()', 1000);
        }
        function showButtons() {
            document.getElementById('btnPrint').style.display = "block";
        }

</script>
    <style type="text/css">
        .style2
        {
            height: 30px;
        }
    </style>
</head>
<body>

    <div class="body_container">
           

           <h3> <center> Invoice </center> </h3>
        


           
            <div style="display:none;" class="center_pop" id="emailBlock" style=" width:300px">
                <div class="bg_gray"> Enter Your Email Address <a onclick="return HidePopUp();" class="fcol_fff cursor_p close_dark"> X</a> </div>
                <div class="bg_white bor_gray padbot4">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tbody><tr>
                            <td height="40" align="center"> <b style="display: none; color:Red" id="err"></b>
                                <input name="txtEmailId" type="text" id="txtEmailId" style=" border: solid 1px #ccc; width:90%; padding:2px;"> </td>
                        </tr>
                        <tr>
                            <td height="40" align="center"> <a id="btnEmailVoucher" class=" btn but_business" onclick="return Validate();"> Send Email </a> </td>
                        </tr>
                    </tbody></table>
                </div>
            </div>
          
          
            <div>
                <label class="pull-right marright_10 martop_10">
                    <a class="cursor_p" id="btnPrint" onclick="return printPage();"> Print </a>
                </label>
              <%--  <label class="pull-right marright_10  martop_10">
                    <a class="cursor_p" id="btnEmail" onclick="return ShowPopUp(this.id);"> Email </a>
                </label>--%>
                <div class="clearfix"> </div>
            </div>
          
          

<%--
          <div class="medialogo"> 
          
         <center>  <img width="160px" src="<%=Request.Url.Scheme %>://www.stictravel.com/ourpartners/images/Royal%20Brunei-new%20logo.png"></center>
          
          </div>--%>

        <%if(dtInvoiceDetails != null && dtInvoiceDetails.Rows.Count >0) %>
              <%{ %>
        <%foreach(System.Data.DataRow dr in dtInvoiceDetails.Rows) %>

         <%{ %>
             <div class="media" style=" font-size:16px">
          <div> 
          
          <div class="col-md-6 padleft0 pad_xs0 media50"> 
          
          <div class="bgdark_brown">
                        <label><strong>Client Details</strong> </label>
                    </div>            

<table class="table table-bordered bg_white">

<tbody><tr> 
<td> Agency Name: </td>
<td> <% =Convert.ToString(dr["Agency_Name"]) %></td>
</tr>

<tr> 
<td> Agency Code:</td>
<td>  <% =Convert.ToString(dr["Agency_Code"]) %></td></td>
</tr>


<tr> 
<td> Agency Address:</td>
<td>  <% =Convert.ToString(dr["Agency_Address"]) %></td></td>
</tr>

<tr> 
<td> Telephone No:</td>
<td>   <% =Convert.ToString(dr["Telephone_No"]) %></td>  </td>
</tr>

</tbody></table>

            
          </div>






					


           <div class="col-md-6 padright0 pad_xs0 media51"> 
           
           
           <div class="bgdark_brown">
                        <label><strong>Invoice Details</strong> </label>
                    </div>








<%if(invoice != null) %>
               <%{ %>

<table class="table table-bordered bg_white">

<tbody><tr> 
<td> Invoice No: 	</td>
<td> <%=invoice.CompleteInvoiceNumber%></td>
</tr>



<tr> 
<td> Invoice Date: </td>
<td>   	<% = invoice.CreatedOn.ToString("dd MMMM yyyy") %> </td>
</tr>

</tbody></table>
               <%} %>
             
           </div>
          
           <div class="clearfix"> </div>
          </div>
          


 <div> 
          
          <div class="col-md-6 padleft0 pad_xs0 media50"> 
          
          <div class="bgdark_brown">
                        <label><strong>Booking Details</strong> </label>
                    </div>


	
 	
 	
 	

 	



<table class="table table-bordered bg_white">

<tbody><tr> 
<td> Package Start Date: 	</td>
<td> 	<% =Convert.ToString(dr["Package_Start_Date"]) %></td>
<td> 		</td>
<td>   </td>


</tr>







<tr> 
<td> Check In: 	 </td>
<td>   	<% =Convert.ToString(dr["Check_In"]) %>	 </td>

<td> Check Out: 		</td>
<td>  <% =Convert.ToString(dr["Check_Out"]) %> </td>
</tr>


<tr> 
<td> No. of Room(s): </td>
<td>   	<% =Convert.ToString(dr["RoomCount"]) %>	 </td>




</tr>



</tbody></table>

          
          </div>






					


           <div class="col-md-6 padright0 pad_xs0 media51"> 
           
           
           <div class="bgdark_brown">
                        <label><strong>Package Details</strong> </label>
                    </div>








<table class="table table-bordered bg_white">

<tbody><tr> 
<td> Package Name: 		</td>
<td> 	<% =Convert.ToString(dr["packageName"]) %></td>
</tr>



<tr> 
<td>Reference No.	 </td>
<td>  <% =Convert.ToString(dr["TripId"]) %> </td>
</tr>



<tr> 
<td> City: 	 	</td>
<td>  <% =Convert.ToString(dr["City"]) %> </td>
</tr>




</tbody></table>


           
           
           </div>
          
           <div class="clearfix"> </div>
          </div>


       <div> 
          
          <div class="col-md-6 padleft0 pad_xs0 media50"> 
          
          <div class="bgdark_brown">
                        <label><strong> Lead Passenger Details</strong> </label>
                    </div>



<table class="table table-bordered bg_white">

<tbody><tr> 
<td>Pax Name: 	 </td>
<td> <b>  <% =Convert.ToString(dr["Pax_Name"]) %> </b></td>
</tr>

<tr> 
<td>Invoiced By: 		 </td>
<td> <% =Convert.ToString(dr["Agency_Name"]) %>  </td>
</tr>















</tbody></table>

          
          </div>






					


           <div class="col-md-6 padright0 pad_xs0 media51"> 
           
           
           <div class="bgdark_brown">
                        <label><strong>Rate Details</strong> </label>
                    </div>


<table class="table table-bordered bg_white">







<tbody><tr> 
    <%if(dr["Promo_Code"] != DBNull.Value && !string.IsNullOrEmpty(dr["Promo_Code"].ToString())){ %>
<tr> 
<td><b>Applied Promo Code:</b> 		 </td>
<td>  <span style=" color:Red"> <%=Convert.ToString(dr["Promo_Code"]) %> </span>  </td>
</tr>
    <%} %>
<tr> 
<td><b> Total Amount:</b> 		 </td>
    <%decimal totalPrice = Math.Ceiling(Convert.ToDecimal(dr["TotalAmount"]));
      if(dr["Promo_Discount_Value"] != DBNull.Value)
      {
          totalPrice = totalPrice - Math.Ceiling(Convert.ToDecimal(dr["Promo_Discount_Value"]));
      }
       %>
<td>  <span style=" color:Red"> <% =Convert.ToString(dr["Currency"]) %> <%=totalPrice %></span>  </td>
</tr>


</tbody></table>


           
           
           </div>
          
           <div class="clearfix"> </div>
          </div>


         

</div>

          <%}%>
         <%}%>

            <div class="clearfix"> </div>
        </div>



</body>
</html>
