﻿<%@ Page Language="C#" %>
	<!DOCTYPE html>
	<html>

	<head id="Head1" runat="server">
		<title>About Us</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.ico">

		<link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="css/RegisterStyle.css" rel="stylesheet" type="text/css" />
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<!-- manual css -->

		<link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
		<link href="build/css/main.min.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<div class="topHeader2">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<a href="bizzlogin.aspx"><img src="images/logoc.jpg"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="heading_bizz">
			<div class="container">
				<h2> About Us </h2>
			</div>
		</div>
		<form runat="server" id="form1">
			<div class="container">
				<div class="innerPage2">
					<div class="row">
						<div class="col-12">
							<p>CCTM, Business Travel, Personalized, Simplified & Delivered. We are the business travel management arm of Cozmo Travel World, we have been simplifying and personalizing business travel for our clients since our inception in 2010. A part of the Air Arabia group, we have an established presence in the UAE and the Gulf region and are rapidly expanding operations into India, with 15 branches and counting.
								<br />
								<br />Globally, we have a strength of 1000+ motivated employees who are proven experts in different aspects of travel. As an IATA certified operator, member of TAFI and TAAI and approved by the Ministry of Tourism, we have very rapidly built a reputation for reliability and superior customer service.
								<br />
								<br />A one-stop-shop CCTM is equipped to streamline every aspect of travel planning and operations. We partner with our customers so they don’t have to take on the hassle and additional costs of liaising with multiple vendors.</p>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="footer_sml_bizz">Copyright 2019 Cozmo Travel World, India. All Rights Reserved.</div>

	</body>

	</html>
