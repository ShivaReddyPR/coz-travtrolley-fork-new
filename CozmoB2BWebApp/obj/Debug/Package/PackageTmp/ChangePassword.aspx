<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="ChangePasswordUI" Title="Change Password" Codebehind="ChangePassword.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
  <script type="text/javascript" language="javascript">
function setFocus()
{
document.getElementById('ctl00_ContentPlaceHolder1_txtCurrentPassword').focus();
}
function validateSave() {
    //event.returnValue = false;
    clearMessage();
    var currentPassword=getElement('txtCurrentPassword').value;
    if(currentPassword.match("^\\s{0,}$")) addMessage('Current password cannot be Blank!','');
    var newPassword=getElement('txtNewPassword').value;
    var confirmPassword=getElement('txtConfirmPassword').value;
    if(newPassword.match("^\\s{0,}$")) addMessage('New password cannot be Blank!','');
    else if(currentPassword==newPassword) addMessage('The Old & New password should not be same!','');
    else if(newPassword!=confirmPassword) addMessage('New & Confirm Password do not match!','');    
    if(getMessage()!=''){
        alert(getMessage());
        clearMessage(); return false;
    }
    else return true;
}
function clearControls()
{
    getElement('txtCurrentPassword').value='';
    getElement('txtNewPassword').value='';
    getElement('txtConfirmPassword').value='';
}
function getElement(id)
{
    return document.getElementById('ctl00_cphTransaction_'+id);
}

</script>
<div class=" paramcon" style="width:90%; margin-top:10px">
<center>
    <table>
        <tr>
            <td align="center">
                    <table border="0">
                        <tr>
                            <td>
                                <asp:Label ID="lblCurrentPassword"  Width="140px" runat="server" Text="Current Password:" ></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtCurrentPassword" runat="server" CssClass="form-control" TextMode="password" Width="146px"></asp:TextBox></td>
                        </tr>
                        
                       
                       <tr> 
                        <td height="10" colspan="2"> </td>
                       </tr>
                        
                        
                        
                        
                        
                        <tr>
                            <td>
                                <asp:Label ID="lblNewPassword" runat="server" Text="New Password:"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtNewPassword" runat="server" CssClass="form-control" TextMode="password"
                                    Width="147px"></asp:TextBox></td>
                        </tr>
                        
                        
                             <tr> 
                        <td height="10" colspan="2"> </td>
                       </tr>
                       
                       
                         <tr>
                            <td>
                                <asp:Label ID="lblConfirmPassword" runat="server" Text="Confirm Password:"></asp:Label></td>
                            <td>
                                <asp:TextBox ID="txtConfirmPassword" runat="server" CssClass="form-control" TextMode="password"
                                    Width="147px"></asp:TextBox></td>
                        </tr>
                        
                             <tr> 
                        <td height="10" colspan="2"> </td>
                       </tr>
                       
                       <% if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType != CT.TicketReceipt.BusinessLayer.AgentType.B2B2B)
                          {%>
                        <tr>
                            <td>
                            </td>
                            <td  align="left">
                                <% if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID != 3967)// For ATM User
                                    {%>
                                <label class=" f_R mar-5"><asp:Button ID="butSave" runat="server" CssClass="button" OnClick="butSave_Click" OnClientClick="return validateSave()"
                                    Text="Save" /></label>
                                    <label class=" f_R mar-5"><asp:Button ID="butClear" runat="server" CssClass="button"  OnClientClick="event.returnValue=false;clearControls();return false;"
                                    Text="Clear" /></label>
                                <%} %>

                            </td>

                        </tr>
                        <%} %>
                        
                             <tr> 
                        <td height="10" colspan="2"> </td>
                       </tr>
                       
                       
                        <tr>
                            <td>
                            </td>
                            <td><asp:HyperLink ID="lnkNewUser" runat="server"  Visible="False">New User ?</asp:HyperLink></td>
                        </tr>
                        
                             <tr> 
                        <td height="10" colspan="2"> </td>
                       </tr>
                       
                       
                        <tr>
                            <td>
                            </td>
                            <td>
                                <asp:HyperLink ID="lnkForgetPwd" runat="server"  Visible="False">Forgot Password ?</asp:HyperLink></td>
                        </tr>
                    </table>
                    
            </td>
        </tr>
    </table>
    </center>
    <asp:Label ID="lblError" runat="server" Style="color: #dd1f10" Visible="False"></asp:Label>
    <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
    </div>
</asp:Content>

