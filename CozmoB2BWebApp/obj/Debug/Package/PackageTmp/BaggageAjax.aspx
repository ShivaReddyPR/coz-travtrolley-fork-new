﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="BaggageAjax" Codebehind="BaggageAjax.aspx.cs" %>

<div class="ns-h3">
    <span style="position: absolute; right: 10px; top: 4px; z-index: 9"><a class="cursor_point" title="close" onclick="hidestuff('BaggageDiv<%=resultId %>');">
        <img src="images/close-icon4.png" alt="close"></a></span>
    Baggage Information
</div>

<table id="ctl00_cphTransaction_dlFlightResults_ctl01_BaggageTable" class="mydatagrid" cellspacing="0" cellpadding="0" border="1" width="100%">
    <tbody>
        <tr>
            <th style="width: 50%"><strong>Sector / Flight</strong></th>
            <th style="width: 50%"><strong>Baggage</strong></th>
        </tr>
        <% for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                { CT.BookingEngine.FlightInfo finfo = result.Flights[i][j];
                    string baggage = string.Empty;
                    if (!string.IsNullOrEmpty(result.BaggageIncludedInFare))
                    {
                        if (!(result.BaggageIncludedInFare.Split(',')[i].Contains("piece") || result.BaggageIncludedInFare.Split(',')[i].Contains("pc")) && !result.BaggageIncludedInFare.Split(',')[i].Contains("Airline") && !result.BaggageIncludedInFare.Split(',')[i].ToLower().Contains("kg"))
                        {
                            baggage = result.BaggageIncludedInFare.Split(',')[i] + " Kg";
                        }
                        else
                        {
                            baggage = result.BaggageIncludedInFare.Split(',')[i];
                        }
                    }
                    else {
                        baggage = "As Per Airline Policy";
                    }%>
        <tr>
            <td><%=finfo.Origin.CityCode + " - " + finfo.Destination.CityCode + " " + finfo.Airline + " " + finfo.FlightNumber %></td>
            <td><%=baggage %></td>
        </tr>
        <%}
    } %>
        
    </tbody>
</table>
<div class="term_s"><span>*</span> The information presented above is as obtained from the airline reservation system. Travtrolly does not guarantee the accuracy of this information. The baggage allowance may vary according to stop-overs, connecting flights and changes in airline rules.</div>