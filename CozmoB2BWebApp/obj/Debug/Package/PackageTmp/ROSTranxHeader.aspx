﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ROSTranxHeaderGUI" Title="ROS Tranx Header List"  ValidateRequest="false" EnableEventValidation="false" Codebehind="ROSTranxHeader.aspx.cs" %>
    <%@ MasterType VirtualPath="~/TransactionBE.master" %>
 <%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
 <script language="JavaScript" type="text/javascript" src="Scripts/jsBE/Utils.js"></script>

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="ash.js"></script>

    <script type="text/javascript" src="Scripts/Common/Common.js"></script>

    <script language="javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js" type="text/javascript"></script>
    
    <asp:HiddenField runat="server" id="hdflastDirection" value="H"></asp:HiddenField>
    <asp:HiddenField runat="server" id="hdfLoadStatus" value=""></asp:HiddenField>
    <asp:HiddenField runat="server" id="hdfEditRowId" value=""></asp:HiddenField>
    <asp:HiddenField runat="server" id="hdfFromOrTo" value=""></asp:HiddenField>
    <asp:HiddenField runat="server" id="hdfHomeDriverTime" value=""></asp:HiddenField>
    <asp:HiddenField runat="server" id="hdfAirportDriverTime" value=""></asp:HiddenField>

      
  
  
   <div class="paramcon">
   
   <div class="col-md-12 padding-0 marbot_10">   <h4> <center> ROS Transaction List</center></h4></div> 
   
 
   
        <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-3 marbot_10"> 
 
 <table> 
<tr> 
 <td width="100"><asp:Label CssClass="pull-right" ID="lblStaff" runat="server" Text="Staff:"></asp:Label> </td>
 
  <td><asp:DropDownList CssClass="form-control" ID="ddlStaff" runat="server" Enabled="true">  </asp:DropDownList> </td>

</tr>
 
 </table>
 
 
 </div>
 
 
   
    
    
    <div class="col-md-3 marbot_10"> 
    
    
     <table> 
<tr> 
 <td width="100"><asp:Label ID="lblFromDate" CssClass="pull-right" runat="server" Text="From Date:"></asp:Label></td>
 
  <td> <uc1:DateControl ID="dcFromDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true"  DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></td>

</tr>
 
 </table>
 
 
    
    
    
     </div>
    
    
    
     
     
 <div class="col-md-3 marbot_10"> 
     
     
      <table> 
<tr> 
 <td Width="100px"> <asp:Label ID="lblToDate" CssClass="pull-right" runat="server" Text="To Date:"></asp:Label></td>
 
  <td><uc1:DateControl ID="dcToDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true"  DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></td>

</tr>
 
 </table>
     
     
     
      </div>
     
     
     
     
     
       <div class="col-md-3 marbot_10"> 
        <asp:Button  ID="btnLoad" Text="Load" runat="server" OnClientClick="return ValidateLoad();" CssClass="btn but_b pull-right marright_10"  OnClick ="btnLoad_Click" ></asp:Button>
        
        
        
        <asp:Button  ID="btnClear" Text="Clear" runat="server"  CssClass="btn but_b pull-right marright_10"  OnClick ="btnClear_Click" ></asp:Button>
        </div>
        
        
       

<div class="clearfix"></div>
    </div>
    
    
       
       
    
    
   
   
   </div>
  
   <div>
   
   
   </div>
        
    
    
    


       
         <div style=" position:relative" class="paramcon grdScrlTrans">
         
         
         <div title="Location" id="divLocation" style="position: absolute; right: 10px;  display:none; z-index:9999 ">
        <table style="border:solid 1px skyblue;background-color:White" border="0" >
        <tr>
                <td valign="top" colspan="2" align="center"><asp:Label ID="lblFromTo" Text="" style="font-weight:bold" runat="server" CssClass="label" ></asp:Label></td>
        </tr>
        <tr >
            <td valign="top" align="right"><asp:Label ID="lblLocationName" Text="Location Name:" Width="100px" runat="server" style="font-weight:bold" CssClass="label" ></asp:Label></td>
            <td><asp:Label  ID="lblLocationNameValue" Text="" runat="server" Width="150px" CssClass="label" ></asp:Label> </td>
        </tr>
        <tr >
            <td valign="top" align="right"><asp:Label ID="lblLocDetails" Text="Location Details:" Width="100px" runat="server" style="font-weight:bold" CssClass="label" ></asp:Label></td>
            <td><asp:Label  ID="lblLocDetailsValue" Text="" runat="server" Width="150px" CssClass="label" ></asp:Label> </td>
        </tr>
        <tr >
            <td valign="top" align="right"><asp:Label ID="lblLocMap" Text="Location Map:" Width="100px" runat="server" style="font-weight:bold" CssClass="label" ></asp:Label></td>
            <td><asp:Label ID="lblLocMapValue" Text="" runat="server" CssClass="label" Width="150px" ></asp:Label> </td>
        </tr>
        <tr>
        <td colspan="2" align="right" valign="top">
                <asp:Button ID="btnOk" runat="server" CssClass="button" Text="Ok" Width="50px" OnClientClick="return HideDiv('view');" />
               <%-- <asp:Button ID="btnCancel" runat="server" CssClass="button" Text="Cancel" Width="60px"  OnClientClick="return HideDiv('view');" />--%>
          </td>
         </tr>
        </table>
    </div>
    
        <div title="editLocation" id="divEditLocation" style="position:absolute; right: 10px; display:none; z-index:9999 ">
        <table style="border:solid 1px skyblue;background-color:White" border="1" >
        <tr>
                <td valign="top" colspan="2" align="center"><asp:Label ID="Label1" Text="" style="font-weight:bold" runat="server" CssClass="label" ></asp:Label></td>
        </tr>
        <tr >
            <td valign="top" align="right"><asp:Label ID="lblLocName" Text="Location Name:" Width="100px" runat="server" style="font-weight:bold" CssClass="label" ></asp:Label></td>
            <td><asp:DropDownList  ID="ddlNewLocation"  runat="server" onChange="return SetNewLocDetails();" Width="150px" CssClass="label" ></asp:DropDownList> </td>
        </tr>    
         <tr >
            <td valign="top" align="right"><asp:Label ID="lblNewLocDetails" Text="Location Details:" Width="100px" runat="server" style="font-weight:bold" CssClass="label" ></asp:Label></td>
            <td><asp:TextBox  ID="txtNewLocDetails" TextMode="MultiLine" runat="server" Width="150px" Height="30px" CssClass="label" ></asp:TextBox> </td>
        </tr>      
        <tr>
        <td colspan="2" align="right" >
                <asp:Button ID="btnUpdate" runat="server" Text="Ok" Width="50px" OnClientClick="return UpdateNewLocation();" />
                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="50px"  OnClientClick="return HideDiv('edit');" />
          </td>
         </tr>
        </table>
    </div>   
            
                 <asp:GridView ID="gvROSDetails" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="RM_ID" 
    EmptyDataText="No ROS DETAILS!" AutoGenerateColumns="false" PageSize="21" GridLines="none"  CssClass="grdTable"
    CellPadding="4" CellSpacing="0"    OnPageIndexChanging="gvROSDetails_PageIndexChanging" OnRowDataBound="gvROSDetails_RowDataBound" 
    OnRowCommand="gvROSDetails_RowCommand" OnRowDeleting="gvROSDetails_RowDeleting"  ShowFooter="true">
    
     <HeaderStyle CssClass="gvHeader3" HorizontalAlign="Left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 

    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' CssClass="" ToolTip='<%# Container.DataItemIndex+1 %>' Width="20px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>  
    
     
    <asp:TemplateField>
    <HeaderStyle  Width="70px"/>
    <HeaderTemplate>
    <label>Direction</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>  
    <asp:HiddenField id="IThdfRMid" runat="server" value='<%# Eval("RM_ID") %>'></asp:HiddenField> 
    <asp:HiddenField id="IThdfDirection" runat="server" value='<%# Eval("RM_DIRECTION") %>'></asp:HiddenField> 
    
    
    <asp:DropDownList ID="ITddlDirection" onChange="return SwapLocations(this.id);" CssClass="inputDdlEnabled form-control" value='<%# Eval("RM_DIRECTION") %>' Enabled="true" Width="85px" runat="server">
    <asp:ListItem Text="H - A" Value="H"></asp:ListItem>
    <asp:ListItem Text="A - H" Value="A"></asp:ListItem>
    </asp:DropDownList>
    
    
    
    </ItemTemplate>    
    </asp:TemplateField> 
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="left"  />
    <HeaderTemplate>
    <table>    
    <tr><td >
    </td>
   <%-- <td>
    <uc1:DateControl ID="HTtxtVSDate" runat="server" DateOnly="true" />
    </td>--%>
   <%-- <td>
    <asp:ImageButton ID="HTbtnVStDate" runat="server" ImageUrl="~/Images/wg_filter.GIF" ImageAlign="AbsMiddle" OnClick="Filter_Click" />
    </td>--%>
    </tr></table>
    <label style="width:140px"><label >StaffId </label></label>    
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblStaffId" Width="145px" runat="server" Text='<%# Eval("STAFF_ID") %>' CssClass="label grdof"  ToolTip='<%# Eval("STAFF_ID") %>' ></asp:Label>                
    </ItemTemplate>                
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderStyle  Width="70px"/>
    <HeaderTemplate>
    <label >Staff Name</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:HiddenField id="IThdfEmpId" runat="server" value='<%# Eval("RM_EMPID") %>'></asp:HiddenField>
    
    <asp:DropDownList ID="ITddlEmpName" CssClass="inputDdlEnabled form-control"  Enabled="true" Width="150px" runat="server" OnSelectedIndexChanged="ITddlEmpName_SelectedIndexChanged" AutoPostBack="true">
    
    </asp:DropDownList>
    </ItemTemplate>    
    </asp:TemplateField> 
    
         <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label >Flight No</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:TextBox ID="ITtxtFlightNo" runat="server" Width="110px" Enabled="true" CssClass="inputEnabled form-control" Text='<%# Eval("RM_FLIGHTNO") %>' MaxLength="20" ></asp:TextBox>
    </ItemTemplate>    
    </asp:TemplateField>  
    
       <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label>From Sector</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>    
     <asp:TextBox ID="ITtxtFromSector" runat="server" Width="110px" Enabled="true" CssClass="inputEnabled form-control" Text='<%# Eval("RM_FROM_SECTOR") %>' MaxLength="20" ></asp:TextBox>
    <%--<uc1:DateControl Id="ITdcVisaIssueDate" Enabled="false"   Value='<%# Eval("vsd_pax_visa_issue_date").Equals(DBNull.Value)?DateTime.MinValue:(DateTime)Eval("vsd_pax_visa_issue_date")%>' runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl>--%>
    </ItemTemplate>    
    </asp:TemplateField>
     
       <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label>To Sector</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>    
     <asp:TextBox ID="ITtxtToSector" runat="server" Width="110px" Enabled="true" CssClass="inputEnabled form-control" Text='<%# Eval("RM_TO_SECTOR") %>' MaxLength="20" ></asp:TextBox>
    <%--<uc1:DateControl Id="ITdcVisaIssueDate" Enabled="false"   Value='<%# Eval("vsd_pax_visa_issue_date").Equals(DBNull.Value)?DateTime.MinValue:(DateTime)Eval("vsd_pax_visa_issue_date")%>' runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl>--%>
    </ItemTemplate>    
    </asp:TemplateField>

     
      <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label>Sign In</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>         
        <uc1:DateControl Id="ITdcSignIn" Enabled="true"   Value='<%# Eval("RM_SIGN_IN")%>' runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" TimeOnly="true" DropDownYears="10" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl>
    </ItemTemplate>    
    </asp:TemplateField>
     
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label>Sign Out</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>         
        <uc1:DateControl Id="ITdcSignOut" Enabled="true"   Value='<%# Eval("RM_SIGN_OUT")%>' runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" TimeOnly="true" DropDownYears="10" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl>
    </ItemTemplate>    
    </asp:TemplateField>
    
      <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" />
    <HeaderTemplate>
    <label>Duty Hours</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>    
     <asp:TextBox ID="ITtxtDutyHours" runat="server" Width="80px" Enabled="true" CssClass="inputEnabled form-control" Text='<%# Eval("RM_DUTY_HRS") %>' MaxLength="20" ></asp:TextBox>
    <%--<uc1:DateControl Id="ITdcVisaIssueDate" Enabled="false"   Value='<%# Eval("vsd_pax_visa_issue_date").Equals(DBNull.Value)?DateTime.MinValue:(DateTime)Eval("vsd_pax_visa_issue_date")%>' runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl>--%>
    </ItemTemplate>    
    </asp:TemplateField>
     
    <asp:TemplateField>
    <HeaderStyle  Width="70px"/>
    <HeaderTemplate>
    <label>From</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:HiddenField id="IThdfFromLocId" runat="server" value='<%# Eval("RM_FROM_LOC_ID") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfFromLocName" runat="server" value='<%# Eval("FROMLOC") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfFromLocDetails" runat="server" value='<%# Eval("locationFromDetails") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfFromLocType" runat="server" value='<%# Eval("RM_FROM_LOC_TYPE") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfFromLocMap" runat="server" value='<%# Eval("RM_FROM_LOC_MAP") %>'></asp:HiddenField>
    <asp:TextBox ID="ITlblFrom" Width="145px" runat="server" EnableViewState="true"  Enabled="false"  Text='<%# Eval("FROMLOC") %>'   style="border:none" ToolTip='<%# Eval("FROMLOC") %>' ></asp:TextBox>                
     <asp:LinkButton ID="ITlnkEditFrom"  runat="Server" OnClientClick=" return ValidateEdit(this.id)" onClick="ITlnkEditFrom_Click" Text="Edit" width="40px"  ></asp:LinkButton>
     <asp:LinkButton ID="ITlnkViewFrom"  runat="Server" OnClientClick="return GetLocationDetails(this.id,'F');" onClick="ITlnkViewFrom_Click" Text="View" width="40px"  ></asp:LinkButton>
        <%--<asp:DropDownList ID="ITddlEmpName" CssClass="inputDdlEnabled" Enabled="true" Width="85px" runat="server"> </asp:DropDownList>--%>
    </ItemTemplate>    
    </asp:TemplateField>     
     

    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="center" Width="100px" />
    <HeaderTemplate>
    <label   style=" width:150px">Date</label>
    </HeaderTemplate>
    <ItemStyle  Width="500px"/>
    <ItemTemplate  >         
        <uc1:DateControl Id="ITdcDate" Enabled="true"   Value='<%# Eval("RM_DATE")%>' runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
    </ItemTemplate>    
    </asp:TemplateField>
    
      <asp:TemplateField>
    <HeaderStyle  Width="70px"/>
    <HeaderTemplate>
    <label >To</label>
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:HiddenField id="IThdfToLocId" runat="server" value='<%# Eval("RM_TO_LOC_ID") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfToLocName" runat="server" value='<%# Eval("TOLOC") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfToLocDetails" runat="server" value='<%# Eval("locationToDetails") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfToLocType" runat="server" value='<%# Eval("RM_TO_LOC_TYPE") %>'></asp:HiddenField>
    <asp:HiddenField id="IThdfToLocMap" runat="server" value='<%# Eval("RM_TO_LOC_MAP") %>'></asp:HiddenField>
    <asp:TextBox ID="ITlblTo" Width="145px" EnableViewState="true" runat="server" Text='<%# Eval("TOLOC") %>' Enabled="false" style="border:none" ToolTip='<%# Eval("TOLOC") %>' ></asp:TextBox>                
    <asp:LinkButton ID="ITlnkEditTo"  runat="Server"  OnClientClick=" return ValidateEdit(this.id)"  onClick="ITlnkEditTo_Click" Text="Edit" width="40px"  ></asp:LinkButton>
     <asp:LinkButton ID="ITlnkViewTo"  runat="Server" OnClientClick="return GetLocationDetails(this.id,'T');" onClick="ITlnkViewTo_Click" Text="View" width="40px"  ></asp:LinkButton>
        <%--<asp:DropDownList ID="ITddlEmpName" CssClass="inputDdlEnabled" Enabled="true" Width="85px" runat="server"> </asp:DropDownList>--%>
    </ItemTemplate>    
    </asp:TemplateField>    
     
     
     <asp:TemplateField>
            <HeaderStyle Width="30px" />
            <HeaderTemplate></HeaderTemplate>
            <ItemTemplate>                
                <asp:ImageButton ID = "ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete"  ></asp:ImageButton>
            </ItemTemplate>            
            <FooterStyle HorizontalAlign="left" />                                        
            <FooterTemplate>
                <asp:LinkButton ID="lnkInsert" runat="server" Text="Add" CommandName="Insert"  OnClientClick="return ValidateDetails();" Width="50px"
                    ></asp:LinkButton>
            </FooterTemplate>
</asp:TemplateField>
   
    
    <%--<asp:TemplateField >
            
            <HeaderStyle   HorizontalAlign="center"/>
            <HeaderTemplate>
                <asp:Label ID="HTlblEdit" Text="" runat="server" Width="70px"></asp:Label>
            </HeaderTemplate>
            
            <ItemTemplate >
                <asp:LinkButton ID="ITlnkEdit"  Text="Edit"  runat="server" CssClass="label" Width="30px" CommandName="Edit" CausesValidation="True"></asp:LinkButton>
                <asp:ImageButton ID = "ibtnDelete" runat="server" ImageUrl="~/Images/Common/Intersoft/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete" OnClientClick = "return getDelConfirm()"></asp:ImageButton>
                
                
            </ItemTemplate>
            <EditItemTemplate>
                <asp:LinkButton ID="EITlnkUpdate" Text="Update" runat="server" CssClass="label" Width="30px" CommandName="Update" OnClientClick="return validateTariffDetails(this.id,'U');" CausesValidation="True"></asp:LinkButton> <asp:LinkButton ID="EITlnkCancel" Text="Cancel" runat="server" CssClass="label" Width="4px" CommandName="Cancel" CausesValidation="True"></asp:LinkButton>
            </EditItemTemplate>
        </asp:TemplateField>--%>
    
        
    </Columns>           
    </asp:GridView>
   
   
   

   </div>
      <div><asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClientClick="return Save();" OnClick="btnSave_Click" /> </div>
            

  
  <script type="text/javascript">

      function GetLocationDetails(id,fromORto) {
          //alert(document.getElementById('divLocation'));
          document.getElementById('divLocation').style.display = "block";
          var rowId = id.substring(0, id.lastIndexOf('_'));
          var locationDetails;
          var locationMap;
          var location = "";
          var locationName = "";
          var direction = document.getElementById(rowId + '_ITddlDirection').value;
          
          if (fromORto == "F") {
              if (direction == "H")
                  location = "Home";
              else
                  location = "Airport";
                  
              locationDetails = document.getElementById(rowId + '_IThdfFromLocDetails').value;
              locationMap = document.getElementById(rowId + '_IThdfFromLocMap').value;
              locationName = document.getElementById(rowId + '_IThdfFromLocName').value;
             } else {

             if (direction == "H")
                 location = "Airport";
             else
                 location = "Home";
                 
                locationDetails = document.getElementById(rowId + '_IThdfToLocDetails').value;
                locationMap = document.getElementById(rowId + '_IThdfToLocMap').value;
                locationName = document.getElementById(rowId + '_IThdfToLocName').value;
            }
            getElement('lblLocDetailsValue').innerHTML = locationDetails;
            getElement('lblLocMapValue').innerHTML = locationMap;
            getElement('lblFromTo').innerHTML = location;
            getElement('lblLocationNameValue').innerHTML = locationName;
            //lblLocationNameValue
            //alert(getElement('lblLocDetailsValue').innerHTML) = locationDetails;
//          document.getElementById('lblLocDetailsValue').innerHTML = locationDetails;
//          document.getElementById('lblLocMapValue').innerHTML = locationMap;
//          document.getElementById('lblFromTo').innerHTML = location;
          
          return false;
      }

      function setDelete(id) {
          var rowId = id.substring(0, id.lastIndexOf('_'));


          document.getElementById('divLocation').style.display = "block";         
          return false;
      }
      function HideDiv(div) {
          if(div=='edit')
              document.getElementById('divEditLocation').style.display = "none";
          else
              document.getElementById('divLocation').style.display = "none";
          
          return false;
      }
      function ValidateEdit(id) {
          var rowId = id.substring(0, id.lastIndexOf('_'));
          if (document.getElementById(rowId + '_ITddlEmpName').selectedIndex <= 0) {
              alert('Please select Employee from the list!');
              return false;
          }
          else {
              //document.getElementById('divEditLocation').style.display = "block";
              getElement('hdfEditRowId').value = rowId;
              return true;
          }
      }
      function UpdateNewLocation() {
          //var rowId = id.substring(0, id.lastIndexOf('_'));
              var rowId = getElement('hdfEditRowId').value;
//          alert(rowId);
          var ddl = getElement('ddlNewLocation');
           var locName = ddl.options[ddl.selectedIndex].innerHTML;
           var locDtls = ddl.options[ddl.selectedIndex].value;
           var locId = locDtls.split("~");
           var locAddress = getElement('txtNewLocDetails').value;
           var fromOrTo = getElement('hdfFromOrTo').value;
           
               //fromOrTo ==F means changing ht efrom point
           if (fromOrTo == "F") {
               document.getElementById(rowId + '_IThdfFromLocId').value = locId[0];
               document.getElementById(rowId + '_IThdfFromLocDetails').value = locAddress;
               document.getElementById(rowId + '_IThdfFromLocName').value = locName;
               if (document.getElementById(rowId + '_ITddlDirection').value == "H")
                   locName = locName + "-Home";  // if direction is H to A then adding "Home" to the From location name
               else
                   locName = locName + "-Airport"; // if direction is A to H then adding "Airport" to the From location name         
                   
               document.getElementById(rowId + '_ITlblFrom').value = locName;
               
           }
           else {
               document.getElementById(rowId + '_IThdfToLocId').value = locId[0];
               document.getElementById(rowId + '_IThdfToLocDetails').value = locAddress;
               document.getElementById(rowId + '_IThdfToLocName').value = locName;
               if (document.getElementById(rowId + '_ITddlDirection').value == "H")
                   locName = locName + "-Airport";  // if direction is H to A then adding "Airport" to the To location name
               else
                   locName = locName + "-Home";     // if direction is A to H then adding "Home" to the To location name         
               document.getElementById(rowId + '_ITlblTo').value = locName;

           }
           document.getElementById('divEditLocation').style.display = 'none';
           getElement('txtNewLocDetails').value = "";
           return false;
                 }

         function SetNewLocDetails() {
            
              var ddl = getElement('ddlNewLocation');
              var locName = ddl.options[ddl.selectedIndex].innerHTML;
              var locId = ddl.options[ddl.selectedIndex].value;
              var locDetails = locId.split("~");
              getElement('txtNewLocDetails').value = locDetails[1];
              //alert(locName);
              return false;
          }
                
    function SwapLocations(id) {
        
            var rowId = id.substring(0, id.lastIndexOf('_'));
            var ddl = document.getElementById(rowId + '_ITddlDirection');
            var direction = ddl.options[ddl.selectedIndex].value;
        
            var tmpFromLoc = "";
            var tmpFromLocId = "";
            var tmpFromLocDetails = "";
            var tmpFromLocMap = "";
            var tmpLocName=""

            tmpFromLocId = document.getElementById(rowId + '_IThdfFromLocId').value;
            tmpFromLocDetails = document.getElementById(rowId + '_IThdfFromLocDetails').value;
            tmpFromLocMap =document.getElementById(rowId + '_IThdfFromLocMap').value;
            tmpFromLoc = document.getElementById(rowId + '_ITlblFrom').value;
            tmpLocName = document.getElementById(rowId + '_IThdfFromLocName').value;
            
            document.getElementById(rowId + '_IThdfFromLocId').value = document.getElementById(rowId + '_IThdfToLocId').value;
            document.getElementById(rowId + '_IThdfFromLocDetails').value = document.getElementById(rowId + '_IThdfToLocDetails').value ;
            document.getElementById(rowId + '_IThdfFromLocMap').value = document.getElementById(rowId + '_IThdfToLocMap').value;
            document.getElementById(rowId + '_ITlblFrom').value = document.getElementById(rowId + '_ITlblTo').value;
            document.getElementById(rowId + '_IThdfFromLocName').value =document.getElementById(rowId + '_IThdfToLocName').value;

            document.getElementById(rowId + '_IThdfToLocId').value = tmpFromLocId;
            document.getElementById(rowId + '_IThdfToLocDetails').value = tmpFromLocDetails;
            document.getElementById(rowId + '_IThdfToLocMap').value = tmpFromLocMap;
            document.getElementById(rowId + '_ITlblTo').value = tmpFromLoc;
            document.getElementById(rowId + '_IThdfToLocName').value= tmpLocName;
            if (direction == "H") {
                document.getElementById(rowId + '_IThdfFromLocType').value = "H";
                document.getElementById(rowId + '_IThdfToLocType').value = "A";
            }
            else {
                document.getElementById(rowId + '_IThdfFromLocType').value = "A";
                document.getElementById(rowId + '_IThdfToLocType').value = "H";

            }
            document.getElementById(rowId + '_ITdcSignIn_Time').value = "00:00";
            document.getElementById(rowId + '_ITdcSignOut_Time').value = "00:00";
            document.getElementById(rowId + '_ITtxtDutyHours').value = "";
            document.getElementById(rowId + '_ITdcDate_Date').value = "";
            document.getElementById(rowId + '_ITdcDate_Time').value = "";
            return false;
        }

        function OnTimeExit(dcId) {
           // alert(dcId);
            var rowId = dcId.substring(0, dcId.lastIndexOf('_'));
            var svcRowId = rowId.substring(0, rowId.lastIndexOf('_'));
            //var fromTime = getElement('dcFromDate_Time');
            var id = rowId.split("_");
            var inDate = "";
            var outDate = "";
            var difInTime="";
            var balTime="";
            var unit = "";
            var inDateInMIN = "";
            var outDateInMIN = "";
            if (id[4] == 'ITdcSignOut') {
                inDate = document.getElementById(svcRowId + '_ITdcSignIn_Time').value;
                inDateDetails = inDate.split(":");
                inDateInMIN = parseInt(inDateDetails[0] * 60) + parseInt(inDateDetails[1]);
                outDate = document.getElementById(svcRowId + '_ITdcSignOut_Time').value;
                outDateDetails = outDate.split(":");
                outDateInMIN = parseInt(outDateDetails[0] * 60) + parseInt(outDateDetails[1]);
                if (outDateInMIN < inDateInMIN) {
                    difInTime = outDateInMIN + (1440 - inDateInMIN) //1440=  24*60
                }
                else
                    difInTime = outDateInMIN - inDateInMIN;

                balTime = difInTime % 60; //to check hrs and mins are there
                if (balTime > 0) {

                    difInTime = difInTime - balTime;
                    unit = difInTime / 60;
                    unit = unit + ' hrs ' + balTime + ' mins';
                }
                else //only hrs
                {
                    unit = difInTime / 60;
                    unit = unit + ' hrs';
                }
                // alert(unit);
                document.getElementById(svcRowId + '_ITtxtDutyHours').value = unit;
                SetPickupTime(svcRowId, inDateInMIN, outDateInMIN,"Out");

            }
            else if (id[4] == 'ITdcSignIn') {
            inDate = document.getElementById(svcRowId + '_ITdcSignIn_Time').value;
            inDateDetails = inDate.split(":");
            inDateInMIN = parseInt(inDateDetails[0] * 60) + parseInt(inDateDetails[1]);
            outDate = document.getElementById(svcRowId + '_ITdcSignOut_Time').value;
            outDateDetails = outDate.split(":");
            outDateInMIN = parseInt(outDateDetails[0] * 60) + parseInt(outDateDetails[1]);
            SetPickupTime(svcRowId, inDateInMIN, outDateInMIN, "In");

            }

        }

        function SetPickupTime(rowId, InTime, OutTime,signOn) {//time with : {

            var direction = document.getElementById(rowId + '_ITddlDirection').value;  
            var timeSplitDtls = "";
            var pickupInMin = "";
            var pickupHR = "";
            var pickupMIN = "";
            var difValIn = parseInt(getElement('hdfHomeDriverTime').value);  // time to be added with actual sign IN to get the pick up time
            var difValOut = parseInt(getElement('hdfAirportDriverTime').value); //time to be added with actual sign OUT to get the pick up time
           // alert(difValIn);
           // alert(difValOut);
            var nextDay = "N";
            if (direction == "H" && signOn=="In"){

                  pickupInMin = InTime + difValIn;                  
                //1440 = 24 hrs means next day
                if (pickupInMin >= 1440) {
                    pickupInMin = pickupInMin-1440;
                    nextDay = "Y";
                }
                if (OutTime <= InTime) {

                    nextDay = "Y";
                }
                pickupMIN = pickupInMin % 60;
                pickupInMin = pickupInMin - pickupMIN;
                pickupHR = pickupInMin / 60;
                if (pickupHR < 10)
                    pickupHR = "0" + pickupHR;
                if (pickupMIN < 10)
                    pickupMIN = "0" + pickupMIN;


                var rmDate = GetDateTimeObject(rowId + '_ITdcDate');
                if (rmDate != null) {
                    var monthArr = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
                    var pickupDate = new Date(rmDate);
                    if (nextDay == "Y")
                        pickupDate.setDate(pickupDate.getDate() + 1);

                    //rmDate.value = pickupDate;
                    document.getElementById(rowId + '_ITdcDate_Date').value = pickupDate.getDate() + '-' + monthArr[(pickupDate.getMonth())] + '-' + pickupDate.getFullYear();
                }              
                document.getElementById(rowId + '_ITdcDate_Time').value = pickupHR + ":" + pickupMIN;

            }
            else if (direction == "A" && signOn == "Out") {
            //alert(signOn);
            //alert(OutTime);
            pickupInMin = OutTime + difValOut;
            //1440 = 24 hrs means next day
            if (pickupInMin >= 1440) {
                pickupInMin = pickupInMin - 1440;
                nextDay = "Y";
            }
            if (OutTime <= InTime) {

                nextDay = "Y";
            }

            pickupMIN = pickupInMin % 60;
            pickupInMin = pickupInMin - pickupMIN;
            pickupHR = pickupInMin / 60;
            if (pickupHR < 10)
                pickupHR = "0" + pickupHR;
            if (pickupMIN < 10)
                pickupMIN = "0" + pickupMIN;

            //if (pickupHR == 0) pickupHR = "00";

//                var rmDate = GetDateTimeObject(svcRowId + '_ITdcDate')
//                if (rmDate != null)
//                    SetPickupTime(svcRowId, inDateInMIN, outDateInMIN);


            var rmDate = GetDateTimeObject(rowId + '_ITdcDate');
            if (rmDate != null) {
                var monthArr = new Array("Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec");
                var pickupDate = new Date(rmDate);
                if (nextDay == "Y")
                    pickupDate.setDate(pickupDate.getDate() + 1);

                    //rmDate.value = pickupDate;
                    document.getElementById(rowId + '_ITdcDate_Date').value = pickupDate.getDate() + '-' + monthArr[(pickupDate.getMonth())] + '-' + pickupDate.getFullYear();
                }              
                document.getElementById(rowId + '_ITdcDate_Time').value = pickupHR + ":" + pickupMIN;
                //document.getElementById(rowId + '_ITdcDate_Time').value = document.getElementById(rowId + '_ITdcSignOut_Time').value;
            }
        
        
        }

        function ValidateLoad() {
            var fromDate = GetDateObject('ctl00_cphTransaction_dcFromDate');

            if (getElement('ddlStaff').selectedIndex <= 0) addMessage('Please select Staff from the list !', '');            
            var toDate = GetDateObject('ctl00_cphTransaction_dcToDate');
            if (fromDate == null) addMessage('Please select From Date !', '');
            if (toDate == null) addMessage('Please select To Date !', '');
            if (fromDate != null && toDate != null && (fromDate > toDate)) addMessage('To-Date should be later than or equal to From-Date !', '');            
                if (getMessage() != '') {
                    alert(getMessage()); clearMessage(); return false;
                }
            }


            function ValidateDetails() {

               
                var counts = getElement("gvROSDetails").rows.length - 1;

                var rows;
                var rowNo;           
                for (var i = 0; i < counts; i++) {

                    if (i + 2 < 10) {

                        rows = "0" + (i + 2);

                    }

                    else {

                        rows = i + 2;

                    }

                    rowNo = i + 1;
                    if (document.getElementById('ctl00_cphTransaction_gvROSDetails_ctl' + rows + '_ITddlEmpName') != null) {
                        if (document.getElementById('ctl00_cphTransaction_gvROSDetails_ctl' + rows + '_ITddlEmpName').selectedIndex <= 0) {

                            alert("Please Select the  Staff in the row " + " " + rowNo + " !");
                            return false;
                        }
                        else if (document.getElementById('ctl00_cphTransaction_gvROSDetails_ctl' + rows + '_ITtxtFlightNo').value == "") {

                            alert("Flight No cannot be blank in the row " + " " + rowNo + " !");
                            return false;
                        }
                        else if (document.getElementById('ctl00_cphTransaction_gvROSDetails_ctl' + rows + '_ITtxtFromSector').value == "") {

                            alert("From Sector cannot be blank in the row " + " " + rowNo + " !");
                            return false;
                        }
                        else if (document.getElementById('ctl00_cphTransaction_gvROSDetails_ctl' + rows + '_ITtxtToSector').value == "") {

                            alert("To Sector cannot be blank in the row " + " " + rowNo + " !");
                            return false;
                        }
                        else if (document.getElementById('ctl00_cphTransaction_gvROSDetails_ctl' + rows + '_ITdcSignIn_Time').value == "") {
                            alert("please enter Sign In Time in the row " + " " + rowNo + " !");
                            return false;

                        }
                        else if (document.getElementById('ctl00_cphTransaction_gvROSDetails_ctl' + rows + '_ITdcSignOut_Time').value == "") {
                            alert("please enter Sign Out Time in the row " + " " + rowNo + " !");
                            return false;

                        }
                        else if (document.getElementById('ctl00_cphTransaction_gvROSDetails_ctl' + rows + '_ITtxtDutyHours').value == "") {

                            alert("Please enter the duty hours in the row " + " " + rowNo + " !");
                            return false;
                        }
                   
                        var rmDate = GetDateTimeObject('ctl00_cphTransaction_gvROSDetails_ctl' + rows + '_ITdcDate');
                        var rmTime = document.getElementById('ctl00_cphTransaction_gvROSDetails_ctl' + rows + '_ITdcDate_Time');
                        
                        if (rmDate == null) {

                            alert("Please enter the Date in the row " + " " + rowNo + " !");
                            return false;
                        }
                        else if (rmTime.value == '' || rmTime.value=='00:00') {
                            alert("Please enter the Time in the row " + " " + rowNo + " !");
                            return false;
                        }
                    }
                }

                return true;

            }
            function Save() {
                
                if (!ValidateDetails()) return false;

            }
            
  </script>
</asp:Content>
