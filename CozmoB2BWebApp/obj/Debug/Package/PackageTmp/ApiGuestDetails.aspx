﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ApiGuestDetails.aspx.cs" Inherits="CozmoB2BWebApp.ApiGuestDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">



<%--    <link href="css/CTStyle.css" rel="stylesheet" type="text/css" />
    <link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />
    <link href="css/Default.css" rel="stylesheet" type="text/css" />
    <link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />
--%>
    <!-- Bootstrap Core CSS -->
<%--    <link href="css/bootstrap.min.css" rel="stylesheet" />

        <!-- manual css -->
        <link href="css/override.css" rel="stylesheet" />
        <link href="<%=Request.Url.Scheme%>://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
        <link href="build/css/main.min.css" rel="stylesheet" type="text/css" />


    <link rel="shortcut icon" href="images/FAV/favicon.ico" />
    <script type="text/javascript" src="Scripts/Menu/query2.js"></script>--%>

    <%-- <script src="scripts/select2.min.js" type="text/javascript"></script>--%>

    <!-- New BE js -->

    <!-- end new Js -->

    <!-- end -->

    <!-- jQuery -->
    <!-- Bootstrap Core JavaScript -->
<%--    <script src="Scripts/bootstrap.min.js"></script>--%>


<%--    <link href="css/toastr.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="build/js/main.min.js"></script>
    <link href="App_Themes/Default/BookingStyle.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/cozmovisa-style.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/main-style.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/style.css" type="text/css" rel="stylesheet" />--%>
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="scripts/jquery-ui.js"></script>    
    <script src="scripts/Common/FlexFields.js"></script>


    <style>
        .error {
            border: 1px solid #D8000C;
        }
        .custom-checkbox-style input[type=checkbox]+label:before{
                line-height: 1;
        }

        .form-control::-webkit-input-placeholder { color: lightgray; }
        .form-control::-moz-placeholder { color: lightgray; }
        .form-control:-ms-input-placeholder { color: lightgray; }

        .disabled { color: #ccc; pointer-events:none; }

        .Ruleshdr { 
            float:left;font-style:normal;	
            background:#18407B;color:#fff;padding:1px 2px 0 2px;
            margin-top:-1px;margin-left:0;margin-right:5px;border-radius:3px; 
        }

        .Rulesdtl { border:1px solid #18407B;margin-right:10px;padding:10px; }

    </style>

   <!--Search Passenger-->
   <script>

       /* Function to bind the search passenger filters html and to show the pop up */
       function ShowSearhPassenger(paxindx, pcid) {

           /* To disable bootstrap modal close on escape button and pop up outside click */

           $("#SearchPassenger").modal({
                backdrop: 'static',
                keyboard: false
           });

           /* If previously opened pop up and the current pop up button click belongs to the same pax, 
            * it will avoid binding the html once again and just open the existing pop up */
           if (cindex == paxindx && document.getElementById('txtSearchEmpId') != null)
               $('#SearchPassenger').modal('show');
           else {

               cindex = paxindx; cid = pcid;
               $('#SearchFilters').children().remove();
               $("#PassengerDetailsList").children().remove();
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchEmpId" class="form-control" placeholder="Please enter employee id" /></div>');
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchFirstName" class="form-control" placeholder="Please enter first name" /></div>');
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchLastName" class="form-control" placeholder="Please enter last name" /></div>');
               $("#SearchFilters").append('<div class="col-md-3"><input type="text" ID="txtSearchEmail" class="form-control" placeholder="Please enter email" /></div>');

               /* If the current pop up was clicked earlier and has any filter options, same filters will assign to the filter controls and, 
                * will perform the search for the same filters */

               if (SearchFilter[cid] != null && SearchFilter[cid] != '') {

                   var Filters = SearchFilter[cid].split('|');
                   document.getElementById('txtSearchEmpId').value = Filters[0];
                   document.getElementById('txtSearchFirstName').value = Filters[1];
                   document.getElementById('txtSearchLastName').value = Filters[2];
                   document.getElementById('txtSearchEmail').value = Filters[3];
                   SearchPassenger();
               }

               $('#SearchPassenger').modal('show');
           }
       }


       var SearchFilter = new Array(20);
       var selctedprofiles = new Array(20);
       var cindex; cid = 0;
       var PaxProfilesData;
       var PaxProfilesFlexData;
       var AirlineCode = '';

       /* Added .1 sec timer to show processing div on search passengers results loading */
       function SearchPassenger() {
           document.getElementById('ctl00_upProgress').style.display = 'block';
           setTimeout(function(){ SearchPassengers(); document.getElementById('ctl00_upProgress').style.display ='none'; }, 100);            
       }

       /* Function to search passengers list with the given filters in the pop up */
       function SearchPassengers() {

           $("#PassengerDetailsList").children().remove();
           var EmployeeId = document.getElementById('txtSearchEmpId').value, FstName = document.getElementById('txtSearchFirstName').value,
               LstName = document.getElementById('txtSearchLastName').value; Email = document.getElementById('txtSearchEmail').value;
           AirlineCode = '';

           if (EmployeeId == '' && FstName == '' && LstName == '' && Email == '') {

               SearchFilter[cid] = '';
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>Please enter any filter option with 3 characters</div>");
               return false;
           }

           if ((EmployeeId != '' && EmployeeId.length < 3) || (FstName != '' && FstName.length < 3) || (LstName != '' && LstName.length < 3) || (Email != '' && Email.length < 3)) {
               SearchFilter[cid] = '';
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>Please enter minimum 3 characters in the filter</div>");
               return false;
           }

           SearchFilter[cid] = EmployeeId + '|' + FstName + '|' + LstName + '|' + Email + '|' + AirlineCode;

           ShowPassengerDetail(AjaxCall('PassengerDetails.aspx/GetPaxProfiles', "{'sFilters':'" + SearchFilter[cid] + "'}"));           
       }

       /* Function to bind passenger search results list to grid */
       function ShowPassengerDetail(results) {

           if (results == '' || results == null || results.length == 0 || results == 'undefined') {
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>No records found</div>");
               return false;
           }
           else {
               PaxProfilesData = JSON.parse(results[0]);
               PaxProfilesFlexData = JSON.parse(results[1]);
           }

           $("#PassengerDetailsList").append('<table id="tblPaxDetails" class="table b2b-corp-table"><tr><th><span>Select</span></th><th><span>Employee Code</span></th><th><span>First Name</span></th><th><span>Last Name</span></th><th><span>Email</span></th><th><span>Passport No</span></th></tr></table>');
           var hasdata = false;

           $.each(PaxProfilesData, function (key, prfdata) {

               if (selctedprofiles.indexOf(prfdata.ProfileId) == -1) {
                   hasdata = true;
                   $("#tblPaxDetails").append('<tr>' +
                       
                       '<td> <span class="glyphicon glyphicon-plus-sign" id="prof_' + prfdata.ProfileId + '" onclick="SelectPaxData(this,' + prfdata.ProfileId + ')" /> </td>' +
                       '<td class="text-left"> <span>' + prfdata.EmployeeId + '</span></td> <td class="text-left"><span>' + prfdata.FName + '</span></td>' +
                       '<td class="text-left"> <span>' + prfdata.LName + '</span></td> <td class="text-left"><span>' + prfdata.Email + '</span></td>' +
                       '<td class="text-left"> <span>' + prfdata.PassportNo + '</span></td>' +
                       '</tr >');
               }
           });

           if (!hasdata) {
               $("#PassengerDetailsList").children().remove();
               $("#PassengerDetailsList").html("<div class='alert alert-danger text-left'>No records found</div>");
               return false;
           }

       }

       /* Function to assign flex field controls data for additional pax */
       function AssignFlexData1(cntrltype, cntrlid, paxid, PrfId, flexId) {

            var flxdata = '';
            $.each(PaxProfilesFlexData, function (sno, prfflxdata) {
                if (prfflxdata.ProfileId == PrfId && flexId == prfflxdata.FlexId)
                    flxdata = prfflxdata.FlexData;
            });                   

           if (flxdata != null && flxdata != 'undefined' && flxdata != '') {

                if ((cntrltype == "T" || cntrltype == "D") && document.getElementById('txt' + paxid + 'Flex' + cntrlid) != null)
                    document.getElementById('txt' + paxid + 'Flex' + cntrlid).value = flxdata;
                else if (cntrltype == "L" && document.getElementById('ddl' + paxid + 'Flex' + cntrlid) != null) {                
                    Setddlval('ddl' + paxid + 'Flex' + cntrlid, flxdata, '');
                }
           }
       }

       /* Function to assign the selected profile data to screen passenger details window */
       function SelectPaxData(event, PrfId) {

           try {
               
               var selpaxdtls = ''
               
               selctedprofiles[cid] = PrfId;

               $.each(PaxProfilesData, function (key, prfdata) {

                   if (prfdata.ProfileId == PrfId)
                       selpaxdtls = prfdata;
               });      

               document.getElementById('spnPaxAdd' + cindex).innerHTML = 'Update Passenger';

               if (cindex.substring(0, 1) == 'A') {

                   Setddlval('ddlAdultTitle-' + cindex.substring(1, cindex.length), selpaxdtls.Title, '');
                   Settextval('txtAdultFirstName-' + cindex.substring(1, cindex.length), selpaxdtls.FName);
                   Settextval('txtAdultLastName-' + cindex.substring(1, cindex.length), selpaxdtls.LName);
               }
               else {

                   Setddlval('ddlChildTitle-' + cindex.substring(1, cindex.length), selpaxdtls.Title, '')
                   Settextval('txtChildFirstName-' + cindex.substring(1, cindex.length), selpaxdtls.FName);
                   Settextval('txtChildLastName-' + cindex.substring(1, cindex.length), selpaxdtls.LName);
               }

               if (cindex == leadpax) {
                                      
                   document.getElementById('txtAdultEmail-01').value = selpaxdtls.Email;
                   document.getElementById('txtAdultPhone-01').value = selpaxdtls.Mobilephone;
                   document.getElementById('txtAdultAddress-01').value = selpaxdtls.Address1;
               }     

               var ancAPI = document.getElementById('ancdivFlex' + cindex);
               if (ancAPI != null && ancAPI.attributes["aria-expanded"].value == 'false')
                   ancAPI.click();

               if (!IsEmpty(document.getElementById('<%=hdnFlexInfo.ClientID%>').value))
                   SetPaxFlexData(cindex, PaxProfilesFlexData, PrfId);

               //if (FlexInfo != null && FlexInfo != 'undefined' && FlexInfo != '' && FlexInfo.length > 0) {

               //    var flexids = []; 

               //    $.each(FlexInfo, function (key, col) {
                                              
               //        flexids.push('|' + col.flexId + col.flexControl + '|');

               //        if (col.FLEXVALID != '' && (flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 || flexids.indexOf('|' + col.FLEXVALID + 'L' + '|') != -1)) {

               //             var dcntid = flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 ? 'txt' + cindex + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') :
               //                 'ddl' + cindex + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'L' + '|');
               //             var curflex = col.flexControl == 'L' ? 'ddl' : 'txt';
                        
               //             BindFlexdynamicddl(document.getElementById(dcntid).value, curflex + cindex + 'Flex' + key, col.flexSqlQuery, curflex); 
               //        }
                       
               //        AssignFlexData(col.flexControl, key, cindex, PrfId, col.flexId);
               //    });    
               //}    
           }
           catch (exception) {
               var exp = exception;
           }
           $('#SearchPassenger').modal('hide');
       }

        /* To set lead pax traveller info for corporate profile */
        function AssignCorpData() {
            
            /* Hide search and add pax options for corporate agents */
            document.getElementById('btnSearchPaxA01').style.display = document.getElementById('divPaxAddA01').style.display = 'none';

            var corpinfo = JSON.parse(document.getElementById('<%=hdnCorpProfInfo.ClientID %>').value);

            /* Assign the corporate profile values to controls when profile id > 0 */
            if (corpinfo != null && corpinfo.ProfileId > 0)
            {
                Setddlval('ddlAdultTitle-01', corpinfo.Title, '');
                Settextval('txtAdultFirstName-01', corpinfo.SurName);
                Settextval('txtAdultLastName-01', corpinfo.Name);
                Settextval('txtAdultPhone-01', corpinfo.Mobilephone);
                Settextval('txtAdultEmail-01', corpinfo.Email);
                Settextval('txtAdultAddress-01', corpinfo.Address1);
                selctedprofiles[0] = corpinfo.ProfileId;
            }

            /* Disable all lead pax fields for corporate except Travel Cordinator */
            if (LoginInfo.MemberType != 'TRAVELCORDINATOR')
                EnableDisableCntrls('Corpdisable', true, null, null);
        }

   </script>

    <%--<script type="text/javascript">

        /* To bind flex field drop down list dynamically based on dependent flex field selected value */
        function BindFlexdynamicddl(selval, ddlid, ddlquery, cntrl) {

            try {

                ddlquery = (ddlquery == null || ddlquery == 'undefined' || ddlquery == '') ? '' : ddlquery.replace(/@value/g, selval);
                if (cntrl == 'ddl' && ddlquery != '')
                    document.getElementById(ddlid).innerHTML = GetFlexddlData(ddlquery, cntrl);
                else
                    document.getElementById(ddlid).value = ddlquery != '' ? GetFlexddlData(ddlquery, cntrl) : selval;
                document.getElementById(ddlid.replace(cntrl, 'div')).style.display = (selval != '' && selval != '-1') ? 'block' : 'none';
                Setddlval(ddlid, '-1', '');
            }
            catch (exception) {
                var exp = exception;
            }
        }

        /* To get data for flex field drop down list based on flex query */
        function GetFlexddlData(ddlquery, cntrl) {

            try {

                ddlquery = ddlquery.replace(/'/g, '^');        
                var ddldata = (ddlquery == '' || ddlquery == 'undefined' || ddlquery == null) ? '' :
                    JSON.parse(AjaxCall('BookOffline.aspx/GetFlexddlData', "{'sddlQuery':'" + ddlquery + "'}"));
                if (ddldata != '') {

                    var col = [];

                    for (var key in ddldata[0]) {
                        col.push(key);
                    }

                    var ddlhtml = '';

                    if (cntrl == 'ddl') {
                        
                        for (var d = 0; d < ddldata.length; d++) {

                            ddlhtml += '<option value="' + ddldata[d][col[0]] + '">' + ddldata[d][col[1]] + '</option>';
                        }
                    }
                    return cntrl == 'txt' ? ddldata[0][col[0]] : '<option selected="selected" value="">--Select--</option>' + ddlhtml;
                }
                else
                    return cntrl == 'txt' ? '' : '<option selected="selected" value="">--Select--</option>';
            }
            catch (exception) {
                return cntrl == 'txt' ? '' : '<option selected="selected" value="">--Select--</option>';
            }
        }

        function BindFlexFields() {

            try {
                
                FlexInfo = JSON.parse(document.getElementById('<%=hdnFlexInfo.ClientID %>').value);
                Flexconfigs = document.getElementById('<%=hdnFlexConfig.ClientID %>').value == '' ? '' :
                    document.getElementById('<%=hdnFlexConfig.ClientID %>').value.split('|');

                for (var i = 0; i < (IsCorp ? 1 : Result.RoomGuest.length); i++) {

                    var NoofAdult = IsCorp ? 1 : Result.RoomGuest[i].noOfAdults;
                    var noofChlid = IsCorp ? 0 : Result.RoomGuest[i].noOfChild;

                    for (var j = 1; j <= NoofAdult; j++) {

                        GetFlexHTML('divFlexA' + i + j, 'A' + i + j);            
                        document.getElementById('divFlexA' + i + j).style.display = 'block';  
                    }

                    for (var k = 1; k <= noofChlid; k++) {

                        GetFlexHTML('divFlexC' + i + k, 'C' + i + k);               
                        document.getElementById('divFlexC' + i + k).style.display = 'block';  
                    }
                }
                    
                $('select').select2();
            }
            catch (exception) {
                var exp = exception;
            }
        }

        /* To Bind Flex fields */
        function GetFlexHTML(divid, paxid) {
            
            if (FlexInfo != null) {

                var flexids = [];
                                
                $.each(FlexInfo, function (key, col) {

                    flexids.push('|' + col.flexId + col.flexControl + '|');

                    //if (col.FlexProductId != '2')
                    //    return; // Currently Loading Flight Flex Details

                    if (Flexconfigs.length > 0) {

                        var IsBind = false; IslableFound = false;
                        for (var r = 0; r < Flexconfigs.length; r++) {

                            if (!IslableFound)
                                IslableFound =  col.flexLabel.toUpperCase() == Flexconfigs[r].split(',')[1].toUpperCase();
                            if (request.Corptravelreason == Flexconfigs[r].split(',')[0] && col.flexLabel.toUpperCase() == Flexconfigs[r].split(',')[1].toUpperCase())
                                IsBind = true;
                        }
                        if (IslableFound && !IsBind) { return; }
                    }
                    
                    var dataevnt = col.flexDataType == 'T' ? '' :
                        col.flexDataType == 'N' ? 'return IsNumeric(event)' : 'return IsAlphaNum(event)';
                    var dateval = col.flexControl == 'D' ? 'ValidateDate(this)' : '';
                    var cntrlhtml = '';
                    if (col.flexControl == 'T' || col.flexControl == 'D')
                        cntrlhtml = AddTextbox(col.flexLabel, paxid + 'Flex' + key, (paxid == leadpax ? col.Detail_FlexData : ''), (col.flexMandatoryStatus == 'Y'), (col.flexControl == 'T' ? dataevnt : ''), '50', (col.flexControl == 'D' ? dateval : ''), col.flexControl == 'D', col.flexDisable);
                    if (col.flexControl == 'L')
                        cntrlhtml = AddFlexddl(col.flexLabel, paxid + 'Flex' + key, '', (col.flexMandatoryStatus == 'Y'), (col.flexSqlQuery.indexOf('@value') != '-1' ? '' : col.flexSqlQuery), paxid, col.flexDisable);

                    var isDependent = false; var dcntid = '';

                    if (col.FLEXVALID != '' && (flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 || flexids.indexOf('|' + col.FLEXVALID + 'L' + '|') != -1)) {

                        dcntid = flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 ? 'txt' + paxid + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') :
                            'ddl' + paxid + 'Flex' + flexids.indexOf('|' + col.FLEXVALID + 'L' + '|');

                        if (document.getElementById(dcntid) == null)
                            return;

                        var curflex = col.flexControl == 'L' ? 'ddl' : 'txt';
                        var ocflex = flexids.indexOf('|' + col.FLEXVALID + 'T' + '|') != -1 ? 'txt' : 'ddl';

                        isDependent = document.getElementById(dcntid).value == '' || document.getElementById(dcntid).value == '-1';             
                        
                        $('#' + dcntid).on('change', function (e) { BindFlexdynamicddl((ocflex == 'txt' ? e.target.value : e.val), curflex + paxid + 'Flex' + key, col.flexSqlQuery, curflex); });
                    }

                    isDependent = paxid == leadpax ? (isDependent && (col.Detail_FlexData == null || col.Detail_FlexData == 'undefined' || col.Detail_FlexData == '')) : isDependent;
                    
                    cntrlhtml = '<div ' + (isDependent ? 'style="display:none"' : '') + ' class="form-group col-md-4 ' + (col.flexDisable ? 'disabled' : '') + '" '
                        + 'id="div' + paxid + 'Flex' + key + '">' + cntrlhtml + '</div>';

                    if (key == 10) {                            

                        $('#' + divid).append('<div class="collapse" id="' + divid + 'MoreFlex"><div id="' + divid + 'More" class="row padding-0 marbot_10 ">' +
                            cntrlhtml + '</div>');
                        $('#' + divid).append(
                            '<a style="color: rgb(0, 0, 0); margin-left: 0px; padding: 0px;" id="anc' + divid + '" ' +
                            'aria-expanded="false" aria-controls="collapseExample" class="advPax-collapse-btn-new float-left"' +
                            'role="button" data-toggle="collapse" href="#' + divid + 'MoreFlex"></a>');
                    }
                    else if (key > 10)
                        $('#' + divid + 'More').append(cntrlhtml);
                    else
                        $('#' + divid).append(cntrlhtml);      

                    if (paxid == leadpax && col.flexControl != 'T') {

                        if (!IsEmpty(dcntid)) 
                            BindFlexdynamicddl(document.getElementById(dcntid).value, 'ddl' + paxid + 'Flex' + key, col.flexSqlQuery, 'ddl');
                        if (!IsEmpty(col.Detail_FlexData) && col.flexControl == 'L' && document.getElementById('ddl' + paxid + 'Flex' + key) != null)
                            Setdropval('ddl' + paxid + 'Flex' + key, col.Detail_FlexData, '');
                    }
                                    
                });                            
            }
        }

        /* To add mandatory aestrick span */
        function AddMandatory(isreq) {
            return isreq ? '<span class="red_span">*</span>' : '';
        }

        /* To add dynamic text box field */
        function AddTextbox(lable, id, val, Isreq, onkeypress, maxlen, onchange, IsDate, IsDisable) {        

            return AddMandatory(Isreq) + '<label><strong>' + lable + ': </strong></label> <input id="txt' + id + '" type="text" onkeypress="' + onkeypress +
                '" placeholder= "Enter ' + lable + '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control ' + (Isreq ? 'FlexReq ' : '') + (IsDate ? 'EnableCal' : '') +
                '" maxlength="' + maxlen + '" ' +
                (onchange != null && onchange != 'undefined' && onchange != '' ? 'onchange="' + onchange + '" ' : '') +
                (val != null && val != 'undefined' && val != '' ? 'value="' + val + '" ' : '') + '>';
        }

        /* To Bind Flex field drop down list based on flex query */
        function AddFlexddl(lable, id, val, Isreq, ddlquery, paxid, IsDisable) {
            
            return AddMandatory(Isreq) + '<label><strong>' + lable + ': </strong></label> <select id="ddl' + id + '" ' + (IsDisable ? 'disabled' : '') + ' class="form-control ' + (Isreq ? 'FlexReq' : '') + '">' +
                (ddlquery == '' ? '<option selected="selected" value="">--Select--</option>' : (paxid == leadpax ? GetFlexddlData(ddlquery, 'ddl')
                : document.getElementById('ddl' + leadpax + id.substring(3, id.length)).innerHTML)) +
                '</select>';
        }

        var FlexInfo = ''; leadpax = 'A01'; Flexconfigs = '';
        var IsCorp = false;

        /* To delete error class for all controls */
        function DelErrorClass() {

            var errmsgs = $('.error');
            if (errmsgs != null && errmsgs.length > 0) {

                $.each(errmsgs, function (key, cntrl) {

                    $('#' + cntrl.id).removeClass('error');
                });
            }
        }

        /* To save flex data */
        function SaveFlexInfo(p) {

            try {
                
                if (document.getElementById('<%=hdnFlexInfo.ClientID %>').value == '')
                    return [];

                var flexcnt = [];                             

                $.each(FlexInfo, function (key, col) {      
                    
                    var flexdat = '';                    
                                                
                    if ((col.flexControl == 'T' || col.flexControl == 'D') && document.getElementById('txt' + p + 'Flex' + key) != null)
                        flexdat = document.getElementById('txt' + p + 'Flex' + key).value;
                    else if (col.flexControl == 'L' && document.getElementById('ddl' + p + 'Flex' + key) != null &&
                        document.getElementById('ddl' + p + 'Flex' + key).value != '' &&
                        document.getElementById('ddl' + p + 'Flex' + key).value != '-1') {
                        flexdat = document.getElementById('ddl' + p + 'Flex' + key).value;
                    } 

                    flexcnt.push({
                        FlexId: col.flexId, FlexLabel: col.flexLabel, FlexGDSprefix: col.flexGDSprefix,
                        FlexData: (flexdat != null && flexdat != '') ? flexdat : '', ProductID: '2'
                    });
                }); 
                                
                return flexcnt;
            }
            catch (exception) {
                return [];
            }
        }

        function ValidateFlex() {

            var fields = $('.FlexReq');
            var Count = 0;

            for (var i = 0; i < fields.length; i++) {
                
                if (fields[i].nodeName.toUpperCase() != "DIV" && $(fields[i]).val().trim() == '') {
                                        
                    $(fields[i]).addClass("error");
                    Count++;                 
                }
            }
            return Count == 0;
        }

        function ValidateDate(event) {
                        
            if (event.value == '')
                return;

            var date = event.value.split('/');

            if (!(date.length == 3 && date[0] < 32 && date[1] < 13 && date[2] < 2100 && date[2] > 1947)) {
                                
                ShowError('Please enter valid date');
                document.getElementById(event.id).focus();
                event.value = '';
            }                
        }

    </script>--%>

    <script>
        var request;//request object from CS
        var Result;//Results from CS
        var SelectedRoom;
        var SelectedroomType;
        var RoomType = [];
        var product; var LoginInfo; var bookingResult;
        var rows;

        $(document).ready(function () {//Page Load

            /* To set corporate flag*/
            IsCorp = document.getElementById('<%=hdnCorpProfInfo.ClientID %>').value != '';

            request = JSON.parse(document.getElementById('<%=hdnObjRequest.ClientID %>').value);            
            Result = JSON.parse(document.getElementById('<%=hdnHotelDetails.ClientID %>').value);
            SelectedroomType = JSON.parse(document.getElementById('<%=hdnSelectedRoomTypes.ClientID %>').value);
            LoginInfo = JSON.parse(document.getElementById('<%=hdnLoginInfo.ClientID %>').value);

            //Agent Balance for Pay By Label 
            $('#agentBalance').children().remove(); 
            $('#agentBalance').text("");  
            var bal = $("#ctl00_lblOBAgentBalance").text();
            bal= bal.match(/(\d+)/g); 
            if (bal != "" && bal!=null)  {                
                $('#agentBalance').append('<em class="currency">' + Result.Currency + ' </em>' + ' ' +bal);
            }
            else {
                $('#agentBalance').append('<em class="currency">' + Result.Currency + ' </em>' + ' ' + $('#ctl00_lblAgentBalance').text());
            }
        
            LoadRoomSummary(SelectedroomType);//Display Hotel Details
            rows = tblContent(SelectedroomType);
            $('#Price-roomsummary').append(rows);

            $(".PDrequired").blur(function () {

                tmpval = $(this).val().trim();
                if (tmpval == '') {
                    $(this).addClass('error'); $(this).val('');
                } else {
                    $(this).removeClass('error');
                    if (this.id == 'txtAdultEmail-01') {
                         $('#txtAdultEmail-01').next('span').remove();
                    }
   
                }
            });

            $('input[type=radio][id=rbnAccount]').change(function () {

                if ($("input:radio[id='rbnAccount']").is(":checked")) {
                    $("#divCard").hide();
                } 
            });

            $('input[type=radio][id=rbncard]').change(function () {

                if ($("input:radio[id='rbncard']").is(":checked")) {
                    $("#divCard").show();
                } 
            });

                        if (SelectedroomType != '') { //lert(SelectedroomType[0].RoomTypeCode.split("||")[3]);
                            if (SelectedroomType[0].RoomTypeCode.split("||")[3] == 'EPS') {
                    $('#divEPSHotels').show();                                      
                }
            }
            
            /* To bind flex fields on page load */
            if (document.getElementById('<%=hdnFlexInfo.ClientID %>').value != '')
                BindRoomPaxFlex(document.getElementById('<%=hdnFlexInfo.ClientID %>').value, document.getElementById('<%=hdnFlexConfig.ClientID %>').value,
                    'divFlex', Result, request.Corptravelreason, IsCorp);

            /* To bind corporate info for lead pax */
            if (document.getElementById('<%=hdnCorpProfInfo.ClientID %>').value != '') 
                AssignCorpData();
            else {

                /* To bind Agent info */
                Settextval('txtAdultPhone-01', LoginInfo.phoneno);
                Settextval('txtAdultEmail-01', LoginInfo.email);
            }

            /* To enable error indicator for flex mandatory fields */
            $(".FlexReq").blur(function () {

                if ($(this).val() == '') 
                    $(this).addClass('error');
                else
                    $(this).removeClass('error');   
            });
        });

        function select(id) {
             tmpval = $('#'+id).val();
                if (tmpval == '') {
                    $('#'+id).addClass('error');
                } else {
                    $('#'+id).removeClass('error');
                }
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }
        // Validating the characters.
        function isAlphaNumeric(evt) {
            evt = (evt) ? evt : event;
            var charCode = (evt.charCode) ? evt.charCode : ((evt.keyCode) ? evt.keyCode :
                ((evt.which) ? evt.which : 0));
            if (charCode == 32)
                return true;
            if ((charCode < 65 || charCode > 90) && (charCode < 97 || charCode > 122)) {
                return false;

            }
            return true;
        }
        //Get Payment Preferences
        function GetPaymentPreference() {
            
            var paymentPrefParams=JSON.parse(document.getElementById('<%=hdnPaymentPrefParams.ClientID %>').value);
            var Data = { SessionId: LoginInfo.SessionId, PaymentPrefParams: paymentPrefParams };
             var apiUrl = DynamicAPIURL();
            $.ajax({
                url: apiUrl+'/api/HBooking/GetPaymentPreference',
                type: 'POST',
                ContentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: Data,

                //jData,
                success: function (data) {

                },
                error: function (xhr, textStatus, errorThrown) {
                    LogError(errorThrown, 'Hotel Booking Error(GetPaymentPreference)');
                    var msg = "Booking may or may not be happened please contact with Admin(GetPaymentPreference)";
                    window.location.href = "ErrorPage.aspx?Err=" + msg;
                }
            });
           
        }

        //Display Hotel Details
        function LoadRoomSummary(SelectedroomType) {
            $('#list-group-roomsummary').children().remove();
            var templateRoomSummary = $('#templateRoomSummary').html();
            $('#startDate').text(new Date(Result.StartDate).toDateString());
            $('#endDate').text(new Date(Result.EndDate).toDateString());
            $('#h4HotelId').text(Result.HotelName);
            $('#HotelAddress').text(Result.HotelAddress);

            var rating;
            if (Result.Rating == 1) {
                rating = 'one-star-hotel';
            }
            else if (Result.Rating == 2) {
                rating = 'two-star-hotel';
            }
            else if (Result.Rating == 3) {
                rating = 'three-star-hotel';
            }
            else if (Result.Rating == 4) {
                rating = 'four-star-hotel';
            }
            else if (Result.Rating == 5) {
                rating = 'five-star-hotel';
            }
            $('#Rating').addClass(rating);
            if (SelectedroomType != null) {

                for (var m = 0; m < SelectedroomType.length; m++) {
                    $('#list-group-roomsummary').append('<li id="List' + m + '" class="item">' + templateRoomSummary + '</li>');
                    $('#h5RoomName').attr('id', 'h5RoomName-' + m);
                    $('#h5RoomName-' + m).text(SelectedroomType[m].RoomTypeName);
                    $('#mealPlanDesc').attr('id', 'mealPlanDesc-' + m);
                    $('#mealPlanDesc-' + m).text(SelectedroomType[m].mealPlanDesc);
                    $('#roomGuestsCount').attr('id', 'roomGuestsCount-' + m);
                    $('#roomGuestsCount-' + m).text(Result.RoomGuest[m].noOfAdults + " Adult(s) & " + Result.RoomGuest[m].noOfChild + " Child(s)");
                     //Display ChildAge 
                   $('#List' + m).find('#ChildAgeInfo').attr('id', 'ChildAgeInfo-' + m);
                    if (Result.RoomGuest[m].noOfChild != 0) {
                        $('#ChildAgeInfo-' + m).show();
                        $('#List' + m).find('#ChildAgeSummary').attr('id', 'ChildAgeSummary-' + m);
                        $('#ChildAgeSummary-' + m).text(Result.RoomGuest[m].childAge + "Year(s)");
                    }
                    else {
                        $('#ChildAgeInfo-' + m).hide();
                    }
                    if (m == 0 && SelectedroomType[m].TaxBreakups != null) {
                        var region = AjaxCall('ApiRoomDetails.aspx/GetRegion', "{'CountryName':'" + request.CountryName + "'}");
                        BindTaxBreakUps(SelectedroomType[m].TaxBreakups, Result.Currency, region);
                    }
                }
            }
            loadControls();
        }
       
        function BindTaxBreakUps(TaxBreakups, Currency, region) {
            var decimalvalue = LoginInfo.Decimal;
            $('#list-group-taxsummary').children().remove();
              $('#list-group-taxsummary-Excl').children().remove();
            var templateTaxSummary = $('#templateTaxSummary').html();
            var templateTaxSummaryExcl = $('#templateTaxSummaryExcl').html();
            //  var groupTaxes = [];
            var TaxBreakups = JSON.parse(AjaxCall('ApiRoomDetails.aspx/GetGroupTaxes', "{'taxLists':'" + JSON.stringify(TaxBreakups) + "'}"));
            $('#list-group-taxsummary').append('<li class="item mb-2"><u><strong>Tax Details</strong></u></li>');
            $('#list-group-taxsummary-Excl').append('<li class="item mb-2"><u><strong>Due @ Hotel (Pay @ Hotel) </strong></u></li>');
            var totIncl = 0;
           $('#totPayatHotel').hide();
           $('#taxsummaryListWrapper').hide();
            $('#list-group-taxsummary').hide();
            for (var k = 0; k < TaxBreakups.length; k++) {
                var breakups = TaxBreakups[k];
                if (breakups.IsIncluded == true) {
                     $('#list-group-taxsummary').show();
                    $('#list-group-taxsummary').append('<li id="ListTax' + k + '" class="item mb-1">' + templateTaxSummary + '</li>');
                    $('#spanTaxBreakup').attr('id', 'spanTaxBreakup-' + k);
                    //$('#spanTaxBreakup-' + k).html("<span class='flex-fill'>" + ((breakups.TaxTitle == "tax_and_service_fee") && region == 'EU' ? "Tax Recover Charges" : (breakups.TaxTitle == "tax_and_service_fee" ? "Tax Recovery Charges & ServiceFee" : breakups.TaxTitle.replace(/_/g, " "))) + "-" + breakups.UnitType + "-Incl :</span><span><strong>" + Currency + " " + breakups.Value.toFixed(decimalvalue) + "</strong> <br>/ " + breakups.FrequencyType + "</span>");
                    $('#spanTaxBreakup-' + k).html("<span class='flex-fill'>" + ((breakups.TaxTitle == "tax_and_service_fee") && region == 'EU' ? "Tax Recover Charges" : (breakups.TaxTitle == "tax_and_service_fee" ? "Tax Recovery Charges & ServiceFee" : breakups.TaxTitle.replace(/_/g, " "))) + "-" + breakups.UnitType + "-Incl :</span><span><strong>" + Currency + " " + breakups.Value.toFixed(decimalvalue) + "</strong> </span>");
                } else {
                    $('#totPayatHotel').show();
                    $('#taxsummaryListWrapper').show();
                    // Excluded Tax
                    $('#list-group-taxsummary-Excl').append('<li id="ListTax' + k + '" class="item mb-1">' + templateTaxSummaryExcl + '</li>');
                    $('#spanTaxBreakupExcl').attr('id', 'spanTaxBreakupExcl-' + k);
                    //$('#spanTaxBreakupExcl-' + k).html("<span class='flex-fill'>" + ((breakups.TaxTitle == "tax_and_service_fee") && region == 'EU' ? "Tax Recover Charges" : (breakups.TaxTitle == "tax_and_service_fee" ? "Tax Recovery Charges & ServiceFee" : breakups.TaxTitle.replace(/_/g, " "))) + "-" + breakups.UnitType + "-Excl (Pay @ Hotel) :</span><span>" + " <strong>" + Currency + " " + breakups.Value.toFixed(decimalvalue) + "</strong> <br>/ " + breakups.FrequencyType + "</span>");
                    $('#spanTaxBreakupExcl-' + k).html("<span class='flex-fill'>" + ((breakups.TaxTitle == "tax_and_service_fee") && region == 'EU' ? "Tax Recover Charges" : (breakups.TaxTitle == "tax_and_service_fee" ? "Tax Recovery Charges & ServiceFee" : breakups.TaxTitle.replace(/_/g, " "))) + "-" + breakups.UnitType + "-Excl (Pay @ Hotel) :</span><span>" + " <strong>" + Currency + " " + breakups.Value.toFixed(decimalvalue) + "</strong> </span>");
                     totIncl += parseFloat(breakups.Value);
                }
            }
            $('#totPayatHotel').html("<div class='d-flex mt-2'><span class='flex-fill'>Total (pay @ Hotel) :</span><span><strong>" + Currency + " " + totIncl.toFixed(decimalvalue) + "</strong></span></div>");
            
       }

        function tblContent(SelectedroomType) {
           
            var lblGSTVAT = request.LoginCountryCode == "IN" ? "Total GST " : "VAT ";
            var VAT  = 0;
            var objItinerary = JSON.parse(document.getElementById('<%=hdnItinerary.ClientID %>').value);
            for (var i = 0; i < objItinerary.Roomtype.length; i++) {
                VAT += objItinerary.Roomtype[i].Price.OutputVATAmount;
            }
             
            var decimalvalue = LoginInfo.Decimal;
            if (SelectedroomType != null) {
                var tr;
                var total = 0;
                tr += "<tr><th scope='row' class='font-weight-bold'>ROOM TYPE</th><td class='font-weight-bold'>TOTAL (in " + Result.Currency + ")</td></tr>"

                for (var m = 0; m < SelectedroomType.length; m++) {
                    tr = tr + "<tr><th scope='row'  class='RoomType' id=FareRoomType-" + m + ">" + SelectedroomType[m].RoomTypeName + "</th > <td id=roomPrice" + m + " >" + (SelectedroomType[m].TotalPrice - SelectedroomType[m].Discount).toFixed(decimalvalue) + "</td > </tr >"
                    total = (eval(total)+eval(SelectedroomType[m].TotalPrice - SelectedroomType[m].Discount)).toFixed(decimalvalue);
                }
                total = parseFloat(total) + parseFloat(VAT);
                tr = tr + "<tr><th scope='row'>"+lblGSTVAT +"</th><td id='VAT' >" + (VAT).toFixed(decimalvalue) + "</td></tr>"
                tr = tr + "<tr><th scope='row' >TOTAL</th><td id='totalRoomPrice'>" + (total).toFixed(decimalvalue) + "</td></tr>"
                //tr = tr + "<tr><th scope='row'>TOTAL INCL MARKUP</th><td id='totalwithMarkUp'></td></tr>"
                
                tr = tr + "<tr><th scope='row' class='font-weight-bold grand-total'>GRAND TOTAL</th><td class='font-weight-bold' id='grandTotal'>" + Math.ceil(total).toFixed(decimalvalue) + "</td></tr>"
                rows = tr;

                //to show for Amount to be booked label
                $('#amountToBeBook').children().remove(); 
                $('#amountToBeBook').text("");  
                $('#amountToBeBook').append('<em class="currency">' + Result.Currency + ' </em>' + ' ' +  Math.ceil(total).toFixed(decimalvalue) );

                return rows;
            }
        }



        //Creating Dynamic Controls based on Noof Room ,Noof adults,Childs
        function loadControls() {

            var GuestAdult = $('#Adult').html();
            var GuestChild = $('#Child').html();
            var Count = 1; var ChildCount = 0; var TotalPax = 1;

            /* To hide remarks div for UAPI hotels, B2b and B2B2b agents */
            if (Result.BookingSource == '23' || LoginInfo.AgentType == 'B2B' || LoginInfo.AgentType == 'B2B2B') 
                $('#divRemarks').attr('style', 'display:none');

            /* To avoid additional pax controls binding for UAPI hotels */
            if (Result.BookingSource == '23')
                return;

            for (var i = 0; i < Result.RoomGuest.length; i++) {
                var NoofAdult = Result.RoomGuest[i].noOfAdults;
                var noofChlid = Result.RoomGuest[i].noOfChild;

                for (var j = 1; j <= NoofAdult; j++) {
                    if (i == 0 && j == 1) {
                    }
                    else {
                        $('#ListGuest').append('<li id="ListAdult' + i + j + '" class="mb-1">' + GuestAdult + '</li>');
                        Count++;

                        var Title = ' <label>Title</label><select class="form-control custom-select PDrequired" id="ddlAdultTitle-' + i + j +
                            '" onchange="select(this.id);"><option value="">Select Title</option> <option value="Mr">Mr</option> <option value="Mrs">Mrs</option><option value="Ms">Ms</option><option value="Mst">Mst</option></select>';

                        $('#RoomCount').attr('id', 'RoomCount-' + i + j);
                        $('#RoomCount-' + i + j).text('Room-' + (i + 1));
                        $('#h4Adult').attr('id', 'h4Adult-' + i + j);
                        $('#h4Adult-' + i + j).text('Guest Adult-' + Count);
                        $('#divAdultTitle').attr('id', 'divAdultTitle-' + i + j);
                        $('#divAdultTitle-' + i + j).append(Title);

                        //$('#ddlAdultTitle').attr('id', 'ddlAdultTitle-' + i + j);
                        //$('#ddlAdultTitle-' + i + j).addClass("PDrequired");

                        $('#s2id_ddlAdultTitle').attr('id', 's2id_ddlAdultTitle-' + i + j);
                        $('#txtAdultFirstName').attr('id', 'txtAdultFirstName-' + i + j);
                        $('#txtAdultFirstName-' + i + j).addClass("PDrequired");
                        $('#txtAdultLastName').attr('id', 'txtAdultLastName-' + i + j);
                        $('#txtAdultLastName-' + i + j).addClass("PDrequired");

                        $('#divFlexA').attr('id', 'divFlexA' + i + j);                        
                        $('#btnSearchPaxA').attr('id', 'btnSearchPaxA' + i + j);                        
                        $('#btnSearchPaxA' + i + j).attr('onclick', 'ShowSearhPassenger("A' + i + j + '", ' + TotalPax + ')');
                        $('#divSearchPaxA').attr('id', 'divSearchPaxA' + i + j);                       
                        $('#divSearchPaxA' + i + j).attr('style', 'display:' + (IsCorp ? 'none' : 'block') + ';');
                        $('#spnPaxAddA').attr('id', 'spnPaxAddA' + i + j);    
                        $('#chkPaxAddA').attr('id', 'chkPaxAddA' + i + j);    
                        $('#divPaxAddA').attr('id', 'divPaxAddA' + i + j);    
                        $('#divPaxAddA' + i + j).attr('style', 'display:' + (IsCorp ? 'none' : 'block') + ';');
                        TotalPax++;
                    }
                }
                for (var k = 1; k <= noofChlid; k++) {
                    $('#ListGuest').append('<li id="ListChild' + i + k + '" class="mb-1">' + GuestChild + '</li>');
                    ChildCount++;
                                            var ChildTitle = ' <label>Title</label><select class="form-control custom-select PDrequired" id="ddlChildTitle-' + i + k + '" onchange="select(this.id);"><option value="">Select Title</option><option value="Master">Master</option> <option value="Mr">Mr</option> <option value="Mrs">Mrs</option></select>';

                    $('#h4Child').attr('id', 'h4Child-' + i + k);
                    $('#h4Child-' + i + k).text('Guest Child-' + ChildCount);
                     $('#divchild').attr('id', 'divchild-' + i + k);
                        $('#divchild-' + i + k).append(ChildTitle);
                    //$('#ddlChildTitle').attr('id', 'ddlChildTitle-' + i + k);
                    //$('#ddlChildTitle-' + i + k).addClass("PDrequired");
                    $('#txtChildFirstName').attr('id', 'txtChildFirstName-' + i + k);
                    $('#txtChildFirstName-' + i + k).addClass("PDrequired");
                    $('#txtChildLastName').attr('id', 'txtChildLastName-' + i + k);
                    $('#txtChildLastName-' + i + k).addClass("PDrequired");
                    $('#divFlexC').attr('id', 'divFlexC' + i + k);
                    $('#btnSearchPaxC').attr('id', 'btnSearchPaxC' + i + k);                        
                    $('#btnSearchPaxC' + i + k).attr('onclick', 'ShowSearhPassenger("C' + i + k + '", ' + TotalPax + ')');
                    $('#divSearchPaxC').attr('id', 'divSearchPaxC' + i + j);                       
                    $('#divSearchPaxC' + i + j).attr('style', 'display:' + (IsCorp ? 'none' : 'block') + ';');
                    $('#spnPaxAddC').attr('id', 'spnPaxAddC' + i + k);    
                    $('#chkPaxAddC').attr('id', 'chkPaxAddC' + i + k);     
                    $('#divPaxAddC').attr('id', 'divPaxAddC' + i + k);    
                    document.getElementById('divPaxAddC' + i + k).style.display = IsCorp == true ? 'none' : 'block';
                    TotalPax++;
                }
            }
            $('select').select2();
        }

        //Booking Confirmation
        function ConfirmBooking() {

            DelErrorClass();

		  $("#ctl00_upProgress").show();
            var fields = $('.PDrequired');
            var Count = 0;
            for (var i = 0; i < fields.length; i++) {
                //console.log(fields[i].nodeName);
                if (fields[i].nodeName != "DIV") {
                    var textLength = $(fields[i]).val().trim();
                    if ($(fields[i]).val().trim() == '' || textLength.length < 2 ) {
                        $(fields[i]).addClass("error");
                        Count++;
                    }
                }


            }

            if ($('#txtAdultEmail-01').val() != "") {
                 var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;  
                if (!emailReg.test($('#txtAdultEmail-01').val())) {
                    $('#txtAdultEmail-01').addClass("error");
                    //$('#txtAdultEmail-01').after('<span style="color:red;">Enter valid Email</span>');
                    $("#ctl00_upProgress").hide();
                    return false;
                } else {
                    $('#txtAdultEmail-01').removeClass("error");

                }
            }

            if (Count == 0 && !ValRoomPaxFlex(Result, IsCorp))
                Count++;

            if (Count != 0) {   
                 $("#ctl00_upProgress").hide();
                return false;
            }
            else {

                //$("#ctl00_upProgress").hide();
                var confirm = $("#ChkConfirm").is(":checked");
                var isTNCconfirm = $("#ChkTNCConfirm").is(":visible");
                var tnconfirm = $("#ChkTNCConfirm").is(":checked");
                var isRoomconfirm = $("#ChkEPSHotelConfirm").is(":visible");
                var Roomconfirm = $("#ChkEPSHotelConfirm").is(":checked");

                if (confirm && tnconfirm && (isRoomconfirm && Roomconfirm)) {
                //if (confirm && (isTNCconfirm && tnconfirm) && (isRoomconfirm && Roomconfirm)) {
                    $('#ChkConfirmDiv').hide();
                    LoadPassengerDetails();
                    Createitinerary();
                    $('.chk_room_msg').hide();
                    $('.chk_confirm_msg').hide();
                    $('.chk_addfee_msg').hide();
                }
                else if (confirm && tnconfirm && !isRoomconfirm) {
                //else if (confirm && isTNCconfirm && !isRoomconfirm) {
                    $('#ChkConfirmDiv').hide();
                    LoadPassengerDetails();
                    Createitinerary();
                    $('.chk_confirm_msg').hide();
                    $('.chk_addfee_msg').hide();
                }
                else {
                    $("#ctl00_upProgress").hide();
                    $('.chk_err_msg').hide();
                    $('#ChkConfirmDiv').show();
                    confirm ? $('.chk_confirm_msg').hide() : $('.chk_confirm_msg').show();
                    tnconfirm ? $('.chk_addfee_msg').hide() : $('.chk_addfee_msg').show();
                    (isRoomconfirm && !Roomconfirm) ? $('.chk_room_msg').show() : $('.chk_room_msg').hide();
                    return false;
                }
                // $("#ctl00_upProgress").hide();
            }
        }
        //Assign all Passenger to Array of Object
        function LoadPassengerDetails() {
            var Hotel = []; var LeadPassenger = {}; var totalpax = 0;
            for (var i = 0; i < Result.RoomGuest.length; i++) {
                Hotel = [];
                var NoofAdult = Result.RoomGuest[i].noOfAdults;
                var noofChlid = Result.RoomGuest[i].noOfChild;
                for (var j = 1; j <= NoofAdult; j++) {
                    var Leadpass;
                    if (i == 0 && j == 1) { Leadpass = true } else { Leadpass = false; }
                    if (!Leadpass && Result.BookingSource == '23') {
                        Hotel.push(LeadPassenger);
                        continue; 
                    }

                    var sCorpPrfId = !IsEmpty(request.Corptraveler) ? request.Corptraveler.split('~')[0] : '';
                    if (!IsEmpty(sCorpPrfId))
                        sCorpPrfId = sCorpPrfId < 0 ? '-1-C' : sCorpPrfId > 0 ? sCorpPrfId : '';
                    if (IsEmpty(sCorpPrfId))                        
                        sCorpPrfId = document.getElementById('chkPaxAdd' + 'A' + i + j).checked ?
                            ((selctedprofiles[totalpax] != null && selctedprofiles[totalpax] != '') ? selctedprofiles[totalpax] + '-U' : '-1') : sCorpPrfId;

                    Hotel.push({
                        PaxId: 0,
                        HotelId: 0,
                        Title: $('#ddlAdultTitle-' + i + j).val(),
                        Firstname: $('#txtAdultFirstName-' + i + j).val(),
                        Middlename: "",
                        Lastname: $('#txtAdultLastName-' + i + j).val(),
                        Phoneno: $('#txtAdultPhone-' + i + j).val(),
                        Countrycode: request.CountryCode,
                        NationalityCode: request.CountryName,
                        Areacode: "",
                        email: $('#txtAdultEmail-' + i + j).val(),
                        Addressline1: $('#txtAdultAddress-' + i + j).val(),
                        Addressline2: "",
                        city: "",//$('#txtAdultCity-' + i + j).val(),
                        Zipcode: "",
                        State: "",
                        Country: request.PassengerCountryOfResidence,
                        Nationality: request.PassengerNationality,
                        PaxType: "Adult",
                        RoomId: 0,
                        LeadPassenger: Leadpass,
                        Age: 0,
                        Employee: "",
                        Division: "",
                        Department: "",
                        Purpose: "",
                        EmployeeID: "",
                        IsGST: false,
                        HotelPassengerGST: {},
                        FlexDetailsList: (document.getElementById('<%=hdnFlexInfo.ClientID %>').value == '' || (IsCorp && !Leadpass)) ? [] : GetPaxFlexInfo('A' + i + j, false, '2'),
                        CorpProfileId: sCorpPrfId
                    });

                    if (Leadpass) 
                        LeadPassenger = Hotel[0];
                    totalpax++;
                }
                for (var k = 1; k <= noofChlid; k++) {
				 var AgeArr = Result.RoomGuest[i].childAge;
                    var Age = AgeArr[k - 1];
                    Hotel.push({
                        PaxId: 0,
                        HotelId: 0,
                        Title: $('#ddlChildTitle-' + i + k).val(),
                        Firstname: $('#txtChildFirstName-' + i + k).val(),
                        Middlename: "",
                        Lastname: $('#txtChildLastName-' + i + k).val(),
                        Phoneno: "",
                        Countrycode: request.CountryCode,
                        NationalityCode:  request.CountryName,
                        Areacode: "",
                        Email: "",
                        Addressline1: "",
                        Addressline2: "",
                        City: "",
                        Zipcode: "",
                        State: "",
                        Country: request.PassengerCountryOfResidence,
                        Nationality: request.PassengerNationality,
                        PaxType: "Child",
                        RoomId: 0,
                        LeadPassenger: false,
                        Age: Age,
                        Employee: "",
                        Division: "",
                        Department: "",
                        Purpose: "",
                        EmployeeID: "",
                        IsGST: false,
                        HotelPassengerGST: {},
                        FlexDetailsList: (document.getElementById('<%=hdnFlexInfo.ClientID %>').value == '' || (IsCorp && !Leadpass)) ? [] : GetPaxFlexInfo('C' + i + k, false, '2'),
                        CorpProfileId: document.getElementById('chkPaxAdd' + 'C' + i + k).checked ?
                            ((selctedprofiles[totalpax] != null && selctedprofiles[totalpax] != '') ? selctedprofiles[totalpax] + '-U' : '-1') : ''
                    });
                    totalpax++;
                }
                RoomType.push({ PassenegerInfo: Hotel });
            }
        }
        //Creating Itinerary in WebMethod
        function Createitinerary() {
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            var roominfo = JSON.stringify(RoomType);
            var addlmarkup = document.getElementById('<%=hdnAsvAmount.ClientID %>').value;
            var remarks = $('#txtRemarks').val();
            var specialRequest = $('#txtSpecialRequest').val();
            $.ajax({
                type: "POST",
                url: "ApiGuestDetails.aspx/Createitinerary",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: "{'data':'" + roominfo + "','addlMarkup':'" + addlmarkup + "','remarks':'" + remarks + "','specialRequest':'" + specialRequest + "' }",
                success: function (data) {
                    if (data == null) {
                        $("#ctl00_upProgress").hide();
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Dear Agent something went wrong, Please try again.";
                    }
                    else {
                        product = JSON.parse(data.d);
                        if (product == "In Sufficient Balance") {
                            $("#ctl00_upProgress").hide();
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Dear Agent you do not have sufficient balance to book a room for this hotel.";
                        }
                        else {
                            document.getElementById('errMess').style.display = "none";
                            document.getElementById('errorMessage').innerHTML = "";
                            BookingHotel();
                        }
                    }

                },
                error: function (d) {
                    $("#ctl00_upProgress").hide();
                }
            });
        }
        function DynamicAPIURL() {
                var url;
                var Secure ='<%=Request.IsSecureConnection%>';
            if (Secure=='True') {
                url = "<%=Request.Url.Scheme%>://" +'<%=Request.Url.Host%>'+"/HotelWebApi";
            }
            else {
                 // url = "<%=Request.Url.Scheme%>://" +'<%=Request.Url.Host%>'+"/HotelWebApi";
                 url = "https://" +'<%=Request.Url.Host%>'+"/HotelWebApi";
            }
            return url;
        }
        function BookingHotel() {
            var Data = { Product: product, AgentId: LoginInfo.AgentId, UserId: LoginInfo.UserId, SessionId: LoginInfo.SessionId, behalfLocation: LoginInfo.OnBehalfAgentLocation };
             var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';
            if (apiUrl == null || apiUrl == '') {
                    apiUrl = DynamicAPIURL();<%--'<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';--%>
                }
            $.ajax({
                url: apiUrl+'/api/HBooking/BookHotel',
                type: 'POST',
                ContentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: Data,
                 beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', "Bearer " + JSON.parse(document.getElementById('<%=hdnTokenId.ClientID %>').value));
                    },
                //jData,
                success: function (data) {
                    var response = JSON.parse(data);
                    //bookingResult = response.bookingResponse;
                    //product = response.itinerary;
                    //BookingConfirm();
                    if (!IsEmpty(response.bookingResponse.ConfirmationNo) && response.bookingResponse.Status != '2') {
                        //window.location.href = (IsCorp ? 'CorpHotelSummary' : 'APIHotelPaymentVoucher') + '.aspx?ConfNo=' + response.bookingResponse.ConfirmationNo;
                        var key = (IsCorp ? 'CorpHotelSummary' : 'APIHotelPaymentVoucher');
                        AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'" + key + "', 'sessionData':'" + response.bookingResponse.ConfirmationNo + "', 'action':'set'}");
                        window.location.href = key + '.aspx';
                    }
                    else if (response.bookingResponse.Status == '13')// Duplicate Booking
                        alert(response.bookingResponse.Error);
                    else
                        window.location.href = "ErrorPage.aspx?Err=" + response.bookingResponse.Error;
                    $("#ctl00_upProgress").hide();
                },
                error: function (xhr, textStatus, errorThrown) {
                    LogError(errorThrown, 'Hotel Booking Web API Error(BookHotel)');
                    var msg = "Booking may or may not be happened please contact with Admin(BookingHotel)";
                    window.location.href = "ErrorPage.aspx?Err=" + msg;
                }
            });
        }
        function BookingConfirm() {
            var bookingResponse = JSON.stringify(bookingResult);
            var itinerary = JSON.stringify(product);
            $.ajax({
                url: "ApiGuestDetails.aspx/CreateInvoice",
                type: "POST",
                async: false,
                dataType: "json",
                contentType: 'application/json; charset=utf-8',
                data: "{'bookingResponse':'" + bookingResponse + "','hotelItinerary':'" + itinerary + "'}",
                success: function (data) {
                    console.log(data.d);
                    var error = data.d;
                    if (error.includes("error")) {
                        var Error = error.split("||");
                        LogError(Error[1], 'Hotel Booking Error(CreateInvoice)');
                        window.location.href = "ErrorPage.aspx?Err=" + Error[1];
                    }
                    else {
                        var response = JSON.parse(data.d);
                        //window.location.href = (IsCorp ? 'CorpHotelSummary' : 'APIHotelPaymentVoucher') + '.aspx?SE=Y&ConfNo=' + response;
			var key = (IsCorp ? 'CorpHotelSummary' : 'APIHotelPaymentVoucher');
                        AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'" + key + "', 'sessionData':'" + ('Y|' + response) + "', 'action':'set'}");
                        window.location.href = key + '.aspx';
                    }

                },
                error: function (xhr, textStatus, errorThrown) {
                    LogError(errorThrown, 'Hotel Booking Error(CreateInvoice)');
                    var msg = "Booking may or may not be happened please contact with Admin (BookingConfirm)";
                    window.location.href = "ErrorPage.aspx?Err=" + msg;
                }
            })
        }
        function CalculateAddlMarkUp() {
             var decimalvalue = LoginInfo.Decimal;
            if (SelectedroomType != null && $('#addlMarkUp').val() != "") {
                if ($('#addlMarkUp').hasClass("error")) {
                    $('#addlMarkUp').removeClass("error");
                }
                 var VAT  = 0;
                 var objItinerary = JSON.parse(document.getElementById('<%=hdnItinerary.ClientID %>').value);
                 for (var i = 0; i < objItinerary.Roomtype.length; i++) {
                    VAT += objItinerary.Roomtype[i].Price.OutputVATAmount;
                 }
                var total = 0;
                for (var m = 0; m < SelectedroomType.length; m++) {
                    total += SelectedroomType[m].TotalPrice - SelectedroomType[m].Discount;
                }
                var type = $('#ddlAddlType').val();
                var markupvalue = $('#addlMarkUp').val() * 1;
                var addlMarkup;
                if (type == "P") {
                    addlMarkup = eval(total) * eval(markupvalue) / 100;
                }
                else {
                    addlMarkup = markupvalue;
                }
                total = eval(total) +eval(VAT) + eval(addlMarkup)
                document.getElementById('<%=hdnAsvAmount.ClientID %>').value = addlMarkup;
                $('#totalRoomPrice').text(total.toFixed(decimalvalue));
                $('#grandTotal').text(Math.ceil(total).toFixed(decimalvalue));
                //to show for Amount to be booked label
                $('#amountToBeBook').children().remove(); 
                $('#amountToBeBook').text("");  
                $('#amountToBeBook').append('<em class="currency">' + Result.Currency + ' </em>' + ' ' + Math.ceil(total).toFixed(decimalvalue) );

                $('#addlMarkUp').val("");
                 $('#ddlAddlType').select2('val', "P");
                
            }
            else {
                $('#addlMarkUp').addClass("error");
            }
        }

        /* To display hotel rules and cancel policy */
        function ShowRules() {

            if (document.getElementById('divRules') != null) {

                $("#CancelPolicyModal").modal();
                return;
            }

            try {

                var RulesInfo = JSON.parse(document.getElementById('<%=hdnSelectedRoomTypes.ClientID %>').value);
                var HotelInfo = JSON.parse(document.getElementById('<%=hdnHotelDetails.ClientID %>').value);
                var objItinerary = JSON.parse(document.getElementById('<%=hdnItinerary.ClientID %>').value);
                var cancellationPolicy= objItinerary.HotelCancelPolicy.split('|').join("<br/>")
                $("#headRoomTypeName").text(RulesInfo[0].RoomTypeName);
                $('#h2CnlPolicy').children().remove();
                $('#h2CnlPolicy').text("");

                $('#h2CnlPolicy').append('<div id="divRules" class="padding-10 refine-result-bg-color"></div>');
                var Ruleshtml = ''; var hdnIsAgent = LoginInfo.AgentType == 'B2B' || LoginInfo.AgentType == 'B2B2B';

                if (Result.BookingSource == '23') {
                
                    var canhtml = '<p class="Ruleshdr">Cancellation policy</p><br />' + '<p class="Rulesdtl">' + cancellationPolicy + '</p><br />';
                    var Otherrules = RulesInfo[0].EssentialInformation.split(',');

                    var Restrict = [];
                    Restrict.push('Rate description'); Restrict.push('Rate amount'); Restrict.push('Room rate'); Restrict.push('Total Amount');

                    $.each(Otherrules, function (key, col) {

                        var Policy = col.trim().split('|');
                        if (Policy.length > 1 && !IsEmpty(Policy[0]) && !IsEmpty(Policy[1]))                                                
                            Ruleshtml += Restrict.indexOf(Policy[0]) > -1 && !hdnIsAgent ? '' : '<p class="Ruleshdr">' + Policy[0] + '</p><br />' + '<p class="Rulesdtl">' + Policy[1] + '</p><br />';
                    });         
                    Ruleshtml = canhtml + Ruleshtml;
                }
                else if (Result.BookingSource == '22') {//Temporary fix to display based on supplier, will be corrected this at supplier API level aling with room details changes.
                    for (var i = 0; i < RulesInfo.length; i++)
                    {
                        cancellationPolicy = cancellationPolicy.replace('^Date and time is calculated based on local time of destination','');
                    }
                    cancellationPolicy = cancellationPolicy+"<br>Date and time is calculated based on local time of destination"
                    Ruleshtml = '<p class="Ruleshdr">Cancellation policy</p><br />' + '<p class="Rulesdtl">' + cancellationPolicy + '</p>';
                    if (!IsEmpty(RulesInfo[0].EssentialInformation)) {
                        //Ruleshtml += '<p class="Ruleshdr">Important Information</p><br />' + '<p class="Rulesdtl">' + RulesInfo[0].EssentialInformation + '</p>';                        
                        Ruleshtml += '<p class="Ruleshdr">Important Information</p><br />' + '<div class="Rulesdtl">' + RulesInfo[0].EssentialInformation.split('^')[0] + '</div>'; 
                    Ruleshtml += '<p class="Ruleshdr">Disclaimer</p><br />' + '<div class="Rulesdtl">';
                    if (!IsEmpty(RulesInfo[0].EssentialInformation.split('^')[1])) {
                      Ruleshtml +=   RulesInfo[0].EssentialInformation.split('^')[1].replace('*', "<br>*") + '<br/><br/>';
                    }
                        Ruleshtml += '<label class="bold"> Bed type availabilities depends @ Check-In time </label></div>';
                        }
                }
                else if (Result.BookingSource == '25') {
                    Ruleshtml = '<p class="Ruleshdr">Cancellation policy</p><br />' + '<p class="Rulesdtl">' + cancellationPolicy + '</p>';
                    if (!IsEmpty(HotelInfo.HotelPolicyDetails)) {
                        Ruleshtml += '<p class="Ruleshdr">Important Information</p><br />' + '<div class="Rulesdtl">' + HotelInfo.HotelPolicyDetails.replace('|',"<br>") + '</div>';
                    }
                }
                else {

                    Ruleshtml = '<p class="Ruleshdr">Cancellation policy</p><br />' + '<p class="Rulesdtl">' + cancellationPolicy + '</p>';
                    if (!IsEmpty(RulesInfo[0].EssentialInformation)) {                                         
                        Ruleshtml += '<p class="Ruleshdr">Important Information</p><br />' + '<div class="Rulesdtl">' + RulesInfo[0].EssentialInformation + '</div>';
                     }
                    }
                        $('#divRules').append(Ruleshtml);
            }
            catch (exception) {

            }            
            $("#CancelPolicyModal").modal();
        }

        /* To display Terms and Contions */
        function ShowConditions() {

            if (document.getElementById('divTNC') != null) {

                $("#TNCModal").modal();
                return;
            }

            try {

                <%--var RulesInfo = JSON.parse(document.getElementById('<%=hdnSelectedRoomTypes.ClientID %>').value);
                $("#headRoomTypeName").text(RulesInfo[0].RoomTypeName);--%>
                $('#h2TNC').children().remove();
                $('#h2TNC').text("");

                $('#h2TNC').append('<div id="divTNC" class="padding-10 refine-result-bg-color"></div>');
                var TNChtml = ''; /*var hdnIsAgent = LoginInfo.AgentType == 'B2B' || LoginInfo.AgentType == 'B2B2B';*/

                   // TNChtml = '<p class="Ruleshdr">Terms & Condition</p><br />' + '<p class="Rulesdtl">This is property collect inventory. Choices between multiple bed types for the same room are requests only and may not be honored at the hotel if availability does not permit.. Checkin begins: 2:00 PM. Checkin ends: 6:00 AM. Minimal age: 18. Instructions: Extraperson charges may apply and vary depending on property policy.</p>';
                TNChtml = '<p class="Ruleshdr">Terms & Condition</p><br />' + '<ul class="Rulesdtl mb-3">' +//This is property collect inventory. Choices between multiple bed types for the same room are requests only and may not be honored at the hotel if availability does not permit.. Checkin begins: 2:00 PM. Checkin ends: 6:00 AM. Minimal age: 18. Instructions: Extraperson charges may apply and vary depending on property policy.</p>';
                    '<li class="d-flex mb-1"> <span class="icon icon-arrow_drop_right"></span><div>All rooms are guaranteed on the day of arrival. In the case of no-show, your room(s) will be released and you will subject to the terms and condition of the Cancellation/No-show policy specified at the time you made the booking as well as noted in the confirmation Email </div> </li>' +
                    '<li class="d-flex mb-1"><span class="icon icon-arrow_drop_right"></span><div>The total price for these booking fees not include mini-bar items, telephone bills, laundry service, etc. The hotel will bill you directly. </div></li>' +
                    '<li class="d-flex mb-1"><span class="icon icon-arrow_drop_right"></span><div>In case where breakfast is included with the room rate, please note that certain hotels may charge extra for children travelling with their parents. If applicable, the hotel will bill you directly. Upon arrival, if you have any questions, please verify with the hotel.</div> </li>' +
                    ' <li class="d-flex mb-1"><span class="icon icon-arrow_drop_right"></span><div>Any complaints related to the respective hotel services,with regards to location, rooms, food, cleaning or other services, the guest will have to directly deal with the hotel. Cozmo Holidays will not be responsible for uch complaints.</div> </li>' +
                    ' <li class="d-flex mb-1"><span class="icon icon-arrow_drop_right"></span><div>The General Hotel Policy: Check-in time at 1400hrs and check-out time 1200hrs Early check-in or late check-out is subject to availability at the time of check-in/check-out at the hotel and cannot be guaranteed at any given point in time</div> </li>' +
                    ' <li class="d-flex mb-1"><span class="icon icon-arrow_drop_right"></span><div>Interconnecting/ Adjoining rooms/any special requests are always subject to availability at the time of check-in, and Cozmo Holidays will not be responsible for any denial of such rooms to the Customer.</div> </li>' +
                    ' <li class="d-flex mb-1"><span class="icon icon-arrow_drop_right"></span><div>Most of the hotels will be asking for credit card or cash amount to be paid upon check-in as guaranteed against any expected extras by the guest, Cozmo Holidays will not be responsible in case the guest doesn’t carry a credit card or enough cash money for the same, and the guest has to follow up directly with the hotel for the refund upon check out, Cozmo holidays is not responsible in case of any delay from central bank for credit card refunds. </div></li></ul>';
                    //if (Result.BookingSource == '22') 
//if (Result.BookingSource == '22') 
                if(SelectedroomType[0].RoomTypeCode.split("||")[3]=='EPS')
                        TNChtml += '<p class="Ruleshdr">Supplier Terms & conditions</p><br />' + '<p class="Rulesdtl"><input id="lnkBookingTNC" type="button" class="btn-link" value="https://developer.expediapartnersolutions.com/terms/en/" onclick="ViewBookingConditions(this.value)" \></p>'+ '<p class="Rulesdtl"><input id="lnkBookTNC" type="button" class="btn-link" value="https://developer.expediapartnersolutions.com/terms/agent/en/" onclick="ViewBookingConditions(this.value)" \></p>';                        
                
                $('#divTNC').append(TNChtml);
            }
            catch (exception) {

            }            
            $("#TNCModal").modal();
        }

        /* Url Redirect */
        function ViewBookingConditions(url) {
            
            var wnd = window.open(url, '_blank', 'width=800,height=500,scroll=yes,resizable=no,menubar=no,status=no');
        }

    </script>

     <input type="hidden" id="hdnItinerary" runat="server" />
    <input type="hidden" id="hdnObjRequest" runat="server" />
    <input type="hidden" id="hdnSelectedRoomTypes" runat="server" />
    <input type="hidden" id="hdnHotelDetails" runat="server" />
    <input type="hidden" id="hdnLoginInfo" runat="server" />
    <input type="hidden" id="hdnAsvAmount" runat="server" value="0" />
    <input type="hidden" id="hdnPaymentPrefParams" runat="server" />
    <input type="hidden" id="hdnFlexInfo" runat="server" />
    <input type="hidden" id="hdnFlexConfig" runat="server" />
    <input type="hidden" id="hdnCorpProfInfo" runat="server" />
    <input type="hidden" id="hdnTokenId" runat="server" />
    <div>
        <div class="htl-apiguest-page ui-listing-page">

            <div class="ui-pax-details-page pt-3">
                <div id="errMess" class="error_module" style="display: none;">
                    <div id="errorMessage" class="alert-messages alert-caution">
                    </div>
                </div>
                <div class="row custom-gutter">
                    <div class="col-md-8">
                        <h3 class="pt-0 pb-3">Guest Details</h3>
                        <div class="ui-form-pax-details mb-3" id="GuestDetails">
                            <!--Room 1 Starts-->
                            <div class="ui-bg-wrapper mb-1  ui-room-wrapper" id="room-1">
                                <div class="row custom-gutter">
                                    <div class="col-md-3">
                                        <span class="font-weight-bold label-title">ADULT 1  (LEAD GUEST) </span>
                                    </div>
                                    <div class="col-md-6">
                                        <button type="button" class="btn btn-link float-right" id="btnSearchPaxA01" onclick="ShowSearhPassenger('A01', 0)">Search Passenger
                                            <span class="glyphicon glyphicon-search"></span></button>
                                    </div>
                                    <div class="col-md-3">
                                        <span class="float-md-right room-type-title " id="Room1Name">Room 1  </span>
                                    </div>
                                </div>
                                <div class="row custom-gutter">
                                    <div id="divPaxAddA01" class="col-md-12">
                                        <span id="spnPaxAddA01" style="float:right">Add Passenger</span><input type="checkbox" id="chkPaxAddA01" style="float:right"/>
                                    </div>
                                </div>
                                <div class="row custom-gutter Corpdisable">
                                    <div class="form-group col-md-3">
                                        <label>Title</label>
                                        <select class="form-control custom-select PDrequired" id="ddlAdultTitle-01" onchange="select(this.id);">
                                            <option value="">Select Title</option>
                                            <option value="Mr">Mr</option>
                                            <option value="Mrs">Mrs</option>
                                            <option value="Ms">Ms</option>
                                            <option value="Miss">Miss</option>
                                            <option value="Mst">Mst</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>First Name</label>
                                        <input type="text" class="form-control PDrequired" placeholder="Enter First Name" id="txtAdultFirstName-01" onkeypress="return isAlphaNumeric(event);">
                                    </div>
                                    <div class="form-group col-md-3">
                                        <label>Last Name</label>
                                        <input type="text" class="form-control PDrequired" placeholder="Enter Last Name" id="txtAdultLastName-01" onkeypress="return isAlphaNumeric(event);">
                                    </div>
                                    <%-- <div class="form-group col-md-3">
                                            <label>Nationality</label>
                                           <input type="text" class="form-control" disabled="disabled" id="txtAdultNationality-01"  />
                                        </div>--%>
                                </div>
                                <div class="row custom-gutter Corpdisable">
                                    <div class="form-group col-md-3" style="display: none">
                                        <label>City</label>
                                        <input type="text" class="form-control" placeholder="Enter City" id="txtAdultCity-01">
                                    </div>
                                    <div class="form-group col-md-3" style="display: none">
                                        <label>Pin Code</label>
                                        <input type="text" class="form-control" placeholder="Enter Pin Code" id="txtAdultPinCode-01" onkeypress="return isNumber(event);">
                                    </div>
                                    <div class="form-group col-md-6">
                                        <label>Address</label>
                                        <textarea class="form-control" rows="2" placeholder="Enter Address" id="txtAdultAddress-01"></textarea>
                                    </div>

                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <hr class="dotted" />
                                    </div>
                                </div>
                            </div>
                            <!--END:Room 1 -->

                            <div class="ui-bg-wrapper">

                                    <div class="row custom-gutter Corpdisable">
                                        <div class="form-group col-md-6">
                                            <label>Email</label>
                                            <input type="text" class="form-control PDrequired" placeholder="Enter E-mail" id="txtAdultEmail-01">
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Phone No</label>
                                            <input type="text" class="form-control PDrequired" placeholder="Enter Phone Number" id="txtAdultPhone-01" onkeypress="return isNumber(event);" maxlength="15">
                                        </div>
                                    </div>
                                    <div class="row custom-gutter baggage-wrapper" style="display:none" id="divFlexA01"></div>
                                </div>
                                <ul class="ui-bg-wrapper mb-1  ui-room-wrapper" id="ListGuest"></ul>

                            <div class="ui-bg-wrapper mb-1" id="divRemarks">

                                    <div class="row custom-gutter">
                                        <div class="form-group col-md-6">
                                            <label>Internal Reference</label>
                                            <textarea class="form-control"   placeholder="Enter Remarks" id="txtRemarks"></textarea>
                                        </div>
                                        <div class="form-group col-md-6">
                                            <label>Special Request</label>
                                            <textarea class="form-control"   placeholder="Enter Special Request" id="txtSpecialRequest"></textarea>
                                          <label  style="text-transform: none;"><b>Please note additional requests are not guranteed you may contact the property/service provider directly to confirm/enquire</b></label><br />

                                        </div>
                                    </div>
                                </div>

                                <div class="ui-bg-wrapper mb-1" style="min-height: 267px;">
                                    <div class="row ui-htldtl-pmt-option">
                                        <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate != "Y")
                                            { %>
                                        <div class="col-md-7">
                                            <div class="row">
                                                <div class="col-md-4">
                                                    <span class="heading-normal text-gray-light mb-3 d-block">Pay by </span>
                                                </div>
                                                <div class="col-md-8">
                                                    <div class="custom-radio-lg mb-3">
                                                        <input type="radio" name="customRadio" id="rbnAccount" checked="checked">
                                                        <label></label>
                                                        <div class="text">Account <span class="ac-balance primary-color" id="agentBalance"></span></div>
                                                    </div>
                                                    <div class="custom-radio-lg" style="display:none;">
                                                        <input type="radio" name="customRadio" id="rbncard">
                                                        <label></label>
                                                        <span class="text">Credit / Debit Card</span>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="row">
                                                <div class="col-md-5">
                                                </div>
                                                <div class="col-md-7">
                                                    <div id="divATB" class="text ml-1"><b>Amount to be Booked  </b><span class="ac-balance primary-color" id="amountToBeBook"></span></div>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="col-md-5 pb-2" id="divCard" style="display:none;">
                                            <div class="row payment-icons-manual">
                                                <div class="col-md-12">
                                                    <div class="input-group py-2">
                                                        <span class="input-group-addon pr-5"><span class="icon-credit-card-alt"></span></span>
                                                        <input type="text" class="form-control" placeholder="Card Number">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                   <div class="input-group py-2">
                                                        <span class="input-group-addon pr-5"><span class="icon-calendar"></span></span>
                                                        <input type="text" class="form-control" placeholder="MM/YY">
                                                    </div>
                                                </div>
                                                <div class="col-md-12">
                                                   <div class="input-group py-2">
                                                        <span class="input-group-addon pr-5">

                                                        <span class="icon icon-check-circle"></span>

                                                    </span>
                                                    <input type="text" class="form-control" placeholder="CVV">
                                                </div>
                                            </div>
                                            <div class="col-md-12">
                                                <div class="input-group py-2">
                                                    <span class="input-group-addon pr-5"><span class="icon-user"></span></span>
                                                    <input type="text" class="form-control" placeholder="Cardholder Name">
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                        <%}%>
                                   
                                        <div class="col-md-12 mt-4">
                                            <div class="alert alert-danger small p-3" role="alert" id="ChkConfirmDiv" style="display:none">                                                
                                                <div class="chk_confirm_msg chk_err_msg py-1 font-weight-bold">Please check the Cancellation Policy & Hotel Policy to continue</div>
                                                <div class="chk_addfee_msg chk_err_msg py-1 font-weight-bold">Please check Terms & Conditions </div>
                                                <div class="chk_room_msg chk_err_msg py-1 font-weight-bold">Please check This Room includes other product.</div>
                                            </div>

                                         
                                                <div>
                                                    <input type="checkbox" id="ChkConfirm" />                                                
                                                    <a id="ancTAC" onclick="ShowRules();"  href="javascript:void(0);">I accept Cancellation Policy & Hotel Policy </a>
                                                </div> 
                                            <%--<div class="alert alert-danger small p-3" role="alert" id="ChkTNCConfirmDiv" style="display:none">                                                
                                                <div class="chk_confirm_msg chk_err_msg py-1 font-weight-bold">Please check the Terms & Conditions checkbox to continue</div>
                                            </div>--%>
                                            <div id="divTNCEPS">
                                                    <input type="checkbox" id="ChkTNCConfirm"/>                                                
                                                    <a id="Termscondition" onclick="ShowConditions();" href="javascript:void(0);">I accept Terms & Conditions</a>
                                                </div> 
                                            <div id="divEPSHotels" style="display:none">
                                                    <input type="checkbox" id="ChkEPSHotelConfirm" />                                                                                                  
                                                 This Room cannot be sold standalone it should be combined with any other product like Air, etc..
                                                </div>
                                                 <div class="custom-checkbox-style dark" style="display: none">
                                                 <input type="checkbox" id="ChkAddFee" class="checkbox" />
                                                 <label  id="lblChkAddFee" for="ChkAddFee" style="text-transform: none;">A fee of AED20 in addition to the hotel cancellation charges applies to all hotel cancellations!</label>
                                                     </div> 

                                                <div class="my-3">
                                           <label  id="lblSupplierPerformance"  style="text-transform: none;"><b>Supplier Performance: </b> I hereby acknowledge and consent that due to the nature of the platform and its connectivity to 3rd party service providers, some processes offered by the 3rd party providers are not under Travtrolley's control.
Furthermore, I accept that it is my responsibility upon signing up with 3rd party service providers to ensure their flows and processes are in compliance with my company's operational requirements. </label>
                                                    </div>
                                              

                                        </div>
                                    <div class="col-md-12 mt-3">
                                      
                                        <input type="button" class="btn btn-primary btn-lg float-right" value="Confirm" onclick="ConfirmBooking()" />
                                    </div>

                                </div>

                            </div>


                        </div>
                    </div>
                    <div class="col-md-4">
                        <div class="row">
                            <div class="col-md-12">

                                <h5 class="pt-3 pb-3 small-heading">YOUR CHOICE</h5>
                                <div class="ui-bg-wrapper">
                                    <div class="row">

                                        <div class="col-md-12">
                                            <h4 class="pb-0" id="h4HotelId"></h4>
                                        </div>
                                        <div class="col-md-12"><small class="text-address pb-3" id="HotelAddress"></small></div>

                                        <div class="col-md-12">
                                            <div id="Rating">
                                            </div>
                                        </div>

                                        <%--For room summary--%>
                                        <ul class="ui-listing-wrapper list-unstyled" id="list-group-roomsummary">
                                        </ul>
                                        <%--End -  For room summary--%>
                                        <div class="col-md-12 mt-2">
                                            <div class="row hotel-bookingdtl-dateinfo">
                                                <div class="col-md-12">
                                                    <span class="float-left text-gray-light"><i class="icon icon-calendar"></i>Check-in</span>
                                                    <span class="float-right text-gray-dark text-weight-400" id="startDate"></span>
                                                </div>
                                                <div class="col-md-12">
                                                    <span class="float-left text-gray-light"><i class="icon icon-calendar"></i>Check-out</span>
                                                    <span class="float-right text-gray-dark text-weight-400" id="endDate"></span>
                                                </div>

                                            </div>
                                        </div>

                                    </div>
                                </div>


                                <h5 class="mt-3 pt-3 pb-3 small-heading">FARE SUMMARY</h5>
                                <div class="ui-bg-wrapper">
                                    <div class="fare-summary">
									 <% if ((CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate ) == "N") { %>
                                        <div class="row custom-gutter">
                                            <div class="col-5">
                                                <div class="form-group">
                                                    <label>ADD MARKUP</label>
                                                    <input type="text" placeholder="0" maxlength="3" name="addlMarkUp" id="addlMarkUp" class="form-control" onkeypress="return isNumber(event);">
                                                </div>
                                            </div>
                                            <div class="col-4">
                                                <div class="form-group">
                                                    <label></label>
                                                    <select class="form-control" id="ddlAddlType">
                                                        <option value="P">Percentage</option>
                                                        <option value="F">Fixed</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-3">
                                                <div class="form-group mt-4 pt-2">
                                                    <input type="button" class="btn btn-primary" value="ADD" onclick="CalculateAddlMarkUp()" />
                                                </div>
                                            </div>
                                        </div><% } %>
                                        <table class="table table-condensed ui-price-table" id="Price-roomsummary">
                                        </table>
                                    </div>
                                    <div>
                                            <%--For tax breakup summary--%>
                                     <ul class="baggage-wrapper small mt-3 pb-3" id="list-group-taxsummary" style="display:none;">
                                     </ul>
                                     <div class="baggage-wrapper small mt-3 pb-3" id="taxsummaryListWrapper" style="display:none;">
                                        <ul id="list-group-taxsummary-Excl"></ul>
                                        <div id="totPayatHotel"></div>
                                     </div>
                                          
                                            <%--  template room summary--%>
    <div id="templateTaxSummary" style="display: none;">
        
                                        <span class="text pt-3 d-flex" id="spanTaxBreakup"></span>
        
                                    </div>
                                   <div id="templateTaxSummaryExcl" style="display: none;">
        
                                        <span class="text pt-3 d-flex" id="spanTaxBreakupExcl"></span>
        
                                    </div>
                                    <%--End -  For tax breakup--%>
                                            
                                        </div>
                                </div>
                                 &nbsp&nbsp <label   style="text-transform: none;"><b>Note: Rates may not include all fees(like Tourism fee etc.,)</b></label><br />


                            </div>
                        </div>
                    </div>
                </div>





            </div>

        </div>
    </div>
    <div class="ui-bg-wrapper mb-1  ui-room-wrapper" style="display: none;" id="Adult">
        <div class="row">
            <div class="col-md-12">
                <hr class="dotted bold" />
            </div>
        </div>
        <div class="row custom-gutter">
            <div class="col-md-3">
                <span class="font-weight-bold label-title" id="h4Adult"></span>
            </div>
            <div class="col-md-6" id="divSearchPaxA" style="display:none">
                <button type="button" id="btnSearchPaxA" class="btn btn-link float-right">Search Passenger<span class="glyphicon glyphicon-search"></span></button>
            </div>
            <div class="col-md-3">
                <span class="float-md-right room-type-title" id="RoomCount"></span>
            </div>
        </div>
        <div class="row custom-gutter">
            <div id="divPaxAddA" class="col-md-12">
                <span id="spnPaxAddA" style="float:right">Add Passenger</span><input type="checkbox" id="chkPaxAddA" style="float:right"/>
            </div>
        </div>

        <div class="row custom-gutter">
            <div class="form-group col-md-4" id="divAdultTitle">
                <%-- <label>Title</label>
                <select class="form-control custom-select" id="ddlAdultTitle">
<option value="">select Title</option>
                        <option value="Mr">Mr</option>
                        <option value="Mrs">Mrs</option>
                </select>--%>
            </div>
            <div class="form-group col-md-4">
                <label>First Name</label>
                <input type="text" class="form-control" id="txtAdultFirstName" placeholder="Enter First Name" onkeypress="return isAlphaNumeric(event);">
            </div>
            <div class="form-group col-md-4">
                <label>Last Name</label>
                <input type="text" class="form-control" id="txtAdultLastName" placeholder="Enter Last Name" onkeypress="return isAlphaNumeric(event);">
            </div>
        </div>
        <div class="row custom-gutter baggage-wrapper" style="display:none" id="divFlexA"></div>
    </div>
    <div class="ui-bg-wrapper mb-1  ui-room-wrapper" style="display: none;" id="Child">
        <div class="row custom-gutter">
            <div class="col-md-4">
                <span class="font-weight-bold label-title" id="h4Child"></span>
            </div>
            <div class="col-md-6" id="divSearchPaxC" style="display:none">
                <button type="button" id="btnSearchPaxC" class="btn btn-link float-right">Search Passenger<span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>
        <div class="row custom-gutter">
            <div id="divPaxAddC" class="col-md-12">
                <span id="spnPaxAddC" style="float:right">Add Passenger</span><input type="checkbox" id="chkPaxAddC" style="float:right"/>
            </div>
        </div>
        <div class="row custom-gutter">
            <div class="form-group col-md-4" id="divchild">
                <%--<label>Title</label>
                <select class="form-control custom-select" id="ddlChildTitle">
                    <option value="">select Title</option>
                    <option value="Master">Master</option>
                    <option value="Mr">Mr</option>
                    <option value="Mrs">Mrs</option>
                </select>--%>
            </div>
            <div class="form-group col-md-4">
                <label>First Name</label>
                <input type="text" class="form-control" placeholder="Enter First Name" id="txtChildFirstName" onkeypress="return isAlphaNumeric(event);">
            </div>
            <div class="form-group col-md-4">
                <label>Last Name</label>
                <input type="text" class="form-control" placeholder="Enter Last Name" id="txtChildLastName" onkeypress="return isAlphaNumeric(event);">
            </div>
        </div>
        <div class="row custom-gutter  baggage-wrapper" style="display:none" id="divFlexC"></div>
    </div>

    <%--  template room summary--%>
    <div id="templateRoomSummary" style="display: none;">
        <div class="col-md-12 mt-2">
            <h5 id="h5RoomName"></h5>
        </div>
        <div class="col-md-12">
            <ul class="ui-hotel-addons-list">
                <li class="with-icon" id="mealPlanDesc"></li>
                <%--<li class="with-icon"  >Room only</li>
								<li class="with-icon">FREE Breakfast</li>
								<li class="with-icon">FREE Cancellation</li>--%>
            </ul>

        </div>
        <div class="col-md-12 mt-2">
            <div class="row hotel-bookingdtl-dateinfo">
                <div class="col-md-12">
                    <span class="float-left text-gray-light" id="roomGuests"><i class="icon icon-group"></i>Room Guests</span>
                    <span class="float-right text-gray-dark text-weight-400" id="roomGuestsCount">1 Room & 2 Adults</span>
                </div>
            </div>
        </div>
         <div class="col-md-12 mt-2">
            <div class="row hotel-bookingdtl-dateinfo">
                <div class="col-md-12" id="ChildAgeInfo">
                   <span class="float-left text-gray-light"><i class="icon icon-group"></i>Child Age</span>
                <span class="float-right text-gray-dark text-weight-400" id="ChildAgeSummary"></span>
                </div>              
            </div>
        </div>
    </div>


    <div id="templateRoomPriceSummary" style="display: none;">
    </div>

    <!-- Search Passenger Modal -->
    <div id="SearchPassenger" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">Search Passenger</h4>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                    <div id="SearchFilters" class="row custom-gutter"></div>
                    </div>
                </div>
                <div class="modal-footer"> <button type="button" class="button" onclick="SearchPassenger(); return false">Search</button> </div>
                <div id="PassengerDetailsList" class="px-3 table table-responsive"></div>
            </div>
        </div>
    </div>
    <!--Search Passenger Modal End-->

    <!--Cancellation policy Modal -->
    <div class="modal fade in farerule-modal-style" data-backdrop="static" id="CancelPolicyModal" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="btnModelClose">&times;</button>
                    <h4 class="modal-title" id="headRoomTypeName"></h4>
                </div>
                <div class="modal-body">                                          
                    <div class="bg_white" id="h2CnlPolicy" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;"></div>
                </div>
            </div>
        </div>
    </div>
    <!--Cancellation policy Modal End-->

    <!--Terms and Condition Modal -->
    <div class="modal fade in farerule-modal-style" data-backdrop="static" id="TNCModal" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="">&times;</button>
                    <h4 class="modal-title" id="">Terms & Conditions</h4>
                </div>
                <div class="modal-body" style="max-height: 504px;">                                          
                    <div class="bg_white" id="h2TNC" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;"></div>
                </div>
            </div>
        </div>
    </div>
    <!--Terms and Condition Modal End-->
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
