﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpPolicyMaster.aspx.cs" Inherits="CozmoB2BWebApp.ExpPolicyMaster" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" /> 
    <link href="css/toastr.css" rel="stylesheet" type="text/css" />

    <script src="scripts/jquery-ui.js"></script>
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>

    <script type="text/javascript"> 

        /* Global variables */
        var apiHost = ''; var bearer = ''; var cntrllerPath = 'api/expenseMasters';
        var selectedPolicyInfo = [];
        var apiAgentInfo = {}; var expPolicyDetails = {}; var agentdata = {}; var typedata = {}; var citydata = {}; var paytypedata = {}; var currencydata = {};
        var costCenterdata = {};
        var EPM_ID = 0; var selectedProfile = 0;;var selectedExpType = 0

        /* Page Load */
        $(document).ready(function () {

            $("#ctl00_upProgress").show();

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {

                alert('Session expired, please login once again.');
                return;
            }

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            GetScreenData();

            $("#ctl00_upProgress").hide();
        });

        /* To get policy agent and company */
        function GetScreenData() {

            var reqData = { AgentInfo: apiAgentInfo, Screen:"PolicyMaster" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getPolicyMasterScreenLoadData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);                        
        }

         /* To bind policy master agent and type */
        function BindScreenData(screenData) {

            agentdata = {};

            if (!IsEmpty(screenData.dtAgents)) {

                agentdata = screenData.dtAgents;
                var options = agentdata.length != 1 ? GetddlOption('', '--Select Agent--') : '';

                $.each(agentdata, function (key, col) {

                    options += GetddlOption(col.agenT_ID , col.agenT_NAME);
                });
                
                $('#ddlAgentId').empty();
                $('#ddlAgentId').append(options); 
                $("#ddlAgentId").select2("val", '');

                if (agentdata.length == 1) {

                    $('#ddlAgentId').attr('disabled', 'disabled');
                    $("#ddlAgentId").select2("val", agentdata[0].agenT_ID);
                }
            }

                BindTypeData(screenData.dtExptypes);

                BindCityData(screenData.dtCity);

                BindPayTypeData(screenData.dtPayType);

                BindCurrencyData(screenData.dtCurrency);
        }

        /* To bind  expenses type data to type control */
        function BindTypeData(tData) {

            typedata = {};

            if (IsEmpty(tData)) {
                $('#ddlType').empty();
                $("#ddlType").select2("val", '');
                return;
            }
              
              typedata = tData;

              
              var options = GetddlOption('', '--Select Type--');

              $.each(typedata, function (key, col) {

                  options += GetddlOption(col.eT_ID, col.type);
              });
                
                $('#ddlType').empty();
                $('#ddlType').append(options); 
                $("#ddlType").select2("val", '');
        }

        /* To bind city data to city control */
        function BindCityData(cData) {

            citydata = {};

            if (IsEmpty(cData)) {
                $('#ddlCity').empty();
                $("#ddlCity").select2("val", '');
                return;
            }
              
              citydata = cData;

              
              var options = GetddlOption('', '--Select City--');

              $.each(citydata, function (key, col) {

                  options += GetddlOption(col.ecT_ID, col.ecT_Name);
              });
                
                $('#ddlCity').empty();
                $('#ddlCity').append(options); 
                $("#ddlCity").select2("val", '');
        }

        /* To bind paytype data to pay type control */
        function BindPayTypeData(ptData) {

            paytypedata = {};

            if (IsEmpty(ptData)) {
                $('#ddlPayType').empty();
                $("#ddlPayType").select2("val", '');
                return;
            }
              
              paytypedata = ptData;

              
              var options = GetddlOption('', '--Select Pay type--');

              $.each(paytypedata, function (key, col) {

                  options += GetddlOption(col.pT_ID, col.pT_Desc);
              });
                
                $('#ddlPayType').empty();
                $('#ddlPayType').append(options); 
                $("#ddlPayType").select2("val", '');
        }

        /* To bind currency data to currency control */
        function BindCurrencyData(curData) {

            currencydata = {};

            if (IsEmpty(curData)) {
                $('#ddlCurrency').empty();
                $("#ddlCurrency").select2("val", '');
                return;
            }
              
              currencydata = curData;

              
              var options = GetddlOption('', '--Select Currency--');

              $.each(currencydata, function (key, col) {

                  options += GetddlOption(col.currency_code, col.currency_code);
              });
                
                $('#ddlCurrency').empty();
                $('#ddlCurrency').append(options); 
                $("#ddlCurrency").select2("val", '');
        }

        /* To set employee name and cost center on employee change */
        function AgentChange(agentId) {

            selectedProfile = agentId;
            if (selectedProfile > 0) {

               var reqData = { AgentInfo: apiAgentInfo, AgentId : parseInt(selectedProfile) };
               var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getExpTypeData';
               WebApiReq(apiUrl, 'POST', reqData, '', BindTypeData, null, null); 
            }
            ClearPolicyInfo();
        }

        /* To set UOM, Paytype and cost center on agent change */
        function ExpTypeChange(typeid) {
            
            selectedExpType = typeid;
            if (selectedExpType > 0) {

               var reqData = { AgentInfo: apiAgentInfo, ExpTypeID : parseInt(selectedExpType) };
               var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getExpTypeChangeData';
               WebApiReq(apiUrl, 'POST', reqData, '', BindTypeChangeData, null, null); 
            }
            ClearPolicyInfo();
            $("#ddlType").select2("val", parseInt(typeid));
        }

         /* To bind policy master costcenter and pay type */
        function BindTypeChangeData(typechangeData) {

            
             BindCostCenterData(typechangeData.dtCostCenter);

             BindPayTypeData(typechangeData.dtPayType);

            if (!IsEmpty(typechangeData.etuom)) {
                $('#txtUOM').val(typechangeData.etuom);
                $('#txtUOM').attr('disabled', 'disabled');
            }
        }

        /* To bind cost center data to costcenter control */
        function BindCostCenterData(ccData) {

            costCenterData = {};

            if (IsEmpty(ccData)) {
                $('#ddlCostCenter').empty();
                $("#ddlCostCenter").select2("val", '');
                return;
            }
              
              costCenterData = ccData;

              var options = GetddlOption('', '--Select Cost Center--');

              $.each(costCenterData, function (key, col) {

                  options += GetddlOption(col.setUpId, col.name);
              });
                
                $('#ddlCostCenter').empty();
                $('#ddlCostCenter').append(options); 
                $("#ddlCostCenter").select2("val", '');
        }

        /* To set currency on city change */
        function CityChange(cityid) {

            selectedCityId = cityid;
            if (selectedCityId > 0) {

               var reqData = { AgentInfo: apiAgentInfo, CityId : parseInt(selectedCityId) };
               var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCityWiseCurrency';
               WebApiReq(apiUrl, 'POST', reqData, '', BindCitywiseCurrency, null, null); 
            }
        }

         /* To bind currency */
        function BindCitywiseCurrency(citycurrency) {

            
            if (!IsEmpty(citycurrency)) {
                $("#ddlCurrency option").filter(function () {
                    return $(this).text() == citycurrency;
                       }).prop('selected', true).trigger("change");
            }
        }

        /* To get policy info */
        function LoadPolicyInfo() {
             if (($("#ddlAgentId").val() == "" || $("#ddlAgentId").val() == "--Select Agent--") || ($("#ddlType").val() == "" || $("#ddlType").val() == "--Select Type--" || $("#ddlType").val() == null)) {
                if ($("#ddlAgentId ").val() == "" || $("#ddlAgentId ").val() == "--Select Type--") {
                    toastr.error('Please Select Agent');
                    $('#ddlAgentId').parent().addClass('form-text-error');
                }
                if ($("#ddlType ").val() == "" || $("#ddlType ").val() == "--Select Type--" || $("#ddlType").val() == null) {
                    toastr.error('Please Select Type');
                    $('#ddlType').parent().addClass('form-text-error');
                }
                return false;
            }
            var reqData = { AgentInfo: apiAgentInfo, TypeId : parseInt($("#ddlType").val()) };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getPolicyInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', BindPolicyInfo, null, null);                        
        }
               
        /* To Bind policy info to grid */
        function BindPolicyInfo(policyInfo) {
            
            if (policyInfo.length == 0) {

                ShowError('No records to show.');
                ClearPolicyInfo();
                return;
            }

            expPolicyDetails = policyInfo;
            
            var gridProperties = GetGridPropsEntity();
            gridProperties.headerColumns = ('Type|Name|City|Units|UOM|Currency|PayType|AllowFutureDate|No. Of Attendees|Cost Center|Fixed Amount|Warning Amount|Block Amount|Warning Message').split('|');
            gridProperties.displayColumns = ('epM_TypeName|epM_Name|epM_CityName|epM_Units|epM_UOM|epM_Currency|epM_PayType|epM_AllowFutureDate|epM_NoOfAttendees|epM_CostCenterName|epM_FixedAmount|epM_WarningAmount|epM_BlockAmount|epM_WarningMsg').split('|');
            gridProperties.pKColumnNames = ('epM_Id').split('|');
            gridProperties.dataEntity = policyInfo;
            gridProperties.divGridId = 'divGridPolicyInfo';
            gridProperties.headerPaging = true;
            gridProperties.gridSelectedRows = selectedPolicyInfo;
            gridProperties.selectType = 'M';
            gridProperties.recordsperpage = 3;

            EnablePagingGrid(gridProperties);
            $('#divPolicyList').show();
        }

        /* To clear Policy info details and hide the div */
        function ClearPolicyInfo() {
             
            selectedProfile = 0;
            selectedExpType = 0;
            selectedPolicyInfo = [];
            expPolicyDetails = {};
            EPM_ID = 0;
            $("#ddlType").select2('val', '');
            $("#txtName").val('');
            $("#ddlCity").select2('val', '');
            $("#txtUnits").val('');
            $("#txtUOM").val('');
            $("#ddlCurrency").select2('val', '');
            $("#ddlPayType").select2('val', '');
            $("#chkFutureDate").prop("checked", false);
            $("#txtAttendees").val('');
            $("#ddlCostCenter").select2('val', '');
            $("#txtFixedAmount").val('');
            $("#txtWarningAmount").val('');
            $("#txtBlockAmount").val('');
            $("#txtWarningMsg").val('');
            $("#ddlType").parent().removeClass('form-text-error');
            $("#txtName").parent().removeClass('form-text-error');
            RemoveGrid();
            $('#divPolicyList').hide();
        }

        /* To delete selected policy info from the grid and update the status in data base */
        function DeletePolicyInfo() {

            var DelPolicyInfo = GetselectedPolicyInfo();
            if (IsEmpty(DelPolicyInfo) || DelPolicyInfo.length == 0) {

                ShowError('Please select policy to delete');
                return;
            }
            
                var reqData = { AgentInfo: apiAgentInfo, DelPolicyInfo, Status: 'D' };
                var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/addPolicyInfo';
                WebApiReq(apiUrl, 'POST', reqData, '', RefreshGrid, null, null);                        
        }

        /* To prepare and get the Policy info entity list for selected Policy */
        function GetselectedPolicyInfo() {

            selectedPolicyInfo = GetSetSelectedRows();
            if (IsEmpty(selectedPolicyInfo) || selectedPolicyInfo.length == 0)                 
                return [];

            var expPolicyInfoSelected = [];
            $.each(selectedPolicyInfo, function (key, col) {
                var ainfo = expPolicyDetails.find(item => item.epM_Id == col);
                ainfo.epM_Status = false;
              expPolicyInfoSelected = expPolicyInfoSelected.concat(ainfo);
            });
            return expPolicyInfoSelected;
        }


        /* To prepare and set Poli info entity */
        function RefreshGrid(resp) {

            $.each(selectedPolicyInfo, function (key, col) {
                var epmid = parseInt(col);
                expPolicyDetails = expPolicyDetails.filter(item => (item.epM_Id) !== epmid)
            });

            if (expPolicyDetails.length == 0) {

                BindPolicyInfo(expPolicyDetails);
                return;
            }

            selectedPolicyInfo = [];
            GetSetSelectedRows('set', selectedPolicyInfo);
            GetSetDataEntity('set', expPolicyDetails);
            LoadData(GetSetCurrentPageNo('get', ''));
        }

        /* To bind update policy info */
        function SaveSuccess(resp) {
             if (EPM_ID > 0) {
                alert('record updated successfully!');
            }
            else if (EPM_ID == 0) {
                alert('record added successfully!');
            }
            $("#ctl00_upProgress").hide();
            ClearPolicyInfo();
        }
        
         /* To edit selected policy info from the grid and update the data in data base */
        function EditPolicyInfo() {

            selectedPolicyInfo = GetSetSelectedRows();
            if (IsEmpty(selectedPolicyInfo) || selectedPolicyInfo.length == 0) {
                
                ShowError('Please select Policy to edit');
                return;
            }

            if (IsEmpty(selectedPolicyInfo) || selectedPolicyInfo.length > 1) {
                
                ShowError('Please select one policy to edit');
                return;
            }
            var exppolicyinfoDetails = expPolicyDetails.find(item => item.epM_Id == selectedPolicyInfo[0]);

            EPM_ID = exppolicyinfoDetails.epM_Id;
            $("#ddlType").val(exppolicyinfoDetails.epM_ET_Id);
            $("#txtName").val(exppolicyinfoDetails.epM_Name);
            $("#ddlCity").select2('val', exppolicyinfoDetails.epM_ECT_Id);
            $("#txtUnits").val(parseFloat(exppolicyinfoDetails.epM_Units).toFixed(2));
            $("#txtUOM").val(exppolicyinfoDetails.epM_UOM);
            $("#ddlCurrency").select2('val', exppolicyinfoDetails.epM_Currency);
            $("#ddlPayType").select2('val', exppolicyinfoDetails.epM_PT_Id);
            if (exppolicyinfoDetails.epM_AllowFutureDate == 'Y') {
                $("#chkFutureDate"). prop("checked", true);
            }
            $("#txtAttendees").val(exppolicyinfoDetails.epM_NoOfAttendees);
            $("#ddlCostCenter").select2('val', exppolicyinfoDetails.epM_CostCentre);
            $("#txtFixedAmount").val(parseFloat(exppolicyinfoDetails.epM_FixedAmount).toFixed(2));
            $("#txtWarningAmount").val(parseFloat(exppolicyinfoDetails.epM_WarningAmount).toFixed(2));
            $("#txtBlockAmount").val(parseFloat(exppolicyinfoDetails.epM_BlockAmount).toFixed(2));
            $("#txtWarningMsg").val(exppolicyinfoDetails.epM_WarningMsg);
        }

        /* To validate and save Policy info */
        function SavePolicyInfo() {
            var policycheck = 'N';
           
            if (($("#ddlAgentId").val() == "" || $("#ddlAgentId").val() == "--Select Agent--") || ($("#ddlType").val() == "" || $("#ddlType").val() == "--Select Type--" || $("#ddlType").val() == null) || ($("#txtName").val() == "" || $("#txtName").val() == "Enter Name")) {
                if ($("#ddlAgentId ").val() == "" || $("#ddlAgentId ").val() == "--Select Type--") {
                    toastr.error('Please Select Agent');
                    $('#ddlAgentId').parent().addClass('form-text-error');
                }
                if ($("#ddlType ").val() == "" || $("#ddlType ").val() == "--Select Type--" || $("#ddlType").val() == null) {
                    toastr.error('Please Select Type');
                    $('#ddlType').parent().addClass('form-text-error');
                }
                if ($("#txtName").val() == "" || $("#txtName").val() == "Enter Name") {
                    toastr.error('Please Enter Name');
                    $('#txtName').parent().addClass('form-text-error');
                }
                return false;
            }
            if ($('input[type="checkbox"]').prop("checked")) {
                policycheck = 'Y';
            }
            
                var Adddata = {
                    AgentInfo: apiAgentInfo,
                    PolicyInfo: {
                            EPM_Id: parseInt(EPM_ID),
                            EPM_ET_Id: parseInt($("#ddlType").val()),
                            EPM_Name: $("#txtName").val(),
                            EPM_ECT_Id: $("#ddlCity").val(),
                            EPM_Units: $("#txtUnits").val(),
                            EPM_UOM: $("#txtUOM").val(),
                            EPM_Currency: $("#ddlCurrency").val(),
                            EPM_PT_Id: $("#ddlPayType").val(),
                            EPM_AllowFutureDate: policycheck,
                            EPM_NoOfAttendees: $("#txtAttendees").val(),
                            EPM_CostCentre: $("#ddlCostCenter").val(),
                            EPM_FixedAmount: $("#txtFixedAmount").val(),
                            EPM_WarningAmount: $("#txtWarningAmount").val(),
                            EPM_BlockAmount: $("#txtBlockAmount").val(),
                            EPM_WarningMsg: $("#txtWarningMsg").val(),
                            EPM_Status: true,
                            EPM_CreatedBy: apiAgentInfo.LoginUserId,
                            EPM_ModifiedBy: apiAgentInfo.LoginUserId
                }
            };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/addPolicyInfo';
            WebApiReq(apiUrl, 'POST', Adddata, '', SaveSuccess, null, null);
        }


        /* To get agent and login info */
        function GetAgentInfo() {

            try {

                var loginInfo = '<%=Settings.LoginInfo == null%>';

                if (loginInfo == true)
                    return apiAgentInfo;

                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {

            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';

            if (IsEmpty(apiHost))
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";

            return apiHost;
        }

        function Setdecimal(money, text) {
            if (money == "")
                money = 0;
            if(text == "Units")
                $("#txtUnits").val(parseFloat(money).toFixed(2));
                if(text == "FixedAmt")
                $("#txtFixedAmount").val(parseFloat(money).toFixed(2));
                if(text == "WarningAmt")
                $("#txtWarningAmount").val(parseFloat(money).toFixed(2));
                if(text == "BlockAmt")
                $("#txtBlockAmount").val(parseFloat(money).toFixed(2));
        }

        /* To validate numeric values of text box and no spacebar */
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

    </script>

    <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">
                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left">Policy Master</h3>
                    </div>
                 </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <button class="btn btn-info mr-2" id="showPolicyMasterList" type="button" data-toggle="collapse" data-target="#PolicyMasterList" aria-expanded="false" aria-controls="PolicyList" onclick="LoadPolicyInfo()">VIEW ALL</button>
                    <button type="button" class="btn btn-gray mr-2" onclick="ClearPolicyInfo();">CLEAR <i class="icon icon-close "></i></button>
                    <button type="button" class="btn btn-primary" onclick="SavePolicyInfo();">SUBMIT <i class="icon icon-play_arrow "></i></button>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label>Agent <span class="fcol_red">*</span></label>
                            <select id="ddlAgentId" class="form-control" onchange="AgentChange(this.value)"></select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Expense Type <span class="fcol_red">*</span></label>
                            <select id="ddlType" class="form-control" onchange="ExpTypeChange(this.value)"></select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Name <span class="fcol_red">*</span></label>
                            <input type="text" id="txtName" name="Name" class="form-control" maxlength="250" placeholder="Enter Name" onkeypress="return IsAlpha(event)" />
                        </div>
                        <div class="form-group col-md-2">
                            <label>City</label>
                            <select id="ddlCity" class="form-control" onchange="CityChange(this.value)"></select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Units</label>
                            <input type="text" id="txtUnits" name="money" class="form-control"  placeholder="0.00" onblur="Setdecimal(this.value,'Units');" onkeypress="return isNumberKey(event)"/>
                        </div>
                        <div class="form-group col-md-2">
                            <label>UOM</label>
                            <input type="text" id="txtUOM" name="UOM" class="form-control"  placeholder="Enter UOM" maxlength="5"/>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Currency</label>
                            <select id="ddlCurrency" class="form-control" ></select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>PayType</label>
                            <select id="ddlPayType" class="form-control" ></select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>AllowFuturedate</label><br />
                            <input type="checkbox" id="chkFutureDate" />
                        </div>
                        <div class="form-group col-md-2">
                            <label>No. of Attendees</label>
                            <input type="text" id="txtAttendees" name="Number" class="form-control" maxlength="4" onkeypress="return isNumberKey(event)" placeholder="Enter Attendee" />
                        </div>
                        <div class="form-group col-md-2">
                            <label>Cost Center</label>
                            <select id="ddlCostCenter" class="form-control" ></select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Fixed Amount</label>
                            <input type="text" id="txtFixedAmount" name="money" class="form-control"  placeholder="0.00" onblur="Setdecimal(this.value,'FixedAmt');" onkeypress="return isNumberKey(event)"/>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Warning Amount</label>
                            <input type="text" id="txtWarningAmount" name="money" class="form-control"  placeholder="0.00" onblur="Setdecimal(this.value,'WarningAmt');" onkeypress="return isNumberKey(event)"/>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Block Amount</label>
                            <input type="text" id="txtBlockAmount" name="money" class="form-control"  placeholder="0.00" onblur="Setdecimal(this.value,'BlockAmt');" onkeypress="return isNumberKey(event)"/>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Warning Message</label>
                            <textarea id="txtWarningMsg" name="WarningMsg" class="form-control" rows="1" cols="250"  placeholder="Enter Warning Message" ></textarea>
                        </div>
                </div>
            </div>
                
                <div id="divPolicyList" class="exp-content-block" style="display:none">
                   <h5 class="mb-3">All Policies</h5>
                    <div class="button-controls text-right">
                        <a class="btn btn-edit" onclick="EditPolicyInfo();">EDIT  <i class="icon icon-edit"></i></a>
                        <a class="btn btn-danger" onclick="DeletePolicyInfo();">DELETE  <i class="icon icon-delete"></i></a>
                    </div>
                    <div id="divGridPolicyInfo"></div>
                </div>

           </div>
        </div>
        <div class="clear "></div>
    </div>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
