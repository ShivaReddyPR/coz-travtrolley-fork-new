﻿<%@ Page Title="Agent Credit Limit" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="AgentCreditLimit.aspx.cs" Inherits="CozmoB2BWebApp.AgentCreditLimit" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <div class="col-md-12">
        <div class="ns-h3">
            <span>Edit Agent Credit Limit</span>
        </div>
        <div class="col-md-12">
            <span>Agent : </span>
            <asp:DropDownList ID="ddlAgents" runat="server" Enabled="false" DataTextField="agent_name"  DataValueField="agent_id" Width="500px" AutoPostBack="true" OnSelectedIndexChanged="ddlAgents_SelectedIndexChanged"></asp:DropDownList>
        </div>
        <br />
        <div class="col-md-6">
            <span>Credit Limit</span>
             <asp:TextBox ID="txtCreditLimit" runat="server" onkeypress="return allowNumerics(event);"  CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-md-6">
            <span>Temp Credit</span>
            <asp:TextBox ID="txtCreditBuffer" runat="server" onkeypress="return allowNumerics(event);" CssClass="form-control"></asp:TextBox>
        </div>
        <br />
         <div class="col-md-6">
            <span>Credit Days : </span>
            <asp:TextBox ID="txtCreditDays" runat="server" onkeypress="return allowNumerics(event);" MaxLength="2" CssClass="form-control"></asp:TextBox>
        </div>
        <div class="col-md-6">
            <span>Agent Current Balance : </span>
            <asp:Label ID="lblCurrentBalance" runat="server" Font-Bold="true" ></asp:Label>
        </div> 
        <br />
        <div class="col-md-12">
            <asp:Label ID="lblMessage" runat="server" style="display:none" Font-Bold="true"></asp:Label>
        </div>
        <br />
        <div class="col-md-12" style="text-align:right">
            <asp:Button ID="btnSave" runat="server" CssClass="btn btn-primary" Text="Save" OnClientClick="return Validate();" OnClick="btnSave_Click" />
        </div>
    </div>
    
    <div class="col-md-12">
        <asp:GridView ID="gvCreditHistory" runat="server" EmptyDataText="No Records found" Width="100%" 
            AllowPaging="true" PageSize="20" Caption="Credit History" CssClass="grdTable" OnPageIndexChanging="gvCreditHistory_PageIndexChanging">
            
            <RowStyle CssClass="gvDtlRow" />
            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
        </asp:GridView>
    </div>

    <script>
        function Validate() {
            if (eval(getElement('txtCreditLimit').value.replace(/,/g, '')) == 0) addMessage('Credit Limit cannot be ZERO');
            //if (eval(getElement('txtCreditBuffer').value.replace(/,/g, '')) == 0) addMessage('Temperory Credit Limit cannot be ZERO');
            if (eval(getElement('txtCreditDays').value) == 0) addMessage('Credit Days cannot be ZERO');
            if (getElement('txtCreditLimit').value=='' || getElement('txtCreditBuffer').value=='' || getElement('txtCreditDays').value=='') addMessage('Fields Cannot be Empty');
            if (getMessage() != '') {
                alert(getMessage()); clearMessage(); return false;
            }
            else
                return true;
        }
        function allowNumerics(evt) {
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode != 46 && charCode > 31
                        && (charCode < 48 || charCode > 57))
                        return false;

                    return true;
                }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
