﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="B2BVisaIframe" EnableEventValidation="true"
    Title="B2BVisaIframe" Codebehind="B2BVisaIframe.aspx.cs" %>

<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <div>
        <iframe style="width: 1490px;" height="650" id="iframeVisa" src="<%=System.Configuration.ConfigurationManager.AppSettings["VisaIframeUrl"].ToString() %>?b2bagentId=<%=b2bagentId %>&userId=<%=userId %>&userLoginName=<%=userName%>&product=<%=product%>&password=<%=password%>">
        </iframe>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
