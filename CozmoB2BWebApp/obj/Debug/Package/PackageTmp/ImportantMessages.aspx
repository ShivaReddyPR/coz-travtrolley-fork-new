﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="ImportantMessages" Title="Untitled Page" Codebehind="ImportantMessages.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript">

        function Save() {
            if (getElement('ddlAgent').selectedIndex <= 0) addMessage('Please Select the Agent!', '');
            if (getElement('txtSubject').value == '') addMessage('Subject cannot be blank!', '');
            if (getElement('txtMessage').value == '') addMessage('Message cannot be blank!', '');
            
            if (getMessage() != '') {
                alert(getMessage()); clearMessage();
                return false;
            }
        }
    </script>
    
    
    
    <div class="body_container">
    
    <div class="paramcon"> 
      <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-3"> <span class="pull-right fl_xs"> <asp:Label ID="lblToAgent" runat="server" Text="To Agent:"> </asp:Label><label style="color: Red">*</label></span></div>
    <div class="col-md-4"><asp:DropDownList ID="ddlAgent" CssClass="inputDdlEnabled form-control" runat="server">
                    </asp:DropDownList> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
          <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-3"> <span class="pull-right fl_xs"><asp:Label ID="lblSubject" runat="server" Text="Subject:"></asp:Label><label style="color: Red">*</label> </span></div>
    <div class="col-md-4"> <asp:TextBox ID="txtSubject" CssClass="inputEnabled form-control" MaxLength="100" runat="server"
                       ></asp:TextBox></div>


    <div class="clearfix"></div>
    </div>
    


      <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-3"> <span class="pull-right fl_xs"><asp:Label ID="lblMessage" runat="server" Text="Message:"></asp:Label><label style="color: Red">*</label> </span> </div>
    <div class="col-md-4"><asp:TextBox ID="txtMessage"  CssClass="inputEnabled text_area" runat="server" TextMode="MultiLine"
                        MaxLength="500" runat="server"  Height="100px"></asp:TextBox> </div>


    <div class="clearfix"></div>
    </div>
    
    



      <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-3"> <span class="pull-right fl_xs"><asp:Label ID="lblReadStatus" runat="server" Text="Read Status:"></asp:Label>
    </span>
    
    </div>
    <div class="col-md-4"><asp:RadioButtonList runat="server" ID="rbtnReadStatus" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Yes" Value="Y" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="N"></asp:ListItem>
                    </asp:RadioButtonList> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
    
          <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-3"> </div>
    <div class="col-md-4">                   <label class="marright_10"> <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn but_b" OnClientClick="return Save();"
                        OnClick="btnSave_Click" /></label>
                    <label class="marright_10"><asp:Button ID="btnCancel" runat="server" Text="Clear" CausesValidation="false"
                        CssClass="btn but_b" OnClick="btnCancel_Click" /></label>
                   <label class="marright_10"><asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn but_b" OnClick="btnSearch_Click" /></label>
    </div>


    <div class="clearfix"></div>
    </div>
    
    </div>
      
      
      <div>  <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>
                    <asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError"></asp:Label></div>
      <div class="clearfix"></div>
      
  </div>
    
 
  
  

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
 
   
<asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="msg_id"
        EmptyDataText="No Messages!" AutoGenerateColumns="false" PageSize="10" GridLines="none"
        CssClass="grdTable" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging"
        OnSelectedIndexChanged="gvSearch_SelectedIndexChanged">
        
        <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                ControlStyle-CssClass="label" ShowSelectButton="True" />
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <ItemStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <b>
                        <label id="HDlblAgent">
                            Agent Name</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblAgent" runat="server" Text='<%# Eval("agent_name") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("agent_name") %>' Width="70px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <ItemStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <b>
                        <label id="HDlblSubject">
                            Subject</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblSubject" runat="server" Text='<%# Eval("subject") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("subject") %>' Width="130px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <HeaderTemplate>
                    <b>
                        <label id="HDlblReadStatus">
                            Read Status</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblReadStatus" runat="server" Text='<%# Eval("read_status") %>'
                        CssClass="label grdof" ToolTip='<%# Eval("read_status") %>' Width="50px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <HeaderTemplate>
                    <b>
                        <label id="HDlblCreatedBy">
                            Created By</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblCreatedBy" runat="server" Text='<%# Eval("CreatedByName") %>'
                        CssClass="label grdof" ToolTip='<%# Eval("CreatedByName") %>' Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <HeaderTemplate>
                    <b>
                        <label id="HDlblCreatedOn">
                            Created Date</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblCreatedBy" runat="server" Text='<%# Eval("createdOn") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("createdOn") %>' Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <HeaderTemplate>
                    <b>
                        <label id="HDlblModifiedBy">
                            Created By</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblModifiedBy" runat="server" Text='<%# Eval("ModifiedByName") %>'
                        CssClass="label grdof" ToolTip='<%# Eval("ModifiedByName") %>' Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <HeaderTemplate>
                    <b>
                        <label id="HDlblModifiedOn">
                            Modified Date</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblModifiedOn" runat="server" Text='<%# Eval("ModifiedOn") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("ModifiedOn") %>' Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
