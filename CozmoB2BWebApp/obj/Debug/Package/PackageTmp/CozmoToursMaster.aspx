﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CozmoToursMasterGUI" Title="Cozmo Tours" Codebehind="CozmoToursMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<div class="body_container">



<div class="paramcon"> 

  <table id="tblCountryDetails" runat="server">
                            <tr>
                           
                           
                            <td>
                            
                                           
                        
                              <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-4"><asp:Label CssClass="pull-right fl_xs" ID="lblSelectCountry" runat="server" Text="Select Country:"></asp:Label> <div class="clearfix"></div></div>
    <div class="col-md-3"><asp:DropDownList ID="ddlCountry" CssClass="form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectIndexChanged"></asp:DropDownList> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
          <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-4"> <asp:Label CssClass="pull-right fl_xs" ID="lblCityName" runat="server" Text="Select City:"></asp:Label></div>
   
    <div class="col-md-3"><asp:DropDownList ID="ddlCity" CssClass="form-control" AutoPostBack="true" runat="server"></asp:DropDownList>
     <div class="clearfix"></div>
     
     </div>


    <div class="clearfix"></div>
    </div>
    
    
          <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-4"> <asp:Label CssClass="pull-right fl_xs" ID="lblActivityName" runat="server" Text="Activity Name:"></asp:Label>
    
    <div class="clearfix"></div>
    
    </div>
    <div class="col-md-3"> <asp:TextBox ID="txtActivity" CssClass="form-control" runat="server" ></asp:TextBox></div>


    <div class="clearfix"></div>
    </div>
    
    
          <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-4"><asp:Label CssClass="pull-right fl_xs" ID="lblUrl" runat="server" Text="URL Link:"></asp:Label> <div class="clearfix"></div></div>
    <div class="col-md-3"> <asp:TextBox CssClass="form-control" ID="txtUrl" Text="" runat="server" ></asp:TextBox></div>


    <div class="clearfix"></div>
    </div>
             
       
        
        
              <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-4"> </div>
    <div class="col-md-3">
             <asp:Button ID="btnSave" Text="Save" runat="server" CssClass="btn but_b marright_10"  OnClick ="btnSave_Click"  style="display:inline;" ></asp:Button>
                     <asp:Button ID="btnCancel" Text="Clear" runat="server" CssClass="btn but_b marright_10"  OnClick="btnCancel_Click"  style="display:inline;"></asp:Button>   
    
     </div>


    <div class="clearfix"></div>
    </div>
    
    
                        
                            </td>
                             
                                
                               
                            </tr>
         
         
                   
                            
                     
                           
   </table>
   
   
   </div>
   
   
   <div class="table-responsive"> 
  <table width="100%"  id="tabSearch" border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td>
                   <asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="True" DataKeyNames="TourId"
   AutoGenerateColumns="False" GridLines="none" EmptyDataText="No Activity Tour List!"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" PageSize="15"
    OnPageIndexChanging="gvSearch_PageIndexChanging" CellPadding="1" CellSpacing="0"  >
     <HeaderStyle  CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow"  HorizontalAlign="left" />
      <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
    <Columns> 
    <asp:CommandField ButtonType="Link" 
            SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  
            ShowSelectButton="True"  >
    
           
<ControlStyle Width="10px"></ControlStyle>
        </asp:CommandField>
    
           
      <asp:TemplateField>
      <HeaderStyle HorizontalAlign="Left" />
    <HeaderTemplate>
    <label><strong> ID</strong></label><br />
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
      <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' CssClass="label"
                            ToolTip='<%# Container.DataItemIndex+1 %>' Width="20px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="Left" />
    <HeaderTemplate>
    <label><strong> Country</strong></label><br />
   <%-- <asp:DropDownList ID="HTddlCountryId"  Width="100px" CssClass="inputEnabled" runat="server"  OnSelectedIndexChanged="HTddlCountryId_SelectIndexChanged" ></asp:DropDownList>--%>
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="actTourCountry"  runat="server" Text='<%# Eval("CountryName") %>' CssClass="label grdof" ToolTip='<%# Eval("CountryName") %>'></asp:Label>
    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>--%>
    </ItemTemplate>    
    
    </asp:TemplateField>     
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="Left" />
    <HeaderTemplate >
    <label><strong> City</strong></label><br />
    <%--<asp:DropDownList ID="HTddlCityId"  Width="100px" CssClass="inputEnabled" runat="server" AutoPostBack="true" OnSelectedIndexChanged="HTddlCityId_SelectIndexChanged" ></asp:DropDownList>--%>
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="actTourCity" runat="server" Text='<%# Eval("CityName") %>' CssClass="label grdof"  ToolTip='<%# Eval("CityName") %>'></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>  
     
     <asp:TemplateField>
     <HeaderStyle HorizontalAlign="Left" />
    <HeaderTemplate >
    <label><strong> Activity Name</strong></label><br />
   <%-- <asp:TextBox ID="HTtxtName"  Width="100px" CssClass="inputEnabled" runat="server" ></asp:TextBox>--%>
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="actTourName" runat="server" Text='<%# Eval("ActivityName") %>' CssClass="label grdof"  ToolTip='<%# Eval("ActivityName") %>' ></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>        
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="Left" />
    <HeaderTemplate >
    <label><strong> Activity URL</strong></label><br />
    <%--<asp:TextBox ID="HTtxtUrl"  Width="100px" CssClass="inputEnabled" runat="server" ></asp:TextBox>--%>
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:HyperLink ID="actTourUrl" runat="server" NavigateUrl='<%# Eval("URL") %>'  Text='Go To Link' Font-Underline="false" Target="_blank" CssClass="label grdof"  ToolTip='<%# Eval("URL") %>'></asp:HyperLink>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    </Columns> 
    <%-- <RowStyle BackColor="#E3EAEB" />
        <EditRowStyle BackColor="#7C6F57" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle  ForeColor="White" HorizontalAlign="Left" Font-Bold="True" />
  <HeaderStyle BackColor="#761002" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" /> 
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
            NextPageText="Next" PreviousPageText="Previous" />    --%>      
    </asp:GridView>
                   
 </td>
 </tr>
 </table>
 
 
 </div>
</div>

</asp:Content>

