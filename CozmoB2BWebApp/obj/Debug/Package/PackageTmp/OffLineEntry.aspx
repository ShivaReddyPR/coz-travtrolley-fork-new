﻿<%@ Page
    Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="OffLineEntry.aspx.cs" Inherits="OffLineEntry" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>

<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc1" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <asp:UpdatePanel ID="uppanel1" runat="server">
        <ContentTemplate>
            <%--<script src="Scripts/OffLineEntryCities.js" type="text/javascript" defer="defer"></script>--%>
            <script src="css/toastr.min.js"></script>
            <link href="css/toastr.css" rel="stylesheet" />
            <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
            <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />

            <script src="scripts/jquery-ui.js"></script>
            <script src="scripts/Common/FlexFields.js"></script>
            <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

            <script type="text/javascript" src="yui/build/event/event-min.js"></script>

            <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

            <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

            <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

            <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

            <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

            <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

            <script src="yui/build/container/container-min.js" type="text/javascript"></script>

            <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
            <%--<script type="text/javascript" src="Scripts/jsBE/Search.js"></script>--%>
            <style>
                .sales_container select,
                input {
                    padding-left: 4px;
                }

                .main_panel {
                    width: 100%;
                    /*			height: 768px;*/
                    display: block;
                    position: relative;
                    margin: 0 auto;
                    overflow: hidden;
                }

                .main_panel_wrap {
                    padding: 5px;
                }

                .show_cont {
                    overflow-y: auto;
                }

                .left_col {
                    background: #89a0c0;
                    padding: 0px;
                    border-right: solid 1px #ccc;
                    color: #fff;
                }

                    .left_col label {
                        margin-bottom: 0px;
                        color: #fff;
                        font-size: 90%;
                    }

                    .left_col a {
                        color: #fff;
                        font-size: 1.1em;
                    }

                    .left_col .form-group {
                        margin-bottom: 4px !important;
                    }

                    .left_col .form-group-title {
                        margin-top: 10px;
                        padding-left: 6px;
                        font-weight: bold;
                        font-size: 90%;
                        background: #1c498a;
                    }

                .right_col {
                    padding: 0px;
                    margin: 0px;
                }

                .main_panel .form-group {
                    margin: 0px;
                }

                .multi_inpt .input-group-addon {
                    padding: 0px 3px 0px 0px;
                    line-height: 1;
                    color: #555;
                    text-align: left !important;
                    background-color: transparent !important;
                    border: 0px solid #ccc;
                    width: 50%;
                }

                .multi_inpt.three-col .input-group-addon {
                    width: 33.33%;
                }

                .sales_container .btn-primary {
                    background: #1c498a;
                }

                .fixed_inpt_cal .pl-0 {
                    padding-left: 0px;
                    margin-bottom: 10px;
                }

                .fixed_inpt_cal .input-group-addon label {
                    float: left;
                }

                .fixed_inpt_cal .input-group-addon {
                    border-radius: 0px;
                    border: none;
                }

                .fixed_inpt_cal .btn_custom {
                    float: right;
                }

                .gridbg td,
                th {
                    font-size: 13px;
                }

                .gridbg {
                    overflow: auto;
                    overflow-y: hidden;
                    width: 100%;
                    background: #eeeeee;
                }

                .sector td,
                th {
                }

                .footer_big {
                    display: none !important;
                }

                .tab-content {
                    padding-top: 10px;
                }

                .col-container {
                    width: 100%;
                    margin-bottom: 70px;
                    position: relative;
                    display: flex;
                }

                .col {
                    -webkit-box-flex: 1;
                    -ms-flex: 1;
                    flex: 1;
                    max-width: 100%;
                    background: #e7e7e7;
                    vertical-align: top;
                    position: relative;
                    float: left;
                }

                .col2 {
                    -webkit-box-flex: 1;
                    -ms-flex: 1;
                    flex: 1;
                    max-width: 100%;
                    vertical-align: top;
                    float: left;
                }

                .col input, .CorpTrvl-page .form-control {
                    border-radius: 0px;
                    height: 30px;
                    text-transform: uppercase;
                }

                .col2 input {
                    border-radius: 0px;
                    height: 30px;
                }

                .col .form-group {
                    margin-bottom: 6px;
                }

                .col .form-group {
                    margin-bottom: 6px;
                }

                .col,
                .col2,
                label {
                    font-size: 12px !important;
                }

                #ticket_tab .flex-row {
                    display: -webkit-box;
                    display: -ms-flexbox;
                    display: flex;
                    -webkit-flex-wrap: wrap;
                    -ms-flex-wrap: wrap;
                    flex-wrap: wrap;
                }

                #ticket_tab div.row {
                    padding-top: 0;
                }


                .pnr-toggle-btn-wrap {
                    font-size: 13px;
                    position: absolute;
                    right: 15px;
                    z-index: 999;
                    background-color: #000;
                    color: #fff;
                    width: 8px;
                    height: 32px;
                    text-align: center;
                    line-height: 28px;
                    border-radius: 40px 0 0px 40px;
                    top: 10px;
                }

                @media(max-width:1199px) {
                    #RightPanel {
                        margin-top: 54px;
                        position: absolute;
                        top: 0;
                        right: 10px;
                        max-width: 100%;
                    }

                        #RightPanel:before {
                            content: '';
                            position: fixed;
                            width: 100%;
                            height: 100%;
                            background-color: rgba(0,0,0,.4);
                            top: 0;
                            left: 0;
                            right: 0;
                            bottom: 0;
                        }

                    #hidePanel .fa:before {
                        content: "\f104";
                    }

                    .expanded-left-panel #hidePanel .fa:before {
                        content: '\f00d';
                    }

                    #RightPanel .main_panel {
                        background-color: #fff;
                    }

                    .pnr-toggle-btn-wrap {
                        right: 18px;
                        width: auto;
                        height: auto;
                        line-height: 20px;
                        border-radius: 50%;
                        top: 7px;
                    }

                        .pnr-toggle-btn-wrap a {
                            width: 20px;
                            height: 20px;
                            display: block;
                        }
                }

                #RightPanel {
                    display: none;
                    padding: 0;
                }

                .expanded-left-panel #RightPanel {
                    display: block;
                }

                .expanded-left-panel .pnr-toggle-btn-wrap .fa-angle-left:before {
                    content: "\f105";
                }

                .showParamToggle[aria-expanded="true"] .fa-plus-square:before {
                    content: "\f146";
                }


                @media(min-width:1199px) {

                    .pnr-toggle-btn-wrap {
                        top: 44%;
                    }

                    #ticket_tab .col-md-1, #ticket_tab .col-md-2, #ticket_tab .col-md-3 {
                        /*max-width: 130px;*/
                    }

                    .col {
                        max-width: 20%;
                    }

                    .col2 {
                        max-width: 100%;
                    }

                    .expanded-left-panel .col2 {
                        max-width: 100%;
                        width: 100%;
                    }

                    .expa .col-container {
                        display: -webkit-box;
                        display: -ms-flexbox;
                        display: flex;
                    }
                }

                #RightPanel .form-control.select2-container, #LeftPanel .form-control.select2-container {
                    height: 30px !important;
                }


                #RightPanel .select2-container .select2-choice, #LeftPanel .select2-container .select2-choice {
                    height: 28px !important;
                    line-height: 30px !important;
                }

                .modal-header {
                    background: #636363;
                }

                [aria-expanded="true"] .expand-details .fa.fa-plus-circle:before {
                    content: "\f056";
                }

                .form-control::-webkit-input-placeholder {
                    color: lightgray;
                }

                .form-control::-moz-placeholder {
                    color: lightgray;
                }

                .form-control:-ms-input-placeholder {
                    color: lightgray;
                }

                .disabled {
                    color: #ccc;
                    pointer-events: none;
                }
            </style>
            <div class="cz-container">
                <script type="text/javascript">
                     var selectedAgentBalance = 0;
                    function showpopup() {
                        $('#FareDiff').modal('show');
                        $('#showParam').collapse('hide');
                        document.getElementById('<%=hdnIsFromEdit.ClientID%>').value = "false";

                    }



                    function DisableSectorsList() {
                        document.getElementById('<%=ddldestination.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector1.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector2.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector3.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector4.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector5.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector6.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector7.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector8.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector9.ClientID%>').disabled = true;
                        document.getElementById('<%=ddlSector10.ClientID%>').disabled = true;
                    }
                    function ReloadSelectedTab() {
                        var selectedTab = document.getElementById('<%=hdnSelectedTabName.ClientID%>').value;
                        if (selectedTab == "TicketTab") {
                            $('#TicketTab').addClass('active');
                            $("#TicketTab").trigger('click');
                            document.getElementById('<%=btnprev.ClientID%>').style.display = 'none';
                            document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                            return;

                        }
                        if (selectedTab == "sectortab") {
                            $('#sectortab').addClass('active');
                            $("#sectortab").trigger('click');
                            document.getElementById('<%=btnprev.ClientID%>').style.display = 'block';
                            document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                            return;

                        }
                        if (selectedTab == "UDIDtab") {
                            $('#UDIDtab').addClass('active');
                            $("#UDIDtab").trigger('click');
                            document.getElementById('<%=btnprev.ClientID%>').style.display = 'block';
                            document.getElementById('<%=btnnext.ClientID%>').value = 'Save';
                            return;

                        }
                        else {
                            $('#TicketTab').addClass('active');
                            $("#TicketTab").trigger('click');
                            document.getElementById('ctl00_cphTransaction_btnprev').style.display = 'none';
                        }
                    }

                    function navigateTab(tabIndex, event) {

                        if (tabIndex == "1") {
                            document.getElementById('<%=btnprev.ClientID%>').style.display = 'none';
                            document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                            document.getElementById('<%=btnSave.ClientID%>').style.display = 'none';
                            document.getElementById('<%=btnnext.ClientID%>').value = 'Next';
                            $('#TicketTab').addClass('active');
                            $('#sectortab').removeClass('active');
                            $('#UDIDtab').removeClass('active');
                            $(".tab-content").find('.tab-pane.active').removeClass('active').removeClass('in');
                            $("#ticket_tab").addClass('in').addClass('active');

                        }

                        if (tabIndex == "2") {
                            if (ValidateCtrls(false)) {
                                document.getElementById('<%=btnprev.ClientID%>').style.display = 'block';
                                document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                                document.getElementById('<%=btnSave.ClientID%>').style.display = 'none';
                                document.getElementById('<%=btnnext.ClientID%>').value = 'Next';
                                $('#sectortab').addClass('active');
                                $('#TicketTab').removeClass('active');
                                $('#UDIDtab').removeClass('active');
                                $(".tab-content").find('.tab-pane.active').removeClass('active').removeClass('in');
                                $("#sectors_tab").addClass('in').addClass('active');
                            }
                            else {
                                setTimeout(function () {
                                    $('#sectortab').parent().removeClass('active');
                                }, 200);
                            }
                        }

                        if (tabIndex == "3") {
                            if (ValidateSectorTab()) {
                                document.getElementById('<%=btnprev.ClientID%>').style.display = 'block';
                                document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';

                                document.getElementById('<%=btnnext.ClientID%>').value = 'Save';

                                $('#UDIDtab').addClass('active');
                                $('#sectortab').removeClass('active');
                                $('#TicketTab').removeClass('active');
                                $(".tab-content").find('.tab-pane.active').removeClass('active').removeClass('in');
                                $("#UDID").addClass('in').addClass('active');
                            }
                            else {
                                setTimeout(function () {
                                    $('#UDIDtab').parent().removeClass('active');
                                }, 200)
                            }

                        }
                    }
                    function SetActiveTab() {

                        if ($('#UDIDtab').hasClass('active')) {
                            $('#UDIDtab').removeClass('active');

                            $('#TicketTab').addClass('active');
                            $("#TicketTab").trigger('click');

                        }
                    }

                    function navigateNextTab() {
                        $('#showParam').collapse('hide');

                        if (ValidateCtrls(false)) {
                            if ($("#chkDraftmode").prop('checked')) {
                                $("#<%=isEditable.ClientID%>").val("1");
                            }
                            else {
                               $("#<%=isEditable.ClientID%>").val("0");
                            }
                            if ($('#sectortab').hasClass('active')) {

                                if (ValidateSectorTab()) {
                                    $('#sectortab').removeClass('active');
                                    $("#TicketTab").removeClass('active');
                                    $("#UDIDtab").addClass('active');
                                    $("#UDIDtab").trigger('click');
                                    document.getElementById('<%=hdnSelectedTabName.ClientID%>').value = 'UDIDtab';
                                document.getElementById('<%=btnnext.ClientID%>').style.display = 'none';
                                document.getElementById('<%=btnSave.ClientID%>').style.display = 'block';
                            }
                        }

                        if ($('#TicketTab').hasClass('active')) {

                            $('#TicketTab').removeClass('active');
                            if (document.getElementById('sectortab').style.display != 'none') {
                                $('#sectortab').addClass('active');
                                $("#sectortab").trigger('click');
                                document.getElementById('<%=btnprev.ClientID%>').style.display = 'block';
                                document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                                    $('#UDIDtab').removeClass('active');

                                }
                            }
                        }
                        return false;
                    }                 

                    function navigatePrevTab() {

                        if ($('#UDIDtab').hasClass('active')) {
                            $('#UDIDtab').removeClass('active');
                            $('#sectortab').addClass('active');
                            $("#sectortab").trigger('click');
                            $("#UDIDtab").removeClass('active');
                            document.getElementById('<%=btnprev.ClientID%>').style.display = 'block';
                            document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                            document.getElementById('<%=btnSave.ClientID%>').style.display = 'none';
                            document.getElementById('<%=hdnSelectedTabName.ClientID%>').value = 'sectortab';

                        }
                        else if ($('#sectortab').hasClass('active')) {
                            $('#sectortab').removeClass('active');
                            $('#TicketTab').addClass('active');
                            $("#UDIDtab").removeClass('active');
                            $("#TicketTab").trigger('click');
                            document.getElementById('<%=btnnext.ClientID%>').style.display = 'block';
                            //document.getElementById('<%=btnnext.ClientID%>').value = 'Next';
                            document.getElementById('<%=btnprev.ClientID%>').style.display = 'none';
                            document.getElementById('<%=hdnSelectedTabName.ClientID%>').value = 'TicketTab';
                        }
                    }    

                    var Ajax;
                    if (window.XMLHttpRequest) {
                        Ajax = new XMLHttpRequest();
                    }
                    else {
                        Ajax = new ActiveXObject("Microsoft.XMLHTTP");
                    }                    

                    function bindRepAirline() {

                        var ddlairline = document.getElementById('<%=ddlairline.ClientID%>');

                        if (ddlairline.value != "-1") {
                            var val = document.getElementById('<%=ddlairline.ClientID%>').value;
                            $('#<%=ddlRepAirline.ClientID%>').select2('val', val);
                        }

                        if (ddlairline.value == '15' || ddlairline.value == '16') {
                            document.getElementById('<%=txtClassSector.ClientID%>').value = 'Y';
                            $('#<%=ddldestination.ClientID%>').select2('val', 'MCT');
                            BindSectors();
                            $('#<%=ddlSector1.ClientID%>').select2('val', 'SHJ');
                            $('#<%=ddlSector2.ClientID%>').select2('val', 'MCT');
                            $('#<%=ddlSector3.ClientID%>').select2('val', 'SHJ');
                            document.getElementById('<%=hndsegmentsCount.ClientID%>').value = '3';
                        }
                    }

                   

                    

                    function ShowSectorTable(number) {
                        if (eval(number) > 0) {
                            document.getElementById('sectortab').style.display = "block";
                        }
                        else
                            document.getElementById('sectortab').style.display = "none";
                        for (var i = 1; i <= number; i++) {

                            document.getElementById('ctl00_cphTransaction_chkIsRefund' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_ddlFromSector' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_ddlToSector' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_txtFlightN0' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').style.display = "block";
                            //if (i == 1) {
                            //    document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value = document.getElementById('ctl00_cphTransaction_txtTravelDt').value;
                            //}
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').style.display = "block";
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').style.display = "block";
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').style.display = "block";
                            document.getElementById('ctl00_cphTransaction_txtFareBasis' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_txtClass' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_txtDuration' + i).style.display = "block";
                            $('span').show();
                        }

                        for (var i = eval(number) + 1; i <= 20; i++) {
                            ////ctl00_cphTransaction_DepartureDt1_Date
                            //document.getElementById('ctl00_cphTransaction_row'+i).style.display = 'none';

                            document.getElementById('ctl00_cphTransaction_chkIsRefund' + i).style.display = 'none';
                            var DeptDt = document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date');
                            DeptDt.style.display = 'none';  //.style.display = 'none';
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').style.display = 'none';
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').style.display = 'none';
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').style.display = 'none';
                        }

                    }

                    function disableSectors() {
                        if (document.getElementById('<%=ddldestination.ClientID%>').value == "-1") {
                            document.getElementById('<%=ddlSector1.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector2.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector3.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector4.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector5.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector6.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector7.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector8.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector9.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector10.ClientID%>').disabled = true;

                        }
                        else {
                            document.getElementById('<%=ddlSector1.ClientID%>').disabled = false;
                            document.getElementById('<%=ddlSector2.ClientID%>').disabled = false;
                            document.getElementById('<%=ddlSector3.ClientID%>').disabled = false;
                            document.getElementById('<%=ddlSector4.ClientID%>').disabled = false;
                            document.getElementById('<%=ddlSector5.ClientID%>').disabled = false;
                        }
                        if (document.getElementById('<%=ddlSector5.ClientID%>').value != "-1") {
                            document.getElementById('<%=ddlSector6.ClientID%>').disabled = false;
                            document.getElementById('<%=ddlSector7.ClientID%>').disabled = false;
                            document.getElementById('<%=ddlSector7.ClientID%>').disabled = false;
                            document.getElementById('<%=ddlSector7.ClientID%>').disabled = false;
                            document.getElementById('<%=ddlSector7.ClientID%>').disabled = false;
                        }
                        else {
                            document.getElementById('<%=ddlSector6.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector7.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector8.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector9.ClientID%>').disabled = true;
                            document.getElementById('<%=ddlSector10.ClientID%>').disabled = true;
                        }
                    }

                    function Check(id) {
                        var val = document.getElementById(id).value;
                        if (val == '0.00') {
                            document.getElementById(id).value = '';
                        }
                    }

                    function Set(id) {
                        var val = document.getElementById(id).value;
                        if (val == '' || val == '0.00' || val == '0') {
                            document.getElementById(id).value = '0.00';
                        }
                        else {
                            var index = val.indexOf('.'); // if val=10.00
                            
                            if (index > 0) {
                                var arr = val.split('.');
                                if (arr[1] == "") {
                                    arr[1] = "00";
                                }
                                val = parseInt(arr[0]) + "." + arr[1];
                            }
                            else if (index = 0)  //if val=.00
                            {
                                val = '0.00';
                            }
                            else             //if val=010
                            {
                                
                                val = val.replace(/\b0+/g, "");                                
                                val = parseFloat(val).toFixed(2);
                                if (isNaN(val)) {
                                    val = "0.00";
                                }
                                                             
                            }
                            document.getElementById(id).value = val;
                        }
                    }

                    function validateSector(event) {
                        var sectorName = $(event);
                        var passData = "sectorName=" + id;
                        Ajax.onreadystatechange = ShowSectorResult;
                        Ajax.open("POST", "OffLineEntry");
                        Ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                        Ajax.send()

                    }

                    function hidePopUp(index) {
                        document.getElementById('<%=hdnSelectedIndex.ClientID%>').value = index;
                        document.getElementById('<%=hdnIsFromEdit.ClientID%>').value = "true";                        
                        $('#showParam').collapse('hide');
                        getElement('EditPNRDiv').style.display = "block";
                        document.forms[0].submit();
                    }

                    function RemovePaxFromPopUp(index) {
                        if (confirm('Do you want to remove?')) {
                            document.getElementById('<%=hdnRemovePax.ClientID%>').value = index;
                            $('#showParam').collapse('hide');
                            document.forms[0].submit();
                        }
                    }

                    function hidepopupdata() {
                        $('#FareDiff').modal('hide');
                    }

                    function HideSearchOption() {
                        $('#showParam').collapse('hide');
                        $('#TicketTab').addClass('active');
                        document.getElementById('<%=btnprev.ClientID%>').style.display = 'none';
                    }

                    function IsAlphaNumeric(e) {
                        var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                        var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
                        return ret;
                    }

                    var cal1;
                    var cal2;

                    function init() {

                        
                        var today = new Date();
                        // For making dual Calendar use CalendarGroup  for single Month use Calendar     
                        cal1 = new YAHOO.widget.Calendar("cal1", "container1");
                        //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
                        cal1.cfg.setProperty("title", "Select sales date");
                        cal1.cfg.setProperty("close", true);
                        cal1.selectEvent.subscribe(setDates1);
                        cal1.render();

                        cal2 = new YAHOO.widget.Calendar("cal2", "container2");
                        cal2.cfg.setProperty("title", "Select travel date");
                        cal2.selectEvent.subscribe(setDates2);
                        cal2.cfg.setProperty("close", true);
                        cal2.render();
                    }
                    function showCal1() {

                        $('container2').context.styleSheets[0].display = "none";
                        $('container1').context.styleSheets[0].display = "block";
                        init();
                        cal1.show();
                        cal2.hide();
                    }


                    var departureDate = new Date();
                    function showCal2() {
                        var date1 = document.getElementById('<%= txtSalesDate.ClientID%>').value;
                        if (date1 == '') {
                            toastr.error('Please select sales date');
                            return false;
                        }
                        $('container1').context.styleSheets[0].display = "none";
                        cal1.hide();
                        init();    
                        if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                            var depDateArray = date1.split('/');

                            var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                            cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                            cal2.cfg.setProperty("pageDate", depDateArray[0] + "/" + depDateArray[2]);
                            cal2.render();
                        }
                        document.getElementById('container2').style.display = "block";
                    }
                    function setDates1() {
                        var date1 = cal1.getSelectedDates()[0];

                        $('IShimFrame').context.styleSheets[0].display = "none";
                        this.today = new Date();
                        var thisMonth = this.today.getMonth();
                        var thisDay = this.today.getDate();
                        var thisYear = this.today.getFullYear();

                        var todaydate = new Date(thisYear, thisMonth, thisDay);
                        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
                        var difference = (depdate.getTime() - todaydate.getTime());

                        departureDate = cal1.getSelectedDates()[0];
                        document.getElementById('errMess').style.display = "none";
                        document.getElementById('errorMessage').innerHTML = "";
                        //			
                        var month = date1.getMonth() + 1;
                        var day = date1.getDate();

                        if (month.toString().length == 1) {
                            month = "0" + month;
                        }

                        if (day.toString().length == 1) {
                            day = "0" + day;
                        }
                        document.getElementById('<%= txtSalesDate.ClientID %>').value = month + "/" + (day) + "/" + date1.getFullYear();
                        cal1.hide();
                    }

                    function setDates2() {
                        var date1 = document.getElementById('<%=txtSalesDate.ClientID %>').value;
                        if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "First select sales date.";
                            return false;
                        }

                        var date2 = cal2.getSelectedDates()[0];
                        var depDateArray = date1.split('/');
                        if (!CheckValidDate(depDateArray[1], depDateArray[0], depDateArray[2])) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                            return false;
                        }
                        document.getElementById('errMess').style.display = "none";
                        document.getElementById('errorMessage').innerHTML = "";

                        // Note: Date()	for javascript take months from 0 to 11
                        var depdate = new Date(depDateArray[2], depDateArray[0] - 1, depDateArray[1]);
                        var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
                        var difference = returndate.getTime() - depdate.getTime();
                        document.getElementById('errMess').style.display = "none";
                        document.getElementById('errorMessage').innerHTML = "";

                        var month = date2.getMonth() + 1;
                        var day = date2.getDate();

                        if (month.toString().length == 1) {
                            month = "0" + month;
                        }

                        if (day.toString().length == 1) {
                            day = "0" + day;
                        }


                        document.getElementById('<%= txtTravelDt.ClientID %>').value = month + "/" + (day) + "/" + date2.getFullYear();
                        cal2.hide();
                    }
                    YAHOO.util.Event.addListener(window, "load", init);

                    function CheckPNRValid() {
                        if (document.getElementById('<%=txtPNRNum.ClientID %>').value.trim().length > 0)
                            document.getElementById('PNR').style.display = 'none';
                    }
                    function CheckTicketNoValid() {
                        var txtNumber = document.getElementById('<%=txtNumber.ClientID%>');
                    if (txtNumber.value.trim().length > 0) {

                        document.getElementById('Number').style.display = 'none';
                        if (txtNumber.value.trim().length != 10) {
                            document.getElementById('<%=txtNumber.ClientID%>').value = '';
                                txtNumber.value = '';
                                txtNumber.focus();
                                toastr.error('Ticket number should be 10 character length.');
                            }
                        }
                    }

                    function CheckSalesDtValid() {
                        var date1 = document.getElementById('<%=txtSalesDate.ClientID %>').value;
                        if (date1.length > 0) {
                            document.getElementById('errMess').style.display = "none";
                        }
                    }
                    function CheckTravelDtValid() {
                        var date1 = document.getElementById('<%=txtTravelDt.ClientID %>').value;
                        if (date1.length > 0) {
                            document.getElementById('traveldate').style.display = "none";
                        }
                    }
                    function CheckPaxNameValid() {
                        if (document.getElementById('paxtxtPaxName').value.indexOf('/') != -1 || document.getElementById('paxtxtPaxName').value.length > 0) {
                            document.getElementById('paxnamemsg').style.display = 'none';
                        }
                    }

                    function CheckClassValid() {
                        if (document.getElementById('<%=txtClassSector.ClientID %>').value.length > 0) {
                            document.getElementById('classerr').style.display = 'none';
                        }
                    }

                    function CheckSellingfareValid() {
                        if (document.getElementById('<%=txtSellingFare.ClientID %>').value.trim().length > 0) {
                            document.getElementById('sellingFare').style.display = 'none';
                        }
                    }

                    function CheckTaxValid() {
                        if (document.getElementById('ctl00_cphTransaction_txtCode0').value.trim().length > 0 || document.getElementById('ctl00_cphTransaction_txtValue0').value.trim().length > 0) {
                            document.getElementById('TaxErrorMsg').style.display = 'none';
                        }
                    }

                    function ValidateCtrls(draftMode) {
                        var isValid = true;
                        var paxDetails = [];
                        $('#showParam').collapse('hide');
                        //document.getElementById('paxname').style.display = 'none';
                        document.getElementById('sellingFare').style.display = 'none';
                        document.getElementById('PNR').style.display = 'none';
                        document.getElementById('AirLine').style.display = 'none';
                        document.getElementById('Number').style.display = 'none';
                        document.getElementById('salesdate').style.display = 'none';
                        document.getElementById('traveldate').style.display = 'none';
                        document.getElementById('locationErr').style.display = 'none';


                       // document.getElementById('TaxErrorMsg').style.display = 'none';
                       // document.getElementById('TrasactionErrormsg').style.display = 'none';
                       // document.getElementById('MarkUPErrormsg').style.display = 'none';
                        //document.getElementById('DiscountDetailsErr').style.display = 'none';
                       // document.getElementById('CommissionErr').style.display = 'none';
                        document.getElementById('classerr').style.display = 'none';

                        document.getElementById('sectors').style.display = 'none';
                       document.getElementById('commision').style.display = 'none';

                        document.getElementById('SectorerrorMessage').style.display = 'none';
                        document.getElementById('destination').style.display = 'none';

                        if ($("#ctl00_cphTransaction_hdnPaxDetails").val() == "") {
                            toastr.error('Please enter pax details');
                            isValid = false;
                        }

                        else {
                            paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                            if (paxDetails.length == 0) {
                                toastr.error('Please enter pax details');
                                isValid = false;
                            }
                            else {
                                var AdultCount = [];
                                var infantCount = [];
                                var childCount = [];
                                if (draftMode == false) {
                                    AdultCount = $.grep(paxDetails, function (val, ind) {
                                        return val.Type == "1";
                                    });
                                    childCount = $.grep(paxDetails, function (val, ind) {
                                        return val.Type == "2";
                                    });
                                    infantCount = $.grep(paxDetails, function (val, ind) {
                                        return val.Type == "3";
                                    });

                                    if (parseInt(AdultCount.length) != parseInt($("#ctl00_cphTransaction_ddlAdultCount").val())) {
                                        toastr.error('Please check adult pax count');
                                        isValid = false;
                                    }
                                    if (parseInt(childCount.length) != parseInt($("#ctl00_cphTransaction_ddlChildCount").val())) {
                                        toastr.error('Please check child pax count');
                                        isValid = false;
                                    }
                                    if (parseInt(infantCount.length) != parseInt($("#ctl00_cphTransaction_ddlInfantCount").val())) {
                                        toastr.error('Please check infant pax count');
                                        isValid = false;
                                    }
                                }

                                if ($("#ctl00_cphTransaction_ddlTransactionType").val() == "16") {
                                    var isCancelltioncharge = true;
                                    $.each(paxDetails, function (paxInd, paxval) {
                                        var cancelSum = parseFloat(parseFloat(paxval.Price.AgentCancellationCharge) + parseFloat(paxval.Price.SupplierCancellationCharge)); 
                                        if (cancelSum <= 0) {
                                            isCancelltioncharge = false;                                           
                                        }
                                    });
                                    if (!isCancelltioncharge) {
                                        toastr.error('Please enter cancellation charge');
                                        isValid = false;
                                    }
                                }
                            }                           
                            
                        } 

                        if (paxDetails.length > 0 && draftMode == false) {
                            var isticketNo = true;
                            $.each(paxDetails, function (paxInd, paxval) {                                
                                if (paxval.TicketNumber=="") {
                                    isticketNo = false;
                                }
                                
                            });
                            if (!isticketNo) {
                                toastr.error('Please enter ticket number for the pax');
                                isValid = false;
                            }
                        }
                        if (paxDetails.length > 0 && $("#ctl00_cphTransaction_ddlSaleType").val()=="N") {
                            var isSellingFare = true;
                            var isTax = true;
                            var isHandling = true;
                            $.each(paxDetails, function (paxInd, paxval) {
                                if (parseFloat(paxval.Price.PublishedFare) ==0) {
                                    isSellingFare = false;
                                }
                                if (paxval.TaxBreakUp.length == 0) {
                                    isTax = false;
                                }
                                if (paxval.HandlingId == "0") {
                                    isHandling = false;
                                }
                            });
                            if (!isSellingFare) {
                                toastr.error('Please enter passenger selling fare');
                                isValid = false;
                            }
                            if (!isTax) {
                                toastr.error('Please enter passenger tax details');
                                isValid = false;
                            }
                            if (!isHandling) {
                                toastr.error('Please enter passenger handling fee');
                                isValid = false;
                            }
                        }
                        if (document.getElementById('<%=ddldestination.ClientID%>').value == "-1") {
                            toastr.error('Please Select Destination');
                            $('#<%=ddldestination.ClientID%>').parent().addClass('form-text-error');
                            isValid = false;
                        }

                        if (document.getElementById('<%=txtClassSector.ClientID%>').value.trim().length <= 0 && draftMode==false) {
                            toastr.error('Please Enter class');
                            $('#<%=txtClassSector.ClientID%>').addClass('form-text-error');
                            isValid = false;
                        }                  

                        if (document.getElementById('<%=rbtnSelf.ClientID %>').checked == true
                            && document.getElementById('<%=ddlCustomer1.ClientID%>').value == "-1"
                            && document.getElementById('<%=ddlMode.ClientID%>').value == "3") {
                            toastr.error('Please Select Customer');
                            $('#<%=ddlCustomer1.ClientID%>').parent().addClass('form-text-error');
                            isValid = false;
                        }
                        else {
                            $('#<%=ddlCustomer1.ClientID%>').parent().removeClass('form-text-error');
                        }

                        
                        if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true
                            && document.getElementById('<%=ddlAgent.ClientID %>').value != "-1"
                            && (document.getElementById('<%=hndLocationId.ClientID%>').value == "-1"
                                || document.getElementById('<%=hndLocationId.ClientID%>').value == ""
                                || document.getElementById('<%=hndLocationId.ClientID%>').value == "0")) {

                            toastr.error('Please Select Location');
                            $('#<%=ddlConsultant.ClientID%>').addClass('form-text-error');
                            isValid = false;

                        }
                        if (document.getElementById('<%=txtPNRNum.ClientID %>').value.trim().length <= 0) {
                            toastr.error('Please enter PNR');
                            $('#<%=txtPNRNum.ClientID%>').addClass('form-text-error');
                            isValid = false;
                        }
                        if (document.getElementById('<%=txtPNRNum.ClientID %>').value.trim().length > 0) {
                            if (document.getElementById('<%=txtPNRNum.ClientID %>').value.trim().length < 6) {
                                toastr.error('PNR Number minimum 6 charectors');
                                $('#<%=txtPNRNum.ClientID%>').addClass('form-text-error');
                                isValid = false;
                            }
                        }
                        if (document.getElementById('<%=ddlairline.ClientID %>').value == "-1") {

                            toastr.error('Please select AirLine');
                            $('#<%=ddlairline.ClientID%>').parent().addClass('form-text-error');
                            isValid = false;
                        }
                        if (document.getElementById('<%=ddlSupplier.ClientID %>').value == "-1" && draftMode == false) {

                            toastr.error('Please select Supplier');
                            $('#<%=ddlSupplier.ClientID%>').parent().addClass('form-text-error');
                            isValid = false;
                        }
                        if (document.getElementById('<%=txtSalesDate.ClientID %>').value.trim().length <= 0) {
                            toastr.error('Please enter salesdate');
                            $('#<%=txtSalesDate.ClientID%>').addClass('form-text-error');
                            isValid = false;
                        }
                        if (document.getElementById('<%=txtTravelDt.ClientID %>').value.trim().length <= 0) {
                            toastr.error('Please enter traveldate');
                            $('#<%=txtTravelDt.ClientID%>').addClass('form-text-error');
                            isValid = false;
                        }    

                        if (document.getElementById('<%=ddlSector6.ClientID%>').value != "" && document.getElementById('<%=ddlSector6.ClientID%>').value != "-1") {
                            if (document.getElementById('<%=txtConjPNR.ClientID %>').value.trim().length > 0) {
                                if (document.getElementById('<%=txtConjPNR.ClientID %>').value.trim().length < 6) {

                                    toastr.error('Conjection PNR Number minimum 6 charectors');
                                    $('#<%=txtConjPNR.ClientID%>').addClass('form-text-error');
                                    isValid = false;
                                }
                            }
                            if (document.getElementById('<%=txtConjPNR.ClientID %>').value.trim().length <= 0) {
                                toastr.error('Please enter Conjection PNR Number');
                                $('#<%=txtConjPNR.ClientID%>').addClass('form-text-error');
                                isValid = false;
                            }
                        }
                        if (document.getElementById('<%=ddlSector6.ClientID%>').value != "" && document.getElementById('<%=ddlSector6.ClientID%>').value != "-1") {
                            if (document.getElementById('<%=txtCongTicketNo.ClientID %>').value.trim().length > 0) {
                                if (document.getElementById('<%=txtCongTicketNo.ClientID %>').value.trim().length < 6) {

                                    toastr.error('Conjection Ticket Number minimum 6 charectors');
                                    $('#<%=txtCongTicketNo.ClientID%>').addClass('form-text-error');
                                    isValid = false;
                                }
                            }
                            if (document.getElementById('<%=txtCongTicketNo.ClientID %>').value.trim().length <= 0) {
                                toastr.error('Please enter Conjection Ticket Number');
                                $('#<%=txtCongTicketNo.ClientID%>').addClass('form-text-error');
                                isValid = false;
                            }
                        }

                        if (document.getElementById('<%=hndLocationId.ClientID %>').value == "-1"
                            || document.getElementById('<%=hndLocationId.ClientID%>').value == ""
                            || document.getElementById('<%=hndLocationId.ClientID%>').value == "0") {
                            toastr.error('Please Select Location');
                            $('#<%=ddlConsultant.ClientID%>').addClass('form-text-error');
                            isValid = false;
                        }
                        
                        var totalSectors = 0;
                        
                        if (isValid) {
                            for (var i = 1; i <= 10; i++)
                            {
                                if (i != 10) {
                                    if ($("#ctl00_cphTransaction_ddlSector" + parseInt(parseInt(i) + 1) + "").val() != "-1"
                                        && $("#ctl00_cphTransaction_ddlSector" + i + "").val() == "-1"
                                        && $("#ctl00_cphTransaction_ddlSector" + parseInt(parseInt(i) + 1) + "").val() != null) {
                                        isValid = false;
                                        toastr.error('Please select Sector-' + i + '');
                                        break;
                                    }
                                }
                                if ($("#ctl00_cphTransaction_ddlSector" + i + "").val() != "-1" &&
                                    $("#ctl00_cphTransaction_ddlSector" + i + "").val() != null) {
                                    totalSectors++;
                                }
                                
                            }
                        } 
                        
                        if (isValid) {
                            isValid = CheckDestinationExistInSectors();
                        }

                        var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                        var currentSum = 0;
                        var cardCharge = 0;
                        var collecedAmt = 0;   
                        var collectedAmount = 0;
                        
                        if (ExistingSettlement != null && ExistingSettlement != "") {
                            let ExistingJson = JSON.parse(ExistingSettlement);
                            $.grep(ExistingJson.SettlementObject, function (item) {
                                currentSum = parseFloat(parseFloat(currentSum) + parseFloat(item.Amount));
                                if (item.SettlementName == "3") {
                                    cardCharge = parseFloat(parseFloat(cardCharge) + parseFloat(item.FOPDetail3));

                                }
                            });
                            currentSum = parseFloat(parseFloat(currentSum) + parseFloat(cardCharge));
                            collectedAmount = parseFloat($("#ctl00_cphTransaction_txtCollected").val()).toFixed(decimalpoint);
                            collectedAmount = eval(collectedAmount) + eval(cardCharge);

                            if (isNaN(collectedAmount)) {
                                collectedAmount = 0.00;
                            }

                            collecedAmt = collectedAmount;                            
                            if (parseFloat(currentSum.toFixed(decimalpoint)) != parseFloat(collectedAmount.toFixed(decimalpoint))) {
                                isValid = false;
                                toastr.error('Please check settlement amount and collected amount');

                            }
                        }

                        else {  

                            if ($("#ctl00_cphTransaction_ddlSaleType").val() == "N") {
                                isValid = false;
                                toastr.error('Please check settlement amount and collected amount');
                            }
                            else {
                                collectedAmount = parseFloat($("#ctl00_cphTransaction_txtCollected").val()).toFixed(decimalpoint);
                                if (isNaN(collectedAmount)) {
                                    collectedAmount = 0.00;
                                }
                                if (collectedAmount > 0) {
                                    if (ExistingSettlement != null && ExistingSettlement != "") {
                                        let ExistingJson = JSON.parse(ExistingSettlement);
                                        $.grep(ExistingJson.SettlementObject, function (item) {
                                            currentSum = parseFloat(parseFloat(currentSum) + parseFloat(item.Amount));
                                            if (item.SettlementName == "3") {
                                                cardCharge = parseFloat(parseFloat(cardCharge) + parseFloat(item.FOPDetail3));

                                            }
                                        });
                                        currentSum = parseFloat(parseFloat(currentSum) + parseFloat(cardCharge));
                                        collectedAmount = eval(collectedAmount) + eval(cardCharge);
                                        collecedAmt = collectedAmount;
                                        if (parseFloat(currentSum.toFixed(decimalpoint)) != parseFloat(collectedAmount.toFixed(decimalpoint))) {
                                            isValid = false;
                                            toastr.error('Please check settlement amount and collected amount');

                                        }
                                    }
                                    else {
                                        isValid = false;
                                        toastr.error('Please check settlement amount and collected amount');
                                    }
                                }
                            }
                        }

                        if (isValid && !$('#sectortab').hasClass('active')) {
                            BindSectorTab();
                        }

                        if (document.getElementById('divCorporate').style.display == 'block') {
                            if ($("#ddlProfiles").val() == 0) {
                                toastr.error('Please Select Profile');
                                $("#ddlProfiles").addClass('form-text-error');
                                isValid = false;
                            }
                            if ($("#ddlTravelReason").val() == 0) {
                                toastr.error('Please select Reason for Travel');
                                $("#ddlTravelReason").addClass('form-text-error');
                                isValid = false;
                            }
                        }                       
                        
                        if ($("#ctl00_cphTransaction_ddlServiceType").val() == "-1") {
                            toastr.error('Please select service type');
                            isValid = false;
                        }

                        if ($("#ctl00_cphTransaction_ddlTransactionType").val() == "-1") {
                            toastr.error('Please select transaction type');
                            isValid = false;
                        }

                        if ($("#ctl00_cphTransaction_ddlTransactionType").val() == "16") {
                            if ($("#ctl00_cphTransaction_txtAgentCancellationAmt").val() == "" || isNumber($("#ctl00_cphTransaction_txtAgentCancellationAmt").val()) != true) {
                                toastr.error('Please enter agent cancell amount');
                                isValid = false;
                            }
                            if ($("#ctl00_cphTransaction_txtSupplierCancelationAmt").val() == "" || isNumber($("#ctl00_cphTransaction_txtSupplierCancelationAmt").val()) != true) {
                                toastr.error('Please enter agent cancell amount');
                                isValid = false;
                            }
                        }

                        //if ($("#ctl00_cphTransaction_handlingFee").val() == 0 && $("#ctl00_cphTransaction_ddlTransactionType").val()!="16") {
                        //    toastr.error('Please select handling fee element');
                        //    isValid = false;
                        //}

                        if ($("#ctl00_cphTransaction_txtPhone").val() == "") {
                            toastr.error('Please enter phone number');
                            isValid = false;
                        }
                        //if (parseFloat($("#ctl00_cphTransaction_txtSellingFare").val()) == 0) {
                        //    toastr.error('Please enter selling fare');
                        //    isValid = false;
                        //}
                        if ($("#ctl00_cphTransaction_txtEmail").val() == "") {
                            toastr.error('Please enter email');
                            isValid = false;
                        }                        
                        
                        if (parseFloat(collecedAmt) > selectedAgentBalance && $("#ctl00_cphTransaction_ddlTransactionType").val()!="16") {
                            alert("Credit Limit Amount Exceeded");
                        }
                        return isValid;
                    }

                    function isNumber(searchValue) {
                        var found = searchValue.search(/^(\d*\.?\d*)$/);
                        //Change to ^(\d*\.?\d+)$ if you don't want the number to end with a . such as 2.
                        //Currently validates .2, 0.2, 2.0 and 2.
                        if (found > -1) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }
                    function ValidateSectorTab1() {
                        var isValid = true;
                        if (document.getElementById('<%=hndsegmentsCount.ClientID%>').value.trim().length > 0 && isValid == true) {
                            var isSectorValid = true;
                            var segmentlength = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                            if (segmentlength == "0") {

                                toastr.error('Please Enter sector Details');
                                $('#<%=hndsegmentsCount.ClientID%>').addClass('form-text-error');
                                document.getElementById('sectortab').click();
                                isValid = false;

                            }

                            for (var i = 1; i <= segmentlength; i++) {
                                if (document.getElementById('s2id_ctl00_cphTransaction_ddlFromSector' + i).innerText.trim() == "Select") {
                                    document.getElementById('SectorerrorMessage').style.display = 'block';
                                    document.getElementById('s2id_ctl00_cphTransaction_ddlFromSector' + i).focus();
                                    toastr.error('Please select From Sector');

                                    $('#s2id_ctl00_cphTransaction_ddlFromSector' + i).parent().addClass('form-text-error');
                                    isSectorValid = false;
                                    document.getElementById('sectortab').click();
                                    isValid = false;
                                }
                                if (document.getElementById('s2id_ctl00_cphTransaction_ddlToSector' + i).innerText.trim() == "Select") {
                                    document.getElementById('SectorerrorMessage').style.display = 'block';
                                    document.getElementById('s2id_ctl00_cphTransaction_ddlToSector' + i).focus();
                                    toastr.error('Please select To Sector');

                                    $('#s2id_ctl00_cphTransaction_ddlToSector' + i).addClass('form-text-error');
                                    document.getElementById('sectortab').click();
                                    isValid = false;

                                }
                                if (document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).value.trim().length == 0) {
                                    document.getElementById('SectorerrorMessage').style.display = 'block';
                                    document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).focus();
                                    toastr.error('Please enter Flying Carrier');

                                    $('#ctl00_cphTransaction_txtFlyingCarrier' + i).addClass('form-text-error');
                                    $('#ctl00_cphTransaction_txtFlyingCarrier' + i).focus();
                                    document.getElementById('sectortab').click();
                                    isValid = false;
                                }
                                if (document.getElementById('ctl00_cphTransaction_txtFlightN0' + i).value.trim().length == 0) {
                                    document.getElementById('SectorerrorMessage').style.display = 'block';
                                    toastr.error('Please enter Flight No');

                                    $('#ctl00_cphTransaction_txtFlightN0' + i).addClass('form-text-error');
                                    document.getElementById('sectortab').click();
                                    isValid = false;
                                }

                                if (document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value.trim().length == 0) {
                                    document.getElementById('SectorerrorMessage').style.display = 'block';
                                    toastr.error('Please enter DeptDate(DD/MM/YYYY)');

                                    $('#ctl00_cphTransaction_DepartureDt' + i + '_Date').addClass('form-text-error');
                                    document.getElementById('sectortab').click();
                                    isValid = false;
                                }

                                if (document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value != "" && document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').value != "") {
                                    var departureDt = GetDateTimeObject('ctl00_cphTransaction_DepartureDt' + i);
                                    var arrivalDt = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + i);

                                    if ((departureDt != null && arrivalDt != null) && departureDt > arrivalDt) {
                                        alert('Departure Date should not be greater than Arrival Date!');
                                        return isValid = false;
                                    }
                                    if (i == 1) {
                                        var SectorMonth = "";
                                        var sectorDate = "";
                                        var date = new Date(departureDt);
                                        if ((date.getMonth() + 1) < 10)
                                            SectorMonth = "0" + (date.getMonth() + 1);
                                        else
                                            SectorMonth = date.getMonth() + 1;

                                        if (date.getDate() < 10) {
                                            sectorDate = "0" + (date.getDate());
                                        }
                                        else
                                            sectorDate = date.getDate();

                                        var sectordeptDate = SectorMonth + '/' + sectorDate + '/' + date.getFullYear();
                                        var traveldate = document.getElementById('<%=txtTravelDt.ClientID%>').value;
                                        alert(sectordeptDate + '-----------' + traveldate);
                                        if (sectordeptDate != traveldate) {
                                            alert('Departure Date should be equal to Travel Date');
                                            return isValid = false;
                                        }
                                    }
                                    else {
                                        var id = i - 1;
                                        var arrivalDatePreviousSector = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + id);
                                        if (arrivalDatePreviousSector != null && departureDt <= arrivalDatePreviousSector) {
                                            alert('Departure Date should be greater than the Previous Sector Arrival Date & Time');
                                            return isValid = false;
                                        }
                                    }
                                }

                                if (document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').value.trim().length == 0) {
                                    document.getElementById('SectorerrorMessage').style.display = 'block';
                                    toastr.error('Please Enter Departure Time');

                                    $('#ctl00_cphTransaction_DepartureDt' + i + '_Time').addClass('form-text-error');
                                    document.getElementById('sectortab').click();
                                    isValid = false;
                                }


                                if (document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').value.trim().length == 0) {
                                    document.getElementById('SectorerrorMessage').style.display = 'block';
                                    toastr.error('Please enter Arrival Date(DD/MM/YYYY)');

                                    $('#ctl00_cphTransaction_ArrivalDt' + i + '_Date').addClass('form-text-error');
                                    document.getElementById('sectortab').click();
                                    isValid = false;
                                }

                                if (document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').value.trim().length == 0) {
                                    document.getElementById('SectorerrorMessage').style.display = 'block';
                                    toastr.error('Please Enter Arrival Time');

                                    $('#ctl00_cphTransaction_ArrivalDt' + i + '_Time').addClass('form-text-error');
                                    document.getElementById('sectortab').click();
                                    isValid = false;
                                }

                                if (document.getElementById('ctl00_cphTransaction_txtFareBasis' + i).value.trim().length == 0) {
                                    document.getElementById('SectorerrorMessage').style.display = 'block';
                                    toastr.error('Please Enter Fare Basis');

                                    $('#ctl00_cphTransaction_txtFareBasis' + i).addClass('form-text-error');
                                    document.getElementById('sectortab').click();
                                    isValid = false;
                                }

                                if (document.getElementById('ctl00_cphTransaction_txtClass' + i).value.trim().length == 0) {
                                    document.getElementById('SectorerrorMessage').style.display = 'block';
                                    toastr.error('Please Enter class');

                                    $('#ctl00_cphTransaction_txtClass' + i).addClass('form-text-error');
                                    document.getElementById('sectortab').click();
                                    isValid = false;
                                }

                                if (document.getElementById('ctl00_cphTransaction_txtDuration' + i).value.trim() == 0) {
                                    document.getElementById('SectorerrorMessage').style.display = 'block';
                                    toastr.error('Please Enter Duration');

                                    $('#ctl00_cphTransaction_txtDuration' + i).addClass('form-text-error');
                                    document.getElementById('sectortab').click();
                                    isValid = false;
                                }
                            }

                        }
                        return isValid;
                    }
                    function CheckDestinationExistInSectors() {
                        var sectorsCount = 0;
                        var destvalue = document.getElementById('<%=ddldestination.ClientID%>').value;

                        sectorsCount = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                       
                        var isValid = false;
                        for (var i = sectorsCount; i > 1; i--) {
                            if (isValid)
                                break;
                            document.getElementById('ctl00_cphTransaction_ddlSector' + i).value
                            if (document.getElementById('ctl00_cphTransaction_ddlSector' + i).value != "-1" && document.getElementById('ctl00_cphTransaction_ddlSector' + i).value != "") {
                                if (document.getElementById('ctl00_cphTransaction_ddlSector' + i).value != destvalue)
                                    isValid = false;
                                else
                                    isValid = true;

                            }
                        }
                        if (!isValid)
                            toastr.error('Please make sure destination should be there in one of the sectors');
                        return isValid;
                    }

                    function disablefield() {
                        if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true) {
                            document.getElementById('wrapper').style.display = "block";
                        }
                        else {
                            document.getElementById('wrapper').style.display = "none";
                        }
                    }

                    function Validate() {
                        var msg = "";
                        if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true) {
                            if (document.getElementById('<%=ddlAgent.ClientID %>').selectedIndex <= 0) {
                                msg += "Please select an agent from the list!";
                            }
                        }
                        if (document.getElementById('<%=txtPnr.ClientID %>').value == '' && document.getElementById('<%=ddlSource.ClientID%>').selectedIndex <= 0) {
                            msg += "Please Select source";
                        }
                        if (msg.length != 0) {
                            alert(msg);
                            return false;
                        }
                        else {
                            return true;
                        }
                    }

                    var taxCounter = 0;
                    function EnableTaxAddctrl(isFromRebindCtrls) {
                        var validate = true;
                        var taxCode;
                        var taxValue;
                        var taxcount = eval(document.getElementById('ctl00_cphTransaction_hndTaxCounter').value);
                        if (isFromRebindCtrls)
                            return validate = true;
                        for (var i = 0; i <= eval(taxcount); i++) {
                            if (i == 0) {
                                taxCode = document.getElementById('paxTaxCode' + i);
                                taxValue = document.getElementById('paxTaxAmount' + i);
                            }
                            else {
                                taxCode = document.getElementById('paxTaxCode' + i);
                                taxValue = document.getElementById('paxTaxAmount' + i);
                            }

                            if (taxValue != null && taxValue.value.length == 0) {
                                toastr.error('Please Enter Tax Amount');
                                $('#paxTaxAmount' + i).addClass('form-text-error');
                                validate = false;
                            }

                            if (validate && taxCode.value > 0 && taxCode.length == 0) {
                                toastr.error('Please Enter Tax Code');
                                $('#paxTaxCode' + i).addClass('form-text-error');
                                validate = false;
                            }
                        }
                        return validate;
                    }                   

                    function ClosePopUp(e) {
                        $(this).close();
                    }

                    function allowNumerics(evt) {
                        var charCode = (evt.which) ? evt.which : evt.keyCode;
                        if (charCode != 46 && charCode > 31
                            && (charCode < 48 || charCode > 57))
                            return false;

                        return true;
                    }

                    function isAlpha(e) {
                        var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                        var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || keyCode == 9 || keyCode == 8 || keyCode == 11);
                        //autoCompInit1();
                        return ret;
                    }

                    function changeToUpperCase(obj) {
                        obj.value = obj.value.toUpperCase();
                    }

                    function checkAlphaNumeric(e) {
                        var regex = new RegExp("[a-zA-Z0-9]");
                        var key = e.keyCode || e.which;
                        key = String.fromCharCode(key);
                        if (!regex.test(key)) {
                            e.returnValue = false;
                            if (e.preventDefault) {
                                e.preventDefault();
                            }
                        }
                    }

                    function isAlphaNumeric(e) {

                        var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                        var ret = ((keyCode >= 46 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 9 || keyCode == 8 || keyCode == 11);
                        return ret;
                    }

                    var decimalpoint = eval('<%=agentDecimalPoints %>');

                    var inputVATcharge = 0;
                    var inputVATcostIncluded = "";
                    var outputVATcharge = 0;
                    var outputVATappliedOn = "";
                    
                    var isValid = true;
                    function CheckDuplicateDynamicSectors(e) {
                        if (e.id.split('_')[2] == "ddlFromSector1") {
                            if (document.getElementById('<%=ddlSector1.ClientID%>').value.length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector1.ClientID %>').value.trim() != e.value.trim()) {
                                //document.getElementById('SectorerrorMessage').style.display = 'block';

                                //document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'

                                toastr.error('Sectors Should be Match.');
                                $('#SectorerrorMessage').addClass('form-text-error');



                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }
                    }
                    if (e.id.split('_')[2] == "ddlToSector1") {
                        if (document.getElementById('<%=ddlSector2.ClientID%>').value.length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector2.ClientID %>').value.trim() != e.value.trim()) {
                                //document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'

                                toastr.error('Sectors Should be Match.');
                                $('#SectorerrorMessage').addClass('form-text-error');


                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlFromSector2") {
                        if (document.getElementById('<%=ddlSector2.ClientID %>').value.length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector2.ClientID %>').value.trim() != e.value.trim()) {
                                //document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'


                                toastr.error('Sectors Should be Match.');
                                $('#SectorerrorMessage').addClass('form-text-error');


                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector2") {
                        if (document.getElementById('<%=ddlSector3.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector3.ClientID %>').value.trim() != e.value.trim()) {
                                //document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                toastr.error('Sectors Should be Match.');
                                $('#SectorerrorMessage').addClass('form-text-error');



                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }
                    }
                    if (e.id.split('_')[2] == "ddlFromSector3") {
                        if (document.getElementById('<%=ddlSector3.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector3.ClientID %>').value.trim() != e.value.trim()) {
                                //document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'

                                toastr.error('Sectors Should be Match.');
                                $('#SectorerrorMessage').addClass('form-text-error');


                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector3") {
                        if (document.getElementById('<%=ddlSector4.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector4.ClientID %>').value.trim() != e.value.trim()) {
                                //document.getElementById('SectorerrorMessage').style.display = 'block';
                                //document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'

                                toastr.error('Sectors Should be Match.');
                                $('#SectorerrorMessage').addClass('form-text-error');


                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlFromSector4") {
                        if (document.getElementById('<%=ddlSector4.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector4.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector4") {
                        if (document.getElementById('<%=ddlSector5.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector5.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlFromSector5") {
                        if (document.getElementById('<%=ddlSector5.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector5.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }

                    if (e.id.split('_')[2] == "ddlToSector5") {
                        if (document.getElementById('<%=ddlSector6.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector6.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlFromSector6") {
                        if (document.getElementById('<%=ddlSector6.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector6.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector6") {
                        if (document.getElementById('<%=ddlSector7.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector7.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlFromSector7") {
                        if (document.getElementById('<%=ddlSector7.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector7.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector7") {
                        if (document.getElementById('<%=ddlSector8.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector8.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }

                    if (e.id.split('_')[2] == "ddlFromSector8") {
                        if (document.getElementById('<%=ddlSector8.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector8.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlToSector8") {
                        if (document.getElementById('<%=ddlSector9.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector9.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }
                    if (e.id.split('_')[2] == "ddlFromSector9") {
                        if (document.getElementById('<%=ddlSector9.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector9.ClientID %>').value.trim() != e.value.trim()) {
                                document.getElementById('SectorerrorMessage').style.display = 'block';
                                document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                return isValid = false;
                            }
                            else
                                document.getElementById('SectorerrorMessage').style.display = 'none';
                        }

                    }

                    if (e.id.split('_')[2] == "ddlToSector9") {
                        if (document.getElementById('<%=ddlSector10.ClientID %>').value.trim().length > 0 && e.value.length > 0) {
                            if (document.getElementById('<%=ddlSector10.ClientID %>').value.trim() != e.value.trim()) {
                                    document.getElementById('SectorerrorMessage').style.display = 'block';
                                    document.getElementById('SectorerrorMessage').innerHTML = 'Sectors Should be Match.'
                                    return isValid = false;
                                }
                                else
                                    document.getElementById('SectorerrorMessage').style.display = 'none';
                            }

                        }

                    }

                    function CheckDuplicatesectors(id) {
                        var isDuplicate = true;
                        var originVal = document.getElementById('s2id_ctl00_cphTransaction_ddlSector' + id).innerText.trim();
                        //var controlID = event.controlID;
                        for (var i = 1; i < 10; i++) {
                            if (id == i)
                                continue;
                            if (originVal.length > 0 && document.getElementById('s2id_ctl00_cphTransaction_ddlSector' + i).innerText.trim() != "Select") {
                                if (originVal == document.getElementById('s2id_ctl00_cphTransaction_ddlSector' + i).innerText.trim()) {
                                    //document.getElementById('s2id_ctl00_cphTransaction_ddlSector' + id).value = "-1";
                                    document.getElementById('sectors').style.display = 'block';
                                    document.getElementById('sectors').innerHTML = "Sector Can't be Duplicated";
                                    document.getElementById('s2id_ctl00_cphTransaction_ddlSector' + i).value = "-1";
                                    return isDuplicate = false;
                                }
                                else
                                    document.getElementById('sectors').style.display = 'none';
                            }
                        }
                        //autoCompInit1();
                        //return isDuplicate;
                    }

          <%--  function CheckDuplicateDestinationSector() {
                if (document.getElementById('<%=txtOrigin.ClientID%>').value.trim().length > 0 && document.getElementById('<%=txtDestination.ClientID%>').value.trim().length > 0) {
                    if (document.getElementById('<%=txtOrigin.ClientID%>').value.toLowerCase().trim() == document.getElementById('<%=txtDestination.ClientID%>').value.toLowerCase().trim()) {
                        document.getElementById('<%=txtDestination.ClientID%>').value = "";
                    document.getElementById('<%=txtDestination.ClientID%>').focus();
                    document.getElementById('origin').style.display = 'block';
                    document.getElementById('origin').innerHTML = "Origin And Destination Should not be same";

                }
                else
                    document.getElementById('origin').style.display = 'none';
            }
        }--%>



                    function ReLoadingTaxControls() {
                        try {

                            var segcount = eval(document.getElementById('<%=hndsegmentsCount.ClientID%>').value);
                        if (segcount > 0)
                            ShowSectorTable(segcount);
                        var taxCounter = 0;
                        var taxCount = document.getElementById('<%=hndTaxCounter.ClientID%>').value;
                        var taxDetailsToBind = document.getElementById('<%=hndTaxCodeValue.ClientID%>').value;
                        for (var i = 0; i < eval(taxCount); i++) {
                            var res = taxDetailsToBind.split('|');
                            if (taxDetailsToBind == "") {
                                AddNewRow('', '', true);
                            }
                            if (taxDetailsToBind != "") {
                                var tax = res[i].split('-');
                                if (i == 0 && eval(tax[0]) == 0) {
                                   <%--// document.getElementById('<%=txtCode0.ClientID%>').value = tax[1].split(',')[0];
                                   // document.getElementById('<%=txtValue0.ClientID%>').value = tax[1].split(',')[1];--%>
                                    }
                                    else {
                                        if ((i + 1) == eval(tax[0])) {
                                            AddNewRow(tax[1].split(',')[0], tax[1].split(',')[1], true);
                                        }
                                    }
                                }
                            }
                        }
                        catch (ex) {
                            taxCounter = 0;
                        }
                    }

                    function ReLoadingTaxControlsFromEdit() {
                        taxCounter = 0;

                        var taxCount = document.getElementById('<%=hndTaxCounter.ClientID%>').value;
                    var taxDetailsToBind = document.getElementById('<%=hndTaxCodeValue.ClientID%>').value;

                    for (var i = 0; i <= eval(taxCount); i++) {
                        if (i == 0) {
                            var res = taxDetailsToBind.split('|');
                            var tax = res[i].split('-');
                            if (i == 0 && eval(tax[0]) == 0) {
                              <%--  //document.getElementById('<%=txtCode0.ClientID%>').value = tax[1].split(',')[0];
                                //document.getElementById('<%=txtValue0.ClientID%>').value = tax[1].split(',')[1];--%>
                                }
                            }
                            else {
                                var res = taxDetailsToBind.split('|');
                                if (i != res.length) {
                                    var tax = res[i].split('-');
                                    if (i == eval(tax[0])) {
                                        AddNewRow(tax[1].split(',')[0], tax[1].split(',')[1], true);
                                    }
                                }
                            }
                        }

                    }

                    function ChangeWidthForCalender() {
                        var segmentCount = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                        //if (eval(segmentCount) > 0) {
                        // for (var i = 1; i <= eval(segmentCount) ; i++) {
                        var Deptdate = document.getElementById('ctl00_cphTransaction_dcFromDate_Date');// + i + '_Date');
                        var Depttime = document.getElementById('ctl00_cphTransaction_dcFromDate_Time');// + i + '_Time');
                        //var Arrdate = document.getElementById('ctl00_cphTransaction_txtArrivalDateTime' + i + '_Date');
                        //var Arrtime = document.getElementById('ctl00_cphTransaction_txtArrivalDateTime' + i + '_Time');
                        Deptdate.style['width'] = '100px';
                        Depttime.style['width'] = '50px';
                        //Arrdate.style['width'] = '100px';
                        //Arrtime.style['width'] = '50px';

                        //var depDateArray = Deptdate.value.split('-');
                        //var MonthArray = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "sep", "Oct", "Nov", "Dec"];
                        ////MonthArray[]
                        //var depdate = new Date(depDateArray[2], depDateArray[1]-1, depDateArray[0]); 

                        ////var returndate = new Date(date2.getFullYear(),date2.getMonth(),date2.getDate());  
                        ////var difference = returndate.getTime() - 
                        //dt1 = new Date(2018,08, 05,11,10);
                        //dt1 = new Date(2018, 08, 05, 11, 13);

                        //var diff = (dt2.getTime() - dt1.getTime()) / 1000;
                        //diff /= 60;
                        //alert(Math.abs(Math.round(diff)));

                        //  }

                        //}
                    }

                </script>

                <uc1:DateControl ID="dcLicExpDate" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" Visible="false" style="width: 100px;"></uc1:DateControl>

                <!-- SmartMenus core CSS (required) -->
                <%--  <link href="App_Themes/Pink/BookingStyle.css" rel="stylesheet" />
    <link href="App_Themes/Pink/cozmovisa-style.css" rel="stylesheet" />
    <link href="App_Themes/Pink/Default.css" rel="stylesheet" />
    <link href="App_Themes/Pink/main-style.css" rel="stylesheet" />
    <link href="App_Themes/Pink/style.css" rel="stylesheet" />--%>

                <asp:HiddenField ID="airlineCodeRet" runat="server" />
                <asp:HiddenField ID="airlineNameRet" runat="server" />
                <asp:HiddenField ID="hndTaxCodeValue" runat="server" />
                <asp:HiddenField ID="hdnEditMode" runat="server" Value="" />
                <asp:HiddenField ID="hdnFlightId" runat="server" Value="0" />
                <asp:HiddenField ID="HdnPaxId" runat="server" Value="0" />
                <asp:HiddenField ID="hdnPriceId" runat="server" Value="0" />                
                <asp:HiddenField runat="server" ID="hdnPaxFlexInfo"></asp:HiddenField>
                <asp:HiddenField ID="hdnloginAgentLocations" runat="server" Value="" />


                <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0"></iframe>
                <div id="errMess" class="error_module" style="display: none;">
                    <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                    </div>
                </div>
                <div class="clear" style="margin-left: 25px">
                    <div id="container1" style="position: absolute; top: 120px; left: 250px; display: none; z-index: 9999">
                    </div>
                </div>
                <div class="clear" style="margin-left: 30px">
                    <div id="container2" style="position: absolute; top: 120px; left: 500px; display: none; z-index: 9999">
                    </div>
                </div>
                <div class="clear" style="margin-left: 30px">
                    <div id="container3" style="position: absolute; top: 120px; left: 500px; display: none; z-index: 9999">
                    </div>
                </div>


                <input type="hidden" id="hdnTaxAmt" name="hdnTaxAmt" value="0" />
                <input type="hidden" id="hdnTaxAmounts" name="hdnTaxAmounts" value="0" />
                <input type="hidden" id="hdnSectorList" name="hdnSectorList" value="" runat="server" />
                <input type="hidden" id="hdnSelectedIndex" name="hdnSelectedIndex" runat="server" />
                <input type="hidden" id="hdnIsFromEdit" name="hdnIsFromEdit" runat="server" value="false" />
                <input type="hidden" id="hdnRemovePax" name="hdnRemovePax" runat="server" />
                <input type="hidden" id="hndTaxCounter" name="hdnTaxCounter" runat="server" value="0" />
                <input type="hidden" id="hdnSelectedTabName" name="hdnSelectedTabName" value="" runat="server" />
                <input type="hidden" id="hdnSaveItinerary" name="hdnSaveItinerary" value="" runat="server" />
                <input type="hidden" id="hdnPaxAdded" name="hdnPaxAdded" runat="server" value="false" />

                <asp:HiddenField ID="hdnBookingId" runat="server" Value="0" />

                <asp:HiddenField ID="hndsegmentsCount" runat="server" Value="0" />
                <asp:HiddenField ID="hdnErrorMsg" runat="server" Value="" />
                <asp:HiddenField ID="hdnsegments" runat="server" Value="" />
                <asp:HiddenField ID="hdnAgentId" runat="server" Value="" />
                <asp:HiddenField ID="hdnTravelResonId" runat="server" Value="" />
                <asp:HiddenField ID="hdnprofileId" runat="server" Value="" />
                <asp:HiddenField ID="hdnlocCountryCode" runat="server" Value="" />
                <asp:HiddenField ID="hndLocationId" runat="server" Value="" />
                <div>
                    <div class="body_container">

                        <%-- <span id="ctl00_upnllocation">--%>

                        <script type="text/javascript" lang="javascript">        
</script>

                        <span id="ctl00_cphTransaction_lblError" style="color: Red"></span>
                        <a class="showParamToggle" style="display: none" role="button" data-toggle="collapse" href="#showParam" aria-expanded="true" aria-controls="showParam">
                            <span class="fa fa-plus-square pr-2"></span>Import PNR
                        </a>

                        <div class="collapse in" id="showParam" style="display: none">
                            <div class="paramcon">

                                <div class="row px-3 px-md-0">

                                    <div class="col-md-2 mt-5 pt-1">
                                        <asp:RadioButton ID="rbtnSelf" runat="server" GroupName="book" onchange="disablefield();"
                                            Font-Bold="True" Text="Self" Checked="true" />
                                        &nbsp;
                   
                    <asp:RadioButton ID="rbtnAgent" runat="server" GroupName="book" onchange="disablefield();"
                        Font-Bold="True" Text="Agency" />

                                    </div>
                                    <div class="col-md-4 mt-5 pt-1" id="wrapper" style="display: none">
                                        <div class="form-group">
                                            <asp:UpdatePanel ID="agentbalance" runat="server">
                                                <ContentTemplate>
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <asp:DropDownList runat="server" ID="ddlAgent" CssClass="form-control" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
                                                        </div>
                                                        <asp:Label ID="lblAgentBalance" CssClass="col-md-6" runat="server" Text="" Style="display: none"></asp:Label>
                                                    </div>

                                                </ContentTemplate>
                                            </asp:UpdatePanel>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>PNR No. </label>
                                            <asp:TextBox ID="txtPnr" runat="server" CssClass="form-control" MaxLength="10" onkeypress="return isAlphaNumeric(event);"></asp:TextBox>
                                            <%--
                                        <input name="ctl00$cphTransaction$txtPnr" type="text" id="ctl00_cphTransaction_txtPnr" class="form-control" />
                                            --%>
                                        </div>

                                    </div>

                                    <div class="col-md-2">
                                        <div class="form-group">
                                            <label>Select Source </label>
                                            <asp:DropDownList runat="server" ID="ddlSource" CssClass="form-control"></asp:DropDownList>
                                        </div>

                                    </div>

                                    <div class="col-md-2">

                                        <div class="form-group">
                                            <label>&nbsp; </label>

                                            <div>

                                                <%-- <a class="btn but_b" data-toggle="modal" data-target="#FareDiff">Search</a>
                                                --%>


                                                <asp:Button runat="server" CssClass="btn but_b" ID="btnRetrieve" Text="Retrieve Information" OnClientClick="return Validate();" OnClick="btnRetrieve_Click" />
                                            </div>

                                        </div>

                                    </div>

                                </div>
                                <div class="">
                                </div>

                                <div class="clearfix"></div>
                            </div>
                        </div>
                        <!-- Content Search -->

                        <!-- end search -->

                        <%--            </span>--%>

                        <asp:HiddenField ID="paymentmode" runat="server" />
                        <asp:HiddenField ID="hndId" runat="server" />
                        <div id="EditPNRDiv" style="display: block" runat="server">
                            <div class="col-container pt-4">
                                <!--left panel-->
                                <div id="LeftPanel" class="col2">
                                    <a href="~/OffLineEntry.aspx" style="display: none" class="backToResults" runat="server"><< Back to Search</a>
                                    <div class="right_col CorpTrvl-page">
                                        <div class="row">
                                            <div class="col-md-12 CorpTrvl-tabbed-panel mt-2 mb-0">
                                                <ul class="nav nav-tabs" id="Tabs">
                                                    <li class="active">
                                                        <a data-toggle="tab" href="#" id="TicketTab" onclick="navigateTab(1,event);">Ticket </a>
                                                    </li>

                                                    <li>
                                                        <a data-toggle="tab" href="#" id="sectortab" onclick="navigateTab(2,event);">Sectors </a>
                                                    </li>

                                                    <li>
                                                        <a data-toggle="tab" href="#" id="UDIDtab" onclick="navigateTab(3,event);">UDID </a>
                                                    </li>

                                                </ul>

                                                <div class="tab-content p-2">
                                                    <div id="ticket_tab" class="tab-pane fade in active">
                                                        <h4>
                                                            <strong>Ticket </strong>
                                                            <span class="float-right" id="spnagentBalance" style="display: none">
                                                                <small class="text-capitalize">Agent Balance:
                                                                <strong id="lblCusBalance"></strong></small>
                                                            </span>
                                                        </h4>
                                                        <div class="row no-gutter">
                                                            <div class="col-md-2 draftModeOn">
                                                                <div class="form-group">
                                                                    <label>Service Type<span id="" class="red_span">*</span></label><%--<label id="lblCusBalance"></label>--%>
                                                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlServiceType">
                                                                        <asp:ListItem Value="-1" Text="--Select Service--"></asp:ListItem>
                                                                        <asp:ListItem Value="1" Text="Flight" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="Hotel"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 draftModeOn">
                                                                <div class="form-group">
                                                                    <label>Transaction Type<span id="" class="red_span">*</span></label><%--<label id="lblCusBalance"></label>--%>
                                                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlTransactionType">
                                                                        <asp:ListItem Value="-1">--Select Transaction Type--</asp:ListItem>
                                                                        <asp:ListItem Value="5" Selected="True">Sale</asp:ListItem>
                                                                        <asp:ListItem Value="7">Void</asp:ListItem>
                                                                        <asp:ListItem Value="16">Refund</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 draftModeOn">
                                                                <div class="form-group">
                                                                    <label>Sale Type<span id="" class="red_span">*</span></label><%--<label id="lblCusBalance"></label>--%>
                                                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlSaleType">                                                                        
                                                                        <asp:ListItem Value="N" Selected="True">Normal</asp:ListItem>
                                                                        <asp:ListItem Value="R">Reissue</asp:ListItem>                                                                        
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 draftModeOn">
                                                                <div class="form-group">
                                                                    <label>Customer 1<span id="spnCustomer" class="red_span">*</span></label><%--<label id="lblCusBalance"></label>--%>
                                                                    <asp:DropDownList runat="server" CssClass="form-control" ID="ddlCustomer1" onchange="Customerchange();"></asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <div class="form-group" id="draftModeDiv">                                                                    
                                                                    <label><input type="checkbox" id="chkDraftmode" />Draft Mode</label>
                                                                </div> 
                                                                <asp:HiddenField ID="isEditable" runat="server" Value="1" />
                                                            </div>
                                                            <div class="col-md-4">
                                                                <div id="divCorporate" style="display: none">
                                                                    <div class="row no-gutter">
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Travel Reason<span id="spnTravelReason" class="red_span">*</span></label>
                                                                                <select id="ddlTravelReason" class="form-control" onchange="EnableTravelTrip(this.value, 0);">
                                                                                    <option value="0">Select Reason</option>
                                                                                </select>

                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label>Profile <span id="spnProfile" class="red_span">*</span></label>
                                                                                <select id="ddlProfiles" class="form-control">
                                                                                    <option value="0">Select Profile</option>
                                                                                </select>

                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                </div>

                                                            </div>
                                                        </div>
                                                        <div class="row no-gutter">
                                                            <div class="col-md-2 draftModeOn">
                                                                <div class="form-group">
                                                                    <label>PNR<span class="red_span">*</span></label>
                                                                    <asp:TextBox ID="txtPNRNum" runat="server" class="form-control" ondrop="return false;" onpaste="return true;" MaxLength="20" onblur=" CheckPNRValid();"></asp:TextBox>
                                                                    <b class="red_span" style="display: none" id="PNR"></b>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 draftModeOn">
                                                                <div class="form-group">

                                                                    <label>Source<span class="red_span">*</span></label>
                                                                    <asp:DropDownList ID="ddlairline" runat="server" CssClass="form-control" onchange="bindRepAirline()"></asp:DropDownList>
                                                                    <%-- <asp:TextBox ID="txtPreferredAirlineRet" CssClass="form-control" Text="Type Preferred Airline"
                                            runat="server" onclick="IntDom('statescontainer5' , 'ctl00_cphTransaction_txtPreferredAirlineRet')"
                                            onblur="markout(this, 'Type Preferred Airline')" onfocus="markin(this, 'Type Preferred Airline')" onkeypress="autoCompInitPrefAirline1()"></asp:TextBox>
                                                            <div id="statescontainer5" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                                display: none; z-index: 103;">
                                                            </div>--%>
                                                                    <b class="red_span" style="display: none" id="AirLine"></b>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 draftModeOn" style="display: none">
                                                                <div class="form-group" style="display: none">

                                                                    <label>Ticket Number<span class="red_span">*</span></label>
                                                                    <asp:TextBox ID="txtNumber" runat="server" class="form-control" onkeypress="return allowNumerics(event);" MaxLength="10" onchange=" CheckTicketNoValid();"></asp:TextBox>
                                                                    <b class="red_span" style="display: none" id="Number"></b>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 draftModeOn">
                                                                <div class="form-group">

                                                                    <label>Rep AirLine</label>
                                                                    <asp:DropDownList ID="ddlRepAirline" runat="server" CssClass="form-control"></asp:DropDownList>

                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 draftModeOn">
                                                                <div class="form-group">

                                                                    <label>Sales Date<span class="red_span">*</span></label>

                                                                    <div class="input-group">
                                                                        <asp:TextBox ID="txtSalesDate" runat="server" data-calendar-contentid="#container1" class="inputEnabled form-control" Enabled="false" onchange="CheckSalesDtValid();" onblur=" CheckSalesDtValid();"></asp:TextBox>
                                                                        <div class="input-group-addon p-0">
                                                                            <a href="javascript:void(null)" onclick="showCal1()">
                                                                                <img id="Img4" src="images/call-cozmo.png" alt="Pick Date" />
                                                                            </a>
                                                                        </div>
                                                                    </div>

                                                                    <b class="red_span" style="display: none" id="salesdate"></b>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 draftModeOn">
                                                                <div class="form-group">
                                                                    <label>Travel Date<span class="red_span">*</span></label>
                                                                    <div class="input-group">
                                                                        <asp:TextBox ID="txtTravelDt" runat="server" class="form-control" data-calendar-contentid="#container2" Enabled="false" onblur=" CheckTravelDtValid();"></asp:TextBox>
                                                                        <div class="input-group-addon p-0">
                                                                            <a href="javascript:void(null)" onclick="showCal2()">
                                                                                <img id="Img5" src="images/call-cozmo.png" alt="Pick Date" />
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                    <b class="red_span" style="display: none" id="traveldate"></b>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 draftModeOff">
                                                                <div class="form-group">
                                                                    <label>Supplier<span class="red_span">*</span></label>

                                                                    <%--<asp:DropDownList ID="ddlSupplier" runat="server" CssClass="form-control" onchange="bindRepAirline()"></asp:DropDownList>--%>
                                                                    <asp:DropDownList ID="ddlSupplier" runat="server" CssClass="form-control"></asp:DropDownList>


                                                                </div>
                                                            </div>

                                                            <%--<div class="col-md-4">
                                                                <div class="row no-gutter">--%>
                                                                    <div class="col-md-2 draftModeOn">
                                                                        <div class="form-group">
                                                                            <label>Destination<span class="red_span">*</span></label>
                                                                            <asp:DropDownList runat="server" ID="ddldestination" CssClass="form-control" onchange="javascript:BindSectors();"></asp:DropDownList>
                                                                            <div style="width: 250%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer2"></div>
                                                                            <b class="red_span" style="display: none" id="destination"></b>
                                                                        </div>
                                                                    </div>

                                                                    <div class="col-md-2 draftModeOff">

                                                                        <div class="form-group">

                                                                            <label>Class<span class="red_span">*</span></label>
                                                                            <asp:TextBox ID="txtClassSector" runat="server" class="form-control" onkeypress="return isAlpha(event);" MaxLength="2" onblur=" CheckClassValid();"></asp:TextBox>
                                                                            <b class="red_span" style="display: none" id="classerr"></b>
                                                                        </div>

                                                                    </div>
                                                               <%-- </div>
                                                            </div>--%>

                                                            <div class="col-md-6">

                                                                <%--                                                    <asp:UpdatePanel ID="updatepanel" runat="server">
                                                        <ContentTemplate>--%>
                                                                <div class="row no-gutter draftModeOn">

                                                                    <div class="col-md">

                                                                        <div class="form-group">

                                                                            <label>Sectors<span class="red_span">*</span></label>
                                                                            <asp:DropDownList runat="server" ID="ddlSector1" CssClass="form-control sectorControl"  Enabled="false"></asp:DropDownList>
                                                                            <%-- <asp:TextBox ID="txtSect1" runat="server" class="form-control" onchange="GetSectorsCount()" onkeypress="return isAlpha(event);" onblur="CheckDuplicatesectors(1);" MaxLength="3"></asp:TextBox>
                                                                            --%>
                                                                            <div style="width: 250%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer3"></div>
                                                                            <b class="red_span" style="display: none" id="sectors"></b>
                                                                        </div>

                                                                    </div>

                                                                    <div class="col-md">

                                                                        <div class="form-group">

                                                                            <label>&nbsp; </label>
                                                                            <asp:DropDownList runat="server" ID="ddlSector2" CssClass="form-control sectorControl"  Enabled="false"></asp:DropDownList>
                                                                            <div style="width: 250%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer4"></div>

                                                                        </div>

                                                                    </div>

                                                                    <div class="col-md">

                                                                        <div class="form-group">

                                                                            <label>&nbsp; </label>
                                                                            <asp:DropDownList runat="server" ID="ddlSector3" CssClass="form-control sectorControl"  Enabled="false"></asp:DropDownList>
                                                                            <div style="width: 250%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer5"></div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="col-md">

                                                                        <div class="form-group">

                                                                            <label>&nbsp; </label>
                                                                            <asp:DropDownList runat="server" ID="ddlSector4" CssClass="form-control sectorControl"  Enabled="false"></asp:DropDownList>
                                                                            <div style="width: 250%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer6"></div>
                                                                        </div>

                                                                    </div>

                                                                    <div class="col-md">

                                                                        <div class="form-group">

                                                                            <label>&nbsp; </label>
                                                                            <asp:DropDownList runat="server" ID="ddlSector5" CssClass="form-control sectorControl"  Enabled="false"></asp:DropDownList>
                                                                            <div style="width: 250%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer7"></div>
                                                                        </div>

                                                                    </div>

                                                                </div>
                                                                <%-- </ContentTemplate>
                                                        <Triggers>
                                                            <asp:AsyncPostBackTrigger ControlID="ddlSector1" EventName="SelectedIndexChanged" />
                                                        </Triggers>
                                                    </asp:UpdatePanel>--%>
                                                            </div>

                                                            <div class="col-md-1 draftModeOn" style="display:none">
                                                                <div class="form-group">
                                                                    <label>Type</label>
                                                                    <asp:DropDownList CssClass="form-control" ID="ddlType" runat="server" OnSelectedIndexChanged="ddlType_SelectedIndexChanged" AutoPostBack="true">
                                                                        <asp:ListItem Value="AD" Text="Adult"></asp:ListItem>
                                                                        <asp:ListItem Value="CHD" Text="Child"></asp:ListItem>
                                                                        <asp:ListItem Value="INF" Text="Infant"></asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-1 draftModeOn" style="display:none">
                                                                <div class="form-group">
                                                                    <label>Title</label>
                                                                    <asp:DropDownList CssClass="form-control" ID="ddlPaxTitle" runat="server">
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 draftModeOn" style="display:none">
                                                                <div class="form-group">

                                                                    <label>Pax Name<span class="red_span">*</span></label>
                                                                    <asp:TextBox ID="txtPaxName" runat="server" class="form-control"
                                                                        ondrop="return false;"
                                                                        onpaste="return true;" MaxLength="50" ToolTip="First Name & last name must be seperated with '/'" onblur=" CheckPaxNameValid();"></asp:TextBox>
                                                                    

                                                                </div>
                                                            </div>

                                                            <div class="col-md-2 draftModeOn">
                                                                <div class="form-group">

                                                                    <label>Locations<span class="red_span">*</span></label>
                                                                    <asp:DropDownList ID="ddlConsultant" runat="server" CssClass="form-control" ></asp:DropDownList>
                                                                </div>
                                                                <b class="red_span" style="display: none" id="locationErr"></b>
                                                            </div>

                                                            <div class="col-md-2 draftModeOff">
                                                                <div class="form-group">

                                                                    <label>Type Code</label>
                                                                    <asp:DropDownList ID="TypeCode" runat="server" CssClass="form-control">
                                                                        <asp:ListItem>Select</asp:ListItem>
                                                                        <asp:ListItem>Deal Code</asp:ListItem>
                                                                        <asp:ListItem>Corp Code</asp:ListItem>
                                                                        <asp:ListItem>Tour Code</asp:ListItem>
                                                                    </asp:DropDownList>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 draftModeOff">
                                                                <div class="form-group">
                                                                    <label>Code</label>
                                                                    <asp:TextBox ID="txtTypeCode" runat="server" onkeypress="return IsAlphaNumeric(event);" MaxLength="20" CssClass="form-control"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-2 draftModeOff">
                                                                <div class="form-group">
                                                                    <label>Sales Executive</label>
                                                                    <asp:DropDownList ID="ddlSalesExecutive" CssClass="form-control select2" runat="server" >
                                                                        
                                                                    </asp:DropDownList>
                                                                    <asp:HiddenField runat="server" Value="0" ID="hdnsalesExecutive"/>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 draftModeOn">
                                                                <div class="form-group">

                                                                    <label>Remarks</label>
                                                                    <asp:TextBox ID="txtRemarks" runat="server" class="form-control"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 draftModeOn">
                                                                <div class="form-group">

                                                                    <label>Phone</label>
                                                                    <asp:TextBox ID="txtPhone" onkeypress="return allowNumerics(event);" runat="server" class="form-control"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 draftModeOn">
                                                                <div class="form-group">

                                                                    <label>Email</label>
                                                                    <asp:TextBox ID="txtEmail" runat="server" class="form-control"></asp:TextBox>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 draftModeOn">
                                                                <div class="form-group">

                                                                    <label>Adult</label>
                                                                    <asp:DropDownList ID="ddlAdultCount" CssClass="form-control select2" runat="server" >
                                                                        
                                                                        <asp:ListItem Value="1" Text="1" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="2" ></asp:ListItem>
                                                                        <asp:ListItem Value="3" Text="3" ></asp:ListItem>
                                                                        <asp:ListItem Value="4" Text="4" ></asp:ListItem>
                                                                        <asp:ListItem Value="5" Text="5" ></asp:ListItem>
                                                                        <asp:ListItem Value="6" Text="6" ></asp:ListItem>
                                                                        <asp:ListItem Value="7" Text="7" ></asp:ListItem>
                                                                        <asp:ListItem Value="8" Text="8" ></asp:ListItem>
                                                                        <asp:ListItem Value="9" Text="9" ></asp:ListItem>
                                                                    </asp:DropDownList>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 draftModeOn">
                                                                <div class="form-group">

                                                                    <label>Child</label>
                                                                    <asp:DropDownList ID="ddlChildCount" CssClass="form-control select2" runat="server" >
                                                                        <asp:ListItem Value="0" Text="0" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Value="1" Text="1" ></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="2" ></asp:ListItem>
                                                                        <asp:ListItem Value="3" Text="3" ></asp:ListItem>
                                                                        <asp:ListItem Value="4" Text="4" ></asp:ListItem>
                                                                        <asp:ListItem Value="5" Text="5" ></asp:ListItem>
                                                                        <asp:ListItem Value="6" Text="6" ></asp:ListItem>
                                                                        <asp:ListItem Value="7" Text="7" ></asp:ListItem>
                                                                        <asp:ListItem Value="8" Text="8" ></asp:ListItem>
                                                                        <asp:ListItem Value="9" Text="9" ></asp:ListItem>
                                                                    </asp:DropDownList>

                                                                </div>
                                                            </div>
                                                            <div class="col-md-3 draftModeOn">
                                                                <div class="form-group">

                                                                    <label>Infant</label>
                                                                    <asp:DropDownList ID="ddlInfantCount" CssClass="form-control select2" runat="server" >
                                                                        <asp:ListItem Value="0" Text="0" Selected="True"></asp:ListItem>
                                                                        <asp:ListItem Value="1" Text="1" ></asp:ListItem>
                                                                        <asp:ListItem Value="2" Text="2" ></asp:ListItem>
                                                                        <asp:ListItem Value="3" Text="3" ></asp:ListItem>
                                                                        <asp:ListItem Value="4" Text="4" ></asp:ListItem>
                                                                        <asp:ListItem Value="5" Text="5" ></asp:ListItem>
                                                                        <asp:ListItem Value="6" Text="6" ></asp:ListItem>
                                                                        <asp:ListItem Value="7" Text="7" ></asp:ListItem>
                                                                        <asp:ListItem Value="8" Text="8" ></asp:ListItem>
                                                                        <asp:ListItem Value="9" Text="9" ></asp:ListItem>
                                                                    </asp:DropDownList>

                                                                </div>
                                                            </div>
                                                        </div>

                                                        <h4 class="draftModeOff">

                                                            <a class="" data-toggle="collapse" href="#ConjDetailsCollapse" role="button" aria-expanded="false" aria-controls="ConjDetailsCollapse">
                                                                <strong>Conj Details</strong>
                                                                <span class="expand-details">
                                                                    <i class="fa fa-plus-circle"></i>
                                                                </span>
                                                            </a>
                                                        </h4>

                                                        <div class="collapse draftModeOff" id="ConjDetailsCollapse" style="background-color:#e7e7e7;margin-top: -1px;">
                                                            <div class="row no-gutter">
                                                                <div class="col-md-12">

                                                                    <div class="table-responsive">

                                                                        <table class="table gridbg">

                                                                            <tr>

                                                                                <th>Ticket No.</th>
                                                                                <th>PNR Number</th>
                                                                                <th>Sector-1</th>
                                                                                <th>Sector-2</th>
                                                                                <th>Sector-3</th>
                                                                                <th>Sector-4</th>
                                                                                <th>Sector-5</th>
                                                                                <th>&nbsp;</th>
                                                                            </tr>

                                                                            <tr>

                                                                                <td>
                                                                                    <asp:TextBox ID="txtCongTicketNo" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);"></asp:TextBox>

                                                                                </td>
                                                                                <td>
                                                                                    <asp:TextBox ID="txtConjPNR" runat="server" class="form-control" ondrop="return false;" onpaste="return true;" MaxLength="10" onkeypress="return isAlphaNumeric(event);"></asp:TextBox></td>
                                                                                <td>
                                                                                    <asp:DropDownList runat="server" ID="ddlSector6" CssClass="form-control sectorControl"  Enabled="false"></asp:DropDownList>
                                                                                    <div style="width: 150%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer8"></div>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList runat="server" ID="ddlSector7" CssClass="form-control sectorControl"  Enabled="false"></asp:DropDownList>
                                                                                    <div style="width: 150%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer9"></div>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList runat="server" ID="ddlSector8" CssClass="form-control sectorControl"  Enabled="false"></asp:DropDownList>
                                                                                    <div style="width: 150%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer10"></div>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList runat="server" ID="ddlSector9" CssClass="form-control sectorControl"  Enabled="false"></asp:DropDownList>
                                                                                    <div style="width: 150%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer11"></div>
                                                                                </td>
                                                                                <td>
                                                                                    <asp:DropDownList runat="server" ID="ddlSector10" CssClass="form-control sectorControl"  Enabled="false"></asp:DropDownList>
                                                                                    <div style="width: 150%; line-height: 30px; color: #000; position: absolute; display: none" id="citycontainer12"></div>
                                                                                </td>

                                                                                <td style="font-size: 18px" width="70"></td>
                                                                            </tr>
                                                                        </table>
                                                                    </div>

                                                                </div>
                                                            </div>
                                                        </div>
                                                         <h4>
                                                            <a class="" data-toggle="collapse" href="#PaxDetailsCollapse" role="button" aria-expanded="false" aria-controls="PaxDetailsCollapse">
                                                                <strong>Pax Details</strong>
                                                                <span class="expand-details">
                                                                    <i class="fa fa-plus-circle"></i>
                                                                </span>
                                                            </a>
                                                        </h4>


                                                        <div class="collapse p-3" id="PaxDetailsCollapse" style="background-color:#e7e7e7;margin-top: -1px;">

                                                            <div class="row" id="paxDetailDiv" style="display:none;">
                                                                <div class="col-md-10">
                                                                    <table class="b2b-corp-table table table-bordered table-condensed" id="paxtable">
                                                                        <thead>
                                                                            <tr class="grid011">
                                                                                <th align="left" scope="col">
                                                                                    <label style="color: Black; font-weight: bold">
                                                                                        Pax Name
                                                                                    </label>
                                                                                </th>
                                                                                <th align="left" scope="col">
                                                                                    <label style="color: Black; font-weight: bold">
                                                                                        Pax Type
                                                                                    </label>
                                                                                </th>

                                                                                <th align="left" scope="col">
                                                                                    <label style="color: Black; font-weight: bold">
                                                                                        <span class="mdt"></span>Action
                                                                                    </label>
                                                                                </th>

                                                                            </tr>
                                                                        </thead>
                                                                        <tbody>                                                                            
                                                                                                                                                     

                                                                        </tbody>
                                                                    </table> 
                                                                    <asp:HiddenField ID="hdnPaxDetails" Value="" runat="server" />
                                                                    
                                                                </div>
                                                                <div class="col-md-2">
                                                                    <a href="javascript:void(0)" id="addNewPaxDetails" class="mt-5 d-block btn btn-outline-primary">
                                                                        <strong> Add New Pax</strong>
                                                                        <span class="icon icon-add"></span>
                                                                    </a>
                                                                </div>
                                                            </div>
                                                            <div class="paxForm">
                                                                <div class="row no-gutter mt-3">
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label>Type</label>
                                                                            <select name="ctl00$cphTransaction$paxddlType" onchange="" id="ctl00_cphTransaction_paxddlType" class="form-control">
                                                                                <option selected="selected" value="AD">Adult</option>
                                                                                <option value="CHD">Child</option>
                                                                                <option value="INF">Infant</option>

                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label>Title</label>
                                                                            <select name="ctl00$cphTransaction$paxddlPaxTitle" id="ctl00_cphTransaction_paxddlPaxTitle" class="form-control">
                                                                                <option value="Mr.">Mr.</option>
                                                                                <option value="Ms.">Ms.</option>
                                                                                <option value="Dr.">Dr.</option>
                                                                                <option value="MSTR">Master</option>

                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label>Pax Name<span class="red_span">*</span></label>
                                                                            <input name="paxtxtPaxName" type="text" maxlength="50" id="paxtxtPaxName" placeholder="First Name/Last Name" title="First Name &amp; last name must be seperated with &#39;/&#39;" class="form-control" ondrop="return false;" onpaste="return true;" onblur=" CheckPaxNameValid();" />
                                                                            <b class="red_span" style="display: none" id="paxnamemsg"></b>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label>Ticket Number<span class="red_span">*</span></label>
                                                                            <input name="" type="text" maxlength="10" id="paxTicketNumber" placeholder="Ticket Number" class="form-control" onkeypress="return allowNumerics(event);"/>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <hr class="my-3 border-secondary" />
                                                               <div class="p-2 jumbotron jumbotron-fluid">
                                                                    <div class="row">
                                                                        <div class="col-md-6">
                                                                            <div class="row no-gutter">
                                                                                <div class="col-md-12">
                                                                                    <div class="mr-3"><strong>Commission</strong></div>
                                                                                    <div class="row no-gutter">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <label> Comm %</label>
                                                                                                <input type="text" class="form-control" id="commissionPercentage" value="0.00" onkeypress="return allowNumerics(event);" onfocus="Check(this.id);" onBlur="Set(this.id);" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <label> Comm On</label>
                                                                                                <input type="text" class="form-control" id="commissionOn" disabled="disabled" value="0.00" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <label>Comm Amnt</label>
                                                                                                <input type="text" class="form-control" id="commissionAmount" disabled="disabled" value="0.00" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">
                                                                                    <div class="mr-3"><strong>Discount</strong></div>
                                                                                    <div class="row no-gutter">
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <label> Disc %</label>
                                                                                                <input type="text" class="form-control" id="discountPercenntage" value="0.00" onkeypress="return allowNumerics(event);" onfocus="Check(this.id);" onBlur="Set(this.id);" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <label> Disc On</label>
                                                                                                <input type="text" class="form-control" id="discountOn" disabled="disabled" value="0.00" />
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <label>Disc Amnt</label>
                                                                                                <input type="text" class="form-control" id="discountAmount" disabled="disabled" value="0.00" />
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-3">
                                                                                    <button type="button" class="btn btn-primary btn-sm small ml-0" data-toggle="modal" id="getHandlingFee" />
                                                                                        Handling Fee Element
                                                                                    </button>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="row no-gutter">
                                                                                <div class="col-md-4">
                                                                                    <div class="form-group pt-4" style="margin-top:2px">
                                                                                        <label>Selling Fare :</label>
                                                                                        <input type="text" class="form-control"  value="0.00" maxlength="7" id="paxtxtSellingFare" tabindex="1" onkeypress="return allowNumerics(event);" ondrop="return false;" onpaste="return true;" onfocus="Check(this.id);" onblur="Set(this.id);">
                                                                                    </div>
                                                                                </div>
                                                                                <div class="col-md-12">

                                                                                    <div class="mr-3"><strong>Tax</strong></div>
                                                                                    <div class="row no-gutter" id="TaxDetails">
                                                                                   
                                                                                    </div>
                                                                                    <div class="row no-gutter" id="paxTaxDiv">

                                                                                        <div class="col-md-6">
                                                                                            <div class="form-group">
                                                                                                <label> Tax Code</label>
                                                                                                <input type="text" class="form-control taxCode" onkeypress="return isAlphaNumeric(event);" onkeyup="return changeToUpperCase(this);" id="paxTaxCode" name="txtCode0"  maxlength="2">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-4">
                                                                                            <div class="form-group">
                                                                                                <label> Amount</label>
                                                                                                <input type="text" class="form-control taxValue" tabindex="1" onkeypress="return allowNumerics(event);" onfocus="Check(this.id);" id="paxTaxAmount" onblur="Set(this.id);"  value="0.00">
                                                                                            </div>
                                                                                        </div>
                                                                                        <div class="col-md-2">
                                                                                            <a id="paxTaxAdd" href="javascript:void(0)" class="mt-5 ml-3 d-block">
                                                                                                <i class="fa fa-plus-circle" id=""></i>
                                                                                            </a>
                                                                                        </div>
                                                                                    </div>
                                                                                </div>
                                                                            </div>

                                                                        </div>
                                                                    </div>
                                                               </div>
                                                                <hr class="my-3 border-secondary" />
                                                                <div class="row no-gutter mt-3">  
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label>Amount 1:</label>
                                                                            <input type="text" class="form-control" id="paxMarkUp" onkeypress="return allowNumerics(event);" onfocus="Check(this.id);" onBlur="Set(this.id);" value="0.00">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2">
                                                                        <div class="form-group">
                                                                            <label>Amount 2:</label>
                                                                            <input type="text" class="form-control" id="paxAmount2" onkeypress="return allowNumerics(event);" onfocus="Check(this.id);" onBlur="Set(this.id);" value="0.00">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2" style="display:none;">
                                                                        <div class="form-group">
                                                                            <label>Transaction Fee</label>
                                                                            <input type="text" class="form-control" id="paxTransactionFee" onkeypress="return allowNumerics(event);" onfocus="Check(this.id);" onBlur="Set(this.id);" value="0.00">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2" style="display:none;">
                                                                        <div class="form-group">
                                                                            <label>Addl Transaction Fee</label>
                                                                            <input type="text" class="form-control" id="paxAddlTransactionFee" onkeypress="return allowNumerics(event);" onfocus="Check(this.id);" onBlur="Set(this.id);" value="0.00">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 cancelCharges" style="display:none;">
                                                                        <div class="form-group">
                                                                            <label>Agent Cancellation</label>
                                                                            <input type="text" class="form-control" id="paxAgentCancelAmount" onkeypress="return allowNumerics(event);" onfocus="Check(this.id);" onBlur="Set(this.id);" value="0.00">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 cancelCharges" style="display:none;">
                                                                        <div class="form-group">
                                                                            <label>Supplier Cancellation</label>
                                                                            <input type="text" class="form-control" id="paxSupplierCancelAmount" onkeypress="return allowNumerics(event);" onfocus="Check(this.id);" onBlur="Set(this.id);" value="0.00">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 cancelCharges" style="display:none;">
                                                                        <div class="form-group">
                                                                            <label id="supplierTaxlbl">Cancellation I/P VAT</label>
                                                                            <input type="text" disabled="disabled" class="form-control" id="paxCancelIPVat" onkeypress="return allowNumerics(event);" onfocus="Check(this.id);" onBlur="Set(this.id);" value="0.00">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-2 cancelCharges" style="display:none;">
                                                                        <div class="form-group">
                                                                            <label id="agentTaxlbl">Cancellation O/P VAT</label>
                                                                            <input type="text" disabled="disabled" class="form-control" id="paxCancelOPVat" onkeypress="return allowNumerics(event);" onfocus="Check(this.id);" onBlur="Set(this.id);" value="0.00">
                                                                        </div>
                                                                    </div>
                                                                 </div>
                                                                 <div class="float-right">
                                                                        <button class="btn btn-sm btn-secondary small py-1 px-3" id="btnClearpaxDetails">Clear</button>
                                                                        <button class="btn btn-sm btn-primary small py-1 px-3" id="addPaxDetails">Add</button>
                                                                 </div>
                                                                 <div class="clear"></div>
                                                            </div>
                                                        </div>
                                                        <h4>
                                                            <strong>Settlement Mode</strong>
                                                        </h4>

                                                        <div class="row draftModeOn">

                                                            <div class="col-md-6" style="max-width: none; min-width: inherit;">
                                                                <div class="x_title pd-3">

                                                                    <div class="clearfix"></div>
                                                                </div>
                                                                <div class="x_content">
                                                                    <!--Start:xcontent-->
                                                                    <table id="tblSettlement" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="row">
                                                                                    <div class="col-xs-12 col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label>
                                                                                                <span id="lblSettlementMode">Mode:</span>
                                                                                            </label>
                                                                                            <div class="custom-select">
                                                                                                <asp:DropDownList ID="ddlMode" runat="server" onchange="Changemode();" CssClass="form-control">
                                                                                                    <asp:ListItem Value="-1" Text="--Select Mode--" data-id="" Selected="True"></asp:ListItem>
                                                                                                    <asp:ListItem Value="1" data-value="Cash" Text="Cash" data-id="SettCashModal"></asp:ListItem>
                                                                                                    <asp:ListItem Value="2"  data-value="Credit" Text="Credit" data-id="SettCreditModal"></asp:ListItem>
                                                                                                    <asp:ListItem Value="3"  data-value="Card" Text="Card" data-id="SettCardModal"></asp:ListItem>
                                                                                                    <%--<asp:ListItem Value="10" data-value="GL" Text="GL" data-id="SettGLModal"></asp:ListItem>--%>
                                                                                                    <%--<asp:ListItem Value="11" Text="Gift Voucher" data-id="SettGiftModal"></asp:ListItem>--%>
                                                                                                    <%--<asp:ListItem Value="12" data-value="Bank_Transfer" Text="Bank Transfer" data-id="SettBankTransferModal"></asp:ListItem>--%>
                                                                                                </asp:DropDownList>
                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div id="CashAmount" class="col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label>
                                                                                                <asp:Label ID="lblCardType" runat="server" Text="Amount:"></asp:Label></label>
                                                                                            <asp:TextBox ID="txtcashAmount" runat="server" Text="0.00" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                        </div>
                                                                                    </div>

                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table class="b2b-corp-table table table-bordered table-condensed" id="tblSettlementDetail">
                                                                                    <thead>
                                                                                        <tr class="grid011">
                                                                                            <th align="left" scope="col">
                                                                                                <label style="color: Black; font-weight: bold">
                                                                                                   Amout </label>
                                                                                            </th>
                                                                                            <th align="left" scope="col">
                                                                                                <label style="color: Black; font-weight: bold">
                                                                                                    Mode</label>
                                                                                            </th>
                                                                                            <th align="left" scope="col">
                                                                                                <label style="color: Black; font-weight: bold">
                                                                                                    <span class="mdt"></span>Action</label>
                                                                                            </th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    </tbody>
                                                                                </table>
                                                                                <asp:HiddenField runat="server" ID="hdnsettlementDetails" Value="" />
                                                                            </td>
                                                                        </tr>

                                                                    </table>

                                                                </div>
                                                                <div class="x_content">
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label>Recieved</label>
                                                                            <input type="text" id="recievedAmt" value="0" readonly="readonly" class="form-control" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label class="text-danger">Due</label>
                                                                            <input type="text" id="dueAmt" value="0" readonly="readonly" class="form-control text-danger" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3" id="ccAmtDiv">
                                                                        <div class="form-group">
                                                                            <label>CC Charge</label>
                                                                            <input type="text" id="cardChargeAmt" value="0" readonly="readonly" class="form-control" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-3">
                                                                        <div class="form-group">
                                                                            <label>To collect</label>
                                                                            <input type="text" id="totalToCollectAmt" value="0" readonly="readonly" class="form-control" />
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="fixed_inpt_cal">

                                                                    <div class="row no-gutter">


                                                                        <div class="col-md-3" id="divInputVAT">
                                                                            <div class="form-group">
                                                                                <label>INPUT VAT  </label>
                                                                                <asp:TextBox ID="TxtInputVAT" runat="server" CssClass="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>
                                                                                <b class="red_span" style="display: none" id="InputVAT"></b>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-3" id="divOutVAT">
                                                                            <div class="form-group">
                                                                                <label id="gstOrVat">OUTPUT VAT  </label>
                                                                                <asp:TextBox ID="txtOutPutVAT" runat="server" Text="0.00" CssClass="form-control" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>
                                                                                <b class="red_span" style="display: none" id="outputVAT"></b>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-2" id="divNetVAT" >
                                                                            <div class="form-group">
                                                                                <label>NetVAT  </label>
                                                                                <asp:TextBox ID="txtNetVAT" runat="server"  onkeypress="return allowNumerics(event);" Text="0.00" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                <b class="red_span" style="display: none" id="netVAT"></b>
                                                                            </div>
                                                                        </div>


                                                                        <div class="col-md-2" id="divGST"  style="display: none">
                                                                            <div class="form-group">
                                                                                <label>GST  </label>
                                                                                <asp:TextBox ID="txtGST" runat="server"  onkeypress="return allowNumerics(event);" Text="0.00" CssClass="form-control" Enabled="false"></asp:TextBox>
                                                                                <b class="red_span" style="display: none" id="GST"></b>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <label>Total Fare  </label>
                                                                                <asp:TextBox ID="txtTotalFare" runat="server"  onkeypress="return allowNumerics(event);" Enabled="false" Text="0.00" class="form-control"></asp:TextBox>
                                                                                <b class="red_span" style="display: none" id="totalFair"></b>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <label>Discount  </label>
                                                                                <asp:TextBox ID="txtDiscount" runat="server"  onkeypress="return allowNumerics(event);" Text="0.00" CssClass="form-control" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>

                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <label>Tax  </label>
                                                                                <asp:TextBox ID="txtTax" runat="server" onchange="javascript:calculateTotalFare();CalculateNetPay();" onkeypress="return allowNumerics(event);" Text="0.00" class="form-control" Enabled="false"></asp:TextBox>
                                                                                <b class="red_span" style="display: none" id="tax"></b>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <label>commission  </label>
                                                                                <asp:TextBox ID="txtCommision" runat="server"  onkeypress="return allowNumerics(event);" Text="0.00" class="form-control" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>
                                                                                <b class="red_span" style="display: none" id="commision"></b>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <label>Collected  </label>
                                                                                <asp:TextBox ID="txtCollected" runat="server" onkeypress="return allowNumerics(event);" Enabled="false" CssClass="form-control" Text="0.00"></asp:TextBox>
                                                                                <b class="red_span" style="display: none" id="collected"></b>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <label>Net Payable  </label>
                                                                                <asp:TextBox ID="txtNetpayable" runat="server" onkeypress="return allowNumerics(event);" Text="0.00" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                                <b class="red_span" style="display: none" id="netpayable"></b>
                                                                            </div>
                                                                        </div>

                                                                        <div class="col-md-2">
                                                                            <div class="form-group">
                                                                                <label>Profit  </label>
                                                                                <asp:TextBox ID="txtProfit" runat="server" onkeypress="return allowNumerics(event);" Text="0.00" Enabled="false" CssClass="form-control"></asp:TextBox>
                                                                                <b class="red_span" style="display: none" id="profit"></b>
                                                                            </div>
                                                                        </div>

                                                                    </div>


                                                                </div>
                                                            </div>

                                                        </div>

                                                    </div>

                                                    <div id="sectors_tab" class="tab-pane fade">

                                                        <h4>
                                                            <strong>Sectors </strong>
                                                        </h4>

                                                        <div><span id="SectorerrorMessage" style="color: Red"></span></div>
                                                        <div class=" sector">
                                                            <div id="divSectors" class="table-responsive" style="width: 100%; overflow: auto">
                                                                <asp:Table Class="table table-bordered b2b-corp-table" ID="tblSector" runat="server" Width="100%">

                                                                    <asp:TableRow>
                                                                        <asp:TableCell>Refundable</asp:TableCell>
                                                                        <asp:TableCell>From Sector</asp:TableCell>
                                                                        <asp:TableCell>To Sector</asp:TableCell>
                                                                        <asp:TableCell>Flying Carrier</asp:TableCell>
                                                                        <asp:TableCell>Flight No</asp:TableCell>
                                                                        <asp:TableCell>DepartureDate - Time</asp:TableCell>
                                                                        <asp:TableCell>ArrivalDate - Time </asp:TableCell>
                                                                        <asp:TableCell>Fare Basis</asp:TableCell>
                                                                        <asp:TableCell>Class</asp:TableCell>
                                                                        <asp:TableCell>Duration</asp:TableCell>
                                                                    </asp:TableRow>


                                                                    <asp:TableRow ID="row1" Style="display: none">

                                                                        <asp:TableCell Width="50px">
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund1" Checked="true" Style="display: none" />
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector1" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector1" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier1" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="2"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN01" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt1" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>

                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt1" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis1" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass1" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="2"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration1" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row2" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund2" Checked="true" Style="display: none" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector2" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector2" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier2" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="2"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN02" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt2" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt2" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis2" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass2" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="2"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration2" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                           
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row3" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund3" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector3" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector3" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier3" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="3"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN03" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt3" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt3" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis3" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass3" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="3"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration3" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row4" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund4" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector4" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector4" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier4" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="4"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN04" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt4" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt4" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis4" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass4" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="4"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration4" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                           
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row5" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund5" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector5" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector5" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier5" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN05" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt5" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt5" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis5" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass5" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="5"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration5" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row6" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund6" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector6" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector6" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier6" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="6"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN06" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="6"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt6" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt6" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis6" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass6" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="6"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration6" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row7" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund7" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector7" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>

                                                                            <asp:DropDownList runat="server" ID="ddlToSector7" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier7" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="7"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN07" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="7"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt7" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt7" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis7" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass7" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="7"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration7" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                           
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row8" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund8" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector8" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector8" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier8" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="8"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN08" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="8"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt8" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt8" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis8" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass8" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="8"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration8" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row9" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund9" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector9" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector9" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier9" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="9"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN09" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="9"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt9" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt9" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis9" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass9" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="9"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration9" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row10" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund10" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector10" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector10" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier10" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN010" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt10" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt10" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis10" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass10" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="10"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration10" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row11" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund11" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector11" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector11" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier11" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="11"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN011" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="11"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt11" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="11" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt11" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="11" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis11" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="11"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass11" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="11"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration11" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row12" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund12" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector12" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector12" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier12" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="12"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN012" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="12"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt12" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="12" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt12" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="12" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis12" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="12"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass12" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="12"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration12" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row13" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund13" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector13" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector13" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier13" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="13"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN013" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="13"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt13" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="13" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt13" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="13" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis13" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="13"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass13" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="13"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration13" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row14" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund14" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector14" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector14" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier14" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="14"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN014" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="14"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt14" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="14" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt14" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="14" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis14" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="14"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass14" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="14"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration14" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row15" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund15" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector15" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector15" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier15" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="15"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN015" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="15"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt15" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="15" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt15" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="15" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis15" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="15"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass15" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="15"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration15" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row16" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund16" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector16" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector16" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier16" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="16"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN016" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="16"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt16" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="16" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt16" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="16" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis16" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="16"></asp:TextBox>
                                                                            
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass16" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="16"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration16" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row17" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund17" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector17" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector17" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier17" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="17"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN017" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="17"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt17" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="17" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt17" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="17" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis17" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="17"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass17" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="17"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration17" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row18" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund18" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector18" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector18" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier18" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="18"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN018" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="18"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt18" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="18" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt18" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="18" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis18" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="18"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass18" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="18"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration18" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                           
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row19" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund19" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector19" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector19" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier19" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="19"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN019" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="19"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt19" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="19" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt19" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="19" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis19" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="19"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass19" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="19"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration19" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                            
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                    <asp:TableRow ID="row20" Style="display: none">

                                                                        <asp:TableCell>
                                                                            <asp:CheckBox runat="server" ID="chkIsRefund20" Checked="true" />
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlFromSector20" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:DropDownList runat="server" ID="ddlToSector20" CssClass="form-control" onchange="return CheckDuplicateDynamicSectors(this);"></asp:DropDownList>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlyingCarrier20" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" ondrop="return false;" onpaste="return true;" MaxLength="20"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFlightN020" runat="server" CssClass="form-control" ondrop="return false;" onpaste="return true;" MaxLength="20"></asp:TextBox>
                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="DepartureDt20" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="20" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <div>
                                                                                <uc1:DateControl ID="ArrivalDt20" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="20" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>
                                                                            </div>

                                                                        </asp:TableCell>
                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtFareBasis20" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="20"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtClass20" runat="server" CssClass="form-control" onkeypress="return isAlpha(event);" ondrop="return false;" onpaste="return true;" MaxLength="20"></asp:TextBox>
                                                                        </asp:TableCell>

                                                                        <asp:TableCell>
                                                                            <asp:TextBox ID="txtDuration20" runat="server" CssClass="form-control" MaxLength="3" ToolTip="Duration should be in Minutes" onkeypress="return allowNumerics(event);"></asp:TextBox>
                                                                           
                                                                            
                                                                        </asp:TableCell>

                                                                    </asp:TableRow>

                                                                </asp:Table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <asp:HiddenField ID="hdnDuration" runat="server" Value="0" />
                                                    <div id="UDID" class="tab-pane fade">
                                                        <h4>
                                                            <strong>UDID </strong>
                                                        </h4>

                                                        <div class="form-group baggage-wrapper" id="divFlex0">

                                                            <div class="row padding-0 marbot_10" id="divFlexFields0"></div>


                                                            <%--<div class="row padding-0 marbot_10" id="tblFlexFields" runat="server">
                                                                <%--<table id="tblFlexFields" runat="server" border="0" cellspacing="0" cellpadding="0"></table>--%>
                                                            <%--</div>--%>
                                                            <%--<a style="color: rgb(0, 0, 0); margin-left: 0px; padding: 0px; display: none;" id="ancfelx" class="advPax-collapse-btn-new float-left" role="button" data-toggle="collapse" href="#FlexFiledscollapse"
                                                                aria-expanded="false" aria-controls="collapseExample"></a>
                                                            <div class="collapse" id="FlexFiledscollapse">
                                                                <div class="row padding-0 marbot_10 " id="tblFlexFields1" runat="server" visible="false">
                                                                    <%--<table id="tblFlexFields" runat="server" border="0" cellspacing="0" cellpadding="0"></table>--%>
                                                            <%--</div>

                                                            </div>--%>
                                                        </div>

                                                    </div>

                                                </div>
                                                <div class="pnr-toggle-btn-wrap">

                                                    <a id="hidePanel" href="javascript:void(0);" style="color: #fff">
                                                        <i class="fa fa-angle-left"></i>
                                                    </a>

                                                </div>

                                            </div>
                                        </div>
                                        <div class="row no-gutter">
                                            <div class="col-md-12" style="text-align: right;">


                                                <div class="pull-right mr-4 mt-2">

                                                    <asp:Button ID="btnAdd" runat="server" CausesValidation="true" OnClientClick="return ValidateCtrls(false);" OnClick="btnAdd_Click" class="btn btn-primary btn_custom pull-left" Text="More Pax" Visible="false" />

                                                    <asp:Button ID="btnprev" runat="server" class="btn btn-primary btn_custom pull-left" OnClientClick="navigatePrevTab(); return false;" Text="Prev" />
                                                    <asp:Button ID="btnnext" runat="server" class="btn btn-primary btn_custom pull-left" OnClientClick="navigateNextTab(); return false;" Text="Next" />

                                                    <asp:Button ID="btnUpdate" runat="server" CausesValidation="true" OnClick="btnUpdate_Click" OnClientClick="return ValidateCtrls(false);" class="btn btn-primary btn_custom pull-left" Text="Update" Visible="false" />

                                                    <asp:Button ID="btnSave" runat="server" OnClick="btnSave_Click" OnClientClick="return Savesegments(event);" class="btn btn-primary btn_custom pull-left" Text="Save" Style="display: none" UseSubmitBehavior="false"/>

                                                    <asp:Button ID="btnClear" runat="server" class="btn btn-secondary btn_custom pull-left" Text="Clear" OnClientClick="window.location = 'OffLineEntry.aspx'; return false;" />
                                                    <div class="clearfix"></div>

                                                </div>


                                            </div>
                                        </div>
                                    </div>

                                </div>

                                <script>
                                    function HidePanel() {

                                        document.getElementById("LeftPanel").style.width = "100%";

                                        document.getElementById("RightPanel").style.display = "none";

                                    }
                                </script>

                                <!--right panel-->
                                <div id="RightPanel" class="col" style="margin-top: 37px;">



                                    <div>

                                        <div class="main_panel">
                                            <div class="main_panel_wrap">

                                                <div class="multi_inpt">
                                                    <span class="input-group-addon">
                                                        <label style="display: block">Currency:</label>
                                                        <asp:DropDownList ID="ddlcurrencycode" runat="server" Enabled="false">
                                                        </asp:DropDownList>
                                                        <asp:HiddenField ID="hdnCurrencyCode" Value="" runat="server" />
                                                        <%--<select disabled="" class="form-control">
                                                    <option>AED </option>
                                                </select>--%>
                                                    </span>

                                                    <span class="input-group-addon">
                                                        <label>Exc. Rate:</label>

                                                        <asp:TextBox ID="txtExcRate" runat="server" Text="1.00" class="form-control" Enabled="false"></asp:TextBox>
                                                        <%--  <input disabled="" class="form-control" value="1.000" type="text">--%>
                                                    </span>

                                                </div>

                                               
                                                <div class="form-group-title">
                                                    <strong>Selling Fare : </strong>
                                                </div>
                                                <div class="form-group">
                                                    <asp:TextBox ID="txtSellingFare" onkeypress="return allowNumerics(event);" runat="server" onchange="javascript:calculateTotalFare();" class="form-control" ondrop="return false;"
                                                        onpaste="return true;" onfocus="Check(this.id);" onBlur="Set(this.id);" MaxLength="7" Text="0.00" TabIndex="1" ReadOnly="true" Enabled="false"></asp:TextBox>
                                                    <b class="red_span" style="display: none" id="sellingFare" ></b>
                                                </div>
                                                <div class="form-group-title" style="display: none">
                                                    <strong>Tax : </strong>

                                                </div>                                                
                                                <div class="multi_inpt">   
                                                    <span class="input-group-addon">
                                                        <label>Tax Amount:</label>
                                                        <input class="form-control" id="totalTaxValue" value="0.00" disabled="disabled" onBlur="Set(this.id);"  TabIndex="1" />
                                                    </span>
                                                </div>                                                

                                                <div class="form-group-title">
                                                    <strong>Commission:</strong>

                                                </div>

                                                <div class="multi_inpt three-col" >
                                                    <span class="input-group-addon">
                                                        <label>Comm %</label>
                                                        <asp:TextBox ID="txtCommper" ReadOnly="true" runat="server" onkeypress="return allowNumerics(event);" MaxLength="3" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);"  TabIndex="1" Enabled="false"></asp:TextBox>
                                                    </span>

                                                    <span class="input-group-addon">
                                                        <label>Comm On</label>
                                                        <asp:TextBox ID="txtCommisionOn" runat="server" onkeypress="return allowNumerics(event);" MaxLength="7" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>
                                                    </span>

                                                    <span class="input-group-addon">
                                                        <label>Comm Amnt</label>
                                                        <asp:TextBox ID="txtCommisionAmt" runat="server" onkeypress="return allowNumerics(event);" MaxLength="7" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>
                                                    </span>

                                                </div>
                                                
                                                <div class="form-group-title" >
                                                    <strong>Discount:</strong>
                                                </div>

                                                <div class="multi_inpt three-col" >
                                                    <span class="input-group-addon">
                                                        <label>Disc %</label>
                                                        <asp:TextBox ID="txtDiscountper" Enabled="false" runat="server" onkeypress="return allowNumerics(event);" MaxLength="3" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);"  TabIndex="1"></asp:TextBox>
                                                    </span>

                                                    <span class="input-group-addon">
                                                        <label>Disc On</label>
                                                        <asp:TextBox ID="txtDiscountOn" runat="server" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false" class="form-control"></asp:TextBox>
                                                    </span>

                                                    <span class="input-group-addon">
                                                        <label>Disc Amnt</label>
                                                        <asp:TextBox ID="txtDiscountAmt" runat="server" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Enabled="false"></asp:TextBox>
                                                    </span>
                                                </div>
                                                <div><b class="red_span" style="display: none" id="DiscountDetailsErr"></b></div>
                                                <%--<button type="button" class="btn btn-primary btn-sm small my-3" id="getHandlingFee">
                                                  Handling Fee Element
                                                </button>--%>
                                                <div class="form-group-title" >
                                                    <strong>Service charge:</strong>

                                                </div>

                                                <div class="multi_inpt" >
                                                    <span class="input-group-addon">
                                                        <label>Amount 1:</label>
                                                        <asp:TextBox ID="txtMarkUp" Enabled="false"  runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" class="form-control" Value="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" TabIndex="1"></asp:TextBox>
                                                    </span>
                                                    <span class="input-group-addon">
                                                        <label>Amount 2:</label>
                                                        <asp:TextBox ID="txtAmount2" Enabled="false" onchange="javascript:calculateProfit();" runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" Value="0.00" class="form-control" onfocus="Check(this.id);" onBlur="Set(this.id);" TabIndex="1"></asp:TextBox>
                                                    </span>
                                                </div>
                                                <div class="multi_inpt cancelCharges"  style="display:none;">
                                                    <span class="input-group-addon">
                                                        <label>Agent Cancellation</label>
                                                        <asp:TextBox Enabled="false" ID="txtAgentCancellationAmt"  runat="server" onkeypress="return allowNumerics(event);"  class="form-control" Value="0.00"  TabIndex="1"></asp:TextBox>
                                                    </span>
                                                    <span class="input-group-addon">
                                                        <label>Supplier Cancellation</label>
                                                        <asp:TextBox Enabled="false" ID="txtSupplierCancelationAmt"  runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" Value="0.00" class="form-control" TabIndex="1"></asp:TextBox>
                                                    </span>
                                                </div>
                                                <div class="multi_inpt cancelCharges" style="display:none;">
                                                    <span class="input-group-addon">
                                                        <label id="lblTotalCancelOpVat">Cancellation O/P VAT</label>
                                                        <asp:TextBox Enabled="false" ID="txtCancelOPVat" runat="server"  class="form-control" Value="0.00" TabIndex="1" ReadOnly="true"></asp:TextBox>
                                                        <asp:HiddenField ID="hdnCancelOPVat" runat="server" Value="0"  />
                                                    </span>
                                                    <span class="input-group-addon">
                                                        <label id="lblTotalCancelIpVat">Cancellation I/P VAT</label>
                                                        <asp:TextBox Enabled="false" ID="txtCancelIPVat"  runat="server" Value="0.00" class="form-control" TabIndex="1" ReadOnly="true"></asp:TextBox>
                                                        <asp:HiddenField ID="hdnCancelIPVat" runat="server" Value="0"  />
                                                    </span>
                                                </div>
                                                <div><b class="red_span" style="display: none" id="MarkUPErrormsg"></b></div>
                                                <div class="multi_inpt" style="display: none">
                                                    <span class="input-group-addon">
                                                        <label >Transaction Fee</label>
                                                        <asp:TextBox ID="txtTransFee" runat="server" onkeypress="return allowNumerics(event);" onchange="javascript:calculateProfit();" MaxLength="6" class="form-control" Value="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" TabIndex="1"></asp:TextBox>
                                                    </span>
                                                    <span class="input-group-addon">
                                                        <label>Addl transaction Fee</label>
                                                        <asp:TextBox ID="txtAddlTransFee" runat="server" onkeypress="return allowNumerics(event);" onchange="javascript:calculateProfit();" MaxLength="6" class="form-control" Value="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" TabIndex="1"></asp:TextBox>
                                                    </span>
                                                </div>
                                                <div><b class="red_span" style="display: none" id="TrasactionErrormsg"></b></div>

                                                <div class="multi_inpt">
                                                    <span class="input-group-addon">
                                                        <%--  <label>Agent PLB</label>--%>
                                                        <asp:TextBox ID="txtAgentPLB" runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Visible="false"></asp:TextBox>
                                                    </span>
                                                    <span class="input-group-addon">
                                                        <%-- <label>Agent Commision</label>--%>
                                                        <asp:TextBox ID="txtAgentCom" runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Visible="false"></asp:TextBox>
                                                    </span>
                                                </div>
                                                <div><b class="red_span" style="display: none" id="AgentComErr"></b></div>

                                                <div class="multi_inpt">
                                                    <span class="input-group-addon">
                                                        <%--  <label>Our PLB</label>--%>
                                                        <asp:TextBox ID="txtOurPLB" runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Visible="false"></asp:TextBox>
                                                    </span>
                                                    <span class="input-group-addon">
                                                        <%-- <label>Our Commision</label>--%>
                                                        <asp:TextBox ID="txtOurComm" runat="server" onkeypress="return allowNumerics(event);" MaxLength="6" class="form-control" Text="0.00" onfocus="Check(this.id);" onBlur="Set(this.id);" Visible="false"></asp:TextBox>
                                                    </span>
                                                </div>
                                                <div><b class="red_span" style="display: none" id="OurCommErr"></b></div>
                                            </div>

                                        </div>

                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>

                    <!--footer starts here-->

                    <!--Modal Popup-->
                    <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
                        <div class="modal-dialog modal-lg" role="document">
                            <div class="modal-content">
                                <div class="modal-header">

                                    <button type="button" data-dismiss="modal" class="close mt-0" style="color: #fff; opacity: 1;">
                                        <span aria-hidden="true">×</span>
                                    </button>

                                    <h4 class="modal-title" id="rtgrt">PNR Search Results  </h4>
                                </div>
                                <span id="array_disp"></span>
                                <div class="modal-body p-0">
                                    <div id="popupData" runat="server">
                                        <%if (itinerary != null && itinerary.Segments != null && itinerary.Passenger != null && itinerary.Segments.Length > 0 && itinerary.Passenger.Length > 0) %>
                                        <%{ %>
                                        <div class="table-responsive">
                                            <table class="table table-bordered b2b-corp-table mb-0" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <thead>
                                                    <tr>
                                                        <th>Invoice Date </th>
                                                        <th>PNR/Ticket</th>
                                                        <th>Sale Date</th>
                                                        <th>Airline</th>
                                                        <th>Pax Name</th>
                                                        <th>Fare</th>
                                                        <th>Tax</th>
                                                        <th>Service Charge</th>
                                                        <th>Markup </th>
                                                        <th>Mode</th>
                                                        <th></th>
                                                        <th></th>
                                                    </tr>

                                                </thead>
                                                <tbody>
                                                    <%for (int i = 0; i < itinerary.Passenger.Length; i++) %>
                                                    <%{ %>

                                                    <%if (itinerary.Passenger[i] != null) %>
                                                    <%{ %>
                                                    <tr>

                                                        <td><%=itinerary.CreatedOn.ToString("dd-MMM-yyyy") %></td>
                                                        <td><%=itinerary.PNR %></td>
                                                        <td><%=itinerary.CreatedOn.ToString("dd-MMM-yyyy") %></td>
                                                        <td><%=itinerary.Segments[0].Airline %></td>
                                                        <td><%=itinerary.Passenger[i].FirstName +"/"+ itinerary.Passenger[i].LastName %></td>
                                                        <td><%=itinerary.Passenger[i].Price.PublishedFare %></td>
                                                        <td><%=itinerary.Passenger[i].Price.Tax %></td>
                                                        <td><%=itinerary.Passenger[i].Price.SeviceTax %></td>
                                                        <td><%=itinerary.Passenger[i].Price.Markup+ itinerary.Passenger[i].Price.AsvAmount %></td>
                                                        <td><%=itinerary.PaymentMode %></td>

                                                        <td><a href="#" class="add-btn" id="Edit_<%=i %>" onclick="hidePopUp('<% =i %>')">Edit </a></td>

                                                        <td><a href="#" class="add-btn" id="Remove_<%=i %>" onclick="RemovePaxFromPopUp('<% =i %>')">Remove </a></td>
                                                        <%-- <asp:LinkButton class="getId" runat="server" ID="hlEdit" OnClick="hlEdit_Click" OnClientClick="hidePopUp(<% =i %>)" Text="Add/Edit"></asp:LinkButton></td>--%>
                                                    </tr>
                                                    <%} %>
                                                    <%} %>
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="py-4 pr-4">
                                            <div class="float-right">
                                                <button type="button" data-dismiss="modal" value="Close" class="btn btn-primary btn_custom">
                                                    <span aria-hidden="true">Close</span>
                                                </button>

                                            </div>
                                            <div class="clear"></div>
                                        </div>
                                        <%} %>
                                    </div>


                                </div>
                            </div>
                        </div>
                    </div>






                </div>
                <div class="clearfix"></div>
                <div class="modal fade" id="SettlementModalContent" tabindex="-1" role="dialog" data-backdrop="static" data-keyboard="false" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"><span id="settlementModeText"></span> Settlement Mode</h5>
                                <button type="button" class="close" id="closeModal" aria-label="Close" style="margin-top: -25px;">
                                    <span class="icon icon-close"></span>
                                </button>
                            </div>
                            <!--Payment CASH-->
                            <div class="modal-body" id="SettCashModal" style="display: none">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Amount</label>
                                            <input class="form-control amt" type="text">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Branch</label>
                                            <select class="form-control" id="cashModalBranch">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Settlement Remark</label>
                                            <input class="form-control" type="text" id="settlementRemarkCash">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Payment CREDIT-->
                            <div class="modal-body" id="SettCreditModal" style="display: none">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Amount</label>
                                            <input class="form-control amt" type="text">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Customer</label>
                                           <%-- <select class="form-control" id="creditModalBranch">
                                            </select>--%>
                                            <input id="creditModalBranch" type="text" class="form-control" placeholder="Customer Name Here" />
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Settlement Remark</label>
                                            <input type="text" class="form-control"  id="settlementRemarkCredit" />
                                        </div>
                                    </div>
                                    <%--<div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Sales Activity</label>
                                            <input class="form-control" type="text" readonly="readonly" id="salesActivity">
                                        </div>
                                    </div>--%>
                                </div>
                            </div>
                            <!--Payment CARD-->
                            <div class="modal-body" id="SettCardModal" style="display: none">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Amount</label>
                                            <input class="form-control amt" type="text">
                                            <label id="lblIncludeCharge" ></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Card Type</label>
                                            <select class="form-control" id="cardType">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Card Number (Last 4 digit)</label>
                                            <input class="form-control" type="text" id="cardNumber" maxlength="4">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Approval Number</label>
                                            <input class="form-control" type="text" id="approvalNumber">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Expiry Date</label>
                                            <input class="form-control" type="date" id="expiryDate">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Card Charge</label>
                                            <input class="form-control" type="text" id="cardCharge" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Settlement Remark</label>
                                            <input class="form-control" type="text" id="settlementRemarkCard">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Payment GL-->
                            <div class="modal-body" id="SettGLModal" style="display: none">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Amount</label>
                                            <input class="form-control amt" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Branch</label>
                                            <select class="form-control" id="glModalBranch">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">GL Charge Code</label>
                                            <select class="form-control" id="glChargeCode">
                                                <option></option>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Settlement Remark</label>
                                            <input class="form-control" type="text" id="settlementRemarkGl">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Payment GiftVoucher-->
                            <div class="modal-body" id="SettGiftModal" style="display: none">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Amount</label>
                                            <input class="form-control amt" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Voucher Type</label>
                                            <select class="form-control" id="voucherType">
                                                <option></option>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Voucher Number</label>
                                            <input class="form-control" type="text" id="voucherNumber">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Settlement Remark</label>
                                            <input class="form-control" type="text" id="settlementRemarkGift">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Payment GiftVoucher-->
                            <div class="modal-body" id="SettBankTransferModal" style="display: none">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Amount</label>
                                            <input class="form-control amt" type="text">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Transfer Type</label>
                                            <select class="form-control" id="transferType">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Ref Number/EN No.</label>
                                            <input class="form-control" type="text" id="refNumberEnNumber">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Ref Date</label>
                                            <input class="form-control" type="date" id="refDate">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Account Number</label>
                                            <input class="form-control" type="text" id="accountNumber">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Bank</label>
                                            <select class="form-control" id="bankName">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Settlement Remark</label>
                                            <input class="form-control" type="text" id="settlementRemarkTransfer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closepopUp">Close</button>
                                <button type="button" class="btn btn-primary" id="addSettlement">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>

                 <div class="modal fade" id="handlingFeeModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="">Handling Fee</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -25px;">
                                    <span class="icon icon-close"></span>
                                </button>
                            </div>
                            <!--Payment CASH-->
                            <div class="modal-body" id="HandlingFeehModalBody">
                                
                            </div>                         
                          
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                <button type="button" class="btn btn-primary" id="addHandlingFee">Save</button>
                            </div>
                        </div>
                    </div> 
                </div>
                <asp:HiddenField ID="segmentId" Value="" runat="server" />
                <asp:HiddenField ID="handlingFee" runat="server" Value="0"  />
                <asp:HiddenField ID="hdnhandlingFeeCharge" runat="server" Value="0"  />
                <script type="text/javascript">
                    var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["CMSUrl"]%>';
                    function StellrMode(obj) {
                        var selectBox = obj;
                        var selected = selectBox.options[selectBox.selectedIndex].value;
                        document.getElementById('<%=paymentmode.ClientID%>').value = selected;
                        /*  cash*/
                        if (selected === '1') {

                            CardType.style.display = "none";
                            CardAmount.style.display = "none";

                            CashAmount.style.display = "block";
                            CardPercentage.style.display = "none";
                            CreditAmount.style.display = "none";
                            Arcode.style.display = "none";

                        }

                        /*  card*/
                        else if (selected === '2') {
                            CardType.style.display = "block";

                            CardAmount.style.display = "block";

                            CashAmount.style.display = "none";

                            CardPercentage.style.display = "block";

                            CreditAmount.style.display = "none";
                            Arcode.style.display = "none";

                        }

                        /*  credit*/
                        else if (selected === '3') {
                            CardType.style.display = "none";

                            CardType.style.display = "none";

                            CardAmount.style.display = "none";

                            CashAmount.style.display = "none";

                            CardPercentage.style.display = "none";

                            CreditAmount.style.display = "block";

                            Arcode.style.display = "block";

                        } else {

                        }

                    }    
                
                    //<![CDATA[
                    disablefield();
                    var getUrlParameter = function getUrlParameter(sParam) {
                        var sPageURL = window.location.search.substring(1),
                            sURLVariables = sPageURL.split('&'),
                            sParameterName,
                            i;

                        for (i = 0; i < sURLVariables.length; i++) {
                            sParameterName = sURLVariables[i].split('=');

                            if (sParameterName[0] === sParam) {
                                return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
                            }
                        }
                    };
                    function DateFormatIng(dt) {
                        var dateData = new Date(dt);
                        var day = dateData.getDate();
                        var month = dateData.getMonth()+1;
                        var year = dateData.getFullYear();
                        if (day < 10) {
                            day = "0" + day;
                        }
                        if (month < 10) {
                            month = "0" + month;
                        }
                        return  month + "/"+ day + "/"+ year;
                    }
                   
                    $(document).ready(function () {
                          <%if (agentLocation.CountryCode == "IN")
                    {%>
                        document.getElementById('<%=hdnlocCountryCode.ClientID%>').value = "IN";
                        document.getElementById('divInputVAT').style.display = 'none';
                        document.getElementById('divOutVAT').style.display = 'block';
                        document.getElementById('divNetVAT').style.display = 'none';
                        $("#gstOrVat").text("GST")
                        document.getElementById('<%=hndLocationId.ClientID%>').value = <%=agentLocation.ID%>;
                        
                                <%}
                    else
                    {%>
                        document.getElementById('divInputVAT').style.display = 'block';
                        document.getElementById('divOutVAT').style.display = 'block';
                        document.getElementById('divNetVAT').style.display = 'block';
                        $("#gstOrVat").text("OUTPUT VAT")

                            <%}%>
                        var paxTypeObj = {
                            "AD": 1,
                            "CHD": 2,
                            "INF":3
                        };
                        let logggedInAgent = document.getElementById('<%=hdnAgentId.ClientID%>').value;
                        //$("#<%=isEditable.ClientID%>").val("0");
                        var urlParam = getUrlParameter('id');
                        if (typeof urlParam !== "undefined") {
                            document.getElementById('ctl00_upProgress').style.display = 'block';
                            $.ajax({
                                url: "OfflineEntry.aspx/LoadItinerary",
                                type: "POST",
                                dataType: "json",
                                data: JSON.stringify({ "flightId": urlParam }),
                                contentType: "application/json; charset=utf-8",
                                success: function (cardResponse) {
                                    var JObject = JSON.parse(cardResponse.d);  
                                    
                                    var segmentArray = [];
                                    $.each(JObject.itinerary.Segments, function (ind, val) {
                                        segmentArray.push(val.SegmentId);
                                    });
                                    $("#ctl00_cphTransaction_segmentId").val(segmentArray.join(","));
                                    if (JObject.itinerary.AliasAirlineCode != null || JObject.itinerary.AliasAirlineCode != "") {
                                        var existingEntry = JObject.itinerary;
                                        $("#ctl00_cphTransaction_ddlCustomer1").select2('val', JObject.booking.AgencyId);
                                        $("#ctl00_cphTransaction_ddlCustomer1").trigger('change');
                                        $("#ctl00_cphTransaction_ddlairline").select2('val', existingEntry.AirlineCode);
                                        $("#ctl00_cphTransaction_ddlairline").trigger('change');
                                        $("#ctl00_cphTransaction_ddlRepAirline").select2('val', existingEntry.AliasAirlineCode.trim());
                                        $("#ctl00_cphTransaction_txtPNRNum").val(existingEntry.PNR);
                                        
                                        $("#ctl00_cphTransaction_ddlSaleType").select2('val',JObject.itinerary.SaleType);
                                        var trvlDate = DateFormatIng(existingEntry.TravelDate);
                                        $("#ctl00_cphTransaction_txtTravelDt").val(trvlDate);
                                        $("#ctl00_cphTransaction_ddldestination").select2('val', existingEntry.Destination);
                                        $("#ctl00_cphTransaction_ddldestination").trigger('change');
                                        document.getElementById('<%=hndLocationId.ClientID%>').value = existingEntry.LocationId;                                        
                                        $("#ctl00_cphTransaction_ddlConsultant").select2('val', existingEntry.LocationId);
                                        $("#ctl00_cphTransaction_ddlConsultant").trigger('change');
                                        $("#ctl00_cphTransaction_ddlAdultCount").select2('val', JObject.handling[0].AdultCount);
                                        $("#ctl00_cphTransaction_ddlChildCount").select2('val', JObject.handling[0].ChildCount);
                                        $("#ctl00_cphTransaction_ddlInfantCount").select2('val',JObject.handling[0].InfantCount);
                                        var seg = 0;
                                        for (var segment = 0; segment < existingEntry.Segments.length; segment++) {
                                            if (segment == 0) {
                                                $("#ctl00_cphTransaction_ddlSector" + parseInt(parseInt(seg) + 1)).select2('val', existingEntry.Segments[segment].Origin.AirportCode);
                                                $("#ctl00_cphTransaction_ddlSector" + parseInt(parseInt(seg) + 1)).trigger('change');
                                                seg++;
                                                $("#ctl00_cphTransaction_ddlSector" + parseInt(parseInt(seg) + 1)).select2('val', existingEntry.Segments[segment].Destination.AirportCode);
                                                $("#ctl00_cphTransaction_ddlSector" + parseInt(parseInt(seg) + 1)).trigger('change');
                                                seg++;
                                            }
                                            else {
                                                $("#ctl00_cphTransaction_ddlSector" + parseInt(parseInt(seg) + 1)).select2('val', existingEntry.Segments[segment].Destination.AirportCode);
                                                $("#ctl00_cphTransaction_ddlSector" + parseInt(parseInt(seg) + 1)).trigger('change');
                                                seg++;
                                            }

                                        }                                        
                                        $("#draftModeDiv").hide();                                        
                                        var paxDetails = [];
                                        $(".paxForm").hide();
                                        $("#paxDetailDiv").show();
                                        $("#paxtable tbody").children().remove();
                                        $.each(existingEntry.Passenger, function (paxInd, paxVal) {
                                            
                                            var keys = $.map(paxTypeObj, function (item, key) {
                                                if (paxVal.Type == item) {
                                                    return key;
                                                }                                                
                                            });
                                            var tickNo = "";
                                            if (JObject.ticket[paxInd].TicketNumber != "NA") {
                                                tickNo=JObject.ticket[paxInd].TicketNumber
                                            }     
                                            
                                            paxDetails.push({
                                                "PaxId": paxVal.PaxId,
                                                "Type": paxVal.Type,
                                                "Title": paxVal.Title,
                                                "FirstName": paxVal.FirstName,
                                                "LastName": paxVal.LastName,
                                                "TicketNumber": tickNo,
                                                "HandlingId": JObject.handling[paxInd].HandlingId,
                                                "TaxBreakUp": JObject.ticket[paxInd].TaxBreakup,
                                                "IsLeadPax": paxVal.IsLeadPax,
                                                "CellPhone": paxVal.CellPhone,
                                                "Email": paxVal.Email,
                                                "Price":PriceObjectCreationForEdit(paxVal.Price,decimalpoint)
                                            });

                                            $("#paxtable tbody").append('<tr><td>' + paxVal.Title + ' ' + paxVal.FirstName + " " + paxVal.LastName + '</td>\
                                                                 <td>'+ $("#ctl00_cphTransaction_paxddlType option[value='" + keys[0] + "']").text() + '</td>\
                                                                 <td><a href="javascript:void(0)" class="ml-3 text-info editPax">\
                                                                        <span class="icon icon-edit"></span>\
                                                                    </a>\</td></tr>');
                                            if (paxVal.IsLeadPax) {
                                                $("#ctl00_cphTransaction_txtEmail").val(paxVal.Email);
                                                $("#ctl00_cphTransaction_txtPhone").val(paxVal.CellPhone);
                                            }
                                        });
                                        $("#ctl00_cphTransaction_hdnPaxDetails").val(JSON.stringify(paxDetails));                                       
                                        $("#ctl00_cphTransaction_ddlCustomer1").attr('disabled', true);
                                        $("#ctl00_cphTransaction_ddlTransactionType").select2('val', JObject.booking.Status);
                                        $("#ctl00_cphTransaction_ddlTransactionType").attr('disabled', true);
                                        
                                        if (JObject.settleMent.length > 0) {
                                            $.each(JObject.settleMent, function (item, valu) {
                                                JObject.settleMent[item]["SettlementName"] = $("#ctl00_cphTransaction_ddlMode option[data-value='" + valu.SettlementType + "']").val();
                                                if (JObject.settleMent[item]["SettlementName"] == '3') {
                                                    $("#tblSettlementDetail tbody").append('<tr><td>' + valu.Amount + ' (Card charge ' + valu.FOPDetail3 + ' )</td>\
                                                                <td>'+ $("#ctl00_cphTransaction_ddlMode option[data-value='" + valu.SettlementType + "']").text() + '</td>\
                                                                <td><a href="#" class="ml-3 text-info editMode">\
									                                    <span class="icon icon-edit"></span>\
								                                    </a>\
								                                    <a href="#" class="ml-3 text-danger deleteMode">\
									                                    <span class="icon icon-delete"></span>\
								                                    </a></td></tr>');
                                                }
                                                else {
                                                    $("#tblSettlementDetail tbody").append('<tr><td>' + valu.Amount + '</td>\
                                                                <td>'+ $("#ctl00_cphTransaction_ddlMode option[data-value='" + valu.SettlementType + "']").text() + '</td>\
                                                                <td><a href="#" class="ml-3 text-info editMode">\
									                                    <span class="icon icon-edit"></span>\
								                                    </a>\
								                                    <a href="#" class="ml-3 text-danger deleteMode">\
									                                    <span class="icon icon-delete"></span>\
								                                    </a></td></tr>');
                                                }

                                            });
                                            $("#ctl00_cphTransaction_hdnsettlementDetails").val(JSON.stringify({ "SettlementObject": JObject.settleMent }));
                                        }
                                          
                                        $("#ctl00_cphTransaction_hdnFlightId").val(existingEntry.FlightId);                                       
                                        $("#ctl00_cphTransaction_hdnBookingId").val(existingEntry.BookingId); 
                                        $("#commissionPercentage").val(paxDetails[0].Price.CommissionValue);
                                        $("#discountPercenntage").val(paxDetails[0].Price.DiscountValue);
                                        $("#commissionPercentage").prop('disabled', true);
                                        $("#discountPercenntage").prop('disabled', true);
                                        FinalCalculation();
                                    }
                                    else {
                                        document.getElementById('ctl00_upProgress').style.display = 'none';
                                    }
                                },
                                error: function () {
                                    alert("Operation failed")
                                    document.getElementById('ctl00_upProgress').style.display = 'none';
                                }
                            });
                        }
                        else {
                            $('#ctl00_cphTransaction_ddlCustomer1').select2('val', logggedInAgent).trigger('change');
                        }
                        function PriceObjectCreationForEdit(priceObj,decimalPnt) {
                            var priceObject = {
                                "PriceId": priceObj.PriceId,
                                "SupplierPrice": parseFloat(parseFloat(priceObj.PublishedFare) + parseFloat(priceObj.Tax)).toFixed(decimalPnt),
                                "PublishedFare": parseFloat(priceObj.PublishedFare).toFixed(decimalPnt),
                                "NetFare": 0,
                                "Tax": parseFloat(priceObj.Tax).toFixed(decimalPnt),
                                "OurCommission": parseFloat(priceObj.OurCommission).toFixed(decimalPnt),
                                "CommissionType": "P",
                                "CommissionValue": parseFloat(priceObj.CommissionValue).toFixed(decimalPnt),
                                "RateOfExchange": parseFloat(priceObj.RateOfExchange).toFixed(decimalPnt),
                                "Markup": parseFloat(priceObj.Markup).toFixed(decimalPnt),
                                "AsvAmount": parseFloat(priceObj.AsvAmount).toFixed(decimalPnt),
                                "AsvElement": priceObj.AsvElement,
                                "Discount": parseFloat(priceObj.Discount).toFixed(decimalPnt),
                                "DiscountType": "P",
                                "DiscountValue": parseFloat(priceObj.DiscountValue).toFixed(decimalPnt),
                                "TransactionFee": parseFloat(priceObj.TransactionFee).toFixed(decimalPnt),
                                "AdditionalTxnFee": parseFloat(priceObj.AdditionalTxnFee).toFixed(decimalPnt),
                                "Currency": priceObj.Currency,
                                "CurrencyCode": priceObj.CurrencyCode,
                                "OurPLB": 0.00,
                                "AgentCommission": 0.00,
                                "AgentPLB": 0.00,
                                "SeviceTax": 0,
                                "InputVATAmount": parseFloat(priceObj.InputVATAmount).toFixed(decimalPnt),
                                "OutputVATAmount": parseFloat(priceObj.OutputVATAmount).toFixed(decimalPnt),
                                "AgentCancellationCharge": parseFloat(priceObj.AgentCancellationCharge).toFixed(decimalPnt),
                                "SupplierCancellationCharge": parseFloat(priceObj.SupplierCancellationCharge).toFixed(decimalPnt),
                                "CancellationInVat": parseFloat(priceObj.CancellationInVat).toFixed(decimalPnt),
                                "CancellationOutVat": parseFloat(priceObj.CancellationOutVat).toFixed(decimalPnt)
                            };
                            return priceObject;
                        }
                        
                        $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-id');

                        $('.add-btn').on('click', function () {
                            $('#EditPNRDiv').show();
                            $('#FareDiff').hide();
                            $('#showParam').collapse('hide');
                            document.getElementById('<%=btnprev.ClientID%>').style.display = 'none';
                        });

                        $('body').on('click', '#hidePanel', function () {
                            $('body').toggleClass('expanded-left-panel');
                        });

                        $('.backToResults').on('click', function () {
                            $('#EditPNRDiv').hide();
                            $('#showParam').collapse('hide');
                        });

                        $('body').addClass('expanded-left-panel');

                        disablefield();

                        $("#addSettlement").on('click', function () {
                            var valid = true;
                            let SettlementDiv = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-id');
                            var amount = $("#" + SettlementDiv + "").find('.amt').val();
                            if (parseFloat(amount) <= 0) {
                                valid = false;
                                toastr.error("Amount should greater than 0");
                                return false;
                            }
                            var SettlementMode = $('#ctl00_cphTransaction_ddlMode').val();
                            var settlementArray = [];
                            var settlementObject = {};
                            if (SettlementMode == '1') {
                                if (amount == "") {
                                    toastr.error('Invalid amount');
                                    valid = false;
                                }
                                else if (isNaN(parseFloat(amount))) {
                                    toastr.error('Please enter valid amount');
                                    valid = false;
                                }
                                else if ($('#cashModalBranch').val() == "-1" || $('#cashModalBranch').val() == "0") {
                                    toastr.error('Please select branch');
                                    valid = false;
                                }
                                if (valid) {
                                    settlementObject["ServiceProductId"] = 1;
                                    settlementObject["ServiceReferenceId"] = 0;
                                    settlementObject["SettlementType"] = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-value');
                                    settlementObject["SettlementName"] = $('#ctl00_cphTransaction_ddlMode').val();
                                    settlementObject["Amount"] = parseFloat(amount);
                                    settlementObject["Currency"] = $('#ctl00_cphTransaction_ddlcurrencycode').val();
                                    settlementObject["FOPDetail1"] = $('#cashModalBranch').val();
                                    settlementObject["FOPDetailRemarks"] = $("#settlementRemarkCash").val();
                                }

                            }
                            else if (SettlementMode == '2') {

                                if (amount == "") {
                                    toastr.error('Invalid amount');
                                    valid = false;
                                }
                                else if (isNaN(parseFloat(amount))) {
                                    toastr.error('Please enter valid amount');
                                    valid = false;
                                }

                                else if ($('#creditModalBranch').val() == "") {
                                    toastr.error('Please enter customer');
                                    valid = false;
                                }

                                if (valid) {
                                    settlementObject["ServiceProductId"] = 1;
                                    settlementObject["ServiceReferenceId"] = 0;
                                    settlementObject["SettlementType"] = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-value');
                                    settlementObject["SettlementName"] = $('#ctl00_cphTransaction_ddlMode').val();
                                    settlementObject["Amount"] = parseFloat(amount);
                                    settlementObject["Currency"] = $('#ctl00_cphTransaction_ddlcurrencycode').val();
                                    settlementObject["FOPDetail1"] = $('#creditModalBranch').val();
                                    //settlementObject["FOPDetail2"] = $('#creditModalBranch option:selected').text();
                                    //settlementObject["FOPDetail3"] = $('#creditModalBranch option:selected').attr('data-activity');
                                    settlementObject["FOPDetailRemarks"] = $("#settlementRemarkCredit").val();
                                }
                            }
                            else if (SettlementMode == '3') {
                                if (amount == "") {
                                    toastr.error('Invalid amount');
                                    valid = false;
                                }
                                else if (isNaN(parseFloat(amount))) {
                                    toastr.error('Please enter valid amount');
                                    valid = false;
                                }
                                else if ($('#cardType').val() == "-1") {
                                    toastr.error('Please select card type');
                                    valid = false;
                                }
                                else if ($('#cardNumber').val() == "") {
                                    toastr.error('Please enter card number');
                                    valid = false;
                                }
                                if (valid) {

                                    settlementObject["ServiceProductId"] = 1;
                                    settlementObject["ServiceReferenceId"] = 0;
                                    settlementObject["SettlementType"] = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-value');
                                    settlementObject["SettlementName"] = $('#ctl00_cphTransaction_ddlMode').val();
                                    settlementObject["Currency"] = $('#ctl00_cphTransaction_ddlcurrencycode').val();
                                    settlementObject["FOPDetail1"] = $('#cardType').val();;
                                    settlementObject["FOPDetail2"] = $('#cardType option:selected').attr('data-rate');
                                    settlementObject["FOPDetail3"] = $('#cardCharge').val();
                                    settlementObject["FOPDetail4"] = $('#cardNumber').val();
                                    settlementObject["FOPDetail5"] = $('#expiryDate').val();
                                    settlementObject["FOPDetail6"] = $('#approvalNumber').val();
                                    settlementObject["Amount"] = parseFloat(amount);
                                    settlementObject["FOPDetailRemarks"] = $("#settlementRemarkCard").val();
                                }

                            }
                            else if (SettlementMode == '10') {
                                if (amount == "") {
                                    toastr.error('Invalid amount');
                                    valid = false;
                                }
                                else if (isNaN(parseFloat(amount))) {
                                    toastr.error('Please enter valid amount');
                                    valid = false;
                                }
                                else if ($('#glModalBranch').val() == "-1" || $('#glModalBranch').val() == "0") {
                                    toastr.error('Please select branch');
                                    valid = false;
                                }
                                else if ($('#glChargeCode').val() == "-1") {
                                    toastr.error('Please select charge');
                                    valid = false;
                                }
                                if (valid) {
                                    settlementObject["ServiceProductId"] = 1;
                                    settlementObject["ServiceReferenceId"] = 0;
                                    settlementObject["SettlementType"] = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-value');
                                    settlementObject["SettlementName"] = $('#ctl00_cphTransaction_ddlMode').val();
                                    settlementObject["Amount"] = parseFloat(amount);
                                    settlementObject["Currency"] = $('#ctl00_cphTransaction_ddlcurrencycode').val();
                                    settlementObject["FOPDetail1"] = $('#glModalBranch').val();
                                    settlementObject["FOPDetail2"] = $('#glChargeCode').val();
                                    settlementObject["FOPDetailRemarks"] = $("#settlementRemarkGl").val();
                                }
                            }
                            else if (SettlementMode == '12') {
                                if (amount == "") {
                                    toastr.error('Invalid amount');
                                    valid = false;
                                }
                                else if (isNaN(parseFloat(amount))) {
                                    toastr.error('Please enter valid amount');
                                    valid = false;
                                }
                                else if ($('#transferType').val() == "-1") {
                                    toastr.error('Please select transfer type');
                                    valid = false;
                                }
                                else if ($("#refNumberEnNumber").val() == "") {
                                    toastr.error('Please enter referene number');
                                    valid = false;
                                }
                                else if ($("#accountNumber").val() == "") {
                                    toastr.error('Please enter account number');
                                    valid = false;
                                }
                                else if ($("#refDate").val() == "") {
                                    toastr.error('Please enter refernce date');
                                    valid = false;
                                }
                                else if ($("#bankName").val() == "-1") {
                                    toastr.error('Please select bank');
                                    valid = false;
                                }
                                if (valid) {
                                    settlementObject["ServiceProductId"] = $("#ctl00_cphTransaction_ddlServiceType").val();
                                    settlementObject["ServiceReferenceId"] = 0;
                                    settlementObject["SettlementType"] = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-value');
                                    settlementObject["SettlementName"] = $('#ctl00_cphTransaction_ddlMode').val();
                                    settlementObject["Amount"] = parseFloat(amount);
                                    settlementObject["Currency"] = $('#ctl00_cphTransaction_ddlcurrencycode').val();
                                    settlementObject["FOPDetail1"] = $('#transferType').val();
                                    settlementObject["FOPDetail2"] = $('#refNumberEnNumber').val();
                                    settlementObject["FOPDetail3"] = $('#accountNumber').val();
                                    settlementObject["FOPDetail4"] = $('#bankName').val();
                                    settlementObject["FOPDetail8"] = $('#refDate').val();;
                                    settlementObject["FOPDetailRemarks"] = $("#settlementRemarkTransfer").val();
                                }

                            }
                            if (valid) {
                                var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                                if (ExistingSettlement != null && ExistingSettlement != "") {
                                    let ExistingJson = JSON.parse(ExistingSettlement);
                                    $.each(ExistingJson.SettlementObject, function (item, value) {
                                        if (value.SettlementName != SettlementMode) {
                                            settlementArray.push(value)
                                        }

                                    });
                                }
                                settlementArray.push(settlementObject);
                                $("#tblSettlementDetail tbody").children().remove();
                                $("#ctl00_cphTransaction_hdnsettlementDetails").val(JSON.stringify({ "SettlementObject": settlementArray }));
                                $('#ctl00_cphTransaction_ddlMode').select2('val', '-1');

                                var ExistingSettlementRecalculate = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                                var recalculateJson = JSON.parse(ExistingSettlementRecalculate);
                                var totalRecivd = 0;
                                var totalDue = 0;
                                var ccCharge = 0;
                                $.each(recalculateJson.SettlementObject, function (item, value) {
                                    let serviceChargeStr = ""
                                    if (value.SettlementName == '3') {
                                        serviceChargeStr = "(Card charge " + value.FOPDetail3 + ")";
                                        ccCharge = parseFloat(value.FOPDetail3).toFixed(decimalpoint);
                                        totalRecivd = parseFloat(parseFloat(totalRecivd) + parseFloat(value.Amount) + parseFloat(ccCharge)).toFixed(decimalpoint);
                                    }
                                    else {
                                        totalRecivd = parseFloat(parseFloat(totalRecivd) + parseFloat(value.Amount)).toFixed(decimalpoint);
                                    }

                                    $("#tblSettlementDetail tbody").append('<tr><td>' + value.Amount + ' ' + serviceChargeStr + '</td>\
                                                                <td>'+ $("#ctl00_cphTransaction_ddlMode option[value='" + value.SettlementName + "']").text() + '</td>\
                                                                <td><a href="#" class="ml-3 text-info editMode">\
									                                    <span class="icon icon-edit"></span>\
								                                    </a>\
								                                    <a href="#" class="ml-3 text-danger deleteMode">\
									                                    <span class="icon icon-delete"></span>\
								                                    </a></td></tr>');
                                });
                                var totalTocollect = parseFloat($("#ctl00_cphTransaction_txtCollected").val()).toFixed(decimalpoint);
                                totalTocollect = parseFloat(parseFloat(totalTocollect) + parseFloat(ccCharge)).toFixed(decimalpoint);
                                totalDue = parseFloat(parseFloat(totalTocollect) - totalRecivd).toFixed(decimalpoint);
                                $("#recievedAmt").val(parseFloat(totalRecivd).toFixed(decimalpoint));
                                $("#dueAmt").val(parseFloat(totalDue).toFixed(decimalpoint));
                                $("#cardChargeAmt").val(parseFloat(ccCharge).toFixed(decimalpoint));
                                $("#totalToCollectAmt").val(parseFloat(totalTocollect).toFixed(decimalpoint));
                                $('#SettlementModalContent').modal('hide');
                                editMode = false;
                            }
                        });

                        $('body').on('click', '.deleteMode', function () {
                            var parentRow = $(this).parents('tr:eq(0)');
                            var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                            if (ExistingSettlement != null && ExistingSettlement != "") {
                                var totalCcCharge = 0;
                                if (ExistingSettlement != null && ExistingSettlement != "") {
                                    let ExistingJsonObj = JSON.parse(ExistingSettlement);
                                    $.each(ExistingJsonObj.SettlementObject, function (item, value) {
                                        if (value.SettlementName == '3') {
                                            totalCcCharge = parseFloat(value.FOPDetail3).toFixed(decimalpoint);
                                        }                               
                                    });
                                }
                                let ExistingJson = JSON.parse(ExistingSettlement);
                                let curObj = ExistingJson.SettlementObject[parentRow[0].rowIndex - 1];
                                var ccCharge = 0;

                                if (curObj.SettlementName == '3') {
                                    ccCharge = parseFloat(curObj.FOPDetail3).toFixed(decimalpoint);
                                }
                                var totalRecivd = 0;
                                var totalDue = 0;
                                var ccCharge2 = 0;
                                $.each(ExistingJson.SettlementObject, function (item, value) {
                                    if (parseInt(item) != parseInt(parentRow[0].rowIndex - 1)) {
                                        if (value.SettlementName == '3') {
                                            ccCharge2 = parseFloat(value.FOPDetail3).toFixed(decimalpoint);
                                            totalRecivd = parseFloat(parseFloat(totalRecivd) + parseFloat(value.Amount) + parseFloat(ccCharge2)).toFixed(decimalpoint);
                                        }
                                        else {
                                            totalRecivd = parseFloat(parseFloat(totalRecivd) + parseFloat(value.Amount)).toFixed(decimalpoint);
                                        }
                                    }
                                });
                                var totalTocollect = parseFloat(parseFloat($("#ctl00_cphTransaction_txtCollected").val()) + parseFloat(totalCcCharge)).toFixed(decimalpoint);
                                totalDue = parseFloat(parseFloat(totalTocollect) - totalRecivd).toFixed(decimalpoint);
                                $("#recievedAmt").val(totalRecivd);
                                $("#dueAmt").val(parseFloat(parseFloat(totalDue) - parseFloat(ccCharge)).toFixed(decimalpoint));
                                $("#cardChargeAmt").val(parseFloat($("#cardChargeAmt").val()) - ccCharge);
                                $("#totalToCollectAmt").val(parseFloat(totalTocollect - ccCharge).toFixed(decimalpoint));
                                ExistingJson.SettlementObject.splice(parentRow[0].rowIndex - 1, 1);

                                $("#ctl00_cphTransaction_hdnsettlementDetails").val(JSON.stringify({ "SettlementObject": ExistingJson.SettlementObject }));
                                $(this).parents('tr:eq(0)').remove();
                            }
                        });
                        var editMode = false;
                        var maxAdd = 0;
                        $('body').on('click', '.editMode', function () {
                            editMode = true;
                            var parentRow = $(this).parents('tr:eq(0)');
                            var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                            var currentSum = 0;
                            if (ExistingSettlement != null && ExistingSettlement != "") {
                                let ExistingJson = JSON.parse(ExistingSettlement);
                                $.grep(ExistingJson.SettlementObject, function (item) {
                                    currentSum = parseFloat(parseFloat(currentSum) + parseFloat(item.Amount));
                                });
                                maxAdd = parseFloat(parseFloat($("#ctl00_cphTransaction_txtcashAmount").val()) - parseFloat(currentSum));
                                currentSum = parseFloat(parseFloat(currentSum) - parseFloat(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].Amount));
                                var OpenDivId = $('#ctl00_cphTransaction_ddlMode option[value="' + ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName + '"]').attr('data-id');
                                $('#SettlementModalContent .modal-body').hide();
                                $("#settlementModeText").text($('#ctl00_cphTransaction_ddlMode option[value="' + ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName + '"]').text());
                                $('#SettlementModalContent').modal('show');
                                $('#' + OpenDivId + '').show();
                                $('#ctl00_cphTransaction_ddlMode').select2('val', ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName);
                                editamt = ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].Amount;
                                if (ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName == '3') {
                                    editCharge = ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail3;
                                }
                                if (ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName == "1") {
                                    $("#SettCashModal .amt").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].Amount);
                                    $("#cashModalBranch").select2('val', ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail1);
                                    $("#settlementRemarkCash").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetailRemarks);
                                }
                                else if (ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName == "2") {
                                    $("#SettCreditModal .amt").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].Amount);
                                    $("#creditModalBranch").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail1);
                                    $("#settlementRemarkCredit").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetailRemarks);
                                }
                                else if (ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName == "3") {
                                    $("#SettCardModal .amt").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].Amount);
                                    $("#cardType").select2('val', ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail1);
                                    $("#cardNumber").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail4);
                                    $("#approvalNumber").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail6);
                                    $("#expiryDate").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail5);
                                    $("#settlementRemarkCard").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetailRemarks);
                                    $('#cardCharge').val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail3);
                                    var totAmt = parseFloat(parseFloat(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].Amount) + parseFloat($("#cardCharge").val())).toFixed(decimalpoint);
                                    $("#lblIncludeCharge").text(totAmt + " will be deducted from card");
                                }
                            }
                        });

                        $('.amt').on('change', function () {
                            if (parseFloat($(this).val()) == 0) {
                                toastr.error('Amount should greater than 0');
                                $(this).val(0);
                                return false;
                            }
                            var SettlementMode = $('#ctl00_cphTransaction_ddlMode').val();
                            var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                            var currentSum = 0;
                            var cardCharge = 0;
                            if (ExistingSettlement != null && ExistingSettlement != "") {
                                let ExistingJson = JSON.parse(ExistingSettlement);                                
                                if (!editMode) {
                                    $.grep(ExistingJson.SettlementObject, function (item) {
                                        if (item.SettlementName != SettlementMode) {
                                            currentSum = parseFloat(parseFloat(currentSum) + parseFloat(item.Amount));
                                            if (item.SettlementName == "3") {
                                                cardCharge = parseFloat(parseFloat(cardCharge) + parseFloat(item.FOPDetail3))
                                            }
                                        }
                                    });
                                }
                                else {
                                    if (maxAdd >= 0) {
                                        $.grep(ExistingJson.SettlementObject, function (item) {
                                            if (item.SettlementName != SettlementMode) {
                                                currentSum = parseFloat(parseFloat(currentSum) + parseFloat(item.Amount));
                                                if (item.SettlementName == "3") {
                                                    cardCharge = parseFloat(parseFloat(cardCharge) + parseFloat(item.FOPDetail3))
                                                }
                                            }
                                        });
                                    }
                                    if (parseFloat(parseFloat($(this).val()).toFixed(decimalpoint)) > parseFloat(parseFloat($("#ctl00_cphTransaction_txtcashAmount").val()).toFixed(decimalpoint))) {
                                        toastr.error('Cannot enter amount greater than total collected amount');
                                        $(this).val(0);
                                        return false;
                                    }
                                }
                            }
                            currentSum = parseFloat(parseFloat(currentSum) + eval(cardCharge) + parseFloat($(this).val()));
                            var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                            var totalSellingFare = 0.00;
                            var totalTax = 0.00;
                            var totalInvat = 0.00;
                            var totalOutVat = 0.00;
                            var profitValue = 0.00;
                            var totalDiscount = 0.00;
                            var cancellationAmt = 0.00;
                            var collectedAmount = 0.00;
                            $.each(paxDetails, function (finalInd, finalVal) {
                                totalSellingFare = parseFloat(parseFloat(totalSellingFare) + parseFloat(finalVal.Price.PublishedFare)).toFixed(decimalpoint);
                                totalTax = parseFloat(parseFloat(totalTax) + parseFloat(finalVal.Price.Tax)).toFixed(decimalpoint);
                                totalInvat = parseFloat(parseFloat(totalInvat) + parseFloat(finalVal.Price.InputVATAmount)).toFixed(decimalpoint);
                                totalOutVat = parseFloat(parseFloat(totalOutVat) + parseFloat(finalVal.Price.OutputVATAmount)).toFixed(decimalpoint);
                                profitValue = parseFloat(parseFloat(profitValue) + parseFloat(finalVal.Price.Markup)
                                    + parseFloat(finalVal.Price.AsvAmount) + parseFloat(finalVal.Price.TransactionFee)
                                    + parseFloat(finalVal.Price.AdditionalTxnFee)).toFixed(decimalpoint);
                                totalDiscount = parseFloat(parseFloat(totalDiscount) + parseFloat(finalVal.Price.Discount)).toFixed(decimalpoint);
                                if ($("#ctl00_cphTransaction_ddlTransactionType").val() == "16") {
                                    cancellationAmt = parseFloat(parseFloat(cancellationAmt) + parseFloat(finalVal.Price.AgentCancellationCharge)).toFixed(decimalpoint);
                                    cancellationAmt = parseFloat(parseFloat(cancellationAmt) + parseFloat(finalVal.Price.CancellationOutVat)).toFixed(decimalpoint);
                                    cancellationAmt = parseFloat(parseFloat(cancellationAmt) + parseFloat(finalVal.Price.SupplierCancellationCharge)).toFixed(decimalpoint);
                                    cancellationAmt = parseFloat(parseFloat(cancellationAmt) + parseFloat(finalVal.Price.CancellationInVat)).toFixed(decimalpoint);
                                }
                            });
                            collectedAmount = eval(totalSellingFare) + eval(totalTax) + eval(profitValue)
                                + eval(totalInvat) + eval(totalOutVat) - eval(totalDiscount) - eval(cancellationAmt) + eval(cardCharge);

                            if (isNaN(collectedAmount)) {
                                collectedAmount = 0.00;
                            }
                           
                            if (parseFloat(currentSum.toFixed(decimalpoint)) > parseFloat(collectedAmount.toFixed(decimalpoint))) {
                                toastr.error('Invalid amount. Please check Collected amount');
                                $(this).val(0);
                                return false;
                            }
                            else {
                                if ($("#ctl00_cphTransaction_ddlMode").val() == '3') {
                                    $("#cardType").trigger('change');
                                }
                                $(this).val(parseFloat($(this).val()).toFixed(decimalpoint));
                            }

                        });

                        function loadCreditCardData() {
                            $.ajax({
                                url: "OfflineEntry.aspx/LoadCreditCard",
                                type: "POST",
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (cardResponse) {
                                    let cardData = JSON.parse(cardResponse.d)
                                    if (cardData.length > 0) {
                                        $("#cardType").append('<option value="-1">--Select Card--</option>')
                                        $.each(cardData, function (cardIndex, cardValue) {
                                            $("#cardType").append('<option value="' + cardValue.card_id + '" data-rate="' + cardValue.card_charge + '">' + cardValue.card_name + '</option>');
                                        });
                                    }
                                },
                                error: function () {

                                }
                            });
                        }

                        function loadTransferTypeData() {
                            $.ajax({
                                url: "OfflineEntry.aspx/LoadTransferType",
                                type: "POST",
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (cardResponse) {
                                    let cardData = JSON.parse(cardResponse.d)
                                    $("#transferType").append('<option value="-1" selected>--Select Transfer Type--</option>')
                                    if (cardData.length > 0) {
                                        $.each(cardData, function (transferIndex, transferValue) {
                                            $("#transferType").append('<option value="' + transferValue.FIELD_ID + '" >' + transferValue.FIELD_TEXT + '</option>');
                                        });
                                    }
                                },
                                error: function () {

                                }
                            });
                        }

                        function loadBankData() {
                            $.ajax({
                                url: "OfflineEntry.aspx/LoadBankData",
                                type: "POST",
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (bankResponse) {
                                    let bankData = JSON.parse(bankResponse.d)
                                    $("#bankName").append('<option value="-1" selected>--Select Transfer Type--</option>')
                                    if (bankData.length > 0) {
                                        $.each(bankData, function (bankIndex, bankValue) {
                                            $("#bankName").append('<option value="' + bankValue.BANK_ID + '" >' + bankValue.BANK_NAME + '</option>');
                                        });
                                    }
                                },
                                error: function () {

                                }
                            });
                        }

                        loadCreditCardData();

                        loadTransferTypeData();

                        //loadBankData();

                        $("#cardType").on('change', function () {
                            if ($(this).val() != "-1") {
                                let SettlementDiv = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-id');
                                var amount = $("#" + SettlementDiv + "").find('.amt').val();
                                if (amount != "") {
                                    if ($("#ctl00_cphTransaction_ddlTransactionType").val() != "16") {
                                        $("#cardCharge").val(parseFloat(parseFloat(amount) * parseFloat(parseFloat($("#cardType option:selected").attr('data-rate')) / 100)).toFixed(decimalpoint));
                                        var totAmt = parseFloat(parseFloat(amount) + parseFloat($("#cardCharge").val())).toFixed(decimalpoint);
                                        $("#lblIncludeCharge").text(totAmt + " will be deducted from card")

                                    }
                                    else {
                                        $("#cardCharge").val(0.00);
                                    }

                                }
                            }
                            else {
                                $("#cardCharge").val(0.00);
                                $("#lblIncludeCharge").text("");
                            }

                        });

                        $("#ctl00_cphTransaction_ddlTransactionType").on('change', function () {
                            if ($(this).val() == "16") {
                                if (document.getElementById('<%=hdnlocCountryCode.ClientID%>').value == "IN") {
                                    $("#agentTaxlbl").text('Agent GST Amount');
                                    $("#supplierTaxlbl").text('Supplier GST Amount');
                                    $("#lblTotalCancelOpVat").text('Agent GST Amount');
                                    $("#lblTotalCancelIpVat").text('Supplier GST Amount');
                                }
                                else {
                                    $("#agentTaxlbl").text('Cancellation O/P VAT');
                                    $("#supplierTaxlbl").text('Cancellation I/P VAT');
                                    $("#lblTotalCancelOpVat").text('Cancellation O/P VAT');
                                    $("#lblTotalCancelIpVat").text('Cancellation I/P VAT');
                                }
                                $(".cancelCharges").css('display', 'block');
                            }
                            else {
                                if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                    var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                    if (paxDetails.length > 0) {
                                        var newPaxDetails = [];
                                        $.each(paxDetails, function (paxInd, paxVal) {
                                            newPaxDetails.push(paxVal);
                                            newPaxDetails[paxInd].Price.AgentCancellationCharge = 0.00;
                                            newPaxDetails[paxInd].Price.SupplierCancellationCharge = 0.00;
                                            newPaxDetails[paxInd].Price.CancellationInVat = 0.00;
                                            newPaxDetails[paxInd].Price.CancellationOutVat = 0.00;                                            
                                        });
                                        $("#ctl00_cphTransaction_hdnPaxDetails").val(JSON.stringify(newPaxDetails));                                        
                                    }
                                    
                                }                                
                                $(".cancelCharges").css('display', 'none');
                            }                          
                            
                            if ($(this).val() != "5") {
                                $("#draftModeDiv").hide();
                                $("#chkDraftmode").prop('checked', false);
                                $("#chkDraftmode").trigger('change');
                                $("#ctl00_cphTransaction_ddlSaleType").select2('val', 'N');
                                $("#ctl00_cphTransaction_ddlSaleType").attr('disabled', true);
                            }
                            else {
                                $("#draftModeDiv").show();
                                $("#ctl00_cphTransaction_ddlSaleType").select2('val', 'N');
                                $("#ctl00_cphTransaction_ddlSaleType").attr('disabled', false);

                            }
                            if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                if (paxDetails.length > 0) {
                                    FinalCalculation();
                                }
                            }
                        });

                        $("#paxAgentCancelAmount").on('change', function () {    
                            var canceloutputVATcharge = 0.00;
                            var decimalpoint = eval('<%=agentDecimalPoints %>');
                            var origin = document.getElementById('<%=ddlSector1.ClientID%>').value;
                            var destination = document.getElementById('<%=ddldestination.ClientID%>').value;
                            var totMarkup = 0;
                            if (origin == "-1") {
                                toastr.error('Please select Sector 1');
                                $(this).val("0.00");
                                $("#paxCancelOPVat").val("0.00");
                                $("#ctl00_cphTransaction_hdnCancelOPVat").val("0.00");
                                return false;
                            }
                            else if (destination == '-1') {
                                toastr.error('Please select destination');
                                $(this).val("0.00");
                                $("#paxCancelOPVat").val("0.00");
                                $("#ctl00_cphTransaction_hdnCancelOPVat").val("0.00");
                                return false;
                            }
                            else if (eval($(this).val()) < 0 || $(this).val() == "") {
                                toastr.error('Please enter agent charge');
                                $("#paxCancelOPVat").val("0.00");
                                $("#ctl00_cphTransaction_hdnCancelOPVat").val("0.00");
                                $(this).val("0.00");
                                return false;
                            }
                            if (eval($(this).val()) > 0 && origin != '' && origin != '-1' && destination != '' && destination != '-1') {
                                var paramList = '';
                                
                                if (document.getElementById('<%=hdnlocCountryCode.ClientID%>').value == "IN") {
                                    var locationId = document.getElementById('<%=hndLocationId.ClientID%>').value;
                                    paramList = 'decimalpoint=' + decimalpoint + '&locationId=' + locationId + '&totMarkup=' + $(this).val() + '&taxType=GST';
                                }
                                else {
                                    paramList = 'sellingFair=' + $(this).val() + '&decimalpoint=' + decimalpoint + '&origin=' + origin + '&destination=' + destination + '&totMarkup=' + totMarkup;
                                }
                                
                                Ajax.onreadystatechange = function () {
                                    
                                    if (Ajax.readyState == 4) {
                                        if (Ajax.status == 200) {
                                            var arrayType = "";
                                            
                                            if (Ajax.responseText.length > 0) {
                                                if (document.getElementById('<%=hdnlocCountryCode.ClientID%>').value == "IN") {
                                                    var GSTamount = 0;
                                                    GSTamount = Ajax.responseText;
                                                    
                                                    document.getElementById('paxCancelOPVat').value = parseFloat(GSTamount).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                                                    document.getElementById('ctl00_cphTransaction_hdnCancelOPVat').value = parseFloat(GSTamount).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                                                }
                                                else {
                                                    var inputVATchargeCancel = 0;
                                                    var inputVATcostIncludedCancel = "";
                                                    var outputVATchargeCancel = 0;
                                                    var outputVATappliedOnCancel = "";
                                                    arrayType = Ajax.responseText.split('|');
                                                    if (Ajax.responseText.indexOf('#') != "-1") {
                                                        //alert(Ajax.responseText);                                        
                                                        arrayType = Ajax.responseText.split('#');
                                                        if (arrayType[0].indexOf('I') == 0) {
                                                            inputVATchargeCancel = arrayType[0].split('|')[0].replace('I', '');
                                                            inputVATcostIncludedCancel = arrayType[0].split('|')[1];
                                                        }
                                                        if (arrayType[1].length > 1 && arrayType[1].indexOf('O') == 0) {
                                                            outputVATchargeCancel = arrayType[1].split('|')[0].replace('O', '');
                                                            outputVATappliedOnCancel = arrayType[1].split('|')[1];
                                                        }
                                                    }
                                                    else {
                                                        if (Ajax.responseText != "") {
                                                            arrayType = Ajax.responseText.split('|');
                                                            if (arrayType[0].indexOf('I') == 0) {
                                                                inputVATchargeCancel = arrayType[0];
                                                                inputVATcostIncludedCancel = arrayType[1];
                                                            }
                                                            if (arrayType[1].indexOf('O') == 0) {
                                                                outputVATchargeCancel = arrayType[0];
                                                                outputVATappliedOnCancel = arrayType[1];
                                                            }
                                                        }
                                                    }
                                                    if (outputVATchargeCancel > 0 || inputVATchargeCancel > 0) {
                                                        var collAmount = document.getElementById("paxAgentCancelAmount").value.replace(/,/g, '');
                                                        canceloutputVATcharge = eval(collAmount) * (outputVATchargeCancel / 100);                                                        
                                                        $("#paxCancelOPVat").val(parseFloat(canceloutputVATcharge).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') }));
                                                        $("#ctl00_cphTransaction_hdnCancelOPVat").val(parseFloat(canceloutputVATcharge).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') }));
                                                    }
                                                }
                                            }
                                            else {
                                                $("#paxCancelOPVat").val("0.00");
                                                $("#ctl00_cphTransaction_hdnCancelOPVat").val("0.00");
                                            }
                                        }
                                    }
                                };
                                Ajax.open("POST", "OffLineEntryAjax.aspx", false);
                                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                                Ajax.send(paramList);
                            }

                        });

                        $("#paxSupplierCancelAmount").on('change', function () {
                            var cancelinputVATcharge = 0.00;
                            var decimalpoint = eval('<%=agentDecimalPoints %>');
                            var origin = document.getElementById('<%=ddlSector1.ClientID%>').value;
                            var destination = document.getElementById('<%=ddldestination.ClientID%>').value;
                            var totMarkup = 0;
                            if (origin == "-1") {
                                toastr.error('Please select Sector 1');
                                $("#paxCancelIPVat").val("0.00");
                                $('#ctl00_cphTransaction_hdnCancelIPVat').val("0.00");
                                $(this).val("0.00");
                                return false;
                            }
                            else if (destination == '-1') {
                                toastr.error('Please select destination');
                                $("#paxCancelIPVat").val("0.00");
                                $('#ctl00_cphTransaction_hdnCancelIPVat').val("0.00");
                                $(this).val("0.00");
                                return false;
                            }
                            else if (eval($(this).val()) < 0 || $(this).val() == "") {
                                toastr.error('Please enter supplier charge');
                                $("#paxCancelIPVat").val("0.00");
                                $('#ctl00_cphTransaction_hdnCancelIPVat').val("0.00");
                                $(this).val("0.00");
                                return false;
                            }
                            if (eval($(this).val()) > 0 && origin != '' && origin != '-1' && destination != '' && destination != '-1') {
                                var paramList = '';
                                if (document.getElementById('<%=hdnlocCountryCode.ClientID%>').value == "IN") {
                                    var locationId = document.getElementById('<%=hndLocationId.ClientID%>').value;
                                    paramList = 'decimalpoint=' + decimalpoint + '&locationId=' + locationId + '&totMarkup=' + $(this).val() + '&taxType=GST';
                                }
                                else {
                                    paramList = 'sellingFair=' + $(this).val() + '&decimalpoint=' + decimalpoint + '&origin=' + origin + '&destination=' + destination + '&totMarkup=' + totMarkup;
                                }
                                
                                Ajax.onreadystatechange = function () {
                                    if (Ajax.readyState == 4) {
                                        if (Ajax.status == 200) {
                                            var arrayType = "";
                                            if (Ajax.responseText.length > 0) {
                                                if (document.getElementById('<%=hdnlocCountryCode.ClientID%>').value == "IN") {
                                                    var GSTamount = 0;
                                                    GSTamount = Ajax.responseText;
                                                    
                                                    document.getElementById('paxCancelIPVat').value = parseFloat(GSTamount).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                                                    document.getElementById('ctl00_cphTransaction_hdnCancelIPVat').value = parseFloat(GSTamount).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') });
                                                }
                                                else {

                                                    var inputVATchargeCancel = 0;
                                                    var inputVATcostIncludedCancel = "";
                                                    var outputVATchargeCancel = 0;
                                                    var outputVATappliedOnCancel = "";
                                                    arrayType = Ajax.responseText.split('|');
                                                    if (Ajax.responseText.indexOf('#') != "-1") {
                                                        //alert(Ajax.responseText);                                        
                                                        arrayType = Ajax.responseText.split('#');
                                                        if (arrayType[0].indexOf('I') == 0) {
                                                            inputVATchargeCancel = arrayType[0].split('|')[0].replace('I', '');
                                                            inputVATcostIncludedCancel = arrayType[0].split('|')[1];
                                                        }
                                                        if (arrayType[1].length > 1 && arrayType[1].indexOf('O') == 0) {
                                                            outputVATchargeCancel = arrayType[1].split('|')[0].replace('O', '');
                                                            outputVATappliedOnCancel = arrayType[1].split('|')[1];
                                                        }
                                                    }
                                                    else {
                                                        if (Ajax.responseText != "") {
                                                            arrayType = Ajax.responseText.split('|');
                                                            if (arrayType[0].indexOf('I') == 0) {
                                                                inputVATchargeCancel = arrayType[0];
                                                                inputVATcostIncludedCancel = arrayType[1];
                                                            }
                                                            if (arrayType[1].indexOf('O') == 0) {
                                                                outputVATchargeCancel = arrayType[0];
                                                                outputVATappliedOnCancel = arrayType[1];
                                                            }
                                                            if (inputVATcostIncludedCancel == "false") {
                                                                var totFair = eval(document.getElementById('paxSupplierCancelAmount').value.replace(/,/g, ''));
                                                                cancelinputVATcharge = totFair * (inputVATchargeCancel / 100);
                                                                $("#paxCancelIPVat").val(parseFloat(cancelinputVATcharge).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') }));
                                                                $('#ctl00_cphTransaction_hdnCancelIPVat').val(parseFloat(cancelinputVATcharge).toLocaleString('en-AE', { minimumFractionDigits: eval('<%=agentDecimalPoints %>'), maximumFractionDigits: eval('<%=agentDecimalPoints %>') }));
                                                            }
                                                        }
                                                    }

                                                }
                                            }
                                            else {
                                                $("#paxCancelIPVat").val("0.00");
                                                $('#ctl00_cphTransaction_hdnCancelIPVat').val("0.00");
                                            }
                                        }
                                    }
                                };
                                Ajax.open("POST", "OffLineEntryAjax", false);
                                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                                Ajax.send(paramList);
                            }
                        });
                        $("#getHandlingFee").on('click', function () {
                            var serviceId = $("#ctl00_cphTransaction_ddlServiceType").val();
                            if (serviceId != "-1") {
                                document.getElementById('ctl00_upProgress').style.display = 'block';
                                $.ajax({
                                    url: "OfflineEntry.aspx/LoadHandlingFee",
                                    type: "POST",
                                    dataType: "json",
                                    data: JSON.stringify({ "serviceId": serviceId }),
                                    contentType: "application/json; charset=utf-8",
                                    success: function (bankResponse) {
                                        $("#HandlingFeehModalBody").children().remove();
                                        let handlingFeeData = JSON.parse(bankResponse.d);
                                        if (handlingFeeData.length > 0) {
                                            var nextRow = true;
                                            var HtmlString = "";
                                            $.each(handlingFeeData, function (feeIndex, feeValue) {
                                                if (nextRow) {
                                                    if (feeValue.handling_fee_status == 'N') {
                                                        HtmlString = '<div class="row"><div class="col-6">\<input type="radio" name="handlingFee_radio" value="' + feeValue.handling_id + '" data-charge="' + feeValue.handling_fee + '" />\
                                                                      <label>' + feeValue.handling_element + '</label></div>';
                                                    }
                                                    else {
                                                        HtmlString = '<div class="row"><div class="col-6">\<input type="radio" name="handlingFee_radio" checked="checked" value="' + feeValue.handling_id + '" data-charge="' + feeValue.handling_fee + '" />\
                                                                      <label>' + feeValue.handling_element + '</label></div>';
                                                    }
                                                    nextRow = false;
                                                }
                                                else {
                                                    HtmlString += '<div class="col-6">\<input type="radio" name="handlingFee_radio" value="' + feeValue.handling_id + '" data-charge="' + feeValue.handling_fee + '" />\
                                                                      <label>' + feeValue.handling_element + '</label></div></div>';
                                                    nextRow = true;
                                                }
                                            });
                                            $("#HandlingFeehModalBody").append(HtmlString);
                                            $("#handlingFeeModal").modal('show');
                                            document.getElementById('ctl00_upProgress').style.display = 'none';
                                        }
                                        else {
                                            document.getElementById('ctl00_upProgress').style.display = 'none';
                                            alert("Handling fee not available")
                                        }
                                    },
                                    error: function () {
                                        document.getElementById('ctl00_upProgress').style.display = 'none';
                                        alert("Handling fee not available")
                                    }
                                });
                            }
                            else {
                                toastr.error("Please select service");
                            }
                        });

                        $("#addHandlingFee").on('click', function () {
                            if ($('input[name="handlingFee_radio"]:checked').length > 0) {
                                $("#ctl00_cphTransaction_handlingFee").val($('input[name="handlingFee_radio"]:checked').val());
                                $("#ctl00_cphTransaction_hdnhandlingFeeCharge").val($('input[name="handlingFee_radio"]:checked').attr('data-charge'));
                                $("#paxMarkUp").val(parseFloat($('input[name="handlingFee_radio"]:checked').attr('data-charge')).toFixed(decimalpoint));
                                //calculateProfit();
                                $("#handlingFeeModal").modal('hide');
                                
                            }
                            else {
                                toastr.error("Please select handling fee");
                            }
                        });

                        $("#ctl00_cphTransaction_ddlSalesExecutive").on('change', function () {
                            if ($(this).val() != "-1") {
                                $("#ctl00_cphTransaction_hdnsalesExecutive").val($("#ctl00_cphTransaction_ddlSalesExecutive").val());
                            }
                            else {
                                $("#ctl00_cphTransaction_hdnsalesExecutive").val(0);
                            }

                        });

                        $("#paxMarkUp").on('change', function () {

                            if (parseFloat($("#ctl00_cphTransaction_handlingFee").val()) != 0) {

                                if (parseFloat($(this).val()) < parseFloat($("#ctl00_cphTransaction_hdnhandlingFeeCharge").val()) || $(this).val() == "") {
                                    toastr.error("Amount 1 should greater than or equal " + $("#ctl00_cphTransaction_hdnhandlingFeeCharge").val() + "");
                                    $(this).val($("#ctl00_cphTransaction_hdnhandlingFeeCharge").val());                                    
                                }                                
                            }
                            else {
                                toastr.error("Please select handling fee element");
                                $(this).val("0.00");
                            }

                        });

                        $("#ctl00_cphTransaction_txtEmail").on('change', function () {
                            var emailStr = $(this).val();
                            var testEmail = /^[A-Z0-9._%+-]+@([A-Z0-9-]+\.)+[A-Z]{2,4}$/i;
                            if (!testEmail.test(emailStr)) {
                                $(this).val("");
                                toastr.error("Invalid email id");
                                return false;
                            }
                            else {
                                if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                    var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                    if (paxDetails.length > 0) {
                                        $.each(paxDetails, function (ind, paxval) {
                                            paxDetails[ind].Email = $("#ctl00_cphTransaction_txtEmail").val();
                                        });
                                        $("#ctl00_cphTransaction_hdnPaxDetails").val(JSON.stringify(paxDetails));
                                    }
                                }

                            }
                        });
                        $("#ctl00_cphTransaction_txtPhone").on('change', function () {
                            if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                    if (paxDetails.length > 0) {
                                        $.each(paxDetails, function (ind, paxval) {
                                            paxDetails[ind].CellPhone = $("#ctl00_cphTransaction_txtPhone").val();
                                        });
                                        $("#ctl00_cphTransaction_hdnPaxDetails").val(JSON.stringify(paxDetails));
                                    }
                                }
                        });

                        $("#chkDraftmode").on('change', function () {
                            if ($(this).prop('checked')) {
                                $("#<%=isEditable.ClientID%>").val("1");
                                $(".draftModeOff").not("#ConjDetailsCollapse").hide();
                                $("a[aria-controls='ConjDetailsCollapse']").attr('aria-expanded', false);
                                $("a[aria-controls='ConjDetailsCollapse']").find('i').removeClass('fa-plus-circle').addClass('fa-minus-circle');
                                $("#ConjDetailsCollapse").removeClass('in');
                                $("#ConjDetailsCollapse").css('min-height', '0px');
                                $("#sectortab").hide();
                                $("#UDIDtab").hide();
                                $("#ctl00_cphTransaction_btnnext").hide();
                                $("#ctl00_cphTransaction_btnprev").hide();
                                $("#ctl00_cphTransaction_btnSave").show();
                                $("#ctl00_cphTransaction_ddlTransactionType option[value='16']").attr('disabled', true);
                                $("#ctl00_cphTransaction_ddlTransactionType").select2('val', '5');
                            }
                            else {
                                $("#<%=isEditable.ClientID%>").val("0");
                                $(".draftModeOff").not("#ConjDetailsCollapse").show();
                                $("a[aria-controls='ConjDetailsCollapse']").attr('aria-expanded', true);
                                //$("a[aria-controls='ConjDetailsCollapse']").trigger('click');
                                $("a[aria-controls='ConjDetailsCollapse']").find('i').removeClass('fa-minus-circle').addClass('fa-plus-circle');
                                $("#ConjDetailsCollapse").addClass('in');
                                $("#ConjDetailsCollapse").css('min-height', '100px');
                                $("#sectortab").show();
                                $("#UDIDtab").show();
                                $("#ctl00_cphTransaction_btnnext").show();
                                $("#ctl00_cphTransaction_btnprev").hide();
                                $("#ctl00_cphTransaction_btnSave").hide();
                                $("#ctl00_cphTransaction_ddlTransactionType option[value='16']").attr('disabled', false);
                            }
                        });

                        $("#closeModal,#closepopUp").on('click', function () {
                            $("#ctl00_cphTransaction_ddlMode").select2('val', '-1');
                            $("#SettlementModalContent").modal('hide');
                            editMode = false;
                            maxAdd = 0;
                        });
                        var isRecalculate = false;
                        var recalculateIndx = 0;
                        var isPaxEdit = false;
                        var paxIndex = 0;
                        var inputPaxVATcharge = 0;
                        var outputPaxVATcharge = 0;
                        
                        $("#addNewPaxDetails").on('click', function () {
                            var validatePax = true;         
                            if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                var adultCount = $("#ctl00_cphTransaction_ddlAdultCount").val();
                                var childCount = $("#ctl00_cphTransaction_ddlChildCount").val();
                                var infantCount = $("#ctl00_cphTransaction_ddlInfantCount").val();
                                var totalPax = parseInt(parseInt(adultCount) + parseInt(childCount) + parseInt(infantCount));
                                var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                if (totalPax <= paxDetails.length) {
                                    toastr.error('Please check pax count');
                                    validatePax = false;
                                }                                
                            }
                            if (validatePax) {
                                $(".paxForm").show();
                                ClearPaxFields();
                            }
                            else {
                                $(".paxForm").hide();
                            }
                        });
                        
                        $("#addPaxDetails").on('click', function (e) {
                            e.preventDefault();
                            if (validatePaxDetails()) {
                                var paxDetails = [];
                                var ticketDetails = [];
                                var Type = $("#ctl00_cphTransaction_paxddlType").val();
                                var Title = $("#ctl00_cphTransaction_paxddlPaxTitle").val();
                                var nameText = $("#paxtxtPaxName").val();
                                var ticketNumber = $("#paxTicketNumber").val();
                                var splitName = nameText.split('/');
                                let fname = splitName[0];
                                let lname = splitName[1];
                                if (typeof (lname) === "undefined") {
                                    lname = "";
                                }
                                var handlingFeeId = $("#ctl00_cphTransaction_handlingFee").val();
                                var taxBrakeUp = [];
                                $.each($(".taxBindDiv"), function (ind, taxValue) {
                                    let Key = $(taxValue).attr("data-code");
                                    let Valu = parseFloat($(taxValue).attr("data-value")).toFixed(decimalpoint);
                                    let breakUp = {};
                                    breakUp["Key"] = Key;
                                    breakUp["Value"] =parseFloat(Valu);
                                    taxBrakeUp.push(breakUp);
                                });
                                $("#paxDetailDiv").show();
                                if ($("#ctl00_cphTransaction_hdnPaxDetails").val() == "") {
                                    paxDetails.push({
                                        "PaxId":0,
                                        "Type": paxTypeObj[Type],
                                        "Title": Title,
                                        "FirstName": fname,
                                        "LastName": lname,
                                        "TicketNumber": ticketNumber,
                                        "HandlingId": handlingFeeId,
                                        "TaxBreakUp": taxBrakeUp,
                                        "IsLeadPax": true,
                                        "CellPhone": $("#ctl00_cphTransaction_txtPhone").val(),
                                        "Email":$("#ctl00_cphTransaction_txtEmail").val(),
                                        "Price": paxWiseCalculation()
                                    });
                                    
                                    $("#paxtable tbody").children().remove();
                                    $("#paxtable tbody").append('<tr><td>' + $("#ctl00_cphTransaction_paxddlPaxTitle").val() + ' ' + fname + " " + lname + '</td>\
                                                                 <td>'+ $("#ctl00_cphTransaction_paxddlType option[value='" + Type + "']").text() + '</td>\
                                                                 <td><a href="javascript:void(0)" class="ml-3 text-info editPax">\
                                                                        <span class="icon icon-edit"></span>\
                                                                    </a>\
                                                                    <a href="javascript:void(0)" class="ml-3 text-danger removePax">\
                                                                        <span class="icon icon-delete"></span>\
                                                                    </a></td></tr>');
                                }
                                else {
                                    paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());

                                    if (isPaxEdit) {
                                        var pxId = 0;
                                        if (paxDetails[paxIndex - 1].PaxId != 0) {
                                            pxId = paxDetails[paxIndex - 1].PaxId;
                                        }
                                        let editedPaxObj = {
                                            "PaxId": pxId,
                                            "Type": paxTypeObj[Type],
                                            "Title": Title,
                                            "FirstName": fname,
                                            "LastName": lname,
                                            "TicketNumber": ticketNumber,
                                            "HandlingId": handlingFeeId,
                                            "TaxBreakUp": taxBrakeUp,
                                            "IsLeadPax": paxDetails[paxIndex - 1].IsLeadPax,
                                            "CellPhone": $("#ctl00_cphTransaction_txtPhone").val(),
                                            "Email": $("#ctl00_cphTransaction_txtEmail").val(),
                                            "Price": paxWiseCalculation()
                                        }
                                        paxDetails[paxIndex - 1] = editedPaxObj;                                        
                                        $("#paxtable tbody").find('tr:eq(' + parseInt(paxIndex - 1) + ')').find('td:eq(0)').text($("#ctl00_cphTransaction_paxddlPaxTitle").val() + fname + " " + lname);
                                        $("#paxtable tbody").find('tr:eq(' + parseInt(paxIndex - 1) + ')').find('td:eq(1)').text($("#ctl00_cphTransaction_paxddlType option[value='" + Type + "']").text());
                                    }
                                    else {
                                        var isIsLeadPax = false;
                                        if (paxDetails.length == 0) {
                                            var isIsLeadPax = true;
                                        }
                                        else {
                                            var isIsLeadPax = false;
                                        }
                                        paxDetails.push({
                                            "PaxId": 0,
                                            "Type": paxTypeObj[Type],
                                            "Title": Title,
                                            "FirstName": fname,
                                            "LastName": lname,
                                            "TicketNumber": ticketNumber,
                                            "HandlingId": handlingFeeId,
                                            "TaxBreakUp": taxBrakeUp,
                                            "IsLeadPax": isIsLeadPax,
                                            "CellPhone": $("#ctl00_cphTransaction_txtPhone").val(),
                                            "Email": $("#ctl00_cphTransaction_txtEmail").val(),
                                            "Price": paxWiseCalculation()
                                        });
                                        $("#paxtable tbody").append('<tr><td>' + $("#ctl00_cphTransaction_paxddlPaxTitle").val() + ' ' + fname + " " + lname + '</td>\
                                                                 <td>'+ $("#ctl00_cphTransaction_paxddlType option[value='" + Type + "']").text() + '</td>\
                                                                 <td><a href="javascript:void(0)" class="ml-3 text-info editPax">\
                                                                        <span class="icon icon-edit"></span>\
                                                                    </a>\
                                                                    <a href="javascript:void(0)" class="ml-3 text-danger removePax">\
                                                                        <span class="icon icon-delete"></span>\
                                                                    </a></td></tr>');
                                    }
                                }
                                $("#ctl00_cphTransaction_hdnPaxDetails").val(JSON.stringify(paxDetails));
                                FinalCalculation();                                
                                if ($("#paxtable tbody").find('tr').length > 0) {
                                    $("#commissionPercentage").prop('disabled', true);                                    
                                    $("#discountPercenntage").prop('disabled', true);                                    
                                }
                                $(".paxForm").hide();
                                ClearPaxFields();
                            }
                        });

                        $("#btnClearpaxDetails").on('click', function (e) {
                            e.preventDefault();
                            ClearPaxFields();
                            if ($("#paxtable tbody").find('tr').length == 0) {
                                $("#commissionPercentage").prop('disabled', false);
                                $("#discountPercenntage").prop('disabled', false);
                                $("#commissionPercentage").val("0.00");
                                $("#discountPercenntage").val("0.00");
                            }
                            if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                var adultCount = $("#ctl00_cphTransaction_ddlAdultCount").val();
                                var childCount = $("#ctl00_cphTransaction_ddlChildCount").val();
                                var infantCount = $("#ctl00_cphTransaction_ddlInfantCount").val();
                                var totalPax = parseInt(parseInt(adultCount) + parseInt(childCount) + parseInt(infantCount));
                                var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                if (totalPax > paxDetails.length) {
                                    $(".paxForm").show();
                                }
                                else {
                                    $(".paxForm").hide();
                                }
                            }  
                        })

                        function validatePaxDetails() {
                            var validatePax = true;
                            if ($("#paxtxtPaxName").val() == "") {
                                validatePax = false;
                                toastr.error('Please enter pax name');
                                $("#paxtxtPaxName").addClass("form-text-error");
                            }
                            else {
                                var nameText = $("#paxtxtPaxName").val();
                                var splitName = nameText.split('/');
                                let fname = splitName[0];

                                
                                if (fname == "") {
                                    validatePax = false;
                                    toastr.error('Please enter pax name');
                                    $("#paxtxtPaxName").addClass("form-text-error");
                                }
                                if (splitName.length <= 1 || splitName[1]=="") {
                                    validatePax = false;
                                    toastr.error('Please enter pax second name');
                                    $("#paxtxtPaxName").addClass("form-text-error");
                                }
                               
                            }
                            if ($("#paxTicketNumber").val() == "" && $("#chkDraftmode").prop('checked') == false) {
                                validatePax = false;
                                toastr.error('Please enter pax ticket number');
                                $("#paxTicketNumber").addClass("form-text-error");
                            }
                            if (parseFloat($("#paxtxtSellingFare").val()) <= 0 && $("#ctl00_cphTransaction_ddlSaleType").val()=='N') {
                                validatePax = false;
                                toastr.error('Please selling fare');
                                $("#paxtxtSellingFare").addClass("form-text-error");
                            }
                            if ($("#ctl00_cphTransaction_ddlSaleType").val()=='N' && ($("#ctl00_cphTransaction_handlingFee").val() == "" || $("#ctl00_cphTransaction_handlingFee").val() == "0")) {
                                validatePax = false;
                                toastr.error('Please select handling fee');
                            }
                            if ($(".taxBindDiv").length == 0 && $("#ctl00_cphTransaction_ddlSaleType").val()=='N') {
                                validatePax = false;
                                toastr.error('Please enter tax');
                                $("#paxTaxAmount").addClass("form-text-error");
                            }
                            if (document.getElementById('<%=hndLocationId.ClientID%>').value == "") {
                                validatePax = false;
                                toastr.error('Please select location');
                            }
                            if ($("#ctl00_cphTransaction_ddlTransactionType").val() == "16") {
                                if (parseFloat($("#paxAgentCancelAmount").val()) == 0) {
                                    validatePax = false;
                                    toastr.error('Please agent cancellation amount');
                                    $("#paxAgentCancelAmount").addClass("form-text-error");
                                }
                                else if (parseFloat($("#paxSupplierCancelAmount").val()) == 0) {
                                    validatePax = false;
                                    toastr.error('Please supplier cancellation amount');
                                    $("#paxSupplierCancelAmount").addClass("form-text-error");
                                }
                            }
                            if (!isPaxEdit) {
                                var paxDetails = [];
                                var currAdult = [];
                                var currChild = [];
                                var curInfant = [];
                                if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                    paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                    currAdult = $.grep(paxDetails, function (val, ind) {
                                        return val.Type == "1";
                                    });
                                    currChild = $.grep(paxDetails, function (val, ind) {
                                        return val.Type == "2";
                                    });
                                    curInfant = $.grep(paxDetails, function (val, ind) {
                                        return val.Type == "3";
                                    });
                                }
                                if ($("#ctl00_cphTransaction_paxddlType").val() == "AD") {
                                    if (parseInt(currAdult.length) == parseInt($("#ctl00_cphTransaction_ddlAdultCount").val()) || parseInt($("#ctl00_cphTransaction_ddlAdultCount").val()) == 0) {
                                        validatePax = false;
                                        toastr.error('Please check adult pax count');
                                    }
                                }
                                else if ($("#ctl00_cphTransaction_paxddlType").val() == "CHD") {
                                    if (parseInt(currChild.length) == parseInt($("#ctl00_cphTransaction_ddlChildCount").val()) || parseInt($("#ctl00_cphTransaction_ddlChildCount").val()) == 0) {
                                        validatePax = false;
                                        toastr.error('Please check child pax count');
                                    }
                                }
                                else {
                                    if (parseInt(curInfant.length) == parseInt($("#ctl00_cphTransaction_ddlInfantCount").val()) || parseInt($("#ctl00_cphTransaction_ddlInfantCount").val()) == 0) {
                                        validatePax = false;
                                        toastr.error('Please check infant pax count');
                                    }
                                }
                            }
                            return validatePax;
                        }

                        $("#ctl00_cphTransaction_paxddlType").on('change', function () {
                            var validatePax = true;
                            var paxDetails = [];
                            var currAdult = [];
                            var currChild = [];
                            var curInfant = [];
                            if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                currAdult = $.grep(paxDetails, function (val, ind) {
                                    return val.Type == "1";
                                });
                                currChild = $.grep(paxDetails, function (val, ind) {
                                    return val.Type == "2";
                                });
                                curInfant = $.grep(paxDetails, function (val, ind) {
                                    return val.Type == "3";
                                });
                            }
                            if ($(this).val() == "AD") {
                                if (parseInt(currAdult.length) == parseInt($("#ctl00_cphTransaction_ddlAdultCount").val()) || parseInt($("#ctl00_cphTransaction_ddlAdultCount").val()) == 0) {
                                    validatePax = false;
                                    toastr.error('Please check adult pax count');
                                }
                            }
                            else if ($(this).val() == "CHD") {
                                if (parseInt(currChild.length) == parseInt($("#ctl00_cphTransaction_ddlChildCount").val()) || parseInt($("#ctl00_cphTransaction_ddlChildCount").val()) == 0) {
                                    validatePax = false;
                                    toastr.error('Please check child pax count');
                                }
                            }
                            else {
                                if (parseInt(curInfant.length) == parseInt($("#ctl00_cphTransaction_ddlInfantCount").val()) || parseInt($("#ctl00_cphTransaction_ddlInfantCount").val()) == 0) {
                                    validatePax = false;
                                    toastr.error('Please check infant pax count');
                                }
                            }
                            if (!validatePax) {
                                if (isPaxEdit) {
                                    var selectedPaxDetails = paxDetails[paxIndex-1];
                                    var keys = $.map(paxTypeObj, function (item, key) {
                                        if (selectedPaxDetails.Type == item) {
                                            return key;
                                        }
                                    });
                                    $("#ctl00_cphTransaction_paxddlType").select2('val', keys[0]);
                                }
                                else {
                                    $("#ctl00_cphTransaction_paxddlType").select2('val',"AD");
                                }
                              
                            }
                            
                        });

                        $("#paxtxtSellingFare").on('change', function () {
                            if ($(this).val() == "" || isNaN($(this).val())) {
                                $(this).val(0.00);
                            }
                           
                            $("#discountOn").val(parseFloat($(this).val()).toFixed(decimalpoint));
                            $("#commissionOn").val(parseFloat($(this).val()).toFixed(decimalpoint));
                            $("#commissionPercentage").trigger("change");
                            $("#discountPercenntage").trigger("change");                           
                        });

                        $("#commissionPercentage").on('change', function () {
                            var commisionAmt = 0.00;
                            if ($(this).val() != "") {
                                commisionAmt = parseFloat(parseFloat($("#commissionOn").val()) * parseFloat($(this).val() / 100)).toFixed(decimalpoint);
                            }
                            if (isNaN(commisionAmt)) {
                                commisionAmt = 0.00;
                            }
                            $("#commissionAmount").val(commisionAmt);
                        });

                        $("#discountPercenntage").on('change', function () {
                            var discountAmt = 0.00;
                            if ($(this).val() != "") {
                                discountAmt = parseFloat(parseFloat($("#discountOn").val()) * parseFloat($(this).val() / 100)).toFixed(decimalpoint);
                            }
                            if (isNaN(discountAmt)) {
                                discountAmt = 0.00;
                            }
                            $("#discountAmount").val(discountAmt);                            
                        });

                        $("#paxTaxAdd").on('click', function () {
                            if ($("#paxTaxAmount").val() != "" && parseFloat($("#paxTaxAmount").val()) > 0 && $("#paxTaxCode").val() != "") {
                                var paxTaxAmount = $("#paxTaxAmount").val();
                                var paxTaxCode = $("#paxTaxCode").val();
                                $("#TaxDetails").append('<div class="col-md-12 taxBindDiv" data-value="' + paxTaxAmount + '" data-code="' + paxTaxCode + '"><div class="updated-row-content bg-secondary text-white rounded px-2 py-1 mb-3" >\
                                                    <span class="text">Code: '+ paxTaxCode + ' | Amount:' + $("#ctl00_cphTransaction_hdnCurrencyCode").val() + ' ' + paxTaxAmount + '</span>\
                                                    <a href="javascript:void(0)" class="add-more remove-row" >\
                                                        <span class="fa fa-close"></span>\
                                                    </a>\
                                                </div></div>');
                                $("#paxTaxAmount").val("");
                                $("#paxTaxCode").val("");                                
                            }
                            else {
                                if (parseFloat($("#paxTaxAmount").val()) == 0) {
                                    toastr.error('Please enter tax amount');
                                }
                                if ($("#paxTaxCode").val() == "") {
                                    toastr.error('Please enter tax code');
                                }
                            }

                        });

                        $("body").on('click', '.remove-row', function () {
                            $(this).parents(".taxBindDiv:eq(0)").remove();
                           // callingVatOrGST(false);
                        });

                        $("#paxAmount2,#paxTransactionFee,#paxAddlTransactionFee").on('change', function () {
                            if ($(this).val() == "") {
                                $(this).val(0);
                            }                           
                        });

                        $("body").on('click', '.editPax', function () {
                            $(".paxForm").show();
                            var parentRow = $(this).parents('tr:eq(0)');
                            var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                            paxIndex = parentRow[0].rowIndex;
                            isPaxEdit = true;
                           
                            var selectedPaxDetails = paxDetails[parentRow[0].rowIndex - 1];
                            var keys = $.map(paxTypeObj, function (item, key) {
                                if (selectedPaxDetails.Type == item) {
                                    return key;
                                }
                            });
                            $("#ctl00_cphTransaction_paxddlType").select2('val', keys[0]);
                            $("#ctl00_cphTransaction_paxddlPaxTitle").select2('val', selectedPaxDetails.Title)
                            $("#paxtxtSellingFare").val(selectedPaxDetails.Price.PublishedFare);
                            $("#paxMarkUp").val(selectedPaxDetails.Price.Markup);
                            $("#paxAmount2").val(selectedPaxDetails.Price.AsvAmount);
                            $("#paxTransactionFee").val(selectedPaxDetails.Price.TransactionFee);
                            $("#paxAddlTransactionFee").val(selectedPaxDetails.Price.AdditionalTxnFee);
                            $("#commissionPercentage").val(selectedPaxDetails.Price.CommissionValue);
                            $("#commissionOn").val(selectedPaxDetails.Price.PublishedFare);
                            $("#commissionAmount").val(selectedPaxDetails.Price.OurCommission);
                            $("#discountPercenntage").val(selectedPaxDetails.Price.DiscountValue);
                            $("#discountOn").val(selectedPaxDetails.Price.PublishedFare);
                            $("#discountAmount").val(selectedPaxDetails.Price.Discount);
                            $("#ctl00_cphTransaction_handlingFee").val(selectedPaxDetails.HandlingId);
                            $(".taxBindDiv").remove();
                            $.each(selectedPaxDetails.TaxBreakUp, function (taxInd, taxVal) {
                                $("#TaxDetails").append('<div class="col-md-12 taxBindDiv" data-value="' + taxVal["Value"] + '" data-code="' + taxVal["Key"] + '"><div class="updated-row-content bg-secondary text-white rounded px-2 py-1 mb-3" >\
                                                    <span class="text">Code: '+ taxVal["Key"] + ' | Amount:' + $("#ctl00_cphTransaction_hdnCurrencyCode").val() + ' ' + taxVal["Value"] + '</span>\
                                                    <a href="javascript:void(0)" class="add-more remove-row" >\
                                                        <span class="fa fa-close"></span>\
                                                    </a>\
                                                </div></div>');
                            });
                            $("#paxAgentCancelAmount").val(parseFloat(selectedPaxDetails.Price.AgentCancellationCharge).toFixed(decimalpoint));
                            $("#paxSupplierCancelAmount").val(parseFloat(selectedPaxDetails.Price.SupplierCancellationCharge).toFixed(decimalpoint));
                            $("#paxCancelIPVat").val(parseFloat(selectedPaxDetails.Price.CancellationInVat).toFixed(decimalpoint));
                            $("#paxCancelOPVat").val(parseFloat(selectedPaxDetails.Price.CancellationOutVat).toFixed(decimalpoint));
                            $("#paxtxtPaxName").val(selectedPaxDetails.FirstName + "/" + selectedPaxDetails.LastName);
                            $("#paxTicketNumber").val(selectedPaxDetails.TicketNumber);
                            inputPaxVATcharge = selectedPaxDetails.Price.InputVATAmount;
                            outputPaxVATcharge = selectedPaxDetails.Price.OutputVATAmount;
                            $("#addPaxDetails").text("Update");
                        });

                        $("body").on('click', '.removePax', function () {
                            var parentRow = $(this).parents('tr:eq(0)');
                            var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                            if (paxDetails[parentRow[0].rowIndex - 1].PaxId == 0) {
                                if (paxDetails[parentRow[0].rowIndex - 1].IsLeadPax == true && paxDetails.length>1) {
                                    paxDetails[parentRow[0].rowIndex].IsLeadPax = true;
                                }
                                paxDetails.splice(parentRow[0].rowIndex - 1, 1);
                                $("#ctl00_cphTransaction_hdnPaxDetails").val(JSON.stringify(paxDetails));
                                $(this).parents('tr:eq(0)').remove();                                
                                if ($("#paxtable tbody").find('tr').length == 0) {
                                    $("#commissionPercentage").prop('disabled', false);
                                    $("#discountPercenntage").prop('disabled', false);
                                    $("#commissionPercentage").val("0.00");
                                    $("#discountPercenntage").val("0.00");
                                    $("#ctl00_cphTransaction_hdnsettlementDetails").val("");
                                    $("#tblSettlementDetail tbody").children().remove();
                                    
                                    $(".paxForm").show();
                                    $("#paxDetailDiv").hide();
                                }
                                ClearPaxFields();
                                FinalCalculation();
                            }
                            else {
                                toastr.error("Can't remove the pax");
                            }
                            
                        });

                        $("#paxTicketNumber").on('change', function () {
                            var txtNumber = $(this).val();
                            if (txtNumber.length > 0) {
                                if (txtNumber.length != 10) {
                                    $(this).val("");
                                    txtNumber.value = '';
                                    $(this).focus();
                                    toastr.error('Ticket number should be 10 character length.');
                                }
                            }
                            if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                if (paxDetails.length > 0) {
                                    var existTicketNumber = $.grep(paxDetails, function (ticketVal, ticketInd) {
                                        if (isPaxEdit) {
                                            if (!ticketInd == paxIndex - 1) {
                                                return ticketVal.TicketNumber == txtNumber;
                                            }
                                        }
                                        else {
                                            return ticketVal.TicketNumber == txtNumber;
                                        }
                                    });
                                    if (existTicketNumber.length > 0) {
                                        $(this).val("");
                                        txtNumber.value = '';
                                        $(this).focus();
                                        toastr.error('Ticket number alredy exist.');
                                    }
                                }
                            }

                        });

                        $(".sectorControl").on('change', function () {
                            var sectorsCount = 0;                            
                            var id = $(this)[0].id.substring($(this)[0].id.length - 1);
                            if (id == 0) {
                                id = Math.ceil(id) + 10;
                            }
                            var selsectorval = $(this)[0].value;
                            var ddldestination = document.getElementById('<%=ddldestination.ClientID%>');
                            if (selsectorval == '' || selsectorval == '-1') {
                                var totalSectors = 0;
                                for (var i = 1; i <= 10; i++) {
                                    if ($("#ctl00_cphTransaction_ddlSector" + i + "").val() != "-1" &&
                                        $("#ctl00_cphTransaction_ddlSector" + i + "").val() != null) {
                                        totalSectors++;
                                    }
                                }
                                document.getElementById('<%=hndsegmentsCount.ClientID%>').value = totalSectors;
                                return;
                            }
                            if (ddldestination.value == '-1' || ddldestination.value == '') {
                                BindSectors();
                                document.getElementById('<%=hndsegmentsCount.ClientID%>').value = sectorsCount;
                                return;
                            }
                            else {
                                if (id > 1) {
                                    var sectorid = id - 1;
                                    var sectorvalue = document.getElementById('ctl00_cphTransaction_ddlSector' + sectorid).value;
                                    if (sectorvalue == '' || sectorvalue == '-1') {
                                        $('#s2id_ctl00_cphTransaction_ddlSector' + id).select2('val', '-1');
                                        toastr.error('Please select previous sector');
                                        return;
                                    }

                                    if (sectorvalue == selsectorval) {
                                        $('#s2id_ctl00_cphTransaction_ddlSector' + id).select2('val', '-1');
                                        toastr.error('Previous and current sectors cannot be same');
                                        return;
                                    }
                                    var nextSectorId = parseInt(parseInt(id) + 1);
                                    var nextSectorval = document.getElementById('ctl00_cphTransaction_ddlSector' + nextSectorId).value;
                                    if (nextSectorval == selsectorval) {
                                        $('#s2id_ctl00_cphTransaction_ddlSector' + id).select2('val', '-1');
                                        toastr.error('Next and current sectors cannot be same');
                                        return;
                                    }
                                }
                                if (id == 1 && ddldestination.value == selsectorval) {
                                    $('#s2id_ctl00_cphTransaction_ddlSector' + id).select2('val', '-1');
                                    toastr.error('Destination and sector-1 canot be same');
                                    return;
                                }
                            }
                            var ddlSector6 = document.getElementById('<%=ddlSector6.ClientID%>');
                            if (id == 5 && ddlSector6.disabled) {
                                document.getElementById('<%=txtConjPNR.ClientID%>').value = document.getElementById('<%=txtPNRNum.ClientID%>').value;
                                for (var i = 6; i <= 10; i++) {
                                    document.getElementById('ctl00_cphTransaction_ddlSector' + i).innerHTML = ddldestination.innerHTML;
                                    $('#s2id_ctl00_cphTransaction_ddlSector' + i).select2('val', '-1');
                                    document.getElementById('ctl00_cphTransaction_ddlSector' + i).disabled = false;
                                }
                            }
                            var isDuplicate = false;
                            var counter = 5;
                            if (!ddlSector6.disabled) {
                                counter = 10;
                            }
                            sectorsCount = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                            if (sectorsCount != '' && sectorsCount != 0) {
                                sectorsCount = 0;
                                for (var i = 1; i <= counter; i++) {
                                    var currsectorval = document.getElementById('ctl00_cphTransaction_ddlSector' + i).value;
                                    if (currsectorval != '' && currsectorval != '-1')
                                        sectorsCount = sectorsCount + 1;
                                }
                            }
                            else {
                                sectorsCount = 1;
                            }
                            document.getElementById('<%=hndsegmentsCount.ClientID%>').value = sectorsCount;
                            if (isDuplicate) {
                                if (Math.ceil(sectorsCount) != Math.ceil(id)) {
                                    $('#s2id_ctl00_cphTransaction_ddlSector' + id).select2('val', '-1');
                                }
                                alert('Sector value cannot be duplicated');
                                return;
                            }
                            if (id == 1) {
                               
                                isRecalculate = true;
                                RecalculateGSTandVat();
                            }
                            $('#showParam').collapse('hide');
                        });

                        $("#ctl00_cphTransaction_ddlConsultant").on('change', function () {
                            var locationCountryCode = '';
                            var locationId = $("#ctl00_cphTransaction_ddlConsultant").val();                           
                            if (locationId == "") {
                                locationId = document.getElementById('<%=hndLocationId.ClientID%>').value;
                            }
                            $.ajax({
                                type: "POST",
                                url: "OfflineEntry.aspx/GetAgentLocationCountryCode",
                                contentType: "application/json; charset=utf-8",
                                data: "{'locationId':'" + locationId + "'}",
                                dataType: "json",
                                async: false,
                                success: function (Et) {
                                    if (Et.d != '') {
                                        locationCountryCode = Et.d;
                                    }

                                    $("#ctl00_cphTransaction_ddlConsultant").select2('val', locationId);
                                    if (document.getElementById('ctl00_upProgress').style.display == 'block') {
                                        document.getElementById('ctl00_upProgress').style.display = 'none';
                                    }
                                },
                                failure: function (error) {
                                    alert(error);
                                    return false;
                                }
                            });
                            //If selected agent location country is India then enabling GST text box control.
                            document.getElementById('<%=hdnlocCountryCode.ClientID%>').value = locationCountryCode;
                            if (document.getElementById('<%=hdnlocCountryCode.ClientID%>').value == "IN") {
                                document.getElementById('divInputVAT').style.display = 'none';
                                document.getElementById('divOutVAT').style.display = 'block';
                                document.getElementById('divNetVAT').style.display = 'none';
                                $("#gstOrVat").text("GST")
                                document.getElementById('<%=hndLocationId.ClientID%>').value = locationId;

                            }
                            else { //If selected agent location country is other than India  then enabling VAT controls
                                document.getElementById('divInputVAT').style.display = 'block';
                                document.getElementById('divOutVAT').style.display = 'block';
                                document.getElementById('divNetVAT').style.display = 'block';
                                $("#gstOrVat").text("OUTPUT VAT")
                                document.getElementById('<%=hndLocationId.ClientID%>').value = locationId;
                            }              

                            if ($("#ctl00_cphTransaction_ddlTransactionType").val() == "16") {
                                if (document.getElementById('<%=hdnlocCountryCode.ClientID%>').value == "IN") {
                                    $("#agentTaxlbl").text('Agent GST Amount');
                                    $("#supplierTaxlbl").text('Supplier GST Amount');
                                    $("#lblTotalCancelOpVat").text('Agent GST Amount');
                                    $("#lblTotalCancelIpVat").text('Supplier GST Amount');
                                }
                                else {
                                    $("#agentTaxlbl").text('Cancellation O/P VAT');
                                    $("#supplierTaxlbl").text('Cancellation I/P VAT');
                                    $("#lblTotalCancelOpVat").text('Cancellation O/P VAT');
                                    $("#lblTotalCancelIpVat").text('Cancellation I/P VAT');
                                }
                                $(".cancelCharges").css('display', 'block');
                            }                            
                            isRecalculate = true;
                            RecalculateGSTandVat();        
                        });

                        $("#ctl00_cphTransaction_txtPhone").on('change', function () {
                            if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                if (paxDetails.length != 0) {
                                    $.each(paxDetails, function (paxInd, paxVal) {
                                        paxVal.CellPhone = $("#ctl00_cphTransaction_txtPhone").val();
                                    });
                                    $("#ctl00_cphTransaction_hdnPaxDetails").val(JSON.stringify(paxDetails));
                                }
                            }
                        });

                        $("#ctl00_cphTransaction_txtEmail").on('change', function () {
                            if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                if (paxDetails.length != 0) {
                                    $.each(paxDetails, function (paxInd, paxVal) {
                                        paxVal.Email = $("#ctl00_cphTransaction_txtEmail").val();
                                    });
                                    $("#ctl00_cphTransaction_hdnPaxDetails").val(JSON.stringify(paxDetails))
                                }
                            }
                        });

                        $("#ctl00_cphTransaction_ddlAdultCount").on('change', function () {
                            paxCountValidate();
                        });

                        $("#ctl00_cphTransaction_ddlChildCount").on('change', function () {
                            paxCountValidate();
                        });

                        $("#ctl00_cphTransaction_ddlInfantCount").on('change', function () {
                            paxCountValidate();
                        });

                        function paxWiseCalculation() {
                            var sellingFare = $("#paxtxtSellingFare").val();
                            var totalTax = 0.00;
                            $.each($(".taxBindDiv"), function (ind, taxValue) {
                                if ($(taxValue).attr("data-value") != "") {
                                    totalTax = parseFloat(parseFloat(totalTax) + parseFloat($(taxValue).attr("data-value")));
                                }
                            });
                            var priceId = 0;
                            if (isPaxEdit) {   
                                var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                if (paxDetails[paxIndex - 1].Price.PriceId != 0) {
                                    priceId = paxDetails[paxIndex - 1].Price.PriceId;
                                }
                            }
                            callingVatOrGST(false);
                            var priceObject = {
                                "PriceId": priceId,
                                "SupplierPrice": parseFloat(parseFloat(sellingFare) + parseFloat(totalTax)).toFixed(decimalpoint),
                                "PublishedFare": parseFloat(sellingFare).toFixed(decimalpoint),
                                "NetFare": 0,
                                "Tax": parseFloat(totalTax).toFixed(decimalpoint),
                                "OurCommission": parseFloat($("#commissionAmount").val()).toFixed(decimalpoint),
                                "CommissionType": "P",
                                "CommissionValue": parseFloat($("#commissionPercentage").val()).toFixed(decimalpoint),
                                "RateOfExchange": parseFloat($("#ctl00_cphTransaction_txtExcRate").val()).toFixed(decimalpoint),
                                "Markup": parseFloat($("#paxMarkUp").val()).toFixed(decimalpoint),
                                "AsvAmount": parseFloat($("#paxAmount2").val()).toFixed(decimalpoint),
                                "AsvElement": "TF",
                                "Discount": parseFloat($("#discountAmount").val()).toFixed(decimalpoint),
                                "DiscountType": "P",
                                "DiscountValue": parseFloat($("#discountPercenntage").val()).toFixed(decimalpoint),
                                "TransactionFee": parseFloat($("#paxTransactionFee").val()).toFixed(decimalpoint),
                                "AdditionalTxnFee": parseFloat($("#paxAddlTransactionFee").val()).toFixed(decimalpoint),
                                "Currency": $("#ctl00_cphTransaction_hdnCurrencyCode").val(),
                                "CurrencyCode": $("#ctl00_cphTransaction_hdnCurrencyCode").val(),
                                "OurPLB": 0.00,
                                "AgentCommission": 0.00,
                                "AgentPLB": 0.00,
                                "SeviceTax": 0,
                                "InputVATAmount": parseFloat(inputPaxVATcharge).toFixed(decimalpoint),
                                "OutputVATAmount": parseFloat(outputPaxVATcharge).toFixed(decimalpoint),
                                "AgentCancellationCharge": parseFloat($("#paxAgentCancelAmount").val()).toFixed(decimalpoint),
                                "SupplierCancellationCharge": parseFloat($("#paxSupplierCancelAmount").val()).toFixed(decimalpoint),
                                "CancellationInVat": parseFloat($("#paxCancelIPVat").val()).toFixed(decimalpoint),
                                "CancellationOutVat": parseFloat($("#paxCancelOPVat").val()).toFixed(decimalpoint)
                            };
                            return priceObject;
                        }

                        function FareCalculationForPax() {                            
                            var sellingFair = $("#paxtxtSellingFare").val();
                            var markup = $("#paxMarkUp").val();
                            var addlmarkup = $("#paxAmount2").val();
                            var transFee = $("#paxTransactionFee").val();
                            var addltransFee = $("#paxAddlTransactionFee").val();
                            var discount = parseFloat($("#discountAmount").val());
                            var totalTax = 0.00;
                            if ($(".taxBindDiv").length > 0) {
                                $.each($(".taxBindDiv"), function (ind, taxValue) {
                                    if ($(taxValue).attr("data-value") != "") {
                                        totalTax = parseFloat(parseFloat(totalTax) + parseFloat($(taxValue).attr("data-value")));
                                    }
                                });
                            }
                            var TotalFare = parseFloat(parseFloat(sellingFair) + parseFloat(totalTax)).toFixed(decimalpoint);
                            var CollectedAmount = parseFloat(parseFloat(sellingFair) + parseFloat(totalTax)
                                + parseFloat(markup) + parseFloat(addlmarkup) + parseFloat(transFee)
                                + parseFloat(addltransFee) - parseFloat(discount)).toFixed(decimalpoint);

                            return {
                                TotalFare: TotalFare,
                                CollectedAmount: CollectedAmount
                            }
                        }

                        function FareReCalculationForPax(indx) {
                            var sellingFair = 0.00;
                            var markup = 0.00;
                            var addlmarkup = 0.00;
                            var transFee = 0.00;
                            var addltransFee = 0.00;
                            var discount = 0.00;
                            var totalTax = 0.00;
                            if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                if (paxDetails.length > 0) {
                                    sellingFair = parseFloat(paxDetails[indx].Price.PublishedFare).toFixed(decimalpoint);
                                    markup = parseFloat(paxDetails[indx].Price.Markup).toFixed(decimalpoint);
                                    addlmarkup = parseFloat(paxDetails[indx].Price.AsvAmount).toFixed(decimalpoint);
                                    transFee = parseFloat(paxDetails[indx].Price.TransactionFee).toFixed(decimalpoint);
                                    addltransFee = parseFloat(paxDetails[indx].Price.AdditionalTxnFee).toFixed(decimalpoint);
                                    discount = parseFloat(paxDetails[indx].Price.Discount).toFixed(decimalpoint);
                                    totalTax=parseFloat(paxDetails[indx].Price.Tax).toFixed(decimalpoint);
                                }
                            }  
                            var TotalFare = parseFloat(parseFloat(sellingFair) + parseFloat(totalTax)).toFixed(decimalpoint);
                            var CollectedAmount = parseFloat(parseFloat(sellingFair) + parseFloat(totalTax)
                                + parseFloat(markup) + parseFloat(addlmarkup) + parseFloat(transFee)
                                + parseFloat(addltransFee) - parseFloat(discount)).toFixed(decimalpoint);

                            return {
                                TotalFare: TotalFare,
                                CollectedAmount: CollectedAmount
                            }
                        }

                        function callingVatOrGST(isAsync) {
                            var sellingFair = $("#paxtxtSellingFare").val();
                            var markup = $("#paxMarkUp").val();
                            var addlmarkup = $("#paxAmount2").val();
                            var transFee = $("#paxTransactionFee").val();
                            var addltransFee = $("#paxAddlTransactionFee").val();
                            if (document.getElementById('<%=hdnlocCountryCode.ClientID%>').value == "IN") {
                                PaxCalculateGST(markup, addlmarkup, transFee, addltransFee, isAsync);
                            }
                            else {
                                PaxCalculateVAT(sellingFair, markup, addlmarkup, transFee, addltransFee, isAsync);
                            }
                        }
                        function calculateRecievedandDue() {
                            var totalRecivd = 0;
                            var totalDue = 0;
                            var ccCharge = 0;
                            var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                            var currentSum = 0;
                            if (ExistingSettlement != null && ExistingSettlement != "") {
                                let ExistingJson = JSON.parse(ExistingSettlement);
                                
                                $.each(ExistingJson.SettlementObject, function (item, value) {
                                    if (value.SettlementName == '3') {
                                        ccCharge = parseFloat(value.FOPDetail3).toFixed(decimalpoint);
                                    }
                                    totalRecivd = parseFloat(parseFloat(totalRecivd) + parseFloat(value.Amount)).toFixed(decimalpoint);
                                });
                                totalRecivd = parseFloat(parseFloat(totalRecivd) + parseFloat(ccCharge)).toFixed(decimalpoint);
                            }
                            var actualCollect = parseFloat($("#ctl00_cphTransaction_txtCollected").val()).toFixed(decimalpoint);
                            var totalTocollect = parseFloat(parseFloat(actualCollect) + parseFloat(ccCharge)).toFixed(decimalpoint);
                           
                            totalDue = parseFloat(parseFloat(totalTocollect) - parseFloat(totalRecivd)).toFixed(decimalpoint);
                            return { 'recieved': totalRecivd, 'due': totalDue, 'ccCharge': ccCharge };
                        }

                        function FinalCalculation() {
                            var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                            var totalSellingFare = 0.00;
                            var totalTax = 0.00;
                            var totalInvat = 0.00;
                            var totalOutVat = 0.00;
                            var profitValue = 0.00;
                            var totalDiscount = 0.00;
                            var totalCommission = 0.00;
                            var cancellationAmt = 0.00;
                            var amt1 = 0.00;
                            var amt2 = 0.00;
                            var totalAgentCancel = 0.00;
                            var totalSupplierCancel = 0.00;
                            var totalCancelIpVat = 0.00;
                            var totalCancelOpVat = 0.00;
                            $.each(paxDetails, function (finalInd, finalVal) {
                                totalSellingFare = parseFloat(parseFloat(totalSellingFare) + parseFloat(finalVal.Price.PublishedFare)).toFixed(decimalpoint);
                                totalTax = parseFloat(parseFloat(totalTax) + parseFloat(finalVal.Price.Tax)).toFixed(decimalpoint);
                                totalInvat = parseFloat(parseFloat(totalInvat) + parseFloat(finalVal.Price.InputVATAmount)).toFixed(decimalpoint);
                                totalOutVat = parseFloat(parseFloat(totalOutVat) + parseFloat(finalVal.Price.OutputVATAmount)).toFixed(decimalpoint);
                                profitValue = parseFloat(parseFloat(profitValue) + parseFloat(finalVal.Price.Markup)
                                    + parseFloat(finalVal.Price.AsvAmount) + parseFloat(finalVal.Price.TransactionFee)
                                    + parseFloat(finalVal.Price.AdditionalTxnFee)).toFixed(decimalpoint);
                                totalDiscount = parseFloat(parseFloat(totalDiscount) + parseFloat(finalVal.Price.Discount)).toFixed(decimalpoint);
                                totalCommission = parseFloat(parseFloat(totalCommission) + parseFloat(finalVal.Price.OurCommission)).toFixed(decimalpoint);
                                amt1 = parseFloat(parseFloat(amt1) + parseFloat(finalVal.Price.Markup)).toFixed(decimalpoint);
                                amt2 = parseFloat(parseFloat(amt2)+parseFloat(finalVal.Price.AsvAmount)).toFixed(decimalpoint);
                                if ($("#ctl00_cphTransaction_ddlTransactionType").val() == "16") {
                                    totalAgentCancel = parseFloat(parseFloat(totalAgentCancel) + parseFloat(finalVal.Price.AgentCancellationCharge)).toFixed(decimalpoint);
                                    totalSupplierCancel = parseFloat(parseFloat(totalSupplierCancel) + parseFloat(finalVal.Price.SupplierCancellationCharge)).toFixed(decimalpoint);
                                    totalCancelIpVat = parseFloat(parseFloat(totalCancelIpVat) + parseFloat(finalVal.Price.CancellationInVat)).toFixed(decimalpoint);
                                    totalCancelOpVat = parseFloat(parseFloat(totalCancelOpVat) + parseFloat(finalVal.Price.CancellationOutVat)).toFixed(decimalpoint);
                                    cancellationAmt = parseFloat(parseFloat(cancellationAmt) + parseFloat(finalVal.Price.AgentCancellationCharge)).toFixed(decimalpoint);
                                    cancellationAmt = parseFloat(parseFloat(cancellationAmt) + parseFloat(finalVal.Price.CancellationOutVat)).toFixed(decimalpoint);
                                    cancellationAmt = parseFloat(parseFloat(cancellationAmt) + parseFloat(finalVal.Price.SupplierCancellationCharge)).toFixed(decimalpoint);
                                    cancellationAmt = parseFloat(parseFloat(cancellationAmt) + parseFloat(finalVal.Price.CancellationInVat)).toFixed(decimalpoint);
                                }
                            });
                            
                            var totalCollected = parseFloat(parseFloat(totalSellingFare) + parseFloat(totalTax) + parseFloat(totalInvat) + parseFloat(totalOutVat)
                                + parseFloat(profitValue) - parseFloat(totalDiscount)).toFixed(decimalpoint);
                            
                            $("#ctl00_cphTransaction_TxtInputVAT").val(parseFloat(totalInvat).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtOutPutVAT").val(parseFloat(totalOutVat).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtNetVAT").val(parseFloat(parseFloat(totalInvat) + parseFloat(totalOutVat)).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtTotalFare").val(parseFloat(parseFloat(totalSellingFare) + parseFloat(totalTax)).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtTax").val(parseFloat(totalTax).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtcashAmount").val(parseFloat(parseFloat(totalCollected) - parseFloat(cancellationAmt)).toFixed(decimalpoint))
                            $("#ctl00_cphTransaction_txtCollected").val(parseFloat(parseFloat(totalCollected) - parseFloat(cancellationAmt)).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtNetpayable").val(parseFloat(parseFloat(totalCollected) - parseFloat(profitValue) - parseFloat(cancellationAmt)).toFixed(decimalpoint));
                            
                            $("#ctl00_cphTransaction_txtProfit").val(parseFloat(profitValue).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtCommision").val(parseFloat(totalCommission).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtDiscount").val(parseFloat(totalDiscount).toFixed(decimalpoint));

                            $("#ctl00_cphTransaction_txtSellingFare").val(parseFloat(totalSellingFare).toFixed(decimalpoint));
                            $("#totalTaxValue").val(totalTax);
                            $("#ctl00_cphTransaction_txtCommisionOn").val(parseFloat(totalSellingFare).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtCommisionAmt").val(parseFloat(totalCommission).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtDiscountOn").val(parseFloat(totalSellingFare).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtDiscountAmt").val(parseFloat(totalDiscount).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtMarkUp").val(parseFloat(amt1).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtAmount2").val(parseFloat(amt2).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtCommper").val($("#commissionPercentage").val());
                            $("#ctl00_cphTransaction_txtDiscountper").val($("#discountPercenntage").val());
                            $("#ctl00_cphTransaction_txtAgentCancellationAmt").val(parseFloat(totalAgentCancel).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtSupplierCancelationAmt").val(parseFloat(totalSupplierCancel).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtCancelOPVat").val(parseFloat(totalCancelOpVat).toFixed(decimalpoint));
                            $("#ctl00_cphTransaction_txtCancelIPVat").val(parseFloat(totalCancelIpVat).toFixed(decimalpoint));

                            var SettlementCalc = calculateRecievedandDue();
                            $("#dueAmt").val(parseFloat(SettlementCalc.due).toFixed(decimalpoint));
                            $("#recievedAmt").val(parseFloat(SettlementCalc.recieved).toFixed(decimalpoint));
                            $("#cardChargeAmt").val(parseFloat(SettlementCalc.ccCharge).toFixed(decimalpoint));
                            $("#totalToCollectAmt").val(parseFloat(parseFloat(totalCollected) + parseFloat(SettlementCalc.ccCharge) - parseFloat(cancellationAmt)).toFixed(decimalpoint))
                        }

                        function paxCountValidate() {
                            var adultCount = $("#ctl00_cphTransaction_ddlAdultCount").val();
                            var childCount = $("#ctl00_cphTransaction_ddlChildCount").val();
                            var infantCount = $("#ctl00_cphTransaction_ddlInfantCount").val();
                            var totalPax = parseInt(parseInt(adultCount) + parseInt(childCount) + parseInt(infantCount));
                            if (totalPax > 9) {
                                toastr.error("Total passenger count not greater than 9");
                                $("#ctl00_cphTransaction_ddlAdultCount").select2("val","1");
                                $("#ctl00_cphTransaction_ddlChildCount").select2("val","0");
                                $("#ctl00_cphTransaction_ddlInfantCount").select2("val","0");
                            }
                            if (parseInt(infantCount) > parseInt(adultCount)) {
                                toastr.error("Infant passenger count not greater than adult count");
                                $("#ctl00_cphTransaction_ddlInfantCount").select2("val","0");
                            }
                        }

                        function ClearPaxFields() {
                            inputPaxVATcharge = 0;
                            outputPaxVATcharge = 0;                           
                            isPaxEdit = false;
                            paxIndex = 0;                            
                            $("#paxtxtSellingFare").val("0.00");
                            $("#paxMarkUp").val("0.00");
                            $("#paxAmount2").val("0.00");
                            $("#paxTransactionFee").val("0.00");
                            $("#paxAddlTransactionFee").val("0.00");  
                            $("#ctl00_cphTransaction_handlingFee").val("0");
                            $(".taxBindDiv").remove();
                            $("#paxAgentCancelAmount").val("0.00");
                            $("#paxSupplierCancelAmount").val("0.00");
                            $("#paxCancelIPVat").val("0.00");
                            $("#paxCancelOPVat").val("0.00");
                            $("#paxtxtPaxName").val("");
                            $("#paxTicketNumber").val("");                            
                            $("#commissionOn").val("0.00");
                            $("#commissionAmount").val("0.00");                            
                            $("#discountOn").val("0.00");
                            $("#discountAmount").val("0.00");
                            $("#paxTaxCode").val("");
                            $("#paxTaxAmount").val("0.00");
                            $("#ctl00_cphTransaction_paxddlType").select2('val', "AD");
                            $("#addPaxDetails").text("Add");
                        }

                        function PaxCalculateVAT(sellingFair, markup, addlmarkup, transFee, addltransFee, isSync) {
                            var decimalpoint = eval('<%=agentDecimalPoints %>');
                            var origin = document.getElementById('<%=ddlSector1.ClientID%>').value;
                            var destination = document.getElementById('<%=ddldestination.ClientID%>').value;
                            var totMarkup = eval(markup) + eval(addlmarkup) + eval(transFee) + eval(addltransFee);
                            if (eval(sellingFair) > 0 && origin != '' && origin != '-1' && destination != '' && destination != '-1') {
                                var paramList = 'sellingFair=' + sellingFair + '&decimalpoint=' + decimalpoint + '&origin=' + origin + '&destination=' + destination + '&totMarkup=' + totMarkup;
                                Ajax.onreadystatechange = CalculatePaxVATComplete;
                                Ajax.open("POST", "OffLineEntryAjax", isSync);
                                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                                Ajax.send(paramList);
                            }
                        }
                        function CalculatePaxVATComplete() {
                            var inputPaxVATcostIncluded = "";
                            var outputPaxVATappliedOn = "";
                            if (Ajax.readyState == 4) {
                                if (Ajax.status == 200) {
                                    if (Ajax.responseText.length > 0) {
                                        var arrayType = "";
                                        if (Ajax.responseText.indexOf('#') != "-1") {
                                            //alert(Ajax.responseText);                                        
                                            arrayType = Ajax.responseText.split('#');
                                            if (arrayType[0].indexOf('I') == 0) {
                                                inputPaxVATcharge = arrayType[0].split('|')[0].replace('I', '');
                                                inputPaxVATcostIncluded = arrayType[0].split('|')[1];
                                            }
                                            if (arrayType[1].length > 1 && arrayType[1].indexOf('O') == 0) {
                                                outputPaxVATcharge = arrayType[1].split('|')[0].replace('O', '');
                                                outputPaxVATappliedOn = arrayType[1].split('|')[1];
                                            }
                                        }
                                        else {
                                            if (Ajax.responseText != "") {
                                                arrayType = Ajax.responseText.split('|');
                                                if (arrayType[0].indexOf('I') == 0) {
                                                    inputPaxVATcharge = arrayType[0];
                                                    inputPaxVATcostIncluded = arrayType[1];
                                                }
                                                if (arrayType[1].indexOf('O') == 0) {
                                                    outputPaxVATcharge = arrayType[0];
                                                    outputPaxVATappliedOn = arrayType[1];
                                                }
                                            }
                                        }
                                        if (inputPaxVATcostIncluded == "false") {
                                            var totFair = 0;
                                            if (!isRecalculate) {
                                                totFair=FareCalculationForPax();
                                            }
                                            else {
                                                totFair = FareReCalculationForPax(recalculateIndx); 
                                            }
                                            inputPaxVATcharge = totFair.TotalFare * (inputPaxVATcharge / 100);
                                        }
                                        if (outputPaxVATcharge > 0 || inputPaxVATcharge > 0) {
                                            var collAmount = 0;
                                            if (!isRecalculate) {
                                                collAmount=FareCalculationForPax();
                                            }
                                            else {
                                                collAmount = FareReCalculationForPax(recalculateIndx); 
                                            }                                            
                                            outputPaxVATcharge = eval(collAmount.CollectedAmount) * (outputPaxVATcharge / 100);
                                        }
                                    }
                                }
                            }
                        }

                        function PaxCalculateGST(markup, addlmarkup, transFee, addltransFee, isAsync)//GST Calulation 
                        {
                            if (addlmarkup == "") {
                                addlmarkup = 0;
                            }
                            var locationId = document.getElementById('<%=hndLocationId.ClientID%>').value;                            
                            var taxType = "GST";
                            var totMarkup = eval(markup) + eval(addlmarkup) + eval(transFee) + eval(addltransFee);
                            var paramList = 'decimalpoint=' + decimalpoint + '&locationId=' + locationId + '&totMarkup=' + totMarkup + '&taxType=' + taxType;
                            Ajax.onreadystatechange = CalculatePaxGSTComplete;
                            Ajax.open("POST", "OffLineEntryAjax", isAsync);
                            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                            Ajax.send(paramList);
                        }
                        function CalculatePaxGSTComplete() {
                            if (Ajax.readyState == 4) {
                                if (Ajax.status == 200) {
                                    if (Ajax.responseText.length > 0) {
                                        outputPaxVATcharge = Ajax.responseText;
                                    }
                                }
                            }
                        }

                        function RecalculateGSTandVat() {
                            if ($("#ctl00_cphTransaction_hdnPaxDetails").val() != "") {
                                var paxDetails = JSON.parse($("#ctl00_cphTransaction_hdnPaxDetails").val());
                                if (paxDetails.length > 0) {

                                    document.getElementById('ctl00_upProgress').style.display = 'block';
                                    $.each(paxDetails, function (paxInd, paxVal) {
                                        if (document.getElementById('<%=hdnlocCountryCode.ClientID%>').value == "IN") {
                                            PaxCalculateGST(paxVal.Price.Markup, paxVal.Price.AsvAmount, paxVal.Price.TransactionFee, paxVal.Price.AdditionalTxnFee, false);
                                        }
                                        else {
                                            PaxCalculateVAT(paxVal.Price.PublishedFare, paxVal.Price.Markup, paxVal.Price.AsvAmount, paxVal.Price.TransactionFee, paxVal.Price.AdditionalTxnFee, false)
                                        }
                                        paxDetails[paxInd].Price.InputVATAmount = inputPaxVATcharge;
                                        paxDetails[paxInd].Price.OutputVATAmount = outputPaxVATcharge;
                                        if ($("#ctl00_cphTransaction_ddlTransactionType").val() == "16") {
                                            RecalculateCancellationVatAndGst(paxVal.Price.AgentCancellationCharge, 0, false, false);
                                            RecalculateCancellationVatAndGst(paxVal.Price.SupplierCancellationCharge, 0, false, true);

                                        }
                                        paxDetails[paxInd].Price.CancellationOutVat = outputVATcharge;
                                        paxDetails[paxInd].Price.CancellationInVat = inputVATcharge;
                                        recalculateIndx++;
                                    });
                                    if ($("#ctl00_cphTransaction_ddlTransactionType").val() == "16") {
                                        if ($("#paxAgentCancelAmount").val() != "" && parseFloat($("#paxAgentCancelAmount").val()) != 0) {
                                            $("#paxAgentCancelAmount").trigger('change');
                                        }
                                        if ($("#paxSupplierCancelAmount").val() != "" && parseFloat($("#paxSupplierCancelAmount").val()) != 0) {
                                            $("#paxSupplierCancelAmount").trigger('change');
                                        }
                                    }
                                    document.getElementById('ctl00_upProgress').style.display = 'none';
                                }
                                else {
                                    if ($("#ctl00_cphTransaction_ddlTransactionType").val() == "16") {
                                        if ($("#paxAgentCancelAmount").val() != "" || parseFloat($("#paxAgentCancelAmount").val())!=0) {
                                            $("#paxAgentCancelAmount").trigger('change');
                                        }
                                        if ($("#paxSupplierCancelAmount").val() != ""|| parseFloat($("#paxSupplierCancelAmount").val())!=0) {
                                            $("#paxSupplierCancelAmount").trigger('change');
                                        }

                                    }
                                }
                                $("#ctl00_cphTransaction_hdnPaxDetails").val(JSON.stringify(paxDetails));
                                FinalCalculation();

                            }
                            else {
                                if ($("#ctl00_cphTransaction_ddlTransactionType").val() == "16") {
                                    if ($("#paxAgentCancelAmount").val() != "" || parseFloat($("#paxAgentCancelAmount").val()) != 0) {
                                        $("#paxAgentCancelAmount").trigger('change');
                                    }
                                    if ($("#paxSupplierCancelAmount").val() != "" || parseFloat($("#paxSupplierCancelAmount").val()) != 0) {
                                        $("#paxSupplierCancelAmount").trigger('change');
                                    }

                                }
                            }
                            isRecalculate = false;
                            recalculateIndx = 0;
                        }   

                        function RecalculateCancellationVatAndGst(cancelCharge, totMarkup, isAsync, isSupplier) {
                            outputVATcharge = 0;
                            inputVATcharge = 0;
                            var decimalpoint = eval('<%=agentDecimalPoints %>');
                            var origin = document.getElementById('<%=ddlSector1.ClientID%>').value;
                            var destination = document.getElementById('<%=ddldestination.ClientID%>').value;
                            var paramList = '';
                            if (document.getElementById('<%=hdnlocCountryCode.ClientID%>').value == "IN") {
                                var locationId = document.getElementById('<%=hndLocationId.ClientID%>').value;
                                paramList = 'decimalpoint=' + decimalpoint + '&locationId=' + locationId + '&totMarkup=' + cancelCharge + '&taxType=GST';
                            }
                            else {
                                paramList = 'sellingFair=' +cancelCharge + '&decimalpoint=' + decimalpoint + '&origin=' + origin + '&destination=' + destination + '&totMarkup=' + totMarkup;
                            }
                           
                            Ajax.onreadystatechange = function () {
                                if (Ajax.readyState == 4) {
                                    if (Ajax.status == 200) {
                                        var arrayType = "";
                                        if (Ajax.responseText.length > 0) {
                                            if (document.getElementById('<%=hdnlocCountryCode.ClientID%>').value == "IN") {                                                
                                                if (isSupplier) {
                                                    inputVATcharge = Ajax.responseText;
                                                }
                                                else {
                                                    outputVATcharge = Ajax.responseText;
                                                }                                                                                                
                                            }
                                            else {
                                                var inputVATchargeCancel = 0;
                                                var inputVATcostIncludedCancel = "";
                                                var outputVATchargeCancel = 0;
                                                var outputVATappliedOnCancel = "";
                                                arrayType = Ajax.responseText.split('|');
                                                
                                                if (Ajax.responseText.indexOf('#') != "-1") {                                                                              
                                                    arrayType = Ajax.responseText.split('#');
                                                    if (arrayType[0].indexOf('I') == 0) {
                                                        inputVATchargeCancel = arrayType[0].split('|')[0].replace('I', '');
                                                        inputVATcostIncludedCancel = arrayType[0].split('|')[1];
                                                    }
                                                    if (arrayType[1].length > 1 && arrayType[1].indexOf('O') == 0) {
                                                        outputVATchargeCancel = arrayType[1].split('|')[0].replace('O', '');
                                                        outputVATappliedOnCancel = arrayType[1].split('|')[1];
                                                    }
                                                }
                                                else {
                                                    if (Ajax.responseText != "") {
                                                        arrayType = Ajax.responseText.split('|');
                                                        if (arrayType[0].indexOf('I') == 0) {
                                                            inputVATchargeCancel = arrayType[0];
                                                            inputVATcostIncludedCancel = arrayType[1];
                                                        }
                                                        if (arrayType[1].indexOf('O') == 0) {
                                                            outputVATchargeCancel = arrayType[0];
                                                            outputVATappliedOnCancel = arrayType[1];
                                                        } 
                                                    }                                                    
                                                }                                                
                                                if (inputVATcostIncludedCancel == "false") {
                                                    var totFair = eval(cancelCharge);
                                                    inputVATcharge = totFair * (inputVATchargeCancel / 100);
                                                }
                                                if (outputVATchargeCancel > 0 || inputVATchargeCancel > 0) {
                                                    
                                                    outputVATcharge = eval(cancelCharge) * (outputVATchargeCancel / 100);
                                                }
                                            }
                                        }
                                    }
                                }
                            };
                            Ajax.open("POST", "OffLineEntryAjax",isAsync);
                            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                            Ajax.send(paramList);
                        }
                    });                    

                    var Durations = [];
                    function BindSectors() {

                        var ddldestination = document.getElementById('<%=ddldestination.ClientID%>');
                        var ddlSector1 = document.getElementById('<%=ddlSector1.ClientID%>');

                        if (ddldestination.value == '-1' || ddldestination.value == '')
                            alert('Please select destination');

                        if (ddlSector1.disabled) {
                            for (var i = 1; i <= 5; i++) {
                                document.getElementById('ctl00_cphTransaction_ddlSector' + i).innerHTML = ddldestination.innerHTML;
                                $('#s2id_ctl00_cphTransaction_ddlSector' + i).select2('val', '-1');
                                document.getElementById('ctl00_cphTransaction_ddlSector' + i).disabled = false;
                            }
                        }
                        else {

                            for (var i = 1; i <= 5; i++) {
                                $('#s2id_ctl00_cphTransaction_ddlSector' + i).select2('val', '-1');
                            }
                            var ddlSector6 = document.getElementById('<%=ddlSector6.ClientID%>');
                            if (!ddlSector1.disabled) {

                                for (var i = 6; i <= 10; i++) {

                                    document.getElementById('ctl00_cphTransaction_ddlSector' + i).innerHTML = '';
                                    $('#s2id_ctl00_cphTransaction_ddlSector' + i).select2('val', '-1');
                                    document.getElementById('ctl00_cphTransaction_ddlSector' + i).disabled = true;

                                }
                            }
                            document.getElementById('<%=hndsegmentsCount.ClientID%>').value = '0';
                        }                       

                    }                    

                    function BindSectorTab() {
                        Durations = [];
                        var sectorsCount = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                        var ddldestination = document.getElementById('<%=ddldestination.ClientID%>');
                        var ddlairline = document.getElementById('<%=ddlairline.ClientID%>');
                        var ddlRepAirline = document.getElementById('<%=ddlRepAirline.ClientID%>');
                        var txtTravelDt = document.getElementById('<%=txtTravelDt.ClientID%>');
                        var txtClassSector = document.getElementById('<%=txtClassSector.ClientID%>');

                        if (eval(sectorsCount) > 0) {
                            document.getElementById('sectortab').style.display = "block";
                        }
                        else
                            document.getElementById('sectortab').style.display = "none";

                        for (var i = 1; i <= sectorsCount - 1; i++) {

                            document.getElementById('ctl00_cphTransaction_row' + i).style.display = "table-row";

                            document.getElementById('ctl00_cphTransaction_ddlFromSector' + i).innerHTML = ddldestination.innerHTML;
                            document.getElementById('ctl00_cphTransaction_ddlToSector' + i).innerHTML = ddldestination.innerHTML;

                            var frmsecvalue = document.getElementById('ctl00_cphTransaction_ddlSector' + i).value;
                            var Tosectorid = i + 1;
                            var Tosecvalue = document.getElementById('ctl00_cphTransaction_ddlSector' + Tosectorid).value;

                            $('#s2id_ctl00_cphTransaction_ddlFromSector' + i).select2('val', frmsecvalue);
                            $('#s2id_ctl00_cphTransaction_ddlToSector' + i).select2('val', Tosecvalue);

                            if (ddlairline.value != '11')
                                document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).value = ddlRepAirline.value;

                            if (i == 1) {
                                txtTravelDt = new Date(txtTravelDt.value);
                                txtTravelDt = txtTravelDt.format('dd-MMM-yyyy');
                            }

                            //if (document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').value == '')
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').value = txtTravelDt;
                            //if (document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value == '')
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value = txtTravelDt;
                            //$('#ctl00_cphTransaction_DepartureDt' + i + '_Time').val("");
                            //$('#ctl00_cphTransaction_ArrivalDt' + i + '_Time').val("");
                            $('#ctl00_cphTransaction_DepartureDt' + i + '_Time').attr("onchange", "return calculateduration(this,'dep', '" + i + "');");
                            $('#ctl00_cphTransaction_ArrivalDt' + i + '_Time').attr("onchange", "return calculateduration(this,'arr', '" + i + "');");

                            document.getElementById('ctl00_cphTransaction_txtClass' + i).value = txtClassSector.value;
                            // document.getElementById('ctl00_cphTransaction_txtDuration' + i).value = '0';
                            $('span').show();
                        }

                        var txtcntrl = 'textbox', dropdown = 'dropdown';

                        for (var i = sectorsCount; i <= 20; i++) {
                            clearcntrls(dropdown, 'ddlFromSector', i, '');
                            clearcntrls(dropdown, 'ddlToSector', i, '');
                            clearcntrls(txtcntrl, 'txtFlyingCarrier', i, '');
                            clearcntrls(txtcntrl, 'txtFlightN0', i, '');
                            clearcntrls(txtcntrl, 'DepartureDt', i, '_Date');
                            clearcntrls(txtcntrl, 'DepartureDt', i, '_Time');
                            clearcntrls(txtcntrl, 'ArrivalDt', i, '_Date');
                            clearcntrls(txtcntrl, 'ArrivalDt', i, '_Time');
                            clearcntrls(txtcntrl, 'txtFareBasis', i, '');
                            clearcntrls(txtcntrl, 'txtClass', i, '');
                            clearcntrls(txtcntrl, 'txtDuration', i, '');
                            document.getElementById('ctl00_cphTransaction_row' + i).style.display = "none";

                        }
                    }

                    function ValidateSectorTab() {
                        var isValid = true;
                        var segmentlength = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                        var emptyvar = '';
                        var valfrmsector = true, valtosector = true, valflycarr = true, valfltno = true, valDepDt = true, valArrDt = true, valdates = true,
                            valfarebasis = true, valclass = true, valduration = true, focusoncntrl = true, valDeptime = true, valArrtime = true, valtraveldt = true, valsettlement = true;
                        valtraveldt = validatetraveldt();
                        if (!valtraveldt) {
                            focusoncntrl = valtraveldt;
                            $('#ctl00_cphTransaction_DepartureDt1_Date').addClass('form-text-error');
                            document.getElementById('ctl00_cphTransaction_DepartureDt1_Date').focus();
                        }

                        for (var i = 1; i <= segmentlength - 1; i++) {

                            isValid = validatemandatory('ddlFromSector', i, 'dropdown', focusoncntrl, emptyvar, 'Select');
                            if (valfrmsector && !isValid) { valfrmsector = focusoncntrl = isValid; }

                            isValid = validatemandatory('ddlToSector', i, 'dropdown', focusoncntrl, emptyvar, 'Select');
                            if (valtosector && !isValid) { valtosector = focusoncntrl = isValid; }

                            isValid = validatemandatory('txtFlyingCarrier', i, 'textbox', focusoncntrl, emptyvar, emptyvar);
                            if (valflycarr && !isValid) { valflycarr = focusoncntrl = isValid; }

                            isValid = validatemandatory('txtFlightN0', i, 'textbox', focusoncntrl, emptyvar, emptyvar);
                            if (valfltno && !isValid) { valfltno = focusoncntrl = isValid; }

                            isValid = validatemandatory('DepartureDt', i, 'textbox', focusoncntrl, '_Date', emptyvar);
                            if (valDepDt && !isValid) { valDepDt = focusoncntrl = isValid; }

                            isValid = validatemandatory('DepartureDt', i, 'textbox', focusoncntrl, '_Time', emptyvar);
                            if (valDeptime && !isValid) { valDeptime = focusoncntrl = isValid; }

                            isValid = valArrDt = validatemandatory('ArrivalDt', i, 'textbox', focusoncntrl, '_Date', emptyvar);
                            if (valArrDt && !isValid) { valArrDt = focusoncntrl = isValid; }

                            isValid = valArrtime = validatemandatory('ArrivalDt', i, 'textbox', focusoncntrl, '_Time', emptyvar);
                            if (valArrtime && !isValid) { valArrtime = focusoncntrl = isValid; }

                            //isValid = validatemandatory('txtFareBasis', i, 'textbox', focusoncntrl, emptyvar, emptyvar);
                            //if (valfarebasis && !isValid) { valfarebasis = focusoncntrl = isValid; }

                            isValid = validatemandatory('txtClass', i, 'textbox', focusoncntrl, emptyvar, emptyvar);
                            if (valclass && !isValid) { valclass = focusoncntrl = isValid; }

                            //isValid = validatemandatory('txtDuration', i, 'textbox', focusoncntrl, emptyvar, '0');
                            //if (valduration && !isValid) { valduration = focusoncntrl = isValid; }

                            isValid = validatedate('DepartureDt', 'dep', i);
                            if (valdates && !isValid) { valdates = focusoncntrl = isValid; }

                            isValid = validatedate('ArrivalDt', 'arr', i);
                            if (valdates && !isValid) { valdates = focusoncntrl = isValid; }
                        }
                        var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                        var currentSum = 0;
                        var cardCharge = 0;
                        if (ExistingSettlement != null && ExistingSettlement != "") {
                            let ExistingJson = JSON.parse(ExistingSettlement);
                            $.grep(ExistingJson.SettlementObject, function (item) {
                                currentSum = parseFloat(parseFloat(currentSum) + parseFloat(item.Amount));
                                if (item.SettlementName == "3") {
                                    cardCharge = parseFloat(parseFloat(cardCharge) + parseFloat(item.FOPDetail3));

                                }
                            });
                            currentSum=parseFloat(parseFloat(currentSum) + parseFloat(cardCharge));
                            var collectedAmount = parseFloat($("#ctl00_cphTransaction_txtCollected").val()).toFixed(decimalpoint);
                            collectedAmount = eval(collectedAmount) + eval(cardCharge);

                            if (isNaN(collectedAmount)) {
                                collectedAmount = 0.00;
                            }
                            if (parseFloat(currentSum.toFixed(decimalpoint)) != parseFloat(collectedAmount.toFixed(decimalpoint))) {
                                isValid = false;
                                valsettlement = focusoncntrl = isValid;
                            }
                        }
                        else {
                            if ($("#ctl00_cphTransaction_ddlSaleType").val() == "N") {
                                isValid = false;
                                valsettlement = focusoncntrl = isValid;
                            }
                            else {
                                var collectedAmount = parseFloat($("#ctl00_cphTransaction_txtCollected").val()).toFixed(decimalpoint);
                                if (isNaN(collectedAmount)) {
                                    collectedAmount = 0.00;
                                }
                                if (collectedAmount > 0) {
                                    if (ExistingSettlement != null && ExistingSettlement != "") {
                                        let ExistingJson = JSON.parse(ExistingSettlement);
                                        $.grep(ExistingJson.SettlementObject, function (item) {
                                            currentSum = parseFloat(parseFloat(currentSum) + parseFloat(item.Amount));
                                            if (item.SettlementName == "3") {
                                                cardCharge = parseFloat(parseFloat(cardCharge) + parseFloat(item.FOPDetail3));
                                            }
                                        });
                                        currentSum = parseFloat(parseFloat(currentSum) + parseFloat(cardCharge));
                                        collectedAmount = eval(collectedAmount) + eval(cardCharge);                                        
                                        if (parseFloat(currentSum.toFixed(decimalpoint)) != parseFloat(collectedAmount.toFixed(decimalpoint))) {
                                            isValid = false;
                                            valsettlement = focusoncntrl = isValid;
                                        }
                                    }
                                    else {
                                        isValid = false;
                                        valsettlement = focusoncntrl = isValid;
                                    }
                                }
                            }                           
                        }

                        if (!valdates) { toastr.error('Please make sure departure date and time is less than arrival date time in all segments'); }
                        if (!valtraveldt) { toastr.error('Please make sure sector-1 departure date is same as travel date'); }
                        if (!valduration) { toastr.error('Please enter duration'); }
                        if (!valclass) { toastr.error('Please enter class'); }
                        if (!valfarebasis) { toastr.error('Please enter fare basis'); }
                        if (!valArrtime) { toastr.error('Please enter arrival time'); }
                        if (!valArrDt) { toastr.error('Please enter arrival date'); }
                        if (!valDeptime) { toastr.error('Please enter departure time'); }
                        if (!valDepDt) { toastr.error('Please enter departure date'); }
                        if (!valfltno) { toastr.error('Please enter flight no'); }
                        if (!valflycarr) { toastr.error('Please enter flying carrier'); }
                        if (!valtosector) { toastr.error('Please select to Sector'); }
                        if (!valfrmsector) { toastr.error('Please select from Sector'); }
                        if (!valsettlement) { toastr.error('Please check the settlement details'); }

                        return focusoncntrl;
                    }

                    function validatemandatory(cntrlid, sectorno, cntrltype, focusoncntrl, datetime, defaultval) {

                        var valid = true;

                        if (cntrltype == 'dropdown') {

                            var ddlvalue = document.getElementById('s2id_ctl00_cphTransaction_' + cntrlid + sectorno).innerText.trim();
                            if (ddlvalue == defaultval || ddlvalue == '') {
                                $('#s2id_ctl00_cphTransaction_' + cntrlid + sectorno).parent().addClass('form-text-error');
                                if (focusoncntrl)
                                    document.getElementById('s2id_ctl00_cphTransaction_' + cntrlid + sectorno).focus();
                                valid = false;
                            }

                        }

                        if (cntrltype == 'textbox') {
                            var txtvalue = document.getElementById('ctl00_cphTransaction_' + cntrlid + sectorno + datetime).value.trim();
                            if (txtvalue == '' || txtvalue == defaultval) {
                                $('#ctl00_cphTransaction_' + cntrlid + sectorno + datetime).addClass('form-text-error');
                                if (focusoncntrl)
                                    document.getElementById('ctl00_cphTransaction_' + cntrlid + sectorno + datetime).focus();
                                valid = false;
                            }
                        }

                        return valid;
                    }

                    function clearcntrls(cntrltype, cntrlid, sectorid, datetime) {

                        if (cntrltype == 'textbox') {
                            document.getElementById('ctl00_cphTransaction_' + cntrlid + sectorid + datetime).value = '';
                        }

                        if (cntrltype == 'dropdown') {

                            $('#s2id_ctl00_cphTransaction_' + cntrlid + sectorid).select2('val', '-1');
                            document.getElementById('ctl00_cphTransaction_' + cntrlid + sectorid).innerHTML = '';
                        }
                    }

                    function Savesegments(ev) {
                        
                        ev.preventDefault();
                        $("#ctl00_cphTransaction_btnSave").prop('disabled', true)
                        document.getElementById('ctl00_upProgress').style.display = 'block';
                        var isValid = true;
                        if ($("#chkDraftmode").prop('checked')) {
                            isValid = ValidateCtrls(true);

                            if (!isValid) {
                                ev.preventDefault();
                                //$("#ctl00_cphTransaction_btnSave").prop('disabled', false);
                                if (!isValid) {
                                    document.getElementById('ctl00_upProgress').style.display = 'none';
                                }
                                $("#ctl00_cphTransaction_btnSave").prop('disabled', false);
                                return isValid;
                            }

                        }                       

                        var segmentlength = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                        var segmentvalues = '';

                        for (var i = 1; i <= segmentlength - 1; i++) {

                            segmentvalues += document.getElementById('ctl00_cphTransaction_ddlFromSector' + i).value + '-' + document.getElementById('ctl00_cphTransaction_ddlToSector' + i).value + '|';

                        }
                        segmentvalues = segmentvalues.slice(0, -1);
                        document.getElementById('<%=hdnsegments.ClientID%>').value = segmentvalues;
                       
                        if (document.getElementById('divCorporate').style.display == 'block') {
                            document.getElementById('<%=hdnprofileId.ClientID%>').value = $("#ddlProfiles").val();
                            document.getElementById('<%=hdnTravelResonId.ClientID%>').value = $("#ddlTravelReason").val().split('~')[0];
                        }
                        if (!$("#chkDraftmode").prop('checked')) {
                            isValid = ValidateFlex(document.getElementById('<%=hdnPaxFlexInfo.ClientID%>').id, false);
                            if (!isValid) {
                                //$("#ctl00_cphTransaction_btnSave").prop('disabled', false)
                                ev.preventDefault();
                            }
                        }
                        if (!isValid) {
                            document.getElementById('ctl00_upProgress').style.display = 'none';
                            $("#ctl00_cphTransaction_btnSave").prop('disabled', false);
                            return isValid
                        }
                        else {
                            __doPostBack('<%=btnSave.UniqueID %>', '');
                            return false;
                        }
                       
                    }

                    function GetAgentInfo() {
                        var agentid = document.getElementById('<%=ddlCustomer1.ClientID%>').value;
                        var agentinfo = '';
                        $.ajax({
                            type: "POST",
                            url: "OfflineEntry.aspx/CheckAgentBalance",
                            contentType: "application/json; charset=utf-8",
                            data: "{'sAgentId':'" + agentid + "'}",
                            dataType: "json",
                            async: false,
                            success: function (Et) {
                                if (Et.d != '') {
                                    agentinfo = Et.d;
                                }
                            },
                            failure: function (error) {
                                alert(error);
                                return false;
                            }
                        });
                        return agentinfo;
                    }
                    
                    function Changemode() {                      
                        var collectedAmount = 0.00;
                        collectedAmount = parseFloat($("#ctl00_cphTransaction_txtCollected").val()).toFixed(decimalpoint);                        
                        if (parseFloat(collectedAmount) > 0) {
                            var SettlementMode = $('#ctl00_cphTransaction_ddlMode').val();
                            if (SettlementMode != "-1") {
                                var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                                var existingMode = [];
                                var currentSum = 0;
                                if (ExistingSettlement != null && ExistingSettlement != "") {
                                    let ExistingJson = JSON.parse(ExistingSettlement);
                                    existingMode = $.grep(ExistingJson.SettlementObject, function (item) {
                                        currentSum = parseFloat(parseFloat(currentSum) + parseFloat(item.Amount));
                                        return item.SettlementName == SettlementMode;
                                    });
                                }
                                if (existingMode.length > 0) {
                                    toastr.error($('#ctl00_cphTransaction_ddlMode option:selected').text() + ' already added');
                                    $('#ctl00_cphTransaction_ddlMode').select2('val', '-1');
                                }
                                else if (parseFloat(currentSum) >= parseFloat(collectedAmount)) {
                                    toastr.error('Amount limit exceed');
                                    $('#ctl00_cphTransaction_ddlMode').select2('val', '-1');
                                }
                                else if ($("#ctl00_cphTransaction_ddlCustomer1").val() == "-1") {
                                    toastr.error('Please select customer');
                                    $('#ctl00_cphTransaction_ddlMode').select2('val', '-1');
                                }
                                else {
                                    let SettlementDiv = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-id');
                                    $('#SettlementModalContent .modal-body').hide();
                                    if ($('#ctl00_cphTransaction_ddlMode option:selected').val() == "2") {
                                        // document.getElementById('ctl00_upProgress').style.display = 'block';
                                        //$.ajax({
                                        //    url: apiUrl+"CMSMasterList",
                                        //    type: "POST",
                                        //    data: JSON.stringify({ "AccountType": "R" }),
                                        //    dataType: "json",
                                        //    contentType: "application/json; charset=utf-8",
                                        //    success: function (cmsResponse) {
                                        //        var cmsJson = cmsResponse;
                                        //        $("#creditModalBranch").children().remove();
                                        //        if (cmsJson.CMSMasterList.length > 0) {
                                        //            $("#creditModalBranch").append('<option value="-1">--Select Branch--</option>');
                                        //            $.each(cmsJson.CMSMasterList, function (cmsInd, cmsVal) {
                                        //                $("#creditModalBranch").append('<option value="' + cmsVal.Code + '" data-activity="'+cmsVal.GroupName+'">' + cmsVal.Code + ' - ' + cmsVal.Name + '</option>')
                                        //            });
                                        //            $("#creditModalBranch").select2('val', '-1');
                                        //        }  
                                        //        document.getElementById('ctl00_upProgress').style.display = 'none';
                                        //    },
                                        //    error: function () {
                                        //        alert("No customer data available");
                                        //        document.getElementById('ctl00_upProgress').style.display = 'none';
                                        //    }
                                        //});
                                        $('#SettlementModalContent').modal('show');
                                        $("#settlementModeText").text($('#ctl00_cphTransaction_ddlMode option:selected').text());
                                        $("#creditModalBranch").val($("#ctl00_cphTransaction_ddlCustomer1 option:selected").text())
                                        $('#' + SettlementDiv + '').show();
                                        let remainingAmount = parseFloat(collectedAmount) - parseFloat(currentSum);
                                        $('#' + SettlementDiv + '').find('.amt').val(parseFloat(remainingAmount).toFixed(decimalpoint));
                                    }
                                    if ($('#ctl00_cphTransaction_ddlMode option:selected').val() == "3") {
                                        $('#SettlementModalContent').modal('show');
                                        $("#settlementModeText").text($('#ctl00_cphTransaction_ddlMode option:selected').text());
                                        $('#' + SettlementDiv + '').show();
                                        let remainingAmount = parseFloat(collectedAmount) - parseFloat(currentSum);
                                        $('#' + SettlementDiv + '').find('.amt').val(parseFloat(remainingAmount).toFixed(decimalpoint));
                                        $("#cardType").select2("val", "-1");
                                        $("#cardNumber").val("");
                                        $("#approvalNumber").val("");
                                        $("#expiryDate").val("");
                                        $("#cardCharge").val(0);
                                        $("#lblIncludeCharge").text("");
                                        $("#settlementRemarkCard").val("");
                                    }
                                    else {
                                        $('#SettlementModalContent').modal('show');
                                        $("#settlementModeText").text($('#ctl00_cphTransaction_ddlMode option:selected').text());
                                        $('#' + SettlementDiv + '').show();
                                        let remainingAmount = parseFloat(collectedAmount) - parseFloat(currentSum);
                                        $('#' + SettlementDiv + '').find('.amt').val(parseFloat(remainingAmount).toFixed(decimalpoint));
                                    }
                                    var ddlMode = document.getElementById('<%=ddlMode.ClientID%>').value;
                                    if (ddlMode == '2') {
                                        $('#spnCustomer').show();
                                    }
                                    else {
                                        $('#spnCustomer').hide();
                                    }
                                }
                            }
                        }
                        else {
                            toastr.error('Please enter Selling Fare');
                            $('#ctl00_cphTransaction_ddlMode').select2('val', '-1');
                        }

                    }
                    function loadSalesExecutive(agentId) {
                        $.ajax({
                            url: "OfflineEntry.aspx/LoadSalesExecutive",
                            type: "POST",
                            data: JSON.stringify({ 'agentId': agentId }),
                            dataType: "json",
                            contentType: "application/json; charset=utf-8",
                            success: function (executiveResponse) {
                                let exectiveData = JSON.parse(executiveResponse.d);
                                $("#ctl00_cphTransaction_ddlSalesExecutive").children().remove();
                                $("#ctl00_cphTransaction_ddlSalesExecutive").append('<option value="-1" selected>--Select Sales Executive--</option>');
                                $.each(exectiveData, function (executiveIndex, execitiveItem) {
                                    $("#ctl00_cphTransaction_ddlSalesExecutive").append('<option value="' + execitiveItem.userId + '">' + execitiveItem.Uer_name + '</option>');
                                });
                                $("#ctl00_cphTransaction_ddlSalesExecutive").select2('val', $("#ctl00_cphTransaction_ddlSalesExecutive option:eq(1)").val());
                            }
                        });
                    }

                    function customLocationLoad() {
                        var sel = document.getElementById('<%=ddlCustomer1.ClientID%>').value;
                        $.ajax({
                            url: "OfflineEntry.aspx/LoadCustomerlocation",
                            type: "POST",
                            data: JSON.stringify({ 'agentId': sel }),
                            dataType: "json",
                            async: false,
                            contentType: "application/json; charset=utf-8",
                            success: function (executiveResponse) {
                                var LocationJson = JSON.parse(executiveResponse.d);
                                $("#ctl00_cphTransaction_ddlConsultant,#cashModalBranch,#glModalBranch").empty();
                                $("#ctl00_cphTransaction_ddlConsultant,#cashModalBranch,#glModalBranch").append($("<option></option>").val('0').html('Select Location'));
                                $.each(LocationJson, function (ind, valu) {
                                    $("#ctl00_cphTransaction_ddlConsultant,#cashModalBranch,#glModalBranch").append($("<option></option>").val(valu.LOCATION_ID).html(valu.LOCATION_CODE + valu.MAP_CODE_NAME));
                                });
                            },
                            error: function () {
                                alert("No customer data available");
                                document.getElementById('ctl00_upProgress').style.display = 'none';
                            }
                        });
                    }

                    function Customerchange() {
                        document.getElementById('ctl00_upProgress').style.display = 'block';
                        document.getElementById('<%=hndLocationId.ClientID%>').value = "";
                        var agentid = document.getElementById('<%=ddlCustomer1.ClientID%>').value;
                        if (agentid != '0' && agentid != '-1') {
                            var agentinfo = GetAgentInfo();
                            var agentdetails = agentinfo.split('|');
                            document.getElementById('spnagentBalance').style.display = 'block';
                            selectedAgentBalance = parseFloat(agentdetails[1]);
                            $('#lblCusBalance').text('(' + agentdetails[0] + ' ' + agentdetails[1] + ')');
                            $("#ctl00_cphTransaction_hdnCurrencyCode").val(agentdetails[0]);
                            $("#ctl00_cphTransaction_ddlcurrencycode").children().remove();
                            $("#ctl00_cphTransaction_ddlcurrencycode").append('<option value="' + agentdetails[0] + '" selected>' + agentdetails[0] + '</option>');
                            customLocationLoad();
                        }
                        else {
                            $('#lblCusBalance').text('');
                            document.getElementById('spnagentBalance').style.display = 'none';
                        }
                        //load corporate Travel Reason & cprofile ids
                        $('#divFlexFields0').empty();

                        $("#ddlTravelReason").empty();
                        $("#ddlTravelReason").append($("<option></option>").val('0').html('Select Reason'));

                        $("#ddlProfiles").empty();
                        $("#ddlProfiles").append($("<option></option>").val('0').html('Select Profile'));

                        var corporatedata = AjaxCall("OfflineEntry.aspx/GetCorporateTravelReasonAndProfiles", "{'agentid':'" + agentid + "'}");
                        if (corporatedata != null && corporatedata.length > 0) {
                            document.getElementById('divCorporate').style.display = 'block';
                            var obj = JSON.parse(corporatedata[0]);
                            $.each(obj, function (corpdata, value) {
                                $("#ddlTravelReason").append($("<option></option>").val(value.ReasonId).html(value.Description));

                            });
                            var obj = JSON.parse(corporatedata[1]);
                            $.each(obj, function (prodata, value) {
                                $("#ddlProfiles").append($("<option></option>").val(value.Profile).html(value.ProfileName));
                            });
                            $("#ddlTravelReason").select2('val', '0');
                            $("#ddlProfiles").select2('val', '0');

                        }
                        else {
                            document.getElementById('divCorporate').style.display = 'none';
                        }
                        GetFlexFields();
                        document.getElementById('ctl00_upProgress').style.display = 'none';
                        loadSalesExecutive(agentid);
                    }
                        
                    var loc;
                    function LoadAgentLocations() {
                        var location = 'ctl00_cphTransaction_ddlConsultant';
                        loc = location;
                        var sel = document.getElementById('<%=ddlCustomer1.ClientID%>').value;
                        if (sel == '-1' || sel == '')
                            sel = document.getElementById('<%=hdnAgentId.ClientID%>').value;

                        var paramList = 'requestSource=getAgentsLocationsByAgentId&AgentId=' + sel + '&id=' + location;
                        var url = "CityAjax?" + paramList;

                        if (window.XMLHttpRequest) {
                            Ajax = new XMLHttpRequest();
                        }
                        else {
                            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
                        }
                        Ajax.onreadystatechange = BindLocationsList;
                        Ajax.open('POST', url);
                        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                        Ajax.send();
                    }

                    function BindLocationsList() {
                        if (Ajax.readyState == 4 && Ajax.status == 200) {
                            var ddl = document.getElementById(loc);
                            if (Ajax.responseText.length > 0) {
                                if (ddl != null) {
                                    ddl.options.length = 0;
                                    var el = document.createElement("option");
                                    el.textContent = "--Select Location--";
                                    el.value = "-1";
                                    ddl.add(el, 0);
                                    var values = Ajax.responseText.split('#')[1].split(',');
                                    for (var i = 0; i < values.length; i++) {
                                        var opt = values[i];
                                        if (opt.length > 0 && opt.indexOf('|') > 0) {

                                            var el = document.createElement("option");
                                            el.textContent = opt.split('|')[0];
                                            el.value = opt.split('|')[1];
                                            ddl.appendChild(el);
                                        }
                                    }

                                    $('#' + ddl.id).select2('val', '-1');
                                }
                            }
                            else//Clear previous agent locations if no locations found for the current agent
                            {
                                if (ddl != null) {
                                    ddl.options.length = 0;
                                    var el = document.createElement("option");
                                    el.textContent = "--Select Location--";
                                    el.value = "-1";
                                    ddl.add(el, 0);
                                    $('#' + ddl.id).select2('val', '-1');//Select Location
                                }
                            }
                        }
                    }

                    function comparedate1(mindate, maxdate, message, event) {

                        if (mindate > maxdate && mindate != '' && maxdate != '' && mindate != null && maxdate != null) {
                            alert(message);
                            event.value = '';
                            event.focus();
                            return false;
                        }
                        else
                            return true;
                    }

                    function comparedate(mindate, maxdate, sectorno, cntrlid) {

                        if (mindate > maxdate && mindate != '' && maxdate != '' && mindate != null && maxdate != null) {
                            $('#ctl00_cphTransaction_' + cntrlid + sectorno + '_Date').addClass('form-text-error');
                            $('#ctl00_cphTransaction_' + cntrlid + sectorno + '_Time').addClass('form-text-error');
                            return false;
                        }
                        else
                            return true;
                    }

                    function validatedate(event, source, sectorno) {
                        var segmentlength = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                        segmentlength = segmentlength - 1;
                        var currsecfrmdt = GetDateTimeObject('ctl00_cphTransaction_DepartureDt' + sectorno);
                        var currsectodt = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + sectorno);
                        if (segmentlength == 1 || (sectorno == 1 && source == 'dep') || (sectorno == segmentlength && source == 'arr')) {

                            if (comparedate(currsecfrmdt, currsectodt, sectorno, event))
                                return true;
                            else
                                return false;
                        }
                        var nextprevsectorno = sectorno;
                        var nextprevsecdate = currsecfrmdt;
                        if (source == 'dep') {
                            nextprevsectorno = Math.ceil(sectorno) - 1;
                            nextprevsecdate = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + nextprevsectorno);
                            if (!comparedate(nextprevsecdate, currsecfrmdt, sectorno, event))
                                return false;
                        }
                        if (source == 'arr') {
                            nextprevsectorno = Math.ceil(sectorno) + 1;
                            nextprevsecdate = GetDateTimeObject('ctl00_cphTransaction_DepartureDt' + nextprevsectorno);
                            if (!comparedate(currsectodt, nextprevsecdate, sectorno, event))
                                return false;
                        }
                        if (comparedate(currsecfrmdt, currsectodt, sectorno, event))
                            return true;
                        else
                            return false;
                    }

                    function validatetraveldt() {
                        var fromDate = document.getElementById('ctl00_cphTransaction_DepartureDt1_Date').value;
                        var travelDt = new Date(document.getElementById('<%=txtTravelDt.ClientID%>').value);
                        travelDt = travelDt.format('dd-MMM-yyyy');
                        if (fromDate != null && travelDt != null) {
                            if (fromDate != travelDt)
                                return false;
                        }
                        return true;
                    }
                    function minuteToHHMM(min) {
                        var mins_num = parseFloat(min, 10); // don't forget the second param
                        var hours = Math.floor(mins_num / 60);
                        var minutes = Math.floor((mins_num - ((hours * 3600)) / 60));
                        var seconds = Math.floor((mins_num * 60) - (hours * 3600) - (minutes * 60));

                        // Appends 0 when unit is less than 10
                        if (hours < 10) { hours = "0" + hours; }
                        if (minutes < 10) { minutes = "0" + minutes; }
                        if (seconds < 10) { seconds = "0" + seconds; }
                        return hours + ':' + minutes;
                    }

                    function calculateduration(event, source, sectorno) {
                        var fromDate = GetDateTimeObject('ctl00_cphTransaction_DepartureDt' + sectorno);
                        var toDate = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + sectorno);
                        var frmtime = document.getElementById('ctl00_cphTransaction_DepartureDt' + sectorno + '_Time').value;
                        var totime = document.getElementById('ctl00_cphTransaction_ArrivalDt' + sectorno + '_Time').value;

                        if ((frmtime != '' && frmtime.length != 5) || (totime != '' && totime.length != 5)) {
                            toastr.error('Please enter valid time(00:01 - 23:59)');
                            event.value = '';
                            event.focus();
                            return false;
                        }

                        if (fromDate != null && toDate != null && frmtime != '' && totime != '') {

                            if (fromDate >= toDate) {
                                document.getElementById('ctl00_cphTransaction_txtDuration' + sectorno).value = '';
                                toastr.error('Departure from date and time should be less than arrival to date and time');
                                document.getElementById('ctl00_cphTransaction_DepartureDt' + sectorno + '_Time').value = "";
                                document.getElementById('ctl00_cphTransaction_ArrivalDt' + sectorno + '_Time').value = "";
                                return false;
                            }
                            else {

                                //var frmtime = document.getElementById('ctl00_cphTransaction_DepartureDt' + sectorno + '_Time').value;
                                var frmarr = frmtime.split(':');
                                var frmhrs = frmarr[0];
                                var frmmins = frmarr[1];

                                //var totime = document.getElementById('ctl00_cphTransaction_ArrivalDt' + sectorno + '_Time').value;
                                var toarr = totime.split(':');
                                var tohrs = toarr[0];
                                var tomins = toarr[1];
                                if (Math.ceil(frmhrs) < 0 || Math.ceil(frmhrs) > 23 || Math.ceil(tohrs) < 0 || Math.ceil(tohrs) > 23
                                    || Math.ceil(frmmins) < 0 || Math.ceil(frmmins) > 60 || Math.ceil(tomins) < 0 || Math.ceil(tomins) > 60) {
                                    toastr.error('Please enter valid time(00:01 - 23:59)');
                                    event.value = '';
                                    event.focus();
                                    return false;
                                }
                                if (frmmins > 0) {
                                    frmmins = 60 - Math.ceil(frmmins);
                                    frmhrs = Math.ceil(frmhrs) + 1;
                                }
                                var duration = ((Math.ceil(tohrs) - Math.ceil(frmhrs)) * 60) + Math.ceil(frmmins) + Math.ceil(tomins);
                                var startDay = new Date(fromDate);
                                var endDay = new Date(toDate);
                                var millisecondsPerDay = 1000 * 60 * 60 * 24;

                                var millisBetween = endDay.getTime() - startDay.getTime();
                                var days = Math.ceil(millisBetween / millisecondsPerDay) - 1;

                                if (duration <= 0)
                                    days = Math.ceil(days) + 1;
                                duration = (Math.ceil(1440) * Math.ceil(days)) + Math.ceil(duration);
                                if (typeof Durations[sectorno - 1] === "undefined") {
                                    Durations.push(duration);
                                }
                                else {
                                    Durations[sectorno - 1] = duration;
                                }
                                var durationString = Durations.join(",");
                                $("#<%=hdnDuration.ClientID%>").val(durationString);
                                document.getElementById('ctl00_cphTransaction_txtDuration' + sectorno).value = minuteToHHMM(duration);
                            }
                        }
                        else
                            document.getElementById('ctl00_cphTransaction_txtDuration' + sectorno).value = '0';

                    }

                    function SaveSuccess(status) {

                        if (status == 'S') {
                            //alert('Itenary saved successfully');
                            //window.open('OfflineEntryPrintReceipt?FlightId=' + document.getElementById('ctl00_cphTransaction_hdnFlightId').value + '', '', 'width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
                            window.open('OfflineEntryPrintReceipt?FlightId=' + document.getElementById('ctl00_cphTransaction_hdnFlightId').value + '', '', 'width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
                        }
                        else
                            alert('Failed to save itinerary, please check the logs.');
                        window.location = 'OffLineEntry';

                    }

                    function Bindsectortabddls() {

                        for (var i = 1; i <= sectorsCount - 1; i++) {

                            document.getElementById('ctl00_cphTransaction_ddlFromSector' + i).innerHTML = '';
                            document.getElementById('ctl00_cphTransaction_ddlToSector' + i).innerHTML = '';
                            document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).value = '';
                            document.getElementById('ctl00_cphTransaction_txtFlightN0' + i).value = '';
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').value = '';
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').value = '';
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').value = '';
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').value = '';
                            document.getElementById('ctl00_cphTransaction_txtFareBasis' + i).value = '';
                            document.getElementById('ctl00_cphTransaction_txtClass' + i).value = '';
                            document.getElementById('ctl00_cphTransaction_txtDuration' + i).value = '';

                            document.getElementById('ctl00_cphTransaction_chkIsRefund' + i).style.display = "none";
                            document.getElementById('ctl00_cphTransaction_ddlFromSector' + i).style.display = "none";
                            document.getElementById('ctl00_cphTransaction_ddlToSector' + i).style.display = "none";
                            document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).style.display = "none";
                            document.getElementById('ctl00_cphTransaction_txtFlightN0' + i).style.display = "none";
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').style.display = "none";
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').style.display = "none";
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').style.display = "none";
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').style.display = "none";
                            document.getElementById('ctl00_cphTransaction_txtFareBasis' + i).style.display = "none";
                            document.getElementById('ctl00_cphTransaction_txtClass' + i).style.display = "none";
                            document.getElementById('ctl00_cphTransaction_txtDuration' + i).style.display = "none";

                            document.getElementById('ctl00_cphTransaction_ddlFromSector' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_ddlToSector' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_txtFlyingCarrier' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_txtFlightN0' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Date').style.display = "block";
                            document.getElementById('ctl00_cphTransaction_DepartureDt' + i + '_Time').style.display = "block";
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Date').style.display = "block";
                            document.getElementById('ctl00_cphTransaction_ArrivalDt' + i + '_Time').style.display = "block";
                            document.getElementById('ctl00_cphTransaction_txtFareBasis' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_txtClass' + i).style.display = "block";
                            document.getElementById('ctl00_cphTransaction_txtDuration' + i).style.display = "block";

                        }
                    }

                    function validatedate_old(event, source, sectorno) {

                        var segmentlength = document.getElementById('<%=hndsegmentsCount.ClientID%>').value;
                        segmentlength = segmentlength - 1;
                        var currsecfrmdt = GetDateTimeObject('ctl00_cphTransaction_DepartureDt' + sectorno);
                        var currsectodt = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + sectorno);
                        if (segmentlength == 1 || (sectorno == 1 && source == 'dep') || (sectorno == segmentlength && source == 'arr')) {
                            if (comparedate(currsecfrmdt, currsectodt, 'Departure date & time should be less than arrival date and time', event))
                                return true;
                            else
                                return false;
                        }
                        var nextprevsectorno = sectorno;
                        var message = '';
                        var nextprevsecdate = currsecfrmdt;
                        if (source == 'dep') {
                            nextprevsectorno = Math.ceil(sectorno) - 1;
                            nextprevsecdate = GetDateTimeObject('ctl00_cphTransaction_ArrivalDt' + nextprevsectorno);
                            message = 'Current sector departure date & time should be greater than previous sector arrival date & time';
                        }
                        if (source == 'arr') {
                            nextprevsectorno = Math.ceil(sectorno) + 1;
                            nextprevsecdate = GetDateTimeObject('ctl00_cphTransaction_DepartureDt' + nextprevsectorno);
                            message = 'Current sector departure date & time should be greater than previous sector arrival date & time';
                        }
                        if (comparedate(nextprevsecdate, currsecfrmdt, message, event)) {
                            if (comparedate(currsecfrmdt, currsectodt, 'Departure date & time should be less than arrival date and time', event))
                                return true;
                            else
                                return false;
                        }
                        else
                            return false;
                    }
                    function GetFormattedDate() {
                        var todayTime = new Date();
                        var month = (todayTime.getMonth() + 1);
                        var day = (todayTime.getDate());
                        var year = (todayTime.getFullYear());
                        return month + "/" + day + "/" + year;
                    }

                    function GetFlexFields() {

                        $('#divFlexFields0').empty();
                        var agentid = document.getElementById('<%=ddlCustomer1.ClientID%>').value;
                        var TravelReason = $("#ddlTravelReason").val();
                        TravelReason = !IsEmpty(TravelReason) && TravelReason != '0' ? TravelReasonsplit('~')[0] : 0;
                        if (agentid != '0' && agentid != '-1') {
                            var data = AjaxCall("OfflineEntry.aspx/LoadFlexControls", "{'agentid':'" + agentid + "'}");
                            if (!IsEmpty(data)) {
                                BindFlexFields(data, '', 'divFlex', 'divFlexFields', 1, TravelReason);
                            }
                            else {
                                document.getElementById('divFlex0').style.display = 'none';
                                $('#divFlexFields0').empty();
                            }
                        }
                    }

                    $(function () {
                        document.getElementById('<%= txtSalesDate.ClientID%>').value = GetFormattedDate();
                    });
                </script>
                
            </div>
        </ContentTemplate>
    </asp:UpdatePanel>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>

