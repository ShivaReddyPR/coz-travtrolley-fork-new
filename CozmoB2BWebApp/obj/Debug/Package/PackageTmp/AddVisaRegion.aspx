﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="AddVisaRegion" Title="Cozmo Travels"
    ValidateRequest="false" Codebehind="AddVisaRegion.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<link rel="stylesheet" type="text/css" href="css/style.css" />
    
    
    
           
           <div> 
    <div class="col-md-6"> <h4> 
    <% if (UpdateRegion > 0)
               {%>
            <span>Update Visa Region</span>
            <% }
               else
               {  %>
            <span>Add Visa Region</span>
            <% }
            %>
            
    
    </h4> </div>
    
    <div class="col-md-6">
    
    
    <asp:HyperLink ID="HyperLink1" class="fcol_blue pull-right" runat="server" NavigateUrl="~/VisaRegionList.aspx">Go to Visa Region List</asp:HyperLink>
    
    
    
    
   

 </div>     
    
    <div class="clearfix"> </div> 
    </div>
    
    
    <div>
            <asp:Label class="font-red" runat="server" ID="lblErrorMessage" Text=""></asp:Label>
        </div>
        
        
                <div class="paramcon"> 
    <div class="col-md-2"> <label>
                        Region Name<sup>*</sup></label></div>
    
    <div class="col-md-2"><p>
                    
                    <span>
                        <asp:TextBox ID="txtRegionName"  CssClass="form-control" runat="server"></asp:TextBox>&nbsp;</span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server"
                        ErrorMessage="Please fill region name " ControlToValidate="txtRegionName"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="rgcityName" runat="server" ErrorMessage="Please enter only character"
                        ValidationExpression="[a-zA-Z\s]+" ControlToValidate="txtRegionName" Display="Dynamic"></asp:RegularExpressionValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ErrorMessage="Please enter maximum 50 character"
                        ValidationExpression="[\S\s]{0,50}" ControlToValidate="txtRegionName" Display="Dynamic"></asp:RegularExpressionValidator></p> </div>   
 
    <div class="col-md-2"><label>
                        Region Code<sup>*</sup></label> </div>
    
    <div class="col-md-2"> <p>
                    
                    <span>
                        <asp:TextBox ID="txtRegionCode" CssClass="form-control" runat="server"
                            MaxLength="4"></asp:TextBox>&nbsp;</span>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" Display="Dynamic"
                        ErrorMessage="Please fill max four character" ValidationExpression="^[a-z,A-Z]{2,4}$"
                        ControlToValidate="txtRegionCode"></asp:RegularExpressionValidator>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please fill region code "
                        ControlToValidate="txtRegionCode" Display="Dynamic"></asp:RequiredFieldValidator>
                </p></div>    

    <div class="col-md-4"><asp:Button ID="btnSave" runat="server" Text=" Save "  CssClass="but but_b" 
                                OnClick="btnSave_Click" /> </div>
    

    </div>
    
    
    
    
    

</asp:Content>
