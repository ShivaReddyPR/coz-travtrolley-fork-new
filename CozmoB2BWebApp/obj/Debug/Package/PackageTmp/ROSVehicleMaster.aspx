﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="ROSVehicleMasterGUI" Title="ROS Vehicle Master" Codebehind="ROSVehicleMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script>
    function Validation() {

        if (getElement('txtVehicleCode').value == '') addMessage('Vehicle Code cannot be blank!', '');

        if (getElement('txtVehicleName').value == '') addMessage('Vehicle Name cannot be blank!', '');
        if (getElement('txtVehicleNumber').value == '') addMessage('Vehicle Number cannot be blank!', '');
       
        if (getMessage() != '') {

            alert(getMessage()); clearMessage(); return false;
        }
    }
</script>



<div class="body_container paramcon">
        
        
             <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> Vehicle Code:*</div>
    <div class="col-md-2"> <asp:TextBox ID="txtVehicleCode" MaxLength="10" CssClass="inputEnabled form-control"  runat="server"
                            ></asp:TextBox></div>
    <div class="col-md-2"> Vehicle Name:*</div>
    
     <div class="col-md-2"> <asp:TextBox ID="txtVehicleName" MaxLength="50" CssClass="inputEnabled form-control"  runat="server"></asp:TextBox></div>
     <div class="col-md-2">Vehicle Number:* </div>
         <div class="col-md-2"> <asp:TextBox ID="txtVehicleNumber" MaxLength="20" CssClass="inputEnabled form-control"  runat="server"></asp:TextBox></div>

<div class="clearfix"></div>
    </div>
    
    
    
   <div class="col-md-12 marbot_10">            
    
    <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="lblSuccess"></asp:Label>
                     <asp:Button ID="btnSave" CssClass="btn but_b pull-right marleft_10" Text="Save" runat="server" OnClientClick="return Validation();"  OnClick="btnSave_Click" style="display:inline;" />
                 
                     <asp:Button ID="btnClear" CssClass="btn but_b pull-right marleft_10" Text="Clear" runat="server" OnClick="btnClear_Click" style="display:inline;"/>&nbsp;&nbsp;
                
                     <asp:Button ID="btnSearch" CssClass="btn but_b pull-right marleft_10" Text="Search" runat="server" OnClick="btnSearch_Click" style="display:inline;"/>
                
    
    </div> 
    
    
        
        <div class="clearfix"> </div>
         
          </div>
          
          


 
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" Runat="Server">
<asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="ve_id" 
    EmptyDataText="No ROS Vehicle List Available!" AutoGenerateColumns="false" PageSize="3" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging" >
    
    <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />" ControlStyle-Width="25px"  ControlStyle-CssClass="label" ShowSelectButton="True" />
    
      
     <asp:TemplateField>
     <HeaderStyle HorizontalAlign="left"/>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc1:Filter   ID="HTtxtVehCode" Width="120px" HeaderText="Vehicle Code" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblVehCode" runat="server" Text='<%# Eval("ve_code") %>' CssClass="label grdof" ToolTip='<%# Eval("ve_code") %>' Width="120px"></asp:Label>
 
    </ItemTemplate>    
    
    </asp:TemplateField>  
    
   
    
       <asp:TemplateField>
        <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtVehName"  Width="120px" CssClass="inputEnabled" HeaderText="Vehicle Name" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblVehName" runat="server" Text='<%# Eval("ve_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("ve_name") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    

 <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtVehNumber"  Width="120px" CssClass="inputEnabled" HeaderText="Vehicle Number" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblVehNumber" runat="server" Text='<%# Eval("ve_number") %>' CssClass="label grdof"  ToolTip='<%# Eval("ve_number") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    
     </Columns>    
     <HeaderStyle CssClass="gvHeader"></HeaderStyle>
        <RowStyle CssClass="gvRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvAlternateRow" />  
    </asp:GridView>
</asp:Content>

