﻿<%@ Page MasterPageFile="~/TransactionBE.master" Language="C#" AutoEventWireup="true" Title="Itinerary Add Service Master"
    Inherits="ItineraryAddServiceMasterGUI" Codebehind="ItineraryAddServiceMaster.aspx.cs" %>

<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

<script type="text/javascript">

    

    //This function validates the user input which accepts only +ve integers with or without decimals.
    //This function will be invoked when the user enters the Adult ,Child,Infant Fare Details.
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 57)) {
            return false;
        }
        return true;
    }

    //This function will validate the user input and allows the user to enter only alpha numeric             characters.
    //This function will be invoked when the user enters the PNR's.
    var specialKeys = new Array();
    specialKeys.push(8); //Backspace
    specialKeys.push(9); //Tab
    specialKeys.push(46); //Delete
    specialKeys.push(36); //Home
    specialKeys.push(35); //End
    specialKeys.push(37); //Left
    specialKeys.push(39); //Right
    function IsAlphaNumeric(e) {
        var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
        var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));

        return ret;
    }



    function Save() {


       
        var selcountriesCount = 0;
        var chkCountries = document.getElementById("<%=chkNationalities.ClientID%>")
        var checkboxCountries = chkCountries.getElementsByTagName("input");

        for (var i = 0; i < checkboxCountries.length; i++) {
            if (checkboxCountries[i].checked) {
                
                
                selcountriesCount++;
            }
        }
        if (getElement('txtServiceCode').value == '') addMessage('Service Code cannot be blank!', '');
        if (getElement('txtServiceName').value == '') addMessage('Service Name cannot be blank!', '');

        if (getElement('txtServiceAmount').value == '') addMessage('Service Amount cannot be blank!', '');

        if (getElement('ddlProduct').selectedIndex <= 0) addMessage('Please select Product from the list!', '');

        if (getElement('ddlAgent').selectedIndex <= 0) addMessage('Please select Agent  from the list!', '');
//        if (getElement('txtServiceType').value == '') addMessage('Service Type cannot be blank!', '');

        if (selcountriesCount <= 0) {

            addMessage('Please select Nationality from the list!', '');
        }
       
        if (getMessage() != '') {

            alert(getMessage()); clearMessage();
            return false;
        }
    }

    function selectAllNationalitiesList() {
            var chkFields = document.getElementById("<%=chkNationalities.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");
            if (document.getElementById("<%=chkAll.ClientID%>").checked == true) {

                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = true;
                }
            }
            else {
                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = false;
                }
            }      
    }
    function disableCheckBox()
    {
         var chkFields = document.getElementById("<%=chkNationalities.ClientID%>");
        var checkboxFields = chkFields.getElementsByTagName("input");
        for (var i = 0; i < checkboxFields.length; i++) {
            if(checkboxFields[i].checked == false)
            {
                document.getElementById("<%=chkAll.ClientID%>").checked = false;
                break;
            }
        }

    }
    
    
</script>


    <div style="text-align: center">
        <h4>
            Service Master</h4>
    </div>
    <asp:HiddenField runat="server" ID="hdfEMId" Value="0"></asp:HiddenField>
    <asp:HiddenField runat="server" ID="hdfMode" Value="0"></asp:HiddenField>
    <div class="body_container">
        <div class=" paramcon" title="header">
            <div class="col-md-12 padding-0">
                <div class="col-md-2 padding-0 marbot_10">
                    Service Code<span class="fcol_red">*</span>
                </div>
                <div class="col-md-2 padding-0 marbot_10">
                    <asp:TextBox  CssClass="form-control" ID="txtServiceCode" runat="server" MaxLength="50" onkeypress="return IsAlphaNumeric(event);"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-12 padding-0">
                <div class="col-md-2 padding-0 marbot_10">
                    Service Name<span class="fcol_red">*</span>
                </div>
                <div class="col-md-2 padding-0 marbot_10">
                    <asp:TextBox  CssClass="form-control" ID="txtServiceName" runat="server" MaxLength="50" onkeypress="return IsAlphaNumeric(event);"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-12 padding-0">
                <div class="col-md-2 padding-0 marbot_10">
                    Service Amount<span class="fcol_red">*</span>
                </div>
                <div class="col-md-2 padding-0 marbot_10">
                    <asp:TextBox onkeypress="return isNumber(event);"  CssClass="form-control" ID="txtServiceAmount" runat="server" MaxLength="50"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-12 padding-0">
                <div class="col-md-2 padding-0 marbot_10">
                    Service Product<span class="fcol_red">*</span>
                </div>
                <div class="col-md-2 padding-0 marbot_10">
                    <asp:DropDownList ID="ddlProduct" CssClass="form-control" runat="server">
                        <asp:ListItem Text="--Select Product--" Value="0"></asp:ListItem>
                        <asp:ListItem Text="FLIGHT" Value="1"></asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-12 padding-0">
                <div class="col-md-2 padding-0 marbot_10">
                    Service Agent<span class="fcol_red">*</span>
                </div>
                <div class="col-md-2 padding-0 marbot_10">
                    <asp:DropDownList ID="ddlAgent" CssClass="form-control" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div class="col-md-12 padding-0" style="display:none;">
                <div class="col-md-2 padding-0 marbot_10">
                    Service Type<span class="fcol_red">*</span>
                </div>
                <div class="col-md-2 padding-0 marbot_10">
                    <asp:TextBox onkeypress="return IsAlphaNumeric(event);" CssClass="form-control" ID="txtServiceType" runat="server" MaxLength="50"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-12 padding-0">
                <div class="col-md-2 padding-0 marbot_10">
                    Service Nationality<span class="fcol_red">*</span>
                    
                </div>

                <div class="col-md-6 padding-0 marbot_10" style="height: 200px; overflow-y: scroll;">
                   <asp:CheckBox ID="chkAll" runat="server" onchange="selectAllNationalitiesList();" Checked="true"  />
                   Select All
                    <asp:CheckBoxList ID="chkNationalities" runat="server" RepeatDirection="Vertical"
                        RepeatColumns="2"  onchange="disableCheckBox();">                      
                    </asp:CheckBoxList>
                </div>
            </div>
            
            <div class="col-md-12 padding-0" id="divPromoStatus" runat="server" visible="false">
                <div class="col-md-2 padding-0 marbot_10">
                    <asp:Label ID="Label1" runat="server" Text="Status:" ></asp:Label>
                </div>
                <div class="col-md-2 padding-0 marbot_10">
                    <asp:CheckBox ID="chbkActive" Text="Active" runat="server" />
                </div>
                 </div>
            
            <div class="col-md-12 padding-0">
              <div class="col-md-6 padding-0 marbot_10">
            <asp:Button ID="btnSave" CssClass="button" Text="Save" runat="server" OnClientClick="return Save();" OnClick="btnSave_Click"
                Style="display: inline;" />&nbsp;&nbsp;
                                    <asp:Button ID="btnClear" CssClass="button" Text="Clear" runat="server" OnClick="btnClear_Click"
                                        Style="display: inline;" />&nbsp;&nbsp;
                                    <asp:Button ID="btnSearch" CssClass="button" Text="Search" runat="server" OnClick="btnSearch_Click"
                                        Style="display: inline;" />
                </div>
                </div>
            
            
            
        </div>
    </div>
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
    <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="service_id"
        EmptyDataText="No Records Found!" AutoGenerateColumns="false" PageSize="10" GridLines="none"
        CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4"
        CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging" >
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                ControlStyle-CssClass="label" ShowSelectButton="True"  />
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtPromoCode" Width="100px" CssClass="inputEnabled" HeaderText="Service Code"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblPromoCode" runat="server" Text='<%# Eval("service_code ") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("service_code ") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtPromoName" Width="100px" CssClass="inputEnabled" HeaderText="Service Name"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblPromoName" runat="server" Text='<%# Eval("service_name") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("service_name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
