﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="hotelStatusUpdate.aspx.cs" Inherits="CozmoB2BWebApp.hotelStatusUpdate" %>
 <%@ MasterType VirtualPath="~/TransactionBE.master"   %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    
     <link href="css/style.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript">  

       $(document).ready(function () {
            $('#<%=txtRemarks.ClientID%>').prop('maxlength', 150);
        });

        function MaxLength(val1, MaxLen)
            {
               $('#counter').text(MaxLen - val1.value.length+' characters are left');
        }

        function cancel()
            {
               $('#divStatusParam').hide();
            $("#<%= btnSubmit.ClientID %>").show();
            document.getElementById('<%= txtConfNo.ClientID %>').disabled = false;
            document.getElementById('<%= txtConfNo.ClientID %>').value = '';
            document.getElementById('<%= txtRemarks.ClientID %>').value = '';
            $('#counter').text('');
            $('#ddlStatus').select2();
            }
       
            function statusCheck() {
                var Confno = $("#<%= txtConfNo.ClientID %>").val();
                if (Confno == '') {
                    $('#<%=lblStatusMsg.ClientID%>').text("");
                    alert('Please enter valid confirmation No.');
                    return false;
                }

                var Obj = {};
                Obj.Confno = Confno;
                var jsons = JSON.stringify(Obj);
                $.ajax({
                    type: "POST",
                    url: "hotelStatusUpdate.aspx/GetStatus",
                    dataType: "json",
                    contentType: "application/json",
                    data: jsons,
                    success: function (response) {
                        var status = response.d;
                        if (status == 'Pending') {
                            var lblID = "<%=lblStatusMsg.ClientID %>";
                     $('#' + lblID).hide();
                     $('#<%=lblStatusMsg.ClientID%>').text("");
                            $('#divStatusParam').show();                           
                            $("#<%= btnSubmit.ClientID %>").hide();
                            document.getElementById('<%= txtConfNo.ClientID %>').disabled = true;
                        }
                        else if (status == '') {
                            var lblID = "<%=lblStatusMsg.ClientID %>";
                            $('#' + lblID).show();
                            $('#<%=lblStatusMsg.ClientID%>').text("Confirmation No. didn't exist.");
                        }
                        else {
                            var lblID = "<%=lblStatusMsg.ClientID %>";
                            $('#' + lblID).show();
                            $('#<%=lblStatusMsg.ClientID%>').text("Confirmation No. is "+status);
                        }
                    }
                     
                });

            }      

        function statusUpdate() {         
          
             var Confno = $("#<%= txtConfNo.ClientID %>").val();
             var ddlsts = document.getElementById("<%=ddlStatus.ClientID %>");
             var selectedText = ddlsts.options[ddlsts.selectedIndex].innerHTML;
            var selectedValue = ddlsts.value;
            if (selectedValue == 'Select') {
                    alert('Please Select Status.');
                    return false;
            }
            var remarks = $("#<%= txtRemarks.ClientID %>").val();
            if (remarks.length <= 0 || remarks == "Enter Remarks here") {
                alert('Please Enter remarks.');
                    return false;
            }
            var Obj = {};
            Obj.Confno = Confno;
            Obj.status = selectedValue;
            Obj.remarks = remarks;
            var jsonssts = JSON.stringify(Obj);
             $.ajax({
                  type: "POST",
                    url: "hotelStatusUpdate.aspx/UpdateStatus",
                   dataType: "json",
                    contentType: "application/json",
                data: jsonssts,
             success: function (response) {  
                 var rowseffect = response.d;
                 if (rowseffect > 0) {
                     var lblID = "<%=lblStatusMsg.ClientID %>";
                     $('#' + lblID).show();
                     $('#<%=lblStatusMsg.ClientID%>').text("Status updated.");
                     $('#divStatusParam').hide();
                     $("#<%= btnSubmit.ClientID %>").show();
                     document.getElementById('<%= txtConfNo.ClientID %>').disabled = false;
                     document.getElementById('<%= txtConfNo.ClientID %>').value = '';
                    document.getElementById('<%= txtRemarks.ClientID %>').value = '';
            $('#counter').text('');    
                 }
                 else {
                     var lblID = "<%=lblStatusMsg.ClientID %>";
                     $('#' + lblID).show();
                     $('#<%=lblStatusMsg.ClientID%>').text("Error occured");
                 }
             }
          
         }); 
        
          
        }
     </script>
  <div>
         <div title="Param" id="divParam">
              <div class="paramcon"> 
       
              <div class="margin-top-5"> 

<div class="col-md-2"> Confirmation No:  </div> 
<div class="col-md-2"> 
    <asp:TextBox ID="txtConfNo" runat="server" CssClass="inputEnabled form-control"></asp:TextBox> 

   </div> 
<div class="col-md-2"> <asp:Button CssClass="btn but_b pull-right" ID="btnSubmit" runat="server" Text="Find" OnClientClick="return statusCheck();" UseSubmitBehavior="false" /> </div> 
<div class="col-md-2">  <asp:Label ID="lblStatusMsg" runat="server" style="display:none;"></asp:Label>  </div>
<div class="clearfix"> </div>
 </div>
                  </div>
             </div>

        <div title="StatusParam" id="divStatusParam" style="display: none;">       
              <div class="paramcon"> 
       
              <div class="margin-top-5"> 


<div class="col-md-2"> Status:  </div> 
<div class="col-md-2"> 
   <asp:DropDownList CssClass="form-control" ID="ddlStatus" runat="server">
                                                            <asp:ListItem Selected ="True" Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="Cancelled" Value="Cancelled"></asp:ListItem>
                                                            </asp:DropDownList>

   </div> 
                  <div class="clearfix"> </div>
                  </div>
                  <div class="margin-top-5"> 
                  <div class="col-md-2"> Remarks:  </div> 
                  <div class="col-md-2">
                      <asp:TextBox CssClass="form-control" ID="txtRemarks" TextMode="MultiLine" Rows="2" Text="Enter Remarks here"
                                                                runat="server" onfocus="this.style.color='#000000'; if( this.value=='Enter Remarks here' ) { this.value=''; }" onkeyup="return MaxLength(this, 150);"></asp:TextBox>
                     
                      <div id="counter" style="color:red;font-weight:bold"></div>
                  </div>
<div class="col-md-2"><div class="margin-top-5">  <asp:Button CssClass="btn but_b pull-right" ID="btnStatusUpt" runat="server" Text="Status Update"
                                            OnClientClick="return statusUpdate();" UseSubmitBehavior="false"/>
    
                      </div> 
    </div>
<div class="col-md-1"><div class="margin-top-5"> 
    
                    <asp:Button CssClass="btn but_b pull-right" ID="btncancel" runat="server" Text="Cancel"
                                            OnClientClick="return cancel();" UseSubmitBehavior="false"/>
    </div>
    </div>
<div class="clearfix"> </div>

 </div>
                  </div>
             </div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
