﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="AddVisaFee" Title="Cozmo Travels" ValidateRequest="false" Codebehind="AddVisaFee.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<link rel="stylesheet" type="text/css" href="css/style.css" />
   
   
   
   
     <div> 
    <div class="col-md-6"> <h4> 
   <asp:Label runat="server" ID="lblStatus" Text="Add Visa Fee"></asp:Label>
            
    
    </h4> </div>
    
    <div class="col-md-6">
    
    <asp:HyperLink ID="HyperLink1" class="fcol_blue pull-right" runat="server" NavigateUrl="~/VisaFeeList.aspx">Go to Visa Fee List</asp:HyperLink>
    
   

 </div>     
    
    <div class="clearfix"> </div> 
    </div>
   
   
   <div>
                <asp:Label runat="server" ID="errMesage" ForeColor="Red"></asp:Label>
            </div>
           
           <div class="paramcon"> 
            
           <div> 
    <div class="col-md-2"><label>
                        Agent<span style="color:red">*</span>
                    </label> </div>
    
    <div class="col-md-2"><p>
                    
                    <span>
                        <asp:DropDownList ID="ddlAgent" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please select an Agent "
                        ControlToValidate="ddlAgent" InitialValue="-1"></asp:RequiredFieldValidator>
                </p> </div>   
 
    <div class="col-md-2"> <label>
                        Visa Nationality<%--<sup>*</sup>--%>
                    </label></div>
    
    <div class="col-md-2"><p>
                    
                    <span>
                        <asp:DropDownList ID="ddlNationality" CssClass="form-control" runat="server">
                        </asp:DropDownList>
                    </span>
                    <%--<asp:RequiredFieldValidator ID="RequiredFieldValidator11" runat="server" ErrorMessage="Please select a nationality"
                        ControlToValidate="ddlNationality" InitialValue="Select Nationality"></asp:RequiredFieldValidator>--%>
                </p> </div>    

    <div class="col-md-2"><label>
                        Visa Country<span style="color:red">*</span>
                    </label> </div>
        <div class="col-md-2"><p>
                    
                    <span>
                        <asp:DropDownList ID="ddlCountry" CssClass="form-control" runat="server"
                            AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                        </asp:DropDownList>
                    </span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" ErrorMessage="Please select a country "
                        ControlToValidate="ddlCountry" InitialValue="Select Country"></asp:RequiredFieldValidator>
                </p> </div>
 <div class="clearfix"> </div>   
    </div>
    
    
    
            <div> 
    <div class="col-md-2"><label>
                        Visa Type<span style="color:red">*</span>
                    </label> </div>
    
    <div class="col-md-2"> <p>
                    
                    <span>
                        <asp:DropDownList ID="ddlVisaType" runat="server" CssClass="form-control">
                            <asp:ListItem Value="-1" Selected="True">Select</asp:ListItem>
                        </asp:DropDownList>
                    </span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="ddlVisaType"
                        runat="server" ErrorMessage="Please select visa type " InitialValue="Select"></asp:RequiredFieldValidator>
                </p></div>   
 
    <div class="col-md-2"><label>
                        City<%--<sup>*</sup>--%>
                    </label> </div>
    
    <div class="col-md-2"><p>
                    
                    <span>
                        <asp:DropDownList ID="ddlCity" runat="server" CssClass="form-control">
                            <asp:ListItem Value="-1" Selected="True">Select</asp:ListItem>
                        </asp:DropDownList>
                        &nbsp;</span><%--<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="ddlCity"
                            runat="server" ErrorMessage="Please select city " InitialValue="Select"></asp:RequiredFieldValidator>--%>
                </p> </div>    

    <div class="col-md-2"> <label>
                        Basic cost<span style="color:red">*</span></label></div>
     <div class="col-md-2"> <p>
                    
                    <span>
                        <asp:TextBox ID="txtCost" runat="server" CssClass="form-control"></asp:TextBox></span>
                        
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please fill  basic cost "
                        ControlToValidate="txtCost" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtCost"
                        Display="Dynamic" ErrorMessage="Value should be numeric" ValidationExpression="^\d{1,9}(\.\d{1,2})?$"></asp:RegularExpressionValidator></p></div>
                        
                        
 <div class="clearfix"> </div>   
    </div>
    
    
    
    
    
           <div class="marbot_10"> 
    <div class="col-md-2"> <label>
                        Markup<span style="color:red">*</span></label></div>
                        
                        
    
    <div class="col-md-2"> 
                    
                    
                        <asp:TextBox ID="txtMarkup" runat="server" CssClass="form-control"></asp:TextBox>
                        
                        
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please fill markup "
                        ControlToValidate="txtMarkup" Display="Dynamic"></asp:RequiredFieldValidator>
                        
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server" ControlToValidate="txtMarkup"
                        Display="Dynamic" ErrorMessage="Value should be numeric" ValidationExpression="^\d{1,9}(\.\d{1,2})?$"></asp:RegularExpressionValidator>
                        
                        </div>   
 
    <div class="col-md-2"> <label>
                        Deposit Charge<span style="color:red">*</span></label> </div>
    
    <div class="col-md-2">
                        <asp:TextBox ID="txtDepositCharge" runat="server" CssClass="form-control"></asp:TextBox>
                        
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server" ErrorMessage="Please fill deposit charge "
                        ControlToValidate="txtDepositCharge" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" ControlToValidate="txtDepositCharge"
                        Display="Dynamic" ErrorMessage=" Value should be numeric " ValidationExpression="^\d{1,9}(\.\d{1,2})?$"></asp:RegularExpressionValidator>
                        
                        </div>    

    <div class="col-md-2">  <label>
                        Insurance Charge<span style="color:red">*</span></label></div>
     <div class="col-md-2">
                   
                   
                        <asp:TextBox ID="txtIsuranceCharge" runat="server" CssClass="form-control"></asp:TextBox>
                        
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" ErrorMessage="Please fill insurance charge "
                        ControlToValidate="txtIsuranceCharge" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" ControlToValidate="txtIsuranceCharge"
                        Display="Dynamic" ErrorMessage="Value should be numeric" ValidationExpression="^\d{1,9}(\.\d{1,2})?$"></asp:RegularExpressionValidator></div>
                        
                        
         
         <div class="clearfix"> </div>               
    

    </div>
    
    
   
   
            <div class="marbot_10"> 
    <div class="col-md-2"><label>
                        Refundable Security<span style="color:red">*</span></label> </div>
    
    <div class="col-md-2"> <p>
                    
                    <span class="">
                        <asp:TextBox ID="txtRefundable" runat="server" CssClass="form-control"></asp:TextBox></span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server" ErrorMessage="Please fill Refundable Security "
                        ControlToValidate="txtRefundable" Display="Dynamic"></asp:RequiredFieldValidator>
                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" ControlToValidate="txtRefundable"
                        Display="Dynamic" ErrorMessage="Value should be numeric" ValidationExpression="^\d{1,9}(\.\d{1,2})?$"></asp:RegularExpressionValidator></p></div>   
 
    <div class="col-md-2"> <label>
                        Message<span style="color:red">*</span></label></div>
    
    <div class="col-md-4"> <p>
                    
                    <span class="">
                        <asp:TextBox ID="txtMessage" runat="server" CssClass="form-control"></asp:TextBox></span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" runat="server" ErrorMessage="Please fill Message "
                        ControlToValidate="txtMessage" Display="Dynamic"></asp:RequiredFieldValidator>
                </p></div>    

   
    
    
    <div class="col-md-2 xspadtop10">  <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="but but_b pull-right"  
                                OnClick="btnSave_Click" /></div>
    
 <div class="clearfix"> </div>         
    </div>
    
    
    
    
   </div> 
   
    
</asp:Content>
