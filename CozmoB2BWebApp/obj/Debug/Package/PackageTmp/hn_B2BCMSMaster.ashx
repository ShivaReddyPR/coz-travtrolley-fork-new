﻿<%@ WebHandler Language="C#" Class="hn_B2BCMSMaster" %>
using System;
using System.Web;
using System.Web.SessionState; 
using System.Collections.Generic;
using CT.Core; 
public class hn_B2BCMSMaster : IHttpHandler, IRequiresSessionState
{
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            List<HttpPostedFile> files = new List<HttpPostedFile>();
            if (context.Session["B2BCMSSliderFiles"] != null)
            {
                files = context.Session["B2BCMSSliderFiles"] as List<HttpPostedFile>;
            }
            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                files.Add(file);
            }
            context.Session["B2BCMSSliderFiles"] = files;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), "0");
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}
