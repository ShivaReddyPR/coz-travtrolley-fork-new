<%@ Page Language="C#" AutoEventWireup="true" Inherits="FareRulePage" Codebehind="FareRule.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>


    <%if (!sessionError)
      { %>
    <%if (error)
      { %>
    <div class="padding-10 refine-result-bg-color">
    Could not retrieve fare rules.
    </div>
    <%
        }
      else
      {%>
    <div class="padding-10 refine-result-bg-color">
        <% SearchResult res = results[resultId - 1];
            for (int i = 0; i < fareRule.Count; i++)
            {

                if (i > 0)
                {   %>
                <div class="border-bottom-black margin-bottom-10"></div>
            <%  }
                if (res.ResultBookingSource != BookingSource.SpiceJet || res.ResultBookingSource != BookingSource.SpiceJetCorp)
                {%>
                <div class="bold">
                    <span><% = fareRule[i].Airline%> :</span><br />
                    <span><% = Util.GetCityName(fareRule[i].Origin)%></span>
                    <span>(<% = fareRule[i].Origin%>) - </span>
                    <span><% = Util.GetCityName(fareRule[i].Destination)%></span>
                    <span>(<% = fareRule[i].Destination%>}</span>
                <%if (res.ResultBookingSource == BookingSource.UAPI || res.ResultBookingSource == BookingSource.TBOAir)
                  { %>
                
                <%
        
        if (res.BaggageIncludedInFare != null && res.BaggageIncludedInFare.Length > 0)
        {
            if (res.BaggageIncludedInFare.Split(',').Length == 1)//One Way
            {
                if (res.BaggageIncludedInFare.Split(',')[0].Contains("piece") || res.BaggageIncludedInFare.Split(',')[0].ToLower().Contains("unit") || res.BaggageIncludedInFare.Split(',')[0].ToLower().Contains("units"))
                {%>
                       Check-In Baggage :  <%=res.BaggageIncludedInFare.Split(',')[0]%>
                        <%}
                else
                { %>
                        Check-In Baggage : <%=(res.BaggageIncludedInFare.Split(',')[0].ToLower().Contains("kg") ? res.BaggageIncludedInFare.Split(',')[0] : res.BaggageIncludedInFare.Split(',')[0] + "Kg")%> Kg
                    <%}
            }
            else if (res.BaggageIncludedInFare.Split(',').Length > i)//Return
            {
                if (res.BaggageIncludedInFare.Split(',')[i].Contains("piece") || res.BaggageIncludedInFare.Split(',')[i].ToLower().Contains("unit") || res.BaggageIncludedInFare.Split(',')[i].ToLower().Contains("units"))
                {%>
                        Check-In Baggage : <%=res.BaggageIncludedInFare.Split(',')[i]%>
                        <%}
                else if (res.BaggageIncludedInFare.Split(',')[i].Length > 0)
                { %>
                       Check-In Baggage :  <%=(res.BaggageIncludedInFare.Split(',')[i].ToLower().Contains("kg") ? res.BaggageIncludedInFare.Split(',')[i] : res.BaggageIncludedInFare.Split(',')[i] + "Kg")%>
                    <%}
            }
        }
                  }%>
                </div>
                <%} if (res.ResultBookingSource == BookingSource.WorldSpan)
                { %>
                <pre>                
                <br /><% = Regex.Replace(Regex.Replace(fareRule[i].FareRuleDetail, "\n", "<br>"), "-FARE BASIS", "-FARE BASIS   ")%><br /><br>                 </pre>
                <% }
                      
                       else if (res.ResultBookingSource == BookingSource.Indigo || res.ResultBookingSource == BookingSource.IndigoCorp)
                { %>
                    <br /><% = fareRule[i].FareRuleDetail.ToString().Replace("\\par","<br/>")%><br /><br>
                   <% }
                      
                else if (res.ResultBookingSource != BookingSource.SpiceJet && res.ResultBookingSource != BookingSource.AirIndiaExpressIntl && res.ResultBookingSource != BookingSource.SpiceJetCorp)
                { %>
                    <br /><% = Regex.Replace(Regex.Replace(fareRule[i].FareRuleDetail, "\n", "<br>"), "-FARE BASIS", "-FARE BASIS   ")%><br /><br>
                   <% }
                      
                     
                      
                      
                else
                { %>
                   <%=HttpUtility.HtmlDecode(fareRule[i].FareRuleDetail.Trim())%>
        <%  }
            }  %>
            
    </div>
    <%}
      }
      else
      { %>
        <div class="padding-10 refine-result-bg-color fleft full-width">
            <span style="width:100%; float:left; text-align:center; padding-top:30px; color:#222">Your session has expired. Please login again!</span>
         </div>
    <%} %>
   #PK#
<%CT.BookingEngine.SearchResult result = results[resultId - 1];
    if(result.ResultBookingSource == BookingSource.PKFares) { %>
<div class="ns-h3">
    <span style="position: absolute; right: 10px; top: 4px; z-index: 9"><a class="cursor_point" title="close" onclick="hidestuff('BaggageDiv<%=resultId %>');">
        <img src="images/close-icon4.png" alt="close"></a></span>
    Baggage Information
</div>

<table id="ctl00_cphTransaction_dlFlightResults_ctl01_BaggageTable" class="mydatagrid" cellspacing="0" cellpadding="0" border="1" width="100%">
    <tbody>
        <tr>
            <th style="width: 50%"><strong>Sector / Flight</strong></th>
            <th style="width: 50%"><strong>Baggage</strong></th>
        </tr>
        <% 
            for (int i = 0; i < result.Flights.Length; i++)
            {
                for (int j = 0; j < result.Flights[i].Length; j++)
                { CT.BookingEngine.FlightInfo finfo = result.Flights[i][j];
                    string baggage = string.Empty;
                    if (!string.IsNullOrEmpty(result.BaggageIncludedInFare))
                    {
                        if (!(result.BaggageIncludedInFare.Split(',')[i].Contains("piece") || result.BaggageIncludedInFare.Split(',')[i].Contains("pc")) && !result.BaggageIncludedInFare.Split(',')[i].Contains("Airline") && !result.BaggageIncludedInFare.Split(',')[i].ToLower().Contains("kg"))
                        {
                            baggage = result.BaggageIncludedInFare.Split(',')[i] + " Kg";
                        }
                        else
                        {
                            baggage = result.BaggageIncludedInFare.Split(',')[i];
                        }
                    }
                    else {
                        baggage = "As Per Airline Policy";
                    }%>
        <tr>
            <td><%=finfo.Origin.CityCode + " - " + finfo.Destination.CityCode + " " + finfo.Airline + " " + finfo.FlightNumber %></td>
            <td><%=baggage %></td>
        </tr>
        <%}
    } %>
    </tbody>
</table>
<%} %>


