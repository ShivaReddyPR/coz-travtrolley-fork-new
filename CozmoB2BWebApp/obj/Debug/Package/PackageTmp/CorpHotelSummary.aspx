﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Title="Corporate Hotel Summary" CodeBehind="CorpHotelSummary.aspx.cs" Inherits="CozmoB2BWebApp.CorpHotelSummary" %>

<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="CT.TicketReceipt.Common" %>
<%@ Import Namespace="CT.Corporate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<asp:HiddenField runat="server" ID="hdnHotelItinerary" Value="" />
    <div class="body_container">
        <div class="tab-content responsive">
            <span class="preheader" style="color: #f3f3f3; display: none !important; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; mso-hide: all !important; opacity: 0; overflow: hidden; visibility: hidden"></span>
            <table class="body" style="Margin: 0; background: #fff !important; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
                <tr style="padding: 0; text-align: left; vertical-align: top">
                    <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                        <center data-parsed="" style="min-width: 700px; width: 100%">
                        <%if(Request.IsSecureConnection){%> <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" align="center" class="float-center" /><%}%>
                            <%else {%> <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" align="center" class="float-center" /><%}%>
                            <h2>Trip Summary</h2>
                            <table align="center" class="container bc-bookinginfo-wrapper float-center" style="Margin: 0 auto; background: #f7f7f7; border: solid #d0cfcf 1px; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 700px">
                                <tbody>
                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">

                                            <table class="main-header" width="100%" cellspacing="0" cellpadding="4" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <strong style="font-weight: 600">Traveler:</strong>
                                                        </td>
                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%=clsHI.CreatedOn.ToString("dd MMM yyyy") %> Trip<%=" "+approvalText+" - " %>   <%=clsHI.Roomtype[0].PassenegerInfo[0].Firstname + " " + clsHI.Roomtype[0].PassenegerInfo[0].Lastname+" - "%> <%=clsHI.CityRef %>

                                                        </td>
                                                       <%if (sTripstatus == "A")
                                                           { %>
                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%--<a href="printHotelVoucher.aspx?ConfNo=<%=clsHI.ConfirmationNo %>" target="_blank">View Voucher</a>--%>
                                                             <a href="#" onclick="viewVoucher('<%=clsHI.ConfirmationNo %>',<%=HotelItinerary.GetHotelId(clsHI.ConfirmationNo) %>)">View Details </a>

                                                        </td>
                                                        <%} %>
                                                    </tr>
                                                </tbody>
                                            </table>

                                            <table width="100%" cellspacing="0" cellpadding="4" border="0" class="table-bg-color" style="background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table>
                                                            <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                                <tbody>


                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600">Trip Reference:</strong>
                                                                        </td>
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            HTL-CT-<%=new BookingDetail(BookingDetail.GetBookingIdByProductId(HotelItinerary.GetHotelId(clsHI.ConfirmationNo), ProductType.Hotel)).BookingId %>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600">Trip Request for <%=clsHI.Roomtype[0].PassenegerInfo[0].Firstname + " " + clsHI.Roomtype[0].PassenegerInfo[0].Lastname%></strong>
                                                                        </td>
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <%=approvalText %> <strong style="color: red; font-weight: 600">&#x231B;</strong>
                                                                        </td>
                                                                    </tr>
                                                                    <%if (clsHI.HotelPolicy != null && clsHI.HotelPolicy.ProfileId > 0) {
                                                                            string sEmpId = new CorporateProfile(clsHI.HotelPolicy.ProfileId, clsHI.AgencyId).EmployeeId;
                                                                            if (!string.IsNullOrEmpty(sEmpId)) { %>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600">Employee ID</strong>
                                                                        </td>
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <%= sEmpId%>
                                                                        </td>
                                                                    </tr>
                                                                    <%} }%>
                                                                    <%if(sTripstatus == "R"){ %>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600">Rejection Reason:</strong>
                                                                        </td>
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <%=CorpProfileApproval.GetRejectionReason(clsHI.HotelId) %>
                                                                        </td>
                                                                    </tr>
                                                                    <%} %>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600">Approval Deadline</strong>
                                                                        </td>
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">

                                                                            <%=DateTime.Now.AddHours(Convert.ToDouble(doDeadLine)).ToString("dd-MMM-yyyy hh:mm:ss tt") %>
                                                                        </td>
                                                                    </tr>
                                                                    <%for (int i = 100; i < clsCPTDtls.ProfileApproversList.Select(p => new { Hierarchy = p.Hierarchy }).Distinct().ToList().Count; i++)
                                                                        { %>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600">Approver <%=i+1 %></strong>
                                                                        </td>
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <%=clsCPTDtls.ProfileApproversList[i].ApproverEmail%>
                                                                        </td>
                                                                    </tr>
                                                                    <%}
                                                                        string fallBackApprover = string.Empty;
                                                                        List<CorpProfileApproval> fallBackApprovers = clsCPTDtls.ProfileApproversList.FindAll(a => a.Hierarchy==clsCPTDtls.ProfileApproversList[0].Hierarchy);
                                                                        if (fallBackApprovers.Count > 100)
                                                                        {
                                                                            fallBackApprover = fallBackApprovers[1].ApproverEmail;
                                                                            %>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600">Fall Back Approver</strong>
                                                                        </td>
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <%=fallBackApprover %>
                                                                        </td>
                                                                    </tr>
                                                                    <%} %>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <hr size="1" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                            <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                                <tbody>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <span class="image-label" style="background-color: #656565; color: #fff; font-size: 11px; font-weight: bold; padding: 1px 6px; text-transform: uppercase">Trip Information</span>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td height="5" colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word"></td>
                                                                    </tr>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600"> Reason for Travel: </strong>
                                                                            <%= clsHI.HotelPolicy != null && clsHI.HotelPolicy.TravelReasonId > 0 ? new CorporateTravelReason(clsHI.HotelPolicy.TravelReasonId).Description : string.Empty %>
                                                                        </td>
                                                                    </tr>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600">
                                                                                Company:
                                                                            </strong><%=clsAM.Name %>

                                                                        </td>
                                                                    </tr>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600">
                                                                                Policy Compliance:
                                                                            </strong><%=clsHI.HotelPolicy != null ? clsHI.HotelPolicy.IsUnderPolicy ? "Inside Policy" : "Outside Policy - " + clsHI.HotelPolicy.PolicyBreakingRules :"" %>

                                                                        </td>
                                                                    </tr>
                                                                    <%if (clsHI.HotelPolicy != null && clsHI.HotelPolicy.PolicyReasonId > 0 && clsHI.HotelPolicy.IsUnderPolicy==false)
                                                                    {
                                                                        string sDesc = new CorporateTravelReason(clsHI.HotelPolicy.PolicyReasonId).Description;
                                                                        if (!string.IsNullOrEmpty(sDesc))
                                                                        { %>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600"> Violation Reason: </strong> <%= sDesc%>
                                                                        </td>
                                                                    </tr>
                                                                    <%} }%>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table>
                                            <table class="flight-table" style="border-bottom: 1px solid #e4e4e4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                   
                                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word; width:300px">
                                                       
                                                        <%if (clsHI.Rating > 0) { %>
                                                            <%string rating = clsHI.Rating.ToString(); rating = rating.Replace("Star", "").ToLower();%>    
                                                          
                                                                <div> 
                                                                <div id="divrating" class="star-rating-list float-left <%=rating %>-star-hotel"></div>                                                             
                                                                <div style=" clear:both; margin:0px; padding:0px;"> </div>
                                                                </div>
                                                       
                                                        <%} %>
                                                        
                                                        <div>  <strong style="float:left"><%=clsHI.HotelName %></strong> 
                                                            <div style=" clear:both; margin:0px; padding:0px;"> </div>                                                       
                                                        </div>
                                                        
                                                        <div> <%=clsHI.HotelAddress1 %> </div>

                                                    </td>
                                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                        Check-in<br /><strong><%=clsHI.StartDate.ToString("dd MMM yyyy") %></strong>
                                                    </td>
                                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                        Check-out<br /><strong><%=clsHI.EndDate.ToString("dd MMM yyyy") %></strong>
                                                    </td>
                                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; vertical-align: top; word-wrap: break-word; text-align:center">
                                                        Nights<br /><%=(clsHI.EndDate - clsHI.StartDate).TotalDays %>
                                                    </td>
                                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; vertical-align: top; word-wrap: break-word; text-align:center">
                                                        Adults<br /> <%=clsHI.Roomtype.Sum(x => x.AdultCount + x.ChildCount) %>
                                                    </td>
                                                </tr>                                                        
                                            </table>
                                            <table class="spacer" style="border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
                                                <tr> 
                                                    <td style="width:600px;">
                                                        <table class="flight-table" style="border-bottom: 1px solid #e4e4e4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
                                              
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left"><strong>Room</strong></th>
                                                                <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; width:220px"><strong>Room Type</strong></th>
                                                                <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left"><strong>Room Price</strong></th>
                                                                <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left"><strong>Tax & Fees</strong></th>                    
                                                            </tr>
                                                            <% 
                                                            int decimalPoint = clsHI.Roomtype[0].Price.DecimalPoint;
                                                            for (int i=0; i<clsHI.Roomtype.Length; i++)
                                                            {
                                                                %>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    Room <%=i+1 %>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=clsHI.Roomtype[i].RoomName %>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=(clsHI.Roomtype[i].Price.NetFare).ToString("N"+ decimalPoint) %>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=(clsHI.Roomtype[i].Price.Tax + clsHI.Roomtype[i].Price.Markup - clsHI.Roomtype[i].Price.Discount).ToString("N"+ decimalPoint) %>
                                                                </td>                                                                           
                                                            </tr>
                                                            <%} %>
                                                        </table>
                                                    </td>
                                                    <td style="vertical-align:top; border-left: 1px solid #a7a7a7"> 
                                                        <table class="flight-table" style=" border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
                                                            <tr> <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left"><strong>Total Fare</strong></th> </tr>
                                                            <tr> 
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align:middle; word-wrap: break-word">                        
                                                                    <%=(clsHI.Roomtype.Sum(x => x.Price.NetFare + x.Price.Tax + x.Price.Markup - x.Price.Discount)).ToString("N"+ clsHI.Roomtype[0].Price.DecimalPoint) %>
                                                                </td>    
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <%if(clsHI.Roomtype[0].PassenegerInfo[0].FlexDetailsList != null){ %>
                                            <table class="" width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <td colspan="1" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <table class="main-header" width="100%" border="0" cellspacing="0" cellpadding="4" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                                <tbody>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600">Reporting Fields:</strong>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="bottom-table-style" width="100%" border="0" cellspacing="0" cellpadding="2" style="background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                <tbody>
                                                    <%foreach (FlightFlexDetails flex in clsHI.Roomtype[0].PassenegerInfo[0].FlexDetailsList){ %>
                                                    <tr class="table-cnt-inner" style="padding: 0; text-align: left; vertical-align: top">
                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%=flex.FlexLabel %>:
                                                        </td>
                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%=flex.FlexData %>
                                                        </td>
                                                    </tr>
                                                    <%} %>
                                                </tbody>
                                            </table>
                                            <%} %>
                                            <table class="" width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <td colspan="1" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <table class="main-header" width="100%" border="0" cellspacing="0" cellpadding="4" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                                <tbody>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600">PNR:</strong>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class="bottom-table-style" width="100%" border="0" cellspacing="0" cellpadding="2" style="background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                <tbody>                                                    
                                                    <tr class="table-cnt-inner" style="padding: 0; text-align: left; vertical-align: top">
                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            PNR:
                                                        </td>
                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%=clsHI.ConfirmationNo %>
                                                        </td>
                                                    </tr>                                                    
                                                </tbody>
                                            </table>
                                             <table class="" width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                <tbody>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                        <td colspan="1" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <table class="main-header" width="100%" border="0" cellspacing="0" cellpadding="4" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                                <tbody>
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                            <strong style="font-weight: 600">Booking Status:</strong>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                                                                        <table class="bottom-table-style" width="100%" border="0" cellspacing="0" cellpadding="2" style="background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                <tbody>                                                    
                                                    <tr class="table-cnt-inner" style="padding: 0; text-align: left; vertical-align: top">
                                                        <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                           <%if (clsHI.Status == HotelBookingStatus.Pending)
                                                               { %> 
                                                            <b style="color: #FF5722"><%=clsHI.Status%> </b>
                                                            <%} %>
                                                            <%else
    { %> <b style="color: #009933">Vouchered </b>
                                                            <%} %>
                                                        </td>
                                                    </tr>                                                    
                                                </tbody>
                                            </table> 
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </center>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="clearfix"></div>
    <script type="text/javascript">
        if (document.getElementById('ctl00_lnkgoogleapi') != null) {
            $('#ctl00_lnkgoogleapi').attr("href", '<%=Request.Url.Scheme%>://ctb2bstage.cozmotravel.com');
        }
        
    </script>
    <script type="text/javascript">
        function viewVoucher(confNo,hotelId) {
            
            AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'PrintHotelVoucher', 'sessionData':'" + (confNo + '|' + hotelId) + "', 'action':'set'}");
            window.open("printHotelVoucher.aspx", "Voucher", "_blank", "width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes");
            return false;
        }
     
    </script>
</asp:Content>

