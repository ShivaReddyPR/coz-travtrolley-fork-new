﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="bizzlogin.aspx.cs" Inherits="CozmoB2BWebApp.bizzlogin" %>
	<!DOCTYPE html>
	<html>

	<head runat="server">
		<title>Corporate Travel</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.ico">	
		<link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="css/RegisterStyle.css" rel="stylesheet" type="text/css" />
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<!-- manual css -->
		<link href="css/override.css" rel="stylesheet" />
		<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
		<link href="build/css/main.min.css" rel="stylesheet" type="text/css" />
        <style>
            .footerReg { margin-top:0px; }
            .theme-border-color, .pagination.tt-paging b a, .btn-primary:hover, .form-control:focus, .select2-drop-active, .form-control.select2-container.select2-dropdown-open, .btn-primary, .but_d, .qq-upload-button {
			    border-color: #ccc !important;
			}
        </style>

	</head>

	<body>

    <%--<script type="text/javascript" src="Scripts/Menu/query2.js"></script>--%>

    <%--jQuery 2.2.2 + Migrate 1.4.1 - Dec2020 Upgrade--%>

     <script src="build/js/jquery-2.2.4.min.js" type="text/javascript"></script>        
     <script type="text/javascript" src="build/js/jquery-migrate-1.4.1.min.js"></script>
       <script>
           jQuery.ajaxPrefilter(function(s){
            if(s.crossDomain){
                s.contents.script = false;
             }
           })
     </script>

<%-- Blocking IE --%>
 <div id="unsupported-message">
	    <a id="unsupported-overlay" href="#unsupported-modal"></a>
	    <div id="unsupported-modal" role="alertdialog" aria-labelledby="unsupported-title">
		    <h2 id="unsupported-title">⚠ Unsupported Browser ⚠</h2>
		    <p>Travtrolley probably won't work great in Internet Explorer. We generally only support the recent versions of major browsers like <a target="_blank" href="https://www.google.com/chrome/">Chrome</a>, <a target="_blank" href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a>, <a target="_blank" href="https://support.apple.com/downloads/safari">Safari.</a>!</p>
				
		    <p>If you think you're seeing this message in error, <a href="mailto:tech@cozmotravel.com">email us at tech@cozmotravel.com</a> and let us know your browser and version.</p>
	    </div>
	    <a id="unsupported-banner">
    <strong> ⚠ Unsupported Browser ⚠ </strong>
		    Travtrolley may not work properly in this browser.
	    </a>
      </div>

    <script>
    // Get IE or Edge browser version
    function detectIE() {
        var ua = window.navigator.userAgent,
            msie = ua.indexOf('MSIE '),
            trident = ua.indexOf('Trident/'),
            edge = ua.indexOf('Edge/');              
         if (msie > 0 || trident > 0 ) {
            $('#unsupported-message').css('display', 'block');
            $('body').css('overflow', 'hidden');
         }
    }
    detectIE();         

</script>
	<style>
	    #unsupported-message {
             display: none;
	    }
		#unsupported-overlay,
		    #unsupported-modal {
		      display: block;
		      position: absolute;
		      position: fixed;
		      left: 0;
		      right: 0;
		      z-index: 9999;
		      background: #000;
		      color: #D5D7DE;
		    }
		
		    #unsupported-overlay {
		      top: 0;
		      bottom: 0;
		    }
		
		    #unsupported-modal {
		      top: 80px;
		      margin: auto;
		      width: 90%;
		      max-width: 520px;
		      max-height: 90%;
		      padding: 40px 20px;
		      box-shadow: 0 10px 30px #000;
		      text-align: center;
		      overflow: hidden;
		      overflow-y: auto;
		      border:solid 2px #ccc8b9;
		    }
		
		    #unsupported-message :last-child { margin-bottom: 0; }
		
		    #unsupported-message h2 {
		      font-size: 34px;
		      color: #FFF;
              margin-bottom: 20px;
		    }
		
		    #unsupported-message h3 {
		      font-size: 20px;
		      color: #FFF;
		    }
		
		    body.hide-unsupport { padding-top: 24px; }
		    body.hide-unsupport #unsupported-message { visibility: hidden; }
		
		    #unsupported-banner {
		      display: block;
		      position: absolute;
		      top: 0;
		      left: 0;
		      right: 0;
		      background:#ccc8b9;
		      color: #000;
		      font-size: 14px;
		      padding: 2px 5px;
		      line-height: 1.5;
		      text-align: center;
		      visibility: visible;
		      z-index: 100000;
		    }
		
		
		
		    @media (max-width: 800px), (max-height: 500px) {
		      #unsupported-message .modal p {
		        /* font-size: 12px; */
		        line-height: 1.2;
		        margin-bottom: 1em;
		      }
		
		      #unsupported-modal {
		        position: absolute;
		        top: 20px;
		      }
		      #unsupported-message h1 { font-size: 22px; }
		      body.hide-unsupport { padding-top: 0px; }
		      #unsupported-banner { position: static; }
		      #unsupported-banner strong,
		      #unsupported-banner u { display: block; }
		    }
	</style>
 <%-- Blocking IE --%>  


		<div class="topHeader2">
			<div class="container">
				<div class="row">
					<div class="col-xs-6">
						<img src="images/logoc.jpg" class="img-responsive">
					</div>
					<%--<div class="col-xs-6">
						<img src="images/bizzlogo.jpg" class="img-responsive float-right" style="display:none">
					</div>--%>
				</div>
			</div>
		</div>
		<form runat="server" id="form1">
			<asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
			<asp:UpdatePanel ID="upnlLogin" runat="server" UpdateMode="conditional">
				<ContentTemplate>
					<div class="bgRegister2">
						<div class="container">
							<div class="row">
								<div class="col-md-7">
									<div class="title">
										<h1> Business Travel, Personalized, Simplified & Delivered  </h1>
									</div>
								</div>
								<div class="col-md-5">
									<div class="createYourAccount2">
										<div class="row">
											<div class="col-md-12">
												<div class="col-12 mb-5">
													<div class="row">
														<div class="col-md-6 col-6 text-right">
<span class="logocctm"> <img src="images/cctmlogoc.jpg" /> </span>
														</div>
														<div class="col-md-6 col-6 text-left">
															<div class="linkChanger"></div>
														</div>
													</div>
												</div>
												<div id="loginTrav">
													<div class="col-12">
														<div class="form-control-holder">
															<div class="icon-holder">
<span class="icon-user"></span>
															</div>
															<asp:TextBox ID="txtLoginName" class="form-control" autocomplete="off" placeholder="User Name" runat="server"></asp:TextBox>
														</div>
													</div>
													<div class="col-12">
														<div class="form-control-holder">
															<div class="icon-holder">
<span class="icon-password"></span>
															</div>
															<asp:TextBox ID="txtPassword" class="form-control" autocomplete="off" placeholder="Password" runat="server" TextMode="password"></asp:TextBox>
														</div>
													</div>
													<div class="col-12"> <span> 
        <asp:Label ID="lblForgotPwd" runat="server" Text="" style="color:Red; font-size:14px;"></asp:Label>
        <asp:Label style="color:Red; font-size:14px;" runat="server" ID="lblError" CssClass="lblError"></asp:Label></span>
														<div class="form-control-holder">
															<asp:Button ID="btnLogin" class="btn btn-lg btn-block" runat="server" Text="SUBMIT" OnClick="btnLogin_Click" OnClientClick=" Validate()" />
														</div>
													</div>
												</div>
												<div id="normalTrav" class="col-12 mt-4 normalTrav" >
													<a style="display:none" id="forgotpassword" href="#" onclick="return ShowPopUp(this.id);"> Forgot password?</a>
												</div></div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
					<div class="modal" id="forgotPwd">
						<div class="modal-content forgot_div"> <em class="close close_popup" style=" position:absolute;  right:10px; top:10px; cursor:pointer">
					  
						<i><img align="right" alt="Close" onclick="HidePopUp()" src="images/close-itimes.gif"></i>
					</em>
							<div>
								<center>
									<h4>Forgot password? </h4>
								</center>
							</div>
							<div>
								<asp:TextBox ID="txtEmailId" CssClass="form-control margin_top10" placeholder="Enter Your Email Address" runat="server" CausesValidation="True"></asp:TextBox>
							</div>
							<div class="martop_14">
								<asp:Button ID="btnGetPassword" CssClass="chosencol btn" runat="server" Text="Get Password" OnClientClick="return Validate()" OnClick="btnGetPassword_Click" /> <b style="display: none; color:Red" id="err"></b>
							</div>
						</div>
					</div>
				</ContentTemplate>
			</asp:UpdatePanel>
		</form>
		<div class="RegisterWrapper">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="headingRegister2">
							<h2> Our Services </h2>
							<div class="saprater"></div>
						</div>
					</div>
				</div>
				<div class="row mt-5">
					<div class="col-md-4">
						<a href="CozmoBiz-AirTickets.aspx" class="productWrap">
							<div class="productIcons"> <span> <label> <img src="build/img/img2/airplane.svg" />  </label>  </span> 
							</div>
							<h3> Air Tickets</h3>
							<p>To ensure the quality and effectiveness of every business.</p>
						</a>
					</div>
					<div class="col-md-4">
						<a href="CozmoBiz-VisaServices.aspx" class="productWrap">
							<div class="productIcons"> <span> <label> <img src="build/img/img2/passport.svg" />  </label>  </span> 
							</div>
							<h3> Visa Services</h3>
							<p>To ensure the quality and effectiveness of every business.</p>
						</a>
					</div>
					<div class="col-md-4">
						<a href="CozmoBiz-Hotels.aspx" class="productWrap">
							<div class="productIcons"> <span> <label> <img src="build/img/img2/hotel.svg" />  </label>  </span> 
							</div>
							<h3>Hotels</h3>
							<p>To ensure the quality and effectiveness of every business.</p>
						</a>
					</div>
					<div class="col-md-4">
						<a href="CozmoBiz-Forex.aspx" class="productWrap">
							<div class="productIcons"> <span> <label> <img src="build/img/img2/currency.svg" />  </label>  </span> 
							</div>
							<h3>Forex</h3>
							<p>To ensure the quality and effectiveness of every business.</p>
						</a>
					</div>
					<div class="col-md-4">
						<a href="CozmoBiz-TravelInsurance.aspx" class="productWrap">
							<div class="productIcons"> <span> <label> <img src="build/img/img2/baggage.svg" />  </label>  </span> 
							</div>
							<h3> Travel Insurance</h3>
							<p>To ensure the quality and effectiveness of every business.</p>
						</a>
					</div>
					<div class="col-md-4">
						<a href="CozmoBiz-CozmoMICE.aspx" class="productWrap">
							<div class="productIcons"> <span> <label> <img src="build/img/img2/team.svg" />  </label>  </span> 
							</div>
							<h3> Mice</h3>
							<p>To ensure the quality and effectiveness of every business.</p>
						</a>
					</div>
				</div>
			</div>
		</div>
<%--		<div style=" display:none" class="TestimonialWrapper">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<div class="headingRegister2">
							<h2>Customer Speaks </h2>
							<div class="saprater"></div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-4">
						<div class="clientBox">
							<div>
<span class="s1"> <img class="clientImage" src="images/cc2.jpg" />  </span>
								<span class="s2">  <img class="clientComma" src="build/img/inverted-commas.svg" />  </span>
								<div class="clearfix"></div>
							</div>
							<div class="clientSpeaks">
								<p>It is not every day that you come across a passionate and trustworthy financial advisor. John Doe is true professional who does his work really well. Thanks John!</p>
							</div>
							<div class="clientName">Johnson.M.J.</div>
							<div class="clientCompany"> <small> Manager at ABC Travel</small> 
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="clientBox">
							<div>
<span class="s1"> <img class="clientImage" src="images/cc1.jpg" />  </span>
								<span class="s2">  <img class="clientComma" src="build/img/inverted-commas.svg" />  </span>
								<div class="clearfix"></div>
							</div>
							<div class="clientSpeaks">
								<p>In just 2 very short meetings with John he managed to find the solution personally catered to my situation and expectations. Punctual, well informed and very reliable.</p>
							</div>
							<div class="clientName">Carol Harper</div>
							<div class="clientCompany"> <small> Director at ABC Travel</small> 
							</div>
						</div>
					</div>
					<div class="col-md-4">
						<div class="clientBox">
							<div>
<span class="s1"> <img class="clientImage" src="images/cc3.jpg" />  </span>
								<span class="s2">  <img class="clientComma" src="build/img/inverted-commas.svg" />  </span>
								<div class="clearfix"></div>
							</div>
							<div class="clientSpeaks">
								<p>A very professional financial advisor, who is true to his word. John has demonstrated a high amount of integrity in the time I have known him, and he is fast to respond to my concerns.</p>
							</div>
							<div class="clientName">Snow.J.R.</div>
							<div class="clientCompany"> <small> Managing Director of ABC Travel</small> 
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>--%>
		<div>
            <pre class="footer_sml_bizz mb-0" style="font-family: 'Quicksand',sans-serif;font-weight: bold;border:0"><label >Copyright 2019 Cozmo Travel LLC, UAE . All Rights Reserved.</label> </pre></div>
		<div style="display: none">
			<table cellpadding="0" cellspacing="0" class="label">
				<tr>
					<td style="width: 150px"></td>
					<td style="width: 150px"></td>
					<td></td>
				</tr>
				<tr>
					<td align="right"></td>
					<td></td>
				</tr>
				<tr>
					<td align="right"></td>
					<td></td>
				</tr>
				<tr>
					<td align="right">
						<asp:Label Visible="false" ID="lblCompanyName" runat="server" Text="Company:"></asp:Label>
					</td>
					<td>
						<asp:DropDownList runat="server" Visible="false" ID="ddlCompany" CssClass="inputDdlEnabled" Width="154px"></asp:DropDownList>
					</td>
				</tr>
				<tr style="height: 15px">
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td align="right"></td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError"></asp:Label>
					</td>
				</tr>
			</table>
		</div>

<%--		<script src="Scripts/bootstrap.min.js"></script>--%>
		<script type="text/javascript" src="build/js/main.min.js"></script>
	<%--	<script src="scripts/select2.min.js" type="text/javascript"></script>--%>
		<script type="text/javascript">		
            
		        function ShowPopUp(id) {
		            document.getElementById('<%=txtEmailId.ClientID %>').value = "";
		            document.getElementById('err').style.display = 'none';
		            document.getElementById('forgotPwd').style.display = "block";
		            var positions = getRelativePositions(document.getElementById(id));
		            //            document.getElementById('forgotPwd').style.left = (positions[0] + 130) + 'px';
		            //            document.getElementById('forgotPwd').style.top = (positions[1] - 200) + 'px';
		            return false;
		        }
		        function HidePopUp() {
		            document.getElementById('forgotPwd').style.display = "none";
		        }
		        function getRelativePositions(obj) {
		            var curLeft = 0;
		            var curTop = 0;
		            if (obj.offsetParent) {
		                do {
		                    curLeft += obj.offsetLeft;
		                    curTop += obj.offsetTop;
		                } while (obj = obj.offsetParent);
		
		            }
		            return [curLeft, curTop];
		        }
		        function Validate() {
		
		            var isValid = true;
		            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
		            document.getElementById('err').style.display = 'none';
		            if (document.getElementById('<%=txtEmailId.ClientID %>').value.length <= 0) {
		                document.getElementById('err').style.display = 'block';
		                document.getElementById('err').innerHTML = "Enter Email address";
		                isValid = false;
		            }
		            else if (reg.test(document.getElementById('<%=txtEmailId.ClientID %>').value)) {
		                document.getElementById('err').style.display = 'none';
		            }
		            else {
		                document.getElementById('err').style.display = 'block';
		                document.getElementById('err').innerHTML = "Enter Correct Email";
		                isValid = false;
		            }
		            if (isValid == true) {
		                return true;
		            }
		            else {
		                return false;
		            }
		        }

		</script>


        
           
	</body>

	</html>
