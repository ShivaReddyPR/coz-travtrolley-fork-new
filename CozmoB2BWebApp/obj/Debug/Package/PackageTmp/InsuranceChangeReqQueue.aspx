﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="InsuranceChangeReqQueueGUI" Title="Insurance Change Request" CodeBehind="InsuranceChangeReqQueue.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <link href="css-holiday/main-style.css" rel="stylesheet" type="text/css" />


    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <script type="text/javascript" src="Scripts\jsBE\organictabs.jquery.js"></script>
    <script type="text/javascript" src="ash.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />


    <div style="padding-top: 10px; position: relative">

        <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="container1" style="position: absolute; top: 60px; right: 0; z-index: 9999; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="container2" style="position: absolute; top: 100px; left: 172px; z-index: 9999; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="container3" style="position: absolute; top: 50px; left: 250px; z-index: 9999; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="container4" style="position: absolute; top: 53px; left: 51%; z-index: 9999; display: none;">
            </div>
        </div>
        <div class="bg_white bor_gray padding-5 paramcon">




            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="lblPurchaseDateFrom" Text="Purchase From Dt:" runat="server"></asp:Label>
                </div>


                <div class="col-md-2">




                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtPurchaseDateFrom" runat="server" CssClass="form-control" Width="110px"></asp:TextBox>
                            </td>
                            <td>
                                <a href="javascript:void(null)" onclick="showCalendar3()">
                                    <img id="Img2" src="images/call-cozmo.png" alt="Pick Date" />
                                </a></td>

                        </tr>

                    </table>

                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblPurDtTo" Text="Purchase To Dt:" runat="server"></asp:Label>
                </div>

                <div class="col-md-2">


                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtPurchaseDateTo" runat="server" CssClass="form-control" Width="110px"></asp:TextBox>
                            </td>
                            <td>
                                <a href="javascript:void(null)" onclick="showCalendar4()">
                                    <img id="Img3" src="images/call-cozmo.png" alt="Pick Date" />
                                </a></td>

                        </tr>

                    </table>






                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblDeptDate" Text="Departure Date:" runat="server"></asp:Label>
                </div>

                <div class="col-md-2">





                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDeptDate" runat="server" Width="110px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td><a href="javascript:void(null)" onclick="showCalendar1()">
                                <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                            </a></td>

                        </tr>

                    </table>




                </div>





                <div class="clearfix"></div>
            </div>




            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="lblArrDate" Text="Arrival Date:" runat="server"></asp:Label>
                </div>

                <div class="col-md-2">






                    <table> 
<tr> 
<td> <asp:TextBox ID="txtArrDate" runat="server" CssClass="form-control" Width="110px"></asp:TextBox></td>
<td><a href="javascript:void(null)" onclick="showCalendar2()">
                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" margin: 5px" />
                    </a> </td>

</tr>

</table>
                </div>




                <div class="col-md-2">Agency:</div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlAgency" runat="server" CssClass="inputDdlEnabled form-control">
                    </asp:DropDownList>
                </div>
               
              
               

                <div class="col-md-2">
                    <asp:Label ID="lblPNRno" Text="PNR No:" runat="server" Visible="false"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:TextBox CssClass="form-control" ID="txtPNRno" runat="server" Visible="false"></asp:TextBox>
                </div>

                <div class="clearfix"></div>
            </div>



            <div class="col-md-12 padding-0 marbot_10">

                <div class="col-md-2">
                    <asp:Label ID="lblOrigin" Text="Origin:" runat="server"></asp:Label>
                </div>

                <div class="col-md-2">
                    <asp:TextBox ID="Origin" CssClass="form-control" runat="server"></asp:TextBox>
                    <div class="clear">
                        <div style="width: 95%; line-height: 30px; color: #000;" id="statescontainer">
                        </div>
                    </div>
                    <div id="multipleCity1">
                    </div>

                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblDestination" Text="Destination:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:TextBox ID="Destination" CssClass="form-control" runat="server"></asp:TextBox>
                    <div id="statesshadow2">
                        <div style="width: 95%; line-height: 30px; color: #000;" id="statescontainer2">
                        </div>
                    </div>
                    <div id="multipleCity2"></div>


                </div>

                <div class="col-md-2">
                    <asp:Label ID="lblPolicyNo" Text="Certificate No:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:TextBox ID="txtPolicyNo" CssClass="form-control" runat="server"></asp:TextBox>
                </div>                  
                <div class="clearfix"></div>
                
            </div>   
         <%-- Added BY Hari Malla 25-01-2019--%>
            <div class="col-md-12 padding-0 marbot_10">
                  <div class="col-md-2"> <asp:Label ID="Label1" Text="Insurance Type" runat="server"></asp:Label></div> 
                 <div class="col-md-2">
                    <asp:DropDownList ID="ddlInsType" runat="server"  CssClass="inputDdlEnabled form-control"  >
                         <asp:ListItem Value="ALL">ALL</asp:ListItem>
                        <asp:ListItem Value="TUNES">TUNES</asp:ListItem>
                        <asp:ListItem Value="RELIGARE">RELIGARE</asp:ListItem>
                    </asp:DropDownList>
                </div>
                </div>
          <div class="clearfix"></div>
        </div>



            <div class="col-md-12 padding-0 marbot_10">

                <div class="col-md-12">

                    <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click" CssClass="but but_b pull-right margin-left-10" OnClientClick="return Validate();" />


                    <asp:Button runat="server" ID="btnClear" Text="Clear" OnClick="btnClear_Click" CssClass="but but_b pull-right margin-left-10" />


                </div>



                <div class="clearfix"></div>
            </div>







            <div class="clear"></div>
        </div>


        <div style="padding-bottom: 10px">
            <div style="text-align: right;"> <%--<%=pagingEnable %> --%>
                Page:
                            <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click" Text="First"></asp:LinkButton>

                <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" />
                <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>

                <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" />

                <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click" Text="Last"></asp:LinkButton>
            </div>

            <div class="clear"></div>

        </div>

        <div>
            <asp:DataList ID="dlInsQueue" runat="server" CellPadding="4"
                DataKeyField="PlanId" Width="100%"
                OnItemCommand="dlInsQueue_ItemCommand"
                OnItemDataBound="dlInsQueue_ItemDataBound">
                <ItemTemplate>
                    <div class=" bor_gray bg_white padding-5 marbot_10" id="Result">
                        <asp:HiddenField ID="hdnPlanId" runat="server" />
                        <asp:HiddenField ID="hdnRequestId" runat="server" />

                        <div class="col-md-12 padding-0 marbot_10">

                            <div class="col-md-4">
                                <span style="color: #2B6BB5; font-size: small; font-weight: bold;">
                                    <%# Eval("PlanTitle")%></span>
                                <span>(<%# Eval("agent_name")%>)</span>
                            </div>

                            <div class="col-md-4">
                                <asp:Label ID="lblStatus" runat="server"></asp:Label>
                            </div>
                            <div class="col-md-4">
                                <span>Purchase Dt:</span>


                                <span style="font-size: 12px; font-weight: bold;">
                                    <%# Eval("PolicyPurchasedDate")%></span>
                            </div>



                            <div class="clearfix"></div>
                        </div>





                        <div class="col-md-12 padding-0 marbot_10">

                            <div class="col-md-4">
                                <span>Certificate No:</span> <span style="font-size: 12px; font-weight: bold;">
                                    <%# Eval("PolicyNo")%></span>
                            </div>

                            <div class="col-md-4">
                                <span style="font-size: small; font-weight: bold;">
                                    <%# Eval("OriginCity")%>
                                         - </span><span style="font-size: small; font-weight: bold;">
                                             <%# Eval("DestinationCity")%></span>
                            </div>
                            <div class="col-md-4">
                                <span>Departure Dt:</span>
                                <span style="font-size: 12px; font-weight: bold;">
                                    <%# Eval("DepartureDate")%></span>
                            </div>



                            <div class="clearfix"></div>
                        </div>





                        <div class="col-md-12 padding-0 marbot_10">

                            <div class="col-md-4">
                                <label style="display: none;">
                                    <span>
                                        <input id="Open" class="button" onclick='location.href =&#039; ViewBookingForInsurance.aspx ? bookingId =<%# Eval("Ins_Id") %>&#039;'
                                            type="button" value="Open" />
                                    </span>

                                </label>

                                <span style="font-size: small; font-weight: bold;">
                                    <asp:Label ID="lblPrice" runat="server" Text='<%# Eval("NetAmount")%>' ></asp:Label></b>   <%--Text='<%# Eval("TotalAmount")%>'--%>
                                         <%--<%# Eval("TotalAmount")%>--%></span>
                            </div>

                            <div class="col-md-4">
                                <span>PNR:</span> <span style="font-size: 12px; font-weight: bold;">
                                    <%# Eval("PNR")%></span>
                            </div>
                            <div class="col-md-4">
                                <span>Return Dt:</span>
                                <span style="font-size: 12px; font-weight: bold;">
                                    <%# Eval("ReturnDate")%></span>
                            </div>



                            <div class="clearfix"></div>
                        </div>


                        <div class="col-md-12 padding-0 marbot_10">

                            <div class="col-md-12">

                                <table>
                                    <tr>
                                        <td><span style="font-size: 12px;">Booked By Location:</span></td>
                                        <td style="width: 320px"><span style="font-size: 12px; font-weight: bold;">
                                            <%# Eval("location_name")%></span></td>
                                        <td><span style="font-size: 12px;">Booked By User:</span></td>
                                        <td style="width: 320px"><span style="font-size: 12px; font-weight: bold;">
                                            <%# Eval("USER_FULLNAME")%></span></td>
                                        <td><span style="font-size: 12px;">Trip Id:</span></td>
                                        <td><span style="font-size: 12px; font-weight: bold;">
                                            <%# Eval("AgetRefNO")%></span></td>
                                    </tr>
                                </table>

                            </div>



                            <div class="clearfix"></div>
                        </div>
                           <div class="col-md-12 padding-0 marbot_10">
                            <div class="col-md-12">
                                  
                                    <span  >   Insurance Type :<strong><asp:Label ID="lblInsType" runat="server" Text='<%# Eval("InsType") %>'></asp:Label></strong></span>
                            
                                </div>
                                 <div class="clearfix"></div>
                           </div>
                        <div class="col-md-12 padding-0 marbot_10">

                            <div class="col-md-12">
                                <asp:Label ID="lblRemarks" runat="server" BackColor="#CCCCCC" Font-Italic="True"
                                    Font-Names="Arial" Font-Size="10pt" Text='<%#Eval("data") %>' Width="100%"></asp:Label>
                            </div>

                            <div class="clearfix"></div>
                        </div>

                        <div>

                            <table id="tblRefund" style="margin-top: 10px" visible="false" width="100%"
                                border='0' cellspacing='0' cellpadding='2' runat="server">
                                <tr>
                                    <td colspan="2" align="right">
                                        <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                                    </td>
                                    <td align="right" class="style3">
                                        <span style="font-weight: bold;">Admin Fee: &nbsp;&nbsp;</span>
                                        <asp:Label ID="lblAgentCurrency" runat="server"></asp:Label>

                                        <asp:TextBox ID="txtAdminFee" runat="server" Width="80px" onKeyPress="return isNumber(event)" Style="border-radius: 5px;" onfocus="Check(this.id);" onBlur="Set(this.id);" Text="0"></asp:TextBox>
                                 </td>
                                    <td>
                                        <span style="font-weight: bold;">Supplier Fee: &nbsp;&nbsp;</span>
                                         <asp:Label ID="lblsupplierCurrency" runat="server"></asp:Label>
            <asp:TextBox ID="txtsupplier" runat="server" Width="80px" Style="border-radius: 5px;" onKeyPress="return isNumber(event)"  onfocus="Check(this.id);" onBlur="Set(this.id);"  Text="0"></asp:TextBox>            &nbsp;
                                  
                                          
                                        
                                    </td>
                                    <td>
                                        
                                        <asp:Button ID="btnRefund" CssClass="button" runat="server" Font-Bold="True" Text="Refund" OnClientClick="javascript:return validateFee();" CommandName="Refund"
                                            CommandArgument='<%#Eval("Ins_Id") %>' />
                                    </td>
                                    <td align="right">
                                        <asp:Button ID="btnOpen" runat="server" Text="Open" Font-Bold="True" Visible="false" />
                                    </td>
                                </tr>
                            </table>

                        </div>



                        <div class="clearfix"></div>
                    </div>
                </ItemTemplate>
            </asp:DataList>

            <div class="clear"></div>
        </div>
    </div>

    <script type="text/javascript">
        var cal1;
        var cal2;
        var cal3;
        var cal4;
        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //            cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select Departure date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDate1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select Arrival date");
            cal2.selectEvent.subscribe(setDate2);
            cal2.cfg.setProperty("close", true);
            cal2.render();

            cal3 = new YAHOO.widget.Calendar("cal3", "container3");
            cal3.cfg.setProperty("title", "Purchase From Date");
            cal3.selectEvent.subscribe(setDate3);
            cal3.cfg.setProperty("close", true);
            cal3.render();

            cal4 = new YAHOO.widget.Calendar("cal4", "container4");
            cal4.cfg.setProperty("title", "Purchase To Date");
            cal4.selectEvent.subscribe(setDate4);
            cal4.cfg.setProperty("close", true);
            cal4.render();
        }
        function showCalendar1() {
            $('container4').context.styleSheets[0].display = "none";
            $('container3').context.styleSheets[0].display = "none";
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            cal1.show();
            cal2.hide();
            cal3.hide();
            cal4.hide();
            init();
        }
        //        var departureDate = new Date();
        function showCalendar2() {
            $('container1').context.styleSheets[0].display = "none";
            $('container3').context.styleSheets[0].display = "none";
            $('container4').context.styleSheets[0].display = "none";
            cal1.hide();
            cal3.hide();
            cal4.hide();
            cal2.show();
            init();
            //            // setting Calender2 min date acoording to calendar1 selected date
            //            var date1 = document.getElementById('<%= txtDeptDate.ClientID%>').value;
            //            //var date1=new Date(tempDate.getDate()+1);

            //            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            //                var depDateArray = date1.split('/');

            //                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);

            //                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
            //                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
            //                cal2.render();
            //            }
            document.getElementById('container2').style.display = "block";
        }

        function showCalendar3() {
            $('container1').context.styleSheets[0].display = "none";
            $('container2').context.styleSheets[0].display = "none";
            $('container4').context.styleSheets[0].display = "none";
            cal1.hide();
            cal2.hide();
            cal4.hide();
            cal3.show();
            init();
            //            // setting Calender2 min date acoording to calendar1 selected date
            //            var date1 = document.getElementById('<%= txtDeptDate.ClientID%>').value;
            //            //var date1=new Date(tempDate.getDate()+1);

            //            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            //                var depDateArray = date1.split('/');

            //                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);

            //                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
            //                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
            //                cal2.render();
            //            }
            document.getElementById('container3').style.display = "block";
        }
        var departureDate = new Date();
        function showCalendar4() {
            $('container1').context.styleSheets[0].display = "none";
            $('container2').context.styleSheets[0].display = "none";
            $('container3').context.styleSheets[0].display = "none";
            cal1.hide();
            cal2.hide();
            cal3.hide();
            cal4.show();
            init();
            var date3 = document.getElementById('<%= txtPurchaseDateFrom.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date3.length != 0 && date3 != "DD/MM/YYYY") {
                var depDateArray = date3.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal4.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal4.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal4.render();
            }
            document.getElementById('container4').style.display = "block";
        }

        function setDate1() {
            var date1 = cal1.getSelectedDates()[0];

            //            $('IShimFrame').context.styleSheets[0].display = "none";
            //            this.today = new Date();
            //            var thisMonth = this.today.getMonth();
            //            var thisDay = this.today.getDate();
            //            var thisYear = this.today.getFullYear();

            //            var todaydate = new Date(thisYear, thisMonth, thisDay);
            //            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            //            var difference = (depdate.getTime() - todaydate.getTime());

            //            if (difference < 0) {
            //                document.getElementById('errMess').style.display = "block";
            //                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            //                return false;
            //            }
            //            departureDate = cal1.getSelectedDates()[0];
            //            document.getElementById('errMess').style.display = "none";
            //            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtDeptDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDate2() {
            //            var date1 = document.getElementById('<%=txtDeptDate.ClientID %>').value;
            //            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            //                document.getElementById('errMess').style.display = "block";
            //                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
            //                return false;
            //            }

            var date2 = cal2.getSelectedDates()[0];

            //            var depDateArray = date1.split('/');

            //            // checking if date1 is valid		    
            //            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
            //                document.getElementById('errMess').style.display = "block";
            //                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
            //                return false;
            //            }
            //            document.getElementById('errMess').style.display = "none";
            //            document.getElementById('errorMessage').innerHTML = "";

            //            // Note: Date()	for javascript take months from 0 to 11
            //            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            //            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            //            var difference = returndate.getTime() - depdate.getTime();

            //            if (difference < 1) {
            //                document.getElementById('errMess').style.display = "block";
            //                document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
            //                return false;
            //            }
            //            if (difference == 0) {
            //                document.getElementById('errMess').style.display = "block";
            //                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
            //                return false;
            //            }
            //            document.getElementById('errMess').style.display = "none";
            //            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtArrDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }

        function setDate3() {
            //            var date1 = document.getElementById('<%=txtPurchaseDateFrom.ClientID %>').value;
            //            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            //                document.getElementById('errMess').style.display = "block";
            //                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
            //                return false;
            //            }

            var date3 = cal3.getSelectedDates()[0];

            //            var depDateArray = date1.split('/');

            //            // checking if date1 is valid		    
            //            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
            //                document.getElementById('errMess').style.display = "block";
            //                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
            //                return false;
            //            }
            //            document.getElementById('errMess').style.display = "none";
            //            document.getElementById('errorMessage').innerHTML = "";

            //            // Note: Date()	for javascript take months from 0 to 11
            //            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            //            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            //            var difference = returndate.getTime() - depdate.getTime();

            //            if (difference < 1) {
            //                document.getElementById('errMess').style.display = "block";
            //                document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
            //                return false;
            //            }
            //            if (difference == 0) {
            //                document.getElementById('errMess').style.display = "block";
            //                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
            //                return false;
            //            }
            departureDate = cal3.getSelectedDates()[0];
            //            document.getElementById('errMess').style.display = "none";
            //            document.getElementById('errorMessage').innerHTML = "";

            var month = date3.getMonth() + 1;
            var day = date3.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtPurchaseDateFrom.ClientID %>').value = day + "/" + month + "/" + date3.getFullYear();
            cal3.hide();
        }

        function setDate4() {
            var date4 = cal4.getSelectedDates()[0];

            var date3 = document.getElementById('<%=txtPurchaseDateFrom.ClientID %>').value;
            if (date3.length == 0 || date3 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select Purchase from date.";
                return false;
            }

            var depDateArray = date3.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Purchase From Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date4.getFullYear(), date4.getMonth(), date4.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Purchase To date should be greater than Purchase from date (" + date3 + ")";
                return false;
            }
            //                        if (difference == 0) {
            //                            document.getElementById('errMess').style.display = "block";
            //                            document.getElementById('errorMessage').innerHTML = "Purchase from date and to date Could not be same";
            //                            return false;
            //                        }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date4.getMonth() + 1;
            var day = date4.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtPurchaseDateTo.ClientID %>').value = day + "/" + month + "/" + date4.getFullYear();
            cal4.hide();
        }

        YAHOO.util.Event.addListener(window, "load", init);

    </script>
    <script type="text/javascript">
        function Validate() {
            if (document.getElementById('<%=txtPurchaseDateFrom.ClientID %>').value != '' && document.getElementById('<%=txtPurchaseDateTo.ClientID %>').value == '') {
                alert("Please Select Purchase To Date!");
                return false;
            }
            return true;
        }
        function CancelInsPlan(index) {
            var val = document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_txtRemarks').value;
            if (document.getElementById('ctl00_cphTransaction_dlInsQueuee_ctl0' + index + '_ddlChangeRequestType').value == "Select") {
                document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_errRemarks').innerHTML = "Please Select Request type!";
                return false;
            }
            else if (val.length <= 0 || val == "Enter Remarks here") {
                document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_errRemarks').innerHTML = "Enter remarks";
                return false;
            }
            else {
                document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_errRemarks').innerHTML = "";
                return true;
            }
        }
        function Check(id) {
            var val = document.getElementById(id).value;
            if (val == '0') {
                document.getElementById(id).value = '';
            }
        }
        function Set(id) {
            var val = document.getElementById(id).value;
            if (val == '' || val == '0') {
                document.getElementById(id).value = '0';
            }
        }

        function ValidateFee(index) {
            document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_btnRefund').style.display = "none";
            var adminFee = document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_txtAdminFee').value;
            var supplierFee = document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_txtsupplier').value;
            var total = document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_lblPrice').innerHTML;
            total = total.replace(',', '');
            var totalAmount = total.split(" ");
            if (parseInt(adminFee) > parseInt(totalAmount[1])) {
                alert("Admin Fee should not be Greater Then Booking Amount");
                return false;
            }           
            var insType = document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_lblInsType').innerText;
            if (insType == "Religare" && parseInt(adminFee)+parseInt(supplierFee) > parseInt(totalAmount[1])) {
                alert("Admin and supplier Fee should not be Greater Then Booking Amount");
                return false;
            }
            return true;
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode==47)) {
                return false;
            }
            return true;
        }
        function message(msg) {
            alert(msg);
        }
    </script>
   
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>

