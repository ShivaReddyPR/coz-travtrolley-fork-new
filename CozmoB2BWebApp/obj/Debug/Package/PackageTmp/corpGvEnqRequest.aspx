﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="corpGvEnqRequestGUI" MasterPageFile="~/TransactionBE.master" Codebehind="corpGvEnqRequest.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="cphTransaction" runat="server">
     <div class="body_container"> 
         <div class="col_md_12">
         <h4> Enquiry Request</h4>
          <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
        </div>
         <div class="bggray bor_gray paddingtop_10 marbot_20" style="height: 150px";> 
    <div class="col-md-2"> 
<div class="form-group">
<label> Traveling Country:  </label>
    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true"> </asp:DropDownList>
</div>

</div>
    <div class="col-md-2"> 
<div class="form-group">
<label> Visa Type:  </label>
    <asp:DropDownList ID="ddlVisaType" runat="server" CssClass="form-control" > </asp:DropDownList>
</div>

</div>


             
    <div class="col-md-2"> 
<div class="form-group">
<label> Nationality:  </label>
    <asp:DropDownList ID="ddlNationality" runat="server" CssClass="form-control"> </asp:DropDownList>
</div>

</div>
    <div class="col-md-2"> 
<div class="form-group">
<label> Residense Country:  </label>
    <asp:DropDownList ID="ddlResidense" runat="server" CssClass="form-control" > </asp:DropDownList>
</div>

</div>
<div class="col-md-1" id="divAdult"> 
<div class="form-group">
<label>Adult </label>

<div><asp:DropDownList ID="ddlAdults"  CssClass="form-control  custom-select" runat="server">
    <asp:ListItem Text="1" Value="1" Selected="True"></asp:ListItem>
    <asp:ListItem Text="2" Value="2"></asp:ListItem>
    <asp:ListItem Text="3" Value="3"></asp:ListItem>
    <asp:ListItem Text="4" Value="4"></asp:ListItem>
    <asp:ListItem Text="5" Value="5"></asp:ListItem>
    <asp:ListItem Text="6" Value="6"></asp:ListItem>
    <asp:ListItem Text="7" Value="7"></asp:ListItem>
    <asp:ListItem Text="8" Value="8"></asp:ListItem>
    <asp:ListItem Text="9" Value="9"></asp:ListItem>
     </asp:DropDownList></div>

</div>
    </div>

    <div class="col-md-1" id="divChild" >
    <div class="form-group">
<label>Child </label>

        <div>
            <asp:DropDownList ID="ddlChilds" runat="server">
                <asp:ListItem Text="0" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                <asp:ListItem Text="9" Value="9"></asp:ListItem>
            </asp:DropDownList>

        </div>

</div>
        </div>
    <div class="col-md-1" id="divInfant">
    <div class="form-group">
<label>Infant </label>

        <div>
            <asp:DropDownList ID="ddlInfants" runat="server">
                <asp:ListItem Text="0" Value="0" Selected="True"></asp:ListItem>
                <asp:ListItem Text="1" Value="1"></asp:ListItem>
                <asp:ListItem Text="2" Value="2"></asp:ListItem>
                <asp:ListItem Text="3" Value="3"></asp:ListItem>
                <asp:ListItem Text="4" Value="4"></asp:ListItem>
                <asp:ListItem Text="5" Value="5"></asp:ListItem>
                <asp:ListItem Text="6" Value="6"></asp:ListItem>
                <asp:ListItem Text="7" Value="7"></asp:ListItem>
                <asp:ListItem Text="8" Value="8"></asp:ListItem>
                <asp:ListItem Text="9" Value="9"></asp:ListItem>
            </asp:DropDownList>
        </div>

</div>
</div>
              <div class="col-md-1"> 
<div class="form-group">
<label> &nbsp; </label>

<div><asp:Button ID="btnSearch" runat="server" Text="Search" class="btn but_b" OnClientClick="return validate();" OnClick="btnSearch_Click"  />
     </div>
</div>

</div>

         </div>
</div>
         <div class="col-md-12">
             

         </div>
         </div>
    <script type="text/javascript">
        function validate() {
            document.getElementById('errMess').style.display = "none";
            if (getElement('ddlCountry').selectedIndex <= 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Country from the list";
                return false;
            }
            if (getElement('ddlVisaType').selectedIndex <= 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Visa Type from the list";
                return false;
            }
            if (getElement('ddlNationality').selectedIndex <= 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Nationality from the list";
                return false;
            }
            if (getElement('ddlResidense').selectedIndex <= 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Residense from the list";
                return false;
            }
        }
    </script>

</asp:Content>

