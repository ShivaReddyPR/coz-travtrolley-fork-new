﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpCompanyVendor.aspx.cs" Inherits="CozmoB2BWebApp.ExpCompanyVendor" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
   
    <link href="css/toastr.css" rel="stylesheet" type="text/css" />
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="scripts/Common/ExpenseObjects.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>

    <script> 

        /* Global variables */
        var apiHost = ''; var cntrllerPath = 'api/expenseMasters';
        var apiAgentInfo = {}; var expRepId = 0; var selectedRows = [];
        var flag = '';
        var CV_Id = 0;       
        var agentdata = {};

        /* Page Load */
        $(document).ready(function () {
            /* Check query string to see if existing report needs to open */
            expRepId = '<%=Request.QueryString["ExpRepId"] == null ? "0" : Request.QueryString["ExpRepId"]%>';

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {
                alert('Session expired, please login once again.');
                return;
            }

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            /* To get page dropdown list */
            GetScreenLoadData();

        });

        /* To get agent and login info */
        function GetAgentInfo() {
            try {
                var loginInfo = '<%=Settings.LoginInfo == null%>';
                if (loginInfo == true)
                    return apiAgentInfo;
                
                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {
            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';
            if (IsEmpty(apiHost))
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";
            return apiHost;
        }

        /* To get agent dropdown list */
        function GetScreenLoadData() {
            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expRepId };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCompanyVendorScreenLoadData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);
        }

        /* To bind agent list to dropdown */
        function BindScreenData(apiData) {
            agentdata = {};
            if (!IsEmpty(apiData.dtAgents)) {

                agentdata = apiData.dtAgents;
                var options = agentdata.length != 1 ? GetddlOption('', '--Select Agent--') : '';
                $.each(agentdata, function (key, col) {

                    options += GetddlOption(col.agenT_ID, col.agenT_NAME);
                });
                $('#ddlAgentId').empty();
                $('#ddlAgentId').append(options);
                $("#ddlAgentId").select2("val", '');
                if (agentdata.length == 1) {

                    $('#ddlAgentId').attr('disabled', 'disabled');
                    $("#ddlAgentId").select2("val", agentdata[0].agenT_ID);
                }
            }
        }
        
        /* To Load expense comapny vendor info */
        function LoadCompanyVendorInfo() {
            var cv_AgentId = $("#ddlAgentId").val();
            if (cv_AgentId == "" || cv_AgentId == "--Select Agent--") {
                toastr.error('Please select agent');
                $('#ddlAgentId').parent().addClass('form-text-error');
                return false;
            }
            var companyVendorInfo = {CV_AgentId: cv_AgentId }
            var reqData = {AgentInfo: apiAgentInfo, CompanyVendorInfo: companyVendorInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCompanyVendor';
            WebApiReq(apiUrl, 'POST', reqData, '', BindComapnyVendorInfo, null, null);
        }

        /* To Bind comapny vendor info to grid */
        function BindComapnyVendorInfo(cvInfo)
        {
            if (cvInfo.length == 0) {

                ShowError('no data available for selected agent.');
                ClearInfo();
                return;
            }
            cvInfoDetails = cvInfo;
            var gridProperties = GetGridPropsEntity();
            gridProperties.headerColumns = ('Code|Name|Type').split('|');
            gridProperties.displayColumns = ('cV_Code|cV_Name|cV_Type').split('|');
            gridProperties.pKColumnNames = ('cV_ID').split('|');
            gridProperties.dataEntity = cvInfo;
            gridProperties.divGridId = 'CompanyVendorList';
            gridProperties.headerPaging = true;
            gridProperties.gridSelectedRows = selectedRows;
            gridProperties.selectType = 'M';
            gridProperties.recordsperpage = 5;

            EnablePagingGrid(gridProperties);
            $('#VendorsList').show();
        }

        /* To clear company vendor details and hide the div */
        function ClearInfo()
        {
            selectedRows = [];
            flag = ''
            CV_Id = 0;
            $("#CV_Code").val('');
            $("#CV_Name").val('');
            $("#CV_Type").select2('val', '-1');

            $("#ddlAgentId").parent().removeClass('form-text-error');
            $("#CV_Code").removeClass('form-text-error');
            $("#CV_Name").removeClass('form-text-error');
            $("#CV_Type").parent().removeClass('form-text-error');
            RemoveGrid();
            $('#VendorsList').hide();
        }

        /* To prepare and get the company vendors entity list for selected rows */
        function GetSelectedRows()
        {
            selectedRows = GetSetSelectedRows();
            if (IsEmpty(selectedRows) || selectedRows.length == 0)
                return [];
            var expcvSelected = [];
            $.each(selectedRows, function (key, col) {

                var cInfo = cvInfoDetails.find(item => item.cV_ID == col);
                cInfo.cV_Status = false;
                cInfo.cV_ModifiedBy = apiAgentInfo.LoginUserId;                
                expcvSelected = expcvSelected.concat(cInfo);
            });
            return expcvSelected;
        }

        /* To prepare and set company vendor info entity */
        function RefreshGrid(apiData)
        {
            $.each(selectedRows, function (key, col) {

                var cvid = parseInt(col);
                cvInfoDetails = cvInfoDetails.filter(item => (item.cV_ID) !== cvid);
            });
            if (cvInfoDetails.length == 0) {
                BindComapnyVendorInfo(cvInfoDetails);
                return;
            }
            selectedRows = [];
            GetSetSelectedRows('set', selectedRows);
            GetSetDataEntity('set', cvInfoDetails);
            LoadData(GetSetCurrentPageNo('get', ''));
        }

        /* To save company vendor info */
        function Save()
        {
            var cV_AgentId = $("#ddlAgentId").val();
            var cV_Code = $("#CV_Code").val();
            var cV_Name = $("#CV_Name").val();
            var cV_Type = $("#CV_Type").val();

            if ((cV_AgentId == "" || cV_AgentId == "--Select Agent--") || (cV_Code == "" || cV_Code == "Enetr Code") || (cV_Name == "" || cV_Name == "Enter Name") || (cV_Type == "-1" || cV_Type == "--Select Type--"))
            {
                if (cV_AgentId == "" || cV_AgentId == "--Select Agent--") {
                    toastr.error('Please Select Agent');
                    $('#ddlAgentId').parent().addClass('form-text-error');
                }
                if (cV_Code == "" || cV_Code == "Enter Code") {
                    toastr.error('Please Enter Code');
                    $("#CV_Code").addClass('form-text-error');
                }
                if (cV_Name == "" || cV_Name == "Enter Name") {
                    toastr.error('Please Enter Name');
                    $("#CV_Name").addClass('form-text-error');
                }
                if (cV_Type == "-1" || cV_Type == "Select") {
                    toastr.error('Please Select Type');
                    $('#CV_Type').parent().addClass('form-text-error');
                }
                return false;
            }           
            //var companyVendorInfo = { CV_ID: CV_Id, CV_AgentId: cV_AgentId, CV_Code: cV_Code, CV_Name: cV_Name, CV_Type: cV_Type, CV_Status: true, CV_CreatedBy: apiAgentInfo.LoginUserId, CV_ModifiedBy: apiAgentInfo.LoginUserId }
            var companyVendorInfo = GetExpCVObj(CV_Id, cV_AgentId, cV_Code, cV_Name, cV_Type, true, apiAgentInfo.LoginUserId);
            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expRepId, CompanyVendorInfo: companyVendorInfo, flag: "INSERT" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveCompanyVendor';
            WebApiReq(apiUrl, 'POST', reqData, '', StatusConfirm, null, null);          
        }

        /* To update selected cvInfo from the grid and update the status in data base */
        function EditCVInfo()
        {
            var delCVendorInfo = GetSelectedRows();
            if (IsEmpty(delCVendorInfo) || delCVendorInfo.length == 0) {
                ShowError('Please check any one of the row in below table');
                return;
            }
            if (IsEmpty(delCVendorInfo) || delCVendorInfo.length > 1) {
                ShowError('Please check one of the row in below table');
                return;
            }
            var cvDetails = cvInfoDetails.find(item => item.cV_ID == selectedRows[0]);
            CV_Id = cvDetails.cV_ID;
            $("#ddlAgentId").select2('val', cvDetails.cV_AgentId);
            $("#CV_Code").val(cvDetails.cV_Code);
            $("#CV_Name").val(cvDetails.cV_Name);
            $("#CV_Type").select2('val', cvDetails.cV_Type);
        }

        /* To delete selected rows from the grid and update the status in data base */
        function Delete()
        {
            var delCVendorInfo = GetSelectedRows();
            if (IsEmpty(delCVendorInfo) || delCVendorInfo.length == 0) {

                ShowError('Please select record to delete');
                return;
            }            
            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expRepId, delCVendorInfo, flag: "DELETE" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveCompanyVendor';
            WebApiReq(apiUrl, 'POST', reqData, '', RefreshGrid, null, null);
        }

        /* To bind status of company vendor info */
        function StatusConfirm() {
            if (CV_Id > 0) {
                alert('record updated successfully!');               
            }
            else if (CV_Id == 0) {
                alert('record added successfully!');             
            }
            ClearInfo();
            LoadCompanyVendorInfo();            
        }

        /* textbox allows only alphanumeric */
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (keyCode == 32));
            return ret;
        }

        /* textbox allows only alphanumeric and @-_$,.:; */
        function check(e) {
            var keynum;
            var keychar;

            // For Internet Explorer
            if (window.event) {
                keynum = e.keyCode;
            }
            // For Netscape/Firefox/Opera
            else if (e.which) {
                keynum = e.which;
            }
            keychar = String.fromCharCode(keynum);
            //List of special characters you want to restrict
            if (keychar == "'" || keychar == "`" || keychar == "!" || keychar == "#" || keychar == "%" || keychar == "^" || keychar == "*" || keychar == "(" || keychar == ")" || keychar == "+" || keychar == "=" || keychar == "/" || keychar == "~" || keychar == "<" || keychar == ">" || keychar == "|" || keychar == "?" || keychar == "{" || keychar == "}" || keychar == "[" || keychar == "]" || keychar == "¬" || keychar == "£" || keychar == '"' || keychar == "\\") {
                return false;
            } else {
                return true;
            }
        }

    </script>

    <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">
                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left">Company Vendor</h3>
                    </div>
                </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <button class="btn btn-info mr-2" id="showVendorsList" type="button" data-toggle="collapse" data-target="#VendorsList" aria-expanded="false" aria-controls="CardsList" onclick="LoadCompanyVendorInfo()">VIEW ALL <i class="icon icon-search "></i></button>
                    <button type="button" class="btn btn-gray mr-2" onclick="ClearInfo();">CLEAR <i class="icon icon-close "></i></button>
                    <button type="button" class="btn btn-primary" onclick="Save();">SUBMIT <i class="icon icon-play_arrow "></i></button>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">                        
                        <div class="form-group col-md-3">
                            <label>Agent</label><span style="color: red;">*</span>
                            <select name="Agent" id="ddlAgentId" class="form-control"></select>                            
                        </div>
                        <div class="form-group col-md-3">
                            <label>Code</label><span style="color: red;">*</span>
                            <input type="text" id="CV_Code" class="form-control" maxlength="10" placeholder="Enter Code" onkeypress="return IsAlphaNumeric(event);" />                          
                        </div>
                        <div class="form-group col-md-3">
                            <label>Name</label><span style="color: red;">*</span>
                            <input type="text" class="form-control" id="CV_Name" maxlength="100" placeholder="Enter Name" onkeypress="return check(event)" />                            
                        </div>
                        <div class="form-group col-md-3">
                            <label>Type</label><span style="color: red;">*</span>
                            <select class="form-control" id="CV_Type">
                                <option value="-1">--Select Type--</option>
                                <option value="Company">Company</option>
                                <option value="Vendor">Vendor</option>
                            </select>                            
                        </div>
                    </div>
                </div>

                <div class="exp-content-block collapse" id="VendorsList" style="display: none;">
                    <h5 class="mb-3 float-left">All Company Vendors</h5>
                    <div class="button-controls text-right">
                        <button class="btn btn-edit" type="button" onclick="EditCVInfo();">EDIT  <i class="icon icon-edit"></i></button>
                        <button class="btn btn-danger" type="button" onclick="Delete();">DELETE  <i class="icon icon-delete"></i></button>
                    </div>
                    <div class="clear"></div>
                    <div class="table-responsive" id="CompanyVendorList"></div>
                </div>

            </div>
        </div>
        <div class="clear "></div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
