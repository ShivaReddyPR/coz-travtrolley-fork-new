﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpAttendeeMaster.aspx.cs" Inherits="CozmoB2BWebApp.ExpAttendeeMaster" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    
    <link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" /> 
    <link href="css/toastr.css" rel="stylesheet" type="text/css" />

    <script src="scripts/jquery-ui.js"></script>
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>

    <script type="text/javascript"> 

        /* Global variables */
        var apiHost = ''; var bearer = ''; var cntrllerPath = 'api/expenseMasters';
        var selectedAttendeeInfo = [];
        var apiAgentInfo = {}; var expAttendeeDetails = {}; var expCrddDetails = {}
        var EAM_ID = 0; var flag = ''; var agenttype = '';var selectedProfile = 0;

        /* Page Load */
        $(document).ready(function () {

            $("#ctl00_upProgress").show();

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {

                alert('Session expired, please login once again.');
                return;
            }

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            GetScreenData();

            $("#ctl00_upProgress").hide();
        });

        /* To get attendee agent and company */
        function GetScreenData() {

            var reqData = { AgentInfo: apiAgentInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getAttendeeMasterScreenLoadData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);                        
        }

         /* To bind attendee master agent and company */
        function BindScreenData(screenData) {

            DdlBind('ddlAgentId', screenData.dtAgents, 'agenT_ID', 'agenT_NAME', '', '', '--Select Agent--', '');

            if (!IsEmpty(screenData.dtAgents) && screenData.dtAgents.length == 1) {

                $('#ddlAgentId').attr('disabled', 'disabled');
                    $("#ddlAgentId").select2("val", screenData.dtAgents[0].agenT_ID);
            }
                        
            BindCompanyVendorData(screenData.dtCompany);
        }

        /* To bind cost center data to costcenter control */
        function BindCompanyVendorData(cvData) {

            DdlBind('ddlCompany', cvData, 'cV_ID', 'cV_Name', '', '', '--Select Company--', '');
        }

        /* To set employee name and cost center on employee change */
        function AgentChange(agentId) {

            selectedProfile = agentId;
            if (selectedProfile > 0) {

               var reqData = { AgentInfo: apiAgentInfo, AgentId : parseInt(selectedProfile) };
               var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCompanyVendorData';
               WebApiReq(apiUrl, 'POST', reqData, '', BindCompanyVendorData, null, null); 
            }
            ClearAttendeeInfo();
        }

        /* To get attendee info */
        function LoadAttendeeInfo() {

             if ($("#ddlAgentId ").val() == "" || $("#ddlAgentId ").val() == "Select") {

                 toastr.error('Please Select Agent');
                 $('#ddlAgentId').parent().addClass('form-text-error');
                 return false;
             }
            var reqData = { AgentInfo: apiAgentInfo, AgentId : parseInt($("#ddlAgentId").val()) };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getAttendeeInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', BindAttendeeInfo, null, null);                        
        }
               
        /* To Bind attendee info to grid */
        function BindAttendeeInfo(attendeeInfo) {
            
            if (attendeeInfo.length == 0) {

                ShowError('No records to show.');
                ClearAttendeeInfo();
                return;
            }

            expAttendeeDetails = attendeeInfo;

            var gridProperties = GetGridPropsEntity();
            gridProperties.headerColumns = ('First Name|Last Name|Title|Type|Company').split('|');
            gridProperties.displayColumns = ('eaM_Name|eaM_LastName|eaM_Title|eaM_Type|eaM_ComanyName').split('|');
            gridProperties.pKColumnNames = ('eaM_ID').split('|');
            gridProperties.dataEntity = attendeeInfo;
            gridProperties.divGridId = 'divGridAttendeeInfo';
            gridProperties.headerPaging = true;
            gridProperties.gridSelectedRows = selectedAttendeeInfo;
            gridProperties.selectType = 'M';
            gridProperties.recordsperpage = 10;

            EnablePagingGrid(gridProperties);
            $('#divAttendeeList').show();
        }

        /* To clear attendee info details and hide the div */
        function ClearAttendeeInfo() {
             
            //expCardDetails = {};
            selectedProfile = 0;
            selectedAttendeeInfo = [];
            $("#txtTitle").val('');
            $("#txtName").val('');
            $("#txtTitle").val('');
            $("#txtLName").val('');
            $("#ddlCompany").select2('val', '');
            EAM_ID = 0;
            flag = ''
            $("#ddlAgentId").removeClass('form-text-error');
            $("#txtTitle").removeClass('form-text-error');
            $("#txtName").removeClass('form-text-error');
            $("#txtLName").removeClass('form-text-error');
            $("#ddlCompany").removeClass('form-text-error');
            $("#txtType").removeClass('form-text-error');
            RemoveGrid();
            $('#divAttendeeList').hide();
        }

        /* To delete selected attendee info from the grid and update the status in data base */
        function DeleteAttendeeInfo() {

            var DelAttendeeInfo = GetselectedAttendeeInfo();
            if (IsEmpty(DelAttendeeInfo) || DelAttendeeInfo.length == 0) {

                ShowError('Please select attendee to delete');
                return;
            }
            
            var reqData = { AgentInfo: apiAgentInfo, DelAttendeeInfo, Status: 'D' };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/addAttendeeInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', RefreshGrid, null, null);                        
        }

        /* To prepare and get the attendee info entity list for selected attendees */
        function GetselectedAttendeeInfo() {

            selectedAttendeeInfo = GetSetSelectedRows('get', '');

            if (IsEmpty(selectedAttendeeInfo) || selectedAttendeeInfo.length == 0)                 
                return [];

            var expAttendeeInfoSelected = [];

            $.each(selectedAttendeeInfo, function (key, col) {

                var ainfo = expAttendeeDetails.find(item => item.eaM_ID == col);
                ainfo.eaM_Status = false;
                expAttendeeInfoSelected = expAttendeeInfoSelected.concat(ainfo);
            });

            return expAttendeeInfoSelected;
        }


        /* To prepare and set attendee info entity */
        function RefreshGrid(resp) {

            $.each(selectedAttendeeInfo, function (key, col) {
                var eamid = parseInt(col);
                expAttendeeDetails = expAttendeeDetails.filter(item => (item.eaM_ID) !== eamid)
            });

            if (expAttendeeDetails.length == 0) {

                BindAttendeeInfo(expAttendeeDetails);
                return;
            }

            selectedAttendeeInfo = [];
            GetSetSelectedRows('set', selectedAttendeeInfo);
            GetSetDataEntity('set', expAttendeeDetails);
            LoadData(GetSetCurrentPageNo('get', ''));
        }

        /* To bind update card info */
        function SaveSuccess(resp) {
            
            alert('record ' + (EAM_ID > 0 ? 'updated' : 'added') + ' successfully!');
            $("#ctl00_upProgress").hide();
            ClearAttendeeInfo();
        }
        
         /* To edit selected attendee info from the grid and update the data in data base */
        function EditAttendeeInfo() {

            selectedAttendeeInfo = GetSetSelectedRows('get', '');
            if (IsEmpty(selectedAttendeeInfo) || selectedAttendeeInfo.length == 0) {
                
                ShowError('Please select attendee to edit');
                return;
            }

            if (IsEmpty(selectedAttendeeInfo) || selectedAttendeeInfo.length > 1) {
                
                ShowError('Please select one attendee to edit');
                return;
            }

            var expattendeeinfoDetails = expAttendeeDetails.find(item => item.eaM_ID == selectedAttendeeInfo[0]);

            EAM_ID = expattendeeinfoDetails.eaM_ID;
            $("#ddlAgentId").select2('val', expattendeeinfoDetails.eaM_AgentId);
            $("#txtTitle").val(expattendeeinfoDetails.eaM_Title);
            $("#txtName").val(expattendeeinfoDetails.eaM_Name);
            $("#txtLName").val(expattendeeinfoDetails.eaM_LastName);
            $("#txtType").val(expattendeeinfoDetails.eaM_Type);
            $("#ddlCompany").select2('val', expattendeeinfoDetails.eaM_CV_Id);
        }

        /* To validate and save attendee info */
        function SaveAttendeeInfo() {
                       
            var IsValid = CheckReqData('ddlAgentId', 'NM', '20', 'Agent', mt);
            IsValid = CheckReqData('ddlCompany', 'NM', '20', 'Company', mt) && IsValid;
            IsValid = CheckReqData('txtName', 'AL', '100', 'first name', mt) && IsValid;
            IsValid = CheckReqData('txtLName', 'AL', '100', 'last name', mt) && IsValid;

            if (!IsValid)                 
                return IsValid;
            
            var Adddata = {
                AgentInfo: apiAgentInfo,
                AttendeeInfo: {
                    EAM_ID: parseInt(EAM_ID),
                    EAM_AgentId: parseInt($("#ddlAgentId").val()),
                    EAM_Title: $("#txtTitle").val(),
                    EAM_Name: $("#txtName").val(),
                    EAM_Type: $("#txtType").val(),
                    EAM_CV_Id: parseInt($("#ddlCompany").val()),
                    EAM_Status: true,
                    EAM_CreatedBy: apiAgentInfo.LoginUserId,
                    EAM_ModifiedBy: apiAgentInfo.LoginUserId,
                    EAM_LastName: $("#txtLName").val(),
                }
            };

            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/addAttendeeInfo';
            WebApiReq(apiUrl, 'POST', Adddata, '', SaveSuccess, null, null);
        }

        /* To get agent and login info */
        function GetAgentInfo() {

            try {

                var loginInfo = '<%=Settings.LoginInfo == null%>';

                if (loginInfo == true)
                    return apiAgentInfo;

                agenttype = '<%=Settings.LoginInfo.AgentType%>';
                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {

            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';

            if (IsEmpty(apiHost))
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";

            return apiHost;
        }
    </script>

    <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">
                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left">Attendee Master</h3>
                    </div>
                 </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <button class="btn btn-info mr-2" id="showAttendeeMasterList" type="button" data-toggle="collapse" data-target="#AttendeeMasterList" aria-expanded="false" aria-controls="CardsList" onclick="LoadAttendeeInfo()">VIEW ALL</button>
                    <button type="button" class="btn btn-gray mr-2" onclick="ClearAttendeeInfo();">CLEAR <i class="icon icon-close "></i></button>
                    <button type="button" class="btn btn-primary" onclick="SaveAttendeeInfo();">SUBMIT <i class="icon icon-play_arrow "></i></button>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label>Agent <span class="fcol_red">*</span></label>
                            <select id="ddlAgentId" class="form-control" onchange="AgentChange(this.value)"></select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>First Name<span class="fcol_red">*</span></label>
                            <input type="text" id="txtName" name="Name" class="form-control" maxlength="100" placeholder="Enter Name" 
                                onkeypress="return onKeyPressVal(this.id, 'AL', '100');" onchange="return validateData(this.id, 'AL', '100', 'first name');">
                        </div>
                        <div class="form-group col-md-2">
                            <label>Last Name<span class="fcol_red">*</span></label>
                            <input type="text" id="txtLName" placeholder="Enter last name" class="form-control" maxlength="100" 
                                onkeypress="return onKeyPressVal(this.id, 'AL', '100');" onchange="return validateData(this.id, 'AL', '100', 'last name');">
                        </div>
                        <div class="form-group col-md-2">
                            <label>Title</label>
                            <input type="text" id="txtTitle" placeholder="Enter title" class="form-control" maxlength="50" 
                                onkeypress="return onKeyPressVal(this.id, 'AL', '50');" onchange="return validateData(this.id, 'AL', '50', 'title');">
                        </div>
                        <div class="form-group col-md-2">
                            <label>Type</label>
                            <input type="text" id="txtType" name="Type" class="form-control" maxlength="50" placeholder="Enter Type" 
                                onkeypress="return onKeyPressVal(this.id, 'AN', '50');" onchange="return validateData(this.id, 'AN', '50', 'type');">
                        </div>
                        <div class="form-group col-md-2">
                            <label>Company <span class="fcol_red">*</span></label>
                            <select id="ddlCompany" class="form-control"></select>
                        </div>
                </div>
            </div>
                
                <div id="divAttendeeList" class="exp-content-block" style="display:none">
                   <h5 class="mb-3">All Attendees</h5>
                    <div class="button-controls text-right">
                        <a class="btn btn-edit" onclick="EditAttendeeInfo();">EDIT  <i class="icon icon-edit"></i></a>
                        <a class="btn btn-danger" onclick="DeleteAttendeeInfo();">DELETE  <i class="icon icon-delete"></i></a>
                    </div>
                    <div id="divGridAttendeeInfo"></div>
                </div>

           </div>
        </div>
        <div class="clear "></div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
