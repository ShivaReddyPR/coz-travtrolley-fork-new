﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FixedDepartureDetailsGUI" Title="FixedDeparture Details" Codebehind="FixedDepartureDetails.aspx.cs" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<link href="css/ModalPop.css"  rel="stylesheet" type="text/css" />
   <script type="text/javascript" src="Scripts/jsBE/ModalPop.js"></script>
   <script type="text/javascript" src="Scripts/jsBE/prototype.js"></script>

  
  
  <script type="text/javascript" src="Scripts/Jquery/jquery.min.js"></script>
<script type="text/javascript" src="Scripts/Jquery/ajax_tabs_m.js"></script>
<link rel="stylesheet" href="css/ajax_tab_css.css">
   
  <!------------------------Departure Date Calendar Script------------->
<script language="javascript" type="text/javascript">
    var day_of_week = new Array('Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat');
    var month_of_year = new Array('January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December');

    //  DECLARE AND INITIALIZE VARIABLES
    var Calendar = new Date();

    var years = '<%=departureYears %>';
    var year; //Calendar.getFullYear();     // Returns year
    var months = '<%=departureMonths %>'; //'0,1,2,3,4,5,6,7,8,9,10,11';
    var month;  // Calendar.getMonth();    // Returns month (0-11)
    var today = '<%=departureDates %>';    // Returns day (1-31) Set the dates for Selection here eg: [1],[15] etc
    var weekday = Calendar.getDay();    // Returns day (1-31)

    var DAYS_OF_WEEK = 7;    // "constant" for number of days in a week
    var DAYS_OF_MONTH = 31;    // "constant" for number of days in a month
    var cal = '';    // Used for printing





    /* VARIABLES FOR FORMATTING
    NOTE: You can format the 'BORDER', 'BGCOLOR', 'CELLPADDING', 'BORDERCOLOR'
    tags to customize your caledanr's look. */

    var TR_start = '<TR>';
    var TR_end = '</TR>';
    var highlight_start = '<TD WIDTH="30" HEIGHT="30"><TABLE CELLSPACING=0 BORDER=1 BGCOLOR=DEDEFF BORDERCOLOR=CCCCCC><TR><TD WIDTH=30 HEIGHT=30><B><CENTER>';
    var highlight_end = '</CENTER></TD></TR></TABLE></B>';
    var TD_start = '<TD WIDTH="30" HEIGHT="30"><CENTER>';
    var TD_end = '</CENTER></TD>';

    /* BEGIN CODE FOR CALENDAR
    NOTE: You can format the 'BORDER', 'BGCOLOR', 'CELLPADDING', 'BORDERCOLOR'
    tags to customize your calendar's look.*/
    cal += '<TABLE BORDER=1 CELLSPACING=0 CELLPADDING=0 BORDERCOLOR=BBBBBB STYLE=FONT-SIZE:14px;>';
    //loop thru all the months in a year
    for (var j = 0; j < years.split(',').length; j++) {
        year = years.split(',')[j];
        cal += "<TR>";
        for (var i = 0; i < months.split(',').length; i++) {

            //Insert new row after displaying 4 months in a row
            //            if (i == 4 || i == 8 || i == 12) {
            //                cal += "<TR>";
            //            }

            month = months.split(',')[i];
            if (month.split('-')[0].indexOf(year) == 0) {
                month = month.split('-')[1];
                Calendar.setDate(1);    // Start the calendar day at '1'
                Calendar.setMonth(month);    // Start the calendar month at now                
                cal += '<TD VALIGN=TOP><TABLE BORDER=1 CELLSPACING=0 CELLPADDING=0 BORDERCOLOR=BBBBBB STYLE=FONT-SIZE:14px;><TR><TD>';
                cal += '<TABLE BORDER=0 CELLSPACING=5 CELLPADDING=5>' + TR_start;
                cal += '<TD COLSPAN="' + DAYS_OF_WEEK + '" BGCOLOR="#EFEFEF"><CENTER><B STYLE=FONT-SIZE:16px;>';
                cal += month_of_year[month] + '   ' + year + '</B>' + TD_end + TR_end;
                cal += TR_start;

                //   DO NOT EDIT BELOW THIS POINT  //

                // LOOPS FOR EACH DAY OF WEEK
                for (index = 0; index < DAYS_OF_WEEK; index++) {

                    // BOLD TODAY'S DAY OF WEEK
                    //        if (weekday == index)
                    //            cal += TD_start + '<B>' + day_of_week[index] + '</B>' + TD_end;

                    // PRINTS DAY
                    //else
                    cal += TD_start + day_of_week[index] + TD_end;
                }

                cal += TD_end + TR_end;
                cal += TR_start;

                // FILL IN BLANK GAPS UNTIL TODAY'S DAY
                for (index = 0; index < Calendar.getDay(); index++)
                    cal += TD_start + '  ' + TD_end;

                // LOOPS FOR EACH DAY IN CALENDAR
                for (index = 0; index < DAYS_OF_MONTH; index++) {
                    if (Calendar.getDate() > index) {
                        // RETURNS THE NEXT DAY TO PRINT
                        week_day = Calendar.getDay();

                        // START NEW ROW FOR FIRST DAY OF WEEK
                        if (week_day == 0)
                            cal += TR_start;

                        if (week_day != DAYS_OF_WEEK) {

                            // SET VARIABLE INSIDE LOOP FOR INCREMENTING PURPOSES
                            var day = Calendar.getDate();

                            // HIGHLIGHT TODAY'S DATE
                            /////////////////////////////////////////////////////////////////
                            //////////////////// Highlighting the Dates /////////////////////
                            if (today.indexOf('[' + year + '-' + month + '-' + Calendar.getDate() + ']') >= 0)
                                cal += highlight_start + day + highlight_end + TD_end;
                            // PRINTS DAY                
                            /////////////////////////////////////////////////////////////////
                            else
                                cal += TD_start + day + TD_end;
                        }

                        // END ROW FOR LAST DAY OF WEEK
                        if (week_day == DAYS_OF_WEEK)
                            cal += TR_end;
                    }

                    // INCREMENTS UNTIL END OF THE MONTH
                    Calendar.setDate(Calendar.getDate() + 1);

                } // end for loop

                cal += '</TD></TR></TABLE></TABLE></TD>';

                //Close the row after displaying 4 months in a row
                //            if (i == 7 || i == 11 || i == 12) {
                //                cal += "</TR>";
                //            }
            }
        }
        cal += "</TR>";
    }
    cal += "</TR></TABLE>";
    //  PRINT CALENDAR
    //document.write(cal);

    //  End -->
        </script>
<!----------------------End Departure Date Calendar----------------------------------------->

    
     <script type="text/javascript">


         $(function() {
             $('.tabs').tabs()
         });

         $('.tabs').bind('change', function(e) {
             var nowtab = e.target // activated tab
             var divid = $(nowtab).attr('href').substr(1);
             if (divid == "ajax") {
                 $.getJSON('<%=Request.Url.Scheme%>://1.upstatic.sinaapp.com/api.php').success(function(data) {
                     $("#" + divid).text(data.msg);
                 });
             }


         });
  
</script>

  
  
   
   
<link rel="stylesheet" type="text/css" href="css/fancybox2.css" media="screen" />


    <script type="text/javascript">
    
        function ShowDiv(name) {
            if (name == "home") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('Inclusions').style.display = "none";
                document.getElementById('Itinerary').style.display = "none";
                document.getElementById('RoomRates').style.display = "none";
                document.getElementById('Terms').style.display = "none";
                return false;
            }
            if (name == "Inclusions") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('home').style.display = "none";
                document.getElementById('Itinerary').style.display = "none";
                document.getElementById('RoomRates').style.display = "none";
                document.getElementById('Terms').style.display = "none";
                return false;
            }
            if (name == "Itinerary") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('home').style.display = "none";
                document.getElementById('Inclusions').style.display = "none";
                document.getElementById('RoomRates').style.display = "none";
                document.getElementById('Terms').style.display = "none";
                return false;
            }
            if (name == "RoomRates") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('home').style.display = "none";
                document.getElementById('Inclusions').style.display = "none";
                document.getElementById('Itinerary').style.display = "none";
                document.getElementById('Terms').style.display = "none";
                return false;
            }
            if (name == "Terms") {
                var div = document.getElementById(name);
                div.style.display = "block";
                document.getElementById('home').style.display = "none";
                document.getElementById('Inclusions').style.display = "none";
                document.getElementById('Itinerary').style.display = "none";
                document.getElementById('RoomRates').style.display = "none";
                return false;
            }
        }

        function Validate() {
            var msg = true;
            if (document.getElementById('<%=ddlDepDate.ClientID %>').selectedIndex <= 0) {
                document.getElementById('errMessHotel').style.display = 'block';
                document.getElementById('errMessHotel').innerHTML = 'Enter Valid Date';
                msg = false;
            }
            else if (parseInt(document.getElementById('<%=ddlAdult.ClientID %>').value) < parseInt(document.getElementById('<%=ddlRooms.ClientID %>').value)) {
                document.getElementById('errMessHotel').style.display = 'block';
                document.getElementById('errMessHotel').innerHTML = 'No of Adults should be greater than or equal to No of Rooms';
                msg = false;
            }
            else {
                document.getElementById('errMessHotel').style.display = "none";
                msg = true;
            }
            return msg;
        }
    </script>

   
    
<script type="text/javascript" language="javascript">

    // Regular expression for Not a number;
    var IsNaN = /\D/;
    function SendMailTemp() {
        var paxName = document.getElementById('txtName').value;
        var phoneNo = document.getElementById('txtPhno').value;
        var email = document.getElementById('txtEmail').value;
        var packageName = document.getElementById('dealName').value;
        var dealId = document.getElementById('dealId').value;
        var productType = document.getElementById('productType').value;
        var agencyId = document.getElementById('agencyId').value;
        var adults = parseInt(document.getElementById('<%=ddlAdult.ClientID %>').value);
        var childs = parseInt(document.getElementById('<%=ddlChild.ClientID %>').value);
        var infants = parseInt(document.getElementById('<%=ddlInfant.ClientID %>').value);
        var day = document.getElementById('<%=ddlDepDate.ClientID %>').options[document.getElementById('<%=ddlDepDate.ClientID %>').selectedIndex].innerHTML;
        var roomCount = parseInt(document.getElementById('<%=ddlRooms.ClientID %>').value);
        var paramList = "";
        paramList += 'dealId=' + dealId;
        paramList += '&agencyId=' + agencyId;
        paramList += '&phoneNo=' + phoneNo;
        paramList += '&paxName=' + paxName;
        paramList += '&EmailId=' + email;
        paramList += '&packageName=' + packageName;
        paramList += '&depDate=' + day
        paramList += '&adults=' + adults;
        paramList += '&childs=' + childs;
        paramList += '&infants=' + infants;
        paramList += '&roomCount=' + roomCount;
        paramList += '&productType=' + productType;
        if (Trim(paxName) == "") {
            document.getElementById('susMessage').style.display = "none";
            document.getElementById('Error').style.display = "block";
            document.getElementById('Error').innerHTML = 'Please enter your name';
            return false;
        }
        if (Trim(email) == "") {
            document.getElementById('Error').style.display = "block";
            document.getElementById('Error').innerHTML = 'Please enter your email';
            return false;
        }
        if (!ValidEmail.test(email)) {
            document.getElementById('susMessage').style.display = "none";
            document.getElementById('Error').style.display = "block";
            document.getElementById('Error').innerHTML = 'Please enter valid email';
            return false;
        }
        if (Trim(phoneNo) == "") {
            document.getElementById('susMessage').style.display = "none";
            document.getElementById('Error').style.display = "block";
            document.getElementById('Error').innerHTML = 'Please enter phone number';
            return false;
        }
        if (isNaN(phoneNo)) {
            document.getElementById('susMessage').style.display = "none";
            document.getElementById('Error').style.display = "block";
            document.getElementById('Error').innerHTML = 'Please enter correct phone number';
            return false;
        }
        paramList += '&FDNoStockEnquiry=true';
        var url = "EmailAjax";
        new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: EmailResponse });
    }
    function EmailResponse(response) {
        if (response.responseText == "true") {
            document.getElementById('inline1').style.display = "none";
            document.getElementById('Error').style.display = "none";
            document.getElementById('susMessage').style.display = "block";
            alert('Enquiry Submitted Successfully');
            HideEmailPopup();
            ResetNoStockForm();
        }
    }
    function ResetForm() {
        document.getElementById('phone').value = "";
        document.getElementById('email').value = "";
        document.getElementById('city').value = "";
        document.getElementById('country').value = "";
        document.getElementById('description').value = ""
        document.getElementById('paxName').value = "";
    }

    function ResetNoStockForm() {
        document.getElementById('txtName').value = "";
        document.getElementById('txtPhno').value = "";
        document.getElementById('txtEmail').value = "";
        document.getElementById('txtPaxCount').value = "";
    }


    function SubmitEnquiry() {
        var phoneNo = document.getElementById('phone').value;
        var activityName = document.getElementById('dealName').value;
        var email = document.getElementById('email').value;
        var productType = document.getElementById('productType').value;
        var city = document.getElementById('city').value;
        var country = document.getElementById('country').value;
        var paxName = document.getElementById('paxName').value;
        var description = document.getElementById('description').value;
        var dealId = document.getElementById('dealId').value;
        var agencyId = document.getElementById('agencyId').value;
        var noofpax = document.getElementById('noofpax').value;
        //var paxName = document.getElementById('paxName').value;

        var paramList = "";
        paramList += '&dealId=' + dealId;
        paramList += '&agencyId=' + agencyId;
        paramList += '&description=' + description;
        paramList += '&productType=' + "F";
        paramList += '&phoneNo=' + phoneNo;
        paramList += '&paxName=' + paxName;
        paramList += '&EmailId=' + email;
        paramList += '&City=' + city;
        paramList += '&Country=' + country;
       // paramList += '&nights=' + $('nights').value;
        paramList += '&activityName=' + activityName;
        paramList += '&Noofpax=' + noofpax;
        if (noofpax <= 0) {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please select no of passengers';
            return false;
        }
        if (Trim(paxName) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter your name';
            return false;
        }
        if (Trim(email) == "") {
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter your email';
            return false;
        }
        if (!ValidEmail.test(email)) {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter valid email';
            return false;
        }
        if (Trim(phoneNo) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter phone number';
            return false;
        }
        if (isNaN(phoneNo)) {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter correct phone number';
            return false;
        }
        if (Trim(country) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter your country';
            return false;
        }
        if (Trim(city) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter your city';
            return false;
        }
        if (Trim(description) == "") {
            document.getElementById('message').style.display = "none";
            document.getElementById('err').style.display = "block";
            document.getElementById('err').innerHTML = 'Please enter some description / comment  of your enquiry';
            return false;
        }
        paramList += '&submitActivityEnquiry=true';
        var url = "EmailAjax";
        new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: EnquiryRes });
    }
    function EnquiryRes(response) {
        if (response.responseText == "true") {
            document.getElementById('inline1').style.display = "none";
            document.getElementById('err').style.display = "none";
            document.getElementById('message').style.display = "block";
            alert('Enquiry Submitted Successfully');
		HideEmailPopup();
            ResetForm();
        }
    }
	function ShowEmailPopup() {

	    //        ModalPop.Container = $$$('emailEnquiry');
	    ModalPop.Container = document.getElementById('emailEnquiry');
        ModalPop.Show({ x: 735, y: 539 }, "<div>ytytyy</div>");

    }

    function HideEmailPopup() {
        ModalPop.Hide();
    }


    function ShowNoStockEmailPopup() {
        ModalPop.Container = document.getElementById('noStockAnEnquiry');
        ModalPop.Show({ x: 735, y: 539 }, "<div>ytytyy</div>");
    }
    
    function ShowPopUP() {
        document.getElementById('popup').style.display = "block";
        document.getElementById('message').style.display = "none";
    }
    function HidePopUP() {
        document.getElementById('popup').style.display = "none";
    }
    function HidePopUp() {

        ModalPop.Hide();
    }  
    function markout(textBox, txt) {
        if (textBox.value == "") {
            textBox.value = txt;
        }
    }
    function markin(textBox, txt) {
        if (textBox.value == txt) {
            textBox.value = "";
        }
    }
    </script>
     <script type="text/javascript">
         function isNumber(evt) {
             evt = (evt) ? evt : window.event;
             var charCode = (evt.which) ? evt.which : evt.keyCode;
             if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                 return false;
             }
             return true;
         }
      </script>
    
    
    <% if (activity != null)
       {%>
     <input type="hidden" id="agencyId" name="agencyId" visible="false" value="<%=Settings.LoginInfo.AgentId %>" />
     
    <input type="hidden" id="dealId"   name="dealId" value="<%=activity.Id %>" />
    <input type="hidden" id="productType"   name="productType" value="F" />
    <input type="hidden" id="dealName" name="dealName" value="<%=activity.Name %>" />
    <%} %>
       
        <div style=" padding-top:10px">
          <div class="ns-h3">
                
                    <asp:Label ID="lblActivity" runat="server" Text="" ></asp:Label>
                </div>
            
            <div class="wraps0ppp">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td width="70%" class="bg_shadow">
                                        <asp:Image ID="imgActivity" runat="server" Width="625px" Height="250px" />
                                    </td>
                                    <td width="30%" align="center" valign="top">
                                        <div id="BookingDetails">
                                        <div class="error_msg" style="display:none;" id="errMessHotel"> </div>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <table width="280px" border="0" align="right" cellpadding="0" cellspacing="0">
                                                            <tr>
                                                                <td align="center">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    Starting From
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <p class="best_price">
                                                                        <%=Settings.LoginInfo.Currency %>
                                                                        <asp:Label ID="lblAmount" runat="server" Text=""></asp:Label>
                                                                        
                                                                    </p>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <span class="spccc">per person</span>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    &nbsp;
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <hr class="b_bot_1 " />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td width="31%" align="right">
                                                                                <img src="images/submit_enquiry_icon.jpg" width="26" height="29">
                                                                            </td>
                                                                            <td width="69%" align="left">
                                                                               <%-- <a class="nor_lnk fancybox" href="#inline1">Submit Enquiry </a>--%>
                                                                                
                                                                                
                                                                                 <a class="nor_lnk" href="#" onclick="ShowEmailPopup()">Email</a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <hr class="b_bot_1 " />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td width="31%" align="right">
                                                                                <img src="images/print_icon.jpg" width="26" height="29">
                                                                            </td>
                                                                            <td width="69%" align="left">
                                                                                <a style=" cursor:pointer" class="nor_lnk" onClick="javascript:window.open('PrintFixedDeparture.aspx','','location=0,status=0,scrollbars=1,width=1000,height=600');">Print this Fixed Departure </a>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <hr class="b_bot_1 " />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                        <td height="20" valign="top">
                                                            <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="41%" align="right">
                                                                            Select Tour Date :
                                                                        </td>
                                                                        <td width="69%" align="left">
                                                                             <asp:DropDownList ID="ddlDepDate" runat="server" Width="120px"></asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <hr class="b_bot_1 " />
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                        <td>
                                                            <table cellspacing="0" cellpadding="0" border="0" width="100%">
                                                                <tbody>
                                                                    <tr>
                                                                        <td width="15%" align="right"> 
                                                                        </td>
                                                                        <td align="left">
                                                                            Adults:
                                                                        </td>
                                                                        <td>
                                                                            Children
                                                                        </td>
                                                                        <td>
                                                                            Infants
                                                                        </td>
                                                                         <td>
                                                                            Rooms
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                    <td width="15%" align="right">Select:</td>
                                                                        <td align="left">
                                                                        &nbsp;<asp:DropDownList ID="ddlAdult" CssClass="inp_01" runat="server">                                                                            
                                                                            <asp:ListItem Selected="True" Value="1" Text="1"></asp:ListItem>                                                                                
                                                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>
                                                                                <asp:ListItem Value="7" Text="7"></asp:ListItem>
                                                                                <asp:ListItem Value="8" Text="8"></asp:ListItem>
                                                                                <asp:ListItem Value="9" Text="9"></asp:ListItem>
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="left">
                                                                        &nbsp;<asp:DropDownList ID="ddlChild"  CssClass="inp_01" runat="server">                                                                            
                                                                                <asp:ListItem Selected="True" value="0" Text="0"></asp:ListItem>
                                                                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>                                                                                
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                        <td align="left">
                                                                        &nbsp;<asp:DropDownList ID="ddlInfant" CssClass="inp_01" runat="server">                                                                            
                                                                                <asp:ListItem Selected="True" Value="0" Text="0"></asp:ListItem>
                                                                                <asp:ListItem Value="1" Text="1"></asp:ListItem>
                                                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>                                                                                
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                         <td align="left">
                                                                        &nbsp;<asp:DropDownList ID="ddlRooms" CssClass="inp_01" runat="server">                                                                            
                                                                                <asp:ListItem Value="1" Text="1" Selected="True"></asp:ListItem>
                                                                                <asp:ListItem Value="2" Text="2"></asp:ListItem>
                                                                                <asp:ListItem Value="3" Text="3"></asp:ListItem>
                                                                                <asp:ListItem Value="4" Text="4"></asp:ListItem>
                                                                                <asp:ListItem Value="5" Text="5"></asp:ListItem>
                                                                                <asp:ListItem Value="6" Text="6"></asp:ListItem>                                                                                
                                                                            </asp:DropDownList>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                            <tr>
                                                                <td align="center">
                                                                    <hr class="b_bot_1 " />
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                 <tr>
                                                        <td align="center" style="vertical-align:bottom; height:35px; padding-top:10px ">
                                                                <asp:LinkButton ID="imgCheck" CssClass="button-new"  Text="Check Availability"
                                                                runat="server" onclick="imgCheck_Click" OnClientClick="return Validate();" />
                                                        </td>
                                                    </tr>
                                               <%-- <tr>
                                                    <td align="center" style="vertical-align:bottom; height:35px; padding-top:10px ">
                                                    
                                                    <a Class="book-now-button" href="FixedDepartureAvailability.aspx"> Book Now</a>
                                                    </td>
                                                </tr>--%>
                                            </table>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="container_iframe">
                                <ul class="tabs">
                                    <li><a href="#home">Overview</a></li>
                                   
                                    <li><a href="#Inclusions">Inclusions & Exclusions</a></li>
                                    <li><a href="#Itinerary">Itinerary</a></li>
                                    <li><a href="#RoomRates">Price</a></li>
                                    <li><a href="#DepartureDates">Departure Dates</a></li>
                                    <li><a href="#Terms">Terms & Conditions</a></li>
                                     <li><a href="#Air">Airline</a></li>
                                    <li><a href="#Hotel">Hotel</a></li>
                                </ul>
                                <div class="pill-content">
                                    <div class="active" id="home">
                                        <%=(activity != null ? activity.Overview : "") %>
                                    </div>
                                   
                                   
                                    <div id="Inclusions">
                                        <label>
                                        <h3>Inclusions:</h3>
                                        <ul></ul>
                                        <%if (activity != null)
                                          { %>
                                            <%foreach (string item in activity.Inclusions)
                                              { %>
                                              <li style=" list-style-type:circle; margin-left:20px;"><%=item%></li>
                                            <%} %></label>
                                        <label>
                                        <h3>Exclusions:</h3>
                                            <%foreach (string item in activity.Exclusions)
                                              { %>
                                            <li style=" list-style-type:circle; margin-left:20px;"><%=item%></li>
                                            <%}
                                          }%></label>
                                    </div>
                                   
                                   
                                    <div id="Itinerary">
                                     <div style="height:350px; font-size:14px;overflow-y:auto;">
                                          
                                            <asp:DataList ID="dlFDItinerary" runat="server" CellPadding="4" DataKeyField="Id"
                                                Width="900px">
                                                <ItemTemplate>
                                                        <table style=" border: solid 1px #ccc; margin-bottom:10px;" class="" width="100%">
                                                           <tr style="height: 25px;">
                                                                <td>

                                                                        <table style="margin-bottom:14px; margin-top:10px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                        <td width="14%"> <strong style=" font-size:14px">Day <%# Eval("Day")%> </strong></td>
                                                                        <td width="86%"></td>
                                                                        </tr>

                                                                        </table>
                                                   
                                                                   
                                                                    
                                                                    
                                                                    
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr style="height: 25px;">
                                                                <td>
                                                                  
                                                                  
                                                                  
                                                                  <table style="margin-bottom:16px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="14%"><strong>ItineraryName: </strong></td>
    <td width="86%"><%# Eval("ItineraryName")%></td>
  </tr>
  
</table>
                                                                  
                                                                    
                                                                    
                                                                    
                                                                    
                                                                </td>
                                                            </tr>
                                                                    <tr style="height: 25px;">
                                                                <td>
                                                                
                                 <table style="margin-bottom:10px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="14%"><strong>  Meals:</strong> </td>
    <td width="86%"><%# Eval("Meals")%></td>
  </tr>
  
</table>
                                                                
                                                                    
                                                                </td>
                                                            </tr>
                                                             <tr style="height: 25px;">
                                                                <td>
                                                                   
                                                                   
                                                                   
  <table style="margin-bottom:14px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" width="14%"> <strong>Itinerary: </strong></td>
    <td width="86%"><%# Eval("Itinerary")%></td>
  </tr>
  
</table>
                                                                   
                                                                   
                                                                   
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                        </div>
                                        <%--<label>
                                            <%=(activity != null ? activity.Itinerary1 : "") %></label>
                                        <label>
                                            <%=(activity != null ? activity.Itinerary2 : "") %></label>
                                            <label><%=(activity != null ? activity.Details : "") %></label>
                                            <h3>Transfer included</h3>
                                           <li style=" list-style-type:circle; margin-left:20px;"> <%=(activity != null ? activity.TransferIncluded : "") %></li>
                                            <h3>Meals Included</h3>
                                            <ul>
                                            <%if(activity != null && activity.MealsIncluded != null){ %>
                                            <%string[] mealItems = activity.MealsIncluded.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                            <%foreach (string meal in mealItems)
                                              { %>
                                              <li style=" list-style-type:circle; margin-left:20px;"><%=meal %></li>
                                            <%} %>
                                            <%} %>
                                            </ul>--%>
                                    </div>
                                    
                                    
                                    <div id="RoomRates">
                                    <label>
                                    <h3>Price</h3>
                                            <asp:DataList ID="dlRoomRates" runat="server" Width="100%">
                                                <ItemTemplate>
                                                    <table width="100%" border="1">
                                                    
                                                        <tr>
                                                         <td height="25px" width="30%">
                                                        
                                                        <%#(Eval("PriceDate")==DBNull.Value)?string.Empty: Convert.ToDateTime(Eval("PriceDate")).ToString("dd-MMM-yyyy")%>
                                                        </td>
                                                           <td height="25px" width="30%">
                                                                <%#Eval("Label") %>
                                                            </td>
                                                            <td  width="70%">
                                                            <%=Settings.LoginInfo.Currency %>
                                                                <%#CTCurrencyFormat(Eval("Total"))%>
                                                                
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                            </label>
                                    </div>
                                   
                                   <div id="DepartureDates">
                                   <span id="DepDate">
                                   </span>
                                   </div>
                                   <div id="Terms">
                                        
                                        
                                        <%--<h3>Things to bring</h3>
                                        <ul>
                                        <%if (activity != null && activity.ThingsToBring != null)
                                          { %>
                                        <%string[] things = activity.ThingsToBring.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                        <%foreach (string thing in things)
                                          { %>
                                          <li style=" list-style-type:circle; margin-left:20px;"><%=thing %></li>
                                        <%} %>
                                        <%} %>
                                        </ul>
                                        <br />
                                        <h3>Cancellation Policy</h3>
                                     <ul>
                                        <%if (activity != null && activity.CancelPolicy != null)
                                          { %>
                                        <%string[] cancelP = activity.CancelPolicy.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                        <%foreach (string cancel in cancelP)
                                          { %>
                                          <li style=" list-style-type:circle; margin-left:20px;"><%=cancel %></li>
                                        <%} %>
                                        <%} %>
                                        </ul>--%>
                                     <h3 style="background-color:#18407B;padding-left: 10px; color: #FFFFFF; font-size: 12px;">Partial Payment Policy</h3>
                                        <ul>
                                        <li>
                                         <table width="100%" border="1" cellpadding="0" cellspacing="0">
                                        <tr><td width="25%">Booking Deposit </td><td width="75%">AED 500 per person </td></tr>
                                        <tr><td>2nd Payment</td><td>50% of the balance amount (45 days before departure or with visa submission whichever comes first  )</td></tr>
                                        <tr><td>Final Payment</td><td>Full payment (30 days before departure )</td></tr>
                                        <tr><td>Optional tours / Extras</td><td>Full Payment at the time of booking</td></tr>
                                        </table>
                                        </li>
                                        </ul>
                                        <h3 style="background-color:#18407B;padding-left: 10px; color: #FFFFFF; font-size: 12px;">Cancellation Policy - Applicable for partial payment </h3>
                                        <ul>
                                        <li>
                                       <table  width="100%" border="1" cellpadding="0" cellspacing="0">
                                        <tr><td width="25%">Booking Deposit </td><td width="75%">Non Refundable  </td></tr>
                                        <tr><td>44 days -31days to departure</td><td>10% of the tour price paid will be refunded</td></tr>
                                        <tr><td>30 days -15 days to departure</td><td>50% of the tour price paid will be refunded</td></tr>
                                        <tr><td>14 days & less</td><td>Non Refundable </td></tr>
                                        <tr><td>Optional Tours / Extras</td><td>Non Refundable</td></tr>
                                        </table>
                                        </li>
                                        </ul>
                                        <h3 style="background-color:#18407B;padding-left: 10px; color: #FFFFFF; font-size: 12px;">Cancellation Policy - Applicable for full payment  </h3>
                                        <ul>
                                        <li>
                                       <table  width="100%" border="1" cellpadding="0" cellspacing="0">
                                        <tr><td width="25%">Booking Deposit </td><td width="75%">Non Refundable  </td></tr>
                                        <tr><td>44 days -31days to departure</td><td>50 % of the tour price paid will be refunded</td></tr>
                                        <tr><td>30 days -15 days to departure</td><td>25 % of the tour price paid  will be refunded </td></tr>
                                        <tr><td>14 days & less</td><td>Non Refundable </td></tr>
                                        <tr><td>Optional Tours / Extras</td><td>Non Refundable</td></tr>
                                        </table>
                                        </li>
                                        </ul>
                                        <h3 style="display:none">Payment Policy</h3>
                                        <ul style="display:none">
                                        
                                        </ul>
                                        <br />
                                        <h3 style="display:none">Cancellation Policy</h3>
                                     <ul style="display:none">
                                        
                                        </ul>
                                        
                                        On behalf of the persons booked, I/We have read, understood & accepted the booking <a href="<%=Request.Url.Scheme%>://www.cozmotravel.com/FixDepTerms.aspx" target=_blank>Terms & Conditions</a>. I /We being duly authorized by the said person do hereby agree & accept the same for self & on behalf of the said persons.
                                    </div>
                                    <div id="Air">
                                      <h3>Airline:</h3>
                                        <asp:Label ID="lblAir" runat="server" Text=""></asp:Label>
                                    </div>
                                    <div id="Hotel">
                                    <h3>Hotel Accommodation:</h3>
                                        <asp:Label ID="lblHotel" runat="server" Text=""></asp:Label>
                                    </div>
                                </div>
                            </div>
                            </td></tr></table>
                            <div class="clear">
                            </div>
            </div>
           
        </div>
        
        <div class="clear">
        </div>
    <div id="emailEnquiry" class="signin_box_class" style="left: 150px; top: 51px; width: 735px; height: 539px; display: none; z-index: 9999;">
        <em class="close" style="position: absolute; right: 10px; top: 10px; cursor: pointer">
            <i>
                <img alt="Close" onclick="HidePopUp()" src="images/close-itimes.gif" /></i>
        </em>
        <div class="summer-promotion-window">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="54%" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <h2 style="color: #0099ff; font-family: Calibri; font-size: 22px">
                                        Why Wait &amp; Watch</h2>
                                </td>
                            </tr>
                            <tr>
                                <td height="20" valign="top">
                                    <strong>Grab your opportunity Immediately</strong>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="border-bottom: solid 1px #CCCCCC">
                    </td>
                </tr>
                <tr>
                    <td height="300" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label style="font-size: 11px">
                                        Leave an enquiry below and member of our team will get back to you soon.</label>
                                    <span style="color: Red" class="red padding-left-125" id="err"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>Number of Pax</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <select id="noofpax" name="noofpax">
                                                    <option value="0">-Select-</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                </select>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>Name</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input style="width: 99%;" type="text" id="paxName" name="paxName" class="send-enq" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>E-mail Id</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input style="width: 99%;" type="text" id="email" name="email" class="send-enq" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>Phone Number</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input style="width: 99%;" type="text" id="phone" onkeypress="return isNumber(event)"
                                                    name="phone" class="send-enq" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>Country of Residence</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input style="width: 99%;" type="text" id="country" name="country" class="send-enq" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>City of Residence</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input style="width: 99%;" type="text" id="city" name="city" class="send-enq" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>Please enter a brief description of your need.</strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <textarea style="width: 99%;" rows="3" cols="100" id="description" name="description"
                                                    class="send-enq"></textarea>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="button-new" onclick="SubmitEnquiry()">Submit</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                <dfn id="errMess"></dfn>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i id="message" style="display: none;"><del>
                                        <img src="Images/greenmark.gif" alt="mark" /></del> <dfn>Our Travel Expert will call
                                            you shortly</dfn> </i>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div id="noStockAnEnquiry" class="signin_box_class" style="left: 150px; top: 51px;
        width: 735px; height: 539px; display: none; z-index: 9999;">
        <em class="close" style="position: absolute; right: 10px; top: 10px; cursor: pointer">
            <i>
                <img alt="Close" onclick="HidePopUp()" src="images/close-itimes.gif" /></i>
        </em>
        <div class="summer-promotion-window">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="54%" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <h2 style="color: #0099ff; font-family: Calibri; font-size: 22px">
                                        Stock Not Available</h2>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="border-bottom: solid 1px #CCCCCC">
                    </td>
                </tr>
                <tr>
                    <td height="300" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label style="font-size: 11px">
                                        Leave an enquiry below and member of our team will get back to you soon.</label>
                                    <span style="color: Red" class="red padding-left-125" id="Error"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>Name</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input style="width: 99%;" type="text" id="txtName" name="paxName" class="send-enq" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>E-mail Id</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input style="width: 99%;" type="text" id="txtEmail" name="email" class="send-enq" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>Phone Number</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input style="width: 99%;" type="text" id="txtPhno" onkeypress="return isNumber(event)"
                                                    name="phone" class="send-enq" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="button-new" onclick="SendMailTemp()">Submit</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i id="susMessage" style="display: none;"><del>
                                        <img src="Images/greenmark.gif" alt="mark" /></del> <dfn>Our Travel Expert will call
                                            you shortly</dfn> </i>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
        <div id="inline1" style="display: none;">
        </div>
         <script>
             document.getElementById('DepDate').innerHTML = cal;
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

