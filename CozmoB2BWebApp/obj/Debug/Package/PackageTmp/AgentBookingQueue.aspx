<%@ Page AutoEventWireup="true" Inherits="BookingQueue"
    Language="C#" MasterPageFile="~/TransactionBE.master" Codebehind="AgentBookingQueue.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.BookingEngine.GDS" %>
<%@ Import Namespace="CT.AccountingEngine" %>
<%@ Import Namespace="System.Collections.Generic" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">
    <link href="css/TicketQuee.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <script>
        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            init();
            cal1.show();
            cal2.hide();
        }


        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= txtFromDate.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());


            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtFromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=txtFromDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            //         if (difference < 1) {
            //             document.getElementById('errMess').style.display = "block";
            //             document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
            //             return false;
            //         }
            //         if (difference == 0) {
            //             document.getElementById('errMess').style.display = "block";
            //             document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
            //             return false;
            //         }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);
    </script>

    <script>
        function show(id) {
            document.getElementById(id).style.visibility = "visible";
        }
        function hide(id) {
            document.getElementById(id).style.visibility = "hidden";
        }


        function showStuff(id) {
            document.getElementById(id).style.display = 'block';
        }


        function hidestuff(boxid) {
            document.getElementById(boxid).style.display = "none";
        }

        function showAgent(id) {

            document.getElementById('DisplayAgent' + id).style.display = 'block';
        }

        function ShowRemarks(id,index)
        {
            
            var ddl = id;
            var txtid='ctl00_cphTransaction_dlBookingQueue_ctl0'+index+'_txtCorpRemarks';
            if($('#'+ddl).val()=='R')
            {                
                $('#'+txtid).show();
            }
            else
            {
                $('#'+txtid).hide();
            }
        }

        function ValidateCorpStatus(index)
        {
            var valid=true;
            var prefix ='ctl00_cphTransaction_dlBookingQueue_ctl0';
            var ddl = '_ddlCorpStatus';
            var txt = '_txtCorpRemarks';
            var lbl = '_lblCorpError';
            if( $('#'+prefix + index + ddl).val()=='-1' )
            {
                $('#'+prefix+index+lbl).html('Please select any status to Update!');
                valid=false;
            }
            else if( $('#'+prefix + index + ddl).val()=='R' && $('#'+prefix + index + txt).val().length == 0 )
            {
                $('#'+prefix+index+lbl).html('Please enter rejection remarks!');    
                valid=false;
            }

            return valid;
        }
  
    </script>

    <script type="text/javascript">
        var Ajax;

        function ShowPage(pageNo) {
            //            if (document.getElementById('OnHold').checked == true) {
            //                document.getElementById('HOLD').value = "true";
            //            }
            //            else {
            //                document.getElementById('HOLD').value = "false";
            //            }
            document.getElementById('<%=PageNoString.ClientID %>').value = pageNo;
            document.getElementById('<%=btnSearch.ClientID %>').click();
        }

        function FilterData() {

            if ($('OnHold').checked == true) {
                $('HOLD').value = "true";
            }
            else {
                $('HOLD').value = "false";
            }
            document.forms[0].submit();
        }



        var sFlightDate = true;
        var sBookingDate = true;

        function swapAllId(i) {
            temp = $("Result-" + i).innerHTML;
            $("Result-" + i).innerHTML = $("Result-" + (i + 1)).innerHTML;
            $("Result-" + (i + 1)).innerHTML = temp;

            temp = $("Airline-" + i).id;
            $("Airline-" + i).id = $("Airline-" + (i + 1)).id;
            $("Airline-" + (i + 1)).id = temp;

            temp = $("Status-" + i).id;
            $("Status-" + i).id = $("Status-" + (i + 1)).id;
            $("Status-" + (i + 1)).id = temp;

            temp = $("PAXName-" + i).id;
            $("PAXName-" + i).id = $("PAXName-" + (i + 1)).id;
            $("PAXName-" + (i + 1)).id = temp;


            temp = $("BookingDate-" + i).id;
            $("BookingDate-" + i).id = $("BookingDate-" + (i + 1)).id;
            $("BookingDate-" + (i + 1)).id = temp;

            temp = $("FlightDate-" + i).id;
            $("FlightDate-" + i).id = $("FlightDate-" + (i + 1)).id;
            $("FlightDate-" + (i + 1)).id = temp;

            temp = $("Result-" + i).style.display;
            $("Result-" + i).style.display = $("Result-" + (i + 1)).style.display;
            $("Result-" + (i + 1)).style.display = temp;
        }

        var confirmingResult = -1;

        function ConfirmBooking(bookingId, pnr, resultNo) {
            if (confirmingResult < 0) {
                if (confirm("Are you sure to confirm " + pnr)) {
                    if (window.XMLHttpRequest) {
                        Ajax = new XMLHttpRequest();
                    }
                    else {
                        Ajax = new ActiveXObject('Microsoft.XMLHTTP');
                    }
                    scroll(0, 0);
                    confirmingResult = resultNo;
                    document.getElementById('MessageBoxC').innerHTML = "Confirming seats : " + pnr;
                    var page = "ConfirmBooking";
                    var pars = "pnr=" + pnr;
                    pars += "&bookingId=" + bookingId;
                    //new Ajax.Request(page, { method: 'post', parameters: pars, onComplete: HandleConfirm });
                    Ajax.onreadystatechange = HandleConfirm;
                    Ajax.open('POST', page);
                    Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    Ajax.send(pars);
                }
            }
            else {
                alert("Confirmation already in progress !");
            }
        }
        function HandleConfirm() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        var confirmRes = Ajax.responseText.split("|");
                        document.getElementById('MessageBoxC').innerHTML = confirmRes[2];
                        if (eval(confirmRes[0]) > 0) {
                            document.getElementById('Status' + confirmingResult).style.display = "none";
                            document.getElementById('ConfirmBlock' + confirmingResult).style.display = "none";
                        }
                        confirmingResult = -1;
                    }
                }
            }
        }


        var releasingResult = -1;
        var releasingBookingId = -1;
        function ReleaseSeat(pnr, ressultNo, bookingId) {
            //$('.modal').modal('hide');
            if (releasingResult < 0) {
                if (confirm("Are you sure to cancel " + pnr)) {
                   
                    if (window.XMLHttpRequest) {
                        Ajax = new XMLHttpRequest();
                    }
                    else {
                        Ajax = new ActiveXObject('Microsoft.XMLHTTP');
                    }
                    scroll(0, 0);
                    releasingResult = ressultNo;
                    releasingBookingId = bookingId;
                    document.getElementById('MessageBox').innerHTML = "Releasing seats : " + pnr;
                    var page = "ReleaseSeat";
                    var pars = "pnr=" + pnr;
                    pars += "&bookingId=" + bookingId;
                    //new Ajax.Request(page, { method: 'post', parameters: pars, onComplete: HandleCancel });
                    Ajax.onreadystatechange = HandleCancel;
                    Ajax.open('POST', page);
                    Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                    Ajax.send(pars);
              }
            }
            else {
                alert("One cancellation already in progress");
            }
        }

        function HandleCancel() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        var cancelRes = Ajax.responseText.split("|");
                        document.getElementById('MessageBox').innerHTML = cancelRes[2];
                        if (eval(cancelRes[0]) > 0) {
                            // document.getElementById('Result-' + releasingResult).style.display = "none";
                            document.getElementById('ctl00_cphTransaction_dlBookingQueue_ctl0' + releasingResult + '_lblReleaseSeat').style.display = "none"

                        }
                        releasingResult = -1;
                        releasingBookingId = -1;
                    }
                }
            }
        }
        function ViewBookingForTicket(BookingId) {
            var finalurl = "ViewBookingForTicket?bookingId=" + BookingId + "&fromAgent=true";
            window.location = finalurl;
        }
        function ViewInvoice(FlightId, AgentId) {
            window.open("CreateInvoice?flightId=" + FlightId + "&agencyId=" + AgentId + "&fromAgent=true", 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
            //            var finalurl = "CreateInvoice.aspx?flightId=" + FlightId + "&agencyId=" + AgentId + "&fromAgent=true";
            //            window.location = finalurl;
        }
      
    </script>

    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
    </iframe>
    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; left: 250px; display: none;
            z-index: 9999">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 120px; left: 500px; display: none;
            z-index: 9999">
        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdfParam" Value="1"></asp:HiddenField>
    <table cellpadding="0" cellspacing="0" class="label">
        <tr>
            <td style="width: 700px" align="left">
                <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
                    onclick="return ShowHide('divParam');">Hide Parameter</a>
            </td>
        </tr>
    </table>
    <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true">
            <div class="paramcon">
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        From Date:
                    </div>
                    <div class="col-md-2">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtFromDate" runat="server" CssClass="inputEnabled form-control"
                                        Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal1()">
                                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">
                        To Date:</div>
                    <div class="col-md-2">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showCal2()">
                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-2">
                        Agent:</div>
                    <div class="col-md-2">
                        <asp:DropDownList AppendDataBoundItems="true" CssClass="form-control" ID="ddlAgency"
                            runat="server" OnSelectedIndexChanged="ddlAgency_SelectedIndexChanged" AutoPostBack="true">
                            <%--<asp:ListItem Value="-1">Select</asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        <asp:Label ID="lblB2BAgent" Text="Client (P):" runat="server"></asp:Label>
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="form-control" OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged"
                            AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        <asp:Label ID="lblB2B2BAgent" Text="Client (C):" runat="server"></asp:Label></div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control"
                            OnSelectedIndexChanged="ddlB2B2BAgent_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        Location:</div>
                    <div class="col-md-2">
                        <asp:DropDownList CssClass="form-control" AppendDataBoundItems="true" ID="ddlLocation"
                            runat="server">
                            <asp:ListItem Selected="True" Value="-1">Select Location</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        Booking Status:
                    </div>
                    <div class="col-md-2">
                        <asp:DropDownList AppendDataBoundItems="true" CssClass="form-control" ID="ddlBookingStatus"
                            runat="server">
                            <asp:ListItem Selected="True" Value="-1">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        Pax Name:</div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtPaxName" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        PNR No.</div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtPNR" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                        <asp:Label ID="lblTransType" runat="server" Text="TransType:" Visible="false"></asp:Label></div>
                    <div class="col-md-2">
                        <asp:DropDownList ID="ddlTransType" CssClass="form-control" runat="server" Visible="false">
                            <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                            <asp:ListItem Value="B2B" Text="B2B"></asp:ListItem>
                            <asp:ListItem Value="B2C" Text="B2C"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                    <div class="col-md-2">
                        Routing Trip Id.</div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtRoutingTrip" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <div class="col-md-2">
                        Corporate Trip Id.</div>
                    <div class="col-md-2">
                        <asp:TextBox ID="txtCorpTripId" runat="server" CssClass="form-control"></asp:TextBox>
                    </div>
                    <br />
                    <div class="col-md-12">
                        <asp:Button ID="btnSearch" CssClass="btn but_b pull-right" runat="server" Text="Search"
                            OnClick="btnSearch_Click" />
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </asp:Panel>
    </div>
    <br />
    <asp:HiddenField ID="PageNoString" runat="server" Value="1" />
    <input id="HOLD" name="HOLD" type="hidden" value="false" />
    <div id="MessageBox" style="color: Red; font-weight: bold">
        <%=message %>
    </div>
    <div id="MessageBoxC" style="color: Red; font-weight: bold">
    </div>
    <input id="ResultCount" type="Hidden" value="" />
    <div style="text-align: right">
        <%=show%></div>
    <div>
        <asp:DataList ID="dlBookingQueue" runat="server" Width="100%" OnItemDataBound="dlBookingQueue_ItemDataBound"
            OnItemCommand="dlBookingQueue_ItemCommand">
            <ItemTemplate>
                <div style="display: none" id="DisplayAgent<%#Container.ItemIndex %>" class="div-TravelAgent">
                    <span style="position: absolute; right: 0px; top: 0px; cursor: pointer"><a href="#"
                        onclick="hidestuff('DisplayAgent<%#Container.ItemIndex %>');">
                        <img src="images/close1.png" /></a></span>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td height="25">
                                <b>
                                    <asp:Label ID="lblAgencyName" runat="server" Text=""></asp:Label></b>
                            </td>
                            <td>
                                Credit Balance: <strong>
                                    <asp:Label ID="lblAgencyBalance" runat="server" Text=""></asp:Label></strong>
                            </td>
                        </tr>
                        <tr>
                            <td height="25">
                                Mob : <strong>
                                    <asp:Label ID="lblAgencyPhone1" runat="server" Text=""></asp:Label></strong>
                            </td>
                            <td>
                                Phone : <strong>
                                    <asp:Label ID="lblAgencyPhone2" runat="server" Text=""></asp:Label></strong>
                            </td>
                        </tr>
                        <tr>
                            <td height="25">
                                Email : <strong>
                                    <asp:Label ID="lblAgencyEmail" runat="server" Text=""></asp:Label></strong>
                            </td>
                            <td>
                                <%--<a href="#">View Full Profile</a>--%>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="tbl">
                    <asp:HiddenField ID="hdnIndex" runat="server" />
                    <div class="col-md-12 padding-0 marbot_10">
                        <div class="col-md-4">
                            <a href="#" onclick="showAgent(<%#Container.ItemIndex %>);"><strong>
                                <asp:Label ID="lblAgentName" runat="server" Text=""></asp:Label></strong></a></div>
                        <div class="col-md-4">
                            <asp:Label ID="lblBookingStatus" runat="server" Text=""></asp:Label>
                        </div>
                        <div class="col-md-4">
                            <label class="pull-left">
                                <strong>PNR-<b><asp:Label ForeColor="Green" ID="lblPNR" runat="server" Text=""></asp:Label></b><span>&nbsp;</span>
                                    <b><asp:Label ID="lblRoutingTripLabel" runat="server" Text=" Routing Trip Id-"></asp:Label><asp:Label ForeColor="Green" ID="lblRoutingTrip" runat="server" Text=""></asp:Label></b>
                                </strong></label>
                            <div style="display: none" id="Release-Seat">
                                <textarea class="" name="textarea" id="textarea" cols="20" rows="4"></textarea>
                            </div>
                            <div>
                                
                                <asp:HyperLink CssClass="btn btn-primary ml-0" ID="isEditBtn" Visible="false" runat="server">Edit</asp:HyperLink>
                            </div>
                        </div>
                         
                        
                       
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="col-md-12 padding-0 marbot_10">
                        <div class="col-md-4">
                            <asp:Label ID="lblAirline" runat="server" Text=""></asp:Label>|
                            <asp:Label ID="lblFlightNumber" runat="server" Text=""></asp:Label>|
                            <asp:Label ID="lblAdultCount" runat="server" Text=""></asp:Label>
                            ADT</div>
                        <div class="col-md-4">
                            <strong>
                                <asp:Label ID="lblTrip" runat="server" Text=""></asp:Label>
                            </strong>
                        </div>
                        <div class="col-md-4">
                            Total Price:<strong><b>
                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label></b> </strong>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="col-md-12 padding-0 marbot_10">
                        <div class="col-md-4">
                            Booked:<b>
                                <asp:Label ID="lblDayString" runat="server" Text=""></asp:Label>(<asp:Label ID="lblBookingDate"
                                    runat="server" Text=""></asp:Label>)</b></div>
                        <div class="col-md-4">
                            <b>
                                <asp:Label ID="lastTicketingDate" ForeColor="Red" runat="server" Text="Last Ticketing Date:"
                                    Visible="false"></asp:Label>
                            </b>
                            <asp:Label ID="lastTicketingDateValue" runat="server" ForeColor="Red" Visible="false"></asp:Label>
                        </div>
                        <div class="col-md-4">
                            Pax Name:<b><asp:Label ID="lblLeadPaxName" runat="server" Text=""></asp:Label></b></div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="col-md-12 padding-0 marbot_10">
                        <div class="col-md-4">
                            Travel Date:<b><asp:Label ID="lblDepartureTime" runat="server" Text=""></asp:Label></b>
                        </div>
                        <div class="col-md-4">
                            Location:<b><asp:Label ID="lblLocation" runat="server" Text=""></asp:Label></b></div>
                        <div class="col-md-4">
                            Booked By :<b><asp:Label ID="lblBookedBy" runat="server" Text=""></asp:Label></b>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                   
                    <div class="col-md-12 padding-0 marbot_10">
                        <%-- Modified below class name for Corporate --%> 
                        <div class="col-md-2">
                             <b><asp:Label ID="lblSource" runat="server" Text=""></asp:Label></b>
                            
                        </div>

                        <%-- Newly Added Content for Corporate Module --%> 
                        <div class="col-md-4">    
                             <strong class="text-success"><asp:Label ID="lblApproved" runat="server"></asp:Label></strong> <!--FOR Approved Status -->
                             <strong class="text-warning"><asp:Label ID="lblPending" runat="server"></asp:Label></strong> <!--FOR Pending Status -->
                             <strong class="text-danger"><asp:Label ID="lblRejected"  runat="server"></asp:Label></strong> <!--FOR Rejected Status -->
                            
                            <strong class="text-success"><asp:Label ID="lblApprovedBy" runat="server" Text=""></asp:Label></strong>
                        </div>
                        <div class="col-md-2">
                            <asp:Label ID="lblCorpStatus" runat="server" Text="Select Status" Visible="false"></asp:Label>
                            <asp:DropDownList ID="ddlCorpStatus" runat="server" Visible="false">
                                <asp:ListItem Selected="True" Text="Select Status" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="Approve" Value="A"></asp:ListItem>
                                <asp:ListItem Text="Reject" Value="R"></asp:ListItem>
                            </asp:DropDownList>
                            <asp:TextBox ID="txtCorpRemarks" runat="server" TextMode="MultiLine" Width="200px" Height="50px" style="display:none;"></asp:TextBox>
                            <asp:Button ID="btnUpdateCorpStatus" Text="Update Status" Visible="false" runat="server" CssClass="button" CommandArgument='<%#Eval("FlightId") %>' CommandName="UpdateCorpStatus" />
                            <asp:Label ID="lblCorpError" runat="server" ForeColor="Red"></asp:Label>
                        </div>
                        <%-- END: Newly Added Content for Corporate Module --%> 


                        <div class="col-md-1" style="text-align:right;margin-left:-25px;">
                            <label>
                                <asp:Label ID="lblViewBooking" runat="server" Text=""></asp:Label>
                            </label>                           
                        </div>
                        <div class="col-md-1" style="text-align:left">
                         <form id="InvoiceForm<%#Container.ItemIndex %>" action="CreateInvoice.aspx" method="post">
                            <input type="hidden" name="agencyId" value='<%#Eval("AgencyId") %>' />
                            <input type="hidden" name="flightId" value='<%#Eval("FlightId") %>' />
                            <input type="hidden" name="pnr" value='<%#Eval("PNR") %>' />
                            <input type="hidden" name="invoiceNumber" />
                            <%-- <asp:Button ID="Button1" runat="server" Text="View Invoice" />--%>
                            <asp:Label ID="lblViewInvoice" runat="server" Text=""></asp:Label>
                            </form>
                        </div>
                    </div>
                   
                    <div class="col-md-12 padding-0 marbot_10">
                        <div style="display: none">
                            Flight:<b><asp:Label ID="lblTimeString" runat="server" Text=""></asp:Label></b>
                        </div>
                        <div class="col-md-3" style="text-align: right;">
                            <asp:Label ID="lblRequestChange" runat="server" Text="Request Change :"></asp:Label></div>
                        <div class="col-md-3">
                            
                            <asp:DropDownList CssClass="form-control" ID="ddlChangeRequestType" runat="server">
                                <asp:ListItem Selected="True" Text="Select" Value="0"></asp:ListItem>
                                <asp:ListItem Text="Void" Value="1"></asp:ListItem>
                                <asp:ListItem Text="Modification" Value="2"></asp:ListItem>
                                <asp:ListItem Text="Refund" Value="3"></asp:ListItem>
                            </asp:DropDownList>
                            <b id="errRemarks" style="color: Red" runat="server"></b>
                        </div>
                        <div class="col-md-3">
                            <asp:TextBox ID="txtRemarks" TextMode="MultiLine" CssClass="form-control" Text="Enter Remarks here"
                                runat="server" onfocus="this.style.color='#000000'; if( this.value=='Enter Remarks here' ) { this.value=''; }"
                                onblur="this.style.color='#000000'; if( this.value=='' ) { this.value='Enter Remarks here'; }"></asp:TextBox>
                        </div>
                        <div class="col-md-3">
                            <div>
                                <asp:Button ID="btnRequest" runat="server" CommandName="ChangeRequest" CommandArgument='<%#Eval("FlightId") %>'
                                    Text="Submit Request" CssClass="button" OnClientClick="return Validate('<%#Container.ItemIndex %>');" />

                                 <asp:HyperLink CssClass="btn btn-primary ml-0" ID="lblReleaseSeat" runat="server">Release Seat</asp:HyperLink> 

                                 <%--   <a href="javascript:void(0);" type="button" class="btn btn-primary" data-toggle="modal" data-target="#TBOConfirm<%#Eval("FlightId") %>">Release Seat</a>--%>

                                <%-- Release Seat Confirmation Modal --%>
                                <%-- <div class="modal fade" data-backdrop="static" tabindex="-1" role="dialog" id="TBOConfirm<%#Eval("FlightId") %>">
                                                  <div class="modal-dialog modal-sm" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-body">
                                                           <p>Are you sure you want to release seat?</p> 
                                                      </div>
                                                      <div class="modal-footer text-center">
                                                          <asp:HyperLink CssClass="btn btn-primary ml-0" ID="lblReleaseSeat" runat="server">Release Seat</asp:HyperLink> 
                                                          <a href="javascript:void(0);" type="button" class="btn btn-primary ml-0"  data-dismiss="modal" >Cancel</a>
                                                      </div>
                                                    </div><!-- /.modal-content -->
                                                  </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->--%>
                            </div>
                            <div>
                                <asp:Label ID="lblRequestStatus" BackColor="#9AF1A2" ForeColor="Black" runat="server"
                                    Font-Italic="True" Font-Names="Arial" Font-Size="10pt"></asp:Label></div>
                        </div>
                        <div class="col-md-9">
                            <div>
                                <table id="tblChangeRequest" width="100%" runat="server">
                                    <tr>
                                        <td align="left" colspan="2" style="padding-left: 20px">
                                            
                                        </td>
                                    </tr>
                                </table>
                                <div style="position: relative">
                                                <asp:LinkButton ID="lnkPayment" runat="server" Visible="false" Text="Payment Information"
                                                    href="#"></asp:LinkButton>
                                                <div id="PaymentInfo-<%#Container.ItemIndex %>" class="visa_PaymentInfo_pop" style="position: absolute;
                                                    display: none; bottom: -70px; left: 234px; height: auto!important; z-index: 9999;">
                                                </div>
                                            </div>
                            </div>
                            <div class="clearfix">
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                 
                    <div class="col-md-12">
                       <asp:Label ID="lblCorpBookingCode" runat="server" Text=""></asp:Label>
                    </div> 
                   
                    
                    <div class="clearfix">
                    </div>
                </div>
            </ItemTemplate>
            <FooterTemplate>
                <asp:Label Visible='<%#bool.Parse((dlBookingQueue.Items.Count==0).ToString())%>'
                    runat="server" ID="lblNoRecord" Text="No Record Found!"></asp:Label>
            </FooterTemplate>
        </asp:DataList>
    </div>
    <div style="text-align: right">
        <%=show%></div>

    <script type="text/javascript">
        $('OnHold').checked = true;
    </script>

    <script type="text/javascript">
        $('AgencyLive').checked = true;             
    </script>

    <script type="text/javascript">
        $('AgencyOnline').checked = true;             
    </script>

    <script type="text/javascript">
        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }
        function CancelAirBooking(index) {
            var val = document.getElementById('ctl00_cphTransaction_dlBookingQueue_ctl0' + index + '_txtRemarks').value;
            if (document.getElementById('ctl00_cphTransaction_dlBookingQueue_ctl0' + index + '_ddlChangeRequestType').value == 0) {
                document.getElementById('ctl00_cphTransaction_dlBookingQueue_ctl0' + index + '_errRemarks').innerHTML = "Please Select Request type!";
                return false;
            }
            else if (val.length <= 0 || val == "Enter Remarks here") {
                document.getElementById('ctl00_cphTransaction_dlBookingQueue_ctl0' + index + '_errRemarks').innerHTML = "Enter remarks";
                return false;
            }
            else {
                document.getElementById('ctl00_cphTransaction_dlBookingQueue_ctl0' + index + '_errRemarks').innerHTML = "";
                return true;
            }
        }


        //Added Function for get payment info for B2C only on 13062016

        var Ajax;
        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        function ViewPaymentInfo(id, bookingId) {

            pblockId = id;
            var url = "PaymentInfoAjax";
            var paramList = 'isPaymentInfo=true';
            paramList += '&bookingId=' + bookingId;
            paramList += '&blockId=' + pblockId;
            document.getElementById('PaymentInfo-' + pblockId).style.display = "block";
            document.getElementById('PaymentInfo-' + pblockId).innerHTML = "Loading...";

            Ajax.onreadystatechange = ShowPaymentInfoPopUp;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }



        function ShowPaymentInfoPopUp() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        var numberOfRecord = eval('<%=recordsPerPage%>');
                        for (var i = 0; i < numberOfRecord; i++) {
                            if (document.getElementById('PaymentInfo-' + i) != null)
                                document.getElementById('PaymentInfo-' + i).style.display = "none";

                        }
                        document.getElementById('PaymentInfo-' + pblockId).style.display = "block";
                        document.getElementById('PaymentInfo-' + pblockId).innerHTML = Ajax.responseText;

                    }
                }
            }
        }
        
    
    </script>

</asp:Content>
