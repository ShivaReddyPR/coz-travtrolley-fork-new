﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="UpdateMarkupGUI" Title="Update Markup" Codebehind="UpdateMarkup.aspx.cs" %>
    <%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<%--   <script type="test/css">
    .tblpax {font-size:13px; border: solid 1px #ccc; }
    .heading {background:#DADADA; font-size:13px; margin-left:10px;  font-weight:bold;}
    </script>--%>
      <style type="text/css">
    .chkBoxList td
    {
        width:160px;
    }
    </style>
<script type="text/javascript">

    //    function ShowAgent(id) {

    //        for (var i = 0; i < id.rows.length; i++) {
    //            var cbl = document.getElementById('<%=chkProduct.ClientID %>').getElementsByTagName("input");
    //            for (j = 0; j < cbl.length; j++) cbl[j].checked = false;
    //            var table = document.getElementById('<%=tblMarkup.ClientID %>');
    //            if (id.rows[i].cells[1].firstChild.checked) {
    //                document.getElementById('<%=ddlAgent.ClientID %>').style.display = "block";
    //            }
    //            else {
    //                document.getElementById('<%=ddlAgent.ClientID %>').style.display = "none";
    //                document.getElementById('<%=ddlAgent.ClientID %>').selectedIndex = 0;
    //            }
    //        }
    //    }

    //    function Show() {
    //        var chk = document.getElementById("<%=chkProduct.ClientID%>");
    //        var checkbox = chk.getElementsByTagName("input");
    //        var counter = 0;
    //        for (var i = 0; i < checkbox.length; i++) {

    //            if (checkbox[i].checked) {
    //                document.getElementById('ctl00_cphTransaction_lblMarkUp_' + i).style.display = "block";
    //                document.getElementById('ctl00_cphTransaction_txtMarkUp_' + i).style.display = "block";
    //                document.getElementById('ctl00_cphTransaction_lblmarkUpType_' + i).style.display = "block";
    //                document.getElementById('ctl00_cphTransaction_ddlMarkupType_' + i).style.display = "block";
    //                document.getElementById('ctl00_cphTransaction_lblDiscountType_' + i).style.display = "block";
    //                document.getElementById('ctl00_cphTransaction_ddlDiscountType_' + i).style.display = "block";
    //                document.getElementById('ctl00_cphTransaction_lblDiscount_' + i).style.display = "block";
    //                document.getElementById('ctl00_cphTransaction_txtDiscount_' + i).style.display = "block";
    //                if (i < 2) {
    //                    document.getElementById('ctl00_cphTransaction_lblSource_' + i).style.display = "block";
    //                    document.getElementById('ctl00_cphTransaction_ddlSource_' + i).style.display = "block";
    //                }
    //            }
    //            else {
    //                document.getElementById('ctl00_cphTransaction_lblMarkUp_' + i).style.display = "none";
    //                document.getElementById('ctl00_cphTransaction_txtMarkUp_' + i).style.display = "none";
    //                document.getElementById('ctl00_cphTransaction_lblmarkUpType_' + i).style.display = "none";
    //                document.getElementById('ctl00_cphTransaction_ddlMarkupType_' + i).style.display = "none";
    //                document.getElementById('ctl00_cphTransaction_lblDiscountType_' + i).style.display = "none";
    //                document.getElementById('ctl00_cphTransaction_ddlDiscountType_' + i).style.display = "none";
    //                document.getElementById('ctl00_cphTransaction_lblDiscount_' + i).style.display = "none";
    //                document.getElementById('ctl00_cphTransaction_txtDiscount_' + i).style.display = "none";
    //                if (i < 2) {
    //                    document.getElementById('ctl00_cphTransaction_lblSource_' + i).style.display = "none";
    //                    document.getElementById('ctl00_cphTransaction_ddlSource_' + i).style.display = "none";
    //                }
    //            }
    //        }
    //    }

    function validation() {

        if (document.getElementById('<%=ddlAgent.ClientID %>').selectedIndex <= 0) {
            document.getElementById('<%= errMess.ClientID %>').style.display = "block";
            document.getElementById('<%= errorMessage.ClientID %>').innerHTML = "Please select Agent!";
            return false;
        }


        var chklist = document.getElementById('<%= chkProduct.ClientID %>');
        var chkListinputs = chklist.getElementsByTagName("input");
        var count = 0;
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                if (i < 2 || i == 4) {
                    if (document.getElementById('ctl00_cphTransaction_ddlSource_' + i).selectedIndex <= 0) {
                        document.getElementById('<%= errMess.ClientID %>').style.display = "block";
                        document.getElementById('<%= errorMessage.ClientID %>').innerHTML = "Please select Source!";
                        return false;
                    }
                }
                count = count + 1;
            }
        }
        if (count == 0) {
            document.getElementById('<%= errMess.ClientID %>').style.display = "block";
            document.getElementById('<%= errorMessage.ClientID %>').innerHTML = "Atleast one Product should be selected !";
            return false;
        }
        return true;
    }


    function Check(id) {
        var val = document.getElementById(id).value;
        if (val == '0.0000') {
            document.getElementById(id).value = '';
        }
    }

    function Set(id) {
        var val = document.getElementById(id).value;
        if (val == '' || val == '0.0000') {
            document.getElementById(id).value = '0.0000';
        }
    }
    function SetValue() {
        var chklist = document.getElementById('<%= chkProduct.ClientID %>');
        var chkListinputs = chklist.getElementsByTagName("input");
        var AgentMarkup = "";
        var OurComm = "";
        var totalFee = ""
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                AgentMarkup = parseFloat(document.getElementById('ctl00_cphTransaction_txtAgentMarkup_' + i).value);
                if (isNaN(AgentMarkup)) {
                    AgentMarkup = 0;
                }
                OurComm = parseFloat(document.getElementById('ctl00_cphTransaction_txtOurComm_' + i).value);
                if (isNaN(OurComm)) {
                    OurComm = 0;
                }
                totalFee = AgentMarkup + OurComm
                document.getElementById('ctl00_cphTransaction_txtMarkUp_' + i).value = totalFee.toFixed(4);
                document.getElementById('ctl00_cphTransaction_txtAgentMarkup_' + i).value = AgentMarkup.toFixed(4);
                document.getElementById('ctl00_cphTransaction_txtOurComm_' + i).value = OurComm.toFixed(4);
            }
        }
    }

//    function agentValidate() {
//        if (document.getElementById('<%=ddlAgent.ClientID %>').selectedIndex <= 0) {
//            document.getElementById('<%= errMess.ClientID %>').style.display = "block";
//            document.getElementById('<%= errorMessage.ClientID %>').innerHTML = "Please select Agent!";
//            return false;
//        }
//    }
</script>

    <div class="">
       
        <div class="ns-h3"> Update Markup</div>
        <asp:HiddenField ID="hdnCount" runat="server" />
        
       <div class="paramcon bg_white bor_gray pad_10">
       
       
      
                  <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-1 col-xs-2 marbot_10"> <asp:Label ID="lblAgent" runat="server" Text="Agent:" Font-Bold="true"></asp:Label></div>
    
    
    <div class="col-md-2 col-xs-10 marbot_10"><asp:DropDownList CssClass="form-control" ID="ddlAgent" runat="server" Style="display: block;" AutoPostBack="true" 
                                OnSelectedIndexChanged="ddlAgent_OnSelectedIndexChanged">
                                </asp:DropDownList> 
                                <div id="errMess" runat="server" class="error_module" style="display: none;">
                                    <div id="errorMessage" runat="server" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                                    </div>
                                </div>
                                
                                
                                </div>
                                
                                
    <div class="col-md-6 col-xs-12"> <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ForeColor="Green"></asp:Label> <asp:Button ID="btnUpdate" CssClass="btn but_b" runat="server" Text="Update" OnClick="btnUpdate_OnClick"
                        OnClientClick="return validation();" />
                        
                        
                        
                  <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn but_b"  OnClick="btnClear_OnClick"/>      
                        </div>



    <div class="clearfix"></div>
    </div>





<div> 

<table cellpadding="0" width="100%" cellspacing="0" border="0">
       
            <tr>
                <td width="160px">
                  <div class=" table-responsive"> 
                    <asp:CheckBoxList ID="chkProduct" runat="server" style="width: 100%;" CssClass="chkBoxList"
                        RepeatDirection="Horizontal" AutoPostBack="true"  OnSelectedIndexChanged="chkProduct_OnSelectedIndexChanged" >
                    </asp:CheckBoxList>
                    </div>
                    <%--AutoPostBack="true" OnSelectedIndexChanged="chkProduct_OnSelectedIndexChanged"  onclick="javascript:agentValidate()""--%>
                </td>
            </tr>
            
            <tr> 
            <td> 
            <div class=" table-responsive"> 
            <table class="tblmarkup" id="tblMarkup" runat="server" style="width: 100%; height: 250px;">
                    </table>
            
            </td>
              </tr>
               </table>

            </div>
          
            
            
            
                       

       




</div>

 
 
 <div> 
 
 <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click">First</asp:LinkButton>
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" />
                            <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" />
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click">Last</asp:LinkButton>
 
 </div>       
        
        
    </div>
    
    
    
    
    <div> 
    
    
    
      <div id="Div1"  runat="server"  class="bg_white margin-top-10 table-responsive" style="width:100%; overflow:auto; color: #000000;">
            <asp:DataList ID="dlMarkup" runat="server" AutoGenerateColumns="false" GridLines="Both"
                BorderWidth="0" CellPadding="0" CellSpacing="0" Width="100%" ForeColor="" ShowHeader="true"
                DataKeyField="MRId">
                <HeaderStyle BackColor="#333333" Font-Bold="true" ForeColor="White" HorizontalAlign="Left" />
                <HeaderTemplate>
                    <table width="100%" class="tblpax">
                        <tr height="20px">
                            <td width="140px" class="heading">
                                Product Type
                            </td>
                            <td width="140px" class="heading">
                                Agent Name
                            </td>
                             <td width="140px" class="heading">
                                Source
                            </td>
                             <td width="60px" class="heading">
                                Flight
                            </td>
                            <td width="60px" class="heading">
                                Journey
                            </td>
                            <td width="60px" class="heading">
                                Carrier
                            </td>
                             <td width="80px" class="heading">
                                AgentMarkup
                            </td>
                             <td width="80px" class="heading">
                                Our Commission
                            </td>
                            <td width="80px" class="heading">
                                Markup
                            </td>
                            
                            <td width="100px" class="heading">
                                MarkupType
                            </td>
                            <td width="80px" class="heading">
                                Discount
                            </td>
                            <td width="100px" class="heading">
                                DiscountType
                            </td>
                             <%--<td  width="100px" class="heading">Handling Fee Type
                                </td>
                                <td  width="100px" class="heading">Handling Fee
                                </td>--%>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table  width="100%" class="tblpax">
                        <tr height="20px">
                            <td width="140px">
                                <%#Eval("productType")%>
                            </td>
                            <td width="140px" align="left">
                                <%#Eval("agent_name")%>
                            </td>
                             <td width="140px" align="left">
                                <%#Eval("SourceId")%>
                            </td>
                             <td width="80px" align="left" style="text-align:left">
                                <%#Eval("FlightType")%>
                            </td>
                            <td width="80px" align="left" style="text-align:left">
                                <%#Eval("JourneyType")%>
                            </td>
                            <td width="80px" align="left" style="text-align:left">
                                <%#Eval("CarrierType")%>
                            </td>
                            <td width="80px" align="left">
                                <%#Eval("AgentMarkup")%>
                            </td>
                            <td width="80px" align="left">
                                <%#Eval("OurCommission")%>
                            </td>
                            <td width="80px" align="left">
                                <%#Eval("Markup")%>
                            </td>
                            <td width="100px" align="left">
                                <%#Eval("MarkupTypeName")%>
                            </td>
                            <td width="80px" align="left">
                                <%#Eval("Discount")%>
                            </td>
                            <td width="100px" align="left">
                                <%#Eval("DiscountTypeName")%>
                            </td>
                            <%-- <td style="text-align: left; width: 60px">
                                    <%#Eval("HandlingFeeType")%>
                                </td>
                                <td style="text-align: left; width: 60px">
                                    <%#Eval("HandlingFeeValue")%>
                                </td>--%>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:DataList>
            
            
      <asp:Label ID="lblMessage" runat="server"></asp:Label>
      </div>
    
    </div>
    
    
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

