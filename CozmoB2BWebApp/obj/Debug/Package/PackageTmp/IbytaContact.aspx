﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IbytaContact.aspx.cs" Inherits="IbytaContact" %>

	<!DOCTYPE html>
	<html>

	<head runat="server">
		<title>Ibyta - Contact Us </title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.ico">	
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<!-- manual css -->

		<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
		<link href="build/css/main.min.css" rel="stylesheet" type="text/css" /> 
        <link href="css/override.css" rel="stylesheet">     
        
		<%--<script type="text/javascript" src="Scripts/Menu/query2.js"></script>--%>
     
    <%--jQuery 2.2.2 + Migrate 1.4.1 - Dec2020 Upgrade--%>

     <script src="build/js/jquery-2.2.4.min.js" type="text/javascript"></script>        
     <script type="text/javascript" src="build/js/jquery-migrate-1.4.1.min.js"></script>
       <script>
           jQuery.ajaxPrefilter(function(s){
            if(s.crossDomain){
                s.contents.script = false;
             }
           })
     </script>

	</head>
<body>
     <div id="unsupported-message">
	        <a id="unsupported-overlay" href="#unsupported-modal"></a>
	        <div id="unsupported-modal" role="alertdialog" aria-labelledby="unsupported-title">
		        <h2 id="unsupported-title">⚠ Unsupported Browser ⚠</h2>
		        <p>Travtrolley probably won't work great in Internet Explorer. We generally only support the recent versions of major browsers like <a target="_blank" href="https://www.google.com/chrome/">Chrome</a>, <a target="_blank" href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a>, <a target="_blank" href="https://support.apple.com/downloads/safari">Safari.</a>!</p>

		        <p>If you think you're seeing this message in error, <a href="mailto:tech@cozmotravel.com">email us at tech@cozmotravel.com</a> and let us know your browser and version.</p>
	        </div>
	        <a id="unsupported-banner">
	        <strong> ⚠ Unsupported Browser ⚠ </strong>
		        Travtrolley may not work properly in this browser.
	        </a>
          </div>
         <script>
            // Get IE or Edge browser version
            function detectIE() {
                var ua = window.navigator.userAgent,
                    msie = ua.indexOf('MSIE '),
                    trident = ua.indexOf('Trident/'),
                    edge = ua.indexOf('Edge/');              
                 if (msie > 0 || trident > 0 ) {
                    $('#unsupported-message').css('display', 'block');
                    $('body').css('overflow', 'hidden');
                }
            }
            detectIE();         

        </script>
	    <style>
	            #unsupported-message {
                     display: none;
	            }
		        #unsupported-overlay,
		            #unsupported-modal {
		              display: block;
		              position: absolute;
		              position: fixed;
		              left: 0;
		              right: 0;
		              z-index: 9999;
		              background: #000;
		              color: #D5D7DE;
		            }
		
		            #unsupported-overlay {
		              top: 0;
		              bottom: 0;
		            }
		
		            #unsupported-modal {
		              top: 80px;
		              margin: auto;
		              width: 90%;
		              max-width: 520px;
		              max-height: 90%;
		              padding: 40px 20px;
		              box-shadow: 0 10px 30px #000;
		              text-align: center;
		              overflow: hidden;
		              overflow-y: auto;
		              border:solid 2px #ccc8b9;
		            }
		
		            #unsupported-message :last-child { margin-bottom: 0; }
		
		            #unsupported-message h2 {
		              font-size: 34px;
		              color: #FFF;
                      margin-bottom: 20px;
		            }
		
		            #unsupported-message h3 {
		              font-size: 20px;
		              color: #FFF;
		            }
		
		            body.hide-unsupport { padding-top: 24px; }
		            body.hide-unsupport #unsupported-message { visibility: hidden; }
		
		            #unsupported-banner {
		              display: block;
		              position: absolute;
		              top: 0;
		              left: 0;
		              right: 0;
		              background:#ccc8b9;
		              color: #000;
		              font-size: 14px;
		              padding: 2px 5px;
		              line-height: 1.5;
		              text-align: center;
		              visibility: visible;
		              z-index: 100000;
		            }
		
		
		
		            @media (max-width: 800px), (max-height: 500px) {
		              #unsupported-message .modal p {
		                /* font-size: 12px; */
		                line-height: 1.2;
		                margin-bottom: 1em;
		              }
		
		              #unsupported-modal {
		                position: absolute;
		                top: 20px;
		              }
		              #unsupported-message h1 { font-size: 22px; }
		              body.hide-unsupport { padding-top: 0px; }
		              #unsupported-banner { position: static; }
		              #unsupported-banner strong,
		              #unsupported-banner u { display: block; }
		            }
                    html,body{
                        min-height: 100%;
                        background-color: #ebf1fb;
                    }
                    body{
                        display:flex;
                        flex-direction:column;
                    }                     
                    .ibyta--top-header {
	                    background-color:#ec0b43;
	                    padding:5px 0 5px 0
                    }
                    .ibyta-footer {
                        bottom: 0;
                        left: 0;
                        right: 0;
                        z-index: 1000;
                        position: relative;
                        flex: 1;
                        display: flex;
                        flex-direction: column;
                        justify-content: flex-end;
                    }
                    .ibyta-footer .copyright {
	                    color:#fff;
	                    text-align:center;
	                    background-color:#000;
	                    padding:4px 0;
                        font-size: 12px;
                    }
                    .ibyta-footer .contact-info{
                        text-align: center;
                        padding-bottom: 5px;
                        color: #ec0b43;
                        background-color: #f5f4f4;
                        padding: 6px 0;
                    }
                    .ibyta-footer .contact-info a{
                        color: #ec0b43;
                    }
                    .ibyta-footer .contact-info em{
                        color:#383838;
                        font-style:normal;
                    }
                    .services-list{
                        text-align: center;
                        padding: 10px 2px;
                          background: #fff;
                          box-shadow: 0 0 5px -2px rgba(0, 0, 0, 0.32);
                    }
                    .services-list li{
                        color: #696969;
                        font-size: 13px;
                        font-weight: 600;
                        padding: 0 20px;
                    }
                    .services-list li:before{
                        content:"";
                        background:url(build/img/ibyta-services.svg) no-repeat;
                        background-position: -124px -60px;
                        width: 38px;
                        height: 30px;
                        display: block;
                        margin:0 auto 8px auto;
                        opacity:.1;
                    }
                    .services-list li:nth-child(2):before{
                        background-position:0 -122px;
                    }
                    .services-list li:nth-child(3):before{
                        background-position:6px -60px;
                    }
                    .services-list li:nth-child(4):before{
                        background-position:-63px -60px;
                    }
                    .services-list li:nth-child(5):before{
                        background-position:9px 0px;
                    }
                    .services-list li:nth-child(6):before{
                        background-position:-187px -60px;
                    }
                    @media(min-width:768px){.ibyta-footer{}}

	        </style>
    
    	<div class="ibyta--top-header">
			<div class="cz-container" style="box-shadow: none;">
				<div class="row px-5 px-lg-0">
					<div class="col-xs-6">
						<a href="ibytalogin.aspx"><img src="images/ibyta-logo.jpg" class="img-responsive" style="max-width: 130px;margin-left: -15px;"></a>
                        <p class="text-white"><small>An AirArabia Group Company</small> </p>
					</div>
					<div class="col-xs-6">
						<a href="Register.aspx" class="float-right mt-3" style="color:#fff;font-size:18px;"><span class="icon-user2"></span> Register</a>
					</div>
				</div>
			</div>
		</div>
        <div class="cz-container" >
            <div class="body_container" style="min-height:77vh">
                <h2>Contact Us</h2>
                <div class="clearfix"></div>

                <div class="pt-5">
                    <div class="row mt-5">
                        <div class="col-md">
                            <strong>Locate Us</strong>
                            <p class="mt-3">
                                Floor No.36<br />
                                Latifa Tower B,<br />
                                Shk. Zayed Road,<br /> 
                                Dubai, UAE
                            </p>
                        </div>
                        <div class="col-md-9">                           
                             <ul class="list-unstyled px-3">
                                 <li><strong>Call us:</strong></li>
                                 <li class=" mt-3"><span class="text-primary">+97144065891</span> | <span class="text-primary">+9714 4065892</span></li>
                                 <li class="mt-5"><strong>Email us:</strong></li>
                                 <li class="text-primary mt-3"><a href="mailto:sales@ibyta.com">sales@ibyta.com</a></li>
                             </ul>
                        </div>
                    </div>
              

                </div>

            </div>
        </div>
         <div class="ibyta-footer">
            <div class="services-list">
                <ul class="list-unstyled text-center mb-0">
                    <li class="d-inline-block">Hotels</li>
                    <li class="d-inline-block">Apartments</li>
                    <li class="d-inline-block">Tours</li>
                    <li class="d-inline-block">Transfers</li>
                    <li class="d-inline-block">Insurance</li>
                    <li class="d-inline-block">Visas</li>
                </ul>                       
            </div>  
            <div class="contact-info">
                    Tel : +97144065891 + 9714 4065892 <em> | </em> suppliers : <a href="mailto:contracting@ibyta.com">contracting@ibyta.com</a> <em> | </em> customers : <a href="mailto:sales@ibyta.com">sales@ibyta.com</a> 
                    <div class="small pt-2 text-secondary">
                    <a class="text-secondary" href="IbytaTerms.aspx">Terms and Conditions</a> | <a class="text-secondary" href="IbytaPrivacyPolicy.aspx">Privacy Policy</a> | <a class="text-secondary" href="IbytaContact.aspx">Contact Us</a>
                    </div>
            </div>
            <div class="copyright">
                    Copyright 2019 Ibyta, UAE . All Rights Reserved.
            </div>          
         </div>
</body>
</html>
