﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="FixedDepartureReceipt" Codebehind="FixedDepartureReceipt.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Fixed Departure Invoice</title>
    <script language="javascript" type="text/javascript">
        function printDiv(divName) {
            document.getElementById('print').style.display = 'none';
        var printContents = document.getElementById(divName).innerHTML;
        var originalContents = document.body.innerHTML;

        document.body.innerHTML = printContents;

        window.print();

        document.body.innerHTML = originalContents;
        document.getElementById('print').style.display = 'block';
    }
</script>
</head>
<body>
    <form id="form1" runat="server">
    <div id="printableArea" runat="server">
                <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                    <tbody>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td width="21%" >
                                                <%--<img width="200" height="71" src="<%=Request.Url.Scheme%>://cozmotravel.com/images/packageVoucherlogo.gif">--%>
                                                <asp:Image ID="imgHeaderLogo" runat="server" Width="200px" Height="60px" AlternateText="AgentLogo" ImageUrl="~/images/logo.jpg" />
                                            </td>
                                            <td width="79%" align="right">
                                                <div style="float: right;" id="print">
                                                    <a href="#" onclick="printDiv('<%=printableArea.ClientID %>')">Print &nbsp;
                                                        <img align="absmiddle" style="padding-left: 2;" src="<%=Request.Url.Scheme%>://cozmotravel.com/Images/print_blue.gif" title="Print"
                                                            alt="print"></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="25" bgcolor="#18407B" align="center">
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Fixed Departure Invoice</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse" class="pd15 padd10">
                                    <tbody>
                                        <tr>
                                            <td height="25">
                                                <strong>File No:</strong>
                                                <asp:Label ID="lblFileNo" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Booking Date: </strong>
                                                <asp:Label ID="lblBookingDate" runat="server" Text=""></asp:Label>
                                                    
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <strong>Tour Name:</strong>
                                                <asp:Label ID="lblTourName" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Departure Date: </strong>
                                                <asp:Label ID="lblDepartureDate" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                            <strong>Lead Pax:</strong>
                                                <asp:Label ID="lblLeadPax" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                            <strong>
                                                <asp:Label ID="lblPaxCount" runat="server" Text=""></asp:Label></strong>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <strong>Package Cost:</strong><asp:Label ID="lblPackage" runat="server" Text=""></asp:Label></td>
                                            <td>
                                                
                                                </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <strong>Amount Paid:</strong><asp:Label ID="lblAmountPaid" runat="server" Text=""></asp:Label></td>
                                            <td>
                                                </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <strong>Balance:</strong>
                                                <asp:Label ID="lblBalance" runat="server" Text=""></asp:Label>
                                                </td>
                                            <td>
                                            <strong>Next Payment Date:</strong>
                                                <asp:Label ID="lblNextPaymentDate" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <strong>Remarks:</strong></td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <asp:Label ID="lblFirstPaymentDate" runat="server" Text=""></asp:Label></td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <asp:Label ID="lblSecondPaymentDate" runat="server" Text=""></asp:Label></td>
                                            <td>
                                                &nbsp;</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="25" bgcolor="#18407B">
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Cancellation Policy</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <h3 style="background-color:#18407B;padding-left: 10px; color: #FFFFFF; font-size: 16px;">Partial Payment Policy</h3>
                                       
                                       
                                         <table width="100%" border="1" cellpadding="0" cellspacing="0">
                                        <tr><td width="25%">Booking Deposit </td><td width="75%">AED 500 per person </td></tr>
                                        <tr><td>2nd Payment</td><td>50% of the balance amount (45 days before departure or with visa submission whichever comes first  )</td></tr>
                                        <tr><td>Final Payment</td><td>Full payment (30 days before departure )</td></tr>
                                        <tr><td>Optional tours / Extras</td><td>Full Payment at the time of booking</td></tr>
                                        </table>
                                        
                                        
                                        <h3 style="background-color:#18407B;padding-left: 10px; color: #FFFFFF; font-size: 16px;">Cancellation Policy - Applicable for partial payment </h3>
                                       
                                       <table  width="100%" border="1" cellpadding="0" cellspacing="0">
                                        <tr><td width="25%">Booking Deposit </td><td width="75%">Non Refundable  </td></tr>
                                        <tr><td>44 days -31days to departure</td><td>10% of the tour price paid will be refunded</td></tr>
                                        <tr><td>30 days -15 days to departure</td><td>50% of the tour price paid will be refunded</td></tr>
                                        <tr><td>14 days & less</td><td>Non Refundable </td></tr>
                                        <tr><td>Optional Tours / Extras</td><td>Non Refundable</td></tr>
                                        </table>
                                        
                                        <h3 style="background-color:#18407B;padding-left: 10px; color: #FFFFFF; font-size: 16px;">Cancellation Policy - Applicable for full payment  </h3>
                                      
                                       <table  width="100%" border="1" cellpadding="0" cellspacing="0">
                                        <tr><td width="25%">Booking Deposit </td><td width="75%">Non Refundable  </td></tr>
                                        <tr><td>44 days -31days to departure</td><td>50 % of the tour price paid will be refunded</td></tr>
                                        <tr><td>30 days -15 days to departure</td><td>25 % of the tour price paid  will be refunded </td></tr>
                                        <tr><td>14 days & less</td><td>Non Refundable </td></tr>
                                        <tr><td>Optional Tours / Extras</td><td>Non Refundable</td></tr>
                                        </table>
                                       
                                        <h3 style="display:none">Payment Policy</h3>
                                        <ul style="display:none">
                                        
                                        </ul>
                                        <br />
                                        <h3 style="display:none">Cancellation Policy</h3>
                                     <ul style="display:none">
                                        
                                        </ul>
                                        
                                        On behalf of the persons booked, I/We have read, understood & accepted the booking <a href="<%=Request.Url.Scheme%>://www.cozmotravel.com/FixDepTerms.aspx" target=_blank>Terms & Conditions</a>. I /We being duly authorized by the said person do hereby agree & accept the same for self & on behalf of the said persons.
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        
                      
                        <tr>
                           <td height="25" bgcolor="#18407B">
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Bank Details</label>
                            </td>
                        </tr>
                        <tr>
                        <td>
                        
                        </td>
                        </tr>
                      
                    </tbody>
                </table>
            
    </div>
    </form>
</body>
</html>
