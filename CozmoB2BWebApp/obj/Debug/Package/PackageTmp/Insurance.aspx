﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="Insurance" Title="Insurance" Codebehind="Insurance.aspx.cs" EnableEventValidation="false"%>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    
    
    <style>
         .disabled-content {
          pointer-events: none;
          position: relative;
        }
        .disabled-content:before {
          content: "";
          width: 100%;
          height: 100%;
          position: absolute;
          top: 0;
          left: 0;
          z-index: 10001;
          background-color: rgba(255, 255, 255, 0.5);
        }

    </style>
    <link rel="stylesheet" href="css/style.css">
<script type="text/javascript">
    var call1;
    var call2;
    //-Flight Calender control
    function init1() {
        var today = new Date();
        // Rendering Cal1
        call1 = new YAHOO.widget.CalendarGroup("call1", "fcontainer1");
        call1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
//        call1.cfg.setProperty("title", "Select your desired departure date:");
        call1.cfg.setProperty("close", true);
        call1.selectEvent.subscribe(setFlightDate1);
        call1.render();
        // Rendering Cal2
        call2 = new YAHOO.widget.CalendarGroup("call2", "fcontainer2");
//        call2.cfg.setProperty("title", "Select your desired return date:");
        call2.selectEvent.subscribe(setFlightDate2);
        call2.cfg.setProperty("close", true);
        call2.render();
    }

    function showFlightCalendar1() {
        call2.hide();
        document.getElementById('fcontainer1').style.display = "block";
        document.getElementById('fcontainer2').style.display = "none";
    }

    function showFlightCalendar2() {
        call1.hide();
        // setting Calender2 min date acoording to calendar1 selected date
        var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
        if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            var depDateArray = date1.split('/');
            var arrMinDate = new Date(depDateArray[2], depDateArray[1], depDateArray[0]);
            call2.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate() + 1) + "/" + depDateArray[2]);
            call2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
            call2.render();
        }
        document.getElementById('fcontainer2').style.display = "block";
    }

    function setFlightDate1() {
        var date1 = call1.getSelectedDates()[0];
        this.today = new Date();
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());
        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Date selected should be greater than or equal to today's date ";
            return false;
        }
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errMess').innerHTML = "";

        var month = date1.getMonth() + 1;
        var day = date1.getDate();

        if (month.toString().length == 1) {
            month = "0" + month;
        }
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        document.getElementById('<%=DepDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
        call1.hide();
//when we select date on Deperturedate,returndate calender shold be opened default
        if (document.getElementById('round-trip') != null && document.getElementById('round-trip').checked == true) {
            showFlightCalendar2();
        }  
    }

    function setFlightDate2() {
        var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
        if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "First select departure date.";
            return false;
        }
        var date2 = call2.getSelectedDates()[0];
        var depDateArray = date1.split('/');
        // checking if date1 is valid		    
        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = " Invalid Departure Date";
            return false;
        }
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errMess').innerHTML = "";
        // Note: Date()	for javascript take months from 0 to 11
        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
        var difference = returndate.getTime() - depdate.getTime();
        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Date of return should be greater than or equal to date of departure (" + date1 + ")";
            return false;
        }
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errMess').innerHTML = "";
        var month = date2.getMonth() + 1;
        var day = date2.getDate();
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        document.getElementById('<%=ReturnDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
        call2.hide();
    }

    YAHOO.util.Event.addListener(window, "load", init1);

    function disablefield() {

       

//        if (document.getElementById('Radio_Agent2').checked == true) {
//            document.getElementById('wrapper1').style.display = "block";
//        }
//        else {
//            document.getElementById('wrapper1').style.display = "none";
//        }

            if (document.getElementById('round-trip').checked == 1) {
                document.getElementById('textbox_A3').style.display = "block";
                document.getElementById('swapAirport').style.display = "block";
                document.getElementById('<%=hdnWayType.ClientID %>').value = "return";
                //document.getElementById('returnAirline').style.display = 'block';
            }
            else {
                document.getElementById('textbox_A3').style.display = "none";
                document.getElementById('swapAirport').style.display = "block";
                document.getElementById('<%=hdnWayType.ClientID %>').value = "oneway";
                //document.getElementById('returnAirline').style.display = 'none';
                document.getElementById('fcontainer2').style.display = 'none';
            }
        }

    var arrayStates = new Array();
    function invokePage(url, passData) {
        if (window.XMLHttpRequest) {
            AJAX = new XMLHttpRequest();
        }
        else {
            AJAX = new ActiveXObject("Microsoft.XMLHTTP");
        }
        if (AJAX) {
            AJAX.open("POST", url, false);
            AJAX.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
            AJAX.send(passData);
            return AJAX.responseText;
        }
        else {
            return false;
        }
    }

    function getAirlines(sQuery) {

        var paramList = 'searchKey=' + sQuery;
        paramList += '&requestSource=' + "PreferredAirline";
        var url = "CityAjax";
        var arrayStates = "";
        var faltoo = invokePage(url, paramList);
        arrayStates = faltoo.split('/');
        if (arrayStates[0] != "") {
            for (var i = 0; i < arrayStates.length; i++) {
                arrayStates[i] = [arrayStates[i].split(',')[1], arrayStates[i]];
            }

            return arrayStates;
        }
        else return (false);
    }




   
    
</script>
<script type="text/javascript">
    function validate() {
//        if (Trim(document.getElementById('<%=Origin.ClientID %>').value) == "" || Trim(document.getElementById('<%=Destination.ClientID %>').value) == "") {
//            document.getElementById('errMess').style.display = "block";
//            document.getElementById('errMess').innerHTML = "Please fill both origin and destination";
//            return false;
//        }
        if (document.getElementById('<%=Origin.ClientID %>').value == "") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please fill origin";
            return false;
        }
        if (document.getElementById('<%=Destination.ClientID %>').value == "") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please fill destination";
            return false;
        }
//        if (document.getElementById('destination').value.indexOf('India') > 0 && document.getElementById('origin').value.indexOf('India') > 0) {
//            document.getElementById('isInternational').value = "false";
//        }
//        else {
//            document.getElementById('isInternational').value = "true";
//        }
//        document.getElementById('showInternational').value = "true"

        var starIndex = (document.getElementById('<%=Destination.ClientID %>').value.length - 4);
        var endIndex = (document.getElementById('<%=Destination.ClientID %>').value.length - 1);
        var intDest = document.getElementById('<%=Destination.ClientID %>').value.substring(starIndex, endIndex);
        if (document.getElementById('<%=Origin.ClientID %>').value == document.getElementById('<%=Destination.ClientID %>').value) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Origin and destination should be different";
            return false;
        }
        if (isNaN(document.getElementById('<%=Destination.ClientID %>').value) == false) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Destination should not be numeric or blank";
            return false;
        }
     
        if (!checkInsuranceDates()) {
            return false;
        }
//        if (document.getElementById('<%=txtDepAirline.ClientID %>').value == "") {
//            document.getElementById('errMess').style.display = "block";
//            document.getElementById('errMess').innerHTML = "Departure Airline should not be blank";
//            return false;
//        }
//        if (document.getElementById('round-trip').checked == true && document.getElementById('<%=txtArrAirline.ClientID %>').value == "") {
//            document.getElementById('errMess').style.display = "block";
//            document.getElementById('errMess').innerHTML = "Arrival Airline should not be blank";
//            return false;
//        }

        if (eval(document.getElementById('<%=ddlAdults.ClientID %>').value) < eval(document.getElementById('<%=ddlInfants.ClientID %>').value)) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Infant count should not be greater than adult count";
            return false;
        }

        if (document.getElementById('<%=hdnInsurance.ClientID%>').value == "InsuranceFull" && document.getElementById('<%=hdnCurrency.ClientID%>').value=="") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please update currency for seleceted PseudoCode";
            return false;
        }

        if (  document.getElementById('<%=ddlPolicyFor.ClientID%>').value == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please select the Policy";
            return false;
        }

        document.getElementById('divPrg').style.display = 'block';

        document.getElementById('PreLoader').style.display = "block";
        document.getElementById('insurance_main_inner').style.display = "none";
        document.getElementById('searchFromCity').innerHTML = document.getElementById('<%=Origin.ClientID %>').value;
        document.getElementById('searchToCity').innerHTML = document.getElementById('<%=Destination.ClientID %>').value;
        return true;
    }

    function checkInsuranceDates() {
        document.getElementById('errMess').style.display = "none";
        var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
        var date2 = document.getElementById('<%=ReturnDate.ClientID %>').value;
        var depDateArray = date1.split('/');
        var retDateArray = date2.split('/');
        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
        if (date1 == "DD/MM/YYYY" || date1 == "") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Select Valid Departure Date";
            return false;
        }


        // checking if date1 is valid		    
        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = " Invalid departure date";
            return false;
        }
        //Checking for 3 days booking validation, written by shiva
//        if (depdate.getDate() < (new Date().getDate() + 3) && depdate.getMonth() == (new Date().getMonth()) && depdate.getFullYear() == (new Date().getFullYear())) {
//            document.getElementById('errMess').style.display = "block";
//            document.getElementById('errMess').innerHTML = "Booking not allowed for next 3 days ";
//            return false;
//        }
        //----------------------------End Validation---------------------------
        if (document.getElementById('round-trip').checked == true) {
            if (date2 == "DD/MM/YYYY" || date2 == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Select Valid Return Date";
                return false;
            }


            // checking if date2 is valid	
            if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = " Invalid return date";
                return false;
            }
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Return date should be greater than or equal to departure date";
                return false;
            }
        }
        return true;
    }
</script>
 
<div style=" position:relative" id="insurance_main_inner">
<asp:HiddenField ID="hdnWayType" runat="server" Value="return" />
<asp:HiddenField ID="hdnPolicyFor" runat="server" Value="" />
<asp:HiddenField ID="hdnIsDomestic" runat="server" Value="false" />
<asp:HiddenField ID="hdnInsurance" runat="server" Value="" />
<asp:HiddenField ID="hdnCurrency" runat="server" Value="" />
   <div class="banner-container" style="background-image:url(build/img/main-bg.jpg)">
      <div class="row no-gutter">
          <div class="col-12 col-lg-6">
            <div id="tabbed-menu" class="search-container">
                  <ul class="tabbed-menu nav">  
                    <li>
                        <a  id="lnkFlights"  href="HotelSearch.aspx?source=Flight" <%=Settings.roleFunctionList.Exists(x => x.ToLower().Contains("hotelsearch.aspx?source=flight")) ? " style='display:block'" : " style='display:none'" %> >                   
                          Flight
                        </a> 
                    </li>                           
                    <li>
                        <a id="lnkHotels"  href="HotelSearch.aspx?source=Hotel">
                         Hotel
                        </a>
                    </li>      
                  <%--  <li>
                        <a  id="lnkACtivity" onclick="Javascript:location='ActivityResults.aspx'" href="#classics2" >
                         Activity
                        </a>
                    </li>    
                    <li><a id="lnkPackages" onclick="Javascript:location='packages.aspx'"  href="#featured2">
                        Packages
                        </a>
                    </li>--%>
                     <li>
                         <a onclick="Javascript:location='Insurance.aspx'" id="lnkInsurance" href="Insurance.aspx"  class="current">
                            Insurance
                         </a>
                   </li>                                                              
                 </ul>
                <div class="search-matrix-wrap auto-height p-4 search-page-form">    
                
                <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
        </div>  
                    <div class="custom-radio-table row my-2">
                          <div class="col-md-3 col-4 mt-2">
                              <input name="RadioGroup3" type="radio" id="one-way" value="radio" onclick="disablefield();" />
                               <label for="one-way">One Way</label>
                          </div>
                          <div class="col-md-3 col-4 mt-2">
                               <input name="RadioGroup3" type="radio" id="round-trip" value="radio" checked="checked" onclick="disablefield();" />
                               <label for="round-trip">Round Trip</label>
                          </div>
                      </div>
                     <div class="row no-gutters">
                         <div class="col-12 col-sm">
                             <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-location" aria-label="Origin Airport"></span>
                                 </div>
                                 <asp:TextBox ID="Origin"  CssClass="form-control origin-airport" placeholder="Origin" onblur="CheckDomestic();"  runat="server"></asp:TextBox>
                                
                                      <div style=" width:250px; line-height:30px; color:#000;margin-top:38px" id="statescontainer" onclick="CheckDomestic();"></div>
                                
                                 <div id="multipleCity1" ></div>
                             </div>
                         </div>
                          <div class="col swap-airports-wrap" id="swapAirport">
                              <a href="javascript:void(0);" class="swap-airports" tabindex="-1" title="Switch Airports"> <span class="icon-loop-alt4"></span></a>
                         </div>                           
                         <div class="col-12 col-sm">
                              <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-location" aria-label="Origin Airport"></span>
                                 </div>

                               <asp:TextBox  ID="Destination" CssClass="form-control dest-airport" placeholder="Destination" onblur="CheckDomestic();" runat="server"></asp:TextBox>
                                                <div id="statesshadow2" style="    width: 250px;    position: absolute;    top: 38px">
                                                    <div style=" width:250px; line-height:30px; color:#000" id="statescontainer2" onclick="CheckDomestic();">
                                                         
                                                    </div>
                                                </div>
                                                <div id="multipleCity2">
                                                </div>
                                   </div>
                         </div>
                    </div>
                    <div class="row custom-gutter">
                         <div class="col-12 col-lg-6">
                             
                                        <a class="form-control-holder"  href="javascript:void(null)" onclick="showFlightCalendar1()">
                                            <div class="icon-holder">
                                                <span class="icon-calendar" aria-label=""></span>
                                            </div>  
                                            <asp:TextBox ID="DepDate" CssClass="form-control" placeholder="Departure Date" runat="server"  autocomplete="off" onfocus="showFlightCalendar1()" data-calendar-contentID="#fcontainer1"></asp:TextBox>                                               
                                        </a>
                                    
                         </div>
                         <div class="col-12 col-lg-6" id="textbox_A3">                             
                                        <a class="form-control-holder"  href="javascript:void(null)" onclick="showFlightCalendar2()">
                                            <div class="icon-holder">
                                                <span class="icon-calendar" aria-label=""></span>
                                            </div>       
                                             <asp:TextBox ID="ReturnDate" placeholder="Return Date" CssClass="form-control" runat="server"  autocomplete="off" data-calendar-contentID="#fcontainer2" onfocus="showFlightCalendar2()"></asp:TextBox>                                             
                                        </a>
                         </div>
   </div>
                    <div class="row custom-gutter">
                         <div class="col-12 col-lg-6">      
                              <div class="form-control-holder">
                                    <div class="icon-holder">
                                        <span class="icon-aircraft-take-off" aria-label="Flight No"></span>
                                    </div>
                                  <asp:TextBox ID="txtDepAirline"  CssClass="form-control" runat="server" placeholder="Departure Airline Code"></asp:TextBox>
                            </div>

                         </div>

                          <div class="col-12 col-lg-6">      
                              <div class="form-control-holder">
                                    <div class="icon-holder">
                                        <span class="icon-aircraft-take-off" aria-label="Flight No"></span>
                                    </div>
                                 <asp:TextBox ID="txtArrAirline"  CssClass="form-control" runat="server" placeholder="Arrival Airline Code
"></asp:TextBox> 
                            </div>

                         </div>
                        <div class="col-12 col-lg-6 "> 
                                <div class="form-control-holder">
                                    <div class="icon-holder">
                                        <span class="icon-star" aria-label=""></span>
                                    </div>
                                        <asp:DropDownList ID="ddlPolicyFor" CssClass="form-control" runat="server" onchange="ShowPaxTypes();">
                                        </asp:DropDownList>
                                </div>

                         </div>
                        <div class="col-12 col-lg-6 " > 
                              <div class="custom-dropdown-wrapper disabled-content" id="divPaxTypes">
                                    <a href="jaavscript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#InsPaxSelectDropDown">
                                        <div class="icon-holder">
 	                                         <span class="icon-group"></span>
                                        </div>
                                        <span class="form-control-text" id="InsSelectedTravelerInfo">1 Adult, 0 Child, 0 Infant</span>                      
                                    </a>
                                    
                                    <div class="dropdown-content d-none right-aligned p-4" id="InsPaxSelectDropDown" style="max-width: 400px;" >                                       
                                        <div class="row no-gutters" id="divAdults">
                                                <div class="col-8">
                                                    <span class="pt-2 d-block" id="spanAdult">Adult(0-75 yrs)</span>
                                                   
                                                </div>
                                                <div class="col-4">
                                                <asp:DropDownList ID="ddlAdults" CssClass="form-control no-select2 ins-pax" runat="server">
                                                        <asp:ListItem Selected="True" Text="1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                            
                                                    </asp:DropDownList>
                                                </div>
                                        </div>
                                        <div class="row no-gutters" id="divChild">
                                            <div class="col-8">
                                                <span class="pt-2 d-block" id="spanChild"> Child/Dependent(2-22 yrs)</span>
                                            </div>
                                            <div class="col-4">
                                    <asp:DropDownList ID="ddlChilds" CssClass="form-control no-select2 ins-pax" runat="server">
                                                        <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                            <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                        <asp:ListItem Text="10" Value="10"></asp:ListItem>
                                                        <asp:ListItem Text="11" Value="11"></asp:ListItem>
                                                        <asp:ListItem Text="12" Value="12"></asp:ListItem>
                                                        <asp:ListItem Text="13" Value="13"></asp:ListItem>
                                                        <asp:ListItem Text="14" Value="14"></asp:ListItem>
                                                        <asp:ListItem Text="15" Value="15"></asp:ListItem>
                                                        <asp:ListItem Text="16" Value="16"></asp:ListItem>
                                                        <asp:ListItem Text="17" Value="17"></asp:ListItem>
                                                        <asp:ListItem Text="18" Value="18"></asp:ListItem>
                                                        <asp:ListItem Text="19" Value="19"></asp:ListItem>
                                                        <asp:ListItem Text="20" Value="20"></asp:ListItem>
                                                    </asp:DropDownList>
                                            </div>
                                        </div>
                                        <div class="row no-gutters" id="divInfant">
                                            <div class="col-8">
                                                <span class="pt-2 d-block" id="spanInfant"> Infant(30 days - 2 yrs)</span>
                                            </div>
                                            <div class="col-4">
                                                <asp:DropDownList ID="ddlInfants" CssClass="form-control no-select2 ins-pax" runat="server">
                                                        <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                    </asp:DropDownList>
                                            </div>
                                        </div>
                                     

                                     


                                    </div>
                                  

                              </div>
                         </div>
                        <div class="col-12 col-lg-6 " id="divPsedo" runat="server" visible="false"> 
                                <div class="form-control-holder">
                                    <div class="icon-holder">
                                        <span class="icon-star" aria-label=""></span>
                                    </div>
                                        <asp:DropDownList ID="ddlPseudo" CssClass="form-control" runat="server"  Visible="false" onchange="GetChannelCurrency();">
                                        </asp:DropDownList>
                                </div>

                         </div>


                </div>
    <div class="row mt-3">             
                                          <div class="col-xs-12 py-3">
                                               <asp:Button CssClass="but but_b pull-right" ID="btnGetQuote" runat="server" Text="Get Quote"
                                        OnClick="btnGetQuote_Click" OnClientClick="return validate();" />

                                          </div>
                                        </div>




            </div>
         </div>
       </div>
    </div>

    <div class="statescontainer4" id="statescontainer4"  style="position:absolute; left:283px;  top:369px; width:200px; display:none; z-index: 300;" ></div> 
    <div id="CitySearchPop" class="light-grey-bg border-y padding-bottom-10 search-popup-parent"
        style="display: none; width: 410px; position: absolute; left: 500px; top: 110px;
        z-index: 100">
        <div id="LoadingCountryList" style="display: none; font-weight: bolder; margin-left: 105px;
            padding-top: 10px">
            Loading
            <img style="width: 80px" src="../images/ajax-loader2.gif" /></div>
        <div id="SelectCountryDropDown" class="fleft padding-5 search-popup-child" style="width: 400px;
            display: none">
        </div>
        <div id="AirportList" class="fleft padding-5" style="height: 15px; display: none">
        </div>
    </div>
    <input type="hidden" id="OfferedFareHidden" value="" />
    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
    </iframe>

        <div class="clear" style="margin-left: 25px">
            <div id="container1" style="position: absolute; top:287px; left: 168px; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="container2" style="position: absolute; top:287px; left: 326px; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="fcontainer1" style="position: absolute; top: 70px; left: 30%; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="fcontainer2" style="position: absolute; top: 70px; left: 50%; display: none;">
            </div>
        </div>
        
        
        
        
        
        </div>
 <div id="">
        <div class="search_container"> 
     
    
        
        
      
     

        

 
  
    
        
        
        
        
        <div style=" padding-top:10px; display:none">
        
               
        <%--<div class="insurance-right-pan">
            <div class="insurance-slide">
                <img src="images/insurance.jpg" /></div>
            <div class="insurance-links">
                <div class="ns-h3">
                    Cozmo Travel Insurance</div>
                <div style="min-height: 122px" class="inwrap">
                 Instead of being occupied with the what ifs, get TUNE PROTECT Travel Insurance by Cozmo Travel when you travel domestically or internationally. 
                 From trip cancellation to loss baggage, delays and personal accidents, we've got you covered. So leave your worries behind and let us deal with them instead.
                </div>
                <div class="clear">
                </div>
            </div>
            <div class="clear">
            </div>
        </div>--%>
        <div class="clear">
        </div>
    </div>
         <div class="clear"></div>
        </div>
        
        
        
        
        
        <div class="row my-3"> 
        
     
     
    <div class="col-md-3"> 
                <div class="inn_wrap01">
             
             <div class="ns-h3">  Information</div>
                    
                <div class="insu_link"> 
                
                       <li><a href="<%=Request.Url.Scheme %>://www.tune2protect.com/cozmotravel/ae/en/TravelIns_Intro.html" target="_blank">Travel Insurance Plans   </a></li>
                            <li><a href="<%=Request.Url.Scheme %>://www.tune2protect.com/cozmotravel/ae/en/FAQ.html" target="_blank">FAQ</a></li>
                            <li><a href="<%=Request.Url.Scheme %>://www.tune2protect.com/cozmotravel/ae/en/MakeAClaim.html" target="_blank">Claims</a></li>
                            <li><a href="<%=Request.Url.Scheme %>://www.tune2protect.com/cozmotravel/ae/en/index.html" target="_blank">Terms &amp; Conditions</a></li>
                        </ul>
                
                </div>    
   
                 
                 </div>
                 
                 </div>   
        
        
        
        
        <div class="col-md-9  hidden-xs"> 
                <div class="inn_wrap01">
             
             <div class="ns-h3">  Travel Insurance</div>
                    
                <div style=" min-height:144px" class=" pad_10"> Instead of being occupied with the what ifs, get TUNE PROTECT Travel Insurance , when you travel domestically or internationally. From trip cancellation to loss baggage, delays and personal accidents, we've got you covered. So leave your worries behind and let us deal with them instead. </div>    
                        
                 
                 </div>
                 
                 </div>           
                        
     
     
     
     
     
        
        </div> 
        
    
    <div class="clear"></div>
    </div>
     <div id="PreLoader" style="display: none">
        <div class="loadingDiv">
            <div>
                <img src="images/preloader11.gif" /></div>
            <div style="font-size: 16px; color: #999999">
                <strong>Awaiting For Insurance Search Results</strong>
            </div>
            <div style="font-size: 21px; color: #3060a0">
                <strong>From City:<span id="searchFromCity">
                  </span> </strong>
                <br />
                <strong>To City:<span id="searchToCity">
                  </span> </strong>
            </div>
        </div>
    </div> 

    <script>
        //Insurance Pax Count
        function InsPaxCount() {
            var adt = $('#ctl00_cphTransaction_ddlAdults').val(),
                       chd = $('#ctl00_cphTransaction_ddlChilds').val(),
                       inf = $('#ctl00_cphTransaction_ddlInfants').val();

            var InsTotalPax = '<strong>';
            InsTotalPax += adt + '</strong> ';
            InsTotalPax += 'Adult';
            if (chd > 0) {
                InsTotalPax += ' , <strong>' + chd + '</strong> Child';
            }
            if (inf > 0) {
                InsTotalPax += ' , <strong>' + inf + '</strong> Infant';
            }
            $('#InsSelectedTravelerInfo').html(InsTotalPax);

            //$('#SelectedTravelerInfo').text(adt + ' Adult ,' + chd + ' Child ,' + inf + ' Infant');
        }
        $('.ins-pax').change(function () {
            InsPaxCount()
        })
       
        function ShowPaxTypes() { //based on the login agent currency code  we are displaying policy for dropdown list. 
            var val = document.getElementById('<%=ddlPolicyFor.ClientID%>').value;
            var currency;
            if (document.getElementById('<%=hdnCurrency.ClientID%>').value != '')
                currency = document.getElementById('<%=hdnCurrency.ClientID%>').value;
            else
                currency = '<%=currency%>'; 
            $('#divPaxTypes').removeClass('disabled-content');
            if (val == 'SeniorCitizen') {
                document.getElementById('<%=hdnPolicyFor.ClientID%>').value = "SeniorCitizen";
                var span = document.getElementById("spanAdult");
                if (currency == "INR" ) {
                    span.textContent = 'Senior Citizen (61 to 80 years)';
                    span.innerHTML = 'Senior Citizen (61 to 80 years)';
                }
                else
                {
                    span.textContent = 'Senior Citizen (above 75 yrs)';
                    span.innerHTML = 'Senior Citizen (above 75 yrs)';
                }
                document.getElementById('spanAdult').style.display = 'none';
                document.getElementById('divChild').style.display = 'none';
                document.getElementById('divInfant').style.display = 'none';
                var spanTraveler = document.getElementById("InsSelectedTravelerInfo");
                spanTraveler.textContent = '1 Adult';
                spanTraveler.innerHTML='1 Adult';
            }
            else {
                document.getElementById('<%=hdnPolicyFor.ClientID%>').value = "Adult";
                document.getElementById('divChild').style.display = 'flex';
                document.getElementById('divInfant').style.display = 'flex';
                var span = document.getElementById("spanAdult");
                var spanChild = document.getElementById("spanChild");
                var spanInfant = document.getElementById("spanInfant");
                
                if (currency == "INR" && document.getElementById('<%=hdnIsDomestic.ClientID%>').value == "false")
                {
                    span.textContent = 'Adult (up to 60 yrs)';
                    span.innerHTML = 'Adult (up to 60 yrs)';

                    spanChild.textContent = 'Child (2-12 years)';
                    spanChild.innerHTML = 'Child (2-12 years)';

                    spanInfant.textContent = 'Infant (3 months-2 years)';
                    spanInfant.innerHTML = 'Infant (3 months-2 years)';
                }
                else if (currency == "INR" && document.getElementById('<%=hdnIsDomestic.ClientID%>').value == "true")
                {
                    span.textContent = 'Adult (up to 70 years)';
                    span.innerHTML = 'Adult (up to 70 years)';

                    spanChild.textContent = 'Child (2-12 years)';
                    spanChild.innerHTML = 'Child (2-12 years)';

                    spanInfant.textContent = 'Infant (6 months-2 years)';
                    spanInfant.innerHTML = 'Infant (6 months-2 years)';
                }
                else
                {
                   span.textContent = 'Adult (0-75 yrs)';
                    span.innerHTML = 'Adult (0-75 yrs)'; 

                    spanChild.textContent = 'Child/Dependent(2-22 yrs)';
                    spanChild.innerHTML = 'Child/Dependent(2-22 yrs)';

                    spanInfant.textContent = 'Infant(30 days - 2 yrs)';
                    spanInfant.innerHTML = 'Infant(30 days - 2 yrs)';
                }
               
                var spanTraveler = document.getElementById("InsSelectedTravelerInfo");
                spanTraveler.textContent = '1 Adult, 0 Child, 0 Infant';
                spanTraveler.innerHTML='1 Adult, 0 Child, 0 Infant';
            }
            document.getElementById('<%=ddlAdults.ClientID%>').value = 1;
            document.getElementById('<%=ddlChilds.ClientID%>').value = 0;
            document.getElementById('<%=ddlInfants.ClientID%>').value = 0;
            
        }

        function GetChannelCurrency()
        {
            var sdata = JSON.parse('<%=data%>');
            for (var c = 0; c < sdata.length; c++)
            {
                if (sdata[c].HAP == document.getElementById('ctl00_cphTransaction_ddlPseudo').value)
                    document.getElementById('<%=hdnCurrency.ClientID%>').value = sdata[c].CurrencyCode;
                
            }
            CheckDomestic();
        }

        //Added for To check the domestic search or not based on that displaying policy for options.
        function CheckDomestic()
        {
            var currency;
            if (document.getElementById('<%=hdnCurrency.ClientID%>').value != '')
                currency = document.getElementById('<%=hdnCurrency.ClientID%>').value;
            else
               currency= '<%=currency%>'; 
            var origin = document.getElementById('<%=Origin.ClientID %>').value;
            var destination = document.getElementById('<%=Destination.ClientID %>').value;
            document.getElementById('<%=ddlPolicyFor.ClientID%>').value = 0;
            if (origin != "" && destination != "" && origin.length > 3 && destination.length > 3) {
                origin = origin.split(',')[1].split('-')[0];
                destination = destination.split(',')[1].split('-')[0];
                $.ajax({
                    url: "Insurance.aspx/IsDomesticSearch",
                    type: 'POST',
                    dataType: 'json',
                    data: "{'origin':'" + origin + "','destination':'" + destination + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {
                        if (data.d && currency == "INR") { //Domestic Travel and Country is India
                            document.getElementById('<%=hdnIsDomestic.ClientID%>').value = "true";
                            document.getElementById('<%=ddlPolicyFor.ClientID%>').options.length = 0;
                            AddItem("--Select Policy For--", "0");
                            AddItem("Adults (up to 70 years)", "Adult");
                            $('#divPaxTypes').addClass('disabled-content');
                        }
                        else if (!data.d && currency == "INR") { //International Travel But Country is IND
                            document.getElementById('<%=hdnIsDomestic.ClientID%>').value = "false";
                            document.getElementById('<%=ddlPolicyFor.ClientID%>').options.length = 0;
                            AddItem("--Select Policy For--", "0");
                            AddItem("Adults (up to 60 years)", "Adult");
                            AddItem("Senior Citizens(above 60 years)", "SeniorCitizen");
                            $('#divPaxTypes').addClass('disabled-content');
                        }
                        else {
                            document.getElementById('<%=hdnIsDomestic.ClientID%>').value = "false";
                            document.getElementById('<%=ddlPolicyFor.ClientID%>').options.length = 0;
                            AddItem("--Select Policy For--", "0");
                            AddItem("Adults (up to 75 years)", "Adult");
                            AddItem("Senior Citizens(above 75 years)", "SeniorCitizen");
                            $('#divPaxTypes').addClass('disabled-content');

                        }
                        
                        $('#<%=ddlPolicyFor.ClientID%>').select2('val', 0);
                    },
                    error: function (data) {
                        alert(data.d);
                    }
                });

            }
        }

        function AddItem(Text,Value)
        {
            var opt = document.createElement("option");
            document.getElementById('<%=ddlPolicyFor.ClientID%>').options.add(opt);
            opt.text = Text;
            opt.value = Value;  
        }
        
    </script>
    <script>
        $(document).ready(function () {
            document.getElementById('<%=ddlPolicyFor.ClientID%>').options.length = 0;
            AddItem("--Select Policy For--", "0");
            $('#<%=ddlPolicyFor.ClientID%>').select2('val', 0);
            

        });
    </script>



    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>

