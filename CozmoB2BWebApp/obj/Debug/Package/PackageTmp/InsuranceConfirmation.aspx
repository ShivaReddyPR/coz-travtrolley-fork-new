﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" EnableEventValidation="false" Inherits="InsuranceConfirmation" Title="Insurance Confirmation" Codebehind="InsuranceConfirmation.aspx.cs" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.Core" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
 <link href="css/steps.css" rel="stylesheet" type="text/css" />
    <%try
        { %>
<script type="text/javascript">
    function checkTerms() {
        if (!document.getElementById('rules').checked) {
            document.getElementById('checkRules').style.display = 'block';
            document.getElementById('MainDiv').style.display = "block";
            document.getElementById('PreLoader').style.display = "none";
            return false;
        }
        else {
            document.getElementById('checkRules').style.display = 'none';
            <%if (!Settings.LoginInfo.AgentBlock) //checking Agent able to book or not Added By brahmam
    {%>
            document.getElementById('MainDiv').style.display = "none";
            document.getElementById('PreLoader').style.display = "block";
            return true;
            <%}
    else
    {%>
            alert('Please contact Administrator'); //We are Showing Alert Message
            return false;
            <%}%>
        }
    }

    
        
    
</script>
   
     
     <div id="MainDiv" >
         <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
             <asp:View ID="ConfirmView" runat="server">
            
<div class="wizard-steps" >
 <div  class="completed-step"><a href="#step-one"><span>1</span>Select Policy</a></div>
  <div  class="completed-step"><a href="#step-two"><span>2</span> PAX Details</a></div>
  <div   class="active-step" ><a href="#"><span>3</span> Secure Payments</a></div>
  <div><a href="#"><span>4</span> Confirmation</a></div>
    <div class="clear"></div>
</div>



<h3> Insurance  Confirmation Plans</h3>
   <div  class="bg_white">  
   
<table class="table table-bordered">
<%if (header != null)
  { %>
  <tr>
    <td height="24"><b><asp:Label ID="lblinsPlan" runat="server" Text="Insurance Plan"></asp:Label></b></td>
    <td height="24"><b><asp:Label ID="Label3" runat="server" Text="Description"></asp:Label></b></td>  
    
    <td height="24"><b><asp:Label ID="Label1" runat="server" Text="Amount"></asp:Label></b></td>
    
  </tr>

 
  <%if (header.InsPlans != null && header.InsPlans.Count > 0)
    { %>
    <%for(int m = 0; m < header.InsPlans.Count; m++)
      { %>
  <tr>
    
    
    <td><label><%=header.InsPlans[m].PlanTitle %></label></td>
    <td><label><%=header.InsPlans[m].PlanDescription.Replace("<>","") %></label></td>
    
    
    <td> <strong><%=Settings.LoginInfo.Currency%> <%=Math.Round(header.InsPlans[m].NetAmount + header.InsPlans[m].InputVATAmount + header.InsPlans[m].Markup - header.InsPlans[m].Discount, Settings.LoginInfo.DecimalValue)%></strong> </td>
  </tr>
  <%} %>
  <%} %>
  <%} %>
 <tr style=" display:none">
    <td height="24"><asp:Label ID="Label5" runat="server" Text="No Of Passengers :"></asp:Label></td>
    <td><asp:Label ID="lblPaxCount" runat="server" Text=""></asp:Label></td>
  </tr>
  
   <tr style=" display:none">
    <td height="24"><asp:Label ID="Label2" runat="server" Text="No Of Qualified Passengers : "></asp:Label></td>
    <td><asp:Label ID="lblQualPax" runat="server" Text=""></asp:Label></td>
  </tr>
   <%if(header.OutputVATAmount >0){ %> 
   <tr >
    <td height="24"><asp:Label ID="lblVAT" runat="server" Text="VAT"></asp:Label></td><td></td>
    <td><asp:Label ID="lblVATamount" runat="server" Text="" Style="font-weight:bold"></asp:Label></td>
  </tr>
     <%} %>
  
  <tr>
    <%if ((!string.IsNullOrEmpty(InsRequest.PseudoCode) && InsRequest.CurrencyCode=="INR") || (string.IsNullOrEmpty(InsRequest.PseudoCode) && Settings.LoginInfo.Currency == "INR"))
    {%>
    <td height="24"><b><asp:Label ID="Label4" runat="server" Text="Grand Total (inclusive of 18% GST):"></asp:Label></b></td>
    <%}
    else
    {%>
    <td height="24"><b><asp:Label ID="Label9" runat="server" Text="Grand Total:"></asp:Label></b></td>
    <%}%>
    <td> </td>
    <td><asp:Label ID="lblAmount" runat="server" Font-Bold="true" Text=""></asp:Label></td>
  </tr>
</table>
   </div>
<table width="100%" border="0" cellspacing="0" cellpadding="0">
  
    <tr>
    <td width="50%" valign="top"></td>
    <td width="50%" valign="top"></td>
  </tr>
  <tr>
    
    
    
    
    <td colspan="2">
    
    
    <div class="col-md-12 padding-0"> 
    
    
    <div class="col-md-6 pad_left0 pad_xs0"> 
    
             <div class="ns-h3">
                        Passenger Details
                      <a class="fcol_fff" style=" position:absolute; right:30px" href="InsurancePax.aspx"> Edit Pax</a>
                    </div>
    
   
   <div  class="bg_white" style=" padding:10px; border: solid 1px #ccc;  max-height:180px;min-height:180px;overflow-y:scroll" >
   
   
    <asp:DataList ID="dlPaxList" runat="server" Width="100%">
                        <ItemTemplate>
                            <div>
                             
                                <b>
                                    <%#Eval("FirstName") %>  <%#Eval("LastName") %></b>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td height="20">
                                            Phone :                                        </td>
                                        <td>
                                            <%#Eval("PhoneNumber") %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">
                                            Address :                                        </td>
                                        <td>
                                            <%#Eval("City") %>,
                                            <asp:Label ID="lblCountry" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td height="20">
                                            E-mail:                                        </td>
                                        <td>
                                            <%#Eval("Email") %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3" class="b_bot">
                                        </td>
                                    </tr>
                                </table>
                                <span class="b_bot"></span>  <br />
                            </div>
                        </ItemTemplate>
                    </asp:DataList>
   </div>
    
    </div>
    
     <div class="col-md-6 padding-0"> 
    <div class="ns-h3" > Terms & Conditions </div>
             
             <div class="bg_white" style=" padding:10px; border: solid 1px #ccc;min-height:180px;">       
                    

<br />
<div>

<input type="checkbox" name="rules" id="rules" />
                       <b>I accept the rules & restrictions.</b><span class="fnt11_gray">(please check the
                        box to continue) </span>
</div>
                        
                        
                    
                    
                    <div id="checkRules"  style="display: none">
                        <div style=" color:Red" id="errorMessage">
                            Please check rules &amp; restrictions box to confirm booking.
                        </div>
                    </div><br />
                    <div >
                        <table  width="100%">
                            <tr class="nA-h4" style=" background:#ccc">
                                <td>
                                    Available Balance :
                                </td>
                                <td  align="right">
                                    <asp:Label ID="lblAgentBalance" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            <tr class="nA-h4">
                                <td>
                                    Amount to be Paid :
                                </td>
                                <td align="right">
                                    <asp:Label ID="lblAmountPaid" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            
                            <tr >
                              
                                <td align="left" colspan="2">
                                    <asp:Label style=" color:Red" ID="lblBalanceEtrror" Font-Bold="true" runat="server" Text=""></asp:Label>
                                </td>
                            </tr>
                            
                        </table>
                    </div>               
    
    </div>
    
    
    </div>
    
    
    <div class="clearfix"> </div>
    
    
    </div>
    
    
    
    
    
    
    </td>
  </tr>
  
  
     <tr>
    <td height="44px">&nbsp;</td>
    <td align="right" valign="bottom"><asp:Button ID="btnPayment" CssClass="btn but_b" runat="server" Text="Confirm" OnClientClick="return checkTerms();" OnClick="btnPayment_Click" /></td>
  </tr>
</table>

 </asp:View>
             
             <asp:View ID="ErrorView" runat="server">
                 <div class="insurance_container">
                     <table>
                         <tr>
                             <td style="height: 200px; text-align: center; width:1500px">
                                 <asp:Label ID="lblError"  runat="server" Text="Your booking might have been confirmed, Please contact Cozmo Travel before trying to book again..."
                                     Font-Size="Larger" ForeColor="Red" ></asp:Label>
                                 </br> </br><u><a href="Insurance.aspx" style="font-size:medium;">Please Search Again</a></u>
                             </td>
                         </tr>
                     </table>
                     <div class="clear">
                     </div>
                 </div>

             </asp:View>
         </asp:MultiView>


</div>
<div id="PreLoader" style="display: none">
        <div class="loadingDiv">
            <div>
                <img src="images/preloader11.gif" /></div>
            <div style="font-size: 16px; color: #999999">
                <strong>Awaiting Insurance Booking confirmation from</strong>
            </div>
            <div style="font-size: 21px; color: #3060a0">
                <strong><span id="searchCity">
                    </span> </strong>
                <br />
                <strong style="font-size: 14px; color: black">do not click the refresh or back button
                    as the transaction may be interrupted or terminated.</strong>
            </div>
        </div>
    </div>
    <%}
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID, "(InsuranceConfirmation.aspx)Error due to :" + ex.ToString(), Request["REMOTE_ADDR"]);
            Response.Redirect("ErrorPage.aspx?Err=" + ex.Message.ToString().Replace("\n", "").Replace("\r", ""), false);
        }%>
     
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
