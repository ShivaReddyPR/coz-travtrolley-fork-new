﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="BaggageInsuranceChangeRequestQueue.aspx.cs" Inherits="CozmoB2BWebApp.BaggageInsuranceChangeRequestQueueGUI" EnableEventValidation="false"%>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<%--<link href="css-holiday/main-style.css" rel="stylesheet" type="text/css" />--%>

<style> .button { background:#1c498a!important }</style>

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
<%--    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />--%>
    <script type="text/javascript" src="Scripts\jsBE\organictabs.jquery.js"></script>
    <script type="text/javascript" src="ash.js"></script>
     <link rel="stylesheet" href="css/style.css">
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
     <link href="css/style.css" rel="stylesheet" type="text/css" /> <!--Added by chandan on  13062016 -->
     
     <link href="css/main-style.css" rel="stylesheet" type="text/css" />

    <div style="padding-top: 10px; position: relative">
        
      
        <div class="clear" style="margin-left: 30px">
            <div id="container1" style="position: absolute; top: 0px; left: 50%; display: none;
                z-index: 9999;">
            </div>
        </div>
      
        <div class="clear" style="margin-left: 30px">
            <div id="container2" style="position: absolute; top: 0px; left: 70%; display: none;
                z-index: 9999;">
            </div>
        </div>
     
     

        <div class="col-md-12 padding-0 margin-bottom-10">
               
             <div class="col-md-2">
                    <asp:Label ID="lblPNRno" Text="PNR No:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:TextBox CssClass="form-control" ID="txtPNRno" runat="server"></asp:TextBox></div>
                <div class="col-md-2">
                    <asp:Label ID="lblCreatedDate" Text="From Date:" runat="server"></asp:Label></div>
                <div class="col-md-2">
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtCreatedDate" runat="server" CssClass="form-control" Width="110px"></asp:TextBox>
                            </td>
                            <td>
                                <a href="javascript:void(null)" onclick="showCalendar1()">
                                    <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div> 
                       <div class="col-md-2">
                    <asp:Label ID="Label1" Text="To Date :" runat="server"></asp:Label></div>
                <div class="col-md-2">
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtToDate" runat="server" CssClass="form-control" Width="110px"></asp:TextBox>
                            </td>
                            <td>
                                <a href="javascript:void(null)" onclick="showCalendar2()">
                                    <img id="Img2" src="images/call-cozmo.png" alt="Pick Date" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div> 
                <div class="clearfix">
                </div>
            </div>
         <div class="col-md-12 padding-0 margin-bottom-10">
              <div class="col-md-2">
                    Agency:</div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlAgency" runat="server" CssClass="inputDdlEnabled form-control"
                         AutoPostBack="true">
                    </asp:DropDownList>
                </div>
             <div class="col-md-2">
                    Policy No:</div>
                <div class="col-md-2">
                   <asp:TextBox ID="txtPolicyNo" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
                <div class="clearfix">
                </div>
         </div>

        <div class="col-md-12 padding-0 margin-bottom-10">
                <label class=" f_R padding-left-5">
                    <asp:Button runat="server" ID="btnSearch" Text="Search"  OnClick="btnSearch_Click"
                        CssClass="btn btn-primary btn_custom"  /></label>
                <label class=" f_R padding-left-5">
                    <asp:Button runat="server" ID="btnClear" Text="Clear"  CssClass="btn btn-primary btn_custom" OnClick="btnClear_Click"/></label>
            </div>
        
           <div style=" padding-bottom:10px"><div style="text-align:right;" <%=pagingEnable %> >
                        
                            <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click" Text="First"  ></asp:LinkButton>
                        
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server"  />
                             <asp:Label ID="lblCurrentPage" runat="server"  ></asp:Label>
                        
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server"   />
                        
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click" Text="Last"></asp:LinkButton>
                    </div>
                    
                    <div class="clear"></div>
                    
                    </div>
                    
             <div>
                 <asp:DataList ID="dlBaggageInsChangeReqQueue" runat="server" CellPadding="4"
                     DataKeyField="BI_ID" Width="100%"  OnItemCommand="dlBaggageInsChangeReqQueue_ItemCommand"  OnItemDataBound="dlBaggageInsChangeReqQueue_ItemDataBound" >
                     <ItemTemplate>
                     <div class=" bor_gray bg_white padding-5 marbot_10" id="Result"> 
                     <asp:HiddenField ID="hdnPlanId" runat="server" />
                     <asp:HiddenField ID="hdnRequestId" runat="server" />
                     
                     <div class="col-md-12 padding-0 marbot_10"> 

<div class="col-md-4"><span style="color:#2B6BB5; font-size:small;font-weight: bold;">
                                         <%# Eval("PLAN_TITLE") +" " + Eval("PLAN_DESCRIPTION")%></span>
                                      <span>(<%# Eval("AgentName")%>)</span> </div>

<div class="col-md-4"><asp:Label ID="lblStatus" runat="server"></asp:Label> </div>
<div class="col-md-4">       <span>Created Dt:</span>
                                              
                                             
                                                 <span style="font-size: 12px; font-weight: bold;">
                                                     <%# Eval("BI_CREATED_ON")%></span></div>



<div class="clearfix"> </div>
</div>



              

  <div class="col-md-12 padding-0 marbot_10"> 

<div class="col-md-4">  <span>Certificate No:</span> <span style="font-size: 12px; font-weight: bold;">
                                         <%# Eval("BI_POLICY_NO")%></span></div>



<div class="clearfix"> </div>
</div>





 <div class="col-md-12 padding-0 marbot_10"> 

<div class="col-md-4"> <label style="display:none;"> 
                                     <span>
                                         <input id="Open" class="button" onclick='location.href=&#039;ViewBookingForInsurance.aspx?bookingId=<%# Eval("BI_ID") %>&#039;'
                                             type="button" value="Open" />
                                     </span>
                                     
                                     </label>
                                     
                           <span style="font-size: small; font-weight: bold;"><asp:Label ID="lblPrice" runat="server" ></asp:Label></b>
                                        <%-- <%# Eval("BI_TOTAL_PRICE")%>--%></span>          
                                     
                                     </div>

<div class="col-md-4">  <span>PNR:</span> <span style="font-size: 12px; font-weight: bold;">
                                         <%# Eval("BI_PNR")%></span> </div>




<div class="clearfix"> </div>
</div>


 <div class="col-md-12 padding-0 marbot_10"> 

<div class="col-md-12"> 

<table>
                             <tr>
                             <td> <span style="font-size: 12px;">Booked By Location:</span></td>
                             <td style="width:320px"> <span style="font-size: 12px; font-weight: bold;">
                                                     <%# Eval("LocationName")%></span></td>
                             <td> <span style="font-size: 12px;">Booked By User:</span></td>
                             <td style="width:320px"><span style="font-size: 12px; font-weight: bold;">
                                                     <%# Eval("UserName")%></span></td>
                                                     <td> <span style="font-size: 12px;">Trip Id:</span></td>
                                                     <td><span style="font-size: 12px; font-weight: bold;">
                                                     <%# Eval("BI_ID")%></span></td>
                             </tr>
                             </table>

</div>



<div class="clearfix"> </div>
</div>

<div class="col-md-12 padding-0 marbot_10"> 

<div class="col-md-12">  <asp:Label ID="lblRemarks" runat="server" BackColor="#CCCCCC" Font-Italic="True"
                                            Font-Names="Arial" Font-Size="10pt" Text='<%#Eval("data") %>' Width="100%"></asp:Label></div>

<div class="clearfix"> </div>
</div>

<div> 

<table id="tblRefund" style="margin-top: 10px" visible="false" width="100%"
                                         border='0' cellspacing='0' cellpadding='2' runat="server">
                                         <tr>
                                             <td colspan="2" align="right">
                                                 <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label>
                                             </td>
                                             <td align="right" class="style3"><span style="font-weight:bold;">Admin Fee: &nbsp;&nbsp;</span>
                                             <asp:Label ID="lblAgentCurrency" runat="server"></asp:Label>
                                                
                                                 <asp:TextBox ID="txtAdminFee" runat="server" Width="80px" onfocus="Check(this.id);" onBlur="Set(this.id);" Text="0"></asp:TextBox>
                                                 &nbsp;
                                             </td>
                                             <%--<td>
                                       AED <asp:TextBox ID="txtSupplierFee" runat="server" Width="80px" Text="0"></asp:TextBox>
                                    </td>--%>
                                             <td>
                                               
                                                 <asp:Button ID="btnRefund" CssClass="button" runat="server" Font-Bold="True" Text="Refund" CommandName="Refund"
                                                     CommandArgument='<%#Eval("BI_ID") %>' />
                                             </td>
                                             <td align="right">
                                                 <asp:Button ID="btnOpen" runat="server" Text="Open" Font-Bold="True" Visible="false" />
                                             </td>
                                         </tr>
                                     </table>

</div>


                        
                        <div class="clearfix"> </div> 
                        </div>
                     </ItemTemplate>
                 </asp:DataList>
                 
                 
                 
                 <div class="clear"></div>
             </div>
    </div>

    <script type="text/javascript">
        var cal1;
        var cal2;
        
        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
//            cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select Departure date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDate1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select Arrival date");
            cal2.selectEvent.subscribe(setDate2);
            cal2.cfg.setProperty("close", true);
            cal2.render();

            
        }
        var cal1;
        var cal2;
       
        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
//            cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select From date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDate1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select To date");
            cal2.selectEvent.subscribe(setDate2);
            cal2.cfg.setProperty("close", true);
            cal2.render();

            
        }
        function showCalendar1() {
           
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            cal1.show();
            cal2.hide();
            
            init();
        }
//        var departureDate = new Date();
        function showCalendar2() {
            $('container1').context.styleSheets[0].display = "none";
           
            cal1.hide();
            
            cal2.show();
            init();
           
            document.getElementById('container2').style.display = "block";
        }

        
        function setDate1() {
            var date1 = cal1.getSelectedDates()[0];

            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtCreatedDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDate2() {
//            var date1 = document.getElementById('<%=txtToDate.ClientID %>').value;
//            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
//                return false;
//            }

            var date2 = cal2.getSelectedDates()[0];


            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }

        
        YAHOO.util.Event.addListener(window, "load", init);
        ////YAHOO.util.Event.addListener(this, "click", init);
        

        function ValidateFee(index) {  
            var adminFee = document.getElementById('ctl00_cphTransaction_dlBaggageInsChangeReqQueue_ctl0' + index + '_txtAdminFee').value;
            var total = document.getElementById('ctl00_cphTransaction_dlBaggageInsChangeReqQueue_ctl0' + index + '_lblPrice').innerHTML;
            total = total.replace(',', '');
            var totalAmount = total.split(" ");
            if (parseFloat(adminFee) > parseFloat(totalAmount[1])) {
                alert("Admin Fee should not be Greater Then Booking Amount");
                return false;
            }
            
        }

    </script>
    
    
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>
