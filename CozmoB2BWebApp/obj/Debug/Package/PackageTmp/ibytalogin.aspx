﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="CozmoB2BWebApp.ibytalogin" Codebehind="ibytalogin.aspx.cs" %>
	<!DOCTYPE html>
	<html>

	<head runat="server">
		<title>Ibyta Login </title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.ico">	
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<!-- manual css -->

		<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
		<link href="build/css/main.min.css" rel="stylesheet" type="text/css" />     
        
		<%--<script type="text/javascript" src="Scripts/Menu/query2.js"></script>--%>
     
    <%--jQuery 2.2.2 + Migrate 1.4.1 - Dec2020 Upgrade--%>

     <script src="build/js/jquery-2.2.4.min.js" type="text/javascript"></script>        
     <script type="text/javascript" src="build/js/jquery-migrate-1.4.1.min.js"></script>
       <script>
           jQuery.ajaxPrefilter(function(s){
            if(s.crossDomain){
                s.contents.script = false;
             }
           })
     </script>

	</head>
     
	<body>
		
                
          <div id="unsupported-message">
	        <a id="unsupported-overlay" href="#unsupported-modal"></a>
	        <div id="unsupported-modal" role="alertdialog" aria-labelledby="unsupported-title">
		        <h2 id="unsupported-title">⚠ Unsupported Browser ⚠</h2>
		        <p>Travtrolley probably won't work great in Internet Explorer. We generally only support the recent versions of major browsers like <a target="_blank" href="https://www.google.com/chrome/">Chrome</a>, <a target="_blank" href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a>, <a target="_blank" href="https://support.apple.com/downloads/safari">Safari.</a>!</p>

		        <p>If you think you're seeing this message in error, <a href="mailto:tech@cozmotravel.com">email us at tech@cozmotravel.com</a> and let us know your browser and version.</p>
	        </div>
	        <a id="unsupported-banner">
	        <strong> ⚠ Unsupported Browser ⚠ </strong>
		        Travtrolley may not work properly in this browser.
	        </a>
          </div>
         <script>
            // Get IE or Edge browser version
            function detectIE() {
                var ua = window.navigator.userAgent,
                    msie = ua.indexOf('MSIE '),
                    trident = ua.indexOf('Trident/'),
                    edge = ua.indexOf('Edge/');              
                 if (msie > 0 || trident > 0 ) {
                    $('#unsupported-message').css('display', 'block');
                    $('body').css('overflow', 'hidden');
                }
            }
            detectIE();         

        </script>
	        <style>
	            #unsupported-message {
                     display: none;
	            }
		        #unsupported-overlay,
		            #unsupported-modal {
		              display: block;
		              position: absolute;
		              position: fixed;
		              left: 0;
		              right: 0;
		              z-index: 9999;
		              background: #000;
		              color: #D5D7DE;
		            }
		
		            #unsupported-overlay {
		              top: 0;
		              bottom: 0;
		            }
		
		            #unsupported-modal {
		              top: 80px;
		              margin: auto;
		              width: 90%;
		              max-width: 520px;
		              max-height: 90%;
		              padding: 40px 20px;
		              box-shadow: 0 10px 30px #000;
		              text-align: center;
		              overflow: hidden;
		              overflow-y: auto;
		              border:solid 2px #ccc8b9;
		            }
		
		            #unsupported-message :last-child { margin-bottom: 0; }
		
		            #unsupported-message h2 {
		              font-size: 34px;
		              color: #FFF;
                      margin-bottom: 20px;
		            }
		
		            #unsupported-message h3 {
		              font-size: 20px;
		              color: #FFF;
		            }
		
		            body.hide-unsupport { padding-top: 24px; }
		            body.hide-unsupport #unsupported-message { visibility: hidden; }
		
		            #unsupported-banner {
		              display: block;
		              position: absolute;
		              top: 0;
		              left: 0;
		              right: 0;
		              background:#ccc8b9;
		              color: #000;
		              font-size: 14px;
		              padding: 2px 5px;
		              line-height: 1.5;
		              text-align: center;
		              visibility: visible;
		              z-index: 100000;
		            }
		
		
		
		            @media (max-width: 800px), (max-height: 500px) {
		              #unsupported-message .modal p {
		                /* font-size: 12px; */
		                line-height: 1.2;
		                margin-bottom: 1em;
		              }
		
		              #unsupported-modal {
		                position: absolute;
		                top: 20px;
		              }
		              #unsupported-message h1 { font-size: 22px; }
		              body.hide-unsupport { padding-top: 0px; }
		              #unsupported-banner { position: static; }
		              #unsupported-banner strong,
		              #unsupported-banner u { display: block; }
		            }
html,body{
    min-height: 100%;
    background-color: #ebf1fb;
}
body{
    display:flex;
    flex-direction:column;
}
.ibyta--main-background {
	background-color:#ec0b43;
	position:relative;
	z-index:100
}
.ibyta--main-background:after {
	content:"";
	background:url(build/img/ibyta-banner-bg-3.png) no-repeat bottom;
	width:100%;
	height:100%;
	position:absolute;
	bottom:-25px;
	z-index:-1
}
@media(min-width:2000px){
    .ibyta--main-background:after{display:none}}
   .ibyta--title-wrapper{
       display:-webkit-box;
       display:-webkit-flex;
       display:-ms-flexbox;
       display:flex;
       -webkit-box-pack:center;
       -webkit-justify-content:center;
       -ms-flex-pack:center;
       justify-content:center;
       min-height:100%;
       -webkit-box-orient:vertical;
       -webkit-box-direction:normal;
       -webkit-flex-direction:column;-ms-flex-direction:column;flex-direction:column
   }
   .ibyta--title-wrapper{color:#fff;position:relative;z-index:100;min-height:580px}  
    .ibyta--title-wrapper:before{
        content:"";
        background:url(build/img/ibyta-banner-bg-1.png) no-repeat;
        background-size:100%;
        opacity:.45;
        width:100%;
        height:100%;
        position:absolute;
        z-index:-1
}
.ibyta--title-wrapper h1 {
	font-size:40px;
	font-weight:700;
	margin-top:-80px
}
.ibyta--title-wrapper p {
	padding-top:20px
}
.ibyta--top-header {
	background-color:#ec0b43;
	padding:5px 0 5px 0
}
.ibyta--form-wrapper {
	background-color:#f1f6fd;
	border-radius:4px;
	padding:0 16px 55px 16px;
	margin-bottom:26px;
	margin-top:26px;
	margin-bottom:30px;
	position:relative;
	box-shadow:0 0 13px -2px rgba(0,
	0,
	0,
	0.87)
}
@media(min-width:768px){.ibyta--form-wrapper{float:right;min-width:325px;max-width:325px}}.ibyta--form-wrapper .form-control-holder {
	position:relative;
	overflow:inherit;
	margin-bottom:30px
}
.ibyta--form-wrapper .form-control-holder .form-control {
	border-radius:0;
	box-shadow:none;
	border:0;
	border-bottom:1px solid #ec0b43;
	padding-left:25px;
	background-color:transparent
}
.ibyta--form-wrapper .form-control-holder .icon-holder {
	border-right:0;
	position:absolute;
	max-width:none;
	min-width:inherit;
	background-color:transparent
}
.ibyta--form-wrapper .form-control-holder .icon-holder .icon {
	font-size:14px;
	padding-top:6px;
	padding-left:6px;
	display:block;
	line-height:22px;
	text-align:left;
	color:#8ea2c1
}
.ibyta--form-wrapper .form-control-holder .label {
	padding-top:0;
	position:absolute;
	top:-14px;
	left:-6px;
	color:#ec0b43;
	text-transform:uppercase;
	font-weight:700
}
.ibyta--login-title-wrapper {
	background-color:#c20041;
	padding:10px 0 10px 10px;
	margin-left:-16px;
	margin-right:-16px;
	margin-bottom:35px;
	color:#fff
}
.ibyta--login-title-wrapper .icon {
	font-size:22px;
	line-height:1;
	margin-right:10px
}
.ibyta--login-title-wrapper .text {
	line-height:2
}
.ibyta--key-notes-wrapper {
	background-color:#fff
}
.ibyta-footer {
    bottom: 0;
    left: 0;
    right: 0;
    z-index: 1000;
    position: relative;
    flex: 1;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
}
.ibyta-footer .copyright {
	color:#fff;
	text-align:center;
	background-color:#000;
	padding:4px 0;
    font-size: 12px;
}
.ibyta-footer .contact-info{
    text-align: center;
    padding-bottom: 5px;
    color: #ec0b43;
    background-color: #f5f4f4;
    padding: 6px 0;
}
.ibyta-footer .contact-info a{
    color: #ec0b43;
}
.ibyta-footer .contact-info em{
    color:#383838;
    font-style:normal;
}
.services-list{
    text-align: center;
    padding: 10px 2px;
      background: #fff;
      box-shadow: 0 0 5px -2px rgba(0, 0, 0, 0.32);
}
.services-list li{
    color: #696969;
    font-size: 13px;
    font-weight: 600;
    padding: 0 20px;
}
.services-list li:before{
    content:"";
    background:url(build/img/ibyta-services.svg) no-repeat;
    background-position: -124px -60px;
    width: 38px;
    height: 30px;
    display: block;
    margin:0 auto 8px auto;
    opacity:.1;
}
.services-list li:nth-child(2):before{
    background-position:0 -122px;
}
.services-list li:nth-child(3):before{
    background-position:6px -60px;
}
.services-list li:nth-child(4):before{
    background-position:-63px -60px;
}
.services-list li:nth-child(5):before{
    background-position:9px 0px;
}
.services-list li:nth-child(6):before{
    background-position:-187px -60px;
}
@media(min-width:768px){.ibyta-footer{}}

	        </style>
		<div class="ibyta--top-header">
			<div class="container">
				<div class="row">
					<div class="col-xs-6">
						<img src="images/ibyta-logo.jpg" class="img-responsive" style="max-width: 130px;margin-left: -15px;">
                        <p class="text-white"><small>An AirArabia Group Company</small> </p>
					</div>
					<div class="col-xs-6">
						<a href="Register.aspx" class="float-right mt-3" style="color:#fff;font-size:18px;"><span class="icon-user2"></span> Register</a>
					</div>
				</div>
			</div>
		</div>
		<form runat="server" id="form1">
			<asp:ScriptManager ID="ScriptManager" runat="server"></asp:ScriptManager>
			<asp:UpdatePanel ID="upnlLogin" runat="server" UpdateMode="conditional">
				<ContentTemplate>
					<div class="ibyta--main-background">
						<div class="container">
							<div class="row">
								<div class="col-md-7 order-1 order-md-0">
									<div class="ibyta--title-wrapper">
										<h1>Inspiring Destinations...</h1>
                                        <p>
                                            Ibyta is a technologically driven b2b system to simplify your travel needs, which indulges in the world of travel by global interventions which are cost effective. We are here to redefine your tryst with 10,000 destinations worldwide.<br />
A great group which is efficient and  consistent, an epitome of courteousness in itself. We have prime access to over 700,000 hotel and apartments where our clients' interests are regarded quintessential .
                                        </p>
									</div>
								</div>
								<div class="col-md-5 order-0 order-md-1">
									<div class="ibyta--form-wrapper">
										<div class="ibyta--login-title-wrapper">
											<span class="icon icon-user-circle float-left"></span>
											<span class="text">Sign in to <img src="build/img/ibyta-logo-white-sm.png" alt="Ibyta Login"></span>
										</div>
										<div class="row">
										
													
											<div class="col-md-12">
												<div id="loginTrav">
													
													<div class="col-12">
													    <asp:Label ID="lblForgotPwd" style="color:#cc1e1a;" runat="server" Text="" CssClass=" small p-3 font-weight-bold"></asp:Label>
													    <asp:Label runat="server" style="color: #cc1e1a;" ID="lblError" CssClass="small p-3 font-weight-bold"></asp:Label>
														<div class="form-control-holder mt-5">
															<span class="label">User Name</span>
															<div class="icon-holder">
<span class="icon icon-user2"></span>
															</div>
															<asp:TextBox ID="txtLoginName" class="form-control" autocomplete="off" placeholder="User Name" runat="server"></asp:TextBox>
														</div>
													</div>
													<div class="col-12">
														
														<div class="form-control-holder">
															<span class="label">Password</span>
															<div class="icon-holder">
<span class="icon icon-password"></span>
															</div>
															<asp:TextBox ID="txtPassword" class="form-control" autocomplete="off" placeholder="Password" runat="server" TextMode="password"></asp:TextBox>
														</div>
													</div>
													<div class="col-12"> 
												
													
												
														
													
													
														<div>
															<asp:Button ID="btnLogin" class="btn btn-block btn-primary" runat="server" Text="SUBMIT" OnClick="btnLogin_Click" OnClientClick=" Validate()" />
														</div>
													</div>
												</div>
												<div id="normalTrav" class="col-12 mt-4 normalTrav">
													
													<a id="forgotpassword" style="font-size: 12px;float: right;" href="javascript:void(0);" onclick="return ShowPopUp(this.id);" ><span class="icon icon-info"></span> Forgot password?</a>
												</div>
											  </div>
											<div class="clearfix"></div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
	
					
					<div class="modal fade" id="forgotPwd" tabindex="-1" role="dialog" aria-labelledby="forgotPwdLabel" aria-hidden="true">
					  <div class="modal-dialog modal-sm" role="document">
						<div class="modal-content">
						  <div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-label="Close" style="font-size: 20px;">
							  <span aria-hidden="true" class="icon icon-close"></span>
							</button>
							<h5 class="modal-title" id="forgotPwdLabel">Forgot password? </h5>
						  </div>
						  <div class="modal-body">
						    <div class="alert alert-danger small p-3 font-weight-bold" role="alert" id="err" style="display: none;"></div>
							<asp:TextBox ID="txtEmailId" CssClass="form-control margin_top10" placeholder="Enter Your Email Address" runat="server" CausesValidation="True"></asp:TextBox>
						  </div>
						  <div class="modal-footer">
							<asp:Button ID="btnGetPassword" CssClass="chosencol btn btn-primary" runat="server" Text="Get Password" OnClientClick="return Validate()" OnClick="btnGetPassword_Click" /> 
						  </div>
						</div>
					  </div>
					</div>
			
			
				</ContentTemplate>
			</asp:UpdatePanel>
		</form>

	
		
		<div class="clearfix"></div>
		<div class="ibyta-footer">
            <div class="services-list">
                <ul class="list-unstyled text-center mb-0">
                    <li class="d-inline-block">Hotels</li>
                    <li class="d-inline-block">Apartments</li>
                    <li class="d-inline-block">Tours</li>
                    <li class="d-inline-block">Transfers</li>
                    <li class="d-inline-block">Insurance</li>
                    <li class="d-inline-block">Visas</li>
                </ul>                       
            </div>  
            <div class="contact-info">
                 Tel : +97144065891 + 9714 4065892 <em> | </em> suppliers : <a href="mailto:contracting@ibyta.com">contracting@ibyta.com</a> <em> | </em> customers : <a href="mailto:sales@ibyta.com">sales@ibyta.com</a> 
               <div class="small pt-2 text-secondary">
                <a class="text-secondary" href="IbytaTerms.aspx">Terms and Conditions</a> | <a class="text-secondary" href="IbytaPrivacyPolicy.aspx">Privacy Policy</a> | <a class="text-secondary" href="IbytaContact.aspx">Contact Us</a>
               </div>
             </div>
            <div class="copyright">
                 Copyright 2019 Ibyta, UAE . All Rights Reserved.
            </div>          
         </div>
		<div style="display: none">
			<table cellpadding="0" cellspacing="0" class="label">
				<tr>
					<td style="width: 150px"></td>
					<td style="width: 150px"></td>
					<td></td>
				</tr>
				<tr>
					<td align="right"></td>
					<td></td>
				</tr>
				<tr>
					<td align="right"></td>
					<td></td>
				</tr>
				<tr>
					<td align="right">
						<asp:Label Visible="false" ID="lblCompanyName" runat="server" Text="Company:"></asp:Label>
					</td>
					<td>
						<asp:DropDownList runat="server" Visible="false" ID="ddlCompany" CssClass="inputDdlEnabled" Width="154px"></asp:DropDownList>
					</td>
				</tr>
				<tr style="height: 15px">
					<td></td>
				</tr>
				<tr>
					<td></td>
					<td align="right"></td>
				</tr>
				<tr>
					<td colspan="3">
						<asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError"></asp:Label>
					</td>
				</tr>
			</table>
		</div>
 		<script src="Scripts/bootstrap.min.js"></script>
		<script type="text/javascript" src="build/js/main.min.js"></script>
	<%--	<script src="scripts/select2.min.js" type="text/javascript"></script>--%>
		<script type="text/javascript">		
            
		        function ShowPopUp(id) {
		            document.getElementById('<%=txtEmailId.ClientID %>').value = "";
		            document.getElementById('err').style.display = 'none';
		            //document.getElementById('forgotPwd').style.display = "block";
					$('#forgotPwd').modal('show')
		            var positions = getRelativePositions(document.getElementById(id));
		            //            document.getElementById('forgotPwd').style.left = (positions[0] + 130) + 'px';
		            //            document.getElementById('forgotPwd').style.top = (positions[1] - 200) + 'px';
		            return false;
		        }
		        function HidePopUp() {
		            //document.getElementById('forgotPwd').style.display = "none";
					$('#forgotPwd').modal('hide')
		        }
		        function getRelativePositions(obj) {
		            var curLeft = 0;
		            var curTop = 0;
		            if (obj.offsetParent) {
		                do {
		                    curLeft += obj.offsetLeft;
		                    curTop += obj.offsetTop;
		                } while (obj = obj.offsetParent);
		
		            }
		            return [curLeft, curTop];
		        }
		        function Validate() {
		
		            var isValid = true;
		            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
		            document.getElementById('err').style.display = 'none';
		            if (document.getElementById('<%=txtEmailId.ClientID %>').value.length <= 0) {
		                document.getElementById('err').style.display = 'block';
		                document.getElementById('err').innerHTML = "Enter Email address";
		                isValid = false;
		            }
		            else if (reg.test(document.getElementById('<%=txtEmailId.ClientID %>').value)) {
		                document.getElementById('err').style.display = 'none';
		            }
		            else {
		                document.getElementById('err').style.display = 'block';
		                document.getElementById('err').innerHTML = "Enter Correct Email";
		                isValid = false;
		            }
		            if (isValid == true) {
		                return true;
		            }
		            else {
		                return false;
		            }
		        }
            
		        $(function () {
		            var pageHeight = $(window).height();
		            console.log(pageHeight)
		            $('body').css('minHeight', pageHeight)
		        })
      
		</script>



	</body>

	</html>
