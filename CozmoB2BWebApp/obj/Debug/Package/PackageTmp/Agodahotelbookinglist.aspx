﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="AgodaHotelBookingListGUI" Codebehind="AgodaHotelBookingList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="DevExpress.Web.v17.2, Version=17.2.7.0, Culture=neutral, PublicKeyToken=b88d1754d700e49a" Namespace="DevExpress.Web" TagPrefix="dx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <script type="text/javascript">
        var start;
        function grid_Init(s, e) {
            GrdBkngList.Refresh();
        }
        function grid_BeginCallback(s, e) {
            start = new Date();
            ClientCommandLabel.SetText(e.command);
            ClientTimeLabel.SetText("working...");
        }
        function grid_EndCallback(s, e) {
            ClientTimeLabel.SetText(new Date() - start);
        }
    </script>
    <br />
    <br />
    <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true">
            <div class="paramcon">
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-1">
                    </div>
                    <div class="col-md-1">
                        From Date:
                    </div>
                    <div class="col-md-2">
                        <dx:ASPxDateEdit ID="deStart" ClientInstanceName="deStart" runat="server" CssClass="form-control">
                            <ValidationSettings Display="Dynamic" SetFocusOnError="True" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="True" ErrorText="Start date is required"></RequiredField>
                            </ValidationSettings>
                        </dx:ASPxDateEdit>
                    </div>
                    <div class="col-md-1">
                        To Date:
                    </div>
                    <div class="col-md-2">
                        <dx:ASPxDateEdit ID="deEnd" ClientInstanceName="deEnd" runat="server" CssClass="form-control">
                            <DateRangeSettings StartDateEditID="deStart"></DateRangeSettings>
                            <ValidationSettings Display="Dynamic" SetFocusOnError="True" CausesValidation="True" ErrorDisplayMode="ImageWithTooltip">
                                <RequiredField IsRequired="True" ErrorText="End date is required"></RequiredField>
                            </ValidationSettings>
                        </dx:ASPxDateEdit>
                    </div>
                    <div class="col-md-1" style="min-width:10%;">
                        Booking Ref. No:
                    </div>
                    <div class="col-md-2">
                        <%-- <asp:TextBox ID="txtBookingRefNo" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>--%>
                        <dx:ASPxTextBox ID="txtBookingRefNo" runat="server" Width="170px" CssClass="form-control"></dx:ASPxTextBox>
                    </div>
                    <div class="col-md-1">
                        <dx:ASPxButton ID="BtnSearch" runat="server" Text="Search" CssClass="form-control but but_b pull-right btn-xs-block" OnClick="btnSearch_Click"></dx:ASPxButton>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                    <div class="col-md-2">
                    </div>
                    <div class="col-md-8">
                        <%--<asp:ObjectDataSource ID="dsHotels" runat="server" EnablePaging="true" SelectMethod="LoadData"
                            TypeName="AgodahotelbookinglistGUI">
                             <SelectParameters>
                                <asp:Parameter Name="P_FROM_DATE" Type="String" />
                                <asp:Parameter Name="P_TO_DATE" Type="String" />
                                <asp:Parameter Name="P_BOOKING_ID" Type="String" />
                            </SelectParameters>

                        </asp:ObjectDataSource>--%>

                        <%--<asp:ObjectDataSource ID="dsHotels" runat="server" EnablePaging="true" SelectMethod="TestMethod"
                           TypeName="Agoda_HotelsList"></asp:ObjectDataSource>--%>

                        <%-- <dx:ASPxGridView ID="ASPxGridView1" runat="server"></dx:ASPxGridView>--%>
                        <dx:ASPxGridView ID="GrdBkngList" runat="server" CssClass="GridPager" ClientInstanceName="GrdBkngList" KeyFieldName="BookingId" Width="100%" AutoGenerateColumns="False"
                           OnPageIndexChanged="grid_PageIndexChanging" OnPageSizeChanged="grid_PageIndexChanging">
                            <%--OnCustomColumnDisplayText="grid_CustomColumnDisplayText" OnSummaryDisplayText="grid_SummaryDisplayText"  DataSourceID="dsHotels" --%>
                            <Columns>
                                <dx:GridViewDataTextColumn FieldName="BookingId" Width="100px" />
                                <dx:GridViewDataTextColumn FieldName="City" Width="200px" ReadOnly="true" />
                                <dx:GridViewDataTextColumn FieldName="HotelName" Width="200px" ReadOnly="true" />
                                <dx:GridViewDataTextColumn FieldName="Checkindate" Width="100px" ReadOnly="true" />
                                <dx:GridViewDataTextColumn FieldName="Checkoutdate" Width="100px" ReadOnly="true" />
                                <dx:GridViewDataTextColumn FieldName="Status" Width="100px" ReadOnly="true">
                                    <Settings AllowAutoFilter="false" />
                                </dx:GridViewDataTextColumn>
                            </Columns>
                            <%-- <Settings ShowFilterRow="true" ShowFilterRowMenu="true" ShowGroupPanel="true" ShowFooter="true" />--%>
                            <SettingsPager>
                                <PageSizeItemSettings Visible="true" Items="10, 20, 50" />
                                
                            </SettingsPager>
                            <SettingsBehavior AllowSort="false" AllowGroup="false" />
                            <SettingsDataSecurity AllowInsert="false" AllowEdit="false" AllowDelete="false" />
                            <%-- <ClientSideEvents Init="grid_Init" BeginCallback="grid_BeginCallback" EndCallback="grid_EndCallback" />--%>
                        </dx:ASPxGridView>
                    </div>
                    <div class="col-md-2">
                    </div>
                </div>

            </div>
        </asp:Panel>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>

