﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" EnableEventValidation="false" CodeBehind="CabRequestForm.aspx.cs" Inherits="CozmoB2BWebApp.CabRequestForm" Title="Cab Request Form" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
    <style>
        .GridPager a, .GridPager span {
            display: block;
            height: 15px;
            width: 15px;
            font-weight: bold;
            text-align: center;
            text-decoration: none;
            margin: 4px;
        }

        .GridPager a {
            background-color: #f5f5f5;
            color: #969696;
            border: 1px solid #969696;
        }

        .GridPager span {
            background-color: #A1DCF2;
            color: #000;
            border: 1px solid #3AC0F2;
        }



        .label {
            line-height: 26px;
            font-size: 12px !important;
            font-family: Arial;
        }

        .firstPopupDivOuter {
            position: absolute;
            top: 50px;
            left: 100px;
            width: 400px;
            height: 300px;
            display: none;
            background-color: #2BA9D9;
            color: #FFF;
        }

        #firstPopupDivInner {
            width: 378px;
            height: 268px;
            background: url(images/thankuwindow.png) no-repeat;
            vertical-align: middle;
            margin: 160px auto;
        }




        a.paging_list {
            background: #fff;
            border: solid 1px #ccc;
            padding: 4px;
            color: Green;
            display: block
        }

        .gvHeader2 {
            background: #1c498a;
            font-size: 12px !important;
            font-family: Arial;
            color: #fff;
            height: 26px;
        }
    </style>
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <script type="text/javascript">
        
        function disablefield() {
            if (document.getElementById('<%=radioAgent.ClientID %>').checked == true) {
                document.getElementById('<%=wrapper1.ClientID%>').style.display = "block";
                document.getElementById('<%=wrapper2.ClientID%>').style.display = "block";
            }
            if (document.getElementById('<%=radioSelf.ClientID%>').checked == true) {
                document.getElementById('<%=wrapper1.ClientID%>').style.display = "none";
                document.getElementById('<%=wrapper2.ClientID%>').style.display = "none";
            }
        }

        function GetCabCharge() {
            if (document.getElementById('<%=ddlRentalType.ClientID%>').value != "-1"
                && document.getElementById('<%=ddlCabPreference.ClientID%>').value != "-1" && document.getElementById('<%=ddlCabModel.ClientID%>').value != "-1") {
                var rentalType = document.getElementById('ctl00_cphTransaction_ddlRentalType').value;
                var cabPreference = document.getElementById('ctl00_cphTransaction_ddlCabPreference').value;
                var cabModel = document.getElementById('ctl00_cphTransaction_ddlCabModel').value;
                $.ajax({
                    url: "CabRequestForm.aspx/GetCabCharge",
                    type: 'POST',
                    dataType: 'json',
                    data: "{'rentalType':'" + rentalType + "','cabModel':'" + cabModel + "' ,'cabPreference':'" + cabPreference + "'}",
                    contentType: "application/json; charset=utf-8",
                    success: function (data) {

                       
                        document.getElementById('<%=lblCharge.ClientID%>').innerText = 'Charge :' + data.d;

                    },
                    error: function (data) {

                    }
                });
            }

        }

        function GetCabPreferences(type) {
            //var parm = type;
            var option = "";
            $.ajax({
                type: "POST",
                url: "CabRequestForm.aspx/GetCabPreferences",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                data: "{'data':'" + type + "'}",
                success: function (data) {
                    $.each(data.d, function (index, item) {
                        option += "<option value = '" + index + " '>" + item + " </option>";
                    });
                    if (type == 'CabType') {
                        $('#ddlCabpreference').append(option);
                    }
                    else if (type == 'SerType') {
                        $('#ddlRentalType').append(option);
                    }
                },
                error: function (data) {

                }
            });
        }

        function ValidateSaveCtrls() {
            var isValid = true;
            var RowCount = <%=gvData.Rows.Count%>;

            if (document.getElementById('ctl00_cphTransaction_txtName').value.trim().length <= 0) {
                document.getElementById('name').style.display = 'block';
                document.getElementById('name').innerText = "Please Enter Name."
                isValid = false;
            }
            if (document.getElementById('ctl00_cphTransaction_txtMobileNumber').value.trim().length <= 0) {
                document.getElementById('mobileno').style.display = 'block';
                document.getElementById('mobileno').innerText = "Please Enter Mobile Number."
                isValid = false;
            }

            return isValid;

        }

        function validate() {

            var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
            //var fromDate = GetDateObject('ctl00_cphTransaction_FromDate');

            document.getElementById('spFromDate').style.display = 'none';
            document.getElementById('toDate').style.display = 'none';
            document.getElementById('rentalType').style.display = 'none';
            document.getElementById('cabpreference').style.display = 'none';
            document.getElementById('pickupfrom').style.display = 'none';
            document.getElementById('dropat').style.display = 'none';
            document.getElementById('specialreq').style.display = 'none';
            document.getElementById('email').style.display = 'none';
            document.getElementById('flightNumber').style.display = 'none';
            document.getElementById('cabmodel').style.display = 'none';
            document.getElementById('cabmodel').style.display = 'none';
            document.getElementById('agent').style.display = 'none';
            document.getElementById('agentlocations').style.display = 'none';
            document.getElementById('name').style.display = 'none';
            document.getElementById('mobileno').style.display = 'none';


            var isValid = ValidateSaveCtrls();

            isValid = checkCabDates();
            if (document.getElementById('ctl00_cphTransaction_FromDate').value.trim().length <= 0) {

                document.getElementById('spFromDate').style.display = 'block';
                document.getElementById('spFromDate').innerText = "Please Select From Date."
                isValid = false;
            }
            if (document.getElementById('ctl00_cphTransaction_ToDate').value.trim().length <= 0) {

                document.getElementById('toDate').style.display = 'block';
                document.getElementById('toDate').innerText = "Please Select To Date."
                isValid = false;
            }

            if (document.getElementById('<%=ddlRentalType.ClientID%>').value == "-1") {
                document.getElementById('rentalType').style.display = 'block';
                document.getElementById('rentalType').innerHTML = 'Please Select Rental Type.'
                isValid = false;
            }

            if (document.getElementById('<%=ddlCabPreference.ClientID%>').value == "-1") {
                document.getElementById('cabpreference').style.display = 'block';
                document.getElementById('cabpreference').innerHTML = 'Please Select Cab Preference.'
                isValid = false;
            }
            if (document.getElementById('<%=ddlCabModel.ClientID%>').value == "-1") {
                document.getElementById('cabmodel').style.display = 'block';
                document.getElementById('cabmodel').innerHTML = 'Please Select Cab Model.'
                isValid = false;
            }
            if (document.getElementById('<%=txtPickUpFrom.ClientID%>').value.trim().length <= 0) {
                document.getElementById('pickupfrom').style.display = 'block';
                document.getElementById('pickupfrom').innerHTML = 'Please Enter PickUp Address.'
                isValid = false;
            }
            if (document.getElementById('<%=txtDropAt.ClientID%>').value.trim().length <= 0) {
                document.getElementById('dropat').style.display = 'block';
                document.getElementById('dropat').innerHTML = 'Please Enter Drop At Address.'
                isValid = false;
            }

            if (document.getElementById('<%=radioAgent.ClientID%>').checked) {
                if (document.getElementById('<%=ddlAgents.ClientID%>').value == "0") {
                    document.getElementById('agent').style.display = 'block';
                    document.getElementById('agent').innerHTML = 'Please select agent'
                    isValid = false;
                }
                if (document.getElementById('<%=ddlAgentsLocations.ClientID%>').value == "" || document.getElementById('<%=ddlAgentsLocations.ClientID%>').value == "-1") {
                    document.getElementById('agentlocations').style.display = 'block';
                    document.getElementById('agentlocations').innerHTML = 'Please select location'
                    isValid = false;
                }
            }
            if (document.getElementById('<%=txtEmail.ClientID%>').value.trim().length <= 0) {
                document.getElementById('email').style.display = 'block';
                document.getElementById('email').innerHTML = 'Please Enter email.'
                isValid = false;
            }
            if (document.getElementById('ctl00_cphTransaction_ddlRentalType').value == 'AR') {

                if (document.getElementById('<%=txtFlightNumber.ClientID%>').value.trim().length <= 0) {
                    document.getElementById('flightNumber').style.display = 'block';
                    document.getElementById('flightNumber').innerHTML = 'Please Enter Flight Number.'
                    document.getElementById('spflightNo').style.display = 'block';
                    document.getElementById('spflightNo').innerHTML = '*';
                    isValid = false;
                }
            }


            if (isValid) {
                if (pattern.test(document.getElementById('<%=txtEmail.ClientID%>').value)) {
                    isValid = true;
                }
                else {
                    document.getElementById('email').style.display = 'block';
                    document.getElementById('email').innerHTML = 'Invalid Email.'
                    isValid = false;
                }
            } else {
                document.getElementById('errMess').style.display = 'block';
                setTimeout(function () {
                    $("#errMess").fadeOut(2000, "linear");
                }, 2000);
            }

            return isValid;

        }

        function checkCabDates() {
            var date1 = document.getElementById('<%=FromDate.ClientID %>').value;
            var date2 = document.getElementById('<%=ToDate.ClientID %>').value;
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();
            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('spFromDate').style.display = "block";
                document.getElementById('spFromDate').innerHTML = " Invalid From Date";
                return false;
            }

            var retDateArray = date2.split('/');

            // checking if date2 is valid	
            if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                document.getElementById('toDate').style.display = "block";
                document.getElementById('toDate').innerHTML = " Invalid ToDate";
                return false;
            }
            var cOutDate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
            if (todaydate.getTime() > cOutDate.getTime()) {
                document.getElementById('toDate').style.display = "block";
                document.getElementById('toDate').innerHTML = "ToDate should be greater than equal to todays date";
                return false;
            }

            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 0) {
                document.getElementById('spFromDate').style.display = "block";
                document.getElementById('spFromDate').innerHTML = "From date should be greater than or equal to ToDate";
                return false;
            }
            return true;
        }

        function isNumbers(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
                return false;
            }
            return true;
        }
        function isAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 46 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 9 || keyCode == 8 || keyCode == 11);
            return ret;
        }

        function isAlpha(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || keyCode == 9 || keyCode == 8 || keyCode == 11);
            //autoCompInit1();
            return ret;
        }
        function checkEmail(inputvalue) {
            var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
            if (pattern.test(inputvalue)) {
                return true;
            } else {
                return false;
            }
        }

        function GetSelectedVal() {
            document.getElementById('<%=hdnLocation.ClientID%>').value = document.getElementById('ctl00_cphTransaction_ddlAgentsLocations').value;

        }

        function HideSuccessMessage() {
            $('#divMsg').fadeTo(5000, 0);
        }

        var Ajax;
        var loc;
        function LoadAgentLocations(id, type) {
       
            var location = 'ctl00_cphTransaction_ddlAgentsLocations';
            $("#<%=ddlAgentsLocations.ClientID%>").empty();
            $("#<%=ddlAgentsLocations.ClientID%>").select2().attr('text', '');
            if (document.getElementById('<%=radioAgent.ClientID %>').checked == true
                && document.getElementById('<%=ddlAgents.ClientID %>').value == "0") {

                if (ddl != null) {
                    ddl.options.length = 0;
                    var el = document.createElement("option");
                    el.textContent = "Select Location";
                    el.value = "-1";
                    ddl.add(el, 0);
                    var values = ""
                    var el = document.createElement("option");
                    el.textContent = "";
                    el.value = "";
                    ddl.appendChild(el);
                }

                $("#<%=ddlAgentsLocations.ClientID%>").empty();
            }

            else {
                var location = 'ctl00_cphTransaction_ddlAgentsLocations';
                loc = location;
                var sel = document.getElementById('<%=ddlAgents.ClientID %>').value;

                var paramList = 'requestSource=getAgentsLocationsByAgentId' + '&AgentId=' + sel + '&id=' + location;;
                var url = "CityAjax";

                if (window.XMLHttpRequest) {
                    Ajax = new XMLHttpRequest();
                }
                else {
                    Ajax = new ActiveXObject("Microsoft.XMLHTTP");
                }
                Ajax.onreadystatechange = BindLocationsList;
                Ajax.open('POST', url);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);
            }

        }
        function BindLocationsList() {
            var ddl = document.getElementById(loc);
            if (Ajax.readyState == 4 && Ajax.status == 200 && Ajax.responseText.length > 0) {
                if (ddl != null) {
                    ddl.options.length = 0;
                    var el = document.createElement("option");
                    el.textContent = "Select Location";
                    el.value = "-1";
                    ddl.add(el, 0);
                    //routing agent Id|location name#location id
                    var values = Ajax.responseText.split('#')[1].split(',');
                    for (var i = 0; i < values.length; i++) {
                        var opt = values[i];
                        if (opt.length > 0 && opt.indexOf('|') > 0) {
                            var el = document.createElement("option");
                            el.textContent = opt.split('|')[0];
                            el.value = opt.split('|')[1];
                            ddl.appendChild(el);
                        }
                    }
                    $('#' + ddl.id).select2('val', '-1');

                }
            }

            if (document.getElementById('<%=hdnLocation.ClientID%>').value != "") {
                var val = document.getElementById('<%=hdnLocation.ClientID%>').value;
                $('#ctl00_cphTransaction_ddlAgentsLocations').select2('val', val);
                //document.getElementById('ctl00_cphTransaction_ddlAgentsLocations').value = document.getElementById('<%=hdnLocation.ClientID%>').value;
            }

        }

        //Calender code 
        var call1;
        var call2;
        //-Cab Calender control
        function init1() {
            var today = new Date();
            // Rendering Cal1
            call1 = new YAHOO.widget.Calendar("call1", "fcontainer1");
            call1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            call1.cfg.setProperty("title", "Select your from date:");
            call1.cfg.setProperty("close", true);
            call1.selectEvent.subscribe(setCabDate1);
            call1.render();
            // Rendering Cal2
            call2 = new YAHOO.widget.Calendar("call2", "fcontainer2");
            call2.cfg.setProperty("title", "Select your to date:");
            call2.selectEvent.subscribe(setCabDate2);
            call2.cfg.setProperty("close", true);
            call2.render();
        }

        function showCabCalendar1() {
            call2.hide();
            init1();
            document.getElementById('fcontainer1').style.display = "block";
            document.getElementById('fcontainer2').style.display = "none";
        }

        function showCabCalendar2() {
            call1.hide();
            init1();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%=FromDate.ClientID %>').value;
            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');
                var arrMinDate = new Date(depDateArray[2], depDateArray[1], depDateArray[0]);
                call2.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate()) + "/" + depDateArray[2]);
                call2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                call2.render();
            }
            document.getElementById('fcontainer2').style.display = "block";
        }
        function setCabDate1() {
            var date1 = call1.getSelectedDates()[0];
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();
            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());
            if (difference < 0) {
                document.getElementById('spFromDate').style.display = "block";
                document.getElementById('spFromDate').innerHTML = "Date selected should be greater than or equal to today's date ";
                return false;
            }
            document.getElementById('spFromDate').style.display = "none";
            document.getElementById('spFromDate').innerHTML = "";

            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=FromDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
            call1.hide();
        }

        function setCabDate2() {
            var date1 = document.getElementById('<%=FromDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('spFromDate').style.display = "block";
                document.getElementById('spFromDate').innerHTML = "First select From date.";
                return false;
            }
            document.getElementById('spFromDate').style.display = "none";
            document.getElementById('spFromDate').innerHTML = "";
            var date2 = call2.getSelectedDates()[0];
            var depDateArray = date1.split('/');
            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('spFromDate').style.display = "block";
                document.getElementById('spFromDate').innerHTML = " Invalid From Date";
                return false;
            }
            document.getElementById('spFromDate').style.display = "none";
            document.getElementById('spFromDate').innerHTML = " ";
            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            if (difference < 0) {
                document.getElementById('toDate').style.display = "block";
                document.getElementById('toDate').innerHTML = "Date of To Date should be greater than or equal to date of From Date (" + date1 + ")";
                return false;
            }
            document.getElementById('toDate').style.display = "none";
            document.getElementById('toDate').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=ToDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            call2.hide();
        }

        YAHOO.util.Event.addListener(window, "load", init1);



    </script>
    <asp:HiddenField ID="hdnId" runat="server" Value="" />
    <asp:HiddenField ID="hdnLocation" runat="server" Value="" />
    <asp:HiddenField ID="hdnRefNumber" runat="server" Value="" />

    <h3><span id="spnTitle">Cab Request </span></h3>

    <div class="search_container p-3">

        <div id="errMess" class="error_module mb-3" style="display: block;">

            <div id="name" class="alert-messages alert-caution" style="display: none"></div>
            <div id="mobileno" class="alert-messages alert-caution" style="display: none"></div>
            <div id="spFromDate" class="alert-messages alert-caution" style="display: none"></div>
            <div id="toDate" class="alert-messages alert-caution" style="display: none"></div>
            <div id="rentalType" class="alert-messages alert-caution" style="display: none"></div>
            <div id="cabmodel" class="alert-messages alert-caution" style="display: none"></div>
            <div id="cabpreference" class="alert-messages alert-caution" style="display: none"></div>
            <div id="pickupfrom" class="alert-messages alert-caution" style="display: none"></div>
            <div id="dropat" class="alert-messages alert-caution" style="display: none"></div>
            <div id="specialreq" class="alert-messages alert-caution" style="display: none"></div>
            <div id="email" class="alert-messages alert-caution" style="display: none"></div>
            <div id="flightNumber" class="alert-messages alert-caution" style="display: none"></div>
            <div id="agent" class="alert-messages alert-caution" style="display: none"></div>
            <div id="agentlocations" class="alert-messages alert-caution" style="display: none"></div>

        </div>

        <div id="divMsg" style="display: block">
            <asp:Label ID="lblSuccessMsg" class="alert-messages alert-success mb-3" Style="display: none;" runat="server" Text="MSG"></asp:Label>
        </div>

        <div id="divContainer" class="row" runat="server">


            <div class="col-4">
                <div class="row custom-gutter">
                    <div class="col-md-6 col-xs-6">
                        <asp:RadioButton ID="radioSelf" CssClass="mt-3 custom-radio-table" GroupName="CabRequest" Checked="true" Text="For Self" onclick="disablefield();" runat="server" />
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <asp:RadioButton ID="radioAgent" CssClass="mt-3 custom-radio-table" Text="Client" GroupName="CabRequest" runat="server" onclick="disablefield();" />
                    </div>
                </div>
            </div>

            <div class="col-6">
                <div class="row custom-gutter">
                    <div class="col-md-6 col-xs-6" id="wrapper1" style="display: none" runat="server">

                        <asp:DropDownList ID="ddlAgents" AppendDataBoundItems="true" CssClass="form-control" runat="server" onchange="LoadAgentLocations(this.id,'F')">
                        </asp:DropDownList>
                        <span class="red_span">*</span>
                    </div>

                    <div class="col-md-6 col-xs-6" id="wrapper2" style="display: none" runat="server">

                        <asp:DropDownList ID="ddlAgentsLocations" CssClass="form-control" runat="server" onchange="GetSelectedVal();">
                        </asp:DropDownList>
                        <span class="red_span">*</span>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">

            <div class="col-md-3">
                <span>Name:</span>
                <div class="form-group">
                    <asp:TextBox ID="txtName" runat="server" CssClass="form-control" MaxLength="50" onKeyPress="return isAlpha(event);"></asp:TextBox>
                    <b class="red_span" style="display: none" id="name"></b>
                </div>
            </div>


            <div class="col-md-3">
                <span>Mobile Number:</span>
                <span class="red_span">*</span>
                <div class="form-group">
                    <asp:TextBox ID="txtMobileNumber" runat="server" CssClass="form-control" MaxLength="10" onKeyPress="return isNumbers(event)"></asp:TextBox>
                    <b class="red_span" style="display: none" id="mobileno"></b>
                </div>
            </div>
            <span class="red_span">*</span>

        </div>
        <div class="row">
            <div class="col-md-3">

                <div>
                    From Date:
                 <span class="red_span">*</span>
                </div>

                <div class="form-control-holder">
                    <div class="icon-holder">
                        <span class="icon-calendar" aria-label=""></span>
                    </div>
                    <asp:TextBox ID="FromDate" CssClass="form-control" Width="110px" runat="server"></asp:TextBox>
                    <a href="javascript:void(null)" onclick="showCabCalendar1()">
                        <img id="Img4" src="images/call-cozmo.png" alt="Pick Date" />
                    </a>

                    <%--<uc1:DateControl ID="FromDate" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>--%>
                    <b class="red_span" style="display: none" id=""></b>
                </div>



            </div>
            <div class="col-md-3">

                <div>
                    To Date:
                <span class="red_span">*</span>
                </div>
                <div class="form-control-holder">
                    <div class="icon-holder">
                        <span class="icon-calendar" aria-label=""></span>
                    </div>
                    <asp:TextBox ID="ToDate" CssClass="form-control" Width="110px" runat="server"></asp:TextBox>
                    <a href="javascript:void(null)" onclick="showCabCalendar2()">
                        <img id="Img3" src="images/call-cozmo.png" alt="Pick Date" />
                    </a>
                    <%--<uc1:DateControl ID="ToDate" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY" DateOnly="true" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday"></uc1:DateControl>--%>
                    <span class="red_span" style="display: none" id=""></span>
                </div>
            </div>
            <div class="col-md-2">

                <div>
                    PickUp Time:
                <span class="red_span">*</span>
                </div>
                <div class="form-control-holder">
                    <div class="icon-holder">
                        <span class="glyphicon glyphicon-time mt-1" aria-label=""></span>
                    </div>
                    <asp:DropDownList id="ddlPickuptime" runat="server" class="form-control custom-select2">
                                      <asp:ListItem value="00:15">00:15</asp:ListItem>
                                      <asp:ListItem value="00:30">00:30</asp:ListItem>
                                      <asp:ListItem value="00:45">00:45</asp:ListItem>
                                      <asp:ListItem value="01:00">01:00</asp:ListItem>
                                      <asp:ListItem value="01:15">01:15</asp:ListItem>
                                      <asp:ListItem value="01:30">01:30</asp:ListItem>
                                      <asp:ListItem value="01:45">01:45</asp:ListItem>
                                      <asp:ListItem value="02:00">02:00</asp:ListItem>
                                      <asp:ListItem value="02:15">02:15</asp:ListItem>
                                      <asp:ListItem value="02:30">02:30</asp:ListItem>
                                      <asp:ListItem value="02:45">02:45</asp:ListItem>
                                      <asp:ListItem value="03:00">03:00</asp:ListItem>
                                      <asp:ListItem value="03:15">03:15</asp:ListItem>
                                      <asp:ListItem value="03:30">03:30</asp:ListItem>
                                      <asp:ListItem value="03:45">03:45</asp:ListItem>
                                      <asp:ListItem value="04:00">04:00</asp:ListItem>
                                      <asp:ListItem value="04:15">04:15</asp:ListItem>
                                      <asp:ListItem value="04:30">04:30</asp:ListItem>
                                      <asp:ListItem value="04:45">04:45</asp:ListItem>
                                      <asp:ListItem value="05:00">05:00</asp:ListItem>
                                      <asp:ListItem value="05:15">05:15</asp:ListItem>
                                      <asp:ListItem value="05:30">05:30</asp:ListItem>
                                      <asp:ListItem value="05:45">05:45</asp:ListItem>
                                    <asp:ListItem value="06:00">06:00</asp:ListItem>
                                    <asp:ListItem value="06:15">06:15</asp:ListItem>
                                    <asp:ListItem value="06:30">06:30</asp:ListItem>
                                    <asp:ListItem value="06:45">06:45</asp:ListItem>
                                    <asp:ListItem value="07:00">07:00</asp:ListItem>
                                    <asp:ListItem value="07:15">07:15</asp:ListItem>
                                    <asp:ListItem value="07:30">07:30</asp:ListItem>
                                    <asp:ListItem value="07:45">07:45</asp:ListItem>
                                    <asp:ListItem value="08:00">08:00</asp:ListItem>
                                    <asp:ListItem value="08:15">08:15</asp:ListItem>
                                    <asp:ListItem value="08:30">08:30</asp:ListItem>
                                    <asp:ListItem value="08:45">08:45</asp:ListItem>
                                    <asp:ListItem value="09:00">09:00</asp:ListItem>
                                    <asp:ListItem value="09:15">09:15</asp:ListItem>
                                    <asp:ListItem value="09:30">09:30</asp:ListItem>
                                    <asp:ListItem value="09:45">09:45</asp:ListItem>
                                    <asp:ListItem value="10:00" Selected="True">10:00</asp:ListItem>
                                    <asp:ListItem value="10:15">10:15</asp:ListItem>
                                    <asp:ListItem value="10:30">10:30</asp:ListItem>
                                    <asp:ListItem value="10:45">10:45</asp:ListItem>
                                    <asp:ListItem value="11:00">11:00</asp:ListItem>
                                    <asp:ListItem value="11:15">11:15</asp:ListItem>
                                    <asp:ListItem value="11:30">11:30</asp:ListItem>
                                    <asp:ListItem value="11:45">11:45</asp:ListItem>
                                    <asp:ListItem value="12:00">12:00</asp:ListItem>
                                    <asp:ListItem value="12:15">12:15</asp:ListItem>
                                    <asp:ListItem value="12:30">12:30</asp:ListItem>
                                    <asp:ListItem value="12:45">12:45</asp:ListItem>
                                    <asp:ListItem value="13:00">13:00</asp:ListItem>
                                    <asp:ListItem value="13:15">13:15</asp:ListItem>
                                    <asp:ListItem value="13:30">13:30</asp:ListItem>
                                      <asp:ListItem value="13:45">13:45</asp:ListItem>
                                      <asp:ListItem value="14:00">14:00</asp:ListItem>
                                      <asp:ListItem value="14:15">14:15</asp:ListItem>
                                      <asp:ListItem value="14:30">14:30</asp:ListItem>
                                      <asp:ListItem value="14:45">14:45</asp:ListItem>
                                      <asp:ListItem value="15:00">15:00</asp:ListItem>
                                      <asp:ListItem value="15:15">15:15</asp:ListItem>
                                      <asp:ListItem value="15:30">15:30</asp:ListItem>
                                      <asp:ListItem value="15:45">15:45</asp:ListItem>
                                      <asp:ListItem value="16:00">16:00</asp:ListItem>
                                      <asp:ListItem value="16:15">16:15</asp:ListItem>
                                      <asp:ListItem value="16:30">16:30</asp:ListItem>
                                      <asp:ListItem value="16:45">16:45</asp:ListItem>
                                      <asp:ListItem value="17:00">17:00</asp:ListItem>
                                      <asp:ListItem value="17:15">17:15</asp:ListItem>
                                      <asp:ListItem value="17:30">17:30</asp:ListItem>
                                      <asp:ListItem value="17:45">17:45</asp:ListItem>
                                      <asp:ListItem value="18:00">18:00</asp:ListItem>
                                      <asp:ListItem value="18:15">18:15</asp:ListItem>
                                      <asp:ListItem value="18:30">18:30</asp:ListItem>
                                      <asp:ListItem value="18:45">18:45</asp:ListItem>
                                      <asp:ListItem value="19:00">19:00</asp:ListItem>
                                      <asp:ListItem value="19:15">19:15</asp:ListItem>
                                      <asp:ListItem value="19:30">19:30</asp:ListItem>
                                      <asp:ListItem value="19:45">19:45</asp:ListItem>
                                      <asp:ListItem value="20:00">20:00</asp:ListItem>
                                      <asp:ListItem value="20:15">20:15</asp:ListItem>
                                      <asp:ListItem value="20:30">20:30</asp:ListItem>
                                      <asp:ListItem value="20:45">20:45</asp:ListItem>
                                    <asp:ListItem value="21:00">21:00</asp:ListItem>
                                    <asp:ListItem value="21:15">21:15</asp:ListItem>
                                    <asp:ListItem value="21:30">21:30</asp:ListItem>
                                    <asp:ListItem value="21:45">21:45</asp:ListItem>
                                    <asp:ListItem value="22:00">22:00</asp:ListItem>
                                    <asp:ListItem value="22:15">22:15</asp:ListItem>
                                    <asp:ListItem value="22:30">22:30</asp:ListItem>
                                    <asp:ListItem value="22:45">22:45</asp:ListItem>
                                    <asp:ListItem value="23:00">23:00</asp:ListItem>
                                    <asp:ListItem value="23:15">23:15</asp:ListItem>
                                    <asp:ListItem value="23:30">23:30</asp:ListItem>
                                    <asp:ListItem value="23:45">23:45</asp:ListItem>
                                    <asp:ListItem value="00:00">00:00</asp:ListItem>
                                    </asp:DropDownList>
                    <%--<uc1:DateControl ID="PickUpTime" runat="server" TimeOnly="true" HorizontalAlignment="Left"></uc1:DateControl>--%>
                    <span class="red_span" style="display: none" id=""></span>
                </div>
            </div>
            <div class="col-md-3">

                <div>
                    Rental Type:
                <span class="red_span">*</span>

                </div>
                <div class="form-group">
                    <asp:DropDownList ID="ddlRentalType" runat="server" onchange="GetCabCharge();" CssClass="form-control"></asp:DropDownList>
                    <span class="red_span" style="display: none" id=""></span>
                </div>
            </div>
            <div class="col-md-3">
                <div>
                    Cab Model:
				<span class="red_span">*</span>
                </div>
                <div class="form-group">
                    <asp:DropDownList ID="ddlCabModel" runat="server" onchange="GetCabCharge();" CssClass="form-control"></asp:DropDownList>
                    <span class="red_span" style="display: none" id=""></span>
                </div>
            </div>
            <div class="col-md-3">
                <div>
                    Cab Preference:
				<span class="red_span">*</span>
                </div>
                <div class="form-group">
                    <asp:DropDownList ID="ddlCabPreference" runat="server" onchange="GetCabCharge();" CssClass="form-control"></asp:DropDownList>
                    <span class="red_span" style="display: none" id="cabpreference"></span>
                    <asp:Label ID="lblCharge" runat="server"></asp:Label>
                </div>
            </div>
            <div class="col-md-3">
                <div>
                    Pick Up From:
				  <span class="red_span">*</span>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="txtPickUpFrom" runat="server" CssClass="form-control"></asp:TextBox>
                    <span class="red_span" style="display: none" id=""></span>
                </div>
            </div>
            <div class="col-md-3">
                <div>
                    Drop At:
				   <span class="red_span">*</span>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="txtDropAt" runat="server" CssClass="form-control"></asp:TextBox>
                    <span class="red_span" style="display: none" id=""></span>
                </div>
            </div>
            <div class="col-md-3">
                <div>
                    Special Request:
                </div>
                <div class="form-group">
                    <asp:TextBox ID="txtSpecialRequest" runat="server" CssClass="form-control"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-3">
                <div>
                    Email:
                <span class="red_span">*</span>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control"></asp:TextBox>
                    <span class="red_span" style="display: none" id=""></span>
                </div>
            </div>
            <div class="col-md-3">
                <div>
                    Flight Number:
				 <span class="red_span" id="spflightNo" style="display: none">*</span>
                </div>
                <div class="form-group">
                    <asp:TextBox ID="txtFlightNumber" runat="server" CssClass="form-control" onkeypress="return isAlphaNumeric(event);" MaxLength="5" ondrop="return false;" onpaste="return true;"></asp:TextBox>
                    <span class="red_span" style="display: none" id=""></span>
                </div>
            </div>
            <div class="clear" style="margin-left: 25px">
                <div id="fcontainer1" style="position: absolute; top: 300px; left: 8px; display: none;">
                </div>
            </div>
            <div class="clear" style="margin-left: 30px">
                <div id="fcontainer2" style="position: absolute; top: 300px; left: 300px; display: none;">
                </div>
            </div>
            <div class="col-12">

                <asp:Button ID="btnclear" runat="server" Text="Clear" CssClass="btn but_b button pull-right" OnClick="btnclear_Click" />
                <asp:Button ID="btnDelete" runat="server" Text="Delete" CssClass="btn but_b button  pull-right" OnClick="btnDelete_Click" OnClientClick="return validate();" Visible="false" />
                <asp:Button ID="btnAdd" runat="server" Text="Add" CssClass="btn but_b button  pull-right" OnClientClick="return validate();" OnClick="btnAdd_Click" />

                <asp:Button ID="btnUpdate" runat="server" Text="Update" CssClass="btn but_b button  pull-right" OnClientClick="return validate();" OnClick="btnUpdate_Click" />

                <div class="clearfix"></div>


            </div>



        </div>

        <div class="row">
            <div class="col-12">
                <div class="table table-responsive ">
                    <asp:GridView ID="gvData" runat="server" AutoGenerateColumns="false" CssClass="table table-bordered b2b-corp-table mt-3" ShowHeaderWhenEmpty="true" EmptyDataText="No Records Found" OnSelectedIndexChanged="gvData_SelectedIndexChanged" AllowPaging="true" PageSize="10" OnPageIndexChanging="gvData_PageIndexChanging" ForeColor="Black">
                        <HeaderStyle CssClass="themecol1" HorizontalAlign="Left"></HeaderStyle>
                        <Columns>
                            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />" ControlStyle-CssClass="label" ShowSelectButton="True">

                                <ControlStyle CssClass="label"></ControlStyle>
                            </asp:CommandField>

                            <asp:BoundField HeaderText="FromDate" DataField="FromDate" />
                            <asp:BoundField HeaderText="ToDate" DataField="ToDate" />
                            <asp:BoundField HeaderText="PickUpTime" DataField="PickUpTime" />
                            <asp:BoundField HeaderText="RentalType" DataField="RentalType" />
                            <asp:BoundField HeaderText="CabModel" DataField="CabModel" />
                            <asp:BoundField HeaderText="CabPreference" DataField="CabPreference" />
                            <asp:BoundField HeaderText="PickUpFrom" DataField="PickUpFrom" />
                            <asp:BoundField HeaderText="DropAt" DataField="DropAt" />
                            <asp:BoundField HeaderText="SpecialRequest" DataField="SpecialRequest" />
                            <asp:BoundField HeaderText="Email" DataField="Email" />
                            <asp:BoundField HeaderText="FlightNumber" DataField="FlightNumber" />

                        </Columns>



                    </asp:GridView>
                </div>
            </div>
        </div>

        <%if (gvData.Rows.Count > 0)
            {%>

        <asp:Button ID="btnClearVal" runat="server" Text="Clear" CssClass="btn but_b button  pull-right" OnClientClick="clearValues();" OnClick="btnClearVal_Click" />
        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn but_b button  pull-right" OnClientClick="return ValidateSaveCtrls();" OnClick="btnSave_Click" />

        <%} %>

        <br />
        <br />
        <br />


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>

