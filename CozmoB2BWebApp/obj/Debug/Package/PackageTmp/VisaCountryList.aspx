﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="VisaCountryList" Title="Cozmo Travels" Codebehind="VisaCountryList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    
       
       
       
                <div> <h4>Visa Country List</h4> </div>
   
        <div class=" bg_white bor_gray pad_10 paramcon">
    <div class="col-md-2"> <label class="pull-right fl_xs">
                 Search by region:</label></div>
    
    <div class="col-md-2"> 
    
    <asp:DropDownList ID="ddlRegion" runat="server" CssClass="form-control" >
                    </asp:DropDownList>
    
    </div>
                

  
  
    <div class="col-md-2 xspadtop10">
    
<asp:Button ID="btnSearch"  CssClass="but but_b" runat="server" Text="Search" OnClick="btnSearch_Click" />
    

    
    
     </div>
    <div class="col-md-2"> 
    
    <asp:HyperLink ID="HyperLink1" CssClass="fcol_blue" runat="server" NavigateUrl="~/AddVisaCountry.aspx">Add New Country </asp:HyperLink>
    
    
    </div>
       
    
    <div class="clearfix"> </div> 
    </div>
    
       
       
       
        
        <div>
            <asp:Label ID="lbl_msg" runat="server" Text=""></asp:Label>
        </div>
        <div class="table-responsive margin-top-10">  
        
        <asp:GridView ID="GridView1" runat="server" CssClass="datagrid" AutoGenerateColumns="False" CellPadding="2"
            Width="100%" DataKeyNames="countryId" ForeColor="#333333" GridLines="None" 
            OnRowDataBound="GridView1_RowDataBound"
            OnRowCommand="GridView1_RowCommand" AllowPaging="True" OnPageIndexChanging="GridView1_PageIndexChanging"
            AllowSorting="True">
            <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
            <Columns>
               
                <asp:BoundField DataField="countryName" HeaderText="Name">
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:BoundField DataField="countryCode" HeaderText="Country Code">
                    <HeaderStyle HorizontalAlign="Left" />
                    <ItemStyle CssClass="uppercase-text" />
                </asp:BoundField>
                <asp:BoundField DataField="isActive" HeaderText="Is Active" ReadOnly="True">
                    <HeaderStyle HorizontalAlign="Left" />
                </asp:BoundField>
                <asp:HyperLinkField Text="Edit" DataNavigateUrlFields="countryId" HeaderText="Edit"
                    DataNavigateUrlFormatString="AddVisaCountry.aspx?id={0}">
                   
                </asp:HyperLinkField>
                <asp:TemplateField HeaderText="Activate/Deactivate">
                    <HeaderStyle HorizontalAlign="left" Width="100px" />
                    <ItemStyle HorizontalAlign="left"/>
                    <ItemTemplate>
                        <asp:LinkButton ID="linkButtonStatus" runat="server"  Text="Activate"
                            CommandName="ChangeStatus">
                        </asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
              <RowStyle />
        <EditRowStyle />
        <SelectedRowStyle  Font-Bold="True" />
        <PagerStyle HorizontalAlign="Left" Font-Bold="True" />
        <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
         <AlternatingRowStyle CssClass="altrow" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
            NextPageText="Next" PreviousPageText="Previous" />
    </asp:GridView>
        
        </div>
    
</asp:Content>
