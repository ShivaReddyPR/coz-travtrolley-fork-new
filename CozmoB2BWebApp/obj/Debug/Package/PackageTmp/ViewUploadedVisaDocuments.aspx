﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ViewUploadedVisaDocuments" Codebehind="ViewUploadedVisaDocuments.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme %>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme %>://www.w3.org/1999/xhtml">
<head runat="server">
<style type="text/css">
    body { font: normal 12px/46px arial, tahoma;}
    .main-div { float:left; border:solid 1px #c0c0c0;width:100%;}
    .main-div h1 { float:left; width:99%;  font-size:15px; font-weight:bold; padding:0; margin:0; border-bottom:1px solid #c0c0c0; padding-left:1%;}
    .main-div .inner-d { float:left; width:96%; padding:2%;}
    .main-div .inner-d div { float:left; width:auto; margin-right:10px;}
    .main-div .inner-d div.right { float:left; width:auto;  }
</style>
    
    
<link href="Style/overlay-popup.css" rel="stylesheet" type="text/css" />
<script src="Scripts/jsBE/ModalPop.js" type="text/javascript"></script>
</head>
<script type="text/javascript">

function newwindow(url)
{
  
 window.open(url,'','toolbar=no,width=900,height=500,resizable=yes,scrollbars=yes',true);

}

function PrintContent(){

  hideAll();
  window.print();
  setTimeout("ShowAll()",200);


//    try{
//   
//    var oIframe = document.getElementById('ifrmPrint');
//    var oContent = document.getElementById('divtoprint').innerHTML;
//    var oDoc = (oIframe.contentWindow || oIframe.contentDocument);
//    if (oDoc.document) oDoc = oDoc.document;
//    oDoc.write("<head><title>Document</title>");
//    oDoc.write('</head><body onLoad="self.print()">');
//    oDoc.write(oContent + "</body>");

//    oDoc.close();

//    }
//    catch(e){
//    
//    self.print();
//    }
}
function hideAll()
{
document.getElementById('btnprint').style.display='none';
document.getElementById('btndownload').style.display='none';
}
function ShowAll()
{
document.getElementById('btnprint').style.display='block';
document.getElementById('btndownload').style.display='block';
}

</script>
<body>
    <form id="form1" runat="server">
    <div class="main-div">
      <% for (int i = 0; i < visaDT.Rows.Count; i++)
        {%>
        <h1> Document Type :  <%= visaDT.Rows[i]["documentType"]%> </h1>
            <div class="inner-d">
            <div id="divtoprint">
            <img alt="Image" src="<%= B2CSiteUrl+visaDT.Rows[i]["documentPath"] %>"  />
            </div>
            <div class="right"> 
            <span  style="float:left;margin-right:8px;"><input id="btnprint" style="font-size:16px" type="button"  onclick="javascript:PrintContent()" value="Print" /></span>
            <span style="float:left;"  >
            <input id="btndownload" type="button" style="font-size:16px"  value="Download" onclick=" javascript:newwindow('DownloadVisaDocument.aspx?paxid='+<%=Request["paxId"]%>+'&documentId='+<%=Request["documentId"] %>);" />
            <%--<a  href="DownloadVisaDocument.aspx"  target="_blank">Download</a>--%></span></div>
            </div>
        <%} %>
    </div>
    </form>
     <iframe id="ifrmPrint" src="#" style="width:0px; height:0px; position:absolute; z-index:-1000; "> </iframe>
    <%-- <span onclick="sm('box',400,40)" class="dialink">modal dialog alert substitute</span>--%>
<%--<div id="box" class="dialog">
<div style="text-align:center"><span id="txt">Press OK to continue.</span><br>
<button onclick="hm('box');okSelected()">OK</button></div>
</div> --%>
</body>
</html>
