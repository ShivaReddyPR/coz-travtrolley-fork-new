﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="SightseeingGUI" Title="Sightseeing Search" Codebehind="Sightseeing.aspx.cs" EnableEventValidation="false" ValidateRequest="false" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    
   
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
   <!-- Here Calling only JavaScript -->
   
   <script type="text/javascript">
       var arrayStates = new Array();
       function invokePage(url, passData) {
           if (window.XMLHttpRequest) {
               AJAX = new XMLHttpRequest();
           }
           else {
               AJAX = new ActiveXObject("Microsoft.XMLHTTP");
           }
           if (AJAX) {
               AJAX.open("POST", url, false);
               AJAX.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
               AJAX.send(passData);
               return AJAX.responseText;
           }
           else {
               return false;
           }
       }

       function getHotelCityList(searchParam) {

           var paramList = 'searchKey=' + searchParam;
           paramList += '&requestSource=' + "HotelSearchDomestic";

           var url = "CityAjax";
           var arrayStates = "";
           var faltoo = invokePage(url, paramList);
           arrayStates = faltoo.split('/');
           if (arrayStates[0] != "") {
               for (var i = 0; i < arrayStates.length; i++) {
                   arrayStates[i] = ["i", arrayStates[i]];
               }

               return arrayStates;
           }
           else return (false);
       }


       function autoCompInit() {
           oACDS3 = new YAHOO.widget.DS_JSFunction(getHotelCityList);
        
           oAutoComp3 = new YAHOO.widget.AutoComplete('city', 'statescontainer3', oACDS3);
           oAutoComp3.prehighlightClassName = "yui-ac-prehighlight";
           oAutoComp3.queryDelay = 0;
           oAutoComp3.minQueryLength = 3;
           oAutoComp3.useIFrame = true;
           oAutoComp3.useShadow = true;

           oAutoComp3.formatResult = function(oResultItem, sQuery) {
               document.getElementById('statescontainer3').style.display = "block";
               var toShow = oResultItem[1].split(',');
               var sMarkup;
               if (toShow[2].length > 0) {
                   sMarkup = toShow[1] + ',' + toShow[2] + ',' + toShow[3];
               }
               else {
                   sMarkup = toShow[1] + ',' + toShow[3];
               }
               //var aMarkup = ["<li>", sMarkup, "</li>"]; 
               var aMarkup = [sMarkup];
               return (aMarkup.join(""));
           };
           oAutoComp3.itemSelectEvent.subscribe(itemSelectHandler3);
       }
       var itemSelectHandler3 = function(sType2, aArgs2) {

           YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
           var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
           var city = oMyAcInstance2[1].split(',');
           document.getElementById('CityCode').value = city[0];
           document.getElementById('CountryName').value = city[3];
          
           document.getElementById('city').value = city[1] + ',' + city[3];
           document.getElementById('statescontainer3').style.display = "none";
           var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
           var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource 
       };
       YAHOO.util.Event.addListener(this, 'load', autoCompInit); //temporarily commented

       function ShowContainer(containerId, inputId) {
var inputId=inputId
           document.getElementById(containerId).style.display = "block";
           var el = document.getElementById(inputId);
           document.getElementById(inputId).focus();
       }
       function foucusOut(textBox, txt) {
           if (textBox.value == "") {
               textBox.value = txt;
           }
       }
       function focusIn(textBox, txt) {
           if (textBox.value == txt) {
               textBox.value = "";
           }
       }


       function ShowChildAge(number) {
           var childCount = eval(document.getElementById('chdRoom-' + number).value);
           var PrevChildCount = eval(document.getElementById('PrevChildCount-' + number).value);
           document.getElementById('ChildBlock-' + number + '-ChildAge-1').selectedIndex  = '0';
           if (eval(document.getElementById('chdRoom-1').value) > 0) {
               document.getElementById('childDetails').style.display = 'block';
           }
           else {
                
               document.getElementById('childDetails').style.display = 'none';
           }
           if (childCount > PrevChildCount) {
               document.getElementById('ChildBlock-' + number).style.display = 'block';
               for (var i = (PrevChildCount + 1); i <= childCount; i++) {
                   document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'block';
                   $('#ChildBlock-' + number + '-ChildAge-' + i).select2('val', '0');
               }
           }
           else if (childCount < PrevChildCount) {
               if (childCount == 0) {
                    $('#ChildBlock-' + number + '-ChildAge-1').select2("val","0");
                   $('#ChildBlock-' + number + '-ChildAge-2').select2("val","0");
                   $('#ChildBlock-' + number + '-ChildAge-3').select2("val","0");
                   $('#ChildBlock-' + number + '-ChildAge-4').select2("val","0");
                   $('#ChildBlock-' + number + '-ChildAge-5').select2("val","0");
                   document.getElementById('ChildBlock-' + number).style.display = 'none';
                  
                   document.getElementById('ChildBlock-' + number + '-Child-1').style.display = 'none';
                   document.getElementById('ChildBlock-' + number + '-Child-2').style.display = 'none';
                   document.getElementById('ChildBlock-' + number + '-Child-3').style.display = 'none';
                   document.getElementById('ChildBlock-' + number + '-Child-4').style.display = 'none';
                   document.getElementById('ChildBlock-' + number + '-Child-5').style.display = 'none';
                   
               }
               else {
                   for (var i = PrevChildCount; i > childCount; i--) {
                       if (i != 0) {
                           document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'none';
                           document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                       }
                   }
               }
           }
           document.getElementById('PrevChildCount-' + number).value = childCount;
       }

       var call1;
       function init1() {
           var today = new Date();
           // Rendering Cal1
           call1 = new YAHOO.widget.CalendarGroup("call1", "fcontainer1");
           call1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
//           call1.cfg.setProperty("title", "Select your desired departure date:");
           call1.cfg.setProperty("close", true);
           call1.selectEvent.subscribe(setDate1);
           call1.render();
       }
       function showCalendar1() {
           init1();
           document.getElementById('fcontainer1').style.display = "block";
       }
       function setDate1() {
           var date1 = call1.getSelectedDates()[0];
           this.today = new Date();
           var thisMonth = this.today.getMonth();
           var thisDay = this.today.getDate();
           var thisYear = this.today.getFullYear();
           var todaydate = new Date(thisYear, thisMonth, thisDay);
           var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
           var difference = (depdate.getTime() - todaydate.getTime());
           if (difference < 0) {
               document.getElementById('errMess').style.display = "block";
               document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
               return false;
           }
           document.getElementById('errMess').style.display = "none";
           document.getElementById('errorMessage').innerHTML = "";

           var month = date1.getMonth() + 1;
           var day = date1.getDate();

           if (month.toString().length == 1) {
               month = "0" + month;
           }
           if (day.toString().length == 1) {
               day = "0" + day;
           }
           document.getElementById('<%=CheckIn.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
           call1.hide();
       }
       YAHOO.util.Event.addListener(window, "load", init1);

       function showStuff(id) {
           document.getElementById(id).style.display = 'block';
       }


       function hidestuff(boxid) {
           document.getElementById(boxid).style.display = "none";
       }
      
       function validateSearch() {
           if (document.getElementById('city').value == '' || document.getElementById('city').value == 'Enter city name') {
               document.getElementById('errMess').style.display = "block";
               document.getElementById('errorMessage').innerHTML = "Please Enter a Destination City.";
               return false;
           }
           if (document.getElementById('<%=radioAgent.ClientID %>') != null && document.getElementById('<%=radioAgent.ClientID %>').checked == true
                    && document.getElementById('<%=ddlAgents.ClientID %>').value == "0")
                {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Client !";
                    return false;
           }
           else if (document.getElementById('<%=radioAgent.ClientID %>') != null && document.getElementById('<%=radioAgent.ClientID %>').checked == true
                    && document.getElementById('<%=ddlAgentsLocations.ClientID %>').value == "-1")
                {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Location !";
                    return false;
                }
           var nationality = document.getElementById('<%=ddlNationality.ClientID %>');
            if (nationality.options[nationality.selectedIndex].value == "Select Nationality") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select your Nationality";
                return false;
            }
           var date1 = document.getElementById('<%= CheckIn.ClientID %>').value;
           this.today = new Date();
           var thisMonth = this.today.getMonth();
           var thisDay = this.today.getDate();
           var thisYear = this.today.getFullYear();

           var todaydate = new Date(thisYear, thisMonth, thisDay);
           if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
               document.getElementById('errMess').style.display = "block";
               document.getElementById('errorMessage').innerHTML = "Please Select Check In Date";
               return false;
           }
           var depDateArray = date1.split('/');

           // checking if date1 is valid
           if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
               document.getElementById('errMess').style.display = "block";
               document.getElementById('errorMessage').innerHTML = " Invalid Check In Date";
               return false;
           }
           if (document.getElementById('chdRoom-1').selectedIndex > 0) {
               var childs = document.getElementById('chdRoom-1').value;
               for (i = 1; i <= childs; i++) {
                   var childage = $('#ChildBlock-1-ChildAge-' + i).val();
                   var childage1= $('#ChildBlock-1-ChildAge-1').val("val","2");
                   if ($('#ChildBlock-1-ChildAge-' + i).val() == 0) {
                       document.getElementById('errMess').style.display = "block";
                       document.getElementById('errorMessage').innerHTML = "Please Select child " + i + " age";
                       return false;
                   }
               }
           }

           document.getElementById('divPrg').style.display = 'none';

           document.getElementById('PreLoader').style.display = "block";
           document.getElementById('sightseeing_main_inner').style.display = "none";
           document.getElementById('searchCity').innerHTML = document.getElementById("city").value;
           document.getElementById('fromDate').innerHTML = document.getElementById('<%=CheckIn.ClientID %>').value;
           document.getElementById('fromDate').style.fontWeight = "bold";
           document.getElementById('fromDate').style.fontSize = "18px";
           return true;
       }
       function SetModifyValues() {
           var adults = '<%=adults %>';
           var childs = '<%=childs %>';
           var childAges = '<%=childAges %>';
           childAges = childAges.split(',');
           document.getElementById('adtRoom-' + 1).value = adults;
           document.getElementById('chdRoom-' + 1).value = childs;
           ShowChildAge(1);
           for (i = 1; i <= childs; i++) {
               document.getElementById('ChildBlock-1-ChildAge-' + i).value = childAges[i - 1];
           }
       }
       function SelectAll() {
           var chkControlId = document.getElementById('<%=chkList.ClientID%>');
           var options = chkControlId.getElementsByTagName('input');
           for (i = 0; i < options.length; i++) {
               var opt = options[i];
               if (opt.type == "checkbox") {
                   opt.checked = true;

               }
           }
       }
       function UnSelectAll() {
           var chkControlId = document.getElementById('<%=chkList.ClientID%>');
           var options = chkControlId.getElementsByTagName('input');
           for (i = 0; i < options.length; i++) {
               var opt = options[i];
               if (opt.type == "checkbox") {
                   opt.checked = false;

               }
           }
       }

       //Start Isbehaf Agent
           function disablefield() {
               if (document.getElementById('<%=radioAgent.ClientID %>').checked == true) {
                document.getElementById('<%=hdnBookingAgent.ClientID %>').value = "Checked";
                document.getElementById('<%=wrapper1.ClientID%>').style.display = "block";
                document.getElementById('<%=wrapper2.ClientID%>').style.display = "block";
            }
            if (document.getElementById('<%=radioSelf.ClientID%>').checked == true) {
                document.getElementById('<%=wrapper1.ClientID%>').style.display = "none";
                document.getElementById('<%=wrapper2.ClientID%>').style.display = "none";
            }
       }

       var Ajax;
        var loc;
        function LoadAgentLocations(id, type) {
            debugger;
            var location = 'ctl00_cphTransaction_ddlAgentsLocations';
            $("#<%=ddlAgentsLocations.ClientID%>").empty();
                $("#<%=ddlAgentsLocations.ClientID%>").select2().attr('text', '');
                if (document.getElementById('<%=radioAgent.ClientID %>').checked == true
                    && document.getElementById('<%=ddlAgents.ClientID %>').value == "0") {

                        if (ddl != null) {
                            ddl.options.length = 0;
                            var el = document.createElement("option");
                            el.textContent = "Select Location";
                            el.value = "-1";
                            ddl.add(el, 0);
                            var values = ""
                            var el = document.createElement("option");
                            el.textContent = "";
                            el.value = "";
                            ddl.appendChild(el);
                        }

                        $("#<%=ddlAgentsLocations.ClientID%>").empty();
                    }

                    else {
                        var location = 'ctl00_cphTransaction_ddlAgentsLocations';
                        loc = location;
                        var sel = document.getElementById('<%=ddlAgents.ClientID %>').value;

                var paramList = 'requestSource=getAgentsLocationsByAgentId' + '&AgentId=' + sel + '&id=' + location;;
                var url = "CityAjax";

                if (window.XMLHttpRequest) {
                    Ajax = new XMLHttpRequest();
                }
                else {
                    Ajax = new ActiveXObject("Microsoft.XMLHTTP");
                }
                Ajax.onreadystatechange = BindLocationsList;
                Ajax.open('POST', url);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);
            }

       }

       function BindLocationsList() {
            var ddl = document.getElementById(loc);
            if (Ajax.readyState == 4 && Ajax.status == 200 && Ajax.responseText.length > 0) {
                if (ddl != null) {
                    ddl.options.length = 0;
                    var el = document.createElement("option");
                    el.textContent = "Select Location";
                    el.value = "-1";
                    ddl.add(el, 0);
                    //routing agent Id|location name#location id
                    var values = Ajax.responseText.split('#')[1].split(',');
                    for (var i = 0; i < values.length; i++) {
                        var opt = values[i];
                        if (opt.length > 0 && opt.indexOf('|') > 0) {
                            var el = document.createElement("option");
                            el.textContent = opt.split('|')[0];
                            el.value = opt.split('|')[1];
                            ddl.appendChild(el);
                        }
                    }
                    $('#' + ddl.id).select2('val', '-1');

                }
            }

            if (document.getElementById('<%=hdnLocation.ClientID%>').value != "") {
                var val = document.getElementById('<%=hdnLocation.ClientID%>').value;
                $('#ctl00_cphTransaction_ddlAgentsLocations').select2('val', val);
            }

       }
       function GetSelectedVal() {
            document.getElementById('<%=hdnLocation.ClientID%>').value = document.getElementById('ctl00_cphTransaction_ddlAgentsLocations').value;

        }

</script>



   
   <!--End JavaScript Here -->
   
   
   <!-- Here Calling Sightseeing Search Functionality -->
   
 <div id="sightseeing_main_inner">
<%--<form id="form1" runat="server">--%>
                       
    <div class="clear" style="margin-left: 25px">
            <div id="fcontainer1" style="position: absolute; top:149px; left: 30%; display: none; z-index:9999;">
               <%--<a onclick="document.getElementById('fcontainer1').style.display = 'none'">
                <img style="cursor: pointer; position: absolute; z-index: 300; top: -12px; right: -10px;"
                    src="images/cross_icon.png" />
            </a>--%>
            </div>
        </div>
     
     
  <div class="search_container"> 
  
  
  <div class="col-md-12"> 

<div class="col-md-12 marbot_10"> <h3> Sightseeing</h3></div>
  
  <div class="col-md-12" id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="color: Red;" class="padding-4 yellow-back width-300 center margin-top-4">
            </div>
        </div>
      <asp:HiddenField ID="hdnLocation" runat="server" Value="" />
      <asp:HiddenField ID="hdnBookingAgent" runat="server" />
       <div id="divContainer" class="row">
        <div class="col-4">
            <div class="row custom-gutter">
                <div class="col-md-6 col-xs-6">
                    <asp:RadioButton ID="radioSelf" CssClass="mt-3 custom-radio-table" GroupName="ActivityRequest" Checked="true" Text="For Self" onclick="disablefield();" runat="server" />
                </div>
                <div class="col-md-6 col-xs-6">
                    <asp:RadioButton ID="radioAgent" CssClass="mt-3 custom-radio-table" Text="Client" GroupName="ActivityRequest" runat="server" onclick="disablefield();" />
                </div>
            </div>
        </div>
             <div class="col-6">
            <div class="row custom-gutter">
                <div class="col-md-6 col-xs-6" id="wrapper1" style="display: none" runat="server">
                   
                    <asp:DropDownList ID="ddlAgents" AppendDataBoundItems="true" CssClass="form-control" runat="server" onchange="LoadAgentLocations(this.id,'F');">
                    </asp:DropDownList>
                </div>

                <div class="col-md-6 col-xs-6" id="wrapper2" style="display: none" runat="server">
                   
                    <asp:DropDownList ID="ddlAgentsLocations" CssClass="form-control" runat="server" onchange="GetSelectedVal();">
                    </asp:DropDownList>
                </div>
            </div>
                 
        </div>
           
             </div>
        
        
        
        
        
        
        <tr> 

<td height="24" colspan="2"> </td>
</tr>







      <div class="col-md-12 padding-0 marbot_10">                                    

  
    <div class="col-md-4">
    <div> Destination City</div>
    
    <div>  <input class="form-control" type="text" autocomplete="off" id="city" name="city"  value="<%=cityName%>" onblur="foucusOut(this, '<%=cityName%>')" onfocus="focusIn(this, '<%=cityName%>')"  onclick="ShowContainer('statescontainer3' , 'city')" />       
    
    
    
     
    
    
    
       <%--<asp:TextBox ID="city" runat="server" CssClass="inp_22_1" Text=""></asp:TextBox>--%>
                    <input type="hidden" id="cityNames" name="cityNames" value="" />
                    <input type="hidden" id="CityCode" name="CityCode"  value="<%=requestObj.DestinationCode%>"  />
                    <input type="hidden" id="CountryName" name="CountryName" value="<%=requestObj.CountryName%>" />
                  </div>
<div id="statescontainer3"   style="width:300px; line-height:30px; color:#000;  left:0; position:absolute; display:none; z-index: 9999;" >
 
 
 
 
 

 </div>
 
     </div>
     
    <div class="col-md-2">
        <div> Nationality
        </div>    
        <div> 
               
            <asp:DropDownList CssClass="form-control"   ID="ddlNationality" runat="server"
            DataTextField="CountryName" DataValueField="CountryCode"  
            AppendDataBoundItems="True">
            <asp:ListItem Selected="True">Select Nationality</asp:ListItem>
            </asp:DropDownList>     
        </div>
    </div>
     
    <div class="col-md-2 martop_xs10">
    <div> Tour Date</div>
    
    
    <div> 
    <table border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td valign="top">
            <asp:TextBox ID="CheckIn" runat="server" Width="110px" CssClass="form-control"></asp:TextBox>
            </td>
            <td>
                <a href="javascript:void(null)" onclick="showCalendar1()">
                    <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date"/>
                </a>
            </td>
    </tr>
        </table>
    
    
    </div>
    
    
     </div>
     
     
     
     
      <div class="col-md-2 col-xs-5 martop_xs10"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>Adults(18+Yrs)</td>
          </tr>
          <tr>
            <td><select class="form-control" name="adtRoom-1" id="adtRoom-1">
                                    <option selected="selected" value="1">1</option>
                                    <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                    <option value="5">5</option>
                                    <option value="6">6</option>
                                    <option value="7">7</option>
                                    <option value="8">8</option>
                                    <option value="9">9</option>
                                    
                                    
                                </select>
                                   <input type="hidden" value="0" id="PrevChildCount-1" name="PrevChildCount-1"/></td>
          </tr>
      </table> </div>
      
      
          <div class="col-md-2 col-xs-7 martop_xs10"> <table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td>Children (till 18 yrs)</td>
          </tr>
          <tr>
            <td><select class="form-control" onChange="javascript:ShowChildAge('1')" name="chdRoom-1" id="chdRoom-1">
                                   <option selected="selected" value="0">none</option>
                                   <option value="1">1</option>
                                   <option value="2">2</option>
                                    <option value="3">3</option>
                                    <option value="4">4</option>
                                     <option value="5">5</option>
                                </select></td>
          </tr>
      </table> </div>  
      
      
      
      


    <div class="clearfix"></div>
    </div>
  
     
     
        <div class="col-md-12 padding-0 marbot_10">    
        
         <div>
     <asp:DropDownList ID="ddlPlus" runat="server" Width="50px" Visible="false">
<asp:ListItem Text="0" Value="0" Selected="True"></asp:ListItem>
<asp:ListItem Text="1" Value="1"></asp:ListItem>
<asp:ListItem Text="2" Value="2"></asp:ListItem>
<asp:ListItem Text="3" Value="3"></asp:ListItem>
<asp:ListItem Text="4" Value="4"></asp:ListItem>
<asp:ListItem Text="5" Value="5"></asp:ListItem>
<asp:ListItem Text="6" Value="6"></asp:ListItem>
<asp:ListItem Text="7" Value="7"></asp:ListItem>
</asp:DropDownList>
       
       
                
                    <div id="room-1" >
    
    
    <div  id="ChildBlock-1" name="ChildBlock-1" style="display:none;">
                           
                         
<div class="col-md-1 col-xs-6" id="ChildBlock-1-Child-1" name="ChildBlock-1-Child-1" style="display:none">
<span class="f_size10">Child 1</span><br /> <select class="form-control" id="ChildBlock-1-ChildAge-1" name="ChildBlock-1-ChildAge-1">
                                        <option selected="selected" value="0">Age?</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                        <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                        
                                    </select>
                                  
                                  </div>
                                  
<div class="col-md-1 col-xs-6" id="ChildBlock-1-Child-2" name="ChildBlock-1-Child-2" style="display:none;">
<span class="f_size10">Child 2</span><br />
 <select class="form-control" id="ChildBlock-1-ChildAge-2" name="ChildBlock-1-ChildAge-2" onChange="javascript:ShowChildAge('1')">
                                       <option selected="selected" value="0">Age?</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                         <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                    </select>
                               
                                 </div>
                                 
            
<div class="col-md-1 col-xs-6" id="ChildBlock-1-Child-3" name="ChildBlock-1-Child-3" style="display:none;">
<span class="f_size10">Child 3</span><br />
 <select class="form-control" id="ChildBlock-1-ChildAge-3" name="ChildBlock-1-ChildAge-3" onChange="javascript:ShowChildAge('1')">
                                       <option value="0" selected="selected">Age?</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                         <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                    </select>
                               
                                 </div>                
                                 
      
      
      
      
<div class="col-md-1 col-xs-6" id="ChildBlock-1-Child-4" name="ChildBlock-1-Child-4" style="display:none;">
<span class="f_size10">Child 4</span><br />
 <select class="form-control" id="ChildBlock-1-ChildAge-4" name="ChildBlock-1-ChildAge-4" onChange="javascript:ShowChildAge('1')">
                                       <option value="0" selected="selected">Age?</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                         <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                    </select>
                               
                                 </div>
      
      
      
<div class="col-md-1 col-xs-6" id="ChildBlock-1-Child-5" name="ChildBlock-1-Child-5" style="display:none; ">
<span class="f_size10">Child 5</span><br />
 <select class="form-control" id="ChildBlock-1-ChildAge-5" name="ChildBlock-1-ChildAge-5" onChange="javascript:ShowChildAge('1')">
                                       <option value="0" selected="selected">Age?</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                        <option value="10">10</option>
                                        <option value="11">11</option>
                                         <option value="12">12</option>
                                        <option value="13">13</option>
                                        <option value="14">14</option>
                                        <option value="15">15</option>
                                        <option value="16">16</option>
                                        <option value="17">17</option>
                                        <option value="18">18</option>
                                    </select>
                               
                                 </div>
                             
                       </div>
    
    
   
   </div>
                        
                        <div id="childDetails" style="display:none;">
                      <!--  <p>Child Details</p>-->
                    </div>  
       
       
       </div>
        
        
  
   <div class="clearfix"></div>
    </div>
  
  
  
  
  
   <div class="col-md-12 padding-0 marbot_10">    
 
  <div class="col-md-2" style="display:none"> <b> Sightseeing Type:</b>      </div> 
  
  <div class="col-md-10 martop_xs10" style="display:none"> <a class="fcol_fff cursor_point" onclick="SelectAll()">Select All</a>
  
  
   <a class="fcol_fff margin-left-10 cursor_point" onclick="UnSelectAll()">Un Select All</a></div>  
   
   
   
     <div class="clearfix"></div>
  </div> 
   
   
  <div class="col-md-12 padding-0 marbot_10">    
 
  <div class="col-md-12" style="display:none"> <asp:CheckBoxList RepeatColumns="8" CssClass="chklist2" ID="chkList" runat="server" RepeatDirection="Vertical" RepeatLayout="Flow">
<asp:ListItem Text="Air Sightseeing" Value="AR"></asp:ListItem>
<asp:ListItem Text="Attractions" Value="AT"></asp:ListItem>
<asp:ListItem Text="Hop On, Hop Off" Value="HO"></asp:ListItem>
<asp:ListItem Text="Show/Concerts" Value="SM"></asp:ListItem>
<asp:ListItem Text="Shuttle" Value="ST"></asp:ListItem>
<asp:ListItem Text="Sightseeing Cruises" Value="CR"></asp:ListItem>
<asp:ListItem Text="Sightseeing Tours" Value="RS"></asp:ListItem>
<asp:ListItem Text="Tourists Pass" Value="TP"></asp:ListItem>
</asp:CheckBoxList>
 </div>
  
  
  
  
  
    <div class="clearfix"></div>
  </div> 
  
  
  
      <div class="col-md-12 padding-0 marbot_10 martop_xs10">                                    

  
    <div class="col-md-3"> 
    
    <div style="display:none"> Sightseeing Category</div>
    
    
    <div style="display:none"> <asp:DropDownList ID="ddlCategory" CssClass="form-control" runat="server">
            <asp:ListItem Value="All" Text="All" Selected="True"></asp:ListItem>
            <asp:ListItem Value="FW" Text="4WD Tours"></asp:ListItem>
            <asp:ListItem Value="AV" Text="Adventure"></asp:ListItem>
            <asp:ListItem Value="AL" Text="Airport Lounges"></asp:ListItem>
            <asp:ListItem Value="CL" Text="Arts & Culture"></asp:ListItem>
            <asp:ListItem Value="BE" Text="Beauty/Spa/Massage"></asp:ListItem>
            <asp:ListItem Value="CS" Text="City Sightseeing"></asp:ListItem>
            <asp:ListItem Value="CM" Text="Cruise & Meal"></asp:ListItem>
            <asp:ListItem Value="CU" Text="Culinary"></asp:ListItem>
            <asp:ListItem Value="CY" Text="Cycling"></asp:ListItem>
            <asp:ListItem Value="EC" Text="Eco-Tours"></asp:ListItem>
            <asp:ListItem Value="EV" Text="Evening Tour"></asp:ListItem>
            <asp:ListItem Value="EX" Text="Excursion"></asp:ListItem>
            <asp:ListItem Value="FM" Text="Family Attraction"></asp:ListItem>
            <asp:ListItem Value="FA" Text="Fashion"></asp:ListItem>
            <asp:ListItem Value="GI" Text="General Interest"></asp:ListItem>
            <asp:ListItem Value="HE" Text="Helicopter Tour"></asp:ListItem>
            <asp:ListItem Value="HI" Text="Historical"></asp:ListItem>
            <asp:ListItem Value="HT" Text="Horticulture"></asp:ListItem>
            <asp:ListItem Value="HB" Text="Hot Air Ballooning"></asp:ListItem>
            <asp:ListItem Value="KA" Text="Kayaking/Canoeing"></asp:ListItem>
            <asp:ListItem Value="MI" Text="Military"></asp:ListItem>
            <asp:ListItem Value="MU" Text="Music"></asp:ListItem>
            <asp:ListItem Value="NA" Text="Nature"></asp:ListItem>
            <asp:ListItem Value="PT" Text="Premium Tours"></asp:ListItem>
            <asp:ListItem Value="RT" Text="Recommended Tours"></asp:ListItem>
            <asp:ListItem Value="RE" Text="Religious"></asp:ListItem>
            <asp:ListItem Value="RB" Text="Restaurants & Bars"></asp:ListItem>
            <asp:ListItem Value="3D" Text="RSSC_3D"></asp:ListItem>
            <asp:ListItem Value="AU" Text="RSSC_AU"></asp:ListItem>
            <asp:ListItem Value="BA" Text="RSSC_BA"></asp:ListItem>
            <asp:ListItem Value="CC" Text="RSSC_CC"></asp:ListItem>
            <asp:ListItem Value="GP" Text="RSSC_GP"></asp:ListItem>
            <asp:ListItem Value="QU" Text="RSSC_QU"></asp:ListItem>
            <asp:ListItem Value="SL" Text="Sailing"></asp:ListItem>
            <asp:ListItem Value="SF" Text="Scenic Flights"></asp:ListItem>
            <asp:ListItem Value="TS" Text="Scenic Train"></asp:ListItem>
            <asp:ListItem Value="SC" Text="Science"></asp:ListItem>
            <asp:ListItem Value="SH" Text="Shopping"></asp:ListItem>
            <asp:ListItem Value="DS" Text="Show/Concert & Meal"></asp:ListItem>
            <asp:ListItem Value="ST" Text="Shuttle"></asp:ListItem>
            <asp:ListItem Value="SP" Text="Sport"></asp:ListItem>
            <asp:ListItem Value="SA" Text="Submarine"></asp:ListItem>
            <asp:ListItem Value="SS" Text="Sunrise/Sunset Tour"></asp:ListItem>
            <asp:ListItem Value="TH" Text="Theme Park"></asp:ListItem>
            <asp:ListItem Value="TR" Text="Transport"></asp:ListItem>
            <asp:ListItem Value="WA" Text="Walking/Trekking"></asp:ListItem>
            <asp:ListItem Value="WR" Text="Water Recreation"></asp:ListItem>
            <asp:ListItem Value="WS" Text="Water Sports"></asp:ListItem>
            <asp:ListItem Value="WD" Text="Weddings"></asp:ListItem>
            <asp:ListItem Value="WT" Text="Wine Tasting"></asp:ListItem>
        </asp:DropDownList></div>
    
    </div>
   
    <div class="col-md-3 hidden-xs" style="display:none">
    <div>  Name or Keyword </div>
    
    <div> <asp:TextBox ID="txtNameKey"  runat="server" CssClass="form-control"></asp:TextBox></div>
    
      </div>
    <div class="col-md-6 martop_xs10"> 
    
    <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="but but_b pull-right" OnClientClick="return validateSearch();" OnClick="btnSearch_Click" />
    
     </div>

    <div class="clearfix"></div>
    </div>
    

  
  
  
  </div>
  
  
  
  <div class="clearfix"></div>
  </div>   
     
     
     
   
   
   
     <div class="col-md-12 padding-0 martop_14"> 
        
     
     
    <div class="col-md-3 pad_left0"> 
                
            <img width="100%" style=" border: solid 1px #fff;"  src="images/Deal_daily.jpg" />  
                
                
                 
                 </div>   
        
        
        
        
        <div class="col-md-9 padding-0 hidden-xs"> 
                <div class="inn_wrap01">
             
             <div class="ns-h3">  Notes</div>
                    
                <div style=" min-height:267px" class=" pad_10"> 
<div class="col-md-12">       Avail our private transfers (supplement). You can book it under "Transfers and Shuttles" (private transfers). 
                 
                 <br /> <br />
                 If the restaurant is busy you may have to wait for a table to become available before you can be seated. Please present your voucher upon arrival at the restaurant. </div>    
                        
                 
                 </div>
                 
                 </div>           
                        
     
     
     
     
     
        
        </div> 
        
          

   
   
   
   </div>  





 <%if (Page.PreviousPage != null && Page.PreviousPage.IsCrossPagePostBack)
   {
       if (Page.PreviousPage.Title == "Sightseeing Search Results")
       { %>

    <script>

        SetModifyValues();
        
    </script>

    <%}
   }
          
    %>
<%--</form>--%>
</div>
   
   <!-- End Sightseeing search Functionality -->
   
    <div id="PreLoader" style="display: none">
        <div class="loadingDiv">
            <div>
                <img src="images/preloader11.gif" /></div>
            <div style="font-size: 16px; color: #999999">
                <strong>Awaiting For Sightseeing Search Results</strong>
            </div>
            <div class="primary-color" style="font-size: 21px;">
                <strong><span id="searchCity">
                   <%-- <%=requestObj.CityName%>, <%=requestObj.CountryName%>--%></span> </strong>
                <br />
                <strong style="font-size: 14px; color: black"></strong>
            </div>
        </div>
        <div class="parameterDiv">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td style="font-size:18px" width="50%">
                       
                            <strong >Tour Date:</strong>
                        <span class="primary-color" id="fromDate">
                        
                            <%--<%=requestObj.TourDate.ToString("dd/MM/yyyy")%><--%></span>
                    </td>
                    <td width="50%">
                        <%--<label class="parameterLabel">
                            <strong>Check out:</strong></label>
                        <span id="toDate"><%=itinerary.EndDate.ToString("dd/MM/yyyy") %></span>--%>
                    </td>
                </tr>
            </table>
        </div>
    </div> 
   
</asp:Content>


