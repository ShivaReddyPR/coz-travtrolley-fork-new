﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="DisplayPNRInformationGUI" Title="PNR Information" Codebehind="DisplayPNRInformation.aspx.cs" %>

<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />
<style>

    .form-control::-webkit-input-placeholder { color: lightgray; }
    .form-control::-moz-placeholder { color: lightgray; }
    .form-control:-ms-input-placeholder { color: lightgray; }

    .disabled { color: #ccc; pointer-events:none; }

</style>
<script src="scripts/jquery-ui.js"></script>
<script src="Scripts/Common/common.js"></script>
<script src="Scripts/Common/FlexFields.js"></script>

<script type="text/javascript">

    $(document).ready(function () {

        /* Check if there is any price changes for existing non ticketed PNR on page load */
        Pricechangealert();

        /* To bind flex fields on page load */
        if (document.getElementById('<%=hdnFlexInfo.ClientID %>').value != '')
            BindFlexFields(document.getElementById('<%=hdnFlexInfo.ClientID %>').value, document.getElementById('<%=hdnFlexConfig.ClientID %>').value, 'divFlex', 'divFlexFields', '1', document.getElementById('<%=hdnTravelReason.ClientID %>').value);

        /* To enable date picker calender icon for flex fields */
        $(".EnableCal").datepicker({ dateFormat: 'dd/mm/yy' });
    });

    function ShowPreloader() {

        if (ValidateFlex(document.getElementById('<%=hdnPaxFlexInfo.ClientID %>').id, false)) {
                        
            document.getElementById('MainDiv').style.display = "none";
            document.getElementById('PreLoader').style.display = "block";        
            return true;
        }
        else
            return false;
    }

    /* To alert price change info and enable flags to update the changed info */
    function Pricechangealert() {

        var Segchange = 'N';
        var RePrice = '<% = itinerary.Passenger.Sum(x => x.Price.PublishedFare + x.Price.Tax + x.Price.AsvAmount +
                                    x.Price.BaggageCharge + x.Price.OtherCharges + x.Price.DynamicMarkup + x.Price.HandlingFeeAmount + x.Price.MealCharge +
                                    x.Price.Markup + x.Price.OutputVATAmount + x.Price.SeatPrice - x.Price.Discount).ToString("N" + agency.DecimalValue)%>';
        var OrigPrice = 0;

        <% if (clsFLITDB.Passenger != null) { %>

        OrigPrice = '<% = clsFLITDB.Passenger.Sum(x => x.Price.PublishedFare + x.Price.Tax + x.Price.AsvAmount +
                                   x.Price.BaggageCharge + x.Price.OtherCharges + x.Price.DynamicMarkup + x.Price.HandlingFeeAmount + x.Price.MealCharge +
                                   x.Price.Markup + x.Price.OutputVATAmount + x.Price.SeatPrice - x.Price.Discount).ToString("N" + agency.DecimalValue)%>';

                <% for (int i = 0; i < clsFLITDB.Segments.Length; i++)
                   {%>
                        if (Segchange == 'N') {

                            <% if (clsFLITDB.Segments[i].DepartureTime != itinerary.Segments[i].DepartureTime || 
                                    clsFLITDB.Segments[i].ArrivalTime != itinerary.Segments[i].ArrivalTime || 
                                    clsFLITDB.Segments[i].BookingClass != itinerary.Segments[i].BookingClass ||
                                    clsFLITDB.Segments[i].CabinClass != itinerary.Segments[i].CabinClass) {%>

                                Segchange = 'Y';
                            <%}%>
                        }
                        
                <%}%>
        <% }%>
              
        document.getElementById('<%=hdnSegChange.ClientID%>').value = Segchange;
        if (OrigPrice != '0' && RePrice != '0' && OrigPrice != RePrice) {

            document.getElementById('<%=hdnPriceChange.ClientID%>').value = 'Y';
            document.getElementById('lblOrigPrice').innerHTML = OrigPrice;
            document.getElementById('lblNewPrice').innerHTML = RePrice;
            $('#divRepriceAlert').modal('show');
        }      
    }
</script>
<%--<form id="form1" action="DisplayPNRInformation.aspx" runat="server">--%>
<div id="MainDiv">
<% if (itinerary != null && !string.IsNullOrEmpty(itinerary.PNR) && string.IsNullOrEmpty(errorMessage))
   { %>
   
<div>


    <div class="col-md-12 padding-0 marbot_10">                              

         
    <div class="col-md-4"><div>
            <span>PNR: <b> 
                <% = itinerary.PNR%>
            </b></span>
        </div> </div>
    
    
    <div class="col-md-4">  Booking on behalf of
            <b> <%= agency.Name%></b> </div>
            
            
    <div class="col-md-4"> 
     Origin ( <%= itinerary.Segments[0].Origin.CityName%> ) To Destination (<%= itinerary.Segments.ToList().Last(s => s.Group == 0).Destination.CityName%>)
    
    </div>



    <div class="clearfix"></div>
    </div>
    
   
 
    <div class="col-md-12 padding-0 marbot_10">      
    
    <div class="col-md-8"> 
    
    
   <div  class="bg_white"> 
     
 <div class="ns-h3">Flight Details </div>  
<div class="table-responsive"> <table class="table">
                                        <tr>
                                            <th>
                                                Flight#</th>
                                            <th>
                                                Origin</th>
                                            <th>
                                                Destination</th>
                                            <th>
                                                Class</th>
                                            <th>
                                                Dep Date Time</th>
                                            <th>
                                                Arr Date Time</th> 
                                        </tr>
                                        <%  for (int i = 0; i < itinerary.Segments.Length; i++)
                                            {
                                                if (itinerary.Segments[i] != null)
                                                { 
                                                 %>
                                        <tr>
                                            <td>
                                                <%= itinerary.Segments[i].Airline%>
                                                -
                                                <%= itinerary.Segments[i].FlightNumber%>
                                            </td>
                                            <td>
                                                <%= itinerary.Segments[i].Origin.AirportCode%>
                                                ,
                                                <%= itinerary.Segments[i].Origin.CityName%>
                                            </td>
                                            <td>
                                                <%= itinerary.Segments[i].Destination.AirportCode%>
                                                ,
                                                <%= itinerary.Segments[i].Destination.CityName%>
                                            </td>
                                            <%if (clsFLITDB != null && clsFLITDB.Segments != null && clsFLITDB.Segments[i] != null && 
                                                    clsFLITDB.Segments[i].BookingClass != itinerary.Segments[i].BookingClass) {%>
                                            <td style="color:red;">
                                                <%= itinerary.Segments[i].BookingClass%>
                                            </td>
                                            <%}else { %>
                                            <td>
                                                <%= itinerary.Segments[i].BookingClass%>
                                            </td>
                                            <%}%>
                                            <%if (clsFLITDB != null && clsFLITDB.Segments != null && clsFLITDB.Segments[i] != null && 
                                                    clsFLITDB.Segments[i].DepartureTime != itinerary.Segments[i].DepartureTime) {%>
                                            <td style="color:red;">
                                                <%= itinerary.Segments[i].DepartureTime.ToString("dd/MM/yyyy - hh:mm tt")%>
                                            </td>
                                            <%}else { %>
                                            <td>
                                                <%= itinerary.Segments[i].DepartureTime.ToString("dd/MM/yyyy - hh:mm tt")%>
                                            </td>
                                            <%}%>
                                            <%if (clsFLITDB != null && clsFLITDB.Segments != null && clsFLITDB.Segments[i] != null && 
                                                    clsFLITDB.Segments[i].ArrivalTime != itinerary.Segments[i].ArrivalTime) {%>
                                            <td style="color:red;">
                                                <%= itinerary.Segments[i].ArrivalTime.ToString("dd/MM/yyyy - hh:mm tt")%>
                                            </td>
                                            <%}else { %>
                                            <td>
                                                <%= itinerary.Segments[i].ArrivalTime.ToString("dd/MM/yyyy - hh:mm tt")%>
                                            </td>
                                            <%}%>                                       
                                        </tr>
                                        <%  }
                                            } %>
                                    </table></div>
 
 
 <div class="clearfix"> </div>
 
 </div>
    
    </div>
    
    
        <div class="col-md-4">
            <div class="ns-h3">
                <span>Sale Summary</span>
            </div>
            <asp:DataList ID="dlSaleSummaryDetails" runat="server" Width="100%" OnItemDataBound="dlSaleSummaryDetails_ItemDataBound">
                <ItemTemplate>


                    <div class="bg_white marbot_20 pad_10">



                        <table style="width: 100%;" border="0" cellspacing="0" cellpadding="0">


                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td>
                                                <span class="fleft">Base Fare </span>
                                            </td>
                                            <td align="right">
                                                <span class=" text-right" >
                                                    <asp:Label ID="lblBaseFare" runat="server" Text=""></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class=" width-50">Tax</span>
                                            </td>
                                            <td align="right">
                                                <span class="text-right" >
                                                    <asp:Label ID="lblTax" runat="server" Text=""></asp:Label></span>
                                            </td>
                                        </tr>

                                        <tr style="display: none;">
                                            <td>
                                                <span class="fleft">Service Fee</span>
                                            </td>
                                            <td align="right">
                                                <span class=" text-right" >
                                                    <asp:Label ID="lblServiceFee" runat="server" Text=""></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fleft width-50">Baggage</span>
                                            </td>
                                            <td align="right">
                                                <span class=" text-right" >
                                                    <asp:Label ID="lblBaggage" runat="server" Text=""></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fleft width-50">Meal</span>
                                            </td>
                                            <td align="right">
                                                <span class=" text-right" >
                                                    <asp:Label ID="lblMeal" runat="server" Text=""></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <span class="fleft width-50">Seat</span>
                                            </td>
                                            <td align="right">
                                                <span class="text-right" >
                                                    <asp:Label ID="lblSeat" runat="server" Text=""></asp:Label>
                                                </span>
                                            </td>
                                        </tr>

                                        <tr>
                                            <td>
                                                <%if (location.CountryCode == "IN")
                                                    { %>
                                                <span class="fleft width-50">TotalGST</span>
                                                <%}
                                                else
                                                { %>
                                                <span class="fleft width-50">VAT</span>
                                                <%} %>
                                                        
                                            </td>
                                            <td align="right">
                                                <span class="text-right" >
                                                    <asp:Label ID="lblVAT" runat="server" Text=""></asp:Label>
                                                </span>
                                            </td>
                                        </tr>
                                        <%if (discount > 0)
                                        { %>
                                        <tr>
                                            <td>
                                                <span class="fleft width-50">Discount(-)</span>
                                            </td>
                                            <td align="right">
                                                <span class="text-right" >
                                                    <%=discount.ToString("N"+agency.DecimalValue) %> <%=agency.AgentCurrency %>
                                                </span>
                                            </td>
                                        </tr>
                                        <%} %>

                                        <tr>
                                            <td>&nbsp;
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>&nbsp;
                                            </td>
                                            <td>&nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>


                                            <td colspan="2">
                                                <hr class="b_bot_1 ">
                                            </td>

                                        </tr>
                                        <tr height="30px">
                                            <td align="left">
                                                <b>Total</b>
                                            </td>
                                            <td align="right">
                                                <b>
                                                    <span class="text-right">
                                                        <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                                    </span>
                                                </b>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                        </table>



                    </div>





                </ItemTemplate>
            </asp:DataList>
        </div>
        <div class="clearfix"></div>
    </div>


         
   
  
 
 
      <div class="clearfix"> </div>
 
</div>

  <div> 

<div class=" col-md-6">

  <div class="bg_white bor_gray"> 
                          <div class="ns-h3">
                                Passengers</div>
                            
                                <% for (int i = 0; i < itinerary.Passenger.Length; i++)
                                   { %>
                          
                           <div class=" pad_10">
                         
                           <label id="Title<%= i %>" ><%= itinerary.Passenger[i].Title%> </label>
                           <label id="FirstName<%= i %>" ><%= itinerary.Passenger[i].FirstName%></label>
                           <label id="LastName" ><%= itinerary.Passenger[i].LastName%></label>
                           <span style="margin-left:5px;">  <% = itinerary.Passenger[i].Type.ToString()%> </span>
                            </div>
                            
                                <% } %>
                               
                       <div class="clearfix"></div>
                            
                            </div>
                            
                            
                        </div>


 <div class="col-md-6" >
 <div class="bg_white bor_gray"> 
    <div class="ns-h3">Select Mode of Payment </div>
        <table width="100%">
            <tr>
                <td align="right">
                    Payment mode:
                </td>
                <td>
                    <asp:DropDownList ID="ddlPaymentType" runat="server" Width="100px">
                        <asp:ListItem Selected="True" Text="On Account" Value="Credit"></asp:ListItem>
                        <asp:ListItem Text="Credit Card" Value="Card"></asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
            <td colspan="2">
            &nbsp;
            </td>
            </tr>
            
        </table>
    </div>
    </div>

<div class="col-md-12" id="divFlex0" style="display: none;margin-top:15px">
    <div class="bg_white p-3">
        <div class="ns-h3">Reporting fields </div>
        <div class="row" id="divFlexFields0"></div>
    </div>
</div>
<br />
<div class="col-md-6"> <asp:Button CssClass="btn but_b pull-right" ID="btnSave" runat="server" Text="Save Booking"
                    OnClick="btnSave_Click" OnClientClick="return ShowPreloader();" />
                    <asp:Button ID="btnHold" CssClass="btn but_b pull-right" runat="server" Text="Hold" Visible="false" OnClick="btnHold_Click" OnClientClick="return ShowPreloader();"/></div>
 
 
 <div class="clearfix"></div>

</div>

<% }else if(string.IsNullOrEmpty(errorMessage)){ %>
    Itinerary details not found
    <%}else{ %>
   <span class="errorMessageBlock" ><%=errorMessage %></span>
    <%} %>
</div>
<%--</form>--%>
<div id="PreLoader" style="display:none;top:0px;margin-top:0px">
        <div class="loadingDiv" style="top:230px;">
            <div id="loadingDivision">
                <img src="images/preloader11.gif" /></div>
            <div style="font-size: 16px; color: #999999">
            
                <strong>Awaiting confirmation</strong>
               
                </div>
            <div style="font-size: 21px; color: #3060a0">
                       
                 <strong style="font-size: 14px; color: black" >do not click the refresh or back button as the transaction may be interrupted or terminated.</strong>
            
                    </div>
        </div>
    </div>
    <div id='EmailDiv' runat='server' style='width: 100%; display: none;'>
        <%//if (isBookingSuccess)
            {
                List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();

                if (ticket != null && ticket.Length > 0)
                {
                    ptcDetails = ticket[0].PtcDetail;
                }
                else// For Hold Bookings
                {
                    ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(itinerary.FlightId);
                }
                AgentMaster agency = new AgentMaster(itinerary.AgencyId);
        %>
        <%//Show the Agent details for First Email only. In case of Corporate booking second & third 
              //emails need not show Agent details in the email body
              //if (emailCounter <= 1)
                { %>
        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                    <asp:Image ID='imgLogo' runat='server' />
                </td>
                <td></td>
                <td></td>
            </tr>
        </table>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td style="width: 70%;"></td>
                <td align="right">
                    <label style="padding-right: 20%"><strong>Agent Name:</strong></label></td>
                <td align="left">
                    <label style="padding-right: 10%"><strong><%=agency.Name%></strong></label></td>
            </tr>
            <tr>
                <td style="width: 70%;"></td>
                <td align="right">
                    <label style="padding-right: 20%"><strong>Phone No:</strong></label></td>
                <td align="left">
                    <label style="padding-right: 10%"><strong><%=agency.Phone1%></strong></label></td>
            </tr>
        </table>
        <% }
            for (int count = 0; count < itinerary.Segments.Length; count++)
            {
                int paxIndex = 0;
                if (Request["paxId"] != null)
                {
                    paxIndex = Convert.ToInt32(Request["paxId"]);
                }

                List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
                if (ptcDetails != null)
                {
                    ptcDetail = ptcDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.SegmentId == itinerary.Segments[count].SegmentId; });
                }
        %>
        <table style='font-size: 12px;' width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>
                    <div style='border: solid 1px #000'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td width='25%' style='background: #1C498A; height: 34px; line-height: 34px; padding-left: 20px;'>
                                    <font color='white'>
                                        <strong>Flight :
                                            <%=itinerary.Segments[count].Origin.CityName%>
                                            to
                                            <%=itinerary.Segments[count].Destination.CityName%></strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px; padding-left: 20px;'>
                                    <font color='white'>
                                        <strong>RESERVATION</strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px; padding-left: 20px;'>
                                    <font color='white'>
                                        <strong>
                                            <%=itinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%></strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px; padding-left: 20px;'>
                                    <font color='white'><strong>Airline Ref : 
                                                <%=(itinerary.Segments[count].AirlinePNR == null || itinerary.Segments[count].AirlinePNR == "" ? (itinerary.PNR.Split('|').Length > 1 ? itinerary.PNR.Split('|')[itinerary.Segments[count].Group] : itinerary.PNR) : (itinerary.Segments[count].AirlinePNR.Split('|').Length > 1 ? itinerary.Segments[count].AirlinePNR.Split('|')[itinerary.Segments[count].Group] : itinerary.Segments[count].AirlinePNR)) %>
                                                </strong></font>
                                </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='width: 100%;'>
                        <tr>
                            <td style='width: 57%;'>
                                <table style='background: #f3f6f8; width: 100%; float: left; border: solid 1px #cdcdcd; margin-top: 2px;'>
                                    <tr>
                                        <td style='width: 50%; float: left; height: 20px; line-height: 20px;'>
                                            <label style='padding-left: 20px'>
                                                <strong>Passenger Name's :</strong></label>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px;'>
                                            <%if(ticket != null && ticket.Length > 0){ %>
                                            <strong>Ticket No </strong>
                                            <%}else { // For Hold Bookings%>
                                            <strong>PNR </strong>
                                            <%} %>

                                        </td>
                                        <td style='width: 25%; float: left; height: 20px; line-height: 20px;'>
                                            <strong>Baggage </strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 43%;'>
                                <table style='background: #f3f6f8; width: 100%; float: right; border: solid 1px #cdcdcd; margin-top: 2px; height: 20px; line-height: 20px;'>
                                    <tr>
                                        <td>
                                            <label style='padding-left: 20px'>
                                                <%if (ticket != null && ticket.Length > 0)
                                                  { %>
                                                <strong>Ticket Date:</strong>
                                                <%=ticket[0].IssueDate.ToString("dd/MM/yyyy")%>
                                                <%}
                                                  else
                                                  {//for Corporate HOLD Booking %>
                                                <strong>Booking Date:</strong>
                                                <%=itinerary.CreatedOn.ToString("dd/MM/yyyy")%>
                                                <%} %>
                                            </label>
                                        </td>
                                        <td>
                                            <label style='padding-left: 20px'>
                                                <strong>Trip ID :</strong>
                                                <%=booking.BookingId%></label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%for (int j = 0; j < itinerary.Passenger.Length; j++)
              { %>
            <tr>
                <td>
                    <table style='width: 100%;'>
                        <tr>
                            <td style='width: 57%;'>
                                <table style='background: #f3f6f8; width: 100%; float: left; border: solid 1px #cdcdcd; margin-top: 2px;'>
                                    <tr>
                                        <td style='width: 50%; float: left; height: 20px; line-height: 20px;'>
                                            <label style='margin-left: 20px; padding: 2px; background: #EBBAD9;'>
                                                <strong>
                                                    <%=itinerary.Passenger[j].Title + " " + itinerary.Passenger[j].FirstName + " " + itinerary.Passenger[j].LastName%>
                                                </strong>
                                            </label>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px;'>
                                            <%if(ticket != null && ticket.Length                                          > 0){ %>
                                            <%=(ticket[j].TicketNumber.Split('|').Length > 1 ? ticket[j].TicketNumber.Split('|')[itinerary.Segments[count].Group] : ticket[j].TicketNumber) %>
                                            <%}else{ //for Corporate HOLD Booking%>
                                            <%=(itinerary.PNR.Split('|').Length > 1 ? itinerary.PNR.Split('|')[itinerary.Segments[count].Group] : itinerary.PNR)%>
                                            <%} %>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px; text-align: right;'>

                                            <%if (itinerary.FlightBookingSource == BookingSource.UAPI || (itinerary.FlightBookingSource == BookingSource.TBOAir && !(itinerary.IsLCC)))
                                                   {
                                                       if (Request["bkg"] == null || Request["bkg"] != null)
                                                       {
                                                           if (ptcDetail.Count > 0)
                                                           {
                                                               string baggage = "";
                                                               foreach (SegmentPTCDetail ptc in ptcDetail)
                                                               {
                                                                   switch (itinerary.Passenger[j].Type)
                                                                   {
                                                                       case PassengerType.Adult:
                                                                           if (ptc.PaxType == "ADT")
                                                                           {
                                                                               baggage = ptc.Baggage;
                                                                           }
                                                                           break;
                                                                       case PassengerType.Child:
                                                                           if (ptc.PaxType == "CNN")
                                                                           {
                                                                               baggage = ptc.Baggage;
                                                                           }
                                                                           break;
                                                                       case PassengerType.Infant:
                                                                           if (ptc.PaxType == "INF")
                                                                           {
                                                                               baggage = ptc.Baggage;
                                                                           }
                                                                           break;
                                                                   }
                                                               }

                                                               if (!string.IsNullOrEmpty(baggage) && baggage.Length > 0 && !baggage.Contains("Bag") && !baggage.ToLower().Contains("piece") && !baggage.ToLower().Contains("unit") && !baggage.ToLower().Contains("units"))
                                                               {%>
                                            <%=(baggage.ToLower().Contains("kg") ? baggage : baggage + " Kg")%>

                                            <%}
                                                               else
                                                               {%>
                                            <%=(string.IsNullOrEmpty(baggage) ? "Airline Norms" : baggage)%>
                                            <%}
                                                        }
                                                        else if (itinerary.Passenger[j].Type != PassengerType.Infant)
                                                        {%>
                                            Airline Norms
                                                        <%}
                                                        //End PtcDetail.Count
                                                    }//End Request("bkg"]
                                                }
                                                else if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.PKFares|| itinerary.FlightBookingSource == BookingSource.SpiceJetCorp|| itinerary.FlightBookingSource == BookingSource.IndigoCorp)
                                                {
                                                    if (itinerary.Passenger[j].Type != PassengerType.Infant)
                                                    { %>
                                            <%=itinerary.Passenger[j].BaggageCode.Split(',')[itinerary.Segments[count].Group]%>
                                            <%}
                                                   }
                                                       else if (itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl)
                                                   {%>

                                            <%=itinerary.Passenger[j].BaggageCode.Split(',')[itinerary.Segments[count].Group]%>

                                            <%}                                                  
                                                   
                                                   else if (itinerary.FlightBookingSource == BookingSource.TBOAir && (itinerary.IsLCC))
                                                   {
                                                       if (itinerary.Passenger[j].Type != PassengerType.Infant)
                                                       {
                                                           string strBaggage = string.Empty;
                                                           if (itinerary.Passenger[j].BaggageCode != string.Empty && itinerary.Passenger[j].BaggageCode.Split(',').Length > 1)
                                                           {
                                                               strBaggage = itinerary.Passenger[j].BaggageCode.Split(',')[count];
                                                           }
                                                           else if (itinerary.Passenger[j].BaggageCode.Split(',').Length <= 1)
                                                           {
                                                               strBaggage = itinerary.Passenger[j].BaggageCode;
                                                           }
                                                           else
                                                           {
                                                               strBaggage = "Airline Norms";
                                                           }
                                            %>
                                            <%=strBaggage%>
                                            <%}
                                                   } %>
                                            
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 43%;'>
                                <table style='background: #f3f6f8; width: 100%; float: right; border: solid 1px #cdcdcd; margin-top: 2px; height: 20px; line-height: 20px'>
                                    <tr>
                                        <td style='width: 50%'>
                                            <label style='padding-left: 20px'>
                                                <strong>PNR No : </strong>
                                                <%=(itinerary.PNR.Split('|').Length > 1 ? itinerary.PNR.Split('|')[itinerary.Segments[count].Group] : itinerary.PNR)%></label>
                                        </td>
                                        <%if(!string.IsNullOrEmpty(itinerary.TripId)){ %>
                                        <td style='width: 50%; text-align: right'>
                                            <strong>Corporate Booking Code</strong>
                                            <%=itinerary.TripId %>
                                        </td>
                                        <%} %>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left'>
                                <label style='padding-left: 20px'>
                                    Date:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Status:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 40%; float: left;'>
                                <label style='padding-left: 20px; color: #08bd48;'>
                                    <strong>
                                        <%=booking.Status.ToString()%></strong></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Departs:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].DepartureTime.ToString("hh:mm tt")%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Arrives:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].ArrivalTime.ToString("hh:mm tt")%>
                                </label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airline:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%try
                                      {
                                          CT.Core.Airline airline = new CT.Core.Airline();
                                          airline.Load(itinerary.Segments[count].Airline);%>
                                    <%=airline.AirlineName%>
                                    <%}
                                      catch { } %>
                                </label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Flight:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].Airline + " " + itinerary.Segments[count].FlightNumber%>
                                    <%try
                                       {  //Loading Operating Airline and showing
                                           if (!itinerary.IsLCC && itinerary.Segments[count].OperatingCarrier != null && itinerary.Segments[count].OperatingCarrier.Length > 0)
                                           {
                                               string opCarrier = itinerary.Segments[count].OperatingCarrier;
                                               CT.Core.Airline opAirline = new CT.Core.Airline();
                                               if (opCarrier.Split('|').Length > 1)
                                               {
                                                   opAirline.Load(opCarrier.Split('|')[0]);
                                               }
                                               else
                                               {
                                                   opAirline.Load(opCarrier.Substring(0, 2));
                                               } %>
                                                        (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                       }
                                       catch { } %>
                                </label>
                            </td>
                        </tr>
                    </table>

                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    From:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].Origin.CityName + ", " + itinerary.Segments[count].Origin.CountryName%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airport:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].Origin.AirportName + ", " + itinerary.Segments[count].DepTerminal%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    To:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].Destination.CityName + ", " + itinerary.Segments[count].Destination.CountryName%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airport:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].Destination.AirportName + ", " + itinerary.Segments[count].ArrTerminal%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Class:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].CabinClass%></label>
                                <%if (itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl|| itinerary.FlightBookingSource == BookingSource.SpiceJetCorp|| itinerary.FlightBookingSource == BookingSource.IndigoCorp)
                                      {%>
                                <label style='padding-left: 20px'>
                                    <label style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 25%;'>
                                        Fare Type :
                                    </label>
                                    <%=itinerary.Segments[count].SegmentFareType%></label>
                                <%} %>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold; width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Duration:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=itinerary.Segments[count].Duration%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <%} %>
        </table>
        <table style='font-size: 12px;' width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0, outputVAT = 0;

                        if (ticket != null && ticket.Length > 500)
                        {
                            for (int k = 0; k < ticket.Length; k++)
                            {
                                AirFare += ticket[k].Price.PublishedFare;
                                Taxes += ticket[k].Price.Tax + ticket[k].Price.Markup;
                                if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += ticket[k].Price.AdditionalTxnFee + ticket[k].Price.OtherCharges + ticket[k].Price.SServiceFee + ticket[k].Price.TransactionFee;
                                }
                                Baggage += ticket[k].Price.BaggageCharge;
                                MarkUp += ticket[k].Price.Markup;
                                Discount += ticket[k].Price.Discount;
                                AsvAmount += ticket[k].Price.AsvAmount;
                                outputVAT += ticket[k].Price.OutputVATAmount;
                            }
                            if (ticket[0].Price.AsvElement == "BF")
                            {
                                AirFare += AsvAmount;
                            }
                            else if (ticket[0].Price.AsvElement == "TF")
                            {
                                Taxes += AsvAmount;
                            }
                        }
                        else
                        {
                            for (int k = 0; k < itinerary.Passenger.Length; k++)
                            {
                                AirFare += itinerary.Passenger[k].Price.PublishedFare;
                                Taxes += itinerary.Passenger[k].Price.Tax + itinerary.Passenger[k].Price.Markup;
                                if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += itinerary.Passenger[k].Price.AdditionalTxnFee + itinerary.Passenger[k].Price.OtherCharges + itinerary.Passenger[k].Price.SServiceFee + itinerary.Passenger[k].Price.TransactionFee;
                                }
                                Baggage += itinerary.Passenger[k].Price.BaggageCharge;
                                MarkUp += itinerary.Passenger[k].Price.Markup;
                                Discount += itinerary.Passenger[k].Price.Discount;
                                AsvAmount += itinerary.Passenger[k].Price.AsvAmount;
                                outputVAT += itinerary.Passenger[k].Price.OutputVATAmount;
                                if (itinerary.Passenger[k].Price.AsvElement == "BF")
                                {
                                    AirFare += AsvAmount;
                                }
                                else if (itinerary.Passenger[k].Price.AsvElement == "TF")
                                {
                                    Taxes += AsvAmount;
                                }
                            }
                        }
                    %>
                    <% if(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId<=1)
                              {
                                  
                    %>
                    <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                            <td>
                                <table width='100%' cellspacing='0' cellpadding='0' border='0'>
                                    <tbody>
                                        <tr>
                                            <td width='41%'></td>
                                            <td>
                                                <table width='100%' class=''>
                                                    <tbody>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Air Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=AirFare.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Taxes & Fees</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Taxes.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%if (itinerary.FlightBookingSource == BookingSource.AirArabia || itinerary.FlightBookingSource == BookingSource.FlyDubai || itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.TBOAir && Baggage > 0 || itinerary.IsLCC || itinerary.FlightBookingSource == BookingSource.IndigoCorp || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp )
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Baggage Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Baggage.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <%if (Discount > 0)
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Discount</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Discount.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>VAT</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>:
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=outputVAT.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: left!important;'>
                                                                <b>Total Air Fare</b>
                                                            </td>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: right!important;'>:
                                                            </td>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                <b>
                                                                    <%if (itinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                                                                       { %>
                                                                    <%=Math.Ceiling((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                    <%}
    else
    { %>
                                                                    <%=((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                    <%} %>
                                                                    <%=agency.AgentCurrency%></b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>

                    <%} %>
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;
                </td>
            </tr>
        </table>
        <%} %>
    </div>
    <!-- Reprice Modal -->
    <div id="divRepriceAlert" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg" style="width:500px">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <%--<button type="button" class="close" data-dismiss="modal">&times;</button>--%>
                    <h4 class="modal-title">Alert</h4>
                </div>
                <div class="modal-body">
                   <div class="form-group">
                        <div id="divPriceAlert"> 
                            <div style="text-align: CENTER; MARGIN-BOTTOM: 20PX;"> 
                                <span style="color: Red; font-size: 14px; font-weight: bold;"> Price has been changed for this Itinerary </span> 
                            </div>
                            <div style="text-align: CENTER;">
                                <div class="col-md-6"> <b>Original Fare :</b> <label id="lblOrigPrice"></label> </div>
                                <div class="col-md-6"> <b>Changed Fare :</b> <label id="lblNewPrice"style="color: Red"></label> </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div id="divRepricefooter" style="margin-top:35px;" class="modal-footer"> 
                    <button type="button" class="button" data-dismiss="modal">OK</button> 
                </div>
            </div>
        </div>
    </div>
    <!--Reprice Modal Modal End-->
    <asp:HiddenField runat="server" ID="hdnPriceChange" />
    <asp:HiddenField runat="server" ID="hdnSegChange" />
    <asp:HiddenField ID="hdnFlexInfo" runat="server" />
    <asp:HiddenField ID="hdnPaxFlexInfo" runat="server" />
    <asp:HiddenField ID="hdnFlexConfig" runat="server" />
    <asp:HiddenField ID="hdnTravelReason" runat="server" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

