﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="DealEntryGUI" Title="Deal Entry" Codebehind="DealEntry.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript">

        function Save() {
            var isSelected = false;
            var chkBoxList = getElement('chkAgents');
            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            if (chkBoxCount.length > 0) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    if (chkBoxCount[i].checked) {
                        isSelected = true;
                    }
                }
            }
            if (!isSelected) addMessage('Please Select @least one Agent!', '');
            if (getElement('txtSubject').value == '') addMessage('Subject cannot be blank!', '');
            if (getElement('txtMessage').value == '') addMessage('Message cannot be blank!', '');
            
            if (getMessage() != '') {
                alert(getMessage()); clearMessage();
                return false;
            }
        }

        function SelectUnSelect(control, state) {
            var chkBoxList = getElement(control);

            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            if (chkBoxCount.length > 0) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    if (chkBoxCount[i].disabled == false) {
                        chkBoxCount[i].checked = state;
                    }
                }
            }

            return false;
        }
    </script>
    
<center>
        <table cellpadding="0" class="label" cellspacing="0" border="0" style="margin-top:30px;">
           
            <tr height="30px;">
                <td align="right" width="150px">
                    <asp:Label ID="lblToAgent" runat="server" Text="To Agent:"> </asp:Label><label style="color: Red">*</label>
                </td>
                <td align="left" width="400px">
                    <div style="height: 170px; width: 300px;">
                        <a id="lnkAgentSelect" onclick="return SelectUnSelect('chkAgents',true);">Select All /</a><a
                            id="lnkAgentUnSelect" onclick="return SelectUnSelect('chkAgents',false);"> Unselect
                            All</a>
                        <div style="width: 100%; height: 150px; overflow: scroll;">
                            <asp:CheckBoxList ID="chkAgents" runat="server" RepeatDirection="Vertical" Height="50px">
                            </asp:CheckBoxList>
                        </div>
                    </div>
                </td>
            </tr>
            <tr height="30px;">
                <td align="right" width="120px">
                    <asp:Label ID="lblSubject" runat="server" Text="Subject:"></asp:Label><label style="color: Red">*</label>
                </td>
                <td align="left" width="300px">
                    <asp:TextBox ID="txtSubject" CssClass="inputEnabled"  runat="server"
                        Width="300px"></asp:TextBox>
                </td>
            </tr>
            <tr height="30px;">
                <td align="right">
                    <asp:Label ID="lblMessage" runat="server" Text="Message:"></asp:Label><label style="color: Red">*</label>
                </td>
                <td align="left">
                    <asp:TextBox ID="txtMessage" CssClass="inputEnabled" runat="server" TextMode="MultiLine"
                         Width="300px" Height="100px"></asp:TextBox>
                </td>
            </tr>
            <tr height="30px;">
                <td align="right">
                    <asp:Label ID="lblReadStatus" runat="server" Text="Read Status:"></asp:Label>
                </td>
                <td>
                    <asp:RadioButtonList runat="server" ID="rbtnReadStatus" RepeatDirection="Horizontal">
                        <asp:ListItem Text="Yes" Value="Y" Selected="True"></asp:ListItem>
                        <asp:ListItem Text="No" Value="N"></asp:ListItem>
                    </asp:RadioButtonList>
                </td>
            </tr>
           
            <tr height="30px">
                <td>
                </td>
                <td align="right">
                    <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="button" OnClientClick="return Save();"
                        OnClick="btnSave_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Clear" CausesValidation="false"
                        CssClass="button" OnClick="btnCancel_Click" />
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="button" OnClick="btnSearch_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="4" align="left" style="height: 20px">
                    <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>
                    <asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError"></asp:Label>
                </td>
            </tr>
        </table>
    </center>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
<asp:GridView ID="gvSearch" Width="80%" runat="server" AllowPaging="true" DataKeyNames="noticeId"
        EmptyDataText="No Messages!" AutoGenerateColumns="false" PageSize="10" GridLines="none"
        CssClass="grdTable" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging"
        OnSelectedIndexChanged="gvSearch_SelectedIndexChanged">
        <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                ControlStyle-CssClass="label" ShowSelectButton="True" />
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <ItemStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <b>
                        <label id="HDlblAgent">
                            Agent Name</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblAgent" runat="server" Text='<%# Eval("agent_name") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("agent_name") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <ItemStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <b>
                        <label id="HDlblSubject">
                            Subject</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblSubject" runat="server" Text='<%# Eval("subject") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("subject") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <HeaderTemplate>
                    <b>
                        <label id="HDlblReadStatus">
                            Read Status</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblReadStatus" runat="server" Text='<%# Eval("readStatus") %>'
                        CssClass="label grdof" ToolTip='<%# Eval("readStatus") %>' Width="50px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <HeaderTemplate>
                    <b>
                        <label id="HDlblCreatedBy">
                            Created By</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblCreatedBy" runat="server" Text='<%# Eval("CreatedByName") %>'
                        CssClass="label grdof" ToolTip='<%# Eval("CreatedByName") %>' Width="130px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <HeaderTemplate>
                    <b>
                        <label id="HDlblCreatedOn">
                            Created Date</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblCreatedBy" runat="server" Text='<%# Eval("createdOn") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("createdOn") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <HeaderTemplate>
                    <b>
                        <label id="HDlblModifiedBy">
                            Modified By</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblModifiedBy" runat="server" Text='<%# Eval("ModifiedByName") %>'
                        CssClass="label grdof" ToolTip='<%# Eval("ModifiedByName") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderStyle-HorizontalAlign="Left">
                <HeaderTemplate>
                    <b>
                        <label id="HDlblModifiedOn">
                            Modified Date</label></b>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblModifiedOn" runat="server" Text='<%# Eval("ModifiedOn") %>' CssClass="label grdof"
                        ToolTip='<%# Eval("ModifiedOn") %>' Width="130px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>

