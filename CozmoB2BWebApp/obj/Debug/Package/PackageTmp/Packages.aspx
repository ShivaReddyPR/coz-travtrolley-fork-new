<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="PackagesUI" Title="Package" Codebehind="Packages.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript" language="javascript">

//    function viewSearchBox() {

//        var value = document.getElementById('<%=ddlPackageCity.ClientID%>').value;

//        if (value == 'Others') {
//            document.getElementById('searchBox').style.display = "block";
//        }
//        else {
//            document.getElementById('searchBox').style.display = "none";
//        }
//    }
    
    </script>
    
    
   <div class="banner-container" style="background-image:url(build/img/main-bg.jpg)">
      <div class="row no-gutter">
          <div class="col-12 col-lg-6">
            <div id="tabbed-menu" class="search-container">
                  <ul class="tabbed-menu nav">  
                    <li>
                        <a  id="lnkFlights" href="HotelSearch.aspx?source=Flight">                   
                          Flight
                        </a> 
                    </li>                           
                    <li>
                        <a id="lnkHotels"  href="HotelSearch.aspx?source=Hotel">
                         Hotel
                        </a>
                    </li>      
                    <li>
                        <a  id="lnkACtivity" onclick="Javascript:location='ActivityResults.aspx'" href="#classics2" >
                         Activity
                        </a>
                    </li>    
                    <li><a id="lnkPackages" onclick="Javascript:location='packages.aspx'"  href="#featured2" class="current">
                        Packages
                        </a>
                    </li>
                     <li>
                         <a onclick="Javascript:location='Insurance.aspx'" id="lnkInsurance" href="Insurance.aspx" >
                            Insurance
                         </a>
                   </li>                                                              
                 </ul>
                   <div class="search-matrix-wrap auto-height p-4 search-page-form">      
                       <div class="custom-radio-table row my-2">
                          <div class="col-12 mt-2">
                              <asp:RadioButtonList Width="100%" ID="rbSelect" runat="server" RepeatDirection="Horizontal">
                                  <asp:ListItem Text="All" Value="A" Selected="True"></asp:ListItem>
                                  <asp:ListItem Text="Outbound Tour" Value="O"></asp:ListItem>
                                  <asp:ListItem Text="Inbound Tour" Value="I"></asp:ListItem>
                                   <asp:ListItem Text="Pilgrim Tour" Value="P"></asp:ListItem>
                              </asp:RadioButtonList>
                          </div>
                      </div>
                      <div class="row custom-gutter">
                         <div class="col-12 col-sm-6">                             
                             <label>Select Destination</label>
                             <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-location" aria-label="Select Destination"></span>
                                 </div>
                                 <asp:DropDownList  ID="ddlPackageCity" class="form-control" runat="server" ></asp:DropDownList>
                             </div>
                         </div>
                         <div class="col-12 col-sm-6">                             
                             <label>Select Package Type</label>
                             <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-beach" aria-label="Select Package Type"></span>
                                 </div>
                                 <asp:DropDownList  ID="ddlPackageTheme" runat="server" class="form-control"></asp:DropDownList>
                             </div>
                         </div>
                      </div>
                      <div class="row custom-gutter">
                         <div class="col-12">     
                           <label>Rates Between <span class="small">(Not Mandatory)</span></label>
                         </div>
                         <div class="col-12 col-sm-3">                             
                            
                             <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-coins" aria-label=""></span>
                                 </div>
                                <asp:TextBox ID="txtFrom" runat="server" class="form-control" onkeypress="return restrictNumeric(this.id,'1');"></asp:TextBox>
                             </div>
                         </div>
                         <div class="col-12 col-sm-3">            
                             <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-coins" aria-label=""></span>
                                 </div>
                                <asp:TextBox ID="txtTo" runat="server"  class="form-control" onkeypress="return restrictNumeric(this.id,'1');"></asp:TextBox>
                             </div>
                         </div>
                         <div class="col-12 col-sm-3">  
                              <asp:Button  ID="btnSerchPackage" CssClass="but but_b" runat="server" OnClick="btnSearchPackage_Click" Text="Search"  class="gray_button"/>
                         </div>
                      </div>

                   </div>



              </div>
           </div>
        </div>
     </div>  
    
    
    
    
    
    
    
    <div class="col-md-12 padding-0 margin-top-10"> 

    
    

     <div class="ns-h3"> <h4> Packages Info</h4> </div>
<div class=" bg_white bor_gray pad_10">


    

     
  <div>
<table border="0" cellpadding="0" cellspacing="0" width="100%">
  <tbody>
    <tr>
      <td>We   understand that todays travellers are demanding and their needs are   diverse. Whether seeking customised corporate solutions or simply   looking for assistance while planning a personal getaway, we are   committed to meeting the individual needs of every customer.</td>
    </tr>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><p><strong>Cozmo Travels portfolio of services:</strong></p></td>
    </tr>
    <tr>
      <td>	Airline reservations and ticketing <br />
        	Hotel bookings <br />
        	Holiday packages <br />
        	UAE visa application processing <br />
        	Foreign visa application assistance <br />
        	Airport meet and greet service <br />
        	Inbound and outbound tours</td>
    </tr>
    <tr>
      <td> </td>
    </tr>
    <tr>
      <td><h4>Specialised services:</h4></td>
    </tr>
    <tr>
      <td><p><strong>Business travel services:</strong></p></td>
    </tr>
    <tr>
      <td><p>In an age where time and resources are of  the essence, Cozmo   Travel aims to ensure seamless efficiency in corporate  travel   management with the following services.</p></td>
    </tr>
    <tr>
      <td>	  Personal handling of every company profile<br />
        	Manual and automated low fare analysis<br />
        	Travel policy compliant and non-compliant options<br />
        	Business conferences and seminars<br />
        	Corporate travel management and staff incentive programmes<br />
        	Visa application assistance<br />
        	24X7 service from dedicated professionals</td>
    </tr>
    
    

        <tr> 
    <td> &nbsp; </td>
    
    </tr>
  </tbody>
</table>

</div>   

 </div>
    
    </div>
        
    
     
        
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

