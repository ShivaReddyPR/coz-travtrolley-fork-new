<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="AddVisaRquirement" Title="Cozmo Travels"
    ValidateRequest="false" Codebehind="AddVisaRquirement.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <meta http-equiv="content-type" content="text/html; charset=utf-8">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    <style type="text/css">
        /*margin and padding on body element
  can introduce errors in determining
  element position and are not recommended;
  we turn them off as a foundation for yui
  CSS treatments. */body
        {
            margin: 0;
            padding: 0;
        }
    </style>
    <link rel="stylesheet" type="text/css" href="yui/build2/menu/assets/skins/sam/menu.css" />
    <link rel="stylesheet" type="text/css" href="yui/build2/button/assets/skins/sam/button.css" />
    <link rel="stylesheet" type="text/css" href="yui/build2/fonts/fonts-min.css" />
    <link rel="stylesheet" type="text/css" href="yui/build2/container/assets/skins/sam/container.css" />
    <link rel="stylesheet" type="text/css" href="yui/build2/editor/assets/skins/sam/editor.css" />

    <script type="text/javascript" src="yui/build2/yahoo-dom-event/yahoo-dom-event.js"></script>

    <script type="text/javascript" src="yui/build2/element/element-min.js"></script>

    <script type="text/javascript" src="yui/build2/container/container-min.js"></script>

    <script type="text/javascript" src="yui/build2/menu/menu-min.js"></script>

    <script type="text/javascript" src="yui/build2/button/button-min.js"></script>

    <script type="text/javascript" src="yui/build2/editor/editor-min.js"></script>
    
    
    
    <div> 
      <div class="col-md-6"> <h4> <asp:Label runat="server" ID="lblStatus" Text="Add Visa Requirement"></asp:Label></h4> </div>
                                
           <div class="col-md-6">
           
           <asp:HyperLink ID="HyperLink1" CssClass="fcol_blue pull-right" runat="server" NavigateUrl="~/VisaRequirementList.aspx">Go to Visa Requirement List</asp:HyperLink>
           
            </div>                     
                                
       <div class="clearfix"> </div> 
       </div>
       
       
       
        <div class="paramcon"> 
    
    
           <div class="margin-bottom-10"> 
    <div class="col-md-2"> <label>
                        Agent<sup>*</sup>
                    </label></div>
    
    <div class="col-md-2"> 
                    
                    <span>
                        <asp:DropDownList ID="ddlAgent" CssClass="form-control" runat="server">
                        </asp:DropDownList>
                    </span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" Display="Dynamic" runat="server" ErrorMessage="Please select a Agent "
                        ControlToValidate="ddlAgent" InitialValue="Select Agent"></asp:RequiredFieldValidator>
              </div>     
    
    <div class="clearfix"> </div> 
    </div>
    
    
           <div class="margin-bottom-10"> 
    <div class="col-md-2"> <label>
                        Requirement Name<sup style="color: Red">*</sup></label></div>
    
    <div class="col-md-2"> 

                    
                   
                        <asp:TextBox ID="txtName"  CssClass=" form-control"  runat="server"></asp:TextBox>
                 
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator12" runat="server" ControlToValidate="txtName"
                        Display="Dynamic" ErrorMessage="Please fill requirement name "></asp:RequiredFieldValidator>
                        
                        <asp:RegularExpressionValidator
                            ID="RegularExpressionValidator6" runat="server" ErrorMessage="Please enter maximum 1000 character"
                            ValidationExpression="[\S\s]{0,1000}" ControlToValidate="txtName" Display="Dynamic"></asp:RegularExpressionValidator>
    
    
    
    </div>     
    
    <div class="clearfix"> </div> 
    </div>
    
    
    
           <div class="margin-bottom-10"> 
    <div class="col-md-2"><label>
                        Country<sup style="color: Red">*</sup></label> </div>
    
    <div class="col-md-2"><p>
                    
                    <span>
                        <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control">
                        </asp:DropDownList>
                    </span>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" Display="Dynamic" runat="server" ControlToValidate="ddlCountry"
                        ErrorMessage="Please select country " InitialValue="Select"></asp:RequiredFieldValidator>
                </p> </div>     
    
    <div class="clearfix"> </div> 
    </div>
    
   
   
          <div class="margin-bottom-10"> 
    <div class="col-md-2"><label>
                        Description</label> </div>
    
    <div class="col-md-4"> <asp:TextBox ID="editor" CssClass="inp_border18" runat="server" TextMode="MultiLine"
                            Height="200" Width="100%"></asp:TextBox></div>     
    
    <div class="clearfix"> </div> 
    </div>
    
    
    
    
    
            <div class="margin-bottom-10"> 
    <div class="col-md-2"> </div>
    
    <div class="col-md-2"><asp:Button ID="btnSave" runat="server" Text="Save"  CssClass="btn but_b"
                            OnClick="btnSave_Click" /> </div>     
    
    <div class="clearfix"> </div> 
    </div>
    
    
    
    
       </div>
    

    
    
    
    
    
    
</asp:Content>
