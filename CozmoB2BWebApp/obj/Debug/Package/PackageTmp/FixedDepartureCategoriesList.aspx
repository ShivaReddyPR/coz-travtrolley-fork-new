﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="FixedDepartureCategoriesListGUI" Title="FixedDeparture CategoriesList" Codebehind="FixedDepartureCategoriesList.aspx.cs" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <link href="css/ModalPop.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript" src="Scripts/jsBE/ModalPop.js"></script>

    <script type="text/javascript" src="Scripts/jsBE/prototype.js"></script>

    <script type="text/javascript">
        //Validate the qty entered against the pax count
        function ValidateQty() {
            var rooms = eval(document.getElementById('<%=hdnRoomCount.ClientID %>').value);
            var rows = document.getElementById('<%=hdnRows.ClientID %>').value;
            var adultCount = parseInt("0");
            var ChildCount = parseInt("0");
            var InfantCount = parseInt("0");
            for (var i = 0; i < rooms; i++) {
                var adult = false;
                for (var j = 0; j < rows; j++) {
                    var txtQty = document.getElementById('ctl00_cphTransaction_txtQty_' + j + i).value;
                    var PaxType = document.getElementById('ctl00_cphTransaction_hdnPaxType_' + j + i).value;
                    var priceId = document.getElementById('ctl00_cphTransaction_hdnPriceId_' + j + i).value;
                    var label = eval(document.getElementById('ctl00_hdn_' + priceId).value);
                    if (i == 0) {
                        label = 0;
                    }
                    var stockInHand = document.getElementById('ctl00_cphTransaction_hdnStockInHand_' + j + i).value;
                    var stockUsed = document.getElementById('ctl00_cphTransaction_hdnStockUsed_' + j + i).value;
                    if (parseInt(txtQty) >= 0) {
                        label += parseInt(txtQty);
                        document.getElementById('ctl00_hdn_' + priceId).value = label;
                        if (PaxType == 'Adult') {
                            if (parseInt(txtQty) > 0) {
                                adultCount = adultCount + parseInt(txtQty);
                                adult = true;
                            }
                        }
                        else if (PaxType == 'Child') {
                            ChildCount = ChildCount + parseInt(txtQty);
                        }
                        else if (PaxType == 'Infant') {
                            InfantCount = InfantCount + parseInt(txtQty);
                        }
                    }
                    if (((parseInt(stockInHand) - parseInt(stockUsed)) - parseInt(label)) < 0) {
                        document.getElementById('noStockAnEnquiry').style.display = 'block';
                        return false;
                    }
                    
                }
                if (adult == false) {
                    if (window.navigator.appCodeName != "Mozilla") {
                        document.getElementById('errMessHotel').style.display = "block";
                        document.getElementById('errMessHotel').innerText = "Atleast 1 Adult should be accompanied per room.";
                    }
                    else {
                        document.getElementById('errMessHotel').style.display = "block";
                        document.getElementById('errMessHotel').textContent = "Atleast 1 Adult should be accompanied per room.";
                    }
                    return false;
                }
            }

            if (adultCount != document.getElementById('<%=hdnAdult.ClientID %>').value) {
                if (window.navigator.appCodeName != "Mozilla") {
                    document.getElementById('errMessHotel').style.display = "block";
                    document.getElementById('errMessHotel').innerText = "Adult count mismatch. Please enter value chosen earlier.";
                }
                else {
                    document.getElementById('errMessHotel').style.display = "block";
                    document.getElementById('errMessHotel').textContent = "Adult count mismatch. Please enter value chosen earlier.";
                }
                return false;
            }
            if (ChildCount != document.getElementById('<%=hdnChild.ClientID %>').value) {
                if (window.navigator.appCodeName != "Mozilla") {
                    document.getElementById('errMessHotel').style.display = "block";
                    document.getElementById('errMessHotel').innerText = "Child count mismatch. Please enter value chosen earlier.";
                }
                else {
                    document.getElementById('errMessHotel').style.display = "block";
                    document.getElementById('errMessHotel').textContent = "Child count mismatch. Please enter value chosen earlier.";
                }
                return false;
            }
            if (InfantCount != document.getElementById('<%=hdnInfant.ClientID %>').value) {

                if (window.navigator.appCodeName != "Mozilla") {
                    document.getElementById('errMessHotel').style.display = "block";
                    document.getElementById('errMessHotel').innerText = "Infant count mismatch. Please enter value chosen earlier.";
                }
                else {
                    document.getElementById('errMessHotel').style.display = "block";
                    document.getElementById('errMessHotel').textContent = "Infant count mismatch. Please enter value chosen earlier.";
                }
                return false;
            }
            return true;
        }

        function ValidateQuote() {

            document.getElementById('ctl00_cphTransaction_btnSubmit').submit();
            //document.forms[0].submit();
        }

        function CalculateTotal1(id) {
            var x = id;
        }
        function CalculateTotal() {
            var decimalpoint = eval('<%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue %>');
            var rooms = eval(document.getElementById('<%=hdnRoomCount.ClientID %>').value);
            var rows = eval(document.getElementById('<%=hdnRows.ClientID %>').value);

            var sum = eval(0);
            for (var i = 0; i < rooms; i++) {
                for (var j = 0; j < rows; j++) {
                    var txtQty = document.getElementById('ctl00_cphTransaction_txtQty_' + j + i).value;
                    var price = eval(0);
                    if (window.navigator.appName.indexOf("Mozilla") <= 0) {
                        price = eval(document.getElementById('ctl00_cphTransaction_lblUnitPrice_' + j + i).innerHTML.replace(',', ''));
                    }
                    else {
                        price = eval(document.getElementById('ctl00_cphTransaction_lblUnitPrice_' + j + i).textContent.replace(',', ''));
                    }
                    var total = txtQty * price;

                    sum += total;
                    document.getElementById('ctl00_cphTransaction_lblTotal_' + j + i).innerHTML = parseFloat(total).toFixed(decimalpoint);
                    document.getElementById('lblSumTotal').innerHTML = parseFloat(sum).toFixed(decimalpoint);
                    
//                    if (window.navigator.appName.indexOf("Mozilla") <= 0) {
//                        document.getElementById('ctl00_cphTransaction_lblTotal_' + j + i).innerHTML = parseFloat(total).toFixed(decimalpoint);
//                        document.getElementById('lblSumTotal').innerHTML = parseFloat(sum).toFixed(decimalpoint);
//                    }
//                    else {
//                        document.getElementById('ctl00_cphTransaction_lblTotal_' + j + i).innerHTML = parseFloat(total).toFixed(decimalpoint);
//                        document.getElementById('lblSumTotal').textContent = parseFloat(sum).toFixed(decimalpoint);
//                    }
                }
            }
        }


        function Check(id) {
            var val = document.getElementById(id).value;
            if (val == '0') {
                document.getElementById(id).value = '';
            }
        }
        function Set(id) {
            var val = document.getElementById(id).value;
            if (val == '' || val == '0') {
                document.getElementById(id).value = '0';
            }
        }

        function ShowBestHolidayPopup() {
            if (ValidateQty()) {
                document.getElementById('divBestHoliday').style.display = 'block';
            }
        }
        function ShowNoStockEmailPopup() {
            ModalPop.Container = document.getElementById('noStockAnEnquiry');
            ModalPop.Show({ x: 735, y: 539 }, "<div>ytytyy</div>");
        }
        
         function HidePopUp() {
             document.getElementById('divBestHoliday').style.display = 'none';
        }
        function ValidateModulepopUp() {
            if (document.getElementById('<%=txtName.ClientID %>').value == "") {
                document.getElementById('Error').style.display = "block";
                document.getElementById('Error').innerHTML = 'Please enter your name';
                return false;
            }
            if (document.getElementById('<%=txtEmail.ClientID %>').value == "") {
                document.getElementById('Error').style.display = "block";
                document.getElementById('Error').innerHTML = 'Please enter your email';
                return false;
            }
            if (!ValidEmail.test(document.getElementById('<%=txtEmail.ClientID %>').value)) {
                document.getElementById('Error').style.display = "block";
                document.getElementById('Error').innerHTML = 'Please enter valid email';
                return false;
            }
            if (document.getElementById('<%=txtEmail.ClientID %>').value == "") {
                document.getElementById('susMessage').style.display = "none";
                document.getElementById('Error').style.display = "block";
                document.getElementById('Error').innerHTML = 'Please enter phone number';
                return false;
            }
        }



        // Regular expression for Not a number;
        var IsNaN = /\D/;
        function SendMail() {
            var paxName = document.getElementById('txtPaxName').value;
            var phoneNo = document.getElementById('txtPaxPhone').value;
            var email = document.getElementById('txtPaxEmail').value;
            var packageName = document.getElementById('dealName').value;
            var dealId = document.getElementById('dealId').value;
            var productType = document.getElementById('productType').value;
            var agencyId = document.getElementById('agencyId').value;
            var adults = parseInt(document.getElementById('<%=hdnAdult.ClientID %>').value);
            var childs = parseInt(document.getElementById('<%=hdnChild.ClientID %>').value);
            var infants = parseInt(document.getElementById('<%=hdnInfant.ClientID %>').value);
            var day = (document.getElementById('ctl00_cphTransaction_lblDate').innerHTML)
            var roomCount = parseInt(document.getElementById('<%=hdnRoomCount.ClientID %>').value);
            var paramList = "";
            paramList += 'dealId=' + dealId;
            paramList += '&agencyId=' + agencyId;
            paramList += '&phoneNo=' + phoneNo;
            paramList += '&paxName=' + paxName;
            paramList += '&EmailId=' + email;
            paramList += '&packageName=' + packageName;
            paramList += '&depDate=' + day
            paramList += '&adults=' + adults;
            paramList += '&childs=' + childs;
            paramList += '&infants=' + infants;
            paramList += '&roomCount=' + roomCount;
            paramList += '&productType=' + productType;
            if (Trim(paxName) == "") {
                document.getElementById('susMessage').style.display = "none";
                document.getElementById('ErrorMes').style.display = "block";
                document.getElementById('ErrorMes').innerHTML = 'Please enter your name';
                return false;
            }
            if (Trim(email) == "") {
                document.getElementById('ErrorMes').style.display = "block";
                document.getElementById('ErrorMes').innerHTML = 'Please enter your email';
                return false;
            }
            if (!ValidEmail.test(email)) {
                document.getElementById('susMessage').style.display = "none";
                document.getElementById('ErrorMes').style.display = "block";
                document.getElementById('ErrorMes').innerHTML = 'Please enter valid email';
                return false;
            }
            if (Trim(phoneNo) == "") {
                document.getElementById('susMessage').style.display = "none";
                document.getElementById('ErrorMes').style.display = "block";
                document.getElementById('ErrorMes').innerHTML = 'Please enter phone number';
                return false;
            }
            if (isNaN(phoneNo)) {
                document.getElementById('susMessage').style.display = "none";
                document.getElementById('ErrorMes').style.display = "block";
                document.getElementById('ErrorMes').innerHTML = 'Please enter correct phone number';
                return false;
            }
            paramList += '&submitNoStockEnquiry=true';
            var url = "EmailAjax";
            new Ajax.Request(url, { method: 'post', parameters: paramList, onComplete: EmailResponse });
        }
        function EmailResponse(response) {
            if (response.responseText == "true") {
                document.getElementById('ErrorMes').style.display = "none";
                document.getElementById('susMessage').style.display = "block";
                alert('Enquiry Submitted Successfully');
                HideNoStockPopUp();
                ResetNoStockForm();
                window.location = "FixedDepartureDetails.aspx?id="+ document.getElementById('dealId').value;
            }
        }
        function HideNoStockPopUp() {
            document.getElementById('noStockAnEnquiry').style.display = 'none';
        }

        function ResetNoStockForm() {
            document.getElementById('txtPaxName').value = "";
            document.getElementById('txtPaxPhone').value = "";
            document.getElementById('txtPaxEmail').value = "";
        }
        
    </script>

    <script type="text/javascript">
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
            return true;
        }
          
    </script>

    <style type="text/css">
        .float-left
        {
            float: left;
            border-style: solid;
            border-width: 1px;
            border-color: Black;
        }
    </style>
      <% if (activity != null)
       {%>
     <input type="hidden" id="agencyId" name="agencyId" visible="false" value="<%=Settings.LoginInfo.AgentId %>" />
     
    <input type="hidden" id="dealId"   name="dealId" value="<%=activity.Id %>" />
    <input type="hidden" id="productType"   name="productType" value="F" />
    <input type="hidden" id="dealName" name="dealName" value="<%=activity.Name %>" />
    <%} %>
    <div id="divBestHoliday"
     style="position:absolute; display:none; padding: 7px 14px 10px 14px; left:200px;top:200px; border:solid 2px #18407B; width:300px; background-color: white;"  >
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
         
            
                <tr>
                    <td width="54%" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <h2 style="color: #0099ff; font-family: Calibri; font-size: 22px">
                                        Get the Best Holiday Deals</h2> 
                                        
                                        
                                        <a style=" position: absolute; right:10px; top:7px; color:#ccc;  font-weight:normal; font-size:18px; text-decoration:none" href="#" onclick="HidePopUp()">X</a>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="border-bottom: solid 1px #CCCCCC">
                    </td>
                </tr>
                <tr>
                    <td  valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label style="font-size: 11px">
                                       Please provide your enquiry below,one of our holiday specialist will get back to you.</label>
                                    <span style="color: Red" class="red padding-left-125" id="Error"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>Name</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox runat="server" ID="txtName" class="send-enq" Style="width: 100%;"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>E-mail Id</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtEmail" runat="server" CssClass="send-enq" Style="width: 100%;"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>Phone Number</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtPhno" runat="server" Style="width: 100%;" CssClass="send-enq"></asp:TextBox>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSubmit" CssClass="button-new" runat="server" Text="Submit" OnClientClick="return ValidateModulepopUp();" OnClick="btnSubmit_Click" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
        </table>
    </div>
    
    <asp:HiddenField ID="hdnRows" runat="server" />
    <asp:HiddenField ID="hdnPaxCount" runat="server" />
    <asp:HiddenField ID="hdnAdult" runat="server" Value="0" />
    <asp:HiddenField ID="hdnChild" runat="server" Value="0" />
    <asp:HiddenField ID="hdnInfant" runat="server" Value="0" />
    <asp:HiddenField ID="hdnRoomCount" runat="server" Value="0" />
    <asp:HiddenField ID="hdnModulepopUp" runat="server" Value="" />
    <div style="padding-top: 10px">
        <div class="ns-h3">
            <asp:Label ID="lblTitle" runat="server" Text=""></asp:Label></div>
        <div class="wraps0ppp">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <label style="font-size: 13px">
                            Availability for <strong>
                                <asp:Label ID="lblAvail" runat="server" Text=""></asp:Label></strong> on <strong>
                                    <asp:Label ID="lblDate" runat="server" Text=""></asp:Label></strong></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <h3>
                            <asp:Label ID="lblName" runat="server" Text=""></asp:Label>
                            :
                            <asp:Label ID="lblDays" runat="server" Text=""></asp:Label>
                            &nbsp;&nbsp;
                            <%if (activity.TransactionHeader != null && activity.TransactionHeader.Rows.Count > 0)
                              { %>
                            (Adults:<%=activity.TransactionHeader.Rows[0]["Adult"]%>, Childs:<%=activity.TransactionHeader.Rows[0]["Child"]%>,
                            Infants:<%=activity.TransactionHeader.Rows[0]["Infant"]%>)
                            <%} %>
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="font-size: 16px; font-weight: bold">
                            Booking for
                            <asp:Label ID="lblActivity" runat="server" Text=""></asp:Label>
                        </div>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div class="error_msg" style="display: none; width: 100%; text-align: center;" id="errMessHotel">
                        </div>
                        <table id="tblPaxPrice" runat="server" width="100%" border="0" cellpadding="0" cellspacing="0">
                        </table>
                    </td>
                </tr>
                <tr height="30px">
                    <td>
                        <table width="100%">
                            <tr>
                                <td width="76%" align="right">
                                    <b>
                                        <label style="font-size: small;">
                                            Total:
                                        </label>
                                    </b>
                                </td>
                                <td align="right" width="17%" style="font-size: small;">
                                    <b>
                                        <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>&nbsp;</b>
                                </td>
                                <td width="14%" align="right">
                                    <b>
                                        <label id="lblSumTotal" style="font-size: small;">
                                            <%=Convert.ToDecimal(0).ToString("N"+CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue) %></label></b>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;
                    </td>
                </tr>
                <tr>
                    <td>
                        <div style="padding-right: 10px" class=" f_R">
                            <a href="#" onclick="ShowBestHolidayPopup();" class="button-new">Generate Quote</a>
                            <%--<asp:LinkButton ID="ImgQuote" runat="server" CssClass="button-new"  Text="Generate Quote" OnClick="ImgQuote_Click"/>  --%>
                            <asp:LinkButton ID="imgSubmit" runat="server" CssClass="button-new" Text="Continue Booking"
                                OnClick="imgSubmit_Click" OnClientClick="return ValidateQty();" />
                            <%---OnClientClick="return ValidateQty();" --%>
                        </div>
                    </td>
                </tr>
            </table>
        </div>
        <div class="clear">
        </div>
        <div id="divPrint" style="display:none;" runat="server">
        <table style="font-family: Arial, Helvetica, sans-serif; line-height: 17px" width="100%"
    border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table style="border: solid 1px #000;" width="100%" border="0" cellpadding="0" cellspacing="0"
                bgcolor="#0099ff">
                <tr>
                    <td width="50%" height="30">
                        <label style="color: #fff; font-size: 18px;">
                            <%=activity.Name%>
                            (<%=activity.Days %>
                            Nights &amp;
                            <%=activity.Days  + 1 %>
                            Days)
                        </label>
                    </td>
                    <td width="50%" align="right">
                        <label style="padding-right: 15px; color: #FFFFFF; font-size: 16px">
                            Starting From- AED
                            <%=minPrice.ToString("N0") %></label>
                        <label style="padding-right: 15px; color: #FFFFFF">
                            per person</label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 5px">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" width="30%">
                        <img width="366px" height="247px" src="<%= activity.ImagePath1%>"
                            alt="<%=activity.Name %>" title="<%=activity.Name %>" />
                    </td>
                    <td width="70%" valign="top">
                        <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                            <tr>
                                <td height="0">
                                    <label style="color: #cc0000; font-size: 16px">
                                        <strong>Overview</strong></label>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; font-size: 14px!important" valign="top">
                                    <label>
                                        <%=activity.Overview %>
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                <tr>
                    <td height="0">
                        <label style="color: #cc0000; font-size: 16px">
                            <strong>Inclusions </strong>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="Inclusions" class="active" >
                            
                                
                                <ul>
                                
                                <%if (activity != null)
                                          { %>
                                <%foreach (string item in activity.Inclusions)
                                              { %>
                                <li style="list-style-type: circle">
                                    <%=item%></li>
                                <%} %>
                            </ul>
                            <label style="color: #cc0000; font-size: 16px">
                                    <strong>Exclusions </strong>
                                </label>
                            <ul>
                                
                                <%foreach (string item in activity.Exclusions)
                                              { %>
                                <li style="list-style-type: circle">
                                    <%=item%></li>
                                <%}
                                          }%></ul>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                <tr>
                    <td height="0">
                        <label style="color: #cc0000; font-size: 16px">
                            <strong>Itinerary</strong></label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                       
                            <div id="Itinerary" class="active" >
                            <asp:DataList ID="dlFDItinerary" runat="server" CellPadding="4" DataKeyField="Id"
                                                Width="900px">
                                                <ItemTemplate>
                                                        <table style=" border: solid 1px #ccc; margin-bottom:10px;" class="" width="100%">
                                                           <tr style="height: 25px;">
                                                                <td>

                                                                        <table style="margin-bottom:14px; margin-top:10px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                        <td width="14%"> <strong style=" font-size:14px">Day <%# Eval("Day")%> </strong></td>
                                                                        <td width="86%"></td>
                                                                        </tr>

                                                                        </table>
                                                   
                                                                   
                                                                    
                                                                    
                                                                    
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr style="height: 25px;">
                                                                <td>
                                                                  
                                                                  
                                                                  
                                                                  <table style="margin-bottom:16px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="14%"><strong>ItineraryName: </strong></td>
    <td width="86%"><%# Eval("ItineraryName")%></td>
  </tr>
  
</table>
                                                                  
                                                                    
                                                                    
                                                                    
                                                                    
                                                                </td>
                                                            </tr>
                                                                    <tr style="height: 25px;">
                                                                <td>
                                                                
                                 <table style="margin-bottom:10px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="14%"><strong>  Meals:</strong> </td>
    <td width="86%"><%# Eval("Meals")%></td>
  </tr>
  
</table>
                                                                
                                                                    
                                                                </td>
                                                            </tr>
                                                             <tr style="height: 25px;">
                                                                <td>
                                                                   
                                                                   
                                                                   
  <table style="margin-bottom:14px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" width="14%"> <strong>Itinerary: </strong></td>
    <td width="86%"><%# Eval("Itinerary")%></td>
  </tr>
  
</table>
                                                                   
                                                                   
                                                                   
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                        </div> 
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                <tr>
                    <td height="0">
                        <label style="color: #cc0000; font-size: 16px">
                            <strong>Price</strong></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="font-size: 14px">
                            <div id="RoomRates">
                                <div>
                                    <asp:DataList id="dlRoomRates" runat="server" width="100%">
                                                <ItemTemplate>
                                                    <table width="100%" border="1">
                                   
                                                        <tr>
                                                        <td height="25px" width="30%">
                                                        
                                                        <%#(Eval("PriceDate")==DBNull.Value)?string.Empty: Convert.ToDateTime(Eval("PriceDate")).ToString("dd-MMM-yyyy")%>
                                                        </td>
                                                            <td width="30%">
                                                                <%#Eval("Label") %>
                                                            </td>
                                                        
                                                           <td width="70%">
                                                               <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>
                                                                <%#CTCurrencyFormat(Eval("Total"))%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                </div>
                            </div>
                        </label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                <tr>
                    <td height="0">
                        <label style="color: #cc0000; font-size: 16px">
                            <strong>Terms & Conditions</strong></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="Terms" class="active" >
                            
                            <h4>Things to bring</h4>
                            <ul>
                                <%if (activity != null && activity.ThingsToBring != null)
                                  { %>
                                <%string[] things = activity.ThingsToBring.Split(','); %>
                                <%foreach (string thing in things)
                                  { %>
                                <li style=" list-style-type:circle; margin-left:20px;">
                                    <%=thing%></li>
                                <%} %>
                                <%} %>
                            </ul>
                            
                            <h4>Cancellation Policy</h4>
                                     <ul>
                                        <%if (activity != null && activity.CancelPolicy != null)
                                          { %>
                                        <%string[] cancelP = activity.CancelPolicy.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                        <%foreach (string cancel in cancelP)
                                          { %>
                                          <li style=" list-style-type:circle; margin-left:20px;"><%=cancel %></li>
                                        <%} %>
                                        <%} %>
                                        </ul>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
</table>
        </div>
    </div>
    
     <div id="noStockAnEnquiry" class="signin_box_class" style="left: 150px; top: 51px;
        width: 735px; height: 539px; display: none; z-index: 9999;">
        <em class="close" style="position: absolute; right: 10px; top: 10px; cursor: pointer">
            <i>
                <img alt="Close" onclick="HideNoStockPopUp()" src="images/close-itimes.gif" /></i>
        </em>
        <div class="summer-promotion-window">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="54%" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <h2 style="color: #0099ff; font-family: Calibri; font-size: 22px">
                                        Stock Not Available</h2>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td valign="top" style="border-bottom: solid 1px #CCCCCC">
                    </td>
                </tr>
                <tr>
                    <td height="300" valign="top">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label style="font-size: 11px">
                                        Leave an enquiry below and member of our team will get back to you soon.</label>
                                    <span style="color: Red" class="red padding-left-125" id="ErrorMes"></span>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>Name</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input style="width: 99%;" type="text" id="txtPaxName" name="paxName" class="send-enq" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>E-mail Id</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input style="width: 99%;" type="text" id="txtPaxEmail" name="email" class="send-enq" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="bottom">
                                                <strong>Phone Number</strong> (required)
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <input style="width: 99%;" type="text" id="txtPaxPhone" onkeypress="return isNumber(event)"
                                                    name="phone" class="send-enq" />
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <a class="button-new" onclick="SendMail()">Submit</a>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <i id="susMessage" style="display: none;"><del>
                                        <img src="Images/greenmark.gif" alt="mark" /></del><dfn>Our Travel Expert will call
                                            you shortly</dfn> </i>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>

