﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="UpdateAgentMarkupGUI" Title="UpdateAgentMarkup" Codebehind="UpdateAgentMarkup.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
 <style type="text/css">
    .chkBoxList td
    {
        width:160px;
    }
    </style>
<script type="text/javascript">
    function validation() {
        if (getElement('ddlAgent').selectedIndex > 0) {
            var chklist = document.getElementById('<%= chkProduct.ClientID %>');
            var chkListinputs = chklist.getElementsByTagName("input");
            var arrayOfCheckBoxLabels = chklist.getElementsByTagName("label");
            var count = 0;
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    var source = arrayOfCheckBoxLabels[i].innerText
                    if (source == 'Flight' || source == 'Hotel' || source == 'Insurance') {
                        if (document.getElementById('ctl00_cphTransaction_ddlSource_' + i).selectedIndex <= 0) {
                            document.getElementById('<%= errMess.ClientID %>').style.display = "block";
                            document.getElementById('<%= errorMessage.ClientID %>').innerHTML = "Please select Source!";
                            return false;
                        }
                    }
                    count = count + 1;
                }

            }
            if (count == 0) {
                document.getElementById('<%= errMess.ClientID %>').style.display = "block";
                document.getElementById('<%= errorMessage.ClientID %>').innerHTML = "Atleast one Product should be selected !";
                return false;
            }
            return true;
        }
        else {
            document.getElementById('<%= errMess.ClientID %>').style.display = "block";
            document.getElementById('<%= errorMessage.ClientID %>').innerHTML = "Please Select agent !";
            return false;
        
        }
    }

    

    function Check(id) {
        var val = document.getElementById(id).value;
        if (val == '0.00') {
            document.getElementById(id).value = '';
        }
    }

    function Set(id) {
        var val = document.getElementById(id).value;
        if (val == '' || val == '0.00') {
            document.getElementById(id).value = '0.00';
        }
    }
    function SetValue() {
        var chklist = document.getElementById('<%= chkProduct.ClientID %>');
        var chkListinputs = chklist.getElementsByTagName("input");
        var AgentMarkup = "";
        for (var i = 0; i < chkListinputs.length; i++) {
            if (chkListinputs[i].checked) {
                AgentMarkup = parseInt(document.getElementById('ctl00_cphTransaction_txtAgentMarkup_' + i).value);
                if (isNaN(AgentMarkup)) {
                    AgentMarkup = 0;
                }
                document.getElementById('ctl00_cphTransaction_txtAgentMarkup_' + i).value = AgentMarkup.toFixed(2);
                
            }
        }
    }
    
</script>
<div style=" padding-top:10px">
        <div class="ns-h3">Update Agent Markup</div>
        
        
        <asp:HiddenField ID="hdnCount" runat="server" />
        <asp:HiddenField ID="hdfAgentId" runat="server" Value="0" />
<asp:HiddenField ID="hdnSourceName" runat="server" Value="0" />
       <div class="bg_white pad_10 bor_gray paramcon">
       
       
       
       <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ForeColor="Green"></asp:Label>
       
           <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-1 col-xs-2 marbot_10"> <asp:Label ID="lblAgent" runat="server" Text="Agent:" Font-Bold="true" ></asp:Label></div>
    
    <div class="col-md-2 col-xs-10 marbot_10"> <asp:DropDownList ID="ddlAgent" runat="server" CssClass="form-control" AutoPostBack="true" 
                                OnSelectedIndexChanged="ddlAgent_OnSelectedIndexChanged">
                                </asp:DropDownList></div>

     <div class="col-md-2 col-xs-12">
     
     <div id="errMess" runat="server" class="error_module" style="display: none;">
                                    <div id="errorMessage" runat="server" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                                    </div>
                                </div>
     <asp:Button ID="btnUpdate" CssClass="btn but_b" runat="server" Text="Update" OnClick="btnUpdate_OnClick"
       
                        OnClientClick="return validation();" />
          <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn but_b"  OnClick="btnClear_Click"/>      
     
      </div>



    <div class="clearfix"></div>
    </div>
    
    
    
    
       
      <div class="mt-1"> 
        <table cellpadding="0" width="100%" cellspacing="0" border="0">
   
            <tr>
                <td>
                    
                    <div class="mt-5 ml-3"> 
                    <asp:CheckBoxList ID="chkProduct" CssClass="chkList" runat="server" RepeatLayout="Table"
                        RepeatDirection="Horizontal" AutoPostBack="true"  OnSelectedIndexChanged="chkProduct_OnSelectedIndexChanged">
                    </asp:CheckBoxList>
                    
                    </div>

                </td>
            </tr>
            <tr>
                <td>
               
               
               
          
               
                
                 
                    <table class="tblmup" id="tblMarkup" runat="server" style="width: 100%; height: 250px;">
                    </table>
                    
                     
                </td>
                
                
            </tr>
                       

        </table>
       
         </div>
        
        
        
        <div>
        
        
         <div id="Div1"  runat="server"  class="bg_white margin-top-10 table-responsive" style="width:100%; overflow:auto; color: #000000;">
            <asp:DataList ID="dlMarkup" runat="server" AutoGenerateColumns="false" GridLines="Both"
                BorderWidth="0" CellPadding="0" CellSpacing="0" Width="100%" ForeColor="" ShowHeader="true" >
                <HeaderStyle BackColor="#333333" Font-Bold="true" ForeColor="White" HorizontalAlign="Left" />
                <HeaderTemplate>
                    <table width="100%" class="tblpax">
                        <tr height="20px">
                            <td width="200px" class="heading">
                                Product Type
                            </td>
                            <td width="200px" class="heading">
                                Agent Name
                            </td>
                             <td width="200px" class="heading">
                                Source
                            </td>
                             <td width="200px" class="heading">
                                AgentMarkup
                            </td>
                            <%--<td width="80px" class="heading">
                                Our Commission
                            </td>
                            <td width="80px" class="heading">
                                Markup
                            </td>
                            --%>
                            <td width="200px" class="heading">
                                MarkupType
                            </td>
                           <%-- <td width="80px" class="heading">
                                Discount
                            </td>
                            <td width="100px" class="heading">
                                DiscountType
                            </td>--%>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table width="100%" class="tblpax">
                        <tr height="20px">
                            <td width="200px">
                                <%#Eval("productType")%>
                            </td>
                            <td width="200px" align="left">
                                <%#Eval("agent_name")%>
                            </td>
                             <td width="200px" align="left">
                                <%#Eval("SourceId")%>
                            </td>
                            <td width="200px" align="left">
                                <%#Convert.ToDecimal(Eval("AgentMarkup")).ToString("N2")%>
                            </td>
                          <%-- <td width="80px" align="left">
                                <%#Eval("OurCommission")%>
                            </td>
                            <td width="80px" align="left">
                                <%#Eval("Markup")%>
                            </td>--%>
                            <td width="200px" align="left">
                                <%#Eval("MarkupTypeName")%>
                            </td>
                            <%--<td width="80px" align="left">
                                <%#Eval("Discount")%>
                            </td>
                            <td width="100px" align="left">
                                <%#Eval("DiscountTypeName")%>
                            </td>--%>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:DataList>
      <asp:Label ID="lblMessage" runat="server"></asp:Label>
      </div>
        
        </div>
        
        
        
        <div> 
        
        
         <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click">First</asp:LinkButton>
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" />
                            <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" />
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click">Last</asp:LinkButton>
        
        </div>
        
        
        
         
        </div>
        </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

