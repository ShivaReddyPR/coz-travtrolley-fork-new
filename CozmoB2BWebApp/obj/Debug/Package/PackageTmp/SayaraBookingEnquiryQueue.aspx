﻿<%@ Page MasterPageFile="~/TransactionBE.master" Language="C#" AutoEventWireup="true" Title="Sayara Booking Enquiry Queue"
    Inherits="SayaraBookingEnquiryQueueUI" EnableEventValidation="false" Codebehind="SayaraBookingEnquiryQueue.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <div class="body_container">
        <h4>
            Sayara Booking Enquiry Queue</h4>
        <div class="row">
            <div class="col-md-2">
                <div class="form-group">
                    <label>
                        From Date</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <uc1:DateControl ID="dcApprovalFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                        </uc1:DateControl>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>
                        To Date</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <uc1:DateControl ID="dcApprovalToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                        </uc1:DateControl>
                    </div>
                </div>
            </div>
             <div class="col-md-2">
                <div class="form-group">
                    <label>
                        Booking Type</label>
                    <div class="input-group">
                        <asp:DropDownList ID="ddlBookngType" runat="server" OnSelectedIndexChanged="ddlBookngType_SelectedIndexChanged" AutoPostBack="true" >
                            <asp:ListItem Value="-1" Text="--Select--"></asp:ListItem>
                            <asp:ListItem Value="KIOSK" Text="KIOSK"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>
                       Vehicle Type</label>
                    <div class="input-group">
                        <asp:DropDownList ID="ddlVehicleType" Enabled="false" runat="server" >
                            <asp:ListItem Value="-1" Text="--Select--"></asp:ListItem>
                            <asp:ListItem Value="LUXURY" Text="LUXURY"></asp:ListItem>
                             <asp:ListItem Value="ECONOMY" Text="ECONOMY"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <label class="center-block">
                    &nbsp;</label>
                <asp:Button OnClick="btnSearch_Click" CssClass="btn btn-primary btn-blue" runat="server"
                    ID="btnSearch" Text="Search" />
            </div>
        </div>
        <div class="col-md-12">
            <asp:UpdatePanel ID="UpdatePanel3" runat="server">
                <ContentTemplate>
                    <asp:GridView AllowPaging="true" PageSize="10" CssClass="table b2b-corp-table" ID="gridViewSCR"
                        DataKeyNames="EnquiryId" runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Bold="true" 
                        OnRowCancelingEdit="gridViewSCR_RowCancelingEdit" OnRowEditing="gridViewSCR_RowEditing"
                        OnRowUpdating="gridViewSCR_RowUpdating" OnPageIndexChanging="gridViewSCR_PageIndexChanging"
                        OnRowDataBound="gridViewSCR_RowDataBound">
                        <Columns>
                            <asp:TemplateField HeaderText="Created On">
                                <ItemTemplate>
                                    <asp:Label ID="lblCreatedOn" runat="server" Text='<%# Eval("CreatedOn")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VoucherNo">
                                <ItemTemplate>
                                    <asp:Label ID="lblVoucherNo" runat="server" Text='<%# Eval("VoucherNo")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Name">
                                <ItemTemplate>
                                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("Name")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Mobile">
                                <ItemTemplate>
                                    <asp:Label ID="lblMobile" runat="server" Text='<%# Eval("Mobile")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PickupLocation">
                                <ItemTemplate>
                                    <asp:Label ID="lblPickupLocation" runat="server" Text='<%# Eval("PickupLocation")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="PickUpDateTime">
                                <ItemTemplate>
                                    <asp:Label ID="lblPickUpDateTime" runat="server" Text='<%# Eval("PickUpDateTime")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="DropOffLocation">
                                <ItemTemplate>
                                    <asp:Label ID="lblDropOffLocation" runat="server" Text='<%# Eval("DropOffLocation")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ArrivalDateTime">
                                <ItemTemplate>
                                    <asp:Label ID="lblArrivalDateTime" runat="server" Text='<%# Eval("ArrivalDateTime")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Currency">
                                <ItemTemplate>
                                    <asp:Label ID="lblCurrency" runat="server" Text='<%# Eval("Currency")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="TotalAmount">
                                <ItemTemplate>
                                    <asp:Label ID="lblTotalAmount" runat="server" Text='<%# Eval("TotalAmount")%>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="VMS Ref numb">
                                <ItemTemplate>
                                    <asp:Label ID="lblVMSRefNum" runat="server" Text='<%# Eval("VMSreceiptNo")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox MaxLength="20" CssClass="form-control" ID="editVMSRefNum" runat="server"
                                        Text='<%# Eval("VMSreceiptNo")%>'></asp:TextBox>
                                    <asp:RequiredFieldValidator ID="smrfvEditURL" runat="server" ControlToValidate="editVMSRefNum"
                                        Text="Required" ValidationGroup="vgEdit" />
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Driver Allocated">
                                <ItemTemplate>
                                    <asp:Label ID="lblDriverAlloc" runat="server" Text='<%# Eval("DriverName")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                
                                <asp:Label Visible="false" ID="lblBokStatus" runat="server" Text='<%# Eval("BookingStatus")%>'> </asp:Label>
                                
                                <asp:Label Visible="false" ID="lblDriverAlloc1" runat="server" Text='<%# Eval("AllocatedDriverId")%>'>
                                
                                </asp:Label>
                                    <asp:DropDownList CssClass="form-control" runat="server" ID="ddlDriver">
                        
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvDriver" runat="server" ControlToValidate="ddlDriver"
                                        ValidationGroup="vgEdit" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vehicle Type">
                                <ItemTemplate>
                                    <asp:Label ID="lblVehType" runat="server" Text='<%# Eval("Vehicle_Name")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                <asp:Label Visible="false" ID="lblVehType1" runat="server" Text='<%# Eval("AllocatedVehicleId")%>'></asp:Label>
                                    <asp:DropDownList CssClass="form-control" runat="server" ID="ddlVehType">
                                        
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvVehType" runat="server" ControlToValidate="ddlVehType"
                                        ValidationGroup="vgEdit" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Vehicle Number">
                                <ItemTemplate>
                                    <asp:Label Text='<%# Eval("VehicleNumber")%>' ID="lblVehNumber" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Booking Status">
                                <ItemTemplate>
                                    <asp:Label Text='<%# Eval("BookStatus")%>' ID="lblBookingStatus" runat="server"></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList CssClass="form-control" runat="server" ID="ddlBookingStatus">
                   
                                    </asp:DropDownList>
                                    <asp:RequiredFieldValidator ID="rfvBookStatus" runat="server" ControlToValidate="ddlBookingStatus"
                                        ValidationGroup="vgEdit" ErrorMessage="Required" InitialValue="0" Display="Dynamic"></asp:RequiredFieldValidator>
                                </EditItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Booking Type">
                                <ItemTemplate>
                                    <asp:Label Text='<%#Eval("Bookingtype")%>' ID="lblBookingType" runat="server"></asp:Label>
                                </ItemTemplate>                               
                            </asp:TemplateField>
                             <asp:TemplateField HeaderText="Vehicle Type">
                                <ItemTemplate>
                                    <asp:Label Text='<%#Eval("VehicleType")%>' ID="lblVehicleType" runat="server"></asp:Label>
                                </ItemTemplate>                               
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="ModifiedOn">
                                <ItemTemplate>
                                    <asp:Label Text='<%# Eval("ModifiedOn")%>' ID="lblModifiedOn" runat="server"></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField HeaderText="Actions">
                                <EditItemTemplate>
                                    <asp:Button ID="btnUpdate" runat="server" CommandName="Update" Text="Update" ValidationGroup="vgEdit" />
                                    <asp:Button ID="btnCancel" runat="server" CommandName="Cancel" Text="Cancel" />
                                </EditItemTemplate>
                                <ItemTemplate>
                                    <asp:Button ID="btnEdit" runat="server" CommandName="Edit" Text="Edit" />
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>
                    </asp:GridView>
                </ContentTemplate>
                <Triggers>
                    <asp:AsyncPostBackTrigger ControlID="gridViewSCR" />
                </Triggers>
            </asp:UpdatePanel>
        </div>
    </div>
</asp:Content>
