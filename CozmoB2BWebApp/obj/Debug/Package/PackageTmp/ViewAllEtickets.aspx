<%@ Page Language="C#" AutoEventWireup="true" Inherits="ViewAllEtickets"  Codebehind="ViewAllEtickets.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme %>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="System.IO" %>
<script language="JavaScript" type="text/javascript" src="JSLib/Utils.js"></script>

<script language="JavaScript" src="prototype.js" type="text/javascript"></script>
<script language="JavaScript" type="text/javascript">
function ClosePop()
{            
    window.close();
}
</script>

<script language="JavaScript" type="text/javascript">
<!--

/* The actual print function */

function prePrint()
{
    document.getElementById('showad').style.display="none";
    document.getElementById('Print').style.display="none";
    document.getElementById('Print1').style.display="none";
   if(document.getElementById('SendMailButton'))
	{
	    
	    document.getElementById('SendMailButton').style.display="none";
	}
    document.getElementById('LowerEmailSpan').style.display='none';
    if(document.getElementById('EditServiceFee'))
	{
	    document.getElementById('EditServiceFee').style.display='none';
	}
    document.getElementById('emailSent').style.display='none';
   // document.getElementById('worldspanPrint').style.display="none";
	window.print();
	setTimeout('showAllButtons()',2000);
	
}
function showAllButtons()
{
    document.getElementById('Print').style.display="block";
	document.getElementById('Print1').style.display="block";
	if(document.getElementById('SendMailButton'))
	{
	    document.getElementById('SendMailButton').style.display="block";
	}
	if(document.getElementById('EditServiceFee'))
	{
	    document.getElementById('EditServiceFee').style.display='block';
	}
    document.getElementById('showad').style.display="block";
	//document.getElementById('worldspanPrint').style.display="block";
}

function ShowEmailDivD()
{  
    document.getElementById('LowerEmailSpan').style.display='block';
    document.getElementById('LowerEmailSpan').innerHTML=document.getElementById('emailBlock').innerHTML;
    document.getElementById('addressBox').focus();
}
function HideEmailDiv()
{    
    document.getElementById('LowerEmailSpan').style.display='none';
    document.getElementById('LowerEmailSpan').innerHTML='';
}
function SendMail()
{
    var addressList=document.getElementById('addressBox').value;    
    if(Trim(addressList)=='')
    {
        document.getElementById('emailSent').style.display='block';
        document.getElementById('messageText').innerHTML='Please fill the address';
        return;
    }
    if(!ValidEmail.test(Trim(addressList)))
    {
        document.getElementById('emailSent').style.display='block';
        document.getElementById('messageText').innerHTML='Please fill correct email address';
        return;
    }
    var paramList ='flightId=' + flightId;
    paramList +=  "&addressList=" + addressList;
    paramList += "&isEticket=true";
    var url = "EmailItineraryPage";
    new Ajax.Request(url, {method: 'post', parameters: paramList, onComplete: DisplayMessage});
}
function DisplayMessage()
{
    document.getElementById('emailSent').style.display='block';
    document.getElementById('messageText').innerHTML='Email Sent Successfully';
    HideEmailDiv();
}
function spicejetEticket()
{
  document.getElementById('SGEticket').submit();
}
function indigoEticket()
{
  document.getElementById('Indigo').submit();
}
var bookingSource;
function EticketRet(source)
{
     bookingSource=source;
     var paramList ='messId=1&source='+source;
     var url = "EticketAjax";
     new Ajax.Request(url, {method: 'post', parameters: paramList,onComplete:SubmitForm});     
}
function SubmitForm(response)
{
    if(bookingSource == 'Amadeus')
    {
        var res = response.responseText;
        document.getElementById('Eticket').action=res;
        document.getElementById('Eticket').submit();
    }
    else if(bookingSource == 'Galileo')
    {
        var viewStat = response.responseText;    
        var url='<%=Request.Url.Scheme %>://www.viewtrip.com?__EVENTTARGET=&__EVENTARGUMENT=__VIEWSTATE='+viewStat+'&lstLanguages=en-US&lstBookingRegion=IN&txtRecLoc=<%=flightItinerary.PNR%>&txtLName=<% =flightItinerary.Passenger[0].LastName%>&btnLocalSignIn=Sign In&rdoClock=12&rdoTemp=F&chkRememberSettings=RememberMe';
        window.location=url;
    }
}
var flightId;
// -->
</script>
<html xmlns="<%=Request.Url.Scheme %>://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>E Ticket</title>    
    <script src="ash.js" type="text/javascript"></script>
    <link rel="stylesheet" type="text/css" href="Eticket.css" />

</head>
<body>
    <form id="form1" runat="server">
    <asp:MultiView ID="MultiViewETicket" runat="server">
        <asp:View ID="TicketView" runat="server">    
        
        <table id="container" cellspacing="0" cellpadding="0">
            <%      bool isLCC = false;
                    string code = string.Empty;
                    string airlineName = string.Empty;
                
                    foreach (FlightInfo segment in flightItinerary.Segments)
                    {
                        string airlineCode = segment.Airline;
                        Airline airline = new Airline();
                        airline.Load(airlineCode);
                        if (airline.AirlineCode == flightItinerary.ValidatingAirlineCode && airlineName == "")
                        {
                            airlineName = airline.AirlineName;
                        }
                        
                        if (airlineCode.Equals("IT"))
                        {
                            int flightnumber = Convert.ToInt16(segment.FlightNumber);
                            if (flightnumber >= Convert.ToInt32(ConfigurationSystem.AirDeccanConfig["StartRangeDnIt"]) && flightnumber <= Convert.ToInt32(ConfigurationSystem.AirDeccanConfig["EndRangeDnIt"]))
                            {
                                isDN = true;
                            }
                        }
                        if (airline.IsLCC)
                        {
                            isLCC = true;
                            code = airline.AirlineCode;
                            break;
                        }
                    }
            string bookingSource = flightItinerary.FlightBookingSource.ToString();
                    
             %>
             
            <tr>
                    <td class="fleft border-bottom-black-1 padding-top-10 margin-left-15" style="width:100%;">
                        <input id="Print" onclick="prePrint()" type="button" value="Print Ticket" />
                    </td>                    
            </tr>
            <tr class="email-message-parent display-none" id="emailSent">
                    <td style="float:left;width:100%;margin:auto;text-align:center;">
                        <span class="email-message-child" id="messageText">Email sent successfully</span>
                    </td>
            </tr>
		<tr>
			<td>
				<table id="header" cellspacing="0" cellpadding="10">
					<tr valign="top">
						<td class="width_230">
							<div id="agency" class="fleft">
							  <% if (IsShowLogo)
                                {
							          %>
								<span style="display:block; width:100%; margin-bottom:10px;"><img id="airlineLogo" src="<%=AirlineLogoPath%>" alt="Airline"  /></span>
								<%} %>
								 <span style="font-size:15px ; font-weight:bold"><%=agency.Name %></span>
							</div>
						</td>
						<td class="width_330">
							<div id="ticket_desc">
								<h1>
								    <%if ((ticket != null && ticket.ETicket))
                                    { %>
								    E - Ticket
								    <%  }
                else
                {
                    if (Request["pendingId"] != null)
                    { %>
                                    E - Ticket
                                    <%}
                                      else
                                      { %>
                                    Itinerary
                                    <%  }
                                    } %>
								</h1> 
								
								<h3>PNR :&nbsp;<%
                                            if (flightItinerary.FlightBookingSource == BookingSource.HermesAirLine)
                                            {%>
                                               <%=ticket.ValidatingAriline%>-<%=flightItinerary.Segments[0].AirlinePNR%>
                                            <%}                                
								            else if (!isLCC)  { %>
                                             <%=bookingSource%> - <%=flightItinerary.PNR%>
                                        <%}
                                         else
                                         { %>
                                        <%=code%> -  <%=flightItinerary.PNR%>
                                        <%} %>
                            </h3> 
                            <% if (ticketList.Count != 0 )
                               {%>
                            <h4>Issued Date : <% = Util.UTCToIST(ticket.IssueDate).ToString("ddd dd MMM yyyy")%>
                            </h4><%} else{%><h4>Issued Date : <% = Util.UTCToIST(booking.CreatedOn).ToString("ddd dd MMM yyyy")%>
                            </h4> <%}%>
                            		
                            <%//string ffNumber = FlightPassenger.GetFFNumber(ticket.PaxId);
                              if (ffNumber != null && ffNumber.Trim() != string.Empty)
                              {%>                                
                              <h4>FQTV : <% = ffNumber%></h4>
                            <%} %>			  
							</div>
						</td>
				
						<td class="width_230" rowspan="2">
							<div id="address">
							        <% if (IsShowLogo)
                                     {
							          %>
							       <span ><img id="agentLogo" src="<%=AgentLogoPath%>" alt=""  /></span>
							      <%} %>
							     
								<span><% = agency.Address %></span>
								<%--<span><%=agency.Address.Line2%></span>--%>
								<span><%=cityName %> <%=agency.POBox %></span>
								<% if (agency.Phone1 != null && agency.Phone1.Length > 0)
                                    { %>
								<span> Phone : <%=agency.Phone1%></span>
								<%  } %>
							</div>
						</td>
					</tr>
					<tr>
					    <td colspan="2">
					        <table width="400px" style="font-size:13px; margin-left:0px;" cellpadding="3" cellspacing="2" border="0">
					            <tr>
					                <td colspan="2" style="font-weight:bold; font-size:16px;">
					                   <% 
                                           if (Request["pendingId"] == null)
                                           {
                                               string ticketStatus = "Cancelled";
                                               for (int j = 0; j < ticketList.Count; j++)
                                               {
                                                   if (ticketList[j].Status != "Cancelled" && ticketList[j].Status != "Voided" && ticketList[j].Status != "Refunded")
                                                   {
                                                       ticketStatus = "Ticketed";
                                                       break;
                                                   }
                                               }
                                               if (ticketStatus == "Cancelled")
                                               { %>
					                 
					                             <span> Ticket Status : <% =ticketStatus%></span>
					                                 <%}
                                      } %>
					                </td>
					            </tr>
					            <tr>
					                <td style="font-size:15px; font-weight:bold;">Passenger Name</td>
					                <td style="font-size:15px; font-weight:bold;">Ticket Number</td>
					            </tr>
					            
					            <%  for (int j = 0; j < ticketList.Count; j++)
                                    {
                                        if (ticketList[j].Status != "Cancelled" && ticketList[j].Status != "Voided" && ticketList[j].Status != "Refunded")
                                        {%>
					            <tr>
					                <%
                                        paxName = FlightPassenger.GetPaxFullName(ticketList[j].PaxId); 
                                            
                                    %>
					                <td><%=paxName%></td>
					                 <% if (ticketList.Count > 0)
                                      {
                                      if (!isLCC)
                                      { %>
					                         <td> <%=ticketList[j].ValidatingAriline%><% = ticketList[j].TicketNumber%></td>
					                <%}
                                   else
                                   { %>
                                    <td> <% = ticketList[j].TicketNumber%></td>
					                 <%}
                                     }%>
					            </tr>
					                    <%}
                                    } %>
					        </table>
					      </td>
					 </tr>
					            
			   </table>
			</td>
		</tr>
		
		<!-- Ticket details table begins -->	
		<%  for (int count = 0; count < flightItinerary.Segments.Length; count++)
        { %>	
			<tr>
			<%  Airline airline = new Airline();
                airline.Load(flightItinerary.Segments[count].Airline);
             %>
				<td>
				<table class="fleft width_666" border="0" cellspacing="0" cellpadding="0">
					<tr>
						<td>
							<table class="flight_desc" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="width_163"><div class="flight_day"><%=flightItinerary.Segments[count].DepartureTime.ToString("ddd dd MMM yyyy") %></div></td>
									<td class="width_324"><div class="flight_name"><% = airline.AirlineName %> <% = flightItinerary.Segments[count].Airline %> <% = flightItinerary.Segments[count].FlightNumber %></div></td>
									<td class="width_163">
									<div class="flight_ref">
									    <%  if (flightItinerary.Segments[count].AirlinePNR != null && flightItinerary.Segments[count].AirlinePNR.Length > 0)
                                        { %>
                                        Airline Ref : <% = flightItinerary.Segments[count].AirlinePNR %>
                                    <%  } %>
								     </div>
                                         <%
                                            string status = "NotConfirmed";
                                            Dictionary<string, List<string>> statusList = ConfigurationSystem.StatusListConfig;
                                            foreach (KeyValuePair<string, List<string>> statuses in statusList)
                                            {
                                                if (statuses.Value.Contains(flightItinerary.Segments[count].Status))
                                                {
                                                    status = statuses.Key;
                                                    break;
                                                }
                                            }
                                        %>  
                                        <div class="flight_ref">
                                            Status : <%=status %>
                                        </div>
								     </td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table class="width_630 fright" border="0" cellspacing="0" cellpadding="5">
								<tr>
									<td class="width_50"><div class="from">From:</div></td>
									<td class="width_460">
										<div class="departure_stn">
											<%=flightItinerary.Segments[count].Origin.AirportCode%>
											(<%=flightItinerary.Segments[count].Origin.AirportName%>)
											<%if (flightItinerary.Segments[count].DepTerminal != null && flightItinerary.Segments[count].DepTerminal.Trim() != "")%>
											<span>Terminal <%=flightItinerary.Segments[count].DepTerminal%></span>
										</div>
									</td>
									<td class="width_120"><div class="departure_time">Dep: <%=flightItinerary.Segments[count].DepartureTime.ToShortTimeString() %></div></td>
								</tr>
								<tr>
									<td class="width_50"><div class="to">To:</div></td>
									<td class="width_460">
										<div class="arrival_stn">
											<%=flightItinerary.Segments[count].Destination.AirportCode%>
											(<%=flightItinerary.Segments[count].Destination.AirportName %>)
											<%if (flightItinerary.Segments[count].ArrTerminal != null && flightItinerary.Segments[count].ArrTerminal.Trim() != "")%>
											 <span>Terminal <%=flightItinerary.Segments[count].ArrTerminal%></span>
										</div>
									</td>
									<td class="width_120"><div class="arrival_time">Arr: <%=flightItinerary.Segments[count].ArrivalTime.ToShortTimeString() %></div></td>
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table class="flight_profile" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="width_50"><span><%=flightItinerary.Segments[count].BookingClass%> Class</span></td>
									<td class="width_80">
									<%if (flightItinerary.Segments[count].Duration.TotalMinutes > 0) %>
									<%{ %>
									    <span>
									        <%= ((flightItinerary.Segments[count].Duration.Days * 24 + flightItinerary.Segments[count].Duration.Hours) + ":" + flightItinerary.Segments[count].Duration.Minutes)%> Hours Flight
									    </span>
									<%} %>
									</td>
									<td class="width_60">
									    <span>
									        <%if(flightItinerary.Segments[count].Stops==0)
                                                {%>
                                                    Non stop
                                                <%}
                                            else
                                            { %>                                    
                                                <%=flightItinerary.Segments[count].Stops%>
                                            stop/s
                                            <%} %>
									    </span>
									</td>
									<td class="width_90">
									<%if (ticketList.Count != 0)
                                      { %>
									    <span>
									        <% 
                                        if (ticket.PtcDetail.Count > 0 && ticket.PtcDetail[paxTypeIndex].Baggage != null && ticket.PtcDetail[paxTypeIndex].Baggage.ToString().Trim() != string.Empty) %>
									        <%{ %>
									        Baggage(per person): <% = ticket.PtcDetail[paxTypeIndex].Baggage%>
									        <% } %>
									    </span>
									<%} %>
									</td>
									<td class="width_80">
									<%  if (flightItinerary.Segments[count].Craft != null && flightItinerary.Segments[count].Craft.Trim()!="")
                                        {   %>
									<span>Aircraft: <% = flightItinerary.Segments[count].Craft %></span></td>
									<% } %>									
									<td class="width_50">
									<span>
									<%string mealCode = string.Empty; %>
									     <%  if (ticket != null) { mealCode = FlightPassenger.GetMealCode(ticket.PaxId); }
                                                    if (mealCode != string.Empty)
                                        {%> Meal:                                    
                                        <%=Meal.GetMeal(mealCode).Description%>
                                        (subject to availability)
                                        <% } %>
									</span>
									</td>									
								</tr>
							</table>
						</td>
					</tr>
					<tr>
						<td>
							<table class="partition"><tr><td></td></tr></table>
						</td>
					</tr>
				</table>
				</td>
			</tr>
	    <% } %>    
		<%--<%if (policy != null && policy.PolicyNumber != null && policy.PolicyNumber.Length > 0)
    { %>
		<tr>
		  <td>
		    <table cellpadding="0" cellspacing="0" border="0">
		      <tr style="padding:10px 0 0 5px; font-size:14px;">
		        <td>Insurance Issuing Company: <b>ICICI Lombard</b></td>
		        <td style="padding-left:30px;">Policy Number: <b><%=policy.PolicyNumber %></b></td>
		      </tr>
		    </table>
		  </td>
		</tr>
		<%} %>--%>
		<tr>
			<td>
				<table class="special_note">
					<tr>
						<td class="note width_460">
							<h4>
								<b>
								    <%  if ((ticketList.Count != 0  && ticket.ETicket) || Request["pendingId"] != null)
                                        {   %>
								        This is an electronic ticket. Please carry a positive identification for check in.
							        <%  }
                                    else
                                    { %>
                                        This is not an E-Ticket. Please collect your ticket from your travel agent.
                                   <%}  %>
                                  </b>
							</h4>
						</td>
                        <%if (preferenceValue == preference.ETicketShowFare)
                          {
                            if(Request["flightId"] != null)
                            {
                              //decimal insurancePrice = 0;
                              decimal totalFare = 0;
                              decimal serviceFee = 0;
                              bool showServiceFee = false;
                              if (ticket.ShowServiceFee == ServiceFeeDisplay.ShowSeparately)
                              {
                                  showServiceFee = true;
                              }
                              else
                              {
                                  showServiceFee = false;
                              }
                              decimal totalTax = 0;
                              decimal publishFare = 0;
                              decimal additionalTxnFee = 0;
                              decimal totalTransactionFee = 0;
                              for (int j = 0; j < ticketList.Count; j++)
                              {

                                  if (ticketList[j].Status != "Cancelled" && ticketList[j].Status != "Voided" && ticketList[j].Status != "Refunded")
                                  {
                                      //  ticketList[j].Price=price.Load(ticketList[j].
                                      
                                      if (flightItinerary.BookingMode == BookingMode.WhiteLabel || flightItinerary.BookingMode == BookingMode.Itimes)
                                      {
                                          totalTax += ticketList[j].Price.Tax + ticketList[j].Price.OtherCharges + ticketList[j].ServiceFee + ticketList[j].Price.SeviceTax + ticketList[j].Price.WLCharge;
                                      }
                                      else
                                      {
                                          if (showServiceFee)
                                          {
                                              totalTax += Convert.ToDecimal(ticketList[j].Price.Tax + ticketList[j].Price.OtherCharges);
                                          }
                                          else
                                          {
                                              totalTax += Convert.ToDecimal(ticketList[j].Price.Tax + ticketList[j].Price.OtherCharges + ticketList[j].ServiceFee);
                                          } 
                                      }


                                      if (flightItinerary.BookingMode == BookingMode.WhiteLabel || flightItinerary.BookingMode == BookingMode.Itimes)
                                      {
                                          totalFare += ticketList[j].Price.PublishedFare + ticketList[j].Price.Tax + ticketList[j].Price.AdditionalTxnFee + ticketList[j].Price.OtherCharges + ticketList[j].ServiceFee + ticketList[j].Price.SeviceTax + ticketList[j].Price.WLCharge;
                                      }
                                      else
                                      {
                                          totalFare += Convert.ToDecimal(ticketList[j].Price.PublishedFare + ticketList[j].Price.Tax + ticketList[j].Price.AdditionalTxnFee + ticketList[j].Price.OtherCharges + ticketList[j].Price.TransactionFee + ticketList[j].ServiceFee);//+ insurancePrice 
                                      }
                                      additionalTxnFee += ticketList[j].Price.AdditionalTxnFee;
                                      publishFare += ticketList[j].Price.PublishedFare;
                                      serviceFee += ticketList[j].ServiceFee;
                                      totalTransactionFee += ticketList[j].Price.TransactionFee;
                                  }
                              }

                              string AmountPrefix = "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"] + "";
                              if (flightItinerary.Passenger[0].Price.Currency != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
                              {
                                  AmountPrefix = flightItinerary.Passenger[0].Price.Currency;
                                  totalTransactionFee = totalTransactionFee / flightItinerary.Passenger[0].Price.RateOfExchange;
                                  publishFare = publishFare / flightItinerary.Passenger[0].Price.RateOfExchange;
                                  totalTax = totalTax / flightItinerary.Passenger[0].Price.RateOfExchange;
                                  additionalTxnFee = additionalTxnFee / flightItinerary.Passenger[0].Price.RateOfExchange;
                                  serviceFee = serviceFee / flightItinerary.Passenger[0].Price.RateOfExchange;
                                  totalFare = totalFare / flightItinerary.Passenger[0].Price.RateOfExchange;
                              }
                                
                                
                                
                               %>
						<td class="width_240">
							<table class="total_amount" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td class="width_120"><span>Air Fare:</span></td>
									<td class="width_120"><span><%=AmountPrefix %>
                                   <% = publishFare.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] )%></span></td>
                                 
								</tr>
								<tr>
									<td class="width_120"><span>Fee & Surcharge:</span></td>
									<td class="width_120"><span><%=AmountPrefix %> <% = totalTax.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] )%></span></td>
								</tr>
                                <%if (additionalTxnFee > 0)
                                  { %>
                                <tr>
                                    <td class="width_120"><span>Txn Fee:</span></td>
                                    <td class="width_120"><span><%=AmountPrefix %> <% = additionalTxnFee.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] )%></span></td>
                                </tr>
                                <%} %>
								<%if (totalTransactionFee > 0)
          { %>
								<tr>
									<td class="width_120"><span>Tra Fee:</span></td>
									<td class="width_120"><span><%=AmountPrefix %> <% = totalTransactionFee.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] )%></span></td>
								</tr>
								<%} %>
								<%--<%if (policy != null && policy.PolicyNumber != null && policy.PolicyNumber.Length > 0)
                                {
                                  insurancePrice = policy.DisplayPrice;
                                    %>
                                    <tr>
									<td class="width_120"><span>Insurance Charges:</span></td>
									<td class="width_120"><span><%=AmountPrefix %> <% = insurancePrice.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] )%></span></td>
								</tr>
								<%} %>--%>
								<% if (showServiceFee)
           {%>
								<tr id="serviceFeeValue">
									<td class="width_120"><span>Txn Fee:</span></td>
									<td class="width_120"><span><%=AmountPrefix %> <% = serviceFee.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] )%></span></td>
									
								</tr>
								<%} %>
								<tr id="serviceFeeEditBlock" style="display:none;"><td class="width_120 fleft"><span>Txn Fee:</span></td>
									<td class="width_120"><span><input type="text"  id="serviceFeeTextbox" style="width:60px;margin: 0 0 0 55px;" value=" <% = serviceFee.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] )%>" /></span></td></tr>
								
                                <tr id="displayServiceFeeBlock" style="display:none;">
									<td class="width_120 fleft"><span>Display Txn Fee:</span></td>
									<td class="width_120"><span><select name="displayServiceFee" id="displayServiceFee">
									    <% if (ticket.ShowServiceFee == ServiceFeeDisplay.ShowInTax)
                {%>
								            <option selected="selected" value="<%=(int)ServiceFeeDisplay.ShowInTax %>"><%=ServiceFeeDisplay.ShowInTax.ToString()%></option>
								            <%}
                      else
                      { %>
								            <option value="<%=(int)ServiceFeeDisplay.ShowInTax %>"><%=ServiceFeeDisplay.ShowInTax.ToString()%></option>
                                            <%} %>                     
                                               <% if (ticket.ShowServiceFee == ServiceFeeDisplay.ShowSeparately)
                                                  {%>                    
								            <option selected="selected" value="<%=(int)ServiceFeeDisplay.ShowSeparately %>"><%=ServiceFeeDisplay.ShowSeparately.ToString()%></option>
								        <%}
                  else
                  { %>
								            <option value="<%=(int)ServiceFeeDisplay.ShowSeparately %>"><%=ServiceFeeDisplay.ShowSeparately.ToString()%></option>                                          
                                          <%} %>                     
								        </select></span></td>
								</tr>
														<tr>
									<td class=""><span>Total Air Fare:</span></td>
									<td class="width_120"><span><%=AmountPrefix %><% = totalFare.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] )%></span></td>
								</tr>
								
							</table>
							
						</td>
       
              <% } }%>
					</tr>
				</table>
			</td>
		</tr>
		<%if(Request["flightId"]!=null) { %>
		<script type="text/javascript">flightId='<%=ticket.FlightId%>'</script>
            <% if (ticket.Remarks.Length > 0)
               { %>
		<tr>
		    <td style="font-weight:bold">
		        <div style="margin-left:15px">Remarks</div>
		    </td>
		</tr>
		<tr>
		    <td>
		            <div style="margin-left:15px;"><%=ticket.Remarks%></div>
		    </td>
		</tr>
		        <%
            }
            if (ticket.FareRule.Length > 0)
            {%>
        <tr>
            <td style="font-weight:bold">
                <%if (ticket.Remarks.Length > 0)
                  { %>
              <div style="margin-top:3px">
                <%} %>
              <div style="margin-left:15px">Fare Rules</div>
            <%if (ticket.Remarks.Length > 0)
              { %>
                </div>
                <%} %>
            </td>
        </tr>
        <tr>
            <td>
                <div style="margin-left:15px"><%=ticket.FareRule%></div>
            </td>
        </tr><%} %>
                <%
            }%>
            <tr>
                <td>
                    <div style="margin-top: 4px">
                    </div>
                </td>
            </tr>
		<tr>
			<td class="fleft">
				<div id="footer">
				<%
                    if (ticketList.Count != 0 && ticketList[0].CorporateCode != null && ticketList[0].CorporateCode.Trim().Length > 0)
                    { %>
				   Promotional Code: <%=ticketList[0].CorporateCode%>
				  <%} %>
				   <%
                      if (ticketList.Count != 0 && ticketList[0].TourCode != null && ticketList[0].TourCode.Trim().Length > 0)
                    { %>
				   | Tour Code: <%=ticketList[0].TourCode%>
				  <%} %>
				  <%
                      if (ticketList.Count != 0 && ticketList[0].Endorsement != null && ticketList[0].Endorsement.Trim().Length > 0)
                    { %>
				   | Endorsement: <%=ticketList[0].Endorsement%>
				  <%} %>
				    <%
			        if(isDN)
                    {%>
                    <p>
                       <h4><b>
                        <%=ConfigurationSystem.AirDeccanConfig["FlightMessage"]%>
                        </b></h4>
                   
                    </p>
                             
                    <%}%>
                 
                <div id="showad">
                <% 
                       if (!isLCC && (ticket.PaxType == PassengerType.Adult || ticket.PaxType == PassengerType.Senior))
                       { %>
<%--<a href="<%=Request.Url.Scheme %>://www.zoomtra.com/e-ticket/click.aspx?source=Ballantine&type=Cocktails" target="_blank"><img alt="Ballantine" src="<%=Request.Url.Scheme %>://www.zoomtra.com/e-ticket/open.aspx?source=Ballantine&type=Cocktails" width="728" height="90"/></a>--%>
<%--<%= CT.CMS.DynamicAdvertise.GetActiveAdDiv(ProductType.Flight,CT.CMS.DyAdPage.ETicket,Request["HTTP_HOST"],Request.ServerVariables["URL"],"")%>--%><%} %>
                </div>
				    <p>
					    Carriage and other services provided by the carrier are subject to conditions of carriage which hereby incorporated by reference. These conditions may be obtained from the issuing carrier. If the passenger's journey involves an ultimate destination or stop in a country other than country of departure the Warsaw convention may be applicable and the convention governs and in most cases limits the liability of carriers for death or personal injury and in respect of loss of or damage to baggage.
				    </p>
				    <p>
				    <%if (ticketList.Count != 0)
             { %>
					    <%=ticket.TicketAdvisory%>
					    <%}
           else
           { %>
					    <%=flightItinerary.TicketAdvisory%>
					    <%} %>
				    </p>
				<%  if(flightItinerary.Segments[0].Airline.ToUpper() == "G8")
                    {   %>
				    <div style="width:100%; float:left">
				        <div style="font-weight: bold; font-size: 16px; margin-top:10px"><span style="border-bottom: solid 2px black;">Airline Terms and Conditions</span></div>
				        <div style="font-weight: bold; margin-top:10px"><span style="border-bottom: solid 2px black;">1. Check In</span></div>
				        <div style="margin-top: 10px">Check-in commences 2 hours prior to scheduled departure (180 minutes incase of Srinagar and Jammu airports).Passengers to report 60 minutes prior to scheduled departure time (90 minutes in case of Srinagar and Jammu airports).</div> 
				        <div style="font-weight: bold; margin-top: 10px">Check-in counter closes strictly 40 minutes prior to the scheduled departure time. Boarding Gate closes 20 minutes prior to scheduled departure time. If you fail to show up at the check-in counter or the boarding gate before the closure time, you will be treated as �no show� passenger and fare paid will be forfeited.</div>
				        <div style="margin-top: 10px">GoAir reserves its right to refuse passage to any Customer who is under the influence of alcohol or drugs, violent or abusive, or where the Company or its duly authorised representatives / staff believe that it is necessary for the safety and comfort of other Customers or for the protection of the aircraft and / or other assets. Such passengers will be treated as �no show� passengers and fare paid will be forfeited.</div>
				        <div style="font-weight: bold; margin-top:10px"><span style="border-bottom: solid 2px black;">2. Baggage Allowance</span></div>
				        <div style="margin-top: 10px">Checked Baggage allowance is 15Kg per Customer (no baggage allowance for infants); excess baggage is charged at Rs 70/Kg. Cabin Baggage allowance is 7Kg per Customer (no baggage allowance for infants). The maximum size of the cabin hand baggage cannot exceed 55 cm x 40 cm x 20 cm (20 x 14 x 9ins). GoAir assumes no responsibility or liability for delay in carriage of baggage by air. </div>
				        <div style="font-weight: bold; margin-top:10px"><span style="border-bottom: solid 2px black;">2. Cancellation and rescheduling of flights</span></div>
				        <div style="margin-top: 10px">GoAir will endeavour to operate the flights as per schedule, however GoAir reserves its right to cancel, reschedule or delay the commencement or continuance of any flight or alter the stopping place or deviate from the route of the journey without thereby incurring any liability in terms of compensation, damages or loss whether direct, indirect, consequential or special or otherwise in any manner whatsoever. In case of any such cancellation or rescheduling of any flight due to any reason, GoAir does not provide any accommodation or compensation to the affected passengers. </div>
				        <div style="font-weight: bold; margin-top:10px"><span style="border-bottom: solid 2px black;">Detailed terms and conditions</span></div>
				        <div style="font-weight: bold; margin-top: 10px">It is mandatory for the Customers to go through the detailed terms and conditions which govern booking of tickets and travel in GoAir network which are displayed at GoAir website <a href="<%=Request.Url.Scheme %>://goair.in/terms.asp" target="_blank"><%=Request.Url.Scheme %>://goair.in/terms.asp</a>. Booking of ticket constitutes the acceptance of these terms and conditions with respect to travel in GoAir network.</div>
				    </div>
				<%  }   %>
				<div id="showTBOAd">
                      <%if (search.Length > 0)
                        {
                            //Session["TBOConnectResults"] = search;%>
                        <div class="cont">        
        <div class="outtxt">
            <span> <%=agency.Name%> can also provide you great deals in <%=req.CityName%>. All hotel featured have been referred by customers and checked for quality of services provided. </span>
        </div>
        <div class="hadtxt">
            <span> Some Offers are : </span>
        </div>
        <div class="wid100"> 
            <table class="txttbl">
                <tr class="txtrw">
                    <td class="col1"> <span style="background:rgb(217,217,217);"><b> Hotel name </b></span>  </td>
                    <td class="col2"> <span style="background:rgb(217,217,217);"><b> Address </b></span> </td>
                    <td class="col3"> <span style="background:rgb(217,217,217);"><b> Star Catagory </b></span> </td>
                    <td class="col4"> <span style="background:rgb(217,217,217);"><b> Price per night </b></span> </td>                    
                </tr>
                     <% string rating = string.Empty;
                        for (int i = 0; i < search.Length; i++)
                        {
                            if (search[i].Rating != HotelRating.All)
                            {
                                rating = search[i].Rating.ToString();
                                rating = rating.Insert(rating.Length - 4, " ");
                            }
                            else
                            {
                                rating = "All";
                            }
                            if (i == Convert.ToInt16(CT.Configuration.ConfigurationSystem.HotelConnectConfig["tboConnectResults"]))
                            {
                                break;
                            }%>
                        
                <tr class="txtrw">
                    <td class="col1"> <span> <%=search[i].HotelName%></span> </td>
                    <td class="col2"> <span> <%=search[i].HotelAddress%></span> </td>
                    <td class="col3"> <span> <%=rating%> </span> </td>
                    <td class="col4"> <span><%=search[i].TotalPrice.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] )%></span> </td>
                </tr>  
                <%} %>              
            </table>
        </div>
        <div class="wid100">
            <span class="fnt"> <b> Call :- <%=agency.Name%>(<%=agency.City%>),<%if (agency.Phone2 != null && agency.Phone2.Length > 0)
                                                                                                    { %><%=agency.Phone2%><%} %><%if (agency.Phone2 != null && agency.Phone2.Length > 0)
                                                                                                                                                                                                           { %> Ph:-<%=agency.Phone1%> <%} %></b> <b style="color:rgb(212,170,86);"> (for more options) </b> </span>
        </div>
    </div>
                        
                      <%} %>
                </div>
			    </div>
			</td>
		</tr>
		
		
		
		<tr id="worldspanPrint" class="fleft" style="width:640px; margin-left:15px;"><td style=" text-align:center; padding-top:10px; padding-bottom:4px;"><b>
                        <% 
                            if (ticketList.Count != 0 && ticket.ETicket && flightItinerary.FlightBookingSource == BookingSource.WorldSpan)
                      {%>
                        (If you are not able to print your ticket Please <a href="https://mytripandmore.com/Frameset.aspx?pageName=Itinerary.aspx&PNR=<%=flightItinerary.PNR.Trim()%>&clockFormat=12&lastName=<%=ticket.PaxLastName%>" target="_blank">
                            click here</a>)
                        <%} %>
                          <% 
                              else if (ticketList.Count != 0 && ticket.ETicket && (flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp))
                            {%>
                        (Click here to view the ticket directly at Spicejet.com <a href="javascript:spicejetEticket()" >
                            click here</a>)
                        <%}
                          else if (ticketList.Count != 0 && ticket.ETicket && (flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp))
                            {%>
                        (Click here to view the ticket directly at goIndigo.com <a href="javascript:indigoEticket()" >
                            click here</a>)
                        <%}
                          else if (ticketList.Count != 0 && ticket.ETicket && flightItinerary.FlightBookingSource == BookingSource.Amadeus)
                            {%>
                        (If you are not able to print your ticket Please <a href="javascript:EticketRet('Amadeus')" >
                            click here</a>)
                        <%}
                          else if (ticketList.Count != 0 && ticket.ETicket && flightItinerary.FlightBookingSource == BookingSource.Galileo)
                            {%>
                        (If you are not able to print your ticket Please <a href="javascript:EticketRet('Galileo')" >
                            click here</a>)
                        <%}%>
                    </b></td></tr>
                    
                    
                    
		<tr>
           <td class="fleft" style="width:100%;border-top:solid 1px #000;">
                <input class="fleft button-width margin-top-5 margin-left-15 padding-left-20" id="Print1" onclick="prePrint()" type="button" value="Print Ticket" />
                <%if (ticketList.Count != 0)
                  {%>
                <input class="fleft button-width margin-top-5 margin-left-15 padding-left-20" id="SendMailButton" onclick="javascript:ShowEmailDivD()" type="button" value="E-mail Ticket" />
                <%} %>
           <span id="Errordiv" style="margin-top:10px; margin-left:5px;color:Red;display:none"></span>
           </td>                                
        </tr>        
		
	</table>
	<span id="LowerEmailSpan" class="fleft" style="margin-left: 15px;">                       
        </span>
        <div id="emailBlock" class="width-200 display-none">
                        <div class="fleft border-y width-200" style="position:absolute; left:340px; top:180px;">
                            <div class="fright text-right padding-5 width-190 light-gray-back">
                                <img alt="Close" onclick="HideEmailDiv()" src="Images/close.gif" /></div>
                            <div class="fleft padding-5 width-190 white-bg border-top-black">
                                <div class="fleft center width-100 bold">
                                    Enter email address</div>
                                <div class="fleft center width-100 margin-top-5">
                                    <input id="addressBox" name="" type="text" /></div>
                                <div class="fleft center width-100 margin-top-5">
                                    <input onclick="SendMail()" type="button" value="Send mail" /></div>
                                <div class="fleft center width-100 margin-top-5 font-10">
                                    <a href="javascript:HideEmailDiv()">Cancel</a></div>
                            </div>
                        </div>
                    </div>
     </asp:View>
        <asp:View ID="ErrorView" runat="server">
            <div class="width-720 margin-left-30" style="text-align: center">
                <br />
                <br />
                <br />
                <span>
                    <asp:Label ID="ErrorMessage" runat="server"></asp:Label>
                </span>
            </div>
        </asp:View>
    </asp:MultiView>
    </form>
		<form id="SGEticket" method="post" action="<%=Request.Url.Scheme %>://book.spicejet.com/skylights/cgi-bin/skylights.cgi">
		<input type="hidden" name="event" value="lookup" />
        <input type='hidden' name='module' value='C3' />
        <input type='hidden' name='page' value='' />

        <input type='hidden' name='language' value='EN' />
        <input type='hidden' name='mode' value='' />
        <input type='hidden' name='sid' value='' />
        <input type='hidden' name='ref' value='' />
		    <input type='hidden' name='pnr1' value='<%=flightItinerary.PNR %>' />
        <input type='hidden' name='firstName' value='<%=flightItinerary.Passenger[0].FirstName%>' />
        <input type='hidden' name='lastName' value='<%=flightItinerary.Passenger[0].LastName%>' />
        <input type='hidden' name='origin' value='<%=flightItinerary.Origin%>' />
        <input type='hidden' name='destination' value='<%=flightItinerary.Destination%>' />
        <input type='hidden' name='pnr2' value='' />
        <input type='hidden' name='deptDay' value='' />
        <input type='hidden' name='departMonth' value='' />
        <input type='hidden' name='contactEmail' value='' />
		</form>		
	
		
    
    
<form id='Indigo' method='POST' action='<%=Request.Url.Scheme %>://book.goindigo.in/skylights/cgi-bin/skylights.cgi'>

		<input type="hidden" name="view_action" value="get"/>

		<input type='hidden' name="pnr" value='<%=flightItinerary.PNR %>'/>
		<input type='hidden' name="pax_first_name" value='<%=flightItinerary.Passenger[0].FirstName%>'/>
		<input type='hidden' name="pax_last_name" value='<%=flightItinerary.Passenger[0].LastName%>' />
		<input type='hidden' name="origin_city" value='<%=flightItinerary.Origin%>' />
		<input type='hidden' name="destination_city" value='<%=flightItinerary.Destination%>' />
		<input type='hidden' name="flight_number" value=""/>
		<input type='hidden' name="depart_date" value=""/>

		<input type='hidden' name="contact_first_name" value=""/>
		<input type='hidden' name="contact_last_name" value=""/>
		<input type='hidden' name="contact_phone_number" value=""/>
		<input type='hidden' name="contact_email" value=""/>
		<input type='hidden' name="contact_zip_code" value=""/>
		<input type='hidden' name="cardholder_name" value=""/>
		<input type='hidden' name="card_type" value=""/>
		<input type='hidden' name="card_number" value=""/>

		<input type='hidden' name="language" value="EN"/>
		<input type='hidden' name="mode" value=""/>
		<input type='hidden' name="page" value="PNR_LOOKUP"/>
		<input type='hidden' name="module" value="C3"/>
		<input type='hidden' name="sid" value=""/>
	</form>
	
	
	
	
	<form id="Eticket" method="post" action="<%=Request.Url.Scheme %>://www.checkmytrip.com">
		    <input type="hidden" name="DIRECT_RETRIEVE" value="true" />
        <input type='hidden' name='SESSION_ID' value='' />
        <input type='hidden' name='REC_LOC' value='<%=flightItinerary.PNR %>' />
        <input type='hidden' name='DIRECT_RETRIEVE_LASTNAME' value='<%=flightItinerary.Passenger[0].LastName%>'/>
  </form>
    
  
</body>
</html>
