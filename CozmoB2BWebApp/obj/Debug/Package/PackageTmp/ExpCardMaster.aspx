﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpCardMaster.aspx.cs" Inherits="CozmoB2BWebApp.ExpCardMaster" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
   
    <link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" /> 
    <link href="css/toastr.css" rel="stylesheet" type="text/css" />

    <script src="scripts/jquery-ui.js"></script>
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>
    


    <script type="text/javascript"> 

        /* Global variables */
        var apiHost = ''; var bearer = ''; var cntrllerPath = 'api/commonMasters';
        var selectedCardInfo = [];
        var apiAgentInfo = {}; var expCardDetails = {}; var agentdata = {}; var costCenterData = []; var expCrddDetails = {}
        var CM_Id = 0; var flag = ''; var agenttype = '';var selectedProfile = 0;

        /* Page Load */
        $(document).ready(function () {

            $("#ctl00_upProgress").show();

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {

                alert('Session expired, please login once again.');
                return;
            }

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            GetScreenData();

            $("#ctl00_upProgress").hide();
        });

        /* To get card master agent and cost center */
        function GetScreenData() {

            var reqData = { AgentInfo: apiAgentInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCardMasterScreenLoadData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);                        
        }

         /* To bind card master agent and cost center */
        function BindScreenData(screenData) {

            agentdata = {};

            if (!IsEmpty(screenData.dtAgents)) {

                agentdata = screenData.dtAgents;
                var options = agentdata.length != 1 ? GetddlOption('', '--Select Agent--') : '';

                $.each(agentdata, function (key, col) {

                    options += GetddlOption(col.agenT_ID , col.agenT_NAME);
                });
                
                $('#ddlAgentId').empty();
                $('#ddlAgentId').append(options); 
                $("#ddlAgentId").select2("val", '');

                if (agentdata.length == 1) {

                    $('#ddlAgentId').attr('disabled', 'disabled');
                    $("#ddlAgentId").select2("val", agentdata[0].agenT_ID);
                }
            }

            if (!IsEmpty(screenData.dtCostCenter))
                BindCostCenterData(screenData.dtCostCenter);
        }

        /* To bind cost center data to costcenter control */
        function BindCostCenterData(ccData) {

            var options = GetddlOption('', '--Select Cost Center--');
            costCenterData = ccData;

            if (!IsEmpty(costCenterData) && costCenterData.length > 0) {

                $.each(costCenterData, function (key, col) {

                    options += GetddlOption(col.setupId, col.name);
                });
            }
                
            $('#ddlCostCenter').empty();
            $('#ddlCostCenter').append(options); 
            $("#ddlCostCenter").select2("val", '');

            if (!IsEmpty(costCenterData) && costCenterData.length == 1) {

                $("#ddlCostCenter").attr("disabled", 'disabled');
                $("#ddlCostCenter").select2("val", costCenterData[0].setupId);
            }
        }

        /* To set employee name and cost center on employee change */
        function AgentChange(agentId) {

            selectedProfile = agentId;
            if (selectedProfile > 0) {

               var reqData = { AgentInfo: apiAgentInfo, AgentId : parseInt(selectedProfile) };
               var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCostCenterData';
               WebApiReq(apiUrl, 'POST', reqData, '', BindCostCenterData, null, null); 
            }
            ClearCardInfo();
        }

        /* To get card info */
        function LoadCardInfo() {
             if ($("#ddlAgentId ").val() == "" || $("#ddlAgentId ").val() == "Select") {
                 toastr.error('Please Select Agent');
                 $('#ddlAgentId').parent().addClass('form-text-error');
                 return false;
             }
            var reqData = { AgentInfo: apiAgentInfo, AgentId : parseInt($("#ddlAgentId").val()) };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCardsInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', BindCardInfo, null, null);                        
        }
               
        /* To Bind card info to grid */
        function BindCardInfo(cardsInfo) {
            
            if (cardsInfo.length == 0) {

                ShowError('No records to show.');
                ClearCardInfo();
                return;
            }

            expCardDetails = cardsInfo;

            var gridProperties = GetGridPropsEntity();
            gridProperties.headerColumns = ('Card Holder Name|Cost Center|Card Type|Card Number|Card Expiry|Card CVV').split('|');
            gridProperties.displayColumns = ('cM_Name|cM_CostCenterName|cM_Type|cM_Number|cM_Expiry|cM_CVV').split('|');
            gridProperties.pKColumnNames = ('cM_ID').split('|');
            gridProperties.dataEntity = cardsInfo;
            gridProperties.divGridId = 'divGridCardInfo';
            gridProperties.headerPaging = true;
            gridProperties.gridSelectedRows = selectedCardInfo;
            gridProperties.selectType = 'M';
            gridProperties.recordsperpage = 3;

            EnablePagingGrid(gridProperties);
            $('#divCardList').show();
        }

        /* To clear card info details and hide the div */
        function ClearCardInfo() {
             
            //expCardDetails = {};
            selectedProfile = 0;
            selectedCardInfo = [];
            $("#txtName").val('');
            $("#ddlCostCenter").select2('val', '');
            $("#txtType").val('');
            $("#txtNumber").val('');
            $("#txtExpiry").val('');
            $("#txtCVV").val('');
            CM_Id = 0;
            flag = ''
            $("#ddlAgentId").parent().removeClass('form-text-error');
            $("#txtName").parent().removeClass('form-text-error');
            $("#ddlCostCenter").parent().removeClass('form-text-error');
            $("#txtType").parent().removeClass('form-text-error');
            $("#txtNumber").parent().removeClass('form-text-error');
            $("#txtExpiry").parent().removeClass('form-text-error');
            RemoveGrid();
            $('#divCardList').hide();
        }

        /* To delete selected card info from the grid and update the status in data base */
        function DeleteCardInfo() {

            var DelCardInfo = GetSelectedCardInfo();
            if (IsEmpty(DelCardInfo) || DelCardInfo.length == 0) {

                ShowError('Please select cards to delete');
                return;
            }
                var reqData = { AgentInfo: apiAgentInfo, DelCardInfo, flag: 'Delete' };
                var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/addCardsInfo';
                WebApiReq(apiUrl, 'POST', reqData, '', RefreshGrid, null, null);                        
        }

        /* To prepare and get the card info entity list for selected cards */
        function GetSelectedCardInfo() {

            selectedCardInfo = GetSetSelectedRows();
            if (IsEmpty(selectedCardInfo) || selectedCardInfo.length == 0)                 
                return [];

            var expCardInfoSelected = [];
            $.each(selectedCardInfo, function (key, col) {
                var cinfo = expCardDetails.find(item => item.cM_ID == col);
                cinfo.cM_Status = false;
              expCardInfoSelected = expCardInfoSelected.concat(cinfo);
            });
            return expCardInfoSelected;
        }

        /* To prepare and set expense report card info entity */
        function GetCardInfoEntity(cmId, agentId, status, userId) {

            return CardInfo= {

                CM_ID: parseInt(cmId),
                CM_AgentId: parseInt(agentId),
                CM_Status: status,
                CM_CreatedBy: parseInt(userId)
            };
        }

        /* To prepare and set card info entity */
        function RefreshGrid(resp) {

            $.each(selectedCardInfo, function (key, col) {

                var cmid = parseInt(col);
                expCardDetails = expCardDetails.filter(item => (item.cM_ID) !== cmid)
            });

            if (expCardDetails.length == 0) {

                BindCardInfo(expCardDetails);
                return;
            }

            selectedCardInfo = [];
            GetSetSelectedRows('set', selectedCardInfo);
            GetSetDataEntity('set', expCardDetails);
            LoadData(GetSetCurrentPageNo('get', ''));
        }

        /* To bind update card info */
        function SaveSuccess(resp) {
             if (CM_Id > 0) {
                alert('record updated successfully!');
            }
            else if (CM_Id == 0) {
                alert('record added successfully!');
            }
            //LoadCardInfo();
            $("#ctl00_upProgress").hide();
            ClearCardInfo();
        }
        
         /* To delete selected card info from the grid and update the status in data base */
        function EditCardInfo() {

            selectedCardInfo = GetSetSelectedRows();
            if (IsEmpty(selectedCardInfo) || selectedCardInfo.length == 0) {
                
                ShowError('Please select card to edit');
                return;
            }

            if (IsEmpty(selectedCardInfo) || selectedCardInfo.length > 1) {
                
                ShowError('Please select one card to edit');
                return;
            }
            var expcardinfoDetails = expCardDetails.find(item => item.cM_ID == selectedCardInfo[0]);

            CM_Id = expcardinfoDetails.cM_ID;
            $("#ddlAgentId").select2('val', expcardinfoDetails.cM_AgentId);
            $("#txtName").val(expcardinfoDetails.cM_Name);
            $("#ddlCostCenter").select2('val', expcardinfoDetails.cM_CostCenter);
            $("#txtType").val(expcardinfoDetails.cM_Type);
            $("#txtNumber").val(expcardinfoDetails.cM_Number);
            $("#txtExpiry").val(expcardinfoDetails.cM_Expiry);
            $("#txtCVV").val(expcardinfoDetails.cM_CVV);                 
        }

        /* To validate and save card info */
        function SaveCardInfo() {
           
            if (($("#ddlAgentId").val() == "" || $("#ddlAgentId").val() == "Select") || ($("#txtName").val() == "" || $("#txtName").val() == "Enter Name") || ($("#ddlCostCenter").val() == "" || $("#ddlCostCenter").val() == "Select" || $("#ddlCostCenter").val() == null) || ($("#txtType").val() == "" || $("#txtType").val() == "Enter Type") || ($("#txtNumber").val() == "" || $("#txtNumber").val() == "Enter Number")) {
                if ($("#ddlAgentId ").val() == "" || $("#ddlAgentId ").val() == "Select") {
                    toastr.error('Please Select Agent');
                    $('#ddlAgentId').parent().addClass('form-text-error');
                }
                if ($("#txtName").val() == "" || $("#txtName").val() == "Enter Name") {
                    toastr.error('Please Enter Name');
                    $('#txtName').parent().addClass('form-text-error');
                }
                if ($("#ddlCostCenter").val() == "" || $("#ddlCostCenter").val() == "Select" || $("#ddlCostCenter").val() == null) {
                    toastr.error('Please Select Cost Center');
                    $('#ddlCostCenter').parent().addClass('form-text-error');
                }
                if ($("#txtType").val() == "" || $("#txtType").val() == "Enter Type") {
                    toastr.error('Please Enter Type');
                    $('#txtType').parent().addClass('form-text-error');
                }
                if ($("#txtNumber").val() == "" || $("#txtNumber").val() == "Enter Number") {
                    toastr.error('Please Enter Number');
                    $('#txtNumber').parent().addClass('form-text-error');
                }
                return false;
            }
            
            if ($("#txtExpiry").val() != "" && $("#txtExpiry").val().length != 5) {
                toastr.error('Please Enter Expiry in MM/YY');
                $('#txtExpiry').parent().addClass('form-text-error');
                return false;
            }
            else {
                if ($("#txtExpiry").val() != "") {
                    var valNew = $("#txtExpiry").val().split('/');
                    var mm = /^(0[1-9]|1[0-2])$/
                    var dt = new Date()
                    var year = dt.getFullYear();
                    year = year.toString().substr(-2);
                    if (!mm.test(valNew[0])) {
                        toastr.error('Please Enter month in correct format');
                        $('#txtExpiry').parent().addClass('form-text-error');
                        return false;
                    }
                    if (valNew[1] < year) {
                    toastr.error('Please Enter year in correct format');
                    $('#txtExpiry').parent().addClass('form-text-error');
                    return false;
                    }
                }
            }
                var Adddata = {
                    AgentInfo: apiAgentInfo,
                    CardInfo: {
                            CM_ID: parseInt(CM_Id),
                            CM_AgentId: parseInt($("#ddlAgentId").val()),
                            CM_Name: $("#txtName").val(),
                            CM_CostCenter: parseInt($("#ddlCostCenter").val()),
                            CM_Type: $("#txtType").val(),
                            CM_Number: $("#txtNumber").val(),
                            CM_Expiry: $("#txtExpiry").val(),
                            CM_CVV: $("#txtCVV").val(),
                            CM_Status: true,
                            CM_CreatedBy: apiAgentInfo.LoginUserId,
                            CM_ModifiedBy: apiAgentInfo.LoginUserId
                },
                flag: 'ADD'
            };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/addCardsInfo';
            WebApiReq(apiUrl, 'POST', Adddata, '', SaveSuccess, null, null);
        }


        /* To get agent and login info */
        function GetAgentInfo() {

            try {

                var loginInfo = '<%=Settings.LoginInfo == null%>';

                if (loginInfo == true)
                    return apiAgentInfo;

                agenttype = '<%=Settings.LoginInfo.AgentType%>';
                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {

            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';

            if (IsEmpty(apiHost))
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";

            return apiHost;
        }

        /* To validate expiry format MM/YY */
        function expiryFormat(evt) {
        var inputChar = String.fromCharCode(evt.keyCode);
        var code = evt.keyCode;
        var allowedKeys = [8];
            if (allowedKeys.indexOf(code) !== -1) {
                return;
            }

            evt.target.value = evt.target.value.replace(/^([1-9]\/|[2-9])$/g, '0$1/')
            .replace(/^(0[1-9]|1[0-9])$/g, '$1/')
            .replace(/^([0-1])([3-9])$/g, '0$1/$2')
            .replace(/^(0?[1-9]|1[0-2])([0-9]{2})$/g, '$1/$2')
            .replace(/^([0]+)\/|[0]+$/g, '0')
            .replace(/[^\d\/]|^[\/]*$/g, '')
            .replace(/\/\//g, '/');
        }

        /* To validate numeric values of text box and no spacebar */
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        /* To validate Alpha numeric values of text box */
        function IsAlphaNum(e) {

            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            return ((keyCode >= 46 && keyCode <= 58) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122)
                || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
        }

        /* To validate Alpha values of text box */
        function IsAlpha(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            return ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122)
                || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
        }

        /* To validate Alpha values of text box */
        var specialKeys = new Array();

        specialKeys.push(8); //Backspace
        specialKeys.push(9); //Tab
        specialKeys.push(46); //Delete
        specialKeys.push(36); //Home
        specialKeys.push(35); //End
        specialKeys.push(37); //Left
        specialKeys.push(39); //Right

    </script>

   <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">
                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left">Card Master</h3>
                    </div>
                 </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <button class="btn btn-info mr-2" id="showCardMasterList" type="button" data-toggle="collapse" data-target="#CardMasterList" aria-expanded="false" aria-controls="CardsList" onclick="LoadCardInfo()">VIEW ALL</button>
                    <button type="button" class="btn btn-gray mr-2" onclick="ClearCardInfo();">CLEAR <i class="icon icon-close "></i></button>
                    <button type="button" class="btn btn-primary" onclick="SaveCardInfo();">SUBMIT <i class="icon icon-play_arrow "></i></button>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">
                        <div class="form-group col-md-2">
                            <label>Agent <span class="fcol_red">*</span></label>
                            <select id="ddlAgentId" class="form-control" onchange="AgentChange(this.value)"></select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Name <span class="fcol_red">*</span></label>
                            <input type="text" id="txtName" name="Name" class="form-control" maxlength="50" placeholder="Enter Name" onkeypress="return IsAlphaNum(event)" />
                        </div>
                        <div class="form-group col-md-2">
                            <label>Cost Center <span class="fcol_red">*</span></label>
                            <select id="ddlCostCenter" class="form-control"></select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Type <span class="fcol_red">*</span></label>
                            <input type="text" id="txtType" name="Type" class="form-control" maxlength="20" placeholder="Enter Type" onkeypress="return IsAlphaNum(event)" />
                        </div>
                        <div class="form-group col-md-2">
                            <label>Number <span class="fcol_red">*</span></label>
                            <input type="text" id="txtNumber" name="Number" class="form-control" maxlength="20" onkeypress="return isNumberKey(event)" placeholder="Enter Number" />
                        </div>
                        <div class="form-group col-md-2">
                            <label>Expiry</label>
                            <input type="text" id="txtExpiry" name="MM" class="form-control" maxlength="5" style="width: 70px; padding-right: 2px;" placeholder="MM/YY" onkeyup="return expiryFormat(event)" />
                        </div>
                        <div class="form-group col-md-2">
                             <label>CVV</label>
                            <input type="text" id="txtCVV" name="CVV" class="form-control" maxlength="5" style="width: 60px" placeholder="CVV" onkeypress="return isNumberKey(event)" />
                        </div>
                </div>
            </div>
                
                <div id="divCardList" class="exp-content-block" style="display:none">
                   <h5 class="mb-3">All Cards</h5>
                    <div class="button-controls text-right">
                        <a class="btn btn-edit" onclick="EditCardInfo();">EDIT  <i class="icon icon-edit"></i></a>
                        <a class="btn btn-danger" onclick="DeleteCardInfo();">DELETE  <i class="icon icon-delete"></i></a>
                    </div>
                    <div id="divGridCardInfo"></div>
                </div>

           </div>
        </div>
        <div class="clear "></div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
