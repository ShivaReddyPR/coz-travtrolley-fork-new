<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ROSUploadPageUI"  ValidateRequest="false" EnableEventValidation="false" Codebehind="ROSUploadPage.aspx.cs" %>
 <%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc2" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
 <asp:HiddenField runat="server" Value="" ID="hdfDocNo"/>





<div class="bg_white bor_gray pad_10"> 

<center> 

<iframe width="700" height="70" src="RosterUploadCtrl.aspx" frameborder="0"> </iframe>




</center>


<center> 
<table>

    <tr>
                               
                <td><asp:Button runat="server" ID="btnSearch" Text="Reconcile" OnClientClick="GetiFrameValue();" CssClass="button" OnClick="btnSearch_Click" /></td>
               
                <td class=" pad_10"><asp:TextBox ID="txtROSDocNo" Width="200px" CssClass="form-control" runat="server" Text=""></asp:TextBox></td>
                
                <td><asp:Button runat="server" ID="btnClear" Text="Clear" CssClass=" button" OnClick="btnClear_Click" /></td>
                
          <%--       <td><asp:Button runat="server" ID="btnClear" Text="Clear" Width="50px"  CssClass="button" OnClick="btnClear_Click" /></td>--%>
            </tr>
            
</table>



</div>


<div style="border:solid 0px #ccc">

    
<div id="dvPaxGrid" runat="server" style="padding-bottom:5px;overflow-y:auto; border:solid 0px #ccc; margin-bottom:5px;  min-height:80px;"  visible="false">
<center>

<table>
<tr>
<td>
<asp:Label runat="server" ID="lblMissMatched" Text="MisMatched Roster Details"></asp:Label>
</td>
</tr>
<tr>
<td>
<asp:GridView ID="gvROSUploadMismatchList" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="UPLD_ID" 
    EmptyDataText="No Roster List!" AutoGenerateColumns="false" PageSize="25" 
            GridLines="Horizontal"  CssClass="grdTable"  CellPadding="4" CellSpacing="0" PagerSettings-Mode="NumericFirstLast"    
            OnPageIndexChanging="gvROSUploadMismatchList_PageIndexChanging"     >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvRow01" />    
    <Columns> 
    
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' CssClass="label" ToolTip='<%# Container.DataItemIndex+1 %>' Width="40px"></asp:Label>
    </ItemTemplate>    
    </asp:TemplateField>  

    <asp:TemplateField>
    <HeaderStyle VerticalAlign="middle" />
    <HeaderTemplate>
      <cc2:Filter ID="HTtxtDocNumber" Width="70px" HeaderText="Doc #" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblRMid" Width="130px" runat="server" Text='<%# Eval("UPLD_RM_DOC_NUMBER") %>' CssClass="label grdof"  ToolTip='<%# Eval("UPLD_RM_DOC_NUMBER") %>' ></asp:Label>                
    </ItemTemplate>  
               
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderStyle VerticalAlign="middle" />
    <HeaderTemplate>
      <cc2:Filter ID="HTtxtFileName" Width="70px" HeaderText="File Name" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblFileName" Width="130px" runat="server" Text='<%# Eval("UPLD_FILE_NAME") %>' CssClass="label grdof"  ToolTip='<%# Eval("UPLD_FILE_NAME") %>' ></asp:Label>                
    </ItemTemplate>  
               
    </asp:TemplateField>
    
    <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc2:Filter ID="HTtxtStaffId" Width="70px" HeaderText="Staff Id" CssClass="inputEnabled" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblStaffId" runat="server" Text='<%# Eval("[UPLD_STAFF_ID]") %>' CssClass="label grdof" ToolTip='<%# Eval("[UPLD_STAFF_ID]") %>' Width="100px"></asp:Label>
   
    
    </ItemTemplate>    
    
    </asp:TemplateField>  
      
     <asp:TemplateField>
    <HeaderStyle />
    <HeaderTemplate>
    <cc2:Filter ID="HTtxtFlightNo" HeaderText="Flight No" CssClass="inputEnabled" Width="120px" OnClick="Filter_Click" runat="server"  />                 
    </HeaderTemplate>
    <ItemStyle  />
    <ItemTemplate>
    <asp:Label ID="ITlblFlightNo" runat="server" Text='<%# Eval("UPLD_FLIGHT_NO") %>' CssClass="label grdof"  ToolTip='<%# Eval("UPLD_FLIGHT_NO") %>' Width="120px"></asp:Label>
    </ItemTemplate> 
        
    </asp:TemplateField>
       
     <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc2:Filter ID="HTRMDate" Width="80px" CssClass="inputEnabled" HeaderText="Date" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblRMDate" runat="server" Text='<%# CZDateFormat(Eval("UPLD_RM_DATE")) %>' CssClass="label grdof" ToolTip='<%# (Eval("UPLD_RM_DATE")) %>' Width="100px"></asp:Label>
    </ItemTemplate>    
          
    </asp:TemplateField>  
    
     
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc2:Filter ID="HTtxtFromSector"  Width="100px" CssClass="inputEnabled" HeaderText="From Sector" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblFromSector" runat="server" Text='<%# Eval("UPLD_FROM_SECTOR") %>' CssClass="label grdof"  ToolTip='<%# Eval("UPLD_FROM_SECTOR") %>' Width="100px"></asp:Label>                
    </ItemTemplate>  
         
    </asp:TemplateField>
     <asp:TemplateField>
    <HeaderTemplate >
    <cc2:Filter ID="HTtxtToSector"  Width="100px" CssClass="inputEnabled" HeaderText="To Sector" OnClick="Filter_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblToSector" runat="server" Text='<%# Eval("UPLD_TO_SECTOR") %>' CssClass="label grdof"  ToolTip='<%# Eval("UPLD_TO_SECTOR") %>' Width="125px"></asp:Label>                
    </ItemTemplate>  
     
    </asp:TemplateField>
</Columns>
</asp:GridView>
</td>
</tr>

<tr>
        <td width="80px"><asp:Button OnClick="btnExport_Click"  runat="server" id="btnExport" Text="Export To Excel" CssClass="button" /> </td>
 </tr>
 </table>



</center> 
</div> 
<table border ="0">
<tr>
<td colspan="3">



</td>
</tr>
<tr>
<td colspan="3">
<%--<div id="Div3" runat="server" style="padding-bottom:5px;overflow-y:auto;  border:solid 1px #ccc; margin-bottom:5px; min-height:80px;height:100px;width:100%" >--%>

</td>
</tr>
<tr>
<td>




</td>
</tr>
<tr>

</tr>

</table>
    </div>
    <div>
    <asp:DataGrid ID="dgROSMisMatchList" runat="server" AutoGenerateColumns="false" >
    <Columns>
    <%--<asp:BoundColumn HeaderText="S.No." DataField="receipt_number" HeaderStyle-Font-Bold="true" ></asp:BoundColumn>--%>

    <asp:BoundColumn HeaderText="Doc Number" DataField="UPLD_RM_DOC_NUMBER" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="File Name" DataField="UPLD_FILE_NAME" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="STAFF_ID" DataField="UPLD_STAFF_ID" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="FLIGHTNO" DataField="UPLD_FLIGHT_NO" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="DATE" DataField="UPLD_RM_DATE" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="FROM_SECTOR" DataField="UPLD_FROM_SECTOR" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="TO_SECTOR" DataField="UPLD_TO_SECTOR" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="DUTY_HOURS" DataField="UPLD_DUTY_HRS" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    
    <asp:BoundColumn HeaderText="ETD" DataField="UPLD_ETD" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="ETA" DataField="UPLD_ETA" HeaderStyle-Font-Bold="true"></asp:BoundColumn>   
   
    </Columns>
    </asp:DataGrid>
    
    
    
    </div>
 <script type="text/javascript">



//function Save()
//    { 
//         clearMessage();
//    var fromDate=GetDateObject('ctl00_cphTransaction_dcFromDate');
//    //var fromTime=getElement('dcFromDate_Time');
//    var toDate=GetDateObject('ctl00_cphTransaction_dcToDate');
//    //var toTime=getElement('dcToDate_Time');
//    if(fromDate==null) addMessage('Please select From Date !','');
//    //alert(fromTime);
//    //if(fromTime.value=='') addMessage('Please select From Time!','');
//    if(toDate==null) addMessage('Please select To Date !','');
//   // if(toTime.value=='') addMessage('Please select To Time!','');
//    if((fromDate!=null && toDate!=null) && fromDate>toDate) addMessage('From Date should not be later than To Date!','');
//    if(getMessage()!=''){ 
//    alert(getMessage()); clearMessage(); return false; }
     // }

     function GetiFrameValue() {

         var iframe = getElement('iFrameExport');         
         var innerDoc = iframe.contentWindow.document;      //iframe.contentDocument; //|| iframe.contentWindow.document;         
         var iFrametxtBox = innerDoc.getElementById('txtDocNumber');

         //alert(iFrametxtBox.value);
         getElement("hdfDocNo").value = iFrametxtBox.value;
         if(getElement("hdfDocNo").value!='')
            getElement("txtROSDocNo").value = iFrametxtBox.value;
     }
   


</script>
</asp:Content>

