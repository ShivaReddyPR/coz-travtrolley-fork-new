﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="ViewBookingForActivity" Title="Untitled Page" Codebehind="ViewBookingForActivity.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">




<div class="body_container"> 

<div id="Div1"  runat="server"  class="one">
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
      <tr>
      <%if (ActHdr != null && ActHdr.Rows.Count > 0)
      { %>
        
        <%if (ActHdr.Rows[0]["isFixedDeparture"].ToString() == "N")
          { %>
        
        <td height="30"> <h4>Activity Header</h4></td>
       
       
       
        <td>  <asp:LinkButton ID="lnkActShowVoucher" CssClass="pull-right" runat="server" Text="Activity Voucher"  OnClick="lnkActShowVoucher_Click"></asp:LinkButton> 
        
         <a  Class="pull-right marright_10" href="ActivityQueue.aspx" > Activity Queue</a></td>
      <%}
          else
          { %>
          <td height="30"> <h3>Fixed Departure Header</h3></td>
        <td><asp:LinkButton ID="lnkFixShowVoucher" runat="server" Text="Fixed Departure Voucher"  OnClick="lnkFixShowVoucher_Click"></asp:LinkButton></td>
        <td><a href="B2CFixDepartureQueue.aspx" class="nounderline" >Fixed Departure Queue</a></td>
            <%} %>
      <%} %>
      </tr>
    </table>  
    
<div class="bg_white bor_gray pad_10">
<asp:HiddenField ID="hdnBookingId" runat="server" />
    
      

    
    <table cellpadding="0" cellspacing="0" width="100%">
    
    <%if (ActHdr != null && ActHdr.Rows.Count > 0)
      { %>
    <tr height="30px">
    <td  width="100px"><label>Activity Name: </label></td>
    <td><b><%= ActHdr.Rows[0]["activityName"]%> </b> </td>
    <td width="50px"><label>TripId: </label></td>
    <td><b><%= ActHdr.Rows[0]["TripId"]%> </b> </td>
    <td width="130px"><label>Transaction Date: </label></td>
    <td><b><%= ActHdr.Rows[0]["TransactionDate"]%> </b> </td>
    </tr>
    
    <tr height="30px">
    <td  width="100px"><label>Booking Date: </label></td>
    <td><b><%= ActHdr.Rows[0]["Booking"]%> </b> </td>
    <td width="90px"><label>Agency Name: </label></td>
    <td><b><%= ActHdr.Rows[0]["agent_name"]%> </b> </td>
    <td width="130px"><label>Total Price: </label></td>
    <td><b><%=agency.AgentCurrency%> &nbsp<%= Convert.ToDecimal(ActHdr.Rows[0]["TotalPrice"]).ToString("N"+agency.DecimalValue)%> </b> </td>
    </tr>
    
    <%--<tr height="30px">
    <td  width="100px"><label>Order Id: </label></td>
    <td><b><%= ActHdr.Rows[0]["OrderId"]%> </b> </td>
    
    <td width="130px"><label>TransactionT ype: </label></td>
    <td><b><%= ActHdr.Rows[0]["TransactionType"]%> </b> </td>
    </tr>--%>
    
    <tr height="30px">
    <td  width="100px"><label>Pax Count: </label></td>
    <td><b><%= ActHdr.Rows[0]["Adult"]%> </b> </td>
    <td width="85px" style="display:none;"><label>Childs: </label></td>
    <td style="display:none;"><b><%= ActHdr.Rows[0]["Child"]%> </b> </td>
    <td width="130px" style="display:none;"><label>Infants: </label></td>
    <td style="display:none;"><b><%= ActHdr.Rows[0]["Infant"]%> </b> </td>
    </tr>
    
    <%} %>
    </table>
    </div>
    
    <br />
<%--    <div style="width: 978px; height:320px; overflow:scroll; color: #000000; background-color: #FFFFFF; border: solid 1px #ccc;">--%>
    
        <div>
    <asp:DataList ID="dlPaxDetails" runat="server" Width="100%">
                                <HeaderTemplate>
                                    <table class="" width="100%" border="0" cellspacing="0" cellpadding="5">
                                        <tr height="30px">
                                            <td width="05%" class="heading">
                                                S#
                                            </td>
                                            <td width="20%" class="heading">
                                                Full Name
                                            </td>
                                            <td width="25%" class="heading">
                                                Email
                                            </td>
                                            <td width="20%" class="heading">
                                                Phone No
                                            </td>
                                            <td width="20%" class="heading">
                                                Nationality
                                            </td>
                                        </tr>
                                    </table>
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <table class="tblpax" width="100%" border="0" cellspacing="0" cellpadding="5">
                                        <tr height="30px">
                                            <td width="5%">
                                                <%#Eval("PaxSerial")%>
                                            </td>
                                            <td width="20%">
                                                <%#Eval("FirstName")%>&nbsp;<%# Eval("LastName")%>
                                            </td>
                                            <td width="25%">
                                                <%#Eval("Email")%>
                                            </td>
                                            <td width="20%">
                                                <%#Eval("PhoneCountryCode")%>&nbsp;<%# Eval("Phone")%>
                                            </td>
                                            <td width="20%">
                                                <%#Eval("Nationality")%>
                                            </td>
                                        </tr>
                                    </table>
                                </ItemTemplate>
                            </asp:DataList>
    </div>
    
          </div>
 
 
 </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">

</asp:Content>

