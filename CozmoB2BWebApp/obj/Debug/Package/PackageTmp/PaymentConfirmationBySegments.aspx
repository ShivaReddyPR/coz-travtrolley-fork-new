﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="PaymentConfirmationBySegments" Title="Payment Confirmation" Codebehind="PaymentConfirmationBySegments.aspx.cs" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="System.Linq" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<script src="Scripts/jsBE/SearchResult.js" type="text/javascript"></script>
    <script type="text/javascript">
        function SeatAssignAlert(msg) {

            document.getElementById('divSeatsalertText').innerHTML = msg.replace(/@/g, '</br>');
            $('#Seatsalert').modal('show');
        }
        function Cancel(id) {
           $('#'+id).modal('hide');
        }
        function validate() {
            if (document.getElementById('<%=chkRules.ClientID %>').checked == true) {
                $('.modal').modal('hide');
                document.getElementById('rules').style.display = 'none';
                <%if (!Settings.LoginInfo.AgentBlock) //checking Agent able to book or not Added By brahmam
    {%>
                document.getElementById('<%=hdfAction.ClientID %>').value = "Ticket";
                document.getElementById('PreLoader').style.display = "block";
                document.getElementById('MainDiv').style.display = "none";
                document.getElementById('divPrg').style.display = "none";
                return true;
                <%}
    else
    {%>
                alert('Please contact Administrator'); //We are Showing Alert Message
                return false;
                <%}%>
            }
            else {
                document.getElementById('rules').style.display = 'block';
                document.getElementById('PreLoader').style.display = "none";   
                return false;
            }
        }
        function setAction() {
            if (validate()) {
                document.getElementById('<%=hdfAction.ClientID %>').value = "Hold";
                if (document.getElementById('<%=ddlPaymentType.ClientID %>').selectedIndex == 1) {//Show preloader for CreditCard also
                    document.getElementById('PreLoader').style.display = "block";
                    document.getElementById('MainDiv').style.display = "none";
                    document.getElementById('divPrg').style.display = "none";
                }
                return true;
            }
            else {
                return false;
            }
        }
        function PriceContinue() {
            //document.getElementById('FareDiff').style.display = 'none';
            $('#FareDiff').modal('hide')
            document.getElementById('<%=imgBtnPayment.ClientID %>').style.display = 'block';

        }

        function Deselect() { //Added by Suresh
            var radioButtons = document.getElementById("<%=rblAgentPG.ClientID%>");
                var inputs = radioButtons.getElementsByTagName("input");
            var bookingAmount = eval(document.getElementById("ctl00_cphTransaction_hdnBookingAmount").defaultValue.replace(',',''));
                var currentCode = document.getElementById("<%=lblTxnAmount.ClientID%>").innerHTML.split(' ')[0];
            for (var x = 0; x < inputs.length; x++) {
                if (inputs[x].checked) {
                    inputs[x].checked = false;
                    document.getElementById("ctl00_cphTransaction_lblTxnAmount").innerHTML
                        = currentCode + " " + parseFloat(bookingAmount).toLocaleString('en-AE', {minimumFractionDigits: eval('<%=agency.DecimalValue %>'), maximumFractionDigits: eval('<%=agency.DecimalValue %>')});
                }
            }
        } //===============================================

        
        function CardChargeEnable() {

            //Deselect(); //Added by Suresh
             <%--var radioButtons = document.getElementById("<%=rblAgentPG.ClientID%>");
            var inputs = radioButtons.getElementsByTagName("input");
             var selected;
            for (var x = 0; x < inputs.length; x++) {
                if (inputs[x].checked) {

                }
            }--%>
 
            FillCharges();//==================================
            if (document.getElementById('ctl00_cphTransaction_ddlPaymentType').value == "Card") {
                document.getElementById('divCardCharge').style.display = "block";
                document.getElementById('lblCardCharges').innerHTML = parseFloat(eval('<%=charges %>')).toLocaleString('en-AE', {minimumFractionDigits: eval('<%=agency.DecimalValue %>'), maximumFractionDigits: eval('<%=agency.DecimalValue %>')});

                //Added by somasekhar on 02/07/2018                
                document.getElementById('divAgentPG').style.display = "block";
            }
            else {
                document.getElementById('divCardCharge').style.display = "none";
                //document.getElementById('lblCardCharges').innerHTML = "";

                //Added by somasekhar on 02/07/2018                
                document.getElementById('divAgentPG').style.display = "none";
                var bookingAmount = eval(document.getElementById("ctl00_cphTransaction_hdnBookingAmount").defaultValue.replace(',',''));
                var currentCode = document.getElementById("<%=lblTxnAmount.ClientID%>").innerHTML.split(' ')[0];
                
                    document.getElementById("ctl00_cphTransaction_lblTxnAmount").innerHTML
                        = currentCode + " " + parseFloat(bookingAmount).toLocaleString('en-AE', {minimumFractionDigits: eval('<%=agency.DecimalValue %>'), maximumFractionDigits: eval('<%=agency.DecimalValue %>')});
            }
        }
        function BagPriceContinue() {
           // document.getElementById('BagFareDiff').style.display = 'none';
            $('#BagFareDiff').modal('hide');
        }

        function PriceCancel() {
           // document.getElementById('BagFareDiff').style.display = 'none';
            $('#BagFareDiff').modal('hide');
            window.location.href = 'HotelSearch.aspx?source=Flight';
        }
        function FillCharges() {  // Added by Suresh
 
            var bookingAmount = document.getElementById("<%=hdnBookingAmount.ClientID%>").defaultValue;
            var radioButtons = document.getElementById("<%=rblAgentPG.ClientID%>");
            var inputs = radioButtons.getElementsByTagName("input");

            var selected;
            for (var x = 0; x < inputs.length; x++) {
                var totalamount = bookingAmount.replace(/\,/g, '');
                if (inputs[x].checked) {
                    var charge = inputs[x].value;
                    //document.getElementById('divCardCharge').style.display = "block";
                    document.getElementById('lblCardCharges').innerHTML = parseFloat(charge).toLocaleString('en-AE', {minimumFractionDigits: eval('<%=agency.DecimalValue %>'), maximumFractionDigits: eval('<%=agency.DecimalValue %>')});
                    var currentCode = document.getElementById("<%=lblTxnAmount.ClientID%>").innerHTML.split(' ')[0];
                    if (charge != 0) {
                        var amount = parseFloat(totalamount) + (parseFloat(totalamount) * (parseFloat(charge) / 100));
                        document.getElementById("<%=lblTxnAmount.ClientID%>").innerHTML
                            = currentCode + " " + amount.toLocaleString('en-AE', {minimumFractionDigits: eval('<%=agency.DecimalValue %>'), maximumFractionDigits: eval('<%=agency.DecimalValue %>')});

                    }
                    else {
                        document.getElementById("ctl00_cphTransaction_lblTxnAmount").innerHTML
                            = currentCode + " " + parseFloat(totalamount).toLocaleString('en-AE', {minimumFractionDigits: eval('<%=agency.DecimalValue %>'), maximumFractionDigits: eval('<%=agency.DecimalValue %>')});
                    }


                }
            }
            if (selected) {
                alert(selected.value);
            }
        }//==============================
    </script>

<asp:HiddenField ID="hdnAgencyName" runat="server" />
<asp:HiddenField ID="hdnAgencyPhone" runat="server" />

    

    <div class="modal fade pymt-modal" data-backdrop="static" id="Seatsalert" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content" style="width:500px;">
              <div class="modal-header">
                  <h4 class="modal-title" id="SeatsalertLabel">Seat assignment alert</h4>
              </div>
              <div class="modal-body">
                <div id="divSeatsalertText" style="color:Red; font-size:14px; font-weight:bold; padding:10px;"></div>                  
                <div class="padtop_4">
                    <input type="button" id="SeatsalertContinue" class="btn but_b" onclick="Cancel('Seatsalert')" />
                </div>
              </div>
            </div>
          </div>
        </div>


        <div class="modal fade pymt-modal" data-backdrop="static" id="FareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
          <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
              <div class="modal-header">
                  <button type="button" class="close mt-0" style="color:#fff;opacity:.8;" data-dismiss="modal" aria-label="Close"" id="btnok" onclick="PriceContinue()"><span aria-hidden="true">&times;</span></button>
                  <h4 class="modal-title" id="myModalLabel">Booking Alert</h4>
              </div>
              <div class="modal-body">
                  
 <div id="divFareDiffText" style="color:Red; font-size:14px; font-weight:bold; padding:10px;">Your booking Airline is Low Cost Carrier, for any cancellations, respective Airline cancellation charges will be applied</div>

<%--<div style=" padding-bottom:10px;"> <center> <input type="button value="OK" class="button-normal"/></center> </div>--%>
                     </div>
            </div>
          </div>
        </div>



    <asp:HiddenField ID="hdfStop" runat="server" Value="0" />
    <asp:HiddenField ID="hdfAction" runat="server" />
    <asp:HiddenField ID="hdnBookingAmount" runat="server" />


    <div id="MainDiv">
    <div class="ns-h3">Payment Confirmation</div>
        <div>
            <asp:MultiView ID="MultiView1" runat="server" ActiveViewIndex="0">
                <asp:View ID="PaymentView" runat="server">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <asp:DataList ID="dlFlight" runat="server" OnItemDataBound="dlFlight_ItemDataBound"
                                    Width="100%" Caption="Onward Flight">
                                    <ItemTemplate>
                                        <div class="bg_white pad_10"> 
                 
                                <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="imgFlightCarrierCode" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFlightOriginCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFlightDestinationCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFlightDuration" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblOnFlightWayType" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            
                                                                                                                  
                                                        </tr>
                                                           <tr>
                                                            <td>
                                                                <asp:Label ID="lblFlightCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFlightDepartureDate" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblFlightArrivalDate" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblOnDefaultBaggage" runat="server" CssClass="d-block" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td height="14px" colspan="4">
                                                                <hr class="b_bot_1" />
                                                            </td>
                                                        </tr>
                                                      
                                                    </table>
                               </div>
                                    </ItemTemplate>
                                </asp:DataList>    

                                <asp:DataList ID="dlFlightReturn" runat="server" OnItemDataBound="dlFlightReturn_ItemDataBound"
                                    Width="100%" Caption="Return Flight">
                                    <ItemTemplate>
                                        <div class="bg_white pad_10"> 
                 
                                <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tr>
                                                            <td>
                                                                <asp:Image ID="imgFlightCarrierCode" runat="server" />
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFlightOriginCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFlightDestinationCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFlightDuration" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                                <br />
                                                                <asp:Label ID="lblOnFlightWayType" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            
                                                                                                                  
                                                        </tr>
                                                           <tr>
                                                            <td>
                                                                <asp:Label ID="lblFlightCode" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                            <td>
                                                                <asp:Label ID="lblFlightDepartureDate" runat="server" Text=""></asp:Label>
                                                            </td>
                                                            <td >
                                                                <asp:Label ID="lblFlightArrivalDate" runat="server" Text=""></asp:Label>
                                                            </td>
                                                             <td>
                                                                <asp:Label ID="lblRetDefaultBaggage" runat="server" CssClass="d-block" Text="" Font-Bold="true"></asp:Label>
                                                            </td>
                                                        </tr>
                                                        
                                                        <tr>
                                                            <td height="14px" colspan="4">
                                                                <hr class="b_bot_1" />
                                                            </td>
                                                        </tr>
                                                      
                                                    </table>
                               </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:DataList ID="dlPaxPrice" runat="server" Width="100%" OnItemDataBound="dlPaxPrice_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="bg_white">
                                            <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="55%" class="gray-smlheading">
                                                        <strong>Onward Fare</strong>
                                                    </td>
                                                    <td class="gray-smlheading" width="35%" align="right"><strong>Price</strong></td>
                                                    <td width="10%" class="gray-smlheading" align="right"></td>
                                                </tr>
                                                <tr>
                                                    <td>Total AirFare
                                                                    <%--<asp:Label ID="lblAdultCount" runat="server" Text=""></asp:Label>--%>
                                                    </td>

                                                    <td align="right">
                                                        <asp:Label ID="lblBaseFare" runat="server" Text=""></asp:Label>
                                                        <%--Text='<%#Eval("BaseFare","{0:0.000}") %>'--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Tax
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblTax" runat="server" Text=""></asp:Label>
                                                        <%--'<%#Eval("Tax","{0:0.000}") %>'--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>K3 Tax
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblK3Tax" runat="server" Text=""></asp:Label>
                                                        <%--'<%#Eval("Tax","{0:0.000}") %>'--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblDiscount" runat="server" Text="Discount" Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblDiscountAmount" runat="server" Text="" Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Total
                                                                    <%--<asp:Label ID="lblTotalAdults" runat="server" Text=""></asp:Label>--%>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblBaggage" runat="server" Text="Baggage " Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblBaggagePrice" runat="server" Text='' Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>

                                                <%if (onwardResult.ResultBookingSource == BookingSource.Indigo || onwardResult.ResultBookingSource == BookingSource.SpiceJet || onwardResult.ResultBookingSource == BookingSource.SpiceJetCorp || onwardResult.ResultBookingSource == BookingSource.IndigoCorp || onwardResult.ResultBookingSource == BookingSource.GoAir || onwardResult.ResultBookingSource == BookingSource.GoAirCorp || onwardResult.ResultBookingSource == BookingSource.AirArabia) %>
                                                <%{ %>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMeal" runat="server" Text="Meal " Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblMealPrice" runat="server" Text='' Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <%} %>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblSeat" runat="server" Text="Seats" Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblSeatPrice" runat="server" Text='' Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%if ((CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                                                            { %>
                                                                    Total GST
                                                                    <%}
                                                                        else if (!CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent && CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.LocationCountryCode == "IN")
                                                                        {%>
                                                                                Total GST
                                                                        <%}
                                                                            else
                                                                            { %>
                                                                    VAT 
                                                                    <%} %>
                                                    </td>
                                                    <td align="right">

                                                        <asp:Label ID="lblVATAmount" runat="server"></asp:Label>


                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong class="spnred">Grand Total Offer Fare</strong>
                                                    </td>
                                                    <td align="right">
                                                        <strong>
                                                            <asp:Label ID="lblMarkupTotal" runat="server" Text=''></asp:Label></strong>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr class="d-none">
                                                    <td>
                                                        <strong class="spnred">Grand Total Pub Fare</strong>
                                                    </td>
                                                    <td align="right">
                                                        <strong>
                                                            <asp:Label ID="lblTotalPubFare" runat="server" Text=''></asp:Label></strong>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>

                                <asp:DataList ID="dlPaxPriceReturn" runat="server" Width="100%" OnItemDataBound="dlPaxPriceReturn_ItemDataBound">
                                    <ItemTemplate>
                                        <div class="bg_white">
                                            <table class="table901" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="55%" class="gray-smlheading">
                                                        <strong>Return Fare</strong>
                                                    </td>
                                                    <td class="gray-smlheading" width="35%" align="right"><strong>Price</strong></td>
                                                    <td width="10%" class="gray-smlheading" align="right"></td>
                                                </tr>
                                                <tr>
                                                    <td>Total AirFare
                                                                    <%--<asp:Label ID="lblAdultCount" runat="server" Text=""></asp:Label>--%>
                                                    </td>

                                                    <td align="right">
                                                        <asp:Label ID="lblBaseFare" runat="server" Text=""></asp:Label>
                                                        <%--Text='<%#Eval("BaseFare","{0:0.000}") %>'--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Tax
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblTax" runat="server" Text=""></asp:Label>
                                                        <%--'<%#Eval("Tax","{0:0.000}") %>'--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>K3 Tax
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblK3Tax" runat="server" Text=""></asp:Label>
                                                        <%--'<%#Eval("Tax","{0:0.000}") %>'--%>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblDiscount" runat="server" Text="Discount" Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblDiscountAmount" runat="server" Text="" Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>Total
                                                                    <%--<asp:Label ID="lblTotalAdults" runat="server" Text=""></asp:Label>--%>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblTotal" runat="server"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblBaggage" runat="server" Text="Baggage " Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblBaggagePrice" runat="server" Text='' Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>

                                                <%if (returnResult.ResultBookingSource == BookingSource.Indigo || returnResult.ResultBookingSource == BookingSource.SpiceJet || returnResult.ResultBookingSource == BookingSource.SpiceJetCorp || returnResult.ResultBookingSource == BookingSource.IndigoCorp || returnResult.ResultBookingSource == BookingSource.GoAir || returnResult.ResultBookingSource == BookingSource.GoAirCorp || returnResult.ResultBookingSource == BookingSource.AirArabia) %>
                                                <%{ %>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblMeal" runat="server" Text="Meal " Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblMealPrice" runat="server" Text='' Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <%} %>
                                                <tr>
                                                    <td>
                                                        <asp:Label ID="lblSeat" runat="server" Text="Seats" Visible="false"></asp:Label>
                                                    </td>
                                                    <td align="right">
                                                        <asp:Label ID="lblSeatPrice" runat="server" Text='' Visible="false"></asp:Label>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <%if ((CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN"))
                                                            { %>
                                                                    Total GST
                                                                    <%}
                                                                        else if (!CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent && CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.LocationCountryCode == "IN")
                                                                        {%>
                                                                                Total GST
                                                                        <%}
                                                                            else
                                                                            { %>
                                                                    VAT 
                                                                    <%} %>
                                                    </td>
                                                    <td align="right">

                                                        <asp:Label ID="lblVATAmount" runat="server"></asp:Label>


                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <strong class="spnred">Grand Total Offer Fare</strong>
                                                    </td>
                                                    <td align="right">
                                                        <strong>
                                                            <asp:Label ID="lblMarkupTotal" runat="server" Text=''></asp:Label></strong>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                                <tr  class="d-none">
                                                    <td>
                                                        <strong class="spnred">Grand Total Pub Fare</strong>
                                                    </td>
                                                    <td align="right">
                                                        <strong>
                                                            <asp:Label ID="lblTotalPubFare" runat="server" Text=''></asp:Label></strong>
                                                    </td>
                                                    <td></td>
                                                </tr>
                                            </table>
                                        </div>
                                    </ItemTemplate>
                                </asp:DataList>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <div class="col-md-6 pad_left0">
                                                <div class="ns-h3">
                                                    <strong>Passenger Details</strong>
                                                    <%if(onwardResult.IsLCC || (returnResult!= null ?returnResult.IsLCC:false))
                                                              %>
                                                    <%{ %>
                                                    <a style="color: #fff; padding-right: 10px; font-weight: normal;display:none;" class="pull-right" href="javascript:history.go(-1);">Edit Pax</a>
                                                    <%} %>
                                                </div>
                                                <div class="bg_white bor_gray pad_10 payment_confirm_pax">
                                                    <asp:DataList ID="dlPassengers" runat="server" Width="100%">
                                                        <ItemTemplate>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tr>
                                                                    <td height="20">
                                                                        <strong>
                                                                            <asp:Label ID="lblAdultName" runat="server" Text='<%#Eval("FirstName") %>'></asp:Label></strong>
                                                                        (<%#Eval("Type") %> )<br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20">Passport #:
                                                                                    <asp:Label ID="lblPassportNumber" runat="server" Text='<%#Eval("PassportNo") %>'></asp:Label><br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20">Phone:
                                                                                    <asp:Label ID="lblPhone" runat="server" Text='<%#Eval("CellPhone") %>'></asp:Label><br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20">Address :
                                                                                    <asp:Label ID="lblAddress" runat="server" Text='<%#Eval("AddressLine1") %>'></asp:Label><br />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20">Email:
                                                                                    <asp:Label ID="lblEmail" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td height="20">Seat Preference:
                                                                                    <asp:Label ID="lblSelectedSeats" runat="server" Text='<%#Eval("SeatInfo") %>'></asp:Label>
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="6">
                                                                        <hr class="b_bot_1 my-1"></hr>
                                                                    </td>
                                                                </tr>
                                                            </table>
                                                        </ItemTemplate>
                                                    </asp:DataList>
                                                </div>
                                            </div>
                                            <div class="col-md-6 pad_right0">
                                                <div class="ns-h3"><strong>Payment Details</strong> </div>
                                                <div class="modal fade in farerule-modal-style" data-backdrop="static" id="FareRuleBlock" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <div class="modal-header">
                                                                <button type="button" class="close mt-0" style="color: #fff; opacity: .8;" onclick="FareRuleHide()"><span aria-hidden="true">×</span></button>
                                                                <h4 class="modal-title" id="FareRuleHead"><span id="FareRuleHeadTitle"></span></h4>
                                                            </div>
                                                            <div class="modal-body">
                                                                <div class="bg_white" id="FareRuleBody" style="padding: 10px; width: 100%; font-size: 13px; height: 92%; overflow: auto;"></div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="bg_white bor_gray pad_10">
                                                    <div>
                                                        <table width="100%" border="0" cellpadding="0" cellspacing="0">

                                                            <tr>
                                                                <td>
                                                                    <div style="padding: 0px 10px 0px 10px">

                                                                        <ul style="margin-left: 10px;">
                                                                            <li>Ticket changes may incur penalties and/or increased fares.</li>
                                                                            <li>Tickets are nontransferable and name changes are not allowed.</li>
                                                                            <!--<li>Read an overview of all the<a href="#"> rules &amp; restrictions</a> applicable to 
                                                                        this fare.</li>-->
                                                                            <%if (onwardResult != null && Session["sessionId"] != null)
                                                                            { %>
                                                                            <li>Read the complete<a href="Javascript:FareRule(<%=onwardResult.ResultId %>,'<%=Session["sessionId"].ToString() %>')"> penalty rules for changes and cancellations</a>
                                                                                applicable to this fare.</li>
                                                                            <%} %>
                                                                        </ul>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="10px">
                                                                    <hr class="b_bot_1 my-1">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table style="margin-left: 10px;" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td width="4%">
                                                                                <span style="font-size: 11px">
                                                                                    <asp:CheckBox ID="chkRules" runat="server" />
                                                                                </span>
                                                                            </td>
                                                                            <td width="96%">
                                                                                <strong>I have read and accept the rules &amp; restrictions.</strong>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2" style="width: 100%">
                                                                                <div id="rules" style="color: Red; display: none; width: 100%">
                                                                                    Please check rules and restrictions to continue.
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td>
                                                                                <label id="lblHold" runat="server" style="padding-left: 10px;" visible="false">
                                                                                    Want to hold your ticket till you pay?</label>
                                                                            </td>
                                                                            <td align="center">
                                                                                <label>
                                                                                    <asp:Button CssClass="btn but but_b" ID="btnHold" runat="server" Text="Hold" OnClientClick="return setAction();" OnClick="btnHold_Click" Visible="false" />
                                                                                    <%--TBO Hold - Modal Button (Hidden)--%>
                                                                                    <a href="javascript:void(0);" type="button" class="btn but_b" style="display: none" data-target="#TBOConfirm" id="TBOHoldConfirmBtn">Hold</a>

                                                                                </label>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="10px">
                                                                    <hr class="b_bot_1  my-1">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <label style="padding-left: 10px;">
                                                                        Select Payment Mode :
                                                                <select style="width: 140px;" class="" id="ddlPaymentType" runat="server" onchange="CardChargeEnable()">
                                                                    <option value="Credit">On Account</option>
                                                                    <option value="Card">Credit Card</option>
                                                                </select>
                                                                    </label>
                                                                    <%-- Added by Somasekhar on 02/07/2018 -- Agent PG Options--%>
                                                                    <div id="divAgentPG" style="display: none; margin-bottom: 10px">
                                                                        Payment Gateways
                                                                <asp:RadioButtonList ID="rblAgentPG" runat="server" onchange=" return FillCharges()">
                                                                    <%--  OnSelectedIndexChanged="rblAgentPG_SelectedIndexChanged" AutoPostBack="true" onchange=" return CardChargeEnable()">--%>
                                                                </asp:RadioButtonList>
                                                                        <div id="pgValidation" style="color: red; display: none; width: 100%;">
                                                                            Please select one of the payment gateways.
                                                                        </div>
                                                                    </div>
                                                                    <%--   ==========================--%>

                                                                    <div style="display: none; color: Red; font-weight: bold;" id="divCardCharge">
                                                                        Credit Card Charges
                                                                <label id="lblCardCharges" style="color: Red; font-weight: bold;">
                                                                </label>
                                                                        % applicable on Total Amount
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="10px">
                                                                    <hr class="b_bot_1  my-1">
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <div style="padding: 5px 0px 10px 0px">
                                                                        <table id="tblAgentBal" width="100%">
                                                                            <tbody>
                                                                                <tr class="nA-h4">
                                                                                    <td align="right">Available Balance :
                                                                                    </td>
                                                                                    <td align="right" style="font-weight: bold; padding-right: 10px">
                                                                                        <asp:Label ID="lblAgentBalance" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>


                                                                                <tr class="nA-h4">
                                                                                    <td align="right">Amount to be Booked :
                                                                                    </td>
                                                                                    <td align="right" style="font-weight: bold; padding-right: 10px">
                                                                                        <asp:Label ID="lblTxnAmount" runat="server"></asp:Label>
                                                                                    </td>
                                                                                </tr>

                                                                                <tr>
                                                                                    <td align="left" colspan="2">
                                                                                        <asp:Label Style="color: Red" ID="lblBalanceError" Font-Bold="true" runat="server"
                                                                                            Text=""></asp:Label>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>

                                                                        </table>
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td>
                                                                    <asp:Label ID="lblMessage" runat="server" ForeColor="Red" Text=""></asp:Label>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td align="right">
                                                                    <label style="padding-right: 10px">
                                                                        <%--<asp:ImageButton ID="imgBtnPayment" runat="server" ImageUrl="images/confirm-button.jpg"
                                                                    OnClientClick="return validate();" OnClick="imgBtnPayment_Click" />--%>
                                                                        <%if (agency != null && agency.AgentTicketingAllowed)
                                                                        {  %>
                                                                        <%--TBO Confirm - Modal Button (Hidden)--%>
                                                                        <a href="javascript:void(0);" type="button" class="btn but_b" style="display: none" data-target="#TBOConfirm" id="TBOConfirmBtn">Book Now 
                                                                        </a>

                                                                        <asp:Button ID="imgBtnPayment" runat="server" CssClass=" btn but_b" Text="Confirm" OnClientClick="return validate();" OnClick="imgBtnPayment_Click" />

                                                                        <%  } %>
                                                                    </label>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </div>
                                                    <div class="clearfix"></div>
                                                </div>
                                            </div>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;
                            </td>
                        </tr>
                    </table>
                    <%//if (!btnHold.Visible)
                        try
                        {
                            if (onwardFlightItinerary.IsLCC || (returnFlightItinerary != null?returnFlightItinerary.IsLCC:false))
                            {

                    %>
                    <script type="text/javascript">
                        //document.getElementById('FareDiff').style.display = 'block';
                        $('#FareDiff').modal('show')
                        document.getElementById('divFareDiffText').innerHTML = 'Your booking Airline is Low Cost Carrier, for any cancellations, respective Airline cancellation charges will be applied';
                        document.getElementById('<%=imgBtnPayment.ClientID %>').style.display = 'none';

                    </script>
                    <%}
                        else if (onwardResult.ResultBookingSource == BookingSource.TBOAir)
                        { %>
                    <script type="text/javascript">
                        //document.getElementById('FareDiff').style.display = 'block';
                        $('#FareDiff').modal('show')
                        document.getElementById('divFareDiffText').innerHTML = 'For cancellation/void of this ticket please contact on the same day of ticket issuance before GST 21:30, any requests after the said time/date will not be entertained.';
                        document.getElementById('<%=imgBtnPayment.ClientID %>').style.display = 'none';

                    </script>
                    <%}
                        // Added by lokesh on 7-June-2018 
                        // Need to display the below alert for PK Source 
                        else if (onwardResult.ResultBookingSource == BookingSource.PKFares)
                        { %>
                    <script type="text/javascript">
                        //document.getElementById('FareDiff').style.display = 'block';
                        $('#FareDiff').modal('show')
                        document.getElementById('divFareDiffText').innerHTML = 'Unless ticket number is shown on the ticket copy.Ticket copy should not be handed over to customer.';
                        document.getElementById('<%=imgBtnPayment.ClientID %>').style.display = 'none';

                    </script>
                    <%} %>

                    <%if ((onwardResult.ResultBookingSource == BookingSource.AirArabia || (returnResult != null && returnResult.ResultBookingSource == BookingSource.AirArabia)) && (onOriginalFare != onCurrentFare || retOriginalFare != retCurrentFare))
                        {
                    %>

                    <div class="modal fade pymt-modal" data-backdrop="static" id="BagFareDiff" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
                        <div class="modal-dialog modal-sm" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <h4 class="modal-title" id="FareRuleHead">Baggage Price for the Itinerary has been revised!</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="" id="FareRuleBody" style="text-align: center;">
                                        <div>
                                            <span id="priceChange" style="color: Red; font-size: 14px; font-weight: bold; display: none;">Price has been changed for this Itinerary</span>
                                        </div>
                                        <div class="col-md-12" id="onDiv" style="display: none">
                                            <div>
                                                <span class="ns-h3">
                                                    Including <%=((Settings.LoginInfo != null && Settings.LoginInfo.IsOnBehalfOfAgent && !string.IsNullOrEmpty(location.CountryCode) && location.CountryCode == "IN") ? "GST":"VAT") %>
                                                </span>
                                            </div>
                                            <div class="col-md-6">                                                
                                                <label id="lblOnOriginal">
                                                   
                                                </label>
                                            </div>
                                            <div class="col-md-6">
                                                
                                                <label id="lblOnCurrent" style="color: Red">
                                                   
                                                </label>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <div class="col-md-12" id="retDiv" style="display:none">
                                            <div>
                                                <span class="ns-h3">
                                                    Including <%=((Settings.LoginInfo != null && Settings.LoginInfo.IsOnBehalfOfAgent && !string.IsNullOrEmpty(location.CountryCode) && location.CountryCode == "IN") ? "GST":"VAT") %>
                                                </span>
                                            </div>
                                            <div class="col-md-6">                                                
                                                <label id="lblRetOriginal">
                                                  
                                                </label>
                                            </div>
                                            <div class="col-md-6">                                                
                                                <label id="lblRetCurrent" style="color: Red;">
                                                  
                                                </label>
                                            </div>
                                            <div class="clearfix">
                                            </div>
                                        </div>
                                        <div class="padtop_4">
                                            <input type="button" id="Continue" class="btn but_b" value="Continue Booking" onclick="BagPriceContinue()" />
                                        </div>
                                        <div class="padtop_4">
                                            <input type="button" id="Cancel" class="btn but_b" value="Search Again" onclick="PriceCancel()"
                                                style="width: 150px;" />
                                        </div>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>



                   <%-- <script type="text/javascript">
                        //document.getElementById('BagFareDiff').style.display = 'block';
                        $('#BagFareDiff').modal('show');
                        <%if(onOriginalFare != onCurrentFare) { %>
                        $('#onDiv').show();
                        <%}if(retOriginalFare != retCurrentFare){%>
                        $('#retDiv').show();
                        <%}%>
                        $('#<%=imgBtnPayment.ClientID %>').hide();
                        
                    </script>--%>
                    <%}
                    }
                    catch { } %>
                </asp:View>
                <asp:View ID="ErrorView" runat="server">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>

                                        </td>

                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/HotelSearch.aspx?source=Flight">Go to Search Page</asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </asp:View>
            </asp:MultiView>

        </div>
    </div>

          <!-- For TBO Validation -->                                       
            <div class="modal fade" tabindex="-1" role="dialog" id="TBOConfirm">
                <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body text-center">
                        VOID RESTRICTIONS APPLY. THIS TICKET CANNOT BE VOID
                    </div>
                    <div class="modal-footer text-center">
                        <a class="btn btn-primary" href="" data-dismiss="modal">Cancel </a>
                    </div>
                </div><!-- /.modal-content -->
                </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
	    <!-- END  TBO Validation -->
   
   
   
   
   
   
   
    <!-- Modified by Lokesh  on 19Sep2019-->
    <!-- New Design for E-ticket in Email-->

    <!------- START:ONWARD EMAIL DIV-------------->
    <div id='EmailDiv' runat='server' style='width: 600px; display: none;'>
        
      <%if (onwardFlightItinerary != null && onwardFlightItinerary.FlightId > 0) %>
        <%{ %>

         <div>

                           
                           
                            <% int bc = 0;
                                List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();

                                  if (ticketList != null && ticketList.Count > 0)
                                  {
                                      ptcDetails = ticketList[0].PtcDetail;
                                  }
                                  else// For Hold Bookings
                                  {
                                      ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(onwardFlightItinerary.FlightId);
                                  }
                                for (int count = 0; count < onwardFlightItinerary.Segments.Length; count++)
                                {

                                    CT.Core.Airline airline = new CT.Core.Airline();
                                          airline.Load(onwardFlightItinerary.Segments[count].Airline);
                            booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(onwardFlightItinerary.FlightId));

                                    int paxIndex = 0;
               if (Request["paxId"] != null)
               {
                   paxIndex = Convert.ToInt32(Request["paxId"]);
               }

               List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
               ptcDetail = ptcDetails.FindAll(delegate(SegmentPTCDetail ptc) { return ptc.SegmentId == onwardFlightItinerary.Segments[count].SegmentId; });
                                    
                                    %>

  <div style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%; Margin: 0; background: #f3f3f3 !important; box-sizing: border-box; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100% !important">
    <span class="preheader" style="color: #f3f3f3; display: none !important; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; mso-hide: all !important; opacity: 0; overflow: hidden; visibility: hidden"></span>
    <table class="body" style="Margin: 0; background: #f3f3f3 !important; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
      <tr style="padding: 0; text-align: left; vertical-align: top">
        <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
          <center data-parsed="" style="min-width: 700px; width: 100%">
            <table align="center" class="wrapper b2b-eticket-wrapper float-center" style="Margin: 0 auto; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><td class="wrapper-inner" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
<%if (count == 0)
    { %>
            	<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
       <% } %>     
            	<table align="center" class="container" style="Margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: inherit; vertical-align: top; width: 700px"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
            


<%if (count == 0) { %>
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            
            
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-9 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 509px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<h2 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 22px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left; word-wrap: normal"><strong>e-Ticket</strong></h2>
            			 <span> <%if (onwardFlightItinerary != null && onwardFlightItinerary.CreatedOn != DateTime.MinValue) %>
                    <%{ %>
                                              Booking Date:  <%=onwardFlightItinerary.CreatedOn.ToString("ddd") + "," + onwardFlightItinerary.CreatedOn.ToString("dd MMM yyyy")%>
                    <%} %>	</span>
            		</th></tr></table></th>
            		<th class="small-12 large-3 columns last" right="" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 159px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">            			

                             <asp:Image ID="imgLogo" style="-ms-interpolation-mode: bicubic; border-width: 0px; clear: both; display: block; height: 51px; max-width: 100%; outline: none; text-decoration: none; width: 159px" runat="server" AlternateText="AgentLogo"  />

            		</th></tr></table></th>
            	  </tr></tbody></table>
            	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 15px; font-weight: bold; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left; word-wrap: normal">Reservation Details</h3>
            		</th></tr></table></th>
            		<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<table class="float-right text-right" id="tblPrintEmailActions1" align="right" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: right; vertical-align: top; width: 100%">
            				<tr style="padding: 0; text-align: left; vertical-align: top">
            					<td class="float-right text-right" valign="middle" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: right; vertical-align: middle; word-wrap: break-word">
            						<span class="float-right text-right" style="Margin: 0; color: #2199e8; font-family: 'Open Sans', Arial, sans-serif; font-weight: normal; height: 20px; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none">
                                        
<%--                                             <img id="imgEmail" runat="server" src="https://ctb2b.cozmotravel.com/images/email_icon.png" alt="Email" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: inline; height: 12px; margin-right: 5px; max-width: 100%; outline: none; text-decoration: none; vertical-align: middle; width: 15px" />
                                              <asp:LinkButton ID="btnEmail" runat="server" Text="Email" OnClientClick="return ShowPopUp(this.id);" />--%>
                                            </span>
            				<span class="float-right text-right" style="Margin: 0; color: #2199e8; font-family: 'Open Sans', Arial, sans-serif; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none">
                           
<%--                                    <img id="imgPrint" runat="server" src="https://ctb2b.cozmotravel.com/images/print_icon.jpg" alt="Print" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: inline; height: 20px; max-width: 100%; outline: none; text-decoration: none; vertical-align: middle; width: 20px" class="" />

                                          <asp:LinkButton ID="btnPrint" runat="server" Text="Print" OnClientClick="return prePrint();" />--%>
                                    </span>	
            					</td>
            				</tr>
            			</table>
            			
            		</th></tr></table></th>
            	  </tr></tbody></table>
            
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody>
                      <tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">PNR NO. | <strong> <%=(onwardFlightItinerary.PNR.Split('|').Length > 1 ? onwardFlightItinerary.PNR.Split('|')[onwardFlightItinerary.Segments[count].Group] : onwardFlightItinerary.PNR)%></strong></p>
            	  	</th></tr></table></th>


            	  	<%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo != null) %>
                          <%{
                                  var parentid1 =CT.TicketReceipt.BusinessLayer.AgentMaster.GetParentId(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                                  CT.TicketReceipt.BusinessLayer.LocationMaster location1 = new CT.TicketReceipt.BusinessLayer.LocationMaster(onwardFlightItinerary.LocationId); 
                                  
                                  %>
                                  
                          <%if (parentid1 == 2125 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId == 2125) %>
                          <%{ %>
            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">Client |  <strong> <%=hdnAgencyName.Value %></strong>Address | <strong> <%=location1.Address %></strong> </p>
            	  	</th></tr></table></th>
                          <%} %>
                          <%else %>
                          <%{ %>
                          <th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">Agent Name |  <strong> <%=hdnAgencyName.Value %></strong></p>
            	  	</th></tr></table></th>

                          <%} %>

                          <%} %>




            	  </tr>




       


  <%var parentid =CT.TicketReceipt.BusinessLayer.AgentMaster.GetParentId(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                                                                                         if (parentid == 2125 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId==2125)

                                                                                         {
                                                                                      CT.TicketReceipt.BusinessLayer.LocationMaster location = new CT.TicketReceipt.BusinessLayer.LocationMaster(onwardFlightItinerary.LocationId);       %>

                  <tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">GSTIN | <strong>  <%=location.GstNumber %></strong></p>
            	  	</th></tr></table></th>
            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"></th>
            	  </tr>
        <%} %>


            	                                                                                                                                                                              </tbody></table>
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
        <%if(!string.IsNullOrEmpty(onwardFlightItinerary.RoutingTripId)){ %>
                       	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">Routing Trip Id | <strong> <%=onwardFlightItinerary.RoutingTripId %></strong></p>
            	  	</th></tr></table></th>
 <%} %>

            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">PHONE  |  <strong><%=hdnAgencyPhone.Value %></strong></p>
            	  	</th></tr></table></th>
            	  </tr></tbody></table>

    <% }%>









        <%if (count == 0)
                                                { %>
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">	
            	  		<table class="b2b-baggage-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Passenger Name</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">E-Ticket Number	</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Baggage</th>
            				
                            <%if (onwardFlightItinerary != null && onwardFlightItinerary.FlightBookingSource == BookingSource.AirArabia || onwardFlightItinerary.FlightBookingSource == BookingSource.UAPI || onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp)  %>
                                                        <%{ %>        
                                  	<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Meal</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Seats</th>
       <%} %>


            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Airline Ref.</th>
            	  			</tr>




                                 <%if (onwardFlightItinerary.Passenger !=null)
                                                        {%>
                                                    <%for (int j = 0; j < onwardFlightItinerary.Passenger.Length; j++)
                                                        {
                                                    %>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">	
                                                   <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">      <%=onwardFlightItinerary.Passenger[j].Title + " " + onwardFlightItinerary.Passenger[j].FirstName + " " + onwardFlightItinerary.Passenger[j].LastName%></td>
                                                  <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%if (ticketList != null && ticketList.Count > 0) { %>
                                           <%=(ticketList[j].TicketNumber.Split('|').Length > 1 ? ticketList[j].TicketNumber.Split('|')[onwardFlightItinerary.Segments[count].Group] : ticketList[j].TicketNumber) %>
                                            <%} else { //for Corporate HOLD Booking%>
                                        <%=(onwardFlightItinerary.PNR.Split('|').Length > 1 ? onwardFlightItinerary.PNR.Split('|')[onwardFlightItinerary.Segments[count].Group] : onwardFlightItinerary.PNR)%>
                                            <%} %>

                                                  </td>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            
                                                            
   <%if (onwardFlightItinerary.FlightBookingSource == BookingSource.Amadeus || onwardFlightItinerary.FlightBookingSource == BookingSource.UAPI || (onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir && !(onwardFlightItinerary.IsLCC)))
    {%>
                             <%=GetBaggageForGDS(onwardFlightItinerary.FlightId,onwardFlightItinerary.Passenger[j].Type)%>
    <%}                                                            
 else if (onwardFlightItinerary.FlightBookingSource == BookingSource.AirArabia || onwardFlightItinerary.FlightBookingSource == BookingSource.FlyDubai || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo || onwardFlightItinerary.FlightBookingSource == BookingSource.PKFares || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.Jazeera || onwardFlightItinerary.FlightBookingSource == BookingSource.SalamAir)
    {
        if (onwardFlightItinerary.Passenger[j].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(onwardFlightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : onwardFlightItinerary.Passenger[j].BaggageCode)%>
                                                     <%}
    }
    else if (onwardFlightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl && onwardFlightItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
                                                       
                                                            <%=(string.IsNullOrEmpty(onwardFlightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : onwardFlightItinerary.Passenger[j].BaggageCode)%>
                                                        
                                                    <%}
    //Added by lokesh on 22-06-2018
    //For Amadeus Air Source
    //Display baggage information based on the pax type.
    else if (onwardFlightItinerary.FlightBookingSource == BookingSource.Amadeus && onwardFlightItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
              <%=(string.IsNullOrEmpty(onwardFlightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : onwardFlightItinerary.Passenger[j].BaggageCode)%>
    <%}

    else if (onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir && (onwardFlightItinerary.IsLCC))
    {
        if (onwardFlightItinerary.Passenger[j].Type != PassengerType.Infant)
        {
            string strBaggage = string.Empty;
            
            if (!string.IsNullOrEmpty(onwardFlightItinerary.Passenger[j].BaggageCode))
            {
                strBaggage = onwardFlightItinerary.Passenger[j].BaggageCode;
            }
            else
            {
                strBaggage = "Airline Norms";
            }
                                                           %>
                                                      <%=strBaggage%> 
                                                    <%}
    } %> </td>


                                                        <%if (onwardFlightItinerary.FlightBookingSource == BookingSource.AirArabia || onwardFlightItinerary.FlightBookingSource == BookingSource.UAPI || onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{
                                                                string MealDesc = string.Empty;
                                                                %>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (onwardFlightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(onwardFlightItinerary.Passenger[j].MealDesc)) %>
                                                            <%{
                                                                    MealDesc = onwardFlightItinerary.Passenger[j].MealDesc;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Meals"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (onwardFlightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(onwardFlightItinerary.Passenger[j].Seat.Code)) %>
                                                            <%{
                                                                    MealDesc = onwardFlightItinerary.Passenger[j].Seat.Code;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Seats"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <%} %>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">  <%=(onwardFlightItinerary.Segments[count].AirlinePNR == null || onwardFlightItinerary.Segments[count].AirlinePNR == "" ? (onwardFlightItinerary.PNR.Split('|').Length > 1 ? onwardFlightItinerary.PNR.Split('|')[onwardFlightItinerary.Segments[count].Group] : onwardFlightItinerary.PNR) : onwardFlightItinerary.Segments[count].AirlinePNR) %></td>
                                                    </tr>
                                                    <%}
                                                        }%>




            	  		</table>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th>

            	  	                                                                                                                                                                                                                                                                                                                                                                                                                                                    </tr></table></th>	  	
            	  </tr></tbody></table>

    <% }%>





            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">			  
            	  	  <table class="flight-list-table" style="border: 1px solid #f3f3f3; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  	    <tr class="first-row" style="padding: 0; text-align: left; vertical-align: top">
            	  	    	<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 4px; text-align: left; vertical-align: top; word-wrap: break-word;">

            	  	    		<img class="float-left" src="https://ctb2b.cozmotravel.com/images/AirlineLogo/<%=airline.AirlineCode%>.gif" alt="<%=airline.AirlineName%>" style="-ms-interpolation-mode: bicubic; clear: both; display: block; float: left; max-width: 100%; outline: none; padding-right: 5px; text-align: left; text-decoration: none; width: auto">

            					<p class="float-left" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; padding-top: 5px; text-align: left"><%airline.Load(onwardFlightItinerary.Segments[count].Airline); %> <%=airline.AirlineName%> <strong><%=onwardFlightItinerary.Segments[count].Airline + "-" + onwardFlightItinerary.Segments[count].FlightNumber%></strong>
                                           <%try
                                                                                                {
                                                                                                    if ((!onwardFlightItinerary.IsLCC && onwardFlightItinerary.Segments[count].OperatingCarrier != null && onwardFlightItinerary.Segments[count].OperatingCarrier.Length > 0)||(onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo && onwardFlightItinerary.Segments[count].OperatingCarrier != null && onwardFlightItinerary.Segments[count].OperatingCarrier.Length > 0 && onwardFlightItinerary.Segments[count].OperatingCarrier != "6E"))////Modified for Indigo CodeShare
                                                                                                    {
                                                                                                        string opCarrier = onwardFlightItinerary.Segments[count].OperatingCarrier;
                                                                                                        CT.Core.Airline opAirline = new CT.Core.Airline();
                                                                                                        if (opCarrier.Split('|').Length > 1)
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Split('|')[0]);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Substring(0, 2));
                                                                                                        } %>
                                             (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                                }
                                                catch { } %>
                                    
                                    </p>
            	  	    	</td>
            	  	    	<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 4px; text-align: left; vertical-align: middle; word-wrap: break-word">
            	  	    		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right"><strong> <%=onwardFlightItinerary.Segments[count].Origin.CityName%> to <%=onwardFlightItinerary.Segments[count].Destination.CityName%></strong> | <strong><%=onwardFlightItinerary.Segments[count].DepartureTime.ToString("ddd")+","+onwardFlightItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy")%></strong></p>
            	  	    	</td>
            	  	    </tr>
            			<tr style="padding: 0; text-align: left; vertical-align: top">
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word;width:30%">
            					<strong class="city-code" style="font-size: 18px"><%=onwardFlightItinerary.Segments[count].Origin.CityCode%></strong> <%=onwardFlightItinerary.Segments[count].Origin.CityName%><br>
            					<strong class="time" style="font-size: 14px"><%=onwardFlightItinerary.Segments[count].DepartureTime.ToString("HH:mm tt")%></strong><br>
            					<%=onwardFlightItinerary.Segments[count].DepartureTime.ToString("ddd")+","+onwardFlightItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy")%><br>
            					<%=onwardFlightItinerary.Segments[count].Origin.AirportName %><br>
            					Terminal <%=onwardFlightItinerary.Segments[count].DepTerminal %>
            				</td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: center; vertical-align: middle; word-wrap: break-word;width:20%">
            				 <center style="min-width: 21px; width: 100%" data-parsed="">
            						<img src="https://ctb2b.cozmotravel.com/images/aircraft_icon.png" alt="Flight Duration" style="-ms-interpolation-mode: bicubic; Margin: 0 auto; clear: both; display: block; float: none; margin: 0 auto; max-width: 100%; min-width: 21px; outline: none; text-align: center; text-decoration: none; vertical-align: middle; width: 21px" align="center" class="float-center">
            						   <%=onwardFlightItinerary.Segments[count].Duration.Hours +"hr "+onwardFlightItinerary.Segments[count].Duration.Minutes+"m"%>
            				  </center>
            			  </td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; border-right: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word;width:30%">
            					<strong class="city-code" style="font-size: 18px"><%=onwardFlightItinerary.Segments[count].Destination.CityCode%></strong> <%=onwardFlightItinerary.Segments[count].Destination.CityName%><br> 
            					<strong class="time" style="font-size: 14px"><%=onwardFlightItinerary.Segments[count].ArrivalTime.ToString("HH:mm tt")%></strong><br>
            					<%=onwardFlightItinerary.Segments[count].ArrivalTime.ToString("ddd")+","+onwardFlightItinerary.Segments[count].ArrivalTime.ToString("dd MMM yyyy")%><br>
            					<%=onwardFlightItinerary.Segments[count].Destination.AirportName %><br>
            					Terminal  <%=onwardFlightItinerary.Segments[count].ArrTerminal %>
            				</td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word;width:20%">
            					<strong>
                                     <%if (onwardFlightItinerary.Segments[count].Stops == 0) %>
                    <%{ %>
                    <strong> Non-Stop </strong>
                    <%} %>
                    <%else if (onwardFlightItinerary.Segments[count].Stops == 1) %>
                    <%{ %>
                    <strong> Single Stop </strong>
                    <%} %>
                    <%else if (onwardFlightItinerary.Segments[count].Stops == 2) %>
                    <%{ %>
                    <strong> Two Stops </strong>
                    <%} %>
                    <%else if (onwardFlightItinerary.Segments[count].Stops> 2) %>
                    <%{ %>
                    <strong> Two+ Stops </strong>
                    <%} %>

            					</strong><br>
            					Class | <%=onwardFlightItinerary.Segments[count].CabinClass%><br>
                                      <%if (onwardFlightItinerary.FlightBookingSource == BookingSource.FlyDubai || onwardFlightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.Jazeera || onwardFlightItinerary.FlightBookingSource == BookingSource.AirArabia)
                                                                                                {%>
            					Fare Type | <%=onwardFlightItinerary.Segments[count].SegmentFareType%><br>
                                        <%} %>
            					<strong><%=booking.Status.ToString()%></strong>
            				</td>
            			</tr>










            		</table></th>

  

<%--<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th>--%>


            	  	                                                                                                                                                                                                                                                                                                                                                                                                                                                    </tr></table></th>		  	
            	  </tr></tbody></table>   <%}%>  



            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 15px; font-weight: bold; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left; word-wrap: normal"><%--Price Details--%></h3>
            	  	</th>
                          <th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: right"> <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: #5f5f5f;" id="ancFare" runat="server"
                                            onclick="return ShowHide('divFare');"  class="showhideDiv"><%--Hide Fare--%></a>	</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>


              
                           
                            <div title="Param" id="divFare" runat="server" style="font-size: 12px;">

  <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0, outputVAT = 0,Meal=0,SeatPrice=0,k3Tax=0;

                        if (ticketList != null && ticketList.Count > 0)
                        {
                            List<TaxBreakup> allPaxTaxBreakups = new List<TaxBreakup>();
                            ticketList.ForEach(t =>  t.TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); } ));

                              switch(onwardFlightItinerary.FlightBookingSource)
                                  {
                                     case BookingSource.GoAir:
                                     case BookingSource.GoAirCorp:
                                          k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("ST") || t.TaxCode.Contains("CT") || t.TaxCode.Contains("IT")? t.TaxValue : 0);
                                          break;
                                    case BookingSource.UAPI:
                                         k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                         break;
                                    case BookingSource.SpiceJet:
                                    case BookingSource.SpiceJetCorp:
                                    case BookingSource.Indigo:
                                    case BookingSource.IndigoCorp:
                                         k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("GST") ? t.TaxValue : 0);
                                         break;
                                    default:
                                         k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                         break;
                                 }
                            for (int k = 0; k < ticketList.Count; k++)
                            {
                                AirFare += ticketList[k].Price.PublishedFare + ticketList[k].Price.HandlingFeeAmount;
                                Taxes += ticketList[k].Price.Tax + ticketList[k].Price.Markup;
                                if (onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.OtherCharges + ticketList[k].Price.SServiceFee + ticketList[k].Price.TransactionFee;
                                }
                                Baggage += ticketList[k].Price.BaggageCharge;
                                Meal += ticketList[k].Price.MealCharge;
                                SeatPrice += ticketList[k].Price.SeatPrice;
                                MarkUp += ticketList[k].Price.Markup;
                                Discount += ticketList[k].Price.Discount;
                                AsvAmount += ticketList[k].Price.AsvAmount;
                                outputVAT += ticketList[k].Price.OutputVATAmount;
                            }
                            if (ticketList[0].Price.AsvElement == "BF")
                            {
                                AirFare += AsvAmount;
                            }
                            else if (ticketList[0].Price.AsvElement == "TF")
                            {
                                Taxes += AsvAmount;
                            }
                        }
                        else
                        {
                            for (int k = 0; k < onwardFlightItinerary.Passenger.Length; k++)
                            {
                                AirFare += onwardFlightItinerary.Passenger[k].Price.PublishedFare + onwardFlightItinerary.Passenger[k].Price.HandlingFeeAmount;
                                Taxes += onwardFlightItinerary.Passenger[k].Price.Tax + onwardFlightItinerary.Passenger[k].Price.Markup;
                                if (onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += onwardFlightItinerary.Passenger[k].Price.AdditionalTxnFee + onwardFlightItinerary.Passenger[k].Price.OtherCharges + onwardFlightItinerary.Passenger[k].Price.SServiceFee + onwardFlightItinerary.Passenger[k].Price.TransactionFee;
                                }
                                Baggage += onwardFlightItinerary.Passenger[k].Price.BaggageCharge;
                                Meal += onwardFlightItinerary.Passenger[k].Price.MealCharge;
                                SeatPrice += onwardFlightItinerary.Passenger[k].Price.SeatPrice;
                                MarkUp += onwardFlightItinerary.Passenger[k].Price.Markup;
                                Discount += onwardFlightItinerary.Passenger[k].Price.Discount;
                                AsvAmount += onwardFlightItinerary.Passenger[k].Price.AsvAmount;
                                outputVAT += onwardFlightItinerary.Passenger[k].Price.OutputVATAmount;
                                if (onwardFlightItinerary.Passenger[k].Price.AsvElement == "BF")
                                {
                                    AirFare += AsvAmount;
                                }
                                else if (onwardFlightItinerary.Passenger[k].Price.AsvElement == "TF")
                                {
                                    Taxes += AsvAmount;
                                }
                            }
                        }
                                            %>
                                            <% if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.BaseAgent)
                                               { %>

            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<table class="pricing-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Air Fare</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=AirFare.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right"> Taxes & Fees</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word"> <%=(Taxes-k3Tax).ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right"> K3 Tax</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=k3Tax.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>

            	  			</tr>
                    

                          <%if (onwardFlightItinerary.FlightBookingSource == BookingSource.AirArabia || onwardFlightItinerary.FlightBookingSource == BookingSource.FlyDubai || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp && Baggage > 0 || onwardFlightItinerary.IsLCC)
                                                                                    { %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Baggage Fare </th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=Baggage.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>

                                   <%} %>


                                        <%if (onwardFlightItinerary != null && (onwardFlightItinerary.FlightBookingSource == BookingSource.UAPI || onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAir || onwardFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.AirArabia)) %>
                                                                                <%{ %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Meal Price</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=Meal.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Seat Price</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">    <%=SeatPrice.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>

     <%} %>


             <%if (Discount > 0)
                                                                                    { %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Discount</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  -<%=Discount.ToString("N" + agency.DecimalValue) %><%=agency.AgentCurrency%></td>
            	  			</tr>
       <%} %>


            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">
                                                                 <%CT.TicketReceipt.BusinessLayer.LocationMaster locationGST = new CT.TicketReceipt.BusinessLayer.LocationMaster(onwardFlightItinerary.LocationId);
                                                                                             if (locationGST.CountryCode == "IN")
                                                                                            {%>
                                                                                        GST
                                                                                        <%}    else
    { %>
                                                                                        VAT
                                                                                        <%} %>
                                                                                           

                                      </th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=outputVAT.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  
                                 <th valign="middle" width="220px" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">
            				 	     <strong style="font-size: 11px"> This is an Electronic ticket. Please carry a positive identification for Check in.</strong>
            				     </th>					
            	  				<th width="220px" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right;vertical-align:middle"><strong>Total Air Fare</strong></th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word;vertical-align:middle">

                                       <%if (onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                                                                            { %>
                                                                                        <b><%=Math.Ceiling((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%}
                                                                                         else if (onwardFlightItinerary.FlightBookingSource == BookingSource.OffLine) 
                                                                                            { %>
                                                                                        <b><%=((AirFare + Taxes + outputVAT ) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%}

                                                                                            else
                                                                                            { %>
                                                                                        <b><%=((AirFare + Taxes + Baggage + Meal + SeatPrice + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%} %>
            	  				</td>
            	  			</tr>
            	  		</table>
            	  	</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>

     <%}
                                              %>

         </div>

            	    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
             	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">For international flights, you are suggested to arrive at the airport at least 3-4 hours prior to your departure time to check in and go through security. For connecting flights, please confirm at the counter whether not you can check your luggage through to the final destination when you check in.</p>
            			 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">Don’t forget to purchase travel insurance for your visit.Please contact us to purchase travel insurance.</p>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>
            	
            
            
            
            
            
            
            	</td></tr>
                    <tr><td>
                       <% if (onwardFlightItinerary != null && onwardFlightItinerary.Segments.ToList().All(x => x.Destination.CountryCode == x.Origin.CountryCode) && (onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || onwardFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp))
                           { %>
             <%if (onwardFlightItinerary.FlightBookingSource == BookingSource.Indigo || onwardFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp)
                 { %>
                 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
             	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">
             <strong style="color:red">Important Update: Please read carefully.</strong><br/>
            Due to ongoing COVID-19 situation, for Safe and Seamless journey and as per the implementation of various safety procedure<br/>
            For Safe and Seamless travels of our guests. It is mandatory to do <span style="text-decoration:underline;font-weight:bold"><span style="color:red">“Web Check-in” along with carrying Boarding pass, affixing of baggage label</span> and following all government guidelines while travelling.</span>
            <ul style="font-weight:bold;">
            <li>Web check-in is now mandatory for each passenger for all Domestic flights.</li>
            <li>Mandatory Check-in: Web check-in to be done upto <span style="text-decoration:underline">75 minutes</span> prior from the schedule flight departure of their domestic flight.<br/>
            How to Check In :  Click on the following link which will take you to the Airline Website <a href="https://www.goindigo.in/how-to-check-in.html?linkNav=how-to-check-in_header" target="_blank">https://www.goindigo.in/how-to-check-in.html?linkNav=how-to-check-in_header</a>. Once clicking on the link, it will be redirected to Indigo Airline website Check-in page. Passengers have to mention PNR and Email/Last name in the specified field to proceed with the Check-in process.</li> 
            <li>Passenger Health declaration status and their mobile should have <span style="text-decoration:underline">“Arogya Setu”</span> app which shows Green status.  Mandatory to add <span style="text-decoration:underline">SSR “HCOK”</span> while Web Checkin.</li>
            <%if(agency.AgentCurrency != "INR"){ //Hide only for India Agents%>
            <li>Passengers travelling with Check in luggage, need to add their luggage during web check-in. Per Passenger 1 piece of Checked in Baggage upto 20 kgs will be allowed. Addition to this one small hand luggage max of 7 Kgs per passenger will be allowed.</li>   
                <%} %>
            <li>Carriage of printout of Boarding card and Baggage label which will be sent to the guest on their contact details or can download during web checkin.</li> 
            <li>Also can carry Mobile boarding pass and carry printed or written baggage label, format will be available on Indigo website.</li> 
            <li>Before reaching to the airport, Passenger need to paste the attached Baggage label on the baggage OR guest can write the mandatory fields i.e. PNR, Name of the passenger, Sector and Flight Number on plain paper & paste the same on baggage which should be prominently visible, as airport may not be having any pasting material to place the labels on the baggage.</li>
            <li>Passenger must be wearing mask properly at the airport & throughout the journey.</li> 
            <li>Passenger must sanitize at airports at various points.</li> 
            <li>Maintain Social distancing at the airport as per the markings/signage.</li> 
            <li>For Safety of all customers, Indigo reserves the rights to refuse the carriage of any passenger whose contact details are missing. Also, its mandatory to do the Web Check-in.</li>
            </ul>
            
                    </p>
            			 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left"></p>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>
            <%}
                if (onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || onwardFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
                { %>
                <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
             	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">
                <strong style="color:red">Important Update: Please read carefully.</strong><br/>
                Due to ongoing COVID-19 situation, for Safe and Seamless journey and as per the implementation of various safety procedure<br/>
                For Safe and Seamless travels of our guests. It is mandatory to do <span style="text-decoration:underline;font-weight:bold"><span style="color:red">“Web Check-in” along with carrying Boarding pass, affixing of baggage label</span> and following all government guidelines while travelling.</span>
                <ul style="font-weight:bold;">
                <li>Web check-in is now mandatory for each passenger for all Domestic flights.</li>
                <li>Mandatory Check-in: Web check-in to be done upto <span style="text-decoration:underline">75 minutes</span> prior from the schedule flight departure of their domestic flight.<br/>
                How to Check In :  Click on the following link which will take you to the Airline Website <a href="https://book.spicejet.com/Search.aspx?op=4" target="_blank">https://book.spicejet.com/Search.aspx?op=4 </a>. Once clicking on the link, it will be redirected to Spice Jet Airline website Check-in page. Passengers have to mention PNR and Email/Last name in the specified field to proceed with the Check-in process.</li> 
                <li>Passenger Health declaration status and their mobile should have <span style="text-decoration:underline">“Arogya Setu”</span> app which shows Green status.  Mandatory to add <span style="text-decoration:underline">SSR “HCOK”</span> while Web Checkin.</li>
                <%if(agency.AgentCurrency != "INR"){ //Hide only for India Agents%>
            <li>Passengers travelling with Check in luggage, need to add their luggage during web check-in. Per Passenger 1 piece of Checked in Baggage upto 20 kgs will be allowed. Addition to this one small hand luggage max of 7 Kgs per passenger will be allowed.</li>   
                <%} %>
                <li>Carriage of printout of Boarding card and Baggage label which will be sent to the guest on their contact details or can download during web checkin.</li> 
                <li>Also can carry Mobile boarding pass and carry printed or written baggage label, format will be available on Spice Jet website.</li> 
                <li>Before reaching to the airport, Passenger need to paste the attached Baggage label on the baggage OR guest can write the mandatory fields i.e. PNR, Name of the passenger, Sector and Flight Number on plain paper & paste the same on baggage which should be prominently visible, as airport may not be having any pasting material to place the labels on the baggage.</li>
                <li>Passenger must be wearing mask properly at the airport & throughout the journey.</li> 
                <li>Passenger must sanitize at airports at various points.</li> 
                <li>Maintain Social distancing at the airport as per the markings/signage.</li> 
                <li>For Safety of all customers, Spice Jet reserves the rights to refuse the carriage of any passenger whose contact details are missing. Also, its mandatory to do the Web Check-in.</li>
                </ul></p>
            			 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left"></p>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>
             <%}
                 } %>
                        </td></tr></tbody></table>
            
            </td></tr></table>
            
          </center>
        </td>
      </tr>
    </table>



            	<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
  
    <!-- prevent Gmail on iOS font size manipulation -->
   <div style="display:none; white-space:nowrap; font:15px courier; line-height:0"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
  </div>

                                    <!-- Added by lokesh on 7-June-2018-->
        <!-- Need to display the below note in email for PK Fares -->
         <%if (onwardFlightItinerary != null && onwardFlightItinerary.FlightBookingSource == BookingSource.PKFares) %>
         <%{ %>
         <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                <span style="color:red">Note: This is just an itinerary. Ticket copy should not be handed over to customer unless ticket number is shown on the ticket.</span>
                    
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <%} %>



                        </div>


        <%} %>
  
   
    </div>
   <!------- End:ONWARD EMAIL DIV-------------->


  <!------- START:RETURN EMAIL DIV-------------->
    <div id='ReturnEmailDiv' runat='server' style='width: 600px; display: none;'>
        <%if (returnFlightItinerary != null && returnFlightItinerary.FlightId > 0) %>
        <%{ %>
         <div>

                           
                           
                            <% int bc = 0;
                                List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();

                                  if (ticketList != null && ticketList.Count > 0)
                                  {
                                      ptcDetails = ticketList[0].PtcDetail;
                                  }
                                  else// For Hold Bookings
                                  {
                                      ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(returnFlightItinerary.FlightId);
                                  }
                                for (int count = 0; count < returnFlightItinerary.Segments.Length; count++)
                                {

                                    CT.Core.Airline airline = new CT.Core.Airline();
                                          airline.Load(returnFlightItinerary.Segments[count].Airline);
                            booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(returnFlightItinerary.FlightId));

                                    int paxIndex = 0;
               if (Request["paxId"] != null)
               {
                   paxIndex = Convert.ToInt32(Request["paxId"]);
               }

               List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
               ptcDetail = ptcDetails.FindAll(delegate(SegmentPTCDetail ptc) { return ptc.SegmentId == returnFlightItinerary.Segments[count].SegmentId; });
                                    
                                    %>

  <div style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%; Margin: 0; background: #f3f3f3 !important; box-sizing: border-box; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100% !important">
    <span class="preheader" style="color: #f3f3f3; display: none !important; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; mso-hide: all !important; opacity: 0; overflow: hidden; visibility: hidden"></span>
    <table class="body" style="Margin: 0; background: #f3f3f3 !important; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
      <tr style="padding: 0; text-align: left; vertical-align: top">
        <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
          <center data-parsed="" style="min-width: 700px; width: 100%">
            <table align="center" class="wrapper b2b-eticket-wrapper float-center" style="Margin: 0 auto; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><td class="wrapper-inner" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
<%if (count == 0)
    { %>
            	<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
       <% } %>     
            	<table align="center" class="container" style="Margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: inherit; vertical-align: top; width: 700px"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
            


<%if (count == 0) { %>
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            
            
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-9 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 509px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<h2 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 22px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 10px; padding: 0; text-align: left; word-wrap: normal"><strong>e-Ticket</strong></h2>
            			 <span> <%if (returnFlightItinerary != null && returnFlightItinerary.CreatedOn != DateTime.MinValue) %>
                    <%{ %>
                                              Booking Date:  <%=returnFlightItinerary.CreatedOn.ToString("ddd") + "," + returnFlightItinerary.CreatedOn.ToString("dd MMM yyyy")%>
                    <%} %>	</span>
            		</th></tr></table></th>
            		<th class="small-12 large-3 columns last" right="" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 159px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">            			

                             <asp:Image ID="Image1" style="-ms-interpolation-mode: bicubic; border-width: 0px; clear: both; display: block; height: 51px; max-width: 100%; outline: none; text-decoration: none; width: 159px" runat="server" AlternateText="AgentLogo"  />

            		</th></tr></table></th>
            	  </tr></tbody></table>
            	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 15px; font-weight: bold; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left; word-wrap: normal">Reservation Details</h3>
            		</th></tr></table></th>
            		<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<table class="float-right text-right" id="tblPrintEmailActions2" align="right" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: right; vertical-align: top; width: 100%">
            				<tr style="padding: 0; text-align: left; vertical-align: top">
            					<td class="float-right text-right" valign="middle" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: right; vertical-align: middle; word-wrap: break-word">
            						<span class="float-right text-right" style="Margin: 0; color: #2199e8; font-family: 'Open Sans', Arial, sans-serif; font-weight: normal; height: 20px; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none">
                                        
<%--                                             <img id="imgEmail" runat="server" src="https://ctb2b.cozmotravel.com/images/email_icon.png" alt="Email" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: inline; height: 12px; margin-right: 5px; max-width: 100%; outline: none; text-decoration: none; vertical-align: middle; width: 15px" />
                                              <asp:LinkButton ID="btnEmail" runat="server" Text="Email" OnClientClick="return ShowPopUp(this.id);" />--%>
                                            </span>
            				<span class="float-right text-right" style="Margin: 0; color: #2199e8; font-family: 'Open Sans', Arial, sans-serif; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left; text-decoration: none">
                           
<%--                                    <img id="imgPrint" runat="server" src="https://ctb2b.cozmotravel.com/images/print_icon.jpg" alt="Print" style="-ms-interpolation-mode: bicubic; border: none; clear: both; display: inline; height: 20px; max-width: 100%; outline: none; text-decoration: none; vertical-align: middle; width: 20px" class="" />

                                          <asp:LinkButton ID="btnPrint" runat="server" Text="Print" OnClientClick="return prePrint();" />--%>
                                    </span>	
            					</td>
            				</tr>
            			</table>
            			
            		</th></tr></table></th>
            	  </tr></tbody></table>
            
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody>
                      <tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">PNR NO. | <strong> <%=(returnFlightItinerary.PNR.Split('|').Length > 1 ? returnFlightItinerary.PNR.Split('|')[returnFlightItinerary.Segments[count].Group] : returnFlightItinerary.PNR)%></strong></p>
            	  	</th></tr></table></th>



	<%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo != null) %>
                          <%{
                                  var parentid1 =CT.TicketReceipt.BusinessLayer.AgentMaster.GetParentId(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                                  CT.TicketReceipt.BusinessLayer.LocationMaster location1 = new CT.TicketReceipt.BusinessLayer.LocationMaster(returnFlightItinerary.LocationId); 
                                  
                                  %>
                                  
                          <%if (parentid1 == 2125 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId == 2125) %>
                          <%{ %>
            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">Client |  <strong> <%=hdnAgencyName.Value %></strong> Address | <strong> <%=location1.Address %></strong> </p>
            	  	</th></tr></table></th>
                          <%} %>
                          <%else %>
                          <%{ %>
                          <th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">Agent Name |  <strong> <%=hdnAgencyName.Value %></strong></p>
            	  	</th></tr></table></th>

                          <%} %>

                          <%} %>




            	  </tr>




       


  <%var parentid =CT.TicketReceipt.BusinessLayer.AgentMaster.GetParentId(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                                                                                         if (parentid == 2125 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId==2125)

                                                                                         {
                                                                                      CT.TicketReceipt.BusinessLayer.LocationMaster location = new CT.TicketReceipt.BusinessLayer.LocationMaster(returnFlightItinerary.LocationId);       %>

                  <tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">GSTIN | <strong>  <%=location.GstNumber %></strong></p>
            	  	</th></tr></table></th>
            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"></th>
            	  </tr>
        <%} %>


            	                                                                                                                                                                              </tbody></table>
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
        <%if(!string.IsNullOrEmpty(returnFlightItinerary.RoutingTripId)){ %>
                       	<th class="small-12 large-6 columns first" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 8px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">Routing Trip Id | <strong> <%=returnFlightItinerary.RoutingTripId %></strong></p>
            	  	</th></tr></table></th>
 <%} %>

            	  	<th class="small-12 large-6 columns last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 8px; padding-right: 16px; text-align: left; width: 334px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right">PHONE  |  <strong><%=hdnAgencyPhone.Value %></strong></p>
            	  	</th></tr></table></th>
            	  </tr></tbody></table>

    <% }%>









        <%if (count == 0)
                                                { %>
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">	
            	  		<table class="b2b-baggage-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Passenger Name</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">E-Ticket Number	</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Baggage</th>
            				
                            <%if (returnFlightItinerary != null && returnFlightItinerary.FlightBookingSource == BookingSource.AirArabia || returnFlightItinerary.FlightBookingSource == BookingSource.UAPI || returnFlightItinerary.FlightBookingSource == BookingSource.Indigo || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || returnFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || returnFlightItinerary.FlightBookingSource == BookingSource.GoAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp)  %>
                                                        <%{ %>        
                                  	<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Meal</th>
            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Seats</th>
       <%} %>


            					<th style="Margin: 0; background: linear-gradient(to bottom, #fafafa 0%, #e8e8e8 100%); border: 1px solid #ddd; border-color: #dbdbdb; color: #333; filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7',GradientType=0 ); font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top">Airline Ref.</th>
            	  			</tr>




                                 <%if (returnFlightItinerary.Passenger !=null)
                                                        {%>
                                                    <%for (int j = 0; j < returnFlightItinerary.Passenger.Length; j++)
                                                        {
                                                    %>
                                                    <tr style="padding: 0; text-align: left; vertical-align: top">	
                                                   <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">      <%=returnFlightItinerary.Passenger[j].Title + " " + returnFlightItinerary.Passenger[j].FirstName + " " + returnFlightItinerary.Passenger[j].LastName%></td>
                                                  <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            <%if (ticketList != null && ticketList.Count > 0) { %>
                                           <%=(ticketList[j].TicketNumber.Split('|').Length > 1 ? ticketList[j].TicketNumber.Split('|')[returnFlightItinerary.Segments[count].Group] : ticketList[j].TicketNumber) %>
                                            <%} else { //for Corporate HOLD Booking%>
                                        <%=(returnFlightItinerary.PNR.Split('|').Length > 1 ? returnFlightItinerary.PNR.Split('|')[returnFlightItinerary.Segments[count].Group] : returnFlightItinerary.PNR)%>
                                            <%} %>

                                                  </td>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            
                                                            
   <%if (returnFlightItinerary.FlightBookingSource == BookingSource.Amadeus || returnFlightItinerary.FlightBookingSource == BookingSource.UAPI || (returnFlightItinerary.FlightBookingSource == BookingSource.TBOAir && !(returnFlightItinerary.IsLCC)))
    {%>
                             <%=GetBaggageForGDS(returnFlightItinerary.FlightId,returnFlightItinerary.Passenger[j].Type)%>
    <%}                                                            
 else if (returnFlightItinerary.FlightBookingSource == BookingSource.AirArabia || returnFlightItinerary.FlightBookingSource == BookingSource.FlyDubai || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.Indigo || returnFlightItinerary.FlightBookingSource == BookingSource.PKFares || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || returnFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || returnFlightItinerary.FlightBookingSource == BookingSource.GoAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp || returnFlightItinerary.FlightBookingSource == BookingSource.Jazeera)
    {
        if (returnFlightItinerary.Passenger[j].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(returnFlightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : returnFlightItinerary.Passenger[j].BaggageCode)%>
                                                     <%}
    }
    else if (returnFlightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl && returnFlightItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
                                                       
                                                            <%=(string.IsNullOrEmpty(returnFlightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : returnFlightItinerary.Passenger[j].BaggageCode)%>
                                                        
                                                    <%}
    //Added by lokesh on 22-06-2018
    //For Amadeus Air Source
    //Display baggage information based on the pax type.
    else if (returnFlightItinerary.FlightBookingSource == BookingSource.Amadeus && returnFlightItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
              <%=(string.IsNullOrEmpty(returnFlightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : returnFlightItinerary.Passenger[j].BaggageCode)%>
    <%}

    else if (returnFlightItinerary.FlightBookingSource == BookingSource.TBOAir && (returnFlightItinerary.IsLCC))
    {
        if (returnFlightItinerary.Passenger[j].Type != PassengerType.Infant)
        {
            string strBaggage = string.Empty;
            
            if (!string.IsNullOrEmpty(returnFlightItinerary.Passenger[j].BaggageCode))
            {
                strBaggage = returnFlightItinerary.Passenger[j].BaggageCode;
            }
            else
            {
                strBaggage = "Airline Norms";
            }
                                                           %>
                                                      <%=strBaggage%> 
                                                    <%}
    } %> </td>


                                                        <%if (returnFlightItinerary.FlightBookingSource == BookingSource.AirArabia || returnFlightItinerary.FlightBookingSource == BookingSource.UAPI || returnFlightItinerary.FlightBookingSource == BookingSource.Indigo || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || returnFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || returnFlightItinerary.FlightBookingSource == BookingSource.GoAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{
                                                                string MealDesc = string.Empty;
                                                                %>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (returnFlightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(returnFlightItinerary.Passenger[j].MealDesc)) %>
                                                            <%{
                                                                    MealDesc = returnFlightItinerary.Passenger[j].MealDesc;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Meals"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                       <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                             <%if (returnFlightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(returnFlightItinerary.Passenger[j].Seat.Code)) %>
                                                            <%{
                                                                    MealDesc = returnFlightItinerary.Passenger[j].Seat.Code;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Seats"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <%} %>
                                                      <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f9f7f7; border: 1px solid #ddd; border-collapse: collapse !important; border-color: #dbdbdb !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.42857143; margin: 0; padding: 8px; text-align: left; vertical-align: top; word-wrap: break-word">  <%=(returnFlightItinerary.Segments[count].AirlinePNR == null || returnFlightItinerary.Segments[count].AirlinePNR == "" ? (returnFlightItinerary.PNR.Split('|').Length > 1 ? returnFlightItinerary.PNR.Split('|')[returnFlightItinerary.Segments[count].Group] : returnFlightItinerary.PNR) : returnFlightItinerary.Segments[count].AirlinePNR) %></td>
                                                    </tr>
                                                    <%}
                                                        }%>




            	  		</table>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th>

            	  	                                                                                                                                                                                                                                                                                                                                                                                                                                                    </tr></table></th>	  	
            	  </tr></tbody></table>

    <% }%>





            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">			  
            	  	  <table class="flight-list-table" style="border: 1px solid #f3f3f3; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  	    <tr class="first-row" style="padding: 0; text-align: left; vertical-align: top">
            	  	    	<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 4px; text-align: left; vertical-align: top; word-wrap: break-word;">

            	  	    		<img class="float-left" src="https://ctb2b.cozmotravel.com/images/AirlineLogo/<%=airline.AirlineCode%>.gif" alt="<%=airline.AirlineName%>" style="-ms-interpolation-mode: bicubic; clear: both; display: block; float: left; max-width: 100%; outline: none; padding-right: 5px; text-align: left; text-decoration: none; width: auto">

            					<p class="float-left" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; padding-top: 5px; text-align: left"><%airline.Load(returnFlightItinerary.Segments[count].Airline); %> <%=airline.AirlineName%> <strong><%=returnFlightItinerary.Segments[count].Airline + "-" + returnFlightItinerary.Segments[count].FlightNumber%></strong>
                                           <%try
                                                                                                {
                                                                                                    if ((!returnFlightItinerary.IsLCC && returnFlightItinerary.Segments[count].OperatingCarrier != null && returnFlightItinerary.Segments[count].OperatingCarrier.Length > 0)||(returnFlightItinerary.FlightBookingSource == BookingSource.Indigo && returnFlightItinerary.Segments[count].OperatingCarrier != null && returnFlightItinerary.Segments[count].OperatingCarrier.Length > 0 && returnFlightItinerary.Segments[count].OperatingCarrier != "6E"))////Modified for Indigo CodeShare
                                                                                                    {
                                                                                                        string opCarrier = returnFlightItinerary.Segments[count].OperatingCarrier;
                                                                                                        CT.Core.Airline opAirline = new CT.Core.Airline();
                                                                                                        if (opCarrier.Split('|').Length > 1)
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Split('|')[0]);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Substring(0, 2));
                                                                                                        } %>
                                             (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                                }
                                                catch { } %>
                                    
                                    </p>
            	  	    	</td>
            	  	    	<td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 4px; text-align: left; vertical-align: middle; word-wrap: break-word">
            	  	    		<p class="text-right" style="Margin: 0; Margin-bottom: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: right"><strong> <%=returnFlightItinerary.Segments[count].Origin.CityName%> to <%=returnFlightItinerary.Segments[count].Destination.CityName%></strong> | <strong><%=returnFlightItinerary.Segments[count].DepartureTime.ToString("ddd")+","+returnFlightItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy")%></strong></p>
            	  	    	</td>
            	  	    </tr>
            			<tr style="padding: 0; text-align: left; vertical-align: top">
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word;width:30%">
            					<strong class="city-code" style="font-size: 18px"><%=returnFlightItinerary.Segments[count].Origin.CityCode%></strong> <%=returnFlightItinerary.Segments[count].Origin.CityName%><br>
            					<strong class="time" style="font-size: 14px"><%=returnFlightItinerary.Segments[count].DepartureTime.ToString("HH:mm tt")%></strong><br>
            					<%=returnFlightItinerary.Segments[count].DepartureTime.ToString("ddd")+","+returnFlightItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy")%><br>
            					<%=returnFlightItinerary.Segments[count].Origin.AirportName %><br>
            					Terminal <%=returnFlightItinerary.Segments[count].DepTerminal %>
            				</td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: center; vertical-align: middle; word-wrap: break-word;width:20%">
            				 <center style="min-width: 21px; width: 100%" data-parsed="">
            						<img src="https://ctb2b.cozmotravel.com/images/aircraft_icon.png" alt="Flight Duration" style="-ms-interpolation-mode: bicubic; Margin: 0 auto; clear: both; display: block; float: none; margin: 0 auto; max-width: 100%; min-width: 21px; outline: none; text-align: center; text-decoration: none; vertical-align: middle; width: 21px" align="center" class="float-center">
            						   <%=returnFlightItinerary.Segments[count].Duration.Hours +"hr "+returnFlightItinerary.Segments[count].Duration.Minutes+"m"%>
            				  </center>
            			  </td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; border-right: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word;width:30%">
            					<strong class="city-code" style="font-size: 18px"><%=returnFlightItinerary.Segments[count].Destination.CityCode%></strong> <%=returnFlightItinerary.Segments[count].Destination.CityName%><br> 
            					<strong class="time" style="font-size: 14px"><%=returnFlightItinerary.Segments[count].ArrivalTime.ToString("HH:mm tt")%></strong><br>
            					<%=returnFlightItinerary.Segments[count].ArrivalTime.ToString("ddd")+","+returnFlightItinerary.Segments[count].ArrivalTime.ToString("dd MMM yyyy")%><br>
            					<%=returnFlightItinerary.Segments[count].Destination.AirportName %><br>
            					Terminal  <%=returnFlightItinerary.Segments[count].ArrTerminal %>
            				</td>
            				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 10px; text-align: left; vertical-align: top; word-wrap: break-word;width:20%">
            					<strong>
                                     <%if (returnFlightItinerary.Segments[count].Stops == 0) %>
                    <%{ %>
                    <strong> Non-Stop </strong>
                    <%} %>
                    <%else if (returnFlightItinerary.Segments[count].Stops == 1) %>
                    <%{ %>
                    <strong> Single Stop </strong>
                    <%} %>
                    <%else if (returnFlightItinerary.Segments[count].Stops == 2) %>
                    <%{ %>
                    <strong> Two Stops </strong>
                    <%} %>
                    <%else if (returnFlightItinerary.Segments[count].Stops> 2) %>
                    <%{ %>
                    <strong> Two+ Stops </strong>
                    <%} %>

            					</strong><br>
            					Class | <%=returnFlightItinerary.Segments[count].CabinClass%><br>
                                      <%if (returnFlightItinerary.FlightBookingSource == BookingSource.FlyDubai || returnFlightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.Indigo || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || returnFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || returnFlightItinerary.FlightBookingSource == BookingSource.GoAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp || returnFlightItinerary.FlightBookingSource == BookingSource.Jazeera || returnFlightItinerary.FlightBookingSource == BookingSource.AirArabia)
                                                                                                {%>
            					Fare Type | <%=returnFlightItinerary.Segments[count].SegmentFareType%><br>
                                        <%} %>
            					<strong><%=booking.Status.ToString()%></strong>
            				</td>
            			</tr>










            		</table></th>

  

<%--<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th>--%>


            	  	                                                                                                                                                                                                                                                                                                                                                                                                                                                    </tr></table></th>		  	
            	  </tr></tbody></table>   <%}%>  



            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<h3 style="Margin: 0; Margin-bottom: 10px; color: inherit; font-family: 'Open Sans', Arial, sans-serif; font-size: 15px; font-weight: bold; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left; word-wrap: normal"><%--Price Details--%></h3>
            	  	</th>
                          <th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: right"> <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: #5f5f5f;" id="a1" runat="server"
                                            onclick="return ShowHide('divFare');"  class="showhideDiv"><%--Hide Fare--%></a>	</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>


              
                           
                            <div title="Param" id="div1" runat="server" style="font-size: 12px;">

  <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0, outputVAT = 0,Meal=0,SeatPrice=0,k3Tax=0;

                        if (ticketList != null && ticketList.Count > 0)
                        {
                            List<TaxBreakup> allPaxTaxBreakups = new List<TaxBreakup>();
                            ticketList.ForEach(t =>  t.TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); } ));

                              switch(returnFlightItinerary.FlightBookingSource)
                                  {
                                     case BookingSource.GoAir:
                                     case BookingSource.GoAirCorp:
                                          k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("ST") || t.TaxCode.Contains("CT") || t.TaxCode.Contains("IT")? t.TaxValue : 0);
                                          break;
                                    case BookingSource.UAPI:
                                         k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                         break;
                                    case BookingSource.SpiceJet:
                                    case BookingSource.SpiceJetCorp:
                                    case BookingSource.Indigo:
                                    case BookingSource.IndigoCorp:
                                         k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("GST") ? t.TaxValue : 0);
                                         break;
                                    default:
                                         k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                         break;
                                 }
                            for (int k = 0; k < ticketList.Count; k++)
                            {
                                AirFare += ticketList[k].Price.PublishedFare + ticketList[k].Price.HandlingFeeAmount;
                                Taxes += ticketList[k].Price.Tax + ticketList[k].Price.Markup;
                                if (returnFlightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.OtherCharges + ticketList[k].Price.SServiceFee + ticketList[k].Price.TransactionFee;
                                }
                                Baggage += ticketList[k].Price.BaggageCharge;
                                Meal += ticketList[k].Price.MealCharge;
                                SeatPrice += ticketList[k].Price.SeatPrice;
                                MarkUp += ticketList[k].Price.Markup;
                                Discount += ticketList[k].Price.Discount;
                                AsvAmount += ticketList[k].Price.AsvAmount;
                                outputVAT += ticketList[k].Price.OutputVATAmount;
                            }
                            if (ticketList[0].Price.AsvElement == "BF")
                            {
                                AirFare += AsvAmount;
                            }
                            else if (ticketList[0].Price.AsvElement == "TF")
                            {
                                Taxes += AsvAmount;
                            }
                        }
                        else
                        {
                            for (int k = 0; k < returnFlightItinerary.Passenger.Length; k++)
                            {
                                AirFare += returnFlightItinerary.Passenger[k].Price.PublishedFare + returnFlightItinerary.Passenger[k].Price.HandlingFeeAmount;
                                Taxes += returnFlightItinerary.Passenger[k].Price.Tax + returnFlightItinerary.Passenger[k].Price.Markup;
                                if (returnFlightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                {
                                    Taxes += returnFlightItinerary.Passenger[k].Price.AdditionalTxnFee + returnFlightItinerary.Passenger[k].Price.OtherCharges + returnFlightItinerary.Passenger[k].Price.SServiceFee + returnFlightItinerary.Passenger[k].Price.TransactionFee;
                                }
                                Baggage += returnFlightItinerary.Passenger[k].Price.BaggageCharge;
                                Meal += returnFlightItinerary.Passenger[k].Price.MealCharge;
                                SeatPrice += returnFlightItinerary.Passenger[k].Price.SeatPrice;
                                MarkUp += returnFlightItinerary.Passenger[k].Price.Markup;
                                Discount += returnFlightItinerary.Passenger[k].Price.Discount;
                                AsvAmount += returnFlightItinerary.Passenger[k].Price.AsvAmount;
                                outputVAT += returnFlightItinerary.Passenger[k].Price.OutputVATAmount;
                                if (returnFlightItinerary.Passenger[k].Price.AsvElement == "BF")
                                {
                                    AirFare += AsvAmount;
                                }
                                else if (returnFlightItinerary.Passenger[k].Price.AsvElement == "TF")
                                {
                                    Taxes += AsvAmount;
                                }
                            }
                        }
                                            %>
                                            <% if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.BaseAgent)
                                               { %>

            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            	  	<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            	  		<table class="pricing-table" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Air Fare</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=AirFare.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right"> Taxes & Fees</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word"> <%=(Taxes-k3Tax).ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right"> K3 Tax</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=k3Tax.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>

            	  			</tr>
                    

                          <%if (returnFlightItinerary.FlightBookingSource == BookingSource.AirArabia || returnFlightItinerary.FlightBookingSource == BookingSource.FlyDubai || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.TBOAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp && Baggage > 0 || returnFlightItinerary.IsLCC)
                                                                                    { %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Baggage Fare </th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=Baggage.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>

                                   <%} %>


                                        <%if (returnFlightItinerary != null && (returnFlightItinerary.FlightBookingSource == BookingSource.UAPI || returnFlightItinerary.FlightBookingSource == BookingSource.Indigo || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || returnFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp || returnFlightItinerary.FlightBookingSource == BookingSource.GoAir || returnFlightItinerary.FlightBookingSource == BookingSource.GoAirCorp || returnFlightItinerary.FlightBookingSource == BookingSource.AirArabia)) %>
                                                                                <%{ %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Meal Price</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=Meal.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Seat Price</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">    <%=SeatPrice.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>

     <%} %>


             <%if (Discount > 0)
                                                                                    { %>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">Discount</th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  -<%=Discount.ToString("N" + agency.DecimalValue) %><%=agency.AgentCurrency%></td>
            	  			</tr>
       <%} %>


            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  				
            	  				<th colspan="2" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">
                                                                 <%CT.TicketReceipt.BusinessLayer.LocationMaster locationGST = new CT.TicketReceipt.BusinessLayer.LocationMaster(returnFlightItinerary.LocationId);
                                                                                             if (locationGST.CountryCode == "IN")
                                                                                            {%>
                                                                                        GST
                                                                                        <%}    else
    { %>
                                                                                        VAT
                                                                                        <%} %>
                                                                                           

                                      </th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word">  <%=outputVAT.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%></td>
            	  			</tr>
            	  			<tr style="padding: 0; text-align: left; vertical-align: top">	  
                                 <th valign="middle" width="220px" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right">
            				 	     <strong style="font-size: 11px"> This is an Electronic ticket. Please carry a positive identification for Check in.</strong>
            				     </th>					
            	  				<th width="220px" style="Margin: 0; background-color: #f3f3f3; border: 1px solid #f3f3f3; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right;vertical-align:middle"><strong>Total Air Fare</strong></th>
            	  				<td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border: 1px solid #f3f3f3; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 12px; text-align: right; vertical-align: top; word-wrap: break-word;vertical-align:middle">

                                       <%if (returnFlightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                                                                            { %>
                                                                                        <b><%=Math.Ceiling((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%}
                                                                                         else if (returnFlightItinerary.FlightBookingSource == BookingSource.OffLine) 
                                                                                            { %>
                                                                                        <b><%=((AirFare + Taxes + outputVAT ) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%}

                                                                                            else
                                                                                            { %>
                                                                                        <b><%=((AirFare + Taxes + Baggage + Meal + SeatPrice + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%} %>
            	  				</td>
            	  			</tr>
            	  		</table>
            	  	</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>

     <%}
                                              %>

         </div>

            	    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
             	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">For international flights, you are suggested to arrive at the airport at least 3-4 hours prior to your departure time to check in and go through security. For connecting flights, please confirm at the counter whether not you can check your luggage through to the final destination when you check in.</p>
            			 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">Don’t forget to purchase travel insurance for your visit.Please contact us to purchase travel insurance.</p>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>
            	
            
            
            
            
            
            
            	</td></tr>
                    <tr><td>
                       <% if (returnFlightItinerary != null && returnFlightItinerary.Segments.ToList().All(x => x.Destination.CountryCode == x.Origin.CountryCode) && (returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || returnFlightItinerary.FlightBookingSource == BookingSource.Indigo || returnFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp))
                           { %>
             <%if (returnFlightItinerary.FlightBookingSource == BookingSource.Indigo || returnFlightItinerary.FlightBookingSource == BookingSource.IndigoCorp)
                 { %>
                 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
             	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">
             <strong style="color:red">Important Update: Please read carefully.</strong><br/>
            Due to ongoing COVID-19 situation, for Safe and Seamless journey and as per the implementation of various safety procedure<br/>
            For Safe and Seamless travels of our guests. It is mandatory to do <span style="text-decoration:underline;font-weight:bold"><span style="color:red">“Web Check-in” along with carrying Boarding pass, affixing of baggage label</span> and following all government guidelines while travelling.</span>
            <ul style="font-weight:bold;">
            <li>Web check-in is now mandatory for each passenger for all Domestic flights.</li>
            <li>Mandatory Check-in: Web check-in to be done upto <span style="text-decoration:underline">75 minutes</span> prior from the schedule flight departure of their domestic flight.<br/>
            How to Check In :  Click on the following link which will take you to the Airline Website <a href="https://www.goindigo.in/how-to-check-in.html?linkNav=how-to-check-in_header" target="_blank">https://www.goindigo.in/how-to-check-in.html?linkNav=how-to-check-in_header</a>. Once clicking on the link, it will be redirected to Indigo Airline website Check-in page. Passengers have to mention PNR and Email/Last name in the specified field to proceed with the Check-in process.</li> 
            <li>Passenger Health declaration status and their mobile should have <span style="text-decoration:underline">“Arogya Setu”</span> app which shows Green status.  Mandatory to add <span style="text-decoration:underline">SSR “HCOK”</span> while Web Checkin.</li>
            <%if(agency.AgentCurrency != "INR"){ //Hide only for India Agents%>
            <li>Passengers travelling with Check in luggage, need to add their luggage during web check-in. Per Passenger 1 piece of Checked in Baggage upto 20 kgs will be allowed. Addition to this one small hand luggage max of 7 Kgs per passenger will be allowed.</li>   
                <%} %>
            <li>Carriage of printout of Boarding card and Baggage label which will be sent to the guest on their contact details or can download during web checkin.</li> 
            <li>Also can carry Mobile boarding pass and carry printed or written baggage label, format will be available on Indigo website.</li> 
            <li>Before reaching to the airport, Passenger need to paste the attached Baggage label on the baggage OR guest can write the mandatory fields i.e. PNR, Name of the passenger, Sector and Flight Number on plain paper & paste the same on baggage which should be prominently visible, as airport may not be having any pasting material to place the labels on the baggage.</li>
            <li>Passenger must be wearing mask properly at the airport & throughout the journey.</li> 
            <li>Passenger must sanitize at airports at various points.</li> 
            <li>Maintain Social distancing at the airport as per the markings/signage.</li> 
            <li>For Safety of all customers, Indigo reserves the rights to refuse the carriage of any passenger whose contact details are missing. Also, its mandatory to do the Web Check-in.</li>
            </ul>
            
                    </p>
            			 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left"></p>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>
            <%}
                if (returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJet || returnFlightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
                { %>
                <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
             	  <hr style="margin-left:15px;margin-right:15px">
            	  <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            	  <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top">
            		<th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left; width: 684px"><table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tr style="padding: 0; text-align: left; vertical-align: top"><th style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0; text-align: left">
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left">
                <strong style="color:red">Important Update: Please read carefully.</strong><br/>
                Due to ongoing COVID-19 situation, for Safe and Seamless journey and as per the implementation of various safety procedure<br/>
                For Safe and Seamless travels of our guests. It is mandatory to do <span style="text-decoration:underline;font-weight:bold"><span style="color:red">“Web Check-in” along with carrying Boarding pass, affixing of baggage label</span> and following all government guidelines while travelling.</span>
                <ul style="font-weight:bold;">
                <li>Web check-in is now mandatory for each passenger for all Domestic flights.</li>
                <li>Mandatory Check-in: Web check-in to be done upto <span style="text-decoration:underline">75 minutes</span> prior from the schedule flight departure of their domestic flight.<br/>
                How to Check In :  Click on the following link which will take you to the Airline Website <a href="https://book.spicejet.com/Search.aspx?op=4" target="_blank">https://book.spicejet.com/Search.aspx?op=4 </a>. Once clicking on the link, it will be redirected to Spice Jet Airline website Check-in page. Passengers have to mention PNR and Email/Last name in the specified field to proceed with the Check-in process.</li> 
                <li>Passenger Health declaration status and their mobile should have <span style="text-decoration:underline">“Arogya Setu”</span> app which shows Green status.  Mandatory to add <span style="text-decoration:underline">SSR “HCOK”</span> while Web Checkin.</li>
                <%if(agency.AgentCurrency != "INR"){ //Hide only for India Agents%>
            <li>Passengers travelling with Check in luggage, need to add their luggage during web check-in. Per Passenger 1 piece of Checked in Baggage upto 20 kgs will be allowed. Addition to this one small hand luggage max of 7 Kgs per passenger will be allowed.</li>   
                <%} %>
                <li>Carriage of printout of Boarding card and Baggage label which will be sent to the guest on their contact details or can download during web checkin.</li> 
                <li>Also can carry Mobile boarding pass and carry printed or written baggage label, format will be available on Spice Jet website.</li> 
                <li>Before reaching to the airport, Passenger need to paste the attached Baggage label on the baggage OR guest can write the mandatory fields i.e. PNR, Name of the passenger, Sector and Flight Number on plain paper & paste the same on baggage which should be prominently visible, as airport may not be having any pasting material to place the labels on the baggage.</li>
                <li>Passenger must be wearing mask properly at the airport & throughout the journey.</li> 
                <li>Passenger must sanitize at airports at various points.</li> 
                <li>Maintain Social distancing at the airport as per the markings/signage.</li> 
                <li>For Safety of all customers, Spice Jet reserves the rights to refuse the carriage of any passenger whose contact details are missing. Also, its mandatory to do the Web Check-in.</li>
                </ul></p>
            			 <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
            			<p style="Margin: 0; Margin-bottom: 0; color: #7E7E7E; font-family: 'Open Sans', Arial, sans-serif; font-size: 11px; font-weight: normal; line-height: 1.3; margin: 0; margin-bottom: 0; padding: 0; text-align: left"></p>
            		</th>
<th class="expander" style="Margin: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; line-height: 1.3; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0"></th></tr></table></th>
            	  </tr></tbody></table>
             <%}
                 } %>
                        </td></tr></tbody></table>
            
            </td></tr></table>
            
          </center>
        </td>
      </tr>
    </table>



            	<table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table> 
  
    <!-- prevent Gmail on iOS font size manipulation -->
   <div style="display:none; white-space:nowrap; font:15px courier; line-height:0"> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; </div>
  </div>

                                    <!-- Added by lokesh on 7-June-2018-->
        <!-- Need to display the below note in email for PK Fares -->
         <%if (returnFlightItinerary != null && returnFlightItinerary.FlightBookingSource == BookingSource.PKFares) %>
         <%{ %>
         <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                <span style="color:red">Note: This is just an itinerary. Ticket copy should not be handed over to customer unless ticket number is shown on the ticket.</span>
                    
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <%} %>



                        </div>

       <%} %>
    </div>
   <!------- End:RETURN EMAIL DIV-------------->

   
    <div id="PreLoader" style="display:none">
        <div class="loadingDiv" style="top:230px;">
            <div>
                <img src="images/preloader11.gif" /></div>
            <div style="font-size: 16px; color: #999999">            
                <strong>Awaiting confirmation</strong>               
                </div>
            <div style="font-size: 21px; color: #3060a0">                       
                 <strong style="font-size: 14px; color: black" >do not click the refresh or back button as the transaction may be interrupted or terminated.</strong>
            
                    </div>
        </div>
       <%--<div class="parameterDiv">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td width="50%">
                        <label class="parameterLabel">
                            <strong>Check In:</strong></label>
                        <span id="fromDate"><%=itinerary.StartDate.ToString("dd/MM/yyyy") %></span>
                    </td>
                    <td width="50%">
                        <label class="parameterLabel">
                            <strong>Check out:</strong></label>
                        <span id="toDate"><%=itinerary.EndDate.ToString("dd/MM/yyyy") %></span>
                    </td>
                </tr>
            </table>
        </div>--%>
    </div>

    <%--Script for TBO & PKFare Modal Popup --%>
    <% if (onwardResult != null && (onwardResult.ResultBookingSource == BookingSource.TBOAir || onwardResult.ResultBookingSource == BookingSource.PKFares) )   {  %>
     <script>              
         //Inserting BOOK NOW button into Modal View
         $('#ctl00_cphTransaction_imgBtnPayment').insertBefore('#TBOConfirm [data-dismiss="modal"]');   
         if ($('#ctl00_cphTransaction_btnHold').length) {
             //Inserting HOLD button into Modal View
             $('#ctl00_cphTransaction_btnHold').insertBefore('#TBOConfirm [data-dismiss="modal"]');
             $('#ctl00_cphTransaction_btnHold').hide();
             $('#TBOHoldConfirmBtn').show();
         }
         $('#TBOConfirmBtn').show();

         //Showing Modal for TBO Confirm
          $('#TBOConfirmBtn').click(function () {                 
              if (document.getElementById('<%=chkRules.ClientID %>').checked == true) {   
                  $('#ctl00_cphTransaction_btnHold').hide();
                  $('#ctl00_cphTransaction_imgBtnPayment').show();
                  $('#ctl00_cphTransaction_imgBtnPayment').css('display', '');
                  $('#rules').hide();
                  $('#TBOConfirm').modal('show');
             } else {
                 validate()
             }
          })
         //Showing Modal for TBO Hold
         $('#TBOHoldConfirmBtn').click(function () {             
             if (document.getElementById('<%=chkRules.ClientID %>').checked == true) {                 
                 $('#ctl00_cphTransaction_imgBtnPayment').hide();
                 $('#ctl00_cphTransaction_btnHold').show();
                  $('#rules').hide();
                  $('#TBOConfirm').modal('show');
              } else {
                  validate()
              }
         })
    </script>
    <%  } if (Settings.LoginInfo !=null && Settings.LoginInfo.IsCorporate == "Y" && onwardResult != null && onwardFlightItinerary != null){%>
<script>
<%//TODO: remove if handled in .cs. Kept for temp provision in Live site
    bool autoTicketingAllowed = false, completeBooking = false, isTBOBooking=false;
    if (Settings.LoginInfo.IsCorporate == "Y" && onwardResult.TravelPolicyResult != null && onwardResult.TravelPolicyResult.PolicyBreakingRules != null && onwardResult.TravelPolicyResult.PolicyBreakingRules.ContainsKey("AUTOTICKETING"))
    {
        if (onwardResult.TravelPolicyResult.PolicyBreakingRules["AUTOTICKETING"][0].Split(':')[1].ToLower() == "y")
        {
            autoTicketingAllowed = true;
        }
    }
    else if (Settings.LoginInfo.IsCorporate == "Y" && onwardResult.TravelPolicyResult != null && onwardResult.TravelPolicyResult.PolicyBreakingRules != null && onwardResult.TravelPolicyResult.PolicyBreakingRules.ContainsKey("COMPLETEBOOKING"))
    {
        if (onwardResult.TravelPolicyResult.PolicyBreakingRules["COMPLETEBOOKING"][0].Split(':')[1].ToLower() == "y")
        {
            completeBooking = true;
        }
    }

    isTBOBooking = onwardFlightItinerary.FlightBookingSource == BookingSource.TBOAir ? true : false;

    if (autoTicketingAllowed)
    {
        if (!isTBOBooking)
        {%>
    $('#<%=imgBtnPayment.ClientID%>').show(); //show ticketing option in case of corp booking (Policy: autoTicketing)
    <%}
    else
    {%>
    $('#TBOConfirmBtn').show(); //show ticketing option in case of corp booking for TBOAir (Policy: autoTicketing)
    <%}
    }
    else if (completeBooking)
    {
        if (onwardFlightItinerary != null && !onwardFlightItinerary.IsLCC)
        {
            if (!isTBOBooking)
            {%>         
                 $('#<%=imgBtnPayment.ClientID%>').hide();//Hide ticketing option in case of corp GDS booking (Policy: CompleteBooking)
            <%}
    else
    { %>
                $('#TBOConfirmBtn').hide();//Hide ticketing option in case of corp GDS booking for TBOAir (Policy: CompleteBooking)
        <%}%>
            $('#<%=btnHold.ClientID%>').val("Complete Booking");
        <%}
    else if (onwardFlightItinerary != null && onwardFlightItinerary.IsLCC)
    {%>
        $('#<%=imgBtnPayment.ClientID%>').prop("value","Complete Booking");
        $('#TBOConfirmBtn').prop("value","Complete Booking");
        $('#<%=btnHold.ClientID%>').hide();//Hide Hold option in case of corp LCC booking (Policy: CompleteBooking)
        <%if (isTBOBooking)
        {%> 
            $('#TBOConfirmBtn').show();//show ticketing option in case of corp LCC booking for TBOAir (Policy: CompleteBooking)
        <%}
        else
        {%> 
            $('#<%=imgBtnPayment.ClientID%>').show();//Show ticketing option in case of corp LCC booking (Policy: CompleteBooking)
        <%}
        }
    }%>

    if ($('#<%=lblMessage.ClientID%>').html().includes("You are not allowed to book a Ticket beyond your limit"))
        $('#TBOConfirmBtn').hide();

$('#tblAgentBal').hide();//Hide the agent balance in case of corp booking by default
</script>
<%}%>
    

    <script>
        $(document).ready(function (){                     

           <% if (onOriginalFare > 0 && onCurrentFare > 0 && onOriginalFare != onCurrentFare)
        {%>
                $('#onDiv').show();
                $('#lblOnOriginal').html('Onward Original: ' + <%=onOriginalFare %>);
                $('#lblOnCurrent').html('Changed: ' + <%=onCurrentFare %>);
            <%}
        if (retOriginalFare > 0 && retCurrentFare > 0 && retOriginalFare != retCurrentFare)
        {%>
                $('#retDiv').show();
                $('#lblRetOriginal').html('Return Original: ' + <%=retOriginalFare%>);
                $('#lblRetCurrent').html('Changed: ' + <%=retCurrentFare%>);
            
            <%}
        if ((onOriginalFare > 0 && onCurrentFare > 0 && onOriginalFare != onCurrentFare) || (retOriginalFare > 0 && retCurrentFare > 0 && retOriginalFare != retCurrentFare)) {%>
                $('#BagFareDiff').modal('show');
                $('#<%=imgBtnPayment.ClientID %>').hide();
            <%}%>
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
