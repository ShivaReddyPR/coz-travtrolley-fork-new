﻿<%@ Page Language="C#" MasterPageFile="~/Transaction.master" AutoEventWireup="true" Inherits="TermsUI" Title="" Codebehind="Terms.aspx.cs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
 
 
 
 
 <style> 
 
 .body_container { line-height:24px; font-size:14px; }

 </style>
 <div class="body_container">
 

<div class="col-md-12"> 
 <h4 class=" paddingtop_10 paddingbot_10"> General  Terms of Agreement</h4>





<p><strong>Terms of Use:</strong><br />
  What is this document?</p>
<p>1.1.These terms of use,  read together with the Privacy Policy located at (&quot;Privacy Policy&quot;),  constitutes a legal and binding contract (&quot;Agreement&quot;) between you  and Cozmo Travel L.L.C, a company validly existing under the laws of UAE  (&quot;TravTrolley&quot;) providing, inter alia, the terms that govern your  access to use (i) TravTrolley's website (&quot;Website&quot;), (ii)  TravTrolley's mobile applications (&quot;Mobile Applications&quot;), (iii)  TravTrolley's online travel, accommodation, and allied reservation services,  and (iv) any other service that may be provided by TravTrolley from time to  time (collectively referred to as the &quot;Services&quot;). You hereby agree  and understand that this Agreement is a binding contract between TravTrolley  and any person who accesses, browses, or uses the Services in any manner and  accordingly you hereby agree to be bound by the terms contained in this  Agreement. If you do not agree to the terms contained in this Agreement, you  shall not have the right to use the Services and shall forthwith leave the  Website and stop using the Mobile Applications. The terms contained in this  Agreement shall be accepted without any modification. The use of the Services  would constitute acceptance of the terms of this Agreement.</p>
<p>1.2. You must be 16 (Sixteen)  Lunar years of age or older to register, or visit or use the Services in any  manner. By registering, visiting or using the Services, you hereby represent  and warrant to TravTrolley that you are 16 (Sixteen) lunar years of age or older,  and that you have the right, authority and capacity to use the Services and  agree to and abide by this Agreement. If you are using the Services on behalf  of another organization or entity (&quot;Organization&quot;), then you are  agreeing to be bound by the Agreement on behalf of that Organization and you  represent and warrant that you have the authority to bind the Organization to  this Agreement. In that case, &quot;you&quot; and &quot;your&quot; refers to  you and the concerned Organization.</p>
<p>1.3. This Agreement is  published in compliance of, and is governed by the provisions of UAE laws.</p>
<p>1.4. TravTrolley  authorizes you to view and access the content available on the Services solely  for ordering, receiving, delivering and communicating only as per this  Agreement. The contents of the Services, information, text, graphics, images,  logos, button icons, software code, interface, design and the collection,  arrangement and assembly of content on the Website, Mobile Applications or any  of the other Services (&quot;TravTrolley Content&quot;), are the property of  TravTrolley and are protected under copyright, trademark and other applicable  laws. You shall not modify the TravTrolley Content or reproduce, display,  publicly perform, distribute, or otherwise use the TravTrolley Content in any  way for any public or commercial purpose or for personal gain.</p>
<p>1.5. All rights and  liabilities of TravTrolley with respect to any Services to be provided by  TravTrolley shall be restricted to the scope of this Agreement. In addition to  this Agreement, you shall also ensure that you are in compliance with the terms  and conditions of the third parties, whose links are contained/embedded in the  Services, with whom you choose to transact with. It is hereby clarified that  TravTrolley shall not be held liable for any transaction between you and any  such third party.</p>
<p>1.6. Where there is  contradiction between this Agreement and the conditions mentioned under the  provisions of the Privacy Policy located at<strong>&lt;&lt;&gt;&gt;</strong> . this Agreement shall prevail.</p>
<p><strong><br />
Use of Services</strong></p>
<p>2.1. TravTrolley permits  the viewing, copying, downloading materials available on the Website and the  Mobile Applications provided that the material so obtained is used solely for  personal and non-commercial purposes and such proprietary notices appearing on  the material are reproduced.</p>
<p>2.2. You hereby agree  that you shall not at any time (i) distribute, resell, cross-sell, or permit  access to theServices to any third party, (ii) permit multiple end users to  access the Services using shared login credentials (i.e., a shared email  address and password), (iii) use the Services other than in accordance with (a)  the instructions or documentation which TravTrolley may provide from time to  time, (b) applicable laws and (c) the terms contained in this Agreement.</p>
<p>2.3. TravTrolley may, at  any time and without having to serve any prior notice to you, (i) upgrade,  update, change, modify, or improve the Services or a part of the Services in a  manner it may deem fit, (ii) change any promotion scheme, promotion period,  grace period (by whatever name it is called) and (iii) change the contents of  this Agreement or the Privacy Policy. It is your responsibility, in such cases,  to review the terms of the Agreement from time to time. Such change shall be  made applicable when they are posted. TravTrolley may also alter or remove any  content from the Website or the Mobile Applications without notice and without  liability.</p>
<p>2.4. TravTrolley reserves  the right, at its sole discretion, to suspend your ability to use or access the  Services (or a part of the Services) at any time while TravTrolley investigates  complaints or alleged violations of this Agreement, or for any other reason.  Further, it shall also have the ability to prohibit or restrict you from using  the Services if TravTrolley, in its opinion, feels that you are misusing the  Services in any manner whatsoever.</p>
<p>2.5. You agree to abide  by the terms and conditions of purchase imposed by any third-party supplier  (such as airline companies, hotels, agents, etc.) (&quot;Suppliers&quot;) with  whom you elect to transact by using the Services, including, but not limited  to, payment of all amounts when due and with the Supplier's rules and  restrictions regarding availability, booking, cancelling, rescheduling and use  of fares, products, or services. You understand that any violation of any such  Supplier's rules and restrictions may result in cancellation of your  reservation(s), in your being denied access to the applicable product or  services, in your forfeiting any monies paid for such reservation(s), and/or in  TravTrolley debiting your account for any costs that it incurs as a result of  such violation.</p>
<p>2.6. TravTrolley may,  from time to time, run promotional campaign and contests that require you to  send in material or information about yourself. Each such promotional campaign  and contests has its own rules and regulations, which you must read and agree  to before you participate in the same.</p>
<p>2.7. The use of Services  is governed by TravTrolley's Privacy Policy, available at<strong>&lt;&lt;&gt;&gt;</strong>. TravTrolley's Privacy Policy sets forth its  practices regarding the collection, use and disclosure of personal information  that it obtains about you in connection with the Services.</p>
<p><strong><br />
Payments</strong></p>
<p>3.1. TravTrolley shall  have the right to charge transaction fees based on certain completed  transactions using the Services. These charges/fees may also be altered by  TravTrolley without any notice. You shall be completely responsible for all  charges, fees, duties, taxes, and assessments arising out of the use of the  Services.</p>
<p>3.2. By booking with  Gocozmo, you authorize Gocozmo and its agents to transact with your bank or  other payment gateways to obtain the necessary information required to confirm  payment, collect payment, resolve inquiries and billing disputes, and/or as  otherwise required to manage the booking. Gocozmo does not collect or store  your credit card information. Please refer to our Privacy Policy located at  &lt;&lt;&gt;&gt; to learn more.</p>
<p>3.3You hereby understand and  agree that your reservation/booking is contingent upon TravTrolley receiving  the applicable fees/consideration/fares in its account and unless such monies  have been credited into TravTrolley's account, it shall be under no obligation  to issue you with the relevant tickets, reservation confirmation, passenger  name record (PNR) or such other confirmations in connection with the Services.</p>
<p>3.4. In the event a  booking or reservation does not get confirmed for any reason, TravTrolley is  under no obligation to make another booking to compensate or replace the  earlier booking. All subsequent bookings shall be treated as new transaction  without any reference to the earlier transaction. All refunds shall take place  as per the normal banking payment cycle.</p>
<p>3.5. Payment shall be  made through the payment gateways authorized by TravTrolley. You must adhere to  the terms and conditions that are prescribed by payment gateways through which  you choose to transact. TravTrolley shall not be gateways.</p>
<p><strong><br />
User Covenants.</strong></p>
<p>4.1. TravTrolley hereby  informs you that you are not permitted to host, display, upload, modify,  publish, transmit, update or share any information that: </p>
<p>(i) belongs to another  person and to which you do not have any right; is grossly harmful, harassing,  blasphemous, defamatory, obscene, pornographic, pedophilic, libelous, invasive  of another's privacy, hateful, or racially, ethnically objectionable,  disparaging, relating to or encouraging money laundering or gambling, or  otherwise unlawful in any manner whatsoever;<br />
  (ii) harms minors in any  way;<br />
  (iii) infringes any  patent, trademark, copyright or other proprietary rights; <br />
  (iv) violates any law for  the time being in force; <br />
  (v) deceives or misleads  the addressee about the origin of such messages or communicates any information  which is grossly offensive or menacing in nature; <br />
  (vi) impersonates or  defames another person; <br />
  (vii) contains software  viruses or any other computer code, files or programs designed to interrupt,  destroy or limit the functionality of any computer resource; and <br />
  (viii)threatens the  unity, integrity, defense, security or sovereignty of UAE, friendly relations  with foreign states, or public order or causes incitement to the commission of  any cognizable offence or prevents investigation of any offence or is insulting  to any other nation.</p>
<p>4.2. You are also  prohibited from:</p>
<p>(i) violating or  attempting to violate the integrity or security of the Website, the Mobile  Application or any TravTrolley Content; <br />
  (ii) transmitting any  information on or through the Website and Mobile Applications that is  disruptive or competitive to the provision of Services by TravTrolley; <br />
  (iii) intentionally  submitting on the Website or Mobile Applications any incomplete, false or  inaccurate information;<br />
  (iv) making any  unsolicited communications to other users of the Services; <br />
  (v) using any engine,  software, tool, agent or other device or mechanism (such as spiders, robots,  avatars or intelligent agents) to navigate or search the Website;<br />
  (vi) attempting to  decipher, decompile, disassemble or reverse engineer any part of the Website; <br />
  (vii) copying or  duplicating in any manner any of the TravTrolley Content or other information  available from the Website; and framing or hotlinking or deep linking any  TravTrolley Content.</p>
<p>4.3. TravTrolley, upon  obtaining knowledge by itself or having been brought to actual knowledge by an  affected person in writing or through email about any such information as  mentioned in clause 4.2 above, shall be entitled to disable such information  that is in contravention of clause 4.2, TravTrolley shall be entitled to  preserve such information and associated records for production to governmental  authorities for investigation purposes.</p>
<p>4.4. TravTrolley may disclose  or transfer information provided by you to its affiliates in other countries,  and you hereby consent to such transfer, if such transfer is necessary for the  performance of the lawful contract between TravTrolley or any person on its  behalf and the user or where you have consented to data transfer.</p>
<p><strong><br />
Third party information</strong></p>
<p>5.1.The Website may  provide information regarding third party website(s), affiliates or business  partners and/or contain links to their websites. Such information and links are  provided solely for the purpose of your reference. TravTrolley is not endorsing  the material on the Website, is not responsible for such errors and  representation nor is it associated with it and you shall access these websites  at your own risk. Further, it is up to you to take precautions to ensure that  whatever links you select or software you download, whether from the Website,  Mobile Applications, or other Services, is free of such items such as, but not  limited to, viruses, worms, trojan horses, defects and other items of a  destructive nature.</p>
<p><strong><br />
Visa Obligations.</strong></p>
<p>6.1. Your travel to  foreign countries may be subject to the visa and other immigration related  requirements as maybe prescribed by appropriate authorities from time to time.  You hereby understand and agree that you shall have to procure the applicable  visa and comply with all applicable immigration requirements by yourself and  TravTrolley shall not be under any obligation to inform you or assist you with  obtaining the appropriate visa (including transit-visas, on-entry visas, etc.)  or with the concerned immigration requirements. Further, TravTrolley shall not  be responsible for any issues, including inability to travel, arising due to  your failure to obtain the appropriate visa or clear any immigration  obligations that you may have.</p>

<p><strong><br />Intellectual property  rights</strong></p>
<p>7.1.All the intellectual  property used on the Website by TravTrolley, service providers or any third  party shall remain the property of TravTrolley, service provider or any other  third party as the case may be. Except as provided in the Agreement, the materials  may not be modified, copied, reproduced, distributed, republished, downloaded,  displayed, sold, compiled, posted or transmitted in any form or by any means,  including but not limited to, electronic, mechanical, photocopying, recording  or other means, without the prior express written permission of TravTrolley.</p>

<p><strong><br />
Unlawful or Prohibited  Use</strong></p>
<p>8.1.You warrant to  TravTrolley that you will comply with all applicable laws, statutes, ordinances  and regulations regarding the use of TravTrolley's Services and any other  related activities. You further warrant that you will not use this Website in  any way prohibited by terms contained in this Agreement or under applicable  law.</p>


<p><strong><br />
Liability</strong></p>
<p>9.1. You hereby  acknowledge and agree that TravTrolley provides intermediary services and is  not, and shall not be deemed to be a Supplier, and therefore may not be held  responsible in any way for any lack or deficiency of services provided by any  Suppliers you choose to engage or hire or appoint TravTrolley the Services.  Therefore, TravTrolley is not liable for any errors, omissions,  representations, warranties, breaches or negligence of any of the Suppliers or  for any personal injuries, death, property damage, or other damages or expenses  resulting there from.</p>
<p>9.2. TravTrolley shall  have no liability in the event of any delay, cancellation, overbooking, strike,  force majeure or other causes beyond their direct control, and shall have no  responsibility for any additional expenses incurred by you in connection with  the same.</p>
<p>9.3. You hereby  understand and acknowledge that the fares provided by TravTrolley on the  Website or Mobile Applications are subject to change at the respective  Supplier's discretion and TravTrolley shall not be responsible for any increase/change  in the fares/fees provided by a particular Supplier (and as displayed by  TravTrolley on the Website or Mobile Applications).</p>
<p>9.4. TravTrolley shall  not be liable for any damages whatsoever including, without limitation, damages  for loss of use, data or profits, arising out of or in any way connected with  the inability to use or performance of the Website, Mobile Applications or any  other Service. This shall extend to the loss suffered by you due to delay or  inability to use or access the Website or the Mobile Applications.</p>
<p>9.5. TravTrolley shall  not be responsible or liable to you in any manner for any losses, damage,  injuries or expenses incurred by you as a result of any disclosures made by  TravTrolley, where you have consented to the making of such disclosures. If you  have revoked such consent under the terms of the Privacy Policy, then  TravTrolley shall not be responsible or liable in any manner to you for any  losses, damage, injuries or expenses incurred as a result of any disclosures  made by TravTrolley prior to its actual receipt of such revocation.</p>
<p>9.6.TravTrolley shall not  be responsible to provide any updates on schedules, availability,  cancellations, and modifications to the services provided by the Suppliers.</p>
<p>9.7. The maximum  liability of TravTrolley, in respect of any Services provided, shall be limited  up to a maximum of AED1,000.</p>
<p><strong><br />
Indemnity</strong></p>
<p>10.1.You hereby agree to  indemnify and hold harmless TravTrolley, its affiliates, officers, directors, employees,  consultants, licensors, agents, and representatives from any and all third  party claims, losses, liability, damages, and/or costs (including reasonable  attorney fees and costs) arising from (i) your access to or use of Services,  (ii) violation of the Agreement, (iii) infringement, or infringement by any  other user of your account with TravTrolley, and (iv) infringement of any  intellectual property or other right of any person or entity. TravTrolley will  notify you promptly of any such claim, loss, liability, or demand, and in  addition to your foregoing obligations, you agree to provide us with reasonable  assistance, at your expense, in defending any such claim, loss, liability,  damage, or cost.</p>
<p><strong><br />
Severability</strong></p>
<p>11.1. If any provision of  this Agreement is determined to be invalid or unenforceable in whole or in  part, such invalidity or unenforceability shall attach only to such provision  or part of such provision and the remaining part of such provision and all  other provisions of this Agreement shall continue to be in full force and  effect.</p>
<p><strong><br />
Term and Termination</strong></p>
<p>12.1. This Agreement will  remain in full force and effect while you use any Service in any form or  capacity.</p>
<p>12.2. In the event  TravTrolley discovers or has reasons to believe at any time during or after  receiving a request for Services from you that the request for Services is  either unauthorized or there has been misrepresentation of facts, TravTrolley  shall have the right to take any steps against you, including cancellation of  the bookings, forfeiture of payment etc. without providing you with prior  intimation. TravTrolley shall not be responsible for any damages causes as such  a consequence</p>
<p>12.3. TravTrolley  reserves the right to terminate its Services provided to you in the event of  breach of any terms contained in this Agreement, misrepresentation of  information, any unlawful activity or is unable to verify or authenticate any  information the user submits to TravTrolley.</p>
<p>12.4. Clauses under the  headings Covenants, Liability, Indemnity, Intellectual Property, Dispute  Resolution, and Notices shall continue and survive the termination of this  Agreement.</p>
<p><strong><br />
Dispute Resolution and  Governing Law</strong></p>
<p>13.1. This Agreement and  any contractual obligation between TravTrolley and you will be governed by the  laws of UAE, subject to the exclusive jurisdiction of courts UAE.</p>
<p><strong><br />
Headings</strong></p>
<p>14.1. The headings and  subheadings herein are included for convenience and identification only and are  not intended to describe, interpret, define or limit the scope, extent or  intent of this Agreement, the terms of services or the right to use the Website  by the User contained herein or any other section or pages of the Website in  any manner whatsoever.</p>
<p><strong><br />
Notices</strong></p>
<p>14.2. All notices and  communications shall be in writing, in English and shall deemed given if  delivered personally or by commercial messenger or courier service, or mailed  by registered or certified mail (return receipt requested) or sent TravTrolley  email/ facsimile, with due acknowledgment of complete transmission to the  following address:</p>
<p><strong><br />RULES AND RESTRICTIONS</strong></p>
<ul>
  <li><span dir="ltr"> </span>Until the payment is realized in full, should there be an  increase in airfare or taxes, the same will be intimated and charged to you.</li>
  <li><span dir="ltr"> </span>Until the payment is realized in full, the booking is subject to  cancellation by the airline or TravTrolley without prior notice.</li>
  <li><span dir="ltr"> </span>If the payment is not approved on given credit card number(s),  you will be notified as soon as possible. In such a scenario you need to speak  to the credit card company, or provide alternate credit card number, or change  mode of payment as applicable.</li>
  <li><span dir="ltr"> </span>Should there be any error on the booking/ ticket; you need to  report the same within 24 hours of getting an email from us.</li>
  <li><span dir="ltr"> </span>TravTrolley is not responsible for any schedule change by the  airline after issuance of the ticket, but will inform you of the same if TravTrolley  is informed by the airlines. It is advisable to reconfirm your flight timings  24 hours prior to your flight departure.</li>
  <li><span dir="ltr"> </span>All tickets are subject to date change and cancellation  penalties with exception of some tickets being non-refundable but reusable  later as per the rules of the airline. Partially or half-utilized tickets are  non-refundable.</li>
  <li><span dir="ltr"> </span>All refunds, including refund for NO SHOW shall be governed by  the respective airline&rsquo;s policy.</li>
  <li><span dir="ltr"> </span>In cases where customer is entitled for full refund, a  transaction-processing per ticket will be applicable.</li>
  <li><span dir="ltr"> </span>All special fares are subject to certain rules and regulations  and airlines may restrict upgrade to any higher class. For any clarifications,  please contact the respective airlines.</li>
  <li><span dir="ltr"> </span>Every open ticket has a ticket validity within which the travel  has to be finished, and date change on the ticket can be done only as per the  validity of ticket. Should you require further details on this, kindly contact  us through our live chat.</li>
  <li><span dir="ltr"> </span>Frequent flier miles will be as on the sole discretion of  respective airlines. TravTrolley will not be responsible for any mileage  guarantee.</li>
  <li><span dir="ltr"> </span>Infant is defined as a traveler whose age must be under 24  months throughout the entire journey. If the infant is 24 months or above on  the return journey then the infant fare would not be applicable and you will  need to buy the ticket under applicable child fare.</li>
  <li><span dir="ltr"> </span>A maximum of one infant is allowed to travel with each adult  passenger. In case of more than one infants travelling with one adult, first  infant would be charged the infant fare but for the second infant applicable  child fare would be charged.</li>
  <li><span dir="ltr"> </span>Few airlines do not allow children to travel alone  (Unaccompanied Minors) or with an accompanying young adult (between 12 - 16  years of age). Before making any such booking you are requested to have it  checked either with our customer care or with respective airlines.</li>
  <li><span dir="ltr"> </span>You are requested to contact our customer care for your date  change and cancellation requests. We will assist you as per your requirements  but still there might be few cases where we will request you to contact the  airlines directly.</li>
  <li><span dir="ltr"> </span>In addition to the airlines date change penalty, TravTrolley  charges a date change assistance service charge per passenger.</li>
  <li><span dir="ltr"> </span>In addition to the stipulated airline cancellation fee, all  airlines will also charge an RAF (Refund Admin Fee) per passenger per ticket.</li>
  <li><span dir="ltr"> </span>TravTrolley will charge a service fee per passenger for all  cancellations.</li>
  <li><span dir="ltr"> </span>All bookings booked on TravTrolley are subject to cancellation  penalty levied by the airlines.</li>
  <li><span dir="ltr"> </span>In case of full cancellation, refunds will be usually processed  within 7 business days while in case of partial cancellation, refunds might  take up to three months&rsquo; time to be processed.</li>
  <li><span dir="ltr"> </span>There are times when we are unable to confirm a reservation. In  the rare event that this occurs, we will attempt to reach you by phone and  email so that we can rebook you. You must call us back within 48 hours to  ensure that your booking is confirmed by us. Any change in the fare in the  meantime shall be charged to you.</li>
  <li><span dir="ltr"> </span>Before the commencement of your air travel, please check that  you possess all the valid documents required for travel: passport, air tickets,  valid VISA, health certificates, travel insurance, etc. Contact consulate/  airline for further details.</li>
  <li><span dir="ltr"> </span>In case of cancellation, amount paid for insurance will be  non-refundable, this is applicable only in case if you have booked insurance  along with ticket.</li>
  <li><span dir="ltr"> </span>You are requested to consult consulate/airlines to check the  visa requirement for your travel. TravTrolley will not be responsible for any  visa related information.</li>
</ul>



 
  </div>
  
  
  <div class="clearfix"> </div>
 
 </div>
 
</asp:Content>

