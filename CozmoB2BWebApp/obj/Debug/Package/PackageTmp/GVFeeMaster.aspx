<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="GVFeeMasterUI" Title="Global Visa Fee Master" Codebehind="GVFeeMaster.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="dc" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <center>
        <table cellpadding="0"  cellspacing="2">
            <tr>
                <td style="height: 10px">
                </td>
            </tr>
            <tr>
                <td>
                    <table class="trpad" width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td align="right" height="23">
                                <asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label><span style="color:red">*:</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlCountry" CssClass="inputDdlEnabled form-control" runat="server" Width="154px" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged" AutoPostBack="true">
                                </asp:DropDownList>
                            </td>
                            <td align="right">
                                <asp:Label ID="lblNationality" runat="server" Text="Nationality"></asp:Label><span style="color:red">*:</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlNationality" CssClass="inputDdlEnabled form-control" runat="server" Width="154px">
                                </asp:DropDownList>
                            </td>
                           <td align="right" height="23">
                                <asp:Label ID="lblResidence" runat="server" Text="Residence"></asp:Label><span style="color:red">*:</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlResidence" CssClass="inputDdlEnabled form-control" runat="server" Width="154px">
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                             <td align="right">
                                <asp:Label ID="lblVisaType" runat="server" Text="Visa Type"> </asp:Label><span style="color:red">*:</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlVisaType" CssClass="inputDdlEnabled form-control" runat="server" Width="154px">
                                </asp:DropDownList>
                            </td>
                           
                            <td align="right">
                                <asp:Label ID="lblVisaCategory" runat="server" Text="Visa Category"></asp:Label><span style="color:red">*:</span>
                            </td>
                            <td>
                                <asp:DropDownList ID="ddlVisaCategory" CssClass="inputDdlEnabled form-control" runat="server"
                                    Width="154px">
                                </asp:DropDownList>
                            </td>
                            <td>
                                <asp:Label ID="lblProcessingDays" runat="server" Text="Processing Days" /><span style="color:red">*:</span>
                            </td>
                            <td>
                                <asp:TextBox ID="txtProcessing" CssClass="form-control inputDdlEnabled" runat="server" Width="154px" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td style="height: 20px">
                </td>
            </tr>
            <tr> 
            <td> 
            <table> 
            <tr> 
            <td valign="top">
            <table>
                <tr>
                    <td height="23">

                    </td>
                    <td colspan="5">
                        <table>
                            <tr>
                                <td>
                                    <center> 
                                        <asp:Label Style="font-weight: bold" Width="60px"  ID="lblAdults" Text="Adults" runat="server">
                                        </asp:Label>
                                    </center>
                                </td>
                                <td >
                                    <center>
                                        <asp:Label Style="font-weight: bold"  Width="60px" ID="lblChild" Text="Child" runat="server">
                                        </asp:Label>
                                    </center>
                                </td>
                                <td>
                                    <center>
                                         <asp:Label ID="lblInfants" Style="font-weight: bold" Width="60px"  Text="Infants" runat="server" >
                                         </asp:Label>
                                    </center>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td align="right" height="23">
                        <asp:Label ID="lblVisaFee" runat="server" Text="Visa Fee"></asp:Label><span style="color:red">*:</span>
                    </td>
                    <td colspan="5">
                        <table>
                            <tr>
                                <td>
                                    <asp:TextBox ID="txtAdultsVisaFee" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>

                                </td>
                                <td>
                                    <asp:TextBox ID="txtChildVisaFee" onkeypress="return restrictNumeric(this.id,'2');"
                                                    runat="server" Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>

                                </td>
                                <td align="left">
                                                <asp:TextBox ID="txtInfantVisaFee" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>

                                </td>
                            </tr>
                        </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" height="23">
                                    <asp:Label ID="lblVisaService" runat="server" Text="Service Charge"></asp:Label><span style="color:red">*:</span>
                                </td>
                                <td colspan="5">
                                    <table>
                                       
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="txtAdultsService" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtChildService" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtInfantsService" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" height="23">
                                    <asp:Label ID="Label4" runat="server" Text="Urgent Fee"></asp:Label><span style="color:red">*:</span>
                                </td>
                                <td colspan="5">
                                    <table>
                                        <tr>
                                            <td align="left">
                                                <asp:TextBox ID="txtAdultUrgFee" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtChildUrgFee" onkeypress="return restrictNumeric(this.id,'2');"
                                                    runat="server" Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtInfantUrgFee" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" height="23">
                                    <asp:Label ID="Label5" runat="server" Text="Add2. Charge"></asp:Label><span style="color:red">*:</span>
                                </td>
                                <td colspan="5">
                                    <table>
                                        <tr>
                                            <td align="left">
                                                <asp:TextBox ID="txtAdultsAdd2Fee" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtChildAdd2Fee" onkeypress="return restrictNumeric(this.id,'2');"
                                                    runat="server" Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtInfantAdd2Fee" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td height="23">
                                    <asp:Label ID="Label9" runat="server" Text="Appointment Charges"></asp:Label><span style="color:red">*:</span>
                                </td>
                                <td colspan="5">
                                    <table>
                                        <tr>
                                            <td align="left">
                                                <asp:TextBox ID="txtAdultsAdd1Fee" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtChildAdd1Fee" onkeypress="return restrictNumeric(this.id,'2');"
                                                    runat="server" Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtInfantAdd1Fee" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td align="right" height="23">
                                    <asp:Label ID="lblCourierFee" runat="server" Text="Courier Charge"></asp:Label><span style="color:red">*:</span>
                                </td>
                                <td colspan="5">
                                    <table>
                                        <tr>
                                            <td align="left">
                                                <asp:TextBox ID="txtAdultCourierFee" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtChildCourierFee" onkeypress="return restrictNumeric(this.id,'2');"
                                                    runat="server" Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                            <td align="left">
                                                <asp:TextBox ID="txtInfantCourierFee" runat="server" onkeypress="return restrictNumeric(this.id,'2');"
                                                    Width="60px" CssClass="inputEnabled" onfocus="Check(this.id);" onblur="Set(this.id);"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </tr>
                           
                           
                           </table> 
             </td>
            </tr>
            </table>
            </td>
            </tr>
            <tr>
                <td>
                    <center>
                        <div style=" text-align:right" class="pad20">
                            <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn but_b" OnClientClick="return Save();" OnClick="btnSave_Click" />
                            <asp:Button ID="btnCancel" runat="server" Text="Clear" CssClass="btn but_b" OnClick="btnCancel_Click" />
                            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn but_b" OnClick="btnSearch_Click" />
                            <asp:Button ID="btnDelete" Text="Delete" runat="server" CssClass="btn but_b" Visible="false" OnClick="btnDelete_Click" />
                            <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>
                            <asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError"></asp:Label>
                        </div>
                    </center>
                </td>
            </tr>
        </table>
    </center>

    <script type="text/javascript">
        function validateDtlAddUpdate(action, id) {
            try {
                var msg = '';
                var rowId = id.substring(0, id.lastIndexOf('_'));

                if (action == 'I') {

                    if (document.getElementById(rowId + '_FTtxtFeeCode').value == '') msg += 'Please Enter Fee Code! \n';
                }
                else if (action == 'U') {

                    if (document.getElementById(rowId + '_EITtxtFeeCode').value == '') msg += 'Please Enter Fee Code!\n';
                }
                if (msg != '') {
                    alert(msg);
                    return false;
                }
                return true;
            }
            catch (e) {
                return true;
            }
        }


        function Save() {

            //var dd=GetDateObject('ctl00_cphTransaction_dcPaxPspIssueDate');

            // alert(document.getElementById('ctl00_cphTransaction_dcSubmissionFrom_Time').value);

            //var dd=GetDateObject('ctl00_cphTransaction_dcSubmissionFrom');
            // alert(GetDateObject('ctl00_cphTransaction_dcSubmissionFrom'));
            //alert(Document.('ctl00_cphTransaction_dcSubmissionFrom_Date_Time').value)


            if (parseFloat(getElement('txtAdultsVisaFee').value) == 0 || getElement('txtAdultsVisaFee').value == '') addMessage('Adult VisaFee cannot be blank/zero!', '');
            //if(getElement('txtChildVisaFee').value==0) addMessage('Child Visa Fee cannot be blank!','');        
            //if(getElement('txtInfantVisaFee').value==0) addMessage('Infant Visa Fee cannot be blank!','');        

            if (getElement('ddlCountry').selectedIndex <= 0) addMessage('Please Select Country from the List!', '');
            if (getElement('ddlNationality').selectedIndex <= 0) addMessage('Please Select Nationality from the List!', '');
            if (getElement('ddlVisaType').selectedIndex <= 0) addMessage('Please Select VisaType from the List!', '');
            if (getElement('ddlResidence').selectedIndex <= 0) addMessage('Please Select Residence from the List!', '');
            if (getElement('ddlVisaCategory').selectedIndex <= 0) addMessage('Please Select Visa Category from the List!', '');
            //if (getElement('ddlCity').selectedIndex <= 0) addMessage('Please Select City from the List!', '');

            //if(getElement('ddlPresenceStatus').selectedIndex<=0) addMessage('Please Select Presence Status from the List!','');
            //if (getElement('hdfOBVisaFeeMode').value != "0") addMessage('OB Visa Fee Details in Edit Mode!', '');

            if (getMessage() != '') {
                alert(getMessage()); clearMessage(); return false;
            }

        }
        function setValue(id, action) {
            var rowId = id.substring(0, id.lastIndexOf('_'));
            var adults = 0;
            var child = 0;
            var infant = 0;
            if (action == "I") {
                adults = parseFloat(document.getElementById(rowId + '_FTtxtAdults').value);
                document.getElementById(rowId + '_FTtxtAdults').value = adults.toFixed(2);
                child = parseFloat(document.getElementById(rowId + '_FTtxtChild').value);
                document.getElementById(rowId + '_FTtxtChild').value = child.toFixed(2);
                infant = parseFloat(document.getElementById(rowId + '_FTtxtInfants').value);
                document.getElementById(rowId + '_FTtxtInfants').value = infant.toFixed(2);
            }
            else if (action == "U") {
                adults = parseFloat(document.getElementById(rowId + '_EITtxtAdults').value);
                document.getElementById(rowId + '_EITtxtAdults').value = adults.toFixed(2);
                child = parseFloat(document.getElementById(rowId + '_EITtxtChild').value);
                document.getElementById(rowId + '_EITtxtChild').value = child.toFixed(2);
                infant = parseFloat(document.getElementById(rowId + '_EITtxtInfants').value);
                document.getElementById(rowId + '_EITtxtInfants').value = infant.toFixed(2);
            }
        }
        function Check(id) {
            var visaFeeAdult = 0;
            visaFeeAdult = parseFloat(document.getElementById(id).value);
            document.getElementById(id).value = visaFeeAdult.toFixed(2);
        }
        function Set(id) {
            var visaFeeAdult = 0;
            visaFeeAdult = parseFloat(document.getElementById(id).value);
            document.getElementById(id).value = visaFeeAdult.toFixed(2);
        }
    </script>

</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
    <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="charge_id"
        EmptyDataText="No Requirements List!" AutoGenerateColumns="false" PageSize="10"
        GridLines="none" CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged"
        CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">
        <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                ControlStyle-CssClass="" ShowSelectButton="True" />
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtCountry" Width="150px" CssClass="inputEnabled" HeaderText="Country"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <%-- <ItemStyle HorizontalAlign="left" />--%>
                <ItemTemplate>
                    <asp:Label ID="ITlblCountry" runat="server" Text='<%# Eval("charge_country_name") %>'
                        CssClass="grdof" ToolTip='<%# Eval("charge_country_name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtNationality" Width="150px" CssClass="inputEnabled" HeaderText="Nationality"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblNationality" runat="server" Text='<%# Eval("charge_nationality_name") %>'
                        CssClass="grdof" ToolTip='<%# Eval("charge_nationality_name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTxtVisaType" Width="100px" CssClass="inputEnabled" HeaderText="Visa Type"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITlblVisaType" runat="server" Text='<%# Eval("charge_visa_type_name") %>'
                        CssClass="grdof" ToolTip='<%# Eval("charge_visa_type_name") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtResidence" Width="100px" CssClass="inputEnabled" HeaderText="Residence"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITresiName" runat="server" Text='<%# Eval("charge_residence_name") %>'
                        CssClass="grdof" ToolTip='<%# Eval("charge_residence_name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtVisaCategory" Width="100px" HeaderText="Visa Category" CssClass="inputEnabled"
                        OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblVisaCategory" runat="server" Text='<%# Eval("charge_visa_category_name") %>'
                        CssClass="grdof" ToolTip='<%# Eval("charge_visa_category_name") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
</asp:Content>
