﻿<%@ Page Language="C#" AutoEventWireup="true" EnableEventValidation="false" Inherits="PrintSightseeingVoucherGUI" Codebehind="PrintSightseeingVoucher.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />

    <title>Sightseeing Voucher</title>
    
    
    
              <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

  <!-- manual css -->
    <link href="css/override.css" rel="stylesheet">


<!-- Bootstrap Core JavaScript -->
<script src="Scripts/bootstrap.min.js"></script>



</head>
<body>
    <form id="form1" runat="server">
    
        <script>
            function printPage() {
                document.getElementById('btnPrint').style.display = "none";
                window.print();
                //alert('2');
                setTimeout('showButtons()', 1000);
                // alert('3');
                return false;
            }
            function showButtons() {
                document.getElementById('btnPrint').style.display = "block";
            }
//        function printDiv(divName) {
//            var printContents = document.getElementById(divName).innerHTML;
//            var originalContents = document.body.innerHTML;

//            document.body.innerHTML = printContents;

//            window.print();

//            document.body.innerHTML = originalContents;
//        }
        function Validate() {
            var isValid = true;
            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
            document.getElementById('err').style.display = 'none';
            if (document.getElementById('<%#txtEmailId.ClientID %>').value.length <= 0) {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Email address";
                isValid = false;
            }
            else if (reg.test(document.getElementById('<%#txtEmailId.ClientID %>').value)) {
                document.getElementById('err').style.display = 'none';
            }
            else {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Correct Email";
                isValid = false;
            }
            if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
        }
        function ShowPopUp(id) {
            document.getElementById('txtEmailId').value = "";
            document.getElementById('err').style.display = 'none';
            document.getElementById('emailBlock').style.display = "block";
            //            var positions = getRelativePositions(document.getElementById(id));
            //            document.getElementById('emailBlock').style.left = (530) + 'px';
            //            document.getElementById('emailBlock').style.top = (400) + 'px';
            return false;
        }
        function HidePopUp() {
            document.getElementById('emailBlock').style.display = "none";
        }
        
    </script>
<div class="col-md-12">
    <div class="col-md-6"><asp:Image ID="imgHeaderLogo" runat="server" Visible="false" /></div>
 <div id="emailBlock" class="showmsg" style="position:absolute;display:none; left:400px;top:200px;width:300px;">
          <div class="showMsgHeading"> Enter Your Email Address</div>
          
          <a style=" position:absolute; right:5px; top:3px;" onclick="return HidePopUp();" href="#" class="closex"> X</a>
           <div class="padding-5"> 
           <div style=" background:#fff">
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
                       
                           <tr>
                           <td height="40" align="center">
                            <b style="display: none; color:Red" id="err"></b>
                           <asp:TextBox style=" border: solid 1px #ccc; width:90%; padding:2px;"  ID="txtEmailId" runat="server" CausesValidation="True" ></asp:TextBox>
                           </td>
                           </tr>
                           <tr>
                           
                           <td height="40" align="center">
                           <asp:Button CssClass="button-normal" ID="btnEmailVoucher" runat="server" Text="Send Email" OnClientClick="return Validate()" OnClick="btnEmailVoucher_Click" />
                           </td>
                       </tr>
                   </table>
            </div>
           
           
           
             </div>
           
           
               
           </div>
    <div class="" id="printableArea" runat="server">
     
     
            
               <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                    <tbody>
                        <tr>
                            <td>
                            
                            
                            
    <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-4" style='margin-top:5px;'><img src="<%=logoPath %>" width='180px' height='60px'/ ></div>  <%--<div style='margin-top:5px;'><img src="<%=logoPath %>" width='180px' height='60px'/></div>--%>
    <div class="col-md-8"> 
    
    
   <label class="pull-right mar10">  <asp:Button ID="btnPrint" runat="server" Text="Print Voucher" OnClientClick="return printPage();" /></label>
    
      <label class="pull-right mar10">  <asp:Button ID="btnEmail" runat="server" Text="Email Voucher" OnClientClick="return ShowPopUp(this.id);"/></label>
    
    
    </div>




    <div class="clearfix"></div>
    </div>
    
    
    
    
                            
                            
                                
                            </td>
                          
                        </tr>
                        
                        
                        <tr> 
                        <td><h3> <center>  Sightseeing Voucher</center></h3> </td>
                        
                        </tr>
                        
                         <tr><td height="10"></td></tr>
                        <tr>
                            <td>
                            
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse">
                                    <tbody>
                                   
                                    <tr>
                                    <td class="themecol1" colspan="2" >
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Voucher Details</label>
                            </td>
                                    </tr>
                                        <tr>
                                            <td  height="25">
                                                <strong>Booking Reference No:</strong>
                                                <asp:Label ID="lblConfirmation" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                        <tr>
                                            <td>
                                                <strong>Printed On:</strong>
                                                <asp:Label ID="lblBookingDate" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <strong>Itinerary Number:</strong>
                                                <asp:Label ID="lblBookingRef" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                        <tr>
                                            <td>
                                                <strong>Booked By : </strong>
                                                <asp:Label ID="lblBookedBy" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Booking Status :</strong>
                                                
                                            <asp:Label ID="lblBookingStatus" runat="server" Text=""></asp:Label></td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                         <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                           <td class="themecol1" colspan="2">
                               <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Service Provider Details</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse" class="pd15 padd10 ">
                                    <tbody>
                                        <tr>
                                           <td width="50%" height="20" valign="top">
                                                <strong>Supplier Name :</strong> 
                                                <asp:Label ID="lblSupplierName" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                         <tr>
                                           <td width="50%" height="20" valign="top">
                                                <strong>Supplier Address :</strong> 
                                                <asp:Label ID="lblServiceName" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                           <td width="50%" height="20" valign="top">
                                                <strong>Telephone :</strong> 
                                                <asp:Label ID="lblTelphone" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <%--<tr>
                                           <td width="50%" height="20" valign="top">
                                                <strong>Email :</strong> 
                                                <asp:Label ID="lblEmail" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                    </tbody>
                                    </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td class="themecol1">
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Passenger Details</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse" class="pd15 padd10 ">
                                    <tbody>
                                        <tr>
                                            <td width="50%" height="20" valign="top">
                                                <strong>Passenger Name:</strong> 
                                                <asp:Label ID="lblGuestName" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                        <tr>
                                            <td width="50%">
                                                <strong>Service Type :</strong> 
                                                <asp:Label ID="lblServiceType" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td height="20" valign="top">
                                                <strong>City :</strong> 
                                                <asp:Label ID="lblCity" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                        <tr>
                                            <td>
                                                <strong>Supplier Reference:</strong> 
                                                <asp:Label ID="lblSupplierInfo" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="top">
                                                <strong>Tour Date:</strong>: 
                                                <asp:Label ID="lblTransactionDate" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                        <tr>
                                            <td>
                                                <strong>Tour Time:</strong> 
                                                <asp:Label ID="lblPickupTime" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="top">
                                                <strong>Tour Type:</strong> : 
                                                <asp:Label ID="lblTourName" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>
                                        <%--<tr>
                                            <td>
                                                <strong>Pick up Location:</strong> 
                                                <asp:Label ID="lblPickupPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                       <%-- <tr>
                                            <td height="20" valign="top">
                                                <strong>Starting Point:</strong> 
                                                <asp:Label ID="lblStartingPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                            </tr>--%>
                                        <%--<tr>
                                             <td height="20" valign="top">
                                                <strong>Ending Point:</strong> 
                                                <asp:Label ID="lblEndingPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                         <tr>
                                             <td height="20" valign="top">
                                                <strong>Guest Pickup Point:</strong> 
                                                <asp:Label ID="lblPickPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <strong>Additional Requests:</strong>  
                                                <asp:Label ID="lblAdditional" runat="server" Text=""></asp:Label>
                                            </td>
                                           
                                        </tr>
                                        <%--<tr>
                                            <td height="20" valign="top">
                                                <strong>Tour Address :</strong> 
                                                <asp:Label ID="lblTourAddress" runat="server" Text=""></asp:Label>
                                            </td>
                                             <td valign="top" style="height: 20px">
                                                <strong>Tour Tel number :</strong> 
                                                <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label>
                                            </td>
                                            
                                        </tr>--%>
                                       <%-- <tr>
                                           
                                            <td style="height: 20px">
                                                <strong>Entrance Fees :</strong> <%=agent.AgentCurrency%> 
                                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Special Name :</strong> 
                                                <asp:Label ID="lblSpecialName" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                        <%-- <tr>
                                        <td>
                                                <strong>Pick up point :</strong> 
                                                <asp:Label ID="lblPickupPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                           
                                            <td >
                                                <strong>Pick up Time :</strong> 
                                                <asp:Label ID="lblPickupTime" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>--%>
                                       
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        
                      <%
                           if (itinerary != null)
                           {
                               if (!string.IsNullOrEmpty(itinerary.CancellationPolicy))
                               {
                                   string[] GetData = itinerary.CancellationPolicy.Split('@');
                                   string[] cancelData = GetData[0].Split('|');
                                    string[] amendData = new string[0]; 
                                    if (GetData.Length > 1)
                                    {
                                       amendData= GetData[1].Split('|');
                                    }
                           %>
                        <tr>
                            <td class="themecol1">
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Cancellation & Charges
                                </label>
                            </td>
                        </tr>
                         <tr>
                            <td  style="padding-top:5px;">
                                <%for (int i = 0; i < cancelData.Length; i++)
                                  { %>
                                    <p  style="padding-left:5px;"> <%=cancelData[i]%></p>
                                    
                                    <%} %>
                           
                               <h5>
                                   <span style="padding-left: 5px;">Amendment Policy :</span></h5>
                               <%for (int j = 0; j < amendData.Length; j++)
                                 { %>
                               <p style="padding-left: 5px;">
                                   <%=amendData[j]%></p>
                               <%} %>
                           </td>
                       </tr>
                       <%} %>
                       <tr><td><br /></td></tr>
                       <%
                               /*Condition for Notes 12/03/2016*/
                           if (!string.IsNullOrEmpty(itinerary.Note))
                           {
                               string noteInfo = itinerary.Note.Split('#')[0];
                        %>
                        <tr>
                            <td class="themecol1">
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Notes &amp; Additional Information</label>
                            </td>
                        </tr>
                        <tr>
                        <td  style="padding-top:5px;">
                        <p style="padding-left:5px;"><%=noteInfo%></p>
                        </td>
                        </tr>
                        
                        <%
                               /*condition For Additional Information 12/03/2016*/
                           if (itinerary.Note.Contains("#"))
                           {
                               string[] addInfo = itinerary.Note.Split('#')[1].Split('|');

                               foreach (string ad in addInfo)
                               {
                                          %>
                        <tr>
                        <td style="padding-top:5px;">
                        <span style="padding-left:5px;"><%=ad%></span><br />
                        </td>
                        </tr>
                        <%}
                           }
                           }%>
                         <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                         <%
                           }%>
                        <%
                               
                           if (!string.IsNullOrEmpty(itinerary.DepPointInfo))
                           {
                               
                        %>
                        <tr>
                            <td class="themecol1">
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Pcikup Locatiom &amp; Pickup Time</label>
                            </td>
                        </tr>
                        <tr>
                        <td  style="padding-top:5px;">
                             <%string[] aData = itinerary.DepPointInfo.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries);
                                       foreach (string depoint in aData)
                                       {%>
                                                <li>
                                                    <%=depoint%></li>
                                                <%}
                                                %>
                        </td>
                        </tr>
                         <%
                           }%>
                     
                            <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <%
                            /*Condition for Notes 12/03/2016*/
                            if (!string.IsNullOrEmpty(itinerary.PickupPoint))
                            {
                               
                        %>
                        <tr>
                            <td class="themecol1">
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                   Guest PickUp Point</label>
                            </td>
                        </tr>
                        <tr>
                        <td  style="padding-top:5px;">
                        <p style="padding-left:5px;"><%=itinerary.PickupPoint%></p>
                        </td>
                        </tr>
                        <%} %>
                        <tr>
                            <td height="40" bgcolor="" align="center">
                                <div style="border: solid 1px #ccc; padding: 10px; text-align: center">
                                    Cozmo Holidays Tel. No. :00971600524444
                                    <br />
                                    Email : <a style="color: #ff7800; text-decoration: none;" href="mailto:ziyad@cozmotravel.com">
                                        info@cozmotravel.com</a>
                                </div>
                            </td>
                        </tr>
                        <tr>
                          <td >
                        <center>
                        <%--<div>Booked and payable by Gullivers Travel Associates <br/> Only Payment For Extras To Be Collected From The Client.</div>--%>
                        </center>
                        </td>
                        </tr>
                    </tbody>
                </table>
         </div>
        
        <div class="clearfix"></div>
            
    </div>
   
    </form>
</body>
</html>
