﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpTypeMaster.aspx.cs" Inherits="CozmoB2BWebApp.ExpTypeMaster" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <link href="css/toastr.css" rel="stylesheet" type="text/css" />
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>

    <script>

        /* Global variables */
        var apiHost = ''; var cntrllerPath = 'api/expenseMasters'; var cntrlrPath = 'api/CommonMasters';
        var apiAgentInfo = {}; var expRepId = 0; var selectedRows = [];
        var costCenterData = {}; var categoriesData = {};
        var flag = ''; var uomData = {};
        var ET_Id = 0;
        var agentdata = {}; var selectedProfile = 0;

        /* Page Load */
        $(document).ready(function () {
            /* Check query string to see if existing report needs to open */
            expRepId = '<%=Request.QueryString["ExpRepId"] == null ? "0" : Request.QueryString["ExpRepId"]%>';

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {
                alert('Session expired, please login once again.');
                return;
            }

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            /* To get screen dropdown list data */
            GetScreenData();
        });

        /* To get agent and login info */
        function GetAgentInfo() {
            try {
                var loginInfo = '<%=Settings.LoginInfo == null%>';
                if (loginInfo == true)
                    return apiAgentInfo;

                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {
            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';
            if (IsEmpty(apiHost))
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";
            return apiHost;
        }

        /* To get agent and cost center */
        function GetScreenData() {
            var reqData = { AgentInfo: apiAgentInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getTypesScreenLoadData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);
        }

        /* To bind agent and cost center */
        function BindScreenData(screenData) {
            agentdata = {};
            if (!IsEmpty(screenData.dtAgents)) {

                agentdata = screenData.dtAgents;
                var options = agentdata.length != 1 ? GetddlOption('', '--Select Agent--') : '';
                $.each(agentdata, function (key, col) {

                    options += GetddlOption(col.agenT_ID, col.agenT_NAME);
                });
                $('#ddlAgentId').empty();
                $('#ddlAgentId').append(options);
                $("#ddlAgentId").select2("val", '');
                if (agentdata.length == 1) {

                    $('#ddlAgentId').attr('disabled', 'disabled');
                    $("#ddlAgentId").select2("val", agentdata[0].agenT_ID);
                }
            }           
            if (!IsEmpty(screenData.dtCostCenter))
                BindCostCenterData(screenData.dtCostCenter);
            if (!IsEmpty(screenData.dtCategories))
                BindCategoriesData(screenData.dtCategories);
            if (!IsEmpty(screenData.dtUOM))
                BindUOMData(screenData.dtUOM);
        }

        /* To bind cost center data to costcenter control */
        function BindCostCenterData(ccData) {
            costCenterData = {};
            if (IsEmpty(ccData)) {
                $('#ddlCostCenter').empty();
                $("#ddlCostCenter").select2("val", '');
                return;
            }
            costCenterData = ccData;
            var options = costCenterData.length != 1 ? GetddlOption('', '--Select Cost Center--') : '';
            $.each(costCenterData, function (key, col) {

                options += GetddlOption(col.setupId, col.name);
            });
            $('#ddlCostCenter').empty();
            $('#ddlCostCenter').append(options);
            $("#ddlCostCenter").select2("val", '');
            if (costCenterData.length == 1) {
                $("#ddlCostCenter").select2("val", costCenterData[0].setupId);
            }
        }

        /* To bind categories data to EC_ID control */
        function BindCategoriesData(cData) {
            categoriesData = {};
            if (IsEmpty(cData)) {
                $('#ddlCategories').empty();
                $("#ddlCategories").select2("val", '');
                return;
            }
            categoriesData = cData;
            var options = categoriesData.length != 1 ? GetddlOption('', '--Select Categories--') : '';
            $.each(categoriesData, function (key, col) {

                options += GetddlOption(col.eC_ID, col.eC_Desc);
            });
            $('#ddlCategories').empty();
            $('#ddlCategories').append(options);
            $("#ddlCategories").select2("val", '');

            if (categoriesData.length == 1) {
                $("#ddlCategories").select2("val", categoriesData[0].eC_ID);
            }
        }

        /* To bind UOM data to UOM control */
        function BindUOMData(uData) {
            uomData = {};
            if (IsEmpty(uData)) {
                $('#ddlUOM').empty();
                $("#ddlUOM").select2("val", '');
                return;
            }
            uomData = uData;
            var options = uomData.length != 1 ? GetddlOption('', '--Select UOM--') : '';
            $.each(uomData, function (key, col) {

                options += GetddlOption(col.setupId, col.name);
            });
            $('#ddlUOM').empty();
            $('#ddlUOM').append(options);
            $("#ddlUOM").select2("val", '');
            if (uomData.length == 1) {
                $("#ddlUOM").select2("val", uomData[0].setupId);
            }
        }

        /* To set agent name and cost center on agent change */
        function AgentChange(agentId) {
            selectedProfile = agentId;
            if (selectedProfile > 0) {

                var reqData = { AgentInfo: apiAgentInfo, AgentId: parseInt(selectedProfile) };
                var apiUrl = apiHost.trimRight('/') + '/' + cntrlrPath.trimRight('/') + '/getCostCenterData';
                WebApiReq(apiUrl, 'POST', reqData, '', BindCostCenterData, null, null);
            }
            GetCate(agentId);
            GetUOM(agentId);
            ClearInfo();
        }

        /* To set agent name and categories on agent change */
        function GetCate(agentId) {
            selectedProfile = agentId;
            if (selectedProfile > 0) {

                var reqData = { AgentInfo: apiAgentInfo, AgentId: parseInt(selectedProfile) };
                var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCategoriesData';
                WebApiReq(apiUrl, 'POST', reqData, '', BindCategoriesData, null, null);
            }
            ClearInfo();
        }

        /* To set Agent name and UOM on Agent change */
        function GetUOM(agentId) {
            selectedProfile = agentId;
            if (selectedProfile > 0) {

                var reqData = { AgentInfo: apiAgentInfo, AgentId: parseInt(selectedProfile) };
                var apiUrl = apiHost.trimRight('/') + '/' + cntrlrPath.trimRight('/') + '/getUOMData';
                WebApiReq(apiUrl, 'POST', reqData, '', BindUOMData, null, null);
            }
            ClearInfo();
        }

        /* To prepare and get the types entity list for selected rows */
        function GetSelectedRows() {
            selectedRows = GetSetSelectedRows();
            if (IsEmpty(selectedRows) || selectedRows.length == 0)
                return [];
            var exptSelected = [];
            $.each(selectedRows, function (key, col) {

                var tInfo = etInfoDetails.find(item => item.eT_ID == col);
                tInfo.eT_Status = false;
                tInfo.eT_ModifiedBy = apiAgentInfo.LoginUserId;
                exptSelected = exptSelected.concat(tInfo);
            });
            return exptSelected;
        }

        /* To prepare and set types info entity */
        function RefreshGrid(resp) {
            $.each(selectedRows, function (key, col) {

                var etid = parseInt(col);
                etInfoDetails = etInfoDetails.filter(item => (item.eT_ID) !== etid);
            });
            if (etInfoDetails.length == 0) {
                BindTypesInfo(etInfoDetails);
                return;
            }
            selectedRows = [];
            GetSetSelectedRows('set', selectedRows);
            GetSetDataEntity('set', etInfoDetails);
            LoadData(GetSetCurrentPageNo('get', ''));
        }

        /* To load expense types info */
        function LoadTypesInfo() {
            var eT_AgentId = $("#ddlAgentId").val();
            if (eT_AgentId == "" || eT_AgentId == "--Select Agent--") {
                toastr.error('Please select agent');
                $('#ddlAgentId').parent().addClass('form-text-error');
                return false;
            }
            var typesInfo = {ET_AgentId: eT_AgentId }
            var reqData = {AgentInfo: apiAgentInfo, TypesInfo: typesInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getTypes';
            WebApiReq(apiUrl, 'POST', reqData, '', BindTypesInfo, null, null);
        }

        /* To bind expense types info to grid */
        function BindTypesInfo(etInfo) {
            if (etInfo.length == 0) {

                ShowError('no data available for selected agent.');
                ClearInfo();
                return;
            }
            etInfoDetails = etInfo;
            var gridProperties = GetGridPropsEntity();
            gridProperties.headerColumns = ('Code|Desc|UOM|GLAccount').split('|');
            gridProperties.displayColumns = ('eT_Code|eT_Desc|eT_UOM|eT_GLAccount').split('|');
            gridProperties.pKColumnNames = ('eT_ID').split('|');
            gridProperties.dataEntity = etInfo;
            gridProperties.divGridId = 'ExpTypeList';
            gridProperties.headerPaging = true;
            gridProperties.gridSelectedRows = selectedRows;
            gridProperties.selectType = 'M';
            gridProperties.recordsperpage = 5;

            EnablePagingGrid(gridProperties);
            $('#TypeList').show();
        }

        /* To save expense types info */
        function Save() {

            var IsValid = CheckReqData('ddlAgentId', 'NM', '20', 'Agent', mt);
            IsValid = CheckReqData('ddlCostCenter', 'NM', '20', 'Cost centre', mt) && IsValid;
            IsValid = CheckReqData('ddlCategories', 'NM', '20', 'Category', mt) && IsValid;
            IsValid = CheckReqData('ET_Code', 'AL', '20', 'Code', mt) && IsValid;
            IsValid = CheckReqData('ET_Desc', 'AL', '20', 'Description', mt) && IsValid;
            IsValid = CheckReqData('ddlUOM', 'AL', '5', 'UOM', mt) && IsValid;

            if (!IsValid)                 
                return IsValid;

            var typesInfo = {

                ET_ID: ET_Id,
                ET_AgentId: $("#ddlAgentId").val(),
                ET_Code: $("#ET_Code").val(),
                ET_Desc: $("#ET_Desc").val(),
                ET_EC_ID: $("#ddlCategories").val(),
                ET_UOM: GetddlSelText('ddlUOM'),
                ET_CostCenter: $("#ddlCostCenter").val(),
                ET_GLAccount: $("#ET_GLAccount").val(),
                ET_EnableItemize: ($('#ET_EnableItemize').is(':checked') ? 'Y' : 'N'),
                ET_EnableAttendee: ($('#ET_EnableAttendee').is(':checked') ? 'Y' : 'N'),
                ET_EnableTaxCalc: ($('#ET_EnableTaxCalc').is(':checked') ? 'Y' : 'N'),
                ET_Status: true,
                ET_CreatedBy: apiAgentInfo.LoginUserId,
                ET_ModifiedBy: apiAgentInfo.LoginUserId,
                ET_ReceiptNotReq: ($('#ET_ReceiptNotReq').is(':checked') ? 'Y' : 'N')
            }           

            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expRepId, TypesInfo: typesInfo, flag: "INSERT" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveTypes';
            WebApiReq(apiUrl, 'POST', reqData, '', StatusConfirm, null, null);            
        }

        /* To bind status of types info */
        function StatusConfirm() {
            if (ET_Id > 0) {
                alert('record updated successfully!');              
            }
            else if (ET_Id == 0) {
                alert('record added successfully!');             
            }
            ClearInfo();
            LoadTypesInfo();           
        }

        /* To update selected types info from the grid and update the status in data base */
        function EditTypesInfo() {
            var delTInfo = GetSelectedRows();
            if (IsEmpty(delTInfo) || delTInfo.length == 0) {
                ShowError('Please check any one of the row in below table');
                return;
            }
            if (IsEmpty(delTInfo) || delTInfo.length > 1) {
                ShowError('Please check one of the row in below table');
                return;
            }
            var etDetails = etInfoDetails.find(item => item.eT_ID == selectedRows[0]);
            ET_Id = etDetails.eT_ID;
            $("#ddlAgentId").select2('val', etDetails.eT_AgentId);
            $("#ET_Code").val(etDetails.eT_Code);
            $("#ddlCategories").select2('val', etDetails.eT_EC_ID);
            $("#ET_Desc").val(etDetails.eT_Desc);            
            $("#ddlCostCenter").select2('val', etDetails.eT_CostCenter);
            $("#ET_GLAccount").val(etDetails.eT_GLAccount);
            $("#ddlUOM option").filter(function () {
                return $(this).text() == etDetails.eT_UOM;
            }).prop('selected', true).trigger("change");

            $("#ET_EnableItemize").prop('checked', etDetails.eT_EnableItemize == "Y");
            $("#ET_EnableTaxCalc").prop('checked', etDetails.eT_EnableTaxCalc == "Y");
            $("#ET_EnableAttendee").prop('checked', etDetails.eT_EnableAttendee == "Y");
            $("#ET_ReceiptNotReq").prop('checked', etDetails.eT_ReceiptNotReq == "Y");
        }

        /* To delete selected rows from the grid and update the status in data base */
        function Delete() {
            var delTInfo = GetSelectedRows();
            if (IsEmpty(delTInfo) || delTInfo.length == 0) {

                ShowError('Please select record to delete');
                return;
            }            
            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expRepId, delTInfo, flag: "DELETE" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveTypes';
            WebApiReq(apiUrl, 'POST', reqData, '', RefreshGrid, null, null);
        }

        /* To clear types details and hide the div */
        function ClearInfo() {
            selectedRows = [];
            selectedProfile = 0;
            flag = ''
            ET_Id = 0;
            $("#ET_Code").val('');
            $("#ddlUOM").select2('val', '');
            $("#ET_GLAccount").val('');
            $("#ET_Desc").val('');
            $("#ddlCostCenter").select2('val', '');
            $("#ddlCategories").select2('val', '');
            $("#ET_EnableItemize").prop('checked', false);
            $("#ET_EnableTaxCalc").prop('checked', false);
            $("#ET_EnableAttendee").prop('checked', false);
            $("#ET_ReceiptNotReq").prop('checked', false);

            $("#ddlAgentId").parent().removeClass('form-text-error');
            $("#ET_Code").removeClass('form-text-error');
            $("#ET_Desc").removeClass('form-text-error');
            $("#ddlCostCenter").parent().removeClass('form-text-error');
            $("#ddlCategories").parent().removeClass('form-text-error');
            $("#ddlUOM").parent().removeClass('form-text-error');
            RemoveGrid();
            $('#TypeList').hide();
        }

        /* textbox allows only alphanumeric and @-_$,.:; */
        function check(e) {
            var keynum;
            var keychar;

            // For Internet Explorer
            if (window.event) {
                keynum = e.keyCode;
            }
            // For Netscape/Firefox/Opera
            else if (e.which) {
                keynum = e.which;
            }
            keychar = String.fromCharCode(keynum);
            //List of special characters you want to restrict
            if (keychar == "'" || keychar == "`" || keychar == "!" || keychar == "#" || keychar == "%" || keychar == "^" || keychar == "*" || keychar == "(" || keychar == ")" || keychar == "+" || keychar == "=" || keychar == "/" || keychar == "~" || keychar == "<" || keychar == ">" || keychar == "|" || keychar == "?" || keychar == "{" || keychar == "}" || keychar == "[" || keychar == "]" || keychar == "¬" || keychar == "£" || keychar == '"' || keychar == "\\") {
                return false;
            } else {
                return true;
            }
        }

        /* textbox allows only alphanumeric */
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (keyCode == 32));
            return ret;
        }

        /* textbox allows only numeric */
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }
    </script>

    <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">
                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left">Type Master</h3>
                    </div>
                </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <button class="btn btn-info mr-2" id="showTypesList" type="button" data-toggle="collapse" data-target="#TypeList" aria-expanded="false" aria-controls="TypesList" onclick="LoadTypesInfo()">VIEW ALL <i class="icon icon-search "></i></button>
                    <button type="button" class="btn btn-gray mr-2" onclick="ClearInfo();">CLEAR <i class="icon icon-close "></i></button>
                    <button type="button" class="btn btn-primary" onclick="Save();">SUBMIT <i class="icon icon-play_arrow "></i></button>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">
                        <input type="hidden" id="ET_ID" name="ET_ID" />
                        <div class="form-group col-md-3">
                            <label>Agent</label><span style="color: red;">*</span>
                            <select name="Agent" id="ddlAgentId" class="form-control" onchange="AgentChange(this.value)"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Cost Center</label><span style="color: red;">*</span>
                            <select name="CostCenter" id="ddlCostCenter" class="form-control"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Code</label> <span style="color: red;">*</span>
                            <input type="text" id="ET_Code" class="form-control" maxlength="10" placeholder="Enter Code" onkeypress="return IsAlphaNumeric(event);" />
                        </div>
                        <div class="form-group col-md-3">
                            <label>Desc</label><span style="color: red;">*</span>
                            <input type="text" class="form-control" id="ET_Desc" maxlength="75" placeholder="Enter Desc" onkeypress="return check(event)" />
                        </div>
                        <div class="form-group col-md-3">
                            <label>Categories</label><span style="color: red;">*</span>
                            <select name="Categories" class="form-control" id="ddlCategories"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>UOM</label><span style="color: red;">*</span>
                            <select name="UOM" class="form-control" id="ddlUOM"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>GLAccount</label>
                            <input type="text" class="form-control" id="ET_GLAccount" maxlength="20" placeholder="Enter GLAccount" onkeypress="return IsAlphaNumeric(event);" />
                        </div>
                         <div class="form-group col-md-3" style="margin-top:24px;">
                            <input type="checkbox" id="ET_EnableItemize" />
                            <label>Enable Itemize</label>
                        </div>
                         <div class="form-group col-md-3">
                            <input type="checkbox" id="ET_EnableAttendee" />
                            <label>Enable Attendee</label>
                        </div>
                         <div class="form-group col-md-3">
                            <input type="checkbox" id="ET_EnableTaxCalc" />
                            <label>Enable TaxCalc</label>
                        </div>
                         <div class="form-group col-md-3">
                            <input type="checkbox" id="ET_ReceiptNotReq" />
                            <label>Receipt Not Required</label>
                        </div>
                    </div>
                </div>

                <div class="exp-content-block collapse" id="TypeList" style="display: none;">
                    <h5 class="mb-3 float-left">All Types</h5>
                    <div class="button-controls text-right">
                        <button class="btn btn-edit" type="button" onclick="EditTypesInfo();">EDIT  <i class="icon icon-edit"></i></button>
                        <button class="btn btn-danger" type="button" onclick="Delete();">DELETE  <i class="icon icon-delete"></i></button>
                    </div>
                    <div class="clear"></div>
                    <div class="table-responsive" id="ExpTypeList"></div>
                </div>

            </div>
        </div>
        <div class="clear"></div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
