﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="CorporateExcelDownload.aspx.cs" Title="Corporate" Inherits="CozmoB2BWebApp.CorporateExcelDownload" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript">

        function setCheck(ctrl) {           
                var selected = document.getElementById(ctrl).checked;

            if (ctrl == 'ctl00_cphTransaction_chkAgent0') {

                for (i = 1; i < eval(document.getElementById('<%=hdnActiveAgents.ClientID %>').value); i++) {

                    document.getElementById('ctl00_cphTransaction_chkAgent' + i).checked = selected;
                }
            }
           
            else {
                if (document.getElementById(ctrl).checked == false) {
                    if (ctrl.includes('chkAgent'))
                        document.getElementById('ctl00_cphTransaction_chkAgent0').checked = selected;
                }
                else {
                    if (eval(document.getElementById('<%=hdnActiveAgents.ClientID %>').value)-1 == $('#dvCheckBoxListControl input[type=checkbox]:checked').length) {
                        document.getElementById('ctl00_cphTransaction_chkAgent0').checked = true;
                    }
                }
            }
        }

        function ExporttoExcel() {

            if ($("#ctl00_cphTransaction_dcFromDate_Date").val() == "" || $("#ctl00_cphTransaction_dcToDate_Date").val() == "") {
                if ($("#ctl00_cphTransaction_dcFromDate_Date").val() == "") {
                    toastr.error('Please Select From Date');
                    $('#ctl00_cphTransaction_dcFromDate_Date').parent().addClass('form-text-error');
                }
                if ($("#ctl00_cphTransaction_dcToDate_Date").val() == "") {
                    toastr.error('Please Select To Date');
                    $('#ctl00_cphTransaction_dcToDate_Date').parent().addClass('form-text-error');
                }
                return false;
            }
            else {
                var fromDate = $("#ctl00_cphTransaction_dcFromDate_Date").val();
                var toDate =   $("#ctl00_cphTransaction_dcToDate_Date").val();
                if (fromDate > toDate) {
                    toastr.error('Please Select From Date should be less than todate.');
                    return false;
                }
            }

            if (hotelAgentValidate()) {

            document.getElementById('<%=hdnFromdate.ClientID %>').value = $("#ctl00_cphTransaction_dcFromDate_Date").val();
            document.getElementById('<%=hdnTodate.ClientID %>').value = $("#ctl00_cphTransaction_dcToDate_Date").val();
            var active = eval(document.getElementById('<%=hdnActiveAgents.ClientID %>').value);
                        var selected = "";
                        for (i = 1; i <= active - 1; i++) {

                        var chkSupplier = document.getElementById('ctl00_cphTransaction_chkAgent' + i);

                        if (chkSupplier != null && chkSupplier.checked)
                            selected += document.getElementById('ctl00_cphTransaction_hdfAgentId' + i).value + ',';
                        }
            document.getElementById('<%=hdnSelectedAgents.ClientID %>').value = selected.slice(0, selected.lastIndexOf(','));
            $('#ctl00_upProgress').show();
            document.getElementById('<%=hdnSubmit.ClientID %>').value = "ExporttoExcel"
                document.forms[0].submit();
                $('#ctl00_upProgress').hide();
                //alert("Download Successfully");
            }
        }

        function hotelAgentValidate() {

            var active = eval(document.getElementById('<%=hdnActiveAgents.ClientID %>').value);
            var counter = 0;

            for (i = 1; i < active; i++) {

                if (document.getElementById('ctl00_cphTransaction_chkAgent' + i) != null && document.getElementById('ctl00_cphTransaction_chkAgent' + i).checked) {
                    counter++;
                }
            }

            if (counter == 0) {
                toastr.error('Please select atleast 1 Agent');
                return false;
            }
            return true;
        }

</script>
    <asp:HiddenField ID="hdnSubmit" runat="server" />
    <asp:HiddenField ID="hdnActiveAgents" runat="server"  />
    <asp:HiddenField ID="hdnSelectedAgents" runat="server"  />
    <asp:HiddenField ID="hdnAgentLocation" runat="server" /> 
    <asp:HiddenField ID="hdnFromdate" runat="server" />
    <asp:HiddenField ID="hdnTodate" runat="server" />
    
    <div class="body_container">
        <div class="col-md-12 CorpTrvl-tabbed-panel">
            
            <div class="tab-content responsive">
                <div style="background: #fff; padding: 10px;">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    From Date <span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                    </uc1:DateControl>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    To Date <span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                </div>
                            </div>
                        </div>
                      
                        <div class="col-md-4">
                                <div class="custom-dropdown-wrapper">
                                     <label>
                                    AgentList <span class="fcol_red">*</span></label>
                                    <a href="jaavscript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#AgentsDropdown">
                                        <div class="form-control-holder">
                                            <span class="form-control-text" id="htlSelectedAgents">Select Agents</span>                                  
                                        </div>  
                                    </a>
                        <div class="dropdown-content d-none p-4" id="AgentsDropdown">                         
                                        <div class="row no-gutters">                                            
                                            <div class="col-md-12" id="dvCheckBoxListControl">                               
                                                <table width="100%" id="tblAgents" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" enableviewstate="true" runat="server" border="0" cellspacing="0" cellpadding="0"></table>  
                                                <%--<a href="javascript:void(0);" class="btn btn-primary btn-sm float-right dd-done-btn">DONE</a>--%>
                                            </div>                                             
                                        </div>
                                    </div>
                                    </div>
                            </div>

                        <div class="col-md-4">
                            <label class="center-block">
                                &nbsp;</label>
                            <input class="btn but_d btn_xs_block cursor_point mar-5" type="button" name="action" id="action" value="Export to Excel"
                                                            onclick="return ExporttoExcel();" />
                        </div>
                    </div>
                    </div>
                </div>

            <div id="divMessage" style="display: none; color: Red; font-weight: bold; text-align: center;" runat="server">
                    </div>
            </div>
        </div>
    </asp:Content>