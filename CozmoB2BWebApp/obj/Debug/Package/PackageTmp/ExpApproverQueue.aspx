﻿<%@ Page Title="Expense Approver Queue" enableEventValidation="true" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpApproverQueue.aspx.cs" Inherits="CozmoB2BWebApp.ExpApproverQueue" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <style>
        .body_container {
            padding: 0 !important;
        }
    </style>
    <link href="css/toastr.css" rel="stylesheet" type="text/css" />
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="scripts/Common/ExpenseObjects.js" type="text/javascript"></script>
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="scripts/jquery-ui.js"></script>
    <script type="text/javascript">
        var FromDate = '', cntrllerPath = 'api/expTransactions', selectedProfile = 0, receiptStatus = {}, selectedReports = [];
        var employeeProfiles = {}, apiAgentInfo = {}, approverQueue = {}, receiptStatus = {}, expApprovalReports = {}, approverReports = {};
        var paymentTypes = {}, costCenters = {}, selectedcostCenter = 0; var srAction = ''; var webAppHost = '';
        try {
            $(document).ready(function () {
                $("#ctl00_upProgress").hide();
               
                /* Date Control for From Date*/
                FromDate = new Date();
                $("#ctl00_cphTransaction_txtFromDate").datepicker(
                    {
                        changeYear: true,
                        changeMonth: true,
                        minDate: -300,
                        dateFormat: 'dd/mm/yy',
                        onSelect: function (dateText, inst) {
                            var selectedDate = new Date(ConvertDate(dateText));
                            selectedDate.setDate(selectedDate.getDate() + 1);
                            $("#ctl00_cphTransaction_txtToDate").datepicker("option", "minDate", selectedDate);
                        }
                    }
                ).datepicker("setDate", FromDate);

            /* Date Control for To Date*/                
                $("#ctl00_cphTransaction_txtToDate").datepicker(
                    {
                        changeYear: true,
                        changeMonth: true,                         
                        dateFormat: 'dd/mm/yy'
                    }).datepicker("setDate", FromDate);

                $("#ctl00_cphTransaction_txtFromDate").change(function () {
                    var dtToday = new Date($("#ctl00_cphTransaction_txtFromDate").val());
                    $("#ctl00_cphTransaction_txtToDate").datepicker(
                        {
                            changeYear: true,
                            changeMonth: true,
                            minDate: dtToday,
                            dateFormat: 'dd/mm/yy'
                        });
                });

                webAppHost = '<%=Request.Url.Scheme + "://" + Request["HTTP_HOST"]%>';

                /* Prepare agent and login info for expense web api request call and assign to global variable */
                GetAgentInfo();

                /* Check the session info and revert if expired */
                if (IsEmpty(apiAgentInfo)) {

                    alert('Session expired, please login once again.');
                    return;
                }

                /* Prepare expense api host url and assign to global variable */
                GetExpenseApiHost();

                /* Load screen data */
                GetScreenData();

                $("#ctl00_upProgress").hide();
            });

            /* Get the Approver Queue */
            function BindApproverData() {

                approverQueue = {};
                var fromDate = new Date($("#ctl00_cphTransaction_txtFromDate").datepicker("getDate"));
                var toDate = new Date($("#ctl00_cphTransaction_txtToDate").datepicker("getDate"));

                if (fromDate > toDate || toDate == undefined || toDate == '') {

                    ShowError('From Date must be less than to Date');
                    return false;
                } else {

                    var FromDate = ConvertDate($('#ctl00_cphTransaction_txtFromDate').val()) + " 00:00:01";
                    var ToDate = ConvertDate($('#ctl00_cphTransaction_txtToDate').val()) + " 23:59:59";
                    var AgentId = parseInt('<%=Settings.LoginInfo.AgentId%>');
                    var ProfileId = $('#ctl00_cphTransaction_ddlProfile').val();
                    var CostCenter = $('#ctl00_cphTransaction_ddlCostCenter').val();
                    var ReceiptStatus = $('#ctl00_cphTransaction_ddlReceiptStatus').val();
                    var PaymentType = $('#ctl00_cphTransaction_ddlPaymentType').val();
                    var ReportTitle = $('#ctl00_cphTransaction_txtTitle').val() != '' ? $('#ctl00_cphTransaction_txtTitle').val().trim() : null;
                    var ReportReferenceNo = $('#ctl00_cphTransaction_txtRefNo').val() != '' ? $('#ctl00_cphTransaction_txtRefNo').val().trim() : null;
                    var ApproverStatus ='0';

                    approverQueue = GetExpApproverQueueObj(FromDate, ToDate, AgentId, ProfileId, CostCenter, ReceiptStatus, PaymentType,
                        ReportTitle, ReportReferenceNo, ApproverStatus);
                }
            }

            /* To get expense  Approval reports and Info */
            function GetScreenData() {

                BindApproverData();
                var reqData = { AgentInfo: apiAgentInfo, ApproverQueue: approverQueue,type:'AQ' };
                var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getExpenseApproverQueue';
                WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);
            }

            /* To bind expense Receipts,Reports and corp profiles */
            function BindScreenData(screenData) {

                BindDropDown(screenData.dtProfiles, screenData.dtReceiptStatus, screenData.dtPaymentType, screenData.dtcostCenters)
                BindApprovalReports(screenData.dtApprovalReceipts);
            }

            /* To bind expense Receipts,receipt status ,payment type and corp profiles */
            function BindDropDown(dtProfiles, dtReceiptStatus, dtPaymentType,dtcostCenters) {

                employeeProfiles = {};

                if (!IsEmpty(dtProfiles)) {

                    employeeProfiles = dtProfiles;
                    var options = employeeProfiles.length != 1 ? GetddlOption('0', 'All') : '';

                    $.each(employeeProfiles, function (key, col) {

                        options += GetddlOption(col.profileId, col.employeeId + ' - ' + col.name);
                    });

                    $('#ctl00_cphTransaction_ddlProfile').empty();
                    $('#ctl00_cphTransaction_ddlProfile').append(options);
                    $("#ctl00_cphTransaction_ddlProfile").select2("val", 0);

                    if (employeeProfiles.length == 1) {

                        $('#ctl00_cphTransaction_ddlProfile').attr('disabled', 'disabled');
                        $("#ctl00_cphTransaction_ddlProfile").select2("val", employeeProfiles[0].profileId);
                        selectedProfile = employeeProfiles[0].profileId;
                    }
                }

                receiptStatus = {}
                var options = receiptStatus.length != 1 ? GetddlOption('0', 'All') : '';

                options += GetddlOption('RU', 'Receipt Uploaded');
                options += GetddlOption('NR', 'No Receipt');

                $('#ctl00_cphTransaction_ddlReceiptStatus').empty();
                $('#ctl00_cphTransaction_ddlReceiptStatus').append(options);
                $("#ctl00_cphTransaction_ddlReceiptStatus").select2("val", '0');

                paymentTypes = {};
                if (!IsEmpty(dtPaymentType)) {

                    paymentTypes = dtPaymentType;
                    var options = paymentTypes.length != 1 ? GetddlOption('0', 'All') : '';

                    $.each(paymentTypes, function (key, col) {

                        options += GetddlOption(col.pT_ID, col.pT_Desc);
                    });

                    $('#ctl00_cphTransaction_ddlPaymentType').empty();
                    $('#ctl00_cphTransaction_ddlPaymentType').append(options);
                    $("#ctl00_cphTransaction_ddlPaymentType").select2("val", '0');

                }
                                   
                costCenters = [];

                var options = costCenters.length != 1 ? GetddlOption('0', 'All') : '';

                if (!IsEmpty(dtcostCenters)) {

                    costCenters = dtcostCenters;
                    

                    $.each(costCenters, function (key, col) {

                        options += GetddlOption(col.cC_Id, col.cC_Name);
                    });
                }

                $('#ctl00_cphTransaction_ddlCostCenter').empty();
                $('#ctl00_cphTransaction_ddlCostCenter').append(options);
                $("#ctl00_cphTransaction_ddlCostCenter").select2("val", '0');

                if (costCenters.length == 1 && false) {

                    $('#ctl00_cphTransaction_ddlCostCenter').attr('disabled', 'disabled');
                    $("#ctl00_cphTransaction_ddlCostCenter").select2("val", costCenters[0].cC_Id);
                    selectedcostCenter = costCenters[0].profileId;
                }
            }

            /* To Bind the Expense Receipts to grid */
            function BindApprovalReports(reports) {

                $('#CardsList').show();

                if (reports.length == 0) {

                    $('#CardsList').hide();

                    if (srAction == 'S')
                        ShowError('No records Found.');

                    ClearTrips();
                    return;
                }

                expApprovalReports = reports;
                var headerColumns = 'Date|Report Ref #|Employee ID|Employee Name|Cost Center|Report Title|Business Purpose|IS Claim|Claim Amount|Reimbursed Amount|';
                headerColumns += 'Expense Type|City|Payment Type|Receipt Status|Currency|Status|Comments';
                var dataColumns = 'eR_Date|eR_RefNo|employeeId|employeeName|costCentre|eR_Title|eR_Purpose|eD_ClaimExpense|eD_TotalAmount|reimburseAmount|eT_Desc';
                dataColumns += '|eD_City|eD_PayType|eD_ReceiptStatus|eD_Currency|approvalStatus|comments';

                var gridProperties = GetGridPropsEntity();
                gridProperties.headerColumns = headerColumns.split('|');
                gridProperties.displayColumns = dataColumns.split('|');
                gridProperties.pKColumnNames = ('eD_Id|eR_ProfileId|approverId').split('|');
                gridProperties.dataEntity = expApprovalReports;
                gridProperties.divGridId = 'divApproverGridInfo';
                gridProperties.headerPaging = false;
                gridProperties.gridSelectedRows = selectedReports;
                gridProperties.selectType = 'M';
                gridProperties.recordsperpage = 10;

                 gridProperties.columnproperties = {};

                // Assigning the grid column custom controls,events,attributes...
                var gridColumnproperties = {};
                for (var i = 0; i < gridProperties.displayColumns.length; i++) {

                    var columnproperties = GetGridColumnProperties();
                    columnproperties.columnName = gridProperties.displayColumns[i];

                    if (gridProperties.displayColumns[i] == 'eR_RefNo') {

                        columnproperties.columnControl = 'link';
                        columnproperties.columnAttributes = { 'style': 'font-weight:bold', 'href': 'javascript:void(0)' };
                        columnproperties.columnEvents = { 'onclick': 'ShowReferenceDetails' };
                        columnproperties.columnEventParams = { 'onclick': 'eD_ER_Id' }
                    }

                    if (gridProperties.displayColumns[i] == 'eR_Title') {

                        columnproperties.columnControl = 'link';
                        columnproperties.columnAttributes = { 'style': 'font-weight:bold', 'href': 'javascript:void(0)' };
                        columnproperties.columnEvents = { 'onclick': 'OpenExpReport' };
                        columnproperties.columnEventParams = { 'onclick': 'eD_ER_Id|eD_Id' };
                    }

                    if (gridProperties.displayColumns[i] == 'eT_Desc') {

                        columnproperties.columnControl = 'link';
                        columnproperties.columnAttributes = { 'style': 'font-weight:bold', 'href': 'javascript:void(0)' };
                        columnproperties.columnEvents = { 'onclick': 'OpenExpCreate' };
                        columnproperties.columnEventParams = { 'onclick': 'eD_ER_Id|eD_Id' };
                    }

                    if (gridProperties.displayColumns[i] == 'comments') {

                        columnproperties.columnControl = 'textarea';
                        columnproperties.columnAttributes = {
                            'class': 'form-control', 'disabled': 'disabled',
                            'style': 'width:250px', 'placeholder': 'Comments Here'
                        };
                        columnproperties.columnEvents = { 'id': 'txtComments_eD_Id_eR_ProfileId_approverId' };
                        columnproperties.columnEventParams = { 'id': 'eD_Id|eR_ProfileId|approverId' };
                    }

                    gridColumnproperties[gridProperties.displayColumns[i]] = columnproperties;
                }

                gridProperties.columnproperties = gridColumnproperties;
                EnablePagingGrid(gridProperties);
                $('#divTrips').show();
            }

            /* To set selected row value on grid row check box */
            function SelectRowCheckBox(event, selectedval) {

                if (event.checked == true) {

                    currentPageSelectedRows.push(selectedval);
                    gridProps.gridSelectedRows.push(selectedval);
                    $('#txtComments_' + selectedval.replace(/\|/g, '_')).attr('disabled',false);
                }
                else {

                    currentPageSelectedRows = currentPageSelectedRows.filter(item => item !== selectedval);
                    gridProps.gridSelectedRows = gridProps.gridSelectedRows.filter(item => item !== selectedval);
                     $('#txtComments_' + selectedval.replace(/\|/g, '_')).attr('disabled',true);
                }

                selectAllRows = currentPageSelectedRows.length == gridProps.recordsperpage;
                $('#' + chkSelectAllId).prop('checked', selectAllRows);
            }

            /* To clear Approver details and hide the div */
            function ClearTrips() {

                RemoveGrid();
                $('#divTrips').hide();
            }

            
            /* To show grid selected page as active and load the page data */
            function showselpage(event, pageno) {

                event.parentNode.className = 'page active';
                LoadData(pageno);

                setTimeout(function () {
                    $('#tbRows TR').each(function () {

                        $('#' + $(this).find('td input[type=checkbox]').attr('id')).attr('checked', selectAllRows);
                        $('#' + $(this).find('td textarea').attr('id')).attr('disabled', selectAllRows);

                    });
                }, 500);
            }

            
            /* To select all rows in the grid */
            function SelectAll(event) {

                selectAllRows = event.checked;
                $("#" + tbodyRowsId).find('input[type=checkbox]').each(function () {
                    this.checked = selectAllRows;
                });

                if (event.checked == true) {

                    currentPageSelectedRows = currentPageAllPKValues;
                    gridProps.gridSelectedRows = gridProps.gridSelectedRows.concat(currentPageAllPKValues);
                    $("#" + tbodyRowsId).find('textarea').each(function () {
                        $("#" + this.id).attr('disabled', false);;
                        $("#" + this.id).val('');
                    });
                }
                else {

                    currentPageSelectedRows = [];
                    $.each(currentPageAllPKValues, function (key, col) {
                        gridProps.gridSelectedRows = gridProps.gridSelectedRows.filter(item => item !== col);
                    });
                    $("#" + tbodyRowsId).find('textarea').each(function () {
                        $("#" + this.id).attr('disabled', true);;
                        $("#" + this.id).val('');
                    });
                }
            }
          
            /* Getting the Report reference Datails */
            function ShowReferenceDetails(expDtlId) {

                if (parseInt(expDtlId) > 0) {

                    AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpReportPrint', 'sessionData':'" + JSON.stringify({ expDetailId: expDtlId }) + "', 'action':'set'}");
                    window.open("ExpReportPrint.aspx", "Expense Report", "width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
                }
            }
          
            /* Open expense report screen */
            function OpenExpReport(expRepId, expDetailId) {

                if (parseInt(expRepId) > 0 && parseInt(expDetailId) > 0) {

                    AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpReport', 'sessionData':'" + JSON.stringify({ ExpRepId: expRepId, ExpDtlId: expDetailId }) + "', 'action':'set'}");
                    window.location.href = "ExpReport.aspx";
                }
            }
          
            /* Open expense create screen */
            function OpenExpCreate(expRepId, expDetailId) {

                if (parseInt(expRepId) > 0 && parseInt(expDetailId) > 0) {

                    AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpCreate', 'sessionData':'" + JSON.stringify({ ExpRepId: expRepId, ExpDtlId: expDetailId }) + "', 'action':'set'}");
                    window.location.href = "ExpCreate.aspx";
                }
            }
          
            /* To get agent and login info */
            function GetAgentInfo() {

                try {

                    var loginInfo = '<%=Settings.LoginInfo == null%>';

                    if (loginInfo == true)
                        return apiAgentInfo;

                    var agentId = '<%=Settings.LoginInfo.AgentId%>';
                    var loginUser = '<%=Settings.LoginInfo.UserID%>';
                    var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                    var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                    var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                    apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
                }
                catch (excp) {
                    var exception = excp;
                }
                return apiAgentInfo;
            }

            /* To get expense api host url */
            function GetExpenseApiHost() {

                apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';

                if (IsEmpty(apiHost))
                    apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";

                return apiHost;
            }

            /* Show the Date controls*/
            function showDatepicker(id) {

                $('#ctl00_cphTransaction_' + id).focus();
            }

            /* Getting the Date from Date controls*/
            function ConvertDate(selector) {
                // To matach with Main search Date Format
                var from = selector.split("/");
                var Date = from[1] + "/" + from[0] + "/" + from[2];
                return Date;
            }

            /* To prepare and get the expense Approval Reports entity list for selected Reports */
            function GetSelectedReports() {

                selectedReports = GetSetSelectedRows();
                if (IsEmpty(selectedReports) || selectedReports.length == 0)
                    return [];

                var expReportSelected = [];
                $.each(selectedReports, function (key, col) {
                    var selval = col.split('|');
                    expReportSelected.push(selval);
                });
                return expReportSelected;
            }

            /* To submit expense report */
            function ApprovalConfirmation() {

                var expReportSelected = GetSelectedReports();
                if (IsEmpty(expReportSelected) || expReportSelected.length == 0) {

                    ShowError('Please select expense(s) to approve.');
                    return false;
                }

                $('#divApproveConfirm').modal('show');
            }   

            /*Approving the Reports */
            function ReportApproval(statusType) {

                $('#divApproveConfirm').modal('hide');
                var expReportSelected = GetSelectedReports();
                var approverReports = [], ids = [], isValid = true, Remarks = '';

                if (expReportSelected.length > 0 && statusType != '') {

                    for (var i = 0; i < expReportSelected.length; i++)
                        ids.push(parseInt(expReportSelected[i]));   
                    
                    // Validating Comments when status either reject or revise.
                    if (statusType != 'A') {

                        $('#tbRows TR').each(function () {

                            if ($('#' + $(this).find('td input[type=checkbox]').attr('id')).prop('checked')) {

                                Remarks = $('#' + $(this).find('td textarea').attr('id')).val();

                                if (IsEmpty(Remarks)) {

                                    isValid = false;
                                    return ShowError('Please Enter the Comments.');                                    
                                }
                            }
                        });
                    }

                    if (isValid) {

                        for (var i = 0; i < expApprovalReports.length; i++) {

                            if (ids.includes(expApprovalReports[i].eD_Id)) {

                                var txtId = 'txtComments_' + expApprovalReports[i].eD_Id + "_" + expApprovalReports[i].eR_ProfileId + "_" + expApprovalReports[i].approverId;
                                Remarks = statusType != 'A' || !IsEmpty($('#' + txtId)) ? $('#' + txtId).val() : "";

                                approverReports.push({
                                    ApprovalStatus: statusType,
                                    ExpDetailId: parseInt(expApprovalReports[i].eD_Id),
                                    expApproverId: expApprovalReports[i].approverId,
                                    ProfileID: 0,
                                    CreatedBy: '<%=Settings.LoginInfo.UserID%>',
                                    ExpRepId: parseInt(expApprovalReports[i].eD_ER_Id),
                                    Remarks: Remarks
                                });
                            }
                        }

                        BindApproverData();

                        srAction = statusType;
                        
                        var reqData = { AgentInfo: apiAgentInfo, ApproverReports: approverReports, ApproverQueue: approverQueue, type: 'AQ', Host: webAppHost };
                        var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveExpApprovalReports';
                        WebApiReq(apiUrl, 'POST', reqData, '', ApproverSuccessReports, null, null);
                    }
                }

                $("#ctl00_upProgress").hide();
                return isValid;
            }

            /*Approving status Reports */
            function ApproverSuccessReports(response) {

                ClearTrips();
                selectedReports = [];

                /* Date Control for To Date*/
                $("#ctl00_cphTransaction_txtFromDate").datepicker({ dateFormat: 'dd/mm/yy' });

                /* Date Control for To Date*/
                $("#ctl00_cphTransaction_txtToDate").datepicker({
                    minDate: ConvertDate($("#ctl00_cphTransaction_txtFromDate").val()),
                    dateFormat: 'dd/mm/yy'
                });

                BindApprovalReports(response);

                if (srAction != 'S')
                    alert('Expense(s) ' + (srAction == 'A' ? 'approved' : srAction == 'RJ' ? 'rejected' : srAction == 'RV' ? 'revised' : 'status updated') + ' successfully.');
            }

            /*get the Approving Reports */
            function showQueueRecords(action) {

                srAction = action;
                var approverReports = [];
                BindApproverData();

                var reqData = { AgentInfo: apiAgentInfo, ApproverReports: approverReports, ApproverQueue: approverQueue ,type:'AQ', Host: webAppHost};
                var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveExpApprovalReports';
                WebApiReq(apiUrl, 'POST', reqData, '', ApproverSuccessReports, null, null);
            }


        } catch (e) {

            var msg = JSON.stringify(e.stack);
            exceptionSaving(msg);
        }
    </script>

    <div class="body_container p-0 corp-exp-module">

        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">

                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left">Expense Approver Queue </h3>

                    </div>

                </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <a class="btn btn-gray mr-2 " onclick="window.location.href='ExpApproverqueue.aspx'">CLEAR <i class="icon icon-close "></i></a>
                    <a class="btn btn-primary " id="showCardsList" onclick="showQueueRecords('S')">SEARCH <i class="icon icon-search "></i></a>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">
                        <div class="form-group col-6 col-md-2">
                            <label>From Date</label>
                            <div class="input-group mb-3">
                                <asp:TextBox runat="server" class="form-control" placeholder="From Date" ID="txtFromDate" />
                                <div class="input-group-append" onclick="showDatepicker('txtFromDate')">
                                    <span class="input-group-text" id="basic-addon1"><i class="icon icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-6 col-md-2">
                            <label>To Date</label>
                            <div class="input-group mb-3">
                                <asp:TextBox runat="server" class="form-control" placeholder="To Date" ID="txtToDate" />
                                <div class="input-group-append" onclick="showDatepicker('txtToDate')">
                                    <span class="input-group-text" id="basic-addon2"><i class="icon icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Employee</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlProfile">
                                <asp:ListItem Value="0">All</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="form-group col-md-2">
                            <label>Cost Center</label>
                             <asp:DropDownList runat="server" class="form-control" ID="ddlCostCenter">
                                <asp:ListItem Value="0">All</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Receipt Status</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlReceiptStatus">
                                <asp:ListItem Value="0">All</asp:ListItem>
                            </asp:DropDownList>

                        </div>
                        <div class="form-group col-md-2">
                            <label>Payment Type</label>
                            <asp:DropDownList runat="server" class="form-control" ID="ddlPaymentType">
                                <asp:ListItem Value="0">All</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Report Ref #</label>
                            <asp:TextBox runat="server" class="form-control" ID="txtRefNo" placeholder="Reference No" />
                        </div>
                        <div class="form-group col-md-2">
                            <label>Report Title</label>
                            <asp:TextBox runat="server" class="form-control" placeholder="Title" ID="txtTitle" />
                        </div>
                        

                    </div>

                </div>
                <div class="exp-content-block" id="CardsList">

                    <h5 class="mb-3 float-left">Expense Details</h5>
                    <div class="button-controls text-right">
                        <a class="btn btn-primary mr-2" onclick="ApprovalConfirmation()">APPROVE</a>
                        <a class="btn btn-info mr-2" onclick="return ReportApproval('RV')">REVISE</a>
                        <a class="btn btn-danger" onclick="return ReportApproval('RJ')">REJECT</a>
                    </div>
                    <div class="clear"></div>
                    <div class="table-responsive">
                        <div id="divApproverGridInfo"></div>
                    </div>
                </div>

            </div>
        </div>


        <div class="clear "></div>
    </div>
    <!-- Expense approve confirm popup -->
    <div class="modal fade" id="divApproveConfirm" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="color: white">
                    <span ID="title" Style="color: white; text-align: center; margin-top: 15px">Approver Electronic Agreement</span>
                    <button type="button" class="close" data-dismiss="modal" style="height: 10px; width: 10px" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body" id="modalReports"><ul><li>By clicking "Accept" I hereby certify that the expenses in this report are directly linked to AAME business.</li></ul></div>
                <div class="modal-footer">
                    <a class="btn btn-danger mr-2" data-dismiss="modal" >Cancel</a>
                    <a class="btn btn-info mr-2" onclick="return ReportApproval('A')">Accept & Approve</a>
                </div>
            </div>
        </div>
    </div>
</asp:Content>
