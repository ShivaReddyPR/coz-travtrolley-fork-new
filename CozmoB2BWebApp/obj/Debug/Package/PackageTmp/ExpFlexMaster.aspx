﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpFlexMaster.aspx.cs" Inherits="CozmoB2BWebApp.ExpFlexMaster" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <link href="css/toastr.css" rel="stylesheet" type="text/css" />
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>

    <script>
        /* Global variables */
        var apiHost = ''; var cntrllerPath = 'api/expenseMasters';
        var apiAgentInfo = {}; var expRepId = 0; var selectedRows = [];
        var flag = ''; var typesData = {}; var orderDependData = {};
        var EF_Id = 0; var displayDependData = {};
        var agentdata = {};

        /* Page Load */
        $(document).ready(function () {
            /* Check query string to see if existing report needs to open */
            expRepId = '<%=Request.QueryString["ExpRepId"] == null ? "0" : Request.QueryString["ExpRepId"]%>';

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {
                alert('Session expired, please login once again.');
                return;
            }

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            /* To get page load dropdown list */
            GetScreenData();

            /* To change control display SQLQuery */
            $('#ddlControl').change(function (e) {
                var eF_Control = $("#ddlControl").val();
                if (eF_Control == "DDL") {
                    $('#divSQLQuery').show();
                    toastr.error('Please Enter SQLQuery');
                    $("#EF_SQLQuery").addClass('form-text-error');
                }
                else {
                    $('#divSQLQuery').hide();
                    $("#EF_SQLQuery").val('');
                    $("#EF_SQLQuery").removeClass('form-text-error');
                }
            });

            $('#ddlEF_ET_Id').change(function (e) {
                GetDisplayDepend();
            });

            $('#ddlType').change(function (e) {
                GetDisplayDepend();
            });
        });

        /* To get agent and login info */
        function GetAgentInfo() {
            try {
                var loginInfo = '<%=Settings.LoginInfo == null%>';
                if (loginInfo == true)
                    return apiAgentInfo;

                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {
            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';
            if (IsEmpty(apiHost))
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";
            return apiHost;
        }

        /* To get agent */
        function GetScreenData() {
            var reqData = { AgentInfo: apiAgentInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getFlexMasterScreenLoadData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);
        }

        /* To bind agent */
        function BindScreenData(screenData) {
            agentdata = {};
            if (!IsEmpty(screenData.dtAgents)) {

                agentdata = screenData.dtAgents;
                var options = agentdata.length != 1 ? GetddlOption('', '--Select Agent--') : '';
                $.each(agentdata, function (key, col) {

                    options += GetddlOption(col.agenT_ID, col.agenT_NAME);
                });
                $('#ddlAgentId').empty();
                $('#ddlAgentId').append(options);
                $("#ddlAgentId").select2("val", '');
                if (agentdata.length == 1) {

                    $('#ddlAgentId').attr('disabled', 'disabled');
                    $("#ddlAgentId").select2("val", agentdata[0].agenT_ID);
                }
              
                if (!IsEmpty(screenData.dtTypes))
                    BindTypesData(screenData.dtTypes);
                if (!IsEmpty(screenData.dtOrderDepend))
                    BindOrderDependData(screenData.dtOrderDepend);
            }
        }

        /* To bind types data to expense type control */
        function BindTypesData(tData) {
            typesData = {};
            if (IsEmpty(tData)) {
                $('#ddlEF_ET_Id').empty();
                $("#ddlEF_ET_Id").select2("val", '');
                return;
            }
            typesData = tData;
            var options = typesData.length != 1 ? GetddlOption('', '--Select Type--') : '';
            $.each(typesData, function (key, col) {

                options += GetddlOption(col.eT_ID, col.eT_Desc);
            });
            $('#ddlEF_ET_Id').empty();
            $('#ddlEF_ET_Id').append(options);
            $("#ddlEF_ET_Id").select2("val", '');

            if (typesData.length == 1) {
                $("#ddlEF_ET_Id").select2("val", typesData[0].eT_ID);
            }
        }

        /* To bind orderDepend data to orderDepend field control */
        function BindOrderDependData(oData) {
            orderDependData = {};
            if (IsEmpty(oData)) {
                $('#ddlOrderDependId').empty();
                $("#ddlOrderDependId").select2("val", '');
                return;
            }
            orderDependData = oData;
            var options = orderDependData.length != 1 ? GetddlOption('', '--Select OrderField--') : '';
            $.each(orderDependData, function (key, col) {

                options += GetddlOption(col.eF_ID, col.eF_Label);
            });
            $('#ddlOrderDependId').empty();
            $('#ddlOrderDependId').append(options);
            $("#ddlOrderDependId").select2("val", '');

            if (orderDependData.length == 1) {
                $("#ddlOrderDependId").select2("val", orderDependData[0].eF_ID);
            }
        }

        /* To bind displayDepend data to displayDepend field control */
        function BindDisplayDependData(dData) {
            displayDependData = {};
            if (IsEmpty(dData)) {
                $('#ddlDisplayDependId').empty();
                $("#ddlDisplayDependId").select2("val", '');
                return;
            }
            displayDependData = dData;
            var options = displayDependData.length != 1 ? GetddlOption('', '--Select DisplayField--') : '';
            $.each(displayDependData, function (key, col) {

                options += GetddlOption(col.eF_ID, col.eF_Label);
            });
            $('#ddlDisplayDependId').empty();
            $('#ddlDisplayDependId').append(options);
            $("#ddlDisplayDependId").select2("val", '');
            if (displayDependData.length == 1) {
                $("#ddlDisplayDependId").select2("val", displayDependData[0].eF_ID);
            }
        }

        /* To set agent and expense types on agent change */
        function AgentChange(agentId) {
            selectedProfile = agentId;
            if (selectedProfile > 0) {

                var reqData = { AgentInfo: apiAgentInfo, AgentId: parseInt(selectedProfile) };
                var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getTypesData';
                WebApiReq(apiUrl, 'POST', reqData, '', BindTypesData, null, null);
            }
            GetOrderDepend(agentId);
            ClearInfo();
        }

        /* To set agent and orderDepend field on agent change */
        function GetOrderDepend(agentId) {
            selectedProfile = agentId;
            if (selectedProfile > 0) {

                var reqData = { AgentInfo: apiAgentInfo, AgentId: parseInt(selectedProfile) };
                var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getOrderDepend';
                WebApiReq(apiUrl, 'POST', reqData, '', BindOrderDependData, null, null);
            }
            ClearInfo();
        }

        /* To set displayDepend field  on flex type and expense type change */
        function GetDisplayDepend() {
            var EF_Type = $('#ddlType').val();
            var EF_ET_Id = $('#ddlEF_ET_Id').val();
            ddField = { EF_Type: EF_Type, EF_ET_Id: EF_ET_Id };
            var reqData = { AgentInfo: apiAgentInfo, ddfield: ddField };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getDisplayDepend';
            WebApiReq(apiUrl, 'POST', reqData, '', BindDisplayDependData, null, null);
        }

        /* To prepare and get the flex entity list for selected rows */
        function GetSelectedRows() {
            selectedRows = GetSetSelectedRows();
            if (IsEmpty(selectedRows) || selectedRows.length == 0)
                return [];
            var expfSelected = [];
            $.each(selectedRows, function (key, col) {

                var fInfo = efInfoDetails.find(item => item.eF_ID == col);
                fInfo.eF_Status = false;
                fInfo.eF_ModifiedBy = apiAgentInfo.LoginUserId;
                expfSelected = expfSelected.concat(fInfo);
            });
            return expfSelected;
        }

        /* To prepare and set flex info entity */
        function RefreshGrid(resp) {
            $.each(selectedRows, function (key, col) {

                var efid = parseInt(col);
                efInfoDetails = efInfoDetails.filter(item => (item.eF_ID) !== efid);
            });
            if (efInfoDetails.length == 0) {
                BindFlexInfo(efInfoDetails);
                return;
            }
            selectedRows = [];
            GetSetSelectedRows('set', selectedRows);
            GetSetDataEntity('set', efInfoDetails);
            LoadData(GetSetCurrentPageNo('get', ''));
        }

        /* To load expense flex info */
        function LoadFlexInfo() {
            var eF_AgentId = $("#ddlAgentId").val();
            var eF_ET_Id = $("#ddlEF_ET_Id").val();
            if ((eF_AgentId == "" || eF_AgentId == "--Select Agent--") || (eF_ET_Id == "" || eF_ET_Id == "--Select Type--" || eF_ET_Id == null)) {
                if (eF_AgentId == "" || eF_AgentId == "--Select Agent--") {
                    toastr.error('Please select agent');
                    $('#ddlAgentId').parent().addClass('form-text-error');
                }
                if (eF_ET_Id == "" || eF_ET_Id == "--Select Type--" || eF_ET_Id == null) {
                    toastr.error('Please select expenseType');
                    $('#ddlEF_ET_Id').parent().addClass('form-text-error');
                }
                return false;
            }            
            var flexInfo = { EF_ET_Id: eF_ET_Id }
            var reqData = {AgentInfo: apiAgentInfo, FlexInfo: flexInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getFlexInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', BindFlexInfo, null, null);
        }

        /* To bind expense flex info to grid */
        function BindFlexInfo(efInfo) {
            if (efInfo.length == 0) {

                ShowError('no data available for selected expense type.');
                ClearInfo();
                return;
            }
            efInfoDetails = efInfo;
            var gridProperties = GetGridPropsEntity();
            gridProperties.headerColumns = ('Label|Control|DataType|Mandatory|Order|OrderDependField|DisplayDependField').split('|');
            gridProperties.displayColumns = ('eF_Label|eF_Control|eF_DataType|eF_Mandatory|eF_Order|eF_OrderLabel|eF_DisplayLabel').split('|');
            gridProperties.pKColumnNames = ('eF_ID').split('|');
            gridProperties.dataEntity = efInfo;
            gridProperties.divGridId = 'ExpFlexList';
            gridProperties.headerPaging = true;
            gridProperties.gridSelectedRows = selectedRows;
            gridProperties.selectType = 'M';
            gridProperties.recordsperpage = 5;

            EnablePagingGrid(gridProperties);
            $('#FlexList').show();
        }

        /* To save expense flex info */
        function Save() {
            var eF_AgentId = $("#ddlAgentId").val();
            var eF_ET_Id = $("#ddlEF_ET_Id").val();
            var eF_Label = $("#EF_Label").val();
            var eF_Control = $("#ddlControl").val();
            var eF_DataType = $("#ddlDataType").val();
            var eF_Order = $("#EF_Order").val();
            var eF_SQLQuery = $("#EF_SQLQuery").val();
            var eF_OrderDependId = $("#ddlOrderDependId").val();
            var eF_DisplayDependId = $("#ddlDisplayDependId").val();
            var eF_FieldWidth = $("#ddlFieldWidth").val();
            var eF_CssClass = $("#EF_CssClass").val();
            var eF_Type = $("#ddlType").val();            
            var eF_Mandatory = $('#EF_Mandatory:input[type="checkbox"]:checked').length;
            if (eF_Mandatory == 1) {
                eF_Mandatory = 'Y';
            }
            else {
                eF_Mandatory = 'N';
            }
            var eF_IsNewRow = $('#EF_IsNewRow:input[type="checkbox"]:checked').length;
            if (eF_IsNewRow == 1) {
                eF_IsNewRow = 'Y';
            }
            else {
                eF_IsNewRow = 'N';
            }
            var eF_IsDisable = $('#EF_IsDisable:input[type="checkbox"]:checked').length;
            if (eF_IsDisable == 1) {
                eF_IsDisable = 'Y';
            }
            else {
                eF_IsDisable = 'N';
            }
            var eF_GroupFlag = $('#EF_GroupFlag:input[type="checkbox"]:checked').length;
            if (eF_GroupFlag == 1) {
                eF_GroupFlag = 'Y';               
            }
            else {
                eF_GroupFlag = 'N';
            }
            var eF_HideField = $('#EF_HideField:input[type="checkbox"]:checked').length;
            if (eF_HideField == 1) {
                eF_HideField = 'Y';
            }
            else {
                eF_HideField = 'N';
            }
            if ((eF_AgentId == "" || eF_AgentId == "--Select Agent--") || (eF_ET_Id == "" || eF_ET_Id == "--Select Type--" || eF_ET_Id == null) || (eF_Label == "" || eF_Label == "Enetr Label") || (eF_Control == "-1" || eF_Control == "--Select Control--") || (eF_DataType == "-1" || eF_DataType == "--Select DataType--") || (eF_Order == "" || eF_Order == "Enter Order")) {
                if (eF_AgentId == "" || eF_AgentId == "--Select Type--") {
                    toastr.error('Please Select Agent');
                    $('#ddlAgentId').parent().addClass('form-text-error');
                }
                if (eF_ET_Id == "" || eF_ET_Id == "--Select Type--" || eF_ET_Id == null) {
                    toastr.error('Please Select expenseType');
                    $('#ddlEF_ET_Id').parent().addClass('form-text-error');
                }
                if (eF_Label == "" || eF_Label == "Enter Label") {
                    toastr.error('Please Enter Label');
                    $("#EF_Label").addClass('form-text-error');
                }
                if (eF_Control == "-1" || eF_Control == "--Select Control--") {
                    toastr.error('Please Select Control');
                    $('#ddlControl').parent().addClass('form-text-error');
                }
                if (eF_DataType == "-1" || eF_DataType == "--Select DataType--") {
                    toastr.error('Please Select DataType');
                    $("#ddlDataType").parent().addClass('form-text-error');
                }
                if (eF_Order == "" || eF_Order == "Enter Order") {
                    toastr.error('Please Enter Order');
                    $("#EF_Order").addClass('form-text-error');
                }
                if (eF_Control == "DDL") {
                    $('#divSQLQuery').show();
                    toastr.error('Please Enter SQLQuery');
                    $("#EF_SQLQuery").addClass('form-text-error');
                }
                return false;
            }

            var flexInfo = { EF_ID: EF_Id, EF_ET_Id: eF_ET_Id, EF_Label: eF_Label, EF_Control: eF_Control, EF_DataType: eF_DataType, EF_Order: eF_Order, EF_SQLQuery: eF_SQLQuery, EF_Mandatory: eF_Mandatory, EF_OrderDependId: eF_OrderDependId, EF_DisplayDependId: eF_DisplayDependId, EF_IsDisable: eF_IsDisable, EF_FieldWidth: eF_FieldWidth, EF_IsNewRow: eF_IsNewRow, EF_CssClass: eF_CssClass, EF_Type: eF_Type, EF_GroupFlag: eF_GroupFlag, EF_HideField: eF_HideField, EF_Status: true, EF_CreatedBy: apiAgentInfo.LoginUserId, EF_ModifiedBy: apiAgentInfo.LoginUserId }
            var reqData = { AgentInfo: apiAgentInfo, FlexInfo: flexInfo, flag: "INSERT" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveFlexInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', StatusConfirm, null, null);                      
        }

        /* To bind status of flex info */
        function StatusConfirm() {
             if (EF_Id > 0) {
                alert('record updated successfully!');            
            }
            else if (EF_Id == 0) {
                alert('record added successfully!');            
            }
            ClearInfo();
            LoadFlexInfo();
            GetDisplayDepend();
        }

        /* To update selected flex info from the grid and update the status in data base */
        function EditFlexInfo() {
            var delFInfo = GetSelectedRows();
            if (IsEmpty(delFInfo) || delFInfo.length == 0) {
                ShowError('Please check any one of the row in below table');
                return;
            }
            if (IsEmpty(delFInfo) || delFInfo.length > 1) {
                ShowError('Please check one of the row in below table');
                return;
            }
            var efDetails = efInfoDetails.find(item => item.eF_ID == selectedRows[0]);
            EF_Id = efDetails.eF_ID;
            $("#ddlEF_ET_Id").select2('val', efDetails.eF_ET_Id);
            $("#EF_Label").val(efDetails.eF_Label);
            $("#ddlControl").select2('val', efDetails.eF_Control);
            $("#ddlDataType").select2('val', efDetails.eF_DataType);

            if (efDetails.eF_Mandatory == "Y") {
                $("#EF_Mandatory").prop('checked', true);
            }
            else {
                $("#EF_Mandatory").prop('checked', false);
            }            
            if (efDetails.eF_Control == "DDL") {
                $("#divSQLQuery").show();
                $("#EF_SQLQuery").val(efDetails.eF_SQLQuery);
            }
            else {
                $("#divSQLQuery").hide();
                $("#EF_SQLQuery").val('');
            }
            $("#EF_Order").val(efDetails.eF_Order);
            Setddlval("ddlOrderDependId", efDetails.eF_OrderDependId, '');
            Setddlval("ddlDisplayDependId", efDetails.eF_DisplayDependId, '');
            $("#ddlType").select2('val', efDetails.eF_Type);
            if (efDetails.eF_GroupFlag == "Y") {
                $("#EF_GroupFlag").prop('checked', true);
            }
            else {
                $("#EF_GroupFlag").prop('checked', false);
            }
            if (efDetails.eF_IsDisable == "Y") {
                $("#EF_IsDisable").prop('checked', true);
            }
            else {
                $("#EF_IsDisable").prop('checked', false);
            }
            if (efDetails.eF_IsNewRow == "Y") {
                $("#EF_IsNewRow").prop('checked', true);
            }
            else {
                $("#EF_IsNewRow").prop('checked', false);
            }
            if (efDetails.eF_HideField == "Y") {
                $("#EF_HideField").prop('checked', true);
            }
            else {
                $("#EF_HideField").prop('checked', false);
            }
            Setddlval("ddlFieldWidth", efDetails.eF_FieldWidth, '');
            $("#EF_CssClass").val(efDetails.eF_CssClass);           
        }

        /* To delete selected rows from the grid and update the status in data base */
        function Delete() {
            var delFInfo = GetSelectedRows();
            if (IsEmpty(delFInfo) || delFInfo.length == 0) {

                ShowError('Please select record to delete');
                return;
            }
            var reqData = { AgentInfo: apiAgentInfo, delFInfo, flag: "DELETE" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveFlexInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', RefreshGrid, null, null);
        }

        /* To clear flex details and hide the div */
        function ClearInfo() {
            selectedRows = [];
            selectedProfile = 0;
            flag = ''
            EF_Id = 0;
            $("#EF_Label").val('');
            $("#ddlControl").select2('val', '-1');
            $("#ddlDataType").select2('val', '-1');
            $("#EF_Order").val('');
            $("#ddlType").select2('val', 'N');
            $("#ddlFieldWidth").select2('val', '-1');
            $("#ddlOrderDependId").select2('val', '');
            $("#ddlDisplayDependId").select2('val', '');
            $("#EF_CssClass").val('');
            $("#EF_SQLQuery").val('');
            $("#EF_Mandatory").prop('checked', false);
            $("#EF_IsDisable").prop('checked', false);
            $("#EF_IsNewRow").prop('checked', false);
            $("#EF_GroupFlag").prop('checked', false);
            $("#EF_HideField").prop('checked', false);

            $("#ddlAgentId").parent().removeClass('form-text-error');
            $("#ddlEF_ET_Id").parent().removeClass('form-text-error');
            $("#EF_Label").removeClass('form-text-error');
            $("#ddlControl").parent().removeClass('form-text-error');
            $("#ddlDataType").parent().removeClass('form-text-error');
            $("#EF_Order").removeClass('form-text-error');
            $("#EF_SQLQuery").removeClass('form-text-error');
            RemoveGrid();
            $('#FlexList').hide();
            $('#divSQLQuery').hide();
        }

        /* textbox allows only numeric */
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        /* textbox allows only alphanumeric */
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (keyCode == 32));
            return ret;
        }
    </script>

    <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">
                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left">Flex Master</h3>
                    </div>
                </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <button class="btn btn-info mr-2" id="showFlextList" type="button" data-toggle="collapse" data-target="#FlexList" aria-expanded="false" aria-controls="FlexList" onclick="LoadFlexInfo()">VIEW ALL <i class="icon icon-search "></i></button>
                    <button type="button" class="btn btn-gray mr-2" onclick="ClearInfo();">CLEAR <i class="icon icon-close "></i></button>
                    <button type="button" class="btn btn-primary" onclick="Save();">SUBMIT <i class="icon icon-play_arrow "></i></button>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">                        
                        <div class="form-group col-md-3">
                            <label>Agent</label><span style="color: red;">*</span>
                            <select name="Agent" id="ddlAgentId" class="form-control" onchange="AgentChange(this.value)"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Expense Type</label><span style="color: red;">*</span>
                            <select name="ET_Id" id="ddlEF_ET_Id" class="form-control"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Label</label><span style="color: red;">*</span>
                            <input type="text" id="EF_Label" class="form-control" maxlength="50" placeholder="Enter Label" onkeypress="return IsAlphaNumeric(event);" />
                        </div>
                        <div class="form-group col-md-3">
                            <label>Control</label><span style="color: red;">*</span>
                            <select class="form-control" id="ddlControl">
                                <option value="-1">--Select Control--</option>
                                <option value="DATE">Date</option>
                                <option value="DDL">Dropdown</option>
                                <option value="TEXTBOX">TextBox</option>
                                <option value="CHECKBOX">CheckBox</option>
                                <option value="TEXTAREA">TextArea</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Data Type</label><span style="color: red;">*</span>
                            <select class="form-control" id="ddlDataType">
                                <option value="-1">--Select DataType--</option>
                                <option value="AL">Alphabetic</option>
                                <option value="AN">Alphanumeric</option>
                                <option value="NM">Numeric</option>
                                <option value="DC">Decimal</option>
                                <option value="DT">Date</option>
                                <option value="TS">FreeText</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Order</label><span style="color: red;">*</span>
                            <input type="text" class="form-control" id="EF_Order" maxlength="50" placeholder="Enter Order" onkeypress="return isNumberKey(event)" />
                        </div>
                        <div class="form-group col-md-3">
                            <label>Flex Type</label>
                            <select class="form-control" id="ddlType">
                                <option value="N">Normal Flex</option>
                                <option value="I">Itemized Flex</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Order DependField</label>
                            <select name="ODependentId" class="form-control" id="ddlOrderDependId"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Display DependField</label>
                            <select name="DDependentId" class="form-control" id="ddlDisplayDependId"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Field Width</label>
                            <select class="form-control" id="ddlFieldWidth">
                                <option value="-1">--Select FieldWidth--</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                                <option value="11">11</option>
                                <option value="12">12</option>
                            </select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Css Class</label>
                            <input type="text" class="form-control" id="EF_CssClass" maxlength="200" placeholder="Enter CssClass" onkeypress="return IsAlphaNumeric(event)" />
                        </div>
                        <div class="form-group col-md-3" style="margin-top: 23px;">
                            <input type="checkbox" id="EF_Mandatory" />
                            <label>Mandatory</label>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="checkbox" id="EF_IsDisable" />
                            <label>IsDisable</label>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="checkbox" id="EF_IsNewRow" />
                            <label>IsNewRow</label>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="checkbox" id="EF_GroupFlag" />
                            <label>GroupFlag</label>
                        </div>
                        <div class="form-group col-md-3">
                            <input type="checkbox" id="EF_HideField" />
                            <label>Hide</label>
                        </div>                        
                        <div class="form-group col-md-3" style="display: none;" id="divSQLQuery">
                            <label>SQL Query</label><span style="color: red;">*</span>
                            <textarea rows="3" maxlength="1000" name="Query" id="EF_SQLQuery" class="form-control" placeholder="Enter SQL Query...."></textarea>
                        </div>
                    </div>
                </div>

                <div class="exp-content-block collapse" id="FlexList" style="display: none;">
                    <h5 class="mb-3 float-left">All Flex Details</h5>
                    <div class="button-controls text-right">
                        <button class="btn btn-edit" type="button" onclick="EditFlexInfo();">EDIT  <i class="icon icon-edit"></i></button>
                        <button class="btn btn-danger" type="button" onclick="Delete();">DELETE  <i class="icon icon-delete"></i></button>
                    </div>
                    <div class="clear"></div>
                    <div class="table-responsive" id="ExpFlexList"></div>
                </div>

            </div>
        </div>
        <div class="clear"></div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
