﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IbytaTerms.aspx.cs" Inherits="IbytaTerms" %>
	<!DOCTYPE html>
	<html>

	<head runat="server">
		<title>Ibyta - Terms and Conditions </title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.ico">	
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<!-- manual css -->

		<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
		<link href="build/css/main.min.css" rel="stylesheet" type="text/css" /> 
        <link href="css/override.css" rel="stylesheet">     
        
		<%--<script type="text/javascript" src="Scripts/Menu/query2.js"></script>--%>
     
    <%--jQuery 2.2.2 + Migrate 1.4.1 - Dec2020 Upgrade--%>

     <script src="build/js/jquery-2.2.4.min.js" type="text/javascript"></script>        
     <script type="text/javascript" src="build/js/jquery-migrate-1.4.1.min.js"></script>
       <script>
           jQuery.ajaxPrefilter(function(s){
            if(s.crossDomain){
                s.contents.script = false;
             }
           })        
     </script>

	</head>
<body>
     <div id="unsupported-message">
	        <a id="unsupported-overlay" href="#unsupported-modal"></a>
	        <div id="unsupported-modal" role="alertdialog" aria-labelledby="unsupported-title">
		        <h2 id="unsupported-title">⚠ Unsupported Browser ⚠</h2>
		        <p>Travtrolley probably won't work great in Internet Explorer. We generally only support the recent versions of major browsers like <a target="_blank" href="https://www.google.com/chrome/">Chrome</a>, <a target="_blank" href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a>, <a target="_blank" href="https://support.apple.com/downloads/safari">Safari.</a>!</p>

		        <p>If you think you're seeing this message in error, <a href="mailto:tech@cozmotravel.com">email us at tech@cozmotravel.com</a> and let us know your browser and version.</p>
	        </div>
	        <a id="unsupported-banner">
	        <strong> ⚠ Unsupported Browser ⚠ </strong>
		        Travtrolley may not work properly in this browser.
	        </a>
          </div>
         <script>
            // Get IE or Edge browser version
            function detectIE() {
                var ua = window.navigator.userAgent,
                    msie = ua.indexOf('MSIE '),
                    trident = ua.indexOf('Trident/'),
                    edge = ua.indexOf('Edge/');              
                 if (msie > 0 || trident > 0 ) {
                    $('#unsupported-message').css('display', 'block');
                    $('body').css('overflow', 'hidden');
                }
            }
            detectIE();         

        </script>
	    <style>
	            #unsupported-message {
                     display: none;
	            }
		        #unsupported-overlay,
		            #unsupported-modal {
		              display: block;
		              position: absolute;
		              position: fixed;
		              left: 0;
		              right: 0;
		              z-index: 9999;
		              background: #000;
		              color: #D5D7DE;
		            }
		
		            #unsupported-overlay {
		              top: 0;
		              bottom: 0;
		            }
		
		            #unsupported-modal {
		              top: 80px;
		              margin: auto;
		              width: 90%;
		              max-width: 520px;
		              max-height: 90%;
		              padding: 40px 20px;
		              box-shadow: 0 10px 30px #000;
		              text-align: center;
		              overflow: hidden;
		              overflow-y: auto;
		              border:solid 2px #ccc8b9;
		            }
		
		            #unsupported-message :last-child { margin-bottom: 0; }
		
		            #unsupported-message h2 {
		              font-size: 34px;
		              color: #FFF;
                      margin-bottom: 20px;
		            }
		
		            #unsupported-message h3 {
		              font-size: 20px;
		              color: #FFF;
		            }
		
		            body.hide-unsupport { padding-top: 24px; }
		            body.hide-unsupport #unsupported-message { visibility: hidden; }
		
		            #unsupported-banner {
		              display: block;
		              position: absolute;
		              top: 0;
		              left: 0;
		              right: 0;
		              background:#ccc8b9;
		              color: #000;
		              font-size: 14px;
		              padding: 2px 5px;
		              line-height: 1.5;
		              text-align: center;
		              visibility: visible;
		              z-index: 100000;
		            }
		
		
		
		            @media (max-width: 800px), (max-height: 500px) {
		              #unsupported-message .modal p {
		                /* font-size: 12px; */
		                line-height: 1.2;
		                margin-bottom: 1em;
		              }
		
		              #unsupported-modal {
		                position: absolute;
		                top: 20px;
		              }
		              #unsupported-message h1 { font-size: 22px; }
		              body.hide-unsupport { padding-top: 0px; }
		              #unsupported-banner { position: static; }
		              #unsupported-banner strong,
		              #unsupported-banner u { display: block; }
		            }
                    html,body{
                        min-height: 100%;
                        background-color: #ebf1fb;
                    }
                    body{
                        display:flex;
                        flex-direction:column;
                    }                     
                    .ibyta--top-header {
	                    background-color:#ec0b43;
	                    padding:5px 0 5px 0
                    }
                    .ibyta-footer {
                        bottom: 0;
                        left: 0;
                        right: 0;
                        z-index: 1000;
                        position: relative;
                        flex: 1;
                        display: flex;
                        flex-direction: column;
                        justify-content: flex-end;
                    }
                    .ibyta-footer .copyright {
	                    color:#fff;
	                    text-align:center;
	                    background-color:#000;
	                    padding:4px 0;
                        font-size: 12px;
                    }
                    .ibyta-footer .contact-info{
                        text-align: center;
                        padding-bottom: 5px;
                        color: #ec0b43;
                        background-color: #f5f4f4;
                        padding: 6px 0;
                    }
                    .ibyta-footer .contact-info a{
                        color: #ec0b43;
                    }
                    .ibyta-footer .contact-info em{
                        color:#383838;
                        font-style:normal;
                    }
                    .services-list{
                        text-align: center;
                        padding: 10px 2px;
                          background: #fff;
                          box-shadow: 0 0 5px -2px rgba(0, 0, 0, 0.32);
                    }
                    .services-list li{
                        color: #696969;
                        font-size: 13px;
                        font-weight: 600;
                        padding: 0 20px;
                    }
                    .services-list li:before{
                        content:"";
                        background:url(build/img/ibyta-services.svg) no-repeat;
                        background-position: -124px -60px;
                        width: 38px;
                        height: 30px;
                        display: block;
                        margin:0 auto 8px auto;
                        opacity:.1;
                    }
                    .services-list li:nth-child(2):before{
                        background-position:0 -122px;
                    }
                    .services-list li:nth-child(3):before{
                        background-position:6px -60px;
                    }
                    .services-list li:nth-child(4):before{
                        background-position:-63px -60px;
                    }
                    .services-list li:nth-child(5):before{
                        background-position:9px 0px;
                    }
                    .services-list li:nth-child(6):before{
                        background-position:-187px -60px;
                    }
                    @media(min-width:768px){.ibyta-footer{}}

	        </style>
    
    	<div class="ibyta--top-header">
			<div class="cz-container" style="box-shadow: none;">
				<div class="row px-5 px-lg-0">
					<div class="col-xs-6">
						<a href="ibytalogin.aspx"><img src="images/ibyta-logo.jpg" class="img-responsive" style="max-width: 130px;margin-left: -15px;"></a>
                        <p class="text-white"><small>An AirArabia Group Company</small> </p>
					</div>
					<div class="col-xs-6">
						<a href="Register.aspx" class="float-right mt-3" style="color:#fff;font-size:18px;"><span class="icon-user2"></span> Register</a>
					</div>
				</div>
			</div>
		</div>
        <div class="cz-container">
            <div class="body_container">
                <h2>Terms and Conditions</h2>
                <div class="clearfix"></div>
                <h5 class="pt-4">What is this document?</h5>
                <p>
                    1.1.These terms of use, read together with the Privacy Policy located at ("Privacy Policy"), constitutes a legal and binding contract ("Agreement") between you and Ibyta Travels LLC, a company validly existing under the laws of UAE providing, inter alia, the terms that govern your access to use (i) Ibyta's website ("Website"), (ii) Ibyta's mobile applications ("Mobile Applications"), (iii) Ibyta's online travel, accommodation, and allied reservation services, and (iv) any other service that may be provided by Ibyta from time to time (collectively referred to as the "Services"). You hereby agree and understand that this Agreement is a binding contract between Ibyta and any person who accesses, browses, or uses the Services in any manner and accordingly you hereby agree to be bound by the terms contained in this Agreement. If you do not agree to the terms contained in this Agreement, you shall not have the right to use the Services and shall forthwith leave the Website and stop using the Mobile Applications. The terms contained in this Agreement shall be accepted without any modification. The use of the Services would constitute acceptance of the terms of this Agreement.
                </p>

                <p>
                    1.2. You must be 16 (Sixteen) Lunar years of age or older to register, or visit or use the Services in any manner. By registering, visiting or using the Services, you hereby represent and warrant to Ibyta that you are 16 (Sixteen) lunar years of age or older, and that you have the right, authority and capacity to use the Services and agree to and abide by this Agreement. If you are using the Services on behalf of another organization or entity ("Organization"), then you are agreeing to be bound by the Agreement on behalf of that Organization and you represent and warrant that you have the authority to bind the Organization to this Agreement. In that case, "you" and "your" refers to you and the concerned Organization.
                </p>

                <p>
                    1.3. This Agreement is published in compliance of, and is governed by the provisions of UAE laws.
                </p>

                <p>
                    1.4. Ibyta authorizes you to view and access the content available on the Services solely for ordering, receiving, delivering and communicating only as per this Agreement. The contents of the Services, information, text, graphics, images, logos, button icons, software code, interface, design and the collection, arrangement and assembly of content on the Website, Mobile Applications or any of the other Services ("Ibyta Content"), are the property of Ibyta  and are protected under copyright, trademark and other applicable laws. You shall not modify the Ibyta Content or reproduce, display, publicly perform, distribute, or otherwise use the Ibyta Content in any way for any public or commercial purpose or for personal gain.
                </p>
                <p>
                    1.5. All rights and liabilities of Ibyta with respect to any Services to be provided by Ibyta shall be restricted to the scope of this Agreement. In addition to this Agreement, you shall also ensure that you are in compliance with the terms and conditions of the third parties, whose links are contained/embedded in the Services, with whom you choose to transact with. It is hereby clarified that Ibyta shall not be held liable for any transaction between you and any such third party.
                </p>

                <p>
                    1.6. Where there is contradiction between this Agreement and the conditions mentioned under the provisions of the Privacy Policy located at <<>> . this Agreement shall prevail.
                </p>
		           
                <h5 class="pt-4">Use of Services</h5>
                <p>2.1. Ibyta permits the viewing, copying, downloading materials available on the Website and the Mobile Applications provided that the material so obtained is used solely for personal and non-commercial purposes and such proprietary notices appearing on the material are reproduced.</p>
                <p>
                    2.2. You hereby agree that you shall not at any time (i) distribute, resell, cross-sell, or permit access to the Services to any third party, (ii) permit multiple end users to access the Services using shared login credentials (i.e., a shared email address and password), (iii) use the Services other than in accordance with (a) the instructions or documentation which Ibyta may provide from time to time, (b) applicable laws and (c) the terms contained in this Agreement.
                </p>
                <p>
                    2.3. Ibyta may, at any time and without having to serve any prior notice to you, (i) upgrade, update, change, modify, or improve the Services or a part of the Services in a manner it may deem fit, (ii) change any promotion scheme, promotion period, grace period (by whatever name it is called) and (iii) change the contents of this Agreement or the Privacy Policy. It is your responsibility, in such cases, to review the terms of the Agreement from time to time. Such change shall be made applicable when they are posted. Ibyta may also alter or remove any content from the Website or the Mobile Applications without notice and without liability.
                </p>
                <p>
                    2.4. Ibyta reserves the right, at its sole discretion, to suspend your ability to use or access the Services (or a part of the Services) at any time while Ibyta investigates complaints or alleged violations of this Agreement, or for any other reason. Further, it shall also have the ability to prohibit or restrict you from using the Services if Ibyta, in its opinion, feels that you are misusing the Services in any manner whatsoever.
                </p>
                <p>
                    2.5. You agree to abide by the terms and conditions of purchase imposed by any third-party supplier (such as airline companies, hotels, agents, etc.) ("Suppliers") with whom you elect to transact by using the Services, including, but not limited to, payment of all amounts when due and with the Supplier's rules and restrictions regarding availability, booking, cancelling, rescheduling and use of fares, products, or services. You understand that any violation of any such Supplier's rules and restrictions may result in cancellation of your reservation(s), in your being denied access to the applicable product or services, in your forfeiting any monies paid for such reservation(s), and/or in Ibyta debiting your account for any costs that it incurs as a result of such violation.
                </p>

                <p>
                    2.6. Ibyta may, from time to time, run promotional campaign and contests that require you to send in material or information about yourself. Each such promotional campaign and contests has its own rules and regulations, which you must read and agree to before you participate in the same.
                </p>

                <p>
                    2.7. The use of Services is governed by Ibyta's Privacy Policy, available at <<>>. Ibyta's Privacy Policy sets forth its practices regarding the collection, use and disclosure of personal information that it obtains about you in connection with the Services.
                </p>


                 <h5 class="pt-4">Payments</h5>
                <p>3.1. Ibyta shall have the right to charge transaction fees based on certain completed transactions using the Services. These charges/fees may also be altered by Ibyta without any notice. You shall be completely responsible for all charges, fees, duties, taxes, and assessments arising out of the use of the Services.</p>

                <p>3.2. By booking with Ibyta, you authorize Ibyta and its agents to transact with your bank or other payment gateways to obtain the necessary information required to confirm payment, collect payment, resolve inquiries and billing disputes, and/or as otherwise required to manage the booking. Ibyta does not collect or store your credit card information. Please refer to our Privacy Policy located at <<>> to learn more.</p>

                <p>3.3You hereby understand and agree that your reservation/booking is contingent upon Ibyta receiving the applicable fees/consideration/fares in its account and unless such monies have been credited into Ibyta’s account, it shall be under no obligation to issue you with the relevant tickets, reservation confirmation, passenger name record (PNR) or such other confirmations in connection with the Services.</p>

                <p>3.4. In the event a booking or reservation does not get confirmed for any reason, Ibyta is under no obligation to make another booking to compensate or replace the earlier booking. All subsequent bookings shall be treated as new transaction without any reference to the earlier transaction. All refunds shall take place as per the normal banking payment cycle.</p>

                <p>
                    3.5. Payment shall be made through the payment gateways authorized by Ibyta. You must adhere to the terms and conditions that are prescribed by payment gateways through which you choose to transact. Ibyta shall not be gateways.
                </p>

                <h5 class="pt-4">User Covenants</h5>

                <p>4.1. Ibyta  hereby informs you that you are not permitted to host, display, upload, modify, publish, transmit, update or share any information that:</p>

                <ul>
                    <li>(i) belongs to another person and to which you do not have any right; is grossly harmful, harassing, blasphemous, defamatory, obscene, pornographic, paedophilic, libellous, invasive of another's privacy, hateful, or racially, ethnically objectionable, disparaging, relating to or encouraging money laundering or gambling, or otherwise unlawful in any manner whatsoever;</li>
                    <li>(ii) harms minors in any way;</li>
                    <li>(iii) infringes any patent, trademark, copyright or other proprietary rights;</li>
                    <li>(iv) violates any law for the time being in force;</li>
                    <li>(v) deceives or misleads the addressee about the origin of such messages or communicates any information which is grossly offensive or menacing in nature;</li>
                    <li>(vi) impersonates or defames another person;</li>
                    <li>(vii) contains software viruses or any other computer code, files or programs designed to interrupt, destroy or limit the functionality of any computer resource and threatens the unity, integrity, defence, security or sovereignty of UAE, friendly relations with foreign states, or public order or causes incitement to the commission of any cognizable offence or prevents investigation of any offence or is insulting to any other nation.</li>
                </ul>

                <p>You are also prohibited from:</p>
                    <ul>
                        <li>(i) violating or attempting to violate the integrity or security of the Website, the Mobile Application or any Ibyta Content;</li>
                        <li>(ii) transmitting any information on or through the Website and Mobile Applications that is disruptive or competitive to the provision of Services by Ibyta;</li>
                        <li>(iii) intentionally submitting on the Website or Mobile Applications any incomplete, false or inaccurate information;</li>
                        <li>(iv) making any unsolicited communications to other users of the Services;</li>
                        <li>(v) using any engine, software, tool, agent or other device or mechanism (such as spiders, robots, avatars or intelligent agents) to navigate or search the Website;</li>
                        <li>(vi) attempting to decipher, decompile, disassemble or reverse engineer any part of the Website;</li>
                        <li>(vii) copying or duplicating in any manner any of the Ibyta Content or other information available from the Website; and framing or hotlinking or deep linking any Ibyta Content.</li>
                    </ul>
                <p>4.3. Ibyta, upon obtaining knowledge by itself or having been brought to actual knowledge by an affected person in writing or through email about any such information as mentioned in clause 4.2 above, shall be entitled to disable such information that is in contravention of clause 4.2, Ibyta shall be entitled to preserve such information and associated records for production to governmental authorities for investigation purposes.</p>

                <p>4.4. Ibyta may disclose or transfer information provided by you to its affiliates in other countries, and you hereby consent to such transfer, if such transfer is necessary for the performance of the lawful contract between Ibyta or any person on its behalf and the user or where you have consented to data transfer.</p>

                <h5 class="pt-4">Third party information</h5>
                <p>
                    5.1.The Website may provide information regarding third party website(s), affiliates or business partners and/or contain links to their websites. Such information and links are provided solely for the purpose of your reference. Ibyta is not endorsing the material on the Website, is not responsible for such errors and representation nor is it associated with it and you shall access these websites at your own risk. Further, it is up to you to take precautions to ensure that whatever links you select or software you download, whether from the Website, Mobile Applications, or other Services, is free of such items such as, but not limited to, viruses, worms, trojan horses, defects and other items of a destructive nature.
                </p>

                 <h5 class="pt-4">Visa Obligations</h5>
                <p>6.1. Your travel to foreign countries may be subject to the visa and other immigration related requirements as maybe prescribed by appropriate authorities from time to time. You hereby understand and agree that you shall have to procure the applicable visa and comply with all applicable immigration requirements by yourself and Ibyta shall not be under any obligation to inform you or assist you with obtaining the appropriate visa (including transit-visas, on-entry visas, etc.) or with the concerned immigration requirements. Further, Ibyta shall not be responsible for any issues, including inability to travel, arising due to your failure to obtain the appropriate visa or clear any immigration obligations that you may have.</p>


                 <h5 class="pt-4">Intellectual property rights</h5>
                 <p>7.1.All the intellectual property used on the Website by Ibyta, service providers or any third party shall remain the property of  Ibyta, service provider or any other third party as the case may be. Except as provided in the Agreement, the materials may not be modified, copied, reproduced, distributed, republished, downloaded, displayed, sold, compiled, posted or transmitted in any form or by any means, including but not limited to, electronic, mechanical, photocopying, recording or other means, without the prior express written permission of Ibyta.</p>


                <h5 class="pt-4">Unlawful or Prohibited Use</h5>
                <p>8.1.You warrant to Ibyta that you will comply with all applicable laws, statutes, ordinances and regulations regarding the use of  Ibyta's Services and any other related activities. You further warrant that you will not use this Website in any way prohibited by terms contained in this Agreement or under applicable law.</p>


                
                <h5 class="pt-4">Unlawful or Prohibited Use</h5>

                <p>9.1. You hereby acknowledge and agree that Ibyta provides intermediary services and is not, and shall not be deemed to be a Supplier, and therefore may not be held responsible in any way for any lack or deficiency of services provided by any Suppliers you choose to engage or hire or appoint Ibyta the Services. Therefore, Ibyta is not liable for any errors, omissions, representations, warranties, breaches or negligence of any of the Suppliers or for any personal injuries, death, property damage, or other damages or expenses resulting there from.</p>
                <p>9.2. Ibyta shall have no liability in the event of any delay, cancellation, overbooking, strike, force majeure or other causes beyond their direct control, and shall have no responsibility for any additional expenses incurred by you in connection with the same.</p>
                <p>9.3. You hereby understand and acknowledge that the fares provided by Ibyta on the Website or Mobile Applications are subject to change at the respective Supplier's discretion and Ibyta shall not be responsible for any increase/change in the fares/fees provided by a particular Supplier (and as displayed by Ibyta on the Website or Mobile Applications).</p>
                <p>9.4. Ibyta shall not be liable for any damages whatsoever including, without limitation, damages for loss of use, data or profits, arising out of or in any way connected with the inability to use or performance of the Website, Mobile Applications or any other Service. This shall extend to the loss suffered by you due to delay or inability to use or access the Website or the Mobile Applications.</p>
                <p>9.5. Ibyta shall not be responsible or liable to you in any manner for any losses, damage, injuries or expenses incurred by you as a result of any disclosures made by Ibyta, where you have consented to the making of such disclosures. If you have revoked such consent under the terms of the Privacy Policy, then Ibyta shall not be responsible or liable in any manner to you for any losses, damage, injuries or expenses incurred as a result of any disclosures made by Ibyta prior to its actual receipt of such revocation.</p>
                <p>9.6. Ibyta shall not be responsible to provide any updates on schedules, availability, cancellations, and modifications to the services provided by the Suppliers.</p>
                <p>9.7. The maximum liability of Ibyta, in respect of any Services provided, shall be limited up to a maximum of AED1,000.</p>


                <h5 class="pt-4">Indemnity</h5>
                <p>10.1.You hereby agree to indemnify and hold harmless Ibyta, its affiliates, officers, directors, employees, consultants, licensors, agents, and representatives from any and all third party claims, losses, liability, damages, and/or costs (including reasonable attorney fees and costs) arising from (i) your access to or use of Services, (ii) violation of the Agreement, (iii) infringement, or infringement by any other user of your account with Ibyta, and (iv) infringement of any intellectual property or other right of any person or entity. Ibyta will notify you promptly of any such claim, loss, liability, or demand, and in addition to your foregoing obligations, you agree to provide us with reasonable assistance, at your expense, in defending any such claim, loss, liability, damage, or cost.</p>



                <h5 class="pt-4">Severability</h5>
                <p>11.1. If any provision of this Agreement is determined to be invalid or unenforceable in whole or in part, such invalidity or unenforceability shall attach only to such provision or part of such provision and the remaining part of such provision and all other provisions of this Agreement shall continue to be in full force and effect.</p>



                <h5 class="pt-4">Term and Termination</h5>
                <p>12.1. This Agreement will remain in full force and effect while you use any Service in any form or capacity.</p>
                <p>12.2. In the event Ibyta discovers or has reasons to believe at any time during or after receiving a request for Services from you that the request for Services is either unauthorized or there has been misrepresentation of facts, Ibyta shall have the right to take any steps against you, including cancellation of the bookings, forfeiture of payment etc. without providing you with prior intimation. Ibyta shall not be responsible for any damages causes as such a consequence</p>
                <p>12.3. Ibyta reserves the right to terminate its Services provided to you in the event of breach of any terms contained in this Agreement, misrepresentation of information, any unlawful activity or is unable to verify or authenticate any information the user submits to Ibyta.</p>
                <p>12.4. Clauses under the headings Covenants, Liability, Indemnity, Intellectual Property, Dispute Resolution, and Notices shall continue and survive the termination of this Agreement.</p>



                <h5 class="pt-4">Dispute Resolution and Governing Law</h5>
                <p>13.1. This Agreement and any contractual obligation between Ibyta and you will be governed by the laws of UAE, subject to the exclusive jurisdiction of courts at UAE.</p>
                

                <h5 class="pt-4">Headings</h5>
                <p>14.1. The headings and subheadings herein are included for convenience and identification only and are not intended to describe, interpret, define or limit the scope, extent or intent of this Agreement, the terms of services or the right to use the Website by the User contained herein or any other section or pages of the Website in any manner whatsoever.</p>


                <h5 class="pt-4">Notices</h5>
                <p>14.2. All notices and communications shall be in writing, in English and shall deemed given if delivered personally or by commercial messenger or courier service, or mailed by registered or certified mail (return receipt requested) or sent Ibyta email/ facsimile, with due acknowledgment of complete transmission to the following address:</p>

                <h5 class="pt-4">RULES AND RESTRICTIONS</h5>
                    <ul>
                        <li>Until the payment is realized in full, should there be an increase in airfare or taxes, the same will be intimated and charged to you.</li>
                        <li>Until the payment is realized in full, the booking is subject to cancellation by the airline or Ibyta without prior notice.</li>
                        <li>If the payment is not approved on given credit card number(s), you will be notified as soon as possible. In such a scenario you need to speak to the credit card company, or provide alternate credit card number, or change mode of payment as applicable.</li>
                        <li>Should there be any error on the booking/ ticket; you need to report the same within 24 hours of getting an email from us.</li>
                        <li>Ibyta is not responsible for any schedule change by the airline after issuance of the ticket, but will inform you of the same if Ibyta is informed by the airlines. It is advisable to reconfirm your flight timings 24 hours prior to your flight departure.</li>
                        <li>All tickets are subject to date change and cancellation penalties with exception of some tickets being non-refundable but reusable later as per the rules of the airline. Partially or half-utilized tickets are non-refundable.</li>
                        <li>All refunds, including refund for NO SHOW shall be governed by the respective airline’s policy.</li>
                        <li>In cases where customer is entitled for full refund, a transaction-processing fee per ticket will be applicable.</li>
                        <li>All special fares are subject to certain rules and regulations and airlines may restrict upgrade to any higher class. For any clarifications, please contact the respective airlines.</li>
                        <li>Every open ticket has a ticket validity within which the travel has to be finished, and date change on the ticket can be done only as per the validity of ticket. Should you require further details on this, kindly contact us through our live chat.</li>
                        <li>Frequent flier miles will be as on the sole discretion of respective airlines. Ibyta will not be responsible for any mileage guarantee.</li>
                        <li>Infant is defined as a traveler whose age must be under 24 months throughout the entire journey. If the infant is 24 months or above on the return journey then the infant fare would not be applicable and you will need to buy the ticket under applicable child fare.</li>
                        <li>A maximum of one infant is allowed to travel with each adult passenger. In case of more than one infants travelling with one adult, first infant would be charged the infant fare but for the second infant applicable child fare would be charged.</li>
                        <li>Few airlines do not allow children to travel alone (Unaccompanied Minors) or with an accompanying young adult (between 12 - 16 years of age). Before making any such booking you are requested to have it checked either with our customer care or with respective airlines.</li>
                        <li>You are requested to contact our customer care for your date change and cancellation requests. We will assist you as per your requirements but still there might be few cases where we will request you to contact the airlines directly.</li>
                        <li>In addition to the airlines date change penalty, Ibyta charges a date change assistance service charge per passenger.</li>
                        <li>In addition to the stipulated airline cancellation fee, all airlines will also charge an RAF (Refund Admin Fee) per passenger per ticket.</li>
                        <li>Ibyta will charge a service fee per passenger for all cancellations.</li>
                        <li>All bookings booked on Ibyta are subject to cancellation penalty levied by the airlines.</li>
                        <li>In case of full cancellation, refunds will be usually processed within 7 business days while in case of partial cancellation, refunds might take up to three months’ time to be processed.</li>
                        <li>There are times when we are unable to confirm a reservation. In the rare event that this occurs, we will attempt to reach you by phone and email so that we can rebook you. You must call us back within 48 hours to ensure that your booking is confirmed by us. Any change in the fare in the meantime shall be charged to you.</li>
                        <li>Before the commencement of your air travel, please check that you possess all the valid documents required for travel: passport, air tickets, valid VISA, health certificates, travel insurance, etc. Contact consulate/ airline for further details.</li>
                        <li>In case of cancellation, amount paid for insurance will be non-refundable, this is applicable only in case if you have booked insurance along with ticket.</li>

                    </ul>

                <div class="alert alert-warning"> <a href="https://ibyta.com">ibyta.com</a> will NOT deal or provide any services or products to any of OFAC (Office of Foreign Assets Control) sanctions countries in accordance with the law of UAE.</div>
            </div>
        </div>
         <div class="ibyta-footer">
            <div class="services-list">
                <ul class="list-unstyled text-center mb-0">
                    <li class="d-inline-block">Hotels</li>
                    <li class="d-inline-block">Apartments</li>
                    <li class="d-inline-block">Tours</li>
                    <li class="d-inline-block">Transfers</li>
                    <li class="d-inline-block">Insurance</li>
                    <li class="d-inline-block">Visas</li>
                </ul>                       
            </div>  
            <div class="contact-info">
                    Tel : +97144065891 + 9714 4065892 <em> | </em> suppliers : <a href="mailto:contracting@ibyta.com">contracting@ibyta.com</a> <em> | </em> customers : <a href="mailto:sales@ibyta.com">sales@ibyta.com</a> 
                    <div class="small pt-2 text-secondary">
                    <a class="text-secondary" href="IbytaTerms.aspx">Terms and Conditions</a> | <a class="text-secondary" href="IbytaPrivacyPolicy.aspx">Privacy Policy</a> | <a class="text-secondary" href="IbytaContact.aspx">Contact Us</a>
                   </div>
                </div>
            <div class="copyright">
                    Copyright 2019 Ibyta, UAE . All Rights Reserved.
            </div>          
         </div>
</body>
</html>
