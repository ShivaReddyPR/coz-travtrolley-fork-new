﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpCreate.aspx.cs" Inherits="CozmoB2BWebApp.ExpCreate" %>

<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="System.Security.AccessControl" %>
<%@ Import Namespace="System.IO" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    
    <link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />    
    <link href="css/toastr.css" rel="stylesheet" type="text/css" />
    <link href="DropzoneJs_scripts/dropzone.css" rel="stylesheet" />

    <style>
        /**THE SAME CSS IS USED IN ALL 3 DEMOS**/
        /**gallery margins**/
        ul.gallery {
            margin-left: 3vw;
            margin-right: 3vw;
        }

        .zoom {
            -webkit-transition: all 0.35s ease-in-out;
            -moz-transition: all 0.35s ease-in-out;
            transition: all 0.35s ease-in-out;
            cursor: -webkit-zoom-in;
            cursor: -moz-zoom-in;
            cursor: zoom-in;
        }

            .zoom:hover,
            .zoom:active,
            .zoom:focus {
                /**adjust scale to desired size, 
        add browser prefixes**/
                -ms-transform: scale(2.5);
                -moz-transform: scale(2.5);
                -webkit-transform: scale(2.5);
                -o-transform: scale(2.5);
                transform: scale(2.5);
                position: relative;
                z-index: 100;
            }

        /**To keep upscaled images visible on mobile, 
        increase left & right margins a bit**/
        @media only screen and (max-width: 768px) {
            ul.gallery {
                margin-left: 15vw;
                margin-right: 15vw;
            }

            /**TIP: Easy escape for touch screens,
        give gallery's parent container a cursor: pointer.**/
            .DivName {
                cursor: pointer
            }
        }
    </style>
    
    <link href="scripts/EasyAutocomplete-1.3.5/easy-autocomplete.min.css" rel="stylesheet" />
    <link href="scripts/EasyAutocomplete-1.3.5/easy-autocomplete.themes.min.css" rel="stylesheet" />

    <script src="scripts/EasyAutocomplete-1.3.5/jquery.easy-autocomplete.min.js"></script>    
    <script src="DropzoneJs_scripts/dropzone.js"></script>    
    <script src="scripts/jquery-ui.js"></script>
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="scripts/Common/ExpenseObjects.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>

    <script type="text/javascript">

        /* Global variables */
        var mt = ''; var apiHost = mt; var bearer = mt; var cntrllerPath = 'api/expTransactions';         

        /* Expense header variables */
        var apiAgentInfo = {}; var expenseReport = {}; var expCustomers = {}; var webAppHost = mt;
        var expRepId = 0; var selectedProfile = 0; var empCostCentre = 0; var displayMode = mt;
        var agentCurrrency = mt; var agentName = mt; var expAction = mt; var taxFactor = 0; var enableSubmit = mt;

        /* Expense details variables */        
        var expRepDetails = []; var selExpDetails = {}; var selExpDtlId = 0; var dcAction = mt; var expRepDtlMsgs = []; 
        var selExpDetailIds = []; var allExpDetailIds = []; var copyFlag = false; var expRepMessages = [];

        /* To clear expense details global variables */        
        function ClearExpDetailsVariables() {

            expRepDetails = []; selExpDetails = {}; selExpDtlId = 0; dcAction = mt; expRepDtlMsgs = [];
            selExpDetailIds = []; allExpDetailIds = []; copyFlag = false; expRepMessages = [];
        }

        /* Expense flex variables */        
        var fielddivhtml = '<div id="divid" class="form-group col-md-colwidth" display>@req<label>labelname</label>inputcntrl</div>';
        var defseloption = '<option selected="selected" value="">--Select--</option>';  var reqClass = 'fieldreq';
        var expCategories = {}; var expTypes = {}; var selExpType = {};
        var defFlex = {}; var normalFlex = {}; var itemFlex = {}; 
        var dateCntrils = []; var dllFeilds = []; var GroupFeilds = [];        

        /* To clear expense flex global variables */        
        function ClearFlexVariables() {

            selExpType = {}; defFlex = {}; normalFlex = {}; itemFlex = {}; 
            dateCntrils = []; dllFeilds = []; GroupFeilds = [];        
        }

        /* Expense policy variables */        
        var policyMaster = []; var fixPolicyExc = [];

        /* To clear expense policy global variables */        
        function ClearPolicyVariables() {

            policyMaster = []; fixPolicyExc = [];
        }

        /* Attendee variables */        
        var attCount = 0; var attendeeBal = 0; var attendeeMaster = {}; var loadMode = mt; var NewCompanies = [];
        var NewAttendeeData = []; var selAMIds = []; var selExpAttendees = []; var expAttendeeIds = [];

        /* To clear expense attendee global variables */        
        function ClearAttendeeVariables() {

            attCount = 0; attendeeBal = 0; attendeeMaster = {}; loadMode = mt; NewCompanies = [];
            NewAttendeeData = []; selAMIds = []; selExpAttendees = []; expAttendeeIds = [];   
        }

        /* Expense receipts variables */        
        var fileUploadPath = mt; var allUploadedRceipts = []; var selUploadedRceipts = []; 
        var allopenReceiptsIds = []; var openReceiptsList = []; var selOpenReceipts = []; var openReceiptsDtls = [];
        var repAction = mt;

        /* To clear expense receipts global variables */        
        function ClearReceiptsVariables() {

            allUploadedRceipts = []; selUploadedRceipts = []; 
            allopenReceiptsIds = []; openReceiptsList = []; selOpenReceipts = []; openReceiptsDtls = [];
            repAction = mt;  
        }
        
        /* To clear all global variables */     
        function ClearGlobalVariables() {

            ClearExpDetailsVariables();
            ClearFlexVariables();
            ClearPolicyVariables();
            ClearAttendeeVariables();
            ClearReceiptsVariables();
            $('#tpl-container').children().remove();
            $('.dz-filename').remove();
            $('#tbOpenReceipts').children().remove();
            $('#tbUploadedReceipts').children().remove();
            $('#errMess').children().remove();
            $('#expense-tab').children().remove();
        }

        /* To enable view mode of screen */
        function EnableViewMode() {
                   
            var modifyStatus = ('O|RS|RV').split('|');

            if (enableSubmit == 'N' || (!IsEmpty(selExpDetails) && !IsEmpty(selExpDetails.eD_APPROVALSTATUS) &&
                modifyStatus.indexOf(selExpDetails.eD_APPROVALSTATUS) == -1)) {

                $('#divDelSubmit').attr('style', 'display:none !important');
                $('#divAddDelReceipts').hide();
                $('#divCopyDelete').hide();                
                $('#divSaveClear').hide();
                $('#btnSelNewAttendee').hide();
                $('#btnAddNewAttendee').hide();
                $('#btnDelAttendee').hide();
                $('#divdz').hide();                
                $('#lnkExpType').attr('style', 'pointer-events: none;');
                $('#lnkTaxCalc').attr('style', 'pointer-events: none;');
            }

            if (!IsEmpty(selExpDetails) && !IsEmpty(selExpDetails.eD_APPROVALSTATUS))
                $('#lnkExpType').attr('style', 'pointer-events: none;');
        }

        /* Page Load */
        $(document).ready(function () {

            /* Check query string to see if existing report needs to open */
            <%--expRepId = '<%=Request.QueryString["ExpRepId"] == null ? "0" : Request.QueryString["ExpRepId"]%>';
            displayMode = '<%=Request.QueryString["Mode"] == null ? "0" : Request.QueryString["Mode"]%>';
            selExpDtlId = '<%=Request.QueryString["ExpDtlId"] == null ? "0" : Request.QueryString["ExpDtlId"]%>';--%>

            /* Check session string to see if existing report needs to open */
            var pageParams = JSON.parse(AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpCreate', 'sessionData':'', 'action':'get'}"));
            expRepId = displayMode = selExpDtlId = 0;
            if (!IsEmpty(pageParams)) {

                expRepId = !IsEmpty(pageParams.ExpRepId) ? pageParams.ExpRepId : 0;
                displayMode = !IsEmpty(pageParams.Mode) ? pageParams.Mode : 0;
                selExpDtlId = !IsEmpty(pageParams.ExpDtlId) ? pageParams.ExpDtlId : 0;
            }

            webAppHost = '<%=Request.Url.Scheme + "://" + Request["HTTP_HOST"]%>';

            /* Check the expense report ref no and revert if not valid */
            if (expRepId == 0 && selExpDtlId == 0) {

                ShowError('Invalid expense report reference no.');
                return;
            }

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {

                ShowError('Session expired, please login once again.');
                return;
            }

            var pathError = mt;

            fileUploadPath = '<%=ConfigurationManager.AppSettings["ExpenseReceiptsFilePath"]%>';

            if (IsEmpty(fileUploadPath)) 
                pathError = 'Please configure path to upload files.';

            if (IsEmpty(pathError) && CompFields('<%= Directory.Exists(ConfigurationManager.AppSettings["ExpenseReceiptsFilePath"])%>', 'False'))
                pathError = 'File upload path not found to upload files.';

            if (IsEmpty(pathError) && CompFields('<%= GenericStatic.DirectoryHasPermission(ConfigurationManager.AppSettings["ExpenseReceiptsFilePath"], FileSystemRights.Write)%>', 'False'))
                pathError = 'Access denied to path to upload files.';

            if (!IsEmpty(pathError)) {

                ShowError(pathError);
                $('#divOpenReceipts').hide();
                $('#divdz').hide();
            }

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            /* Load screen data */
            GetScreenData('O');              

            /* Enable drop zone */
            SetDropZoneControl('expense-dropzone', 'hn_ExpenseReceiptUploader.ashx', 'tpl-container', null, 3);
        });

        /* To get expense info and corp profiles */
        function GetScreenData(action) {

            expAction = action;

            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expRepId };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getExpCreateData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);                        
        }

        /* To bind expense info and corp profiles */
        function BindScreenData(screenData) {
                        
            var sMsg = mt;
            if (IsEmpty(screenData.categories) || IsEmpty(screenData.exptypes)) 
                sMsg = 'Expense categories / types info missing';

            if (IsEmpty(screenData.expreport))
                sMsg = 'Expense header info missing';

            if (!IsEmpty(sMsg)) {

                ShowError(sMsg);
                return;
            }

            BindExpHeader(screenData.expreport);
            BindExpDetailsGrid(screenData.expdetails);
            BindExpDetailsMsgs(screenData.expmessages, 'SL');
            BindExpCatTypes(screenData.categories, screenData.exptypes);     
        }

        /* To bind expense header data */
        function BindExpHeader(repdata) {

            expenseReport = repdata[0];
            $('#hdrTitle').html(expenseReport.eR_Title);
            $('#divhdrDate').html(GetDateFormat(expenseReport.eR_Date, '/', 'ddmmyyyy'));
            $('#divhdrRefNo').html(expenseReport.eR_RefNo);
            $('#divhdrEmpCode').html(expenseReport.employeeId);
            $('#divhdrEmpName').html(expenseReport.empname);
            $('#divhdrCostCentre').html(expenseReport.ccName);
        }        

        /* To bind expense details messages */
        function BindExpDetailsMsgs(expDtlMsgs, action) {

            if (action == 'SL')
                $('#errMess').children().remove();

            if (IsEmpty(expDtlMsgs) || expDtlMsgs.length == 0)
                return;

            expRepDtlMsgs = expDtlMsgs.filter(x => x.erM_ED_Id !== selExpDtlId);
            $.each(expRepDtlMsgs, function (key, dtlCol) {

                var expdtltype = expRepDetails.find(x => x.eD_Id == dtlCol.erM_ED_Id);

                if (dtlCol.erM_Message.toUpperCase().indexOf('WARNING') == -1)
                    $('#errMess').show().append('<div class="alert-messages px-4 py-1 mb-2 text-danger divMsg' + dtlCol.erM_ED_Id + '"><span class="icon icon-warning pr-3"></span>' + expdtltype.eT_Desc + ' - ' + dtlCol.erM_Message + '</div>');
                else
                    $('#errMess').show().append('<div class="alert-messages px-4 py-1 mb-2 text-success divMsg' + dtlCol.erM_ED_Id + '"><span class="icon icon-info pr-3"></span>' + expdtltype.eT_Desc + ' - ' + dtlCol.erM_Message + '</div>');
            });            
        }        

        /* To bind expense details grid */
        function BindExpDetailsGrid(expdetails) {

            if (IsEmpty(expdetails) || expdetails.length == 0)
                return;

            enableSubmit = 'N';
            expRepDetails = expdetails;
            $('#tbExpDetails').children().remove();
                        
            $.each(expRepDetails, function (key, dtlCol) {

                enableSubmit = enableSubmit == 'N' && dtlCol.eD_APPROVALSTATUS != 'A' &&
                    dtlCol.eD_APPROVALSTATUS != 'RJ' && dtlCol.eD_APPROVALSTATUS != 'S' && dtlCol.eD_APPROVALSTATUS != 'R' && dtlCol.eD_APPROVALSTATUS != 'I' ? 'Y' : enableSubmit;
                AddExpDetailsRowHtml(dtlCol.eD_Id, dtlCol.transDate, dtlCol.eT_Desc, dtlCol.eD_TotalAmount, dtlCol.eD_ReceiptStatus);                
            });            

            if (expenseReport.eR_Aprroved == 'S')
                $('#divCopyDelete').hide();

            if (enableSubmit == 'N')
                EnableViewMode();
        }

        /* To get expense details grid row html */
        function AddExpDetailsRowHtml(eDId, eDTransDate, eTDesc, eDTotalAmount, eDRS) {

            allExpDetailIds.push(eDId);
            var detailsHtml = '<tr id="trExpDtl' + eDId + '">';

            detailsHtml += '<td><div class="custom-checkbox-style dark"><input onclick="SelectExpDetail(this, ' + eDId + ')" id="chk' + eDId + '" type="checkbox" class="form-control"><label></label></div></td>';
            detailsHtml += '<td>' + eDTransDate + '</td>';
            detailsHtml += '<td><a onclick="GetSelExpDetails(' + eDId + ')">' + eTDesc + '</a></td>';
            detailsHtml += '<td>' + eDTotalAmount + '</td>';
            detailsHtml += '<td>' + ((IsEmpty(eDRS) || eDRS == 'NR') ? ('<a onclick="ShowAffidavit(this, ' + eDId + ')">Generate</a>') : mt) + '</td>';
            detailsHtml += '</tr>';
            $('#tbExpDetails').append(detailsHtml);
        }

        /* To delete expense details grid row html */
        function DelExpDetailsRowHtml(eDId) {

            allExpDetailIds = allExpDetailIds.filter(x => x !== eDId);
            expRepDtlMsgs = expRepDtlMsgs.filter(x => x.erM_ED_Id !== eDId);
            $('#trExpDtl' + eDId).remove();
            $('.divMsg' + eDId).remove();
            $('#chkAllExpDtlGrid').prop('checked', false);
        }

        /* To select all expenses in expense details grid */
        function SelectAllExpenses(event) {

            selExpDetailIds = event.checked ? allExpDetailIds : [];
            $("#tbExpDetails").find('input[type=checkbox]').each(function () { this.checked = event.checked; });
        }

        /* To add selected expense detail ids */
        function SelectExpDetail(event, eDId) {

            if (event.checked)
                selExpDetailIds.push(eDId);
            else
                selExpDetailIds = selExpDetailIds.filter(x => x !== eDId);
            $('#chkAllExpDtlGrid').prop('checked', selExpDetailIds.length == allExpDetailIds.length);
        }

        /* To populate selected expense details in edit mode */
        function GetSelExpDetails(eDId) {

            AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpCreate', 'sessionData':'" + JSON.stringify({ ExpRepId: expRepId, ExpDtlId: eDId }) + "', 'action':'set'}");
            window.location.href = "ExpCreate.aspx";
        }

        /* To delete expense details from report */
        function DelSelExpDetails() {

            if (selExpDetailIds.length == 0) {

                ShowError('Please select expense to delete.');
                return;
            }

            var reqData = { AgentInfo: apiAgentInfo, ExpDtlIds: selExpDetailIds };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/removeExpDtlData';
            WebApiReq(apiUrl, 'POST', reqData, '', RefreshExpDetails, null, null);                
        }

        /* To delete expense details from report */
        function RefreshExpDetails() {

            if (selExpDetailIds.length == 0) 
                return;

            $.each(selExpDetailIds, function (key, dtId) {

                expRepDetails = expRepDetails.filter(x => x.eD_Id !== dtId);
                DelExpDetailsRowHtml(dtId);
            });
            selExpDetailIds = [];
        }

        /* To copy expense details from report */
        function CopySelExpDetails() {

            if (selExpDetailIds.length == 0) {

                ShowError('Please select expense to copy.');
                return;
            }

            if (selExpDetailIds.length > 1) {

                ShowError('Please select one expense to copy.');
                return;
            }

            var currSelId = selExpDetailIds[0];
            ClearGlobalVariables();
            copyFlag = true;
            selExpDtlId = currSelId;            
            GetScreenData('O');           
            $('#showSidePanel').click();
        }

        /* To show affidavit for selected expense type */
        function ShowAffidavit(event, etID) {

            AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpAffidavit', 'sessionData':'" + JSON.stringify({ ExpDtlId: etID }) + "', 'action':'set'}");
            window.open('ExpAffidavit.aspx', 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes');
        }

        /* To bind expense categories and types pop up */
        function BindExpCatTypes(catdata, typesdata) {
            
            expCategories = catdata;
            expTypes = typesdata;

            $('#divExpCatTypes').children().remove();
                        
            var expCatHtml = '';

            $.each(expCategories, function (key, col) {

                expCatHtml += '<ul class="list p-4"><li class="cat-title"><strong>' + col.eC_Desc + '</strong></li>'

                var expCatTypes = expTypes.filter(item => item.eT_EC_Id === col.eC_ID);
                $.each(expCatTypes, function (key, exptype) {
                   expCatHtml += '<li><a href="javascript:void(0)" onclick="GetExpTypeFields(' + exptype.eT_ID + ', 0)">' + exptype.eT_Desc + '</a></li>';
                });

                expCatHtml += "<\/ul>";

            });

            $('#divExpCatTypes').append(expCatHtml);

            if (selExpDtlId == 0 && expenseReport.eR_Aprroved == 'S') {

                if (expAction != 'S' && enableSubmit == 'Y') {

                    ShowError('Expense report already submitted, cannot create new expenses.');
                    $('#divSaveClear').hide();
                }

                $('#showSidePanel').click();
                return false;
            }

            if (selExpDtlId > 0) {

                var etypeId = expRepDetails.find(x => x.eD_Id == selExpDtlId);
                GetExpTypeFields(etypeId.eD_ET_Id, selExpDtlId);
            }
            else {

                $('#expense-tab').children().remove();

                var fieldsHtml = '<div class="form-row"><div class="form-group col-md-4"><label>Expense Type</label>' +
                    '<span class="input-group-text" id=""><a onclick="ShowExpTypes();"><u> Select Expense Type </u></a></span></div></div>';           

                $('#expense-tab').append(fieldsHtml);

                if (expAction == 'O')
                    $('#divExpTypesPopup').modal('show');

		        $("#ctl00_upProgress").removeAttr('style');
            }
        }

        /* To get expense type fields data */
        function GetExpTypeFields(etID, DtlId) {

            if (!IsEmpty(selExpType.eT_ID) && selExpType.eT_ID == etID)
                return;

            selExpType = expTypes.find(item => item.eT_ID === etID);

            var reqData = { AgentInfo: apiAgentInfo, ExpTypId: etID, ExpDtlId: DtlId };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getExpTypeFieldsData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindExpFields, null, null);                        
        }

        /* To bind expense details grid */
        function BindExpFields(details) {

            var fieldsData = details.data;
            if (IsEmpty(fieldsData.defaultflex)) {

                ShowError('Expense type fields missing');
                return;
            }

            ClearExpDetails();            
            selExpDetails = !IsEmpty(details.expDataDtls) ? details.expDataDtls : {};
            BindDefFlex(fieldsData.defaultflex, fieldsData.normalflex);

            if (!IsEmpty(fieldsData.itemflex))
                BindItemFlex(fieldsData.itemflex);

            policyMaster = !IsEmpty(fieldsData.policymaster) ? fieldsData.policymaster : [];
            expCustomers = !IsEmpty(fieldsData.customers) ? fieldsData.customers : [];
            taxFactor = !IsEmpty(fieldsData.taxinfo) && !IsEmpty(fieldsData.taxinfo[0].taxfactor) ? fieldsData.taxinfo[0].taxfactor : 0;
            fixPolicyExc = !IsEmpty(fieldsData.fpcurrency) ? fieldsData.fpcurrency : [];

            PostFiledBinding();
            $('#divExpTypesPopup').modal('hide');
            $('select').select2();
        }

        /* To set default flex field values */
        function PostFiledBinding() {

            EnableViewMode();
            EnableCalender();
            AddNewAttendeeRowHtml(GetSplitVal(expenseReport.empname, ' ', 0), expenseReport.designation, expenseReport.designation, 0.00, agentName, -1, 0, GetSplitVal(expenseReport.empname, ' ', 1));
            SetddlValues();
            SetDefaultValues();
            if (selExpDetails.eD_ExcRate == null || selExpDetails.eD_ExcRate == 0) { CurrencyOnChange($('#ddlCurrency').val()); }
            else CurrencyOnChange(selExpDetails.eD_ExcRate);
            IsBillableClick(GetExpFieldData('Is Billable'));
            PaymentTypeOnChange(GetddlSelText('ddlPaymentType'), GetExpFieldData('Card Name'), 'SL');
            SetReceiptStatus();
            if (copyFlag == false)
                BindUploadedReceipts();
            PolicyMessages();
            BindSelExpAttendees();
        }

        /* To set values for default fields in case of new expense */
        function SetDefaultValues() {

            if (!IsEmpty(selExpDetails) && !IsEmpty(selExpDetails.eD_ET_Id))
                return;

            Setdropval('ddlPaymentType', 'CASH', 'text');
            $("#txtTransactionDate").val(GetDateFormat(new Date(), '-', 'ddmmyyyy'));
        }

        /* To reset selected expense details variables */
        function ResetSelDetails() {

            if (copyFlag) { selExpDetails = {}; selExpDtlId = 0; }            
        }

        /* To set expense field drop down values */
        function SetddlValues() {

            if (IsEmpty(dllFeilds) || dllFeilds.length == 0)
                return;

            $.each(dllFeilds, function (key, colName) {

                Setdropval(colName.ddl_Id, colName.ddl_Value, mt);
            });

            var currValue = !IsEmpty(selExpDetails) ? GetExpFieldData('Currency') : agentCurrrency;
            Setdropval('ddlCurrency', currValue, 'text');
            Setdropval('ddlCityOfPurchase', GetCityInfoFromddl(GetExpFieldData('City Of Purchase'), 'V'), '');
        }

        /* To set receipt status */
        function SetReceiptStatus() {
            

            if ((selExpType.eT_ReceiptNotReq == 'Y' && IsEmpty(selExpDetails.eD_ReceiptStatus)) ||
                (!IsEmpty(selExpDetails) && !IsEmpty(selExpDetails.eD_ET_Id) && selExpDetails.eD_ET_Id > 0 && IsEmpty(selExpDetails.eD_ReceiptStatus)))
                Setdropval('ddlReceiptStatus', 'NR', mt);
            else if (expenseReport.affidivit != '1') {

                Setdropval('ddlReceiptStatus', 'RU', mt);
                $('#ddlReceiptStatus').attr('disabled', 'disabled');
            }
        }

        /* To get city name from ddl */
        function GetCityInfoFromddl(cityId, type) {

            if (IsEmpty(cityId) || cityId == 0)
                return mt;

            var cities = $('#ddlCityOfPurchase option');
            var cityinfo = mt;
                        
            $.each(cities, function (key, colName) { if (!IsEmpty(colName.value) && GetSplitVal(colName.value, '-', 0) == cityId) { cityinfo = type == 'V' ? colName.value : colName.text; return; } });

            return cityinfo;
        }

        /* To enable calender for date fields */
        function EnableCalender() {

            if (!IsEmpty(policyMaster) && !IsEmpty(policyMaster.length) && policyMaster.length > 0 && policyMaster.find(x => x.epM_AllowFutureDate == 'N')) {

                dateCntrils = dateCntrils.filter(x => x !== 'txtTransactionDate');
                $("#txtTransactionDate").datepicker({ dateFormat: 'dd-mm-yy', maxDate: 0 });   
            }
            
            $.each(dateCntrils, function (key, colName) {

                $("#" + colName).datepicker({ dateFormat: 'dd-mm-yy' });   
            });
        }        

        /* To bind expense details grid */
        function BindDefFlex(defFlexData, norFlexData) {
                        
            defFlex = defFlexData;
            normalFlex = !IsEmpty(norFlexData) ? norFlexData : normalFlex;

            $('#expense-tab').children().remove();
            
            var pendingNormalFlex = normalFlex;
            
            var newRowHtml = '</div><div class="form-row">';

            var fieldsHtml = '<div class="form-row"><div class="form-group col-md-4"><label>Expense Type</label><span class="input-group-text" id="">' +
                '<a id="lnkExpType" onclick="ShowExpTypes();"><u>' + selExpType.eT_Desc + '</u></a></span></div>';

            $.each(defFlex, function (key, colName) {

                fieldsHtml += colName.eF_IsNewRow == 'Y' ? newRowHtml : mt;
                fieldsHtml += GetFlexHtml(colName);
                fieldsHtml += GetAddtionalHtml(colName);

                if (!IsEmpty(normalFlex) && normalFlex.length > 0 && normalFlex.find(x => x.eF_OrderDependId == colName.eF_ID)) {

                    var filternormalflex = normalFlex.filter(x => x.eF_OrderDependId == colName.eF_ID);
                    $.each(filternormalflex, function (key, norFlexName) {

                        pendingNormalFlex = pendingNormalFlex.filter(x => x.eF_ID !== norFlexName.eF_ID);
                        fieldsHtml += norFlexName.eF_IsNewRow == 'Y' ? newRowHtml : mt;
                        fieldsHtml += GetFlexHtml(norFlexName);
                    });
                }
            });

            fieldsHtml += newRowHtml;

            $.each(pendingNormalFlex, function (key, colName) {

                fieldsHtml += colName.eF_IsNewRow == 'Y' ? newRowHtml : mt;
                fieldsHtml += GetFlexHtml(colName);
                fieldsHtml += GetAddtionalHtml(colName);
            });

            fieldsHtml += '</div>';            
            $('#expense-tab').append(fieldsHtml);
        }

        /* To bind expense details grid */
        function BindItemFlex(itemFlexes) {
                        
            itemFlex = itemFlexes;

            $('#itemize-tab').children().remove();
            
            var newRowHtml = '</div><div class="form-row">';

            var fieldsHtml = '<div class="form-row">';

            $.each(itemFlex, function (key, colName) {

                fieldsHtml += colName.eF_IsNewRow == 'Y' ? newRowHtml : '';
                fieldsHtml += GetFlexHtml(colName);
            });

            fieldsHtml += '</div>';            
            $('#itemize-tab').append(fieldsHtml);
        }

        /* To get adjacent html's for few default fields */
        function GetAddtionalHtml(colName) {
            
            if (CompFields(colName.eF_Label, 'Total Amount') && selExpType.eT_EnableItemize == 'Y')
                return '<div class="form-group col-md-3 mt-5"><a href="javascript:void(0)" id="btnItemize" class="btn btn-info show-itemize">ITEMIZE</a></div>';

            if (CompFields(colName.eF_Label, 'Customer') && selExpType.eT_EnableAttendee == 'Y')
                return '<div id="divAttend" class="form-group col-md-3 mt-5"><a href="javascript:void(0)" class="btn btn-info btn-add-attendees" type="button"' +
                    ' data-toggle="collapse" data-target="#attendeesCollapse" aria-expanded="false" aria-controls="attendeesCollapse" > Add Attendees</a ></div>';

            if (CompFields(colName.eF_Label, 'Claim Expense') && selExpType.eT_EnableAttendee == 'Y')
                return divAttendeeHTML.replace(/@currency/g, agentCurrrency);

            return mt;
        }

        /* To get append html's for few default fields */
        function GetAppendHtml(colName) {
            
            if (CompFields(colName.eF_Label, 'Tax Amount') && selExpType.eT_EnableTaxCalc == 'Y')
                return '<div id="divCalTax" class="input-group-append"><span class="input-group-text" id=""><a id="lnkTaxCalc" onclick="CalculateTAX();"><u>Calculate</u></a></span></div>';
            
            if (CompFields(colName.eF_Label, 'Exchange Rate'))
                return '<div id="divShowExc" class="input-group-append"><span id=""><a id="lnkExcRate" target="_blank" href="https://www1.oanda.com/currency/converter/"><u>Click to know exchange rates</u></a></span></div>';

            return mt;
        }

        /* To get default field html */
        function GetFlexHtml(colName) {

            var fieldval = GetFieldValue(colName);
            var fieldsHtml = mt;

            if (CompFields(colName.eF_Control, 'TEXTBOX') || CompFields(colName.eF_Control, 'DATE') || CompFields(colName.eF_Control, 'TEXTAREA')) 
                fieldsHtml += GetTextInputHtml(colName, '1000', GetOnchangeFunction(colName), fieldval);

            if (CompFields(colName.eF_Control, 'CHECKBOX'))                 
                fieldsHtml += GetCheckBoxInputHtml(colName, GetOnchangeFunction(colName), fieldval);

            if (CompFields(colName.eF_Control, 'DDL'))                 
                fieldsHtml += GetddlInputHtml(colName, GetOnchangeFunction(colName), fieldval);                                        

            return fieldsHtml;
        }

        /* To get expense details field data */
        function GetExpFieldData(fieldname) {

            return CompFields(fieldname, 'Transaction Date') ? GetDateFormat(selExpDetails.eD_TransDate, '-', 'ddmmyyyy') :
                CompFields(fieldname, 'Payment Type') ? selExpDetails.eD_PT_Id : CompFields(fieldname, 'Card Name') ? selExpDetails.eD_CM_Id :
                    CompFields(fieldname, 'Receipt Status') ? selExpDetails.eD_ReceiptStatus : CompFields(fieldname, 'City Of Purchase') ? selExpDetails.eD_ECT_Id :
                        CompFields(fieldname, 'UOM') ? selExpDetails.eD_UOM : CompFields(fieldname, 'Units') ? selExpDetails.eD_Units :
                            CompFields(fieldname, 'Unit Amount') ? selExpDetails.eD_UnitPrice : CompFields(fieldname, 'Currency') ? selExpDetails.eD_Currency :
                                CompFields(fieldname, 'Exchange Rate') ? selExpDetails.eD_ExcRate : CompFields(fieldname, 'Amount In') ? selExpDetails.eD_ExcAmount :
                                    CompFields(fieldname, 'Tax Amount') ? selExpDetails.eD_Tax : CompFields(fieldname, 'Total Amount') ? selExpDetails.eD_TotalAmount :
                                        CompFields(fieldname, 'Is Billable') ? selExpDetails.eD_IsBillable :
                                            CompFields(fieldname, 'Claim Expense') ? selExpDetails.eD_ClaimExpense :
                                                CompFields(fieldname, 'Comments') ? selExpDetails.eD_Comment : mt;
        }

        /* To get the default fields default value */
        function GetFieldValue(colName) {

            var defValue = mt;

            if (colName.eF_Type == 'D') {

                if (!IsEmpty(selExpDetails.eD_ET_Id))
                    defValue = GetExpFieldData(colName.eF_Label);
                else {
                                        
                    if (CompFields(colName.eF_Label, 'Claim Expense'))
                        defValue = true;

                    if (CompFields(colName.eF_Label, 'UOM'))
                        defValue = selExpType.eT_UOM;

                    if (CompFields(colName.eF_Label, 'Exchange Rate'))
                        defValue = 1;

                    if (CompFields(colName.eF_Label, 'Total Amount') || CompFields(colName.eF_Label, 'Amount In') || CompFields(colName.eF_Label, 'Tax Amount') || CompFields(colName.eF_Label, 'Unit Amount') || CompFields(colName.eF_Label, 'Units'))
                        defValue = AgentDecimalRound(0);
                }
            }

            if (colName.eF_Type != 'D' && !IsEmpty(selExpDetails.expFlexDatas) && selExpDetails.expFlexDatas.length > 0) {

                var selFlex = selExpDetails.expFlexDatas.find(x => x.efD_EF_Id == colName.eF_ID);
                defValue = !IsEmpty(selFlex) ? selFlex.efD_Data : mt;
            }

            return defValue;
        }

        /* To get the on change/click function name for the dynamic fields */
        function GetOnchangeFunction(colName) {

            var fnName = mt;

            if (colName.eF_Type == 'D') {
                                
                if (CompFields(colName.eF_Label, 'Transaction Date'))
                    fnName = "ValDuplicateExp('TD')";

                if (CompFields(colName.eF_Label, 'Payment Type'))
                    fnName = "PaymentTypeOnChange(event.added.text, '', 'OC')";

                if (CompFields(colName.eF_Label, 'City Of Purchase'))
                    fnName = 'CityOnChange(event.val)';

                if (CompFields(colName.eF_Label, 'Currency'))
                    fnName = 'CurrencyOnChange(event.val)';

                if (CompFields(colName.eF_Label, 'Customer'))
                    fnName = 'CustomerOnChange(event)';

                if (CompFields(colName.eF_Label, 'Is Billable'))
                    fnName = 'IsBillableClick(this.checked)';

                if (CompFields(colName.eF_Label, 'Units'))
                    fnName = 'UnitsOnChange()';

                if (CompFields(colName.eF_Label, 'Tax Amount') || CompFields(colName.eF_Label, 'Exchange Rate') ||
                    CompFields(colName.eF_Label, 'Total Amount') || CompFields(colName.eF_Label, 'Unit Amount')) {

                    fnName = 'CalculateTotal()';
                }
            }

            if (colName.eF_Type != 'D') {

                if (colName.eF_Type == 'N' && normalFlex.find(x => x.eF_DisplayDependId == colName.eF_ID)) {

                    var depField = normalFlex.find(x => x.eF_DisplayDependId == colName.eF_ID);
                    var cntrlvaltype = CompFields(colName.eF_Control, 'TEXTBOX') ? 'e.target.value' : CompFields(colName.eF_Control, 'DDL') ? 'e.val' : 'e.checked';
                    var depFieldType = CompFields(colName.eF_Control, 'TEXTBOX') ? 'txt' : CompFields(colName.eF_Control, 'DDL') ? 'ddl' : 'chk';
                    fnName = "BindFlexdynamicddl(" + cntrlvaltype + ", '" + depFieldType + GetCntrlId(label) + "', '" + depField.eF_SQLQuery + "', '" + depFieldType + "');";
                }

                if (colName.eF_Type == 'I' && itemFlex.find(x => x.eF_DisplayDependId == colName.eF_ID)) {

                    var depField = normalFlex.find(x => x.eF_DisplayDependId == colName.eF_ID);
                    var cntrlvaltype = CompFields(colName.eF_Control, 'TEXTBOX') ? 'e.target.value' : CompFields(colName.eF_Control, 'DDL') ? 'e.val' : 'e.checked';
                    var depFieldType = CompFields(colName.eF_Control, 'TEXTBOX') ? 'txt' : CompFields(colName.eF_Control, 'DDL') ? 'ddl' : 'chk';
                    fnName = "BindFlexdynamicddl(" + cntrlvaltype + ", '" + depFieldType + GetCntrlId(label) + "', '" + depField.eF_SQLQuery + "', '" + depFieldType + "');";
                }                
            }

            return fnName;
        }

        /* To get flex input html for text box control */
        function GetTextInputHtml(colName, datalength, onchangefn, fieldval) {

            var label = colName.eF_Label;            
            var fieldwidth = IsEmpty(colName.eF_FieldWidth) ? '2' : colName.eF_FieldWidth;
            var datatype = "'" + colName.eF_DataType + "'";
            datalength = "'" + datalength + "'";
            var cntrlid = GetCntrlId(label);

            if (CompFields(colName.eF_DataType, 'DT'))
                dateCntrils.push('txt' + cntrlid);

            if (CompFields(colName.eF_GroupFlag, 'Y'))
                GroupFeilds.push('txt' + cntrlid);

            var startTag = CompFields(colName.eF_Control, 'TEXTAREA') ? 'textarea' : 'input';
            var endTag = CompFields(colName.eF_Control, 'TEXTAREA') ? '</textarea>' : mt;
            var inputtype = CompFields(colName.eF_Control, 'TEXTAREA') ? mt : 'type="text"';
            var txtAreaVal = CompFields(colName.eF_Control, 'TEXTAREA') ? fieldval : mt;
            fieldval = CompFields(colName.eF_Control, 'TEXTAREA') ? mt : fieldval;

            label = CompFields(colName.eF_Label, 'Amount In') ? colName.eF_Label + ' ' + agentCurrrency : colName.eF_Label;

            var fnchange = (IsEmpty(onchangefn) && datatype == "'DT'") ? "return validateData(this.id, " + datatype + ", " + datalength + ", '" + label + "');" : onchangefn;
            var keypress = datatype == "'DT'" ? mt : "return onKeyPressVal(event, " + datatype + ", " + datalength + ");";

            var ctrlhtml = CompFields(colName.eF_Label, 'Tax Amount') ? '<div class="input-group mb-3">' : mt;

            ctrlhtml += '<' + startTag + ' id="txt' + cntrlid + '" ' + inputtype + ' class="form-control" placeholder="Enter ' + label +
                '" onkeypress="' + keypress + '" onchange="' + fnchange + '" ' + (colName.eF_IsDisable == 'Y' ? 'disabled="disabled" ' : mt) +
                (IsEmpty(fieldval) ? '' : 'value="' + fieldval + '"') + '>' + txtAreaVal + endTag + GetAppendHtml(colName);

            ctrlhtml += CompFields(colName.eF_Label, 'Tax Amount') ? '</div>' : mt;

            return fielddivhtml.replace('colwidth', fieldwidth).replace('labelname', label).
                replace('@req', (colName.eF_Mandatory == 'Y' ? '<span class="red_span">*</span>' : mt)).replace('inputcntrl', ctrlhtml).
                replace('divid', ('div' + cntrlid)).replace('display', ((!IsEmpty(colName.eF_DisplayDependId) == true || colName.eF_HideField == 'Y') ? 'style="display:none" ' : mt));
        }

        /* To get flex input html for check box control */
        function GetCheckBoxInputHtml(colName, onchangefn, fieldval) {

            var fieldwidth = IsEmpty(colName.eF_FieldWidth) ? '2' : colName.eF_FieldWidth;
            var cntrlid = GetCntrlId(colName.eF_Label);

            var booldivhtml = '<div id="div' + cntrlid + '" class="form-group col-md-' + fieldwidth + ' custom-checkbox-style dark mt-4" ' +
                ((!IsEmpty(colName.eF_DisplayDependId) == true || colName.eF_HideField == 'Y') ? 'style="display:none" ' : mt) + '>';
            
            booldivhtml += '<input id="chk' + cntrlid + '" type="checkbox" class="form-control" ' + (!IsEmpty(onchangefn) ? 'onclick="' + onchangefn + '" ' : mt) +
                (fieldval == false ? '' : 'checked="checked"') + (colName.eF_IsDisable == 'Y' ? 'disable="disable" ' : mt) + 'name="' + cntrlid + '" >';

            booldivhtml += '<label for="' + cntrlid + '">' + colName.eF_Label + '</label></div>';

            return booldivhtml;
        }

        /* To get flex input html for drop down list control */
        function GetddlInputHtml(colName, onchangefn, fieldval) {

            var fieldwidth = IsEmpty(colName.eF_FieldWidth) ? '2' : colName.eF_FieldWidth;
            var cntrlid = GetCntrlId(colName.eF_Label);

            var selectdivhtml = '<div id="div' + cntrlid + '" class="form-group col-md-' + fieldwidth + '" ' + ((!IsEmpty(colName.eF_DisplayDependId) == true || colName.eF_HideField == 'Y') ? 'style="display:none" ' : mt) + '>';
            selectdivhtml += (colName.eF_Mandatory == 'Y' ? '<span class="red_span">*</span>' : mt) + '<label for="' + cntrlid + '">' + colName.eF_Label + '</label>';

            selectdivhtml += '<select id="ddl' + cntrlid + '" class="form-control ' + (colName.eF_Mandatory == 'Y' ? reqClass : mt) + '" onchange="' + onchangefn + '" ' +
                (colName.eF_IsDisable == 'Y' ? 'disable="disable" ' : mt) + '>';

            if (IsEmpty(colName.eF_DisplayDependId) == true) {

                dllFeilds.push(GetddlDataEntity('ddl' + cntrlid, fieldval, colName.eF_SQLQuery));
                selectdivhtml += GetDynamicddlData(colName.eF_SQLQuery, 'ddl', fieldval);
            }

            selectdivhtml += '</select></div>';

            return selectdivhtml;
        }

        /* To remove all spaces and give the text value */
        function GetddlDataEntity(ctlid, fieldval, sqlQuery) {

            return {

                ddl_Id: ctlid,
                ddl_Value: fieldval,
                ddl_Query: IsEmpty(sqlQuery) ? mt : sqlQuery.replace('<<agentid>>', apiAgentInfo.AgentId).replace('<<profileid>>', expenseReport.eR_ProfileId).
                    replace('<<cc>>', expenseReport.eR_ProfileCC).replace('<<val>>', fieldval).replace(/'/g, '^')
            };
        }

        /* To format sql query with PK values */
        function FormatSQLQuery(fieldval, sqlQuery) {

            return IsEmpty(sqlQuery) ? mt : sqlQuery.replace('<<agentid>>', apiAgentInfo.AgentId).replace('<<profileid>>', expenseReport.eR_ProfileId).
                replace('<<cc>>', expenseReport.eR_ProfileCC).replace('<<val>>', fieldval).replace(/'/g, '^');
        }

        /* To remove all spaces and give the text value */
        function GetCntrlId(label) {

            return label.replace(/ /g, mt).trim();
        }        

        /* To get data for drop down list based on sql query */
        function GetDynamicddlData(ddlquery, cntrl, selval) {

            try {

                ddlquery = FormatSQLQuery(selval, ddlquery);
                
                var ddlJsondata = IsEmpty(ddlquery) ? mt : AjaxCall('BookOffline.aspx/GetFlexddlData', "{'sddlQuery':'" + ddlquery + "'}");
                var ddldata = IsEmpty(ddlJsondata) ? mt : JSON.parse(ddlJsondata);

                if (!IsEmpty(ddldata) && ddldata.length > 0) {

                    var col = [];

                    for (var key in ddldata[0]) {
                        col.push(key);
                    }

                    var ddlhtml = mt;

                    if (cntrl == 'ddl') {

                        for (var d = 0; d < ddldata.length; d++) {

                            ddlhtml += GetddlOption(ddldata[d][col[0]], ddldata[d][col[1]]);
                        }
                    }
                    return cntrl == 'txt' ? ddldata[0][col[0]] : defseloption + ddlhtml;
                }
                else
                    return cntrl == 'txt' ? mt : defseloption;
            }
            catch (exception) {
                var excp = exception;
                return cntrl == 'txt' ? mt : defseloption;
            }
        }
        
        /* To bind flex field drop down list dynamically based on dependent flex field selected value */
        function BindFlexdynamicddl(selval, ddlid, ddlquery, cntrl) {

            try {
                                
                if (cntrl == 'ddl' && !IsEmpty(ddlquery))
                    document.getElementById(ddlid).innerHTML = GetDynamicddlData(ddlquery, cntrl, selval);
                else
                    document.getElementById(ddlid).value = !IsEmpty(ddlquery) ? GetDynamicddlData(ddlquery, cntrl, selval) : selval;
                document.getElementById(ddlid.replace(cntrl, 'div')).style.display = 'block';
                Setddlval(ddlid, mt, mt);
            }
            catch (exception) {
                var exp = exception;
            }
        }

        /* To show expense types */
        function ShowExpTypes() {

            $('#divExpTypesPopup').modal('show');
        }

        /* To show card details on payment type change */
        function PaymentTypeOnChange(text, cardId, action) {

            if (!IsEmpty(text) && text.indexOf('CARD') > -1) {

                cardId = IsEmpty(cardId) && expenseReport.isCardAllowedExpense == '1' && expenseReport.expenseCardID > 0 ? expenseReport.expenseCardID : cardId;
                var cardquery = defFlex.find(x => x.eF_Label == 'Card Name');
                BindFlexdynamicddl(cardId, 'ddlCardName', cardquery.eF_SQLQuery, 'ddl');
                if (!IsEmpty(cardId))
                    Setdropval('ddlCardName', cardId, mt);
            }
            else
                $('#divCardName').hide();
            ValidatePolicy(mt);

            if (action == 'OC')
                ValDuplicateExp('PT');
        }

        /* To calculate total on currency on change */
        function CurrencyOnChange(eventval) {

            var excAmount = IsEmpty(eventval) ? 0.00 : GetSplitVal(eventval.toString(), '-', 0);
            $('#txtExchangeRate').val(excAmount);
            PolicyMessages();
            ApplyFixedPolicy('CC');
            CalculateTotal();
        }

        /* To select currency on city change */
        function CityOnChange(event) {
                        
            Setddlval('ddlCurrency', GetSplitVal(event, '-', 1), 'text');
            CurrencyOnChange($('#ddlCurrency').val());
        }

        /* To calculate total on units change */
        function UnitsOnChange() {
                        
            CurrencyOnChange($('#ddlCurrency').val());
        }

        /* To enable customer on is billable click */
        function IsBillableClick(event) {

            if (event) {

                var custquery = defFlex.find(x => x.eF_Label == 'Customer');
                BindFlexdynamicddl(mt, 'ddlCustomer', custquery.eF_SQLQuery, 'ddl');                
                $('#divCustomer').show();
            }
            else                 
                $('#divCustomer').hide();
        }

        /* To populate attendees info */
        function CustomerOnChange(event) {

            ResetAttemdeesDiv(mt);
        }

        /* To card details on payment type change */
        function CalculateTotal() {

            var totalAmount = GetTextBoxDecVal('txtUnitAmount') * GetTextBoxDecVal('txtUnits');

            var excAmount = GetTextBoxDecVal('txtExchangeRate') * totalAmount;
            $('#txtAmountIn').val(AgentDecimalRound(excAmount));

            var excTotal = AgentDecimalRound(GetTextBoxDecVal('txtTaxAmount') + excAmount);
            $('#txtTotalAmount').val(excTotal);
                        
            SetNewAttendeeAmount();
            ValidatePolicy(mt);
        }

        /* To calculate tax */
        function CalculateTAX() {

            var excAmount = GetTextBoxDecVal('txtAmountIn');
            var taxAmount = excAmount * (taxFactor / 100);
            $('#txtTaxAmount').val(AgentFixedDecimal(taxAmount));
            CalculateTotal();
        }   

        /* To card details on payment type change */
        function CompareItemizeTotal() {

            if (IsEmpty(GroupFeilds) || GroupFeilds.length == 0)
                return true;

            var totGroupAmount = 0;
            $.each(GroupFeilds, function (key, col) { totGroupAmount += GetTextBoxDecVal(col); });

            if (totGroupAmount != GetTextBoxDecVal('txtTotalAmount')) {

                ShowError('Itemize tab fields total amount should match expense total amount');
                return false;
            }

            return true;
        }   

        /* To validate duplciate records */
        function ValDuplicateExp(action) {

            if (action == 'S' && !ValidateSave())
                return false;

            if (IsEmpty(selExpType) || IsEmpty(selExpType.eT_ID) || selExpType.eT_ID <= 0 ||
                IsEmpty($('#txtTransactionDate').val()) || IsEmpty($('#ddlPaymentType').val()) ||
                expRepId <= 0) {

                return true;
            }

            dcAction = action;

            var pkeys = ('@ED_ER_Id|@ED_TransDate|@ED_ET_Id|@ED_PT_Id|@ED_Id').split('|');
            var pvalues = (expRepId + '|' + $('#txtTransactionDate').val() + '|' + selExpType.eT_ID + '|' + $('#ddlPaymentType').val() + '|' + selExpDtlId + '').split('|');

            GetDupRecords(apiAgentInfo, apiHost, 'CT_T_EXP_REPORTDATA', pkeys, pvalues, DuplicateResp);            
        }     

        /* To read duplciate records validation response */
        function DuplicateResp(resp) {

            //if (IsEmpty(resp) || resp.length == 0) {
            if (true) { // removed unique combination checking(date,exptype,paytype)

                if (dcAction == 'S')
                    SaveExpense();
                return;
            }
            
            var msg = ('Transaction date: ' + $('#txtTransactionDate').val() + ', Expense Type: ' + selExpType.eT_Desc + ', Payment Type: ' +
                GetddlSelText('ddlPaymentType') + ' combination already exist.');
            ShowError(msg);
            return false;
        }

        /* To clear global variables of expense details */
        function ClearExpDetails() {

            selExpDetails = {}; 
            defFlex = {}; normalFlex = {}; itemFlex = {};   
            ResetAttemdeesDiv('refresh');
        }

        /* To display policy warning/block messages */
        function PolicyMessages() {

            $('#errMess').hide();
            $('#errMess').children().remove();
            expRepMessages = [];

            var selCity = GetControlSplitVal('ddlCityOfPurchase', '-', 0);
                        
            if (IsEmpty(selCity) || IsEmpty(policyMaster) || IsEmpty(policyMaster.length) || policyMaster.length == 0) 
                return;      

            var policies = policyMaster.filter(x => !IsEmpty(x.epM_ECT_Id) && x.epM_ECT_Id == selCity);

            if (IsEmpty(policies) || IsEmpty(policies.length) || policies.length == 0) 
                return;      

            var stWarnings = policies.filter(x => !IsEmpty(x.epM_WarningMsg));
            $.each(stWarnings, function (key, col) {

                $('#errMess').show().append('<div class="alert-messages px-4 py-1 text-success mb-2"><span class="icon icon-info pr-3"></span>' + selExpType.eT_Desc + ' - ' + col.epM_WarningMsg + '</div>');
                expRepMessages.push(col.epM_WarningMsg);
            });

            var fixPolicy = policies.filter(x => !IsEmpty(x.epM_FixedAmount) && Math.ceil(x.epM_FixedAmount) > 0);
            $.each(fixPolicy, function (key, policy) { PreparePolicyMessage(policy, 'F', 'Unit Amount is fixed and not editable, with '); });

            var attendeePolicy = policies.filter(x => !IsEmpty(x.epM_NoOfAttendees) && Math.ceil(x.epM_NoOfAttendees) > 0);
            $.each(attendeePolicy, function (key, policy) { PreparePolicyMessage(policy, 'A', 'Minimum no of attendees should be '); });

            var blockPolicy = policies.filter(x => !IsEmpty(x.epM_BlockAmount) && Math.ceil(x.epM_BlockAmount) > 0);
            $.each(blockPolicy, function (key, policy) { PreparePolicyMessage(policy, 'B', 'Total Amount should not be greater than '); });

            var warningPolicy = policies.filter(x => !IsEmpty(x.epM_WarningAmount) && Math.ceil(x.epM_WarningAmount) > 0);
            $.each(warningPolicy, function (key, policy) { PreparePolicyMessage(policy, 'W', 'Warning Amount should not be greater than '); });
        }

        /* To prepare policy message based on policy details */
        function PreparePolicyMessage(policyDetails, msgType, msgText) {
            
            if (!IsEmpty(policyDetails)) {

                var plAmount = msgType == 'F' ? (!IsEmpty(policyDetails.epM_FixedAmount) ? policyDetails.epM_FixedAmount : 0) :
                    (msgType == 'A' || msgType == 'B') ? (!IsEmpty(policyDetails.epM_BlockAmount) ? policyDetails.epM_BlockAmount : 0) :
                        msgType == 'W' ? (!IsEmpty(policyDetails.epM_WarningAmount) ? policyDetails.epM_WarningAmount : 0) : 0;

                if (plAmount > 0) {

                    var plcityName = !IsEmpty(policyDetails.epM_ECT_Id) ? GetCityInfoFromddl(policyDetails.epM_ECT_Id, 'T') : mt;
                    var plpaymentName = !IsEmpty(policyDetails.epM_PT_Id) ? GetddlTextOrVal('ddlPaymentType', policyDetails.epM_PT_Id, 'T', 'V') : mt;
                    var plcurrency = !IsEmpty(policyDetails.epM_Currency) ? policyDetails.epM_Currency : mt;
                    var plunits = !IsEmpty(policyDetails.epM_Units) ? policyDetails.epM_Units : mt;

                    msgText = msgType == 'A' ? msgText + policyDetails.epM_NoOfAttendees + ' with total amount of ' : msgText;
                    var selCurrencyVal = GetddlSelText('ddlCurrency');
                    selCurrencyVal = selCurrencyVal == '--Select--' ? mt : selCurrencyVal;
                    var plAmountOriginal = plAmount;

                    if (!IsEmpty(selCurrencyVal) && plcurrency != selCurrencyVal && !IsEmpty(fixPolicyExc) && fixPolicyExc.length > 0) {

                        var excFactor = fixPolicyExc.find(x => x.eM_BaseCurrency == selCurrencyVal && !IsEmpty(x.eM_Rate) && x.eM_Rate > 0);
                        plAmount = !IsEmpty(excFactor) && !IsEmpty(excFactor.eM_Rate) && excFactor.eM_Rate > 0 ? AgentFixedDecimal(plAmount / excFactor.eM_Rate) : plAmount;// mod by ziyad as using agent base currency list(policy base currency may vary)
                    }

                    var msg = msgText + (IsEmpty(selCurrencyVal) ? plcurrency : selCurrencyVal) + ' ' + plAmount
                        + (plAmount == plAmountOriginal ? mt : '(' + plcurrency + ' ' + plAmountOriginal + ')')
                        + ((IsEmpty(plunits) || plunits == 0) ? mt : ' per ' + plunits + ' units') + (IsEmpty(plpaymentName) ? mt : ' for ' + plpaymentName + ' payment')
                        + (IsEmpty(plcityName) ? mt : ' in ' + plcityName + ' city.');

                    if (msgType == 'W')
                        $('#errMess').show().append('<div class="alert-messages text-success px-4 py-1 mb-2"><span class="icon icon-info pr-3"></span>' + selExpType.eT_Desc + ' - ' + msg + '</div>');
                    else
                        $('#errMess').show().append('<div class="alert-messages text-danger px-4 py-1 mb-2"><span class="icon icon-warning pr-3"></span>' + selExpType.eT_Desc + ' - ' + msg + '</div>');

                    expRepMessages.push(msg);
                }
                else if (msgType == 'A') {

                    $('#errMess').show().append('<div class="alert-messages text-danger px-4 py-1 mb-2"><span class="icon icon-warning pr-3"></span>' + (selExpType.eT_Desc + ' - ' + msgText + policyDetails.epM_NoOfAttendees) + '</div>');
                    expRepMessages.push(msgText + policyDetails.epM_NoOfAttendees);
                }                
            }
        }

        /* To validate block/warning amount policy for all cities */
        function ValidatePolicy(action) {

            var selCityVal = GetControlSplitVal('ddlCityOfPurchase', '-', 0);
            var units = GetTextBoxDecVal('txtUnits');
            var unitAmount = GetTextBoxDecVal('txtUnitAmount');

            if (IsEmpty($('#ddlCurrency').val()) || IsEmpty(selCityVal) || Math.ceil(units) <= 0 || Math.ceil(unitAmount) <= 0)
                return true;

            var policies = policyMaster.filter(x => ((IsEmpty(x.epM_BlockAmount) && IsEmpty(x.epM_WarningAmount)) || (Math.ceil(x.epM_BlockAmount) <= 0 && Math.ceil(x.epM_WarningAmount) <= 0)));
            if (!IsEmpty(policies) && policies.length > 0)
                return true;
                        
            var selCityName = GetddlSelText('ddlCityOfPurchase');
            var selCurrencyVal = GetddlSelText('ddlCurrency');
            var selPayType = $('#ddlPaymentType').val();
            var selPayName = GetddlSelText('ddlPaymentType');
            var totalAmount = units * unitAmount;

            var errorMsg = mt;
            var blockPolicy = policyMaster.filter(x => x.epM_ECT_Id == selCityVal && !IsEmpty(x.epM_BlockAmount) && Math.ceil(x.epM_BlockAmount) > 0);

            if (!IsEmpty(blockPolicy) && blockPolicy.length > 0) {

                $.each(blockPolicy, function (key, policy) { 

                    //var policyApply = IsEmpty(policy.epM_ECT_Id) || (!IsEmpty(policy.epM_ECT_Id) && policy.epM_ECT_Id == selCityVal);
                    var policyApply = (IsEmpty(policy.epM_PT_Id) || (!IsEmpty(policy.epM_PT_Id) && policy.epM_PT_Id == selPayType)); 
                    //policyApply = policyApply && (IsEmpty(policy.epM_Currency) || (!IsEmpty(policy.epM_Currency) && policy.epM_Currency == selCurrencyVal)); 

                    var blockAmount = policy.epM_BlockAmount;

                    if (policy.epM_Currency != selCurrencyVal && !IsEmpty(fixPolicyExc) && fixPolicyExc.length > 0) {

                        var excFactor = fixPolicyExc.find(x => x.eM_BaseCurrency == selCurrencyVal && !IsEmpty(x.eM_Rate) && x.eM_Rate > 0);
                        blockAmount = !IsEmpty(excFactor) && !IsEmpty(excFactor.eM_Rate) && excFactor.eM_Rate > 0 ? (blockAmount / excFactor.eM_Rate) : blockAmount;// mod by ziyad as using agent base currency list(policy base currency may vary)
                    }

                    blockAmount = AgentFixedDecimal(blockAmount);

                    errorMsg = (policyApply && (totalAmount > blockAmount)) ?
                        'Total amount should be <= ' + (policy.epM_BlockAmount == blockAmount ? policy.epM_Currency : selCurrencyVal) + ' ' + blockAmount
                        + (policy.epM_BlockAmount == blockAmount ? mt : '(' + policy.epM_Currency + ' ' + policy.epM_BlockAmount + ')')
                        + (IsEmpty(policy.epM_PT_Id) ? mt : ' for ' + selPayName + ' payment')
                        + (IsEmpty(policy.epM_ECT_Id) ? mt : ' in ' + selCityName + ' city') + '.' : mt;      

                    return IsEmpty(errorMsg);
                });

                if (!IsEmpty(errorMsg))
                    ShowError(errorMsg);
            }                

            var warningPolicy = IsEmpty(errorMsg) ? policyMaster.filter(x => x.epM_ECT_Id == selCityVal && !IsEmpty(x.epM_WarningAmount) && Math.ceil(x.epM_WarningAmount) > 0) : mt;

            if (action != 'S' && !IsEmpty(warningPolicy) && warningPolicy.length > 0) {

                $.each(warningPolicy, function (key, policy) { 

                    //var policyApply = IsEmpty(policy.epM_ECT_Id) || (!IsEmpty(policy.epM_ECT_Id) && policy.epM_ECT_Id == selCityVal);
                    var policyApply = (IsEmpty(policy.epM_PT_Id) || (!IsEmpty(policy.epM_PT_Id) && policy.epM_PT_Id == selPayType)); 
                    //policyApply = policyApply && (IsEmpty(policy.epM_Currency) || (!IsEmpty(policy.epM_Currency) && policy.epM_Currency == selCurrencyVal)); 

                    var warningAmount = policy.epM_WarningAmount;

                    if (policy.epM_Currency != selCurrencyVal && !IsEmpty(fixPolicyExc) && fixPolicyExc.length > 0) {

                        var excFactor = fixPolicyExc.find(x => x.eM_BaseCurrency == selCurrencyVal && !IsEmpty(x.eM_Rate) && x.eM_Rate > 0);
                        warningAmount = !IsEmpty(excFactor) && !IsEmpty(excFactor.eM_Rate) && excFactor.eM_Rate > 0 ? (warningAmount / excFactor.eM_Rate) : warningAmount;// mod by ziyad as using agent base currency list(policy base currency may vary)
                    }

                    warningAmount = AgentFixedDecimal(warningAmount);
                                        
                    errorMsg = (policyApply && (totalAmount > warningAmount)) ?
                        'Total amount exceeded warning limit of ' + (policy.epM_BlockAmount == warningAmount ? policy.epM_Currency : selCurrencyVal) + ' ' + warningAmount
                        + (policy.epM_BlockAmount == warningAmount ? mt : '(' + policy.epM_Currency + ' ' + policy.epM_WarningAmount + ')')
                        + (IsEmpty(policy.epM_PT_Id) ? mt : ' for ' + selPayName + ' payment')
                        + (IsEmpty(policy.epM_ECT_Id) ? mt : ' in ' + selCityName + ' city') + '.' : mt;      

                    return IsEmpty(errorMsg);
                });

                if (!IsEmpty(errorMsg))
                    ShowError(errorMsg);
            }   

            return IsEmpty(errorMsg);
        }

        /* To validate attendees policy for all cities */
        function ValidateAttendeePolicy() {

            var selCityVal = GetControlSplitVal('ddlCityOfPurchase', '-', 0);

            if (selExpType.eT_EnableAttendee != 'Y' || IsEmpty(selCityVal))
                return true;

            var attendeePolicy = policyMaster.filter(x => x.epM_ECT_Id == selCityVal && !IsEmpty(x.epM_NoOfAttendees) && Math.ceil(x.epM_NoOfAttendees) > 0);
            if (IsEmpty(attendeePolicy) || attendeePolicy.length == 0)
                return true;

            var selCurrencyVal = GetddlSelText('ddlCurrency');
            var selCityName = GetddlSelText('ddlCityOfPurchase');
            var selPayType = $('#ddlPaymentType').val();
            var selPayName = GetddlSelText('ddlPaymentType');
            var errorMsg = mt;

            $.each(attendeePolicy, function (key, policy) { 

                //var policyApply = IsEmpty(policy.epM_ECT_Id) || (!IsEmpty(policy.epM_ECT_Id) && policy.epM_ECT_Id == selCityVal);
                var policyApply = (IsEmpty(policy.epM_PT_Id) || (!IsEmpty(policy.epM_PT_Id) && policy.epM_PT_Id == selPayType)); 

                var blockAmount = !IsEmpty(policy.epM_BlockAmount) && Math.ceil(policy.epM_BlockAmount) > 0 ? policy.epM_BlockAmount : 0;
                var oBlockamount = blockAmount;

                if (!IsEmpty($('#ddlCurrency').val()) && policy.epM_Currency != selCurrencyVal && !IsEmpty(fixPolicyExc) && fixPolicyExc.length > 0) {

                    var excFactor = fixPolicyExc.find(x => x.eM_BaseCurrency == selCurrencyVal && !IsEmpty(x.eM_Rate) && x.eM_Rate > 0);
                    blockAmount = !IsEmpty(excFactor) && !IsEmpty(excFactor.eM_Rate) && excFactor.eM_Rate > 0 ? (blockAmount / excFactor.eM_Rate) : blockAmount;// mod by ziyad as using agent base currency list(policy base currency may vary)
                }

                blockAmount = AgentFixedDecimal(blockAmount);

                errorMsg = (policyApply && (expAttendeeIds.length < policy.epM_NoOfAttendees)) ?
                    'Minimum no of attendees should be ' + policy.epM_NoOfAttendees
                    + (Math.ceil(blockAmount) > 0 ? (' with Total amount of ' + selCurrencyVal + ' ' + blockAmount +
                        (oBlockamount == blockAmount ? mt : '(' + policy.epM_Currency + ' ' + oBlockamount + ')')) : mt)
                    + (IsEmpty(policy.epM_PT_Id) ? mt : ' for ' + selPayName + ' payment') +
                    (IsEmpty(policy.epM_ECT_Id) ? mt : ' in ' + selCityName + ' city') + '.' : mt;

                return IsEmpty(errorMsg);
            });

            if (!IsEmpty(errorMsg))
                ShowError(errorMsg);

            return IsEmpty(errorMsg);
        }

        /* Applying fixed amount policy for all cities */
        function ApplyFixedPolicy(action) {

            if (IsEmpty(policyMaster) || IsEmpty(policyMaster.length) || policyMaster.length == 0)
                return true;

            var fixedPolicy = policyMaster.filter(x => !IsEmpty(x.epM_FixedAmount) && Math.ceil(x.epM_FixedAmount) > 0);

            if (IsEmpty(fixedPolicy) || fixedPolicy.length == 0)                 
                return true;                    
                
            var errorMsg = mt;
            var fPolicy = fixedPolicy.find(x => x.epM_ECT_Id == GetControlSplitVal('ddlCityOfPurchase', '-', 0));
            var selCurrencyVal = GetddlSelText('ddlCurrency');            

            if (!IsEmpty(selCurrencyVal) && action == 'CC' && !IsEmpty(fPolicy)) {

                $('#txtUnitAmount').attr('disabled', 'disabled');

                var fixedAmount = fPolicy.epM_FixedAmount;

                if (fPolicy.epM_Currency != selCurrencyVal && !IsEmpty(fixPolicyExc) && fixPolicyExc.length > 0) {

                    var citycurrency = selCurrencyVal;
                    var excFactor = fixPolicyExc.find(x => x.eM_BaseCurrency == citycurrency && !IsEmpty(x.eM_Rate) && x.eM_Rate > 0);
                    fixedAmount = !IsEmpty(excFactor) && !IsEmpty(excFactor.eM_Rate) && excFactor.eM_Rate > 0 ? (fixedAmount / excFactor.eM_Rate) : fixedAmount;// mod by ziyad as using agent base currency list(policy base currency may vary)
                }

                var unitAmount = fixedAmount / (IsEmpty(fPolicy.epM_Units) || Math.ceil(fPolicy.epM_Units) <= 0 ? 1 : fPolicy.epM_Units);
                $('#txtUnitAmount').val(AgentFixedDecimal(unitAmount));

                //errorMsg = (fixedAmount < (unitAmount * units)) ?
                //    'Total amount should be less than ' + citycurrency + ' ' + fixedAmount + ' for city ' + GetddlSelText('ddlCityOfPurchase') : mt;    

                //if (!IsEmpty(errorMsg))
                //    ShowError(errorMsg);
            }
            else {

                $('#txtUnitAmount').removeAttr('disabled');
                if (selExpDtlId == 0)
                    $('#txtUnitAmount').val(AgentFixedDecimal(0));
            }

            return IsEmpty(errorMsg);
        }

        var divAttendeeHTML = '<div class="col-12 div-attendees collapse" class="collapse" id="attendeesCollapse">' +
                                '<a href="javascript:void(0);" class="float-right close-btn close-collapse-panel" id="closeCollapsePanel"><span class="icon icon-close"></span></a>' +
                                '<div class="clear"></div>' +
                                '<h4 class="mt-0 float-left">Attendees</h4>' +
                                '<div class="float-lg-right">' +
                                    '<span class="pr-2">Attendees:<label id="lblNoOfAttendees">1</label></span>| ' +
                                    '<span class="px-2">Attendee Total: <strong>@currency <label id="lblAttendeeAmount">0.00</label></strong></span> | ' +
                                    '<span class="px-2">Remaining: <strong>@currency <label id="lblAttendeeBalance">0.00</label></strong></span>' +
                                '</div>' +
                                '<div class="clear"></div>' +
                                '<div class="button-controls mt-2 ">' +
                                    '<a class="btn btn-primary float-right" onclick="AddNewAttendee();" id="btnAddNewAttendee">Add New Attendee <i class="icon icon-plus"></i></a>' +
                                    '<a style="display:none" class="btn btn-primary float-left" onclick="SelNewAttendee();" id="btnSelNewAttendee">Select Attendee <i class="icon icon-plus"></i></a>' +
                                    '<a class="btn btn-danger float-right" onclick="DelAttendee();" id="btnDelAttendee">REMOVE <i class="icon icon-delete2"></i></a>' +
                                '</div>' +
                                '<div class="clear"></div>' +
                                '<div class="table-responsive">' +
                                    '<table class="table small mt-2 exp-sidetable" id="attendeesTableList">' +
                                        '<thead>' +
                                            '<tr>' +
                                                '<th><div class="custom-checkbox-style dark"><input onclick="SelAllExpAttendees(this);" id="chkAllExpAttendees" type="checkbox" class="form-control"><label></label></div></th>' +
                                                '<th>First Name</th>' +
                                                '<th>Last Name</th>' +
                                                '<th>Attendee Title</th>' +
                                                '<th>Company</th>' +
                                                '<th>Attendee Type</th>' +
                                                '<th>Amount</th>' +
                                            '</tr>' +
                                        '</thead>' +
                                        '<tbody id="tbAttendees"></tbody>' +
                                    '</table>' +
                                '</div>' +
                            '</div>';

        
        /* To set attendee header lables */
        function SetAttendeeHdrs() {

            $('#lblNoOfAttendees').html('<strong>' + expAttendeeIds.length + '</strong>');

            var TotalAttendAmount = 0
            $.each(expAttendeeIds, function (key, col) { TotalAttendAmount += GetTextBoxDecVal(col.RowId.replace('trExpAtt', 'txtExpAttAmount')); });

            $('#lblAttendeeAmount').html('<strong>' + AgentDecimalRound(TotalAttendAmount) + '</strong>');

            var totalAmount = GetTextBoxDecVal('txtTotalAmount');
            attendeeBal = totalAmount - AgentDecimalRound(TotalAttendAmount).replace(/\,/g,'');            
            $('#lblAttendeeBalance').html('<strong>' + AgentDecimalRound(totalAmount - TotalAttendAmount) + '</strong>');
        }

        /* To set default first attendee amount */
        function SetFirstAttendeeAmount(amount) {

            if ($('#txtExpAttAmount1') == null)
                return;

            amount = AgentDecimalRound(removeComma(amount));
            $('#txtExpAttAmount1').val(removeComma(amount));
        }

        /* To reset all attendee global variables and grid */
        function ResetAttemdeesDiv(from) {

            if (from != 'refresh') {

                var allAttendeeIds = expAttendeeIds.filter(x => x.EamId !== -1);
                selExpAttendees = [];

                $.each(allAttendeeIds, function (key, col) { selExpAttendees.push(col.RowId); });

                if (selExpAttendees.length > 0)
                    DelAttendee();
                expAttendeeIds = expAttendeeIds.filter(x => x.EamId == -1);
            }
            else
                expAttendeeIds = [];
            
            attCount = expAttendeeIds.length; selExpAttendees = []; attendeeMaster = {}; selAMIds = []; 
            attendeeBal = 0; NewAttendeeData = []; NewCompanies = []; 
        }

        /* To bind selected expense attendees */
        function BindSelExpAttendees() {

            if (IsEmpty(selExpDetails) || IsEmpty(selExpDetails.expAttendees) || selExpDetails.expAttendees.length == 0) {

                ResetSelDetails();
                return;
            }
            
            Setdropval('ddlCustomer', selExpDetails.eD_CV_Id, mt);
            loadMode = 'EL';
            GetCompanyAttendees();
        }

        /* To show attendee master info for selected company */
        function SelNewAttendee() {

            if (!ValidateAttendee())
                return;

            if (attendeeMaster.length > 0 && $('#divAMData').children().length > 0) {

                $('#divAttendeesPopup').modal('show');
                return;
            }

            if (attendeeMaster.length > 0 && $('#divAMData').children.length > 0) {

                BindNewAttendeePopUp(attendeeMaster);
                return;
            }

            GetCompanyAttendees();       
        }

        /* To get company attendees data */
        function GetCompanyAttendees() {

            var reqData = { AgentInfo: apiAgentInfo, CompanyId: 0 };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCompanyAttendees';
            WebApiReq(apiUrl, 'POST', reqData, '', BindNewAttendeePopUp, null, null);     
        }

        /* To bind attendee master list and show in popup */
        function BindNewAttendeePopUp(masterData) {

            attendeeMaster = masterData;

            if (loadMode == 'EL') {

                $.each(selExpDetails.expAttendees, function (key, col) { selAMIds.push(col.eA_EAM_Id); });
                AddAMInfo();
                SetAttendeeAmount();
                loadMode = mt;
                $("#ctl00_upProgress").hide();                
                ResetSelDetails();
                return;
            }

            if (attendeeMaster.length == 0) {

                ShowError('No attendees found for Customer ' + GetddlSelText('ddlCustomer') + '.');
                return;
            }

            var cntrlId = "#txtExpAttName" + attCount;
            var atoptions = {

                data: attendeeMaster,
                getValue: "eaM_Name",
                list: {
                    match: {
                        enabled: true
                    },
                    onSelectItemEvent: function () {

                        selAttName = mt;
                        var attNameIndex = $(cntrlId).getSelectedItemIndex();
                        selAttName = $(cntrlId).getItemData(attNameIndex);
                    },
                    onHideListEvent: function () {
                        SetAttendeeInfo(cntrlId.replace('#', mt));
                    }
                },
                theme: "plate"
            };

            $(cntrlId).easyAutocomplete(atoptions);
            return;

            //var gridProperties = GetGridPropsEntity();
            //gridProperties.headerColumns = ('First Name|Last Name|Title|Attendee Type|Company').split('|');
            //gridProperties.displayColumns = ('eaM_Name|eaM_LastName|eaM_Title|eaM_Type|eaM_ComanyName').split('|');
            //gridProperties.pKColumnNames = ('eaM_ID').split('|');
            //gridProperties.dataEntity = attendeeMaster;
            //gridProperties.divGridId = 'divAMData';
            //gridProperties.gridSelectedRows = selAMIds;
            //gridProperties.selectType = 'M';
            //gridProperties.recordsperpage = 10;

            //EnablePagingGrid(gridProperties);
                        
            //$('#divAttendeesPopup').modal('show');
        }
        var selAttName = mt;
        /* To get attendee master list on expense load */
        function SetAttendeeAmount() {

            var nonFirst = expAttendeeIds.filter(x => x.RowId !== 'trExpAtt1');
            var atTotal = 0;
            $.each(nonFirst, function (key, col) {

                var ead = selExpDetails.expAttendees.find(x => x.eA_EAM_Id == col.EamId);
                $('#' + col.RowId.replace('trExpAtt', 'txtExpAttAmount')).val(ead.eA_Amount);
                atTotal += eval(ead.eA_Amount);
            });

            SetFirstAttendeeAmount(GetTextBoxDecVal('txtTotalAmount') - atTotal);
        }

        /* To set attendee info on attendee name selection */
        function SetAttendeeInfo(id) {

            $('#' + id.replace('txtExpAttName', 'txtExpAttLName')).val(mt);
            $('#' + id.replace('txtExpAttName', 'txtExpAttTitle')).val(mt);
            $('#' + id.replace('txtExpAttName', 'txtExpAttType')).val(mt);
            $('#' + id.replace('txtExpAttName', 'txtExpAttCustomer')).val(mt);

            if (!validateData(id, 'AL', '100', 'Attendee Name') || IsEmpty(selAttName) || IsEmpty($('#' + id).val()) || IsEmpty(attendeeMaster) || attendeeMaster.length == 0)
                return false;

            var selATNameDtls = selAttName;
            
            if (IsEmpty(selATNameDtls) || selATNameDtls.eaM_Name !== $('#' + id).val())
                return false;

            if (!IsEmpty(selAMIds) && selAMIds.length > 0 && selAMIds.find(x => x == selATNameDtls.eaM_ID)) {

                $('#' + id).val(mt);
                $('#' + id).focus();
                ShowError('Attendee already added to expense, please select different attendee');
                return false;
            }

            $('#' + id.replace('txtExpAttName', 'txtExpAttLName')).val(selATNameDtls.eaM_LastName);
            $('#' + id.replace('txtExpAttName', 'txtExpAttTitle')).val(selATNameDtls.eaM_Title);
            $('#' + id.replace('txtExpAttName', 'txtExpAttType')).val(selATNameDtls.eaM_Type);

            var custmer = expCustomers.find(x => x.cV_ID == selATNameDtls.eaM_CV_Id)
            $('#' + id.replace('txtExpAttName', 'txtExpAttCustomer')).val(custmer.cV_Name);

            selAMIds.push(selATNameDtls.eaM_ID);
            selAttName = mt;
        }

        /* To add attendee master data to expense attendees */
        function AddAMInfo() {

            selAMIds = loadMode == 'EL' ? selAMIds : GetSetSelectedRows('get', mt);
            if (IsEmpty(selAMIds) || selAMIds.length == 0) {

                ShowError('Please select attendee.');
                return;
            }
            
            $.each(selAMIds, function (key, col) {

                if (!expAttendeeIds.find(x => x.EamId == col)) {

                    var amData = attendeeMaster.find(x => x.eaM_ID == col)
                    if (amData != null) {
                        AddNewAttendeeRowHtml(amData.eaM_Name, amData.eaM_Title, amData.eaM_Type, 0, amData.eaM_ComanyName, amData.eaM_ID, 0, amData.eaM_LastName);
                    }
                }                
            });  
            
            $('#divAttendeesPopup').modal('hide');
            SetNewAttendeeAmount();
        }

        /* To add new attendee row in attendee grid */
        function AddNewAttendee() {

            if (!ValidateAttendee())
                return;

            AddNewAttendeeRowHtml(mt, mt, mt, 0, GetddlSelText('ddlCustomer'), 0, 0, mt);
            SetNewAttendeeAmount();
        }

        /* To set attendee amounts equally when a new attendee is added */
        function SetNewAttendeeAmount() {

            if (selExpType.eT_EnableAttendee != 'Y')
                return;

            var totalAmount = GetTextBoxDecVal('txtTotalAmount');
            var perAttendeeAmount = removeComma(AgentDecimalRound(totalAmount / expAttendeeIds.length));

            $.each(expAttendeeIds, function (key, col) {
                                
                $('#' + col.RowId.replace('trExpAtt', 'txtExpAttAmount')).val(perAttendeeAmount);
            });

            SetAttendeeHdrs();
        }

        /* To validate expense attendee row */
        function ValidateAttendee() {

            var IsValid = true;

            $.each(expAttendeeIds, function (key, col) {

                if (col.EamId == '0') {

                    IsValid = CheckReqData(col.RowId.replace('trExpAtt', 'txtExpAttName'), 'AL', '100', 'Attendee First Name', mt) && IsValid;
                    IsValid = CheckReqData(col.RowId.replace('trExpAtt', 'txtExpAttLName'), 'AL', '100', 'Attendee Last Name', mt) && IsValid;
                    IsValid = CheckReqData(col.RowId.replace('trExpAtt', 'txtExpAttTitle'), 'AN', '50', 'Attendee Title', mt) && IsValid;
                    IsValid = CheckReqData(col.RowId.replace('trExpAtt', 'txtExpAttType'), 'AN', '50', 'Attendee Type', mt) && IsValid;
                    IsValid = CheckReqData(col.RowId.replace('trExpAtt', 'txtExpAttCustomer'), 'AN', '100', 'Customer', mt) && IsValid;
                }                
                IsValid = CheckReqData(col.RowId.replace('trExpAtt', 'txtExpAttAmount'), 'DC', '20', 'Attendee Amount', '0') && IsValid;
            });
            
            return IsValid;
        }

        /* To delete attendee row from attendee grid */
        function DelAttendee() {

            if (selExpAttendees.length == 0) {

                ShowError('Please select attendee to remove.')
                return;
            }

            $.each(selExpAttendees, function (key, col) {

                expAttendeeIds = expAttendeeIds.filter(x => x.RowId !== col)
                $('#' + col).remove();
            });
            selExpAttendees = [];
            $('#chkAllExpAttendees').prop('checked', false);
            SetNewAttendeeAmount();
        }

        /* To set selected expense attendees */
        function SelExpAttendee(event, rowid) {

            if (event.checked == true)
                selExpAttendees.push(rowid);
            else
                selExpAttendees = selExpAttendees.filter(x => x !== rowid);
            $('#chkAllExpAttendees').prop('checked', (selExpAttendees.length == (expAttendeeIds.length - 1)))
        }

        /* To select all expense attendees */
        function SelAllExpAttendees(event) {
                        
            if (event.checked == true) {

                $.each(expAttendeeIds, function (key, col) { selExpAttendees.push(col.RowId); });
                selExpAttendees = selExpAttendees.filter(x => x !== 'trExpAtt1');
            }
            else
                selExpAttendees = [];
            $("#tbAttendees").find('input[type=checkbox]').each(function () { this.checked = event.checked; });
        }

        /* To validate and set amount headers on attendee amount change */
        function AmountChange(dataCntrlId, type, length, fieldName) {

            if (validateData(dataCntrlId, type, length, fieldName))
                SetAttendeeHdrs();
        }

        /* To add new row to attendee  */
        function AddNewAttendeeRowHtml(name, title, attendeetype, amount, company, eamId, eAID, lastname) {

            attCount++;
            var isNewEmp = IsEmpty(name);
            var newRowId = 'trExpAtt' + attCount;
            var trId = "'" + newRowId + "'";
            var keypress = "return onKeyPressVal(event, 'AL', '100');";
            var textchange = mt; //"return SetAttendeeInfo(this.id);";

            var newRow = '<tr id="' + newRowId + '"><td>';

            newRow += attCount == 1 ? mt : '<div class="custom-checkbox-style dark"><input onclick="SelExpAttendee(this, ' + trId + ')" id="chkExpAtt' + attCount + '" type="checkbox" class="form-control"><label></label></div>';
            newRow += '</td>'

            newRow += '<td>';
            newRow += isNewEmp ? '<input id="txtExpAttName' + attCount + '" type="text" onkeypress="' + keypress + '" onchange="' + textchange + '" placeholder="Enter First Name" class="form-control ">' : name;
            newRow += '</td>';

            textchange = "return validateData(this.id, 'AL', '100', 'Attendee Name');";

            newRow += '<td>';
            newRow += isNewEmp ? '<input id="txtExpAttLName' + attCount + '" type="text" onkeypress="' + keypress + '" onchange="' + textchange + '" placeholder="Enter Last Name" class="form-control ">' : lastname;
            newRow += '</td>';

            keypress = "return onKeyPressVal(event, 'AL', '50');";
            textchange = "return validateData(this.id, 'AL', '50', 'Attendee Title');";
            newRow += '<td>';
            newRow += isNewEmp ? '<input id="txtExpAttTitle' + attCount + '" type="text" onkeypress="' + keypress + '" onchange="' + textchange + '" placeholder="Enter Title" class="form-control ">' : title;
            newRow += '</td>';

            keypress = "return onKeyPressVal(event, 'TS', '100');";
            textchange = "return validateData(this.id, 'TS', '100', 'Attendee Customer');";
            newRow += '<td>';
            newRow += isNewEmp ? '<input id="txtExpAttCustomer' + attCount + '" type="text" onkeypress="' + keypress + '" onchange="' + textchange + '" placeholder="Enter Customer" class="form-control ">' : company;
            newRow += '</td>';

            //newRow += '<td>';
            //newRow += isNewEmp ? '<select id="ddlExpAttCustomer' + attCount + '" class="form-control"></select>' : company;
            //newRow += '</td>';

            keypress = "return onKeyPressVal(event, 'AL', '50');";
            textchange = "return validateData(this.id, 'AL', '50', 'Attendee Type');";
            newRow += '<td>';
            newRow += isNewEmp ? '<input id="txtExpAttType' + attCount + '" type="text" value="Business Guest" onkeypress="' + keypress + '" onchange="' + textchange + '" placeholder="Enter Attendee Type" class="form-control">' : attendeetype;
            newRow += '</td>';

            keypress = "return onKeyPressVal(event, 'DC', '20');";
            textchange = "return AmountChange(this.id, 'DC', '20', 'Attendee Amount');";
            newRow += '<td>';
            newRow += '<input value="' + removeComma(amount) + '" id="txtExpAttAmount' + attCount + '" type="text" onkeypress="' + keypress + '" onchange="' + textchange + '" placeholder="Enter Attendee Amount" class="form-control">';
            newRow += '</td>';

            newRow += '</tr>';
            $('#tbAttendees').append(newRow);

            if (isNewEmp == true) {

                var options = {

                  data: expCustomers,
                  getValue: "cV_Name",
                  list: { match: { enabled: true } },
                  theme: "plate"
                };

                $("#txtExpAttCustomer" + attCount).easyAutocomplete(options);

                if (!IsEmpty(attendeeMaster) && attendeeMaster.length > 0)
                    BindNewAttendeePopUp(attendeeMaster);
                else
                    GetCompanyAttendees();
            }

            //if (isNewEmp == true) 
            //    BindCustomerOptions('ddlExpAttCustomer' + attCount);

            expAttendeeIds.push(GetAttendeeIdObj('trExpAtt' + attCount, isNewEmp, eamId, eAID));
            SetAttendeeHdrs();
        }

        /* To bind customer ddl in attendees grid */
        function BindCustomerOptions(ddlId) {

            var newCustomer = expAttendeeIds.find(col => col.EamId == '0');

            if (!IsEmpty(newCustomer) && !IsEmpty(newCustomer.RowId)) {

                document.getElementById(ddlId).innerHTML = document.getElementById(newCustomer.RowId.replace('trExpAtt', 'ddlExpAttCustomer')).innerHTML;
                return;
            }

            DdlBind(ddlId, expCustomers, 'cV_ID', 'cV_Name', mt, mt, '--Select--', mt);
        }

        /* To hold selected attedees info */
        function GetAttendeeIdObj(rowid, newEmp, eamId, eAID) {

            return {
                RowId: rowid, NewEmp: newEmp, EamId: eamId, EAID: eAID
            };
        }

        /* To bind uploaded receipts to grid */
        function BindUploadedReceipts() {

            if (IsEmpty(selExpDetails) || IsEmpty(selExpDetails.expReceipts) || selExpDetails.expReceipts.length == 0)
                return;

            selExpDetails.expReceipts = selExpDetails.expReceipts.filter(x => x.rC_FileName != 'NR');

            $.each(selExpDetails.expReceipts, function (key, rcData) { AddUploadedReceiptsRowHtml(rcData.rC_CreatedDate, rcData.rC_FileName, rcData.rC_Id, rcData.rC_Path); });  
        }        
                
        /* To get open receipts */
        function GetOpenReceipts() {

            var reqData = { AgentInfo: apiAgentInfo, ProfileId: expenseReport.eR_ProfileId };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getOpenRceiptsDetails';
            WebApiReq(apiUrl, 'POST', reqData, '', BindOpenReceipts, null, null);     
        }        

        /* To get open receipts */
        function BindOpenReceipts(receiptsData) {

            if (receiptsData.length == 0 || receiptsData.dtOpenReceipts.length == 0) {

                ShowError('No uploaded receipts found.');
                return false;
            }

            openReceiptsList = receiptsData.dtOpenReceipts;
            openReceiptsDtls = receiptsData.dtOpenReceiptsDtls;

            $('#tbOpenReceipts').children().remove();
            $.each(openReceiptsList, function (key, col) { AddOpenReceiptsRowHtml(col.rC_CreatedDate, col.rC_Name); });  
            $('#divOpenReceiptsPopup').modal('show');
        }        
                
        /* To set selected open receipts */
        function SelectOpenReceipts(event, rcName) {

            if (event.checked == true)
                selOpenReceipts.push(rcName);
            else
                selOpenReceipts = selOpenReceipts.filter(x => x !== rcName);
            $('#chkAllOpenReceipts').prop('checked', (selOpenReceipts.length == allopenReceiptsIds.length));
        }

        /* To select all open receipts */
        function SelectAllOpenReceipts(event) {
                        
            selOpenReceipts = event.checked == true ? allopenReceiptsIds : [];
            $("#tbOpenReceipts").find('input[type=checkbox]').each(function () { this.checked = event.checked; });
        }

        /* To add open receipts to expense */
        function AddUploadedReceipts() {

            if (IsEmpty(selOpenReceipts) || selOpenReceipts.length == 0) {

                ShowError('Please select receipt to add.');
                return;
            }

            $.each(selOpenReceipts, function (key, col) {

                var filterData = openReceiptsDtls.filter(x => x.rC_Name == col);
                $.each(filterData, function (key, rcData) {
                                        
                    AddUploadedReceiptsRowHtml(rcData.rC_CreatedDate, rcData.rC_FileName, rcData.rC_Id, rcData.rC_Path);    
                });
            });  
            
            $('#divOpenReceiptsPopup').modal('hide');
        }

        /* To delete uploaded receipts from receipt grid */
        function DeleteReceipts() {

            if (selUploadedRceipts.length == 0) {

                ShowError('Please select receipt to remove.')
                return;
            }

            $.each(selUploadedRceipts, function (key, col) {

                allUploadedRceipts = allUploadedRceipts.filter(x => x !== col);
                $('#trUR' + col).remove();
            });

            selUploadedRceipts = [];
            $('#chkAllUploadedReceipts').prop('checked', false);
        }

        /* To set selected uploaded receipts */
        function SelectUploadedReceipts(event, rcid) {

            if (event.checked == true)
                selUploadedRceipts.push(rcid);
            else
                selUploadedRceipts = selUploadedRceipts.filter(x => x !== rcid);
            $('#chkAllUploadedReceipts').prop('checked', (selUploadedRceipts.length == allUploadedRceipts.length));
        }

        /* To select all uploaded receipts */
        function SelectAllUploadedReceipts(event) {
                        
            selUploadedRceipts = event.checked == true ? allUploadedRceipts : [];
            $("#tbUploadedReceipts").find('input[type=checkbox]').each(function () { this.checked = event.checked; });
        }        

        /* To add new row to uploaded receipts grid */
        function AddUploadedReceiptsRowHtml(uploaddate, fileName, rcid, imagePath) {

            var newRow = '<tr id="trUR' + rcid + '"><td>';

            newRow += '<div class="custom-checkbox-style dark"><input onclick="SelectUploadedReceipts(this, ' + rcid + ')" id="chkUR' + rcid + '" type="checkbox" class="form-control"><label></label></div>';
            newRow += '</td>'

            newRow += '<td>' + uploaddate + '</td>';
            newRow += '<td>' + fileName + '</td>';
            //alert(imagePath);
            imagePath = imagePath.replaceAll('/', '\\');
            imagePath = imagePath.substring(imagePath.indexOf('\\Upload\\'), imagePath.Length);
            //alert(imagePath);
            var ext = fileName.split('.').reverse()[0];
            ext = ext.toLowerCase();
            if(ext === "pdf"){
                //newRow += '<td><ul class="list-inline gallery"><li><img src="' + imagePath + '" class="thumbnail zoom 1" style="width:100px;" ></li></ul></td>';
                newRow += '<td><a href="#viewPdfModal' + rcid + '" data-toggle="modal" data-target="#viewPdfModal' + rcid + '" class="btn btn-outline btn-default"><img src="build/img/pdf-icon.svg" alt="view PDF" style="width: 35px;"></a><div class="modal fade" id="viewPdfModal' + rcid + '" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"> <div class="modal-content"> <div class="modal-body"><button type="button" class="close text-secondary position-relative" data-dismiss="modal" aria-label="Close" style="top: -10px;right: -8px;"> <span aria-hidden="true" class="icon icon-close"></span> </button><object data="' + imagePath + '" type="application/pdf" height="400" width="100%"></object></div></div></div></div></td>';
            }else if(ext === "jpg" || "jpeg" || "png" ){
                newRow += '<td><a href="#viewPdfModal' + rcid + '" data-toggle="modal" data-target="#viewPdfModal' + rcid + '" class="btn btn-outline btn-default"><img src="' + imagePath + '" class="thumbnail mb-0" style="width:70px;" ></a><div class="modal fade" id="viewPdfModal' + rcid + '" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true"><div class="modal-dialog modal-lg"> <div class="modal-content"> <div class="modal-body"><button type="button" class="close text-secondary position-relative" data-dismiss="modal" aria-label="Close" style="top: -10px;right: -8px;"> <span aria-hidden="true" class="icon icon-close"></span> </button><a href="' + imagePath + '" download="' + imagePath + '" target="_blank"><img src="' + imagePath + '" class="thumbnail" style="margin:0 auto;max-width:100%" ></a></div></div></div></div></td>';
            }else{
                newRow += '<td><a href="' + imagePath + '" download="'+fileName+'" class="btn btn-outline btn-default"><span class="icon icon-files" style="font-size: 24px;line-height: 1;"></span></a></td>';
            }        

            newRow += '</tr>';
            $('#tbUploadedReceipts').append(newRow);
            allUploadedRceipts.push(rcid);
        }

        /* To add new row to open receipts grid */
        function AddOpenReceiptsRowHtml(uploaddate, receiptName) {

            var newRow = '<tr id="trOR' + receiptName + '"><td>';
            var pkvalue = "'" + receiptName + "'";

            newRow += '<div class="custom-checkbox-style dark"><input onclick="SelectOpenReceipts(this, ' + pkvalue + ')" id="chkOR' + receiptName + '" type="checkbox" class="form-control"><label></label></div>';
            newRow += '</td>'

            newRow += '<td>' + uploaddate + '</td>';
            newRow += '<td>' + receiptName + '</td>';

            newRow += '</tr>';
            $('#tbOpenReceipts').append(newRow);
            allopenReceiptsIds.push(receiptName);
        }        

        /* To confirm expense report submit */
        function SubConfirm() {

            if (IsEmpty(expRepDetails) || expRepDetails.length == 0) {

                ShowError('Please add expense to submit report.');
                return false;
            }

            $('#divSubmitConfirm').modal('show');
        }        

        /* To submit expense report */
        function SubDelExpReport(action) {

            repAction = action;

            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expRepId, ProfileId: expenseReport.eR_ProfileId, Action: action, Host: webAppHost };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/subDelExpReport';
            WebApiReq(apiUrl, 'POST', reqData, '', SubDelResposne, null, null);          
        }

        /* To show expense report submit/delete response */
        function SubDelResposne(resp) {

            if (IsEmpty(resp) || IsEmpty(resp.data) || resp.data != 'Success') {

                ShowError('Failed to ' + (repAction == 'D' ? 'deleted' : 'submitted') + ' expense report, please contact admin.');
                return false;
            }

            alert('Expense report ' + (repAction == 'D' ? 'deleted' : 'submitted') + ' successfully');            
            window.location.href = 'ExpReport.aspx';
        }

        /* To clear expense */
        function ClearExpense() {

            AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpCreate', 'sessionData':'" + JSON.stringify({ ExpRepId: expRepId }) + "', 'action':'set'}");
            window.location.href = "ExpCreate.aspx";
        }

        /* To save expense */
        function SaveExpense() {
            
            var ExpFlexData = GetExpFlexData('N');
            ExpFlexData = ExpFlexData.concat(GetExpFlexData('I'));
            var ExpAttendData = GetExpAttendeeData();
            var ExpUplFiles = GetUploadedFiles();
            var ExpMessages = GetExpMessages();
            var ExpAppStatus = !IsEmpty(selExpDetails) && selExpDetails.eD_APPROVALSTATUS == 'RV' ? 'RS' : 'O';

            var receiptStatus = $('#ddlReceiptStatus').val() == 'NR' ? mt : $('#ddlReceiptStatus').val();
            receiptStatus = !IsEmpty(selExpDetails) && !IsEmpty(selExpDetails.eD_ReceiptStatus) && selExpDetails.eD_ReceiptStatus == 'NR' ? 'NR' : receiptStatus;
            
            selExpDetails = GetExpDataObj(selExpDtlId, expRepId, selExpType.eT_ID, GetJsonDate($('#txtTransactionDate').val(), '-', 'ddmmyyyy'), $('#ddlPaymentType').val(),
                $('#ddlCardName').val(), receiptStatus, GetControlSplitVal('ddlCityOfPurchase', '-', 0), $('#txtUOM').val(), GetTextBoxDecVal('txtUnits'),
                GetTextBoxDecVal('txtUnitAmount'), GetddlSelText('ddlCurrency'), GetTextBoxDecVal('txtExchangeRate'), GetTextBoxDecVal('txtAmountIn'), GetTextBoxDecVal('txtTaxAmount'),
                GetTextBoxDecVal('txtTotalAmount'), $('#chkIsBillable').is(':checked'), $('#ddlCustomer').val(), $('#chkClaimExpense').is(':checked'),
                $('#txtComments').val(), 'N', ExpAppStatus, true, apiAgentInfo.LoginUserId, ExpFlexData, ExpAttendData, ExpUplFiles, ExpMessages);

            var reqData = { AgentInfo: apiAgentInfo, expReportData: selExpDetails, expAttendeeMasters: NewAttendeeData, expNewCompanies: NewCompanies };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/saveExpDtlData';
            WebApiReq(apiUrl, 'POST', reqData, '', SaveResponse, null, null);                        
        }

        /* To perform after save action */
        function SaveResponse(resp) {    

            if (resp.data == 'Success') {

                var fileresp = resp.data;
                if (!IsEmpty(resp.expReceipts) && resp.expReceipts.length > 0) {

                    var SavedfileNames = {};

                    $.each(resp.expReceipts, function (key, col) { SavedfileNames[col.rC_Id] = col.rC_FileName; });

                    fileresp = AjaxCall('ExpUploadReceipt.aspx/saveReceiptFiles', "{'response':'" + JSON.stringify(SavedfileNames) + "', 'spath':'" + fileUploadPath + "'}");
                }
                
                if (fileresp == 'Success') 
                    alert('Expense saved successfully.');
                else
                    ShowError('Expense saved successfully, but failed to upload receipts, please contact admin.');  

                ClearGlobalVariables();
                GetScreenData('S');    
                $('#showSidePanel').click();

                $("#ctl00_upProgress").hide();
            }
            else
                ShowError(resp.data);  
        }

        /* To get flex fields data object based on flex type */
        function GetExpAttendeeData() {

            var ExpAttendeeData = [];
            NewAttendeeData = []; NewCompanies = [];
                        
            if (expAttendeeIds.length <= 1)
                return [];

            var newEamId = 0; var newCmpId = 0;

            expAttendeeIds = expAttendeeIds.filter(x => x.RowId !== 'trExpAtt1');

            $.each(expAttendeeIds, function (key, col) {

                var cntrlval = $('#' + col.RowId.replace('trExpAtt', 'txtExpAttAmount')).val();             
                var attId = col.EamId;

                var oldEAId = 0;
                if (!IsEmpty(selExpDetails) && !IsEmpty(selExpDetails.expAttendees) && selExpDetails.expAttendees.length > 0) {

                    var atdata = selExpDetails.expAttendees.find(x => x.eA_EAM_Id == col.EamId);
                    selExpDetails.expAttendees = selExpDetails.expAttendees.filter(x => x.eA_EAM_Id !== col.EamId);
                    oldEAId = IsEmpty(atdata) ? oldEAId : atdata.eA_Id;
                }

                if (oldEAId == 0) {

                    var newName = $('#' + col.RowId.replace('trExpAtt', 'txtExpAttName')).val();
                    var newLName = $('#' + col.RowId.replace('trExpAtt', 'txtExpAttLName')).val();
                    var newtitle = $('#' + col.RowId.replace('trExpAtt', 'txtExpAttTitle')).val();
                    var newtype = $('#' + col.RowId.replace('trExpAtt', 'txtExpAttType')).val();
                    var custName = $('#' + col.RowId.replace('trExpAtt', 'txtExpAttCustomer')).val();
                    var custId = 0;

                    if (!IsEmpty(expCustomers) && expCustomers.length > 0) {

                        var custInfo = expCustomers.find(x => x.cV_Name == custName);
                        newCmpId = IsEmpty(custInfo) ? Math.ceil(newCmpId) - 1 : newCmpId;
                        custId = IsEmpty(custInfo) ? newCmpId : custInfo.cV_ID;
                    }

                    if (custId <= 0)
                        NewCompanies.push(GetExpCVObj(custId, apiAgentInfo.AgentId, custName, custName, 'Company', true, apiAgentInfo.LoginUserId));

                    var selEmpInfo = attendeeMaster.find(x => x.eaM_Name == newName);
                    newEamId = IsEmpty(selEmpInfo) ? Math.ceil(newEamId) - 1 : newEamId;
                    var attId = IsEmpty(selEmpInfo) ? newEamId : selEmpInfo.eaM_ID;

                    NewAttendeeData.push(GetAttendeeMasterObj(attId, apiAgentInfo.AgentId, newtitle, newName, newtype, custId, true, apiAgentInfo.LoginUserId, newLName));
                }
                else if (!IsEmpty(selExpDetails) && !IsEmpty(selExpDetails.expAttendees) && selExpDetails.expAttendees.length > 0)
                    selExpDetails.expAttendees = selExpDetails.expAttendees.filter(x => x.eA_Id !== oldEAId);

                ExpAttendeeData.push(GetExpAttendeeObj(oldEAId, selExpDtlId, attId, cntrlval, true, apiAgentInfo.LoginUserId));
            });

            if (!IsEmpty(selExpDetails) && !IsEmpty(selExpDetails.expAttendees) && selExpDetails.expAttendees.length > 0) {

                $.each(selExpDetails.expAttendees, function (key, col) {

                    ExpAttendeeData.push(GetExpAttendeeObj(col.eA_Id, selExpDtlId, col.eA_EAM_Id, col.eA_Amount, false, apiAgentInfo.LoginUserId));
                });
            }

            return ExpAttendeeData;
        }

        /* To get receipts object based on uploaded files */
        function GetUploadedFiles() {

            var ExpUploadedFiles = [];

            dzUploadedfiles = $('.dz-filename');

            $.each(dzUploadedfiles, function (key, col) {

                ExpUploadedFiles.push(
                    GetExpReceiptObj(0, 0, expenseReport.eR_ProfileId, mt, col.innerText, fileUploadPath, true, apiAgentInfo.LoginUserId, 'S')
                );
            });

            if (allUploadedRceipts.length > 0 && !IsEmpty(selExpDetails) && !IsEmpty(selExpDetails.expReceipts) &&
                !IsEmpty(selExpDetails.expReceipts.length) && selExpDetails.expReceipts.length > 0) {
                
                $.each(allUploadedRceipts, function (key, col) {

                    var rcData = selExpDetails.expReceipts.find(x => x.rC_Id == col && !IsEmpty(x.rC_ED_Id) && x.rC_ED_Id > 0);

                    if (!IsEmpty(rcData) && rcData.rC_ED_Id > 0) {

                        allUploadedRceipts = allUploadedRceipts.filter(x => x !== col);
                        selExpDetails.expReceipts = selExpDetails.expReceipts.filter(x => x.rC_Id !== col);
                    }                    
                });
            }

            if (allUploadedRceipts.length > 0 && !IsEmpty(openReceiptsDtls) && openReceiptsDtls.length > 0) {
                
                $.each(allUploadedRceipts, function (key, col) {

                    var rcData = openReceiptsDtls.find(x => x.rC_Id == col);
                    ExpUploadedFiles.push(GetExpReceiptObj(rcData.rC_Id, selExpDtlId, expenseReport.eR_ProfileId, rcData.rC_Name,
                        rcData.rC_FileName, rcData.rC_Path, true, apiAgentInfo.LoginUserId, 'UI'));
                });
            }

            if (!IsEmpty(selExpDetails) && !IsEmpty(selExpDetails.expReceipts) && selExpDetails.expReceipts.length > 0) {

                $.each(selExpDetails.expReceipts, function (key, col) {

                    ExpUploadedFiles.push(GetExpReceiptObj(col.rC_Id, selExpDtlId, expenseReport.eR_ProfileId, col.rC_Name,
                        col.rC_FileName, col.rC_Path, true, apiAgentInfo.LoginUserId, 'DI'));
                });
            }

            return ExpUploadedFiles;
        }

        /* To get flex fields data object based on flex type */
        function GetExpFlexData(FlexType) {

            var ExpFlexData = [];

            var Flexfields = FlexType == 'N' ? normalFlex : itemFlex;

            if (Flexfields.length == 0)
                return [];

            $.each(Flexfields, function (key, col) {

                var divId = 'div' + GetCntrlId(col.eF_Label);
                if (document.getElementById(divId).style.display != 'none') {

                    var cntrlId = (CompFields(col.eF_Control, 'CHECKBOX') ? 'chk' : CompFields(col.eF_Control, 'DDL') ? 'ddl' : 'txt') + GetCntrlId(col.eF_Label);
                    var cntrlval = cntrlId.startsWith('chk') ? $('#' + cntrlId).is(":checked") : $('#' + cntrlId).val();

                    var oldEFId = 0;
                    if (!IsEmpty(selExpDetails) && !IsEmpty(selExpDetails.expFlexDatas) && selExpDetails.expFlexDatas.length > 0) {

                        var fldata = selExpDetails.expFlexDatas.find(x => x.efD_EF_Id == col.eF_ID);
                        selExpDetails.expFlexDatas = selExpDetails.expFlexDatas.filter(x => x.efD_EF_Id !== col.eF_ID);
                        oldEFId = IsEmpty(fldata) ? 0 : fldata.efD_Id;
                    }

                    if (!IsEmpty(cntrlval))
                        ExpFlexData.push(GetExpFlexObj(oldEFId, selExpDtlId, col.eF_ID, col.eF_Label, cntrlval, FlexType, true, apiAgentInfo.LoginUserId));
                }                
            });

            if (!IsEmpty(selExpDetails) && !IsEmpty(selExpDetails.expFlexDatas) && selExpDetails.expFlexDatas.length > 0) {

                $.each(selExpDetails.expFlexDatas, function (key, col) {

                    ExpFlexData.push(GetExpFlexObj(col.EFD_Id, selExpDtlId, col.EFD_EF_Id, col.EFD_Label, col.EFD_Data, FlexType, false, apiAgentInfo.LoginUserId));
                });
            }

            return ExpFlexData;
        }

        /* To get expense warning messages */
        function GetExpMessages() {

            var ExpMessages = [];

            if (IsEmpty(policyMaster) || IsEmpty(expRepMessages) || expRepMessages.length == 0)
                return ExpMessages;

            var expmsgs = IsEmpty(selExpDetails) || IsEmpty(selExpDetails.expMessages) || selExpDetails.expMessages.length == 0 ? [] : selExpDetails.expMessages

            $.each(expRepMessages, function (key, col) {
                
                var oldmsgs = expmsgs.filter(x => x.erM_Message === col);
                expmsgs = expmsgs.filter(x => x.erM_Message !== col);

                if (IsEmpty(oldmsgs) || oldmsgs.length == 0)
                    ExpMessages.push(GetExpRepMsgObj(0, selExpDtlId, col, true, apiAgentInfo.LoginUserId));          
            });

            if (expmsgs.length > 0) {

                $.each(expmsgs, function (key, col) {
                    
                    ExpMessages.push(GetExpRepMsgObj(col.erM_Id, selExpDtlId, col.erM_Message, false, apiAgentInfo.LoginUserId));          
                });
            }

            return ExpMessages;
        }

        /* To validate save info */
        function ValidateSave() {

            var IsValid = ValidateFlex('D');

            if (normalFlex.length > 0)
                IsValid = ValidateFlex('N') && IsValid;

            var openItemizeTab = IsValid;
            if (itemFlex.length > 0)
                IsValid = ValidateFlex('I') && IsValid;

            if (openItemizeTab && !IsValid)
                $('#btnItemize').click();

            if (expAttendeeIds.length > 1)
                IsValid = ValidateAttendee() && IsValid;
                        
            IsValid = ValidateAttendeePolicy() && IsValid;
            IsValid = ValidatePolicy('S') && IsValid;

            if (document.getElementById('myTab').style.display != 'none')
                IsValid = CompareItemizeTotal() && IsValid;

            if (IsValid && expAttendeeIds.length > 1) {

                IsValid = Math.ceil(attendeeBal) == 0;
                if (!IsValid)
                    ShowError('Attendee total amount should match, expense total amount');
            }

            dzUploadedfiles = $('.dz-filename');
            if (dzUploadedfiles.length == 0 && allUploadedRceipts.length == 0 && $('#ddlReceiptStatus').val() == 'RU') {

                ShowError('Please upload receipt.');
                IsValid = false;
            }

            if ((dzUploadedfiles.length > 0 || allUploadedRceipts.length > 0) && $('#ddlReceiptStatus').val() == 'NR') {

                ShowError('Please remove receipt(s) as receipt status is selcted as No Receipt.');
                IsValid = false;
            }
            return IsValid;
        }

        /* To validate default flex fields */
        function ValidateFlex(Type) {

            var IsValid = true;

            var flxDetails = Type == 'D' ? defFlex : Type == 'N' ? normalFlex : itemFlex;
            flxDetails = flxDetails.filter(x => x.eF_Mandatory == 'Y' && x.eF_Control !== 'CHECKBOX');

            $.each(flxDetails, function (key, col) {

                var divId = 'div' + GetCntrlId(col.eF_Label);
                if (document.getElementById(divId).style.display != 'none') {

                    var cntrlId = (CompFields(col.eF_Control, 'DDL') ? 'ddl' : 'txt') + GetCntrlId(col.eF_Label);
                    IsValid = CheckReqData(cntrlId, col.eF_DataType, '1000', col.eF_Label, GetInvalidValues(col.eF_Label))  && IsValid;
                }                
            });

            return IsValid;
        }

        /* To get invalid values for default flex fields */
        function GetInvalidValues(fieldname) {

            var invalidValues = mt;

            if (CompFields(fieldname, 'Units') || CompFields(fieldname, 'Unit Amount') || CompFields(fieldname, 'Total Amount') ||
                CompFields(fieldname, 'Amount In') || CompFields(fieldname, 'Exchange Rate')) {

                invalidValues = AgentDecimalRound(0);
            }
            return invalidValues;
        }

        /* To round off value to agent decimals */
        function AgentFixedDecimal(val) {

            return FixedDecimalRound(val, <%=Settings.LoginInfo.DecimalValue%>);
        }

        /* To round off agent decimal value */
        function AgentDecimalRound(val) {

            return DecimalRound(val, <%=Settings.LoginInfo.DecimalValue%>, 'AE');
        }

        /* To get agent and login info */
        function GetAgentInfo() {

            try {

                var loginInfo = '<%=Settings.LoginInfo == null%>';

                if (loginInfo == true)
                    return apiAgentInfo;

                agentName = '<%=Settings.LoginInfo.AgentName%>';
                agentCurrrency = '<%=Settings.LoginInfo.Currency%>';
                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {

            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';

            if (IsEmpty(apiHost))                                 
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";

            return apiHost;
        }

    </script>

    <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-xl-10">
                    <div class="d-flex flex-wrap header-blocks">
                        <div class="flex-fill item col-12 col-lg-auto px-4">
                            <label class="d-block">Report Title</label>
                            <h3 id="hdrTitle" class=" m-0 title pt-0 float-md-left text-primary"></h3>
                        </div>
                        <div class="flex-fill pr-2 item">
                            <span class="icon icon-calendar"></span>
                            <label>Date</label>
                            <div id="divhdrDate" class="font-weight-bold"></div>
                        </div>
                        <div class="flex-fill pr-2 item">
                            <span class="icon icon-files"></span>
                            <label>Report Ref</label>
                            <div id="divhdrRefNo" class="font-weight-bold"></div>
                        </div>
                        <div class="flex-fill pr-2 item">
                            <span class="icon icon-id-card"></span>
                            <label>Employee ID</label>
                            <div id="divhdrEmpCode" class="font-weight-bold"></div>
                        </div>
                        <div class="flex-fill pr-2 item">
                            <span class="icon icon-user2"></span>
                            <label>Employee Name</label>
                            <div id="divhdrEmpName" class="font-weight-bold"></div>
                        </div>
                        <div class="flex-fill item">
                            <span class="icon icon-budget"></span>
                            <label>Cost Center</label>
                            <div id="divhdrCostCentre" class="font-weight-bold"></div>
                        </div>
                    </div>

                </div>
                <div id="divDelSubmit" class="col-12 col-xl-2 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <a class="btn btn-danger mr-2 " onclick="return SubDelExpReport('D');">DELETE <i class="icon icon-delete "></i></a>
                    <a class="btn btn-primary " onclick="return SubConfirm('S');">SUBMIT <i class="icon icon-play_arrow "></i></a>
                </div>
            </div>
        </div>
        <div class="c-exp-main-wrapper position-relative">
            <div class="c-exp-left-block" id="createExp-left-block">
             
                <div id="errMess" class="error_module mt-2 mb-2 exp-content-block  p-0 py-2"" style="display:none; ">                        
                </div>
                

                <div class="exp-content-block">
                    <ul class="nav nav-tabs" id="myTab" role="tablist" style="display: none;">
                        <li class="nav-item" role="presentation">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#expense-tab" role="tab" aria-controls="expense-tab" aria-selected="true">Expense</a>
                        </li>
                        <li class="nav-item" role="presentation">
                            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#itemize-tab" role="tab" aria-controls="itemize-tab" aria-selected="false">Trip to Toulouse Expenses</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="myTabContent">
                        <div class="tab-pane fade in show active pt-4" id="expense-tab" role="tabpanel" aria-labelledby="expense-tab"></div>
                        <div class="tab-pane fade  pt-4" id="itemize-tab" role="tabpanel" aria-labelledby="itemize-tab"></div>
                    </div>
                </div>
                <div id="divOpenReceipts" class="exp-content-block">                    
                    <a href="javascript:void(0)l" class="btn btn-info" data-toggle="collapse" data-target="#receiptsCollapse" aria-expanded="false" aria-controls="receiptsCollapse">Receipts</a>
                    <div class="col-12 div-receipts collapse" id="receiptsCollapse">
                        <a href="javascript:void(0);" class="float-right close-btn close-collapse-panel" id="receiptsCollapsePanel"><span class="icon icon-close"></span></a>
                        <div class="clear"></div>
                        <div id="divAddDelReceipts" class="button-controls mt-2 ">
                            <a class="btn btn-primary float-right" onclick="return GetOpenReceipts();" id="btnSelectReceipts">Add Uploaded Receipts<i class="icon icon-plus"></i></a>
                            <a class="btn btn-danger float-right" onclick="return DeleteReceipts();" id="btnDeleteReceipts">Delete Receipts <i class="icon icon-delete2"></i></a>
                        </div>
                        <div class="clear"></div>
                        <div class="table-responsive">
                            <table class="table small mt-2 exp-sidetable" id="ReceiptsTableList">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="custom-checkbox-style dark">
                                                <input id="chkAllUploadedReceipts" onclick="SelectAllUploadedReceipts(this)" type="checkbox" class="form-control"><label></label>
                                            </div>
                                        </th>
                                        <th>Date</th>
                                        <th>File Name</th>
                                        <th>Image</th>
                                    </tr>
                                </thead>
                                <tbody id="tbUploadedReceipts"></tbody>
                            </table>
                        </div>
                    </div>                               
                </div>
                <div id="divdz" class="exp-content-block exp-file-upload-wrap flex-wrap flex-md-nowrap"> 
                    <div action="/file-upload" class="dropzone" id="expense-dropzone"></div>
                    <div id="tpl-container" class="dropzone-previews"></div>
                </div>
                <div class="float-right">
                    <div class="form-row">
                        <div class="tab-pane fade in show active pt-4">
                            <div id="divSaveClear" class="col-md-12 text-right mt-0 mb-5">
                                <a class="btn btn-secondary" onclick="return ClearExpense();">CLEAR</a>
                                <a class="btn btn-primary" onclick="return ValDuplicateExp('S');">SAVE</a>
                            </div>                            
                        </div>
                    </div>
                </div>
            </div>
            <a href="javascript:void(0);" class="show-side-panel" id="showSidePanel"><span class="icon icon-double_arrow"></span></a>
            <div class="c-exp-right-block" id="createExp-right-block">
                <div class="exp-content-block position-relative" style="min-height:100%;">
                    <a href="javascript:void(0);" class="float-right close-btn close-side-panel"><span class="icon icon-double_arrow"></span></a>
                    <h5 class="mb-3 float-left mt-2 ">Expense Details</h5>
                    <div id="divCopyDelete" class="button-controls text-right mt-2 ">
                        <a onclick="CopySelExpDetails();" class="btn btn-secondary">COPY  <i class="icon icon-content_copy"></i></a>
                        <a onclick="DelSelExpDetails();" class="btn btn-danger">DELETE  <i class="icon icon-delete2"></i></a>
                    </div>
                    <div class="clear"></div>
                    <div class="table-responsive">
                        <table class="table small mt-2 exp-sidetable" id="tblExpDtls">
                            <thead>
                                <tr>
                                    <th>
                                        <div class="custom-checkbox-style dark">
                                            <input id="chkAllExpDtlGrid" onclick="SelectAllExpenses(this);" type="checkbox" class="form-control"><label></label>
                                        </div>
                                    </th>
                                    <th>Date</th>
                                    <th>Expense Type</th>
                                    <th>Amount</th>
                                    <th>Generate Receipt</th>
                                </tr>
                            </thead>
                            <tbody id="tbExpDetails"></tbody>
                        </table>
                    </div>
                   
                </div>
            </div>
        </div>
        <div class="clear "></div>
    </div>
    <!-- Expense categories and types popup -->
    <div id="divExpTypesPopup" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header py-3">
                    <button type="button" class="close" data-dismiss="modal" id="btnModelClose" style="margin-top: -6px;">×</button>
                    <h5 class="modal-title">Select Expense Type</h5>        
                </div>
                <div class="modal-body">
                   <div class="form-group">

                        <div class="alert alert-warning py-2" role="alert">
                            To get the accurate exchange rates, click on the oanda website provided in the expense creation page
                        </div>
                        <div id="divExpCatTypes" class="list-unstyled d-flex flex-wrap"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Attendee master popup -->
    <div id="divAttendeesPopup" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                   <div class="form-group">
                        <div class="col-12 div-attendees" id="">                            
                            <div class="button-controls mt-2 ">
                                <a onclick="AddAMInfo();" class="btn btn-primary float-right" id="addNewAttendeeBtn">ADD ATTENDEES <i class="icon icon-plus"></i></a>
                            </div>
                            <div class="clear"></div>
                            <div id="divAMData"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Uploaded files popup -->
    <div id="divOpenReceiptsPopup" class="modal fade" role="dialog">
        <div class="modal-dialog modal-lg">
            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-body">
                   <div class="form-group">        
                        <div class="button-controls mt-2 ">
                            <a onclick="AddUploadedReceipts();" class="btn btn-primary float-right" id="btnAddReceipts">ADD RECEIPTS <i class="icon icon-plus"></i></a>
                        </div>
                        <div class="table-responsive">
                            <table class="table small mt-2 exp-sidetable" id="tblRceipts">
                                <thead>
                                    <tr>
                                        <th>
                                            <div class="custom-checkbox-style dark">
                                                <input id="chkAllOpenReceipts" onclick="SelectAllOpenReceipts(this);" type="checkbox" class="form-control"><label></label>
                                            </div>
                                        </th>
                                        <th>Date</th>
                                        <th>Rceipt Name</th>
                                        <th>Ref No</th>
                                    </tr>
                                </thead>
                                <tbody id="tbOpenReceipts"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Expense report submit confirm popup -->
    <div class="modal fade" id="divSubmitConfirm" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header" style="color: white">
                    <span ID="title" Style="color: white; text-align: center; margin-top: 15px">User Electronic Agreement</span>
                    <button type="button" class="close" data-dismiss="modal" style="height: 10px; width: 10px" aria-label="Close">
                        <span aria-hidden="true">X</span>
                    </button>
                </div>
                <div class="modal-body">
                <p class="mb-2"><strong>By clicking on 'Accept & Submit' button, I Certify that:</strong></p>
                <ul>
                    <li>
                        1.This is a true and accurate accounting of expenses incurred to accomplish official business for the Company and there are no expenses claimed as reimbursable which relate to personal or unallowable expenses.                
                    </li>
                    <li>2.All required receipt images have been attached to this report.</li>
                    <li> 3.I have not received,nor will I recieve, reimbursement from any other source(s) for the expenses claimed.</li>
                    <li>4.In the event of overpayment or if payment is received from another source for any portion of the expenses claimed I assume responsibility for repaying the Company in full for those expenses.</li>
                </ul>

                <p class="mb-2"><strong>Reminder:Receipts Required!</strong></p>

               <p> According to company policy, you must provide receipts for the expenses listed below.
                Original paper receipts must be submitted.
                If you have already provided receipts,you can submit your report now.</p>
          </div>
                <div class="modal-footer">
                    <a class="btn btn-secondary" data-dismiss="modal" >Cancel</a>
                    <a class="btn btn-primary" onclick="SubDelExpReport('S')">Accept & Submit</a>
                </div>
            </div>
        </div>
    </div>    
    <%--<script src="build/js/dropzone.js "></script>--%>
    <script>
        $(function () {

            $('.close-side-panel').on('click', function () {

                $('body').removeClass('side-panel-enabled')
                $('#showSidePanel').fadeIn();
            });

            $('#showSidePanel').on('click', function () {

                $('body').addClass('side-panel-enabled')
                $(this).fadeOut();
            });

            $(document).on('click', '.show-itemize', function () {

                $('#myTab').show()
                $('#myTab li:last-child a').tab('show')
            });

            $(document).on('click', '#closeCollapsePanel', function () {

                $('#attendeesCollapse').collapse('hide');
            });

            $(document).on('click', '#receiptsCollapsePanel', function () {

                $('#receiptsCollapse').collapse('hide');
            });
        });        
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
