﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="TransferSearch.aspx.cs" Inherits="CozmoB2BWebApp.TransferSearch" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
        <style>
        .error {
            border: 1px solid #D8000C;
        }    
        .disabled { color:lightgray; pointer-events:none; }
    </style>       
<%--    <link href="App_Themes/Default/BookingStyle.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/cozmovisa-style.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/main-style.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/style.css" type="text/css" rel="stylesheet" />--%>

    <!-- Required styles for Hotel Listing-->
    <link href="build/css/royalslider.css" rel="stylesheet" type="text/css" />
    <link href="build/css/rs-default.css" rel="stylesheet" type="text/css" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />

   
    <style>
        .error {
            border: 1px solid #D8000C;
        }    
        .disabled { color:lightgray; pointer-events:none; }
    </style>
    <input type="hidden" id="hdnAgentId" runat="server" />
    <input type="hidden" id="hdnbeHalfAgent" value="0" />
    <input type="hidden" id="agentLocation" runat="server" />
        <asp:HiddenField ID="hdnSearchSubmit" runat="server"  />
      <div class="banner-container" style="background-image:url(build/img/main-bg.jpg)">
      <div class="row no-gutter">
          <div class="col-12 col-lg-6">
            <div id="tabbed-menu" class="search-container">
                 <ul class="tabbed-menu nav">  
                  <%--  <li>
                        <a  id="lnkFlights"  href="HotelSearch.aspx?source=Flight">                   
                          Flight
                        </a> 
                    </li> --%>                          
                    <li>
                        <a id="lnkHotels"  href="HotelSearch.aspx?source=Hotel">
                         Hotel
                        </a>
                    </li>      
                    <li>
                        <a  id="lnkACtivity" href="javascript:void(0)" class="current">
                         Transfers
                        </a>
                    </li>                                                   
                 </ul>
				 <div id="errMess" class="error_module" style="display: none;">
                        <div id="errorMessage"  class="alert-messages alert-caution">
                        </div>
                     </div> 
                 <div class="search-matrix-wrap auto-height p-4 search-page-form">   
                
                     
                     <div class="row no-gutter">
                         <div class="col-6 col-lg-6">                              
                             <div class="form-group custom-radio-table">
                                <input type="radio" name="agntType" value="0" id="Self" checked  />    
                                <label for="Self" class="pr-3">Self</label>
                                <input type="radio" name="agntType" value="1" id="Client"  />    
                                <label for="Client">Client</label><br>
                            </div>                            
                         </div>
                         
                     </div>
                     <div class="row no-gutter" id="behalfAgent" style="display:none;">
                         <div class="col-6 col-lg-6">                              
                            <div class="form-group" >
                               <div class="icon-holder">
                                     <span class="" aria-label="Pickup">Select Agent</span>
                                 </div>
                                <select id="b2bAgentSelect" class="form-control select2">

                                </select>
                            </div>                           
                         </div>
                         <div class="col-6 col-lg-6">                              
                            <div class="form-group" >
                               <div class="icon-holder">
                                     <span class="" aria-label="Pickup">Select Location</span>
                                 </div>
                                <select id="b2bAgentLocation" class="form-control select2">

                                </select>
                            </div>                           
                         </div>
                     </div>
  
                     
                     <div class="row no-gutters">
                         <div class="col-12 col-sm">
                            <label>Pickup</label>
                             <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-location" aria-label="Pickup"></span>
                                 </div>
                                 <input id="Picup" class="form-control origin-airport" placeholder="Pickup"  />
                                 <asp:HiddenField ID="hdnPickUpLongtitude" value="0" runat="server" />
                                 <asp:HiddenField ID="hdnPickUplatitude" value="0" runat="server" />
                             </div>          
                         </div>
                         <div class="col swap-airports-wrap mt-4 pt-3" id="swapAirport">
                              <a href="javascript:void(0);" class="swap-airports" tabindex="-1" title="Switch"> <span class="icon-loop-alt4"></span></a>
                         </div>                           
                         <div class="col-12 col-sm">
                              <label>Destination</label>
                              <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-location" aria-label="Origin Destination"></span>
                                 </div>
                                 <input id="Destination" class="form-control dest-airport" placeholder="Destination" />   
                                 <asp:HiddenField ID="hdnDropOffLatitude" value="0" runat="server" />
                                 <asp:HiddenField ID="hdnDropOffLongtitude" value="0" runat="server" />
                             </div>
                         </div>
                    </div>
                    <div class="row custom-gutter">
                        <div class="col-12 col-lg-4 d-none" id="flightDiv">
                            <label>Airline</label>
                            <div class="form-control-holder">
                                <div class="icon-holder">
                                    <span class="icon-location" aria-label="Airline"></span>
                                </div>
                                <select id="AirlineCode" class="form-control">

                                </select>
                            </div>          
                         </div>
                         <div class="col-12 col-lg-4 d-none" id="flightNoDiv">
                              <label>Flight Number</label>
                              <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-location" aria-label="Flight number"></span>
                                 </div>
                                 <input id="flightNumber" class="form-control" placeholder="Flight Number" />  
                                
                             </div>
                         </div>
                        <div class="col-12 col-lg-4 d-none" id="departurCityDiv">
                              <label>Departure City</label>
                              <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-location" aria-label="Diparture City"></span>
                                 </div>
                                 <input id="departureCity" class="form-control" placeholder="Departure City Name" />                                  
                             </div>
                         </div>
                        
                    </div>                     
                    <div class="row custom-gutter">
                        <!--Conditional Textbox - Enable below if Pickup is from Airport -->
                         <div class="col-12 col-lg-3 d-none">  
                            <label>Flight Number</label>                           
                             <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-location" aria-label="Flight Number"></span>
                                 </div>
                                 <asp:TextBox  ID="TextBox2" CssClass="form-control" placeholder="Flight No." runat="server"></asp:TextBox>   
                             </div>
                        </div>
                         <div class="col-12 col-lg-3">  
                             
                            <label>Pickup Date</label>                           
                             <a class="form-control-holder"  href="javascript:void(null)" onclick="showFlightCalendar()">
                                <div class="icon-holder">
                                    <span class="icon-calendar" aria-label=""></span>
                                </div>       
                                <asp:TextBox ID="PickupDate" placeholder="Pickup Date" CssClass="form-control" runat="server" data-calendar-contentID="#pickupCalendarContainer"  autocomplete="off" onclick="showFlightCalendar()" ReadOnly="true"></asp:TextBox>                                        
                             </a>
                             <div id="pickupCalendarContainer" style="display:none;"></div>
                        </div>
                         <div class="col-12 col-lg-3"> 
                            <label>Pickup Time</label>
                            <div class="form-control-holder">
                                <div class="icon-holder">
                                    <span class="icon-clock" aria-label=""></span>
                                </div>  
                                <select class="form-control" id="PicUpTime">                                       
                                </select>       
                            </div> 
                         </div>
                   
                         <div class="col-12 col-lg-3"> 
                            <label>Passengers</label>
                            <div class="form-control-holder">
                                <div class="icon-holder">
                                    <span class="icon-group" aria-label=""></span>
                                </div>  
                                <asp:DropDownList runat="server" CssClass="form-control" ID="passengerCount">
                                        <asp:ListItem>1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                        <asp:ListItem>4</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                </asp:DropDownList>    
                            </div>     
                         </div>
                         <div class="col-12 col-lg-3"> 
                            <label>Luggage</label>
                            <div class="form-control-holder">
                                <div class="icon-holder">
                                    <span class="icon-baggage" aria-label=""></span>
                                </div>  
                                <asp:DropDownList runat="server" CssClass="form-control" ID="Luggage">
                                        <asp:ListItem>0</asp:ListItem>
                                        <asp:ListItem>1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                        <asp:ListItem>4</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                </asp:DropDownList>    
                            </div>     
                         </div>
                         <div class="col-12 col-lg-3"> 
                            <label>Pets</label>
                            <div class="form-control-holder">
                                <div class="icon-holder">
                                    <span class="icon-dog" aria-label=""></span>
                                </div>  
                                <asp:DropDownList runat="server" CssClass="form-control" ID="PetCount">
                                        <asp:ListItem>0</asp:ListItem>
                                        <asp:ListItem>1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                        <asp:ListItem>4</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                </asp:DropDownList>    
                            </div>     
                         </div>
                        <div class="col-12 col-lg-3"> 
                            <label>Sports Luggage</label>
                            <div class="form-control-holder">
                                <div class="icon-holder">
                                    <span class="icon-baggage" aria-label=""></span>
                                </div>  
                                <asp:DropDownList runat="server" CssClass="form-control" ID="SportsLuggage">
                                        <asp:ListItem>0</asp:ListItem>
                                        <asp:ListItem>1</asp:ListItem>
                                        <asp:ListItem>2</asp:ListItem>
                                        <asp:ListItem>3</asp:ListItem>
                                        <asp:ListItem>4</asp:ListItem>
                                        <asp:ListItem>5</asp:ListItem>
                                </asp:DropDownList>    
                            </div>     
                         </div>


                          <div class="col-12 col-lg-4">
                            <label>Child Seats</label>
                                <div class="custom-dropdown-wrapper">
                                   
                                    <a href="jaavscript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#travellerDropdown">
                                        <div class="icon-holder">
 	                                         <span class="icon-group"></span>
                                        </div>
                                        <span class="form-control-text" id=""><strong></strong> Child Seats</span>               
                                    </a>
                                    <div class="dropdown-content right-aligned p-4 d-none" id="travellerDropdown" style="max-width: 200px;">                                       
                                        <div class="row no-gutters">                                             
                                              <div class="col-12">
                                                   <span class="pt-2 d-block"> 3-6 years (15-25kg)</span>
                                                  <select  class="form-control no-select2 childSeat1" id="childSeat1">
                                                      <option selected="selected" value="0">0</option>
	                                                    <option value="1">1</option>
	                                                    <option value="2">2</option>
	                                                    <option value="3">3</option>
	                                                    <option value="4">4</option>
	                                                    <option value="5">5</option>
	                                                    <option value="6">6</option>
	                                                    <option value="7">7</option>
	                                                    <option value="8">8</option>
	                                                    <option value="9">9</option>

                                                    </select>
                                              </div>
                                          </div> 
                                        <div class="row no-gutters">
                                              <div class="col-12">
                                                   <span class="pt-2 d-block"> 6-12 years (22-36kg)</span>
                                                  <select class="form-control no-select2 childSeat2" id="childSeat2">
	                                                <option selected="selected" value="0">0</option>
	                                                <option value="1">1</option>
	                                                <option value="2">2</option>
	                                                <option value="3">3</option>
	                                                <option value="4">4</option>
	                                                <option value="5">5</option>
	                                                <option value="6">6</option>
	                                                <option value="7">7</option>
	                                                <option value="8">8</option>

                                                </select>
                                              </div>
                                          </div> 
                                    
                                          <a href="javascript:void(0);" class="mt-2 btn btn-primary btn-sm float-right dd-done-btn">DONE</a>       
                                    </div>
                                </div>
                            </div>

                         <div class="col-12 col-lg-3">         
                              <label>Voucher Code</label>                                 
                             <input id="VoucherCode" placeholder="Voucher Code" class="form-control" type="text"/>
                        </div>
                        <div class="col-12 col-md-9" id="supplierDiv">
                            <label>Search Suppliers</label>
                                <div class="custom-dropdown-wrapper">
                                    <a href="jaavscript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#TransferSupplierDropdown">
                                        <div class="form-control-holder">
                                            <div class="icon-holder">                                       
                                                    <span class="icon-global-settings"></span>                                     
                                            </div>   
                                            <span class="form-control-text" id="htlSelectedSuppliers">Search Suppliers</span>                                  
                                        </div>  
                                    </a>
                                    <div class="dropdown-content d-none p-4" id="TransferSupplierDropdown">                         
                                        <div class="row no-gutters">                                            
                                            <div class="col-md-12">
                                                <strong> Search Suppliers : </strong>
                                            </div>     
                                            <div class="col-md-12">                               
                                                <table width="100%" id="tblSources" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" enableviewstate="true" runat="server" border="0" cellspacing="0" cellpadding="0"></table>  
                                                <a href="javascript:void(0);" class="btn btn-primary btn-sm float-right dd-done-btn">DONE</a>
                                            </div>                                             
                                        </div>
                                    </div>
                                </div>
                            </div>
                    </div>
                 


                     <div class="row mt-3">
                         <div class="col-xs-12 py-3">
                             <button class="but but_b pull-right" id="searchBtn">Search</button>
                                

                         </div>
                     </div>




            </div>
         </div>
       </div>
    </div>

        
        </div>

<input type="hidden" id="hdnAgentType" runat="server" />
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0mr4cq7HrZgJb9TZMLYWk-yh-_Ahg9Ro&libraries=places&callback=initAutocomplete" async defer></script>
<script>
    var agentType = document.getElementById('<%=hdnAgentType.ClientID %>').value;
    if (agentType != "1" && agentType != "2") {
        $("#supplierDiv").hide();
    }
    
    var PicUpCityCountry;
    var isAirport = false;
    function initAutocomplete() {

        // Create the autocomplete object, restricting the search predictions to
        // geographical location types.
        var options = {
           // types: ['geocode']
        };
        autocompletePicUp = new google.maps.places.Autocomplete(
            document.getElementById('Picup'),
            options
        );
        autocompleteDropOff = new google.maps.places.Autocomplete(
            document.getElementById('Destination'),
            options
        );

        google.maps.event.addListener(autocompletePicUp, 'place_changed', function () {
            var place = autocompletePicUp.getPlace();
            
            if ($.inArray('airport', place.types) != "-1") {
                isAirport = true;
                getAirlineDetails();
            }
            else {
                isAirport = false;
                $("#flightDiv").addClass('d-none');
                $("#flightNumber").val("");
                $("#flightNoDiv").addClass('d-none');
                $("#departureCity").val("");
                $("#departurCityDiv").addClass('d-none');
                $("#AirlineCode").children().remove();
            }
            PicUpCityCountry = GetCountry(place) + '|' + GetCityNamePOI(place).split('|')[0];
            $('#<%=hdnPickUpLongtitude.ClientID %>').val(place.geometry.location.lng());
            $('#<%=hdnPickUplatitude.ClientID %>').val(place.geometry.location.lat());
           
        });
        google.maps.event.addListener(autocompleteDropOff, 'place_changed', function () {
            var place = autocompleteDropOff.getPlace();
            var DropOffCityCountry = GetCountry(place) + '|' + GetCityNamePOI(place).split('|')[0];
            $('#<%=hdnDropOffLatitude.ClientID %>').val(place.geometry.location.lat());
            $('#<%=hdnDropOffLongtitude.ClientID %>').val(place.geometry.location.lng());
        });
    }
    if (localStorage.getItem("transferSearch") != null) {
        localStorage.removeItem("transferSearch");
    }
    $(document).ready(function () {  
        var currentDate = new Date();
        var picUpMinDate = new Date(currentDate.getFullYear(), currentDate.getMonth(), currentDate.getDate() + 1);
        var configOptions = {
            minDate: (picUpMinDate.getMonth() + 1) + "/" + picUpMinDate.getDate() + "/" + picUpMinDate.getFullYear(),
        };
        function DateFormating(dtString) {
            var dateSplit = dtString.split('/');
            return dateSplit[2] + "-" + dateSplit[1] + "-" + dateSplit[0];

        }
        $("#searchBtn").on('click', function (e) {            
            e.preventDefault();
            if (Validate()) {
                var PicUp = {
                    PickUpLatitude: $('#<%=hdnPickUplatitude.ClientID %>').val(),
                    PickUpLongitude: $('#<%=hdnPickUpLongtitude.ClientID %>').val(),
                    PicUPPlace: $("#Picup").val()
                }
                var DropOff = {
                    DropOffLatitude: $('#<%=hdnDropOffLatitude.ClientID %>').val(),
                    DropOffLongitude: $('#<%=hdnDropOffLongtitude.ClientID %>').val(),
                    DropOffPlace: $("#Destination").val()
                }

                let selectedSupplier = $("#ctl00_cphTransaction_tblSources").find('input[type="checkbox"]:checked').not('#ctl00_cphTransaction_chkSource0');
                let selectdSupplierValue = [];
                $.each(selectedSupplier, function (ind, value) {
                    selectdSupplierValue.push($(value).val());
                });
                var childseat1 = 0;
                var childseat2 = 0;
                if ($("#childSeat1").val() != "" || $("#childSeat1").val() != null) {
                    childseat1 = parseInt($("#childSeat1").val())
                }
                if ($("#childSeat2").val() != "" || $("#childSeat2").val() != null) {
                    childseat2 = parseInt($("#childSeat2").val())
                }
               
                

                var data = {
                    TrasnferDate:DateFormating($('#<%=PickupDate.ClientID %>').val()),
                    PickUpTime: $('#PicUpTime').val(),
                    NumberOfPassengers: $('#<%=passengerCount.ClientID %>').val(),
                    AnimalLuggage: $('#<%=PetCount.ClientID %>').val(),
                    Luggage: $('#<%=Luggage.ClientID %>').val(),
                    SportLuggage:$('#<%=SportsLuggage.ClientID %>').val(),
                    PickUpPoint: PicUp,
                    DropOffPoint: DropOff,
                    Sources: selectdSupplierValue,
                    NationalityCode: PicUpCityCountry,
                    ChildSeat1:childseat1,
                    ChildSeat2: childseat2,
                    VoucherCode: $("#VoucherCode").val()
                }
                
                 if ($("input[name='agntType']:checked").val() == "1") {
                     data["beHalfAgent"] = $("#b2bAgentSelect").val();
                     data["AgentLocation"] = $("#b2bAgentLocation").val();
                }
                 else {
                     data["beHalfAgent"] = 0;
                     data["AgentLocation"]=$("#ctl00_cphTransaction_agentLocation").val();
                }
                if (isAirport) {
                    data["flight_code"] = $("#AirlineCode").val();
                    data["flight_number"]= $("#flightNumber").val()
                    data["departure_city"] = $("#departureCity").val();
                    
                }
                else {
                    data["flight_code"] = "0";
                    data["flight_number"] = "";
                    data["departure_city"] = "";
                }
                data["isAirport"] = isAirport;
                localStorage.setItem("transferSearch", JSON.stringify(data));                
                   document.getElementById('<%=hdnSearchSubmit.ClientID %>').value = "search"
                  document.forms[0].submit();
            }
            
        });

        $("input[name='agntType']").on('change', function () {
            $("#b2bAgentSelect").children().remove();
            var agentID=JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
            if ($(this).val() == "1") {
                $("#behalfAgent").css('display', 'block');
                $.ajax({
                    url: "TransferSearch.aspx/LoadB2BAgents",
                    type: "POST",
                    data: JSON.stringify({ agentId: agentID }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (agentResponse) {
                        let agentData = JSON.parse(agentResponse.d);
                        $("#b2bAgentSelect").append('<option value="-1" selected>--Select B2B Agent--</option>')
                        if (agentData.length > 0) {
                            $.each(agentData, function (agentIndex, agentValue) {
                                $("#b2bAgentSelect").append('<option value="' + agentValue.AGENT_ID + '">' + agentValue.AGENT_NAME + '</option>');
                            });
                        }
                        $("#b2bAgentSelect").select2('val', "-1");
                    }
                });
            }
            else {
                $("#behalfAgent").css('display', 'none');
            }            
        });

        $("#b2bAgentSelect").on('change', function () {
            $("#b2bAgentLocation").children().remove();
            if ($(this).val() != "-1") {
                $.ajax({
                    url: "TransferSearch.aspx/LoadLocation",
                    type: "POST",
                    data: JSON.stringify({ agentId: $(this).val() }),
                    dataType: "json",
                    contentType: "application/json; charset=utf-8",
                    success: function (agentResponse) {
                        let agentData = JSON.parse(agentResponse.d);
                        $("#b2bAgentLocation").append('<option value="-1" selected>--Select Location--</option>')
                        if (agentData.length > 0) {
                            $.each(agentData, function (locIndex, locValue) {
                                $("#b2bAgentLocation").append('<option value="' + locValue.LOCATION_ID + '">' + locValue.LOCATION_NAME + '</option>');
                            });
                        }
                        $("#b2bAgentLocation").select2('val', "-1");
                    }
                });
            }
        });

            var startTime = "2016-08-10 00:00:00"
            var endTime = "2016-08-10 23:59:00"    
            var parseIn = function(date_time){
	            var d = new Date();
              d.setHours(date_time.substring(11,13));
	            d.setMinutes(date_time.substring(14,16));
  
              return d;
            }

            //make list
            var getTimeIntervals = function (time1, time2) {
	            var arr = [];
              while(time1 < time2){
  	            arr.push(time1.toTimeString().substring(0,5));
                time1.setMinutes(time1.getMinutes() + 15);
              }
              return arr;
            }

            startTime = parseIn(startTime);
            endTime = parseIn(endTime);

            var intervals = getTimeIntervals(startTime, endTime);
            $('#PicUpTime').children().remove();
        $.each(intervals, function (ind, valu) {
            if (ind == 0) {
                $('#PicUpTime').append('<option value="' + valu + '" selected="selected">' + valu + '</option>');
            }
            else {
                $('#PicUpTime').append('<option value="' + valu + '">' + valu + '</option>');
            }
        });
        $('#PicUpTime').select2('val', $('#PicUpTime option:eq(0)').val());
        var AgentBalance = 0;
        function GetBehalfAgent() {
          let AgentId=  document.getElementById('<%=hdnAgentId.ClientID %>').value
            $.ajax({
                url: "TransferResults.aspx/OnBehalfAgentDetails",
                type: "POST",
                data: JSON.stringify({ agentId: AgentId }),
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                success: function (agentResponse) {               
                    AgentBalance = agentResponse.d.CurrentBalance;

                    $("#ctl00_lblAgentBalance").text(AgentBalance.format(agentResponse.d.DecimalValue,2));
                }
            });
        }
        GetBehalfAgent();

        $("#AirlineCode").on('change', function () {            
            if ($("#AirlineCode").val() == "0") {
                $("#flightNumber").val("");
                $("#flightNoDiv").addClass('d-none');
                $("#departureCity").val("");
                $("#departurCityDiv").addClass('d-none');
            }
            else {
                $("#flightNumber").val("");
                $("#flightNoDiv").removeClass('d-none');
                $("#departureCity").val("");
                $("#departurCityDiv").removeClass('d-none');
            }
        });
        
    });
    function getAirlineDetails() {
        $.ajax({
            url: "TransferSearch.aspx/GetAllAirline",
            type: "POST",            
            dataType: "json",
            contentType: "application/json; charset=utf-8",
            success: function (agentResponse) {
                $("#flightDiv").removeClass('d-none');
                $("#flightNoDiv").removeClass('d-none');
                $("#departurCityDiv").removeClass('d-none');
                let responseJson = JSON.parse(agentResponse.d);
                $("#AirlineCode").children().remove();
                $("#AirlineCode").append('<option value="-1" selected>--Select Airline--</option>');
                $("#AirlineCode").append('<option value="0">No Flight</option>');
                if (responseJson.length > 0) {
                    $.each(responseJson, function (ind, valu) {
                        $("#AirlineCode").append('<option value="'+valu.airlineCode+'">('+valu.airlineCode+') '+valu.airlineName+'</option>')
                    });
                }
                $("#AirlineCode").select2('val', "-1");
            }
        });

    }

    function Validate() {
        var isValid = true;
        if ($("#Picup").val() != "") {
            if ($('#<%=hdnPickUplatitude.ClientID %>').val() == "" || $('#<%=hdnPickUplatitude.ClientID %>').val() == "0") {
                validateMessage("Please select pickup point", true);                
                isValid = false;
                return isValid;
            }
            if ($('#<%=hdnPickUpLongtitude.ClientID %>').val() == "" || $('#<%=hdnPickUpLongtitude.ClientID %>').val() == "0") {
                validateMessage("Please select pickup point", true);
                isValid = false;
                return isValid;
            }
            else {
                validateMessage("", false);
               
            }
        }
        else {
            validateMessage("Please select pickup point", true);
            isValid = false;
            return isValid;
        }
        if ($("#Destination").val() != "") {
            if ($('#<%=hdnDropOffLatitude.ClientID %>').val() == "" || $('#<%=hdnDropOffLatitude.ClientID %>').val() == "0") {
                validateMessage("Please select dropoff point", true);
                isValid = false;
                return isValid;
            }
            if ($('#<%=hdnDropOffLongtitude.ClientID %>').val() == "" || $('#<%=hdnDropOffLongtitude.ClientID %>').val() == "0") {
                validateMessage("Please select dropoff point", true);
                isValid = false;
                return isValid;
            }
            else {
                validateMessage("", false);                
            }
        }
        else {
            validateMessage("Please select dropoff point", true);
            isValid = false;
            return isValid;
        }

        if ($('#<%=PickupDate.ClientID %>').val() == "") {
            validateMessage("Please select pickup date", true);
            isValid = false;
            return isValid;
        }
        if ($('#PicUpTime').val() == "" || $('#PicUpTime').val() == null) {
            validateMessage("Please select pickup time", true);
            isValid = false;
            return isValid;
        }
        if ($('#<%=passengerCount.ClientID %>').val() == "0") {
            validateMessage("Please select passenger count", true);
            return isValid;
        }
        if ($("input[name='agntType']:checked").val() == "1") {
            if ($("#b2bAgentSelect").val() == "-1") {
                validateMessage("Please select on behalf of agent", true);
                 isValid = false;
                return isValid;
            }
            else if ($("#b2bAgentLocation").val() == "-1") {
                validateMessage("Please select agent location", true);
                isValid = false;
                return isValid;
            }
        }
        if (isAirport) {
            if ($("#AirlineCode").val() == "-1") {
                validateMessage("Please select Airline", true);
                isValid = false;
                return isValid;
            }
            else if ($("#AirlineCode").val() != "0") {
                if ($("#flightNumber").val() == "") {
                    validateMessage("Please enter flight number", true);
                    isValid = false;
                    return isValid;
                }
                if ($("#departureCity").val() == "") {
                    validateMessage("Please enter departure city", true);
                    isValid = false;
                    return isValid;
                }
            }
        }
        return isValid;
    }
    function validateMessage(message, isError) { 
        if (isError) {
            $("#errMess").css('display', 'block');
            $("#errorMessage").text(message);
        }
        else {
            $("#errMess").css('display', 'none');
            $("#errorMessage").text("");
        }
    }
    var cal1;
    function init() {
            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar
            cal1 = new YAHOO.widget.Calendar("cal1", "pickupCalendarContainer");
            //cal1 = new YAHOO.widget.Calendar("cal1", "Outcontainer1");
            cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            //            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDate);
            cal1.render();           
        }
    YAHOO.util.Event.addListener(window, "load", init);
    function showFlightCalendar() {            
            document.getElementById('pickupCalendarContainer').style.display = "block";  
    }

    function setDate() {
        let selectDate = cal1.getSelectedDates()[0];
        let month = selectDate.getMonth() + 1;
        let day = selectDate.getDate();
        if (month.toString().length == 1) {
            month = "0" + month;
        }
        if (day.toString().length == 1) {
            day = "0" + day;
        }
        document.getElementById('<%=PickupDate.ClientID %>').value = day + "/" + month + "/" + selectDate.getFullYear();
        cal1.hide();
    }

    function selectOrUnselectSupplier() {
        var isSelectAll = $("#ctl00_cphTransaction_chkSource0").prop('checked');
        if (isSelectAll) {
            $("#ctl00_cphTransaction_tblSources").find('input[type="checkbox"]').not('#ctl00_cphTransaction_chkSource0').prop('checked', true);
        }
        else {
            $("#ctl00_cphTransaction_tblSources").find('input[type="checkbox"]').not('#ctl00_cphTransaction_chkSource0').prop('checked', false);
        }
    }
    function setCheck(ctrl) {        
        let totalCheckBox = $("#ctl00_cphTransaction_tblSources").find('input[type="checkbox"]').not('#ctl00_cphTransaction_chkSource0').length;
        let selectedCheckBox = $("#ctl00_cphTransaction_tblSources").find('input[type="checkbox"]:checked').not('#ctl00_cphTransaction_chkSource0').length;
        if (totalCheckBox == selectedCheckBox) {
            $('#ctl00_cphTransaction_chkSource0').prop('checked', true);
        }
        else {
            $('#ctl00_cphTransaction_chkSource0').prop('checked', false);
        }
    }
    function GetCountry(place) {

        var sCountry = '';
        if (place != null && place.address_components != null && place.address_components.length > 0) {
            $.each(place.address_components, function (key, col) {

                if (col.types != null && col.types.length > 0 && col.types.indexOf('country') > -1)
                    sCountry = col.long_name + '|' + col.short_name;
            });
        }
        return IsEmpty(sCountry) ? place.formatted_address : sCountry;
    }

    /* To get country code and name from google maps result */
    function GetCityNamePOI(place) {

        var sCityName = '';

        if (place != null && place.address_components != null && place.address_components.length > 0) {

            $.each(place.address_components, function (key, col) {

                if (col.types != null && col.types.length > 1 && col.types.indexOf('locality') > -1 && col.types.indexOf('political') > -1)
                    sCityName = col.long_name + '|' + col.short_name;
                if (IsEmpty(sCityName) && col.types != null && col.types.length > 0 && col.types.indexOf('postal_town') > -1)
                    sCityName = col.long_name + '|' + col.short_name;
            });
        }

        return IsEmpty(sCityName) ? place.formatted_address : sCityName;
    }

    

    Number.prototype.format = function (n, x) {
        var re = '\\d(?=(\\d{' + (x || 3) + '})+' + (n > 0 ? '\\.' : '$') + ')';
        return this.toFixed(Math.max(0, ~~n)).replace(new RegExp(re, 'g'), '$&,');
    };
    

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
