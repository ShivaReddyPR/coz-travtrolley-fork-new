﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="TransferVoucher.aspx.cs" Inherits="CozmoB2BWebApp.TransferVoucher" %>

<!DOCTYPE html>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" lang="en" xml:lang="en" style="background: #f3f3f3!important">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width" />
    <title>Transfer Voucher</title>
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <style>
        @media only screen {
            html {
                min-height: 100%;
                background: #f3f3f3
            }
        }

        @media only screen and (max-width:716px) {
            .small-float-center {
                margin: 0 auto !important;
                float: none !important;
                text-align: center !important
            }
        }

        @media only screen and (max-width:716px) {
            table.body img {
                width: auto;
                height: auto
            }

            table.body center {
                min-width: 0 !important
            }

            table.body .container {
                width: 95% !important
            }

            table.body .columns {
                height: auto !important;
                -moz-box-sizing: border-box;
                -webkit-box-sizing: border-box;
                box-sizing: border-box;
                padding-left: 16px !important;
                padding-right: 16px !important
            }

                table.body .columns .columns {
                    padding-left: 0 !important;
                    padding-right: 0 !important
                }

            table.body .collapse .columns {
                padding-left: 0 !important;
                padding-right: 0 !important
            }

            th.small-6 {
                display: inline-block !important;
                width: 50% !important
            }

            th.small-12 {
                display: inline-block !important;
                width: 100% !important
            }

            .columns th.small-12 {
                display: block !important;
                width: 100% !important
            }

            table.menu {
                width: 100% !important
            }

                table.menu td,
                table.menu th {
                    width: auto !important;
                    display: inline-block !important
                }

                table.menu.vertical td,
                table.menu.vertical th {
                    display: block !important
                }

                table.menu[align=center] {
                    width: auto !important
                }
        }

        @media print {
            * {
                -webkit-print-color-adjust: exact !important;
                color-adjust: exact !important
            }
        }
    </style>
    <script language="javascript" src="Scripts/Common/Common.js" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts/Menu/query2.js"></script>
    <script src="Scripts/bootstrap.min.js"></script>
</head>

<body style="-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box; -webkit-text-size-adjust: 100%; margin: 0; background: #f3f3f3!important; box-sizing: border-box; color: #0a0a0a; font-family: 'Open Sans',Arial,sans-serif; font-size: 13px; font-weight: 400; line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100%!important">
    <span class="preheader" style="color: #f3f3f3; display: none!important; font-size: 1px; line-height: 1px; max-height: 0; max-width: 0; mso-hide: all!important; opacity: 0; overflow: hidden; visibility: hidden"></span>
    <table id="voucherBody" class="body" style="margin: 0; background: #f3f3f3!important; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: 'Open Sans',Arial,sans-serif; font-size: 13px; font-weight: 400; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
        <tr style="padding: 0; text-align: left; vertical-align: top">
            <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important; color: #0a0a0a; font-family: 'Open Sans',Arial,sans-serif; font-size: 13px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                <center data-parsed="data-parsed" style="min-width: 700px; width: 100%">
                    <table align="center" class="wrapper b2b-eticket-wrapper float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%">
                        <tr style="padding:0;text-align:left;vertical-align:top">
                            <td class="wrapper-inner" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                <table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                    <tbody>
                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                            <td height="16px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td>
                                        </tr>
                                    </tbody>
                                </table>
                                <table align="center" class="container" style="Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;margin:0 auto;padding:0;text-align:inherit;vertical-align:top;width:700px">
                                    <tbody>
                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                            <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
                                                <table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tbody>
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <td height="16px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:16px;font-weight:400;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                    <tbody>
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <th class="small-12 large-3 columns last"  style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:8px;padding-right:16px;text-align:left;width:159px">
                                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"><img id="imgLogo" src="" alt="AgentLogo" style="-ms-interpolation-mode:bicubic;border-width:0;clear:both;display:block;height:51px;max-width:100%;outline:0;text-decoration:none;width:159px;margin-left: 20px;" /></th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                            <th class="small-12 large-9 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:8px;text-align:left;width:509px">
                                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:center;vertical-align:top;width:100%">
                                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                                            <h2 style="Margin:0;Margin-bottom:10px;color:inherit;font-family:'Open Sans',Arial,sans-serif;font-size:22px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;word-wrap:normal"><strong>Transfer Voucher</strong></h2><span>Booking Date: <span id="BookedDate"></span></span>
                                                                                                                                                                                                                                                                                                                                       
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                            
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <hr style="margin-left:15px;margin-right:15px" />
                                                <table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tbody>
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <td height="10px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:10px;font-weight:400;hyphens:auto;line-height:10px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                    <tbody>
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <th class="small-12 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:8px;text-align:left;width:334px">
                                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                                            <h3 style="Margin:0;Margin-bottom:10px;color:inherit;font-family:'Open Sans',Arial,sans-serif;font-size:15px;font-weight:700;line-height:1.3;margin:0;margin-bottom:0;padding:0;text-align:left;word-wrap:normal">Booking Details</h3>
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                            <th class="small-12 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:8px;padding-right:16px;text-align:left;width:334px">
                                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                                            <table class="float-right text-right" align="right" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:right;vertical-align:top;width:100%">
                                                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                                                    <td class="float-right text-right" valign="middle" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:0;text-align:right;vertical-align:middle;word-wrap:break-word">
                                                                                        <a href="#" id="sendEmailVoucher" class="float-right text-right" style="Margin:0;color:#2199e8;font-family:'Open Sans',Arial,sans-serif;font-weight:400;height:20px;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none"><img src="https://travtrolley.com/images/email_icon.png" alt="Email" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:inline;height:12px;margin-right:5px;max-width:100%;outline:0;text-decoration:none;vertical-align:middle;width:15px" />Email</a>
                                                                                        <a id="printPage" href="#" class="float-right text-right" style="Margin:0;color:#2199e8;font-family:'Open Sans',Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:none" ><img src="https://travtrolley.com/images/print_icon.jpg" alt="Print" style="-ms-interpolation-mode:bicubic;border:none;clear:both;display:inline;height:20px;max-width:100%;outline:0;text-decoration:none;vertical-align:middle;width:20px" class="" /><span style="padding-top:4px">Print</span></a>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                    <tbody>
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <th class="small-12 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:8px;text-align:left;width:334px">
                                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                                            <p style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;text-align:left">Ref NO. | <strong id="ReferenceID2"></strong></p>
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                            <th class="small-12 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:8px;padding-right:16px;text-align:left;width:334px">
                                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                                            <p class="text-right" style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;text-align:right">Agent Name | <strong id="AgentName"></strong></p>
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                    <tbody>
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <th class="small-12 large-6 columns first" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:8px;text-align:left;width:334px">
                                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left"></th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                            <th class="small-12 large-6 columns last" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:8px;padding-right:16px;text-align:left;width:334px">
                                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                                            <p class="text-right" style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;text-align:right">PHONE | <strong id="AgentMobile"></strong></p>
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                    <tbody>
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:684px">
                                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                                            <table class="b2b-baggage-table" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                                                    <th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top">Car Model</th>
                                                                                    <th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top">Number Of Passengers</th>
                                                                                    <th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top">Passenger Name</th>
                                                                                    <th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top" class="LuggageCell">Luggage</th>
                                                                                    <th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top" class="childCell">Child Seats</th>
                                                                                    <th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top" class="animalCell">Animals</th>
                                                                                    <th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top" class="sportsCell">Sports Luggage</th>
                                                                                    <th style="Margin:0;background:linear-gradient(to bottom,#fafafa 0,#e8e8e8 100%);border:1px solid #ddd;border-color:#dbdbdb;color:#333;filter:progid:DXImageTransform.Microsoft.gradient( startColorstr='#f9f9f9', endColorstr='#e7e7e7', GradientType=0 );font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:700;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top">Ref. No.</th>
                                                                                </tr>
                                                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word" id="vehicleName"></td>
                                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word" id="PaxCount"></td>
                                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word" id="PaxName"></td>
                                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word" id="LuggageCount" class="LuggageCell"></td>
                                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word" id="ChiledSeat" class="childCell"></td>
                                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word" id="AnimalSeat" class="animalCell"></td>
                                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word" id="SportsLuggage" class="sportsCell"></td>
                                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f9f7f7;border:1px solid #ddd;border-collapse:collapse!important;border-color:#dbdbdb!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.42857143;margin:0;padding:8px;text-align:left;vertical-align:top;word-wrap:break-word" id="ReferenceID"></td>
                                                                                </tr>
                                                                            </table>
                                                                        </th>
                                                                        <th class="expander" style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                    <tbody>
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:684px">
                                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left">
                                                                            <table class="flight-list-table" style="border:1px solid #f3f3f3;border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                                <tr class="first-row" style="padding:0;text-align:left;vertical-align:top">
                                                                                    <td colspan="2" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f3f3f3;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:5px 4px;text-align:left;vertical-align:top;word-wrap:break-word">
                                                                                        <p class="float-left" style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;text-align:left"><strong id="VehicleClass"> </strong></p>
                                                                                    </td>
                                                                                    <td colspan="2" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;background-color:#f3f3f3;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:5px 4px;text-align:left;vertical-align:middle;word-wrap:break-word">
                                                                                        <p class="text-right" style="Margin:0;Margin-bottom:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;text-align:right"><strong><span class="transferDate">Friday , 25 Oct 2019</span></strong></p>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr style="padding:0;text-align:left;vertical-align:top">
                                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-right:1px solid #f3f3f3;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:5px 10px;text-align:left;vertical-align:top;word-wrap:break-word">
                                                                                        FROM
                                                                                        <br /><strong class="city-code" style="font-size:18px" id="FromLocation"></strong>
                                                                                        <br /><span class="transferDate"></span>
                                                                                        <br />
                                                                                        <br />
                                                                                    </td>
                                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;border-right:1px solid #f3f3f3;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:5px 10px;text-align:left;vertical-align:top;word-wrap:break-word">
                                                                                        TO
                                                                                        <br /><strong class="city-code" style="font-size:18px" id="ToLocation"></strong>
                                                                                        <br /><span class="transferDate"></span>
                                                                                        <br />
                                                                                        <br />
                                                                                    </td>
                                                                                    <td style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:5px 10px;text-align:left;vertical-align:top;word-wrap:break-word">
                                                                                        Pickup Time: <strong id="PickupTime"></strong>
                                                                                        <br />Distance: <strong id="Distance"></strong>                                                                                                                                                                            
                                                                                        <br />Status: <strong>Confirmed</strong>                                                                                        
                                                                                        <br />Start Time: <strong id="startTime"></strong>
                                                                                        <br />End Time: <strong id="endTime"></strong>
                                                                                        <br /><span id="flightNo">Flight Number: <strong id="fltNo"></strong></span>
                                                                                        <br /><span id="departureCity">Departure City: <strong id="depCity"></strong></span>
                                                                                    </td>
                                                                                </tr>
                                                                            </table>
                                                                        </th>
                                                                        <th class="expander" style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- MSg TO Driver, Sign in -->
                                                 <table  class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                    <tbody>
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:684px">
                                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                    
                                                                     <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <td><h4>Message To Driver</h4></td>
                                                                        <td><h4>Pick-up Sign </h4></td>
                                                                    </tr>
                                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <td><label id="lblMsgToDriver" style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;" ></label></td>
                                                                        <td><label id="lblPickupSign" style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;"></label></td>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <!-- Fare Details -->
                                               
                                                <hr style="margin-left:15px;margin-right:15px" />
                                                <table class="spacer" style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                    <tbody>
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <td height="10px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:10px;font-weight:400;hyphens:auto;line-height:10px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&#xA0;</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table class="row" style="border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%">
                                                    <tbody>
                                                        <tr style="padding:0;text-align:left;vertical-align:top">
                                                            <th class="small-12 large-12 columns first last" style="Margin:0 auto;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:684px">
                                                                <table style="border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%">
                                                                    <tr style="padding:0;text-align:left;vertical-align:top">
                                                                        <th style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0 15px;text-align:left">
                                                                           
                                                                            <%--<p style="Margin:0;Margin-bottom:0;color:#7E7E7E;font-family:'Open Sans',Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;margin-bottom:0;padding:0;text-align:left" id="address"></p>--%>
                                                                            <p style="Margin:0;Margin-bottom:10px;color:inherit;font-family:'Open Sans',Arial,sans-serif;font-size:15px;font-weight:700;line-height:1.3;margin:0;margin-bottom:15px;padding:0;text-align:left;word-wrap:normal">Terms & Conditions</p>
                                                                            
                                                                            <p>
                                                                            	If you encounter any issues please call our operations hotline on any of these numbers below, available 24/7:</p>
                                                                            	<p>
Germany: <strong>+49 30 346 497 360 / +49 30 346 497 369 </strong><br>

UK: <strong>+441748220043</strong><br>

USA: <strong>+1 646 687-6714 </strong><br>

UAE : <strong>+971562138662</strong>
                                                                            </p>
<!--	
                                                                                <ul class="Rulesdtl mb-3">
                                                                                    <li class="d-flex mb-1"> 
                                                                                        <span class="icon icon-arrow_drop_right"></span>
                                                                                        <div>All rooms are guaranteed on the day of arrival. In the case of no-show, your room(s) will be released and you will subject to the terms and condition of the Cancellation/No-show policy specified at the time you made the booking as well as noted in the confirmation Email </div> 

                                                                                    </li>
                                                                                    <li class="d-flex mb-1">
                                                                                        <span class="icon icon-arrow_drop_right"></span>
                                                                                        <div>The total price for these booking fees not include mini-bar items, telephone bills, laundry service, etc. The hotel will bill you directly. </div>

                                                                                    </li>
                                                                                    <li class="d-flex mb-1">
                                                                                        <span class="icon icon-arrow_drop_right"></span>
                                                                                        <div>In case where breakfast is included with the room rate, please note that certain hotels may charge extra for children travelling with their parents. If applicable, the hotel will bill you directly. Upon arrival, if you have any questions, please verify with the hotel.</div> 

                                                                                    </li> 
                                                                                    <li class="d-flex mb-1">
                                                                                        <span class="icon icon-arrow_drop_right"></span>
                                                                                        <div>Any complaints related to the respective hotel services,with regards to location, rooms, food, cleaning or other services, the guest will have to directly deal with the hotel. Cozmo Holidays will not be responsible for uch complaints.</div> 

                                                                                    </li> 
                                                                                    <li class="d-flex mb-1">
                                                                                        <span class="icon icon-arrow_drop_right"></span>
                                                                                        <div>The General Hotel Policy: Check-in time at 1400hrs and check-out time 1200hrs Early check-in or late check-out is subject to availability at the time of check-in/check-out at the hotel and cannot be guaranteed at any given point in time</div> 

                                                                                    </li> <li class="d-flex mb-1">
                                                                                        <span class="icon icon-arrow_drop_right"></span>
                                                                                        <div>Interconnecting/ Adjoining rooms/any special requests are always subject to availability at the time of check-in, and Cozmo Holidays will not be responsible for any denial of such rooms to the Customer.</div>

                                                                                     </li> 
                                                                                     <li class="d-flex mb-1">
                                                                                         <span class="icon icon-arrow_drop_right"></span>
                                                                                         <div>Most of the hotels will be asking for credit card or cash amount to be paid upon check-in as guaranteed against any expected extras by the guest, Cozmo Holidays will not be responsible in case the guest doesn’t carry a credit card or enough cash money for the same, and the guest has to follow up directly with the hotel for the refund upon check out, Cozmo holidays is not responsible in case of any delay from central bank for credit card refunds. </div>

                                                                                     </li>

                                                                                </ul>
                                                                            
                                                                        </th>
                                                                        <th class="expander" style="Margin:0;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;visibility:hidden;width:0"></th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                </center>
            </td>
        </tr>
    </table>
    <!-- prevent Gmail on iOS font size manipulation -->
    <div style="display: none; white-space: nowrap; font: 15px courier; line-height: 0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
    <input type="hidden" id="hdnUserId" runat="server" />
    <input type="hidden" id="hdnAgentId" runat="server" />
    <input type="hidden" id="hdnBehalfLocation" runat="server" />
    <input type="hidden" id="hdnSession" runat="server" />
    <div class="modal" tabindex="-1" role="dialog" id="SendEmailModal">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Email</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <label id="fortxtAdminFee">Email Id</label>
                            <div class="">
                                <input type="email" id="emailIdTxt" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <div class="modal-footer">
                        <button type="button" class="btn btn-warning" id="ConfirmSend">Send</button>
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>

        var AgentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
        var UserId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
        var BehalfLocation = JSON.parse(document.getElementById('<%=hdnBehalfLocation.ClientID %>').value);
        var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["TransferWebApiUrl"]%>';
        var agentImage = "/" +'<%=System.Configuration.ConfigurationManager.AppSettings["AgentImage"].ToString().Replace("\\","/")%>';
    </script>
    <script src="scripts/Transfers/transferVoucherGeneration.js" type="text/javascript"></script>
</body>
</html>
