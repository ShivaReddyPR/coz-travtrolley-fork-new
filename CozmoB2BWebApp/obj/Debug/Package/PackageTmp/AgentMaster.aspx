<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="AgentMasterUI" Title="Agent Master" Codebehind="AgentMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
    <%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
    <asp:HiddenField runat="server" ID="hdfAgentImg" Value="0" />
<asp:HiddenField runat="server" ID="hdfCount" Value="0" />
<asp:HiddenField runat="server" ID="hdfImgPath" Value="0" />
 <asp:HiddenField runat="server" ID="hdfRegId" Value="0" />
<%--<div class="grdScrlTrans" style="margin-top:-2px;height:520px;border:solid0px;text-align:center" >--%>





<div class="body_container"> 



  <div class=" paramcon" title="header">       
         
         
               
             
                    <div class="col-md-12 padding-0">          
                                                
    <div class="col-md-2"> <asp:Label ID="lblParentAgent" runat="server" Text="Parent Agent:"></asp:Label></div>
    
    
    <div class="col-md-2"> <asp:DropDownList CssClass="form-control" ID="ddlParentAgent" runat="server" OnSelectedIndexChanged="ddlParentAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></div>
    
    
    <div class="col-md-2"> <asp:Label ID="lblAgentType" runat="server" Text="Agent Type:"></asp:Label></div>
    
    
     <div class="col-md-2"><asp:DropDownList ID="ddlAgentType" CssClass="inputDdlEnabled form-control" Enabled="true" runat="server" OnSelectedIndexChanged="ddlAgentType_SelectedIndexChanged" AutoPostBack="true">
             <asp:ListItem Text="BASEAGENT" Value="1"></asp:ListItem>
            <asp:ListItem Text="AGENT" Value="2"></asp:ListItem>
            <asp:ListItem Text="B2B" Value="3"></asp:ListItem>
            <asp:ListItem Text="B2B2B" Value="4"></asp:ListItem> 
           <%-- <asp:ListItem Text="BASEAGENT" Value="BASEAGENT"></asp:ListItem>
            <asp:ListItem Text="AGENT" Value="AGENT"></asp:ListItem>
            <asp:ListItem Text="B2B" Value="B2B"></asp:ListItem>
            <asp:ListItem Text="B2B2B" Value="B2B2B"></asp:ListItem> --%>
            </asp:DropDownList> </div>
            
            
    <div class="col-md-2"><asp:Label ID="lblCode" Text="Code:" runat="server" ></asp:Label> </div>
    
     <div class="col-md-2">
     
     <label class="pull-left"> 
     
     <asp:TextBox Width="100px"  ID="txtCode" MaxLength="15"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox></label>
     
  <label class="pull-left">  <asp:CheckBox  Text="Block" ID= "chkBlock" runat="server" /></label>
       <div class="clearfix"></div>
      
      </div>
      
      


    <div class="clearfix"></div>
    </div>
    
    
    
    
               <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"><asp:Label ID="lblName" Text="Name:" runat="server"></asp:Label> </div>
    
    
    <div class="col-md-2"> <asp:TextBox ID="txtName"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox></div>
    
    
    <div class="col-md-2"> <asp:Label ID="lblCity" Text="City:" runat="server"></asp:Label></div>
    
     <div class="col-md-2"><asp:TextBox ID="txtCity"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox> </div>
     
     
    <div class="col-md-2"><asp:Label ID="lblState" Text="State:" runat="server" CssClass="label" ></asp:Label> </div>
     <div class="col-md-2"><asp:TextBox ID="txtState"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
    
               <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblCountry" Text="Country:" runat="server"></asp:Label></div>
    
    <div class="col-md-2"> <asp:DropDownList ID="ddlCountry"  runat="server" Enabled="true" CssClass="inputDdlEnabled form-control"></asp:DropDownList></div>
    <div class="col-md-2"> <asp:Label ID="lblPOBox" Text="P.O.Box:" runat="server" ></asp:Label></div>
    
    
     <div class="col-md-2"><asp:TextBox ID="txtPOBox"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox> </div>
     
     
    <div class="col-md-2"><asp:Label ID="lblPhoneNo" Text="Phone No:" runat="server"></asp:Label> </div>
    
    
     <div class="col-md-2"><asp:TextBox ID="txtPhone1"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
               <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"><asp:Label ID="lblAltPhoneNo" Text="Alternate No:" runat="server"></asp:Label> </div>
    
    
    <div class="col-md-2"><asp:TextBox ID="txtPhone2"  runat="server" Enabled="true" CssClass="inputEnabled form-control" ></asp:TextBox> </div>
    
    
    <div class="col-md-2"> <asp:Label ID="lblfax" Text="Fax:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"><asp:TextBox ID="txtFax"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox> </div>
     
     
    <div class="col-md-2"> <asp:Label ID="lblTelex" Text="Telex:" runat="server"></asp:Label></div>
    
     <div class="col-md-2"><asp:TextBox ID="txtTelex"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
    
               <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblEmail1" Text="Email:"  runat="server"></asp:Label></div>
    
    
    <div class="col-md-2"><asp:TextBox ID="txtEmail1"  runat="server"  style="text-transform:lowercase" Enabled="true" CssClass="inputEnabled form-control" Width="140px" ></asp:TextBox> </div>
    
    <div class="col-md-2"><asp:Label ID="lblEmail2" Text="Alternate Email:" runat="server"></asp:Label> </div>
    
     <div class="col-md-2"> <asp:TextBox ID="txtEmail2"  runat="server" style="text-transform:lowercase" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox></div>
     
     
    <div class="col-md-2"> <asp:Label ID="lblAddress" Text="Address:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"><asp:TextBox ID="txtAddress"  runat="server" TextMode="MultiLine" Height="45px" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
               <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"><asp:Label ID="lblLicenseNo" Text="License No:" runat="server"></asp:Label> </div>
    
    <div class="col-md-2"><asp:TextBox ID="txtLicenseNo"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox> </div>
    
    <div class="col-md-2"> <asp:Label ID="lblLicExpDate" Text="Exp. Date:" runat="server"></asp:Label></div>
    
     <div class="col-md-2"><uc1:DateControl ID="dcLicExpDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true"  DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl> </div>
     
    <div class="col-md-2"> <asp:Label ID="lblPassportNo" Text="Passport No:" runat="server"></asp:Label></div>
    
     <div class="col-md-2"> <asp:TextBox ID="txtPassportNo"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox></div>


    <div class="clearfix"></div>
    </div>

            <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblpptIssueDate" Text="Passport Issued On:" runat="server"></asp:Label></div>
    
    <div class="col-md-2"><uc1:DateControl ID="dcpptIssuedDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true"  DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl> </div>
    
    
    <div class="col-md-2"> <asp:Label ID="lblpptExpDate" Text="Passport Exp.On:" runat="server"></asp:Label></div>
    
     <div class="col-md-2"><uc1:DateControl ID="dcpptExpOn" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true"  DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl> </div>
     
     
    <div class="col-md-2"> <asp:Label runat="server" id="lblNationality" Text="Nationality:"></asp:Label></div>
    
     <div class="col-md-2"><asp:DropDownList Id="ddlNationality"  CssClass="inputDdlEnabled form-control"  runat="server" ></asp:DropDownList> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
    
    
               <div class="col-md-12 padding-0 marbot_10">                                      
   
                
                
    <div class="col-md-2"> <asp:Label ID="lblWebsite" Text="Website:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"><asp:TextBox ID="txtWebsite"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox> </div>

  <div class="col-md-2"><asp:Label ID="lblCurrentBalance" Text="Current Balance:" runat="server" CssClass="label" ></asp:Label> </div>
    
     <div class="col-md-2"><asp:TextBox ID="txtCurrentBalance"  runat="server" Enabled="false"  CssClass="inputDisabled form-control"></asp:TextBox> </div>
    <div class="col-md-2"> <asp:Label ID="lblCurrency" Text="Currency:" runat="server"></asp:Label></div>
    
     <div class="col-md-2"><asp:DropDownList ID="ddlCurrency"  runat="server" Enabled="true" CssClass="inputDdlEnabled form-control"></asp:DropDownList> </div>

    <div class="clearfix"></div>
    </div>
               <div class="col-md-12 padding-0 marbot_10">
          <div class="col-md-2">
              <asp:Label ID="lblGrntrStatus" Text="Grntor Status:" runat="server" CssClass="label"
                  Visible="false"></asp:Label>
          </div>
          <div class="col-md-2">
              <asp:DropDownList ID="ddlGrntrStatus" CssClass="inputDdlEnabled form-control" runat="server"
                  Visible="false">
                  <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                  <asp:ListItem Text="No" Value="N"></asp:ListItem>
              </asp:DropDownList>
          </div>
          <div class="col-md-2">
              <asp:Label runat="server" ID="lblVisaSubStatus" Text="Visa Submission:" Visible="false"></asp:Label>
          </div>
          <div class="col-md-2">
              <asp:DropDownList ID="ddlVisaSubmission" CssClass="inputDdlEnabled form-control"
                  runat="server" Visible="false">
                  <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
                  <asp:ListItem Text="No" Value="N"></asp:ListItem>
              </asp:DropDownList>
          </div>
          <div class="col-md-2">
              <asp:Label ID="lblWarnLevel" Text="Alert Level:" runat="server" Visible="false"></asp:Label>
          </div>
          <div class="col-md-2">
              <asp:TextBox ID="txtWarLevel" runat="server" Enabled="true" onkeypress="return restrictNumeric(this.id,'2');"
                  CssClass="inputEnabled" Text="10" Width="50px" Visible="false"></asp:TextBox></div>
          <div class="clearfix">
          </div>
      </div>
      <div class="col-md-12 padding-0 marbot_10">
          <div class="col-md-2">
              <asp:Label ID="lblAgentDecimal" Text="Agent Decimal:" runat="server"></asp:Label>
          </div>
          <div class="col-md-2">
              <asp:TextBox ID="txtAgentDecimal" runat="server" CssClass="inputEnabled form-control"
                  onkeypress="return restrictNumeric(this.id,'2');" MaxLength="1"></asp:TextBox>
          </div>
          <div class="col-md-2">
              <asp:Label ID="Label1" Text="Remarks:" runat="server"></asp:Label>
          </div>
          <div class="col-md-6">
              <asp:TextBox ID="txtRemarks" runat="server" Height="50" TextMode="MultiLine" Enabled="true"
                  CssClass="inputEnabled"></asp:TextBox></div>
          <div class="clearfix">
          </div>
      </div>
     <div class="col-md-12 padding-0 marbot_10">
          <div class="col-md-2">
              <asp:Label ID="lblAgentRouting" Text="Enable Routing:" runat="server"></asp:Label>
          </div>
          <div class="col-md-2">
              <asp:CheckBox ID="chkAgentRouting" runat="server" />
          </div>
          <div class="col-md-2">
              <asp:Label ID="lblAgentReturnFare" Text="Return Fare:" runat="server"></asp:Label>
          </div>
          <div class="col-md-2">
              <asp:CheckBox ID="chkAgentReturnFare" runat="server" />
          </div>
    </div>
    
    
    
     <div class="col-md-12 padding-0 marbot_10">   
    <div class="col-md-2">Agent image:</div>
    <div class="col-md-1"> <CT:DocumentManager ID="agentImage" runat="server" DefaultImageUrl="~/images/common/no_preview.png"
                    DocumentImageUrl="~/images/common/Preview.png"   ShowActualImage="false"  SessionName="crenterlic"
                    DefaultToolTip=""  /></div>
                   <div class="col-md-1"><asp:LinkButton ID="lnkView" runat="server" Text="View&nbsp;Image" Visible="true" OnClientClick="return View();"></asp:LinkButton></div>
         <div class="col-md-2">
            <asp:Image ID="imgPreview" Style="display: none;" runat="server" Height="70px" Width="80px" /> 
             <asp:Label ID="lblUploadMsg" runat="server"></asp:Label>
             <asp:Label ID="lblUpload1" runat="server" Visible="false"></asp:Label>
         </div>
           <div class="col-md-2"></div>
            <div class="col-md-2"> <asp:Label runat="server" id="lblTheme" Text="Theme:"></asp:Label></div>
    
    <div class="col-md-2"> <asp:DropDownList Id="ddlTheme"  CssClass="inputDdlEnabled form-control" runat="server" ></asp:DropDownList></div>
     <div class="clearfix"></div>
    </div>
    
    
      <div class="col-md-12 padding-0 marbot_10">
      <div class="col-md-2">
              <asp:Label runat="server" ID="lblPaymentMode" Text="Payment Mode:"></asp:Label>
          </div>
          <div class="col-md-2">
              <asp:DropDownList ID="ddlPaymentMode" CssClass="inputDdlEnabled form-control" runat="server" onchange="ShowHideCreditDetails()">
                  <asp:ListItem Text="Credit" Selected="True" Value="2"></asp:ListItem>
                  <asp:ListItem Text="Card" Value="3"></asp:ListItem>
                  <asp:ListItem Text="CreditLimit" Value="9"></asp:ListItem>
              </asp:DropDownList>
          </div>
          <div class="col-md-2">
              <asp:Label runat="server" ID="lblMatchingStatus" Text="Matching Status:" Visible="false"></asp:Label>
          </div>
          <div class="col-md-2">
              <asp:DropDownList ID="ddlMatchingStatus" CssClass="inputDdlEnabled form-control"
                  runat="server" Visible="false">
                  <asp:ListItem Text="No" Value="N"></asp:ListItem>
                  <asp:ListItem Text="Yes" Value="Y"></asp:ListItem>
              </asp:DropDownList>
          </div>
          <div class="col-md-2">
          </div>
          <div class="col-md-2">
          </div>
          <div class="clearfix">
          </div>
      </div>
      <div id="divCredit" class="col-md-12 padding-0 marbot_10" style="display:none">
              <div class="col-md-2">
                  <span>Credit Limit</span>
              </div>
              <div class="col-md-2">
                  <asp:TextBox ID="txtCreditLimit" CssClass="form-control" Text="0" runat="server"></asp:TextBox>
              </div>
              <div class="col-md-2">
                  <span>Credit Days</span>
              </div>
              <div class="col-md-2">
                  <asp:TextBox ID="txtCreditDays" CssClass="form-control" Text="0" runat="server"></asp:TextBox>
              </div>
              <div class="col-md-2">
                  <span>Temparory Credit</span>
              </div>
              <div class="col-md-2">
                  <asp:TextBox ID="txtTempCredit" CssClass="form-control" Text="0" runat="server"></asp:TextBox>
              </div>
          <%if(CurrentObject != null && CurrentObject.ID > 0){ %>
          <a href="AgentCreditLimit.aspx?agentid=<%=CurrentObject.ID %>" target="_blank">Edit Credit Limit</a>
          <%} %>
          </div>
      <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <span>Card Type</span>
                </div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlCardType" runat="server" CssClass="form-control">
                        <asp:ListItem Text="--Select Card Type--" Value="-1"></asp:ListItem>
                        <asp:ListItem Text="Master Card" Value="CA"></asp:ListItem>
                        <asp:ListItem Text="Visa Card" Value="VI"></asp:ListItem>
                    </asp:DropDownList>
                </div>
          
                <div class="col-md-2">
                    <span>Card No</span>
                </div>
                <div class="col-md-2">
                    <asp:TextBox ID="txtCardNo" runat="server" CssClass="form-control" placeholder="Card No" />
                </div>

                <div class="col-md-2">
                    <span>Card Expiry</span>
                </div>
                <div class="col-md-2">
                    <asp:TextBox ID="txtCardExpiry" runat="server" CssClass="form-control" placeholder="MM/YY" onKeyPress="return isNumber(event);" MaxLength="5" />
                </div>
                <div class="clearfix">
                </div>
            </div>

      <div class="col-md-12 padding-0 marbot_10">
        <div class="col-md-2">
                  <span>Sales Executive:</span>
              </div>
              <div class="col-md-2">
                   <asp:DropDownList ID="ddlSalesExec" runat="server" CssClass="form-control"></asp:DropDownList>
              </div>
             <div class="col-md-2">
                    <span>Copy Right:</span>
              </div>

            <div class="col-md-6">
                   <asp:TextBox ID="txtCopyRight" CssClass="form-control"  MaxLength="50" Text="" runat="server"></asp:TextBox>
              </div>
</div>

      <div id="divProducts" visible="false" runat="server">
    <div class="col-md-12 padding-0 marbot_10"> 
    <div class="col-md-2"><asp:LinkButton ID="lnkUpdateSourceCredentials" runat="server" Text="Update Source Credentials" Visible="false" style="color:Blue; width:200px;" OnClientClick="ChangeSourceCredentials();" ></asp:LinkButton></div>
    </div>
                   <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label runat="server" id="lblAgentProduct" Text="Agent Product:" ></asp:Label></div>
    
    
    <div class="col-md-8"> <asp:CheckBoxList CssClass="chklist2" ID="chkagentProduct" runat="server" RepeatColumns="8" RepeatDirection="Horizontal" OnSelectedIndexChanged="chkagentProduct_OnSelectedIndexChanged" AutoPostBack="true"></asp:CheckBoxList></div>
    
   

    <div class="clearfix"></div>
    </div> 
    
    
    
    
   <div class="col-md-12 padding-0 marbot_10">  
    
     <div class="col-md-2"></div>
    
    <div><table id="tblActiveSources" runat="server" style="width:300px;">
                    </table> </div>
    
    </div>
         
         </div>
         
         
         
        <div class="col-md-12 padding-0 marbot_10">        
         
        
         <div class="col-md-6">
                 <label style=" padding-right:5px" class="f_R"><asp:Button  ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();" CssClass="btn but_b"  OnClick ="btnSave_Click" ></asp:Button></label>
                 
                 
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b"   OnClick="btnClear_Click"></asp:Button></label>
                    
                    
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="btn but_b"   OnClick="btnSearch_Click"></asp:Button></label>
     
      </div>
         
         </div>
         
         
         
         
         
         
     </div>

<div class="clearfix"></div>

</div>


<table width="100%" border="0">
        <tr>
    <td align="center">
 
 
 <div> 
 
    <asp:HiddenField id="hdfDetailRowId" runat="server"></asp:HiddenField>
    <asp:HiddenField id="hdfDocTypeRowId" runat="server"></asp:HiddenField>
    <asp:HiddenField id="hdfCopyKey" runat="server" Value = "0"></asp:HiddenField>
    <asp:HiddenField ID="hdnAgentId" runat="server" Value="0" />
    <asp:HiddenField ID="hdnCount" runat="server" Value="0" />
    
 
 </div>
    
    
            </td>
        </tr>
            
    </table>    
  <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
<script type="text/javascript">
    function Save() {
        if ('<%=AgentId %>' > 1) {
            if (getElement('ddlParentAgent').selectedIndex <= 0 && getElement('ddlAgentType').value != 'AGENCY') addMessage('Please select Parent Agent from the list!', '');
        }
        if (getElement('txtCode').value == '') addMessage('Code cannot be blank!', '');
        // if(getElement('txtMapLocation').value=='' ) addMessage('Mapping Location cannot be blank!','');
        if (getElement('txtName').value == '') addMessage('Name cannot be blank!', '');
        if (getElement('ddlCountry').selectedIndex <= 0) addMessage('Please select country from the list!', '');
        if (getElement('ddlNationality').selectedIndex <= 0) addMessage('Please select Nationality from the list!', '');
        if (getElement('txtPhone1').value == '') addMessage('Phone No cannot be blank!', '');

        if (getElement('txtEmail1').value == '') addMessage('Email cannot be blank!', '');
        else if (!checkEmail(getElement('txtEmail1').value)) addMessage('Email is not valid!', '');

        if (getElement('txtEmail2').value != '' && !checkEmail(getElement('txtEmail2').value)) addMessage('Alternate Email is not valid!', '');

        if (getElement('chkBlock').checked) {
            if (getElement('txtRemarks').value == '') addMessage('Remarks cannot be blank !', '');
        }
        if (getElement('txtAgentDecimal').value == '') addMessage('Agent Decimal cannot be blank!', '');
        if ($('#<%=ddlPaymentMode.ClientID%>').val() == "9") {
            if (getElement('txtCreditLimit').value == '0') addMessage('Credit Limit cannot be ZERO');
            if (getElement('txtCreditDays').value == '0') addMessage('Credit Days cannot be ZERO');
            if (getElement('txtTempCredit').value == '0') addMessage('Temperory Credit Limit cannot be ZERO');
            
        } 
        if (getMessage() != '') {
            //alert(getMessage()); 
            alert(getMessage()); clearMessage(); return false;
        }
    }

    function View() {
        var count = getElement('hdfCount').value;
        if (count == 1) {

            getElement('hdfCount').value = 0;
            document.getElementById('<%=imgPreview.ClientID %>').style.display = "none"; 
            getElement('lnkView').innerHTML = "View Image";
            return false;
        }
        else {
            document.getElementById('<%=imgPreview.ClientID %>').style.display = "block";
            document.getElementById('<%=imgPreview.ClientID %>').src = getElement('hdfImgPath').value;
            getElement('lnkView').innerHTML = "Hide";
            getElement('hdfCount').value = 1;
            return false;
        }
    }
    function ChangeSourceCredentials() {
        var AgentId = document.getElementById('<%=hdnAgentId.ClientID %>').value;
         window.open("AgentSourceCredentials.aspx?&agencyId=" + AgentId + "&fromAgent=true", 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
    }
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        var len = $('#ctl00_cphTransaction_txtCardExpiry').val().length;
        var expiry = $('#ctl00_cphTransaction_txtCardExpiry').val();
        if (len < 1) {
            if (charCode != 48 && charCode != 49) {
                return false;
            }
        }
        else if (len == 1 && charcode == 48) {
            return false;
        }
        else {
            if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                return false;
            }
        }
        if (len == 2) {
            $('#ctl00_cphTransaction_txtCardExpiry').val(expiry + '/');
        }
        return true;
    }
    function ShowHideCreditDetails() {
        if ($('#<%=ddlPaymentMode.ClientID%>').val() == "9") {
            $('#divCredit').show();
        }
        else {
            $('#divCredit').hide();
        }
    }
    $(document).ready(function () {
        ShowHideCreditDetails();
    });
</script>
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
    <asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="AGENT_ID" 
    EmptyDataText="No Agency List!" AutoGenerateColumns="false" PageSize="17" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging" >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
    
     <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc1:Filter   ID="HTtxtCode" Width="120px" HeaderText="Code" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblCode" runat="server" Text='<%# Eval("agent_code") %>' CssClass="label grdof" ToolTip='<%# Eval("agent_code") %>' Width="120px"></asp:Label>
    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>--%>
    </ItemTemplate>    
    
    </asp:TemplateField>      
      
      <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtName"  Width="100px" CssClass="inputEnabled" HeaderText="Name" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblName" runat="server" Text='<%# Eval("agent_name") %>' CssClass="label grdof"  ToolTip='<%# Eval("agent_name") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtAddress"  Width="150px" CssClass="inputEnabled" HeaderText="Address" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblAddress" runat="server" Text='<%# Eval("agent_address") %>' CssClass="label grdof"  ToolTip='<%# Eval("agent_address") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>        
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtPhone"  Width="150px" CssClass="inputEnabled" HeaderText="Phone" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
   <%-- <ItemStyle HorizontalAlign="left" />--%>
    <ItemTemplate>
    <asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("agent_phone1") %>' CssClass="label grdof"  ToolTip='<%# Eval("agent_phone1") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
   
   
    </Columns>           
    </asp:GridView>

</asp:Content>





