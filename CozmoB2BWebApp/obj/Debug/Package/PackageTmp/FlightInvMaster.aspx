﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" EnableEventValidation="false" Inherits="FlightInvMaster"
    Title="Flight Inventory Master" Codebehind="FlightInvMaster.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <style type="text/css">
        #ctl00_cphTransaction_btnSubmit, #ctl00_cphTransaction_btnSearch
        {
            margin: 10px;
        }
    </style>
    <div class="body_container">

        <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

        <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

        <script type="text/javascript">



            function AddSeatsPNRDivEditMode() {

                //Get the ControlId value from hidden field
                var ctrlID = parseInt(document.getElementById('hdnControlsCount').value);
                //Increment the control id by 1
                ctrlID = ctrlID + 1;
                //Assign that to hidden field.
                document.getElementById('hdnControlsCount').value = ctrlID;

                //Get the corresponding html by passing an ajax request.
                var paramList = 'requestSource=getSeatsPNRHtmlEditMode' + '&id=' + ctrlID;
                var url = "FlightInvAjax";
                Ajax.onreadystatechange = GetSeatsPNRHtml;
                Ajax.open('POST', url, false);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);

                //Send an ajax request and bind all the airlines list to the airlines drop down
                LoadAirlinesList('ddlAirLine' + ctrlID);

                //Send an ajax request and bind all the cabin types to the cabin type drop down
                LoadCabinTypesList('ddlCabinClass' + ctrlID);
            }

            //This function will bind the values for the selected record.
            function bindParams() {
                init1();
                init2();
                init3();
                init4();
                if (document.getElementById('<%=roundtrip.ClientID %>').checked == true) {
                    document.getElementById('divcol').style.display = 'block';
                    document.getElementById('<%=oneway.ClientID %>').disabled = true;
                    document.getElementById('<%=roundtrip.ClientID %>').disabled = false;
                }
                else {
                    document.getElementById('divcol').style.display = 'none';
                    document.getElementById('<%=roundtrip.ClientID %>').disabled = true;
                    document.getElementById('<%=oneway.ClientID %>').disabled = false;
                }

                document.getElementById('divNoOfSeatsUsed').style.display = "block";




                //console.log("==========Executing bindParams function========= ");

                //Load airlines
                LoadAirlinesList('ddlAirLine');
                //Load CabinClass
                LoadCabinTypesList('ddlCabinClass');
                //console.log("==========Successfully Loaded airlines and cabinTypes ========= ");



                //Now bind airline,cabintype,isLcc and BookingClass from the hidden field.

                if (document.getElementById('<%=hdfEModeAirline.ClientID %>') != null) {


                    var airlineValue = document.getElementById('<%=hdfEModeAirline.ClientID %>').value;

                    //console.log("==========Inside first if block ========= ");
                    //console.log("=== Saved airline Value" + airlineValue);


                    //Airline
                    var selAirlineValue = airlineValue.split(',')[0];

                    //console.log("=== Saved airline Value" + selAirlineValue);

                    //Get all the options of the airline dropdown
                    var optionsAirline = document.getElementById('ddlAirLine').options;
                    var p = 0;
                    for (var i = 0; i < optionsAirline.length; i++) {

                        if (optionsAirline[i].value == selAirlineValue) {

                            optionsAirline[i].selected = true;
                            p = i;
                            break;
                        }
                    }
                    document.getElementById('ddlAirLine').options[p].selected = true;


                    //ISLCC
                    var isLCC = airlineValue.split(',')[1];
                    if (isLCC == "True") {


                        document.getElementById("chkIsLCC").checked = true;
                    }
                    else {

                        document.getElementById("chkIsLCC").checked = false;
                    }



                    //Cabin Class
                    var selCabinClassValue = airlineValue.split(',')[2];
                    //Get all the options of the airline dropdown
                    var optionsCabinClass = document.getElementById('ddlCabinClass').options;
                    var p = 0;
                    for (var i = 0; i < optionsCabinClass.length; i++) {

                        if (optionsCabinClass[i].value == selCabinClassValue) {

                            optionsCabinClass[i].selected = true;
                            p = i;
                            break;
                        }
                    }
                    document.getElementById('ddlCabinClass').options[p].selected = true;

                    //Booking Class
                    var selbookingClassValue = airlineValue.split(',')[3];
                    if (selbookingClassValue == '') {
                        document.getElementById('txtBookClass').value = "";

                    }
                    else {
                        document.getElementById('txtBookClass').value = selbookingClassValue;
                    }


                } //EOF: airline,cabintype,isLcc and BookingClass.



                //PNR and seats block

                if (document.getElementById('<%=hdfEModePNR.ClientID %>') != null) {

                    //console.log("============Inside PNR bLock============");

                    var selPNRs = document.getElementById('<%=hdfEModePNR.ClientID %>').value.split('-');

                    //console.log("SelPNR's" + selPNRs);
                    //PNR
                    document.getElementById('txtPNR').value = selPNRs[0].split(',')[0];

                    //Seats
                    var ddSeats = document.getElementById('ddlNoOfSeats');
                    for (var i = 0; i < ddSeats.options.length; i++) {
                        if (ddSeats.options[i].text === selPNRs[0].split(',')[1]) {
                            ddSeats.selectedIndex = i;
                            break;
                        }
                    }

                    //Seats Used
                    var ddSeatsUsed = document.getElementById('ddlNoOfSeatsUsed');
                    for (var y = 0; y < ddSeatsUsed.options.length; y++) {
                        if (ddSeatsUsed.options[y].text === selPNRs[0].split(',')[2]) {
                            ddSeatsUsed.selectedIndex = y;
                            break;
                        }
                    }



                    //Additional PNR's and Seats

                    if (selPNRs.length > 1) {

                        for (var i = 1; i < selPNRs.length; i++) {
                            document.getElementById('hdnControlsCount').value = i;
                            AddSeatsPNRDivEditMode();
                            var ctrlID = i + 1;
                            //Assign PNR Text Box;
                            if (document.getElementById('txtPNR' + ctrlID) != null) {

                                document.getElementById('txtPNR' + ctrlID).value = selPNRs[i].split(',')[0];
                            }
                            //Seats selection

                            if (document.getElementById('ddlNoOfSeats' + ctrlID) != null) {



                                var optionsSeats = document.getElementById('ddlNoOfSeats' + ctrlID).options;
                                //console.log(optionsSeats.length);
                                //console.log(selPNRs[i]);
                                var q = 0;
                                for (var t = 0; t < optionsSeats.length; t++) {
                                    if (optionsSeats[t].value == selPNRs[i].split(',')[1]) {

                                        optionsSeats[t].selected = true;
                                        q = t;

                                        break;
                                    }

                                }

                                document.getElementById('ddlNoOfSeats' + ctrlID).options[q].selected = true;



                            }


                            //Seats used selection

                            if (document.getElementById('ddlNoOfSeatsUsed' + ctrlID) != null) {



                                var optionsSeats = document.getElementById('ddlNoOfSeatsUsed' + ctrlID).options;
                                //console.log(optionsSeats.length);
                                //console.log(selPNRs[i]);
                                var q = 0;
                                for (var t = 0; t < optionsSeats.length; t++) {
                                    if (optionsSeats[t].value == selPNRs[i].split(',')[2]) {

                                        optionsSeats[t].selected = true;
                                        q = t;

                                        break;
                                    }

                                }

                                document.getElementById('ddlNoOfSeatsUsed' + ctrlID).options[q].selected = true;



                            }



                        } //Eof for loop



                    } //EOf additional pnr's






                } //EOF PNR Seats




            } //End of function block



            //This function will validate the user entered input time format.
            function isValidTime(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 46 || charCode > 58)) {
                    return false;
                }
                return true;
            }


            //This function will verify whether the selected airline is ISLCC or not.
            //Depending on that isLCC checkbox will be checked.
            function verifyIsLCC(ctrlid) {


                //console.log("======Executing isLCC function==========");
                //console.log("ctrlId" + ctrlid);
                

                /* if (document.getElementById(ctrlid).value == 'BO') {-- For increasing Seat count for Bus Service
                
                        $('select[id^="ddlNoOfSeats"]').each(function () {
                            for (var i = 10; i <= 60; i++) {
                                // Set the value, creating a new option if necessary
                                if ($(this).find("option[value='" + i + "']").length) {
                                    $(this).val(i).trigger('change');
                                } else {
                                    // Create a DOM Option and pre-select by default
                                    var newOption = new Option(i, i, true, true);
                                    // Append it to the select
                                    $(this).append(newOption).trigger('change');
                                }
                            }
                            $(this).val("1").trigger('change');
                        });
                    }
                    else {
                        $('select[id^="ddlNoOfSeats"]').each(function () {
                            $(this).empty();
                            for (var i = 1; i <= 9; i++) {
                                // Set the value, creating a new option if necessary
                                if ($(this).find("option[value='" + i + "']").length) {
                                    $(this).val(i).trigger('change');
                                } else {
                                    // Create a DOM Option and pre-select by default
                                    var newOption = new Option(i, i, true, true);
                                    // Append it to the select
                                    $(this).append(newOption).trigger('change');
                                }
                            }
                            $(this).val("1").trigger('change');
                        });
                    }*/

                if (document.getElementById(ctrlid) != null) {

                    var e = document.getElementById(ctrlid);
                    var isLCC = e.options[e.selectedIndex].getAttribute('islcc');                            //console.log("isLCC" + isLCC); 
                    if (isLCC == "True") {
                        //console.log("======Inside if block==========");
                        ctrlid = ctrlid.replace("ddlAirLine", "chkIsLCC");
                        document.getElementById(ctrlid).checked = true;
                    }
                    else {
                        //console.log("======Inside else block==========");
                        ctrlid = ctrlid.replace("ddlAirLine", "chkIsLCC");
                        document.getElementById(ctrlid).checked = false;
                    }

                   
                }
            }

           


            //Clears the previously entered user input after saving.
            function Clear() {

                //Send an ajax request and bind all the airlines
                LoadAirlinesList('ddlAirLine');

                //Send an ajax request and bind all the cabin types
                LoadCabinTypesList('ddlCabinClass');

                document.getElementById('<%=hdnAirlineCabinFareBooking.ClientID %>').value = "";
                document.getElementById('<%=hdnPNRSeats.ClientID %>').value = "";
                document.getElementById('hdnControlsCount').value = "0";
                document.getElementById('<%=hdnOutExCity.ClientID %>').value = "";
                document.getElementById('<%=hdnOutToCity.ClientID %>').value = "";
                document.getElementById('<%=hdnInExCity.ClientID %>').value = "";
                document.getElementById('<%=hdnInToCity.ClientID %>').value = "";
                document.getElementById('<%=roundtrip.ClientID %>').checked = false;
                document.getElementById('<%=oneway.ClientID %>').checked = true;
                document.getElementById('divcol').style.display = 'none';
                document.getElementById('<%=hdfEMId.ClientID %>').value = "0";
                document.getElementById('<%=hdfMode.ClientID %>').value = "0";
                document.getElementById('<%=hdfEModePNR.ClientID %>').value = "";
                document.getElementById('<%=hdfEModeAirline.ClientID %>').value = "";
                document.getElementById('<%=roundtrip.ClientID %>').disabled = false;
                document.getElementById('<%=oneway.ClientID %>').disabled = false;
                document.getElementById('divNoOfSeatsUsed').style.display = "none";


            }

            function savePNRSeats() {

                //Create a hidden field
                //Assign multiple values separted by comma and each row value separted by -
                //Values -- (,)
                //Each row -- (-)
                //Eg: SDET,2 - PQRS 5.
                //console.log("==========Executing savePNRSeats Function========");
                var assignedVal = "";
                //console.log("1.assignedVal =" + assignedVal);
                var ctrlID = parseInt(document.getElementById('hdnControlsCount').value);

                //PNR
                if (document.getElementById('txtPNR') != null) {

                    if (document.getElementById('txtPNR').value.length > 0) {
                        assignedVal += document.getElementById('txtPNR').value;
                    }

                } //EOF PNR
                //console.log("1.PNR =" + assignedVal);

                //No.Of Seats
                if (document.getElementById('ddlNoOfSeats') != null) {

                    var e = document.getElementById("ddlNoOfSeats");
                    var ddlSeatsSelValue = e.options[e.selectedIndex].value;
                    assignedVal += "," + ddlSeatsSelValue;
                } //EOF : No.Of Seats

                //No.of seats used
                if (document.getElementById('ddlNoOfSeatsUsed') != null) {

                    var e = document.getElementById("ddlNoOfSeatsUsed");
                    var ddlSeatsSelValue = e.options[e.selectedIndex].value;
                    assignedVal += "," + ddlSeatsSelValue;
                } //EOF : No.of seats used



                //console.log("1.No.Of Seats =" + assignedVal);
                //console.log("Controls Count:" + ctrlID);

                if (ctrlID > 0) {
                    //console.log("======= Iterating through dynamic controls==========");
                    //console.log("=====Appening new additional row=====================");

                    for (var i = 1; i <= ctrlID; i++) {
                        //console.log("===Start : New Record Id" + i);
                        //PNR
                        if (document.getElementById('txtPNR' + i) != null) {
                            assignedVal += "-" + document.getElementById('txtPNR' + i).value;
                        } //EOF :PNR

                        //console.log("1.PNR =" + assignedVal);


                        //No.Of Seats
                        if (document.getElementById('ddlNoOfSeats' + i) != null) {

                            var e = document.getElementById("ddlNoOfSeats" + i);
                            var ddlSeatsSelValue = e.options[e.selectedIndex].value;
                            assignedVal += "," + ddlSeatsSelValue;
                        } //EOF : No.Of Seats

                        //No.Of seats used

                        if (document.getElementById('ddlNoOfSeatsUsed' + i) != null) {

                            var e = document.getElementById("ddlNoOfSeatsUsed" + i);
                            var ddlSeatsSelValue = e.options[e.selectedIndex].value;
                            assignedVal += "," + ddlSeatsSelValue;
                        } //EOF : No.Of Seats Used




                    }  //Eof : for loop


                } //Eof : ctrlID if block
                //console.log("========Finished Exceuting savePNRSeats Function");
                document.getElementById('<%=hdnPNRSeats.ClientID %>').value = assignedVal;
                console.log(document.getElementById('<%=hdnPNRSeats.ClientID %>').value);

            } //EOF : function


            function saveAirlineCabinFareBooking() {
                //console.log("==========Executing saveAirlineCabinFareBooking Function========");
                //Create a hidden field
                //Assign multiple values separted by comma and each row value separted by -
                //Values -- (,)
                //Each row -- (-)
                //Eg: Bh,Yes,All,Fb,A - Bh,Yes,All,Fb,A.
                var assignedVal = "";
                //console.log("1.assignedVal =" + assignedVal);

                var ctrlID = parseInt(document.getElementById('hdnControlsCount').value);

                //Airline
                if (document.getElementById('ddlAirLine') != null) {

                    var e = document.getElementById("ddlAirLine");
                    var ddlAirlineSelValue = e.options[e.selectedIndex].value;
                    assignedVal += ddlAirlineSelValue;
                }
                //console.log("1.Airline =" + assignedVal);

                //IsLCC
                if (document.getElementById('chkIsLCC') != null) {

                    if (document.getElementById('chkIsLCC').checked == true) {
                        assignedVal += "," + "Yes";
                    }
                    else {
                        assignedVal += "," + "No";
                    }
                }
                //console.log("1.IsLCC =" + assignedVal);

                //CabinClass
                if (document.getElementById('ddlCabinClass') != null) {
                    var e = document.getElementById("ddlCabinClass");
                    var ddlCabinClassSelValue = e.options[e.selectedIndex].value;
                    assignedVal += "," + ddlCabinClassSelValue;
                }
                //console.log("1.CabinClass =" + assignedVal);

                //FareBasis

                /* if (document.getElementById('txtFareBasis') != null) {

                    if (document.getElementById('txtFareBasis').value.length > 0) {
                assignedVal += "," + document.getElementById('txtFareBasis').value;
                }
                else {
                assignedVal += "," + ",";
                }
                } */

                //console.log("1.FareBasis =" + assignedVal);

                //Booking Class
                if (document.getElementById('txtBookClass') != null) {

                    if (document.getElementById('txtBookClass').value.length > 0) {
                        assignedVal += "," + document.getElementById('txtBookClass').value;
                    }
                    else {
                        assignedVal += ",";
                    }
                }
                //console.log("1.BookingClass =" + assignedVal);
                //console.log("Controls Count:" + ctrlID);


                if (ctrlID > 0) {

                    //console.log("======= Iterating through dynamic controls==========");
                    //console.log("=====Appening new additional row=====================");

                    //iterate through
                    for (var i = 1; i <= ctrlID; i++) {
                        //console.log("===Start : New Record Id" + i);


                        //Airline
                        if (document.getElementById('ddlAirLine' + i) != null) {
                            var e = document.getElementById("ddlAirLine" + i);
                            var optionSelIndex = e.options[e.selectedIndex].value;
                            assignedVal += "-" + optionSelIndex;
                        } //EOF :Airline

                        //console.log("1.Airline =" + assignedVal);

                        //IsLCC
                        if (document.getElementById('chkIsLCC' + i) != null) {

                            if (document.getElementById('chkIsLCC' + i).checked == true) {
                                assignedVal += "," + "Yes";
                            }
                            else {
                                assignedVal += "," + "No";
                            }
                        } //EOF :IsLCC

                        //console.log("1.IsLCC =" + assignedVal);


                        //CabinClass
                        if (document.getElementById('ddlCabinClass' + i) != null) {
                            var e = document.getElementById("ddlCabinClass" + i);
                            var ddlCabinClassSelValue = e.options[e.selectedIndex].value;
                            assignedVal += "," + ddlCabinClassSelValue;
                        } //EOF :CabinClass

                        //console.log("1.CabinClass =" + assignedVal);


                        //FareBasis
                        /*if (document.getElementById('txtFareBasis' + i) != null) {

                            if (document.getElementById('txtFareBasis' + i).value.length > 0) {
                        assignedVal += "," + document.getElementById('txtFareBasis' + i).value;
                        }
                        else {
                        assignedVal += "," + ",";
                        }
                        } */
                        //EOF :FareBasis

                        //console.log("1.FareBasis =" + assignedVal);


                        //Booking Class
                        if (document.getElementById('txtBookClass' + i) != null) {

                            if (document.getElementById('txtBookClass' + i).value.length > 0) {
                                assignedVal += "," + document.getElementById('txtBookClass' + i).value;
                            }
                            else {
                                assignedVal += ",";
                            }
                        } //EOF :Booking Class

                        console.log("assignedVal ====" + assignedVal);



                    } //EOF : For Loop


                } //EOF : CTRlID If block


                //console.log("========Finished Exceuting saveAirlineCabinFareBooking Function");
                document.getElementById('<%=hdnAirlineCabinFareBooking.ClientID %>').value = assignedVal;

            } //EOF : function






            var Ajax; //New Ajax object.
            var cabinTypes = [];
            var airlineTypes = [];
            var cabinType;
            var airlineType;

            if (window.XMLHttpRequest) {
                Ajax = new window.XMLHttpRequest();
            }
            else {
                Ajax = new ActiveXObject("Microsoft.XMLHTTP");
            }

            //When the DOM is ready send an ajax request and load initially all the airlines and cabin types.
            $(document).ready(function() {
                //alert('dom ready');
                //Send an ajax request and bind all the airlines list to the airlines drop down
                LoadAirlinesList('ddlAirLine');

                //Send an ajax request and bind all the cabin types to the cabin type drop down
                LoadCabinTypesList('ddlCabinClass');
            })

            //This function validates the user input which accepts only +ve integers with or without decimals.
            //This function will be invoked when the user enters the Adult ,Child,Infant Fare Details.
            function isNumber(evt) {
                evt = (evt) ? evt : window.event;
                var charCode = (evt.which) ? evt.which : evt.keyCode;
                if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                    return false;
                }
                return true;
            }

            //This function will validate the user input and allows the user to enter only alpha numeric             characters.
            //This function will be invoked when the user enters the PNR's.
            var specialKeys = new Array();
            specialKeys.push(8); //Backspace
            specialKeys.push(9); //Tab
            specialKeys.push(46); //Delete
            specialKeys.push(36); //Home
            specialKeys.push(35); //End
            specialKeys.push(37); //Left
            specialKeys.push(39); //Right
            function IsAlphaNumeric(e) {
               
                var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
                var ret = ( (keyCode==32) || (keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode));

                return ret;
            }





            //This function adds the airline,farebasis,cabin class and booking class controls
            function AddFareCabinBookingDiv() {

                //Get the ControlId value from hidden field
                var ctrlID = parseInt(document.getElementById('hdnControlsCount').value);
                //Increment the control id by 1
                ctrlID = ctrlID + 1;
                //Assign that to hidden field.
                document.getElementById('hdnControlsCount').value = ctrlID;

                //Get the corresponding html by passing an ajax request.
                var paramList = 'requestSource=getFareCabinBookingHtml' + '&id=' + ctrlID;
                var url = "FlightInvAjax";
                Ajax.onreadystatechange = GetHtml;
                Ajax.open('POST', url, false);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);

                //Send an ajax request and bind all the airlines list to the airlines drop down
                LoadAirlinesList('ddlAirLine' + ctrlID);

                //Send an ajax request and bind all the cabin types to the cabin type drop down
                LoadCabinTypesList('ddlCabinClass' + ctrlID);
            }


            //Get the html corresponding to airline,farebasis,cabin class and booking class controls
            function GetHtml(response) {
                if (Ajax.readyState == 4) {
                    if (Ajax.status == 200) {
                        if (Ajax.responseText.length > 0) {

                            var fareBookingClass = document.getElementById('fareBookingClassDiv');
                            if (fareBookingClass != null) {
                                //This will add the response html to the div
                                //document.getElementById('fareBookingClassDiv').innerHTML += Ajax.responseText;
                                $("#fareBookingClassDiv:last").append(Ajax.responseText);

                            }
                        }
                    }
                }
            }

            //Function which removes the airline,farebasis,cabin class and booking class controls
            function RemoveFareCabinBookingDiv(id) {
                var ctrlID = parseInt(id);
                document.getElementById('fareBooking' + ctrlID).remove();
            }

            //This function adds the Seats and PNR controls div
            function AddSeatsPNRDiv() {

                //Get the ControlId value from hidden field
                var ctrlID = parseInt(document.getElementById('hdnControlsCount').value);
                //Increment the control id by 1
                ctrlID = ctrlID + 1;
                //Assign that to hidden field.
                document.getElementById('hdnControlsCount').value = ctrlID;

                //Get the corresponding html by passing an ajax request.
                var paramList = 'requestSource=getSeatsPNRHtml' + '&id=' + ctrlID;
                var url = "FlightInvAjax";
                Ajax.onreadystatechange = GetSeatsPNRHtml;
                Ajax.open('POST', url, false);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);

                //Send an ajax request and bind all the airlines list to the airlines drop down
                LoadAirlinesList('ddlAirLine' + ctrlID);

                //Send an ajax request and bind all the cabin types to the cabin type drop down
                LoadCabinTypesList('ddlCabinClass' + ctrlID);
            }


            //Get the html Corresponding to Seats and PNR controls div
            function GetSeatsPNRHtml(response) {
                if (Ajax.readyState == 4) {
                    if (Ajax.status == 200) {
                        if (Ajax.responseText.length > 0) {

                            var seatsPNR = document.getElementById('seatsPNRDiv');
                            if (seatsPNR != null) {
                                //This will add the response html to the div
                                // document.getElementById('seatsPNRDiv').innerHTML += Ajax.responseText;
                                $("#seatsPNRDiv:last").append(Ajax.responseText);

                            }
                        }
                    }
                }
            }

            //Function which removes Seats and PNR divs.
            function RemoveSeatsPNRDiv(id) {
                var ctrlID = parseInt(id);
                document.getElementById('seatsPNR' + ctrlID).remove();

            }

            //Load Airlines for particular Airline  dropdown.
            function LoadAirlinesList(airlineType) {
                var paramList = 'requestSource=getAllAirLinesList' + '&id=' + airlineType;
                var url = "FlightInvAjax";
                Ajax.onreadystatechange = GetAirlineTypesList;
                Ajax.open('POST', url, false);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);

            }

            //Gets the airline types
            function GetAirlineTypesList(response) {
                if (Ajax.readyState == 4) {
                    if (Ajax.status == 200) {
                        if (Ajax.responseText.length > 0) {
                            airlineType = Ajax.responseText.split('#')[0];
                            var ddl = document.getElementById(airlineType);
                            if (ddl != null) {
                                ddl.options.length = 0;
                                var el = document.createElement("option");
                                el.textContent = "Select";
                                el.value = "0";
                                ddl.add(el, 0);
                                var values = Ajax.responseText.split('#')[1].split(',');

                                for (var i = 0; i < values.length; i++) {
                                    var opt = values[i];
                                    if (opt.length > 0 && opt.indexOf('|') > 0) {
                                        var el = document.createElement("option");
                                        el.textContent = opt.split('|')[0];
                                        el.value = opt.split('|')[1];
                                        el.setAttribute("isLCC", opt.split('|')[2])
                                        ddl.appendChild(el);

                                    }
                                }

                                if (airlineTypes.length > 0 && airlineTypes[0].trim().length > 0) {
                                    var id = eval(airlineType.split('-')[1]);
                                    if (id == undefined) {
                                        id = 0;
                                    }
                                    else {
                                        id = eval(id - 1);
                                    }

                                    ddl.value = airlineTypes[id];
                                }
                            }
                        }
                    }
                }
            }

            //Load CabinTypes for particular cabin type dropdown.
            function LoadCabinTypesList(cabinType) {
                var paramList = 'requestSource=getCabinTypeList' + '&id=' + cabinType;
                var url = "FlightInvAjax";
                Ajax.onreadystatechange = GetCabinTypesList;
                Ajax.open('POST', url, false);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);
            }

            //Gets the cabin types
            function GetCabinTypesList(response) {
                if (Ajax.readyState == 4) {
                    if (Ajax.status == 200) {
                        if (Ajax.responseText.length > 0) {
                            cabinType = Ajax.responseText.split('#')[0];
                            var ddl = document.getElementById(cabinType);
                            if (ddl != null) {
                                ddl.options.length = 0;
                                var el = document.createElement("option");
                                el.textContent = "Select";
                                el.value = "0";
                                ddl.add(el, 0);
                                var values = Ajax.responseText.split('#')[1].split(',');

                                for (var i = 0; i < values.length; i++) {
                                    var opt = values[i];
                                    if (opt.length > 0 && opt.indexOf('|') > 0) {
                                        var el = document.createElement("option");
                                        el.textContent = opt.split('|')[0];
                                        el.value = opt.split('|')[1];
                                        ddl.appendChild(el);
                                    }
                                }

                                if (cabinTypes.length > 0 && cabinTypes[0].trim().length > 0) {
                                    var id = eval(cabinType.split('-')[1]);
                                    if (id == undefined) {
                                        id = 0;
                                    }
                                    else {
                                        id = eval(id - 1);
                                    }

                                    ddl.value = cabinTypes[id];
                                }
                            }
                        }
                    }
                }
            }

            function ChangeOnwardExCountry() {
                var countryCode = document.getElementById('<%=ddlInExCountry.ClientID %>').value;
                var paramList = 'ccode=' + countryCode + '&requestFrom=cityList';
                var url = "FlightInvAjax";
                var arrayCity = "";
                var faltoo = getFile(url, paramList);
                arrayCity = faltoo.split('/');
                var select = document.getElementById('<%=ddlInExCity.ClientID %>');
                select.options.length = 0; // clear out existing items
                select.options.add(new Option("Select", "Select"))
                if (arrayCity[0] != "") {
                    for (var i = 0; i < arrayCity.length; i++) {
                        var City = arrayCity[i].split(',');
                        select.options.add(new Option(City[0], City[1]))
                    }
                }
                select.options.add(new Option("Other", "Other"))
            }

            function ChangeOnwardToCountry() {
                var countryCode = document.getElementById('<%=ddlInToCountry.ClientID %>').value;
                var paramList = 'ccode=' + countryCode + '&requestFrom=cityList';
                var url = "FlightInvAjax";
                var arrayCity = "";
                var faltoo = getFile(url, paramList);
                arrayCity = faltoo.split('/');
                var select = document.getElementById('<%=ddlInToCity.ClientID %>');
                select.options.length = 0; // clear out existing items
                select.options.add(new Option("Select", "Select"))
                if (arrayCity[0] != "") {
                    for (var i = 0; i < arrayCity.length; i++) {
                        var City = arrayCity[i].split(',');
                        select.options.add(new Option(City[0], City[1]))
                    }
                }
                select.options.add(new Option("Other", "Other"))
            }
            function ChangeReturnExCountry() {
                var countryCode = document.getElementById('<%=ddlOutExCountry.ClientID %>').value;
                var paramList = 'ccode=' + countryCode + '&requestFrom=cityList';
                var url = "FlightInvAjax";
                var arrayCity = "";
                var faltoo = getFile(url, paramList);
                arrayCity = faltoo.split('/');
                var select = document.getElementById('<%=ddlOutExCity.ClientID %>');
                select.options.length = 0; // clear out existing items
                select.options.add(new Option("Select", "Select"))
                if (arrayCity[0] != "") {
                    for (var i = 0; i < arrayCity.length; i++) {
                        var City = arrayCity[i].split(',');
                        select.options.add(new Option(City[0], City[1]))
                    }
                }
                select.options.add(new Option("Other", "Other"))
            }

            function ChangeReturnToCountry() {
                var countryCode = document.getElementById('<%=ddlOutToCountry.ClientID %>').value;
                var paramList = 'ccode=' + countryCode + '&requestFrom=cityList';
                var url = "FlightInvAjax";
                var arrayCity = "";
                var faltoo = getFile(url, paramList);
                arrayCity = faltoo.split('/');
                var select = document.getElementById('<%=ddlOutToCity.ClientID %>');
                select.options.length = 0; // clear out existing items
                select.options.add(new Option("Select", "Select"))
                if (arrayCity[0] != "") {
                    for (var i = 0; i < arrayCity.length; i++) {
                        var City = arrayCity[i].split(',');
                        select.options.add(new Option(City[0], City[1]))
                    }
                }
                select.options.add(new Option("Other", "Other"))
            }


            function getFile(url, passData) {
                if (window.XMLHttpRequest) {
                    AJAX = new XMLHttpRequest();
                }
                else {
                    AJAX = new ActiveXObject("Microsoft.XMLHTTP");
                }
                if (AJAX) {
                    AJAX.open("POST", url, false);
                    AJAX.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                    AJAX.send(passData);
                    return AJAX.responseText;
                }
                else {
                    return false;
                }
            }
    
        </script>

        <script type="text/javascript">
            var call1;
            var call2;
            //-Flight Calender control
            function init1() {
                var today = new Date();
                // Rendering Cal1
                call1 = new YAHOO.widget.CalendarGroup("call1", "fcontainer1");
                call1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
                //            call1.cfg.setProperty("title", "Select your desired departure date:");
                call1.cfg.setProperty("close", true);
                call1.selectEvent.subscribe(setFlightDate1);
                call1.render();
                // Rendering Cal2
                call2 = new YAHOO.widget.CalendarGroup("call2", "fcontainer2");
                //            call2.cfg.setProperty("title", "Select your desired return date:");
                call2.selectEvent.subscribe(setFlightDate2);
                call2.cfg.setProperty("close", true);
                call2.render();
            }

            function showFlightCalendar1() {
                call2.hide();
                $('.fcontainer-calendar').hide();
                document.getElementById('fcontainer1').style.display = "block";
                document.getElementById('fcontainer2').style.display = "none";

                var date1 = document.getElementById('<%=txtInDepDate.ClientID %>').value;
                if (date1.length > 0 && date1 != "DD/MM/YYYY") {
                    var depDateArray = date1.split('/');
                    call1.cfg.setProperty("selected", depDateArray[1] + "/" + eval(depDateArray[0]) + "/" + depDateArray[2]);
                    call1.setMonth(eval(depDateArray[1]) - 1); //Set the calendar month ZERO based index Jan:0 to Dec:11
                }
            }

            function showFlightCalendar2() {
                $('.fcontainer-calendar').hide();
                call1.hide();
                // setting Calender2 min date acoording to calendar1 selected date
                var date1 = document.getElementById('<%=txtInDepDate.ClientID %>').value;
                var date2 = document.getElementById('<%=txtInArrivalDate.ClientID %>').value;
                if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                    var depDateArray = date1.split('/');
                    var retDateArray = date2.split('/');
                    var arrMinDate = new Date(depDateArray[2], depDateArray[1], depDateArray[0]);
                    if (document.getElementById('<%=txtInArrivalDate.ClientID %>').value.length <= 0) {
                        call2.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate() + 1) + "/" + depDateArray[2]);
                        call2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                    } else {
                        call2.cfg.setProperty("selected", retDateArray[1] + "/" + eval(retDateArray[0]) + "/" + retDateArray[2]);
                        call2.setMonth(eval(retDateArray[1]) - 1); //Set the calendar month Jan:0 to Dec:11
                    }
                    call2.render();
                }
                document.getElementById('fcontainer2').style.display = "block";
            }

            function setFlightDate1() {
                var date1 = call1.getSelectedDates()[0];
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();
                var todaydate = new Date(thisYear, thisMonth, thisDay);
                var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
                var difference = (depdate.getTime() - todaydate.getTime());
                if (difference < 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";

                var month = date1.getMonth() + 1;
                var day = date1.getDate();

                if (month.toString().length == 1) {
                    month = "0" + month;
                }
                if (day.toString().length == 1) {
                    day = "0" + day;
                }
                document.getElementById('<%=txtInDepDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
                call1.hide();
            }

            function setFlightDate2() {
                var date1 = document.getElementById('<%=txtInDepDate.ClientID %>').value;
                if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "First select departure date.";
                    return false;
                }
                var date2 = call2.getSelectedDates()[0];
                var depDateArray = date1.split('/');
                // checking if date1 is valid		    
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
                // Note: Date()	for javascript take months from 0 to 11
                var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
                var difference = returndate.getTime() - depdate.getTime();
                if (difference < 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Date of return should be greater than or equal to date of departure (" + date1 + ")";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
                var month = date2.getMonth() + 1;
                var day = date2.getDate();
                if (month.toString().length == 1) {
                    month = "0" + month;
                }
                if (day.toString().length == 1) {
                    day = "0" + day;
                }
                document.getElementById('<%=txtInArrivalDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
                call2.hide();
            }

            YAHOO.util.Event.addListener(window, "load", init1);
        </script>

        <script type="text/javascript">
            var call3;
            var call4;
            //-Flight Calender control
            function init2() {
                var today = new Date();
                // Rendering Cal1
                call3 = new YAHOO.widget.CalendarGroup("call3", "fcontainer3");
                call3.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
                //            call1.cfg.setProperty("title", "Select your desired departure date:");
                call3.cfg.setProperty("close", true);
                call3.selectEvent.subscribe(setFlightDate3);
                call3.render();
                // Rendering Cal2
                call4 = new YAHOO.widget.CalendarGroup("call4", "fcontainer4");
                //            call2.cfg.setProperty("title", "Select your desired return date:");
                call4.selectEvent.subscribe(setFlightDate4);
                call4.cfg.setProperty("close", true);
                call4.render();
            }

            function showFlightCalendar3() {

                $('.fcontainer-calendar').hide();
                call4.hide();
                document.getElementById('fcontainer3').style.display = "block";
                document.getElementById('fcontainer4').style.display = "none";

                var date3 = document.getElementById('<%=txtOutDepDate.ClientID %>').value;
                if (date3.length > 0 && date3 != "DD/MM/YYYY") {
                    var depDateArray = date3.split('/');
                    call3.cfg.setProperty("selected", depDateArray[1] + "/" + eval(depDateArray[0]) + "/" + depDateArray[2]);
                    call3.setMonth(eval(depDateArray[1]) - 1); //Set the calendar month ZERO based index Jan:0 to Dec:11
                }
            }

            function showFlightCalendar4() {
                $('.fcontainer-calendar').hide();
                call3.hide();
                // setting Calender2 min date acoording to calendar1 selected date
                var date3 = document.getElementById('<%=txtOutDepDate.ClientID %>').value;
                var date4 = document.getElementById('<%=txtOutArrivalDate.ClientID %>').value;
                if (date3.length != 0 && date3 != "DD/MM/YYYY") {
                    var depDateArray = date3.split('/');
                    var retDateArray = date4.split('/');
                    var arrMinDate = new Date(depDateArray[2], depDateArray[1], depDateArray[0]);
                    if (document.getElementById('<%=txtOutArrivalDate.ClientID %>').value.length <= 0) {
                        call4.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate() + 1) + "/" + depDateArray[2]);
                        call4.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                    } else {
                        call4.cfg.setProperty("selected", retDateArray[1] + "/" + eval(retDateArray[0]) + "/" + retDateArray[2]);
                        call4.setMonth(eval(retDateArray[1]) - 1); //Set the calendar month Jan:0 to Dec:11
                    }
                    call4.render();
                }
                document.getElementById('fcontainer4').style.display = "block";
            }

            function setFlightDate3() {
                var date3 = call3.getSelectedDates()[0];
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();
                var todaydate = new Date(thisYear, thisMonth, thisDay);
                var depdate = new Date(date3.getFullYear(), date3.getMonth(), date3.getDate());
                var difference = (depdate.getTime() - todaydate.getTime());
                if (difference < 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";

                var month = date3.getMonth() + 1;
                var day = date3.getDate();

                if (month.toString().length == 1) {
                    month = "0" + month;
                }
                if (day.toString().length == 1) {
                    day = "0" + day;
                }
                document.getElementById('<%=txtOutDepDate.ClientID %>').value = day + "/" + (month) + "/" + date3.getFullYear();
                call3.hide();
            }

            function setFlightDate4() {
                var date3 = document.getElementById('<%=txtOutDepDate.ClientID %>').value;
                if (date3.length == 0 || date3 == "DD/MM/YYYY") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "First select departure date.";
                    return false;
                }
                var date4 = call4.getSelectedDates()[0];
                var depDateArray = date3.split('/');
                // checking if date1 is valid		    
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
                // Note: Date()	for javascript take months from 0 to 11
                var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                var returndate = new Date(date4.getFullYear(), date4.getMonth(), date4.getDate());
                var difference = returndate.getTime() - depdate.getTime();
                if (difference < 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Date of return should be greater than or equal to date of departure (" + date3 + ")";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
                var month = date4.getMonth() + 1;
                var day = date4.getDate();
                if (month.toString().length == 1) {
                    month = "0" + month;
                }
                if (day.toString().length == 1) {
                    day = "0" + day;
                }
                document.getElementById('<%=txtOutArrivalDate.ClientID %>').value = day + "/" + month + "/" + date4.getFullYear();
                call4.hide();
            }

            YAHOO.util.Event.addListener(window, "load", init2);
        </script>

        <script type="text/javascript">
            var call5;
            var call6;
            //-Flight Calender control
            function init3() {
                var today = new Date();
                // Rendering Cal1
                call5 = new YAHOO.widget.CalendarGroup("call5", "fcontainer5");
                call5.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
                //            call1.cfg.setProperty("title", "Select your desired departure date:");
                call5.cfg.setProperty("close", true);
                call5.selectEvent.subscribe(setFlightDate5);
                call5.render();
                // Rendering Cal2
                call6 = new YAHOO.widget.CalendarGroup("call6", "fcontainer6");
                //            call2.cfg.setProperty("title", "Select your desired return date:");
                call6.selectEvent.subscribe(setFlightDate6);
                call6.cfg.setProperty("close", true);
                call6.render();
            }

            function showFlightCalendar5() {
                $('.fcontainer-calendar').hide();
                call6.hide();
                document.getElementById('fcontainer5').style.display = "block";
                document.getElementById('fcontainer6').style.display = "none";

                var date5 = document.getElementById('<%=txtBookFromDate.ClientID %>').value;
                if (date5.length > 0 && date5 != "DD/MM/YYYY") {
                    var depDateArray = date5.split('/');
                    call5.cfg.setProperty("selected", depDateArray[1] + "/" + eval(depDateArray[0]) + "/" + depDateArray[2]);
                    call5.setMonth(eval(depDateArray[1]) - 1); //Set the calendar month ZERO based index Jan:0 to Dec:11
                }
            }

            function showFlightCalendar6() {
                $('.fcontainer-calendar').hide();
                call5.hide();
                // setting Calender2 min date acoording to calendar1 selected date
                var date5 = document.getElementById('<%=txtBookFromDate.ClientID %>').value;
                var date6 = document.getElementById('<%=txtBookToDate.ClientID %>').value;
                if (date5.length != 0 && date5 != "DD/MM/YYYY") {
                    var depDateArray = date5.split('/');
                    var retDateArray = date6.split('/');
                    var arrMinDate = new Date(depDateArray[2], depDateArray[1], depDateArray[0]);
                    if (document.getElementById('<%=txtBookToDate.ClientID %>').value.length <= 0) {
                        call6.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate() + 1) + "/" + depDateArray[2]);
                        call6.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                    } else {
                        call6.cfg.setProperty("selected", retDateArray[1] + "/" + eval(retDateArray[0]) + "/" + retDateArray[2]);
                        call6.setMonth(eval(retDateArray[1]) - 1); //Set the calendar month Jan:0 to Dec:11
                    }
                    call6.render();
                }
                document.getElementById('fcontainer6').style.display = "block";
            }

            function setFlightDate5() {
                var date5 = call5.getSelectedDates()[0];
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();
                var todaydate = new Date(thisYear, thisMonth, thisDay);
                var depdate = new Date(date5.getFullYear(), date5.getMonth(), date5.getDate());
                var difference = (depdate.getTime() - todaydate.getTime());
                if (difference < 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";

                var month = date5.getMonth() + 1;
                var day = date5.getDate();

                if (month.toString().length == 1) {
                    month = "0" + month;
                }
                if (day.toString().length == 1) {
                    day = "0" + day;
                }
                document.getElementById('<%=txtBookFromDate.ClientID %>').value = day + "/" + (month) + "/" + date5.getFullYear();
                call5.hide();
            }

            function setFlightDate6() {
                var date5 = document.getElementById('<%=txtBookFromDate.ClientID %>').value;
                if (date5.length == 0 || date5 == "DD/MM/YYYY") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "First select departure date.";
                    return false;
                }
                var date6 = call6.getSelectedDates()[0];
                var depDateArray = date5.split('/');
                // checking if date1 is valid		    
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
                // Note: Date()	for javascript take months from 0 to 11
                var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                var returndate = new Date(date6.getFullYear(), date6.getMonth(), date6.getDate());
                var difference = returndate.getTime() - depdate.getTime();
                if (difference < 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Date of return should be greater than or equal to date of departure (" + date5 + ")";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
                var month = date6.getMonth() + 1;
                var day = date6.getDate();
                if (month.toString().length == 1) {
                    month = "0" + month;
                }
                if (day.toString().length == 1) {
                    day = "0" + day;
                }
                document.getElementById('<%=txtBookToDate.ClientID %>').value = day + "/" + month + "/" + date6.getFullYear();
                call6.hide();
            }

            YAHOO.util.Event.addListener(window, "load", init3);
        </script>

        <script type="text/javascript">
            var call7;
            var call8;
            //-Flight Calender control
            function init4() {
                var today = new Date();
                // Rendering Cal1
                call7 = new YAHOO.widget.CalendarGroup("call7", "fcontainer7");
                call7.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
                //            call1.cfg.setProperty("title", "Select your desired departure date:");
                call7.cfg.setProperty("close", true);
                call7.selectEvent.subscribe(setFlightDate7);
                call7.render();
                // Rendering Cal2
                call8 = new YAHOO.widget.CalendarGroup("call8", "fcontainer8");
                //            call2.cfg.setProperty("title", "Select your desired return date:");
                call8.selectEvent.subscribe(setFlightDate8);
                call8.cfg.setProperty("close", true);
                call8.render();
            }

            function showFlightCalendar7() {
                $('.fcontainer-calendar').hide();
                call8.hide();
                document.getElementById('fcontainer7').style.display = "block";
                document.getElementById('fcontainer8').style.display = "none";

                var date7 = document.getElementById('<%=txtTravelFromDate.ClientID %>').value;
                if (date7.length > 0 && date7 != "DD/MM/YYYY") {
                    var depDateArray = date3.split('/');
                    call7.cfg.setProperty("selected", depDateArray[1] + "/" + eval(depDateArray[0]) + "/" + depDateArray[2]);
                    call7.setMonth(eval(depDateArray[1]) - 1); //Set the calendar month ZERO based index Jan:0 to Dec:11
                }
            }

            function showFlightCalendar8() {
                $('.fcontainer-calendar').hide();
                call7.hide();
                // setting Calender2 min date acoording to calendar1 selected date
                var date7 = document.getElementById('<%=txtTravelFromDate.ClientID %>').value;
                var date8 = document.getElementById('<%=txtTravelToDate.ClientID %>').value;
                if (date7.length != 0 && date7 != "DD/MM/YYYY") {
                    var depDateArray = date7.split('/');
                    var retDateArray = date8.split('/');
                    var arrMinDate = new Date(depDateArray[2], depDateArray[1], depDateArray[0]);
                    if (document.getElementById('<%=txtTravelToDate.ClientID %>').value.length <= 0) {
                        call8.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate() + 1) + "/" + depDateArray[2]);
                        call8.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                    } else {
                        call8.cfg.setProperty("selected", retDateArray[1] + "/" + eval(retDateArray[0]) + "/" + retDateArray[2]);
                        call8.setMonth(eval(retDateArray[1]) - 1); //Set the calendar month Jan:0 to Dec:11
                    }
                    call8.render();
                }
                document.getElementById('fcontainer8').style.display = "block";
            }

            function setFlightDate7() {
                var date7 = call7.getSelectedDates()[0];
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();
                var todaydate = new Date(thisYear, thisMonth, thisDay);
                var depdate = new Date(date7.getFullYear(), date7.getMonth(), date7.getDate());
                var difference = (depdate.getTime() - todaydate.getTime());
                if (difference < 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";

                var month = date7.getMonth() + 1;
                var day = date7.getDate();

                if (month.toString().length == 1) {
                    month = "0" + month;
                }
                if (day.toString().length == 1) {
                    day = "0" + day;
                }
                document.getElementById('<%=txtTravelFromDate.ClientID %>').value = day + "/" + (month) + "/" + date7.getFullYear();
                call7.hide();
            }

            function setFlightDate8() {
                var date7 = document.getElementById('<%=txtTravelFromDate.ClientID %>').value;
                if (date7.length == 0 || date7 == "DD/MM/YYYY") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "First select departure date.";
                    return false;
                }
                var date8 = call8.getSelectedDates()[0];
                var depDateArray = date7.split('/');
                // checking if date1 is valid		    
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
                // Note: Date()	for javascript take months from 0 to 11
                var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                var returndate = new Date(date8.getFullYear(), date8.getMonth(), date8.getDate());
                var difference = returndate.getTime() - depdate.getTime();
                if (difference < 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Date of return should be greater than or equal to date of departure (" + date7 + ")";
                    return false;
                }
                document.getElementById('errMess').style.display = "none";
                document.getElementById('errorMessage').innerHTML = "";
                var month = date8.getMonth() + 1;
                var day = date8.getDate();
                if (month.toString().length == 1) {
                    month = "0" + month;
                }
                if (day.toString().length == 1) {
                    day = "0" + day;
                }
                document.getElementById('<%=txtTravelToDate.ClientID %>').value = day + "/" + month + "/" + date8.getFullYear();
                call8.hide();
            }

            YAHOO.util.Event.addListener(window, "load", init4);
        </script>

        <script type="text/javascript">
            function validate() {

                var e = document.getElementById("ddlAirLine");
                var optionSelIndexAirline = e.options[e.selectedIndex].value;

                var e1 = document.getElementById("ddlCabinClass");
                var optionSelIndexCabinClass = e1.options[e1.selectedIndex].value;


                var ctrlID = parseInt(document.getElementById('hdnControlsCount').value);
                var dynamicAirline = 0;
                var dynamicCabinClass = 0;
                var dynamicPNR = 0;



                if (ctrlID > 0) {

                    //Get the count of unselected airlines
                    for (var i = 0; i <= ctrlID; i++) {
                        if (document.getElementById('ddlAirLine' + i) != null) {
                            var e = document.getElementById("ddlAirLine" + i);
                            var optionSelIndex = e.options[e.selectedIndex].value;
                            if (optionSelIndex == 0) {
                                dynamicAirline++;
                            }
                        }

                    }

                    //Get count of unselected CabinClass
                    for (var i = 0; i <= ctrlID; i++) {
                        if (document.getElementById('ddlCabinClass' + i) != null) {
                            var e = document.getElementById("ddlCabinClass" + i);
                            var optionSelIndex = e.options[e.selectedIndex].value;
                            if (optionSelIndex == 0) {
                                dynamicCabinClass++;
                            }
                        }

                    }
                    //Get the count of unselected PNR's
                    for (var i = 0; i <= ctrlID; i++) {
                        if (document.getElementById('txtPNR' + i) != null && document.getElementById('txtPNR' + i).value.length == 0) {
                            dynamicPNR++;
                        }

                    }
                }

                //regualar expression for valid time or not (00:00 -- 23:59)           
                var re = /^([0-9]|0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/;
                var OutArrTime = document.getElementById('<%=txtOutArrTime.ClientID %>').value;
                var OutDepTime = document.getElementById('<%=txtOutDepTime.ClientID %>').value;

                if (document.getElementById('divcol').style.display == 'block') {
                    var InDepTime = document.getElementById('<%=txtInDepTime.ClientID %>').value;
                    var InArrTime = document.getElementById('<%=txtInArrTime.ClientID %>').value;
                }




                var txtOutDepDate = document.getElementById('<%=txtOutDepDate.ClientID %>').value;
                var txtOutArrivalDate = document.getElementById('<%=txtOutArrivalDate.ClientID %>').value;

                var OutExCity = document.getElementById('<%=ddlOutExCity.ClientID %>').value;
                var OutToCity = document.getElementById('<%=ddlOutToCity.ClientID %>').value;

                if (document.getElementById('divcol').style.display == 'block') {

                    var InExCity = document.getElementById('<%=ddlInExCity.ClientID %>').value;
                    var InToCity = document.getElementById('<%=ddlInToCity.ClientID %>').value;

                }

                var adultTax = parseInt(Math.round(document.getElementById('<%=txtAdultTax.ClientID %>').value));
                var adultFare = parseInt(Math.round(document.getElementById('<%=txtAdultFare.ClientID %>').value));
                var childFare = parseInt(Math.round(document.getElementById('<%=txtChildFare.ClientID %>').value));
                var childTax = parseInt(Math.round(document.getElementById('<%=txtChildTax.ClientID %>').value));
                var infantFare = parseInt(Math.round(document.getElementById('<%=txtInfantFare.ClientID %>').value));
                var infantTax = parseInt(Math.round(document.getElementById('<%=txtInfanttax.ClientID %>').value));

                var adultTotalFare = adultTax + adultFare;
                var childTotalFare = childFare + childTax;
                var infantTotalFare = infantFare + infantTax;





                if (document.getElementById('divcol').style.display == 'block') {
                    var txtInDepDate = document.getElementById('<%=txtInDepDate.ClientID %>').value;
                    var txtInArrivalDate = document.getElementById('<%=txtInArrivalDate.ClientID %>').value;
                }

                var txtBookFromDate = document.getElementById('<%=txtBookFromDate.ClientID %>').value;
                var txtBookToDate = document.getElementById('<%=txtBookToDate.ClientID %>').value;
                var txtTravelFromDate = document.getElementById('<%=txtTravelFromDate.ClientID %>').value;
                var txtTravelToDate = document.getElementById('<%=txtTravelToDate.ClientID %>').value;

                //Out bound: Ex Country:
                if (document.getElementById('<%=ddlOutExCountry.ClientID %>').selectedIndex <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select OutBound Ex.Country";
                    document.getElementById('<%=ddlOutExCountry.ClientID %>').focus();
                    return false;
                }

                //Out bound : Ex City:
                else if (document.getElementById('<%=ddlOutExCity.ClientID %>').selectedIndex <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select OutBound Ex.City";
                    document.getElementById('<%=ddlOutExCity.ClientID %>').focus();
                    return false;
                }

                //Out bound : To Country:
                else if (document.getElementById('<%=ddlOutToCountry.ClientID %>').selectedIndex <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select OutBound To.Country";
                    document.getElementById('<%=ddlOutToCountry.ClientID %>').focus();
                    return false;
                }

                //Out bound : To City:
                else if (document.getElementById('<%=ddlOutToCity.ClientID %>').selectedIndex <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select OutBound To.City";
                    document.getElementById('<%=ddlOutToCity.ClientID %>').focus();
                    return false;
                }

                //If both Outbouund OutExCity and OutToCity are same.
                else if (OutExCity == OutToCity) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Outbound: Ex city and To city are same";
                    document.getElementById('<%=ddlOutToCity.ClientID %>').focus();
                    return false;
                }




                //Outbound : Dep. Date:
                else if (txtOutDepDate.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Outbound Departure Date";
                    document.getElementById('<%=txtOutDepDate.ClientID %>').focus();
                    return false;
                }

                //Outbound : Dep. Time:
                else if (document.getElementById('<%=txtOutDepTime.ClientID %>').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter OutBound Departure Time";
                    document.getElementById('<%=txtOutDepTime.ClientID %>').focus();
                    return false;
                }

                //Outbound : valid Dep. Time:
                else if (!re.test(OutDepTime)) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter valid                 OutBound Departure Time";
                    document.getElementById('<%=txtOutDepTime.ClientID %>').focus();
                    return false;

                }




                //Outbound : Arrival Date:
                else if (txtOutArrivalDate.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Outbound Arrival Date";
                    document.getElementById('<%=txtOutArrivalDate.ClientID %>').focus();
                    return false;
                }

                //Outbound : Arrival Time:
                else if (document.getElementById('<%=txtOutArrTime.ClientID %>').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter OutBound Arrival Time";
                    document.getElementById('<%=txtOutArrTime.ClientID %>').focus();
                    return false;
                }

                //Outbound : valid Arrival. Time:
                else if (!re.test(OutArrTime)) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter valid OutBound Arrival Time";
                    document.getElementById('<%=txtOutArrTime.ClientID %>').focus();
                    return false;

                }



                //Outbound : Flight No.(s):
                else if (document.getElementById('<%=txtOutFlightNo.ClientID %>').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter OutBound Flight No.";
                    document.getElementById('<%=txtOutFlightNo.ClientID %>').focus();
                    return false;
                }


                //In bound :Ex Country: 
                else if (document.getElementById('divcol').style.display == 'block' && document.getElementById('<%=ddlInExCountry.ClientID %>').selectedIndex <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Inbound Ex.Country";
                    document.getElementById('<%=ddlInExCountry.ClientID %>').focus();
                    return false;
                }
                //In bound :Ex City:
                else if (document.getElementById('divcol').style.display == 'block' && document.getElementById('<%=ddlInExCity.ClientID %>').selectedIndex <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Inbound Ex.City";
                    document.getElementById('<%=ddlInExCity.ClientID %>').focus();
                    return false;
                }
                //In bound:To Country
                else if (document.getElementById('divcol').style.display == 'block' && document.getElementById('<%=ddlInToCountry.ClientID %>').selectedIndex <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Inbound To.Country";
                    document.getElementById('<%=ddlInToCountry.ClientID %>').focus();
                    return false;
                }
                //In bound:To City
                else if (document.getElementById('divcol').style.display == 'block' && document.getElementById('<%=ddlInToCity.ClientID %>').selectedIndex <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Inbound To.City";
                    document.getElementById('<%=ddlInToCity.ClientID %>').focus();
                    return false;
                }
                else if (document.getElementById('divcol').style.display == 'block' && (InExCity == InToCity)) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Inbound ExCity and To.City are same.";
                    document.getElementById('<%=ddlInToCity.ClientID %>').focus();
                    return false;
                }



                //In bound: Dep. Date:

                else if (document.getElementById('divcol').style.display == 'block' && txtInDepDate.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Inbound Departure Date";
                    document.getElementById('<%=txtInDepDate.ClientID %>').focus();
                    return false;
                }

                //In bound: Dep. Time:
                else if (document.getElementById('divcol').style.display == 'block' && document.getElementById('<%=txtInDepTime.ClientID %>').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Inbound Departure Time";
                    document.getElementById('<%=txtInDepTime.ClientID %>').focus();
                    return false;
                }

                //In bound: Dep. Time:
                else if (document.getElementById('divcol').style.display == 'block' && !re.test(InDepTime)) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter  valid Inbound Departure Time";
                    document.getElementById('<%=txtInDepTime.ClientID %>').focus();
                    return false;
                }



                //In bound: Arrival Date:
                else if (document.getElementById('divcol').style.display == 'block' && txtInArrivalDate.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Inbound Arrival Date";
                    document.getElementById('<%=txtInArrivalDate.ClientID %>').focus();
                    return false;
                }
                //In bound: Arrival Time:
                else if (document.getElementById('divcol').style.display == 'block' && document.getElementById('<%=txtInArrTime.ClientID %>').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Inbound Arrival Time";
                    document.getElementById('<%=txtInArrTime.ClientID %>').focus();
                    return false;
                }

                //In bound: Arrival. Time:
                else if (document.getElementById('divcol').style.display == 'block' && !re.test(InArrTime)) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter  valid Inbound Arrival Time";
                    document.getElementById('<%=txtInArrTime.ClientID %>').focus();
                    return false;
                }

                //In bound: Flight No.(s)
                else if (document.getElementById('divcol').style.display == 'block' && document.getElementById('<%=txtInFlightNo.ClientID %>').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Inbound Flight No.";
                    document.getElementById('<%=txtInFlightNo.ClientID %>').focus();
                    return false;
                }

                //Airline
                else if (optionSelIndexAirline == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select AirLine";
                    document.getElementById('ddlAirLine').focus();
                    return false;
                }

                //Cabin Class
                else if (optionSelIndexCabinClass == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Cabin Class";
                    document.getElementById('ddlCabinClass').focus();
                    return false;
                }


                //Dynamic Airline 
                else if (dynamicAirline > 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Airline";
                    return false;
                }
                //Dynamic CabinClass 
                else if (dynamicCabinClass > 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Cabin Class";
                    return false;
                }

                //PNR
                else if (document.getElementById('txtPNR').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter PNR";
                    document.getElementById('txtPNR').focus();
                    return false;
                }
                //Dynamic PNR
                else if (dynamicPNR > 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter PNR";
                    return false;
                }
                //Booking Date : From
                else if (txtBookFromDate.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Booking From Date";
                    document.getElementById('<%=txtBookFromDate.ClientID %>').focus();
                    return false;
                }

                //Booking Date : To
                else if (txtBookToDate.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Booking To Date";
                    document.getElementById('<%=txtBookToDate.ClientID %>').focus();
                    return false;
                }



                //Adult Fare
                else if (document.getElementById('<%=txtAdultFare.ClientID %>').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Adult Fare";
                    document.getElementById('<%=txtAdultFare.ClientID %>').focus();
                    return false;
                }


                //Adult Tax
                else if (document.getElementById('<%=txtAdultTax.ClientID %>').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Adult Tax";
                    document.getElementById('<%=txtAdultTax.ClientID %>').focus();
                    return false;
                }

                //If adultTotalFare < = 0
                else if (parseInt(adultTotalFare) <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Adult Total Fare should be greater than zero";
                    return false;
                }


                //Child Fare
                else if (document.getElementById('<%=txtChildFare.ClientID %>').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Child Fare";
                    document.getElementById('<%=txtChildFare.ClientID %>').focus();
                    return false;
                }

                //Child Tax
                else if (document.getElementById('<%=txtChildTax.ClientID %>').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Child Tax";
                    document.getElementById('<%=txtChildTax.ClientID %>').focus();
                    return false;
                }
                //If ChildTotalFare < = 0
                else if (parseInt(childTotalFare) <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Child Total Fare should be greater than zero";
                    return false;
                }




                else {
                    document.getElementById('errorMessage').innerHTML = "";
                    document.getElementById('<%=hdnOutExCity.ClientID %>').value = document.getElementById('<%=ddlOutExCity.ClientID %>').value;
                    document.getElementById('<%=hdnOutToCity.ClientID %>').value = document.getElementById('<%=ddlOutToCity.ClientID %>').value;
                    document.getElementById('<%=hdnInExCity.ClientID %>').value = document.getElementById('<%=ddlInExCity.ClientID %>').value;
                    document.getElementById('<%=hdnInToCity.ClientID %>').value = document.getElementById('<%=ddlInToCity.ClientID %>').value;
                    saveAirlineCabinFareBooking();
                    savePNRSeats();
                }

                return true;
            }
 
 
 
 
 
        </script>

        <script type="text/javascript">
            function disablefield() {
                if (document.getElementById('<%=roundtrip.ClientID %>').checked == true) {
                    document.getElementById('divcol').style.display = 'block';
                }
                else {
                    document.getElementById('divcol').style.display = 'none';
                }
            }
        </script>

        <script type="text/javascript">
            $('iframe').load(function() {
                $('#TopUpFrame').contents().find('#header').hide();
                $('#TopUpFrame').contents().find('div.footer_big').hide();
            }); 
        </script>

        <asp:HiddenField runat="server" ID="hdfEMId" Value="0" />
        <asp:HiddenField runat="server" ID="hdfMode" Value="0"></asp:HiddenField>
        <asp:HiddenField runat="server" ID="hdfEModePNR" Value="" />
        <asp:HiddenField runat="server" ID="hdfEModeAirline" Value="" />
        <asp:HiddenField ID="hdnAirlineCabinFareBooking" runat="server" />
        <asp:HiddenField ID="hdnPNRSeats" runat="server" />
        <input id="hdnControlsCount" type="hidden" value="0" />
        <asp:HiddenField ID="hdnOutExCity" runat="server" />
        <asp:HiddenField ID="hdnOutToCity" runat="server" />
        <asp:HiddenField ID="hdnInExCity" runat="server" />
        <asp:HiddenField ID="hdnInToCity" runat="server" />
        <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
        </iframe>
        <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="fcontainer1" class="fcontainer-calendar" style="position: absolute; top: 179px; left: 10%; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="fcontainer2" class="fcontainer-calendar" style="position: absolute; top: 179px; left: 15%; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="fcontainer3" class="fcontainer-calendar" style="position: absolute; top: 200px; left: 10%; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="fcontainer4" class="fcontainer-calendar" style="position: absolute; top: 200px; left: 15%; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="fcontainer5" class="fcontainer-calendar" style="position: absolute; top: 300px; left: 10%; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="fcontainer6" class="fcontainer-calendar" style="position: absolute; top: 300px; left: 15%; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="fcontainer7" class="fcontainer-calendar" style="position: absolute; top: 300px; left: 40%; display: none;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="fcontainer8" class="fcontainer-calendar" style="position: absolute; top: 300px; left: 48%; display: none;">
            </div>
        </div>
        <div>
            <div class="col-md-4 col-xs-4">
                <asp:RadioButton ID="oneway" Checked="true" GroupName="radio" onclick="disablefield();"
                    runat="server" Text="One Way" Font-Bold="true" />
            </div>
            <div class="col-md-4 col-xs-4">
                <asp:RadioButton ID="roundtrip" GroupName="radio" onclick="disablefield();" runat="server"
                    Text="Round Trip" Font-Bold="true" />
            </div>
        </div>
        <div class="clearfix">
        </div>
        <div class=" paddingtop_10">
            <h4>
                Out bound</h4>
        </div>
        <div class=" paddingbot_10 ">
            <div class="col-md-3">
                <table width="100%">
                    <tr>
                        <td width="100">
                            Ex Country:<span class="fcol_red">*</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlOutExCountry" runat="server" class="form-control" onchange="javascript:ChangeReturnExCountry()">
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-3">
                <table width="100%">
                    <tr>
                        <td width="100">
                            Ex City:<span class="fcol_red">*</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlOutExCity" runat="server" class="form-control">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-3">
                <table width="100%">
                    <tr>
                        <td width="100">
                            To Country:<span class="fcol_red">*</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlOutToCountry" runat="server" class="form-control" onchange="javascript:ChangeReturnToCountry()">
                                <asp:ListItem Selected="True" Text="-Select Country-"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-3">
                <table width="100%">
                    <tr>
                        <td width="100">
                            To City:<span class="fcol_red">*</span>
                        </td>
                        <td>
                            <asp:DropDownList ID="ddlOutToCity" runat="server" class="form-control">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class=" paddingbot_10 paddingtop_10">
            <div class=" paddingbot_10 paddingtop_10">
                <div class="paddingbot_10 paddingtop_10">
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    Dep. Date:<span class="fcol_red">*</span>
                                </td>
                                <td>
                                    <asp:TextBox placeholder="dd/mm/yyyy" MaxLength="10" ID="txtOutDepDate" runat="server"
                                        class="form-control"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showFlightCalendar3()">
                                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    Dep. Time:<span class="fcol_red">*</span>
                                </td>
                                <td>
                                    <asp:TextBox placeholder="HH:MM" MaxLength="5" ID="txtOutDepTime" CssClass="time form-control"
                                        onkeypress="return isValidTime(event);" runat="server" class="form-control"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    Time Zone:
                                </td>
                                <td>
                                    <asp:DropDownList ID="ddlOutTimeZone" runat="server" class="form-control">
                                        <asp:ListItem Selected="True" Text="Select"></asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    Terminal:
                                </td>
                                <td>
                                    <asp:TextBox onkeypress="return IsAlphaNumeric(event);" MaxLength="50" ID="txtOutTerminal"
                                        runat="server" class="form-control"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="paddingbot_10 paddingtop_10">
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    Arrival Date:<span class="fcol_red">*</span>
                                </td>
                                <td>
                                    <asp:TextBox placeholder="dd/mm/yyyy" MaxLength="10" ID="txtOutArrivalDate" runat="server"
                                        class="form-control"></asp:TextBox>
                                </td>
                                <td>
                                    <a href="javascript:void(null)" onclick="showFlightCalendar4()">
                                        <img id="Img2" src="images/call-cozmo.png" alt="Pick Date" />
                                    </a>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    Arrival Time:<span class="fcol_red">*</span>
                                </td>
                                <td>
                                    <asp:TextBox placeholder="HH:MM" MaxLength="5" ID="txtOutArrTime" CssClass="form-control time"
                                        onkeypress="return isValidTime(event);" runat="server" class="form-control"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    Flight No.(s):<span class="fcol_red">*</span>
                                </td>
                                <td>
                                    <asp:TextBox onkeypress="return IsAlphaNumeric(event);" MaxLength="10" ID="txtOutFlightNo"
                                        runat="server" class="form-control"></asp:TextBox>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <%--<a class=" pull-right" href="#"> Add Out Bound </a>--%><div class="clearfix">
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class=" paddingbot_10 border_bot1">
        </div>
        <div class="paddingtop_10">

            <script type="text/javascript">
                $(document).ready(function() {
                    $("#collapsediv").click(function() {

                        $("span").toggleClass("glyphicon-minus");


                    });
                });
            </script>

        </div>
        <div id="divcol" class="collapse">
            <div class="paramcon">
                <div class="paddingtop_10">
                    <h4>
                        In bound</h4>
                </div>
                <div class="paramcon">
                    <div class=" paddingbot_10">
                        <div class="col-md-3">
                            <table width="100%">
                                <tr>
                                    <td width="100">
                                        Ex Country:<span class="fcol_red">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlInExCountry" runat="server" class="form-control" onchange="javascript:ChangeOnwardExCountry()">
                                            <asp:ListItem Selected="True" Text="-Select Country-"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table width="100%">
                                <tr>
                                    <td width="100">
                                        Ex City:<span class="fcol_red">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlInExCity" runat="server" class="form-control">
                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table width="100%">
                                <tr>
                                    <td width="100">
                                        To Country:<span class="fcol_red">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlInToCountry" runat="server" class="form-control" onchange="javascript:ChangeOnwardToCountry()">
                                            <asp:ListItem Selected="True" Text="-Select Country-"></asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table width="100%">
                                <tr>
                                    <td width="100">
                                        To City:<span class="fcol_red">*</span>
                                    </td>
                                    <td>
                                        <asp:DropDownList ID="ddlInToCity" runat="server" class="form-control">
                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class=" paddingbot_10 paddingtop_10">
                        <div class=" paddingbot_10 paddingtop_10">
                            <div class="paddingbot_10 paddingtop_10">
                                <div class="col-md-3">
                                    <table width="100%">
                                        <tr>
                                            <td width="100">
                                                Dep. Date:<span class="fcol_red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox MaxLength="10" placeholder="dd/mm/yyyy" ID="txtInDepDate" runat="server"
                                                    class="form-control"></asp:TextBox>
                                            </td>
                                            <td>
                                                <a href="javascript:void(null)" onclick="showFlightCalendar1()">
                                                    <img id="Img4" src="images/call-cozmo.png" alt="Pick Date" />
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <table width="100%">
                                        <tr>
                                            <td width="100">
                                                Dep. Time:<span class="fcol_red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox placeholder="HH:MM" ID="txtInDepTime" runat="server" CssClass="form-control time"
                                                    MaxLength="5" onkeypress="return isValidTime(event);" class="form-control"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <table width="100%">
                                        <tr>
                                            <td width="100">
                                                Time Zone:
                                            </td>
                                            <td>
                                                <asp:DropDownList ID="ddlInTimeZone" runat="server" class="form-control">
                                                    <asp:ListItem Selected="True" Text="Select"></asp:ListItem>
                                                </asp:DropDownList>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <table width="100%">
                                        <tr>
                                            <td width="100">
                                                Terminal:
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtInTerminal" runat="server" class="form-control"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="clearfix">
                                </div>
                            </div>
                            <div class="paddingbot_10 paddingtop_10">
                                <div class="col-md-3">
                                    <table width="100%">
                                        <tr>
                                            <td width="100">
                                                Arrival Date:<span class="fcol_red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox MaxLength="10" placeholder="dd/mm/yyyy" ID="txtInArrivalDate" runat="server"
                                                    class="form-control"></asp:TextBox>
                                            </td>
                                            <td>
                                                <a href="javascript:void(null)" onclick="showFlightCalendar2()">
                                                    <img id="Img3" src="images/call-cozmo.png" alt="Pick Date" />
                                                </a>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <table width="100%">
                                        <tr>
                                            <td width="100">
                                                Arrival Time:<span class="fcol_red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox placeholder="HH:MM" ID="txtInArrTime" CssClass="form-control time" runat="server"
                                                    MaxLength="5" onkeypress="return isValidTime(event);" class="form-control"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="col-md-3">
                                    <table width="100%">
                                        <tr>
                                            <td width="100">
                                                Flight No.(s):<span class="fcol_red">*</span>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="txtInFlightNo" runat="server" class="form-control"></asp:TextBox>
                                            </td>
                                        </tr>
                                    </table>
                                </div>
                                <div class="clearfix">
                                </div>
                            </div>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class=" paddingbot_10 border_bot1">
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <div class="clearfix">
        </div>
        <div class=" paddingbot_10 border_bot1" id="fareBookingClassDiv">
            <div class="paramcon">
                <div class=" paddingbot_10 paddingtop_10">
                    <div class="col-md-10">
                        <div class="col-md-2">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            Airline:<span class="fcol_red">*</span>
                                        </td>
                                        <td>
                                            <select onchange="verifyIsLCC('ddlAirLine')" id="ddlAirLine" class="form-control">
                                                <option selected="selected" value="Select">Select</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-1">
                            <table>
                                <td>
                                    IsLCC:
                                </td>
                                <td>
                                    <input disabled="disabled" id="chkIsLCC" type="checkbox" />
                                </td>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            Cabin Class:<span class="fcol_red">*</span>
                                        </td>
                                        <td>
                                            <select id="ddlCabinClass" class="form-control">
                                                <option selected="selected" value="Select">Select</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            Fare Basis:
                                        </td>
                                        <td>
                                            <input maxlength="15" onkeypress="return IsAlphaNumeric(event);" class="form-control"
                                                type="text" id="txtFareBasis" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-3">
                            <table>
                                <tbody>
                                    <tr>
                                        <td>
                                            Booking Class:
                                        </td>
                                        <td>
                                            <input maxlength="10" onkeypress="return IsAlphaNumeric(event);" type="text" class="form-control"
                                                id="txtBookClass" />
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-2" style="display: none;">
                        <a class="pull-right" onclick="javascript:AddFareCabinBookingDiv();">Add</a>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <div class=" paddingbot_10 border_bot1" id="seatsPNRDiv">
            <div class="paramcon">
                <div class=" paddingbot_10 paddingtop_10">
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    PNR:<span class="fcol_red">*</span>
                                </td>
                                <td>
                                    <input type="text" onkeypress="return IsAlphaNumeric(event);" maxlength="50" id="txtPNR"
                                        class="form-control" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    No. of Seats(Available):<span class="fcol_red">*</span>
                                </td>
                                <td>
                                    <select id="ddlNoOfSeats" class="form-control">
                                        <option selected="selected" value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3" style="display: none;" id="divNoOfSeatsUsed">
                        <table width="100%">
                            <tr>
                                <td width="100">
                                    No. of Seats(Used):<span class="fcol_red">*</span>
                                </td>
                                <td>
                                    <select id="ddlNoOfSeatsUsed" class="form-control">
                                        <option selected="selected" value="0">0</option>
                                        <option value="1">1</option>
                                        <option value="2">2</option>
                                        <option value="3">3</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                        <option value="9">9</option>
                                    </select>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="col-md-3">
                        <a class=" pull-right" onclick="javascript:AddSeatsPNRDiv();">Add </a>
                        <div class="clearfix">
                        </div>
                    </div>
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="paddingbot_10 border_bot1">
            <div class="paramcon">
                <div class=" paddingbot_10 paddingtop_10">
                    <div class="col-md-6 pad_left0">
                        <div class="col-md-12">
                            <strong>Booking Date:</strong>
                        </div>
                        <div class="col-md-6">
                            <table width="100%">
                                <tr>
                                    <td width="100">
                                        From:<span class="fcol_red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox placeholder="dd/mm/yyyy" MaxLength="10" ID="txtBookFromDate" runat="server"
                                            class="form-control"></asp:TextBox>
                                    </td>
                                    <td>
                                        <a href="javascript:void(null)" onclick="showFlightCalendar5()">
                                            <img id="Img5" src="images/call-cozmo.png" alt="Pick Date" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table width="100%">
                                <tr>
                                    <td width="100">
                                        To:<span class="fcol_red">*</span>
                                    </td>
                                    <td>
                                        <asp:TextBox placeholder="dd/mm/yyyy" MaxLength="10" ID="txtBookToDate" runat="server"
                                            class="form-control"></asp:TextBox>
                                    </td>
                                    <td>
                                        <a href="javascript:void(null)" onclick="showFlightCalendar6()">
                                            <img id="Img6" src="images/call-cozmo.png" alt="Pick Date" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="col-md-6 pad_left0">
                        <div class="col-md-12">
                            <strong>Travel Date:</strong>
                        </div>
                        <div class="col-md-6">
                            <table width="100%">
                                <tr>
                                    <td width="100">
                                        From:
                                    </td>
                                    <td>
                                        <asp:TextBox placeholder="dd/mm/yyyy" MaxLength="10" ID="txtTravelFromDate" runat="server"
                                            class="form-control"></asp:TextBox>
                                    </td>
                                    <td>
                                        <a href="javascript:void(null)" onclick="showFlightCalendar7()">
                                            <img id="Img7" src="images/call-cozmo.png" alt="Pick Date" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <table width="100%">
                                <tr>
                                    <td width="100">
                                        To:
                                    </td>
                                    <td>
                                        <asp:TextBox placeholder="dd/mm/yyyy" MaxLength="10" ID="txtTravelToDate" runat="server"
                                            class="form-control"></asp:TextBox>
                                    </td>
                                    <td>
                                        <a href="javascript:void(null)" onclick="showFlightCalendar8()">
                                            <img id="Img8" src="images/call-cozmo.png" alt="Pick Date" />
                                        </a>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
            </div>
        </div>
        <div class="paddingbot_10 border_bot1">
            <div class=" paddingtop_10">
                <h4>
                    Fare Details</h4>
            </div>
            <div class="paramcon">
                <div class="col-md-4">
                    <table width="100%">
                        <tr>
                            <td colspan="3">
                                <strong>Adult </strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    Fare:<span class="fcol_red">*</span></div>
                                <div>
                                    <asp:TextBox onkeypress="return isNumber(event);" ID="txtAdultFare" runat="server"
                                        class="form-control"></asp:TextBox></div>
                            </td>
                            <td width="10">
                            </td>
                            <td>
                                <div>
                                    Tax:<span class="fcol_red">*</span></div>
                                <div>
                                    <asp:TextBox onkeypress="return isNumber(event);" ID="txtAdultTax" runat="server"
                                        class="form-control"></asp:TextBox>
                                </div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4">
                    <table width="100%">
                        <tr>
                            <td colspan="3">
                                <strong>Child </strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    Fare:<span class="fcol_red">*</span></div>
                                <div>
                                    <asp:TextBox onkeypress="return isNumber(event);" ID="txtChildFare" runat="server"
                                        class="form-control"></asp:TextBox></div>
                            </td>
                            <td width="10">
                            </td>
                            <td>
                                <div>
                                    Tax:<span class="fcol_red">*</span></div>
                                <div>
                                    <asp:TextBox onkeypress="return isNumber(event);" ID="txtChildTax" runat="server"
                                        class="form-control"></asp:TextBox></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-4">
                    <table width="100%">
                        <tr>
                            <td colspan="3">
                                <strong>Infant </strong>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <div>
                                    Fare:</div>
                                <div>
                                    <asp:TextBox onkeypress="return isNumber(event);" ID="txtInfantFare" runat="server"
                                        class="form-control"></asp:TextBox></div>
                            </td>
                            <td width="10">
                            </td>
                            <td>
                                <div>
                                    Tax:</div>
                                <div>
                                    <asp:TextBox onkeypress="return isNumber(event);" ID="txtInfanttax" runat="server"
                                        class="form-control"></asp:TextBox></div>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clearfix">
                </div>
            </div>
        </div>
        <div class="paddingtop_10">
            <asp:Button ID="btnSearch" CssClass="btn but_b pull-right" OnClick="btnSearch_Click"
                runat="server" Text="Search" />
            &nbsp;&nbsp;
            <asp:Button class="btn  but_b pull-right" ID="btnSubmit" runat="server" Text="Submit"
                OnClick="btnSubmit_Click" OnClientClick="return validate();" />
            <!--OnClientClick="return validate();" -->
            <div class="clearfix">
            </div>
        </div>
    </div>
    <div class="clearfix">
    </div>
    <div class="col-md-12">
        <asp:Label runat="server" ID="lblSuccess" CssClass="alert-success"></asp:Label>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
    <div class="col-md-12">
        <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="ID"
            EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
            GridLines="none" CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" OnRowDataBound="gvSearch_RowDataBound"
            CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">
            <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
            <Columns>
                <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                    ControlStyle-CssClass="label" ShowSelectButton="True" />
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtORIGIN" Width="70px" HeaderText="ORIGIN" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblORIGIN" runat="server" Text='<%# Eval("ORIGIN") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("ORIGIN") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtDESTINATION" Width="70px" HeaderText="DESTINATION" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblDESTINATION" runat="server" Text='<%# Eval("DESTINATION") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("DESTINATION") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtPNR" Width="100px" HeaderText="PNR" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblPNR" runat="server" Text='<%# Eval("PNR") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("PNR") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtSEARCHTYPE" Width="70px" HeaderText="SEARCH" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblSEARCHTYPE" runat="server" Text='<%# Eval("SEARCHTYPE") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("SEARCHTYPE") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtDEPDATE" Width="70px" HeaderText="DEP. DATE" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblDEPDATE" runat="server" Text='<%# Eval("DEPDATE") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("DEPDATE") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtRETDATE" Width="70px" HeaderText="RET. DATE" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblRETDATE" runat="server" Text='<%# Eval("RETDATE") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("RETDATE") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtBKEPERIODFROM" Width="70px" HeaderText="BOOK FROM" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblBKEPERIODFROM" runat="server" Text='<%# Eval("BKEPERIODFROM") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("BKEPERIODFROM") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtBKEPERIODTO" Width="70px" HeaderText="BOOK TO" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblBKEPERIODTO" runat="server" Text='<%# Eval("BKEPERIODTO") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("BKEPERIODTO") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtISLCC" Width="70px" HeaderText="IS LCC" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblISLCC" runat="server" Text='<%# Eval("ISLCC") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("ISLCC") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAIRLINE" Width="70px" HeaderText="AIRLINE" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblAIRLINE" runat="server" Text='<%# Eval("AIRLINE") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("AIRLINE") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtARRIVALTIME" Width="70px" HeaderText="ARR. TIME" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblARRIVALTIME" runat="server" Text='<%# Eval("ARRIVALTIME") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("ARRIVALTIME") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtDEPTTIME" Width="70px" HeaderText="DEP. TIME" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblDEPTTIME" runat="server" Text='<%# Eval("DEPTTIME") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("DEPTTIME") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtCABINCLASS" Width="70px" HeaderText="CABIN" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblCABINCLASS" runat="server" Text='<%# Eval("CABINCLASS") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("CABINCLASS") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtFLIGHTNO" Width="70px" HeaderText="FLIGHT NO" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblFLIGHTNO" runat="server" Text='<%# Eval("FLIGHTNO") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("FLIGHTNO") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtPAXTYPE" Width="70px" HeaderText="PAX TYPE" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblPAXTYPE" runat="server" Text='<%# Eval("PAXTYPE") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("PAXTYPE") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                
               
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtBASEFARE" Width="70px" HeaderText="BASE FARE" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblBASEFARE" runat="server" Text='<%# Eval("BASEFARE") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("BASEFARE") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                
                 
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtTAX" Width="70px" HeaderText="TAX" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblTAX" runat="server" Text='<%# Eval("TAX") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("TAX") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtPUBLISHEDFARE" Width="70px" HeaderText="PUB. FARE" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblPUBLISHEDFARE" runat="server" Text='<%# Eval("PUBLISHEDFARE") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("PUBLISHEDFARE") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAVAILQUOTA" Width="70px" HeaderText="AVAILABLE" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblAVAILQUOTA" runat="server" Text='<%# Eval("AVAILQUOTA") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("AVAILQUOTA") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtUSEDQUOTA" Width="70px" HeaderText="USED
                        " CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblUSEDQUOTA" runat="server" Text='<%# Eval("USEDQUOTA") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("USEDQUOTA") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField HeaderText="Status">
                    
                    <ItemTemplate>
                        <asp:LinkButton Font-Bold="true" ID="ITlnkStatus" OnClick="ITlnkStatus_Click" 
                            Font-Size="11px" runat="server" Width="150px"></asp:LinkButton>
     <asp:HiddenField ID="IThdfStatus" runat="server" Value='<%# Bind("INVSTATUS") %>'></asp:HiddenField>
     <asp:HiddenField ID="IThdfInvId" runat="server" Value='<%# Bind("INVID") %>'>
                        </asp:HiddenField>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
