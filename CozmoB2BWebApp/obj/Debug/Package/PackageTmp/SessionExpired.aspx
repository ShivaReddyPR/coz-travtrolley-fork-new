<%@ Page Language="C#" AutoEventWireup="true" Inherits="SessionExpired" Codebehind="SessionExpired.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme %>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme %>://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Session Expired</title>
    <link href="css/CTStyle.css" rel="stylesheet" type="text/css" />
    
    
    <script type="text/javascript">
    function closeWindow()
    {
        window.opener="_self";
        window.close();
    }
     function loadLoginPage()
    {
        window.open('login.aspx','_self');
        return false;
    }
    </script>
</head>
<body>
    <form id="form1" runat="server">
    <div style="width:900px; margin:auto; padding-top:120px;">
            <table cellpadding="2" cellspacing="0" border="0" width="100%" class="clsBorder">
                <tr>
                    <td colspan="3" class="lblHed" style='height: 20px' align="center">
                        Session Expired !!!
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td align="center">
                        <asp:Label CssClass="lblMsg" ID="lblErrMsg" runat="server"></asp:Label>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td colspan="3" align="center">
                        <%--<asp:Button ID="btnClose" runat="server" OnClientClick="return closeWindow();" Text=" Close " CssClass="btnOff"></asp:Button>--%>
                        <asp:Button ID="btnLogin" runat="server" OnClientClick="return loadLoginPage();" Text=" Login " CssClass="btnOff"></asp:Button>
                    </td>
                </tr>
                <tr>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
