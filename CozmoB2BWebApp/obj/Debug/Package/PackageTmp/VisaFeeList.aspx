﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="VisaFeeList" Title="Cozmo Travels" Codebehind="VisaFeeList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
   
<script type="text/javascript">
    function Validate() {
        if (document.getElementById('<%=ddlCountry.ClientID%>') == null || document.getElementById('<%=ddlCountry.ClientID%>').value == -1) {
            alert('Please Select the Country');
            return false;
        }
        else {
            return true;
        }
    }

</script>   
   
     <div> <h4>Visa Fee List</h4> </div>
     
     
      <div class=" bg_white bor_gray pad_10 paramcon">
          <div class="row">
              <div class="col-md-2"> <label class="pull-right fl_xs"> Search by country:</label> </div>
    
            <div class="col-md-2"><asp:DropDownList ID="ddlCountry" CssClass="form-control" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged">
                        </asp:DropDownList> </div>
                
            <div class="col-md-2"> Agent: </div>
            <div class="col-md-2"> <asp:DropDownList ID="ddlAgent" CssClass="form-control" runat="server">
                        </asp:DropDownList></div>
            <div class="col-md-2"> Visa Type: </div>
            <div class="col-md-2"> <asp:DropDownList ID="ddlVisaType" CssClass="form-control" runat="server">
                        </asp:DropDownList></div>
            <div class="clearfix"> </div> 
          </div>
          <div class="row">
            <div class="col-md-2"><label class="pull-right fl_xs"> Nationality: </label></div>
            <div class="col-md-2"> <asp:DropDownList ID="ddlNationality" CssClass="form-control" runat="server">
                        </asp:DropDownList></div>
            <div class="col-md-4 xspadtop10">    <asp:Button ID="btnSearch" CssClass="but but_b pull-right" runat="server" Text="Search" OnClick="btnSearch_Click" OnClientClick="return Validate();"/> </div>
            <div class="col-md-4"><asp:HyperLink ID="HyperLink1" CssClass="fcol_blue pull-right" runat="server" NavigateUrl="~/AddVisaFee.aspx">Add New Visa Fee </asp:HyperLink> </div>
       
    
            <div class="clearfix"> </div> 
        </div>
    </div>
    
    
   
   
    
    <div>
        <asp:Label ID="lbl_msg" runat="server" Text=""></asp:Label>
    </div>
   <div class="table-responsive margin-top-10">  
   
    <asp:GridView ID="GridView1" CssClass="datagrid" runat="server" AutoGenerateColumns="False"
        CellPadding="1" Width="100%" DataKeyNames="visaFeeId" ForeColor="#333333" GridLines="None"
        OnRowDataBound="GridView1_RowDataBound" OnRowCommand="GridView1_RowCommand" AllowPaging="True"
        OnPageIndexChanging="GridView1_PageIndexChanging">
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <Columns>
        <asp:BoundField DataField="agency" HeaderText="Agent" ReadOnly="True">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="visaTypeId" HeaderText="Visa Type" ReadOnly="True">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="visaTypeName" HeaderText="Visa Type Name" ReadOnly="True">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle HorizontalAlign="Left" Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="cost" DataFormatString="{0:n}" HeaderText="Cost">
                <HeaderStyle HorizontalAlign="Left" />
                <ItemStyle Width="80px" />
            </asp:BoundField>
            <asp:BoundField DataField="markup" HeaderText="Markup" DataFormatString="{0:n}">
                <HeaderStyle HorizontalAlign="Center" Width="82px" />
            </asp:BoundField>
            <asp:BoundField DataField="insuranceCharge" HeaderText="Insurance Charge" DataFormatString="{0:n}">
                <HeaderStyle HorizontalAlign="Center" Width="82px" />
            </asp:BoundField>
            <asp:BoundField DataField="refundable" HeaderText="Refundable Security" DataFormatString="{0:n}">
                <HeaderStyle HorizontalAlign="Center" Width="82px" />
            </asp:BoundField>
            <asp:BoundField DataField="nationalityCode" HeaderText="Nationality">
                <HeaderStyle HorizontalAlign="Center" Width="82px" />
            </asp:BoundField>
            <asp:BoundField DataField="isActive" HeaderText="Is Active">
                <HeaderStyle HorizontalAlign="Center" Width="82px" />
            </asp:BoundField>
            
            <asp:HyperLinkField Text="Edit" DataNavigateUrlFields="visaFeeId" HeaderText="Edit"
                DataNavigateUrlFormatString="AddVisaFee.aspx?id={0}">
                
                <HeaderStyle HorizontalAlign="Center" Width="30px" />
                <ItemStyle HorizontalAlign="Left" Width="30px" />
            </asp:HyperLinkField>
            <asp:TemplateField HeaderText="Activate/Deactivate">
                <HeaderStyle HorizontalAlign="Center" />
                <ItemStyle HorizontalAlign="Left" Width="8px" />
                <ItemTemplate>
                    <asp:LinkButton ID="linkButtonStatus" runat="server"  Text="Activate"
                        CommandName="ChangeStatus">
                    </asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
          <RowStyle />
        <EditRowStyle />
        <SelectedRowStyle  Font-Bold="True" />
        <PagerStyle HorizontalAlign="Left" Font-Bold="True" />
        <HeaderStyle Font-Bold="True" HorizontalAlign="Left" />
         <AlternatingRowStyle CssClass="altrow" />
        <PagerSettings FirstPageText="First" LastPageText="Last" Mode="NumericFirstLast"
            NextPageText="Next" PreviousPageText="Previous" />
    </asp:GridView>
    
    
    </div>

</asp:Content>
