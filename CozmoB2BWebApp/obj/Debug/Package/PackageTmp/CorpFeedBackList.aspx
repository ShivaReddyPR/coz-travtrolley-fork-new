﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="CorpFeedBackList" Title="Corporate Feedback List" Codebehind="CorpFeedBackList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script type="text/javascript" src="js/Search.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
<style>
    
  .GridPager a, .GridPager span
    {
        display: block;
        height: 15px;
        width: 15px;
        font-weight: bold;
        text-align: center;
        text-decoration: none; margin:4px;
    }
    .GridPager a
    {
        background-color: #f5f5f5;
        color: #969696;
        border: 1px solid #969696;
    }
    .GridPager span
    {
        background-color: #A1DCF2;
        color: #000;
        border: 1px solid #3AC0F2;
    }  
    
    
    
  .label { line-height:26px; font-size:12px!important; font-family:Arial;}  
    
.firstPopupDivOuter
        {
            position: absolute;
            top: 50px;
            left: 100px;
            width: 400px;
            height: 300px;
            display: none;
            background-color:#2BA9D9;
            color:#FFF;
        }
        #firstPopupDivInner
        {
            width: 378px;
            height: 268px;
            background: url(images/thankuwindow.png) no-repeat;
            vertical-align: middle;
            margin: 160px auto;
        }
        
        
        
        
        a.paging_list { background:#fff; border: solid 1px #ccc; padding:4px; color:Green; display:block }
        
   .gvHeader2 { background:#1c498a; font-size:12px!important; font-family:Arial; color:#fff; height:26px;   }
       
</style>
    <script type="text/javascript">
        //--------------------------Calender control start-------------------------------
        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) /" + today.getDate() /" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }
        function showCal1() {
            init();
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            cal1.show();
            cal2.hide();
            document.getElementById('container1').style.display = "block";

        }


        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= CheckIn.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }
        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            //    if (difference < 0) {
            //        document.getElementById('errMess').style.display = "block";
            //        document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            //        return false;
            //    }
            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= CheckIn.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDates2() {
            var date1 = document.getElementById('<%=CheckIn.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=CheckOut.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);



        function ShowHide(div) {
            if (getElement('hdfParam').value == '1') {
                document.getElementById('ancParam').innerHTML = 'Show Param'
                document.getElementById(div).style.display = 'none';
                getElement('hdfParam').value = '0';
            }
            else {
                document.getElementById('ancParam').innerHTML = 'Hide Param'
                document.getElementById('ancParam').value = 'Hide Param'
                document.getElementById(div).style.display = 'block';
                getElement('hdfParam').value = '1';
            }
        }
        function popupopen(popup_name) {
            document.getElementById(popup_name).style.display = 'block';
        }

        function popupclose(popup_name) {
            document.getElementById(popup_name).style.display = 'none';
        }
    </script>
<script type="text/javascript" language="javascript">
    function newwindow(url) {

        window.open(url, '', 'toolbar=no,width=900,height=500,resizable=yes,scrollbars=yes, position=center', true);

    } 

</script>

    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>
    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0">
    </iframe>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; top: 120px; left: 11%; z-index:100; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; top: 120px; left: 32%; z-index:100; display: none;">
        </div>
    </div>
    <div>
        <asp:HiddenField runat="server" ID="hdfParam" Value="1"></asp:HiddenField>
        <table cellpadding="0" cellspacing="0" class="label">
            <tr>
                <td style="width: 700px" align="left">
                    <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam"
                        onclick="return ShowHide('divParam');">Hide Parameter</a>
                </td>
            </tr>
        </table>
        <div title="Param" id="divParam">
            <asp:Panel runat="server" ID="pnlParam" Visible="true">
             
             
             
             
            <div class="marbot_10 paramcon"> 
            
            <div class="col-md-1 pad0">  From Date:</div>
            
            <div class="col-md-2">  <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="CheckIn" runat="server" Width="100px" CssClass="form-control"></asp:TextBox>
                                    </td>
                                    <td>
                                        <a href="javascript:void(null)" onclick="showCal1()">
                                            <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" style="vertical-align: middle;
                                                margin: 5px" />
                                        </a>
                                    </td>
                                </tr>
                            </table></div>
            
            <div class="col-md-1">To Date: </div>
            
            <div class="col-md-2"> <table border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="CheckOut" runat="server" CssClass="form-control" Width="100px"></asp:TextBox>
                                    </td>
                                    <td align="left">
                                        <a href="javascript:void(null)" onclick="showCal2()">
                                            <img id="Img1" src="images/call-cozmo.png" alt="Pick Date"/>
                                        </a>
                                    </td>
                                </tr>
                            </table></div> 
            
            <div class="col-md-2"><asp:Button CssClass="button" ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />   </div>
            
            </div>
             
             
             
             
             
             
                
            </asp:Panel>
        </div>
    </div>
    
    <div> <asp:GridView ID="gvCorpFBList" runat="server" Width="100%" AutoGenerateColumns="False"
                    AllowPaging="true" PagerSettings-Position="TopAndBottom" PageSize="20" OnPageIndexChanging="gvCorpList_PageIndexChanging"
                    Caption="Corporate Feedback List" CaptionAlign="Top" CssClass="mydatagrid" GridLines="None" CellPadding="0" CellSpacing="0"  
                    OnRowDataBound="gvCorpList_RowDataBound">
                   <HeaderStyle CssClass="mydatagrid themecol1" HorizontalAlign="Left"></HeaderStyle>
            <PagerSettings Position="TopAndBottom"></PagerSettings>
            
            <RowStyle  HorizontalAlign="left" />
            <AlternatingRowStyle CssClass="altrow"/>
                    <Columns>
                     <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="S.No">
                            <ItemTemplate>
                                 <asp:Label width="50px"  ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                           
                            
                        </asp:TemplateField>
                        <asp:TemplateField  HeaderStyle-Font-Bold="true" HeaderText="Name">
                            <ItemTemplate>
                                <asp:Label  ID="Label1" runat="server"  Text='<%# Bind("CFName") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox1" runat="server"  Text='<%# Bind("CFName") %>'></asp:TextBox>
                            </EditItemTemplate>
                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="Email">
                            <ItemTemplate>
                                <asp:Label width="150px"  ID="Label2" runat="server"  Text='<%# Bind("CFEmail") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("CFEmail") %>'></asp:TextBox>
                            </EditItemTemplate>
                            
                        </asp:TemplateField>
                        <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="Company">
                            <ItemTemplate>
                                <asp:Label   ID="Label3" runat="server" Text='<%# Bind("CFCompany") %>' ></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("CFCompany") %>'></asp:TextBox>
                            </EditItemTemplate>
                            
                        </asp:TemplateField>
                        <%--@@@@@@--%>
                       
                        <asp:TemplateField HeaderStyle-Font-Bold="true" HeaderText="Created Date">
                            <ItemTemplate>
                                <asp:Label  width="50px"  ID="Label17" runat="server" 
                                    Text='<%# Bind("CFCreatedOn", "{0:dd/MMM/yyyy}") %>'></asp:Label>
                            </ItemTemplate>
                            <EditItemTemplate>
                                <asp:TextBox  width="50px"  ID="TextBox17" runat="server" Text='<%# Bind("CFCreatedOn") %>'></asp:TextBox>
                            </EditItemTemplate>
                            
                        </asp:TemplateField>
                        <asp:TemplateField  HeaderStyle-Font-Bold="true" HeaderText="View More">
                            <ItemTemplate>
                                 <span style="cursor:pointer; text-decoration:underline;" onclick="newwindow('FeedbackCorp.aspx?Index=<%#Eval("CFId")%>')">View More</span>
                            </ItemTemplate>
                            <%--<EditItemTemplate>
                            <a href="FeedbackCorp.aspx?From=<%#CheckIn %>&To=<%=CheckOut.Text %>">View More</a>
                            </EditItemTemplate>--%>
                            
                        </asp:TemplateField>
                       
                    </Columns>
                    
                    
                         <PagerStyle HorizontalAlign="Left" CssClass="GridPager" />
                </asp:GridView></div>
    
    
    
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

