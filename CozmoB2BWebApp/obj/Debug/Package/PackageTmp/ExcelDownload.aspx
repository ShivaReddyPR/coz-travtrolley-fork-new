﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExcelDownload.aspx.cs" Inherits="CozmoB2BWebApp.ExcelDownload" %>

<!DOCTYPE html>

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo == null)
                { %>
            <a href="AbandonSession.aspx">Session Expired. Please Login again.</a>
            <%} %>
        </div>
    </form>
</body>
</html>
