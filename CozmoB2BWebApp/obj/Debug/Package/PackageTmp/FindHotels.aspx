﻿<%@ Page Title="Hotel Search" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="FindHotels.aspx.cs" Inherits="CozmoB2BWebApp.FindHotels" EnableEventValidation="false" ValidateRequest="false" Trace="false" TraceMode="SortByTime" %>
    <%@ MasterType VirtualPath="~/TransactionBE.master"   %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="CT.CMS" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="System.Data" %>

<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">

    <style type="text/css">
        .body_container {
            min-height: calc(100vh - 166px);
        }

        .title-style {
            display: none !important;
        }

        .latestupdate {
            position: absolute;
            z-index: 10;
            right: 0;
            left: 0;
            padding: 3px 5px 5px 5px;
            top: 0;
        }

        .latestupdate .alert.alert-notice {
            background-color: #fdd75b;
            border-color: #fdd75b;
            color: #20204a;
            padding: 7px 15px;
        }

        .latestupdate .alert-icon {
            color: #fa3535;
            float: left;
            display: block;
            margin-top: -4px;
            width: auto;
            height: 100%;
            text-align: center;
            font-size: 18px;
            font-weight: bolder;
        }

        .alert.alert-notice .alert-icon {
            background-color: #fed750;
        }

        .latestupdate .alert-message {
            padding-left: 20px;
            color: #000;
            display: block;
            padding-right: 40px;
            margin-left: 18px;
            font-weight: bolder;
        }

        .latestupdate .latest-update-title {
            color: #e40a5a;
            line-height: .5;
        }

        .disabled { color:lightgray; pointer-events:none; }       
    </style>
    
    <link href="build/css/owl-carousel/owl.carousel.min.css" rel="stylesheet" type="text/css">
    <link href="build/css/owl-carousel/owl.theme.default.min.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="css/fontawesome/css/font-awesome.min.css" rel="stylesheet" />

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
    <script src="build/js/owl.carousel.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts\jsBE\organictabs.jquery.js"></script>
     <style>
         .divcityName,.divcityPOI{
             transition:all .5s  ease;
             position:relative;
         }
          /*.divcityName.shadow:before,.divcityPOI.shadow:before{
              content:"";
              position:absolute;
                left: 7px;
                top: 0;
                width: 100%;
                height: 39px;
                -webkit-box-shadow: inset 66px 10px 71px -10px rgba(0,0,0,0.75);
            -moz-box-shadow: inset 66px 10px 71px -10px rgba(0,0,0,0.75);
             box-shadow: inset 66px 10px 71px -10px rgba(0,0,0,0.75);
             z-index:-1;
          }*/
         .or-separator{
             position:absolute;
            right: -8px;
            top: 10px;
            background:#444444;
            border-radius: 50%;
            padding: 2px;
            font-size: 11px;
            font-weight: bold;
            z-index: 100;
            color: #fff;
         }
     </style>
    <script type="text/javascript">

        $(document).ready(function () {
        
            $("#Additional-Link").click(function () {
                $("#Additional-Div").toggle();
            });

            //HOtelSEARCH/POI SWITCH            
            $('.divcityName').attr('class', 'col-md-8 divcityName');
            $('.divcityPOI').attr('class', 'col-md-4 divcityPOI');
            $('.divcityPOI .select-dropdown-holder').hide();
            $('.divcityPOI input').on('click focus', function () {
                $('.divcityName').attr('class', 'col-md-3 divcityName');
                $('.divcityPOI').attr('class', 'col-md-9 divcityPOI');
                $('.divcityPOI .select-dropdown-holder').show();
            })
            $('.divcityName input').on('click focus', function () {
                $('.divcityName').attr('class', 'col-md-8 divcityName');
                $('.divcityPOI').attr('class', 'col-md-4 divcityPOI');
                $('.divcityPOI .select-dropdown-holder').hide();
            })
            //END:HOtelSEARCH/POI SWITCH

            //is source is UAH then disable the city,nationality,room dropdown,childages
            <%if (hdnSelectedSources.Value.ToUpper().Contains(HotelBookingSource.UAH.ToString()))
            { %>

                document.getElementById('divcitynationality').style.display = 'none';
                document.getElementById('divcity').style.display = 'none';
                $('.divcityPOI').attr('class', 'col-md-12 divcityPOIOnly');
                $('.divcityPOIOnly .select-dropdown-holder').show();

                $('#roomCount').addClass('disabled');
                document.getElementById('chdRoom-1').style.display = 'none';
                document.getElementById('lblRm1Child').style.display = 'none';
            <%}%>
            BindSuppliers();
            if (!(ctl00_cphTransaction_hdnSelectedSources.value.includes('GIMMONIX')))
                $('#divChildSuppliers').hide();
        });
        function BindSuppliers() {
            var agentid = document.getElementById('<%=ddlAgents.ClientID%>') != null && (!document.getElementById('<%=ddlAgents.ClientID%>').selectedIndex <= 0) ? document.getElementById('<%=ddlAgents.ClientID%>').value :<%=Settings.LoginInfo.AgentId%>;
            var items = JSON.parse(AjaxCall("FindHotels.aspx/BindSuppliers",  "{'agentId':'" +agentid + "'}"));
            ActiveSource = items.Suppliers.length;
            printSuppliers(items.Suppliers);
            if (items.ChildSuppliers != null && items.ChildSuppliers.length > 0) {
                GimmonixSuppliersCount = items.ChildSuppliers.length;
                printChildSuppliers(items.ChildSuppliers);
                 <% if (Settings.LoginInfo.AgentType == AgentType.B2B || Settings.LoginInfo.AgentType == AgentType.B2B2B)
        { %>
            $('#suppContianer').hide();
            $('#childSuppContianer').hide();
            <%}%>
                $('#divChildSuppliers').show();
            }
        }
        function printSuppliers(data) {
            $('#dvCheckBoxListControl').empty();
            var table = $('<table width="100%" id="tblSources" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" border="0" cellspacing="0" cellpadding="0"></table>');
            var counter = 1;

            var allSupp = 0;// IsEmpty(request.SupplierIds) || request.SupplierIds.length == 0 || request.SupplierIds.length == data.length;

            table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({
                type: 'checkbox', checked: (allSupp), name: 'chklistitem', value: "0", id: 'chklistitem0', onclick: 'setCheck("chklistitem0")'
            })).append(
                $('<label>').attr({
                    for: 'chklistitem0'
                }).text("ALL")))
            );

            $(data).each(function () {

                table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({
                    type: 'checkbox', checked: (allSupp), name: 'chklistitem', value: this.Name, id: 'chklistitem' + counter, onclick: 'setCheck("chklistitem' + counter + '")'
                })).append(
                    $('<label>').attr({
                        for: 'chklistitem' + counter++
                    }).text(this.Name))));
            });
                 $('#dvCheckBoxListControl').append(table);
            findSelectedSources();
        }
        function findSelectedSources() {
            var MSource = $('#dvCheckBoxListControl').find('input[type="checkbox"]:checked').map(function () {
                return this.value;
            }).get().join(',');
            document.getElementById('<%=hdnSelectedSources.ClientID %>').value = MSource.replace(/^0,+/, '').trim();
            document.getElementById('<%=hdnActiveSources.ClientID %>').value = MSource.replace(/^0,+/, '').trim().split(',').length;
             
        }
        function printChildSuppliers(data) {
            $('#dvChkbChildListControl').empty();
            $('#divSuppliers').show();
            var table = $('<table width="100%" id="tblChildSources" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" border="0" cellspacing="0" cellpadding="0"></table>');
            var counter = 1;

            var allSupp = 0;//IsEmpty(request.SupplierIds);

            table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({
                type: 'checkbox', checked: allSupp, name: 'chkChdlistitem', value: "0", id: 'chkChdlistitem0', onclick: 'setCheck("chkChdlistitem0")'
            })).append(
                $('<label>').attr({
                    for: 'chkChdlistitem0'
                }).text("ALL")))
            );

            $(data).each(function () {

                table.append($('<tr></tr>').append($('<td></td>').append($('<input>').attr({

                    type: 'checkbox', checked: (allSupp), name: 'chkChdlistitem', value: this.gimmonixsuppid, id: 'chkChdlistitem' + counter, onclick: 'setCheck("chkChdlistitem' + counter + '")'
                })).append(
                    $('<label>').attr({
                        for: 'chkChdlistitem' + counter++
                    }).text(this.Name))));
            });

            $('#dvChkbChildListControl').append(table);
        }
        //PointOfInterst
        var autocomplete;
        var geolocation;
        function initAutocomplete() {

            // Create the autocomplete object, restricting the search predictions to
            // geographical location types.
            autocomplete = new google.maps.places.Autocomplete(
                          /** @type {HTMLInputElement} */(document.getElementById('PointOfInterest')),
                          { types: ['geocode'] });

            google.maps.event.addListener(autocomplete, 'place_changed', function () {

                var place = autocomplete.getPlace(); 
                var CityCountry = GetCountry(place) + '|' + GetCityNamePOI(place).split('|')[0];
               var countrCode=GetCountry(place).split('|')[1];
                var isUAE = (!IsEmpty(countrCode) && countrCode.toUpperCase() == "AE") ? true : false;
            $('#spanNationality').text(isUAE ? 'Select "United Arab Emirates" if guest is resident of UAE' : '');
                $('#<%=hdnPOICityName.ClientID %>').val(CityCountry);
                $('#<%=hdnLongtitude.ClientID %>').val(place.geometry.location.lng());
                $('#<%=hdnlatitude.ClientID %>').val(place.geometry.location.lat());
            });
        }

        function geolocate() {

            if (navigator.geolocation) {

                navigator.geolocation.getCurrentPosition(function(position) {
                    geolocation = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                 };

                    var circle = new google.maps.Circle(
                        {center: geolocation, radius: position.coords.accuracy});
                    autocomplete.setBounds(circle.getBounds());
                });
            }
        }

        function SetModifyValues() {

            var rooms = eval('<%=reqObj.NoOfRooms %>');
            var adults = '<%=adults %>';
            var childs = '<%=childs %>';

            var roomAdults = adults.split(',');
            var roomChilds = childs.split(',');
            var childAges = '<%=childAges %>';
            childAges = childAges.split(',');
        
            document.getElementById('roomCount').value = rooms;
            $('#roomCount').val(rooms);
            ShowRoomDetails();

            //document.getElementById('rating').value = '<%=((int)reqObj.Rating).ToString() %>';
            var age = eval(0);
            for (i = 1; i <= rooms; i++) {

                for (j = 0; j < roomAdults.length; j++) {

                    if (roomAdults[j].split('-')[0] == i) {

                        document.getElementById('adtRoom-' + i).value = roomAdults[j].split('-')[1];
                    }
                }

                for (k = 0; k < roomChilds.length; k++) {

                    if (roomChilds[k].split('-')[0] == i) {

                        document.getElementById('chdRoom-' + i).value = roomChilds[k].split('-')[1];
                        ShowChildAge(k + 1);
                        var chd = eval(roomChilds[k].split('-')[1]);
                        for (l = 0; l < chd; l++) {

                            document.getElementById('ChildBlock-' + (k + 1) + '-ChildAge-' + (l + 1)).value = childAges[age];
                            age++;
                        }
                    }
                }
            }
            hotelPaxCount();
        }
    </script>

    <script type="text/javascript">

        function hotelSourceValidate() {

            var active = eval(document.getElementById('<%=hdnActiveSources.ClientID %>').value);
            var counter = 0;
            if (('<%=Settings.LoginInfo.AgentType %>') == '<%=AgentType.B2B%>' || ('<%=Settings.LoginInfo.AgentType %>') == '<%=AgentType.B2B2B%>') {
                counter = 1;
            } else {
                counter = active;
            }

            if (counter == 0) {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select atleast 1 Supplier";
                return false;
            }
            return true;
        }

        function setCheck(ctrl) {

            var CheckLen = ctrl != 'chklistitem0' ? $('#dvCheckBoxListControl input[type=checkbox]:checked').length : 0;
            var CheckChdLen = ctrl != 'chkChdlistitem0' ? $('#dvChkbChildListControl input[type=checkbox]:checked').length : 0;           
            var selected = document.getElementById(ctrl).checked;

            if (ctrl == 'chklistitem0') {

                for (i = 1; i <= ActiveSource; i++) {

                    document.getElementById('chklistitem' + i).checked = selected;
                }
            }
            else if (ctrl == 'chkChdlistitem0') {
                for (i = 1; i <= GimmonixSuppliersCount; i++) {

                    document.getElementById('chkChdlistitem' + i).checked = selected;
                }
            }
            else {
                if (ctrl.includes('chklistitem'))
                    document.getElementById('chklistitem0').checked = selected && (CheckLen == ActiveSource)
                if (ctrl.includes('chkChdlistitem'))
                    document.getElementById('chkChdlistitem0').checked = selected && (CheckChdLen == GimmonixSuppliersCount)
            }
             hideChildSuppliers();
        }
        function hideChildSuppliers() {

            var isGimmonix = false;
            var $chkbox = $('#tblSources tr').find('input[type="checkbox"]');
            if ($chkbox.length) {
                for (var i = 1; i < $chkbox.length; i++) {
                    if (document.getElementById('chklistitem' + i).checked == true && $chkbox[i].value == "GIMMONIX") {
                        isGimmonix = true;
                    }
                }
            }
            if (isGimmonix) {
             
                $('#divChildSuppliers').show();
            } else {
                $('#divChildSuppliers').hide();
            }
        }
        function HotelSearch() {
            findSelectedSources();
            document.getElementById('container2').style.display = "none";

            if (('<%=Settings.LoginInfo.MemberType %>') == '<%=MemberType.ADMIN%>') {

                if (document.getElementById('<%=rbtnAgent.ClientID %>') != null && document.getElementById('<%=rbtnAgent.ClientID %>').checked) {

                    if (document.getElementById('<%=ddlAgents.ClientID %>').selectedIndex <= 0) {

                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Select Client from the list";
                        return false;
                    }
                }
            }

            if (('<%=Settings.LoginInfo.IsCorporate %>') == 'Y') {

                if (document.getElementById('<%=ddlFlightTravelReasons.ClientID %>') != null &&
                    document.getElementById('<%=ddlFlightTravelReasons.ClientID %>').selectedIndex <= 0) {
                    
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Travel Reason";
                    return false;                    
                }

                if (document.getElementById('<%=ddlFlightEmployee.ClientID %>') != null &&
                    document.getElementById('<%=ddlFlightEmployee.ClientID %>').selectedIndex == -1 || document.getElementById('<%=ddlFlightEmployee.ClientID %>').value=='Select Traveller' ) {
                    
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Traveler";
                    return false;                    
                }
            }

            if (CheckCity() && checkDates() && checkRooms()&& hotelSourceValidate() ) {

                if (document.getElementById('<%=rbtnAgent.ClientID %>') != null && document.getElementById('<%=rbtnAgent.ClientID %>').checked == true
                    && document.getElementById('<%=ddlAgents.ClientID %>').value != "-1"
                    && document.getElementById('<%=ddlHotelAgentLocations.ClientID %>').value != "-1") {
                    document.getElementById('<%=hdnAgentLocation.ClientID %>').value = document.getElementById('<%=ddlHotelAgentLocations.ClientID %>').value;

                }

                <% if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
        { %>
                var active = eval(document.getElementById('<%=hdnActiveSources.ClientID %>').value);
                var selected = document.getElementById('<%=hdnSelectedSources.ClientID %>').value + ',';
                var Supplierid = '';
                if (selected.includes('GIMMONIX')) {
                    var $chdchkbox = $('#tblChildSources tr').find('input[type="checkbox"]:checked');
                    if ($chdchkbox.length) {
                        for (var k = 0; k < $chdchkbox.length; k++) {
                            if($chdchkbox[k].value!='0')
                            Supplierid+=$chdchkbox[k].value+',';
                        }

                    }
                    selected += Supplierid;
                }
                document.getElementById('<%=hdnSelectedSources.ClientID %>').value = selected.slice(0, selected.lastIndexOf(','));
                <%}%>

                    $('#ctl00_upProgress').show();

                    //Detecting only IE Browser..
                    if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
                        document.getElementById('imgHotelLoading').innerHTML = document.getElementById('imgHotelLoading').innerHTML;
                    }
                    document.getElementById('<%=hdnSubmit.ClientID %>').value = "search"
                    document.forms[0].submit();
                }
                else {
                    return false;
                }
            
        }

        function CheckCity() {

            var submit = false;
            var city = document.getElementById('city');

            var countryArr = city.value.split(',');

            if (city.value != "Enter city name") {

                submit = true;
                document.getElementById('searchCity').innerHTML = city.value;
                if (countryArr[1].toUpperCase() == "UNITED ARAB EMIRATES") {
                    document.getElementById('isUAE').innerHTML = "Tourism Dirham Charge to be paid directly by the client to hotel before check out (AED 10- 20 per room per night). Applicable for Check in from 31st March onward..";
                }
                 
                if ($('#PointOfInterest').val() == "" && city.value != "Enter city name") {
                    $('#<%=hdnPOICityName.ClientID %>').val("");
                    $('#<%=hdnLongtitude.ClientID %>').val(0);
                    $('#<%=hdnlatitude.ClientID %>').val(0);
                }
                else {
                    $('#ctl00_cphTransaction_hdnRadius').val(($('#Radius').val()*1000));
                }
            }
            else {

                if ($('#PointOfInterest').val() == "" && city.value == "Enter city name") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter a City or Point of Interest";
                    submit = false;
                }
                else if ($('#PointOfInterest').val() != "" && city.value == "Enter city name") {

                    if ($('#Radius').val() == 0) {

                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please select Radius";
                        submit = false;
                    }
                    else {

                        document.getElementById('searchCity').innerHTML = $('#PointOfInterest').val();
                        if ($('#PointOfInterest').val().indexOf("UNITED ARAB EMIRATES") >= 0) {
                            document.getElementById('isUAE').innerHTML = "Tourism Dirham Charge to be paid directly by the client to hotel before check out (AED 10- 20 per room per night). Applicable for Check in from 31st March onward..";
                        }
                        $('#<%=hdnRadius.ClientID %>').val(($('#Radius').val()*1000));
                        submit = true;
                    }                   
                }                           
            }
            return submit;
        }

        function checkDates() {

            document.getElementById('errMess').style.display = "none";
            var date1 = document.getElementById('<%= CheckIn.ClientID %>').value;
            var date2 = document.getElementById('<%= CheckOut.ClientID %>').value;
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select Check In Date";
                return false;
            }
            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Check In Date";
                return false;
            }
            var cInDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            if (todaydate.getTime() > cInDate.getTime()) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Check In Date should be greater than equal to todays date";
                return false;
            }

            if (date2 != null && (date2 == "DD/MM/YYYY" || date2 == "")) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select Check Out Date";
                return false;
            }
            var retDateArray = date2.split('/');

            // checking if date2 is valid	
            if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Check Out Date";
                return false;
            }
            var cOutDate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
            if (todaydate.getTime() > cOutDate.getTime()) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Check Out Date should be greater than equal to todays date";
                return false;
            }

            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "CheckOut date should be greater than  or equal to CheckIn date";
                return false;
            }

            if (difference == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "CheckIn date and CheckOut date could not be same";
                return false;
            }

            var hdnASources = document.getElementById('<%=hdnSelectedSources.ClientID %>').value;
            var nationality = document.getElementById('<%=ddlNationality.ClientID %>');
            
            if ( hdnASources.indexOf('UAH') == -1 && nationality.options[nationality.selectedIndex].value == "Select Nationality"  ) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select your Nationality";
                return false;
            }

            document.getElementById('fromDate').innerHTML = date1;
            document.getElementById('toDate').innerHTML = date2;

            return true;
        }

        /* To bind Days Calculation between chekin and checkout Dates */
        function MinNights() {
             document.getElementById('container2').style.display = "none";
            var date1 = document.getElementById('<%=CheckIn.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = document.getElementById('<%=CheckOut.ClientID %>').value;
            if (date2.length == 0 || date2 == "DD/MM/YYYY") {

                //document.getElementById('errMess').style.display = "block";
                //document.getElementById('errorMessage').innerHTML = "Please select checkout date.";
                return false;
            }

            var depDateArray = date1.split('/');
            var ARDateArray = date2.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var checkInDt = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var checkoutDt = new Date(ARDateArray[2], ARDateArray[1] - 1, ARDateArray[0]);
            var difference = checkoutDt.getTime() - checkInDt.getTime();
            var MinNights = Math.ceil(difference / (1000 * 3600 * 24));

            if (MinNights < 1) {

             document.getElementById('errMess').style.display = "block";
             document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
             return false;
            }

            if (MinNights == 0) {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
                return false;
            }

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            document.getElementById('ctl00_cphTransaction_txtMinNights').value = MinNights;  
            if (eval(MinNights) != "" && eval(MinNights) > 0)
            {
                DateChange();
            }
        }

        /* To bind checkout date based on MinNights */
        function DateChange() {
            document.getElementById('container2').style.display = "none";
            var mn = document.getElementById('ctl00_cphTransaction_txtMinNights').value;

             var CheckIn = document.getElementById('<%=CheckIn.ClientID %>').value;            
             if (CheckIn != null && (CheckIn == "DD/MM/YYYY" || CheckIn == "")) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select Check In Date";
                return false;
             }
            if (mn != "" && mn > 0) {
                var date1 = cal1.getSelectedDates()[0];

                var arrMinDate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate() + eval(document.getElementById('ctl00_cphTransaction_txtMinNights').value));
                var month = arrMinDate.getMonth() + 1;
                var day = arrMinDate.getDate();

                if (month.toString().length == 1) {
                    month = "0" + month;
                }
                if (day.toString().length == 1) {
                    day = "0" + day;
                }
                
                cal2 = new YAHOO.widget.CalendarGroup("cal2", "container2");
                cal2.selectEvent.subscribe(setDate2);
                cal2.cfg.setProperty("close", true);                 
                document.getElementById('<%=CheckOut.ClientID %>').value = day + "/" + (month) + "/" + arrMinDate.getFullYear();
                cal2.cfg.setProperty("selected", (month) + "/" + day + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("minDate", (month) + "/" + day + "/" + arrMinDate.getFullYear());                  
                cal2.render(); 
            }
            else {
                alert("No Of Nights not allowed Zero or empty");
            }
        }

        function allowNumerics(evt) {
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode != 46 && charCode > 31
                        && (charCode < 48 || charCode > 57))
                        return false;

                    return true;
                }

        function checkRooms() {

            var submit = true;
            var rooms = document.getElementById('roomCount');
            var room = parseInt(rooms.options[rooms.selectedIndex].value);

            if (room > 0) {

                for (var k = 1; k <= room; k++) {

                    if (submit == false) {
                        break;
                    }
                    if (document.getElementById("adtRoom-" + k).value == 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Enter Room " + k + "Details!!";
                        submit = false;
                    }

                    if (document.getElementById("chdRoom-" + k).value > 0) {

                        for (var j = 1; j <= document.getElementById("chdRoom-" + k).value; j++) {

                            var str = "ChildBlock-" + k + "-ChildAge-" + j;
                            if (document.getElementById(str).value < 0) {
                                document.getElementById('errMess').style.display = "block";
                                document.getElementById('errorMessage').innerHTML = "Please Enter Child-" + (parseInt(j)) + "  Age Details in Room " + (parseInt(k)) + "!!";
                                submit = false;
                                break;
                            }

                        }
                    }
                }
            }
            else {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select atleast 1 room";
                submit = false;
            }
            return submit;
        }   

        function CheckCorporateEntries() {

            document.getElementById('errMulti').style.display = "none";
            document.getElementById('errMess').style.display = "none"

            if (document.getElementById('<%=ddlFlightTravelReasons.ClientID %>') != null && document.getElementById('<%=ddlFlightTravelReasons.ClientID %>').value == "-1") {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Travel Reason";
                document.getElementById('<%=ddlFlightTravelReasons.ClientID %>').focus();
                return false;
            }
            else if (document.getElementById('<%=ddlFlightEmployee.ClientID %>') != null && document.getElementById('<%=ddlFlightEmployee.ClientID %>').value == "Select Traveller") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select Traveller / Employee";
                document.getElementById('<%=ddlFlightEmployee.ClientID %>').focus();
                return false;
            }
        }

        var cal1;
        var cal2;

        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar
            cal1 = new YAHOO.widget.CalendarGroup("cal1", "container1");
            //cal1 = new YAHOO.widget.Calendar("cal1", "Outcontainer1");
            cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            //            cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDate1);
            cal1.render();

            cal2 = new YAHOO.widget.CalendarGroup("cal2", "container2");
            //            cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDate2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }

        function showCalendar1() {
        
            if (cal2 != null) cal2.hide();
            document.getElementById('container1').style.display = "block";
            document.getElementById('Outcontainer1').style.display = "block";
            document.getElementById('Outcontainer2').style.display = "none";
        }

        var departureDate = new Date();

        function showCalendar2() {

            $('container1').context.styleSheets[0].display = "none";
            document.getElementById('Outcontainer1').style.display = "none";
            document.getElementById('Outcontainer2').style.display = "block";
            if (cal1 != null) cal1.hide();
        
            var date1 = document.getElementById('<%= CheckIn.ClientID%>').value;
        
            if (date1.length != 0 && date1 != "DD/MM/YYYY") {

                var depDateArray = date1.split('/');
                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);
                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }

        function setDate1() {

            var date1 = cal1.getSelectedDates()[0];

            //document.getElementById('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                return false;
            }
            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= CheckIn.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
                
            cal1.hide();
            document.getElementById('Outcontainer1').style.display = "none";
        
            showCalendar2();
        }

        function setDate2() {

            var date1 = document.getElementById('<%=CheckIn.ClientID %>').value;

            if (date1.length == 0 || date1 == "DD/MM/YYYY") {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 1) {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
                return false;
            }

            if (difference == 0) {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
                return false;
            }

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=CheckOut.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
            document.getElementById('Outcontainer2').style.display = "none";
        }
        YAHOO.util.Event.addListener(window, "load", init);
    
        function getStates_Hotel(sQuery) {

            var paramList = 'searchKey=' + sQuery;

            if (document.getElementById('domesticHotel').checked == true) {

                //paramList += '&requestSource=' + "HotelSearchDomestic";
                paramList += '&requestSource=' + "HotelSearchDomesticForGimmonix";//Added by Harish 
            }
            else {
                paramList += '&requestSource=' + "HotelSearchInternational";
            }

            var url = "CityAjax";
            var arrayStates = "";
            var faltoo = getFile(url, paramList);
            arrayStates = faltoo.split('/');

            if (arrayStates[0] != "") {

                for (var i = 0; i < arrayStates.length; i++) {

                    {
                        arrayStates[i] = [arrayStates[i], arrayStates[i]];

                    }
                    return arrayStates;
                }
            }
            else return (false);
        }

        //for added helper in source destination lookup
        function autoCompInitHotelCity() {

            // Instantiate third data source        
            oACDSHC = new YAHOO.widget.DS_JSFunction(getStates_Hotel); // temporarily autosearch deactivated
            // Instantiate third auto complete        
            oAutoCompHC = new YAHOO.widget.AutoComplete('city', 'statescontainer3', oACDSHC);
            oAutoCompHC.prehighlightClassName = "yui-ac-prehighlight";
            oAutoCompHC.queryDelay = 0;
            oAutoCompHC.minQueryLength = 3;
            oAutoCompHC.useIFrame = true;
            oAutoCompHC.useShadow = true;

            oAutoCompHC.formatResult = function (oResultItem, sQuery) {
                document.getElementById('statescontainer3').style.display = "block";
                var toShow;
                if (oResultItem[1] != null && oResultItem[1].length > 1) {
                    toShow = oResultItem[1].split(',');
                }
                else {
                        toShow = oResultItem.split(',');
                }
                
                var sMarkup;
                     
                sMarkup = toShow.join(',');
            
                var aMarkup = [sMarkup];
                return (aMarkup.join(""));
            };
            oAutoCompHC.itemSelectEvent.subscribe(itemSelectHandlerHC);
        }

        var itemSelectHandlerHC = function (sType2, aArgs2) {

            YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
            var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
            if (oMyAcInstance2[1] != null && oMyAcInstance2[1].length > 1) {
                var city = oMyAcInstance2[1].split(',');
            }
            else {
                var city = oMyAcInstance2.split(',');
            }

            //need to check city having multiple names then added with camma seperated to city
            if (city.length > 2) {
                document.getElementById('CountryName').value = city[city.length - 1];
            }
            else {
                document.getElementById('CountryName').value = city[1];
            }
            var isUAE = (!IsEmpty($('#CountryName').val()) && $('#CountryName').val().toUpperCase() == "UNITED ARAB EMIRATES") ? true : false;
            $('#spanNationality').text(isUAE ? 'Select "United Arab Emirates" if guest is resident of UAE' : '');

            document.getElementById('city').value = city.join(',');
            document.getElementById('statescontainer3').style.display = "none";
            var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
            var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource 
        };
        YAHOO.util.Event.addListener(this, 'load', autoCompInitHotelCity); //temporarily commented
    </script>

    <script type="text/javascript">
    
        function setRadioButton(radiobutton) {

            document.getElementById(radiobutton).checked = true;
            return;
        }

        function ShowRoomDetails() {
        
            var rooms = document.getElementById('roomCount');

            if (rooms != null) {

                var count = eval(rooms.options[rooms.selectedIndex].value);

                if (document.getElementById('PrevNoOfRooms') != null) {

                    var prevCount = eval(document.getElementById('PrevNoOfRooms').value);

                    if (count > prevCount) {

                        for (var i = (prevCount + 1); i <= count; i++) {

                            document.getElementById('room-' + i).style.display = 'flex';
                            document.getElementById('adtRoom-' + i).value = '1';
                            document.getElementById('chdRoom-' + i).value = '0';
                            document.getElementById('PrevChildCount-' + i).value = '0';
                        }
                    }
                    else if (count < prevCount) {

                        for (var i = prevCount; i > count; i--) {

                            document.getElementById('room-' + i).style.display = 'none';
                            document.getElementById('adtRoom-' + i).value = '1';                           
                            var childcount = document.getElementById('chdRoom-' + i).value
                            document.getElementById('ChildBlock-' + i).style.display = 'none';
                            for (var j = 1; j <= childcount; j++) {
                                document.getElementById('ChildBlock-' + i + '-ChildAge-' + j).value = '-1';
                                document.getElementById('ChildBlock-' + i + '-ChildAge-' + j).style.display = 'none';
                                document.getElementById('ChildBlock-' + i + '-Child-' + j).style.display = 'none';
                            }
                            document.getElementById('chdRoom-' + i).value = '0';
                        }
                    }
                }
                document.getElementById('PrevNoOfRooms').value = count;
            }
        }

        /* function to show number of childrens */
        function ShowChildAge(number) {

            var childCount = eval(document.getElementById('chdRoom-' + number).value);
            var PrevChildCount = eval(document.getElementById('PrevChildCount-' + number).value);

            if (eval(document.getElementById('chdRoom-1').value) > 0 || eval(document.getElementById('chdRoom-2').value) > 0 || eval(document.getElementById('chdRoom-3').value) > 0 || eval(document.getElementById('chdRoom-4').value) > 0) {
                document.getElementById('childDetails').style.display = 'block';
            }
            else {
                document.getElementById('childDetails').style.display = 'none';
            }

            if (childCount > PrevChildCount) {

                document.getElementById('ChildBlock-' + number).style.display = 'block';
                for (var i = (PrevChildCount + 1); i <= childCount; i++) {

                    document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'block';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).style.display = 'block';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                    $('#ChildBlock-' + number + '-ChildAge-' + i).select2('destroy');
                }
            }
            else if (childCount < PrevChildCount) {

                if (childCount == 0) {

                    document.getElementById('ChildBlock-' + number).style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-1').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-2').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-3').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-4').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-5').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-ChildAge-6').value = '-1';
                    document.getElementById('ChildBlock-' + number + '-Child-1').style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-Child-2').style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-Child-3').style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-Child-4').style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-Child-5').style.display = 'none';
                    document.getElementById('ChildBlock-' + number + '-Child-6').style.display = 'none';
                }
                else {

                    for (var i = PrevChildCount; i > childCount; i--) {

                        if (i != 0) {

                            document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'none';
                            document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                        }
                    }
                }
            }
            document.getElementById('PrevChildCount-' + number).value = childCount;
        }

        function childInfo(id) {

            var index = parseInt(id);
            var se = $("selectChild-" + index);

            if (parseInt(se.value) > 0) {

                var str = "";

                str += "  <div style='float:left; width:200px;' id='roomChild" + index + "'>";
                for (var i = 0; i < se.value; i++) {

                    str += "<span style='float:left; width:95px; padding-bottom:3px;'>";
                    str += "  <label style='float:left; width:40px; padding-top:4px; font-size:11px;'> Child " + (parseInt(i) + 1) + "</label>";
                    str += "         <em style='float:left;'>";
                    str += "            <select style='width:50px;font-size:10px;font-family:Arial' class='room_guests' name='child" + index + (parseInt(i) + 1) + "' id='child" + index + (parseInt(i) + 1) + "'>";
                    str += "                    <option selected='selected' value='-1'>Age?</option>";

                    str += "                    <option value='0'><1</option>";
                    str += "                      <option value='1' >1</option>";
                    str += "                      <option value='2'>2</option>";
                    str += "                    <option value='3'>3</option>";
                    str += "                        <option value='4'>4</option>";
                    str += "                        <option value='5'>5</option>";
                    str += "                        <option value='6'>6</option>";
                    str += "                     <option value='7'>7</option> ";
                    str += "                     <option value='8'>8</option>";
                    str += "                      <option value='9'>9</option>"
                    str += "                      <option value='10'>10</option>";
                    str += "                      <option value='11'>11</option>";
                    str += "                   <option value='12'>12</option>";
                    str += "                    <option value='13'>13</option> ";
                    str += "                      <option value='14'>14</option>";
                    str += "                      <option value='15'>15</option>";
                    str += "                     <option value='16'>16</option> ";
                    str += "                    <option value='17'>17</option>";
                    str += "                      <option value='18'>18</option>";

                    str += "               </select>";
                    str += "       </em>";
                    str += "        </span>";
                }
                str += "  </div>";

                $("childInfo-" + index).innerHTML = str;
                $("childInfo-" + index).style.display = "block";
            }
            else {
                $("roomChild" + index).style.display = "none";

            }
        }

        function roomInfo(id) {

            if (parseInt(id) == 0) {
                var se = $('roomCount');
            }
            else {
                var se = $('norooms');
            }

            if (parseInt(se.value) > 1) {
                $('guestsInfo').style.display = "none";
            }
            else {
                $('guestsInfo').style.display = "none";
                return;
            }

            var str = "";

            for (var i = 2; i <= se.value; i++) {
                str += "   <div style='float:left; width:220px;' id='noOfPax-" + i + "'>";
                str += "<b style='float:left; padding:8px 5px 0 0;'>Room " + parseInt(i) + "</b>";
                str += "      <div class='no_of_guests'>";
                str += "     <select class='guests'  name='adultCount-" + parseInt(i) + "' id='adultCount-" + parseInt(i) + "'>";
                str += "               <option selected='selected' value='1' >1</option>";
                str += "               <option value='2' >2</option>";
                str += "               <option value='3'>3</option>";
                str += "               <option value='4'>4</option>";
                str += "        </select>";
                str += "       <span>Adults</span>";
                str += "       <b>(18+ yrs)</b>";
                str += "   </div>";
                str += "   <div class='no_of_guests' >";
                str += "       <select class='guests' id='selectChild-" + parseInt(i) + "' name='childCount-" + parseInt(i) + "' onchange='javascript:childInfo(" + parseInt(i) + ")'>";
                str += "               <option selected='selected' value='0'>none</option>";
                str += "               <option value='1'>1</option>";
                str += "               <option value='2'>2</option>";
                str += "           </select>";
                str += "       <span>Children</span>";
                str += "       <b>(Till 18 yrs)</b>";
                str += "   </div>";
                str += "   </div>";
                str += "<div id='childInfo-" + i + "' style='display:none'></div>"
            }

            $('guestsInfo').innerHTML = str;
            $('guestsInfo').style.display = "block";
        }
    </script>

    <script type="text/javascript">
    
        function disablefield() {
        
            if ('<%=userType %>' == 'ADMIN') {

                if (document.getElementById('<%=rbtnAgent.ClientID %>') != null) {

                    if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true) {

                        document.getElementById('wrapper').style.display = "block";
                        document.getElementById('divHotelLocations').style.display = "block";
                    }
                    else {
                        if (document.getElementById('wrapper') != null) {
                            document.getElementById('wrapper').style.display = "none";
                        }
                        document.getElementById('divHotelLocations').style.display = "none";
                    }
                }
            }
        
            document.getElementById('<%=hdnIsCrossPostBack.ClientID%>').value = 'false';
        }
    </script>

    <script type="text/javascript">
        $(function () {

            $("#example-one").organicTabs();

            $("#tabbed-menu").organicTabs({
                "speed": 200
            });

        });
    </script>

    <script type="text/javascript">

        var arrayStates = new Array();

        var Ajax;
        var loc; 
        function LoadAgentLocations(id, type) {

            if ($('#<%=ddlAgents.ClientID %>').val() != "-1") {
                BindSuppliers();
           loc = 'ctl00_cphTransaction_ddlHotelAgentLocations';
           var paramList = 'requestSource=getAgentsLocationsByAgentId' + '&AgentId=' + document.getElementById('<%=ddlAgents.ClientID %>').value + '&id=' + loc;
                var url = "CityAjax";

                if (window.XMLHttpRequest) {
                    Ajax = new XMLHttpRequest();
                }
                else {
                    Ajax = new ActiveXObject("Microsoft.XMLHTTP");
                }
                Ajax.onreadystatechange = BindLocationsList;
                Ajax.open('POST', url);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);
            }
        }
        function BindLocationsList() {

            var ddl = document.getElementById(loc);

            if (Ajax.readyState == 4) {

                if (Ajax.status == 200) {

                    if (Ajax.responseText.length > 0) {

                        if (ddl != null) {

                            ddl.options.length = 0;
                            var el = document.createElement("option");
                            el.textContent = "Select Location";
                            el.value = "-1";
                            ddl.add(el, 0);
                        
                            var values = Ajax.responseText.split('#')[1].split(',');

                            for (var i = 0; i < values.length; i++) {

                                var opt = values[i];
                                if (opt.length > 0 && opt.indexOf('|') > 0) {

                                    var el = document.createElement("option");
                                    el.textContent = opt.split('|')[0];
                                    el.value = opt.split('|')[1];
                                    ddl.appendChild(el);
                                }
                            }
                                              
                            Setddlval(ddl.id, '<%=Settings.LoginInfo.OnBehalfAgentLocation.ToString()%>', '');                
                        }
                    }
                    else {

                        if (ddl != null) {

                            ddl.options.length = 0;
                            var el = document.createElement("option");
                            el.textContent = "Select Location";
                            el.value = "-1";
                            ddl.add(el, 0);
                            $('#' + ddl.id).select2('val', '-1');//Select Location
                        }
                    }
                }
            }
        }

        //Added by somasekhar on 28/09/2018
        // Get Agent County Code Based on AgentId Location Id
        //Used in HotelSearch.aspx page. To set Default Nationality and Residence India for IN county code
        function LoadAgentCountryCode() {

            var location = document.getElementById('ctl00_cphTransaction_ddlHotelAgentLocations').value;
        
            if (location != "") {
                var paramList = 'requestSource=getAgentsCountryByLocId' + '&LocationId=' + location;
                var url = "CityAjax";
                if (window.XMLHttpRequest) {
                    Ajax = new XMLHttpRequest();
                }
                else {
                    Ajax = new ActiveXObject("Microsoft.XMLHTTP");
                }
                Ajax.onreadystatechange = BindLocationCountry;
                Ajax.open('POST', url);
                Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
                Ajax.send(paramList);
            }
        }

        function BindLocationCountry(response) {

            $('#' + getElement('ddlNationality').id).select2('val', "Select Nationality");
            //$('#' + getElement('ddlResidence').id).select2('val', "Select Country");
            if (Ajax.readyState == 4 && Ajax.status == 200 && Ajax.responseText.length > 0) {
                var conuntryCode = Ajax.responseText;
                if (conuntryCode != "") {
                    if (conuntryCode.split('|')[0] == "IN") {
                        $('#' + getElement('ddlNationality').id).select2('val', conuntryCode.split('|')[1]);
                        //$('#' + getElement('ddlResidence').id).select2('val', conuntryCode.split('|')[1]);

                        //$('#' + getElement('ddlNationality').id).select2('val', '20');
                        //$('#' + getElement('ddlResidence').id).select2('val', '20');
                    }
                }

            }
        }
       
    </script>

    <span style="display:none;">
        <dfn>
            <input type="radio" id="domesticHotel" name="hotelSearchArea" onclick="javascript: IntDomChecked()" checked="checked" value="1" />
        </dfn>
        <em>Domestic Search</em> 
    </span>
    <span style="display:none;">
        <dfn>
            <input type="radio" id="intlHotel" name="hotelSearchArea" onclick="javascript: IntDomChecked()" value="0" />
        </dfn> 
        <em>International</em>
    </span>

    <asp:HiddenField ID="hdnSubmit" runat="server" />
    <asp:HiddenField ID="hdnActiveSources" runat="server"  />
    <asp:HiddenField ID="hdnSelectedSources" runat="server"  />
    <asp:HiddenField ID="hdnMultiRows" runat="server" Value="0" />
    <asp:HiddenField ID="hdnIsCrossPostBack" runat="server" Value="false" />
    <asp:HiddenField ID="hdnAllowFlightSearch" runat="server" Value="true" />
    <asp:HiddenField ID="hdnAgentLocation" runat="server" />    
    <asp:HiddenField ID="hdfSupplierCount" runat="server" Value="0"/>   
    <div id="CitySearchPop" class="light-grey-bg border-y padding-bottom-10 search-popup-parent" style="display: none; width: 410px; position: absolute; left: 500px; top: 110px;z-index: 100">
        <div id="LoadingCountryList" style="display: none; font-weight: bolder; margin-left: 105px;
            padding-top: 10px">
            Loading
            <img style="width: 80px" src="../images/ajax-loader2.gif" />
        </div>    
    </div>
    
    <input type="hidden" id="OfferedFareHidden" value="" />
    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0"></iframe>

    <div id="SearchResultLoad" style="display: none; margin-left: 5px; margin-top: -20px;">
        <a id="positionForValidation"></a>
        <img id="Img2" src="../images/indicator.gif" alt="up-arrow" />
        Loading Search Result....
    </div>
    
    <div id="SearchResultBlock"></div>
    <div id="MainDiv" onload="SetModifyValues()"></div>
    <div class="error_module"></div>

    <asp:HiddenField ID="hdnAdults" runat="server"  />
    <asp:HiddenField ID="hdnChilds" runat="server"  />
    <asp:HiddenField ID="hdnChildAges" runat="server"  />
    <asp:HiddenField ID="hdnRating" runat="server"  />
    <input type="hidden" id="hRooms" name="hRooms" value="<%=reqObj.NoOfRooms %>" />

    <%-- left panel startes here--%>           
    <div class="banner-container" style="background-image:url(build/img/main-bg.jpg)">
        <div class="row no-gutter">
            <div class="col-lg-7 col-xl-6">
                <div id="tabbed-menu" class="search-container">            
                    <ul class="tabbed-menu nav">                                             
                        <li>
                             <a href="javascript:void(0);" class="current">Hotel</a>
                        </li>                                                             
                    </ul>                 
                    <div id="errMess" class="error_module" style="display: none;">
                        <div id="errorMessage" class="alert-messages alert-caution"></div>
                    </div>                       
                    <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>                
                    <div class="search-matrix-wrap p-4 search-page-form hotels" data-tab-content="source=Hotel"  id="featured2" >
                        <%if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER || Settings.LoginInfo.IsCorporate == "Y") { %>                              
                        <div class="custom-radio-table row mb-3"  id="tblAgent">                               
                            <div class="col-md-3 d-none">
                                <div class="form-group">
                                    <asp:RadioButton ID="rbtnSelf" Visible="false" runat="server" GroupName="book" onclick="disablefield();"
                                    Checked="true" Font-Bold="True" Text="For Self" />
                                       
                                </div>
                            </div>
                            <div class="col-12">
                                <div class="row custom-gutter">
                                    <%if (Settings.LoginInfo.IsCorporate != "Y") { %>
                                    <div class="col-md-12"> 
                                        <div class="agent-checkbox-wrap float-left">      
                                                <asp:CheckBox ID="rbtnAgent"  Text="" runat="server" onclick="disablefield();"/>    
                                                <label class="tgl-btn" for="ctl00_cphTransaction_rbtnAgent"><em></em></label>
                                        </div> 
                                    </div>
                                    <%}%>
                                    <div class="col-md-6" id="wrapper" style="display:none">
                                        <asp:DropDownList CssClass="form-control" ID="ddlAgents" runat="server" onchange="LoadAgentLocations(this.id,'H')">
                                                </asp:DropDownList>
                                    </div>                               
                                    <div id="divHotelLocations" class="col-md-6" style="display:none;">
                                        <asp:DropDownList ID="ddlHotelAgentLocations" CssClass="form-control" runat="server" onchange="LoadAgentCountryCode()"></asp:DropDownList>                           
                                    </div>
                                    <%if (Settings.LoginInfo.IsCorporate == "Y") { %>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">
                                                
                                            <asp:DropDownList ID="ddlFlightTravelReasons" AppendDataBoundItems="true" CssClass="form-control select-element"  runat="server">
                                            <asp:ListItem Selected="True" Value="-1" Text="Select Reason"></asp:ListItem>
                                            </asp:DropDownList>
                                        </div>                                  
                                    </div>
                                    <div class="col-xs-12 col-md-6">
                                        <div class="form-group">                                                
                                            <asp:DropDownList ID="ddlFlightEmployee" CssClass="form-control select-element"  runat="server">
                                            </asp:DropDownList>
                                        </div>                                  
                                    </div>
                            <%} %>                                   
                                   
                                </div>
                            </div>                                                           
                        </div>                                   
                        <%} %>
                        <div class="row custom-gutter">
                            <div class="col-md-6 divcityName" id="divcity" style="display:block">
                                <div class="form-control-holder">
                                    <div class="icon-holder">
                                        <span class="icon-location" aria-label=""></span>
                                    </div>                                       
                                    <asp:DropDownList ID="ddlCity" CssClass="form-control" runat="server" AppendDataBoundItems="True" 
                                        Visible="false">
                                        <asp:ListItem Selected="True">Select City</asp:ListItem>
                                    </asp:DropDownList>
                                    <input class="form-control" type="text" id="city" name="city" value="<%=cityName%>" onblur="markout(this, '<%=cityName%>')" onfocus="markin(this, '<%=cityName%>')" onclick="IntDom('statescontainer3', 'city')" />
                                    <input type="hidden" id="CityCode" name="CityCode" value="<%=reqObj.CityId %>" />
                                    <input type="hidden" id="CountryName" name="CountryName" value="" />                    
                                    <input type="hidden" name="destinationCity" id="destinationCity" />
                                    <input type="hidden" name="isDomesticHotel" id="isDomesticHotel" />                     
                                    <div id="statescontainer3" style="width:95%; line-height:30px; color:#000; position:absolute; display:none; z-index: 103;    top: 38px;" ></div>    
                                </div>
                                <span class="or-separator">OR</span>
                            </div>     
                            <div class="col-md-6 divcityPOI">
                                <div class="form-control-holder">
                                    <div class="icon-holder">
                                        <span class="icon-map-marked-alt" aria-label=""></span>
                                    </div> 
                                    <input type="text" class="form-control" placeholder="Point of Interest" id="PointOfInterest" onFocus="geolocate()" name="location" />
                                    <asp:HiddenField ID="hdnLongtitude" value="0" runat="server" />
                                    <asp:HiddenField ID="hdnlatitude" value="0" runat="server" />
                                    <asp:HiddenField ID="hdnRadius" value="0" runat="server" />
                                    <asp:HiddenField ID="hdnPOICityName" value="0" runat="server" />

                                    <div class="select-dropdown-holder inputTypeAddkm-wrap ml-2">         
                                        <input type="number" class="form-control small inputTypeAddkm" id="Radius"  value="75"/>                                      
                                    </div>
                                </div>
                            </div>
                                                
                        </div>
                        <div class="row custom-gutter">
                         
                        </div>
                        <div class="row custom-gutter">
                            <div class="col-md-3">
                                <a class="form-control-holder"  href="javascript:void(null)" onclick="showCalendar1()">
                                    <div class="icon-holder">
                                        <span class="icon-calendar" aria-label=""></span>
                                    </div>    
                                    <asp:TextBox Class="form-control" placeholder="Check In" ID="CheckIn" autocomplete="off" data-calendar-contentID="#container1" runat="server"></asp:TextBox>                                                  
                                </a>
                                <div id="Outcontainer1" style="display: none" > <div id="container1" onclick="MinNights();"> </div> </div>
                            </div>

                            <div class="col-md-3">
                                <div class="form-control-holder">
                                    <div class="icon-holder">
                                        <span class="icon-calendar" aria-label=""></span>
                                    </div>
                                    <asp:TextBox placeholder="No Of Nights" CssClass="form-control" onkeypress="return allowNumerics(event);" ID="txtMinNights" runat="server" onKeyUp="DateChange();"></asp:TextBox>                           
                                </div>             
                            </div> 

                            <div class="col-md-3">
                                <a class="form-control-holder" href="javascript:void(null)" onclick="showCalendar2()">
                                    <div class="icon-holder">
                                        <span class="icon-calendar" aria-label=""></span>
                                    </div>   
                                    <asp:TextBox Class="form-control" placeholder="Check Out" ID="CheckOut" autocomplete="off" data-calendar-contentID="#container2" runat="server"></asp:TextBox>  
                                </a>
                                <div id="Outcontainer2" style="display: none;" ><div id="container2" onclick="MinNights();"> </div></div>        
                            </div> 
                            
                            <div class="col-md-6" id="divcitynationality">       
                                <div class="form-control-holder">
                                    <div class="icon-holder">
                                        <span class="icon-passport" aria-label="Select Nationality"></span>
                                    </div>     
                                    <asp:DropDownList CssClass="form-control"   ID="ddlNationality" runat="server"
                                        DataTextField="CountryName" DataValueField="CountryCode"  
                                        AppendDataBoundItems="True">
                                        <asp:ListItem>Select Nationality</asp:ListItem>
                                    </asp:DropDownList>                                                 
                                </div>
                                <span id="spanNationality" style="color:aliceblue;font-weight:bold"></span>
                            </div>      
                        </div>              
                        <div class="row custom-gutter">
                            <div class="col-md-5">
                                <div class="custom-dropdown-wrapper">
                                    <a href="jaavscript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#hotelRoomSelectDropDown">
                                        <div class="icon-holder">
 	                                            <span class="icon-group"></span>
                                        </div>
                                        <span class="form-control-text" id="htlTotalPax">1 Room, 1 Adult</span>               
                                    </a>
                                    <div class="dropdown-content d-none p-4 hotel-pax-dropdown" id="hotelRoomSelectDropDown">     
                                        <div class="row custom-gutter">
                                            <div class="col-4">
                                                ROOM 
                                            </div>
                                            <div class="col-6">
                                                <input type="hidden" value="1" name="PrevNoOfRooms" id="PrevNoOfRooms" />
                                                <div class="form-control-holder">
                                                    <select class="form-control small no-select2 htl-room-select" id="roomCount" name="roomCount" onchange="ShowRoomDetails();">
                                                        <option selected="selected">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <%--<option value="5">5</option>
                                                        <option value="6">6</option>--%>
                                                    </select>
                                                </div>  
                                            </div>
                                        </div>
                                        <div class="row custom-gutter room-wrapper" id="room-1">                                           
                                            <div class="col-12"> <label class="room-label">Room 1</label></div>
                                                <div class="col-4">
                                                    <div class="form-group">  
                                                        <label>Adults</label> 
                                                        <select class="form-control no-select2 adult-pax" name="adtRoom-1" id="adtRoom-1">
                                                            <option selected="selected" value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                        </select>
                                                        <input type="hidden" value="0" id="PrevChildCount-1" name="PrevChildCount-1" />  
                                                    </div>
                                                </div>
                                                <div class="col-4">
                                                    <div class="form-group">                                                     
                                                        <label id="lblRm1Child"> Child(2-12 yrs)</label> 
                                                        <select onchange="javascript:ShowChildAge('1')" name="chdRoom-1" 
                                                            id="chdRoom-1" class="form-control no-select2  child-pax">
                                                            <option selected="selected" value="0">none</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                        </select>
                                                    </div>                                                
                                                </div>
                                            <div class="col-12" id="ChildBlock-1" name="ChildBlock-1" style="display: none; width: 100%;">
                                                <div class="row no-gutters child-age-wrapper">                                                     
                                                    <div class="col-4" id="ChildBlock-1-Child-1" name="ChildBlock-1-Child-1" style="display: none;">
                                                        <div class="form-group">                                                     
                                                            <label>Child 1</label> 
                                                            <select class="form-control" id="ChildBlock-1-ChildAge-1" name="ChildBlock-1-ChildAge-1">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option> 
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-2" name="ChildBlock-1-Child-2" style="display: none;">
                                                        <div class="form-group">                                                     
                                                            <label>Child 2</label> 
                                                            <select class="form-control" id="ChildBlock-1-ChildAge-2" name="ChildBlock-1-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-3" name="ChildBlock-1-Child-3" style="display: none;">
                                                        <div class="form-group">
                                                            <label>Child 3</label> 
                                                            <select class="form-control" id="ChildBlock-1-ChildAge-3" name="ChildBlock-1-ChildAge-3"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-4" name="ChildBlock-1-Child-4" style="display: none;">
                                                        <div class="form-group">
                                                            <label>Child 4</label> 
                                                            <select class="form-control" id="ChildBlock-1-ChildAge-4" name="ChildBlock-1-ChildAge-4"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>                                                           
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-5" name="ChildBlock-1-Child-5" style="display: none;" >
                                                        <div class="form-group">
                                                            <label>Child 5</label> 
                                                            <select class="form-control" id="ChildBlock-1-ChildAge-5" name="ChildBlock-1-ChildAge-5"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-1-Child-6" name="ChildBlock-1-Child-6" style="display: none;">
                                                        <div class="form-group">
                                                            <label>Child 6</label> 
                                                            <select class="form-control" id="ChildBlock-1-ChildAge-6" name="ChildBlock-1-ChildAge-6"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-12" id="childDetails" style="display: none;"></div>
                                                </div>
                                            </div>
                                        </div>    
                                        <div class="row custom-gutter room-wrapper" id="room-2" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 2</label></div>
                                            <div class="col-4">
                                                <div class="form-group">  
                                                    <label>Adults</label> 
                                                    <select name="adtRoom-2" id="adtRoom-2" class="form-control no-select2 adult-pax">
                                                        <option selected="selected" value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                    </select>
                                                    <input type="hidden" value="0" id="PrevChildCount-2" name="PrevChildCount-2" />
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label> Child (2-12 yrs)</label> 
                                                    <select onchange="javascript:ShowChildAge('2')" name="chdRoom-2" id="chdRoom-2" class="form-control no-select2  child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                    </select>     
                                                </div>
                                            </div>
                                            <div class="col-12"  id="ChildBlock-2" name="ChildBlock-2" style="display: none;">
                                                <div class="row no-gutters child-age-wrapper">  
                                                <div class="col-4"  id="ChildBlock-2-Child-1" name="ChildBlock-2-Child-1">
                                                    <div class="form-group">  
                                                    <label>Child 1</label>
                                                    <select class="form-control" id="ChildBlock-2-ChildAge-1" name="ChildBlock-2-ChildAge-1">
                                                        <option value="-1" selected="selected">Age?</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                        <option value="9">9</option>
                                                        <option value="10">10</option>
                                                        <option value="11">11</option>
                                                        <option value="12">12</option>
                                                    </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-2" name="ChildBlock-2-Child-2" style="display: none;">
                                                    <div class="form-group">  
                                                        <label>Child 2</label>
                                                        <select class="form-control" id="ChildBlock-2-ChildAge-2" name="ChildBlock-2-ChildAge-2">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-3" name="ChildBlock-2-Child-3" style="display: none;">
                                                    <div class="form-group">
                                                        <label>Child 3</label>
                                                        <select class="form-control" id="ChildBlock-2-ChildAge-3" name="ChildBlock-2-ChildAge-3">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-4" name="ChildBlock-2-Child-4" style="display: none;">
                                                    <div class="form-group">
                                                        <label>Child 4</label>
                                                        <select class="form-control" id="ChildBlock-2-ChildAge-4" name="ChildBlock-2-ChildAge-4">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-5" name="ChildBlock-2-Child-5" style="display: none;">
                                                    <div class="form-group">
                                                        <label>Child 5</label>
                                                        <select class="form-control" id="ChildBlock-2-ChildAge-5" name="ChildBlock-2-ChildAge-5">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                <div class="col-4"  id="ChildBlock-2-Child-6" name="ChildBlock-2-Child-6" style="display: none;">
                                                    <div class="form-group">
                                                        <label>Child 6</label>
                                                        <select class="form-control" id="ChildBlock-2-ChildAge-6" name="ChildBlock-2-ChildAge-6">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                    </div>
                                                </div>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row custom-gutter room-wrapper" id="room-3" style="display: none;">  
                                            <div class="col-12"> <label class="room-label">Room 3</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                    <select name="adtRoom-3" id="adtRoom-3" class="form-control no-select2 adult-pax">
                                                        <option selected="selected" value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                    </select>
                                                    <input type="hidden" value="0" id="PrevChildCount-3" name="PrevChildCount-3" />
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label> Children (2-12 yrs)</label> 
                                                    <select onchange="javascript:ShowChildAge('3')" name="chdRoom-3" id="chdRoom-3" class="form-control  no-select2  child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                    </select>
                                                </div>
                                            </div>
                                            <div class="col-12"  id="ChildBlock-3" name="ChildBlock-3" style="display: none;">
                                                <div class="row no-gutters child-age-wrapper">     
                                                    <div class="col-4"  id="ChildBlock-3-Child-1" name="ChildBlock-3-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                            <label>Child 1</label>
                                                            <select class="form-control" id="ChildBlock-3-ChildAge-1" name="ChildBlock-3-ChildAge-1">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div> 
                                                    </div>
                                                    <div class="col-4"  id="ChildBlock-3-Child-2" name="ChildBlock-3-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                            <label>Child 2</label>
                                                            <select class="form-control" id="ChildBlock-3-ChildAge-2" name="ChildBlock-3-ChildAge-2"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div> 
                                                    </div>
                                                    <div class="col-4"  id="ChildBlock-3-Child-3" name="ChildBlock-3-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                            <label>Child 3</label>
                                                            <select class="form-control" id="ChildBlock-3-ChildAge-3" name="ChildBlock-3-ChildAge-3"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div> 
                                                    </div>
                                                    <div class="col-4"  id="ChildBlock-3-Child-4" name="ChildBlock-3-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                            <label>Child 4</label>
                                                            <select class="form-control" id="ChildBlock-3-ChildAge-4" name="ChildBlock-3-ChildAge-4"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div> 
                                                    </div>
                                                    <div class="col-4"  id="ChildBlock-3-Child-5" name="ChildBlock-3-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                            <label>Child 5</label>
                                                            <select class="form-control" id="ChildBlock-3-ChildAge-5" name="ChildBlock-3-ChildAge-5"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div> 
                                                    </div>
                                                    <div class="col-4"  id="ChildBlock-3-Child-6" name="ChildBlock-3-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                            <label>Child 6</label>
                                                            <select class="form-control" id="ChildBlock-3-ChildAge-6" name="ChildBlock-3-ChildAge-6"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                        </div> 
                                                    </div>      
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row custom-gutter room-wrapper" id="room-4" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 4</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                            <select name="adtRoom-4" id="adtRoom-4" class="form-control no-select2 adult-pax">
                                                            <option selected="selected" value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                        </select>
                                                        <input type="hidden" value="0" id="PrevChildCount-4" name="PrevChildCount-4" /> 
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>  Child (2-12 yrs)</label> 
                                                    <select onchange="javascript:ShowChildAge('4')" name="chdRoom-4" id="chdRoom-4" class="form-control no-select2 child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                    </select>  

                                                </div>
                                            </div>                                            
                                            <div class="col-12"  id="ChildBlock-4" name="ChildBlock-4" style="display: none;">
                                                <div class="row no-gutters child-age-wrapper">   
                                                    <div class="col-4" id="ChildBlock-4-Child-1" name="ChildBlock-4-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 1</label>
                                                        <select class="form-control" id="ChildBlock-4-ChildAge-1" name="ChildBlock-4-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-2" name="ChildBlock-4-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 2</label>
                                                        <select class="form-control" id="ChildBlock-4-ChildAge-2" name="ChildBlock-4-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-3" name="ChildBlock-4-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 3</label>
                                                        <select class="form-control" id="ChildBlock-4-ChildAge-3" name="ChildBlock-4-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-4" name="ChildBlock-4-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 4</label>
                                                        <select class="form-control" id="ChildBlock-4-ChildAge-4" name="ChildBlock-4-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-5" name="ChildBlock-4-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 5</label>
                                                        <select class="form-control" id="ChildBlock-4-ChildAge-5" name="ChildBlock-4-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-4-Child-6" name="ChildBlock-4-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 6</label>
                                                        <select class="form-control" id="ChildBlock-4-ChildAge-6" name="ChildBlock-4-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                            <option value="12">12</option>
                                                        </select>
                                                        </div>
                                                    </div> 
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row custom-gutter room-wrapper" id="room-5" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 5</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                        <select name="adtRoom-5" id="adtRoom-5" class="form-control adult-pax">
                                                        <option selected="selected" value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                        <option value="7">7</option>
                                                        <option value="8">8</option>
                                                    </select>
                                                    <input type="hidden" value="0" id="PrevChildCount-5" name="PrevChildCount-5" />    
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label> Child (2-12 yrs)</label> 
                                                        <select onchange="javascript:ShowChildAge('5')" name="chdRoom-5" id="chdRoom-5" class="form-control  child-pax">
                                                        <option selected="selected" value="0">none</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                        <option value="6">6</option>
                                                    </select>
                                                </div>
                                            </div>                                            
                                            <div class="col-12"  id="ChildBlock-5" name="ChildBlock-5" style="display: none;">                                             
                                                    <div class="row no-gutters child-age-wrapper">
                                                        <div class="col-4" id="ChildBlock-5-Child-1" name="ChildBlock-5-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 1</label>
                                                            <select class="form-control" id="ChildBlock-5-ChildAge-1" name="ChildBlock-5-ChildAge-1">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-2" name="ChildBlock-5-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 2</label>
                                                            <select class="form-control" id="ChildBlock-5-ChildAge-2" name="ChildBlock-5-ChildAge-2"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-3" name="ChildBlock-5-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 3</label>
                                                            <select class="form-control" id="ChildBlock-5-ChildAge-3" name="ChildBlock-5-ChildAge-3"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-4" name="ChildBlock-5-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 4</label>
                                                            <select class="form-control" id="ChildBlock-5-ChildAge-4" name="ChildBlock-5-ChildAge-4"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-5" name="ChildBlock-5-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 5</label>
                                                            <select class="form-control" id="ChildBlock-5-ChildAge-5" name="ChildBlock-5-ChildAge-5"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                        <div class="col-4" id="ChildBlock-5-Child-6" name="ChildBlock-5-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                            <label>Child 6</label>
                                                            <select class="form-control" id="ChildBlock-5-ChildAge-6" name="ChildBlock-5-ChildAge-6"
                                                                onchange="javascript:ShowChildAge('1')">
                                                                <option value="-1" selected="selected">Age?</option>
                                                                <option value="2">2</option>
                                                                <option value="3">3</option>
                                                                <option value="4">4</option>
                                                                <option value="5">5</option>
                                                                <option value="6">6</option>
                                                                <option value="7">7</option>
                                                                <option value="8">8</option>
                                                                <option value="9">9</option>
                                                                <option value="10">10</option>
                                                                <option value="11">11</option>
                                                                <option value="12">12</option>
                                                            </select>
                                                            </div>
                                                        </div>
                                                    </div>  
                                            </div>
                                        </div>
                                        <div class="row custom-gutter room-wrapper" id="room-6" style="display: none;">                                           
                                            <div class="col-12"> <label class="room-label">Room 6</label></div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label>Adults</label> 
                                                        <select name="adtRoom-6" id="adtRoom-6" class="form-control adult-pax">
                                                        <option selected="selected" value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                    </select>
                                                    <input type="hidden" value="0" id="PrevChildCount-6" name="PrevChildCount-6" />
                                                </div>
                                            </div>
                                            <div class="col-4" >
                                                <div class="form-group">
                                                    <label> Child (2-12 yrs)</label> 
                                                            <select onchange="javascript:ShowChildAge('6')" name="chdRoom-6" id="chdRoom-6" class="form-control child-pax">
                                                            <option selected="selected" value="0">none</option>
                                                            <option value="1">1</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                        </select>  
                                                </div>
                                            </div>                                            
                                            <div class="col-12" id="ChildBlock-6" name="ChildBlock-6" style="display: none;">
                                                    <div class="row no-gutters child-age-wrapper">
                                                    <div class="col-4" id="ChildBlock-6-Child-1" name="ChildBlock-6-Child-1" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 16</label>
                                                        <select class="form-control" id="ChildBlock-6-ChildAge-1" name="ChildBlock-6-ChildAge-1">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-2" name="ChildBlock-6-Child-2" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 26</label>
                                                        <select class="form-control" id="ChildBlock-6-ChildAge-2" name="ChildBlock-6-ChildAge-2"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-3" name="ChildBlock-6-Child-3" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 36</label>
                                                        <select class="form-control" id="ChildBlock-6-ChildAge-3" name="ChildBlock-6-ChildAge-3"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-4" name="ChildBlock-6-Child-4" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 46</label>
                                                        <select class="form-control" id="ChildBlock-6-ChildAge-4" name="ChildBlock-6-ChildAge-4"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-5" name="ChildBlock-6-Child-5" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 56</label>
                                                        <select class="form-control" id="ChildBlock-6-ChildAge-5" name="ChildBlock-6-ChildAge-5"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>
                                                    <div class="col-4" id="ChildBlock-6-Child-6" name="ChildBlock-6-Child-6" style="display: none;">
                                                        <div class="form-group">  
                                                        <label>Child 6</label>
                                                        <select class="form-control" id="ChildBlock-6-ChildAge-6" name="ChildBlock-6-ChildAge-6"
                                                            onchange="javascript:ShowChildAge('1')">
                                                            <option value="-1" selected="selected">Age?</option>
                                                            <option value="2">2</option>
                                                            <option value="3">3</option>
                                                            <option value="4">4</option>
                                                            <option value="5">5</option>
                                                            <option value="6">6</option>
                                                            <option value="7">7</option>
                                                            <option value="8">8</option>
                                                            <option value="9">9</option>
                                                            <option value="10">10</option>
                                                            <option value="11">11</option>
                                                        </select>
                                                        </div>
                                                    </div>    
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>                   
                                         
                            <div class="col-md-3">       
                                <div class="form-control-holder">
                                    <div class="icon-holder">
                                        <span class="icon-star" aria-label=""></span>
                                    </div>             
                                    <asp:DropDownList runat="server"  ID="rating"  CssClass="form-control">
                                        <asp:ListItem selected="True" Text="Show All" value="0"></asp:ListItem>
                                        <asp:ListItem Text="1 rating" Value="1"></asp:ListItem>
                                        <asp:ListItem Text="2 rating" Value="2"></asp:ListItem>
                                        <asp:ListItem Text="3 rating" Value="3"></asp:ListItem>
                                        <asp:ListItem Text="4 rating" Value="4"></asp:ListItem>
                                        <asp:ListItem Text="5 rating" Value="5"></asp:ListItem>
                                    </asp:DropDownList>                                         
                                </div>  
                            </div>   
                            
                            <%--<% if (Settings.LoginInfo.AgentType == AgentType.BaseAgent ||Settings.LoginInfo.AgentType == AgentType.Agent || true  ) { %>--%>
                            <div class="col-md-4" id="suppContianer">
                                <div class="custom-dropdown-wrapper">
                                    <a href="jaavscript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#HtlSupplierDropdown">
                                        <div class="form-control-holder">
                                            <div class="icon-holder">                                       
                                                    <span class="icon-global-settings"></span>                                     
                                            </div>   
                                            <span class="form-control-text" id="htlSelectedSuppliers">Search Suppliers</span>                                  
                                        </div>  
                                    </a>
                                    <div class="dropdown-content d-none p-4" id="HtlSupplierDropdown">                         
                                        <div class="row no-gutters">                                            
                                            <div class="col-md-12">
                                                <strong> Search Suppliers : </strong>
                                            </div>     
                                            <div class="col-md-12" id="dvCheckBoxListControl">                               
                                                <table width="100%" id="tblSources" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" enableviewstate="true" runat="server" border="0" cellspacing="0" cellpadding="0"></table>  
                                                <a href="javascript:void(0);" class="btn btn-primary btn-sm float-right dd-done-btn">DONE</a>
                                            </div>                                             
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--<%} %>       --%>                 
                        </div>                    
                        <div class="row custom-gutter" id="childSuppContianer">   
                            <div class="col-md-8">
                                <div class="form-control-holder">
                                    <div class="icon-holder">
                                        <span class="icon-bed" aria-label="Origin Airport"></span>
                                    </div>                             
                                    <asp:TextBox placeholder="Search By Hotel Name" CssClass="form-control" ID="txtHotelName" runat="server"></asp:TextBox>                           
                                </div>             
                            </div>  
                            <%-- <% if (Settings.LoginInfo.AgentType == AgentType.BaseAgent ||Settings.LoginInfo.AgentType == AgentType.Agent  ) { %>--%>
                            <div class="col-md-4" id="divChildSuppliers">
                                <div class="custom-dropdown-wrapper" id="divChildSuppliers">
                                    <a href="jaavscript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#HtlChildSupplierDropdown">
                                        <div class="form-control-holder mb-0">
                                            <div class="icon-holder">                                       
                                                    <span class="icon-global-settings"></span>                                     
                                            </div>   
                                            <span class="form-control-text mt-2" id="htlChdSelectedSuppliers">Child Suppliers</span>                                  
                                        </div>  
                                    </a>
                                    <div class="dropdown-content d-none p-4" id="HtlChildSupplierDropdown">                         
                                        <div class="row no-gutters">                                            
                                            <div class="col-md-12">
                                                <strong> Child Suppliers : </strong>
                                            </div>     
                                            <div class="col-md-12" id="dvChkbChildListControl">                               
                                               <%-- <table width="100%" id="tblChildSources" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" enableviewstate="true" runat="server" border="0" cellspacing="0" cellpadding="0"></table>  --%> 
                                                <a href="javascript:void(0);" class="btn btn-primary btn-sm float-right dd-done-btn">DONE</a>
                                            </div>                                             
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <%--<%} %>  --%>




                        </div>                                
                        <div class="col-xs-12 py-3">
                            <input class="but but_b pull-right block_xs" type="button" name="action" id="action" value="Search Hotels"
                                                            onclick="return HotelSearch();" />      
                        </div>      
                        <div class="clear"></div>    
                    </div>                                                                        
                    <ul id="classics2" class="hide"></ul>
                </div>
                <!-- END List Wrap -->
            </div>
            <div class="col-lg-5 col-xl-6">                      
                <div class="home-slider-wrapper d-flex flex-column h-100">                    
                    <!-- Tab panes -->
                    <div class="tab-content flex-fill">
                        <div role="tabpanel" class="tab-pane active" id="home">


                            <!--Only for Petrofac-->
                             <% if (Settings.LoginInfo.AgentId ==680 || Settings.LoginInfo.AgentId ==2512 
                                          || Settings.LoginInfo.AgentId ==2785 || Settings.LoginInfo.AgentId ==2786 || Settings.LoginInfo.AgentId ==2787 || Settings.LoginInfo.AgentId ==2788 || Settings.LoginInfo.AgentId ==2826
                                           || Settings.LoginInfo.AgentId ==2827 || Settings.LoginInfo.AgentId ==2828 || Settings.LoginInfo.AgentId ==2931 || Settings.LoginInfo.AgentId ==3551) // for Petrofac (All) ,3551=FTI consultant
                                     { %>
                             
                           <div class="latestupdate" style="bottom:11px;top:auto">	
						        <div class="alert alert-warning p-2 small">
                                    <%if (Settings.LoginInfo.AgentId == 3551)
                                        { %>

                                    <strong>Please <b><a target="_blank" style="cursor:pointer" href="https://travtrolley.com/FITnotes.html">Click Here</a></b> for travel guidlines.</strong>
                                    <%} else{ %>
									<strong>Prior to commencing any international travel please ensure you are aware of quarantine restrictions at all destinations (including where you will be returning to) and that you comply with any Covid19 testing required. For more information see International SOS and IATA Quarantine guidelines available&nbsp;on the <u>Petrofac Hub</u> Travel pages.</strong>
                                    <%} %>
							    </div>
					        </div>
                            <% } %>



                            <%if (clsCMSDet != null && clsCMSDet.sliders != null && clsCMSDet.sliders.Count() > 0)
                            { %>
                            <div class="owl-carousel owl-theme promo-carousel">
                                <%foreach (var slide in clsCMSDet.sliders)
                                { %>
                                <div class="item active"><img src="<%=slide.CMS_IMAGE_FILE_PATH %>" /></div>
                                <%} %>
                            </div>
                            <%} %>
                        </div>   
                        <%if (clsCMSDet != null && clsCMSDet.CMS_IS_BANK)
                        { %>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="table-responsive" style="padding:10px"><%=clsCMSDet.CMS_BANK_DETAILS %></div>
                        </div>  
                        <%} %>
                        <%if (clsCMSDet != null && clsCMSDet.CMS_IS_SUPPORT)
                        { %>
                        <div role="tabpanel" class="tab-pane" id="settings">
                            <div class="table-responsive" style="padding:10px; line-height:21px"><%=clsCMSDet.CMS_SUPPORT_DETAILS %></div>
                        </div>
                        <%} %>
                    </div>
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                        <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Promotions</a></li>
                        <!-- Not Required for Corporate   -->
                        <%if (clsCMSDet != null && clsCMSDet.CMS_IS_BANK)
                        { %>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Bank A/c Details</a></li>
                        <%} %>
                        <%if (clsCMSDet != null && clsCMSDet.CMS_IS_SUPPORT)
                        { %>
                        <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Support</a></li>
                        <%} %>
                    </ul>
                </div>   

                <%--<div class="home-slider-wrapper d-flex flex-column h-100">
                    <% string Currency=Settings.LoginInfo.Currency; %>
                    <!-- Tab panes -->
                    <div class="tab-content flex-fill">
                        <div role="tabpanel" class="tab-pane active" id="home">
                            <%--Notification Petrofac -Start-->
                             <%if (Settings.LoginInfo.AgentId == 680 || Settings.LoginInfo.AgentId == 2512
             || Settings.LoginInfo.AgentId == 2785 || Settings.LoginInfo.AgentId == 2786 || Settings.LoginInfo.AgentId == 2787 || Settings.LoginInfo.AgentId == 2788 || Settings.LoginInfo.AgentId == 2826
              || Settings.LoginInfo.AgentId == 2827 || Settings.LoginInfo.AgentId == 2828 || Settings.LoginInfo.AgentId == 2931) // for Petrofac (All)
                                 { %>
                             <div class="notification-lg-panel" style="padding: 6px;position: absolute;padding: 6px; bottom: 34px;left: 5px;z-index: 10;right: 5px;font-size: 13px;color: #721c24;background-color: #f8d7da;border-color: #f5c6cb;">
                                 <p id="notification-1">There is a travel ban to Algeria for travel between <strong>11th Dec – 14th Dec</strong>. For more details, please contact Agency at <strong>pf-gcc@cozmotravel.com</strong>.</p>                               
                             </div>
                            <%} %>
                             <%--END: Notification Petrofac-->

                            <div class="owl-carousel owl-theme promo-carousel">

                                   <%if (HttpContext.Current.Request.Url.Host.ToLower().Contains("ibyta.com"))
                                    {  %>
                                <div class="item">
                                     <img src="images/banner2976.jpg" />
                                </div>
                                <%} else if (Settings.LoginInfo.AgentId ==680 || Settings.LoginInfo.AgentId ==2512 
                                          || Settings.LoginInfo.AgentId ==2785 || Settings.LoginInfo.AgentId ==2786 || Settings.LoginInfo.AgentId ==2787 || Settings.LoginInfo.AgentId ==2788 || Settings.LoginInfo.AgentId ==2826
                                           || Settings.LoginInfo.AgentId ==2827 || Settings.LoginInfo.AgentId ==2828 || Settings.LoginInfo.AgentId ==2931 ) // for Petrofac (All)
                                     { %>



                                <div class="item">
                                        <img src="images/banner_petrofac.jpg" />
                                </div>
                              
                                    <%} else if (Currency == "SAR")
                                      { %>
                                 <div class="item">
                                    <img src="images/KSA_B2B_Banner.jpg" />
                                </div>
                               <%}
                                    else if (Currency != "INR")
                                                    { %>
                                <%--<div class="item">
                                    <img src="images/banner2.jpg" />
                                </div>-->
                                    <div class="item">
                                    <img src="images/banner3.jpg" />
                                </div>
                                    <div class="item">
                                    <img src="images/banner4.jpg" />
                                </div>
                                <div class="item">
                                    <img src="images/banner5.jpg" />
                                </div>
                                <div class="item">
                                    <img src="images/banner6.jpg" />
                                </div>
                                    <%} else
    {%>
            <div class="item active">
        <img src="images/GPS_Backdrop_GlobalVisa_70x48in.jpg" />
        </div>

        <div class="item">
        <img src="images/GPS_Backdrop_Holidays_70x48in.jpg" />
        </div>
            <div class="item">
        <img src="images/GPS_Backdrop_Services_70X40in.jpg" />
        </div>
        <%} %>
                            </div>
                        </div>
                        <!-- Not Required for Corporate   -->
                        <%if (!(Settings.LoginInfo.IsCorporate == "Y"))
                                { %>
                        <div role="tabpanel" class="tab-pane" id="profile">
                            <div class="table-responsive" style="padding:10px"> 
                            <%if (Currency == "KWD")
                                                        {%>
                            <h4 class="bggray">United Arab Emirates </h4>
                            <table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                            <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">Ahli United Bank</td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>Account Name</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">COZMO TRAVEL</td>
                            </tr>
                            <tr>
                            <td><strong>Account Number</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">1 2 2 9 3 0 1 1 </td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>IBAN No</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">KW 18 BKME 0000 0000 0000 0012293011</td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>Currency</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">AED</td>
                            </tr>

                            <tr>
                            <td valign="bottom"><strong>Branch</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">Fahed Al Salem Street</td>
                            </tr>
                            <%-- <tr>
                            <td valign="bottom"><strong>SWIFT    CODE</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">EBILAEADSUK</td>
                            </tr>-->
                            </table><br />
    
                            <%} else if (Currency == "INR")
                                                        { %>
                            <h4 class="bggray">INDIA </h4>
                            <table class="acdetails table table-structure" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                            <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">ICICI BANK LIMITED</td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>Account Name</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">Cozmo Travel World Private Limited</td>
                            </tr>
                            <tr>
                            <td><strong>Account Number</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">0 0 1 1 0 5 0 2 6 1 3 9 </td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>IFSC CODE</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">ICIC0000011</td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>Currency</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">INR</td>
                            </tr>

                            <tr>
                            <td valign="bottom"><strong>Branch</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">SAGAR AVENUE, GROUND FLOOR, OPP. SHOPPERS STOP, S.V. ROAD ANDHERI WEST 400058</td>
                            </tr>
                            <%-- <tr>
                            <td valign="bottom"><strong>SWIFT    CODE</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">EBILAEADSUK</td>
                            </tr>-->
                            </table><br />
                            <%}

                                                        else if (Currency != "SAR")
                                                        {%>
                            <h4 class="bggray p-3">United Arab Emirates </h4>
                            <table class="acdetails table table-structure" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                            <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">EMIRATES NBD</td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>Account Name</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">COZMO TRAVEL LLC</td>
                            </tr>
                            <tr>
                            <td><strong>Account Number</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">1 0 1 4 0 4 0 9 2 6 9 0 8 </td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>IBAN No</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">AE580260001014040926908</td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>Currency</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">AED</td>
                            </tr>

                            <tr>
                            <td valign="bottom"><strong>Branch</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">Al Souk</td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>SWIFT    CODE</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">EBILAEADSUK</td>
                            </tr>
                            </table><br />
                            <%} else{ %>
                            <h4 class="bggray"> Saudi Arabia </h4> 


                            <table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                            <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">SAMBA</td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>Account Name</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">COZMO TRAVEL LIMITED CO</td>
                            </tr>
                            <tr>
                            <td><strong>Account Number</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">2 1 0 0 2 0 6 0 1 0 </td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>IBAN No</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">SA69 4000 0000 0021 0020 6010 </td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>Currency</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">SAR</td>
                            </tr>

                            <tr>
                            <td valign="bottom"><strong>SWIFT    CODE</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">SAMBSARI</td>
                            </tr>
                            </table>



                            <%--<div style=" border-bottom: solid 2px #ccc"> </div>


                            <table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                            <td valign="bottom"><strong>Bank&nbsp;Name</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">NCB</td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>Accoun Name</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">COZMO TRAVEL LTD.CO</td>
                            </tr>
                            <tr>
                            <td><strong>Account Number</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">2 7 0 5 6 2 8 1 0 0 0 1 0 5 </td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>IBAN No</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">SA53 1000 0027 0562 8100 0105</td>
                            </tr>
                            <tr>
                            <td valign="bottom"><strong>Currency</strong></td>
                            <td valign="bottom">:</td>
                            <td valign="bottom">SAR</td>
                            </tr>


                            </table>
                            -->

                            <%} %>

                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane" id="messages">
                            <div style=" padding:10px">
                                <div class="paramcon"> 
                                    <div class="marbot_10"> 
                                        <div class="col-md-3"> Amount :  <i class="fcol_red">* </i> </div> 
                                        <div class="col-md-4"> <input type="text" class="form-control">   </div> 
                                        <div class="clearfix"> </div> 
                                    </div>
                                    <div> 
                                        <div class="col-md-3"> Booking Ref Details :  <i class="fcol_red">* </i> </div> 
                                        <div class="col-md-4"> <textarea class="form-control" name="" cols="" rows=""></textarea>  </div> 
                                        <div class="clearfix"> </div> 
                                    </div>
                                    <div class="col-md-12"> 
                                        <h4 class="mt-4">Payment Option </h4> 
                                        We accept only GCC countries Bank issued Visa / Master Card / American Express credit cards.<br>
                                        <div> <img src="images/payment-logos.jpg">   </div> 
                                        Payment is processed through an online payment gateway. Your credit card transaction will be processed directly with bank which authorizes your credit card. <br>
                                        <div class="mt-4"> <a class="btn but_b"> Make Payment </a>   </div> 
                                    </div> 
                                    <div class="clearfix"> </div>       
                                </div>
                            </div>
                        </div>
                        <%} %>
                        <div role="tabpanel" class="tab-pane" id="settings">
                            <div class="table-responsive" style="padding:10px; line-height:21px">
                                <table class="acdetails" width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr class="hidden-xs"> 
                                        <td> <center> <img height="136" src="images/support-center.png" /></center></td>
                                    </tr>
                                </table>   
                                <%if (Currency != "INR") { %>
                                <table  border="0" cellspacing="0" cellpadding="0">
                                    <tr> <td colspan="3">  <strong>Contact No.</strong><br /> </td> </tr>
                                        <%if (Currency == "SAR")
                                            { %>

                                        <tr> <td> Landline</td> <td>: </td> <td>+966 11 5200854 </td></tr>
                                        <tr> <td> Mobile</td> <td>: </td> <td>+966 55 3008039 </td></tr>
                                        <%} else { %>
    
                                        <tr> <td> Landline</td> <td>: </td> <td>+971 6 5074592 </td></tr>
                                        <tr> <td> Mobile</td> <td>: </td> <td>+971 56  4479659 </td></tr>
                                        <%} %>
                                    <tr> <td colspan="3">  <strong>Email:</strong><br /> </td> </tr>
                                    <tr> <td>  For any Support Contact</td> <td>: </td> <td> <a href="mailto:B2bsupport@cozmotravel.com">B2bsupport@cozmotravel.com</a></td> </tr>
                                    <tr> <td> For cancellations, refunds, reissuance</td> <td>: </td> <td><a href="mailto:B2bsupport@cozmotravel.com">B2bsupport@cozmotravel.com</a> </td> </tr>
                                      <%if (Currency != "SAR")
                                          {%>
                                    <tr> <td> For Hotels, Packages, Tours, Insurance</td> <td>: </td> <td><a href="mailto:B2bholidays@cozmotravel.com">B2bholidays@cozmotravel.com</a> </td> </tr>
                                    <%} %>
                                    <tr> <td> For Sales</td> <td>: </td> <td><a href="mailto:B2bsales@cozmotravel.com">B2bsales@cozmotravel.com</a></td> </tr>
                                    <%if (Currency != "SAR") {%>
                                    <tr> <td>  For Topup:</td> <td>: </td> <td><a href="mailto:b2btopupcok@cozmotravel.com">b2btopupcok@cozmotravel.com</a> </td></tr>
                                    <%}  else { %>
                                    <tr> <td>  For Topup:</td> <td>: </td> <td><a href="mailto:Ksatopup@cozmotravel.com">Ksatopup@cozmotravel.com</a> </td></tr>
                                    <%} %>
                                </table>
                                <%}
                                else { %>
                                <table  border="0" cellspacing="0" cellpadding="0">
                                    <tr> <td colspan="3">  <strong>Contact No.</strong><br /> </td> </tr>
                                    <tr> <td> Landline</td> <td>: </td> <td>022 7100 4644</td> </tr>
                                    <tr> <td> Mobile</td> <td>: </td> <td>9967444999</td></tr>
                                    <tr> <td colspan="3">  <strong>Email:</strong><br /> </td> </tr>
                                    <tr> <td>  For any Support Email Id</td> <td>: </td> <td> <a href="supportb2b@cozmotravelworld.com">supportb2b@cozmotravelworld.com</a></td> </tr>
                                    <tr> <td> For INFO Email Id</td> <td>: </td> <td><a href="infob2b@cozmotravelworld.com">infob2b@cozmotravelworld.com</a> </td> </tr>
                                    <tr> <td> For TOP UP Email id</td> <td>: </td> <td><a href="topupb2b@cozmotravelworld.com">topupb2b@cozmotravelworld.com</a> </td> </tr>
                                    <tr> <td> For SALES Email Id</td> <td>: </td> <td><a href=" salesb2b@cozmotravelworld.com"> salesb2b@cozmotravelworld.com</a></td> </tr>
                                </table>
                                <%} %>
                            </div>
                        </div>
                    </div>
                <!-- Nav tabs -->
                    <ul class="nav nav-tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab">Promotions</a></li>
                        <!-- Not Required for Corporate   -->
                        <%if (!(Settings.LoginInfo.IsCorporate == "Y")  && !HttpContext.Current.Request.Url.Host.ToLower().Contains("ibyta.com"))
                            { %>
                        <li role="presentation"><a href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Bank A/c Details</a></li>
                    <li role="presentation"><a href="#messages" aria-controls="messages" role="tab" data-toggle="tab">Agent Topup</a></li>
                        <%} %>
                        <%if (!HttpContext.Current.Request.Url.Host.ToLower().Contains("ibyta.com"))
                            {  %>
                    <li role="presentation"><a href="#settings" aria-controls="settings" role="tab" data-toggle="tab">Support</a></li>
                        <%} %>
                    </ul>
                </div>--%>    
            </div>
        </div>
    </div> 
    <%-- right panel startes here--%>
    <div class="row">
        <div class="modal fade pymt-modal" data-backdrop="static" id="ShowMsgDiv" tabindex="-1" role="dialog" aria-labelledby="ShowMsgDiv">
            <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close" onclick="return window.close('ShowMsgDiv');" style="color:#fff;opacity:1;"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Message Details</h4>
                </div>
                <div class="modal-body">                                          
                    <asp:Label ID="lblFare" runat="server" Text=""></asp:Label>
                    <table class="table table-striped table-condensed" >       
                        <tr>
                            <td height="30px" align="left">
                                From:
                            </td>
                            <td align="left">
                                <label id="From" name="From">
                                </label>
                            </td>
                        </tr>
                        <tr>
                            <td height="30px" align="left">
                                Subject:
                            </td>
                            <td align="left">
                                <label id="Subject" name="Subject">
                                </label>
                            </td>
                        </tr>            
                        <tr>
                            <td height="30px" align="left">
                                Message:
                            </td>
                            <td align="left">
                                <label id="Message" name="Message">
                                </label>
                                <asp:Label ID="Label3" runat="server" Text=""></asp:Label>
                            </td>
                        </tr>
                    </table>
                </div>
            </div>
        </div>
    </div>
    </div>
    <div class="col-md-6 ">       
        <h3> Important Messages</h3>
        <div style="overflow-y:scroll; height:180px; border: solid 1px #ccc">               
            <table  width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <asp:GridView CssClass="grdTable" ID="gvMessages" runat="server" AutoGenerateColumns="False" Width="100%"
                            AllowPaging="true" PageSize="8" DataKeyNames="msg_id" CellPadding="0" BorderWidth="0" 
                            CellSpacing="0" GridLines="Horizontal" BorderColor="White" 
                            OnPageIndexChanging="gvMessages_PageIndexChanging" >                                
                            <HeaderStyle CssClass="showMsgHeading"></HeaderStyle>
                            <Columns>
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <HeaderTemplate>
                                        <label><b>From</b></label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblMsgFrom" runat="server" Text='<%# Eval("MsgFrom") %>' ToolTip='<%# Eval("MsgFrom") %>' Width="120px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                                  
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <HeaderTemplate>
                                        <label><b>Subject</b></label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="ITlnkSubject" runat="server" CommandName="Editing" CommandArgument='<%# DataBinder.Eval(Container.DataItem,"msg_id") %>' Text='<%# Eval("subject") %>'  Width="220px" OnClientClick="return GetSelectedRow(this,'ShowMsgDiv'); javascript:shouldsubmit=true;" ></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateField>                                     
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Left">
                                    <HeaderTemplate>
                                        <label ><b>Received On</b></label>
                                    </HeaderTemplate>
                                    <ItemTemplate>
                                        <asp:Label ID="ITlblReceivedOn" runat="server" Text='<%# Eval("ReceivedOn") %>' ToolTip='<%# Eval("ReceivedOn") %>' Width="70px"></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateField>                                    
                                <asp:TemplateField HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign="Left">
                                    <ItemTemplate>
                                        <asp:HiddenField ID="IThdfMessage" runat="server" Value='<%# Eval("message") %>' />
                                    </ItemTemplate>
                                </asp:TemplateField>
                            </Columns>
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="col-md-6">        
        <h3>  Booking History</h3>      
        <div class="">
            <div class="nA-h3"> Booking Made Today </div>
                <div class="nA-h4 ">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td id="tdFligtsBkgCount1" class='hide'>
                                <table>
                                    <tr >
                                        <td width="5%">
                                            <img class="row-image" src="images/flight_sml.png" width="22" height="20" />
                                        </td>
                                        <td width="95%">
                                            Flights : 
                                            <asp:HyperLink ID="lblFlightsBookingCountToday" runat="server" NavigateUrl="~/AgentBookingQueue.aspx?FromDate="></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td id="tdHotelsBkgCount1" class='current'>
                                <table>
                                    <tr >
                                        <td width="5%">
                                            <img class="row-image" src="images/hotel_sml.png" width="22" height="20" />
                                        </td>
                                        <td width="95%">
                                            Hotels : 
                                            <%--<asp:Label ID="lblHotelsBookingCountToday" runat="server" Text=""></asp:Label>--%>
                                            <asp:HyperLink ID="lblHotelsBookingCountToday" runat="server" NavigateUrl="~/HotelBookingQueue.aspx?FromDate="></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="nA-h3"> Booking Made in Last 7 Days </div>
                <div class="nA-h4 ">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td id="tdFligtsBkgCount2" class='hide'>
                                <table width="100%" border="0"  cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/flight_sml.png" width="22" height="20" />
                                        </td>
                                        <td width="95%">
                                            Flights : 
                                            <%--<asp:Label ID="lblFlightsBkgWeeklyCount" runat="server" Text=""></asp:Label>--%>
                                            <asp:HyperLink ID="lblFlightsBkgWeeklyCount" runat="server" NavigateUrl="~/AgentBookingQueue.aspx?FromDate="></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td id="tdHotelsBkgCount2" class='current'>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/hotel_sml.png" width="22" height="20" />
                                        </td>
                                        <td width="95%">
                                            Hotels : 
                                            <%--<asp:Label ID="lblHotelsBkgWeeklyCount" runat="server" Text=""></asp:Label>--%>
                                            <asp:HyperLink ID="lblHotelsBkgWeeklyCount" runat="server" NavigateUrl="~/HotelBookingQueue.aspx?FromDate="></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="nA-h3"> Booking Made in Last 30 Days</div>
                <div class="nA-h4 ">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td id="tdFligtsBkgCount3" class='hide'>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/flight_sml.png" width="22" height="20" />
                                        </td>
                                        <td width="95%">
                                            Flights : 
                                            <asp:HyperLink ID="lblFlightsBkgMonthlyCount" runat="server" NavigateUrl="~/AgentBookingQueue.aspx?FromDate="></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                                <td id="tdHotelsBkgCount3" class='current'>
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td width="5%">
                                            <img class="row-image" src="images/hotel_sml.png" width="22" height="20" />
                                        </td>
                                        <td width="95%">
                                            Hotels : 
                                            <asp:HyperLink ID="lblHotelsBkgMonthlyCount" runat="server" NavigateUrl="~/HotelBookingQueue.aspx?FromDate="></asp:HyperLink>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </div>
                <br />
            </div>     
        </div>
    
    <div id="PreLoader" style="display:none;position:relative;top:0;">
        <div class="loadingDiv">
            <div style="font-size: 14px; color:#FE642E">
                <strong> <span id="isUAE"></span> </strong>
            </div>
            <div id="imgHotelLoading">
                <img src="images/preloader11.gif" /></div>
            <div style="font-size: 16px; color: #999999">
                <strong>Loading results for</strong></div>
            <div style="font-size: 21px;" class="primary-color">
                <strong> <span id="searchCity"></span> </strong></div>
            </div>
        <div class="parameterDiv" style="top:435px">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td width="50%">
                            <label class="primary-color">
                                <b class="primary-color">Check In:</b></label>
                        
                            <strong> <span id="fromDate"></span></strong>
                        </td>
                        <td width="50%">
                            <label class="primary-color">
                                <b class="primary-color">Check out:</b></label>
                            <strong> <span id="toDate"></span></strong>
                        </td>
                    </tr>
                </table>
            </div>
    </div>    

    <script type="text/javascript">

        $('.select-element').select2();

        $('.owl-carousel').owlCarousel({
            loop: true,
            margin: 10,
            items: 1,
            nav: true,
                autoplay: true,
        autoplayTimeout:3000,// 3 Seconds
        autoplayHoverPause:true
        })

        //Hotel Pax Count
        function hotelPaxCount() {

            var roomCount = $('#PrevNoOfRooms').val();
            var htlAdultCount = 0,
                htlChildCount = 0;
            $('#hotelRoomSelectDropDown select.adult-pax').each(function (index) {
                count = index + 1;
                if (count > roomCount) {
                    return false;
                }
                htlAdultCount += parseInt($(this).val());
            })
            $('#hotelRoomSelectDropDown select.child-pax').each(function (index) {
                count = index + 1;
                if (count > roomCount) {
                    return false;
                }
                htlChildCount += parseInt($(this).val());
            });
            var HotelTotalPax = '<strong>';
            HotelTotalPax += roomCount;
            HotelTotalPax += '</strong> Room , <strong>';
            HotelTotalPax += htlAdultCount;
            HotelTotalPax += '</strong> Adult ';
            if (htlChildCount > 0) {
                HotelTotalPax += ', <strong>' + htlChildCount + '</strong> Child';
            }
            $('#htlTotalPax').html(HotelTotalPax);
        }
        hotelPaxCount()
        $('.hotel-pax-dropdown select').change(function () {
        
            hotelPaxCount()
        })


        //Activete Hotel/Flight Search Tab based on URL query string
        var URLLocation = location.href;
        var URLparam = URLLocation.substring(URLLocation.indexOf("?") + 1);
        //$('[data-tab-content]').hide();
        $('[data-tab-content="' + URLparam + '"]').show();

    </script>

    <!-- loading for flight end-->
     <script type="text/javascript">

         function GetSelectedRow(lnk, id) {

             var row = lnk.parentNode.parentNode;
             var gv = document.getElementById("<%=gvMessages.ClientID %>");
             var rwIndex = row.rowIndex;

             document.getElementById('From').innerHTML = gv.rows[rwIndex].cells[0].childNodes[1].innerHTML;
             document.getElementById('Subject').innerHTML = gv.rows[rwIndex].cells[1].childNodes[1].innerHTML;
             document.getElementById('Message').innerHTML = gv.rows[rwIndex].cells[3].childNodes[1].value;

             var div = document.getElementById(id);
             div.style.visibility = 'visible';
             return false;
         }

         function close(id) {
             var div = document.getElementById(id);
             div.style.visibility = 'hidden';
             return false;
         }     
    </script>     
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0mr4cq7HrZgJb9TZMLYWk-yh-_Ahg9Ro&libraries=places&callback=initAutocomplete" async defer></script>

    <%if (Page.PreviousPage != null && Page.PreviousPage.IsCrossPagePostBack) {
        if (Page.PreviousPage.Title == "Hotel Search Results")  { %>
            <script>

                <%if (Settings.LoginInfo.OnBehalfAgentLocation > 0)
                {%> 
                LoadAgentLocations('ct100_cphTransaction_ddlAgents', 'H');
                <%}%>
                SetModifyValues();
                disablefield();
            </script>
        <%}            

    }%> 
</asp:Content>
