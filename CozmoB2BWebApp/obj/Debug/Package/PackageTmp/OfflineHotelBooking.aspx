﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="OfflineHotelBooking.aspx.cs" Inherits="CozmoB2BWebApp.OfflineHotelBooking" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <style>
        .search-matrix-wrap {
            background-color: #f7f7f7;
            min-height: auto
        }

            .search-matrix-wrap .form-control-holder {
                margin-bottom: 0px;
                border: solid 1px #ccc;
            }

            .search-matrix-wrap .btn-group {
                border: solid 1px #ccc;
            }

            .search-matrix-wrap .form-control, .search-matrix-wrap .form-control-element, .search-matrix-wrap .form-control-holder .form-control-text, .CorpTrvl-page .form-control {
                height: 30px;
                line-height: 30px;
            }



        .form-control-holder .form-control.select2-container .select2-choice, .form-control-holder .select2-container.form-control-element .select2-choice {
            height: inherit;
            line-height: inherit;
        }


        .search-matrix-wrap .form-control-holder .icon-holder span, .search-matrix-wrap .form-control-holder .icon-holder {
            padding-top: 0px;
        }


        .search-matrix-wrap .with-custom-dropdown::after {
            padding-top: 6px;
        }


        .rulddlhotel {
            color: Black
        }




        .CorpTrvl-page .policy-textarea {
            height: 80px;
        }

        .bg_supplier {
            background-color: #60c231;
            color: #fff;
        }

        .bg_agent {
            background-color: #6d6d6d;
            color: #fff;
        }




        .rptdiv {
            margin-bottom: 10px
        }

            .rptdiv .roomNumber {
                background: #ccc;
                display: block;
                padding: 0px 10px 0px 10px;
                line-height: 24px
            }

            .rptdiv .borsap {
                border: solid 1px #ccc;
                padding: 0px 10px 0px 10px
            }

        [aria-expanded="true"] .fa.fa-plus-circle:before {
            content: "\f056";
        }

        .error {
            border: 1px solid #D8000C;
        }
    </style>
    <script src="css/toastr.min.js"></script>
    <link href="css/toastr.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="/scripts/Jquery/Jquery-ui.min.js"></script>
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="scripts/jquery-ui.js"></script>
    <script src="Scripts/Common/common.js"></script>
    <script src="Scripts/Common/FlexFields.js"></script>
    <script type="text/javascript">
        var count = 1;
        var Request = [];
        var exchangeRate = 1;
        var roomTypes; var mealPlans; var markupValue; var markupType;
        var vatdetails;
        var LoginInfo;
        var HotelItinerary = [];
        var HotelRoomsDetails = [];
        var HotelRoom = [];
        var PassenegerInfo = [];
        var defaultAgentInfo;
        var defaultFlexDetails;
        var onlineSources;
        var offlineSources;
        var suppliers = [];
        var CityName; var CountryName;
        $(document).ready(function () {//Page Load
            LoginInfo = JSON.parse(document.getElementById('<%=hdnLoginInfo.ClientID %>').value);
            defaultAgentInfo = LoginInfo;
           
        });
        function ShowRoomDetails() {

            try {
                var rooms = document.getElementById('roomCount');
                if (rooms != null) {
                    count = eval(rooms.options[rooms.selectedIndex].value);
                    if (document.getElementById('PrevNoOfRooms') != null) {
                        var prevCount = eval(document.getElementById('PrevNoOfRooms').value);

                        if (count > prevCount) {
                            for (var i = (prevCount + 1); i <= count; i++) {

                                document.getElementById('room-' + i).style.display = 'flex';
                                document.getElementById('adtRoom-' + i).value = '1';
                                document.getElementById('chdRoom-' + i).value = '0';
                                document.getElementById('PrevChildCount-' + i).value = '0';

                            }

                        }
                        else if (count < prevCount) {
                            for (var i = prevCount; i > count; i--) {
                                document.getElementById('room-' + i).style.display = 'none';
                                document.getElementById('adtRoom-' + i).value = '1';
                                var childcount = document.getElementById('chdRoom-' + i).value
                                document.getElementById('ChildBlock-' + i).style.display = 'none';
                                for (var j = 1; j <= childcount; j++) {
                                    document.getElementById('ChildBlock-' + i + '-ChildAge-' + j).value = '-1';
                                    document.getElementById('ChildBlock-' + i + '-ChildAge-' + j).style.display = 'none';
                                    document.getElementById('ChildBlock-' + i + '-Child-' + j).style.display = 'none';
                                }
                                document.getElementById('chdRoom-' + i).value = '0';
                            }
                        }
                    }
                    document.getElementById('PrevNoOfRooms').value = count;
                    hotelPaxCount();
                }
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function ShowChildAge(number) {
            try {
                var childCount = eval(document.getElementById('chdRoom-' + number).value);
                var PrevChildCount = eval(document.getElementById('PrevChildCount-' + number).value);
                if (eval(document.getElementById('chdRoom-1').value) > 0 || eval(document.getElementById('chdRoom-2').value) > 0 || eval(document.getElementById('chdRoom-3').value) > 0 || eval(document.getElementById('chdRoom-4').value) > 0) {
                    document.getElementById('childDetails').style.display = 'block';
                }
                else {
                    document.getElementById('childDetails').style.display = 'none';
                }
                if (childCount > PrevChildCount) {
                    document.getElementById('ChildBlock-' + number).style.display = 'block';
                    for (var i = (PrevChildCount + 1); i <= childCount; i++) {
                        document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'block';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).style.display = 'block';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                        $('#ChildBlock-' + number + '-ChildAge-' + i).select2('destroy');
                    }
                }
                else if (childCount < PrevChildCount) {
                    if (childCount == 0) {
                        document.getElementById('ChildBlock-' + number).style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-1').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-2').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-3').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-4').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-5').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-ChildAge-6').value = '-1';
                        document.getElementById('ChildBlock-' + number + '-Child-1').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-2').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-3').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-4').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-5').style.display = 'none';
                        document.getElementById('ChildBlock-' + number + '-Child-6').style.display = 'none';

                    }

                    else {
                        for (var i = PrevChildCount; i > childCount; i--) {
                            if (i != 0) {
                                document.getElementById('ChildBlock-' + number + '-Child-' + i).style.display = 'none';
                                document.getElementById('ChildBlock-' + number + '-ChildAge-' + i).value = '-1';
                            }
                        }
                    }
                }
                document.getElementById('PrevChildCount-' + number).value = childCount;
                hotelPaxCount();
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function show_OnlineSupplier() {
            try {
                $("#divSupplier").show();
                $('#ddlOnlineSuppliers').val('-1');
                $('#ddlOfflineSuppliers').val('-1');
                $('#ddlOfflineSuppliers').select2();
                document.getElementById('offline_supplier_list').style.display = "none";
                document.getElementById('online_supplier_list').style.display = "block";
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }

        function showSupplierrates() {
            try {
                document.getElementById('divAgentrates').style.display = "none";
                document.getElementById('divSupplierrates').style.display = "block";
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }

        }
        function showAgentrates() {
            try {
                document.getElementById('divSupplierrates').style.display = "none";
                document.getElementById('divAgentrates').style.display = "block";
                if ($('#txtRmSuppRate1').val() > 0) {
                    BindAgentRoomRates();
                }
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function exceptionSaving(msg) {
            var data = JSON.parse(AjaxCalls("OfflineHotelBooking.aspx/auditSaving", "{'exception':'" + msg + "'}"));
        }
        function show_OfflineSupplier() {
            $("#divSupplier").show();
            document.getElementById('online_supplier_list').style.display = "none";
            $('#ddlOfflineSuppliers').val('-1');
            $('#ddlOnlineSuppliers').val('-1');
            $('#ddlOnlineSuppliers').select2();
            document.getElementById('offline_supplier_list').style.display = "block";
        }

        function showHideAgent() {
            var x = document.getElementById("clientDiv");
            if (x.style.display === "none") {
                x.style.display = "block";
                $('#divAgentBalance').hide();
                 //document.getElementById('lblAgentBalance').style.display = "none";
            } else {
                x.style.display = "none";
                $('#divAgentBalance').show();
                 //document.getElementById('lblAgentBalance').style.display = "none";
                $('#ddlLocation').empty();
                $('#ddlAgent').val('-1')
                LoginInfo.AgentId = defaultAgentInfo.AgentId;
                LoginInfo.LocationID = defaultAgentInfo.LocationID;
                LoginInfo.Decimal = defaultAgentInfo.Decimal;
                LoginInfo.Currency = defaultAgentInfo.Currency;
                $('#divFlexFields0').empty();
                if (defaultFlexDetails != null && defaultFlexDetails.length > 0) {
                    BindFlexFields(JSON.stringify(defaultFlexDetails), null, 'divFlex', 'divFlexFields', '1', '0');
                    $('#UDIDTab').show();
                    //$('#btnContinue').show();
                    $('#btnSubmit').hide();
                }
                else {
                    $('#UDIDTab').hide();
                    $('#btnContinue').hide();
                    //$('#btnSubmit').show();
                }
            }
        }



        $(function () {
            $('#ddlSerchbyHotel').change(function () {
                $('.rulddlhotel').hide();
                $('#' + $(this).val()).show();
            });
            $('#ddlAgent').change(function () {
                $('.agnetBalance').hide();
                $('#' + $(this).val()).show();
            });
        });




        function addHotelDetails() {
            try {
                if (('<%=Settings.LoginInfo.MemberType %>') == '<%=MemberType.ADMIN%>') {

                    if (document.getElementById("clientDiv").style.display == "block") {

                        if ($('#ddlAgent').val() <= 0) {
                            $('#s2id_ddlAgent').css("border", "1px solid #D8000C");
                            return;
                        }
                        else {
                            $('#s2id_ddlAgent').css("border", "");
                        }
                        if ($('#ddlAgent').val() > 0 && $('#ddlLocation').val() <= 0) {
                            $('#s2id_ddlLocation').css("border", "1px solid #D8000C");
                            return;
                        }
                        else {
                            $('#s2id_ddlLocation').css("border", "");
                        }
                    }
                }
                if ($('#txtCity').val() == '') {
                    $('#txtCity').css("border", "1px solid #D8000C");
                    return;
                }
                else {
                    $('#txtCity').css("border", "");
                }
                if ($('#txtCheckin').val() == '') {
                    $('#txtCheckin').css("border", "1px solid #D8000C");
                    return;
                }
                else {
                    $('#txtCheckin').css("border", "");
                }
                if ($('#txtCheckout').val() == '') {
                    $('#txtCheckout').css("border", "1px solid #D8000C");
                    return;
                }
                else {
                    $('#txtCheckout').css("border", "");
                }

                if ($('#ddlNationality').val() == '-1') {
                    ;
                    $('#s2id_ddlNationality').css("border", "1px solid #D8000C");
                    return;
                }
                else {
                    $('#s2id_ddlNationality').css("border", "");
                }
                if ($('#ddlResidence').val() == '-1') {
                    $('#s2id_ddlResidence').css("border", "1px solid #D8000C");
                    return;
                }
                else {
                    $('#s2id_ddlResidence').css("border", "");
                }
                if ($('#ddlRating').val() <= '0') {
                    $('#s2id_ddlRating').css("border", "1px solid #D8000C");
                    return;
                }
                else {
                    $('#s2id_ddlRating').css("border", "");
                }
                Request = [];
                var roomGuest = [];
                var noofAdults; var noofChilds; var childAges = [];
                for (var i = 1; i <= count; i++) {
                    var ages = '';
                    noofChilds = $('#chdRoom-' + i).val();
                    childAges = [];
                    if (noofChilds > 0) {
                        for (var j = 1; j <= noofChilds; j++) {

                            if ($('#ChildBlock-' + i + '-ChildAge-' + j).val() == "-1") {
                                document.getElementById('errMess').style.display = "block";
                                document.getElementById('errorMessage').innerHTML = "Please Select child" + j + " age of room " + i;
                                return false;
                            }
                            else {
                                document.getElementById('errMess').style.display = "none";
                                if (ages != '') {
                                    ages = ages + "|" + $('#ChildBlock-' + i + '-ChildAge-' + j).val();
                                } else {
                                    ages = $('#ChildBlock-' + i + '-ChildAge-' + j).val();
                                }
                            }
                        }
                    }
                    if (ages != '') {
                        childAges = ages.split('|');
                    }
                    roomGuest.push({
                        noOfAdults: $('#adtRoom-' + i).val(),
                        noOfChild: noofChilds,
                        childAge: childAges.length > 0 ? childAges : null
                    })
                }
                if ($('#ddlSerchbyHotel').val() <= '0') {
                    $('#s2id_ddlSerchbyHotel').css("border", "1px solid #D8000C");
                    return;
                }
                else {
                    if ($('#txtHtlPhone').val() == '') {
                        $('#txtHtlPhone').css("border", "1px solid #D8000C");
                        return;
                    }
                    else if ($('#txtHtlAddress').val() == '') {
                        $('#txtHtlAddress').css("border", "1px solid #D8000C");
                        return;
                    }
                    else {
                        $('#s2id_ddlSerchbyHotel').css("border", "");
                        $('#txtHtlPhone').css("border", "");
                        $('#txtHtlAddress').css("border", "");
                    }
                }
                if ($('#txtSupplier').val() == '') {
                    $('#txtSupplier').css("border", "1px solid #D8000C");
                    return;
                }
                else {
                    $('#txtSupplier').css("border", "");
                }

                if ($('#ddlSupplierCurrency').val() <= "0") {
                    $('#s2id_ddlSupplierCurrency').css("border", "1px solid #D8000C");
                    return;
                }
                else {
                    $('#s2id_ddlSupplierCurrency').css("border", "");
                }           
                Request.push({
                    cityName: CityName,
                    noOfRooms: count,
                    hotelName: ($('#ddlSerchbyHotel') != null && $('#ddlSerchbyHotel').val() > 0) ? $('#ddlSerchbyHotel option:selected').text() : "",
                    rating: $('#ddlRating').val() > 0 ? $('#ddlRating').val() : 0,
                    currency: ($("#ddlSupplierCurrency option:selected").text() != "Supplier Currency") ? $("#ddlSupplierCurrency option:selected").text() : "AED",
                    startDate: ConvertDate($('#txtCheckin').val()),
                    endDate: ConvertDate($('#txtCheckout').val()),
                    countryName: CountryName,
                    roomGuest: roomGuest,
                    passengerNationality: $('#ddlNationality').val(),
                    passengerCountryOfResidence: $('#ddlResidence').val()
                });
                loadControls();
                if (loadRoomrates()) {
                    document.getElementById('add_hotel_details_con').style.display = "block";
                    $('#btnClear').show(); 
                    //Added by Anji displaying continue and submit button.
                    if ($('#ddlAgent').val() > 0)
                        $('#btnSubmit').show();
                    else
                        $('#btnContinue').show();
                    //End
                    $('#txtAgentAmount').val('0.00');
                    $('#txtInVat').val('0.00');
                    $('#lblTtlOutVat').val('OUTPUT VAT');
                    $('#txtOutVat').val('0.00');
                    $('#txtTotalVat').val('0.00');
                    $('#txtNetPay').val('0.00');
                    $('#txtProfit').val('0.00');
                    $('#txtCollected').val('0.00');
                     $('#txtToCollected').val('0');
                    $.ajax({
                        type: "POST",
                        url: "OfflineHotelBooking.aspx/getVatDetails", //It calls our web method  
                        dataType: "json",
                        contentType: "application/json; charset=utf-8",
                        data: "{'countryName':'" + CountryName + "'}",
                        //async: false,
                        success: function (data) {
                            vatdetails = JSON.parse(data.d);
                        },
                        error: function (d) {
                        }
                    });
                    disableSearchParameters(true);
                }
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }

        }

        function submit() {
            try {
                if (validatePassanger() && validateRoomRates() && validateAgentRoomRates() && validateCancelPolicies() && validateFlexFields()) {
                    createItinerary();
                    $("#ctl00_upProgress").show();
                    var itinerary = JSON.stringify(HotelItinerary[0]).replace('&', '').replace('#', '').replace('<', '').replace('>', '').replace('%', '').replace(/'/g, '').replace(/\\"/g, '');
                    var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                    var data = JSON.parse(AjaxCalls("OfflineHotelBooking.aspx/createBooking", "{'hotelItinerary':'" + itinerary + "','hotelrequest':'" + JSON.stringify(Request[0]) + "','Settlement':'" + ExistingSettlement +"'}"));
                    //var response = JSON.parse(data.d);
                    if (data != null && !IsEmpty(data["ConfirmationNo"])) {
                        //window.location.href = 'APIHotelPaymentVoucher.aspx?SE=Y&ConfNo=' + data["ConfirmationNo"];
                        AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'APIHotelPaymentVoucher', 'sessionData':'" + data["ConfirmationNo"] + "', 'action':'set'}");
                        window.location.href = 'APIHotelPaymentVoucher.aspx';
                    }
                    else {
                        $("#ctl00_upProgress").hide();
                        var msg;
                        if (data != null && !IsEmpty(data["Error"])) {
                            msg = data["Error"];
                        }
                        else {
                            msg = "Booking may or may not be happened please contact with Admin";
                        }
                        data = exceptionSaving(msg);
                        window.location.href = "ErrorPage.aspx?Err=" + msg;
                    }
                }
            } catch (e) {
                $("#ctl00_upProgress").hide();
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function validatePassanger() {
            var submit = true;
            var Hotelrequest = Request[0];
            document.getElementById('errMess').style.display = "none";
            for (var i = 0; i < Hotelrequest.noOfRooms; i++) {
                for (var j = 1; j <= Hotelrequest.roomGuest[i].noOfAdults; j++) {
                    if ($('#ddlpaxTitle-' + i + j).val() == '0') {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Select Adult" + j + " title of room " + (i + 1);
                        submit = false;
                        break;
                    }
                    if ($('#txtFirstName-' + i + j).val() == '') {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please enter Adult" + j + " FirstName of room " + (i + 1);
                        submit = false;
                        break;
                    }
                    if ($('#txtLastName-' + i + j).val() == '') {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please enter Adult" + j + " LastName of room " + (i + 1);
                        submit = false;
                        break;
                    }
                    if ($('#txtMobileNumber-' + i + j).val() == '') {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please enter Adult" + j + " MobileNumber of room " + (i + 1);
                        submit = false;
                        break;
                    }
                    if ($('#txtEmail-' + i + j).val() == '') {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please enter Adult" + j + " Email of room " + (i + 1);
                        submit = false;
                        break;
                    } else {
                        var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                        if (!emailReg.test($('#txtEmail-' + i + j).val())) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Adult" + j + " Email of room " + (i + 1) + " is not valid mail";
                            submit = false;
                            break;
                        }
                    }
                }
                for (var k = 1; k <= Hotelrequest.roomGuest[i].noOfChild; k++) {
                    if (submit == true) {
                        if ($('#ddlpaxTitleC-' + i + k).val() == '0') {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Please Select Child" + k + " title of room " + (i + 1);
                            submit = false;
                            break;
                        }
                        if ($('#txtFirstNameC-' + i + k).val() == '') {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Please enter Child" + k + " FirstName of room " + (i + 1);
                            submit = false;
                            break;
                        }
                        if ($('#txtLastNameC-' + i + k).val() == '') {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Please enter Child" + k + " LastName of room " + (i + 1);
                            submit = false;
                            break;
                        }
                        //if ($('#txtMobileNumberC-' + i + j).val() == '') {
                        //    document.getElementById('errMess').style.display = "block";
                        //    document.getElementById('errorMessage').innerHTML = "Please enter Child" + j + " MobileNumber of room " + (i + 1);
                        //    submit = false;
                        //}
                        //if ($('#txtEmail-' + i + j).val() == '') {
                        //    document.getElementById('errMess').style.display = "block";
                        //    document.getElementById('errorMessage').innerHTML = "Please enter Child" + j + " Email of room " + (i + 1);
                        //    submit = false;
                        //} else {
                        //    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
                        //    if (!emailReg.test($('#txtEmail-' + i + j).val())) {
                        //        document.getElementById('errMess').style.display = "block";
                        //        document.getElementById('errorMessage').innerHTML = "Child" + j + " Email of room " + (i + 1) + " is not valid mail";
                        //        submit = false
                        //    }
                        //}
                    }
                }
            }
            return submit;
        }
        function validateRoomRates() {
            var submit = true;
            var Hotelrequest = Request[0];
            document.getElementById('errMess').style.display = "none";
            for (var i = 1; i <= Hotelrequest.noOfRooms; i++) {
                if ($('#ddlRoomTypes' + i).val() == '-1') {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Room" + i + " RoomTypes ";
                    submit = false;
                    break;
                }

                if ($('#ddlRoomMealPlans' + i).val() == '-1') {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Room" + i + " MealPlans ";
                    submit = false;
                    break;
                }
                if ($('#txtRmSuppRate' + i).val() == '' || $('#txtRmSuppRate' + i).val() < '0') {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter Supplier Amount for room" + (i);
                    submit = false;
                    break;
                }


                if ($('#txtAgtRmExrate' + i).val() == '' || $('#txtAgtRmExrate' + i).val() < '0') {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "There is no exchange Rates of Room" + (i);
                    submit = false;
                    break;
                }

                if ($('#txtAgtRmRate' + i).val() == '' || $('#txtAgtRmRate' + i).val() < '0') {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select Agent Room Wise Rates of Room" + (i);
                    submit = false;
                    break;
                }
            }
            return submit;
        }
        function validateAgentRoomRates() {
            var submit = true;
            if ($('#txtAgentAmount').val() == '0.00' || $('#txtCollected').val() == '0.00' || $('#txtNetPay').val() == '0.00') {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Agent Amount Should not be Empty-Please select Agent Room Wise Amounts ";
                submit = false;
            }
             var collecedAmt = document.getElementById('txtCollected').value;
                        var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();                        
                        var currentSum = 0;
                        if (ExistingSettlement != null && ExistingSettlement != "") {
                            let ExistingJson = JSON.parse(ExistingSettlement);
                            $.grep(ExistingJson.SettlementObject, function (item) {
                                currentSum = parseFloat(parseFloat(currentSum) + parseFloat(item.Amount));
                            });
                        }
                        if (parseFloat(collecedAmt.replace(/[^0-9\.]/g, '')) > parseFloat(currentSum)) {
                            toastr.error('Please check settlement amount and collected amount');
                            submit = false;
                        }

            return submit;
        }
        function validateCancelPolicies() {
            var submit = true;
            if ($('#hdnCancelCount').val() > 0) {
                for (var index = 1; index <= $('#hdnCancelCount').val(); index++) {
                    if ($('#txtCancelDays' + index).val() == '') {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please select cancellation before days";
                        submit = false;
                        break;
                    }
                    if ($('#txtCancelNights' + index).val() == '' && $('#txtCancelPercent' + index).val() == '') {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please enter any  cancellation charge type";
                        submit = false;
                        break;
                    }
                }
            }
            return submit;
        }

        function validateFlexFields() {
            var submit = true;
            if (ValidateFlex('', false)) {
            }
            else {
                submit = false;
            }
            return submit;
        }
        function navigateNextTab() {

            try {
                document.getElementById('errMess').style.display = "none";
                if (validatePassanger() && validateRoomRates() && validateAgentRoomRates() && validateCancelPolicies()) {
                    $('#paxTabDiv').removeClass('active in');
                    $('#Tabs li:first').removeClass('active');
                    $('#btnContinue').hide();

                    $('#btnSubmit').show();

                    $('#UDIDDiv').addClass('active in');
                    $('#Tabs li:last').addClass('active');
                }

            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }

        }
        function createItinerary() {
            HotelItinerary = [];
            var Hotelrequest = Request[0];
            var Price = [];
            HotelRoom = [];
            Price = [];

            suppliers = $('#ddlOnlineSuppliers').val() > 0 ? $('#ddlOnlineSuppliers option:selected').text() : $('#ddlOfflineSuppliers option:selected').text();
            for (var i = 0; i < Hotelrequest.noOfRooms; i++) {
                PassenegerInfo = [];
                var pricetype = '<%=CT.BookingEngine.PriceType.NetFare%>';
                Price.push({
                    Markup: HotelRoomsDetails[i].Markup,
                    MarkupValue: HotelRoomsDetails[i].MarkupValue,
                    MarkupType: HotelRoomsDetails[i].MarkupType,
                    InputVATAmount: HotelRoomsDetails[i].InputVATAmount,
                    OutputVATAmount: $('#txtRmOutVat' + (i + 1)).val(),
                    TaxDetails: HotelRoomsDetails[i].TaxDetail,
                    SupplierPrice: HotelRoomsDetails[i].supplierPrice,
                    SupplierCurrency: $('#ddlSupplierCurrency option:selected').text(),
                    RateOfExchange: exchangeRate,
                    AccPriceType: pricetype,
                    <%--CurrencyCode: '<%=Settings.LoginInfo.Currency %>',
                    Currency: '<%=Settings.LoginInfo.Currency %>',--%>
                    CurrencyCode: LoginInfo.Currency,
                    Currency: LoginInfo.Currency,
                    NetFare: HotelRoomsDetails[i].SellingFare,
                    AsvAmount: $('#txtAgtRmAddlmkpAmount' + (i + 1)).val(),
                    AsvType: $('#txtAgtRmAddlmkpType' + (i + 1)).val(),
                    AsvElement: $('#txtAgtRmAddlmkpValue-' + (i + 1)).val()
                })



                for (var j = 1; j <= Hotelrequest.roomGuest[i].noOfAdults; j++) {
                    var Leadpass;
                    if (i == 0 && j == 1) { Leadpass = true } else { Leadpass = false; }
                    PassenegerInfo.push({
                        PaxId: 0,
                        HotelId: 0,
                        Title: $('#ddlpaxTitle-' + i + j).val(),
                        Firstname: $('#txtFirstName-' + i + j).val(),
                        Middlename: "",
                        Lastname: $('#txtLastName-' + i + j).val(),
                        Phoneno: $('#txtMobileNumber-' + i + j).val(),
                        Countrycode: Hotelrequest.CountryCode,
                        NationalityCode: Hotelrequest.CountryName,
                        Areacode: "",
                        email: $('#txtEmail-' + i + j).val(),
                        Addressline1: "",
                        Addressline2: "",
                        city: "",//$('#txtAdultCity-' + i + j).val(),
                        Zipcode: "",
                        State: "",
                        Country: Hotelrequest.PassengerCountryOfResidence,
                        Nationality: Hotelrequest.PassengerNationality,
                        PaxType: "Adult",
                        RoomId: 0,
                        LeadPassenger: Leadpass,
                        Age: 0,
                        Employee: "",
                        Division: "",
                        Department: "",
                        Purpose: "",
                        EmployeeID: "",
                        IsGST: false,
                        HotelPassengerGST: {},
                        FlexDetailsList: !IsEmpty(defaultFlexDetails) && Leadpass ? GetPaxFlexInfo('0', false, '2') : []
                    })

                }
                for (var k = 1; k <= Hotelrequest.roomGuest[i].noOfChild; k++) {
                    var AgeArr = Hotelrequest.roomGuest[i].childAge;
                    var Age = AgeArr[k - 1];
                    PassenegerInfo.push({
                        PaxId: 0,
                        HotelId: 0,
                        Title: $('#ddlpaxTitleC-' + i + k).val(),
                        Firstname: $('#txtFirstNameC-' + i + k).val(),
                        Middlename: "",
                        Lastname: $('#txtLastNameC-' + i + k).val(),
                        Phoneno: "",
                        Countrycode: Hotelrequest.CountryCode,
                        NationalityCode: Hotelrequest.CountryName,
                        Areacode: "",
                        Email: "",
                        Addressline1: "",
                        Addressline2: "",
                        City: "",
                        Zipcode: "",
                        State: "",
                        Country: Hotelrequest.PassengerCountryOfResidence,
                        Nationality: Hotelrequest.PassengerNationality,
                        PaxType: "Child",
                        RoomId: 0,
                        LeadPassenger: false,
                        Age: Age,
                        Employee: "",
                        Division: "",
                        Department: "",
                        Purpose: "",
                        EmployeeID: "",
                        IsGST: false,
                        HotelPassengerGST: {}
                    });

                }

                HotelRoom.push({
                    RoomName: document.getElementById("ddlRoomTypes" + (i + 1)).options[document.getElementById("ddlRoomTypes" + (i + 1)).selectedIndex].text,
                    MealPlanDesc: document.getElementById("ddlRoomMealPlans" + (i + 1)).options[document.getElementById("ddlRoomMealPlans" + (i + 1)).selectedIndex].text,
                    RatePlanCode: "R1",
                    Ameneties: "",
                    RoomTypeCode: $('#ddlRoomTypes' + (i + 1)).val(),
                    AdultCount: Hotelrequest.roomGuest[i].noOfAdults,
                    ChildCount: Hotelrequest.roomGuest[i].noOfChild,
                    ChildAge: Hotelrequest.roomGuest[i].childAge,
                    NoOfUnits: "1",
                    RateType:'<%=CT.BookingEngine.RoomRateType.DOTW%>',
                    Price: Price[i],
                    PassenegerInfo: PassenegerInfo,
                    Gxsupplier: suppliers
                })
            }
            var cancelpolicyMsg = '';
            try {
                if ($('#hdnCancelCount').val() > 0) {
                    for (var index = 1; index <= $('#hdnCancelCount').val(); index++) {
                        if ($('#txtCancelDays' + index) != null && $('#txtCancelDays' + index).val() > 0) {
                            var dt = new Date(ConvertDate($("#txtCheckin").val()));
                            dt.setDate(dt.getDate() - $('#txtCancelDays' + index).val());
                            var msg = "cancellation policy is appled before " + dt.format(dt.getMonth() + 1 + "/" + dt.getDate() + "/" + dt.getFullYear()) + " you will be charged";
                            cancelpolicyMsg = cancelpolicyMsg == '' ? msg : cancelpolicyMsg + "|" + msg;
                        }
                        if ($('#txtCancelDays' + index) != null && $('#txtCancelDays' + index).val() > 0 && $('#rbtnCancelNights' + index) != null && $('#rbtnCancelNights' + index).is(':checked') == true) {
                            cancelpolicyMsg = cancelpolicyMsg + "," + $('#txtCancelNights' + index).val() + " nights of amount";
                        }
                        if ($('#txtCancelDays' + index) != null && $('#txtCancelDays' + index).val() > 0 && $('#rbtnCancelPercent' + index) != null && $('#rbtnCancelPercent' + index).is(':checked') == true) {
                            cancelpolicyMsg = cancelpolicyMsg + "," + $('#txtCancelPercent' + index).val() + " % of amount";
                        }
                    }
                }
                else {
                    if ((getElement('rbtnNonRefund').checked)) {
                        cancelpolicyMsg = "Non-refundable and full charge will apply once booking is completed.";
                    }

                }
                var locationId = $('#ddlLocation').val() > 0 ? $('#ddlLocation option:selected').val() : LoginInfo.LocationID;
                HotelItinerary.push({
                    AgencyId: LoginInfo.AgentId,
                    EndDate: Hotelrequest.endDate,
                    StartDate: Hotelrequest.startDate,
                    HotelCode: $('#ddlSerchbyHotel').val(),
                    HotelAddress1: $('#txtHtlAddress').val(),
                    HotelAddress2: $('#txtHtlPhone').val(),
                    HotelName: Hotelrequest.hotelName,
                    NoOfRooms: Hotelrequest.noOfRooms,
                    Rating: Hotelrequest.rating,
                    CityRef: Hotelrequest.cityName,
                    Map: '',
                    HotelCancelPolicy: cancelpolicyMsg,
                    Remarks: $('#txtRemarks').val(),
                    SpecialRequest: '',
                    VoucherStatus: true,
                    LastCancellationDate: Hotelrequest.startDate,
                    //LocationId: LoginInfo.LocationID,
                    LocationId: locationId,
                    //AgencyId: LoginInfo.AgentId,
                    IsDomestic: false,
                    Roomtype: HotelRoom,
                    ConfirmationNo: $('#txtSupplier').val(),
                    TotalPrice: eval($('#txtAgentAmount').val()) + eval($('#txtInVat').val()),
                    HotelPolicyDetails: $('#txtTerms').val(),
                })
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }

        }

        //Creating Dynamic Controls based on Noof Room ,Noof adults,Childs
        function loadControls() {
            try {
                var Hotelrequest = Request[0];
                var GuestAdult = $('#Adult').html();
                var Count = 0; var ChildCount = 0; var TotalPax = 1;
                $('#ListGuest').empty();
                for (var i = 0; i < Hotelrequest.noOfRooms; i++) {
                    for (var j = 1; j <= Hotelrequest.roomGuest[i].noOfAdults; j++) {
                        $('#ListGuest').append('<li id="ListAdult' + i + j + '" class="ui-bg-wrapper mb-1  ui-room-wrapper">' + GuestAdult + '</li>');
                        Count++;

                        var Title = ' <label>Title<font color="red">*</font></label><select class="form-control custom-select PDrequired" id="ddlpaxTitle-' + i + j +
                            '" onchange="select(this.id);"><option value="0">Select Title</option> <option value="Mr">Mr</option> <option value="Mrs">Mrs</option></select>';
                        $('#RoomCount').attr('id', 'RoomCount-' + i + j);
                        $('#RoomCount-' + i + j).attr("style", "display:None");
                        if ((i == 0 && j == 1) || (i == 1 && j == 1) || (i == 2 && j == 1) || (i == 3 && j == 1)) {
                            $('#RoomCount').attr('id', 'RoomCount-' + i + j);
                            $('#RoomCount-' + i + j).attr("style", "display:block");
                            $('#RoomCount-' + i + j).text('Room-' + (i + 1));
                            //  $('#RoomCount').attr('id', 'RoomCount-' + i + j);

                        }
                        $('#h4PaxType').attr('id', 'h4PaxType-' + i + j);
                        if (i == 0 && j == 1) {
                            $('#h4PaxType-' + i + j).text('ADULT-' + Count + '(LEAD GUEST)');
                        }
                        else {
                            $('#h4PaxType-' + i + j).text('GUEST ADULT-' + Count);
                        }
                        $('#divpaxTitle').attr('id', 'divpaxTitle-' + i + j);
                        $('#divpaxTitle-' + i + j).append(Title);
                        var firstname = '<label>First Name<font color="red">*</font></label><input type="text" class="form-control" placeholder="First Name" maxlength="50"  id="txtFirstName-' + i + j + '">    '
                        $('#divpaxFirstName').attr('id', 'divpaxFirstName-' + i + j);
                        $('#divpaxFirstName-' + i + j).append(firstname);

                        var lastname = '<label>Last Name<font color="red">*</font></label><input type="text" class="form-control" placeholder="Last Name" maxlength="50" id="txtLastName-' + i + j + '">    '
                        $('#divpaxLastName').attr('id', 'divpaxLastName-' + i + j);
                        $('#divpaxLastName-' + i + j).append(lastname);

                        var mobileno = '<label>Mobile Number<font color="red">*</font></label><input type="text" class="form-control" placeholder="Enter Mobile No" onkeypress="return isNumber(event);" maxlength="50" id="txtMobileNumber-' + i + j + '">    '
                        $('#divpaxMobileNo').attr('id', 'divpaxMobileNo-' + i + j);
                        $('#divpaxMobileNo-' + i + j).append(mobileno);

                        var email = '<label>Email<font color="red">*</font></label><input type="text" class="form-control" maxlength="50" placeholder="Enter Email" id="txtEmail-' + i + j + '">    '
                        $('#divpaxEmail').attr('id', 'divpaxEmail-' + i + j);
                        $('#divpaxEmail-' + i + j).append(email);


                    }
                    for (var k = 1; k <= Hotelrequest.roomGuest[i].noOfChild; k++) {


                        $('#ListGuest').append('<li id="ListAdultC' + i + k + '" class="ui-bg-wrapper mb-1  ui-room-wrapper">' + GuestAdult + '</li>');
                        ChildCount++;

                        var Title = ' <label>Title<font color="red">*</font></label><select class="form-control custom-select PDrequired" id="ddlpaxTitleC-' + i + k +
                            '" onchange="select(this.id);"><option value="0">Select Title</option> <option value="Mr">Mr</option> <option value="Mrs">Mrs</option></select>';

                        $('#RoomCount').attr('id', 'RoomCountC-' + i + j);
                        $('#RoomCountC-' + i + j).attr("style", "display:None");
                        $('#h4PaxType').attr('id', 'h4PaxTypeC-' + i + k);
                        $('#h4PaxTypeC-' + i + k).text('GUEST CHILD-' + ChildCount);

                        $('#divpaxTitle').attr('id', 'divpaxTitleC-' + i + k);
                        $('#divpaxTitleC-' + i + k).append(Title);
                        var firstname = '<label>First Name<font color="red">*</font></label><input type="text" class="form-control" placeholder="First Name" id="txtFirstNameC-' + i + k + '">    '
                        $('#divpaxFirstName').attr('id', 'divpaxFirstNameC-' + i + k);
                        $('#divpaxFirstNameC-' + i + k).append(firstname);

                        var lastname = '<label>Last Name<font color="red">*</font></label><input type="text" class="form-control" placeholder="Last Name" id="txtLastNameC-' + i + k + '">    '
                        $('#divpaxLastName').attr('id', 'divpaxLastNameC-' + i + k);
                        $('#divpaxLastNameC-' + i + k).append(lastname);

                        var mobileno = '<label>Mobile Number</label><input type="text" class="form-control" placeholder="Enter Mobile No" id="txtMobileNumberC-' + i + k + '" onkeypress="return isNumber(event);">    '
                        $('#divpaxMobileNo').attr('id', 'divpaxMobileNoC-' + i + k);
                        $('#divpaxMobileNoC-' + i + k).append(mobileno);

                        var email = '<label>Email</label><input type="text" class="form-control" placeholder="Enter Email" id="txtEmailC-' + i + k + '">    '
                        $('#divpaxEmail').attr('id', 'divpaxEmailC-' + i + k);
                        $('#divpaxEmailC-' + i + k).append(email);


                    }
                }
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        //Creating Dynamic Controls based on Noof Rooms and no of nights
        function loadRoomrates() {
            var isExchangeRates = true;
            $('#ListRoomRates').empty();
            $('#ListAgentRoomRates').empty();
            var currency = $("#ddlSupplierCurrency option:selected").text();
            var Agentid = $('#ddlAgent').val() > 0 ? $('#ddlAgent').val() : '';           
            var locationId=$('#ddlLocation').val() > 0 ? $('#ddlLocation').val() : '';
            try {
                var data = JSON.parse(AjaxCalls("OfflineHotelBooking.aspx/getExchangeRates", "{'currency':'" + currency + "','agentId':'" + Agentid + "','locationId':'" + locationId + "'}"));
                if (data.exchangeRates == '' || data.exchangeRates == 0)
                {
                    $('#lstError').show();
                    $('#lblError').text('No exchange rates for selected Currency please contact with Admin');
                    isExchangeRates = false;
                }
                else {
                    $('#lstError').hide();
                    exchangeRate = data.exchangeRates;
                    markupType = data.markupType;
                    markupValue = data.markup;
                    LoginInfo.OnBehalfAgentLocation = data.Location;
                    var Supplierrates = $('#roomRate').html();
                    var Agentrates = $('#roomAgentRate').html();
                    var nights = 2;
                    var Hotelrequest = Request[0];
                    for (var i = 1; i <= Hotelrequest.noOfRooms; i++) {
                        var options = "";
                        //Supplier Room Rates
                        $('#ListRoomRates').append('<li id="ListRoomWiseRates' + i + + '" class="ui-bg-wrapper mb-1  ui-room-wrapper">' + Supplierrates + '</li>');
                        $('#lblRmRateCount').attr('id', 'lblRmRateCount' + i);
                        $('#ddlRoomTypes').attr('id', 'ddlRoomTypes' + i);
                        $.each(roomTypes, function (index, item) {
                            $('#ddlRoomTypes' + i).append('<option value="' + item.roomTypeCode + '">' + item.roomName + '</option>');
                        });
                        options = "";
                        $('#ddlRoomMealPlans').attr('id', 'ddlRoomMealPlans' + i);
                        $.each(mealPlans, function (index, item) {
                            options += "<option value='" + item.mealPlanCode + "'>" + item.MealPlanDesc + "</option>";
                        });
                        $('#ddlRoomMealPlans' + i).append(options);
                        $('#s2id_ddlRoomTypes').attr('id', 's2id_ddlRoomTypes' + i);
                        $('#s2id_ddlRoomTypes' + i).attr("style", "display:none");
                        $('#s2id_ddlRoomMealPlans').attr('id', 's2id_ddlRoomMealPlans' + i);
                        $('#s2id_ddlRoomMealPlans' + i).attr("style", "display:none");
                        $('#ddlRoomTypes' + i).attr("style", "display:block");
                        $('#ddlRoomMealPlans' + i).attr("style", "display:block");
                        $('#lblRmRateCount' + i).text('Room ' + i);
                        $('#txtRmSuppRate').attr('id', 'txtRmSuppRate' + i);
                        $('#txtRmSuppRate' + i).attr('onkeypress', 'return isNumber(event);'); 
                        //Agent Room Rates
                        $('#ListAgentRoomRates').append('<li id="ListAgentRoomWiseRates' + i + + '" class="ui-bg-wrapper mb-1  ui-room-wrapper">' + Agentrates + '</li>');
                        $('#lblRmAgentRateCount').attr('id', 'lblRmAgentRateCount' + i);
                        $('#lblRmAgentRateCount' + i).text('Room ' + i);
                        $('#txtAgtRmExrate').attr('id', 'txtAgtRmExrate' + i);
                        $('#txtAgtRmCurrency').attr('id', 'txtAgtRmCurrency' + i);
                        $('#txtAgtRmCurrency' + i).val(LoginInfo.Currency);
                        $('#txtAgtRmRate').attr('id', 'txtAgtRmRate' + i);


                        $('#txtAgtRmMkpType').attr('id', 'txtAgtRmMkpType' + i);
                        $('#txtAgtRmMkpValue').attr('id', 'txtAgtRmMkpValue' + i);
                        $('#txtAgtRmMkpAmount').attr('id', 'txtAgtRmMkpAmount' + i);
                        $('#txtAgtRmAddlmkpType').attr('id', 'txtAgtRmAddlmkpType' + i);
                        $('#txtAgtRmAddlmkpValue').attr('id', 'txtAgtRmAddlmkpValue-' + i);
                        $('#txtAgtRmAddlmkpValue-' + i).attr('onchange', 'BindAddlMarkUp(this.id);');
                        $('#txtAgtRmAddlmkpValue-' + i).attr('onkeypress', 'return isNumber(event);');
                        $('#txtAgtRmAddlmkpAmount').attr('id', 'txtAgtRmAddlmkpAmount' + i);
                        $('#txtAgtRmAddlmkpAmount' + i).attr('onchange', 'BindAgentRoomRates();');
                        $('#txtAgtRmAddlmkpType' + i).val('F');
                        $('#txtAgtRmAddlmkpValue-' + i).val(0);
                        $('#txtAgtRmAddlmkpAmount' + i).val(0);
                        $('#lblOutVat').attr('id', 'lblOutVat' + i);
                        $('#lblOutVat' + i).text(LoginInfo.OnBehalfAgentLocation == "IN" ? "GST Amount" : "Output Vat");
                        $('#txtRmInVat').attr('id', 'txtRmInVat' + i);
                        $('#txtRmOutVat').attr('id', 'txtRmOutVat' + i);
                    }
                }
                return isExchangeRates;
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function clearControls() {
            try {
                var Hotelrequest = Request[0];
                disableSearchParameters(false);
                LoginInfo.AgentId = defaultAgentInfo.AgentId;
                LoginInfo.LocationID = defaultAgentInfo.LocationID;
                LoginInfo.Decimal = defaultAgentInfo.Decimal;
                LoginInfo.OnBehalfAgentLocation = defaultAgentInfo.OnBehalfAgentLocation;
                $('#ddlAgent').val('-1');
                $('#ddlAgent').select2();
                $("#ddlLocation option").remove();
                var $chkbox = $('#s2id_ddlLocation').find('span');
                $chkbox[0].innerText = "Select Location";
                // $("#s2id_ddlLocation option").remove();
                var options = "<option value='" + -1 + "'>Select Location</option>";
                $('#ddlLocation').append(options);
                //$("#s2id_ddlLocation").append(options);
                // document.getElementById("tglAgentCtr").disabled = true;
                $('#txtCity').val('');
                $('#txtCheckin').val('');
                $("#txtCheckout").val('');
                $("#ddlNationality").val('-1');
                $("#ddlNationality").select2();
                $("#ddlResidence").val('-1');
                $("#ddlResidence").select2();
                $("#roomCount").val('1');
                var childcount = document.getElementById('chdRoom-1').value
                document.getElementById('ChildBlock-1').style.display = 'none';
                for (var c = 1; c <= childcount; c++) {
                    document.getElementById('ChildBlock-1' + '-ChildAge-' + c).value = '-1';
                    document.getElementById('ChildBlock-1' + '-ChildAge-' + c).style.display = 'none';
                    document.getElementById('ChildBlock-1' + '-Child-' + c).style.display = 'none';
                }
                document.getElementById('PrevChildCount-1').value = 0;
                $("#chdRoom-1").val('0');
                ShowRoomDetails();

                $("#roomCount").select2();
                $("#htlTotalPax").val('1 Room, 1 Adult');

                $("#ddlOnlineSuppliers").val('-1');
                $("#ddlOfflineSuppliers").val('-1');
                $("#ddlOnlineSuppliers").select2();
                $("#ddlOfflineSuppliers").select2();
                $("#ddlSupplierCurrency").val('-1');
                $("#ddlSupplierCurrency").select2();
                $("#s2id_ddlLocation option").remove();
                $("#s2id_ddlSerchbyHotel option").remove();
                $("#ddlSerchbyHotel option").remove();
                options = "";
                options = "<option value='" + 0 + "'>Search by Hotel Name</option>";
                $("#ddlSerchbyHotel").append(options);
                var $chkbox = $('#s2id_ddlSerchbyHotel').find('span');
                $chkbox[0].innerText = "Search by Hotel Name";
                $("#txtSupplier").val('');
                $("#ddlRating").val('0');
                $("#ddlRating").select2();
                $('#txtHtlPhone').val('');
                $('#txtHtlAddress').val('');
                $('#txtSupplier').val('');
                $('#divHotelAddress').hide();
                for (var i = 0; i < Hotelrequest.noOfRooms; i++) {
                    for (var j = 1; j <= Hotelrequest.roomGuest[i].noOfAdults; j++) {
                        $('#ddlpaxTitle-' + i + j).val('0');
                        $('#ddlpaxTitle-' + i + j).select2();
                        $('#txtFirstName-' + i + j).val('');
                        $('#txtFirstName-' + i + j).val('');
                        $('#txtLastName-' + i + j).val('');
                        $('#txtMobileNumber-' + i + j).val('');
                        $('#txtEmail-' + i + j).val('');
                    }
                    for (var j = 1; j <= Hotelrequest.roomGuest[i].noOfChild; j++) {
                        $('#ddlpaxTitleC-' + i + j).val('0');
                        $('#ddlpaxTitleC-' + i + j).select2();
                        $('#txtFirstNameC-' + i + j).val('');
                        $('#txtFirstNameC-' + i + j).val('');
                        $('#txtLastNameC-' + i + j).val('');
                        $('#txtMobileNumberC-' + i + j).val('');
                        $('#txtEmailC-' + i + j).val('');
                    }
                    $('#ddlRoomTypes' + eval(i + 1)).val('-1');
                    $('#ddlRoomTypes1' + eval(i + 1)).select2();
                    $('#ddlRoomMealPlans' + eval(i + 1)).val('-1');
                    $('#ddlRoomMealPlans' + eval(i + 1)).select2();
                    $('#txtRmSuppRate' + eval(i + 1)).val('');
                    $('#txtAgtRmExrate' + eval(i + 1)).val('');

                    $('#txtAgtRmRate' + eval(i + 1)).val('');
                    $('#txtAgtRmMkpType' + eval(i + 1)).val('');
                    $('#txtAgtRmMkpValue' + eval(i + 1)).val('');
                    $('#txtAgtRmMkpAmount' + eval(i + 1)).val('');
                    $('#txtAgtRmAddlmkpType' + eval(i + 1)).val('F');
                    $('#txtAgtRmAddlmkpValue-' + eval(i + 1)).val('0');
                    $('#txtAgtRmAddlmkpAmount' + eval(i + 1)).val('0');
                    $('#txtRmInVat' + eval(i + 1)).val('');
                    $('#txtRmOutVat' + eval(i + 1)).val('');
                     $('#lblOutVat' + eval(i + 1)).text('Output Vat');
                }
                $('#txtAgentAmount').val('0.00');
                $('#txtInVat').val('0.00');
                $('#txtOutVat').val('0.00');
                $('#lblTtlOutVat').text('OUTPUT VAT');
                $('#txtTotalVat').val('0.00');
                $('#txtCollected').val('0.00');
                $('#txtNetPay').val('0.00');
                $('#txtProfit').val('0.00');
                $('#txtTerms').val('');
                $('#txtRemarks').val('');
                $('#ListCancellationPolicies').empty();
                $('#hdnCancelCount').val('0');
                 $('#txtToCollected').val('0');
                $("#rbtnNonRefund").prop("checked", true);
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function disableSearchParameters(val) {
            try {
                document.getElementById('txtCity').disabled = (val == true) ? true : false;
                document.getElementById('txtCheckin').disabled = (val == true) ? true : false;
                document.getElementById("txtCheckout").disabled = (val == true) ? true : false;
                $("#ddlNationality").prop("disabled", (val == true) ? true : false);
                $("#ddlResidence").prop("disabled", (val == true) ? true : false);
                $("#roomCount").prop("disabled", (val == true) ? true : false);

                for (var i = 1; i <= Request[0].noOfRooms; i++) {
                    $("#adtRoom-" + i).prop("disabled", (val == true) ? true : false);
                    $("#chdRoom-" + i).prop("disabled", (val == true) ? true : false);
                    for (var j = 1; j <= Request[0].roomGuest[i - 1]["noOfChild"]; j++) {
                        $("#ChildBlock-" + i + "-ChildAge-" + j).prop("disabled", (val == true) ? true : false);
                    }
                }

                $("#ddlOnlineSuppliers").prop("disabled", (val == true) ? true : false);
                $("#ddlOfflineSuppliers").prop("disabled", (val == true) ? true : false);
                $("#ddlSupplierCurrency").prop("disabled", (val == true) ? true : false);
                $("#ddlSerchbyHotel").prop("disabled", (val == true) ? true : false);
                document.getElementById("txtSupplier").disabled = (val == true) ? true : false;
                $("#ddlRating").prop("disabled", (val == true) ? true : false);
                document.getElementById('txtHtlPhone').disabled = (val == true) ? true : false;
                document.getElementById('txtHtlAddress').disabled = (val == true) ? true : false;
                $("#ddlAgent").prop("disabled", (val == true) ? true : false);
                $("#ddlLocation").prop("disabled", (val == true) ? true : false);
                //document.getElementById("tglAgentCtr").disabled = (val == true) ? true : false;
                $('.tgl tgl-light').prop("disabled", false);
                //$(".tgl tgl-light").prop("disabled", (val == true) ? true : false);
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function select(id) {
            tmpval = $('#' + id).val();
            if (tmpval == '') {
                $('#' + id).addClass('error');
            } else {
                $('#' + id).removeClass('error');
            }
        }
        function ConvertDate(selector) { // To matach with Main search Date Format
            try {
                var from = selector.split("/");
                var Date = from[1] + "/" + from[0] + "/" + from[2];
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
            return Date;
        }


        function BindAgentRoomRates() {
            var Hotelrequest = Request[0];
            try {
                HotelRoomsDetails = [];
                for (var i = 1; i <= Hotelrequest.noOfRooms; i++) {
                    $('#txtAgtRmExrate' + i).val(exchangeRate);
                    $('#txtAgtRmRate' + i).val(($('#txtRmSuppRate' + i).val() * exchangeRate).toFixed(LoginInfo.Decimal));
                    $('#txtAgtRmMkpType' + i).val(markupType);
                    $('#txtAgtRmMkpValue' + i).val(markupValue);
                    var hotelTotalPrice = 0;
                    var vatAmount = 0;
                    var outVatAmount = 0;
                    var pageLevelMatkUp = 0;
                    hotelTotalPrice = $('#txtAgtRmRate' + i).val();
                    if (vatdetails != null && vatdetails.InputVAT != null) {
                        var data = JSON.parse(AjaxCalls("OfflineHotelBooking.aspx/calculateInputVat", "{'roomPrice':'" + hotelTotalPrice + "','decimalpoint':'" + LoginInfo.Decimal + "','vatDetails':'" + JSON.stringify(vatdetails) + "'}"));
                        hotelTotalPrice = data.RoomPrice;
                        vatAmount = data.VatAmount;
                        //hotelTotalPrice = Math.round(hotelTotalPrice, LoginInfo.Decimal); // commented by Anji based on vinay sir suggestions.
                    }
                    $('#txtAgtRmMkpAmount' + i).val((markupType == "F") ? markupValue : (hotelTotalPrice * (markupValue / 100)).toFixed(LoginInfo.Decimal));
                    $('#txtRmInVat' + i).val(vatAmount.toFixed(LoginInfo.Decimal));
                    var Markup = (markupType == "F") ? markupValue : (hotelTotalPrice) * (markupValue / 100);
                    if (LoginInfo.OnBehalfAgentLocation == "IN") {//GST caluculation
                        var data = JSON.parse(AjaxCalls("OfflineHotelBooking.aspx/LoadGSTValues", "{'roomMarkUp':'" + Markup + "'}"));
                        outVatAmount = data;
                    }
                    else {
                        pageLevelMatkUp = $('#txtAgtRmAddlmkpAmount' + i).val() != '' && $('#txtAgtRmAddlmkpAmount' + i).val() > 0 ? $('#txtAgtRmAddlmkpAmount' + i).val() : 0;
                        //hotelTotalPrice = Math.ceil(eval(hotelTotalPrice) + eval(pageLevelMatkUp));
                        Markup = (eval(Markup) + eval(pageLevelMatkUp)).toFixed(LoginInfo.Decimal);
                        if (vatdetails != null && vatdetails.OutputVAT != null) {
                            var data = JSON.parse(AjaxCalls("OfflineHotelBooking.aspx/calculateOutputVat", "{'TotalAmount':'" + Math.ceil(eval(hotelTotalPrice) + eval(Markup)) + "','TotalMarkup':'" + Markup + "','vatDetails':'" + JSON.stringify(vatdetails) + "'}"));
                            outVatAmount = data;
                        }
                    }
                    $('#txtRmOutVat' + i).val(outVatAmount.toFixed(LoginInfo.Decimal));
                    var fromDate = new Date($("#txtCheckin").datepicker("getDate"));
                    var toDate = new Date($("#txtCheckout").datepicker("getDate"));
                    var diffDate = (toDate - fromDate) / (1000 * 60 * 60 * 24);
                    var days = Math.round(diffDate);
                    var hRoomRates = [days - 1];
                    var totalprice = hotelTotalPrice;


                    var selectedDate = new Date(ConvertDate($("#txtCheckin").val()));
                    for (var fareIndex = 1; fareIndex < days; fareIndex++) {
                        var price = hotelTotalPrice / days;
                        if (fareIndex == days - 1) {
                            price = totalprice;
                        }
                        totalprice -= price;

                        hRoomRates.push({
                            Amount: price,
                            BaseFare: price,
                            SellingFare: price,
                            Totalfare: price,
                            RateType: 2,
                            Days: selectedDate.setDate(selectedDate.getDate() + (fareIndex - 1))
                        })

                    }
                    HotelRoomsDetails.push({
                        SequenceNo: i,
                        RoomTypeName: $('#ddlRoomTypes' + i + 'option:selected').text(),
                        RoomTypeCode: $('#ddlRoomTypes' + i).val(),
                        mealPlanDesc: $('#ddlRoomMealPlans' + i + 'option:selected').text(),
                        TotalPrice: hotelTotalPrice,
                        Markup: ((markupType == "F") ? markupValue : (hotelTotalPrice) * (markupValue / 100)),
                        MarkupType: markupType,
                        MarkupValue: markupValue,
                        SellingFare: hotelTotalPrice,
                        supplierPrice: $('#txtRmSuppRate' + i).val(),
                        InputVATAmount: vatAmount,
                        TaxDetail: vatdetails,
                        Rates: hRoomRates
                    })



                }
                var totalRoomAmount = 0;
                var toalInVatAmount = 0;
                var totalOutVatAmount = 0;
                var totalMarkUp = 0;
                var totalAddlMarkUp = 0;
                var collectedAmount = 0;
                for (var j = 1; j <= HotelRoomsDetails.length; j++) {
                    toalInVatAmount = eval(toalInVatAmount) + eval($('#txtRmInVat' + j).val());
                    totalOutVatAmount = eval(totalOutVatAmount) + eval($('#txtRmOutVat' + j).val());
                    totalRoomAmount = eval(totalRoomAmount) + eval($('#txtAgtRmRate' + j).val());
                    totalMarkUp = eval(totalMarkUp) + eval($('#txtAgtRmMkpAmount' + j).val());
                    totalAddlMarkUp = $('#txtAgtRmAddlmkpAmount' + j).val() > 0 ? eval(totalAddlMarkUp) + eval($('#txtAgtRmAddlmkpAmount' + j).val()) : totalAddlMarkUp;
                    collectedAmount = eval(collectedAmount) + eval(HotelRoomsDetails[j - 1].TotalPrice);
                }
                $('#txtAgentAmount').val((totalRoomAmount + totalMarkUp).toFixed(LoginInfo.Decimal));
                $('#txtInVat').val(toalInVatAmount.toFixed(LoginInfo.Decimal));
                $('#lblTtlOutVat').text(LoginInfo.OnBehalfAgentLocation == "IN" ? "TOTAL GST" : "OUTPUT VAT");
                $('#txtOutVat').val(totalOutVatAmount.toFixed(LoginInfo.Decimal));
                $('#txtTotalVat').val((toalInVatAmount + totalOutVatAmount).toFixed(LoginInfo.Decimal));
                $('#txtCollected').val((collectedAmount + totalMarkUp + + totalOutVatAmount + totalAddlMarkUp).toFixed(LoginInfo.Decimal));
                $('#txtNetPay').val((collectedAmount).toFixed(LoginInfo.Decimal)); // totaloutvatAmount removed anji , based on ziyad sir suggestions.
                $('#txtProfit').val((totalMarkUp + totalAddlMarkUp).toFixed(LoginInfo.Decimal));
                $('#txtToCollected').val((collectedAmount + totalMarkUp + + totalOutVatAmount + totalAddlMarkUp).toFixed(LoginInfo.Decimal));
                 var dueAndRecievd = calculateRecievedandDue();                            
                            $("#dueAmt").val(parseFloat(dueAndRecievd.due).toFixed(LoginInfo.Decimal));
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }

        var hotellist = [];
        var City = [];
        var agents = [];
        try {
            $(document).ready(function () {
                BindDataonpageload();
                $("#txtCity").autocomplete({
                    source: City,
                    minLength: 3,
                    open: function (event, ui) {
                        $("#txtCity").autocomplete("widget").css("width", "243px");
                    }
                });
                //Date Controls
                FromDate = new Date();
                $("#txtCheckin").datepicker(
                    {
                        minDate: -300,
                        numberOfMonths: [1, 2],
                        dateFormat: 'dd/mm/yy',
                        onSelect: function (dateText, inst) {
                            var selectedDate = new Date(ConvertDate(dateText));
                            selectedDate.setDate(selectedDate.getDate() + 1);
                            $("#txtCheckout").datepicker("option", "minDate", selectedDate);
                            var fromDate = new Date($("#txtCheckin").datepicker("getDate"));
                            var toDate = new Date($("#txtCheckout").datepicker("getDate"));
                            if (fromDate > toDate) {
                                $("#txtCheckout").datepicker("setDate", selectedDate);
                                // $("#txtToDate").val(selectedDate);

                            }

                        },
                        onchange: function (dateText, inst) {


                        }
                    }
                ).datepicker("setDate", FromDate);
                FromDate.setDate(FromDate.getDate() + 1);
                $("#txtCheckout").datepicker(
                    {
                        numberOfMonths: [1, 2],
                        minDate: FromDate,
                        dateFormat: 'dd/mm/yy'
                    })
            });
        } catch (e) {
            var msg = JSON.stringify(e.stack);
            exceptionSaving(msg);
        }

        function ConvertDate(selector) { // To matach with Main search Date Format
            var from = selector.split("/");
            var Date = from[1] + "/" + from[0] + "/" + from[2];
            return Date;
        }
        function BindDataonpageload() {
            try {
                var options = "";
                var data = JSON.parse(AjaxCalls("OfflineHotelBooking.aspx/LoadRequiredData", ""));
                $.each(data.Nationality, function (index, item) {
                    options += "<option value='" + index + "'>" + item + "</option>";
                });
                $('#ddlNationality').append(options);
                $('#ddlResidence').append(options);
                $('#ddlResidence').change(function () {
                });
                options = "";
                $.each(data.Agents, function (index, item) {
                    options += "<option value='" + item.AGENT_ID + "'>" + item.AGENT_NAME + "</option>";
                });
                agents = data.Agents;
                $('#ddlAgent').append(options);
                options = "";
                $.each(data.Currency, function (index, item) {
                    options += "<option value='" + item.CURRENCY_ID + "'>" + item.CURRENCY_CODE + "</option>";
                });
                $('#ddlSupplierCurrency').append(options);
                $.each(data.Cities, function (index, item) {
                    City.push(item);
                });
                roomTypes = data.RoomTypes;
                mealPlans = data.MealPlans;
                onlineSources = data.OnlineSources;
                options = "";
                $.each(data.OnlineSources, function (index, item) {
                    options += "<option value='" + item.SourceId + "'>" + item.Name + "</option>";
                });
                $('#ddlOnlineSuppliers').append(options);
                offlineSources = data.OfflineSources;
                options = "";
                $.each(data.OfflineSources, function (index, item) {
                    options += "<option value='" + item.SourceId + "'>" + item.Name + "</option>";
                });
                $('#ddlOfflineSuppliers').append(options);
                $("#divSupplier").show();
                document.getElementById('offline_supplier_list').style.display = "none";
                document.getElementById('online_supplier_list').style.display = "block";
                /* To bind flex fields on page load */
                if (data.flexFields != null && data.flexFields.length > 0) {
                    defaultFlexDetails = data.flexFields;
                    BindFlexFields(JSON.stringify(data.flexFields), null, 'divFlex', 'divFlexFields', '1', '0');

                    /* To enable date picker calender icon for flex fields */
                    $(".EnableCal").datepicker({ dateFormat: 'dd/mm/yy' });
                    //$('#btnContinue').show();
                    // $('#btnSave').show();
                }
                else {
                    $('#UDIDTab').hide();
                    $('#btnContinue').hide();
                    $('#btnSubmit').show();
                }

            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function AddHotelnames() {
            try {
                var City = $('#txtCity').val();
                //for checking city having multiple names or not
                if (City.split(',').length > 2) {
                    CountryName = City.split(',')[City.split(',').length - 1];
                }
                else {
                    CountryName = City.split(',')[1];
                }
                CityName = City.split(',')[0];
                var options = "";
                $('#ddlSerchbyHotel').empty();
                hotellist = AjaxCalls("OfflineHotelBooking.aspx/LoadHotelNames", "{'city':'" + CityName + "','country':'" + CountryName + "'}");
                $.each(hotellist, function (index, item) {
                    options += "<option value='" + item.HotelCode + "'>" + item.HotelName + "</option>";
                });
                $('#ddlSerchbyHotel').append(options);
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function bindHotelAddress() {
            try {
                $('#divHotelAddress').show();
                var selectedHotel = hotellist.find(x => x.HotelCode == $('#ddlSerchbyHotel').val());
                selectedHotel.PhoneNumber != null ? $('#txtHtlPhone').val(selectedHotel.PhoneNumber) : $('#txtHtlPhone').val('');
                selectedHotel.HotelAddress != null ? $('#txtHtlAddress').val(selectedHotel.HotelAddress) : $('#txtHtlAddress').val('');
                selectedHotel.Rating != null ? $('#ddlRating').val(selectedHotel.Rating) : $('#ddlRating').val(0);
                $('#ddlRating').select2();
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        function AjaxCalls(url, data) {
            try {
                var obj = "";
                $.ajax({
                    type: "POST",
                    url: url,
                    dataType: "json",
                    data: data,
                    contentType: "application/json; charset=utf-8",
                    async: false,
                    success: function (data) {
                        obj = (data == null || data.d == null || data.d == 'undefined' || data.d == '') ? '' : data.d;
                    },
                    error: function (d) {
                        console.log(JSON.stringify(error));
                    }
                });
                return obj;
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }
        $("#paxTab").click(function () {
            $('#btnContinue').show();

        });

        function getLocationsByAgentId() {
            try {
                var options = "<option value='" + -1 + "'>Select Location  </option>";
                var Agentid = $('#ddlAgent').val();
                var selectedAgent = agents.find(x => x.AGENT_ID == Agentid);
                
                $('#divBehalfAgentBalance').show();
                $('#spnbalance').text(selectedAgent.current_balance);
                var data = JSON.parse(AjaxCalls("OfflineHotelBooking.aspx/locationsByAgentId", "{'agentId':'" + Agentid + "'}"));
                $.each(data.Locations, function (index, item) {
                    options += "<option value='" + item.LOCATION_ID + "'>" + item.LOCATION_NAME + "</option>";
                });

                $("#ddlLocation option").remove();
                $('#ddlLocation').append(options);
                options = "";
                LoginInfo.AgentId = Agentid;
                LoginInfo.Decimal = data.Agency["DecimalValue"];
                LoginInfo.Currency = data.Agency["AgentCurrency"];
                $('#divFlexFields0').empty();
                if (data.flexFields != null && data.flexFields.length > 0) {
                    BindFlexFields(JSON.stringify(data.flexFields), null, 'divFlex', 'divFlexFields', '1', '0');
                    $('#UDIDTab').show();
                    $('#btnContinue').show();
                    $('#btnSubmit').hide();
                }
                else {
                    $('#UDIDTab').hide();
                    $('#btnContinue').hide();
                    //$('#btnSubmit').show();
                }

            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }

        }
        function navigateTab(id) {
            if (('<%=Settings.LoginInfo.AgentId %>') == LoginInfo.AgentId) {
                if ((id == 1 || id == 2) && defaultFlexDetails != null && defaultFlexDetails.length == 0) {
                    $('#btnContinue').hide();
                    $('#btnSubmit').show();
                }
            }
        }
        function ShowDiv() {
            try {
                var val = $('#hdnCancelCount').val();
                val = eval(val) + 1;
                var count;
                if (val == "1") {
                    count = 1;
                }
                else {
                    count = val - 1;
                }
                $('#hdnCancelCount').val(val);
                var ListCancel = $('#CancelDiv').html();
                $('#ListCancellationPolicies').append('<li id="ListCancellationPolicies' + val + '" class="ui-bg-wrapper mb-1  ui-room-wrapper">' + ListCancel + '</li>');
                //  $('#CancelDiv'+count).attr('id', 'CancelDiv' + val);
                $('#txtCancelDays').attr('id', 'txtCancelDays' + val);
                $('#txtCancelDays' + val).attr('onkeypress', 'return isNumber(event);');
                $('#rbtnCancelNights').attr('id', 'rbtnCancelNights' + val);
                $('#rbtnCancelNights' + val).attr('name', 'Cancel' + val);
                $('#rbtnCancelNights' + val).attr('onchange', "toggle('" + val + "','Percent','Nights');");
                $('#txtCancelNights').attr('id', 'txtCancelNights' + val);
                $('#txtCancelNights' + val).attr('onkeypress', 'return isNumber(event);');
                $('#rbtnCancelPercent').attr('id', 'rbtnCancelPercent' + val);
                $('#rbtnCancelPercent' + val).attr('name', 'Cancel' + val);
                $('#rbtnCancelPercent' + val).attr('onchange', "toggle('" + val + "','Nights','Percent');");
                $('#txtCancelPercent').attr('id', 'txtCancelPercent' + val);
                $('#txtCancelPercent' + val).attr('onkeypress', 'return isNumber(event);');
                $('#btnRemove').attr('id', 'btnRemove-' + val);
                var ctrl = 'btnRemove-' + val;
                $('#btnRemove-' + val).attr('onclick', 'Remove(this.id);');
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }


        }
        function Remove(id) {
            if (confirm("Do you want to remove this.")) {
                try {
                    var removeValue = id.split('-')[1];
                    $('#ListCancellationPolicies' + id.split('-')[1]).remove();
                    if ($('#hdnCancelCount').val() == id.split('-')[1]) {
                        $('#hdnCancelCount').val(id.split('-')[1] - 1);
                    }
                    else {
                        $('#hdnCancelCount').val($('#hdnCancelCount').val() - 1)
                    }
                } catch (e) {
                    var msg = JSON.stringify(e.stack);
                    exceptionSaving(msg);
                }
            }

        }
        function toggle(val, item1, item2) {
            document.getElementById('txtCancel' + item1 + val).disabled = true;
            document.getElementById('txtCancel' + item1 + val).value = "";
            document.getElementById('txtCancel' + item2 + val).disabled = false;
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode <= 45 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function ShowHeaders() {
            if (getElement('rbtnRefund').checked) {
                document.getElementById('divHeaders').style.display = "block";
            }
            else {
                document.getElementById('divHeaders').style.display = "none";
            }
        }
        //Hotel Pax Count
        function hotelPaxCount() {

            var roomCount = $('#PrevNoOfRooms').val();
            var htlAdultCount = 0,
                htlChildCount = 0;
            var countroom = 1;
            $('#hotelRoomSelectDropDown select.adult-pax').each(function (index) {
                countroom = index + 1;
                if (countroom > roomCount) {
                    return false;
                }
                htlAdultCount += parseInt($(this).val());
            })
            $('#hotelRoomSelectDropDown select.child-pax').each(function (index) {
                countroom = index + 1;
                if (countroom > roomCount) {
                    return false;
                }
                htlChildCount += parseInt($(this).val());
            });
            var HotelTotalPax = '<strong>';
            HotelTotalPax += roomCount;
            HotelTotalPax += '</strong> Room , <strong>';
            HotelTotalPax += htlAdultCount;
            HotelTotalPax += '</strong> Adult ';
            if (htlChildCount > 0) {
                HotelTotalPax += ', <strong>' + htlChildCount + '</strong> Child';
            }
            $('#htlTotalPax').html(HotelTotalPax);
        }
        hotelPaxCount()
        $('.hotel-pax-dropdown select').change(function () {

            hotelPaxCount()
        })
        function BindAddlMarkUp(id) {

            try {

                var idValue = id.split('-')[1];
                if ($('#txtAgtRmAddlmkpType' + idValue).val() != '' && ($('#txtAgtRmAddlmkpType' + idValue).val() == 'P' || $('#txtAgtRmAddlmkpType' + idValue).val() == 'F') && $('#txtAgtRmAddlmkpValue-' + idValue).val() > 0) {
                    var Markup = ($('#txtAgtRmAddlmkpType' + idValue).val() == "F") ? $('#txtAgtRmAddlmkpValue-' + idValue).val() : ((eval($('#txtAgtRmRate' + idValue).val()) + eval(HotelRoomsDetails[idValue - 1].InputVATAmount) + eval(HotelRoomsDetails[idValue - 1].Markup)) * ($('#txtAgtRmAddlmkpValue-' + idValue).val() / 100));
                    $('#txtAgtRmAddlmkpAmount' + idValue).val(Markup)
                }
                else {
                    $('#txtAgtRmAddlmkpAmount' + idValue).val(0)
                }
                BindAgentRoomRates();
            } catch (e) {
                var msg = JSON.stringify(e.stack);
                exceptionSaving(msg);
            }
        }

        $(function () {
            $('.no-select2').select2('destroy');
        })
        
    </script>


    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <div class="body_container">
        
        <input type="hidden" id="hdnLoginInfo" runat="server" />
        <asp:HiddenField ID="hdnloginAgentLocations" runat="server" Value="" />
        <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" class="alert-messages alert-caution"></div>
        </div>

        <div id="search_hotel_con" class="search-matrix-wrap hotels">

            <div class="custom-radio-table row mb-3">

                <div class="col-12">
                    <div class="row custom-gutter">

                        <div class="col-md-12">

                            <div class="agent-checkbox-wrap float-left">

                                <input id="tglAgentCtr" type="checkbox" name="tglAgentCtr" class="tgl tgl-light" />

                                <label onclick="showHideAgent()" class="tgl-btn" for="tglAgentCtr"><em></em></label>
                            </div>
                            <div class="row" id="divAgentBalance" style="display:none">
                                    
                                        <span class="icon-money icon"></span>
                                        <%-- <span class="currency" id="spncurrency">AED</span>--%>
                                        <asp:Label ID="lblAgentBalance" CssClass="font_blue" runat="server" Text=""></asp:Label>
                                        
                                    </div>


                        </div>

                        




                        <div id="clientDiv" class="col-md-12" style="display: none">

                            <div class="row">
                                <div class="col-md-3 mt-2">
                                    <div class="form-control-holder">
                                        <select id="ddlAgent" class="form-control" onchange="getLocationsByAgentId();">
                                            <option value="-1">Select Client</option>
                                        </select>
                                    </div>

                                </div>


                                <div class="col-md-3 mt-2">

                                    <div class="form-control-holder">
                                        <select id="ddlLocation" class="form-control">
                                            <option value="-1">Select Location</option>
                                        </select>

                                    </div>

                                </div>

                                <div class="col-md-6 mt-2 text-right">



                                   <div class="agnetBalance text-primary" id="divBehalfAgentBalance" >
                                    
                                        <span class="icon-money icon"></span>
                                        <%-- <span class="currency" id="spncurrency">AED</span> --%>
                                        <span class="agent-balance" id="spnbalance"></span>
                                        
                                    </div>
                                </div>
                            </div>
                        </div>



                    </div>
                </div>

            </div>

            <div class="row custom-gutter mb-3">



                <div class="col-md-4">
                    <div class="form-control-holder position-relative">
                        <div class="icon-holder">
                            <span class="icon-location"></span>
                        </div>
                        <input id="txtCity" class="form-control" type="text" placeholder="Enter city name" onchange="AddHotelnames();" />
                    </div>
                </div>

                <div class="col-md-2">
                    <a class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-calendar" aria-label=""></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Check In" id="txtCheckin" />
                    </a>

                </div>

                <div class="col-md-2">
                    <a class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-calendar" aria-label=""></span>
                        </div>
                        <input type="text" class="form-control" placeholder="Check Out" id="txtCheckout" />
                    </a>

                </div>


                <div class="col-md-2">
                    <div class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-passport" aria-label="Select Nationality"></span>
                        </div>
                        <select class="form-control" id="ddlNationality">
                            <option value="-1">Select Nationality</option>

                        </select>
                    </div>
                </div>

                <div class="col-md-2">
                    <div class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-passport" aria-label="Country of Residence"></span>
                        </div>
                        <select class="form-control" id="ddlResidence">
                            <option selected="selected" value="-1">Country of Residence</option>

                        </select>
                    </div>
                </div>


            </div>

            <div class="row custom-gutter mb-3">



                <div class="col-md-3">

                    <div class="custom-dropdown-wrapper">
                        <a href="jaavscript:void(0);" class="form-control-element with-custom-dropdown" data-dropdown="#hotelRoomSelectDropDown">
                            <div class="form-control-holder">
                                <div class="icon-holder">
                                    <span class="icon-group"></span>
                                </div>
                                <span class="form-control-text" id="htlTotalPax">1 Room, 1 Adult</span>
                            </div>
                        </a>
                        <div class="dropdown-content d-none p-4 hotel-pax-dropdown" id="hotelRoomSelectDropDown" style="max-height: 270px;">
                            <div class="row custom-gutter">
                                <div class="col-4">ROOM</div>
                                <div class="col-6">
                                    <input type="hidden" value="1" name="PrevNoOfRooms" id="PrevNoOfRooms" />
                                    <div class="form-control-holder">
                                        <select class="form-control small no-select2 htl-room-select" id="roomCount" name="roomCount" onchange="ShowRoomDetails();">
                                            <option selected="selected">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row custom-gutter room-wrapper" id="room-1">
                                <div class="col-12">
                                    <label class="room-label">Room 1</label>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Adults</label>
                                        <select class="form-control no-select2 adult-pax" name="adtRoom-1" id="adtRoom-1">
                                            <option selected="selected" value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                        </select>
                                        <input type="hidden" value="0" id="PrevChildCount-1" name="PrevChildCount-1" />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Child(2-12 yrs)</label>
                                        <select onchange="javascript:ShowChildAge('1')" name="chdRoom-1" id="chdRoom-1" class="form-control no-select2  child-pax">
                                            <option selected="selected" value="0">none</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12" id="ChildBlock-1" name="ChildBlock-1" style="display: none; width: 100%;">
                                    <div class="row no-gutters child-age-wrapper">
                                        <div class="col-4" id="ChildBlock-1-Child-1" name="ChildBlock-1-Child-1" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 1</label>
                                                <select class="form-control" id="ChildBlock-1-ChildAge-1" name="ChildBlock-1-ChildAge-1">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-1-Child-2" name="ChildBlock-1-Child-2" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 2</label>
                                                <select class="form-control" id="ChildBlock-1-ChildAge-2" name="ChildBlock-1-ChildAge-2" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-1-Child-3" name="ChildBlock-1-Child-3" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 3</label>
                                                <select class="form-control" id="ChildBlock-1-ChildAge-3" name="ChildBlock-1-ChildAge-3" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-1-Child-4" name="ChildBlock-1-Child-4" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 4</label>
                                                <select class="form-control" id="ChildBlock-1-ChildAge-4" name="ChildBlock-1-ChildAge-4" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-1-Child-5" name="ChildBlock-1-Child-5" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 5</label>
                                                <select class="form-control" id="ChildBlock-1-ChildAge-5" name="ChildBlock-1-ChildAge-5" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-1-Child-6" name="ChildBlock-1-Child-6" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 6</label>
                                                <select class="form-control" id="ChildBlock-1-ChildAge-6" name="ChildBlock-1-ChildAge-6" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-12" id="childDetails" style="display: none;">
                                            <!--<p>Child Details</p>-->
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row custom-gutter room-wrapper" id="room-2" style="display: none;">
                                <div class="col-12">
                                    <label class="room-label">Room 2</label>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Adults</label>
                                        <select name="adtRoom-2" id="adtRoom-2" class="form-control no-select2 adult-pax">
                                            <option selected="selected" value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                        </select>
                                        <input type="hidden" value="0" id="PrevChildCount-2" name="PrevChildCount-2" />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Child (2-11 yrs)</label>
                                        <select onchange="javascript:ShowChildAge('2')" name="chdRoom-2" id="chdRoom-2" class="form-control no-select2  child-pax">
                                            <option selected="selected" value="0">none</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12" id="ChildBlock-2" name="ChildBlock-2" style="display: none;">
                                    <div class="row no-gutters child-age-wrapper">
                                        <div class="col-4" id="ChildBlock-2-Child-1" name="ChildBlock-2-Child-1">
                                            <div class="form-group">
                                                <label>Child 1</label>
                                                <select class="form-control" id="ChildBlock-2-ChildAge-1" name="ChildBlock-2-ChildAge-1">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-2-Child-2" name="ChildBlock-2-Child-2" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 2</label>
                                                <select class="form-control" id="ChildBlock-2-ChildAge-2" name="ChildBlock-2-ChildAge-2">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-2-Child-3" name="ChildBlock-2-Child-3" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 3</label>
                                                <select class="form-control" id="ChildBlock-2-ChildAge-3" name="ChildBlock-2-ChildAge-3">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-2-Child-4" name="ChildBlock-2-Child-4" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 4</label>
                                                <select class="form-control" id="ChildBlock-2-ChildAge-4" name="ChildBlock-2-ChildAge-4">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-2-Child-5" name="ChildBlock-2-Child-5" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 5</label>
                                                <select class="form-control" id="ChildBlock-2-ChildAge-5" name="ChildBlock-2-ChildAge-5">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-2-Child-6" name="ChildBlock-2-Child-6" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 6</label>
                                                <select class="form-control" id="ChildBlock-2-ChildAge-6" name="ChildBlock-2-ChildAge-6">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row custom-gutter room-wrapper" id="room-3" style="display: none;">
                                <div class="col-12">
                                    <label class="room-label">Room 3</label>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Adults</label>
                                        <select name="adtRoom-3" id="adtRoom-3" class="form-control no-select2 adult-pax">
                                            <option selected="selected" value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                        </select>
                                        <input type="hidden" value="0" id="PrevChildCount-3" name="PrevChildCount-3" />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Children (2-11 yrs)</label>
                                        <select onchange="javascript:ShowChildAge('3')" name="chdRoom-3" id="chdRoom-3" class="form-control  no-select2  child-pax">
                                            <option selected="selected" value="0">none</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12" id="ChildBlock-3" name="ChildBlock-3" style="display: none;">
                                    <div class="row no-gutters child-age-wrapper">
                                        <div class="col-4" id="ChildBlock-3-Child-1" name="ChildBlock-3-Child-1" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 1</label>
                                                <select class="form-control" id="ChildBlock-3-ChildAge-1" name="ChildBlock-3-ChildAge-1">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-3-Child-2" name="ChildBlock-3-Child-2" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 2</label>
                                                <select class="form-control" id="ChildBlock-3-ChildAge-2" name="ChildBlock-3-ChildAge-2" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-3-Child-3" name="ChildBlock-3-Child-3" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 3</label>
                                                <select class="form-control" id="ChildBlock-3-ChildAge-3" name="ChildBlock-3-ChildAge-3" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-3-Child-4" name="ChildBlock-3-Child-4" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 4</label>
                                                <select class="form-control" id="ChildBlock-3-ChildAge-4" name="ChildBlock-3-ChildAge-4" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-3-Child-5" name="ChildBlock-3-Child-5" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 5</label>
                                                <select class="form-control" id="ChildBlock-3-ChildAge-5" name="ChildBlock-3-ChildAge-5" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-3-Child-6" name="ChildBlock-3-Child-6" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 6</label>
                                                <select class="form-control" id="ChildBlock-3-ChildAge-6" name="ChildBlock-3-ChildAge-6" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row custom-gutter room-wrapper" id="room-4" style="display: none;">
                                <div class="col-12">
                                    <label class="room-label">Room 4</label>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Adults</label>
                                        <select name="adtRoom-4" id="adtRoom-4" class="form-control no-select2 adult-pax">
                                            <option selected="selected" value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                        </select>
                                        <input type="hidden" value="0" id="PrevChildCount-4" name="PrevChildCount-4" />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Child (2-11 yrs)</label>
                                        <select onchange="javascript:ShowChildAge('4')" name="chdRoom-4" id="chdRoom-4" class="form-control no-select2 child-pax">
                                            <option selected="selected" value="0">none</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12" id="ChildBlock-4" name="ChildBlock-4" style="display: none;">
                                    <div class="row no-gutters child-age-wrapper">
                                        <div class="col-4" id="ChildBlock-4-Child-1" name="ChildBlock-4-Child-1" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 1</label>
                                                <select class="form-control" id="ChildBlock-4-ChildAge-1" name="ChildBlock-4-ChildAge-1">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-4-Child-2" name="ChildBlock-4-Child-2" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 2</label>
                                                <select class="form-control" id="ChildBlock-4-ChildAge-2" name="ChildBlock-4-ChildAge-2" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-4-Child-3" name="ChildBlock-4-Child-3" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 3</label>
                                                <select class="form-control" id="ChildBlock-4-ChildAge-3" name="ChildBlock-4-ChildAge-3" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-4-Child-4" name="ChildBlock-4-Child-4" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 4</label>
                                                <select class="form-control" id="ChildBlock-4-ChildAge-4" name="ChildBlock-4-ChildAge-4" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-4-Child-5" name="ChildBlock-4-Child-5" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 5</label>
                                                <select class="form-control" id="ChildBlock-4-ChildAge-5" name="ChildBlock-4-ChildAge-5" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-4-Child-6" name="ChildBlock-4-Child-6" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 6</label>
                                                <select class="form-control" id="ChildBlock-4-ChildAge-6" name="ChildBlock-4-ChildAge-6" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row custom-gutter room-wrapper" id="room-5" style="display: none;">
                                <div class="col-12">
                                    <label class="room-label">Room 5</label>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Adults</label>
                                        <select name="adtRoom-5" id="adtRoom-5" class="form-control adult-pax">
                                            <option selected="selected" value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                            <option value="7">7</option>
                                            <option value="8">8</option>
                                        </select>
                                        <input type="hidden" value="0" id="PrevChildCount-5" name="PrevChildCount-5" />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Child (2-11 yrs)</label>
                                        <select onchange="javascript:ShowChildAge('5')" name="chdRoom-5" id="chdRoom-5" class="form-control  child-pax">
                                            <option selected="selected" value="0">none</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12" id="ChildBlock-5" name="ChildBlock-5" style="display: none;">
                                    <div class="row no-gutters child-age-wrapper">
                                        <div class="col-4" id="ChildBlock-5-Child-1" name="ChildBlock-5-Child-1" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 1</label>
                                                <select class="form-control" id="ChildBlock-5-ChildAge-1" name="ChildBlock-5-ChildAge-1">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-5-Child-2" name="ChildBlock-5-Child-2" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 2</label>
                                                <select class="form-control" id="ChildBlock-5-ChildAge-2" name="ChildBlock-5-ChildAge-2" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-5-Child-3" name="ChildBlock-5-Child-3" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 3</label>
                                                <select class="form-control" id="ChildBlock-5-ChildAge-3" name="ChildBlock-5-ChildAge-3" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-5-Child-4" name="ChildBlock-5-Child-4" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 4</label>
                                                <select class="form-control" id="ChildBlock-5-ChildAge-4" name="ChildBlock-5-ChildAge-4" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-5-Child-5" name="ChildBlock-5-Child-5" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 5</label>
                                                <select class="form-control" id="ChildBlock-5-ChildAge-5" name="ChildBlock-5-ChildAge-5" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-5-Child-6" name="ChildBlock-5-Child-6" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 6</label>
                                                <select class="form-control" id="ChildBlock-5-ChildAge-6" name="ChildBlock-5-ChildAge-6" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                    <option value="12">12</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="row custom-gutter room-wrapper" id="room-6" style="display: none;">
                                <div class="col-12">
                                    <label class="room-label">Room 6</label>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Adults</label>
                                        <select name="adtRoom-6" id="adtRoom-6" class="form-control adult-pax">
                                            <option selected="selected" value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                        </select>
                                        <input type="hidden" value="0" id="PrevChildCount-6" name="PrevChildCount-6" />
                                    </div>
                                </div>
                                <div class="col-4">
                                    <div class="form-group">
                                        <label>Child (2-11 yrs)</label>
                                        <select onchange="javascript:ShowChildAge('6')" name="chdRoom-6" id="chdRoom-6" class="form-control child-pax">
                                            <option selected="selected" value="0">none</option>
                                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-12" id="ChildBlock-6" name="ChildBlock-6" style="display: none;">
                                    <div class="row no-gutters child-age-wrapper">
                                        <div class="col-4" id="ChildBlock-6-Child-1" name="ChildBlock-6-Child-1" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 16</label>
                                                <select class="form-control" id="ChildBlock-6-ChildAge-1" name="ChildBlock-6-ChildAge-1">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-6-Child-2" name="ChildBlock-6-Child-2" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 26</label>
                                                <select class="form-control" id="ChildBlock-6-ChildAge-2" name="ChildBlock-6-ChildAge-2" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-6-Child-3" name="ChildBlock-6-Child-3" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 36</label>
                                                <select class="form-control" id="ChildBlock-6-ChildAge-3" name="ChildBlock-6-ChildAge-3" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-6-Child-4" name="ChildBlock-6-Child-4" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 46</label>
                                                <select class="form-control" id="ChildBlock-6-ChildAge-4" name="ChildBlock-6-ChildAge-4" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-6-Child-5" name="ChildBlock-6-Child-5" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 56</label>
                                                <select class="form-control" id="ChildBlock-6-ChildAge-5" name="ChildBlock-6-ChildAge-5" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-4" id="ChildBlock-6-Child-6" name="ChildBlock-6-Child-6" style="display: none;">
                                            <div class="form-group">
                                                <label>Child 6</label>
                                                <select class="form-control" id="ChildBlock-6-ChildAge-6" name="ChildBlock-6-ChildAge-6" onchange="javascript:ShowChildAge('1')">
                                                    <option value="-1" selected="selected">Age?</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                    <option value="6">6</option>
                                                    <option value="7">7</option>
                                                    <option value="8">8</option>
                                                    <option value="9">9</option>
                                                    <option value="10">10</option>
                                                    <option value="11">11</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="col-md-3">


                    <div class="btn-group " data-toggle="buttons">

                        <label onclick="show_OnlineSupplier()" class="btn btn-default active">
                            <input name="yesno" type="radio" id="onnlineRadio" value="Online Supplier" checked>
                            Online Supplier
                        </label>

                        <label onclick="show_OfflineSupplier()" class="btn btn-default">
                            <input onclick="javascript: yesnoCheck();" name="yesno" type="radio" id="offlineRadio" value="Offline Supplier">
                            Offline Supplier
                        </label>



                    </div>

                </div>

                <div class="col-md-3">
                    <div id="divSupplier" style="display: none;">
                        <div id="online_supplier_list">
                            <div class="form-control-holder">
                                <select id="ddlOnlineSuppliers" class="form-control">
                                    <option value="-1">Select Online Supplier</option>
                                </select>

                            </div>
                        </div>
                        <div id="offline_supplier_list">
                            <div class="form-control-holder">
                                <select id="ddlOfflineSuppliers" class="form-control">
                                    <option value="-1">Select Offline Supplier</option>
                                </select>

                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-money" aria-label=""></span>
                        </div>
                        <select class="form-control" id="ddlSupplierCurrency">
                            <option selected="selected" value="-1">Supplier Currency</option>


                        </select>
                    </div>
                </div>
            </div>

            <div class="row custom-gutter mb-3">

                <div class="col-md-5">
                    <div class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-bed" aria-label=""></span>
                        </div>
                        <select id="ddlSerchbyHotel" class="form-control" onchange="bindHotelAddress()">
                            <option selected="selected" value="0">Search by Hotel Name</option>


                        </select>
                    </div>





                </div>

                <div class="col-md-4">

                    <div class="form-control-holder">
                        <input type="text" class="form-control" id="txtSupplier" placeholder="Confirm. No./ Supplier Ref#">
                    </div>
                </div>


                <div class="col-md-2">
                    <div class="form-control-holder">
                        <div class="icon-holder">
                            <span class="icon-star" aria-label=""></span>
                        </div>
                        <select class="form-control" id="ddlRating">
                            <option selected="selected" value="0">Star Rating</option>
                            <option value="1">1 rating</option>
                            <option value="2">2 rating</option>
                            <option value="3">3 rating</option>
                            <option value="4">4 rating</option>
                            <option value="5">5 rating</option>
                        </select>
                    </div>
                </div>




            </div>




            <div class="row custom-gutter">



                <div class="col-md-10">

                    <div class="rulddlhotel" id="divHotelAddress" style="display: none">

                        <div class="row custom-gutter">
                            <div class="col-md-4">
                                <div class="form-control-holder">
                                    <input type="text" placeholder="Phone no." maxlength="20" class="form-control" id="txtHtlPhone" onkeypress="return isNumber(event)">
                                </div>
                            </div>

                            <div class="col-md-8">
                                <div class="form-control-holder">
                                    <input type="text" class="form-control" id="txtHtlAddress">
                                </div>

                            </div>
                        </div>
                    </div>
                </div>


                <div class="col-md-2">
                    <input class="but but_b pull-right block_xs" type="button" value="Add Details" onclick="addHotelDetails()" />
                </div>


            </div>





            <div class="clear"></div>
        </div>   
     <div class="paramcon bg_white pad_10" title="Param" id="lstError" style="display:none">   
         
     <div> <center><label id="lblError" class="lblSuccess" style="color:red;FONT-SIZE: 14px;"></label></center></div>                                                
    <div class="clearfix"></div>
    </div>     
        <div style="display: none" id="add_hotel_details_con">
            <div class="right_col CorpTrvl-page">
                <div class="row">
                    <div class="col-md-12 CorpTrvl-tabbed-panel mt-2 mb-0">

                        <ul class="nav nav-tabs" id="Tabs">
                            <li class="active">
                                <a data-toggle="tab" href="#paxTabDiv" id="paxTab" onclick="navigateTab(1);">Passenger Details  </a>
                            </li>



                            <li>
                                <a data-toggle="tab" href="#UDIDDiv" id="UDIDTab" onclick="navigateTab(2);">UDID </a>
                            </li>

                        </ul>

                        <div class="tab-content p-2">

                            <div id="paxTabDiv" class="tab-pane fade in active">

                                <h4><a class="" data-toggle="collapse" href="#PaxDetailsCollapse" role="button" aria-expanded="false" aria-controls="PaxDetailsCollapse">
                                    <strong>Passenger Details</strong> <span class="expand-details"><i class="fa fa-plus-circle"></i></span></a>
                                </h4>

                                <div class="collapse" id="PaxDetailsCollapse">

                                    <ul class="ui-bg-wrapper mb-1  ui-room-wrapper" id="ListGuest"></ul>

                                    <div class="row no-gutter border-bottom pb-3 pt-3" style="display: none;" id="Adult">

                                        <div class="col-12 mb-3">

                                            <div class="row">

                                                <div class="col-6">
                                                    <strong>
                                                        <label id="h4PaxType"></label>
                                                    </strong>
                                                </div>

                                                <div class="col-6 text-muted">
                                                    <strong>
                                                        <label id="RoomCount"></label>
                                                    </strong>
                                                </div>


                                            </div>
                                        </div>

                                        <div class="col-12 mb-3">

                                            <div class="row">
                                                <div class="col-md-1">
                                                    <div class="form-group" id="divpaxTitle">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" id="divpaxFirstName">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" id="divpaxLastName">
                                                    </div>
                                                </div>
                                                <div class="col-md-2">
                                                    <div class="form-group" id="divpaxMobileNo">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group" id="divpaxEmail">
                                                    </div>
                                                </div>

                                            </div>
                                        </div>
                                    </div>

                                </div>
                                <h4><a class="" data-toggle="collapse" href="#SupplierRatesCollapse" role="button" aria-expanded="false" aria-controls="SupplierRatesCollapse">
                                    <strong>Room Rates (per room supplier rate)</strong> <span class="expand-details"><i class="fa fa-plus-circle"></i></span></a>
                                </h4>
                                <div class="collapse" id="SupplierRatesCollapse">
                                    <div class="btn-group " data-toggle="buttons">

                                        <label onclick="showSupplierrates()" class="btn btn-default active">
                                            <input name="yesno" type="radio" id="tabSupplierRates" value="Supplier" checked>
                                            Supplier Roomwise Rates
                                        </label>

                                        <label onclick="showAgentrates()" class="btn btn-default">
                                            <input name="yesno" type="radio" id="tabAgentRates" value="Agent">
                                            Agent Roomwise Rates
                                        </label>
                                    </div>
                                    <div id="divSupplierrates" style="display: none">
                                        <ul class="ui-bg-wrapper mb-1  ui-room-wrapper" id="ListRoomRates"></ul>
                                        <div class="rptdiv" id="roomRate" style="display: none">
                                            <div class="row">
                                                <div class="col-12">
                                                    <span class="roomNumber"><strong>
                                                        <label id="lblRmRateCount"></label>
                                                    </strong></span>
                                                </div>
                                                <div class="col-12">
                                                    <div class="borsap">
                                                        <div class="row no-gutter mt-3">
                                                            <div class="col-md-2">
                                                                <label>Room Types</label>
                                                                <select id="ddlRoomTypes" class="form-control no-select2">
                                                                    <option value="-1">Select RoomType</option>
                                                                </select>
                                                            </div>
                                                            <div class="col-md-2">
                                                                <label>Room Meal Basis</label>
                                                                <div>
                                                                    <select id="ddlRoomMealPlans" class="form-control no-select2">
                                                                        <option value="-1">Select MealPlan</option>

                                                                    </select>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row no-gutter mt-3">


                                                            <div class="col-12">
                                                                <table width="100%" class="table-borderless text-center">
                                                                    <tr>
                                                                        <td>&nbsp</td>
                                                                        <td class="bg_supplier" align="left"><strong>RoomRates</strong></td>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">
                                                <table width="100%" class="table-borderless text-center">

                                                    <tr>
                                                        <td>

                                                            <table class="table table-striped">
                                                                <tr>
                                                                    <td><strong>Room Rate</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <input id="txtRmSuppRate" style="width: 60px" maxlength="10" onkeypress="return isNumber(event)" /></td>
                                                                </tr>


                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>

                                            </div>

                                        </div>


                                    </div>


                                    <div id="divAgentrates" style="display: none">
                                        <ul class="ui-bg-wrapper mb-1  ui-room-wrapper" id="ListAgentRoomRates"></ul>
                                        <div class="rptdiv" id="roomAgentRate" style="display: none">


                                            <div class="row">


                                                <div class="col-12">
                                                    <span class="roomNumber"><strong>
                                                        <label id="lblRmAgentRateCount"></label>
                                                    </strong></span>
                                                </div>


                                                <div class="col-12">


                                                    <div class="borsap">


                                                        <div class="row no-gutter mt-3">


                                                            <div class="col-12">
                                                                <table width="100%" class="table-borderless text-center">
                                                                    <tr>
                                                                        <td>&nbsp</td>
                                                                        <td class="bg_agent" align="left"><strong>RoomRates</strong></td>
                                                                        <td class="bg_agent" align="left"><strong>Markup</strong></td>
                                                                        <td class="bg_agent" align="center"><strong>AddlMarkup</strong></td>
                                                                        <td class="bg_agent" align="center"><strong>Vat Amounts</strong></td>
                                                                        <%--<td class="bg_agent" align="center"  ><strong> Collection</strong></td>--%>
                                                                    </tr>
                                                                </table>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-12">


                                                <table width="100%" class="table-borderless text-center">

                                                    <tr>

                                                        <td>

                                                            <table class="table table-striped">
                                                                <tr>
                                                                    <td><strong>Currency</strong></td>
                                                                    <td><strong>ExRate</strong></td>
                                                                    <td><strong>Room Rate</strong></td>
                                                                    <td><strong>Type</strong></td>
                                                                    <td><strong>Value</strong></td>
                                                                    <td><strong>Amount</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <input id="txtAgtRmCurrency" style="width: 60px" disabled />
                                                                    </td>
                                                                    <td>
                                                                        <input id="txtAgtRmExrate" style="width: 60px" disabled /></td>
                                                                    <td>
                                                                        <input id="txtAgtRmRate" style="width: 60px" disabled /></td>
                                                                    <td>
                                                                        <input id="txtAgtRmMkpType" style="width: 60px" disabled /></td>
                                                                    <td>
                                                                        <input id="txtAgtRmMkpValue" style="width: 60px" disabled/></td>
                                                                    <td>
                                                                        <input id="txtAgtRmMkpAmount" style="width: 60px" disabled /></td>
                                                                </tr>
                                                            </table>
                                                        </td>
                                                        <td>
                                                            <table class="table table-striped">
                                                                <tr>
                                                                    <td><strong>Type</strong></td>
                                                                    <td><strong>Value</strong></td>
                                                                    <td><strong>Amount</strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <input id="txtAgtRmAddlmkpType" style="width: 60px" maxlength="3"/></td>
                                                                    <td>
                                                                        <input id="txtAgtRmAddlmkpValue" style="width: 60px" maxlength="5" onkeypress="return isNumber(event)"></td>
                                                                    <td>
                                                                        <input id="txtAgtRmAddlmkpAmount" style="width: 60px" disabled /></td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                        <td>
                                                            <table class="table table-striped">
                                                                <tr>
                                                                    <td><strong>Input Vat</strong></td>
                                                                    <td><strong><label id="lblOutVat">Output Vat</label></strong></td>
                                                                </tr>
                                                                <tr>
                                                                    <td>
                                                                        <input id="txtRmInVat" style="width: 60px" disabled /></td>
                                                                    <td>
                                                                        <input id="txtRmOutVat" style="width: 60px" disabled /></td>
                                                                </tr>
                                                            </table>
                                                        </td>

                                                    </tr>
                                                </table>

                                            </div>

                                        </div>
                                    </div>
                                </div>
                                <h4><a class="" data-toggle="collapse" href="#SettlementModeCollapse" role="button" aria-expanded="false" aria-controls="SettlementModeCollapse">
                                    <strong>Settlement Mode </strong><span class="expand-details"><i class="fa fa-plus-circle"></i></span></a>
                                </h4>
                                <div class="collapse" id="SettlementModeCollapse">


                                    <div class="row">

                                        <div class="col-md-6" style="max-width: none; min-width: inherit;">
                                            <div class="x_title pd-3">

                                                   <div class="clearfix"></div>
                                                </div>
                                            <div class="x_content">
                                             <table id="ctl00_cphTransaction_tblSettlement" width="100%">
                                                                        <tr>
                                                                            <td>
                                                                                <div class="row">
                                                                                    <div class="col-xs-12 col-md-6">
                                                                                        <div class="form-group">
                                                                                            <label>
                                                                                                <span id="ctl00_cphTransaction_lblSettlementMode">Mode:</span>
                                                                                            </label>
                                                                                            <div class="custom-select">
                                                                                               <asp:DropDownList ID="ddlMode" runat="server" onchange="Changemode();" CssClass="form-control">
                                                                                                    <asp:ListItem Value="-1" Text="--Select Mode--" data-id="" Selected="True"></asp:ListItem>
                                                                                                    <asp:ListItem Value="1" data-value="Cash" Text="Cash" data-id="SettCashModal"></asp:ListItem>
                                                                                                    <asp:ListItem Value="2"  data-value="Credit" Text="Credit" data-id="SettCreditModal"></asp:ListItem>
                                                                                                    <asp:ListItem Value="3"  data-value="Card" Text="Card" data-id="SettCardModal"></asp:ListItem>
                                                                                                    <asp:ListItem Value="10" data-value="GL" Text="GL" data-id="SettGLModal"></asp:ListItem>
                                                                                                    <%--<asp:ListItem Value="11" Text="Gift Voucher" data-id="SettGiftModal"></asp:ListItem>--%>
                                                                                                    <asp:ListItem Value="12" data-value="Bank_Transfer" Text="Bank Transfer" data-id="SettBankTransferModal" Enabled="false"></asp:ListItem>
                                                                                                </asp:DropDownList>

                                                                                            </div>
                                                                                        </div>
                                                                                    </div>

                                                                                    <div id="CashAmount" class="col-md-6">
                                                                                       <div class="form-group">
                                                        <label>Total RoomAmount</label>
                                                        <input type="text" id="txtAgentAmount" class="form-control" disabled>
                                                    </div>
                                                                                    </div>

                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td colspan="2">
                                                                                <table class="b2b-corp-table table table-bordered table-condensed" id="tblSettlementDetail">
                                                                                    <thead>
                                                                                        <tr class="grid011">
                                                                                            <th align="left" scope="col">
                                                                                                <label style="color: Black; font-weight: bold">
                                                                                                    Amount</label>
                                                                                            </th>
                                                                                            <th align="left" scope="col">
                                                                                                <label style="color: Black; font-weight: bold">
                                                                                                    Mode</label>
                                                                                            </th>
                                                                                            <th align="left" scope="col">
                                                                                                <label style="color: Black; font-weight: bold">
                                                                                                    <span class="mdt"></span>Action</label>
                                                                                            </th>
                                                                                        </tr>
                                                                                    </thead>
                                                                                    <tbody>
                                                                                    </tbody>
                                                                                </table>
                                                                                <asp:HiddenField runat="server" ID="hdnsettlementDetails" Value="" />
                                                                            </td>
                                                                        </tr>

                                                                    </table>
                                                </div>
                                                            <div class="x_content">
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label>Recieved</label>
                                                                            <input type="text" id="recievedAmt" value="0" readonly="readonly" class="form-control" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4">
                                                                        <div class="form-group">
                                                                            <label class="text-danger">Due</label>
                                                                            <input type="text" id="dueAmt" value="0" readonly="readonly" class="form-control text-danger" />
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-4" id="ccAmtDiv" style="display:none" >
                                                                        <div class="form-group">
                                                                            <label>CC Charge</label>
                                                                            <input type="text" id="cardChargeAmt" value="0" readonly="readonly" class="form-control" />
                                                                        </div>
                                                                    </div>
                                                                 <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>ToCollected  </label>
                                                        <input type="text" id="txtToCollected" value="0.00" disabled="disabled" class="form-control">
                                                    </div>
                                                </div>
                                                                </div>

                                        </div>
                                        <div class="col-md-6">
                                            <div class="row no-gutter">

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>INPUT VAT  </label>
                                                        <input type="text" id="txtInVat" value="0.00" disabled="disabled" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label id="lblTtlOutVat">OUTPUT VAT  </label>
                                                        <input type="text" id="txtOutVat" value="0.00" disabled="disabled" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>NetVAT  </label>
                                                        <input type="text" id="txtTotalVat" value="0.00" disabled="disabled" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-3" style="display: none">
                                                    <div class="form-group">
                                                        <label>Total Fare  </label>
                                                        <input type="text" id="txtTotalFare" value="0.00" disabled="disabled" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-3" style="display: none">
                                                    <div class="form-group">
                                                        <label>Discount  </label>
                                                        <input type="text" id="txtDiscount" value="0.00" disabled="disabled" class="form-control">
                                                    </div>
                                                </div>
                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Collected  </label>
                                                        <input type="text" id="txtCollected" value="0.00" disabled="disabled" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Net Payble  </label>
                                                        <input type="text" id="txtNetPay" value="0.00" disabled="disabled" class="form-control">
                                                    </div>
                                                </div>

                                                <div class="col-md-3">
                                                    <div class="form-group">
                                                        <label>Profit  </label>
                                                        <input type="text" id="txtProfit" value="0.00" disabled="disabled" class="form-control">
                                                    </div>
                                                </div>
                                                                                               

                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="">Cancellation Detials:</div>
                                <div>
                                    <asp:RadioButton ID="rbtnNonRefund" Checked="true" runat="server" Text="Non Refundable" GroupName="cancel" onchange="return ShowHeaders();" Width="180px" />
                                    <asp:RadioButton ID="rbtnRefund" runat="server" Text="Refundable" Width="150px" GroupName="cancel" onchange="return ShowHeaders();" />
                                </div>
                                <div class="row no-gutter mt-4">
                                    <div id="divHeaders" style="display: none;">
                                        <input id="hdnCancelCount" type="hidden" value="0" />
                                        <a id="addCancelpolicy" style="height: 20px;" onclick="return ShowDiv();">Add Cancellation Policy <span class=" glyphicon glyphicon-plus-sign"></span></a></br>
   <ul class="ui-bg-wrapper mb-1  ui-room-wrapper" id="ListCancellationPolicies"></ul>
                                        <div id="CancelDiv" style="display: none;">
                                            <%-- <asp:HiddenField ID="hdnCancelDiv" runat="server" />--%>
                                            <table width="100%">
                                                <tr height="30px">
                                                    <td width="20px">&nbsp;
                                                    </td>
                                                    <td width="180px" align="right">
                                                        <b>&nbsp;&nbsp; Cancellation within &nbsp;&nbsp;&nbsp;</b>
                                                    </td>
                                                    <td width="45px;">
                                                        <input type="text" maxlength="3" id="txtCancelDays" style="width: 40px;" ondrop="return false;" onkeypress="return isNumber(event)" />
                                                    </td>
                                                    <td width="200px">&nbsp;&nbsp;days prior to check in &nbsp;
                      <input type="radio" name="Cancel" id="rbtnCancelNights" checked>
                                                    </td>
                                                    <td width="250px">&nbsp;&nbsp;
                       <input type="text" maxlength="2" id="txtCancelNights" style="width: 40px;" ondrop="return false;" onkeypress="return isNumber(event)" />
                                                        &nbsp;&nbsp; nights &nbsp; / &nbsp;
                        <input type="radio" name="Cancel" id="rbtnCancelPercent">
                                                        &nbsp;&nbsp;
                         <input type="text" maxlength="3" id="txtCancelPercent" style="width: 40px;" ondrop="return false;" onkeypress="return isNumber(event)" disabled />
                                                    </td>
                                                    <td width="250px">% of booking amount will be charged &nbsp;&nbsp;&nbsp;
                                                    </td>
                                                    <td>
                                                        <img src="images/delete.gif" alt="Remove" id="btnRemove" />
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                    </div>
                                </div>

                                <div class="row no-gutter">

                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Comment Contract (Terms & Conditions Provided by Supplier will be Displayed on Booking) </label>
                                            <textarea id="txtTerms" class="form-control policy-textarea" rows="5"></textarea>
                                        </div>


                                    </div>

                                    <div class="col-12">
                                        <div class="form-group">
                                            <label>Remark (Important Notes For BackOffice Team) </label>
                                            <textarea id="txtRemarks" class="form-control policy-textarea" rows="5" maxlength="200"></textarea>
                                        </div>
                                    </div>
                                    <div class="col-12" style="display: none">

                                        <h4>Email To </h4>
                                        <div class="form-check-inline">
                                            <label class="form-check-label">
                                                <input id="chkAgtEmail" type="checkbox" class="form-check-input">Agent & Consultant
                                            </label>
                                        </div>
                                    </div>

                                </div>
                            </div>
                            <div id="UDIDDiv" class="tab-pane fade">
                                <div class="row">
                                    <div class="col-md-12" id="divFlex0" style="display: none; margin-top: 15px">
                                        <div class="bg_white p-3">
                                            <div class="ns-h3">Reporting fields </div>
                                            <div class="row" id="divFlexFields0"></div>
                                        </div>
                                    </div>


                                </div>

                            </div>

                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
    <div class="clearfix"></div>
    <div class="modal fade" id="SettlementModalContent" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h5 class="modal-title" id="exampleModalLabel"><span id="settlementModeText"></span> Settlement Mode</h5>
                                <button type="button" class="close" data-dismiss="modal" aria-label="Close" style="margin-top: -25px;">
                                    <span class="icon icon-close"></span>
                                    
                                </button>
                            </div>
                            <!--Payment CASH-->
                            <div class="modal-body" id="SettCashModal" style="display: none">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Amount</label>
                                            <input class="form-control amt" type="text" onkeypress="return isNumber(event)">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Branch</label>
                                            <select class="form-control" id="cashModalBranch">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Settlement Remark</label>
                                            <input class="form-control" type="text" id="settlementRemarkCash">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Payment CREDIT-->
                            <div class="modal-body" id="SettCreditModal" style="display: none">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Amount</label>
                                            <input class="form-control amt" type="text" onkeypress="return isNumber(event)">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Branch</label>
                                            <%--<select class="form-control" id="creditModalBranch">
                                            </select>--%>
                                            <input class="form-control" type="text" id="creditModalBranch" placeholder="Customer Name Here"/>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Settlement Remark</label>
                                            <input class="form-control" type="text" id="settlementRemarkCredit">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Payment CARD-->
                            <div class="modal-body" id="SettCardModal" style="display: none">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Amount</label>
                                            <input class="form-control amt" type="text" onkeypress="return isNumber(event)">
                                            <label id="lblIncludeCharge" ></label>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Card Type</label>
                                            <select class="form-control" id="cardType">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Card Number (Last 4 digit)</label>
                                            <input class="form-control" type="text" id="cardNumber" maxlength="4" onkeypress="return isNumber(event)" >
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Approval Number</label>
                                            <input class="form-control" type="text" id="approvalNumber">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Expiry Date</label>
                                            <input class="form-control" type="date" id="expiryDate">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Card Charge</label>
                                            <input class="form-control" type="text" id="cardCharge" readonly="readonly">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Settlement Remark</label>
                                            <input class="form-control" type="text" id="settlementRemarkCard">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Payment GL-->
                            <div class="modal-body" id="SettGLModal" style="display: none">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Amount</label>
                                            <input class="form-control amt" type="text" onkeypress="return isNumber(event)">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Branch</label>
                                            <select class="form-control" id="glModalBranch">
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">GL Charge Code</label>
                                            <select class="form-control" id="glChargeCode">
                                                <option></option>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Settlement Remark</label>
                                            <input class="form-control" type="text" id="settlementRemarkGl">
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <!--Payment GiftVoucher-->
                            <div class="modal-body" id="SettGiftModal" style="display: none">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Amount</label>
                                            <input class="form-control amt" type="text" onkeypress="return isNumber(event)">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Voucher Type</label>
                                            <select class="form-control" id="voucherType">
                                                <option></option>
                                                <option></option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Voucher Number</label>
                                            <input class="form-control" type="text" id="voucherNumber">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Settlement Remark</label>
                                            <input class="form-control" type="text" id="settlementRemarkGift">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <!--Payment GiftVoucher-->
                            <div class="modal-body" id="SettBankTransferModal" style="display: none">
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Amount</label>
                                            <input class="form-control amt" type="text" onkeypress="return isNumber(event)">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Transfer Type</label>
                                            <select class="form-control" id="transferType">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Ref Number/EN No.</label>
                                            <input class="form-control" type="text" id="refNumberEnNumber">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Ref Date</label>
                                            <input class="form-control" type="date" id="refDate">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Account Number</label>
                                            <input class="form-control" type="text" id="accountNumber">
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Bank</label>
                                            <select class="form-control" id="bankName">
                                                
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-4">
                                        <div class="form-group">
                                            <label class="d-block">Settlement Remark</label>
                                            <input class="form-control" type="text" id="settlementRemarkTransfer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-secondary" data-dismiss="modal" id="Closesettlement">Close</button>
                                <button type="button" class="btn btn-primary" id="addSettlement">Save changes</button>
                            </div>
                        </div>
                    </div>
                </div>
    <div class="row no-gutter">
        <div class="col-md-12" style="text-align: right;">



            <div class="pull-right mr-4 mt-2">
                <a id="btnClear" class="btn btn-primary btn_custom pull-left mr-4" href="#" style="display: none" onclick="clearControls();">Clear </a>
               
                <a id="btnContinue" class="btn btn-primary btn_custom pull-left" style="display: none" onclick="navigateNextTab();" href="#">Continue </a>

                <a id="btnSubmit" style="display: none" onclick="submit();" class="btn btn-primary btn_custom pull-left" href="#">Submit </a>
                <div class="clearfix"></div>
            </div>



        </div>
    </div>
   <script type="text/javascript">
                 function Changemode() {                                                
                        var discount = 0;                       
                        var sellingFare = 0;
                        var tax = 0;
                        var markup = 0;
                        var addlmarkup = 0;
                     var collectedAmount = 0;
                     var ccharge=$("#cardChargeAmt").val();
                      Agentlocations();
                        
                        let cancellationAmt = 0;
                       if (document.getElementById('txtCollected').value > 0)
                       totalPrice = document.getElementById('txtCollected').value.replace(',', '');
                        collectedAmount = eval(totalPrice);

                     
                        //CalculateVAT();
                        editMode = false;
                        editamt = 0;
                        editCharge = 0;
                        if (isNaN(collectedAmount)) {
                            collectedAmount = 0.00;
                        }
                        if (parseFloat(collectedAmount) > 0) {
                            var SettlementMode = $('#ctl00_cphTransaction_ddlMode').val();
                            if (SettlementMode != "-1") {
                                var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                                var existingMode = [];
                                var currentSum = 0;
                                if (ExistingSettlement != null && ExistingSettlement != "") {
                                    let ExistingJson = JSON.parse(ExistingSettlement);
                                    existingMode = $.grep(ExistingJson.SettlementObject, function (item) {
                                        currentSum = parseFloat(parseFloat(currentSum) + parseFloat(item.Amount));
                                        return item.SettlementType == SettlementMode;
                                    });
                                }
                                if (SettlementMode == "3")
                                    document.getElementById('ccAmtDiv').style.display = "block";
                                if (existingMode.length > 0) {
                                    toastr.error($('#ctl00_cphTransaction_ddlMode option:selected').text() + ' already added');
                                    $('#ctl00_cphTransaction_ddlMode').select2('val', '-1');
                                }
                                else if (parseFloat(currentSum) >= parseFloat(collectedAmount)) {
                                    toastr.error('Amount limit exceed');
                                    $('#ctl00_cphTransaction_ddlMode').select2('val', '-1');
                                }
                                else if ($("#ctl00_cphTransaction_ddlCustomer1").val() == "-1") {
                                    toastr.error('Please select customer');
                                    $('#ctl00_cphTransaction_ddlMode').select2('val', '-1');
                                }
                                else {
                                    let SettlementDiv = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-id');
                                    $('#SettlementModalContent .modal-body').hide();

                                    if ($('#ctl00_cphTransaction_ddlMode option:selected').val() == "2") {
                                        //    document.getElementById('ctl00_upProgress').style.display = 'block';
                                        //    $.ajax({
                                        //        url: "http://travlogix1.cozmotravel.com/FinanceInterfaceAPI/api/interface/CMSMasterList",
                                        //        type: "POST",
                                        //        data: JSON.stringify({ "AccountType": "R" }),
                                        //        dataType: "json",
                                        //        contentType: "application/json; charset=utf-8",
                                        //        success: function (cmsResponse) {
                                        //            var cmsJson = cmsResponse;
                                        //            $("#creditModalBranch").children().remove();
                                        //            if (cmsJson.CMSMasterList.length > 0) {
                                        //                $("#creditModalBranch").append('<option value="-1">--Select Branch--</option>');
                                        //                $.each(cmsJson.CMSMasterList, function (cmsInd, cmsVal) {
                                        //                    $("#creditModalBranch").append('<option value="' + cmsVal.Code + '">' + cmsVal.Code + ' - ' + cmsVal.Name + '</option>')
                                        //                });
                                        //                $("#creditModalBranch").select2('val', '-1');
                                        //            }  
                                        //            document.getElementById('ctl00_upProgress').style.display = 'none';
                                        //        }
                                        //    });
                                            $('#SettlementModalContent').modal('show');
                                        $("#settlementModeText").text($('#ctl00_cphTransaction_ddlMode option:selected').text());
                                        if ($('#ddlAgent').val() > 0)
                                            $("#creditModalBranch").val($('#ddlAgent option:selected').text());
                                                 else
                                            $("#creditModalBranch").val('<%=Settings.LoginInfo.AgentName%>');
                                            $('#' + SettlementDiv + '').show();
                                            let remainingAmount = parseFloat(collectedAmount) - parseFloat(currentSum);
                                            $('#' + SettlementDiv + '').find('.amt').val((remainingAmount).toFixed(LoginInfo.Decimal));
                                    }
                                    else {
                                        $('#SettlementModalContent').modal('show');
                                        $("#settlementModeText").text($('#ctl00_cphTransaction_ddlMode option:selected').text());
                                        $('#' + SettlementDiv + '').show();
                                        let remainingAmount = parseFloat(collectedAmount) - parseFloat(currentSum);
                                        $('#' + SettlementDiv + '').find('.amt').val((remainingAmount).toFixed(LoginInfo.Decimal));
                                    }

                                    var ddlMode = document.getElementById('<%=ddlMode.ClientID%>').value;
                                    <%--$('#<%=ddlCustomer1.ClientID%>').select2('val', '-1');
                                        Customerchange();--%>
                                    }

                                }

                        }
                        else {
                            toastr.error('Please enter Room amount');
                            $('#ctl00_cphTransaction_ddlMode').select2('val', '-1');
                        }

                    }

       function loadCreditCardData() {
                            $.ajax({
                                url: "OfflineHotelBooking.aspx/LoadCreditCard",
                                type: "POST",
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (cardResponse) {
                                    let cardData = JSON.parse(cardResponse.d)
                                    if (cardData.length > 0) {
                                        $("#cardType").append('<option value="-1">--Select Card--</option>')
                                        $.each(cardData, function (cardIndex, cardValue) {
                                            $("#cardType").append('<option value="' + cardValue.card_id + '" data-rate="' + cardValue.card_charge + '">' + cardValue.card_name + '</option>');
                                        });
                                    }
                                }
                            });
       }

        function loadTransferTypeData() {
                            $.ajax({
                                url: "OfflineHotelBooking.aspx/LoadTransferType",
                                type: "POST",
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (cardResponse) {
                                    let cardData = JSON.parse(cardResponse.d)
                                    $("#transferType").append('<option value="-1" selected>--Select Transfer Type--</option>')
                                    if (cardData.length > 0) {
                                        $.each(cardData, function (transferIndex, transferValue) {
                                            $("#transferType").append('<option value="' + transferValue.FIELD_ID + '" >' + transferValue.FIELD_TEXT + '</option>');
                                        });
                                    }
                                }
                            });
       }

       function loadBankData() {
                            $.ajax({
                                url: "OfflineHotelBooking.aspx/LoadBankData",
                                type: "POST",
                                dataType: "json",
                                contentType: "application/json; charset=utf-8",
                                success: function (bankResponse) {
                                    let bankData = JSON.parse(bankResponse.d)
                                    $("#bankName").append('<option value="-1" selected>--Select Bank--</option>')
                                    if (bankData.length > 0) {
                                        $.each(bankData, function (bankIndex, bankValue) {
                                            $("#bankName").append('<option value="' + bankValue.BANK_ID + '" >' + bankValue.BANK_NAME + '</option>');
                                        });
                                    }
                                }
                            });
       }

       loadCreditCardData();
       loadTransferTypeData();
       loadBankData();
       $("#cardType").on('change', function () {
                            if ($(this).val() != "-1") {
                                let SettlementDiv = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-id');
                                var amount = $("#" + SettlementDiv + "").find('.amt').val();
                                if (amount != "") {
                                    
                                        $("#cardCharge").val(parseFloat(parseFloat(amount) * parseFloat(parseFloat($("#cardType option:selected").attr('data-rate')) / 100)).toFixed(LoginInfo.Decimal));
                                        var totAmt = parseFloat(parseFloat(amount) + parseFloat($("#cardCharge").val())).toFixed(LoginInfo.Decimal);
                                        $("#lblIncludeCharge").text(totAmt + " will be deducted from card")

                                   
                                }
                            }
                            else {
                                $("#cardCharge").val(0.00);
                                $("#lblIncludeCharge").text("");
                            }
                            
                        });


   </script>
   <script type="text/javascript">
        $("#addSettlement").on('click', function () {
                            var valid = true;
                            let SettlementDiv = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-id');
                            var amount = $("#" + SettlementDiv + "").find('.amt').val();
                            var SettlementMode = $('#ctl00_cphTransaction_ddlMode').val();
                            var settlementArray = [];
                            var settlementObject = {};
                            if (SettlementMode == '1') {
                                if (amount == "") {
                                    toastr.error('Invalid amount');
                                    valid = false;
                                }
                                else if (isNaN(parseFloat(amount))) {
                                    toastr.error('Please enter valid amount');
                                    valid = false;
                                }
                                else if ($('#cashModalBranch').val() == "-1" || $('#cashModalBranch').val() == "0") {
                                    toastr.error('Please select branch');
                                    valid = false;
                                }
                                else if ($("#settlementRemarkCash").val() == "") {
                                    toastr.error('Please enter remark');
                                    valid = false;
                                }
                                if (valid) {
                                    settlementObject["ServiceProductId"] = 2;
                                    settlementObject["ServiceReferenceId"] = 0;
                                    settlementObject["SettlementType"] = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-value');
                                    settlementObject["SettlementName"]=$('#ctl00_cphTransaction_ddlMode').val();
                                    settlementObject["Amount"] = parseFloat(amount);
                                    settlementObject["Currency"] = defaultAgentInfo.Currency;
                                    settlementObject["FOPDetail1"] = $('#cashModalBranch').val();
                                    settlementObject["FOPDetailRemarks"] = $("#settlementRemarkCash").val();
                                }

                            }
                            else if (SettlementMode == '2') {

                                if (amount == "") {
                                    toastr.error('Invalid amount');
                                    valid = false;
                                }
                                else if (isNaN(parseFloat(amount))) {
                                    toastr.error('Please enter valid amount');
                                    valid = false;
                                }
                                else if ($('#creditModalBranch').val() == "") {
                                    toastr.error('Please enter branch');
                                    valid = false;
                                }
                                else if ($("#settlementRemarkCredit").val() == "") {
                                    toastr.error('Please enter remark');
                                    valid = false;
                                }
                                if (valid) {
                                    settlementObject["ServiceProductId"] = 2;
                                    settlementObject["ServiceReferenceId"] = 0;
                                    settlementObject["SettlementType"] = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-value');
                                    settlementObject["SettlementName"]=$('#ctl00_cphTransaction_ddlMode').val();
                                    settlementObject["Amount"] = parseFloat(amount);
                                    settlementObject["Currency"] =defaultAgentInfo.Currency;
                                    settlementObject["FOPDetail1"] = $('#creditModalBranch').val();
                                    //settlementObject["FOPDetail2"] = $('#creditModalBranch').val(); //$('#creditModalBranch option:selected').text(); 
                                    //settlementObject["FOPDetail3"] = $('#creditModalBranch option:selected').attr('data-activity');
                                    settlementObject["FOPDetailRemarks"] = $("#settlementRemarkCredit").val();
                                }
                            }
                            else if (SettlementMode == '3') {
                                if (amount == "") {
                                    toastr.error('Invalid amount');
                                    valid = false;
                                }
                                else if (isNaN(parseFloat(amount))) {
                                    toastr.error('Please enter valid amount');
                                    valid = false;
                                }
                                else if ($('#cardType').val() == "-1") {
                                    toastr.error('Please select card type');
                                    valid = false;
                                }
                                else if ($('#cardNumber').val() == "") {
                                    toastr.error('Please enter card number');
                                    valid = false;
                                }
                                else if ($("#settlementRemarkCard").val() == "") {
                                    toastr.error('Please enter remark');
                                    valid = false;
                                }
                                else if ($("#expiryDate").val() == "") {
                                    toastr.error('Please select ExpiryDate');
                                    valid = false;
                                }
                                if (valid) {

                                    settlementObject["ServiceProductId"] = 2;
                                    settlementObject["ServiceReferenceId"] = 0;
                                    settlementObject["SettlementType"] = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-value');
                                    settlementObject["SettlementName"]=$('#ctl00_cphTransaction_ddlMode').val();
                                    settlementObject["Currency"] = defaultAgentInfo.Currency;
                                    settlementObject["FOPDetail1"] = $('#cardType').val();;
                                    settlementObject["FOPDetail2"] = $('#cardType option:selected').attr('data-rate');
                                    settlementObject["FOPDetail3"] = $('#cardCharge').val();
                                    settlementObject["FOPDetail4"] = $('#cardNumber').val();
                                    settlementObject["FOPDetail5"] = $('#expiryDate').val();
                                    settlementObject["FOPDetail6"] = $('#approvalNumber').val();
                                    settlementObject["Amount"] = parseFloat(amount);
                                    settlementObject["FOPDetailRemarks"] = $("#settlementRemarkCard").val();
                                   var cardCharge = parseFloat(amount) * parseFloat(parseFloat($("#cardType option:selected").attr('data-rate')) / 100);
                                    var collAmount = document.getElementById('txtCollected').value.replace(/,/g, '');
                                    //document.getElementById('txtCollected').value = parseFloat(eval(cardCharge) + eval(collAmount)).toFixed(LoginInfo.Decimal);
                                    document.getElementById('txtToCollected').value = parseFloat(eval(cardCharge) + eval(collAmount)).toFixed(LoginInfo.Decimal);
                                }

                            }
                            else if (SettlementMode == '10') {
                                if (amount == "") {
                                    toastr.error('Invalid amount');
                                    valid = false;
                                }
                                else if (isNaN(parseFloat(amount))) {
                                    toastr.error('Please enter valid amount');
                                    valid = false;
                                }
                                else if ($('#glModalBranch').val() == "-1" || $('#glModalBranch').val() == "0") {
                                    toastr.error('Please select branch');
                                    valid = false;
                                }
                                else if ($('#glChargeCode').val() == "-1") {
                                    toastr.error('Please select charge');
                                    valid = false;
                                }
                                else if ($("#settlementRemarkGl").val() == "") {
                                    toastr.error('Please enter remark');
                                    valid = false;
                                }
                                if (valid) {
                                    settlementObject["ServiceProductId"] = 2;
                                    settlementObject["ServiceReferenceId"] = 0;
                                    settlementObject["SettlementType"] = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-value');
                                    settlementObject["SettlementName"]=$('#ctl00_cphTransaction_ddlMode').val();
                                    settlementObject["Amount"] = parseFloat(amount);
                                    settlementObject["Currency"] =defaultAgentInfo.Currency;
                                    settlementObject["FOPDetail1"] = $('#glModalBranch').val();
                                    settlementObject["FOPDetail2"] = $('#glChargeCode').val();
                                    settlementObject["FOPDetailRemarks"] = $("#settlementRemarkGl").val();
                                }
                            }
                            else if (SettlementMode == '12') {
                                if (amount == "") {
                                    toastr.error('Invalid amount');
                                    valid = false;
                                }
                                else if (isNaN(parseFloat(amount))) {
                                    toastr.error('Please enter valid amount');
                                    valid = false;
                                }
                                else if ($('#transferType').val() == "-1") {
                                    toastr.error('Please select transfer type');
                                    valid = false;
                                }
                                else if ($("#refNumberEnNumber").val() == "") {
                                    toastr.error('Please enter referene number');
                                    valid = false;
                                }
                                else if ($("#accountNumber").val() == "") {
                                    toastr.error('Please enter account number');
                                    valid = false;
                                }
                                else if ($("#refDate").val() == "") {
                                    toastr.error('Please enter refernce date');
                                    valid = false;
                                }
                                else if ($("#bankName").val() == "-1") {
                                    toastr.error('Please select bank');
                                    valid = false;
                                }
                                else if ($("#settlementRemarkTransfer").val() == "") {
                                    toastr.error('Please enter remark');
                                    valid = false;
                                }
                                if (valid) {
                                    settlementObject["ServiceProductId"] = 2;
                                    settlementObject["ServiceReferenceId"] = 0;
                                    settlementObject["SettlementType"] = $('#ctl00_cphTransaction_ddlMode option:selected').attr('data-value');
                                    settlementObject["SettlementName"]=$('#ctl00_cphTransaction_ddlMode').val();
                                    settlementObject["Amount"] = parseFloat(amount);
                                    settlementObject["Currency"] = defaultAgentInfo.Currency;
                                    settlementObject["FOPDetail1"] = $('#transferType').val();
                                    settlementObject["FOPDetail2"] = $('#refNumberEnNumber').val();
                                    settlementObject["FOPDetail3"] = $('#accountNumber').val();
                                    settlementObject["FOPDetail4"] = $('#bankName').val();
                                    settlementObject["FOPDetail8"] = $('#refDate').val();;
                                    settlementObject["FOPDetailRemarks"] = $("#settlementRemarkTransfer").val();
                                }

                            }
                            if (valid) {


                                var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();

                                if (ExistingSettlement != null && ExistingSettlement != "") {
                                    let ExistingJson = JSON.parse(ExistingSettlement);
                                    $.each(ExistingJson.SettlementObject, function (item, value) {
                                        if (value.SettlementName != SettlementMode) {
                                            settlementArray.push(value)
                                        }

                                    });
                                }
                                settlementArray.push(settlementObject);
                                $("#tblSettlementDetail tbody").children().remove();
                                $("#ctl00_cphTransaction_hdnsettlementDetails").val(JSON.stringify({ "SettlementObject": settlementArray }));
                                $('#ctl00_cphTransaction_ddlMode').select2('val', '-1');
                                
                                var ExistingSettlementRecalculate = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                                var recalculateJson = JSON.parse(ExistingSettlementRecalculate);
                                var totalRecivd = 0;
                                var totalDue = 0;
                                var ccCharge = 0;
                                $.each(recalculateJson.SettlementObject, function (item, value) {
                                    let serviceChargeStr = ""
                                    if (value.SettlementName == '3') {
                                        serviceChargeStr = "(Card charge " + value.FOPDetail3 + ")";
                                        ccCharge = parseFloat(value.FOPDetail3).toFixed(LoginInfo.Decimal);
                                        totalRecivd = parseFloat(parseFloat(totalRecivd) + parseFloat(value.Amount) + parseFloat(ccCharge)).toFixed(LoginInfo.Decimal);
                                    }
                                    else {
                                        totalRecivd = parseFloat(parseFloat(totalRecivd) + parseFloat(value.Amount)).toFixed(LoginInfo.Decimal);
                                    }
                                    
                                    $("#tblSettlementDetail tbody").append('<tr><td>' + value.Amount + ' ' + serviceChargeStr + '</td>\
                                                                <td>'+ $("#ctl00_cphTransaction_ddlMode option[value='" + value.SettlementName + "']").text() + '</td>\
                                                                <td><a href="#" class="ml-3 text-info editMode">\
									                                    <span class="icon icon-edit">Edit</span>\
								                                    </a>\
								                                    <a href="#" class="ml-3 text-danger deleteMode">\
									                                    <span class="icon icon-delete">Delete</span>\
								                                    </a></td></tr>');
                                });
                                var totalTocollect = CalculateCollectedAmtForAll();                               
                                totalDue = parseFloat(parseFloat(totalTocollect)- totalRecivd).toFixed(LoginInfo.Decimal);
                                $("#recievedAmt").val(totalRecivd);
                                $("#dueAmt").val(totalDue);
                                $("#cardChargeAmt").val(ccCharge);
                                //$("#totalToCollectAmt").val(parseFloat(totalTocollect).toFixed(LoginInfo.decimalpoint));
                                $('#SettlementModalContent').modal('hide');
                            }
       });
        $('body').on('click', '.deleteMode', function () {
                            var parentRow = $(this).parents('tr:eq(0)');                            
                            var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                            if (ExistingSettlement != null && ExistingSettlement != "") {
                                let ExistingJson = JSON.parse(ExistingSettlement);
                                let curObj = ExistingJson.SettlementObject[parentRow[0].rowIndex - 1];
                                var ccCharge = 0;
                               
                                if (curObj.SettlementName == '3') {
                                   ccCharge = parseFloat(curObj.FOPDetail3).toFixed(LoginInfo.Decimal);
                                    var cashamt = eval(document.getElementById('txtAgentAmount').value.replace(/,/g, ''));
                                }
                                var totalRecivd = 0;
                                var totalDue = 0;
                                var ccCharge2 = 0;
                                $.each(ExistingJson.SettlementObject, function (item, value) {
                                    if (parseInt(item) != parseInt(parentRow[0].rowIndex - 1)) {
                                        if (value.SettlementName == '3') {
                                            ccCharge2 = parseFloat(value.FOPDetail3).toFixed(LoginInfo.Decimal);
                                            totalRecivd = parseFloat(parseFloat(totalRecivd) + parseFloat(value.Amount) + parseFloat(ccCharge2)).toFixed(LoginInfo.Decimal);
                                        }
                                        else {
                                            totalRecivd = parseFloat(parseFloat(totalRecivd) + parseFloat(value.Amount)).toFixed(LoginInfo.Decimal);
                                        }
                                    }                                   
                                    
                                });
                                var totalTocollect = CalculateCollectedAmtForAll();                               
                                totalDue = parseFloat(parseFloat(totalTocollect) - totalRecivd).toFixed(LoginInfo.Decimal);
                                $("#recievedAmt").val(totalRecivd);
                                $("#dueAmt").val(parseFloat(parseFloat(totalDue) - parseFloat(ccCharge)).toFixed(LoginInfo.Decimal));
                                $("#cardChargeAmt").val(parseFloat($("#cardChargeAmt").val()) - ccCharge);
                                $("#txtToCollected").val(parseFloat(totalTocollect-ccCharge).toFixed(LoginInfo.Decimal));
                                ExistingJson.SettlementObject.splice(parentRow[0].rowIndex - 1, 1);

                                $("#ctl00_cphTransaction_hdnsettlementDetails").val(JSON.stringify({ "SettlementObject": ExistingJson.SettlementObject }));
                                $(this).parents('tr:eq(0)').remove();
                            }
                        });
        

       $('body').on('click', '.editMode', function () {
                            editMode = true;
                            var parentRow = $(this).parents('tr:eq(0)');
                            var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                            var currentSum = 0;
                            if (ExistingSettlement != null && ExistingSettlement != "") {
                                let ExistingJson = JSON.parse(ExistingSettlement);
                                $.grep(ExistingJson.SettlementObject, function (item) {
                                    currentSum = parseFloat(parseFloat(currentSum) + parseFloat(item.Amount));
                                });
                                currentSum = parseFloat(parseFloat(currentSum) - parseFloat(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].Amount));
                                var OpenDivId = $('#ctl00_cphTransaction_ddlMode option[value="' + ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName + '"]').attr('data-id');
                                $('#SettlementModalContent .modal-body').hide();
                                $("#settlementModeText").text($('#ctl00_cphTransaction_ddlMode option[value="' + ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName + '"]').text());
                                $('#SettlementModalContent').modal('show');
                                $('#' + OpenDivId + '').show();
                                $('#ctl00_cphTransaction_ddlMode').select2('val', ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName);
                                editamt = ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].Amount;
                                if (ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName == '3') {
                                    editCharge = ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail3;
                                }
                                if (ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName == "1") {
                                    $("#SettCashModal .amt").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].Amount.toFixed(LoginInfo.Decimal));
                                    $("#cashModalBranch").select2('val', ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail1);
                                    $("#settlementRemarkCash").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetailRemarks);
                                }
                                else if (ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName == "2") {
                                    $("#SettCreditModal .amt").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].Amount.toFixed(LoginInfo.Decimal));
                                    $("#creditModalBranch").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail1);
                                    $("#settlementRemarkCredit").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetailRemarks);
                                }
                                else if (ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].SettlementName == "3") {
                                    $("#SettCardModal .amt").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].Amount.toFixed(LoginInfo.Decimal));
                                    $("#cardType").select2('val', ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail1);
                                    $("#cardNumber").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail4);
                                    $("#approvalNumber").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail6);
                                    $("#expiryDate").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail5);
                                    $("#settlementRemarkCard").val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetailRemarks);
                                    $('#cardCharge').val(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].FOPDetail3);
                                    var totAmt = parseFloat(parseFloat(ExistingJson.SettlementObject[parentRow[0].rowIndex - 1].Amount) + parseFloat($("#cardCharge").val())).toFixed(LoginInfo.Decimal);
                                    $("#lblIncludeCharge").text(totAmt + " will be deducted from card");
                                    

                                }

                            }
                        });

        $('.amt').on('change', function () {
                            var SettlementMode = $('#ctl00_cphTransaction_ddlMode').val();
                            var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                            var currentSum = 0;
                            var cardCharge = 0;
                            if (ExistingSettlement != null && ExistingSettlement != "") {
                                let ExistingJson = JSON.parse(ExistingSettlement);
                                $.grep(ExistingJson.SettlementObject, function (item) {
                                    if (item.SettlementType != SettlementMode) {
                                        currentSum = parseFloat(parseFloat(currentSum) + parseFloat(item.Amount));
                                        if (item.SettlementType == "3") {
                                            cardCharge = parseFloat(parseFloat(cardCharge) + parseFloat(item.FOPDetail3))
                                        }
                                    }
                                });
                            }
                            currentSum = parseFloat(parseFloat(currentSum) + parseFloat($(this).val()));
                            var totalPrice = 0;
                            var collectedAmount = 0;

                            let cancellationAmt = 0;

                            if (document.getElementById('txtToCollected').value > 0)
                            totalPrice = document.getElementById('txtToCollected').value.replace(',', '');

                            collectedAmount = eval(totalPrice);

                           
                            //CalculateVAT();
                            if (isNaN(collectedAmount)) {
                                collectedAmount = 0.00;
                            }
                            
                            if (currentSum > collectedAmount) {
                                toastr.error('Invalid amount. Please check Collected amount');
                                $(this).val(0);
                                return false;
            }
             else {
                                if ($("#ctl00_cphTransaction_ddlMode").val() == '3') {
                                    $("#cardType").trigger('change');
                                }
                                $(this).val(parseFloat($(this).val()).toFixed(LoginInfo.Decimal));
                            }

        });

        
                function Agentlocations() {
	
	        var agentid = defaultAgentInfo.AgentId;
	
	         if (agentid != '0' && agentid != '-1') {
		 
		         $("#cashModalBranch,#glModalBranch").empty();
		         $("#cashModalBranch,#glModalBranch").append($("<option></option>").val('0').html('Select Location'));
		         var locdata = document.getElementById('ctl00_cphTransaction_hdnloginAgentLocations').value;
		         var obj = JSON.parse(locdata);
                     $.each(obj, function (locdata, value) {
                         $("#cashModalBranch,#glModalBranch").append($("<option></option>").val(value.LOCATION_ID).html(value.MAP_CODE_NAME));
                     });
	         }
       }

        function calculateRecievedandDue() {
                        var totalRecivd = 0;
                        var totalDue = 0;
                        var ccCharge = 0;
                        var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                        var currentSum = 0;
                        if (ExistingSettlement != null && ExistingSettlement != "") {
                            let ExistingJson = JSON.parse(ExistingSettlement);
                            $.each(ExistingJson.SettlementObject, function (item, value) {
                                if (value.SettlementType == '3') {
                                    ccCharge = parseFloat(value.FOPDetail3).toFixed(LoginInfo.Decimal);
                                }
                                totalRecivd = parseFloat(parseFloat(currentSum) + parseFloat(value.Amount) + parseFloat(ccCharge)).toFixed(LoginInfo.Decimal);
                            });
                        }
                        var totalTocollect = CalculateCollectedAmtForAll();
                        totalDue = parseFloat(parseFloat(totalTocollect) + parseFloat(ccCharge) - totalRecivd).toFixed(LoginInfo.Decimal);
                        return { 'recieved': totalRecivd, 'due': totalDue, 'ccCharge': ccCharge };
       }

       function CalculateCollectedAmtForAll() {
                        var discount = 0;
                        var comm = 0;
                        var sellingFare = 0;
                        var tax = 0;
                        var markup = 0;
                        var addlmarkup = 0;
                        var collectedAmount = 0;
                        var GST = 0;
                        var inpVat = 0;
                        var outVat = 0;
           var ccCharge = 0;
           var totalAmount = 0;
                       if (document.getElementById('txtToCollected').value.trim().length > 0) {
                            totalAmount = document.getElementById('txtToCollected').value.replace(',', '');
                        }
                        
                       // discount = document.getElementById('txtDiscount').value.replace(',', '');
                        
                       // if (document.getElementById('txtOutVat').value.trim().length > 0) {
                       //     outVat = document.getElementById('txtOutVat').value.replace(',', '');
                       // }
                       
                       // let cancellationAmt = 0;
                       // var ExistingSettlement = $("#ctl00_cphTransaction_hdnsettlementDetails").val();
                       // if (ExistingSettlement != null && ExistingSettlement != "") {
                       //     let ExistingJson = JSON.parse(ExistingSettlement);
                       //     $.each(ExistingJson.SettlementObject, function (item, value) {
                       //         if (value.SettlementType == '3') {
                       //             ccCharge = parseFloat(value.FOPDetail3).toFixed(LoginInfo.decimalpoint);
                       //         }                               
                       //     });
                       // }
                        collectedAmount = eval(totalAmount) ;
                        if (isNaN(collectedAmount)) {
                            collectedAmount = 0.00;
                        }
                        return collectedAmount;
                    }
       $("#Closesettlement").on('click', function () {
           $('#ctl00_cphTransaction_ddlMode').select2('val', '-1');
        });


    </script>
</asp:Content>

