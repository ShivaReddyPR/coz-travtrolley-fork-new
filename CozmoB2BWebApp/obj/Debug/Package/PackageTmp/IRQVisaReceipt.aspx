﻿<%@ Page Title="IRQ Visa Receipt" Language="C#" EnableEventValidation="false" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="IRQVisaReceipt" Codebehind="IRQVisaReceipt.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <asp:HiddenField runat="server" ID="hdnVisaPrice" />
    <%-- <asp:HiddenField runat="server" ID="hdnNationality" />
    <asp:HiddenField runat="server" ID="hdnChdNationality" />
    <asp:HiddenField runat="server" ID="hdnInfNationality" />--%>
    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />

    <script src="yui/build/utilities/utilities.js" type="text/javascript"></script>
    <script type="text/javascript">
        var openFile = function (event, id, type, paxType, spnId, imgId) {
            var input = event.target;

            var reader = new FileReader();
            reader.onload = function () {
                var dataURL = reader.result;
                var binary = "";
                var bytes = new Uint8Array(reader.result);
                var length = bytes.byteLength;
                for (var i = 0; i < length; i++) {
                    binary += String.fromCharCode(bytes[i]);
                }
                document.getElementById('hdnPP' + paxType + type + "_" + id).value = btoa(binary);
                saveBlobAsFile(input.files[0], type, id, paxType);

                //for Birth Certificate span is not present
                if (type != 'Birth') {
                    //removing the span errormessages
                    document.getElementById(spnId).value = '';
                    document.getElementById(spnId).style.display = 'none';
                }

                //showing the image uploaded
                document.getElementById(imgId).style.display = 'block';
            };
            reader.readAsArrayBuffer(input.files[0]);

        };

        function saveBlobAsFile(blob, type, id, paxType) {

            var reader = new FileReader();

            reader.onloadend = function () {
                var base64 = reader.result;
                document.getElementById('PP' + paxType + type + "_" + id).src = base64;
            };

            reader.readAsDataURL(blob);
        }
        function paxCountChanged() {
            var visaType = document.getElementById('<%=ddlVisaType.ClientID%>').value;
            //clear();
            if (visaType != "-1") {

                document.getElementById('ddlAdult').disabled = false;
                document.getElementById('ddlChild').disabled = false;
                document.getElementById('ddlInfant').disabled = false;
                document.getElementById('ctl00_cphTransaction_btnSave').style.display = "block";

                document.getElementById('txtAdtFullName_1').disabled = false;
                document.getElementById('ddlAdtdobDay_1').disabled = false;
                document.getElementById('ddlAdtdobMonth_1').disabled = false;
                document.getElementById('ddlAdtdobYear_1').disabled = false;
                document.getElementById('txtAdtPassport_1').disabled = false;
                document.getElementById('ddlAdtPassExpiryDay_1').disabled = false;
                document.getElementById('ddlAdtPassExpiryMonth_1').disabled = false;
                document.getElementById('ddlAdtPassExpiryYear_1').disabled = false;
                document.getElementById('fileAdtPassportFront_1').disabled = false;
                document.getElementById('fileAdtPassportBack_1').disabled = false;
                document.getElementById('fileAdtPassportPhoto_1').disabled = false;
                document.getElementById('fileAdtBirthCerificate_1').disabled = false;

                var adtCount = document.getElementById('ddlAdult').value;
                var chdCount = document.getElementById('ddlChild').value;
                var infCount = document.getElementById('ddlInfant').value;

                var adtCounter = eval(document.getElementById('ctl00_cphTransaction_hdnAdultCounter').value);
                var chdCounter = eval(document.getElementById('ctl00_cphTransaction_hdnChildCounter').value);
                var infCounter = eval(document.getElementById('ctl00_cphTransaction_hdnInfantCounter').value);


                document.getElementById('ctl00_cphTransaction_hdnAdultCount').value = adtCount;
                document.getElementById('ctl00_cphTransaction_hdnChildCount').value = chdCount;
                document.getElementById('ctl00_cphTransaction_hdnInfantCount').value = infCount;
                var priceArray = visaType.toString().split('~');
                var totalPaxCount = (parseInt(adtCount) + parseInt(chdCount) + parseInt(infCount));

                var visaPrice = parseInt(priceArray[1]) * totalPaxCount;
                document.getElementById('lblPrice').value = '<%=currency %>' + ' ' + parseFloat(visaPrice).toFixed(2);

                
                if (totalPaxCount > (adtCounter + chdCounter + infCounter)) {
                    //document.getElementById('addMore1').style.display = "block";
                    AddNewRow();
                }
                else
                {//Remove applicant row             
                    if ((adtCounter - adtCount) > 0) {
                        for (var i = parseInt(adtCount) ; adtCount < adtCounter; i++) {
                            removeApplicant(eval(i + 1), 'Adt');
                            adtCounter--;
                        }
                    }

                    if ((chdCounter - chdCount) > 0) {
                        for (var i = parseInt(chdCount) ; chdCount < chdCounter; i++) {
                            removeApplicant(eval(i + 1), 'Chd');
                            chdCounter--;
                        }
                    }

                    if ((infCounter - infCount) > 0) {
                        for (var i = parseInt(infCount) ; infCount < infCounter; i++) {
                            removeApplicant(eval(i + 1), 'Inf');
                            infCounter--;
                        }
                    }

                    (document.getElementById('ctl00_cphTransaction_hdnAdultCounter').value) = adtCounter;
                    (document.getElementById('ctl00_cphTransaction_hdnChildCounter').value) = chdCounter;
                    (document.getElementById('ctl00_cphTransaction_hdnInfantCounter').value) = infCounter;

                }
            } else {
                alert('Please Select VisaType');
                document.getElementById('ddlAdult').disabled = true;
                document.getElementById('ddlChild').disabled = true;
                document.getElementById('ddlInfant').disabled = true;

                document.getElementById('txtAdtFullName_1').disabled = true;
                document.getElementById('ddlAdtdobDay_1').disabled = true;
                document.getElementById('ddlAdtdobMonth_1').disabled = true;
                document.getElementById('ddlAdtdobYear_1').disabled = true;
                document.getElementById('txtAdtPassport_1').disabled = true;
                document.getElementById('ddlAdtPassExpiryDay_1').disabled = true;
                document.getElementById('ddlAdtPassExpiryMonth_1').disabled = true;
                document.getElementById('ddlAdtPassExpiryYear_1').disabled = true;
                document.getElementById('fileAdtPassportFront_1').disabled = true;
                document.getElementById('fileAdtPassportBack_1').disabled = true;
                document.getElementById('fileAdtPassportPhoto_1').disabled = true;
                document.getElementById('fileAdtBirthCerificate_1').disabled = true;


                document.getElementById('ctl00_cphTransaction_btnSave').style.display = "none";
                document.getElementById('lblPrice').value = '<%=currency %>' + ' ' + 0;
                //document.getElementById('addMore1').style.display = "none";
            }
        }

        function isAlpha(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 32 || keyCode == 9 || keyCode == 8 || keyCode == 11);
            return ret;
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && ((charCode < 47 || charCode > 57))) {
                return false;
            }
            else {
                return true;
            }
        }

        function isAlphaNumeric(e) {
            //cal3.hide();
            //cal4.hide();
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || keyCode == 9 || keyCode == 8 || keyCode == 11);
            return ret;
        }


        var NumOfRow = 1;

        function AddNewRow() {
            //if (validate())
            {
                // get the refference of the main Div
                var mainDiv = document.getElementById('divApplicantDet');

                var adults = eval(document.getElementById('ddlAdult').value);
                var childs = eval(document.getElementById('ddlChild').value);
                var infants = eval(document.getElementById('ddlInfant').value);
                var totalPax = adults + childs + infants;

                var adtCounter = eval(document.getElementById('ctl00_cphTransaction_hdnAdultCounter').value);
                var chdCounter = eval(document.getElementById('ctl00_cphTransaction_hdnChildCounter').value);
                var infCounter = eval(document.getElementById('ctl00_cphTransaction_hdnInfantCounter').value);

                var adtDetails = document.getElementById('ctl00_cphTransaction_hdnAdtApplicantDetails').value;
                var chdDetails = document.getElementById('ctl00_cphTransaction_hdnChdApplicantDetails').value;
                var infDetails = document.getElementById('ctl00_cphTransaction_hdnInfApplicantDetails').value;

                //if (adtDetails != "" || chdDetails != "" || infDetails != "") {
                //    var adtDetList = adtDetails.split(',');
                //    var chdDetList = chdDetails.split(',');
                //    var infDetList = infDetails.split(',');
                    
                //    if (adtDetList != "" && adtDetList.length > 0) {
                //        if (adtDetList[0] != "") {
                //            for (var i = 0; i < adtDetList.length; i++) {
                //                if (parseInt(adtDetList[i]) > 0) {
                //                    AddPaxTypeRow('Adt', mainDiv, parseInt(adtDetList[i]));
                //                    adtCounter += 1;
                //                }
                //            }
                //        }
                //    }
                //    if (chdDetList != "" && chdDetList.length > 0) {
                //        if (chdDetList[0] != "") {
                //            for (var i = 0; i < chdDetList.length; i++) {
                //                if (parseInt(chdDetList[i]) > 0) {
                //                    AddPaxTypeRow('Chd', mainDiv, parseInt(chdDetList[i]));
                //                    chdCounter += 1;
                //                }
                //            }
                //        }
                //    }
                //    if (infDetList != "" && infDetList.length > 0) {
                //        if (infDetList[0] != "") {
                //            for (var i = 0; i < infDetList.length; i++) {
                //                if (parseInt(infDetList[i]) > 0) {
                //                    AddPaxTypeRow('Inf', mainDiv, parseInt(infDetList[i]));
                //                    infCounter += 1;
                //                }
                //            }
                //        }
                //    }
                //}

                for (var i = adtCounter; i < adults; i++) {
                    if (adtCounter < adults) {
                        AddPaxTypeRow('Adt', mainDiv, eval(i + 1));
                        adtCounter += 1;
                    }
                }

                for (var i = chdCounter; i < childs; i++) {
                    if (chdCounter < childs) {
                        AddPaxTypeRow('Chd', mainDiv, eval(i + 1));
                        chdCounter += 1;
                    }
                }

                for (var i = infCounter; i < infants; i++) {
                    if (infCounter < infants) {
                        AddPaxTypeRow('Inf', mainDiv, eval(i + 1));
                        infCounter += 1;
                    }
                }

                document.getElementById('ctl00_cphTransaction_hdnAdultCounter').value = adtCounter;
                document.getElementById('ctl00_cphTransaction_hdnChildCounter').value = chdCounter;
                document.getElementById('ctl00_cphTransaction_hdnInfantCounter').value = infCounter;

                //if (totalPax == eval(adtCounter + chdCounter + infCounter)) {
                //    document.getElementById('addMore1').style.display = 'none';
                //}
            }
        }

        function AddPaxTypeRow(type, maindiv, index) {
            var div = document.createElement('div');
            //div.className = "row";
            div.setAttribute('id', 'divMain_' + type + '_' + index);

            var divCol = document.createElement('div');
            divCol.className = "col-md-3";
            divCol.setAttribute('id', 'divLabel_' + type + '_' + index);
            var paxType;

            if (type == 'Adt') {
                paxType = 'Adult';
            }
            else if (type == 'Chd') {
                paxType = 'Child';
            }
            else {
                paxType = 'Infant';
            }

            var lbl = document.createElement('label');
            lbl.id = 'lbl_' + type +"_"+ index;
            lbl.innerHTML = "<strong>" + paxType + " Applicant " + index + ":</strong>";

            divCol.appendChild(lbl);
            div.appendChild(divCol);
            //maindiv.appendChild(div);

            AddPaxTypeControls(type, maindiv, index, div);
        }

        function AddPaxTypeControls(type, mainDiv, id, upperDiv) {
            var div = document.createElement('div');
            div.className = "row";
            div.setAttribute('id', 'divControls_' + type + '_' + id);
            ////////////////////////////////////////////
            ///                 Full Name            ///
            ////////////////////////////////////////////
            var divFName = document.createElement('div');
            divFName.setAttribute('class', 'col-md-3');

            var divFGName = document.createElement('div');
            divFGName.setAttribute('class', 'form-group');

            var lblFName = document.createElement('label');
            lblFName.innerHTML = "Applicant Full Name <span style='color:red'>*</span>:";

            // create new textbox
            var txtFName = document.createElement('input');
            txtFName.type = 'text';
            txtFName.className = "form-control";
            txtFName.setAttribute('id', 'txt' + type + 'FullName_' + id);
            txtFName.name = 'txt' + type + 'FullName_' + id;
            txtFName.setAttribute('onkeypress', 'return isAlpha(event);');
            txtFName.onchange = function () {
                removeErrorMessage('spn' + type + 'FullName_' + id);
            }

            var spanFName = document.createElement('span');
            spanFName.style.display = "none";
            spanFName.style.color = "red";
            spanFName.id = "spn" + type + "FullName_" + id;

            divFGName.appendChild(lblFName);
            divFGName.appendChild(txtFName);

            divFName.appendChild(divFGName);
            divFName.appendChild(spanFName);
            div.appendChild(divFName);


            ///////////////////////////////////////////
            //              DOB control             ///
            ///////////////////////////////////////////
            var divDOB = document.createElement('div');
            divDOB.setAttribute('class', 'col-md-3');

            var divFGDOB = document.createElement('div');
            divFGDOB.setAttribute('class', 'form-group');

            var lblDOB = document.createElement('label');
            lblDOB.innerHTML = "Date Of Birth <span style='color:red'>*</span>:";
            var divrows = document.createElement('div');
            divrows.setAttribute('class', 'row');
            var ddlday = document.createElement('select');
            ddlday.setAttribute('onload', 'BindDays');

            var option1 = document.createElement('option');

            var ddldiv = document.createElement('div');
            ddldiv.setAttribute('class', 'col-md-4')
            ddlday.className = "form-control";

            ddlday.setAttribute('id', 'ddl' + type + "dobDay_" + id);
            ddlday.name = "ddl" + type + "dobDay_" + id;
            ddlday.onchange = function () {
                removeErrorMessage('spn' + type + 'DOB_' + id);
            }


            var ddldobMonth = document.createElement('select');
            var ddldiv1 = document.createElement('div');
            ddldiv1.setAttribute('class', 'col-md-4')
            ddldobMonth.className = "form-control";
            ddldobMonth.setAttribute('id', 'ddl' + type + "dobMonth_" + id);
            ddldobMonth.name = "ddl" + type + "dobMonth_" + id;
            ddldobMonth.onchange = function () {
                removeErrorMessage('spn' + type + 'DOB_' + id);
            }
            //ddlday.setAttribute('disabled', 'disabled');


            var ddldobYear = document.createElement('select');
            var ddldiv2 = document.createElement('div');
            ddldiv2.setAttribute('class', 'col-md-4')
            ddldobYear.className = "form-control";
            ddldobYear.setAttribute('id', 'ddl' + type + "dobYear_" + id);
            ddldobYear.name = "ddl" + type + "dobYear_" + id;
            ddldobYear.onchange = function () {
                removeErrorMessage('spn' + type + 'DOB_' + id);
            }

            for (var i = 0; i <= 31; i++) {
                var opt = document.createElement('option');

                if (i == 0) {
                    opt.value = -1;
                    opt.innerHTML = "Day";
                    opt.setAttribute("selected", "selected");
                    ddlday.appendChild(opt);
                }
                else if (i <= 9) {
                    opt.value = i;
                    opt.innerHTML = "0" + i;
                    ddlday.appendChild(opt);
                }
                else {
                    opt.value = i;
                    opt.innerHTML = i;
                    ddlday.appendChild(opt);
                }

            }
            for (var i = 0; i <= 12; i++) {
                var opt1 = document.createElement('option');

                if (i == 0) {
                    opt1.value = -1;
                    opt1.innerHTML = "Month";
                    opt1.setAttribute("selected", "selected");
                    ddldobMonth.appendChild(opt1);
                }
                if (i == 1) {
                    opt1.value = 1;
                    opt1.innerHTML = "Jan";
                    ddldobMonth.appendChild(opt1);
                }
                else if (i == 2) {
                    opt1.value = 2;
                    opt1.innerHTML = "Feb";
                    ddldobMonth.appendChild(opt1);
                }
                else if (i == 3) {
                    opt1.value = 3;
                    opt1.innerHTML = "Mar";
                    ddldobMonth.appendChild(opt1);
                }
                else if (i == 4) {
                    opt1.value = 4;
                    opt1.innerHTML = "Apr";
                    ddldobMonth.appendChild(opt1);
                }
                else if (i == 5) {
                    opt1.value = 5;
                    opt1.innerHTML = "May";
                    ddldobMonth.appendChild(opt1);
                }
                else if (i == 6) {
                    opt1.value = 6;
                    opt1.innerHTML = "Jun";
                    ddldobMonth.appendChild(opt1);
                }
                else if (i == 7) {
                    opt1.value = 7;
                    opt1.innerHTML = "Jul";
                    ddldobMonth.appendChild(opt1);
                }
                else if (i == 8) {
                    opt1.value = 8;
                    opt1.innerHTML = "Aug";
                    ddldobMonth.appendChild(opt1);
                }
                else if (i == 9) {
                    opt1.value = 9;
                    opt1.innerHTML = "Sep";
                    ddldobMonth.appendChild(opt1);
                }
                else if (i == 10) {
                    opt1.value = 10;
                    opt1.innerHTML = "Oct";
                    ddldobMonth.appendChild(opt1);
                }
                else if (i == 11) {
                    opt1.value = 11;
                    opt1.innerHTML = "Nov";
                    ddldobMonth.appendChild(opt1);
                }
                else if (i == 12) {
                    opt1.value = 12;
                    opt1.innerHTML = "Dec";
                    ddldobMonth.appendChild(opt1);
                }
            }
            var todayDate = new Date();
            var presentYear = parseInt(todayDate.getFullYear());
            for (var i = (presentYear - 101) ; i <= presentYear; i++) {
                var opt2 = document.createElement('option');

                if (i == (presentYear - 101)) {
                    opt2.value = -1;
                    opt2.innerHTML = "Year";
                    opt2.setAttribute("selected", "selected");
                    ddldobYear.appendChild(opt2);
                }
                else {
                    opt2.value = i;
                    opt2.innerHTML = i;
                    ddldobYear.appendChild(opt2);
                }

            }


            var spanDOB = document.createElement('span');
            spanDOB.style.display = "none";
            spanDOB.style.color = "red";
            spanDOB.id = "spn" + type + "DOB_" + id;

            divFGDOB.appendChild(lblDOB);

            ddldiv.appendChild(ddlday);

            ddldiv1.appendChild(ddldobMonth);
            ddldiv2.appendChild(ddldobYear);


            divrows.appendChild(ddldiv);
            divrows.appendChild(ddldiv1);
            divrows.appendChild(ddldiv2);
            divFGDOB.appendChild(divrows);
            divDOB.appendChild(divFGDOB);
            divDOB.appendChild(spanDOB);

            div.appendChild(divDOB);

            ///////////////////////////////////////////
            //      Passport Number control         ///
            ///////////////////////////////////////////

            //var divrows1 = document.createElement('divrow');
            //divrows1.setAttribute('class', 'row');
            var divPN = document.createElement('div');
            divPN.setAttribute('class', 'col-md-2');

            var divFGPN = document.createElement('div');
            divFGPN.setAttribute('class', 'form-group');

            var lblPN = document.createElement('label');
            lblPN.innerHTML = "Passport No <span style='color:red'>*</span>:";

            var txtPN = document.createElement('input');
            txtPN.type = 'text';
            txtPN.className = "form-control";
            txtPN.setAttribute('id', 'txt' + type + 'Passport_' + id);
            txtPN.name = "txt" + type + "Passport_" + id;
            txtPN.onchange = function () {
                removeErrorMessage('spn' + type + 'Passport_' + id);
            }

            var spanPN = document.createElement('span');
            spanPN.style.display = "none";
            spanPN.style.color = "red";
            spanPN.id = "spn" + type + "Passport_" + id;

            divFGPN.appendChild(lblPN);
            divFGPN.appendChild(txtPN);

            divPN.appendChild(divFGPN);
            divPN.appendChild(spanPN);

            div.appendChild(divPN);
            // divrows1.appendChild(divPN)


            ///////////////////////////////////////////
            ///         Passport Expiry control     ///
            ///////////////////////////////////////////


            var divPE = document.createElement('div');
            divPE.setAttribute('class', 'col-md-3');

            var divFGPE = document.createElement('div');
            divFGPE.setAttribute('class', 'form-group');

            var lblPE = document.createElement('label');
            lblPE.innerHTML = "Passport Expiry <span style='color:red'>*</span>:";


            var divrows1 = document.createElement('div');
            divrows1.setAttribute('class', 'row');
            var expday = document.createElement('select');
            var expdiv = document.createElement('div');
            expdiv.setAttribute('class', 'col-md-4')
            expday.className = "form-control";
            expday.setAttribute('id', 'ddl' + type + "PassExpiryDay_" + id);
            expday.name = "ddl" + type + "PassExpiryDay_" + id;
            expday.onchange = function () {
                removeErrorMessage('spn' + type + 'PassExpiry_' + id);
            }

            var expmonth = document.createElement('select');
            var expmdiv = document.createElement('div');
            expmdiv.setAttribute('class', 'col-md-4')
            expmonth.className = "form-control";
            expmonth.setAttribute('id', 'ddl' + type + "PassExpiryMonth_" + id);
            expmonth.name = "ddl" + type + "PassExpiryMonth_" + id;
            expmonth.onchange = function () {
                removeErrorMessage('spn' + type + 'PassExpiry_' + id);
            }
            var expyear = document.createElement('select');
            var expydiv = document.createElement('div');
            expydiv.setAttribute('class', 'col-md-4')
            expyear.className = "form-control";
            expyear.setAttribute('id', 'ddl' + type + "PassExpiryYear_" + id);
            expyear.name = "ddl" + type + "PassExpiryYear_" + id;
            expyear.onchange = function () {
                removeErrorMessage('spn' + type + 'PassExpiry_' + id);
            }



            for (var i = 0; i <= 31; i++) {
                var opt3 = document.createElement('option');

                if (i == 0) {
                    opt3.value = -1;
                    opt3.innerHTML = "Day";
                    opt3.setAttribute("selected", "selected");
                    expday.appendChild(opt3);
                }
                else if (i <= 9) {
                    opt3.value = i;
                    opt3.innerHTML = "0" + i;
                    expday.appendChild(opt3);
                }
                else {
                    opt3.value = i;
                    opt3.innerHTML = i;
                    expday.appendChild(opt3);
                }

            }
            for (var i = 0; i <= 12; i++) {
                var opt4 = document.createElement('option');

                if (i == 0) {
                    opt4.value = -1;
                    opt4.innerHTML = "Month";
                    opt4.setAttribute("selected", "selected");
                    expmonth.appendChild(opt4);
                }
                else if (i == 1) {
                    opt4.value = i;
                    opt4.innerHTML = "Jan";
                    expmonth.appendChild(opt4);
                }
                else if (i == 2) {
                    opt4.value = i;
                    opt4.innerHTML = "Feb";
                    expmonth.appendChild(opt4);
                }
                else if (i == 3) {
                    opt4.value = i;
                    opt4.innerHTML = "Mar";
                    expmonth.appendChild(opt4);
                }
                else if (i == 4) {
                    opt4.value = i;
                    opt4.innerHTML = "Apr";
                    expmonth.appendChild(opt4);
                }
                else if (i == 5) {
                    opt4.value = i;
                    opt4.innerHTML = "May";
                    expmonth.appendChild(opt4);
                }
                else if (i == 6) {
                    opt4.value = i;
                    opt4.innerHTML = "Jun";
                    expmonth.appendChild(opt4);
                }
                else if (i == 7) {
                    opt4.value = i;
                    opt4.innerHTML = "Jul";
                    expmonth.appendChild(opt4);
                }
                else if (i == 8) {
                    opt4.value = i;
                    opt4.innerHTML = "Aug";
                    expmonth.appendChild(opt4);
                }
                else if (i == 9) {
                    opt4.value = i;
                    opt4.innerHTML = "Sep";
                    expmonth.appendChild(opt4);
                }
                else if (i == 10) {
                    opt4.value = i;
                    opt4.innerHTML = "Oct";
                    expmonth.appendChild(opt4);
                }
                else if (i == 11) {
                    opt4.value = i;
                    opt4.innerHTML = "Nov";
                    expmonth.appendChild(opt4);
                }
                else if (i == 12) {
                    opt4.value = i;
                    opt4.innerHTML = "Dec";
                    expmonth.appendChild(opt4);
                }
            }
            for (var i = (presentYear - 1) ; i <= parseInt(presentYear + 40) ; i++) {
                var opt5 = document.createElement('option');

                if (i == (presentYear - 1)) {
                    opt5.value = -1;
                    opt5.innerHTML = "Year";
                    opt5.setAttribute("selected", "selected");
                    expyear.appendChild(opt5);
                }
                else {
                    opt5.value = i;
                    opt5.innerHTML = i;
                    expyear.appendChild(opt5);
                }

            }



            var spanPE = document.createElement('span');
            spanPE.style.display = "none";
            spanPE.style.color = "red";
            spanPE.id = "spn" + type + "PassExpiry_" + id;

            divFGPE.appendChild(lblPE);
            // divFGPE.appendChild(txtPE);
            expdiv.appendChild(expday);
            expmdiv.appendChild(expmonth);
            expydiv.appendChild(expyear);
            divrows1.appendChild(expdiv);
            divrows1.appendChild(expmdiv);
            divrows1.appendChild(expydiv);
            divFGPE.appendChild(divrows1);
            divPE.appendChild(divFGPE);
            divPE.appendChild(spanPE);

            div.appendChild(divPE);


            ///////////////////////////////////////////
            ///         Nationality control     ///
            ///////////////////////////////////////////
            var divNat = document.createElement('div');
            divNat.setAttribute('class', 'col-md-1');

            var divFGNat = document.createElement('div');
            divFGNat.setAttribute('class', 'form-group');

            var lblNat = document.createElement('label');
            lblNat.innerHTML = "Nationality <span style='color:red'>*</span>:";

            var ddlNat = document.createElement('select');
            ddlNat.className = "form-control";
            ddlNat.setAttribute('id', 'ddl' + type + "Nationality_" + id);
            ddlNat.name = "ddl" + type + "Nationality_" + id;
            ddlNat.setAttribute('disabled', 'disabled');
            ddlNat.onchange = function () {
                removeErrorMessage('spn' + type + 'Nationality_' + id);
            }
            var option = document.createElement('option');
            option.innerText = "Iraq";
            option.setAttribute("selected", "selected");
            ddlNat.appendChild(option);

            var spanNat = document.createElement('span');
            spanNat.style.display = "none";
            spanNat.style.color = "red";
            spanNat.id = "spn" + type + "Nationality_" + id;


            divFGNat.appendChild(lblNat);
            divFGNat.appendChild(ddlNat);

            divNat.appendChild(divFGNat);
            divNat.appendChild(spanNat);

            div.appendChild(divNat);

            var fileIndex = eval(id);


            var divPF = document.createElement('div');
            //divPF.className = 'row';

            var divColPF = document.createElement('div');
            divColPF.className = 'col-md-12';

            var divBGPF = document.createElement('div');
            divBGPF.className = 'bggray col-md-12';

            var strong = document.createElement('strong');
            strong.innerHTML = "Documents to be Uploaded";

            divBGPF.appendChild(strong);
            divColPF.appendChild(divBGPF);

            var divPadPF = document.createElement('div');
            divPadPF.className = 'bg_white pad_10';

            //Passport Front page
            var divlblPF = document.createElement('div');
            divlblPF.className = 'col-md-12';
            divlblPF.innerHTML = "Passport Copy Front Page";

            divPadPF.appendChild(divlblPF);

            var divFilePF = document.createElement('div');
            divFilePF.className = 'col-md-12';

            var filePF = document.createElement('input');
            filePF.type = 'file';
            filePF.setAttribute('onchange', 'openFile(event,' + fileIndex + ',"Front","' + type + '","spn' + type + 'PassportFront_' + fileIndex + '","PP' + type + 'Front_' + fileIndex + '");');
            filePF.setAttribute('accept', 'image/*');
            filePF.id = 'file' + type + 'PassportFront_' + fileIndex;
            filePF.name = 'file' + type + 'passportFront_' + fileIndex;
            
            divFilePF.appendChild(filePF);

            var hdnPF = document.createElement('input');
            hdnPF.type = 'hidden';
            hdnPF.id = 'hdnPP' + type + 'Front_' + fileIndex;
            hdnPF.name = 'hdnPP' + type + 'Front_' + fileIndex;

            var imgPF = document.createElement('img');
            imgPF.width = 100;
            imgPF.height = 100;
            imgPF.id = 'PP' + type + 'Front_' + fileIndex;
            imgPF.name = 'PP' + type + 'Front_' + fileIndex;
            imgPF.style.display = 'none';

            var spanPF = document.createElement('span');
            spanPF.style.display = "none";
            spanPF.style.color = "red";
            spanPF.id = "spn" + type + "PassportFront_" + id;

            divFilePF.appendChild(hdnPF);
            divFilePF.appendChild(imgPF);
            divFilePF.appendChild(spanPF);
            divPadPF.appendChild(divFilePF);

            //Passport Back page
            var divlblPB = document.createElement('div');
            divlblPB.className = 'col-md-12';
            divlblPB.innerHTML = "Passport Copy Back Page";

            divPadPF.appendChild(divlblPB);

            var divFilePB = document.createElement('div');
            divFilePB.className = 'col-md-12';

            var filePB = document.createElement('input');
            filePB.type = 'file';
            filePB.setAttribute('onchange', 'openFile(event,' + fileIndex + ',"Back","' + type + '","spn' + type + 'PassportBack_' + fileIndex + '","PP' + type + 'Back_' + fileIndex + '");');
            filePB.setAttribute('accept', 'image/*');
            filePB.id = 'file' + type + 'PassportBack_' + fileIndex;
            filePB.name = 'file' + type + 'PassportBack_' + fileIndex;

            divFilePB.appendChild(filePB);

            var hdnPB = document.createElement('input');
            hdnPB.type = 'hidden';
            hdnPB.id = 'hdnPP' + type + 'Back_' + fileIndex;
            hdnPB.name = 'hdnPP' + type + 'Back_' + fileIndex;

            var imgPB = document.createElement('img');
            imgPB.width = 100;
            imgPB.height = 100;
            imgPB.id = 'PP' + type + 'Back_' + fileIndex;
            imgPB.name = 'PP' + type + 'Back_' + fileIndex;
            imgPB.style.display = 'none';

            var spanPB = document.createElement('span');
            spanPB.style.display = "none";
            spanPB.style.color = "red";
            spanPB.id = "spn" + type + "PassportBack_" + id;

            divFilePB.appendChild(hdnPB);
            divFilePB.appendChild(imgPB);
            divFilePB.appendChild(spanPB);
            divPadPF.appendChild(divFilePB);

            //Passport Clear Photo
            var divlblPP = document.createElement('div');
            divlblPP.className = 'col-md-12';
            divlblPP.innerHTML = "Passport Size Clear Photo";

            divPadPF.appendChild(divlblPP);

            var divFilePP = document.createElement('div');
            divFilePP.className = 'col-md-12';

            var filePP = document.createElement('input');
            filePP.type = 'file';//'spn' + type + 'BirthCertificate_' + fileIndex
            filePP.setAttribute('onchange', 'openFile(event,' + fileIndex + ',"Photo","' + type + '","spn' + type + 'PassportPhoto_' + fileIndex + '","PP' + type + 'Photo_' + fileIndex + '");');
            filePP.setAttribute('accept', 'image/*');
            filePP.id = 'file' + type + 'PassportPhoto_' + fileIndex;
            filePP.name = 'file' + type + 'PassportPhoto_' + fileIndex;

            divFilePP.appendChild(filePP);

            var hdnPP = document.createElement('input');
            hdnPP.type = 'hidden';
            hdnPP.id = 'hdnPP' + type + 'Photo_' + fileIndex;
            hdnPP.name = 'hdnPP' + type + 'Photo_' + fileIndex;

            var imgPP = document.createElement('img');
            imgPP.width = 100;
            imgPP.height = 100;
            imgPP.id = 'PP' + type + 'Photo_' + fileIndex;
            imgPP.name = 'PP' + type + 'Photo_' + fileIndex;
            imgPP.style.display = 'none';

            var spanPP = document.createElement('span');
            spanPP.style.display = "none";
            spanPP.style.color = "red";
            spanPP.id = 'spn' + type + 'PassportPhoto_' + fileIndex;

            divFilePP.appendChild(hdnPP);
            divFilePP.appendChild(imgPP);
            divFilePP.appendChild(spanPP);
            divPadPF.appendChild(divFilePP);

            //Birth Certificate
            var divlblPC = document.createElement('div');
            divlblPC.className = 'col-md-12';
            divlblPC.innerHTML = "Birth Certificate";

            divPadPF.appendChild(divlblPC);

            var divFilePC = document.createElement('div');
            divFilePC.className = 'col-md-12';

            var filePC = document.createElement('input');
            filePC.type = 'file';
            filePC.setAttribute('onchange', 'openFile(event,' + fileIndex + ',"Birth","' + type + '","spn' + type + 'BirthCertificate_' + fileIndex + '","PP' + type + 'Birth_' + fileIndex + '");');
            filePC.setAttribute('accept', 'image/*');
            filePC.id = 'file' + type + 'BirthCertificate_' + fileIndex;
            filePC.name = 'file' + type + 'BirthCertificate_' + fileIndex;

            divFilePC.appendChild(filePC);

            var hdnPC = document.createElement('input');
            hdnPC.type = 'hidden';
            hdnPC.id = 'hdnPP' + type + 'Birth_' + fileIndex;
            hdnPC.name = 'hdnPP' + type + 'Birth_' + fileIndex;

            var imgPC = document.createElement('img');
            imgPC.width = 100;
            imgPC.height = 100;
            imgPC.id = 'PP' + type + 'Birth_' + fileIndex;
            imgPC.name = 'PP' + type + 'Birth_' + fileIndex;
            imgPC.style.display = 'none';

            var spanPB = document.createElement('span');
            spanPB.style.display = "none";
            spanPB.style.color = "red";
            spanPB.id = 'spn' + type + 'BirthCertificate_' + fileIndex;

            divFilePC.appendChild(hdnPC);
            divFilePC.appendChild(imgPC);
            divPadPF.appendChild(divFilePC);

            divColPF.appendChild(divPadPF)
            divPF.appendChild(divColPF);
            div.appendChild(divPF);

            var divBtn = document.createElement('div');
            divBtn.style.textAlign = 'right';
            divBtn.setAttribute('id', 'divBtn_' + type + '_' + id);
            //divBtn.className = 'row';


            var paxTypeName = null;
            if (type == 'Adt')
            {
                paxTypeName = 'Adult';
            }
            else if (type == 'Chd')
            {
                paxTypeName = 'Child';
            }
            else if (type == 'Inf')
            {
                paxTypeName = 'Infant';
            }

            // create remove button for each email adress
            var newButton = document.createElement('input');
            newButton.type = 'button';
            newButton.value = 'Remove ' + paxTypeName + ' ' + id;
            newButton.id = 'btn' + type + id;


            // atach event for remove button click
            newButton.onclick = function RemoveEntry() {
                var adtCounter = document.getElementById('ctl00_cphTransaction_hdnAdultCounter').value;
                var chdCounter = document.getElementById('ctl00_cphTransaction_hdnChildCounter').value;
                var infCounter = document.getElementById('ctl00_cphTransaction_hdnInfantCounter').value;


                var parentDiv = document.getElementById('divApplicantDet');
                var divId = upperDiv.id.split('_')[2];
                var divPaxType = upperDiv.id.split('_')[1];
                //parentDiv.removeChild(div);
                parentDiv.removeChild(upperDiv);
                //parentDiv.removeChild(divBtn);

                var adtDetails = document.getElementById('ctl00_cphTransaction_hdnAdtApplicantDetails').value;
                var chdDetails = document.getElementById('ctl00_cphTransaction_hdnChdApplicantDetails').value;
                var infDetails = document.getElementById('ctl00_cphTransaction_hdnInfApplicantDetails').value;

                if (type == 'Adt') {
                    adtCounter -= 1;
                    document.getElementById('ctl00_cphTransaction_hdnAdultCounter').value = adtCounter;
                    if (adtDetails == "") {
                        adtDetails = divId;
                    }
                    else {
                        adtDetails += "," + divId;
                    }
                    $('#ddlAdult').val(adtCounter).trigger('change');
                    document.getElementById('ddlAdult').focus();
                    reAssigningId(divId, divPaxType);
                }
                else if (type == 'Chd') {
                    chdCounter -= 1;
                    document.getElementById('ctl00_cphTransaction_hdnChildCounter').value = chdCounter;
                    if (chdDetails == "") {
                        chdDetails = id;
                    }
                    else {
                        chdDetails += "," + id;
                    }

                    $('#ddlChild').val(chdCounter).trigger('change');
                    document.getElementById('ddlChild').focus();
                    reAssigningId(divId, divPaxType);
                }
                else if (type == 'Inf') {
                    infCounter -= 1;
                    document.getElementById('ctl00_cphTransaction_hdnInfantCounter').value = infCounter;
                    if (infDetails == "") {
                        infDetails = id;
                    }
                    else {
                        infDetails += "," + id;
                    }


                    $('#ddlInfant').val(infCounter).trigger('change');
                    document.getElementById('ddlInfant').focus();
                    reAssigningId(divId, divPaxType);
                }

                document.getElementById('ctl00_cphTransaction_hdnAdtApplicantDetails').value = adtDetails;
                document.getElementById('ctl00_cphTransaction_hdnChdApplicantDetails').value = chdDetails;
                document.getElementById('ctl00_cphTransaction_hdnInfApplicantDetails').value = infDetails;


                var adults = eval(document.getElementById('ddlAdult').value);
                var childs = eval(document.getElementById('ddlChild').value);
                var infants = eval(document.getElementById('ddlInfant').value);
                var totalPax = adults + childs + infants;

                //if (totalPax > eval(parseInt(adtCounter) + parseInt(chdCounter) + parseInt(infCounter))) {
                //    document.getElementById('addMore1').style.display = 'block';
                //}
            }

            divBtn.appendChild(newButton);
            upperDiv.appendChild(div);
            upperDiv.appendChild(divBtn);
            mainDiv.appendChild(upperDiv);
            //mainDiv.appendChild();
        }

        function removeErrorMessage(id) {
            document.getElementById(id).value = '';
            document.getElementById(id).style.display = 'none';
        }

        function reAssigningId(id, paxType)
        {
            var mainDiv = document.getElementById('divApplicantDet');
            var childDiv = mainDiv.childNodes;
            var childDivIdList = "";
            for (var i = 1; i < childDiv.length; i++)
            {
                var divIdName = childDiv[i].id.split('_');
                if (divIdName != "" && divIdName[1] == paxType && divIdName[2] > id)
                {
                    var paxMainDiv = childDiv[i];
                    paxMainDiv.setAttribute("id","divMain_"+paxType+"_"+id)
                    var divLabel = paxMainDiv.childNodes[0];
                    var divControl = paxMainDiv.childNodes[1];
                    var divBtn = paxMainDiv.childNodes[2];

                    var paxTypeName = null;
                    if (paxType == 'Adt') {
                        paxTypeName = 'Adult';
                    }
                    else if (paxType == 'Chd') {
                        paxTypeName = 'Child';
                    }
                    else if (paxType == 'Inf') {
                        paxTypeName = 'Infant';
                    }

                    if (divLabel.childNodes.length > 0) {
                        divLabel.setAttribute('id', 'divLabel_' +paxType +"_"+ id);
                        divLabel.innerHTML = "<strong>" + paxTypeName + " Applicant " + id + ":</strong>";
                    }
                    if (divControl.childNodes.length > 0)
                    {
                        divControl.setAttribute("id", "divControl_" + paxType + "_" + id);
                        var divFullName = divControl.childNodes[0];
                        var divDob = divControl.childNodes[1];
                        var divPassportNo = divControl.childNodes[2];
                        var divPassportExpiry = divControl.childNodes[3];
                        var divNationality = divControl.childNodes[4];
                        var divDocuments = divControl.childNodes[5];

                        //FullName
                        if (divFullName.childNodes.length > 0)
                        {
                            var txtFullName = divFullName.childNodes;
                            //TextBox
                            var fullName = txtFullName[0];
                            var fullNameId = fullName.childNodes[1];
                            fullNameId.setAttribute('id', 'txt' + paxType + 'FullName_' + id);
                            fullNameId.setAttribute('name', 'txt' + paxType + 'FullName_' + id);

                            //span
                            var spnFullName = txtFullName[1];
                            spnFullName.setAttribute('id', 'spn' + paxType + 'FullName_' + id);
                        }

                        //DateOfBirth
                        if (divDob.childNodes.length > 0)
                        {
                            var dobChilds = divDob.childNodes;
                            var dobDet = dobChilds[0].childNodes[1];
                            var dobDropDowns = dobDet.childNodes;

                            var divDay = dobDropDowns[0];
                            var divMonth = dobDropDowns[1];
                            var divYear = dobDropDowns[2];

                            //Day
                            divDay.childNodes[0].setAttribute('id', 'ddl' + paxType + 'dobDay_' + id);
                            divDay.childNodes[0].setAttribute('name', 'ddl' + paxType + 'dobDay_' + id);
                            //Month
                            divMonth.childNodes[0].setAttribute('id', 'ddl' + paxType + 'dobMonth_' + id);
                            divMonth.childNodes[0].setAttribute('name', 'ddl' + paxType + 'dobMonth_' + id);
                            //Year
                            divYear.childNodes[0].setAttribute('id', 'ddl' + paxType + 'dobYear_' + id);
                            divYear.childNodes[0].setAttribute('name', 'ddl' + paxType + 'dobYear_' + id);


                            //span
                            var spnDob = dobChilds[1];
                            spnDob.setAttribute('id', 'spn' + paxType + 'DOB_' + id);
                        }

                        //PassportNo
                        if (divPassportNo.childNodes.length > 0) {
                            var txtPassportNo = divPassportNo.childNodes;
                            //TextBox
                            var passportNo = txtPassportNo[0];
                            var passportNoId = passportNo.childNodes[1];
                            passportNoId.setAttribute('id', 'txt' + paxType + 'Passport_' + id);
                            passportNoId.setAttribute('name', 'txt' + paxType + 'Passport_' + id);

                            //span
                            var spnPassportNo = txtPassportNo[1];
                            spnPassportNo.setAttribute('id', 'spn' + paxType + 'Passport_' + id);
                        }

                        //Passport Expiry
                        if (divPassportExpiry.childNodes.length > 0) {
                            var passportExpChilds = divPassportExpiry.childNodes;
                            var passportExpDet = passportExpChilds[0].childNodes[1];
                            var passportExpDropDowns = passportExpDet.childNodes;

                            var divDay = passportExpDropDowns[0];
                            var divMonth = passportExpDropDowns[1];
                            var divYear = passportExpDropDowns[2];

                            //Day
                            divDay.childNodes[0].setAttribute('id', 'ddl' + paxType + 'PassExpiryDay_' + id);
                            divDay.childNodes[0].setAttribute('name', 'ddl' + paxType + 'PassExpiryDay_' + id);
                            //Month
                            divMonth.childNodes[0].setAttribute('id', 'ddl' + paxType + 'PassExpiryMonth_' + id);
                            divMonth.childNodes[0].setAttribute('name', 'ddl' + paxType + 'PassExpiryMonth_' + id);
                            //Year
                            divYear.childNodes[0].setAttribute('id', 'ddl' + paxType + 'PassExpiryYear_' + id);
                            divYear.childNodes[0].setAttribute('name', 'ddl' + paxType + 'PassExpiryYear_' + id);


                            //span
                            var spnPassportExp = passportExpChilds[1];
                            spnPassportExp.setAttribute('id', 'spn' + paxType + 'PassExpiry_' + id);
                        }

                        //Nationality
                        if (divNationality.childNodes.length > 0) {
                            var nationalityChilds = divNationality.childNodes;
                            var nationalityDet = nationalityChilds[0].childNodes[1];
                            //var nationalityDropDown = nationalityDet.childNodes;

                            //var nationality = nationalityDropDown[0];

                            //nationality
                            nationalityDet.setAttribute('id', 'ddl' + paxType + 'Nationality_' + id);
                            nationalityDet.setAttribute('name', 'ddl' + paxType + 'Nationality_' + id);
                            
                            //span
                            var spnNationality = nationalityChilds[1];
                            spnNationality.setAttribute('id', 'spn' + paxType + 'Nationality_' + id);
                        }

                        //Documents
                        if (divDocuments.childNodes.length > 0)
                        {
                            var docChilds = divDocuments.childNodes[0].childNodes[1];
                            if (docChilds.childNodes.length > 0)
                            {
                                var fileId ="";
                                var hdnFile ="";
                                var imageFile ="";
                                var spnFile = "";

                                for (var j = 0; j < docChilds.childNodes.length;j++)
                                {
                                    var documents = docChilds.childNodes[j];
                                    if (j == 1) {
                                        fileId = documents.childNodes[0];
                                        hdnFile = documents.childNodes[1];
                                        imageFile = documents.childNodes[2];
                                        spnFile = documents.childNodes[3];

                                        fileId.setAttribute('id', 'file' + paxType + 'PassportFront_'+id);
                                        fileId.setAttribute('name', 'file' + paxType + 'PassportFront_' + id);
                                        fileId.setAttribute('onchange', 'openFile(event,' + id + ',"Front","' + paxType + '","spn' + paxType + 'PassportFront_' + id + '","PP' + paxType + 'Front_' + id + '")');

                                        hdnFile.setAttribute('id', 'hdnPP' + paxType + 'Front_' + id);
                                        hdnFile.setAttribute('name', 'hdnPP' + paxType + 'Front_' + id);

                                        imageFile.setAttribute('id', 'PP' + paxType + 'Front_' + id);
                                        imageFile.setAttribute('name', 'PP' + paxType + 'Front_' + id);

                                        spnFile.setAttribute('id', 'spn' + paxType + 'PassportFront_' + id);
                                        spnFile.setAttribute('name', 'spn' + paxType + 'PassportFront_' + id);
                                    }
                                    if (j == 3) {
                                        fileId = documents.childNodes[0];
                                        hdnFile = documents.childNodes[1];
                                        imageFile = documents.childNodes[2];
                                        spnFile = documents.childNodes[3];

                                        fileId.setAttribute('id', 'file' + paxType + 'PassportBack_' + id);
                                        fileId.setAttribute('name', 'file' + paxType + 'PassportBack_' + id);
                                        fileId.setAttribute('onchange', 'openFile(event,' + id + ',"Back","' + paxType + '","spn' + paxType + 'PassportBack_' + id + '","PP' + paxType + 'Back_' + id + '")');

                                        hdnFile.setAttribute('id', 'hdnPP' + paxType + 'Back_' + id);
                                        hdnFile.setAttribute('name', 'hdnPP' + paxType + 'Back_' + id);

                                        imageFile.setAttribute('id', 'PP' + paxType + 'Back_' + id);
                                        imageFile.setAttribute('name', 'PP' + paxType + 'Back_' + id);

                                        spnFile.setAttribute('id', 'spn' + paxType + 'PassportBack_' + id);
                                        spnFile.setAttribute('name', 'spn' + paxType + 'PassportBack_' + id);
                                    }
                                    if (j == 5) {
                                        fileId = documents.childNodes[0];
                                        hdnFile = documents.childNodes[1];
                                        imageFile = documents.childNodes[2];
                                        spnFile = documents.childNodes[3];

                                        fileId.setAttribute('id', 'file' + paxType + 'PassportPhoto_' + id);
                                        fileId.setAttribute('name', 'file' + paxType + 'PassportPhoto_' + id);
                                        fileId.setAttribute('onchange', 'openFile(event,' + id + ',"Photo","' + paxType + '","spn' + paxType + 'PassportPhoto_' + id + '","PP' + paxType + 'Photo_' + id + '")');

                                        hdnFile.setAttribute('id', 'hdnPP' + paxType + 'Photo_' + id);
                                        hdnFile.setAttribute('name', 'hdnPP' + paxType + 'Photo_' + id);

                                        imageFile.setAttribute('id', 'PP' + paxType + 'Photo_' + id);
                                        imageFile.setAttribute('name', 'PP' + paxType + 'Photo_' + id);

                                        spnFile.setAttribute('id', 'spn' + paxType + 'PassportPhoto_' + id);
                                        spnFile.setAttribute('name', 'spn' + paxType + 'PassportPhoto_' + id);
                                    }
                                    if (j == 7) {//As Birth Certificate is not mandatory the span is not present
                                        fileId = documents.childNodes[0];
                                        hdnFile = documents.childNodes[1];
                                        imageFile = documents.childNodes[2];
                                        
                                        fileId.setAttribute('id', 'file' + paxType + 'BirthCertificate_' + id);
                                        fileId.setAttribute('name', 'file' + paxType + 'BirthCertificate_' + id);
                                        fileId.setAttribute('onchange', 'openFile(event,' + id + ',"Birth","' + paxType + '","spn' + paxType + 'BirthCertificate_' + id + '","PP' + paxType + 'Birth_' + id + '")');

                                        hdnFile.setAttribute('id', 'hdnPP' + paxType + 'Birth_' + id);
                                        hdnFile.setAttribute('name', 'hdnPP' + paxType + 'Birth_' + id);

                                        imageFile.setAttribute('id', 'PP' + paxType + 'Birth_' + id);
                                        imageFile.setAttribute('name', 'PP' + paxType + 'Birth_' + id);

                                        
                                    }
                                }
                            }
                        }
                    }
                    if (divBtn.childNodes.length > 0) {
                        divBtn.setAttribute('id', 'divBtn_' + paxType + '_' + id);
                        var btn = divBtn.childNodes[0];
                        btn.setAttribute('id', 'btn' + paxType + id);
                        
                        btn.setAttribute('value', 'Remove ' + paxTypeName + ' ' + id);
                    }


                    //id value should be incremented only after assigning.
                    id++;
                }
            }
        }


        function validate() {
            document.getElementById('ctl00_cphTransaction_lblErrorMessage').value = "";
            var visaType = document.getElementById('<%=ddlVisaType.ClientID%>').value;
                //cal3.hide();
                //cal4.hide();
                if (visaType != "-1") {
                    var adtCounter = document.getElementById('ctl00_cphTransaction_hdnAdultCounter').value;
                    var chdCounter = document.getElementById('ctl00_cphTransaction_hdnChildCounter').value;
                    var infCounter = document.getElementById('ctl00_cphTransaction_hdnInfantCounter').value;
                    var totalPaxCounter = (parseInt(adtCounter) + parseInt(chdCounter) + parseInt(infCounter));
                    var bool = true;

                    for (var i = 0; i < adtCounter; i++) {
                        bool = validateControls('Adt', eval(i + 1));
                    }

                    for (var i = 0; i < chdCounter; i++) {
                        bool = validateControls('Chd', eval(i + 1));
                    }

                    for (var i = 0; i < infCounter; i++) {
                        bool = validateControls('Inf', eval(i + 1));
                    }




                    return bool;
                } else {
                    alert('Please Select VisaType');
                    document.getElementById('ddlAdult').disabled = true;
                    document.getElementById('ddlChild').disabled = true;
                    document.getElementById('ddlInfant').disabled = true;
                    document.getElementById('ctl00_cphTransaction_btnSave').style.display = "block";
                    document.getElementById('lblPrice').value = '<%=currency %>' + ' ' + 0;
                    //document.getElementById('addMore1').style.display = "none";
                    return false;
                }
            }



            function clear() {
                document.getElementById('divApplicantDet').innerHTML = "";
                document.getElementById('ctl00_cphTransaction_hdnAdultCounter').value = "1";
                document.getElementById('ctl00_cphTransaction_hdnChildCounter').value = "0";
                document.getElementById('ctl00_cphTransaction_hdnInfantCounter').value = "0";
                document.getElementById('ctl00_cphTransaction_hdnAdultCount').value = "1";
                document.getElementById('ctl00_cphTransaction_hdnChildCount').value = "0";
                document.getElementById('ctl00_cphTransaction_hdnInfantCount').value = "0";
                document.getElementById('ctl00_cphTransaction_hdnAdtApplicantDetails').value = "";
                document.getElementById('ctl00_cphTransaction_hdnChdApplicantDetails').value = "";
                document.getElementById('ctl00_cphTransaction_hdnInfApplicantDetails').value = "";
                document.getElementById('ctl00_cphTransaction_lblErrorMessage').style.display = "none";
                document.getElementById('ctl00_cphTransaction_lblErrorMessage').value = "";
                document.getElementById('txtAdtFullName_1').value = "";
                // document.getElementById('txtAdtDOB_1').value = "";
                document.getElementById('txtAdtPassport_1').value = "";
                document.getElementById('txtAdtPassExpiry_1').value = "";

                document.getElementById('spnAdtFullName_1').style.display = "none";
                document.getElementById('spnAdtFullName_1').innerHTML = "";
                document.getElementById('spnAdtDOB_1').style.display = "none";
                document.getElementById('spnAdtDOB_1').innerHTML = "";
                document.getElementById('spnAdtPassport_1').style.display = "none";
                document.getElementById('spnAdtPassport_1').innerHTML = "";
                document.getElementById('spnAdtPassExpiry_1').style.display = "none";
                document.getElementById('spnAdtPassExpiry_1').innerHTML = "";
                document.getElementById('spnAdtNationality_1').style.display = "none";
                document.getElementById('spnAdtNationality_1').innerHTML = "";
                document.getElementById('spnAdtDOB_1').innerHTML = "";
                document.getElementById('spnAdtPassExpiry_1').innerHTML = "";
            }

            function removeApplicant(id, paxType) {
                var mainDiv = document.getElementById('divApplicantDet');
                var removingNode = document.getElementById('divMain_' + paxType + '_' + id);
                
                mainDiv.removeChild(removingNode);
                //var txt = document.getElementById('txt' + paxType + 'FullName_' + id);
                //var btn = document.getElementById('btn' + paxType + id);
                //var lbl = document.getElementById('lbl' + paxType + id);

                //if (lbl != null) mainDiv.removeChild(lbl.parentNode.parentNode);
                //if (txt != null) mainDiv.removeChild(txt.parentNode.parentNode.parentNode);
                //if (btn != null) mainDiv.removeChild(btn.parentNode);
            }

            //function applicantDetails(id, paxType) {
            //    var adtApplicantDetails = "";
            //    var chdApplicantDetails = "";
            //    var infApplicantDetails = "";
            //    var adtCounter = document.getElementById('ctl00_cphTransaction_hdnAdultCounter').value;
            //    var chdCounter = document.getElementById('ctl00_cphTransaction_hdnChildCounter').value;
            //    var infCounter = document.getElementById('ctl00_cphTransaction_hdnInfantCounter').value;

            //    var totalPaxCounter = (parseInt(adtCounter) + parseInt(chdCounter) + parseInt(infCounter));

            //    var adtCount = document.getElementById('ctl00_cphTransaction_hdnAdultCount').value;
            //    var chdCount = document.getElementById('ctl00_cphTransaction_hdnChildCount').value;
            //    var infCount = document.getElementById('ctl00_cphTransaction_hdnInfantCount').value;

            //    var totalPaxCount = (parseInt(adtCount) + parseInt(chdCount) + parseInt(infCount));
            //    if (totalPaxCounter <= totalPaxCount) {
            //        var appName = "";
            //        var dob = "";
            //        var passportNo = "";
            //        var passportExpiry = "";
            //        var nationality = "";
            //        var passportFront = "";
            //        var passportBack = "";
            //        var passportPhoto = "";
            //        var birthCertificate = "";

            //        var j = 1;//for child applicant number
            //        var k = 1;//for infant applicant number

            //        for (var counter = 1; counter <= totalPaxCounter; counter++) {

            //            if (id.toString() == "" || id.toString == null || parseInt(id) < 1) {
            //                id = 0;
            //            }
            //            //, ==> using for seperation of two Id's.
            //            //_ ==> using for seperation of two applicants.
            //            if (parseInt(id) != counter) {
            //                if (counter <= totalPaxCounter && counter <= parseInt(adtCounter)) {
            //                    appName = document.getElementById('txtAdtFullName_' + counter).value;
            //                    //dob = document.getElementById('txtAdtDOB_' + counter).value;
            //                    dob = new Date(eval(document.getElementById('ddlAdtdobYear_' + counter).value), eval(document.getElementById('ddlAdtdobMonth_' + counter).value) - 1, eval(document.getElementById('ddlAdtdobDay_' + counter).value));
            //                    passportNo = document.getElementById('txtAdtPassport_' + counter).value;
            //                    passportExpiry = document.getElementById('txtAdtPassExpiry_' + counter).value;
            //                    nationality = document.getElementById('ddlAdtNationality_' + counter).value;

            //                    passportFront = document.getElementById('hdnPP' + paxType + 'Front' + id).value;
            //                    passportBack = document.getElementById('hdnPP' + paxType + 'Back' + id).value;
            //                    passportPhoto = document.getElementById('hdnPP' + paxType + 'Photo' + id).value;
            //                    birthCertificate = document.getElementById('hdnPP' + paxType + 'Birth' + id).value;

            //                    adtApplicantDetails += appName + ",";
            //                    adtApplicantDetails += dob + ",";
            //                    adtApplicantDetails += passportNo + ",";
            //                    adtApplicantDetails += passportExpiry + ",";
            //                    adtApplicantDetails += nationality + ",";
            //                    adtApplicantDetails += passportFront + ",";
            //                    adtApplicantDetails += passportBack + ",";
            //                    adtApplicantDetails += passportPhoto + ",";
            //                    adtApplicantDetails += birthCertificate + "_";
            //                }
            //                else if (counter > parseInt(adtCounter) && counter <= totalPaxCounter && j <= parseInt(chdCounter)) {
            //                    appName = document.getElementById('txtChdFullName_' + counter).value;
            //                    //dob = document.getElementById('txtChdDOB_' + counter).value;
            //                    dob = new Date(eval(document.getElementById('ddlAdtdobYear_' + counter).value), eval(document.getElementById('ddlAdtdobMonth_' + counter).value) - 1, eval(document.getElementById('ddlAdtdobDay_' + counter).value));
            //                    passportNo = document.getElementById('txtChdPassport_' + counter).value;
            //                    passportExpiry = document.getElementById('txtChdPassExpiry_' + counter).value;
            //                    nationality = document.getElementById('ddlChdNationality_' + counter).value;

            //                    passportFront = document.getElementById('hdnPP' + paxType + 'Front' + id).value;
            //                    passportBack = document.getElementById('hdnPP' + paxType + 'Back' + id).value;
            //                    passportPhoto = document.getElementById('hdnPP' + paxType + 'Photo' + id).value;
            //                    birthCertificate = document.getElementById('hdnPP' + paxType + 'Birth' + id).value;

            //                    chdApplicantDetails += appName + ",";
            //                    chdApplicantDetails += dob + ",";
            //                    chdApplicantDetails += passportNo + ",";
            //                    chdApplicantDetails += passportExpiry + ",";
            //                    chdApplicantDetails += nationality + ",";
            //                    chdApplicantDetails += passportFront + ",";
            //                    chdApplicantDetails += passportBack + ",";
            //                    chdApplicantDetails += passportPhoto + ",";
            //                    chdApplicantDetails += birthCertificate + "_";
            //                    j++;
            //                }
            //                else if (counter > (parseInt(adtCounter) + parseInt(chdCounter)) && counter <= totalPaxCounter && k <= parseInt(infCounter)) {
            //                    appName = document.getElementById('txtInfFullName_' + counter).value;
            //                    //dob = document.getElementById('txtInfDOB_' + counter).value;
            //                    dob = new Date(eval(document.getElementById('ddlAdtdobYear_' + counter).value), eval(document.getElementById('ddlAdtdobMonth_' + counter).value) - 1, eval(document.getElementById('ddlAdtdobDay_' + counter).value));
            //                    passportNo = document.getElementById('txtInfPassport_' + counter).value;
            //                    passportExpiry = document.getElementById('txtInfPassExpiry_' + counter).value;
            //                    nationality = document.getElementById('ddlInfNationality_' + counter).value;

            //                    passportFront = document.getElementById('hdnPP' + paxType + 'Front' + id).value;
            //                    passportBack = document.getElementById('hdnPP' + paxType + 'Back' + id).value;
            //                    passportPhoto = document.getElementById('hdnPP' + paxType + 'Photo' + id).value;
            //                    birthCertificate = document.getElementById('hdnPP' + paxType + 'Birth' + id).value;

            //                    infApplicantDetails += appName + ",";
            //                    infApplicantDetails += dob + ",";
            //                    infApplicantDetails += passportNo + ",";
            //                    infApplicantDetails += passportExpiry + ",";
            //                    infApplicantDetails += nationality + ",";
            //                    infApplicantDetails += passportFront + ",";
            //                    infApplicantDetails += passportBack + ",";
            //                    infApplicantDetails += passportPhoto + ",";
            //                    infApplicantDetails += birthCertificate + "_";
            //                    k++;
            //                }
            //            }
            //        }

            //        document.getElementById('ctl00_cphTransaction_hdnAdtApplicantDetails').value = adtApplicantDetails;
            //        document.getElementById('ctl00_cphTransaction_hdnChdApplicantDetails').value = chdApplicantDetails;
            //        document.getElementById('ctl00_cphTransaction_hdnInfApplicantDetails').value = infApplicantDetails;

            //        if (paxType == "Adult") {
            //            document.getElementById('ctl00_cphTransaction_hdnAdultCounter').value = (adtCounter - 1);
            //        } else if (paxType == "Child") {
            //            document.getElementById('ctl00_cphTransaction_hdnChildCounter').value = (chdCounter - 1);
            //        }
            //        else if (paxType == "Infant") {
            //            document.getElementById('ctl00_cphTransaction_hdnInfantCounter').value = (infCounter - 1);
            //        }

            //    }

            //}

            function validateSave() {

                document.getElementById('ctl00_cphTransaction_lblErrorMessage').style.display = "none";
                document.getElementById('ctl00_cphTransaction_lblErrorMessage').value = "";

                var isValid = true;
                var visaType = document.getElementById('<%=ddlVisaType.ClientID%>').value;

            if (visaType != "-1") {

                var adtCounter = document.getElementById('ctl00_cphTransaction_hdnAdultCounter').value;
                var chdCounter = document.getElementById('ctl00_cphTransaction_hdnChildCounter').value;
                var infCounter = document.getElementById('ctl00_cphTransaction_hdnInfantCounter').value;

                var adtCount = document.getElementById('ctl00_cphTransaction_hdnAdultCount').value;
                var chdCount = document.getElementById('ctl00_cphTransaction_hdnChildCount').value;
                var infCount = document.getElementById('ctl00_cphTransaction_hdnInfantCount').value;
                var totalPaxCount = (parseInt(adtCount) + parseInt(chdCount) + parseInt(infCount));
                var totalPaxCounter = (parseInt(adtCounter) + parseInt(chdCounter) + parseInt(infCounter));

                for (var i = 0; i < adtCounter; i++) {
                    isValid = validateControls('Adt', eval(i + 1));
                }

                for (var i = 0; i < chdCounter; i++) {
                    isValid = validateControls('Chd', eval(i + 1));
                }

                for (var i = 0; i < infCounter; i++) {
                    isValid = validateControls('Inf', eval(i + 1));
                }

                if (isValid) {
                    if (document.getElementById('lblPrice') != null && document.getElementById('lblPrice').value.length > 0) {
                        document.getElementById('<%=hdnVisaPrice.ClientID%>').value = document.getElementById('lblPrice').value;
                    }
                    <%--for (var i = 1; i <= adtCount; i++) {
                        if (document.getElementById('ddlAdtNationality_' + i) != null && document.getElementById('ddlAdtNationality_' + i).value.length > 0) {

                           //Eg : AE~IN~PK
                            var selectedNationality =  document.getElementById('ddlAdtNationality_'+ i).value;
                            if (document.getElementById('<%=hdnNationality.ClientID%>').value.length > 0)
                            {
                                document.getElementById('<%=hdnNationality.ClientID%>').value = document.getElementById('<%=hdnNationality.ClientID%>').value + '~' + selectedNationality;
                            }
                            else {
                                document.getElementById('<%=hdnNationality.ClientID%>').value = selectedNationality;
                            }  
                    }

                    }
                    for (var i = 1; i <= chdCount; i++) {
                        if (document.getElementById('ddlChdNationality_' + i) != null && document.getElementById('ddlChdNationality_' + i).value.length > 0) {
                            var selectedchaildNationality = document.getElementById('ddlChdNationality_' + i).value;
                        if (document.getElementById('<%=hdnChdNationality.ClientID%>').value .length > 0) {
                            document.getElementById('<%=hdnChdNationality.ClientID%>').value = document.getElementById('<%=hdnChdNationality.ClientID%>').value + '~' + selectedchaildNationality;
                        }
                            else
                        {
                            document.getElementById('<%=hdnChdNationality.ClientID%>').value = selectedchaildNationality;
                        }
                    }

                    }
                    for (var i = 1; i <=infCount; i++) {
                        if (document.getElementById('ddlInfNationality_' + i) != null && document.getElementById('ddlInfNationality_' + i).value.length > 0) { 
                            var selectedinfNationality = document.getElementById('ddlInfNationality_' + i).value;
                            if (document.getElementById('<%=hdnInfNationality.ClientID%>').value.length > 0)
                            {
                                document.getElementById('<%=hdnInfNationality.ClientID%>').value = document.getElementById('<%=hdnInfNationality.ClientID%>').value + '~' + selectedinfNationality;
                            }
                            else
                                {
                                document.getElementById('<%=hdnInfNationality.ClientID%>').value = selectedinfNationality;   
                            }
                    }

                    }--%>


                }


                return isValid;
            }
            else {
                alert('Please Select VisaType');
                document.getElementById('ddlAdult').disabled = true;
                document.getElementById('ddlChild').disabled = true;
                document.getElementById('ddlInfant').disabled = true;
                document.getElementById('ctl00_cphTransaction_btnSave').style.display = "block";
                document.getElementById('lblPrice').value = '<%=currency %>' + ' ' + 0;
                //document.getElementById('addMore1').style.display = "none";
                return false;
            }
        }

        function validateControls(type, i) {
            var bool = true;
            var paxType;
            var sysdate = new Date();
            if (type == 'Adt') {
                paxType = 'Adult';
            }
            else if (type == 'Chd') {
                paxType = 'Child';
            }
            else {
                paxType = 'Infant';
            }

            if (document.getElementById('txt' + type + 'FullName_' + i) != null && document.getElementById('txt' + type + 'FullName_' + i).value.trim().length == 0) {
                document.getElementById('spn' + type + 'FullName_' + i).style.display = "block";
                document.getElementById('spn' + type + 'FullName_' + i).innerHTML = "Please enter the " + paxType + " Applicant " + i + " FullName.";
                bool = false;
            }
            else {
                if (document.getElementById('spn' + type + 'FullName_' + i) != null) {
                    document.getElementById('spn' + type + 'FullName_' + i).style.display = "none";
                    document.getElementById('spn' + type + 'FullName_' + i).innerHTML = "";
                }
            }
            if ((document.getElementById('ddl' + type + 'dobYear_' + i) != null) && (document.getElementById('ddl' + type + 'dobMonth_' + i) != null) && (document.getElementById('ddl' + type + 'dobDay_' + i) != null)) {
                if (document.getElementById('ddl' + type + 'dobDay_' + i).value == "-1") {
                    if (document.getElementById('spn' + type + 'DOB_' + i) != null) {
                        document.getElementById('spn' + type + 'DOB_' + i).style.display = "block";
                        document.getElementById('spn' + type + 'DOB_' + i).innerHTML = "Please enter the " + paxType + " Applicant " + i + " DateOfBirth Date.";
                        bool = false;
                    }
                }
                else if (document.getElementById('ddl' + type + 'dobMonth_' + i).value == "-1") {
                    if (document.getElementById('spn' + type + 'DOB_' + i) != null) {
                        document.getElementById('spn' + type + 'DOB_' + i).style.display = "block";
                        document.getElementById('spn' + type + 'DOB_' + i).innerHTML = "Please enter the " + paxType + " Applicant " + i + " DateOfBirth Month.";
                        bool = false;
                    }
                }
                else if (document.getElementById('ddl' + type + 'dobYear_' + i).value == "-1") {
                    if (document.getElementById('spn' + type + 'DOB_' + i) != null) {
                        document.getElementById('spn' + type + 'DOB_' + i).style.display = "block";
                        document.getElementById('spn' + type + 'DOB_' + i).innerHTML = "Please enter the " + paxType + " Applicant " + i + " DateOfBirth Year.";
                        bool = false;
                    }
                }
                else {
                    var dobDate = new Date(eval(document.getElementById('ddl' + type + 'dobYear_' + i).value), eval(document.getElementById('ddl' + type + 'dobMonth_' + i).value) - 1, eval(document.getElementById('ddl' + type + 'dobDay_' + i).value));
                    var timeDiff = Math.abs(sysdate.getTime() - dobDate.getTime());
                    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
                    if (type == 'Adt') {
                        if (Math.ceil(diffDays / 365) <= 12) {
                            document.getElementById('spn' + type + 'DOB_' + i).style.display = "block";
                            document.getElementById('spn' + type + 'DOB_' + i).innerHTML = paxType + " Applicant " + i + " Age should greater than 12 years.";
                            bool = false;
                        }
                        else {
                            if (document.getElementById('spn' + type + 'DOB_' + i) != null) {
                                document.getElementById('spn' + type + 'DOB_' + i).style.display = "none";
                                document.getElementById('spn' + type + 'DOB_' + i).innerHTML = "";
                            }

                        }
                    }
                    if (type == 'Chd') {
                        if ((Math.ceil(diffDays / 365) <= 2) || (Math.ceil(diffDays / 365) > 12)) {
                            document.getElementById('spn' + type + 'DOB_' + i).style.display = "block";
                            document.getElementById('spn' + type + 'DOB_' + i).innerHTML = paxType + " Applicant " + i + " Age should less than or equal to 12 years";
                            bool = false;
                        }
                        else {
                            if (document.getElementById('spn' + type + 'DOB_' + i) != null) {
                                document.getElementById('spn' + type + 'DOB_' + i).style.display = "none";
                                document.getElementById('spn' + type + 'DOB_' + i).innerHTML = "";
                            }
                        }
                    }
                    if (type == 'Inf') {
                        if ((Math.ceil(diffDays / 365) <= 0) || (Math.ceil(diffDays / 365) > 2)) {
                            document.getElementById('spn' + type + 'DOB_' + i).style.display = "block";
                            document.getElementById('spn' + type + 'DOB_' + i).innerHTML = paxType + " Applicant " + i + " Age should less than or equal to 2 years.";
                            bool = false;
                        }
                        else {
                            if (document.getElementById('spn' + type + 'DOB_' + i) != null) {
                                document.getElementById('spn' + type + 'DOB_' + i).style.display = "none";
                                document.getElementById('spn' + type + 'DOB_' + i).innerHTML = "";
                            }
                        }
                    }
                }
            }

            if (document.getElementById('txt' + type + 'Passport_' + i) != null && document.getElementById('txt' + type + 'Passport_' + i).value.trim().length == 0) {
                document.getElementById('spn' + type + 'Passport_' + i).style.display = "block";
                document.getElementById('spn' + type + 'Passport_' + i).innerHTML = "Please enter the " + paxType + " Applicant " + i + " Passport No.";
                bool = false;
            }
            else {
                if (document.getElementById('spn' + type + 'Passport_' + i) != null) {
                    document.getElementById('spn' + type + 'Passport_' + i).style.display = "none";
                    document.getElementById('spn' + type + 'Passport_' + i).innerHTML = "";
                }
            }

            if ((document.getElementById('ddl' + type + 'PassExpiryYear_' + i) != null) && (document.getElementById('ddl' + type + 'PassExpiryMonth_' + i) != null) && (document.getElementById('ddl' + type + 'PassExpiryDay_' + i) != null)) {
                if (document.getElementById('ddl' + type + 'PassExpiryDay_' + i).value == "-1") {
                    if (document.getElementById('spn' + type + 'PassExpiry_' + i) != null) {
                        document.getElementById('spn' + type + 'PassExpiry_' + i).style.display = "block";
                        document.getElementById('spn' + type + 'PassExpiry_' + i).innerHTML = "Please enter the " + paxType + " Applicant " + i + " Passport Expire Date.";
                        bool = false;
                    }
                }
                else if (document.getElementById('ddl' + type + 'PassExpiryMonth_' + i).value == "-1") {
                    if (document.getElementById('spn' + type + 'PassExpiry_' + i) != null) {
                        document.getElementById('spn' + type + 'PassExpiry_' + i).style.display = "block";
                        document.getElementById('spn' + type + 'PassExpiry_' + i).innerHTML = "Please enter the " + paxType + " Applicant " + i + " Passport Expire Month.";
                        bool = false;
                    }
                }
                else if (document.getElementById('ddl' + type + 'PassExpiryYear_' + i).value == "-1") {
                    if (document.getElementById('spn' + type + 'PassExpiry_' + i) != null) {
                        document.getElementById('spn' + type + 'PassExpiry_' + i).style.display = "block";
                        document.getElementById('spn' + type + 'PassExpiry_' + i).innerHTML = "Please enter the " + paxType + " Applicant " + i + " Passport Expire Year.";
                        bool = false;
                    }
                }
                else {

                    var expDate = new Date(eval(document.getElementById('ddl' + type + 'PassExpiryYear_' + i).value), eval(document.getElementById('ddl' + type + 'PassExpiryMonth_' + i).value) - 1, eval(document.getElementById('ddl' + type + 'PassExpiryDay_' + i).value));
                    if (expDate <= sysdate) {
                        if (document.getElementById('spn' + type + 'PassExpiry_' + i) != null) {
                            document.getElementById('spn' + type + 'PassExpiry_' + i).style.display = "block";
                            document.getElementById('spn' + type + 'PassExpiry_' + i).innerHTML = 'Passport Expire date should be Greater than current date';
                            bool = false;
                        }
                    }
                    else {
                        if (document.getElementById('spn' + type + 'PassExpiry_' + i) != null) {
                            document.getElementById('spn' + type + 'PassExpiry_' + i).style.display = "none";
                            document.getElementById('spn' + type + 'PassExpiry_' + i).innerHTML = "";
                        }
                    }
                }
            }
            //if (document.getElementById('ddl' + type + 'Nationality_' + i) != null && document.getElementById('ddl' + type + 'Nationality_' + i).value.trim().length == 0) {
            //    document.getElementById('spn' + type + 'Nationality_' + i).style.display = "block";
            //    document.getElementById('spn' + type + 'Nationality_' + i).innerHTML = "Please select the " + paxType + " Applicant " + i + " Nationality.";
            //    bool = false;
            //}
            //else {
            //    if (document.getElementById('spn' + type + 'Nationality_' + i) != null) {
            //        document.getElementById('spn' + type + 'Nationality_' + i).style.display = "none";
            //        document.getElementById('spn' + type + 'Nationality_' + i).innerHTML = "";
            //    }
            //    bool = true;
            //}
            if (document.getElementById('file' + type + 'PassportFront_' + i) != null && document.getElementById('file' + type + 'PassportFront_' + i).value.trim().length == 0) {
                document.getElementById('spn' + type + 'PassportFront_' + i).style.display = "block";
                document.getElementById('spn' + type + 'PassportFront_' + i).innerHTML = "Please select the " + paxType + " Applicant " + i + " Passport Front Page.";
                bool = false;
            }
            else {
                if (document.getElementById('spn' + type + 'PassportFront_' + i) != null) {
                    document.getElementById('spn' + type + 'PassportFront_' + i).style.display = "none";
                    document.getElementById('spn' + type + 'PassportFront_' + i).innerHTML = "";
                }
            }
            if (document.getElementById('file' + type + 'PassportBack_' + i) != null && document.getElementById('file' + type + 'PassportBack_' + i).value.trim().length == 0) {
                document.getElementById('spn' + type + 'PassportBack_' + i).style.display = "block";
                document.getElementById('spn' + type + 'PassportBack_' + i).innerHTML = "Please select the " + paxType + " Applicant " + i + " Passport Back Page.";
                bool = false;
            }
            else {
                if (document.getElementById('spn' + type + 'PassportBack_' + i) != null) {
                    document.getElementById('spn' + type + 'PassportBack_' + i).style.display = "none";
                    document.getElementById('spn' + type + 'PassportBack_' + i).innerHTML = "";
                }
            }
            if (document.getElementById('file' + type + 'PassportPhoto_' + i) != null && document.getElementById('file' + type + 'PassportPhoto_' + i).value.trim().length == 0) {
                document.getElementById('spn' + type + 'PassportPhoto_' + i).style.display = "block";
                document.getElementById('spn' + type + 'PassportPhoto_' + i).innerHTML = "Please select the " + paxType + " Applicant " + i + " Passport Size Photo.";
                bool = false;
            }
            else {
                if (document.getElementById('spn' + type + 'PassportPhoto_' + i) != null) {
                    document.getElementById('spn' + type + 'PassportPhoto_' + i).style.display = "none";
                    document.getElementById('spn' + type + 'PassportPhoto_' + i).innerHTML = "";
                }
            }
            return bool;
        }


    </script>
    <asp:HiddenField ID="hdnAdultCount" Value="1" runat="server" />
    <asp:HiddenField ID="hdnChildCount" Value="0" runat="server" />
    <asp:HiddenField ID="hdnInfantCount" Value="0" runat="server" />
    <asp:HiddenField ID="hdnAdultCounter" Value="1" runat="server" />
    <asp:HiddenField ID="hdnChildCounter" Value="0" runat="server" />
    <asp:HiddenField ID="hdnInfantCounter" Value="0" runat="server" />
    <asp:HiddenField ID="hdnAdtApplicantDetails" Value="" runat="server" />
    <asp:HiddenField ID="hdnChdApplicantDetails" Value="" runat="server" />
    <asp:HiddenField ID="hdnInfApplicantDetails" Value="" runat="server" />
    <%-- using for showing the calendars --%>
    <%--<asp:HiddenField ID="hdnDOBId" Value="0" runat="server" />
    <asp:HiddenField ID="hdnPassExpiryId" Value="0" runat="server" />--%>


    <div class="clear" style="margin-left: 30px">
        <div id="container3" style="position: absolute; top: 240px; left: 7%; z-index: 9999; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container4" style="position: absolute; top: 240px; left: 40%; z-index: 9999; display: none;">
        </div>
    </div>

    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>

    <div class="row">
        <div class="col-md-12">
            <asp:Label ID="lblErrorMessage" runat="server" Style="color: red" Text=""></asp:Label>
            <div class="bg_white pad_10">
                <div class="col-md-3">
                    <div class="form-group">
                        <label><strong>Visa Type<span style="color: red">*</span>: </strong></label>
                        <asp:DropDownList ID="ddlVisaType" runat="server" onchange="paxCountChanged();">
                        </asp:DropDownList>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Adult<span style="color: red">*</span>: </label>

                        <select class="form-control" id="ddlAdult" name="ddlAdult" onchange="paxCountChanged()" disabled="disabled">
                            <option value="1" selected="selected">1 </option>
                            <option value="2">2 </option>
                            <option value="3">3 </option>
                            <option value="4">4 </option>
                            <option value="5">5 </option>
                            <option value="6">6 </option>
                            <option value="7">7 </option>
                            <option value="8">8 </option>
                            <option value="9">9 </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Child: </label>

                        <select class="form-control" id="ddlChild" name="ddlChild" onchange="paxCountChanged()" disabled="disabled">
                            <option value="0" selected="selected">Select </option>
                            <option value="1">1 </option>
                            <option value="2">2 </option>
                            <option value="3">3 </option>
                            <option value="4">4 </option>
                            <option value="5">5 </option>
                            <option value="6">6 </option>
                            <option value="7">7 </option>
                            <option value="8">8 </option>
                            <option value="9">9 </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Infant: </label>

                        <select class="form-control" id="ddlInfant" name="ddlInfant" onchange="paxCountChanged()" disabled="disabled">
                            <option value="0" selected="selected">Select </option>
                            <option value="1">1 </option>
                            <option value="2">2 </option>
                            <option value="3">3 </option>
                            <option value="4">4 </option>
                            <option value="5">5 </option>
                            <option value="6">6 </option>
                            <option value="7">7 </option>
                            <option value="8">8 </option>
                            <option value="9">9 </option>
                        </select>
                    </div>
                </div>
                <div class="col-md-3">
                    <div class="form-group">
                        <label>Visa Price: </label>
                        <input id="lblPrice" name="lblPrice" disabled="disabled" value='<%=currency %> 0' type="text" class="form-control" />
                    </div>
                </div>

                <div class="bggray col-md-12">
                    <strong>Applicant Details </strong>
                </div>

                <div class="row">
                    <div class="col-md-3">
                        <label id="lblApplicant1"><strong>Adult Applicant 1: </strong></label>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Applicant Full Name<span style="color: red">*</span>: </label>
                            <input type="text" id="txtAdtFullName_1" name="txtAdtFullName_1" class="form-control" onkeypress="return isAlpha(event);" onchange="removeErrorMessage('spnAdtFullName_1');" disabled="disabled" />
                            <span style="color: red; display: none" id="spnAdtFullName_1"></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Date of Birth<span style="color: red">*</span>:</label>
                            <div class="row">

                                <div class="col-md-4">
                                    <select class="form-control pull-left" id="ddlAdtdobDay_1" name="ddlAdtdobDay_1" style="width: 60px;" disabled="disabled" onchange="removeErrorMessage('spnAdtDOB_1');">


                                        <option selected="selected" value="-1">Day</option>

                                        <%for (var i = 1; i <= 31; i++)
                                        { %>
                                        <%if (i <= 9)
                                        {%>
                                        <option value="<%=i %>"><%="0"+i %></option>
                                        <% } %>
                                        <%else
                                            {%>
                                        <option value="<%=i %>"><%=i %></option>
                                        <% }
                                        %>
                                        <%}%>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control " id="ddlAdtdobMonth_1" name="ddlAdtdobMonth_1" disabled="disabled" onchange="removeErrorMessage('spnAdtDOB_1');">
                                        <option selected="selected" value="-1">Month</option>
                                        <option value="01">Jan</option>
                                        <option value="02">Feb</option>
                                        <option value="03">Mar</option>
                                        <option value="04">Apr</option>
                                        <option value="05">May</option>
                                        <option value="06">Jun</option>
                                        <option value="07">Jul</option>
                                        <option value="08">Aug</option>
                                        <option value="09">Sep</option>
                                        <option value="10">Oct</option>
                                        <option value="11">Nov</option>
                                        <option value="12">Dec</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control pull-left" id="ddlAdtdobYear_1" name="ddlAdtdobYear_1" disabled="disabled" onchange="removeErrorMessage('spnAdtDOB_1');">
                                        <option selected="selected" value="-1">Year</option>

                                        <%DateTime today = DateTime.Now;
                                            for (var i = (today.Year - 100); i <= today.Year; i++)
                                            {%>
                                        <option value="<%=i %>"><%=i %></option>

                                        <%  }%>
                                    </select>
                                </div>
                            </div>
                            <span class="error_msg" id="spnAdtDOB_1"></span>
                        </div>
                    </div>


                    <div class="col-md-2">
                        <div class="form-group">
                            <label>Passport No.<span style="color: red">*</span>: </label>
                            <input type="text" id="txtAdtPassport_1" name="txtAdtPassport_1" class="form-control" onkeypress="return isAlphaNumeric(event);" disabled="disabled" onchange="removeErrorMessage('spnAdtPassport_1');" />
                            <span style="color: red; display: none" id="spnAdtPassport_1"></span>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>Passport Expiry Date<span style="color: red">*</span>: </label>
                            <div class="row">
                                <div class="col-md-4">
                                    <select class="form-control pull-left" id="ddlAdtPassExpiryDay_1" name="ddlAdtPassExpiryDay_1" style="width: 60px;" disabled="disabled" onchange="removeErrorMessage('spnAdtPassExpiry_1');">
                                        <option selected="selected" value="-1">Day</option>
                                        <%for (var i = 1; i <= 31; i++)
                                            { %>
                                        <%if (i <= 9)
                                            {%>
                                        <option value="<%=i %>"><%="0"+i %></option>
                                        <% } %>
                                        <%else
                                            {%>
                                        <option value="<%=i %>"><%=i %></option>
                                        <% }
                                        %>
                                        <%}%>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control pull-left" id="ddlAdtPassExpiryMonth_1" name="ddlAdtPassExpiryMonth_1" disabled="disabled" onchange="removeErrorMessage('spnAdtPassExpiry_1');">
                                        <option selected="selected" value="-1">Month</option>
                                        <option value="01">Jan</option>
                                        <option value="02">Feb</option>
                                        <option value="03">Mar</option>
                                        <option value="04">Apr</option>
                                        <option value="05">May</option>
                                        <option value="06">Jun</option>
                                        <option value="07">Jul</option>
                                        <option value="08">Aug</option>
                                        <option value="09">Sep</option>
                                        <option value="10">Oct</option>
                                        <option value="11">Nov</option>
                                        <option value="12">Dec</option>
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <select class="form-control pull-left" id="ddlAdtPassExpiryYear_1" name="ddlAdtPassExpiryYear_1" disabled="disabled" onchange="removeErrorMessage('spnAdtPassExpiry_1');">
                                        <option selected="selected" value="-1">Year</option>
                                        <%for (var i = today.Year; i <= (today.Year + 40); i++)
                                            {%>
                                        <option value="<%=i %>"><%=i %></option>

                                        <%  }%>
                                    </select>
                                </div>

                            </div>
                            <span class="error_msg" id="spnAdtPassExpiry_1"></span>
                        </div>
                    </div>
                    <div class="col-md-1">
                        <div class="form-group">
                            <label>Nationality<span style="color: red">*</span>: </label>
                            <select class="form-control" id="ddlAdtNationality_1" name="ddlAdtNationality_1" disabled="disabled" onchange="removeErrorMessage('spnAdtNationality_1');">
                                <option>Iraq </option>
                            </select>
                            <span style="color: red; display: none" id="spnAdtNationality_1"></span>
                        </div>
                    </div>
                </div>
                <div class="row">

                    <div class="col-md-12">
                        <div class="bggray col-md-12">
                            <strong>Documents to be Uploaded  </strong>
                        </div>
                        <div class="bg_white pad_10 col-md-12">
                            <table style="width: 100%">
                                <tr>
                                    <td>Passport copy front page </td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="file" disabled="disabled" name="fileAdtPassportFront_1" id="fileAdtPassportFront_1" accept="image/*" onchange="openFile(event,1,'Front','Adt','spnAdtPassportFront_1','PPAdtFront_1');" />
                                        <input type="hidden" name="hdnPPAdtFront_1" id="hdnPPAdtFront_1" />
                                        <img id="PPAdtFront_1" alt="Front" height="100" width="100" style="display:none" />
                                        <span id="spnAdtPassportFront_1" style="color: red; display: none;"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Passport copy back page</td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="file" disabled="disabled" name="fileAdtPassportBack_1" id="fileAdtPassportBack_1" accept="image/*" onchange="openFile(event,1,'Back','Adt','spnAdtPassportBack_1', 'PPAdtBack_1');" />
                                        <input type="hidden" name="hdnPPAdtBack_1" id="hdnPPAdtBack_1" />
                                        <img id="PPAdtBack_1" alt="Back" height="100" width="100" style="display:none" />
                                        <span id="spnAdtPassportBack_1" style="color: red; display: none;"></span>
                                    </td>
                                </tr>

                                <tr>
                                    <td>Passport size clear Photograph</td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="file" disabled="disabled" name="fileAdtPassportPhoto_1" id="fileAdtPassportPhoto_1" accept="image/*" onchange="openFile(event,1,'Photo','Adt','spnAdtPassportPhoto_1','PPAdtPhoto_1');" />
                                        <input type="hidden" name="hdnPPAdtPhoto_1" id="hdnPPAdtPhoto_1" />
                                        <img id="PPAdtPhoto_1" alt="Photo" height="100" width="100" style="display:none" />
                                        <span id="spnAdtPassportPhoto_1" style="color: red; display: none;"></span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>Birth Cerificate</td>
                                </tr>
                                <tr>
                                    <td>
                                        <input type="file" disabled="disabled" name="fileAdtBirthCerificate_1" id="fileAdtBirthCerificate_1" accept="image/*" onchange="openFile(event,1,'Birth','Adt','spnAdtBirthCertificate_1', 'PPAdtBirth_1');" />
                                        <input type="hidden" name="hdnPPAdtBirth_1" id="hdnPPAdtBirth_1" />
                                        <img id="PPAdtBirth_1" alt="Birth Certificate" height="100" width="100"  style="display:none" />
                                        <span id="spnAdtBirthCertificate_1" style="color: red; display: none;"></span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </div>



                </div>
                <div id="divApplicantDet">
                </div>
            </div>
            <div class="row">
                <div class="col-md-1 pull-right">
                    <%--<input type="button" value="AddMore" id="addMore1" style="display: none" onclick="AddNewRow()" />--%>
                </div>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-8">
            <br />
            <%--<input type="submit" id="btnSave" style="display:none" value="Save" onclick="return validateSave();" /> --%>
            <asp:Button ID="btnSave" OnClientClick="return validateSave();" Style="display: none" Text="Save" runat="server" OnClick="btnSave_Click" />
        </div>
    </div>




    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0"></iframe>



    <div id="emailHeaderDet" runat="server" visible="false">
        <strong>Applied VisaType :</strong> %VisaType%,
        <br />
        <strong>Adult Count :</strong> %AdultCount%,
        <br />
        <strong>Child Count :</strong> %ChildCount%,
        <br />
        <strong>Infant Count :</strong> %InfantCount%,
        <br />
        <strong>Visa Price :</strong> %VisaPrice%
        <br />
        <br />
        <br />
        <strong>Applicant Details:</strong><br />
        <br />
    </div>
    <div id="emailApplicantDet" runat="server" visible="false">
        <strong>PaxType :</strong> %PaxType%,<br />
        <strong>ApplicantName :</strong> %ApplicantName%,
        <br />
        <strong>DateOfBirth :</strong> %DateOfBirth%,
        <br />
        <strong>PassportNumber :</strong> %PassportNumber%,
        <br />
        <strong>Passport ExpiryDate :</strong> %PassportExpiry%,
        <br />
        <strong>Nationality :</strong> %Nationality%,
        <br />
        <br />
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
