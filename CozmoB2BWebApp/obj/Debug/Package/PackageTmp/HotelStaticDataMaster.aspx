﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true"  ValidateRequest="false" CodeBehind="HotelStaticDataMaster.aspx.cs"  Inherits="CozmoB2BWebApp.HotelStaticDataMaster" %>
 <%@ MasterType VirtualPath="~/TransactionBE.master"   %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    
     <%--<script src="Scripts/Common/common.js"></script>--%>
    <div class="col-md-12" style="align-content: center">
        <h3>Hotel Static Data Master</h3>
    </div>
    <div class="body_container" style="height: 600px">
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                HotelId :
                <span class="red_span">*</span>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtHotelId" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
                <span class="red_span" style="display: none" id="HotelId"></span>
            </div>
            <div class="col-md-2">
                Hotel Name :
                <span class="red_span">*</span>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtHotelName" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                 <span class="red_span" style="display: none" id="HotelName"></span>
            </div>
            <div class="col-md-2">
                Country Name :
                 <span class="red_span">*</span>
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlCountry" runat="server" AppendDataBoundItems="true" CssClass="form-control">
                    <asp:ListItem Value="-1">Please Select Country</asp:ListItem>
                </asp:DropDownList>
                <span class="red_span" style="display: none" id="Country"></span>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                City Name :
                 <span class="red_span">*</span>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtCityName" runat="server" CssClass="form-control" MaxLength="50"></asp:TextBox>
                <span class="red_span" style="display: none" id="City"></span>
            </div>
            <div class="col-md-2">
                Address :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtAddress" runat="server" CssClass="form-control"  MaxLength="200" Width="500"></asp:TextBox>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                ZipCode :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtZipcode" runat="server" CssClass="form-control" MaxLength="20"></asp:TextBox>
            </div>
            <div class="col-md-2">
                Rating :
                 <span class="red_span">*</span>
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlRating" runat="server" CssClass="form-control" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">Please Select Rating</asp:ListItem>
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                    <asp:ListItem Value="5">5</asp:ListItem>
                </asp:DropDownList>
                <span class="red_span" style="display: none" id="Rating"></span>
            </div>
            <div class="col-md-2">
                LAT :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtLat" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                LNG :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtLng" runat="server" CssClass="form-control"></asp:TextBox>
            </div>
            <div class="col-md-2">
                Room Count :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtRoom" runat="server" CssClass="form-control" onKeyPress="return isNumbersRoom(event)"></asp:TextBox>
            </div>
            <div class="col-md-2">
                Phone :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtPhone" runat="server" CssClass="form-control" MaxLength="30" onKeyPress="return isNumbers(event)"></asp:TextBox>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                Email :
                 <span class="red_span">*</span>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtEmail" runat="server" CssClass="form-control" MaxLength="100"></asp:TextBox>
                <span class="red_span" style="display: none" id="Email"></span>
            </div>
            <div class="col-md-2">
                WebSite:
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtWebSite" runat="server" CssClass="form-control" MaxLength="1000"></asp:TextBox>
            </div>
            <div class="col-md-2">
                PropertyCategory :
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlCategory" runat="server" CssClass="form-control" Enabled="false">
                    <asp:ListItem Value="Hotel">Hotel</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-12 padding-0 marbot_10">
             <div class="col-md-2">
                ChainCode :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtChainCode" runat="server" CssClass="form-control" MaxLength="5"></asp:TextBox>
            </div>
            <div class="col-md-2">
                Description :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtDescription" TextMode="MultiLine" runat="server" CssClass="form-control"></asp:TextBox>
            </div> 
			<div class="col-md-2">
                Rezlive ID :
            </div>
			<div class="col-md-2">
				<asp:TextBox ID="txtRezliveid" runat="server" CssClass="form-control" onKeyPress="return isNumbers(event)"></asp:TextBox>
			</div>
            <div class="clearfix"></div>
        </div>
		 <div class="col-md-12 padding-0 marbot_10">
			  <div class="col-md-2">
               His ID:
            </div>
			 <div class="col-md-2">
				 <asp:TextBox ID="txtHisID" runat="server" CssClass="form-control" onKeyPress="return isNumbers(event)"></asp:TextBox>
			 </div>
			 <div class="col-md-2">
              Illusions hotelcode:
            </div>
			 <div class="col-md-2">
				 <asp:TextBox ID="txtIlluHotelCode" runat="server" CssClass="form-control" ></asp:TextBox>
			 </div>
			 <div class="col-md-2">
              Illusions Citycode:
            </div>
			 <div class="col-md-2">
				 <asp:TextBox ID="txtIlluCityCode" runat="server" CssClass="form-control" ></asp:TextBox>
			 </div>
			 <div class="clearfix"></div>
			 </div>
        <div class="col-md-12 padding-0 marbot_10">
              <div class="col-md-2">
                hotel Facility:
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtFacility" TextMode="multiline" runat="server" CssClass="form-control" onkeydown="return (event.keyCode!=13);"/>
            </div>
            <div class="clearfix"></div>
            </div>
        <div class="col-md-12 padding-0 marright_10">
            <div class="col-md-2">
                ImageUrl:
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtImageUrl" TextMode="MultiLine"  runat="server" CssClass="form-control" onkeydown="return (event.keyCode!=13);"></asp:TextBox>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-12">
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn but_b button pull-right" OnClientClick="Search();"/>
            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn but_b button pull-right" OnClick="btnClear_Click" />
            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn but_b button  pull-right" OnClientClick="return Save();" />
        </div>
        <asp:HiddenField ID="hid" runat="server" Value="0" />
        <asp:Label ID="lblSuccessMsg" runat="server" ></asp:Label> 
    </div>
    <script type="text/javascript">
        function Save() {
            var Valid = true;

            if (document.getElementById('<%=txtHotelId.ClientID%>').value.trim().length <= 0) {
                document.getElementById('HotelId').style.display = 'block';
                document.getElementById('HotelId').innerHTML = 'Please Enter Hotel Id';
                Valid = false;
            }

            if (document.getElementById('<%=txtHotelName.ClientID%>').value.trim().length <= 0) {
                document.getElementById('HotelName').style.display = 'block';
                document.getElementById('HotelName').innerHTML = 'Please Enter Hotel Name';
                Valid = false;
            }

            if (document.getElementById('<%=ddlCountry.ClientID%>').value == -1) {
                document.getElementById('Country').style.display = 'block';
                document.getElementById('Country').innerHTML = 'Please Select Country';
                Valid = false;
            }

             if (document.getElementById('<%=txtCityName.ClientID%>').value.trim().length <= 0) {
                document.getElementById('City').style.display = 'block';
                document.getElementById('City').innerHTML = 'Please Enter City Name';
                Valid = false;
            }

            if (document.getElementById('<%=ddlRating.ClientID%>').value == -1) {
                document.getElementById('Rating').style.display = 'block';
                document.getElementById('Rating').innerHTML = 'Please Select Rating';
                Valid = false;
            }

            if (document.getElementById('<%=txtEmail.ClientID%>').value.trim().length <= 0) {
                document.getElementById('Email').style.display = 'block';
                document.getElementById('Email').innerHTML = 'Please Enter Email';
                Valid = false;
            }
            else if (!checkEmail(document.getElementById('<%=txtEmail.ClientID %>').value.trim())) {
                document.getElementById('Email').style.display = 'block';
                document.getElementById('Email').innerHTML = 'Please enter valid Email Address';
                Valid = false;
            }

            if (Valid == true) {
                return true;
            }
            else {
                return false;
            }
            
        }
        function isNumbers(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            //if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47 || charCode<=43)) {
            if (charCode > 31 && (charCode < 43 || charCode > 57 )) {
                return false;
            }
            return true;
        }
        function isNumbersRoom(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
                return false;
               }
            return true;
        }
        function Search() {
           location.href = "HotelStaticListQueue.aspx";
        }
    </script>
    

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
