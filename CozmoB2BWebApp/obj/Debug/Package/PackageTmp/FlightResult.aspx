﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" EnableEventValidation="false" Inherits="FlightResultUI" Title="Flight Search Results" CodeBehind="FlightResult.aspx.cs" %>

<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <style type="text/css">
    .body_container {
    min-height: calc(100vh - 166px);
}
    .corp-travel-dropDown {
    min-width: 220px !important;
    top: auto !important;
    bottom: -70px !important;
    padding: 15px !important;
    margin: 0 !important;
    right: 25% !important;
    left: auto !important;
    box-shadow: rgba(0,0,0,0.6) 0 1px 11px;
}
    .corpPolicyTip{
        font-size: 11px;
        font-weight: 700;
        color: #575857;
        max-height: 55px;
        overflow: hidden;
        overflow-y: auto;
    }
    .chkList{
        margin-top:0;
        margin-left:0;
    }
    #ctl00_cphTransaction_chkListAirlines label {
        left: auto;
        position: static;
        margin-left: 5px;
    }
    .onlythis{
        top:3px;
    }
    .disabled {
    color: lightgray;
    pointer-events: none;
}
    </style>
    <%--<script src="Scripts/jsBE/prototype.js" type="text/javascript"><bagga/script>--%>
    <%--<script type="text/javascript" src="Scripts/jsBE/Search.js"></script>--%>

    <script type="text/javascript" src="Scripts/jsBE/SearchResult.js"></script>

    <link rel="stylesheet" href="css/jquery-ui.css" />
    <link href="css/select2.css" rel="stylesheet" />


    <%--<script src="Scripts/SliderJquery/jquery-ui.js" type="text/javascript"></script> --%>
    <script src="Scripts/SliderJquery/jquery-ui-1.12.js" type="text/javascript"></script>

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />

    <script src="Scripts/MultiCity.js" type="text/javascript" defer="defer"></script>
  
  
  <%--<script src="Scripts/PrefAirline.js" type="text/javascript" defer="defer"></script>--%>

    <script type="text/javascript">
        $(function() {

            $("#slider-range").slider({
               range: "max",
                min: parseFloat(document.getElementById('<%=hdnMinValue.ClientID %>').value),
                max: parseFloat(document.getElementById('<%=hdnMaxValue.ClientID %>').value),
                step: 1,
                values: [parseFloat(document.getElementById('<%=hdnPriceMin.ClientID %>').value), parseFloat(document.getElementById('<%=hdnPriceMax.ClientID %>').value)],
                slide: function(event, ui) {

                    if (ui.values[0] <= ui.values[1]) {
                        $("#amount").val(ui.values[0] + "-" + ui.values[1]);
                        document.getElementById('<%=hdnPriceMin.ClientID %>').value = ui.values[0];
                        document.getElementById('<%=hdnPriceMax.ClientID %>').value = ui.values[1];
                    }
                    else {
                        $("#amount").val(parseFloat(document.getElementById('<%=hdnMinValue.ClientID %>').value) + "-" + parseFloat(document.getElementById('<%=hdnMaxValue.ClientID %>').value));
                        document.getElementById('<%=hdnPriceMin.ClientID %>').value = parseFloat(document.getElementById('<%=hdnMinValue.ClientID %>').value);
                        document.getElementById('<%=hdnPriceMax.ClientID %>').value = parseFloat(document.getElementById('<%=hdnMaxValue.ClientID %>').value);
                    }
                }
            });
            $("#amount").val($("#slider-range").slider("values", 0) + "-" + $("#slider-range").slider("values", 1));

        });
   
        $(function() {
            $("#slider-range").slider({
                change: function() {
                    var duration = document.getElementById('amount').value;
                    var min = duration.split('-')[0];
                    var max = duration.split('-')[1];                   

                    document.getElementById('<%=hdnPriceMin.ClientID %>').value = min;
                    document.getElementById('<%=hdnPriceMax.ClientID %>').value = max;
                    document.getElementById('<%=Change.ClientID %>').value = "false";
                    document.getElementById('<%=hdnFilter.ClientID %>').value = "Price";
                    document.getElementById('label').InnerHTML = max;
                    if (document.getElementById("chkDownAll") != null) {
                        document.getElementById("chkDownAll").checked = false;
                        SelectAll();//Unselect all checkboxes when filtered cleared
                    }

                    document.forms[0].submit();
                    
                }
            });
        });

        function AssignCorpData() {

            <%if (Settings.LoginInfo.IsCorporate == "Y")
        {%> 
            $('#ctl00_cphTransaction_ddlBookingClass').prop('selectedIndex', '0').change();
            $('#ctl00_cphTransaction_ddlAdults').attr('disabled', 'disabled');
            $('#ctl00_cphTransaction_ddlChilds').attr('disabled', 'disabled');
            $('#ctl00_cphTransaction_ddlInfants').attr('disabled', 'disabled');

            $('#CorpDiv').show();

            var profileId = '<%=request.CorporateTravelProfileId%>';
            var reasonId = '<%=request.CorporateTravelReasonId%>';
            var ddlFTReasons = document.getElementById('<%=ddlFlightTravelReasons.ClientID%>');
            var ddlFEmployees = document.getElementById('<%=ddlFlightEmployee.ClientID%>');
            for (i = 0; i < ddlFTReasons.length; i++) {
                if (ddlFTReasons.options[i].value.startsWith(reasonId)) {
                    $('#<%=ddlFlightTravelReasons.ClientID%> option').eq(i).prop('selected', true);
                    $('#<%=ddlFlightTravelReasons.ClientID%>').select2('val', ddlFTReasons.options[i].value);
                    ddlFTReasons.value = ddlFTReasons.options[i].value;
                    $('#<%=hdnTravelReason.ClientID%>').val(ddlFTReasons.options[i].value);
                    break;
                }
            }
            //document.getElementById('<%=ddlFlightTravelReasons.ClientID%>').value = $('#<%=hdnTravelReason.ClientID%>').val();
            for (i = 0; i < ddlFEmployees.length; i++) {
                if (ddlFEmployees.options[i].value.startsWith(profileId)) {
                    $('#<%=ddlFlightEmployee.ClientID%> option').eq(i).prop('selected', true);
                    $('#<%=ddlFlightEmployee.ClientID%>').select2('val', ddlFEmployees.options[i].value);
                    ddlFEmployees.value = ddlFEmployees.options[i].value;
                    $('#<%=hdnFlightEmployee.ClientID%>').val(ddlFEmployees.options[i].value);
                    break;
                }
            }
            //document.getElementById('<%=ddlFlightEmployee.ClientID%>').value = $('#<%=hdnFlightEmployee.ClientID%>').val();
            <%}
            else
            {%>
            $('#ctl00_cphTransaction_ddlBookingClass').select2('val', '2');
            <%}%>


            $("#ctl00_cphTransaction_ddlOnwardTimings").select2('val', $("#ctl00_cphTransaction_hdnOnTimingFilter").val());
            $("#ctl00_cphTransaction_ddlReturnTimings").select2('val', $("#ctl00_cphTransaction_hdnRetTimingFilter").val());

        }
    </script>


    <script type="text/javascript">
        function ShowLoader(resId, travResId) {
            window.scrollTo(0, 0);
            if (travResId == '') {
                travResId = '0';
            }
            $('.modal').modal('hide');
            document.getElementById('PreLoader').style.display = "block";
            document.getElementById('MainDiv').style.display = "none";
            AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'PageParams', 'sessionData':'" + resId + "," + travResId + "', 'action':'set'}");
            if (travResId == '-1')//for TBOAir
                window.location.href = "PassengerDetails";
        }
        var Ajax;
           if (window.XMLHttpRequest) {
               Ajax = new XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        var ID;
        function Book(id) {
            document.getElementById('<%=Change.ClientID %>').value = "true";
            document.getElementById('<%=hdnFilter.ClientID %>').value = "";
            ID = id;
            var passData = "id=" + id;
            
            Ajax.onreadystatechange = ShowTravelReason
            Ajax.open("POST", "CorpProfileBookingAjax");
            Ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");

            Ajax.send(passData);            
        }

        function ShowTravelReason() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        document.getElementById('divTravelReason' + ID).style.display = 'block';
                        document.getElementById('divTravelReason' + ID).innerHTML = Ajax.responseText;
                        
                    }
                    else {                        
                        ShowLoader(ID,'0');                        
                        window.location.href = "PassengerDetails";
                    }
                }
            }
        }

        function ShowPopup(id) {
            document.getElementById('<%=Change.ClientID %>').value = "true";
            document.getElementById('<%=hdnFilter.ClientID %>').value = "";            
            document.getElementById('divTravelPopup' + id).style.display = 'block';                        
        }


        function HidePopup(id) {
            document.getElementById('divTravelPopup' + id).style.display = 'none';
            document.getElementById("errorReason").style.display = "none";
        }
        

        function HideTravelReason(id) {
            document.getElementById('divTravelReason' + id).style.display = 'none';
            document.getElementById("errorReason").style.display = "none";
            
        }

        function BookNow(resId) {            
            if (document.getElementById("ddlTravelReason" + resId).value != "-1") {
                document.getElementById("errorReason").style.display = "none";
                ShowLoader(resId, document.getElementById("ddlTravelReason" + resId).value.split('~')[0]);                
                window.location.href = "PassengerDetails";
            }
            else {
                document.getElementById("errorReason").style.display = "block";                
            }
        }
        
        function show(id) {
            document.getElementById(id).style.display = "block";
        }
        function hide(id) {
            document.getElementById(id).style.display = "none";
        }
        function ShowPage(pageNo) {
            //$('<%=PageNoString.ClientID %>').value = pageNo;
            document.getElementById('<%=PageNoString.ClientID %>').value = pageNo;
            document.getElementById('<%=Change.ClientID %>').value = "false";            
            $(document.forms[0]).submit();
        }
        function FilterResults(type) {
            // Added by hari malla on 24-05-2019.
            // does not allow the filtering when empty results.  
            var rows = $('#ctl00_cphTransaction_hdnResultCount').val(); 
             if ( parseInt(rows)> 1) {
                document.getElementById('<%=hdnFilter.ClientID %>').value = type;
                document.getElementById('<%=Change.ClientID %>').value = "false";
                if (document.getElementById("chkDownAll") != null) {
                    document.getElementById("chkDownAll").checked = false;
                    SelectAll();//Unselect all checkboxes when filtered cleared
                }
                 if ($('#ctl00_cphTransaction_chkAirline').is(":checked") || type == "Airline" || type == "Stops"
                     || type == "Refund" || type == "nonRefund")
                    document.forms[0].submit();
                }
        }


        function SortByDuration() {
            if (document.getElementById("durationSort").value == "By Journey Time") {//Total Duration
                document.getElementById("ctl00_cphTransaction_ddlStops").value = "-1";
                document.getElementById("ctl00_cphTransaction_ddlDeparture").value = "-1";
                document.getElementById("ctl00_cphTransaction_ddlAirline").value = "-1";
                document.getElementById("ctl00_cphTransaction_ddlArrival").value = "-1";
                FilterResults('TD');
            }
            else if (document.getElementById("durationSort").value == "By Flying Duration") {//Flying Duration
                document.getElementById("ctl00_cphTransaction_ddlStops").value = "-1";
                document.getElementById("ctl00_cphTransaction_ddlDeparture").value = "-1";
                document.getElementById("ctl00_cphTransaction_ddlAirline").value = "-1";
                document.getElementById("ctl00_cphTransaction_ddlArrival").value = "-1";
                FilterResults('FD');
            }
                // Added by hari malla on 24-05-2019 for default filtering.
            else if (document.getElementById("durationSort").value == "Duration") {// Duration
                document.getElementById("ctl00_cphTransaction_ddlStops").value = "-1";
                document.getElementById("ctl00_cphTransaction_ddlDeparture").value = "-1";
                document.getElementById("ctl00_cphTransaction_ddlAirline").value = "-1";
                document.getElementById("ctl00_cphTransaction_ddlArrival").value = "-1";
                FilterResults('D');
            }
        }

        


        function OnlyThis(item, count) {
            for (var i = 0; i < eval(count); i++) {
                if (document.getElementById('ctl00_cphTransaction_chkListAirlines_' + i) != null) {
                    document.getElementById('ctl00_cphTransaction_chkListAirlines_' + i).checked = false;
                }
            }
            document.getElementById(item).checked = true;
            document.getElementById('ctl00_cphTransaction_chkAirline').checked = false;
            FilterResults('Airline');
        }
        
//        $(document).ready(function() {
//            if (document.getElementById('<%=chkListAirlines.ClientID %>')) {
//                var input = document.getElementById('<%=chkListAirlines.ClientID %>').getElementsByTagName("input");
//                for (var i = 0; i < input.length; i++) {
//                    if (input[i].type == "checkbox") {
//                        var ele = input[i];
//                        var id = $(ele).attr('id');
//                        $(ele).attr('onclick', 'filterFlightResults(' + id + ')');
//                    }
//                }
//            }
//        })
        function filterFlightResults(ele) {
            if (document.getElementById('<%=chkListAirlines.ClientID %>')) {
                //getting the id of the checkbox element.
                var input = document.getElementById('<%=chkListAirlines.ClientID %>').getElementsByTagName("input");
                for (var i = 0; i < input.length; i++) {
                    if (input[i].type == "checkbox") {
                        input[i].checked = false;
                    }
                }
                var id = ele.id;
                document.getElementById(id).checked = true;
                document.getElementById('<%=chkAirline.ClientID %>').checked = false;

                document.getElementById('<%=hdnFilter.ClientID %>').value = 'Airline';
                document.getElementById('<%=Change.ClientID %>').value = "false";
                document.forms[0].submit();
            }
        }

        function SelectUnSelect(control) {
            var chkBoxList = getElement(control);

            var chkBoxCount = chkBoxList.getElementsByTagName("input");
            if (chkBoxCount.length > 0) {
                for (var i = 0; i < chkBoxCount.length; i++) {
                    if (document.getElementById('<%=chkAirline.ClientID %>').checked == true) {
                        chkBoxCount[i].checked = true;

                        if ($('#ctl00_cphTransaction_chkRefundable').is(":visible"))
                            $('#ctl00_cphTransaction_chkRefundable').prop("checked", true);
                        if ($('#ctl00_cphTransaction_chkNonRefundable').is(":visible"))
                            $('#ctl00_cphTransaction_chkNonRefundable').prop("checked", true);
                        $('#ctl00_cphTransaction_chkNonStop').prop("checked",true);
                        $('#ctl00_cphTransaction_chkOneStop').prop("checked",true);
                        $('#ctl00_cphTransaction_chkTwoStops').prop("checked",true);
                    }
                    else {
                        chkBoxCount[i].checked = false;
                        if ($('#ctl00_cphTransaction_chkRefundable').is(":visible"))
                            $('#ctl00_cphTransaction_chkRefundable').prop("checked", false);
                        if ($('#ctl00_cphTransaction_chkNonRefundable').is(":visible"))
                            $('#ctl00_cphTransaction_chkNonRefundable').prop("checked", false);
                        $('#ctl00_cphTransaction_chkNonStop').prop("checked", false);
                        $('#ctl00_cphTransaction_chkOneStop').prop("checked", false);
                        $('#ctl00_cphTransaction_chkTwoStops').prop("checked", false);

                    }
                }
            }
            FilterResults('Airline');

        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 48 || charCode > 57 || charCode == 46)) {
                return false;
            }
            return true;
        }
        //@@@@ select multiple items with select checkbox . chandan 14/12/2015
        function selectBox(id) {
            var checkBoxId = id;
            var getResultID = checkBoxId.split('-');
            var resultId = getResultID[1];
            var hdnArray = '';
            var hdnField = document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail');
            var chkAll = document.getElementById('chkDownAll');

            if (document.getElementById(checkBoxId).checked) {
                if (hdnField.value != "") {
                    hdnArray = hdnField.value.split(',');

                    if (hdnArray.indexOf('' + resultId) == -1) hdnField.value += ',' + resultId;

                }
                else {
                    hdnField.value = resultId;
                }
            }
            else {
                var getNewValues = '';
                var getUnselectValue = hdnField.value.split(',');

                if (getUnselectValue.length > 0) {

                    for (var k = 0; k < getUnselectValue.length; k++) {
                        if (getUnselectValue[k] == resultId)
                            getUnselectValue[k] = "";
                        else {
                            if (getNewValues != '')
                                getNewValues += ',' + getUnselectValue[k];
                            else
                                getNewValues = '' + getUnselectValue[k];
                        }
                    }
                }

                chkAll.checked = false;//Uncheck the Select All checkbox

                document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value = '';
                if (getNewValues != '') {
                    document.getElementById('<%=hdnCheckboxEmail.ClientID %>').value = getNewValues;
                }

            }
        }

    
    </script>

    <script>

        var bagId;
        function showStuff(id,baggage,source) {
            if (id.indexOf('BaggageDiv') >= 0 && source == "PKFares" && (baggage.length == 0)) {
                bagId = id;
                
                var passData = "resultId=" + id.replace('BaggageDiv', '') + "&sessionId=<%=Session["sessionId"]%>";
                Ajax.onreadystatechange = ShowBaggage
                Ajax.open("POST", "BaggageAjax");
                Ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                Ajax.send(passData);
                document.getElementById('BaggageWaitDiv' + id.replace('BaggageDiv', '')).style.display = "block";
            }
            else {
                document.getElementById(id).style.display = 'block';
            }
        }


        function hidestuff(boxid) {
            document.getElementById(boxid).style.display = "none";
        }

        

        function ShowBaggage() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        document.getElementById(bagId).style.display = 'block';
                        document.getElementById(bagId).innerHTML = Ajax.responseText;
                        document.getElementById('BaggageWaitDiv' + bagId.replace('BaggageDiv', '')).style.display = "none";
                    }                    
                }
            }
        }

  
    </script>

    <script>
        $(document).ready(function() {


            $("#ButModify").click(function() {

                $("#DivModify").toggle();

            });

        });
    </script>

    <script type="text/javascript">
        //js function used for next day & previous day results
        function DaySelection(symbol) {
            var dateTxt = $("#ctl00_cphTransaction_DepDate").val();
            var depDate = new Date(dateTxt.split('/')[2] + "/" + dateTxt.split('/')[1] + "/" + dateTxt.split('/')[0]);
            dateTxt = $("#ctl00_cphTransaction_ReturnDateTxt").val();
            var retDate = new Date(dateTxt.split('/')[2] + "/" + dateTxt.split('/')[1] + "/" + dateTxt.split('/')[0]);
            if (symbol == "-") {
                depDate.setDate(depDate.getDate() - 1);
            }
            else {
                depDate.setDate(depDate.getDate() + 1);
            }        

            $("#ctl00_cphTransaction_DepDate").val(depDate.getDate() + "/" + (depDate.getMonth() + 1) + "/" + depDate.getFullYear());
            $("#ctl00_cphTransaction_hdnModifySearch").val("0");
            $("#ctl00_cphTransaction_hdnFilter").val("");
            $("#ctl00_cphTransaction_Change").val("");            

            //If Departure date is greater than return date then expand div and show the validation msg
            if (retDate.getTime() < depDate.getTime()) {
                $("#ctl00_cphTransaction_btnSearchFlight").click();
                $("#ButModify").click();
            }
            else
                $("#ctl00_cphTransaction_btnSearchFlight").click();
        }

        //    if (document.getElementById('roundtrip').checked == true) {
        //        document.getElementById('tblNormal').style.display = 'block';
        //        document.getElementById('tblMultiCity').style.display = 'none';
        //        document.getElementById('textbox_A3').style.visibility = "visible";
        //      
        //        document.getElementById('tblMultiCity').style.display = 'none';
        //       

        //    }
        //  if (document.getElementById('oneway').checked == true) {
        //        document.getElementById('tblNormal').style.display = 'block';
        //        document.getElementById('tblMultiCity').style.display = 'none';
        //        document.getElementById('textbox_A3').style.visibility = "hidden";
        //        
        //        document.getElementById('tblMultiCity').style.display = 'none';
        //        


        //    }
        //    
        //    
        //   if (document.getElementById('multicity').checked == true) {
        //        document.getElementById('tblNormal').style.display = 'none';
        //        document.getElementById('tblMultiCity').style.display = 'block';
        //       
        //           
        //       
        //    }

    </script>

    <script type="text/javascript" >
        function disablefield() {
            var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
            if ('<%=userType %>' == 'ADMIN' ||'<%=userType %>' == 'SUPER') {

                if (document.getElementById('<%=radioAgent.ClientID %>') != null && document.getElementById('<%=radioAgent.ClientID %>').checked == true) {
                    document.getElementById('<%=hdnBookingAgent.ClientID %>').value = "Checked";
                    document.getElementById('wrapper1').style.display = "block";
                    document.getElementById('divLocations').style.display = "block";
                }
                else {
                    document.getElementById('wrapper1').style.display = "none";
                    document.getElementById('<%=hdnBookingAgent.ClientID %>').value = "UnChecked";
                    document.getElementById('divLocations').style.display = "none";
                }
            }

            if (document.getElementById('<%=roundtrip.ClientID %>').checked == true) {
                document.getElementById('tblNormal').style.display = 'block';
                document.getElementById('tblMultiCity').style.display = 'none';
                document.getElementById('textbox_A3').style.visibility = "visible";
                document.getElementById('<%=hdnWayType.ClientID %>').value = "return";

                // document.getElementById('tblGrid').style.display='none';
                document.getElementById('tblMultiCity').style.display = 'none';
                if (document.getElementById('<%=hdnSourceBinding.ClientID%>').value == 'false') {
                    if (chkSupp != null) {
                        var checkbox = chkSupp.getElementsByTagName("input");
                        for (var i = 0; i < checkbox.length; i++) {
                            checkbox[i].checked = true;
                            checkbox[i].disabled = false;
                            document.getElementById("<%=chkFlightAll.ClientID %>").checked = true;
                            document.getElementById("<%=chkFlightAll.ClientID %>").disabled = false;
                        }
                    }
                }
            }
            else if (document.getElementById('<%=oneway.ClientID %>').checked == true) {
                document.getElementById('tblNormal').style.display = 'block';
                document.getElementById('tblMultiCity').style.display = 'none';
                document.getElementById('textbox_A3').style.visibility = "hidden";
                document.getElementById('<%=hdnWayType.ClientID %>').value = "oneway";
                // document.getElementById('tblGrid').style.display='none';
                document.getElementById('tblMultiCity').style.display = 'none';
                if (document.getElementById('<%=hdnSourceBinding.ClientID%>').value == 'false') {
                    if (chkSupp != null) {
                        var checkbox = chkSupp.getElementsByTagName("input");
                        for (var i = 0; i < checkbox.length; i++) {
                            checkbox[i].checked = true;
                            checkbox[i].disabled = false;
                            document.getElementById("<%=chkFlightAll.ClientID %>").checked = true;
                            document.getElementById("<%=chkFlightAll.ClientID %>").disabled = false;
                        }
                    }
                }
            }
            else if (document.getElementById('<%= multicity.ClientID%>').checked == true) {
                document.getElementById('tblNormal').style.display = 'none';
                document.getElementById('tblMultiCity').style.display = 'block';
                // document.getElementById('tblGrid').style.display='block';
                document.getElementById('<%=hdnWayType.ClientID %>').value = "multiway";
                var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");

                var reqType = document.getElementById("<%=hdnWayType.ClientID %>");
                if (reqType.value == 'multiway') {
                    if (chkSupp != null) {
                        var checkbox = chkSupp.getElementsByTagName("input");
                        for (var i = 0; i < checkbox.length; i++) {
                            var source = checkbox[i].nextSibling.innerHTML;
                            if (source == "G9" || source == "FZ" || source == "SG" || source == "6E" || source == "IX" ) {
                                checkbox[i].checked = false;
                                checkbox[i].disabled = true;
                                document.getElementById("<%=chkFlightAll.ClientID %>").checked = false;
                                document.getElementById("<%=chkFlightAll.ClientID %>").disabled = true;
                            }
                        }
                    }
                }
            }
            document.getElementById('<%=hdnSourceBinding.ClientID%>').value = 'false';
            EnableVisaChange();
        }
        //Function Which Shows the VisaChangeCheckBox When the User Selects the SHJ airport code.
        var airportCodes = ['SHJ'];
        function EnableVisaChange() {
            
            var isOnewayChecked = $("#ctl00_cphTransaction_oneway").is(":checked");
            var isRoundtripChecked = $("#ctl00_cphTransaction_roundtrip").is(":checked");
            var origin = $('#ctl00_cphTransaction_Origin').val().toUpperCase().trim();
            var destination = $('#ctl00_cphTransaction_Destination').val().toUpperCase().trim();
            
            console.log('Origin: ' + origin);
            console.log('Destination: ' + destination);
            if ((isOnewayChecked || isRoundtripChecked) && <%=(Settings.LoginInfo.IsCorporate == "N").ToString().ToLower()%> && <%=(Settings.LoginInfo.Currency == "AED").ToString().ToLower()%>) {
                if ((origin != undefined && origin.length >= 3)) {
                    var code = origin.split(')')[0].replace('(', '');
                    var dest = destination.split(')')[0].replace('(', '');
                    console.log('Origin: ' + code);
                    console.log('Destination: ' + dest);
                    if (airportCodes.includes(code)) {//Validating only Origin for Visa Change
                        $('#chkVisaEnable').prop("checked", false);
                        $('#divVisaEnable').show();
                        console.log('VisaChange Show');
                    }
                    else {
                        $('#divVisaEnable').hide();
                        $('#chkVisaEnable').prop('checked', false);
                        console.log('VisaChange Hide');
                        EnableAllSources();
                    }
                }
                else {
                    $('#divVisaEnable').hide();
                    $('#chkVisaEnable').prop("checked", false);
                    EnableAllSources();
                }
            } else {
                $('#divVisaEnable').hide();
                $('#chkVisaEnable').prop("checked", false);
                EnableAllSources();
            }
        }

        function EnableAllSources() {
            $('#Additional-Div input[type=checkbox]').each(function () {
                var id = $(this).attr('id');

                
                $('#' + id).prop("disabled", false);
            });
        } 

        var call1;
        var call2;
        //-Flight Calender control
        function init1() {
            var today = new Date();
            // Rendering Cal1
            call1 = new YAHOO.widget.CalendarGroup("call1", "fcontainer1");
            call1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            //            call1.cfg.setProperty("title", "Select your desired departure date:");
            call1.cfg.setProperty("close", true);
            call1.selectEvent.subscribe(setFlightDate1);
            call1.render();
            // Rendering Cal2
            call2 = new YAHOO.widget.CalendarGroup("call2", "fcontainer2");
            //            call2.cfg.setProperty("title", "Select your desired return date:");
            call2.selectEvent.subscribe(setFlightDate2);
            call2.cfg.setProperty("close", true);
            call2.render();
        }

        function showFlightCalendar1() {
            call2.hide();
            document.getElementById('fcontainer1').style.display = "block";
            document.getElementById('fcontainer2').style.display = "none";

            var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
            if (date1.length > 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');
                call1.cfg.setProperty("selected", depDateArray[1] + "/" + eval(depDateArray[0]) + "/" + depDateArray[2]);
                //call1.setMonth(eval(depDateArray[1]) - 1); //Set the calendar month ZERO based index Jan:0 to Dec:11
            }
            call1.render();
        }

        function showFlightCalendar2() {
            call1.hide();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
            var date2 = document.getElementById('<%=ReturnDateTxt.ClientID %>').value;
            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');
                var retDateArray = date2.split('/');
                var arrMinDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                if (document.getElementById('<%=ReturnDateTxt.ClientID %>').value.length <= 0) {
                    call2.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate() ) + "/" + depDateArray[2]);
                    call2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                } else {
                    call2.cfg.setProperty("selected", retDateArray[1] + "/" + eval(retDateArray[0]) + "/" + retDateArray[2]);
                    //call2.setMonth(eval(retDateArray[1]) - 1); //Set the calendar month Jan:0 to Dec:11
                    call2.cfg.setProperty("minDate", depDateArray[1] + "/" + (arrMinDate.getDate()) + "/" + depDateArray[2]);
                }
                call2.render();
            }
            document.getElementById('fcontainer2').style.display = "block";
        }

        function setFlightDate1() {
            var date1 = call1.getSelectedDates()[0];
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();
            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());
            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=DepDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();
            call1.hide();
            //when we select date on Deperturedate,returndate calender shold be opened default in Modify Search
            if (document.getElementById('<%=roundtrip.ClientID %>') != null && document.getElementById('<%=roundtrip.ClientID %>').checked == true) {
                showFlightCalendar2();
            }
        }

        function setFlightDate2() {
            var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select departure date.";
                return false;
            }
            var date2 = call2.getSelectedDates()[0];
            var depDateArray = date1.split('/');
            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();
            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of return should be greater than or equal to date of departure (" + date1 + ")";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            var month = date2.getMonth() + 1;
            var day = date2.getDate();
            if (month.toString().length == 1) {
                month = "0" + month;
            }
            if (day.toString().length == 1) {
                day = "0" + day;
            }
            document.getElementById('<%=ReturnDateTxt.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            call2.hide();
        }

        YAHOO.util.Event.addListener(window, "load", init1);



        function autoCompInitPrefAirline() {
            oACDSPA = new YAHOO.widget.DS_JSFunction(getAirlines);
            // Instantiate third auto complete
            oAutoCompPA = new YAHOO.widget.AutoComplete('<%=txtPreferredAirline.ClientID %>', 'statescontainer4', oACDSPA);
            oAutoCompPA.prehighlightClassName = "yui-ac-prehighlight";
            oAutoCompPA.queryDelay = 0;
            oAutoCompPA.minQueryLength = 2;
            oAutoCompPA.useIFrame = true;
            oAutoCompPA.useShadow = true;

            oAutoCompPA.formatResult = function(oResultItem, sQuery) {
                document.getElementById('statescontainer4').style.display = "block";
                var toShow = oResultItem[1].split(',');
                var sMarkup = toShow[0] + ',' + toShow[1]; ;
                //var aMarkup = ["<li>", sMarkup, "</li>"]; 
                var aMarkup = [sMarkup];
                return (aMarkup.join(""));
            };
            oAutoCompPA.itemSelectEvent.subscribe(itemSelectHandlerPA);
        }
        var itemSelectHandlerPA = function(sType2, aArgs2) {

            YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
            var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
            var city = oMyAcInstance2[1].split(',');
            document.getElementById('ctl00_cphTransaction_txtPreferredAirline').value = city[0];
            document.getElementById('<%=airlineCode.ClientID %>').value = city[0];
            document.getElementById('<%=airlineName.ClientID %>').value = city[1];
            document.getElementById('statescontainer4').style.display = "none";
            var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
            var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource 
        };
        YAHOO.util.Event.addListener(this, 'load', autoCompInitPrefAirline); //temporarily commented

          ////////////////////////////////////////////////////////////////////////////////////////////////////////////////

       function autoCompInitPrefAirline1() {
    if (!$('statescontainer5')) {
        return;
    }
    oACDSPA1 = new YAHOO.widget.DS_JSFunction(getAirlines);
    // Instantiate first auto complete
    oAutoCompPA1 = new YAHOO.widget.AutoComplete('txtPreferredAirline0', 'statescontainer5', oACDSPA1);
    oAutoCompPA1.prehighlightClassName = "yui-ac-prehighlight";
    oAutoCompPA1.queryDelay = 0;
    oAutoCompPA1.minQueryLength = 2;
    oAutoCompPA1.useIFrame = true;
    oAutoCompPA1.useShadow = true;

    oAutoCompPA1.formatResult = function(oResultItem, sQuery) {
        document.getElementById('statescontainer5').style.display = "block";
        var toShow = oResultItem[1].split(',');
        var sMarkup = toShow[0] + ',' + toShow[1]; ;
        //var aMarkup = ["<li>", sMarkup, "</li>"]; 
        var aMarkup = [sMarkup];
        return (aMarkup.join(""));
    };
    oAutoCompPA1.itemSelectEvent.subscribe(itemSelectHandlerPA1);

    oACDSPA2 = new YAHOO.widget.DS_JSFunction(getAirlines);
    // Instantiate second auto complete
    oAutoCompPA2 = new YAHOO.widget.AutoComplete('txtPreferredAirline1', 'statescontainer6', oACDSPA2);
    oAutoCompPA2.prehighlightClassName = "yui-ac-prehighlight";
    oAutoCompPA2.queryDelay = 0;
    oAutoCompPA2.minQueryLength = 2;
    oAutoCompPA2.useIFrame = true;
    oAutoCompPA2.useShadow = true;

    oAutoCompPA2.formatResult = function(oResultItem, sQuery) {
        document.getElementById('statescontainer6').style.display = "block";
        var toShow = oResultItem[1].split(',');
        var sMarkup = toShow[0] + ',' + toShow[1]; ;
        //var aMarkup = ["<li>", sMarkup, "</li>"]; 
        var aMarkup = [sMarkup];
        return (aMarkup.join(""));
    };
    oAutoCompPA2.itemSelectEvent.subscribe(itemSelectHandlerPA2);

    oACDSPA3 = new YAHOO.widget.DS_JSFunction(getAirlines);
    // Instantiate third auto complete
    oAutoCompPA3 = new YAHOO.widget.AutoComplete('txtPreferredAirline2', 'statescontainer7', oACDSPA3);
    oAutoCompPA3.prehighlightClassName = "yui-ac-prehighlight";
    oAutoCompPA3.queryDelay = 0;
    oAutoCompPA3.minQueryLength = 2;
    oAutoCompPA3.useIFrame = true;
    oAutoCompPA3.useShadow = true;

    oAutoCompPA3.formatResult = function(oResultItem, sQuery) {
        document.getElementById('statescontainer7').style.display = "block";
        var toShow = oResultItem[1].split(',');
        var sMarkup = toShow[0] + ',' + toShow[1]; ;
        //var aMarkup = ["<li>", sMarkup, "</li>"]; 
        var aMarkup = [sMarkup];
        return (aMarkup.join(""));
    };
    oAutoCompPA3.itemSelectEvent.subscribe(itemSelectHandlerPA3);
}
var itemSelectHandlerPA1 = function(sType2, aArgs2) {

    YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
    var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
    var city = oMyAcInstance2[1].split(',');
     document.getElementById('txtPreferredAirline0').value = city[0];
    document.getElementById('ctl00_cphTransaction_airlineCode').value += "," + city[0];
    document.getElementById('ctl00_cphTransaction_airlineName').value += "," + city[1];
    document.getElementById('statescontainer5').style.display = "none";
    var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
    var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource 
};

var itemSelectHandlerPA2 = function(sType2, aArgs2) {
    YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
    var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
    var city = oMyAcInstance2[1].split(',');
     document.getElementById('txtPreferredAirline1').value = city[0];
    document.getElementById('ctl00_cphTransaction_airlineCode').value += "," + city[0];
    document.getElementById('ctl00_cphTransaction_airlineName').value += "," + city[1];
    document.getElementById('statescontainer6').style.display = "none";
    var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
    var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource 
};
var itemSelectHandlerPA3 = function(sType2, aArgs2) {
    YAHOO.log(sType2); //this is a string representing the event; e.g., "itemSelectEvent"         
    var oMyAcInstance2 = aArgs2[2]; // your AutoComplete instance 
    var city = oMyAcInstance2[1].split(',');
     document.getElementById('txtPreferredAirline2').value = city[0];
    document.getElementById('ctl00_cphTransaction_airlineCode').value += "," + city[0];
    document.getElementById('ctl00_cphTransaction_airlineName').value += "," + city[1];
    document.getElementById('statescontainer7').style.display = "none";
    var elListItem2 = aArgs2[1]; //the <li> element selected in the suggestion container 
    var aData2 = aArgs2[2]; //array of the data for the item as returned by the DataSource
};
YAHOO.util.Event.addListener(this, 'load', autoCompInitPrefAirline1); //temporarily commented

        /////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        var arrayStates = new Array();
        function invokePage(url, passData) {
            if (window.XMLHttpRequest) {
                AJAX = new XMLHttpRequest();
            }
            else {
                AJAX = new ActiveXObject("Microsoft.XMLHTTP");
            }
            if (AJAX) {
                AJAX.open("POST", url, false);
                AJAX.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                AJAX.send(passData);
                return AJAX.responseText;
            }
            else {
                return false;
            }
        }
        function getAirlines(sQuery) {

            var paramList = 'searchKey=' + sQuery;
            paramList += '&requestSource=' + "PreferredAirline";
            var url = "CityAjax";
            var arrayStates = "";
            var faltoo = invokePage(url, paramList);
            arrayStates = faltoo.split('/');
            if (arrayStates[0] != "") {
                for (var i = 0; i < arrayStates.length; i++) {
                    arrayStates[i] = [arrayStates[i].split(',')[1], arrayStates[i]];
                }

                return arrayStates;
            }
            else return (false);
        }
        function FlightSearch() { 

            document.getElementById('fcontainer1').style.display = document.getElementById('fcontainer2').style.display = 'none';

            if (CheckCorporateEntries() && CheckFlightCities() && checkFlightDates()) {
                if (eval(document.getElementById('<%=ddlAdults.ClientID %>').value) < eval(document.getElementById('<%=ddlInfants.ClientID %>').value)) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Infant count should not be greater than adult count";
                    return false;
                } else if (document.getElementById('divPrefAirline0').style.display == 'block' && document.getElementById('ctl00_cphTransaction_txtPreferredAirline').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter your first preferred airline";
                    document.getElementById('ctl00_cphTransaction_txtPreferredAirline').focus();
                    return false;
                } else if (document.getElementById('divPrefAirline0').style.display == 'block' && document.getElementById('txtPreferredAirline0').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter your second preferred airline";
                    document.getElementById('txtPreferredAirline0').focus();
                    return false;
                } else if (document.getElementById('divPrefAirline1').style.display == 'block' && document.getElementById('txtPreferredAirline1').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter your third preferred airline";
                    document.getElementById('txtPreferredAirline1').focus();
                    return false;
                } else if (document.getElementById('divPrefAirline2').style.display == 'block' && document.getElementById('txtPreferredAirline2').value.length == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please enter your fourth preferred airline";
                    document.getElementById('txtPreferredAirline2').focus();
                    return false;
                }
               else if (document.getElementById('<%=radioAgent.ClientID %>') != null && document.getElementById('<%=radioAgent.ClientID %>').checked == true && document.getElementById('<%=ddlAirAgentsLocations.ClientID %>') != null && (document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value.length == 0 || document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value == "-1"))
                {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Location !";
                    return false;
                }
                else if (document.getElementById('<%=ddlFlightTravelReasons.ClientID %>') != null) {
                    var travelReason = document.getElementById('<%=ddlFlightTravelReasons.ClientID %>');

                    if (travelReason.options[travelReason.selectedIndex].text == "Entitlement" && document.getElementById('ctl00_cphTransaction_DepDate').value.length > 0) {
                        var depVal = document.getElementById('ctl00_cphTransaction_DepDate').value;
                        var departure = depVal.split('/');
                        var depDate = new Date(departure[2], eval(departure[1]) - 1, departure[0]);
                        var today = new Date();
                        var validDate = new Date(today.getFullYear(), today.getMonth(), today.getDate() + 30);
                        if (validDate > depDate) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Please select Travel Date after 30 days from current date";
                            document.getElementById('ctl00_cphTransaction_DepDate').focus();
                            return false;
                        }
                    }
                }
                else if (!ValidateAirline(0)) {
                    return false;
                }
                else if (!ValidateAirline(1)) {
                    return false;
                }
                else if (!ValidateAirline(2)) {
                    return false;
                }
                if (flightSourceValidate()) {
                    document.getElementById('<%=hdnModifySearch.ClientID %>').value = "1"; //Modify Search purpose
                    document.getElementById('FlightPreLoader').style.display = "block";
                    document.getElementById('MainDiv').style.display = "none";
                    document.getElementById('divPrg').style.display = "none";

                    if (document.getElementById('<%=radioAgent.ClientID %>') != null && document.getElementById('<%=radioAgent.ClientID %>').checked == true
                        && document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value != "-1") {
                        document.getElementById('<%=hdnAgentLocation.ClientID %>').value = document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value;
                    }
                    //detecting all IE vesions of browsers
                    if (navigator.userAgent.indexOf('MSIE') !== -1 || navigator.appVersion.indexOf('Trident/') > 0) {
                        document.getElementById('imgFlightLoading').innerHTML = document.getElementById('imgFlightLoading').innerHTML;
                    }
                    $('SearchResultLoad').context.styleSheets[0].display = "block";
                    document.getElementById('<%=hdnSubmit.ClientID %>').value = "searchFlight"
                    return true;
                }
                else {
                    return false;
                }
            }
            else {
                return false;
            }
        }

        function CheckCorporateEntries() {
            if ($('#CorpDiv').is(':visible')) {
                if (document.getElementById('<%=ddlFlightTravelReasons.ClientID %>').value == "-1" && document.getElementById('<%=hdnTravelReason.ClientID %>').value == "") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select Reason for Travel";
                    document.getElementById('<%=ddlFlightTravelReasons.ClientID %>').focus();
                    return false;
                }
                else if (document.getElementById('<%=ddlFlightEmployee.ClientID %>').value == "Select" && document.getElementById('<%=hdnFlightEmployee.ClientID %>').value == "") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select Traveller / Employee";
                    document.getElementById('<%=ddlFlightEmployee.ClientID %>').focus();
                    return false;
                }
                else {
                    document.getElementById('errorMessage').innerHTML = "";
                    document.getElementById('errMess').style.display = "none";
                    return true;
                }
            }
            else {
                return true;
            }
        }
        
        function CheckFlightCities() {

            if (document.getElementById('<%=hdnWayType.ClientID %>').value != "multiway") {
                var origin = document.getElementById('<%=Origin.ClientID %>').value;
                var dest = document.getElementById('<%=Destination.ClientID %>').value;

                if (origin.length <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter a Origin City";
                    return false;
                }
                else if (dest.length <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter a Destination City";
                    return false;
                }
                else {
                    document.getElementById('errMess').style.display = "none";
                    var arrOrigin = origin.split(',')[0].split(')')[1];
                    var arrDest = dest.split(',')[0].split(')')[1];
                    document.getElementById('flightOnwards').innerHTML = origin.substring(1, 4) + ' ' + '(' + arrOrigin + ')' + ' - ' + dest.substring(1, 4) + ' ' + '(' + arrDest + ')';
                    document.getElementById('flightReturn').innerHTML = dest.substring(1, 4) + ' ' + '(' + arrDest + ')' + ' - ' + origin.substring(1, 4) + ' ' + '(' + arrOrigin + ')';
                    if (document.getElementById('<%=hdnWayType.ClientID %>').value == "oneway") {
                        document.getElementById('twowaytable').style.display = 'none';
                    }
                    //                    if (window.navigator.appName == "Netscape") {
                    //                        setTimeout('document.images.imgPreloader.src = "images/preloader11.gif"', 200); 
                    //                    }
                    return true;
                }
            }
            else {
                var city1 = document.getElementById('<%=City1.ClientID%>').value;
                var city2 = document.getElementById('<%=City2.ClientID%>').value;
                var city3 = document.getElementById('<%=City3.ClientID%>').value;
                var city4 = document.getElementById('<%=City4.ClientID%>').value;
                var city5 = document.getElementById('<%=City5.ClientID%>').value;
                var city6 = document.getElementById('<%=City6.ClientID%>').value;
                var city7 = document.getElementById('<%=City7.ClientID%>').value;
                var city8 = document.getElementById('<%=City8.ClientID%>').value;
                var city9 = document.getElementById('<%=City9.ClientID%>').value;
                var city10 = document.getElementById('<%=City10.ClientID%>').value;
                var city11 = document.getElementById('<%=City11.ClientID%>').value;
                var city12 = document.getElementById('<%=City12.ClientID%>').value;

                if (city1.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter first City";
                    document.getElementById('<%=City1.ClientID%>').focus();
                    return false;
                } else if (city2.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter second City";
                    document.getElementById('<%=City2.ClientID%>').focus();
                    return false;
                } else if (city3.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter third City";
                    document.getElementById('<%=City3.ClientID%>').focus();
                    return false;
                } else if (city4.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter fourth City";
                    document.getElementById('<%=City4.ClientID%>').focus();
                    return false;
                } else if (tblRow3.style.display == 'block' && city5.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 5th City";

                    document.getElementById('<%=City5.ClientID%>').focus();
                    return false;
                } else if (tblRow3.style.display == 'block' && city6.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 6th City";

                    document.getElementById('<%=City6.ClientID%>').focus();
                    return false;
                } else if (tblRow4.style.display == 'block' && city7.length <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 7th City";

                    document.getElementById('<%=City7.ClientID%>').focus();
                    return false;
                } else if (tblRow4.style.display == 'block' && city8.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 8th City";

                    document.getElementById('<%=City8.ClientID%>').focus();
                    return false;
                } else if (tblRow5.style.display == 'block' && city9.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 9th City";

                    document.getElementById('<%=City9.ClientID%>').focus();
                    return false;
                } else if (tblRow5.style.display == 'block' && city10.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 10th City";

                    document.getElementById('<%=City10.ClientID%>').focus();
                    return false;
                } else if (tblRow6.style.display == 'block' && city11.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 11th City";

                    document.getElementById('<%=City11.ClientID%>').focus();
                    return false;
                } else if (tblRow6.style.display == 'block' && city12.length <= 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Enter 12th City";

                    document.getElementById('<%=City12.ClientID%>').focus();
                    return false;
                }
                else {
                    document.getElementById('errMulti').style.display = "none";

                    var success = true;
                    document.getElementById('onetwoDiv').style.display = 'none';
                    document.getElementById('multiDiv').style.display = 'block';
                    var arrCity1 = city1.split(',')[0].split(')')[1];
                    var arrCity2 = city2.split(',')[0].split(')')[1];
                    var arrCity3 = city3.split(',')[0].split(')')[1];
                    var arrCity4 = city4.split(',')[0].split(')')[1];
                    document.getElementById('multiway1').innerHTML = city1.substring(1, 4) + ' ' + '(' + arrCity1 + ')' + ' - ' + city2.substring(1, 4) + ' ' + '(' + arrCity2 + ')';
                    document.getElementById('multiway2').innerHTML = city3.substring(1, 4) + ' ' + '(' + arrCity3 + ')' + ' - ' + city4.substring(1, 4) + ' ' + '(' + arrCity4 + ')';
                    var tdIndex = 3;
                    for (var i = 5; i < 13; i++) {
                        if (document.getElementById('tblRow' + tdIndex).style.display == "block") {
                            var addlCity1 = document.getElementById('ctl00_cphTransaction_City' + i).value;
                            var addlCity2 = document.getElementById('ctl00_cphTransaction_City' + (i + 1)).value;
                            var arraddlCity1 = addlCity1.split(',')[0].split(')')[1];
                            var arraddlCity2 = addlCity2.split(',')[0].split(')')[1];

                            if (addlCity1.length <= 0 || addlCity2.length <= 0) {
                                document.getElementById('errMess').style.display = "block";
                                document.getElementById('errorMessage').innerHTML = "Please Enter all Cities";
                                success = false;
                                break;
                            }
                            document.getElementById('multiwayable' + tdIndex).style.display = "block";
                            document.getElementById('multiway' + tdIndex).innerHTML = addlCity1.substring(1, 4) + ' ' + '(' + arraddlCity1 + ')' + ' - ' + addlCity2.substring(1, 4) + ' ' + '(' + arraddlCity2 + ')';

                            tdIndex++;
                        }
                        i++;
                    }
                    //                    if (window.navigator.appName == "Netscape") {
                    //                        setTimeout('document.images.imgPreloader.src = "images/preloader11.gif"', 200);
                    //                    }

                    document.getElementById('imgPreloader').src = "images/preloaderFlight.gif";
                    return true;

                }
            }
        }
        function checkFlightDates() {

            if (document.getElementById('<%=hdnWayType.ClientID %>').value != "multiway") {
                var date1 = document.getElementById('<%=DepDate.ClientID %>').value;
                var date2 = document.getElementById('<%=ReturnDateTxt.ClientID %>').value;
                this.today = new Date();
                var thisMonth = this.today.getMonth();
                var thisDay = this.today.getDate();
                var thisYear = this.today.getFullYear();
                document.getElementById('deptDate').innerHTML = date1;
                document.getElementById('arrivalDate').innerHTML = date2;
                var todaydate = new Date(thisYear, thisMonth, thisDay);
                if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Departure Date";
                    return false;
                }
                var depDateArray = date1.split('/');

                // checking if date1 is valid		    
                if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                    return false;
                }
                var cInDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                // Removing same day validation
//                if (todaydate.getTime() > cInDate.getTime()) {
//                    document.getElementById('errMess').style.display = "block";
//                    document.getElementById('errorMessage').innerHTML = " Departure Date should be greater than equal to todays date";
//                    return false;
//                }
                if (document.getElementById('<%=roundtrip.ClientID%>').checked == true) {
                    if (date2 != null && (date2 == "DD/MM/YYYY" || date2 == "")) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Please Select Return Date";
                        return false;
                    }

                    var retDateArray = date2.split('/');

                    // checking if date2 is valid	
                    if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = " Invalid Return Date";
                        return false;
                    }
                    var cOutDate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                    if (todaydate.getTime() > cOutDate.getTime()) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Return Date should be greater than equal to todays date";
                        return false;
                    }

                    var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                    var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                    var difference = returndate.getTime() - depdate.getTime();

                    if (difference < 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Departure date should be greater than or equal to Return date";
                        return false;
                    }
                }
                return true;
            }
            else if (document.getElementById('<%=hdnWayType.ClientID %>').value == "multiway") {

                var date1 = document.getElementById('ctl00_cphTransaction_Time1').value;
                var date2 = document.getElementById('ctl00_cphTransaction_Time2').value;
                var date3 = document.getElementById('ctl00_cphTransaction_Time3').value;
                var date4 = document.getElementById('ctl00_cphTransaction_Time4').value;
                var date5 = document.getElementById('ctl00_cphTransaction_Time5').value;
                var date6 = document.getElementById('ctl00_cphTransaction_Time6').value;

                if (date1.length == 0 || date2.length == 0) {
                    document.getElementById('errMulti').style.display = "block";
                    document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                    if (date1.length == 0) {
                        document.getElementById('ctl00_cphTransaction_Time1').focus();
                    }
                    else {
                        document.getElementById('ctl00_cphTransaction_Time2').focus();
                    }
                    return false;
                }
                else if (date1.length > 0 && date2.length > 0) {
                    this.today = new Date();
                    var thisMonth = this.today.getMonth();
                    var thisDay = this.today.getDate();
                    var thisYear = this.today.getFullYear();

                    var todaydate = new Date(thisYear, thisMonth, thisDay);
                    if (date1 != null && (date1 == "DD/MM/YYYY" || date1 == "")) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                        document.getElementById('ctl00_cphTransaction_Time1').focus();
                        return false;
                    }
                    var depDateArray = date1.split('/');

                    // checking if date1 is valid		    
                    if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                        document.getElementById('ctl00_cphTransaction_Time1').focus();
                        return false;
                    }
                    var cInDate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                    if (todaydate.getTime() > cInDate.getTime()) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = " Departure Date should be greater than equal to todays date";
                        return false;
                    }

                    if (date2 != null && (date2 == "DD/MM/YYYY" || date2 == "")) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Return Date";
                        document.getElementById('ctl00_cphTransaction_Time2').focus();
                        return false;
                    }

                    var retDateArray = date2.split('/');

                    // checking if date2 is valid	
                    if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                        document.getElementById('ctl00_cphTransaction_Time2').focus();
                        return false;
                    }
                    var cOutDate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                    if (todaydate.getTime() > cOutDate.getTime()) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Return Date should be greater than equal to todays date";
                        return false;
                    }

                    var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                    var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
                    var difference = returndate.getTime() - depdate.getTime();

                    if (difference < 0) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Departure date should be greater than or equal to Return date";
                        return false;
                    }

                    if ((tblRow3.style.display == 'block' && date3.length == 0)) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                        document.getElementById('ctl00_cphTransaction_Time3').focus();
                        return false;
                    }
                    else if ((tblRow4.style.display == 'block' && date4.length == 0)) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";

                        document.getElementById('ctl00_cphTransaction_Time4').focus();
                        return false;
                    }
                    else if ((tblRow5.style.display == 'block' && date5.length == 0)) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";

                        document.getElementById('ctl00_cphTransaction_Time5').focus();
                        return false;
                    }
                    else if ((tblRow6.style.display == 'block' && date6.length == 0)) {
                        document.getElementById('errMulti').style.display = "block";
                        document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";

                        document.getElementById('ctl00_cphTransaction_Time6').focus();
                        return false;
                    }
                    else if ((tblRow3.style.display == 'block' && date3.length > 0)) {
                        this.today = new Date();
                        var thisMonth = this.today.getMonth();
                        var thisDay = this.today.getDate();
                        var thisYear = this.today.getFullYear();

                        var todaydate = new Date(thisYear, thisMonth, thisDay);
                        if (date3 != null && (date3 == "DD/MM/YYYY" || date3 == "")) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time3').focus();
                            return false;
                        }
                        var depDateArray = date3.split('/');

                        // checking if date1 is valid		    
                        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time3').focus();
                            return false;
                        }
                        //                       
                    }

                    else if ((tblRow4.style.display == 'block' && date4.length > 0)) {
                        if (date4 != null && (date4 == "DD/MM/YYYY" || date4 == "")) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time4').focus();
                            return false;
                        }
                        var retDateArray = date4.split('/');

                        // checking if date2 is valid	
                        if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time4').focus();
                            return false;
                        }

                    }

                    else if ((tblRow5.style.display == 'block' && date5.length > 0)) {
                        this.today = new Date();
                        var thisMonth = this.today.getMonth();
                        var thisDay = this.today.getDate();
                        var thisYear = this.today.getFullYear();

                        var todaydate = new Date(thisYear, thisMonth, thisDay);
                        if (date5 != null && (date5 == "DD/MM/YYYY" || date5 == "")) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time5').focus();
                            return false;
                        }
                        var depDateArray = date5.split('/');

                        // checking if date1 is valid		    
                        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time5').focus();
                            return false;
                        }

                    }

                    else if ((tblRow6.style.display == 'block' && date6.length > 0)) {
                        if (date6 != null && (date6 == "DD/MM/YYYY" || date6 == "")) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = "Please Select Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time6').focus();
                            return false;
                        }
                        var retDateArray = date6.split('/');

                        // checking if date2 is valid	
                        if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                            document.getElementById('errMulti').style.display = "block";
                            document.getElementById('errorMulti').innerHTML = " Invalid Departure Date";
                            document.getElementById('ctl00_cphTransaction_Time6').focus();
                            return false;
                        }

                    }

                    document.getElementById('multiDate1').innerHTML = date1;
                    document.getElementById('multiDate2').innerHTML = date2;
                    for (var k = 3; k <= 6; k++) {
                        if (document.getElementById('tblRow' + k).style.display == "block") {
                            document.getElementById('multiDate' + k).innerHTML = document.getElementById('ctl00_cphTransaction_Time' + k).value;
                        }
                    }
                }
                return true;
            }
        }
        function flightSourceValidate() {
            var isValid = true;
            var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
            if (chkSupp != null) {
                var checkbox = chkSupp.getElementsByTagName("input");
                var counter = 0;
                for (var i = 0; i < checkbox.length; i++) {
                    if (checkbox[i].checked) {
                        counter++;
                    }
                }
                if (counter == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select atleast 1 Supplier";
                    isValid = false;
                }
            }
            //Go Air: Allows only 6 Passengers to book[Adult+Child+Infant]
            //Go Air : Restrict the search if there are more than 6 passengers.
            if (isValid) {
                var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
                var passerngerCount = eval(document.getElementById('<%=ddlAdults.ClientID %>').value) + eval(document.getElementById('<%=ddlChilds.ClientID %>').value) + eval(document.getElementById('<%=ddlInfants.ClientID %>').value);
                if (chkSupp != null) {
                    var checkbox = chkSupp.getElementsByTagName("input");
                    for (var i = 0; i < checkbox.length; i++) {
                        if (checkbox[i].checked) {
                            if (document.getElementById('ctl00_cphTransaction_chkSuppliers_' + i).nextSibling.innerHTML == "G8") {
                                if (passerngerCount > 6) {
                                    document.getElementById('errMess').style.display = "block";
                                    document.getElementById('errorMessage').innerHTML = "GoAir Doesn't allow morethan 6 passengers";
                                    isValid = false;
                                }
                           }
                        }
                    }

                }
            }

            return isValid;
        }
        function setCheck(ctrl) {
            if (document.getElementById(ctrl).checked == false) {
                document.getElementById('ctl00_cphTransaction_chkSource0').checked = false;
            }
        }
        function setUncheck() {
            var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
            if (chkSupp != null) {
                var checkbox = chkSupp.getElementsByTagName("input");
                for (var i = 0; i < checkbox.length; i++) {
                    if (!checkbox[i].checked) {
                        document.getElementById("<%=chkFlightAll.ClientID%>").checked = false;
                    }
                }
            }

        }
    </script>

    <script language="javascript">
        var cntr = 0;
        var rid = 3;

        //add a new row to the table
        function addRow() {
            document.getElementById('tblRow' + rid).style.display = 'block';
            if (rid < 6) {
                rid = rid + 1;
            }

            cntr = cntr + 1;
            //alert(cntr);
            if (cntr == 4) {
                document.getElementById('btnAddRow').style.display = 'none';
            }

        }

        //deletes the specified row from the table
        function removeRow(src) {

            document.getElementById('tblRow' + src).style.display = 'none';
            if (src == 3) {
                document.getElementById('ctl00_cphTransaction_Time3').value = "";
                document.getElementById('ctl00_cphTransaction_City5').value = "";
                document.getElementById('ctl00_cphTransaction_City6').value = "";
            }
            else if (src == 4) {
                document.getElementById('ctl00_cphTransaction_Time4').value = "";
                document.getElementById('ctl00_cphTransaction_City7').value = "";
                document.getElementById('ctl00_cphTransaction_City8').value = "";
            }
            else if (src == 5) {
                document.getElementById('ctl00_cphTransaction_Time5').value = "";
                document.getElementById('ctl00_cphTransaction_City9').value = "";
                document.getElementById('ctl00_cphTransaction_City10').value = "";
            }
            else {
                document.getElementById('ctl00_cphTransaction_Time6').value = "";
                document.getElementById('ctl00_cphTransaction_City11').value = "";
                document.getElementById('ctl00_cphTransaction_City12').value = "";
            }
            rid = src;
            cntr = cntr - 1;
            if (cntr < 4)
                document.getElementById('btnAddRow').style.display = 'block';



        }
        function selectFlightSources() {
            var chkSupp = document.getElementById("<%=chkSuppliers.ClientID%>");
            if (chkSupp != null) {
                var checkbox = chkSupp.getElementsByTagName("input");
                if (document.getElementById('<%=chkFlightAll.ClientID %>') != null) {
                    if (document.getElementById('<%=chkFlightAll.ClientID %>').checked) {
                        for (i = 0; i < checkbox.length; i++) {
                            document.getElementById('ctl00_cphTransaction_chkSuppliers_' + i).checked = true;
                        }
                    }
                    else {
                        for (i = 0; i < checkbox.length; i++) {
                            document.getElementById('ctl00_cphTransaction_chkSuppliers_' + i).checked = false;
                        }
                    }
                }
            }
        }

        //Adding prefered Airline Added by brahmam 14.10.2016
        function AddPrefAirline() {

            if (document.getElementById('divPrefAirline0').style.display == "none") {
                document.getElementById('divPreferredAirline0').style.display = 'block';
                document.getElementById('divPrefAirline0').style.display = 'block';
                document.getElementById('removeRowLink0').style.display = 'block';
                document.getElementById('txtPreferredAirline0').value = '';
            } else if (document.getElementById('divPrefAirline1').style.display == "none") {
                document.getElementById('divPreferredAirline1').style.display = 'block';
                document.getElementById('divPrefAirline1').style.display = 'block';
                document.getElementById('removeRowLink1').style.display = 'block';
                document.getElementById('txtPreferredAirline1').value = '';
            }
            else if (document.getElementById('divPrefAirline2').style.display == "none") {
                document.getElementById('divPreferredAirline2').style.display = 'block';
                document.getElementById('divPrefAirline2').style.display = 'block';
                document.getElementById('removeRowLink2').style.display = 'block';
                document.getElementById('txtPreferredAirline2').value = '';
            }
            if (document.getElementById('divPrefAirline0').style.display == "block" && document.getElementById('divPrefAirline1').style.display == "block" && document.getElementById('divPrefAirline2').style.display == "block") {
                document.getElementById('addRowLink').style.display = 'none';
            }

        }
        //removing prefered Airline Added by brahmam 14.10.2016
        function RemovePrefAirline(index) {
            document.getElementById('divPreferredAirline' + index).style.display = 'none';
            document.getElementById('divPrefAirline' + index).style.display = 'none';
            document.getElementById('removeRowLink' + index).style.display = 'none';
            document.getElementById('addRowLink').style.display = 'block';
            //$('#divPreferredAirline' + index).find('input').val('');
            $('#txtPreferredAirline' + index).val('');
           <%-- var airlineCodes = document.getElementById('<%=airlineCode.ClientID %>').value.split(',');
            airlineCodes.splice((index + 1), 1);
            var arCodes = '';
            for (var i = 0; i < airlineCodes.length; i++) {
                if (arCodes == '') {
                    arCodes = airlineCodes[i];
                }
                else {
                    arCodes = arCodes + ',' + airlineCodes[i];
                }
            }
            document.getElementById('<%=airlineCode.ClientID %>').value = arCodes;--%>
        }

        //To Validate Duplicate Preferred Airline
        function ValidateAirline(id) {
            var isValid = true;
            var prefAir1 = document.getElementById('txtPreferredAirline' + id).value;
            if (document.getElementById('ctl00_cphTransaction_txtPreferredAirline').value.trim().length > 0 && prefAir1.trim().length > 0) {
                if (document.getElementById('ctl00_cphTransaction_txtPreferredAirline').value == prefAir1) {
                    errMess.style.display = 'block';
                    errorMessage.innerHTML = 'Duplicate Preferred Airline not allowed';
                    document.getElementById('txtPreferredAirline' + id).focus();
                    isValid = false;
                }
                else if (document.getElementById('txtPreferredAirline' + eval(id - 1)) != null && document.getElementById('txtPreferredAirline' + eval(id - 1)).value == prefAir1) {
                    errMess.style.display = 'block';
                    errorMessage.innerHTML = 'Duplicate Preferred Airline not allowed';
                    document.getElementById('txtPreferredAirline' + id).focus();
                    isValid = false;
                }
            }
            return isValid;
        }
        //To Remove input preferred airline code from airlineCode hidden field
        //when preferred airline cleared from textbox
        function RecheckAirline(index) {



            // commented by bangar
            <%--if (index == 0 && document.getElementById('<%=txtPreferredAirline.ClientID %>').value != '')
                return;

            if (index > 0 && document.getElementById('txtPreferredAirline' + (index - 1)) != null && document.getElementById('txtPreferredAirline' + (index - 1)).value != '')
                return;--%>

            var airlineCodes = document.getElementById('<%=airlineCode.ClientID %>').value.split(',');
            airlineCodes.splice((index), 1);
            var arCodes = '';
            for (var i = 0; i < airlineCodes.length; i++) {
                if (arCodes == '') {
                    arCodes = airlineCodes[i];
                }
                else {
                    arCodes = arCodes + ',' + airlineCodes[i];
                }
            }
            document.getElementById('<%=airlineCode.ClientID %>').value = arCodes;
        }
    </script>
    <asp:HiddenField ID="hdnAgentLocation" runat="server" />
    <asp:HiddenField ID="hdnPriceMax" runat="server" Value="0" />
    <asp:HiddenField ID="hdnPriceMin" runat="server" Value="0" />
    <asp:HiddenField ID="hdnMinValue" runat="server" Value="0" />
    <asp:HiddenField ID="hdnMaxValue" runat="server" Value="0" />
    <asp:HiddenField ID="hdnFilter" runat="server" />
    <asp:HiddenField ID="hdnCheckboxEmail" runat="server"></asp:HiddenField>
    <asp:HiddenField ID="PageNoString" runat="server" />
    <asp:HiddenField ID="Change" runat="server" />
    <asp:HiddenField ID="hdnOnTimingFilter" runat="server" Value="Any Time" />
    <asp:HiddenField ID="hdnRetTimingFilter" runat="server" Value="Any Time" />
    <asp:HiddenField ID="hdnFlightEmployee" runat="server" />
    <asp:HiddenField ID="hdnTravelReason" runat="server" />

    <div id="MainDiv">
    
    
    
    

                         
    <div id="mcontainer1" style="position: absolute; top: 260px; left: 54%; display: none; z-index:9999"> </div>

               
      <div id="mcontainer2" style="position: absolute; top: 270px; left: 54%; display: none; z-index:9999"> </div>
 
               
       <div id="mcontainer3" style="position: absolute; top: 280px; left: 54%; display: none; z-index:9999"> </div>
           
      
       <div id="mcontainer4" style="position: absolute; top: 290px; left: 54%; display: none; z-index:9999"> </div>

               
       <div id="mcontainer5" style="position: absolute; top: 300px; left: 54%; display: none; z-index:9999"> </div>
 
                
      <div id="mcontainer6" style="position: absolute; top: 310px; left: 54%; display: none; z-index:9999"> </div>
                   
              
 
    <div class="modal fade in farerule-modal-style" data-backdrop="static" id="FareRuleBlock" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
            <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close mt-0" style="color:#fff;opacity:.8;" onclick="FareRuleHide()"><span aria-hidden="true">×</span></button>
                        <h4 class="modal-title" id="FareRuleHead"><span id="FareRuleHeadTitle"></span></h4>
                    </div>
                    <div class="modal-body">                                          
                    <div class="bg_white" id="FareRuleBody" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;"></div>
                    </div>
            </div>
            </div>
        </div>        
                
                    
                    
<%--        <div style="padding-top: 10px; position: relative" >
            <div class="fare_rule_block" id="FareRuleBlock" style="z-index: 9999;">
                <div id="FareRuleHead">
                    <div class="showMsgHeading">
                        <label id="FareRuleHeadTitle">
                        </label>
                        <a class="closex cursor_point" onclick="FareRuleHide();" style="position: absolute;
                            right: 5px; top: 3px;">X</a>
                    </div>
                </div>
                <div class="body" id="FareRuleBody" style="height: 350px; overflow-y: scroll; width: 100%;">
                </div>
            </div>
            <div class="clear">
            </div>
        </div>--%>
         <div class="error_module">
             <asp:Label ID="lblMessage" runat="server" Text=""></asp:Label>
                    </div>
        <div class=" bg_white bor_gray padding-5 mb-0 row">
        
        
        <div class="col-md-1 d-none d-lg-block">
        <table> 
        
        <tr style=""> 
        
        <td> <strong> Search summary</strong>  </td>
        
        <td><asp:Image ID="Image11" runat="server" ImageUrl="~/images/vline.jpg" /> </td>
        
        </tr>
        
        
        </table>
        
        
        
        </div>
        
       
            <div class="col-md-9" style="font-size: 13px;">  
                      
                <table width="100%" cellpadding="0" cellspacing="0" class="flight-search-modify">
                    <tr>
                        <td align="center">
                            Type
                        </td>
                        <td rowspan="2">
                            <asp:Image ID="Image7" runat="server" ImageUrl="~/images/vline.jpg" />
                            </td>
                        <%if (request != null && request.Type != CT.BookingEngine.SearchType.MultiWay)
                          { %>
                         
                        <td>
                            From
                        </td>
                        <td>&nbsp;</td>
                        <td>
                            To
                        </td>
                        <%if (request != null && request.Type != CT.BookingEngine.SearchType.MultiWay)
                          { %>
                        <td rowspan="2"><asp:Image ID="Image10" runat="server" ImageUrl="~/images/vline.jpg" /></td>
                        <td>
                            Depart 
                            <asp:Label ID="lblDepDay" runat="server" Text=""></asp:Label>
                        </td>
                        <%if (request != null && request.Type == CT.BookingEngine.SearchType.Return)
                          { %>
                        <td>
                            Return 
                            <asp:Label ID="lblRetDay" runat="server" Text=""></asp:Label>
                        </td>
                        <%}
                          }
                          }
                          else if(request != null)
                          {
                               for (int i = 0; i < request.Segments.Length; i++ )
                              {
                                  FlightSegment seg = request.Segments[i];
                                  if(i==0){%>
                                  <td>&nbsp;</td>
                                  <%} %>
                              <td>&nbsp;</td>
                              <td>&nbsp;</td>
                              
                          <%}
                          } %>
                        <td rowspan="2"><asp:Image ID="Image9" runat="server" ImageUrl="~/images/vline.jpg" /></td>
                        <td align="center">
                            Class
                        </td>
                        <td rowspan="2"><asp:Image ID="Image8" runat="server" ImageUrl="~/images/vline.jpg" /></td>
                        <td align="center">
                            Travellers
                        </td>
                       
                       
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblTrip" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </td>
                        <%if (request != null && request.Type == CT.BookingEngine.SearchType.MultiWay)
                          {
                              for (int i = 0; i < request.Segments.Length; i++ )
                              {
                                  FlightSegment seg = request.Segments[i];
                                  if (i == 0)
                                  {%>
                              <td>
                            <strong>
                                <%=seg.Origin%>
                            </strong>
                        </td>
                        <%} %>
                        <td rowspan="2">                           
                            <asp:Image ID="Image12" runat="server" ImageUrl="~/images/OnewayArrow.png" />                           
                        </td>                        
                        <td>
                            <strong>
                                <%=seg.Destination%>
                                </strong>
                        </td>
                        
                              <%}
                          }
                          else
                          { %>
                        <td>
                            <strong>
                                <asp:Label ID="lblOrigin" runat="server" Text=""></asp:Label>
                            </strong>
                        </td>
                        <td rowspan="2">
                            <%if (request != null && request.Type == CT.BookingEngine.SearchType.Return)
                              { %>
                            <asp:Image ID="Image5" runat="server" ImageUrl="~/images/ReturnArrows.png" />
                            <%}
                              else
                              { %>
                            <asp:Image ID="Image6" runat="server" ImageUrl="~/images/OnewayArrow.png" />
                            <%} %>
                        </td>
                        
                        <td>
                            <strong>
                                <asp:Label ID="lblDestination" runat="server" Text=""></asp:Label>
                                </strong>
                        </td>
                        <td>
                            
                            <a href="Javascript:DaySelection('-')" class="btn btn-primary btn-sm p-0 px-2 prev-day modify-day" style="
    font-size: 10px;
"> <span class="icon-navigate_before"></span><span class="d-none d-xl-inline">Previous Day</span> </a>
                            <strong>
                                <asp:Label ID="lblDep" runat="server" Text=""></asp:Label>
                                </strong>
                             <%-- <%if(request.Type == CT.BookingEngine.SearchType.OneWay){ %>
                             <a href="Javascript:DaySelection('+')" class="btn btn-primary btn-sm p-0 px-2 next-day modify-day" style="
    font-size: 10px;
"><span class="d-none d-xl-inline">Next Day</span><span class="icon-navigate_next"></span></a><%} %>--%>
                             <a href="Javascript:DaySelection('+')" class="btn btn-primary btn-sm p-0 px-2 next-day modify-day" style="
    font-size: 10px;"><span class="d-none d-xl-inline">Next Day</span><span class="icon-navigate_next"></span></a>
                        </td>
                        <% if (request != null && request.Type == CT.BookingEngine.SearchType.Return)
                           { %>
                        <td>
                            <strong>
                                <asp:Label ID="lblRet" runat="server" Text=""></asp:Label>
                                </strong>                           
                        </td>
                        <%}
                          } %>
                        <td align="center">
                            <asp:Label ID="lblClass" runat="server" Text="" Font-Bold="true"></asp:Label>
                        </td>
                        <td align="center">
                            <asp:Label ID="lblAdultCount" runat="server" Text=""></asp:Label>
                            <strong>Adult </strong>
                            <asp:Label ID="lblChildCount" runat="server" Text=""></asp:Label>
                            <strong>Child </strong>
                            <asp:Label ID="lblInfantCount" runat="server" Text=""></asp:Label>
                            <strong>Infant </strong>
                        </td>
                       
                    </tr>
                </table>
            </div>
            <div class="col-md-2 text-align-center">
                <%--<asp:LinkButton ID="lnkModifySearch" runat="server" CssClass="fcol_blue" PostBackUrl="~/HotelSearch.aspx?source=Flight">» Modify Search</asp:LinkButton>--%>
                <asp:HiddenField ID="hdnSubmit" runat="server" />
                <asp:HiddenField ID="hdnWayType" runat="server" Value="return" />
                <asp:HiddenField ID="hdnModifySearch" runat="server" Value="0" />
                <asp:HiddenField ID="hdnSourceBinding" runat="server" Value="false" />
             
                  
               
        
                <a style="margin-top:7px" class="but but_b cursor_point float-none float-lg-right" id="ButModify">Modify Search </a>
                <div class="clearfix">
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <!--Modify container starts -->
        <div style="display: none" id="DivModify" class="margin_bot20 bg_white bor_gray marbot_10">
            <div class="search_container">
                <div class="col-md-12">
                    <div class="col-md-12">
                       
                        <h3>  Modify Search </h3>
                       
                    </div>
                    
                    <div id="errMess" class="error_module" style="display: none;">
                        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                        </div>
                    </div>
                   
                   
                    <div>
                        <asp:HiddenField ID="hdnBookingAgent" runat="server" />
                        <%if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
                          { %>
                        <div id="trAgent"  class="custom-radio-table mb-1">
                            <div class="col-md-4 mb-4">
                                

                                <div class="row"> 

                                <div class="col-md-12"> 
                                <asp:RadioButton ID="radioSelf" GroupName="Flight" Checked="true" Text="For Self"
                                    onclick="disablefield();" runat="server" /> 
                                    </div>



                                   <div class="col-md-12"> 
                                    <div id="wrapper1" style="display: none">
                                   <div>  Select Client:</div>
                                    <%--<asp:TextBox ID="txtAirAgents" runat="server" CssClass="form-control" Enabled="false"></asp:TextBox>--%>
                                    
                                    <div> <asp:DropDownList ID="ddlAirAgents" CssClass="form-control" runat="server" onchange="LoadAgentLocations()" Enabled="false">
                                                            </asp:DropDownList></div>

                                </div> 
                                </div>
                                    </div>




                                    </div>



                             <div class="col-md-4 mb-4">

                                 <div class="row"> 

                                <div class="col-md-12"> 

                                 <asp:RadioButton ID="radioAgent" Text="Client" GroupName="Flight" runat="server" onclick="disablefield();" /> 
                                 
                                 </div>


                                <div class="col-md-12"> 
                                 <div id="divLocations" style="display:none">                            
                          
                                    <div id="wrapper3"> Select Location:  </div>                                                   
                                    
                              
                              <div id="wrapper2">
                                        <asp:DropDownList ID="ddlAirAgentsLocations" CssClass="form-control" runat="server">
                                        </asp:DropDownList>
                                    </div>

                               
                                </div>
                                
                                
                                </div>


                            </div>
                           
                            
                          
                          </div>
                           


                          
                        </div>
                        <%} %>
                    </div>




                    <div class="custom-radio-table">                    
                        <div id="CorpDiv" style="display:none">
                         <div class="col-xs-12 col-md-6">
                                  <div class="form-group">
                                      <!--<label>Select Travel Reason</label>-->
                                      <asp:DropDownList ID="ddlFlightTravelReasons" AppendDataBoundItems="true" CssClass="form-control select-element"  runat="server">
                                       <asp:ListItem Selected="True" Value="-1" Text="Select Reason for Travel"></asp:ListItem>
                                      </asp:DropDownList>
                                  </div>                                  
                              </div>
                              <div class="col-xs-12 col-md-6">
                                  <div class="form-group">
                                      <!--<label>Specify Traveller / Employee Number</label>-->
                                      <!--if we select Client in Hotel search page after moving forward and in modify search client is not auto selecting in drop down due to client value -1 so auto selecting "Select Traveller/Employee " if we select client in Hotel search page -->
                                      <asp:DropDownList ID="ddlFlightEmployee" AppendDataBoundItems="true" CssClass="form-control select-element"  runat="server">
                                      <asp:ListItem Selected="True" Value="Select" Text="Select Traveller / Employee"></asp:ListItem>
                                      </asp:DropDownList>
                                  </div>                                  
                              </div>
                            </div>
                              
                                 <div class="clearfix"> </div> 

                        <div class="col-md-4 col-xs-4">
                            <asp:RadioButton ID="oneway" GroupName="radio" onclick="disablefield();" runat="server"
                                Text="One Way" Font-Bold="true" />
                        </div>
                        <div class="col-md-4 col-xs-4">
                            <asp:RadioButton ID="roundtrip" GroupName="radio" onclick="disablefield();" runat="server"
                                Text="Round Trip" Font-Bold="true" />
                        </div>
                        <div class="col-md-4 col-xs-4 hidden-xs">
                            <label style="display: block">
                                <%if(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsCorporate=="N"){ %>
                                <asp:RadioButton ID="multicity" GroupName="radio" onclick="disablefield();" runat="server"
                                    Text="Multi City" Font-Bold="true" />
                                <%} %>
                            </label>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                    <div id="tblNormal">
                        <table border="0" cellspacing="0" cellpadding="0" width="100%">
                            <tr>
                                <td>
                                    <div>
                                        <div class="col-md-6">
                                            <div>
                                                From</div>
                                            <div>
                                                <asp:TextBox ID="Origin" CssClass="form-control" runat="server" onblur="EnableVisaChange();" ></asp:TextBox>
                                                <div class="clear">
                                                    <div style="width: 300px; line-height: 30px; color: #000;" id="statescontainer">
                                                    </div>
                                                </div>
                                                <div id="multipleCity1">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div>
                                                Going To</div>
                                            <div>
                                                <asp:TextBox ID="Destination" CssClass="form-control" runat="server"></asp:TextBox>
                                                <div id="Div1">
                                                    <div style="width: 300px; line-height: 30px; color: #000;" id="statescontainer2">
                                                    </div>
                                                </div>
                                                <div id="multipleCity2">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            <tr><td><div class="col-md-6"><asp:CheckBox ID="chkNearByPort" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></div></td></tr>
                            <tr>
                                <td height="10">
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="col-md-12 padding-0">
                                        <div class="col-md-6 col-xs-12 div1024">


                                              <div style="display:none" id="fcontainer1"> </div>
      
      


                                                       
               

                                            <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td colspan="3">
                                                        Departure
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <asp:TextBox Width="110px" ID="DepDate" data-calendar-contentID="#fcontainer1" CssClass="form-control" runat="server"></asp:TextBox>
                                                    </td>
                                                    <td>
                                                        <a href="javascript:void(null)" onclick="showFlightCalendar1()">
                                                            <img id="Img4" src="images/call-cozmo.png" alt="Pick Date" />
                                                        </a>
                                                    </td>
                                                    <td>
                                                        <asp:DropDownList ID="ddlDepTime" CssClass="form-control" runat="server">
                                                            <asp:ListItem Selected="True" Text="Any Time" Value="Any Time"></asp:ListItem>
                                                            <asp:ListItem Text="Morning" Value="Morning"></asp:ListItem>
                                                            <asp:ListItem Text="Afternoon" Value="Afternoon"></asp:ListItem>
                                                            <asp:ListItem Text="Evening" Value="Evening"></asp:ListItem>
                                                            <asp:ListItem Text="Night" Value="Night"></asp:ListItem>
                                                        </asp:DropDownList>
                                                    </td>
                                                </tr>
                                            </table>
                                        </div>
                                        <div id="textbox_A3" class="col-md-6 col-xs-12 div1024">


                                              <div style="display:none" id="fcontainer2"> </div>  



                                            <div>
                                                <table border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td colspan="3">
                                                        Return
                                                    </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <asp:TextBox Width="110px" ID="ReturnDateTxt"  data-calendar-contentID="#fcontainer2"  CssClass="form-control" runat="server"></asp:TextBox>
                                                        </td>
                                                        <td>
                                                            <a href="javascript:void(null)" onclick="showFlightCalendar2()">
                                                                <img id="Img3" src="images/call-cozmo.png" alt="Pick Date" />
                                                            </a>
                                                        </td>
                                                        <td>
                                                            <asp:DropDownList ID="ddlReturnTime" CssClass="form-control" runat="server">
                                                                <asp:ListItem Selected="True" Text="Any Time" Value="Any Time"></asp:ListItem>
                                                                <asp:ListItem Text="Morning" Value="Morning"></asp:ListItem>
                                                                <asp:ListItem Text="Afternoon" Value="Afternoon"></asp:ListItem>
                                                                <asp:ListItem Text="Evening" Value="Evening"></asp:ListItem>
                                                                <asp:ListItem Text="Night" Value="Night"></asp:ListItem>
                                                            </asp:DropDownList>
                                                        </td>
                                                    </tr>
                                                </table>
                                            </div>
                                        </div>
                                        <div class="clearfix">
                                        </div>
                                    </div>


                                    <style> 
               
               @media only screen and (min-device-width:768px) and (max-device-width:1024px) {
    .div1024 {
     width:100%;
    }
}

               </style>
                                </td>
                            </tr>
                            </table>
                    </div>
                    <style>
                        #tblMultiCity input[type=text]
                        {
                            width: 96%;
                        }
                        .time_t
                        {
                            width: 100px !important;
                        }
                    </style>
                    <div class="col-md-12 martop_14">
                        <table style="display: none; width: 100%" id="tblMultiCity">
                            <tr>
                                <td colspan="2">
                                    <table id="tblGrid" runat="server"> 
                                        <tr>
                                            <td style="width: 320px;">
                                                Departure
                                            </td>
                                            <td style="width: 320px;">
                                                Arrival
                                            </td>
                                            <td>
                                                Date
                                            </td>
                                            <td style="width: 100px;">
                                            </td>
                                            <td style="width: 100px;">
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <asp:TextBox ID="City1" runat="server" CssClass="form-control"></asp:TextBox>
                                                <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                    id="citycontainer1">
                                                </div>
                                            </td>
                                            <td>
                                                <asp:TextBox ID="City2" runat="server" CssClass="form-control"></asp:TextBox>
                                                <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                    id="citycontainer2">
                                                </div>
                                            </td>
                                            <td valign="bottom">
                                                <asp:TextBox ID="Time1" runat="server" data-calendar-contentID="#mcontainer1" CssClass="form-control time_t"></asp:TextBox>
                                            </td>
                                            <td>
                                                <a href="javascript:void(null)" onclick="showMultiCityCal1()">
                                                    <img id="Img5" src="images/call-cozmo.png" alt="Pick Date" />
                                                </a>
                                            </td>
                                            <td>
                                                &nbsp;
                                            </td>
                                        </tr>
                                        <tr><td><asp:CheckBox ID="chkNearByPort1" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                        <tr>
                                            <td colspan="5">
                                                <table style="margin-bottom: 10px; margin-top: 10px">
                                                    <tr>
                                                        <td style="width: 320px;">
                                                            <asp:TextBox ID="City3" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                                id="citycontainer3">
                                                            </div>
                                                        </td>
                                                        <td style="width: 320px;">
                                                            <asp:TextBox ID="City4" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div id="statesshadow2">
                                                                <div style="width: 300px; position: absolute; display: none" id="citycontainer4">
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td valign="bottom">
                                                            <asp:TextBox ID="Time2" data-calendar-contentID="#mcontainer2" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 100px;">
                                                            <a href="javascript:void(null)" onclick="showMultiCityCal2()">
                                                                <img id="Img6" src="images/call-cozmo.png" alt="Pick Date" />
                                                            </a>
                                                        </td>
                                                        <td style="width: 100px;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    <tr><td><asp:CheckBox ID="chkNearByPort2" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <table id="tblRow3" style="display: none; margin-bottom: 10px">
                                                    <tr>
                                                        <td style="width: 320px;">
                                                            <asp:TextBox ID="City5" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                                id="citycontainer5">
                                                        </td>
                                                        <td style="width: 320px;">
                                                            <asp:TextBox ID="City6" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                                id="citycontainer6">
                                                        </td>
                                                        <td>
                                                            <asp:TextBox ID="Time3" data-calendar-contentID="#mcontainer3" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                        </td>
                                                        <td style="width: 100px;">
                                                            <a href="javascript:void(null)" onclick="showMultiCityCal3()">
                                                                <img id="Img7" src="images/call-cozmo.png" alt="Pick Date" />
                                                            </a>
                                                        </td>
                                                        <td style="width: 100px;">
                                                            <input type='image' name='imageField' id='image1' src='images/minus.gif' onclick='removeRow(3);return false;'>
                                                        </td>
                                                    </tr>
                                                    <tr><td><asp:CheckBox ID="chkNearByPort3" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <table id="tblRow4" style="display: none; margin-bottom: 10px">
                                                <tr>
                                                    <td style="width: 320px;">
                                                        <asp:TextBox ID="City7" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                            id="citycontainer7">
                                                        </div>
                                                    </td>
                                                    <td style="width: 320px;">
                                                        <asp:TextBox ID="City8" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                            id="citycontainer8">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Time4" data-calendar-contentID="#mcontainer4" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 100px;">
                                                        <a href="javascript:void(null)" onclick="showMultiCityCal4()">
                                                            <img id="Img8" src="images/call-cozmo.png" alt="Pick Date" />
                                                        </a>
                                                    </td>
                                                    <td style="width: 100px;">
                                                        <input type='image' name='imageField' id='image2' src='images/minus.gif' onclick='removeRow(4);return false;'>
                                                    </td>
                                                    </tr>
                                                <tr><td><asp:CheckBox ID="chkNearByPort4" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <table id="tblRow5" style="display: none; margin-bottom: 10px">
                                                <tr>
                                                    <td style="width: 320px;">
                                                        <asp:TextBox ID="City9" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                            id="citycontainer9">
                                                        </div>
                                                    </td>
                                                    <td style="width: 320px;">
                                                        <asp:TextBox ID="City10" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                            id="citycontainer10">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Time5" data-calendar-contentID="#mcontainer5" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 100px;">
                                                        <a href="javascript:void(null)" onclick="showMultiCityCal5()">
                                                            <img id="Img9" src="images/call-cozmo.png" alt="Pick Date" />
                                                        </a>
                                                    </td>
                                                    <td style="width: 100px;">
                                                        <input type='image' name='imageField' id='image3' src='images/minus.gif' onclick='removeRow(5);return false;'>
                                                    </td>
                                                    </tr>
                                                <tr><td><asp:CheckBox ID="chkNearByPort5" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td colspan="5">
                                                <table id="tblRow6" style="display: none; margin-bottom: 10px">
                                                <tr>
                                                    <td style="width: 320px;">
                                                        <asp:TextBox ID="City11" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                            id="citycontainer11">
                                                        </div>
                                                    </td>
                                                    <td style="width: 320px;">
                                                        <asp:TextBox ID="City12" runat="server" CssClass="form-control"></asp:TextBox>
                                                        <div style="width: 300px; line-height: 30px; color: #000; position: absolute; display: none"
                                                            id="citycontainer12">
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <asp:TextBox ID="Time6" data-calendar-contentID="#mcontainer6" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                    </td>
                                                    <td style="width: 100px;">
                                                        <a href="javascript:void(null)" onclick="showMultiCityCal6()">
                                                            <img id="Img10" src="images/call-cozmo.png" alt="Pick Date" style="vertical-align: middle;
                                                                margin: 0px;" />
                                                        </a>
                                                    </td>
                                                    <td style="width: 100px;">
                                                        <input type='image' name='imageField' id='image4' src='images/minus.gif' onclick='removeRow(6);return false;'>
                                                    </td>
                                                    </tr>
                                                <tr><td><asp:CheckBox ID="chkNearByPort6" CssClass="custom-checkbox-table" runat="server" Text="Search for near by origin airports" /></td></tr>
                                                </table>
                                            </td>
                                        </tr>
                                    </table>
                                    
                                    <div style="height: 36px">
                                        <u><a class="cursor_point" id="btnAddRow" onclick="addRow();">+ Add
                                            Row </a></u>
                                    </div>
                                    <div id="errMulti" style="display: none">
                                        <div id="errorMulti" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                                        </div>
                                    </div>
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
                <table width="100%" border="0" cellpadding="5" cellspacing="0">
                    <tr>
                        <td>
                            <div class="col-md-12">
                                <div class="col-md-3">
                                    <div>
                                        Adult
                                    </div>
                                    <div>
                                        <asp:DropDownList ID="ddlAdults" CssClass="form-control"  runat="server">
                                            <asp:ListItem Selected="True" Text="1" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                            <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div>
                                        Child
                                    </div>
                                    <div>
                                        <asp:DropDownList ID="ddlChilds" CssClass="form-control" runat="server">
                                            <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div>
                                        infant</div>
                                    <div>
                                        <asp:DropDownList ID="ddlInfants" CssClass="form-control" runat="server">
                                            <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
                                            <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                            <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                            <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                            <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                            <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div>
                                        Class</div>
                                    <div>
                                        <asp:DropDownList ID="ddlBookingClass" runat="server" CssClass="form-control">
                                           <%-- <asp:ListItem  Text="Any" Value="1"></asp:ListItem>
                                            <asp:ListItem Selected="True" Text="Economy" Value="2"></asp:ListItem>
                                            <asp:ListItem Text="Premium Economy" Value="3"></asp:ListItem>
                                            <asp:ListItem Text="Business" Value="4"></asp:ListItem>
                                            <asp:ListItem Text="First" Value="6"></asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </div>
                                </div>
                                <div class="clearfix">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                         <%CT.Core.AgentAppConfig appConfig = new CT.Core.AgentAppConfig();
                            appConfig.AgentID = Settings.LoginInfo.AgentId;
                            string showPA = "display: block;";
                            CT.Core.AgentAppConfig showPAForAgent = appConfig.GetConfigData().Find(x => x.AppKey.ToLower() == "showpreferredairline");
                            if (showPAForAgent != null && showPAForAgent.AppValue.ToLower() == "false")
                                showPA = "display:none;";%>
                        <td>
                            <div class="col-md-12 martop_14">
                                <div style="<%=showPA%>">
                                <div class="col-md-3">
                                    <asp:TextBox ID="txtPreferredAirline" CssClass="form-control" placeholder="Type Preferred Airline"
                                        runat="server" onclick="IntDom('statescontainer4' , 'ctl00_cphTransaction_txtPreferredAirline')"
                                        onchange="RecheckAirline(0)"></asp:TextBox>
                                    <asp:HiddenField ID="airlineCode" runat="server" />
                                    <asp:HiddenField ID="airlineName" runat="server" />
                                    
                                    
                                      
                <div id="statescontainer4" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                                display: none; z-index: 103;">
                </div>
                
                
                
                                </div>
                              
                              
                              
                              
                              <div class="col-md-3 martop_10"> 
                                                        <a class="fcol_fff cursor_point" id="addRowLink" onclick="AddPrefAirline()">+ Add New Preferred Airline   <%--<img src="images/plus.gif" --%></a>
                                                    

                                                    </div>
                                                    </div>
                                                    
                                <div class="col-md-3 martop_10">
                                    <asp:RadioButtonList Width="100%" CssClass="custom-radio-table" ID="rbtnlMaxStops" runat="server" RepeatDirection="Horizontal">
                                        <asp:ListItem Value="-1" Text="All Flights" Selected="True"></asp:ListItem>
                                        <asp:ListItem Value="0" Text="Direct"></asp:ListItem>
                                        <asp:ListItem Value="1" Text="1 Stop"></asp:ListItem>
                                        <asp:ListItem Value="2" Text="2 Stops"></asp:ListItem>
                                    </asp:RadioButtonList>
                                </div>
                                <div class="col-md-3 martop_10">
                                    <asp:CheckBox ID="chkRefundFare" CssClass="custom-checkbox-table" runat="server" Text="Refundable Fares Only" />
                                </div>
                                <div id="divVisaEnable" style="float: right; margin-top: 26px; margin-right: 30px;display:none">
                                 <span class="custom-checkbox-table">
                                     <input type="checkbox" id="chkVisaEnable" onchange="UnSelectNonVisaSources();" /><label for="chkVisaEnable">Need visa Change</label>
                                 </span>
                             </div> 
                                
                                <div class="clearfix">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                                            <td style="<%=showPA%>">
                                                  <div class="col-md-12"> 
                                             
                                             <div class="col-md-3 martop_10" id="divPreferredAirline0"> 
                                             <div id="divPrefAirline0" style="display: none">
                                                            <input type="text" id="txtPreferredAirline0" name="txtPreferredAirline0" class="form-control" onclick="IntDom('statescontainer5' , 'txtPreferredAirline0')"
                                                                placeholder="Type Preferred Airline" />
                                                            <div id="statescontainer5" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                                display: none; z-index: 103;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 martop_10">
                                                        <a class="fcol_fff cursor_point martop_10" id="removeRowLink0" 
                                                            style="cursor: pointer; display: none">
                                                            <img src="images/minus.gif" onclick="RemovePrefAirline(0)"/></a>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" id="divPreferredAirline1">
                                                    <div class="col-md-3 martop_10">
                                                        <div id="divPrefAirline1" style="display: none">
                                                            <input type="text" id="txtPreferredAirline1" name="txtPreferredAirline1" class="form-control"
                                                                onclick="IntDom('statescontainer6' , 'txtPreferredAirline1')" placeholder="Type Preferred Airline" 
                                                                />
                                                            <div id="statescontainer6" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                                display: none; z-index: 103;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 martop_10">
                                                        <a class="fcol_fff cursor_point martop_10" id="removeRowLink1" 
                                                            style="cursor: pointer; display: none">
                                                            <img src="images/minus.gif" onclick="RemovePrefAirline(1)"/></a>
                                                    </div>
                                                </div>
                                                <div class="col-md-12" id="divPreferredAirline2">
                                                    <div class="col-md-3 martop_10">
                                                        <div id="divPrefAirline2" style="display: none">
                                                            <input type="text" id="txtPreferredAirline2" name="txtPreferredAirline2" class="form-control"
                                                                onclick="IntDom('statescontainer7' , 'txtPreferredAirline2')" placeholder="Type Preferred Airline"
                                                                />
                                                            <div id="statescontainer7" style="width: 300px; line-height: 30px; color: #000; position: absolute;
                                                                display: none; z-index: 103;">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-md-3 martop_10">
                                                        <a class="fcol_fff cursor_point martop_10" id="removeRowLink2" 
                                                            style="cursor: pointer; display: none">
                                                            <img src="images/minus.gif" onclick="RemovePrefAirline(2)"/></a>
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                    <tr>
                        <td>
                            <div class="col-md-12" style="margin-top: 14px" id="Additional-Div">
                                <%if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                                  { %>
                                <%} %>
                                <div class="col-md-4">
                                    <asp:Label ID="lblSearchSupp" runat="server" Text="Search Suppliers :" Visible="true"
                                        Font-Bold="true"></asp:Label>
                                </div>
                                <div class="col-md-12">
                                    <label class="pull-left chckboxTravel">
                                        <asp:CheckBox ID="chkFlightAll" CssClass="chkChoice custom-checkbox-table" runat="server" Text="All" Checked="true" Visible="true"
                                            onclick="selectFlightSources();" />
                                    </label>
                                    <label class="pull-left table-responsive chkChoiceTableTravel">
                                        <asp:CheckBoxList ID="chkSuppliers" CssClass="chkChoice table table-condensed custom-checkbox-table" runat="server" RepeatDirection="Horizontal" Visible="true"
                                            onclick="setUncheck();">
                                        </asp:CheckBoxList>
                                    </label>
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <div class="col-md-12">
                                <label class=" pull-right">
                                    <asp:Button CssClass="but but_b pull-right btn-xs-block" runat="server" ID="btnSearchFlight"
                                        Text="Search Flights" OnClientClick="return FlightSearch();" OnClick="btnSearchFlight_Click" />
                                </label>
                                <div class="clearfix">
                                </div>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            &nbsp;
                        </td>
                    </tr>
                </table>
            </div>
            <%--<div style="padding-top:10px"> 
                          
                          
                             <div>
                    

                              
                      
                           
                          <div>
                            
                                    
                              <div class="col-md-4 col-xs-4"><asp:RadioButton ID="oneway" GroupName="radio" onclick="disablefield();" runat="server" Text="One Way" Font-Bold="true" />  </div>
                              <div class="col-md-4 col-xs-4"> <asp:RadioButton ID="roundtrip" GroupName="radio" onclick="disablefield();" runat="server" Text="Round Trip" Font-Bold="true" /> </div>
                              <div class="col-md-4 col-xs-4 hidden-xs"><label style="display:block">
                                                            
                                                             <asp:RadioButton ID="multicity" GroupName="radio" onclick="disablefield();" runat="server"  Text="Multi City" Font-Bold="true"/></label> </div>
                              
                           
                            <div class="clearfix"> </div>
                           </div>
                           
                           <div id="tblNormal">
                           
                           <table border="0" cellspacing="0" cellpadding="0" width="100%" >
                           
             
                                    <tr>
                                        <td>
                                        
                       
                                        
                                        <div>
                                        
                                        <div class="col-md-6">  
                                        <div>  From</div>
                                        <div>  <asp:TextBox ID="Origin" CssClass="form-control" runat="server"></asp:TextBox>
                                                                    <div class="clear">
                                                                        <div style="width:300px; line-height:30px; color:#000;" id="statescontainer">
                                                                        </div>
                                                                    </div>
                                                                    <div id="multipleCity1">
                                                                    </div></div>
                                        
                                        </div>
                                        
                                         <div class="col-md-6">  
                                         
                                          <div>  Going To</div>
                                         
                                         <div> <asp:TextBox ID="Destination" CssClass="form-control" runat="server"></asp:TextBox>
                                                                    <div id="Div1">
                                                                        <div style="width:300px; line-height:30px; color:#000;" id="statescontainer2">
                                                                        </div>
                                                                    </div>
                                                                    <div id="multipleCity2">
                                                                    </div></div>
                                         
                                         
                                         </div>
                                         
                                         <div class="clearfix"> </div>
                                         
                                         </div>
                                            
                                        </td>
                                    </tr>
                                    
                                    
                                    
                                    <tr> 
                                    
                                    <td height="10"> </td>
                                    </tr>
                                    
                                    
                                    <tr> 
                                    
                                    
                                    <td> 
                                    
                                      <div class="col-md-12 padding-0">
                                        
                                        <div class="col-md-6"> 
                                         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        
                                                                        <tr> 
                                                                        <td colspan="3">Departure</td>
                                                                        </tr>
                                                                        
                                                                        <tr>
                                                                            <td>
                                                                                <asp:TextBox ID="DepDate" CssClass="form-control" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td>
                                                                                <a href="javascript:void(null)" onclick="showFlightCalendar1()">
                                                                                    <img id="Img4" src="images/call-cozmo.png" alt="Pick Date" />
                                                                                </a>
                                                                            </td>
                                                                            <td class=" hidden-xs">
                                                                                <asp:DropDownList ID="ddlDepTime" CssClass="form-control"  runat="server">
                                                                                    <asp:ListItem Selected="True" Text="Any Time" Value="Any Time"></asp:ListItem>
                                                                                    <asp:ListItem Text="Morning" Value="Morning"></asp:ListItem>
                                                                                    <asp:ListItem Text="Afternoon" Value="Afternoon"></asp:ListItem>
                                                                                    <asp:ListItem Text="Evening" Value="Evening"></asp:ListItem>
                                                                                    <asp:ListItem Text="Night" Value="Night"></asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                        
                                        </div>
                                        
                                         <div  id="textbox_A3" class="col-md-6"> 
                                         <div>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                            
                                                                            <td colspan="3"> 
                                                                              Return
                                                                            
                                                                            </td>
                                                                            
                                                                            
                                                                            <tr>
                                                                                <td>
                                                                                    <asp:TextBox ID="ReturnDateTxt" CssClass="form-control" runat="server"></asp:TextBox>
                                                                                </td>
                                                                                <td>
                                                                                    <a href="javascript:void(null)" onclick="showFlightCalendar2()">
                                                                                        <img id="Img3" src="images/call-cozmo.png" alt="Pick Date" />
                                                                                    </a>
                                                                                </td>
                                                                               <td class=" hidden-xs">
                                                                         <asp:DropDownList ID="ddlReturnTime" CssClass="form-control" runat="server">
                                                                         <asp:ListItem Selected="True" Text="Any Time" Value="Any Time"></asp:ListItem>
                                                                                        <asp:ListItem Text="Morning" Value="Morning"></asp:ListItem>
                                                                                        <asp:ListItem Text="Afternoon" Value="Afternoon"></asp:ListItem>
                                                                                        <asp:ListItem Text="Evening" Value="Evening"></asp:ListItem>
                                                                                        <asp:ListItem Text="Night" Value="Night"></asp:ListItem>
                                                                                    </asp:DropDownList>
                                                                                </td>
                                                                            </tr>
                                                                        </table>
                                                        </div>
                                        
                                        </div>
                                     <div class="clearfix"> </div>
                                         
                                         </div>
                                    
                                   </td>
                                    
                                    </tr>
                                    
                                    
                                    
                                    
                                    <tr></tr>
                                    </table>
                           
                           
                           
                           </div>
            
            
            
            <style> 
            
            #tblMultiCity input[type=text] {  width:96%; }
            
            
            .time_t { width:100px!important}
            
            </style>               
                           
                            <div class="col-md-12 martop_14"> 
                           <table style="display: none; width: 100%" id="tblMultiCity">
                                        <tr>
                                            <td colspan="2">
                                                <table id="tblGrid" runat="server">
                                                    <tr>
                                                        <td style="width: 320px;">
                                                            Departure
                                                        </td>
                                                        <td style="width: 320px;">
                                                            Arrival
                                                        </td>
                                                        <td>
                                                            Date
                                                        </td>
                                                      <td style="width: 100px;">
                                                       
                                                        </td>
                                                        
                                                        <td style="width: 100px;">
                                                        
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                     
                                                            <asp:TextBox ID="City1" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div style="width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer1">
                                                            </div>
                                                        </td>
                                                        <td>
                                                           
                                                            <asp:TextBox ID="City2" runat="server"  CssClass="form-control"></asp:TextBox>
                                                            <div style=" width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer2">
                                                            </div>
                                                        </td>
                                                        <td valign="bottom">
                                                           
                                                            <asp:TextBox ID="Time1" runat="server" CssClass="form-control time_t" ></asp:TextBox>
                                                            
                                                        </td>
                                                        <td>
                                                        <a href="javascript:void(null)" onclick="showMultiCityCal1()">
                                                                <img id="Img5" src="images/call-cozmo.png" alt="Pick Date" />
                                                            </a>
                                                        </td>
                                                        <td>
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    
                                                    
                                                    <tr>
                                                    
                                                    <td colspan="5">
                                                    
                                                   <table style="margin-bottom:10px; margin-top:10px"> 
                                                    
                                                    
                                                    <tr>
                                                         <td style="width: 320px;">
                                                            <asp:TextBox ID="City3" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div style=" width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer3">
                                                            </div>
                                                        </td>
                                                        <td style="width: 320px;">
                                                           
                                                            <asp:TextBox ID="City4" runat="server" CssClass="form-control"></asp:TextBox>
                                                            <div id="statesshadow2">
                                                                <div style=" width:300px; position: absolute; display: none" id="citycontainer4">
                                                                </div>
                                                            </div>
                                                        </td>
                                                        <td valign="bottom">
                                                           
                                                            <asp:TextBox ID="Time2" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                            
                                                        </td>
                                                       <td style="width: 100px;">
                                                        <a href="javascript:void(null)" onclick="showMultiCityCal2()">
                                                                <img id="Img6" src="images/call-cozmo.png" alt="Pick Date"/>
                                                            </a>
                                                        </td>
                                                        
                                                        <td style="width: 100px;">
                                                            &nbsp;
                                                        </td>
                                                    </tr>
                                                    
                                                    </table>
                                                    
                                                    
                                                    
                                                     </td>
                                                    </tr>
                                                  
                                                  
                                                  
                                                  
                                                    <tr>
                                                        <td colspan="5">
                                                        
                                                        
                                                 <table id="tblRow3" style="display: none;margin-bottom:10px"> 
                                                        
                                                        <tr>
                                                        <td style="width:320px;">
                                                                <asp:TextBox ID="City5" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer5">
                                                        </td>
                                                        <td style="width:320px;">
                                                          
                                                                    <asp:TextBox ID="City6" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer6">
                                                        </td>
                                                        <td>
                                                       <asp:TextBox  ID="Time3" runat="server" CssClass="form-control time_t" ></asp:TextBox>
                                                        </td>
                                                      
                                                      <td style="width:100px;">
                                                          <a href="javascript:void(null)" onclick="showMultiCityCal3()">
                                                                        <img id="Img7" src="images/call-cozmo.png" alt="Pick Date" />
                                                                    </a> 
                                                        </td>
                                                        
                                                        <td style="width:100px;">
                                                         <input type='image' name='imageField' id='image1' src='images/minus.gif' onclick='removeRow(3);return false;'>
                                                        </td>
                                                    </tr>
                                                      </table>  
                                                        
                                                            
                                                              
                                                        </td>
                                                    </tr>
                                                   
                                                   
                                                    <tr>
                                                        <td colspan="5">
                                                            <table id="tblRow4" style="display: none;margin-bottom:10px">
                                                             <td style="width:320px;">
                                                                    
                                                                    <asp:TextBox ID="City7" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer7">
                                                                    </div>
                                                                </td>
                                                            <td style="width:320px;">
                                                                   
                                                                    <asp:TextBox ID="City8" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer8">
                                                                    </div>
                                                                </td>
                                                              
                                                              <td>
                                                                    
                                                                    <asp:TextBox ID="Time4" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                                    
                                                                </td>
                                                                
                                                                 <td style="width: 100px;"> <a href="javascript:void(null)" onclick="showMultiCityCal4()">
                                                                        <img id="Img8" src="images/call-cozmo.png" alt="Pick Date"/>
                                                                    </a> </td>
                                                                
                                                              <td style="width: 100px;">
                                                                    <input type='image' name='imageField' id='image2' src='images/minus.gif' onclick='removeRow(4);return false;'>
                                                                </td>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    
                                                    
                                                    
                                                    
                                                    <tr>
                                                        <td colspan="5">
                                                            <table id="tblRow5" style="display: none;margin-bottom:10px">
                                                               <td style="width:320px;">
                                                                   
                                                                    <asp:TextBox ID="City9" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer9">
                                                                    </div>
                                                                </td>
                                                                <td style="width:320px;">
                                                                   
                                                                    <asp:TextBox ID="City10" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer10">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                  
                                                                    <asp:TextBox ID="Time5" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                                    
                                                                </td>
                                                                
                                                                
                                                                  <td style="width:100px;"><a href="javascript:void(null)" onclick="showMultiCityCal5()">
                                                                        <img id="Img9" src="images/call-cozmo.png" alt="Pick Date" />
                                                                    </a>  </td>
                                                                
                                                                
                                                                <td style="width:100px;">
                                                                    <input type='image' name='imageField' id='image3' src='images/minus.gif' onclick='removeRow(5);return false;'>
                                                                </td>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td colspan="5">
                                                            <table id="tblRow6" style="display: none; margin-bottom:10px">
                                                                <td style="width:320px;">
                                                                   
                                                                    <asp:TextBox ID="City11" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer11">
                                                                    </div>
                                                                </td>
                                                                <td style="width:320px;">
                                                                    
                                                                    <asp:TextBox ID="City12" runat="server" CssClass="form-control"></asp:TextBox>
                                                                    <div style="width:300px; line-height:30px; color:#000; position: absolute; display: none" id="citycontainer12">
                                                                    </div>
                                                                </td>
                                                                <td>
                                                                   
                                                                    <asp:TextBox ID="Time6" runat="server" CssClass="form-control time_t"></asp:TextBox>
                                                                    
                                                                </td>
                                                                
                                                                 <td style="width:100px;"> <a href="javascript:void(null)" onclick="showMultiCityCal6()">
                                                                        <img id="Img10" src="images/call-cozmo.png" alt="Pick Date" style="vertical-align: middle;
                                                                            margin: 0px;" />
                                                                    </a> </td>
                                                                
                                                               <td style="width:100px;"> 
                                                                    <input type='image' name='imageField' id='image4' src='images/minus.gif' onclick='removeRow(6);return false;'>
                                                                </td>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </table>
                                               
                                               
                                                <div style=" height:36px">
                                                 
                                                    
                                                   <u> <a style=" color:#fff; cursor:pointer" id="btnAddRow" onclick="addRow();">+ Add Row  </a></u> 
                                                    
                                                    </div>
                                                   
                                                   
                                                    <div id="errMulti" style="display:none">
                                                    <div id="errorMulti" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
                                                    </div>
                                            </td>
                                        </tr>
                                    </table>
                            
                            </div>
                          
                          
                          
                          
                          
                          
                        <div class="martop_14">  <table width="100%" border="0" cellpadding="5" cellspacing="0">
                               
                                    <tr> 
                                    <td>
                                    
                                      <div> 
                                    
                                    <div class="col-md-3"> 
                                    <div> Adult </div>
                                    <div> <asp:DropDownList ID="ddlAdults" CssClass="form-control" runat="server">
                                                                        <asp:ListItem Selected="True" Text="1" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                                        <asp:ListItem Text="9" Value="9"></asp:ListItem>
                                                                    </asp:DropDownList></div>
                                    
                                    </div>
                                    
                                    
                                    
                                        <div class="col-md-3"> 
                                    <div> Child </div>
                                    <div><asp:DropDownList ID="ddlChilds" CssClass="form-control" runat="server">
                                                                        <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                                    </asp:DropDownList> </div>
                                    
                                    </div>
                                    
                                    
                                        <div class="col-md-3"> 
                                    <div>  infant</div>
                                    <div> <asp:DropDownList ID="ddlInfants" CssClass="form-control" runat="server">
                                                                        <asp:ListItem Selected="True" Text="0" Value="0"></asp:ListItem>
                                                                        <asp:ListItem Text="1" Value="1"></asp:ListItem>
                                                                        <asp:ListItem Text="2" Value="2"></asp:ListItem>
                                                                        <asp:ListItem Text="3" Value="3"></asp:ListItem>
                                                                        <asp:ListItem Text="4" Value="4"></asp:ListItem>
                                                                        <asp:ListItem Text="5" Value="5"></asp:ListItem>
                                                                        <asp:ListItem Text="6" Value="6"></asp:ListItem>
                                                                        <asp:ListItem Text="7" Value="7"></asp:ListItem>
                                                                        <asp:ListItem Text="8" Value="8"></asp:ListItem>
                                                                    </asp:DropDownList></div>
                                    
                                    </div>
                                    
                                    
                                        <div class="col-md-3"> 
                                    <div>  Class</div>
                                    <div><asp:DropDownList ID="ddlBookingClass" runat="server" CssClass="form-control">
                                                        <asp:ListItem Selected="True" Text="Any" Value="1"></asp:ListItem>
                                                        <asp:ListItem Text="Economy" Value="2"></asp:ListItem>
                                                        <asp:ListItem Text="Business" Value="4"></asp:ListItem>
                                                        <asp:ListItem Text="First" Value="6"></asp:ListItem>
                                                        </asp:DropDownList> </div>
                                    
                                    </div>
                                    
                                    
                                    
                                    
                                    <div class="clearfix"> </div>
                                    
                                   </div>
                                    
                                    </td>
                                    </tr>
                                    
                                    
                                    
                                    
                                    
                                    <tr>
                                        <td>
                                        
                                        
                                         <div class="martop_14">
                                         
                                         <div class="col-md-4"> 
                                         
                                         
                                           <asp:TextBox ID="txtPreferredAirline" CssClass="form-control" Text="Type Preferred Airline"
                                                                        runat="server" onclick="IntDom('statescontainer4' , 'ctl00_cphTransaction_txtPreferredAirline')" onblur="markout(this, 'Type Preferred Airline')" onfocus="markin(this, 'Type Preferred Airline')"></asp:TextBox>
                                                                        
                                                                    <asp:HiddenField ID="airlineCode" runat="server" />
                                                                    <asp:HiddenField ID="airlineName" runat="server" />
                                                                    
                                                                    
                                                                    </div>
                                         
                                          <div class="col-md-4 martop_10"> 
                                               
                                               <asp:RadioButtonList Width="100%" ID= "rbtnlMaxStops" runat="server" RepeatDirection="Horizontal" >
    <asp:ListItem Value="-1" Text="All Flights" Selected="True"></asp:ListItem>
    <asp:ListItem Value="0" Text="Direct" ></asp:ListItem>
    <asp:ListItem Value="1" Text="1 Stop"></asp:ListItem>
    <asp:ListItem Value="1" Text="2 Stops"></asp:ListItem>
    </asp:RadioButtonList>
                                               </div>
                                         
                                             
                                              
                                              
                                               <div class="col-md-4 martop_10"> 
                                               
                                               <asp:CheckBox ID="chkRefundFare" runat="server" Text="Refundable Fares Only" />
                                               
                                               </div>
                                              
                                              
                                              
                                                                    
                                         
                                          <div class="clearfix"> </div>
                                         </div>
                                        
                                            
                                        </td>
                                    </tr>
                                    
                                    
          
                                    
                               
                                    
                                  
                                  
                                  
                                  <tr> 
                                  
                                  
                                  <td> 
                                  
                                  <div class="col-md-12"> 
                                
                                
                                
                                    <div  style="padding-top:14px" class="col-md-8">
                                     <label><strong> Search Suppliers : </strong> </label>
                                          
                                       
 
                                       <label>
      <input type="checkbox" name="CheckboxGroup1" value="checkbox" id="CheckboxGroup1_0" />
      All</label>
   
   
    <label>
      <input type="checkbox" name="CheckboxGroup1" value="checkbox" id="CheckboxGroup1_0" />
      UA</label>
  
    <label>
      <input type="checkbox" name="CheckboxGroup1" value="checkbox" id="CheckboxGroup1_1" />
      G9</label>
  
    <label>
      <input type="checkbox" name="CheckboxGroup1" value="checkbox" id="CheckboxGroup1_2" />
      FZ</label>
  
    <label>
      <input type="checkbox" name="CheckboxGroup1" value="checkbox" id="CheckboxGroup1_3" />
      SG</label>
      
      
       <label>
      <input type="checkbox" name="CheckboxGroup1" value="checkbox" id="CheckboxGroup1_4" />
      TA</label>
      
                                   </div>
                                
                                 <div class="col-md-4">
                                <label class="pull-right">
                                              
                                            
                                           
                                           
                                           
                                           <a href="#" class="btn but_b">
                                                        Search </a>
                                            
                                            
                                            
                                            </label>
                                 
                                 
                              
                                 
                                 <div class="clearfix"> </div>
                                 
                                </div>
                                  
                                  </td>
                                  </tr>
                                  
                                  
                                  
                                  <tr> 
                                  
                                  <td> &nbsp;</td>
                                  
                                  </tr>
                                  
                                    
                                </table></div>
                          </div>
                                
                                
                                </div>--%>
        </div>
        <!--Modify container ends -->
        
        <div class="col-md-12 mb-1"> 
             <%if (filteredResults != null && filteredResults.Length > 0)
                 { %>
           <ul class="pagination pull-right mt-2 ml-4"> 
              
               <li><a id="EmailLink" style="display: block;font-weight:bold;" href="javascript:ShowEmailDiv('Email')"><i class="fa fa-envelope"></i> Email Itineraries</a></li>
               <%if (agency.RequiredItinerary)
    { %>
               <li><a id="DownloadLink" style="display: block;font-weight:bold;" href="javascript:ShowEmailDiv('Download')"><i class="fa fa-envelope"></i> Download Itineraries</a></li>
               <%}%>
    
           </ul>       
        <%--    <div class="pagination tt-paging mt-2"><a href="Javascript:DaySelection('-')">Previous Day</a></div>            
            <div class="pagination tt-paging pull-right mt-2"><a href="Javascript:DaySelection('+')">Next Day</a><span>&nbsp;&nbsp;</span><%=show%> </div> --%>

               
    <%} %>   
            
       <div class="clearfix"></div>
       
        </div>
       
       
        
        <div class="row custom-gutter">
            <div class="col-12 col-lg-2 padding-0 hidden-xs">
             <%--    <%if (filteredResults != null && filteredResults.Length > 0)
                 { %>
                 <div class="float-left d-flex  pagination tt-paging mb-4 mr-3 bold text-center" style="width: 230px;">
                    <a href="Javascript:DaySelection('-')" class="w-50"><span class="icon-navigate_before"></span> Previous Day</a>
                    <a href="Javascript:DaySelection('+')" class="w-50">Next Day <span class="icon-navigate_next"></span></a>
                </div>
                   <%} %> --%> 
                <div>
                    <div class="inn_wrap01">
                    <div class="pad-5">
                    <a href="#" onclick="ClearFilter()">Clear Filters</a>                    
                    </div>                    
                        <div class="ns-h3">
                            Narrow Result</div>
                        <div class="gray-smlheading">
                            <div class="pad-5">
                                

                               <label>
      
      <asp:CheckBox ID="chkAirline" CssClass="chkChoice custom-checkbox-table chk-Suppliers-table" tex="Airlines" runat="server"  Text="Airlines" Checked="true" onclick="SelectUnSelect('chkListAirlines');" />
      

    
 </label>

                            </div>
                            
                            
                            
                            
                            
                            <div class="mb-3">
                            
                           

                                <asp:CheckBoxList ID="chkListAirlines" CssClass="chkList" runat="server" onchange="FilterResults('Airline');">

                                
                                
                             
                                </asp:CheckBoxList>
                                

                                <div class="clearfix"> </div>
                                
                            </div>
                            <div class="ns-h3">
                                Flight Details</div>
                            <div class="pad-5">
                                <table class="pdr_2px" width="100%" border="0" cellspacing="0" cellpadding="0">
                                     <tr id="trRefundable">
                                        <td width="100%">
                                            <%if(request.Sources.Count == 1 && request.Sources[0] == "PK") { %>
                                           <label>
                                            <asp:CheckBox ID="CheckBox1" CssClass="chkChoice custom-checkbox-table chk-Suppliers-table" runat="server" Text="Refundable" style="display:none;" onchange="FilterResults('Refund');"
                                                Checked="true" />

                                                </label>

                                            <%}else { %>
                                            
                                            <label> 

                                            <asp:CheckBox ID="chkRefundable" CssClass="chkChoice custom-checkbox-table chk-Suppliers-table" runat="server" Text="Refundable" onchange="FilterResults('Refund');"
                                                Checked="true" />


                                                </label>


                                            <%} %>
                                        </td>
                                    </tr>
                                     <tr id="trNonRefundable">
                                        <td width="100%">
                                        <label>


                                            <asp:CheckBox ID="chkNonRefundable" runat="server" CssClass="chkChoice custom-checkbox-table chk-Suppliers-table" Text="Non Refundable" onchange="FilterResults('nonRefund');"
                                                Checked="true" />



                                                </label>

                                        </td>
                                    </tr> 
                                </table>
                            </div>
                            <div class="ns-h3">
                                No. of Stops</div>
                            <div class="pad-5">
                                <table class="pdr_2px" width="100%" border="0" cellspacing="0" cellpadding="0">
<%--                                    <%if(request != null && (request.MaxStops == "" || request.MaxStops == "0")){ %>--%>                                         
                                    <tr id="trNonStop">
                                        <td width="100%">   
                                        
                                        <label> 
                                                                             
                                            <asp:CheckBox ID="chkNonStop" runat="server" Text="Non Stop"  CssClass="chkChoice custom-checkbox-table chk-Suppliers-table" onchange="FilterResults('Stops');"
                                                Checked="true" />



                                                </label>
                                        </td>
                                    </tr>  
                                    <%-- <%} if (request != null && (request.MaxStops == "" || request.MaxStops == "1"))
                                      { %> --%>
 
                                     <tr id="trOneStop">
                                        <td width="100%">


                                        <label> 

                                            <asp:CheckBox ID="chkOneStop" runat="server"  CssClass="chkChoice custom-checkbox-table chk-Suppliers-table" Text="One Stop" onchange="FilterResults('Stops');"
                                                Checked="true" />


                                                </label>


                                        </td>
                                    </tr>
                                    <%-- <%} if (request != null && (request.MaxStops == "" || request.MaxStops == "2"))
                                      { %>--%>
                                     <tr id="trTwoStops">
                                        <td width="100%">
                                           
                                           <label> 


                                           
                                            <asp:CheckBox ID="chkTwoStops"  CssClass="chkChoice custom-checkbox-table chk-Suppliers-table" runat="server" Text="Two Stops" onchange="FilterResults('Stops');"
                                                Checked="true" />


                                                </label>

                                        </td>
                                    </tr>
                                <%--    <%} %>--%>
                                </table>
                            </div>
                            <div class="ns-h3">
                                Sort By</div>
                            <div class="pad-5">
                                <asp:DropDownList CssClass="form-control no-select2" ID="ddlPrice" runat="server" onchange="FilterResults('PriceSort');">
                                    <asp:ListItem Selected="True" Value="Lowest" Text="Lowest Price"></asp:ListItem>
                                    <asp:ListItem Value="Highest" Text="Highest Price"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList CssClass="form-control no-select2" ID="ddlDeparture" runat="server" style="display:none;" onchange="FilterResults('DepSort');">
                                    <asp:ListItem Value="-1" Text="Departure"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Shortest"></asp:ListItem>
                                    <asp:ListItem Value="1" Text="Longest"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList CssClass="form-control no-select2" ID="ddlArrival" runat="server" style="display:none;" onchange="FilterResults('ArrSort');">
                                    <asp:ListItem Value="-1" Text="Arrival"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Shortest"></asp:ListItem>                                  
                                    <asp:ListItem Value="1" Text="Longest"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList CssClass="form-control no-select2" ID="ddlAirline" runat="server" style="display:none;" onchange="FilterResults('AirlineSort');">
                                    <asp:ListItem Value="-1" Text="Airline"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Ascending"></asp:ListItem>                                    
                                    <asp:ListItem Value="1" Text="Descending"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:DropDownList CssClass="form-control no-select2" ID="ddlStops" runat="server" style="display:none;" onchange="FilterResults('StopSort');">
                                    <asp:ListItem Value="-1" Text="Stops"></asp:ListItem>
                                    <asp:ListItem Value="0" Text="Direct"></asp:ListItem>                                    
                                    <asp:ListItem Value="1" Text="Non-Direct"></asp:ListItem>
                                </asp:DropDownList>                                
                            </div>
                            <div class="ns-h3">
                                Timing Range</div>
                            <div class="timing-range-selector">
                                <%if (!request.TimeIntervalSpecified)
                                    {  %>
                                <ul>
                                    <li>
                                        <button id="btnNight" type="button" class="timing-filter night" onclick="FilterTiming('Night')">
                                            00:00<em>-</em>04:59
                                        </button>
                                    </li>
                                    <li>
                                        <button id="btnMorning" type="button" class="timing-filter morning" onclick="FilterTiming('Morning')">
                                            05:00<em>-</em>11:59
                                        </button>
                                    </li>
                                    <li>
                                        <button id="btnNoon" type="button" class="timing-filter noon" onclick="FilterTiming('Noon')">
                                            12:00<em>-</em>17:59
                                        </button>
                                    </li>
                                    <li>
                                        <button id="btnEvening" type="button" class="timing-filter evening" onclick="FilterTiming('Evening')">
                                            18:00<em>-</em>23:59
                                        </button>
                                    </li>
                                </ul>
                                <%}
    else
    { %>
                                   <label>Onward Time Filters</label>
                                   <asp:DropDownList ID="ddlOnwardTimings" CssClass="form-control" onchange="FilterTimingResults()" runat="server" AppendDataBoundItems="true">
                                       <asp:ListItem Selected="True" Text="Any Time" Value="Any Time"></asp:ListItem>
                                   </asp:DropDownList>     
                                <%if (request.Type == CT.BookingEngine.SearchType.Return)
    { %>
                                   <label>Return Time Filters</label>
                                   <asp:DropDownList ID="ddlReturnTimings" CssClass="form-control" onchange="FilterTimingResults()" runat="server" AppendDataBoundItems="true">
                                       <asp:ListItem Selected="True" Text="Any Time" Value="Any Time"></asp:ListItem>
                                   </asp:DropDownList>
                                <%}
    } %>
                            </div>
                            <div class="pad-5" style="display:none">
                                <asp:DropDownList CssClass="form-control no-select2" runat="server" ID="ddlTimings" onchange="FilterResults('Time');">
                                    <asp:ListItem Selected="True" Value="0" Text="All"></asp:ListItem>
                                    <asp:ListItem Text="Morning" Value="1"></asp:ListItem>
                                    <asp:ListItem Text="After Noon" Value="2"></asp:ListItem>
                                    <asp:ListItem Text="Evening" Value="4"></asp:ListItem>
                                    <asp:ListItem Text="Night" Value="6"></asp:ListItem>
                                </asp:DropDownList>
                                <label>
                                </label>
                            </div>
                            <div class="ns-h3">
                                Price Range</div>
                            <div class="pad-5">
                                <p>
                                    <label>
                                        Price range:</label><label id="label" onkeypress="return false;"></label>
                                    <input type="text" id="amount" onkeypress="return false;" style="text-align: center;
                                        border: 0; font-weight: bold;" />
                                </p>
                            </div>
                            <div class="pad-5">
                                <div style="width: 90%; margin: auto" id="slider-range" class="flight-slider-range">
                                </div>
                            </div>
                            
                            <div class="ns-h3" id="divPolicy" runat="server" style="display:none">
                                Policy 
                            </div>
                            <div class="pad-5" id="chkpolicies" runat="server" style="display:none">
                                <asp:CheckBox ID="chkInpolicy" runat="server" Text="In Policy" onchange="FilterResults('Policy');"  Checked="true"/>
                                <asp:CheckBox ID="chkOutpolicy" runat="server" Text="Out Policy" onchange="FilterResults('Policy');" Checked="true"/>
                            </div>
                            
                            <div class="pad-5">
                                <asp:Button ID="btnFilter" Visible="false" runat="server" Text="Filter" OnClick="btnFilter_Click"
                                    OnClientClick="FilterResults();" />
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-12 col-lg-10">
                <div>
                    <table style="width: 100%">
                        <tr>
                            <td style="width:50%">
                                <div class="pday-nday" style="display: none">
                                    <asp:LinkButton ID="lnkPrevDayOB" runat="server" CssClass="prv" CommandName="PreviousDayOB"
                                        OnClick="lnkPrevDayOB_Click">Previous Day</asp:LinkButton>
                                    <label>
                                        Onward -
                                    </label>
                                    <asp:LinkButton ID="lnkNextDayOB" runat="server" CssClass="nxt" CommandName="NextDayOB"
                                        OnClick="lnkNextDayOB_Click">Next Day</asp:LinkButton>
                                </div>
                            </td>
                            <td style="width: 50%; text-align: right">
                                <div class="pday-nday" style="display: none">
                                    <asp:LinkButton ID="lnkPrevDayIB" runat="server" CssClass="prv" CommandName="PreviousDayIB"
                                        OnClick="lnkPrevDayIB_Click">Previous Day</asp:LinkButton>
                                    <label>
                                        Return -
                                    </label>
                                    <asp:LinkButton ID="lnkNextDayIB" runat="server" CssClass="nxt" CommandName="NextDayIB"
                                        OnClick="lnkNextDayIB_Click">Next Day</asp:LinkButton>
                                </div>
                            </td>
                        </tr>
                    </table>
                    
                    
                    
                    <div>              </div>
                     <asp:HiddenField ID="hdnResultCount" runat="server" Value="" />
                    <%if (filteredResults != null && filteredResults.Length > 0)
                      { %>
       
                       
                       
                       
                    <input id="ResultCountMax" type="hidden" value="<% =maxResult %>" />
                    <input id="ResultCountMin" type="hidden" value="<% =minResult %>" />
                    <%}
                      else
                      { %>
                    <%if (!IsPostBack)
                      { %>
                    <div style="text-align: center; font-weight: bold; border: 1px">
                        No Flights Found
                        <br />
                        <%--<a href="HotelSearch.aspx?source=Flight">Go To Search Page</a> --%>
                        <asp:LinkButton ID="lnkSearch" runat="server" PostBackUrl="~/HotelSearch?source=Flight">Go to Search page</asp:LinkButton>
                    </div>
                    <%}
                      else
                      {%>
                    <div style="text-align: center; font-weight: bold; border: 1px">
                        No Flitered Records </div>
                        <%} %>
                        <%} %>
                     <div>
                        <asp:DataList ID="dlFlightResults" runat="server" Width="100%" OnItemDataBound="dlFlightResults_ItemDataBound" >
                            <HeaderTemplate>
           <div style="line-height:30px; font-weight:bold; margin-bottom:4px;font-size: 13px;" class="hidden-xs border_1 bg_white bor_gray ">
                                    <div class="col-md-2"><a id="airlineSort" href="#" class="flight-sort-btn"><span class="glyphicon glyphicon-arrow-down"></span>Airline</a></div>
                                    <div class="col-md-2 pad_left0"><a id="departureSort" href="#" class="flight-sort-btn"><span class="glyphicon glyphicon-arrow-down"></span>Departure</a></div>
                                    <div class="col-md-2 pad0""><a id="arrivalSort" href="#" class="flight-sort-btn"><span class="glyphicon glyphicon-arrow-down"></span>Arrival</a></div>
                                    <div class="col-md-2 pad0""><a id="stopsSort" href="#" class="flight-sort-btn"><span class="glyphicon glyphicon-arrow-down"></span>Stops</a></div>
                                    <div class="col-md-2 pad0"><%--<span class="glyphicon glyphicon-arrow-down duration-sort-icon"></span>--%>
                                        <select id="durationSort" class="form-control duration-sorting no-select2" onchange="SortByDuration()">
                                            <option>Duration</option>
                                            <option>By Journey Time</option>
                                            <option>By Flying Duration</option>                                        
                                        </select>
                                    </div>
                                    <div class="col-md-2 pad0"><center><a id="priceSort" href="#" class="flight-sort-btn"><span class="glyphicon glyphicon-arrow-down"></span>Price</a>                                    
                                            <input type="checkbox" id="chkDownAll" title="Select All for Download" onchange="SelectAll()"/> Select All                                        
                                        </center>
                                        </div>
                                    <div class="clearfix"> </div>
                              </div>




                            </HeaderTemplate>
                            <ItemTemplate>
                               
                    
             
      
     
                               
                                    <div>
                                        <div class="email-itenary" id="emailBlock">
                                            <div class="fleft search-child-block" style="background-color: Red;">
                                                <div class="display-none yellow-back center" id="error">
                                                </div>
                                            </div>
                                            <div class="showMsgHeading">
                                                Enter Email Address <a class="closex" href="#" onclick="HideEmailDiv('emailBlock');" style="position: absolute;
                                                    right: 5px; top: 3px;">X</a>
                                            </div>
                                            <div class="pad_10">
                                                <div id="itineraryCount">
                                                    Selected 2 Itineraries for Email</div>
                                                <div>
                                                    
                                                    <input class="form-control" id="addressBox" type="text" name="" />
                                                    <span style="color: Red" id="errortext"></span>
                                                    
                                                </div>
                                              

                                                <div style="display: <% =Settings.LoginInfo.IsCorporate=="Y"?"none":"block" %>">
                                                    Add Markup:
                                                </div>
                                                <div style="display: <% =Settings.LoginInfo.IsCorporate=="Y"?"none":"block" %>">
                                                    <input class="form-control" type="text" id="txtMarkup" onkeypress="return isNumber(event);"
                                                        name="txtMarkup" value="0" maxlength="5" /></div>
                                              
                                                <div class="fleft center width-100 margin-top-5">
                                                    <p id="emailStatus" style="color: #18407B; display: none;">
                                                    </p>
                                                </div>
                                                <div>
                                                    <input class="btn but_b" type="button" value="Submit" onclick="SendMail('email')" />                                                    
                                                </div>                                                
                                            </div>
                                            <div class=" clear">
                                            </div>
                                        </div>

                                        <div class="email-itenary" id="downloadBlock">
                                            <div class="fleft search-child-block" style="background-color: Red;">
                                                <div class="display-none yellow-back center" id="error">
                                                </div>
                                            </div>
                                             <div class="showMsgHeading">
                                                Download Selected Iitneraries <a class="closex" href="#" onclick="HideEmailDiv('downloadBlock');" style="position: absolute;
                                                    right: 5px; top: 3px;">X</a>
                                            </div>
                                            <div class="pad_10">
                                                <div id="itineraryCount1">
                                                    Selected 2 Itineraries for Download</div>
                                              
                                                

                                                <div style="display:none">
                                                    Add Markup:</div>
                                                <div style="display:none">
                                                    <input class="form-control" type="text" id="txtDownloadMarkup" onkeypress="return isNumber(event);"
                                                        name="txtDownloadMarkup" value="0" maxlength="5"  /></div>
                                                
                                                <div class="fleft center width-100 margin-top-5">
                                                    <p id="emailStatus" style="color: #18407B; display: none;">
                                                    </p>
                                                </div>
                                                <div>
                                                    <input class="btn but_b" type="button" value="Download" onclick="SelectResult()" />                                                    
                                                </div>                                              
                                            </div>
                                            <div class=" clear">
                                            </div>
                                        </div>
                                    </div>
                                    
                                    
                                   
                                   <div class="row no-gutters"> 
                                   
                                    <asp:Label ID="lblFareSummary" runat="server" Text=""></asp:Label>
                                    <input type="hidden" id='resultId-<%#Eval("ResultId") %>' value='<%#Eval("ResultId") %>' />
                                    <%--<div style="position: absolute; width: 600px;height:300px; font-size: 14px;
            top: 100px; left: 150px; display: none; border: solid 1px #DDB566; z-index:1; background-color: #FBEBCD;" id='fare_room_<%#Eval("ResultId") %>' >
                            <img src="images/close.gif" alt="Close" onclick="hide('fare_room_<%#Eval("ResultId") %>');" />
                            <div class="cnt091" style="width: 100%;height:100%;overflow:scroll;">
                           
                                <h3>
                                    Fare Rules</h3>             
                                <asp:Label ID="lblFareRule" runat="server" Text=""></asp:Label>                       
                                <table width="100%" border="0" cellpadding="0" cellspacing="0" class="hd_tabl" style="display:none">
                                    <tr>
                                        <td width="54%">
                                            AirLine
                                        </td>
                                        <td width="46%">
                                            <asp:Label ID="lblAirline" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Fare Basis Code
                                        </td>
                                        <td>
                                            <asp:Label ID="lblFareBasisCode" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Type of Fare
                                        </td>
                                        <td>
                                            <asp:Label ID="lblFareType" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Refund Type
                                        </td>
                                        <td>
                                            <asp:Label ID="lblRefundType" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cancellation Policy<br />
                                            <span class="term_s">{ Per Person Per Sector }</span><br />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCancellationText" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Re-issuance<br />
                                            <span class="term_s">{ Per Person Per Sector }</span><br />
                                        </td>
                                        <td>
                                            <asp:Label ID="lblReissuance" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Cancellation service Fee
                                        </td>
                                        <td>
                                            <asp:Label ID="lblCancelServiceFee" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            Rescheduling service Fee
                                        </td>
                                        <td>
                                            <asp:Label ID="lblReSchServiceFee" runat="server" Text=""></asp:Label>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </div>--%>
                      
                                    <div class="col-md-12 border_1 bg_white bor_gray pad0" style="margin-bottom:5px;">
                                    <div class="row custom-gutter"> 
                                        <div class="col-md-10 pad0 flightresultDiv" id="divMain" runat="server">
                                        </div>
                                        
                                         <div class="col-md-2">
                                        <%--  right box starts--%>
                                        <%if(request.Type == CT.BookingEngine.SearchType.OneWay){ %>
                                        <div class="paddingbot_10 intertrndfaitbox p-3 p-sm-0" >
                                        <%}else{ %>
                                        <div class="paddingbot_10 intertrndfaitbox p-3 p-sm-0" style="margin-top:10%;position:relative">
                                        <%} %>
                                          <%--  <div>--%>
                                                <div class="paddingbot_10 text-center text-lg-left">
                                                <%--    <label class="xslineh_40 font_med">--%>
                                                        <span class="flightprice">
                                                          <b>  <%=agency.AgentCurrency %>
                                                                <asp:Label ID="lblPrice" runat="server" Text=""></asp:Label>
                                                            </b>
                                                            </span> 
                                                  <%--  </label>--%>
                                                   <%-- <label class="hidden-xs" style="padding-left: 5px;">--%>
                                                        <a onmouseover="show('fare_summary_<%#Eval("ResultId") %>');" onmouseout="hide('fare_summary_<%#Eval("ResultId") %>');"
                                                            class="fare_summary hidden-xs cursor_point">
                                                            <img src="images/fare_summary.jpg" /></a>
                                                   <%-- </label>--%>
                                                </div>
                                                <div class="paddingbot_10 text-center text-lg-left">

                                                    

                                                  
                                      
                                                   


                                      

                                                    <a href="javascript:void(0);" type="button" class="btn but_b btn_xs_block cursor_point" data-toggle="modal" data-target="#TBOConfirm<%#Eval("ResultId") %>" style='<%#Eval("ResultBookingSource").ToString()=="TBOAir" || Eval("ResultBookingSource").ToString()=="PKFares" ?"display:block":"display:none" %>'>
                                                      Book Now 
                                                    </a>

                                        

                                                    <asp:HyperLink ID="btnBookNow" CssClass="btn but_b btn_xs_block cursor_point" Text="Book Now" runat="server" style='<%#Eval("ResultBookingSource").ToString()!="TBOAir" && Eval("ResultBookingSource").ToString()!="PKFares" ?"display:block":"display:none" %>' />
                                           


                                                </div>
			<!-- For TBO Validation -->
                                       
                                               <div class="modal fade" tabindex="-1" role="dialog" id="TBOConfirm<%#Eval("ResultId") %>">
                                                  <div class="modal-dialog" role="document">
                                                    <div class="modal-content">
                                                      <div class="modal-body">
                                                            VOID RESTRICTIONS APPLY. THIS TICKET CANNOT BE VOIDED
                                                      </div>
                                                      <div class="modal-footer text-center">
                                                         <a  class="btn btn-primary" onclick="ShowLoader(<%#Eval("ResultId") %>,'-1');">BOOK </a>
                                                      </div>
                                                    </div><!-- /.modal-content -->
                                                  </div><!-- /.modal-dialog -->
                                                </div><!-- /.modal -->
				<!-- END  TBO Validation -->

                                            
                                            <ul id='divTravelReason<%#Eval("ResultId") %>' class="dropdown-menu corp-travel-dropDown" aria-labelledby="dLabel">
                                            </ul>
                                            <div id='divTravelPopup<%#Eval("ResultId") %>' class="dropdown-menu corp-travel-dropDown" aria-labelledby="dLabel" style="display:none">
                                                <div class="row">
                                                    <div class="col-xs-12">
                                                        <div class="form-group">
                                                            <label>
                                                                Please Select Travel Reason</label>
                                                            <select id='ddlTravelReason<%#Eval("ResultId") %>'>
                                                            <option value="-1">Select Reason</option>
                                                            <% foreach (System.Data.DataRow row in dtTravelReason.Rows)
                                                                {%>
                                                                <option value='<%=row["ReasonId"].ToString() %>'><%=row["Description"].ToString()%></option>
                                                                <%} %>
                                                            </select>  
                                                            <span id="errorReason" class="errorMessageBlock" style="display: none">Invalid Reason
                                                                </span>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12">
                                                        <button type="button" class="btn btn-primary" onclick='BookNow(<%#Eval("ResultId") %>)'>
                                                            Continue</button>
                                                        <button type="button" class="btn btn-deafult cancel-btn" onclick='HidePopup(<%#Eval("ResultId") %>)'>
                                                            Cancel</button>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            <div class="flightResultDropdown-wrap">
                                              <div id="DivPrefAirline" runat="server" class="dropdown flightResultDropdown" style="display:none">
                                                  <button type="button" class="corp-icon-wrapper" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      <%--<span id="SpanPrefAirline" runat="server" class="glyphicon glyphicon-star" aria-hidden="true"></span>--%>
                                                      <asp:Image ID="ImgPreferredAirline" runat="server" />
                                                      
                                                  </button>
                                                  
                                              </div>
                                             <div id="DivInOut" runat="server" class="dropdown flightResultDropdown" style="display:none">
                                                  <button type="button" class="corp-icon-wrapper" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      <%--<span id="SpanInOut" runat="server" class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>--%>
                                                      <asp:Image ID="ImgInOutPolicy" runat="server" />
                                                     
                                                  </button>
                                                  
                                              </div>
                                             <div id="DivNegotiatedFare" class="dropdown flightResultDropdown" runat="server" style="display:none">
                                                  <button type="button" class="corp-icon-wrapper" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      <%--<span class="glyphicon glyphicon-check" aria-hidden="true"></span>--%>
                                                      <asp:Image ID="ImgNegotiateFare" ImageUrl="images/icon-negotiate.png" runat="server" />                                                      
                                                  </button>
                                                  
                                              </div>
                                              <div id="DivLLF" class="dropdown flightResultDropdown" runat="server" style="display:none">
                                                  <button type="button" class="corp-icon-wrapper" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      <asp:Image ID="Image13" style="max-width: 32px;" ImageUrl="images/llf-icon.png" runat="server" /> 
                                                  </button>                                                  
                                              </div>

                                                <div class="clearfix"></div>
                                                 <div class="corpPolicyTip" >
                                                   <span class="text-left py-2 d-block" runat="server" id="spnInOutPolicy">
                                                   </span>
                                                  
                                                 </div>
                                            </div>



                                           <%-- </div>--%>
                                           
                                           <%-- <div class="xslineh_40">--%>
                                                <div >
                                                    <%--<a class="fcol_black cursor_point" onclick="showStuff('ViewFlight<%#Eval("ResultId") %>');">View
                                                        Flight Details</a>--%>
                                                </div>
                                                <div >
                                                    <%--<a class="fcol_black cursor_point" href="Javascript:FareRule(<%# Eval("ResultId") %>,'<%#Session["sessionId"].ToString() %>')">
                                                        <span>Fare Rules</span></a>--%>
                                                </div>
                                                <div class="clearfix">
                                                </div>
                                                
                                                 <div>
                                                <table width="100%">
                                                    <%--<td width="50%">
                                                        <asp:Label ID="lblBookingSource" runat="server" Text="" Font-Bold="true"></asp:Label>
                                                    </td>
                                                    <td>
                                                        <label>
                                                            <input id='checkboxEmail-<%#Eval("ResultId")%>' onchange="selectBox(this.id)" type="checkbox" />
                                                            <img src="images/email_icon.png"></a> </label>
                                                    </td>--%>
                                                </table>
                                            </div>
                                            <%--</div>--%>
                                        </div>
                                        </div>
                                        <%--  right box ends--%>
                                        <%--<table style="display: none" class="repeat-table" width="100%" border="0" cellspacing="0"
                                            cellpadding="0">
                                            <%--firoz commented 29th june 2016 to discuss with ziyad-
                                            <tr>
                                                <td height="6px" colspan="6">
                                                    <marquee><asp:Label ID = "lblRemarks" runat ="server" Font-Size= "Smaller" ForeColor ="Red"  Text = "" ></asp:Label></marquee>
                                                    <hr class="b_bot_1 " />
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="6">
                                                    <marquee><asp:Label ID =  "lblRetRemarks" runat ="server" Font-Size= "Smaller" ForeColor ="Red" Text = ""></asp:Label></marquee>
                                                </td>
                                            </tr>
                                        </table>--%>
                                        
                                        <div class="clearfix">
                                        </div>
                                        
                                        
                                        </div>
                                        <div class="col-md-12 search_container py-1" style="vertical-align:middle;">
                                            <table width="100%" class="flight-info-band">
                                                <tr>
                                                    <td style="width: 20%;">
                                                        <a class="cursor_point fcol_fff" onclick="showStuff('ViewFlight<%#Eval("ResultId") %>','','');">
                                                            View Flight Details</a>
                                                    </td>
                                                    <td style="width: 10%;">
                                                        <a class="cursor_point fcol_fff" href="Javascript:FareRule(<%# Eval("ResultId") %>,'<%#Session["sessionId"].ToString() %>')">
                                                            <span>Fare Rules</span></a>
                                                    </td>
                                                    <td style="width:10%">                                                                                                                
                                                    <a class="cursor_point fcol_fff" onclick="showStuff('BaggageDiv<%# Eval("ResultId") %>','<%# Eval("BaggageIncludedInFare") %>','<%#Eval("ResultBookingSource").ToString() %>')">Baggage</a>                                                        
                                                    </td>
                                                 
                                                    <asp:Literal ID="lblTotalDuration" runat="server" Text="" ></asp:Literal>                                                       
                                                   
                                                    <td width="10%" align="right">
                                                        <asp:Label ID="lblBookingSource" runat="server" Text="" Font-Bold="true" CssClass="d-block">></asp:Label>
                                                        <!--Added by lokesh on 7-June-2018 -->
                                                        <!-- Need to display the additional label as Global Fares for PK Source -->
                                                        <!-- The below label should be displayed only if the booking source is PK Fares and only to the base agent -->                                              
                                                        <asp:Label ID="lblPKGlobalFares" runat="server" Text="Global Fares" Font-Bold="true" CssClass="d-block"></asp:Label>
                     
                                                    </td>
                                                    <td style="width: 5%;" align="right" valign="middle">
                                                        <label style="vertical-align:middle;">
                                                            <input id='checkboxEmail-<%#Eval("ResultId")%>' onchange="selectBox(this.id)" type="checkbox" />
                                                            <img src="images/email_icon.png"></a>
                                                        </label>
                                                    &nbsp;&nbsp;&nbsp;&nbsp;</td>
                                                </tr>
                                            </table>
                                            
                                        </div>
                                        <div class="clearfix">  </div>
                                        
                                    </div>
                                    <div class="col-md-12 border_1 marbot_10 bg_white pad0 viewflightdetails" id='ViewFlight<%#Eval("ResultId") %>' style="display: none;">
                                        <a name="view_fare_details3"></a>
                                        <div class="f_deatils_main">
                                            <div class="inn_head ">
                                                <div class="hding_96">
                                                    <span>Flight Details</span>
                                                    
                                                     <span style="position:absolute; right:0px; top:4px; z-index:9"><a class="cursor_point" title="close"
                                                        onclick="hidestuff('ViewFlight<%#Eval("ResultId") %>');">
                                                        <img src="images/close-icon5.png" alt="close"></a></span>
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                        
                                                </div>
                                                <table class="flight_details_1" id="flightDetails" width="100%" border="0" runat="server"
                                                    cellspacing="0" cellpadding="0">
                                                </table>
                                                <div class="clear">
                                                </div>
                                            </div>
                                            <div class="clear">
                                            </div>
                                        </div>
                                    </div>

                                        <div id="BaggageWaitDiv<%#Eval("ResultId") %>" class="col-md-12 border_1 bg_white" style="z-index: 9999; display: none; padding-top: -10px; position: relative; text-align: center; height: 100px;">
                                            <div class="ns-h3">                                                
                                                Please wait while we fetch Baggage Information
                                            </div>
                                            <img src="images/ajax-loader.gif" alt="spinner" />
                                        </div>
                                    
                                    <div class="col-md-12 border_1 marbot_10 bg_white pad0" id='BaggageDiv<%#Eval("ResultId") %>' style="display: none;">
                                    <div class="ns-h3"><span style=" position:absolute; right:10px; top:4px; z-index:9"><a class="cursor_point" title="close"
                                                        onclick="hidestuff('BaggageDiv<%#Eval("ResultId") %>');">
                                                        <img src="images/close-icon4.png" alt="close"></a></span>
                                    Baggage Information
                                    </div>
                                    
                                    <table class="mydatagrid" id="BaggageTable" width="100%" border="1" runat="server"
                                                    cellspacing="0" cellpadding="0">
                                                    <tr><th style='width: 50%'><strong>Sector / Flight</strong></th><th style='width: 50%'><strong>Baggage</strong></th></tr>
                                                </table>
                                        <div class="term_s"><span>*</span> The information presented above is as obtained from the airline reservation system. Travtrolly does not guarantee the accuracy of this information. The baggage allowance may vary according to stop-overs, connecting flights and changes in airline rules.</div>
                                    </div>
                                    
                                </div>
                                
                                
                                </div>
                            </ItemTemplate>
                        </asp:DataList>
                         <%if(filteredResults != null && filteredResults.Length > 0){ %>
                         <div class="pagination tt-paging pull-right mt-2">  <%=show%> </div> 
                         <%} %>
                    </div>
                    <div class="clear">
                    </div>
                </div>
            </div>
            <div class="clearfix">
            </div>
        </div>
        
        

    <!-- loading for flight end-->
        <div class="clear">
        </div>
    </div>

   
            
            <!-- loading for flight -->

    <div id="FlightPreLoader" style="width: 100%; z-index: 999; display: none;">
        <div class="loadingDiv">
            <div id="imgFlightLoading">
                <img id="imgPreloader" height="200px" src="https://gocozmo.com/images/preloaderFlight.gif" /></div>
            <div style="font-size: 16px; color: #999999">
                <strong>Please wait... </strong>
            </div>
            <div style="font-size: 21px;">
                We are searching for the best available flights</div>
            <div class="" id="onetwoDiv">
                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                        <td>
                            <table id="onewaytable" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="2%">
                                        <img src="images/departure_flight.png" width="16" height="15" />
                                    </td>
                                    <td width="98%" align="left">
                                        <label class="primary-color">
                                            <strong><span id="flightOnwards" class="primary-color"></span></strong>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="left">
                                        <span style="font-size: 21px" id="deptDate"></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table id="twowaytable" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td width="2%">
                                        <img src="images/arrival_flight.png" width="16" height="15" />
                                    </td>
                                    <td width="98%" align="left">
                                        <label class="primary-color">
                                            <strong><span id="flightReturn" class="primary-color"></span></strong>
                                        </label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        &nbsp;
                                    </td>
                                    <td align="left">
                                        <span style="font-size: 21px" id="arrivalDate"></span>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="" id="multiDiv" style="display: none;">
                <center>
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td>
                                <table id="multiwayable1" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="2%">
                                            <img src="images/departure_flight.png" width="16" height="15" />
                                        </td>
                                        <td width="98%" align="left">
                                            <label class="primary-color">
                                                <strong><span id="multiway1"></span></strong>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            <span style="font-size: 21px" id="multiDate1"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table id="multiwayable2" width="90%" border="0" align="right" cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="2%">
                                            <img src="images/departure_flight.png" width="16" height="15" />
                                        </td>
                                        <td width="98%" align="left">
                                            <label class="primary-color">
                                                <strong><span id="multiway2"></span></strong>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            <span style="font-size: 21px" id="multiDate2"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="multiwayable3" style="display: none;" width="90%" border="0" align="right"
                                    cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="2%">
                                            <img src="images/departure_flight.png" width="16" height="15" />
                                        </td>
                                        <td width="98%" align="left">
                                            <label class="primary-color">
                                                <strong><span id="multiway3"></span></strong>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            <span style="font-size: 21px" id="multiDate3"></span>
                                        </td>
                                    </tr>
                                </table>
                                <td>
                                    <table id="multiwayable4" style="display: none;" width="90%" border="0" align="right"
                                        cellpadding="0" cellspacing="0">
                                        <tr>
                                            <td width="2%">
                                                <img src="images/departure_flight.png" width="16" height="15" />
                                            </td>
                                            <td width="98%" align="left">
                                                <label class="primary-color">
                                                    <strong><span id="multiway4"></span></strong>
                                                </label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>
                                                &nbsp;
                                            </td>
                                            <td align="left">
                                                <span style="font-size: 21px" id="multiDate4"></span>
                                            </td>
                                        </tr>
                                    </table>
                                </td>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table id="multiwayable5" style="display: none;" width="90%" border="0" align="right"
                                    cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="2%">
                                            <img src="images/departure_flight.png" width="16" height="15" />
                                        </td>
                                        <td width="98%" align="left">
                                            <label class="primary-color">
                                                <strong><span id="multiway5"></span></strong>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            <span style="font-size: 21px" id="multiDate5"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td>
                                <table id="multiwayable6" style="display: none;" width="90%" border="0" align="right"
                                    cellpadding="0" cellspacing="0">
                                    <tr>
                                        <td width="2%">
                                            <img src="images/departure_flight.png" width="16" height="15" />
                                        </td>
                                        <td width="98%" align="left">
                                            <label class="primary-color">
                                                <strong><span id="multiway6"></span></strong>
                                            </label>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                        <td align="left">
                                            <span style="font-size: 21px" id="multiDate6"></span>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </center>
            </div>
        </div>
    </div>
    <div id="PreLoader" style=" width:100%;  z-index:999; top:0px;  display:none;">
        <div class="loadingDiv">
    <div id="Div2"> <img id="img1" height="200px" src="<%=Request.Url.Scheme%>://gocozmo.com/images/preloaderFlight.gif" /></div>

    <div style="font-size:16px; color:#999999 "><strong> Please wait... </strong></div>
    <div style="font-size:21px; ">We're re-confirming availability on the chosen flights...</div>


    </div>
    </div>
     <asp:UpdatePanel ID="upLoadCorp" runat="server" >
         <Triggers>
             <asp:PostBackTrigger ControlID="btnSearchFlight" />             
         </Triggers>
         <ContentTemplate>
             <script>
                 AssignCorpData();
             </script>
         </ContentTemplate>
     </asp:UpdatePanel>

    <% if (request != null && request.Segments != null)
       {
             
    %>

    <script>
        disablefield();  
    </script>

    <%
        if (request.Type == CT.BookingEngine.SearchType.MultiWay)
        {%>
    <script>
        var rows = eval('<%=(request.Segments != null && request.Segments.Length > 2 ? (request.Segments.Length-2) : 0) %>');
        if (rows > 0) {
            for (var i = 0; i < rows; i++) {
                addRow();
            }
        }          
    </script>

    <%}
       }
    %>

    <script src="Scripts/select2.min.js" type="text/javascript"></script>

    <script type="text/javascript">
        $(function() {
            $('[data-toggle="tooltip"]').tooltip({ tooltipClass: "custom-tooltip-style", position: { my: "left top+15", at: "left bottom-10", collision: "flipfit"} })
        })

        /*
         * Function for Selecting all results irrespective of pagination based on the result indexes
         * and checked indexes will be stored in ctl00_cphTransaction_hdnCheckboxEmail
         * */
        function SelectAll() {           

            var chkAll = document.getElementById('chkDownAll');            
            var hdnArray = '';
            var hdnField = document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail');
            hdnField.value = "";
            if (chkAll.checked) {

                if (document.getElementById("ctl00_cphTransaction_hdnFilter").value == "ClearFilter") {
                    var filteredIds = '<%=resultIds%>'
                    var ids = filteredIds.split(',');
                    for (var i = 0; i < ids.length; i++) {
                        var index = ids[i];
                        if (document.getElementById('checkboxEmail-' + index) != null) {
                            document.getElementById('checkboxEmail-' + index).checked = chkAll.checked;
                            //console.log('WO Filter Result:' + index + ' Checked');
                        }
                        else {
                            //console.log('WO Filter Result:' + index + ' Checked');
                        }

                        if (hdnField.value != "") {
                            hdnArray = hdnField.value.split(',');
                            if (hdnArray.indexOf('' + index) == -1) hdnField.value += ',' + index;
                        }
                        else {
                            hdnField.value = index;
                        }
                    }
                }
                //If Any Filter selected
                else {
                    
                    var filteredIds = '<%=resultIds%>'
                    var ids = filteredIds.split(',');
                    for (var i = 0; i < ids.length; i++) {
                        var index = ids[i];
                        if (document.getElementById('checkboxEmail-' + index) != null) {
                            document.getElementById('checkboxEmail-' + index).checked = chkAll.checked;
                            //console.log('Filtered Result:' + index + ' Checked');
                        }
                        else {
                            //console.log('Filtered Result:' + index + ' not Checked');
                        }

                        if (hdnField.value != "") {
                            hdnArray = hdnField.value.split(',');
                            if (hdnArray.indexOf('' + index) == -1) hdnField.value += ',' + index;
                        }
                        else {
                            hdnField.value = index;
                        }
                    }
                }
            }
            else {
                hdnField.value = "";
                var filteredIds = '<%=resultIds%>'
                    var ids = filteredIds.split(',');
                for (var i = 1; i <= ids.length; i++) {
                    if (document.getElementById('checkboxEmail-' + i) != null) {
                        document.getElementById('checkboxEmail-' + i).checked = chkAll.checked;
                        //console.log('Unchecking Result: ' + index);
                    }
                }
            }
        }

        //@@@@ add for selected items checked. chandan 14/12/2015
        $(document).ready(function () {
            BindItineraryCheckBoxes();            
        });

        /*
         * function for selecting the checkboxes in the current page when navigated using pagination after SelectAll is performed
         * */
        function BindItineraryCheckBoxes() {
            if (document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value.length > 0) {
                var hdnCheckedBox = document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value.split(',');
                var chkAll = document.getElementById('chkDownAll');
                var filteredIds = '<%=resultIds%>'
                var ids = filteredIds.split(',');
                var resultCount = ids.length;

                if (resultCount == hdnCheckedBox.length) {
                    chkAll.checked = true;
                }

                for (var j = 0; j < resultCount; j++) {
                    var index = hdnCheckedBox[j].trim();
                    if (document.getElementById('checkboxEmail-' + index) != null) {
                        document.getElementById('checkboxEmail-' + index).checked = true;
                    }
                }
            }
        }

        var prefAirlines = '<%=preferredAirlines %>';        
        
        if (prefAirlines.split(',').length > 1) {//additional preferred airlines are added then add them for modify search

            if (prefAirlines.split(',').length >= 2) {
                document.getElementById('removeRowLink0').style.display = 'block'; //show remove link
                document.getElementById('txtPreferredAirline0').value = prefAirlines.split(',')[1];
                document.getElementById('divPrefAirline0').style.display = 'block';
                prow = 1;
            }
            if (prefAirlines.split(',').length >= 3) {
                document.getElementById('removeRowLink1').style.display = 'block'; //show remove link
                document.getElementById('txtPreferredAirline1').value = prefAirlines.split(',')[2];
                document.getElementById('divPrefAirline1').style.display = 'block';
                prow = 2;
            }
            if (prefAirlines.split(',').length >= 4) {
                document.getElementById('removeRowLink2').style.display = 'block'; //show remove link
                document.getElementById('txtPreferredAirline2').value = prefAirlines.split(',')[3];
                document.getElementById('divPrefAirline2').style.display = 'block';
                document.getElementById('addRowLink').style.display = 'none'; //hide add link
                prow = 3;
            }
            
        }
        //Dropdown closing 
       
        $('.flightResultDropdown .dropdown-menu').bind('click', function (e) {
            e.stopPropagation();
        })
        $('.cancel-btn').click(function () {
            $('.flightResultDropdown ').removeClass('open');
        });
        
        $('.select-element').select2();

//        $('.flight-sort-btn').each(function() {
//            $(this).click(function() {
//                $(this).find('span').toggleClass('active');               
//                
//            })
//        });

        /******************************************************************************
        *        Clear filter code 
        *******************************************************************************/
        function ClearFilter() {
            document.getElementById("ctl00_cphTransaction_ddlPrice").value = "Lowest";
            document.getElementById("ctl00_cphTransaction_ddlStops").value = "-1";
            document.getElementById("ctl00_cphTransaction_ddlDeparture").value = "-1";
            document.getElementById("ctl00_cphTransaction_ddlArrival").value = "-1";
            document.getElementById("ctl00_cphTransaction_ddlAirline").value = "-1";
            document.getElementById("ctl00_cphTransaction_ddlAirline").value = "-1";            
            document.getElementById('<%=ddlTimings.ClientID %>').value = "0";
            document.getElementById("ctl00_cphTransaction_chkAirline").checked = true;
            if (document.getElementById("chkDownAll") != null) {
                document.getElementById("chkDownAll").checked = false;
                SelectAll();//Unselect all checkboxes when filtered cleared
            }
            /*
                Set all airlines checkboxes checked = True on clear by default
            */
            var chkBoxList = getElement('chkListAirlines');
            if (chkBoxList != null) {
                var chkBoxCount = chkBoxList.getElementsByTagName("input");
                if (chkBoxCount.length > 0) {
                    for (var i = 0; i < chkBoxCount.length; i++) {
                        if (document.getElementById('<%=chkAirline.ClientID %>').checked == true) {
                            chkBoxCount[i].checked = true;
                        }
                        else {
                            chkBoxCount[i].checked = false;
                        }
                    }
                }
            }
            /*
                        End airlines checkboxes checking code
            */
            if ($('#ctl00_cphTransaction_chkRefundable').is(":visible")) {
            <%if(request.Sources.Count == 1 && request.Sources[0] != "PK") {%>             
                document.getElementById('ctl00_cphTransaction_chkRefundable').checked = true;
        <%} else {%>
                document.getElementById('ctl00_cphTransaction_chkRefundable').checked = true;
            <%}%>
            }
        <%if (!request.RefundableFares) { %>
            var NonRefundable = $('#ctl00_cphTransaction_chkNonRefundable').is(":visible");
            if (NonRefundable) {
                document.getElementById('ctl00_cphTransaction_chkNonRefundable').checked = true;
            }
        <%}%>    
         <%if (request.MaxStops == "-1" || request.MaxStops =="")
        {%>
             var nonstopstatus = $('#ctl00_cphTransaction_chkNonStop').is(":visible");
            var onestopstatus = $('#ctl00_cphTransaction_chkOneStop').is(":visible");
            var twostopstatus = $('#ctl00_cphTransaction_chkTwoStops').is(":visible");
            if (nonstopstatus) {
                document.getElementById('ctl00_cphTransaction_chkNonStop').checked = true;
            }
            if (onestopstatus) {
                document.getElementById('ctl00_cphTransaction_chkOneStop').checked = true;
            }
            if (twostopstatus) {
                document.getElementById('ctl00_cphTransaction_chkTwoStops').checked = true;
            }
            <%}else if(request.MaxStops=="0" && chkNonStop.Visible) {%>
            document.getElementById('ctl00_cphTransaction_chkNonStop').checked = true;
            <%}else if(request.MaxStops=="1" && chkOneStop.Visible) {%>
            document.getElementById('ctl00_cphTransaction_chkOneStop').checked = true;            
            <%}else if(request.MaxStops=="2" && chkTwoStops.Visible) {%>
            document.getElementById('ctl00_cphTransaction_chkTwoStops').checked = true;
            <%}%>
            document.getElementById('ctl00_cphTransaction_PageNoString').value = "1";            
            document.getElementById("ctl00_cphTransaction_hdnFilter").value = "ClearFilter";            
            document.getElementById("ctl00_cphTransaction_Change").value = "false";
            //Destroy slider and recreate it again
            $("#slider-range").slider("destroy");
            $("#slider-range").slider({
                range:true,
                min: parseFloat(document.getElementById('ctl00_cphTransaction_hdnPriceMin').value),
                max: parseFloat(document.getElementById('ctl00_cphTransaction_hdnPriceMax').value),
                step: 1,
                values: [parseFloat(document.getElementById('<%=hdnPriceMin.ClientID %>').value), parseFloat(document.getElementById('<%=hdnPriceMax.ClientID %>').value)]
            });
            //Attach slider change event
            $("#slider-range").slider({
                change: function() {
                    var duration = document.getElementById('amount').value;
                    var min = duration.split('-')[0];
                    var max = duration.split('-')[1];
                    document.getElementById('<%=hdnPriceMin.ClientID %>').value = min;
                    document.getElementById('<%=hdnPriceMax.ClientID %>').value = max;
                    document.getElementById('<%=Change.ClientID %>').value = "false";
                    document.getElementById('<%=hdnFilter.ClientID %>').value = "Price";
                    document.getElementById('label').InnerHTML = max;
                    document.forms[0].submit();                    
                }
            });

            $('#ctl00_cphTransaction_hdnOnTimingFilter').val('Any Time');
            $('#ctl00_cphTransaction_hdnRetTimingFilter').val('Any Time');

            document.forms[0].submit(); 
        }

        $("#priceSort").click(function() {
        $(this).find('span').toggleClass('active');
        document.getElementById("ctl00_cphTransaction_ddlStops").value = "-1";
        document.getElementById("ctl00_cphTransaction_ddlDeparture").value = "-1";
        document.getElementById("ctl00_cphTransaction_ddlAirline").value = "-1";
        document.getElementById("ctl00_cphTransaction_ddlArrival").value = "-1";
            if ($('#priceSort').find('span').hasClass('active')) {
                document.getElementById("ctl00_cphTransaction_ddlPrice").value = "Highest";
                FilterResults('PriceSort');
            }
            else {
                document.getElementById("ctl00_cphTransaction_ddlPrice").value = "Lowest";
                FilterResults('PriceSort');
            }
        });

        $("#airlineSort").click(function() {
            $(this).find('span').toggleClass('active');
            document.getElementById("ctl00_cphTransaction_ddlStops").value = "-1";
            document.getElementById("ctl00_cphTransaction_ddlDeparture").value = "-1";
            document.getElementById("ctl00_cphTransaction_ddlArrival").value = "-1";
            if ($('#airlineSort').find('span').hasClass('active')) {
                document.getElementById("ctl00_cphTransaction_ddlAirline").value = "1";
                FilterResults('PriceSort');
            }
            else {
                document.getElementById("ctl00_cphTransaction_ddlAirline").value = "0";
                FilterResults('PriceSort');
            }
        });

        $("#stopsSort").click(function() {
            $(this).find('span').toggleClass('active');
            document.getElementById("ctl00_cphTransaction_ddlAirline").value = "-1";
            document.getElementById("ctl00_cphTransaction_ddlDeparture").value = "-1";
            document.getElementById("ctl00_cphTransaction_ddlArrival").value = "-1";
            if ($('#stopsSort').find('span').hasClass('active')) {
                document.getElementById("ctl00_cphTransaction_ddlStops").value = "1";
                FilterResults('PriceSort');
            }
            else {
                document.getElementById("ctl00_cphTransaction_ddlStops").value = "0";
                FilterResults('PriceSort');
            }
        });

        $("#departureSort").click(function() {
            $(this).find('span').toggleClass('active');
            document.getElementById("ctl00_cphTransaction_ddlStops").value = "-1";
            document.getElementById("ctl00_cphTransaction_ddlAirline").value = "-1";
            document.getElementById("ctl00_cphTransaction_ddlArrival").value = "-1";
            if ($('#departureSort').find('span').hasClass('active')) {
                document.getElementById("ctl00_cphTransaction_ddlDeparture").value = "1";
                FilterResults('PriceSort');
            }
            else {
                document.getElementById("ctl00_cphTransaction_ddlDeparture").value = "0";
                FilterResults('PriceSort');
            }
        });

        $("#arrivalSort").click(function() {
            $(this).find('span').toggleClass('active');
            document.getElementById("ctl00_cphTransaction_ddlStops").value = "-1";
            document.getElementById("ctl00_cphTransaction_ddlDeparture").value = "-1";
            document.getElementById("ctl00_cphTransaction_ddlAirline").value = "-1";
            if ($('#arrivalSort').find('span').hasClass('active')) {
                document.getElementById("ctl00_cphTransaction_ddlArrival").value = "1";
                FilterResults('PriceSort');
            }
            else {
                document.getElementById("ctl00_cphTransaction_ddlArrival").value = "0";
                FilterResults('PriceSort');
            }
        });


        /*       Setting Up/Down arrows code based on sorting selection             */
        if (document.getElementById("ctl00_cphTransaction_ddlPrice").value == "Highest") {
            $('#priceSort').find('span').addClass('active');
        }
        else {
            $('#priceSort').find('span').removeClass('active');
        }

        if (document.getElementById("ctl00_cphTransaction_ddlAirline").value == "1") {
            $('#airlineSort').find('span').addClass('active');
        }
        else {
            $('#airlineSort').find('span').removeClass('active');
        }
        if (document.getElementById("ctl00_cphTransaction_ddlDeparture").value == "1") {
            $('#departureSort').find('span').addClass('active');
        }
        else {
            $('#departureSort').find('span').removeClass('active');
        }
        if (document.getElementById("ctl00_cphTransaction_ddlArrival").value == "1") {
            $('#arrivalSort').find('span').addClass('active');
        }
        else {
            $('#arrivalSort').find('span').removeClass('active');
        }
        if (document.getElementById("ctl00_cphTransaction_ddlStops").value == "1") {
            $('#stopsSort').find('span').addClass('active');
        }
        else {
            $('#stopsSort').find('span').removeClass('active');
        }

        if (document.getElementById("ctl00_cphTransaction_hdnFilter").value == "TD") {
            document.getElementById('durationSort').value = "By Journey Time";
        }
        else if (document.getElementById("ctl00_cphTransaction_hdnFilter").value == "FD") {
            document.getElementById('durationSort').value = "By Flying Duration";
        }
        
        /*                          End Sorting selection                                */

        function ShowOnlyThis() {            
        var count =eval('<%=airlineList.Count %>');
            for (var i = 0; i < count; i++) {
                var item = "ctl00_cphTransaction_chkListAirlines_" + i;
                if (document.getElementById(item) != null) {
                    var anchor = document.createElement("A");
                    var text = document.createTextNode("Only This");
                    anchor.setAttribute("id", "onlythis" + i);
                    anchor.setAttribute("class", "onlythis");
                    anchor.setAttribute("href", "#");
                    anchor.setAttribute("onclick", "OnlyThis('" + item + "','" + count + "')");
                    anchor.appendChild(text);
                    var chk = document.getElementById(item);
                    if (chk.parentNode.childNodes.length == 2) {
                        chk.parentNode.appendChild(anchor);
                    }
                }
            }
        }
        ShowOnlyThis();

        /*                  Timing Range filtering code                     */
        //Hightlighting the timing selection before postback
        function FilterTiming(timing) {
            <%if (!request.TimeIntervalSpecified)
        {%> 
        if (timing == "Night") {
            document.getElementById('<%=ddlTimings.ClientID %>').value = "6";
                document.getElementById('btnNight').style.backgroundColor = "#f1f1f1";
                document.getElementById('btnMorning').style.backgroundColor = "#ffffff";
                document.getElementById('btnEvening').style.backgroundColor = "#ffffff";
                document.getElementById('btnNoon').style.backgroundColor = "#ffffff";
            }
            else if (timing == "Morning") {
                document.getElementById('<%=ddlTimings.ClientID %>').value = "1";
                document.getElementById('btnNight').style.backgroundColor = "#ffffff";
                document.getElementById('btnMorning').style.backgroundColor = "#f1f1f1";
                document.getElementById('btnEvening').style.backgroundColor = "#ffffff";
                document.getElementById('btnNoon').style.backgroundColor = "#ffffff";
            }
            else if (timing == "Noon") {
                document.getElementById('<%=ddlTimings.ClientID %>').value = "2";
                document.getElementById('btnNight').style.backgroundColor = "#ffffff";
                document.getElementById('btnMorning').style.backgroundColor = "#ffffff";
                document.getElementById('btnEvening').style.backgroundColor = "#ffffff";
                document.getElementById('btnNoon').style.backgroundColor = "#f1f1f1";
            }
            else if (timing == "Evening") {
                document.getElementById('<%=ddlTimings.ClientID %>').value = "4";
                document.getElementById('btnNight').style.backgroundColor = "#ffffff";
                document.getElementById('btnMorning').style.backgroundColor = "#ffffff";
                document.getElementById('btnEvening').style.backgroundColor = "#f1f1f1";
                document.getElementById('btnNoon').style.backgroundColor = "#ffffff";
            }
            FilterResults('Time'); 
            <%}%>
        }
        <%if (!request.TimeIntervalSpecified)
        {%> 
        //Hightlighting the timing selection after postback
        if (document.getElementById('<%=ddlTimings.ClientID %>').value == "6") {
            document.getElementById('btnNight').style.backgroundColor = "#f1f1f1";
            document.getElementById('btnMorning').style.backgroundColor = "#ffffff";
            document.getElementById('btnEvening').style.backgroundColor = "#ffffff";
            document.getElementById('btnNoon').style.backgroundColor = "#ffffff";
        }
        else if (document.getElementById('<%=ddlTimings.ClientID %>').value == "1") {
            document.getElementById('btnNight').style.backgroundColor = "#ffffff";
            document.getElementById('btnMorning').style.backgroundColor = "#f1f1f1";
            document.getElementById('btnEvening').style.backgroundColor = "#ffffff";
            document.getElementById('btnNoon').style.backgroundColor = "#ffffff";
        }
        else if (document.getElementById('<%=ddlTimings.ClientID %>').value == "2") {
            document.getElementById('btnNight').style.backgroundColor = "#ffffff";
            document.getElementById('btnMorning').style.backgroundColor = "#ffffff";
            document.getElementById('btnEvening').style.backgroundColor = "#ffffff";
            document.getElementById('btnNoon').style.backgroundColor = "#f1f1f1";
        }
        else if (document.getElementById('<%=ddlTimings.ClientID %>').value == "4") {
            document.getElementById('btnNight').style.backgroundColor = "#ffffff";
            document.getElementById('btnMorning').style.backgroundColor = "#ffffff";
            document.getElementById('btnEvening').style.backgroundColor = "#f1f1f1";
            document.getElementById('btnNoon').style.backgroundColor = "#ffffff";
        }
        else {
            document.getElementById('btnNight').style.backgroundColor = "#ffffff";
            document.getElementById('btnMorning').style.backgroundColor = "#ffffff";
            document.getElementById('btnEvening').style.backgroundColor = "#ffffff";
            document.getElementById('btnNoon').style.backgroundColor = "#ffffff";
        }
        <%}%>
        /*                          End Timing Range                        */

        function SelectResult() {
            if (document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value.length > 0) {
                HideEmailDiv('downloadBlock');
                window.location.href = "ExcelDownload?ids=" + document.getElementById('ctl00_cphTransaction_hdnCheckboxEmail').value + "&markup=" + (document.getElementById('txtDownloadMarkup').value.trim().length > 0 ? document.getElementById('txtDownloadMarkup').value : "0");
            }
            else {
                document.getElementById('emailStatus').innerHTML = 'Please select at least one Itinerary';
                document.getElementById('emailStatus').style.display = 'block';
            }
        }
        var loc;
        function LoadAgentLocations() {
        var location = 'ctl00_cphTransaction_ddlAirAgentsLocations';
       loc=location;
            var sel = document.getElementById('ctl00_cphTransaction_ddlAirAgents').value;
       
        var paramList = 'requestSource=getAgentsLocationsByAgentId&AgentId=' + sel + '&id=' + location;;
        var url = "CityAjax?"+paramList;

            var agentId = $('#<%=ddlAirAgents.ClientID %>').val();
            $('#'+location).select2('val', '-1');//Clear selected value
            $('#<%=ddlFlightTravelReasons.ClientID%>').select2('val', '-1');
            $('#<%=ddlFlightEmployee.ClientID%>').select2('val', 'Select');

            if ($('#<%=ddlAirAgents.ClientID %> option:selected').text().indexOf('(') == 0 && $('#<%=ddlAirAgents.ClientID %> option:selected').text().lastIndexOf('(') == 0) {
                $('#CorpDiv').show();
                $('#travellerDropdown').parent(':first-child').addClass('form-control-holder form-control-element with-custom-dropdown disabled');
                $('#divSearchOptions').hide();                
                LoadCorpDropDownForOBAgent(agentId, 'E');
                LoadCorpDropDownForOBAgent(agentId, 'T');
            }
            else {
                $('#CorpDiv').hide();
                $('#travellerDropdown').parent(':first-child').removeClass('disabled');
                $('#divSearchOptions').show();
                LoadCorpDropDownForOBAgent(agentId, '');//To Load Time Intervals
            }

        if (window.XMLHttpRequest) {
            Ajax = new XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        Ajax.onreadystatechange = BindLocationsList;
        Ajax.open('POST', url);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send();
    }
        function BindLocationsList() {
        
            if (Ajax.readyState == 4 && Ajax.status == 200)  
            {
                var ddl = document.getElementById(loc);
                if(Ajax.responseText.length > 0) {                
                    if (ddl != null) {
                    ddl.options.length = 0;
                    var el = document.createElement("option");
                    el.textContent = "Select Location";
                    el.value = "-1";
                    ddl.add(el, 0);
                    var values = Ajax.responseText.split('#')[1].split(',');
                    for (var i = 0; i < values.length; i++) {
                        var opt = values[i];
                        if (opt.length > 0 && opt.indexOf('|') > 0) {
                            var el = document.createElement("option");
                            el.textContent = opt.split('|')[0];
                            el.value = opt.split('|')[1];
                            ddl.appendChild(el);
                        }
                    }
                    

                    <%if (Settings.LoginInfo.OnBehalfAgentLocation > 0)
                    {%>
                        $('#' + ddl.id).select2('val', '<%=Settings.LoginInfo.OnBehalfAgentLocation.ToString()%>');
                    <%}else{%>
                        $('#' + ddl.id).select2('val', '-1');//Select Location                
                    <%} %>
                    }                
                }
                else//Clear previous agent locations if no locations found for the current agent
                {
                    if(ddl != null)
                    {
                        ddl.options.length=0;
                        var el = document.createElement("option");
                        el.textContent = "Select Location";
                        el.value = "-1";
                        ddl.add(el, 0);
                        $('#' + ddl.id).select2('val', '-1');//Select Location
                    }
                }
            }
        }

        
       <%if(Settings.LoginInfo.IsOnBehalfOfAgent){%>
        $('#<%=ddlAirAgents.ClientID%>').change(function () {
            $("#<%=ddlAirAgents.ClientID%> option:selected").each(function () {
                if ($(this).text().indexOf("(") == 0 && $(this).text().lastIndexOf("(") == 0) {
                    $('#CorpDiv').show();
                    LoadCorpDropDownForOBAgent($(this).val(), 'E');
                    LoadCorpDropDownForOBAgent($(this).val(), 'T');
                }
                else {
                    $('#CorpDiv').hide();                    
                }
            });
        });
        LoadAgentLocations();
        $('#ctl00_cphTransaction_ddlAirAgentsLocations').select2('val', <%=Settings.LoginInfo.OnBehalfAgentLocation%>);
        <%}%>
        <%if(!IsPostBack){%>              
        document.getElementById("ctl00_cphTransaction_hdnFilter").value = "ClearFilter";
        <%}%>
        function pageLoad() { 
            var refundstatus = $('#ctl00_cphTransaction_chkRefundable').is(":visible");
            var nonrefundstatus = $('#ctl00_cphTransaction_chkNonRefundable').is(":visible");
            var nonstopstatus = $('#ctl00_cphTransaction_chkNonStop').is(":visible");
            var onestopstatus = $('#ctl00_cphTransaction_chkOneStop').is(":visible");
            var twostopstatus = $('#ctl00_cphTransaction_chkTwoStops').is(":visible");
            if (refundstatus) {
                $('#trRefundable').show();
            }
            else {
                $('#trRefundable').hide();
            }
            if (nonrefundstatus) {
                $('#trNonRefundable').show();
            }
            else {
                $('#trNonRefundable').hide();
            }
            if (nonstopstatus) {
                $('#trNonStop').show();
            }
            else {
                $('#trNonStop').hide();
            }
            if (onestopstatus) {
                $('#trOneStop').show();
            }
            else {
                $('#trOneStop').hide();
            }
            if (twostopstatus) {
                $('#trTwoStops').show();
            }
            else {
                $('#trTwoStops').hide();
            } 
        }
     
         //Function which disables the remaining sources checkbox selection 
         //Enables only the FI Source.
        var sources = ['FI'];
        function UnSelectNonVisaSources() { 
            
            var isChecked = $("#chkVisaEnable").is(":checked");
            $('#Additional-Div input[type=checkbox]').each(function () {
                var id = $(this).attr('id');
                var src = $('#' + id).next('label').text().toUpperCase().trim();
                if (sources.includes(src) && isChecked) {
                    $('#' + id).prop("checked", true); 
                    $('#' + id).prop("disabled", true);
                }
                else if (isChecked)
                {
                      $('#' + id).prop("checked", false);
                      $('#' + id).prop("disabled", true);
                }
                else {
                    $('#' + id).prop("checked", true);
                     $('#' + id).prop("disabled", false);
                }
            });
        }

        

        function CheckForVisaChange() {
            EnableVisaChange();
            
            if (<%=(Settings.LoginInfo.IsCorporate == "N").ToString().ToLower()%> && <%=(Settings.LoginInfo.Currency == "AED").ToString().ToLower()%>) {
                var onlyFI = false;
                var totalChecked = $('input[id^=ctl00_cphTransaction_chkSuppliers]:checked');
                var isVisible = $("#divVisaEnable").css("display");
                console.log("VC " + isVisible);
                if (totalChecked.length == 1) {
                    $(totalChecked).each(function () {
                        var id = $(this).attr('id');
                        var src = $('#' + id).next('label').text().toUpperCase().trim();
                        if (sources.includes(src))
                            onlyFI = true;
                    });
                }
                if (onlyFI && isVisible=='block') {//If only FI is checked then Set checked Visa Change
                    console.log('VC Visible');
                    $('#chkVisaEnable').prop("checked", true);
                    UnSelectNonVisaSources();
                }
                else
                    EnableAllSources();
            }            
        }

        function FilterTimingResults() {
            $("#ctl00_cphTransaction_hdnOnTimingFilter").val($("#ctl00_cphTransaction_ddlOnwardTimings").val());
            $("#ctl00_cphTransaction_hdnRetTimingFilter").val($("#ctl00_cphTransaction_ddlReturnTimings").val());

            FilterResults('Time');
        }

        window.onload = CheckForVisaChange();    

         $(document).ready(AssignCorpData);
        
        var prm = Sys.WebForms.PageRequestManager.getInstance();

        prm.add_endRequest(function () {
            // re-bind your jQuery events here
            AssignCorpData();

        });


       
            

         $("#<%=ddlFlightEmployee.ClientID%>").change(function () {
            $("#<%=ddlFlightEmployee.ClientID%> option:selected").each(function () {
                $('#<%=hdnFlightEmployee.ClientID%>').val($(this).val());
            });
        });
        $("#<%=ddlFlightTravelReasons.ClientID%>").change(function () {
            $("#<%=ddlFlightTravelReasons.ClientID%> option:selected").each(function () {
                $('#<%=hdnTravelReason.ClientID%>').val($(this).val());
            });
        });

        function LoadCorpDropDownForOBAgent(agentId, type) {
            $('#<%=ddlFlightTravelReasons.ClientID%>').select2('val', '-1');
            $('#<%=ddlFlightEmployee.ClientID%>').select2('val', 'Select');
            console.log('Reason:' + $('#<%=ddlFlightTravelReasons.ClientID%>').val() + ', profile:' + $('#<%=ddlFlightEmployee.ClientID%>').val());
            $.ajax({
                type: "POST",
                url: "CommonWebMethods.aspx/GetCorporateDropDownValues",
                contentType: "application/json; charset=utf-8",
                data: "{'agentId':'" + agentId + "','valueType':'" + type + "'}",
                dataType: "json",
                async: true,
                success: function (data) {
                    if (data.d != undefined) {
                        var ddlFTReasons = document.getElementById('<%=ddlFlightTravelReasons.ClientID%>');
                        var ddlFEmployees = document.getElementById('<%=ddlFlightEmployee.ClientID%>');

                        var cvalues = data.d.split(',');

                        if (type == 'E') {
                            ddlFEmployees.options.length = 0;
                            var el = document.createElement("option");
                            el.textContent = "Select Traveller / Employee";
                            el.value = "Select";
                            ddlFEmployees.add(el, 0);
                            
                            for (var i = 0; i < cvalues.length; i++) {
                                var opt = cvalues[i];
                                if (opt.length > 0 && opt.indexOf('#') > 0) {
                                    var el = document.createElement("option");
                                    el.textContent = opt.split('#')[1];
                                    el.value = opt.split('#')[0];
                                    ddlFEmployees.appendChild(el);
                                }
                            }
                        }
                        else {
                            ddlFTReasons.options.length = 0;
                            var el = document.createElement("option");
                            el.textContent = "Select Reason For Travel";
                            el.value = "-1";
                            ddlFTReasons.add(el, 0);
                            
                            for (var i = 0; i < cvalues.length; i++) {
                                var opt = cvalues[i];
                                if (opt.length > 0 && opt.indexOf('#') > 0) {
                                    var el = document.createElement("option");
                                    el.textContent = opt.split('#')[1];
                                    el.value = opt.split('#')[0];
                                    ddlFTReasons.appendChild(el);
                                }
                            }                            
                        }
                    }
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
            });             
            
            //Binding the Time Intervals for Corporate Agent

            $.ajax({
                type: "POST",
                url: "CommonWebMethods.aspx/GetTimeIntervals",
                contentType: "application/json; charset=utf-8",
                data: "{'agentId':'" + agentId + "'}",
                dataType: "json",
                async: true,
                success: function (data) {
                    if (data.d != undefined) {
                        var ddlDepTime = document.getElementById('<%=ddlDepTime.ClientID%>');
                        var ddlRetTime = document.getElementById('<%=ddlReturnTime.ClientID%>');

                        var cvalues = data.d.split(',');

                        ddlDepTime.options.length = 0;
                        var el = document.createElement("option");
                        el.textContent = "Any Time";
                        el.value = "Any";
                        ddlDepTime.add(el, 0);

                        ddlRetTime.options.length = 0;
                        var el = document.createElement("option");
                        el.textContent = "Any Time";
                        el.value = "Any";
                        ddlRetTime.add(el, 0);

                        for (var i = 0; i < cvalues.length; i++) {
                            var opt = cvalues[i];
                            if (opt.length > 0) {
                                var options = opt.split('|');

                                var el = document.createElement("option");
                                el.textContent = options[0];
                                el.value = options[1];
                                ddlDepTime.appendChild(el);

                                el = document.createElement("option");
                                el.textContent = options[0];
                                el.value = options[1];
                                ddlRetTime.appendChild(el);

                            }
                        }
                    }
                },
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
            });
        }

    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>

