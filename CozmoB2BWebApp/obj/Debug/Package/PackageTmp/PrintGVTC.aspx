﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PrintOBVisaTCUI" Codebehind="PrintGVTC.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" >
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Print OB Visa Submission Form</title>
   <link type="text/css" href="Styles/default.css" rel="stylesheet" />
<style type="text/css">
<!--
body,td,th {
	font-family: Arial, Helvetica, sans-serif; font-size:12px;
}
-->
</style></head>
<script type="text/javascript" language="javascript">

 function printPage()
    {
        document.getElementById('btnPrint').style.display="none";
	    window.print();	 
	    //alert('2');
	    setTimeout('showButtons()',1000);
	    //alert('3');
    }
    function showButtons()
    {
        document.getElementById('btnPrint').style.display="block";	    
    }
    </script>
    
    

<body>
  <div id="divPrintButton" style="text-align:left;">
    <input style="width:100px;" id="btnPrint" onclick="return printPage();" class="button" type="button" value="Print Form" />
    </div>
<table style="margin:auto" width="800px" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td align="center"><strong><h2>COZMO TRAVEL</h2></strong >
     <label> Visa Application Submission Form</label></td>
  </tr>
    <tr>
        <td>
    <hr />
            </td>
        </tr>
  <tr>
    <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
        <%if(tranxVS !=null && tranxVS.PassengerList !=null && tranxVS.PassengerList.Count > 0) { %>
      <tr>
        <td width="50%"><strong>Document No:</strong>
         <label><%=tranxVS.DocNumber %></label></td>
         
        <td width="50%"><strong>Date:</strong><asp:Label ID="lblDocumentsDate" runat="server" ></asp:Label> </td>
      </tr>
       <tr>
        <td width="50%"><strong>Travel Reason:</strong>
         <label><%=tranxVS.TravelReason %></label></td>
         
        <td width="50%"><strong>Traveller Name:</strong><%=tranxVS.PassengerList[0].Fpax_name %></td>
      </tr>
         <tr>
        <td width="50%"><strong>Traveling Country:</strong>
         <label><%=tranxVS.SelCountry %></label></td>
         
        <td width="50%"><strong>Visa Type:</strong><%=tranxVS.SelVisaType %></td>
      </tr>
          <tr>
        <td width="50%"><strong>Visa Category::</strong>
         <label><%=tranxVS.VisaCategory%></label></td>
         
        <td width="50%"><strong>Passport Number:</strong><%=tranxVS.PassengerList[0].Fpax_passport_no %></td>
      </tr>
         <tr>
        <td width="50%"><strong>Payment Mode:</strong>
         <label><%=tranxVS.SettlementMode.ToString() %></label></td>
         <td width="50%"><strong>Travel Date:</strong>
             <label><%=tranxVS.TTTravelDate.ToString("dd MMM yyyy") %></label></td>
        
      </tr>
          <tr>
       <td width="50%"><strong>Total Cost:</strong><%=tranxVS.CurrencyCode %> <%=tranxVS.TotVisaFee.ToString("N2") %></td>
      </tr>
        <tr><td>
        <h2>Passenger Details </h2></td></tr>
        <% int i = 0;
            foreach (CT.GlobalVisa.GVPassenger pass in tranxVS.PassengerList)
            {
                i++;%>
         <tr><td>
        <h2>Pax  <%=i %></h2></td></tr>
         <tr>
        <td width="50%"><strong>Applicant Name:</strong>
         <label><%=pass.Fpax_name%></label></td>
         
        <td width="50%"><strong>Age Group:</strong><%=pass.Fpax_passport_type %></td>
      </tr>
          <tr>
        <td width="50%"><strong>Gender:</strong>
         <label><%=pass.Fpax_gender%></label></td>
         
        <td width="50%"><strong>Nationality:</strong><%=tranxVS.SelNationality %></td>
      </tr>
         <tr>
        <td width="50%"><strong>Martial Status:</strong>
         <label><%=pass.Fpax_marital_status%></label></td>
         
        <td width="50%"><strong>Profession:</strong><%=pass.Fpax_profession %></td>
      </tr>
         <tr>
        <td width="50%"><strong>Email:  </strong>
         <label><%=pass.Fpax_email%></label></td>
         
        <td width="50%"><strong>Phone:</strong><%=pass.Fpax_phone %></td>
      </tr>
          <tr>
        <td width="50%"><strong>Passport Type:    </strong>
         <label><%=pass.Fpax_passport_type%></label></td>
         
        <td width="50%"><strong>Passport No:</strong><%=pass.Fpax_passport_no %></td>
      </tr>
         <tr>
        <td width="50%"><strong>Issued Date:    </strong>
         <label><%=pass.Fpax_psp_issue_date.ToString("dd MMM yyyy")%></label></td>
         
        <td width="50%"><strong>Expiry Date: </strong><%=pass.Fpax_psp_expiry_date.ToString("dd MMM yyyy") %></td>
      </tr>
          <tr>
        <td width="50%"><strong>Passport Country:      </strong>
         <label><%=pass.Fapx_Issue_CountryName%></label></td>
         
        
      </tr>
        <tr>
           <td width="50%"><strong>Fax:      </strong>
         <label><%=pass.Fpax_fax%></label></td>
         
        <td width="50%"><strong>City: </strong><%=pass.Fpax_city %></td>
      </tr>
        <tr>
     <td width="50%"><strong>Zip Postal/Code</strong>
         <label><%=pass.Fpax_zip_code%></label></td>
         
        <td width="50%"><strong>Residence Country: </strong><%=tranxVS.SelResidence %></td>
      </tr>
         <tr>
     <td width="50%"><strong>Office address:  </strong>
         <label><%=pass.Fpax_adds1%></label></td>
         
        <td width="50%"><strong>Residence address: </strong><%=pass.Fpax_adds2 %></td>
      </tr>
        <%} %>
        
      <%} %>
      
      
      
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2"><table width="100%" border="0" cellspacing="0" cellpadding="0">
          <tr>
            <td height="40">&nbsp;</td>
            <td align="right" valign="top"><label>Name &amp; Signature</label></td>
          </tr>
          <tr>
            <td height="90" valign="top"><label style="border-top: solid 1px #CCCCCC">Name &amp; Signature of Applicant / Authorised Person </label></td>
            <td align="right" valign="top"><label style="border-top: solid 1px #CCCCCC"> Received Passport &amp; Other Original Documents</label></td>
          </tr>
        </table></td>
      </tr>
      <tr>
        <td colspan="2"><span class="style1">
          <label>Declaration:</label>
        </span></td>
      </tr>
      <tr>
        <td colspan="2"><label>I Hereby Confirms as under:</label>
          <br />
          <table class="f10px"  cellpadding="4" cellspacing="0">
            <tbody>
              <tr>
                <td></td>
                <td></td>
              </tr>
              <tr>
                <td valign="top">i)</td>
                <td valign="top">The Above Document have been submitted   to M/S Cozmo Travel for the express purpose of application of visas for   the Country as mentioned                                      above.I/We authorize M/S Cozmo   Travel to submit the documents and to collect the documnets from the   consulate on my/our bahalf. </td>
              </tr>
              <tr>
                <td valign="top">ii)</td>
                <td valign="top">I/We confirm that i am authorised by the other applications (other than me) to submit documents under the clause (I). </td>
              </tr>
              <tr>
                <td valign="top">iii)</td>
                <td valign="top">I/We indemnify M/S Cozmo travel for any   and all declarations made under the above application.I confirm that i   am responsible for all declarations made under the application. </td>
              </tr>
              <tr>
                <td valign="top">iv)</td>
                <td valign="top">I/We confirm that i indemnify M/S Cozmo Travel for rejection of the application on any or all grounds by the Consulate. </td>
              </tr>
              <tr>
                <td valign="top">v)</td>
                <td valign="top">I/We indemnify M/S Cozmo travel   towards any express or implied financial or other liabilities arising   out of the above application.M/S Cozmo Travel shall not responsible for   any liabilty arising out of non receipt of visas.M/S Cozmo Travel                          will not be responsible for any cancellation or   other charges including but not restricted to airlines,hotels,insurence   etc. </td>
              </tr>
              <tr>
                <td valign="top">vi)</td>
                <td valign="top">On submission of the application all   charges mentioned above are payable to M/S Cozmo Travel including the   Consulate fees and M/S Cozmo Travel service charges irrespective of   receipt of visas. </td>
              </tr>
              <tr>
                <td valign="top">vii)</td>
                <td valign="top">I/We agree that indemnify M/S Cozmo   travel will not take the responsibilities in case of loss of the   documents including the passport by the Consulate. M/S Cozmo Travel will   be indemnified from all claims including but not restricted to   legel,financial or                           implied losses out of such an event. </td>
              </tr>
              
              <tr>
                <td valign="top">viii)</td>
                <td valign="top">Approval of your visa application is at the sole discretion of the Immigration officer at the Consulate / Embassy and we at Cozmo Travel do not influence their decision making for approval / rejection of your visa application      </td>          
              </tr>
                
              <tr>
                <td valign="top">ix)</td>
                <td valign="top">The rules of Immigration are subjected to change from time to time and your application could be subjected to the same while under process. </td>          
              </tr>
                              
              <tr>
                <td valign="top">x)</td>
                <td valign="top">In case of an application being rejected / put on hold Cozmo Travel would be in no position to refund the visa application or processing fees    </td>          
              </tr>
            </tbody>
          </table></td>
      </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
      <tr>
        <td colspan="2">&nbsp;</td>
      </tr>
      
    </table></td>
  </tr>
</table>
</body>
</html>
