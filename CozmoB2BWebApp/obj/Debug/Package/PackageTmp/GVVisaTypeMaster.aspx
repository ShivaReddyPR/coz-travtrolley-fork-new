﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="GVVisaTypeMasterGUI" MasterPageFile="~/TransactionBE.master" Title="VisaType Master" Codebehind="GVVisaTypeMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="content1" ContentPlaceHolderID="cphTransaction" runat="server">
<%--<div> <h4>Visa Type Master</h4> </div>--%>
<script type="text/javascript">
    function Save() {
        if (getElement('txtVisaTypeCode').value == '') addMessage('VisaType Code  cannot be blank!', '');
        if (getElement('txtVisaTypeName').value == '') addMessage('VisaType Name cannot be blank!', '');
        if (getElement('ddlCountry').selectedIndex <= 0) addMessage('Please select Country from the list!', '');
        //if (getElement('ddlAgent').selectedIndex <= 0) addMessage('Please select Agent from the list!', '');
        if (getMessage() != '') {
            alert(getMessage()); clearMessage();
            return false;
        }
    }
</script>
<div class="body_container" style="height:460px">
<asp:HiddenField ID="hdfVisaTypeId" runat="server" Value="0" />
<div> <h4>VisaType Master</h4> </div>
<div class=" bg_white bor_gray pad_10 paramcon">
<div class="col-md-12 padding-0 ">
                <div class="col-md-2">
                    <asp:Label ID="lblVisaTypeCode" runat="server" Text="VisaType Code:"></asp:Label><sup style="color:Red">*</sup>
                </div>
                <div class="col-md-2">
                    <asp:TextBox  CssClass="form-control" ID="txtVisaTypeCode" runat="server" MaxLength="10"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-12 padding-0">
                <div class="col-md-2">
                    <asp:Label ID="lblVisaTypeName" runat="server" Text="VisaType Name:"></asp:Label><sup style="color:Red">*</sup>
                </div>
                <div class="col-md-2">
                    <asp:TextBox  CssClass="form-control" ID="txtVisaTypeName" runat="server" MaxLength="100"></asp:TextBox>
                </div>
            </div>
            <div class="col-md-12 padding-0">
                <div class="col-md-2">
                    <asp:Label ID="lblCountry" runat="server" Text="VisaType Country:"></asp:Label><sup style="color:Red">*</sup>
                </div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                </div>
            </div>
            <div class="col-md-12 padding-0" style="display:none;">
                <div class="col-md-2">
                    <asp:Label ID="lblAgent" runat="server" Text="VisaType Agent:"></asp:Label><sup style="color:Red">*</sup>
                </div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlAgent" runat="server" CssClass="form-control" Enabled="true"></asp:DropDownList>
                </div>
            </div>
            <div style="padding:150px 210px">
            <asp:Button ID="btnSave" CssClass="button" Text="Save" runat="server" OnClientClick="return Save();" OnClick="btnSave_Click" />&nbsp;&nbsp;
                                    <asp:Button ID="btnClear" CssClass="button" Text="Clear" runat="server" OnClick="btnClear_Click"  />&nbsp;&nbsp;
                                    <asp:Button ID="btnSearch" CssClass="button" Text="Search" runat="server" OnClick="btnSearch_Click"  />
            </div>
<asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>

</div>
</div>
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
    <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="VISA_TYPE_ID"
        EmptyDataText="Data not Found!" AutoGenerateColumns="false" PageSize="10" GridLines="none" CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
        OnPageIndexChanging="gvSearch_PageIndexChanging">
         <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />" ControlStyle-CssClass="label" ShowSelectButton="True" />
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="left" />
                  <HeaderTemplate>
                    <asp:Label ID="lblCode" runat="server" Text="VisaType Code" CssClass="label grdof" Width="150px"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCode" runat="server" Text='<%# Eval("VISA_TYPE_CODE") %>' CssClass="label grdof" Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemStyle HorizontalAlign="left" />
                  <HeaderTemplate>
                    <asp:Label ID="lblName" runat="server" Text="VisaType Name" CssClass="label grdof" Width="150px"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblName" runat="server" Text='<%# Eval("VISA_TYPE_NAME") %>' CssClass="label grdof"  Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:TemplateField>
                <ItemStyle HorizontalAlign="left" />
                  <HeaderTemplate>
                    <asp:Label ID="lblAgentName" runat="server" Text="AgentName" CssClass="label grdof" Width="150px"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblAgentName" runat="server" Text='<%# Eval("AGENT_NAME") %>' CssClass="label grdof"  Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>--%>
             <asp:TemplateField>
                <ItemStyle HorizontalAlign="left" />
                  <HeaderTemplate>
                    <asp:Label ID="lblCountryName" runat="server" Text="CountryName" CssClass="label grdof" Width="150px"></asp:Label>
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblCountryName" runat="server" Text='<%# Eval("COUNTRYNAME") %>' CssClass="label grdof"  Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
        </Columns>
    </asp:GridView>
    

</asp:Content>
