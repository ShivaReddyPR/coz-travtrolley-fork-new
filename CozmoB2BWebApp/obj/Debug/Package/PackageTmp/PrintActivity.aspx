﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PrintActivityGUI" Codebehind="PrintActivity.aspx.cs" %>

<%@ Import Namespace="System.Collections.Generic" %>
<head id="Head1" runat="server">
<link rel="stylesheet" href="css/ajax_tab_css.css">
<%--<link rel="stylesheet" href="css/main-style.css">--%>

<script type="text/javascript" language="javascript">
    function printpage() {
        window.print()
    }
</script>
</head>
<form id="form1">
<div>
    <input type="button" value="Print this page" onclick="printpage()"></div>
<table style="font-family: Arial, Helvetica, sans-serif; line-height: 17px" width="100%"
    border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table class="themecol1" width="100%" border="0" cellpadding="0" cellspacing="0">
                <tr>
                    <td width="50%" height="30">
                        <label style="color: #fff; font-size: 18px;">
                            <%=activity.Name%>
                            (<%=activity.Days %>
                            Nights &amp;
                            <%=activity.Days  + 1 %>
                            Days)
                        </label>
                    </td>
                    <td width="50%" align="right">
                        <label style="padding-right: 15px; color: #FFFFFF; font-size: 16px">
                            Starting From- AED
                            <%=minPrice.ToString("N0") %></label>
                        <label style="padding-right: 15px; color: #FFFFFF">
                            per person</label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 5px">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" width="30%">
                        <img width="366px" height="247px" src="<%=imageServerPath %><%= activity.ImagePath1%>"
                            alt="<%=activity.Name %>" title="<%=activity.Name %>" />
                    </td>
                    <td width="70%" valign="top">
                        <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                            <tr>
                                <td height="0">
                                    <label style="color: #cc0000; font-size: 16px">
                                        <strong>Overview</strong></label>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; font-size: 14px!important" valign="top">
                                    <label>
                                        <%=activity.Overview %>
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                <tr>
                    <td height="0">
                        <label style="color: #cc0000; font-size: 16px">
                            <strong>Inclusions </strong>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="Inclusions" class="active" >
                            
                                
                                <ul>
                                
                                <%if (activity != null)
                                          { %>
                                <%foreach (string item in activity.Inclusions)
                                              { %>
                                <li style="list-style-type: circle">
                                    <%=item%></li>
                                <%} %>
                            </ul>
                            <label style="color: #cc0000; font-size: 16px">
                                    <strong>Exclusions </strong>
                                </label>
                            <ul>
                                
                                <%foreach (string item in activity.Exclusions)
                                              { %>
                                <li style="list-style-type: circle">
                                    <%=item%></li>
                                <%}
                                          }%></ul>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                <tr>
                    <td height="0">
                        <label style="color: #cc0000; font-size: 16px">
                            <strong>Itinerary</strong></label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                       
                            <div id="Itinerary" class="active" >
                                <label>
                                    <%=(activity != null ? activity.Itinerary1 : "") %></label>
                                <label>
                                    <%=(activity != null ? activity.Itinerary2 : "") %></label>
                                <%=(activity != null ? activity.Details : "") %>
                        
                        <h4>
                            Transfer included</h4>
                            <ul>
                        <li style="list-style-type: circle">
                            <%=(activity != null ? activity.TransferIncluded : "") %></li></ul>
                        <h4>
                            Meals Included</h4>
                        <ul>
                            <%if(activity != null && activity.MealsIncluded != null){ %>
                            <%string[] mealItems = activity.MealsIncluded.Split(','); %>
                            <%foreach (string meal in mealItems)
                                              { %>
                            <li style="list-style-type: circle">
                                <%=meal %></li>
                            <%} %>
                            <%} %>
                        </ul>
                        </div> 
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                <tr>
                    <td height="0">
                        <label style="color: #cc0000; font-size: 16px">
                            <strong>Price</strong></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="font-size: 14px">
                            <div id="RoomRates">
                                <div>
                                    <asp:DataList id="dlRoomRates" runat="server" width="100%">
                                                <ItemTemplate>
                                                    <table width="100%" border="1">
                                   
                                                        <tr>
                                                            <td width="30%">
                                                                <%#Eval("Label") %>
                                                            </td>
                                                        
                                                           <td width="70%">
                                                           <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>
                                                                <%#CTCurrencyFormat(Eval("Total"))%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                </div>
                            </div>
                        </label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                <tr>
                    <td height="0">
                        <label style="color: #cc0000; font-size: 16px">
                            <strong>Terms & Conditions</strong></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="Terms" class="active" >
                            
                            <h4>Things to bring</h4>
                            <ul>
                                <%if (activity != null && activity.ThingsToBring != null)
                                  { %>
                                 <%string[] things = activity.ThingsToBring.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                <%foreach (string thing in things)
                                  { %>
                                <li style=" list-style-type:circle; margin-left:20px;">
                                    <%=thing%></li>
                                <%} %>
                                <%} %>
                            </ul>
                            
                            <h4>Cancellation Policy</h4>
                                     <ul>
                                        <%if (activity != null && activity.CancelPolicy != null)
                                          { %>
                                        <%string[] cancelP = activity.CancelPolicy.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                        <%foreach (string cancel in cancelP)
                                          { %>
                                          <li style=" list-style-type:circle; margin-left:20px;"><%=cancel %></li>
                                        <%} %>
                                        <%} %>
                                        </ul>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
</table>

</form>
