﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="ROSEmployeeMasterGUI" Title="ROS Employee Master" Codebehind="ROSEmployeeMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<script>
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 57)) {
            return false;
        }
        return true;
    }

    function Validation() {

        if (getElement('txtEmpStaffId').value == '') addMessage('Staff Id cannot be blank!', '');
        if (getElement('ddlEmpStaffType').selectedIndex <= 0) addMessage('Select Staff Type!', '');
        if (getElement('ddlEmpTitle').selectedIndex <= 0) addMessage('Please select title!', '');
        if (getElement('txtEmpFName').value == '') addMessage('First Name cannot be blank!', '');
        if (getElement('txtEmpLName').value == '') addMessage('Last Name cannot be blank!', '');
        if (getElement('txtEmpMobNo').value == '') addMessage('Mob. No. cannot be blank!', '');
        if (getElement('txtEmpEmail').value == '') addMessage('Email cannot be blank!', '');
        else if (!checkEmail(getElement('txtEmpEmail').value)) addMessage('Email is not valid!', '');
        if (getElement('hdfMode').value == '0') {
            if (getElement('txtEmpPassword').value == '') addMessage('Password cannot be blank!', '');
            if (getElement('txtEmpConfirmPassword').value == '') addMessage('Confirm Password cannot be blank!', '');
          
        }
        
        if (getElement('ddlEmpLocFrom').selectedIndex <= 0) addMessage('Select Location From!', '');
        if (getElement('ddlEmpLocTo').selectedIndex <= 0) addMessage('Select Location To!', '');
        if (getElement('hdfMode').value == '0') {
            if (getElement('txtEmpPassword').value != getElement('txtEmpConfirmPassword').value) addMessage('Password Mismatch!', '');
        }

        if (getMessage() != '') {
            
            alert(getMessage()); clearMessage(); return false;
        }
    }

    function CheckPassword() {

        if (getElement('txtEmpPassword').value != getElement('txtEmpConfirmPassword').value) addMessage('Password Mismatch!', '');
        if (getMessage() != '') {
            alert(getMessage()); clearMessage(); return false;
        }
    }


    function SetTariff() {

        //alert(getElement('ddlTariff').value);
        var ddl = getElement('ddlTariff');
       // alert(ddl);
        var selectedTariff = ddl.options[ddl.selectedIndex].innerHTML;
        var trfAmount = selectedTariff.split("~");
        getElement('txtTariffAmount').value= trfAmount[1];
    
    
    }


    
</script>

<style>
p{width:120px;}

.outer-center {
    float: right;
    right: 50%;
    position: relative;
}
.inner-center {
    float: right;
    right: -50%;
    position: relative;
}
.clear {
    clear: both;
}
</style>




<asp:HiddenField runat="server" id="hdfMode" value="0" ></asp:HiddenField>   

<div class="body_container paramcon"> 

 <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2">Staff ID:* </div>
    <div class="col-md-2"> <asp:TextBox ID="txtEmpStaffId" MaxLength="30" CssClass="inputEnabled form-control"  runat="server"
                            ></asp:TextBox></div>
    <div class="col-md-2"> Staff Type:*</div>
     <div class="col-md-2"> <asp:DropDownList ID="ddlEmpStaffType" CssClass="inputDdlEnabled form-control" runat="server"
                             >
                             <asp:ListItem Value="-1" Text="Select Staff Type"></asp:ListItem>
                             <asp:ListItem Value="Crew" Text="Crew"></asp:ListItem>
                             <asp:ListItem Value="Driver" Text="Driver"></asp:ListItem>
                         </asp:DropDownList></div>
    
     <div class="col-md-2">  Title:*</div>
         <div class="col-md-2"><asp:DropDownList ID="ddlEmpTitle" CssClass="inputDdlEnabled form-control" runat="server"
                            >
                             <asp:ListItem Value="-1" Text="Select Title"></asp:ListItem>
                             <asp:ListItem Value="Mr." Text="Mr"></asp:ListItem>
                             <asp:ListItem Value="Ms." Text="Ms"></asp:ListItem>
                         </asp:DropDownList> </div>

<div class="clearfix"></div>
    </div>
       
    
     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> First Name*: </div>
    <div class="col-md-2"><asp:TextBox ID="txtEmpFName" CssClass="inputEnabled form-control"   runat="server"></asp:TextBox> </div>
    <div class="col-md-2">  Last Name*:</div>
     <div class="col-md-2"><asp:TextBox ID="txtEmpLName" CssClass="inputEnabled form-control" runat="server"></asp:TextBox> </div>
     <div class="col-md-2">  Mobile No.*:</div>
         <div class="col-md-2"><asp:TextBox ID="txtEmpMobNo" CssClass="inputEnabled form-control" MaxLength="15" onkeypress="return isNumber(event);" runat="server" Width="120px"></asp:TextBox> </div>

<div class="clearfix"></div>
    </div>
    
      
    
     <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2">  Alternate Mob. No.:</div>
    <div class="col-md-2"><asp:TextBox ID="txtEmpAltMobNo" CssClass="inputEnabled form-control" MaxLength="15" onkeypress="return isNumber(event);" runat="server"></asp:TextBox> </div>
    <div class="col-md-2"> Email.:</div>
     <div class="col-md-2"> <asp:TextBox ID="txtEmpEmail" CssClass="inputEnabled form-control" runat="server"></asp:TextBox></div>
    
     <div class="col-md-2">Password*: </div>
         <div class="col-md-2"><asp:TextBox ID="txtEmpPassword" CssClass="inputEnabled form-control" MaxLength="50" EnableViewState="true" TextMode="Password" runat="server" ></asp:TextBox> </div>

<div class="clearfix"></div>
    </div>
     
 
 
    <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2">Password Confirmation*.:</div>
    <div class="col-md-2"> <asp:TextBox ID="txtEmpConfirmPassword" CssClass="inputEnabled form-control" MaxLength="50" TextMode="Password" Onchange="return CheckPassword();"  runat="server"></asp:TextBox></div>
    
    <div class="col-md-2"> Location From*: </div>
     <div class="col-md-2"> <asp:DropDownList ID="ddlEmpLocFrom" CssClass="inputDdlEnabled form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEmpLocFrom_OnSelectedIndexChanged">
                              </asp:DropDownList></div>
     <div class="col-md-2">  Location From Map.:</div>
         <div class="col-md-2"><asp:TextBox ID="txtEmpLocFromMap" CssClass="inputEnabled form-control"  runat="server"></asp:TextBox> </div>

<div class="clearfix"></div>
    </div>



   <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> Location From Detail.: </div>
 
 
    <div class="col-md-6"><asp:TextBox ID="txtEmpLocFromDetail" CssClass="inputEnabled" TextMode="MultiLine" runat="server" Height="60px" Width="100%"></asp:TextBox></div>
   
   


<div class="clearfix"></div>
    </div>


   <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> Location To*:</div>
    <div class="col-md-2"><asp:DropDownList ID="ddlEmpLocTo" CssClass="inputDdlEnabled form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEmpLocTo_OnSelectedIndexChanged"
                           >
                             
                         </asp:DropDownList> </div>
                         
                         
    <div class="col-md-2">  Location To Map.:</div>
    
    
     <div class="col-md-2"><asp:TextBox ID="txtEmpLocToMap" CssClass="inputEnabled form-control"  runat="server" ></asp:TextBox> </div>
   

<div class="clearfix"></div>
    </div>





   <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> Location To Detail.:</div>
    <div class="col-md-6"> <asp:TextBox ID="txtEmpLocToDetail" CssClass="inputEnabled" TextMode="MultiLine" runat="server" Height="60px" Width="100%"></asp:TextBox></div>


<div class="clearfix"></div>
    </div>


   <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2">Designation.: </div>
    <div class="col-md-2"><asp:TextBox ID="txtEmpDesig" CssClass="inputEnabled form-control" runat="server"></asp:TextBox> </div>

<div class="col-md-2">Tariff.: </div>
    <div class="col-md-2"><asp:DropDownList ID="ddlTariff" onChange="SetTariff();" CssClass="inputDdlEnabled form-control" runat="server"></asp:DropDownList>
    <asp:TextBox ID="txtTariffAmount" CssClass="inputEnabled form-control" runat="server"></asp:TextBox>
     </div>

 <div class="col-md-2"> Service Status*:</div>
    <div class="col-md-2"><asp:DropDownList ID="ddlService" CssClass="inputDdlEnabled form-control" runat="server" >
    <asp:ListItem Text="Active" Value="A"></asp:ListItem>                         
    <asp:ListItem Text="Terminated" Value="T"></asp:ListItem>                         
                         </asp:DropDownList>
     <label>Remarks</label>
      <asp:TextBox ID="txtRemarks" TextMode="MultiLine" Height="60px"  CssClass="inputEnabled form-control" runat="server"></asp:TextBox>
                          </div>

  <div class="col-md-8 martop_xs10"> 
    <asp:Button ID="btnSave" CssClass="btn but_b marright_10 pull-right" Text="Save" runat="server" OnClientClick="return Validation();"  OnClick="btnSave_Click" style="display:inline;" />&nbsp;&nbsp;
                 
                     <asp:Button ID="btnClear" CssClass="btn but_b marright_10 pull-right" Text="Clear" runat="server" OnClick="btnClear_Click" style="display:inline;"/>&nbsp;&nbsp;
                
                     <asp:Button ID="btnSearch" CssClass="btn but_b marright_10 pull-right" Text="Search" runat="server" OnClick="btnSearch_Click" style="display:inline;"/>
                


</div>

<div class="clearfix"></div>
    </div>
    
   
    
    
       


<div class="clearfix"></div>

</div>


 <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="lblSuccess"></asp:Label>
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" Runat="Server">
<asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="emp_Id" 
    EmptyDataText="No ROS Employee List Available!" AutoGenerateColumns="false" PageSize="15" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging" >
    
    <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />" ControlStyle-Width="25px"  ControlStyle-CssClass="label" ShowSelectButton="True" />
    
      
     <asp:TemplateField>
     <HeaderStyle HorizontalAlign="left"/>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc1:Filter   ID="HTtxtEmpStaffId" Width="120px" HeaderText="Staff Id" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblEmpStaffId" runat="server" Text='<%# Eval("staff_id") %>' CssClass="label grdof" ToolTip='<%# Eval("staff_id") %>' Width="120px"></asp:Label>
 
    </ItemTemplate>    
    
    </asp:TemplateField>  
    
   
    
       <asp:TemplateField>
        <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtEmpName"  Width="120px" CssClass="inputEnabled" HeaderText="Name" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmpName" runat="server" Text='<%# Eval("emp_Name") %>' CssClass="label grdof"  ToolTip='<%# Eval("emp_Name") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    

 <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtEmpMobNo"  Width="120px" CssClass="inputEnabled" HeaderText="Mobile No." OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmpMobNo" runat="server" Text='<%# Eval("mobileNo") %>' CssClass="label grdof"  ToolTip='<%# Eval("mobileNo") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtEmpAltrNo"  Width="120px" CssClass="inputEnabled" HeaderText="Alternate No." OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmpAltrNo" runat="server" Text='<%# Eval("alternateNo") %>' CssClass="label grdof"  ToolTip='<%# Eval("alternateNo") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtEmpEmail"  Width="120px" CssClass="inputEnabled" HeaderText="Email" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmpEmail" runat="server" Text='<%# Eval("email") %>' CssClass="label grdof"  ToolTip='<%# Eval("email") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
   
   
   <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtEmpStaffType"  Width="120px" CssClass="inputEnabled" HeaderText="Staff Type" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmpStaffType" runat="server" Text='<%# Eval("staffType") %>' CssClass="label grdof"  ToolTip='<%# Eval("staffType") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    
    <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtEmpLocFrom"  Width="120px" CssClass="inputEnabled" HeaderText="Location From" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmpLocFrom" runat="server" Text='<%# Eval("locationFrom") %>' CssClass="label grdof"  ToolTip='<%# Eval("locationFrom") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    
     <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtEmpLocFromDet"  Width="120px" CssClass="inputEnabled" HeaderText="Location From Details" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmpLocFromDet" runat="server" Text='<%# Eval("locationFromDetails") %>' CssClass="label grdof"  ToolTip='<%# Eval("locationFromDetails") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    
    
     <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtEmpLocFromMap"  Width="120px" CssClass="inputEnabled" HeaderText="Location From Map" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmpLocFromMap" runat="server" Text='<%# Eval("locationFromMap") %>' CssClass="label grdof"  ToolTip='<%# Eval("locationFromMap") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    
    <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtEmpLocTo"  Width="120px" CssClass="inputEnabled" HeaderText="Location To" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmpLocTo" runat="server" Text='<%# Eval("locationTo") %>' CssClass="label grdof"  ToolTip='<%# Eval("locationTo") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    
     <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtEmpLocToDet"  Width="120px" CssClass="inputEnabled" HeaderText="Location To Details" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmpLocToDet" runat="server" Text='<%# Eval("locationToDetails") %>' CssClass="label grdof"  ToolTip='<%# Eval("locationToDetails") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtEmpLocToMap"  Width="120px" CssClass="inputEnabled" HeaderText="Location To Map" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmpLocToMap" runat="server" Text='<%# Eval("locationToMap") %>' CssClass="label grdof"  ToolTip='<%# Eval("locationToMap") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
     <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtEmpDesig"  Width="120px" CssClass="inputEnabled" HeaderText="Designation" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblEmpDesig" runat="server" Text='<%# Eval("designation") %>' CssClass="label grdof"  ToolTip='<%# Eval("designation") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
     </Columns>    
     <HeaderStyle CssClass="gvHeader"></HeaderStyle>
        <RowStyle CssClass="gvRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvAlternateRow" />  
    </asp:GridView>
</asp:Content>
