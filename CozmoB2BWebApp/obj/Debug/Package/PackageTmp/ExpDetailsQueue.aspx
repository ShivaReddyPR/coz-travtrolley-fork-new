﻿<%@ Page Title="Expense Details Queue" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpDetailsQueue.aspx.cs" Inherits="CozmoB2BWebApp.ExpDetailsQueue" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <style>
        .body_container {
            padding: 0 !important;
        }
    </style>
    
    <link href="css/toastr.css" rel="stylesheet" type="text/css" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="scripts/Common/ExpenseObjects.js" type="text/javascript"></script>    
    <script src="scripts/jquery-ui.js"></script>

    <script type="text/javascript">

        var FromDate = '', cntrllerPath = 'api/expTransactions', selectedReports = [], scAction = '';;
        var employeeProfiles = [], apiAgentInfo = {}, approverQueue = {}, expApprovalReports = [];
        var paymentTypes = [], costCenters = []

        try {
            $(document).ready(function () {
                $("#ctl00_upProgress").hide();

                /* Date Control for From Date*/
                FromDate = new Date();
                $("#txtFromDate").datepicker(
                    {
                        changeYear: true,
                        changeMonth: true,
                        minDate: -300,
                        dateFormat: 'dd/mm/yy',
                        onSelect: function (dateText, inst) {
                            var selectedDate = new Date(ConvertDate(dateText));
                            selectedDate.setDate(selectedDate.getDate() + 1);
                            $("#txtToDate").datepicker("option", "minDate", selectedDate);
                        }
                    }
                ).datepicker("setDate", FromDate);

                /* Date Control for To Date*/
                $("#txtToDate").datepicker(
                    {
                        changeYear: true,
                        changeMonth: true,
                        dateFormat: 'dd/mm/yy'
                    }).datepicker("setDate", FromDate);

                $("#txtFromDate").change(function () {
                    var dtToday = new Date($("#txtFromDate").val());
                    $("#txtToDate").datepicker(
                        {
                            changeYear: true,
                            changeMonth: true,
                            minDate: dtToday,
                            dateFormat: 'dd/mm/yy'
                        });
                });

                /* Prepare agent and login info for expense web api request call and assign to global variable */
                GetAgentInfo();

                /* Check the session info and revert if expired */
                if (IsEmpty(apiAgentInfo)) {

                    alert('Session expired, please login once again.');
                    return;
                }

                /* Prepare expense api host url and assign to global variable */
                GetExpenseApiHost();

                /* Load screen data */
                GetScreenData('SL');

                $("#ctl00_upProgress").hide();
            });

            /* Get the Approver Queue */
            function BindApproverData() {

                approverQueue = {};
                var fromDate = new Date($("#txtFromDate").datepicker("getDate"));
                var toDate = new Date($("#txtToDate").datepicker("getDate"));

                if (fromDate > toDate || toDate == undefined || toDate == '') {

                    alert('From Date must be less than to Date');
                    return false;
                } else {                   

                    var FromDate = ConvertDate($('#txtFromDate').val()) + " 00:00:01";
                    var ToDate = ConvertDate($('#txtToDate').val()) + " 23:59:59";
                    var AgentId = apiAgentInfo.AgentId;
                    var ProfileId = $('#ddlProfile').val();
                    var CostCenter = $('#ddlCostCenter').val();
                    var ReceiptStatus = $('#ddlReceiptStatus').val();
                    var PaymentType = $('#ddlPaymentType').val();
                    var ReportTitle = $('#txtTitle').val() != '' ? $('#txtTitle').val().trim() : null;
                    var ReportReferenceNo = $('#txtRefNo').val() != '' ? $('#txtRefNo').val().trim() : null;
                    var ApproverStatus = $('#ddlApproverStatus').val();
                  
                    approverQueue = GetExpApproverQueueObj(FromDate, ToDate, AgentId, ProfileId, CostCenter, ReceiptStatus,
                        PaymentType, ReportTitle, ReportReferenceNo, ApproverStatus);
                }
            }

            /* To get expense  Approval reports and Info */
            function GetScreenData(action) {

                scAction = action;
                BindApproverData();

                var selPayIds = scAction == 'PY' ? GetSetSelectedRows('get', '') : [];

                if (scAction == 'PY' && selPayIds.length == 0) {

                    ShowError('Please select expense to pay.');
                    return false;
                }

                var reqData = { AgentInfo: apiAgentInfo, ApproverQueue: approverQueue, type: 'QD', SelExpenses: selPayIds };
                var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getExpenseApproverQueue';
                WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);
            }

            /* To bind expense Receipts,Reports and corp profiles */
            function BindScreenData(screenData) {

                if (scAction == 'SL')
                    BindDropDown(screenData.dtProfiles, screenData.dtPaymentType, screenData.dtcostCenters);

                BindApprovalReports(screenData.dtApprovalReceipts);
            }

            /* To bind expense Receipts, receipt status, payment type and corp profiles */
            function BindDropDown(dtProfiles, dtPaymentType, dtcostCenters) {
                
                var options = GetddlOption('0', 'All');

                options += GetddlOption('RU', 'Receipt Uploaded');
                options += GetddlOption('NR', 'No Receipt');

                $('#ddlReceiptStatus').empty();
                $('#ddlReceiptStatus').append(options);
                $("#ddlReceiptStatus").select2("val", '0');

                DropDownBind('ddlProfile', dtProfiles, 'profileId', 'employeeId-surName name', '-', 'profileId');
                DropDownBind('ddlPaymentType', dtPaymentType, 'pT_ID', 'pT_Desc', '', 'pT_ID');
                DropDownBind('ddlCostCenter', dtcostCenters, 'cC_Id', 'cC_Name', '', 'cC_Id');
            }

            /* To Bind drop downs data */
            function DropDownBind(ddlId, dataEntity, valColNames, textColNames, separator, defSelVal) {

                DdlBind(ddlId, (IsEmpty(dataEntity) ? [] : dataEntity), valColNames, textColNames, separator,
                    '0', 'All', ((IsEmpty(dataEntity) || dataEntity.length != 1) ? '0' : dataEntity[0][defSelVal]));

                if (ddlId != 'ddlCostCenter' && !IsEmpty(dataEntity) && dataEntity.length == 1) 
                    $('#' + ddlId).attr('disabled', 'disabled');
            }

            /* To Bind the Expense Receipts to grid */
            function BindApprovalReports(reports) {
                                
                ClearDetails();

                if (reports.length == 0) {

                    if (scAction != 'SL')
                        ShowError('No records found.');
                    return;
                }

                expApprovalReports = reports;
                var headerColumns = 'Date|Report Ref #|Employee ID|Employee Name|Cost Center|Report Title|Business Purpose|Claim Expense|Claim Amount|Reimbursed Amount|';
                headerColumns += 'Expense Type|City|Payment Type|Receipt Status|Currency|Status|GL';
                var dataColumns = 'eR_Date|eR_RefNo|employeeId|employeeName|costCentre|eR_Title|eR_Purpose|eD_ClaimExpense|eD_TotalAmount|reimburseAmount|eT_Desc';
                dataColumns += '|eD_City|eD_PayType|eD_ReceiptStatus|eD_Currency|approvalStatus|eT_GLAccount';

                var gridProperties = GetGridPropsEntity();
                gridProperties.headerColumns = headerColumns.split('|');
                gridProperties.displayColumns = dataColumns.split('|');
                gridProperties.pKColumnNames = ('eD_Id').split('|');
                gridProperties.dataEntity = expApprovalReports;
                gridProperties.divGridId = 'divApproverGridInfo';
                gridProperties.headerPaging = true;
                gridProperties.gridSelectedRows = selectedReports;
                gridProperties.selectType = 'N';
                gridProperties.recordsperpage = 10;

                gridProperties.columnproperties = {};

                // Assigning the grid column custom controls,events,attributes...
                var gridColumnproperties = {};

                for (var i = 0; i < gridProperties.displayColumns.length; i++) {

                    var columnproperties = GetGridColumnProperties();
                    columnproperties.columnName = gridProperties.displayColumns[i];

                    if (gridProperties.displayColumns[i] == 'eR_RefNo') {
                        columnproperties.columnControl = 'link';
                        columnproperties.columnAttributes = { 'style': 'font-weight:bold', 'href': 'javascript:void(0)' };
                        columnproperties.columnEvents = { 'onclick': 'ShowReferenceDetails' };
                        columnproperties.columnEventParams = { 'onclick': 'eD_ER_Id' }
                    }

                    if (gridProperties.displayColumns[i] == 'eR_Title') {
                        columnproperties.columnControl = 'link';
                        //columnproperties.columnAttributes = { 'style': 'font-weight:bold' };
                        //columnproperties.columnEvents = { 'href': 'ExpReport.aspx?ExpRepId=eD_ER_Id&ExpDtlId=eD_Id' };
                        //columnproperties.columnEventParams = { 'href': 'eD_ER_Id|eD_Id' }
                        columnproperties.columnAttributes = { 'style': 'font-weight:bold', 'href': 'javascript:void(0)' };
                        columnproperties.columnEvents = { 'onclick': 'OpenExpReport' };
                        columnproperties.columnEventParams = { 'onclick': 'eD_ER_Id|eD_Id' };
                    }

                    if (gridProperties.displayColumns[i] == 'eT_Desc') {
                        columnproperties.columnControl = 'link';
                        //columnproperties.columnAttributes = { 'style': 'font-weight:bold' };
                        //columnproperties.columnEvents = { 'href': 'ExpCreate.aspx?ExpRepId=eD_ER_Id&ExpDtlId=eD_Id' };
                        //columnproperties.columnEventParams = { 'href': 'eD_ER_Id|eD_Id' }
                        columnproperties.columnAttributes = { 'style': 'font-weight:bold', 'href': 'javascript:void(0)' };
                        columnproperties.columnEvents = { 'onclick': 'OpenExpCreate' };
                        columnproperties.columnEventParams = { 'onclick': 'eD_ER_Id|eD_Id' };
                    }
                     
                    gridColumnproperties[gridProperties.displayColumns[i]] = columnproperties;
                }

                if (expApprovalReports.find(x => x.iSPay == 'Y')) {

                    gridProperties.selectColProps = GetGridRowProp();
                    gridProps.selectColProps.columnName = 'iSPay';
                    gridProps.selectColProps.colValue = 'Y';
                    gridProps.selectColProps.colCondition = '=';
                    gridProperties.selectType = 'M';
                    $('#lnkPay').show();
                }

                gridProperties.columnproperties = gridColumnproperties;
                EnablePagingGrid(gridProperties);
                $('#ExpDetailsList').show();
                $('#divTrips').show();
            }

            /* To clear Approver details and hide the div */
            function ClearDetails() {

                RemoveGrid();
                $('#divTrips').hide();
                $('#lnkPay').hide();
                $('#ExpDetailsList').hide();
            }

            /* To get agent and login info */
            function GetAgentInfo() {

                try {

                    var loginInfo = '<%=Settings.LoginInfo == null%>';

                    if (loginInfo == true)
                        return apiAgentInfo;

                    var agentId = '<%=Settings.LoginInfo.AgentId%>';
                    var loginUser = '<%=Settings.LoginInfo.UserID%>';
                    var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                    var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                    var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                    apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
                }
                catch (excp) {
                    var exception = excp;
                }
                return apiAgentInfo;
            }

            /* To get expense api host url */
            function GetExpenseApiHost() {

                apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';

                if (IsEmpty(apiHost))
                    apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";

                return apiHost;
            }

            /* Show the Date controls*/
            function showDatepicker(id) {
                $('#' + id).focus();
            }

            /* Getting the Date from Date controls*/
            function ConvertDate(selector) {
                // To matach with Main search Date Format
                var from = selector.split("/");
                var Date = from[1] + "/" + from[0] + "/" + from[2];
                return Date;
            }
          
            /* Getting the Report reference Datails */
            function ShowReferenceDetails(expDtlId) {

                if (parseInt(expDtlId) > 0) {

                    AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpReportPrint', 'sessionData':'" + JSON.stringify({ expDetailId: expDtlId }) + "', 'action':'set'}");
                    window.open("ExpReportPrint.aspx", "Expense Report", "width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
                }
            }
          
            /* Open expense report screen */
            function OpenExpReport(expRepId, expDetailId) {

                if (parseInt(expRepId) > 0 && parseInt(expDetailId) > 0) {

                    AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpReport', 'sessionData':'" + JSON.stringify({ ExpRepId: expRepId, ExpDtlId: expDetailId }) + "', 'action':'set'}");
                    window.location.href = "ExpReport.aspx";
                }
            }
          
            /* Open expense create screen */
            function OpenExpCreate(expRepId, expDetailId) {

                if (parseInt(expRepId) > 0 && parseInt(expDetailId) > 0) {

                    AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpCreate', 'sessionData':'" + JSON.stringify({ ExpRepId: expRepId, ExpDtlId: expDetailId }) + "', 'action':'set'}");
                    window.location.href = "ExpCreate.aspx";
                }
            }

        } catch (e) {
            var msg = JSON.stringify(e.stack);
            exceptionSaving(msg);
        }

        //To bind Expenses details
        function GenerateExcel() {
                        
            var tab_text = "<table  border='1'>";
            tab_text += '<tr style="font-weight:bold;text-align:center">'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Date </span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Report Ref #</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Employee ID</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Employee Name</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Cost Center</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Report Title</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Business Purpose</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Claim Expenses</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Claim Amount</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Reimbursed Amount</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Expense Type</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">City</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Payment Type</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Receipt Status</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Currency</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">Status</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">GL</span></td>';
                + '</tr>';
            
            $.each(expApprovalReports, function (key, col) {

                tab_text += '<tr>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eR_Date + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eR_RefNo + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.employeeId + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.employeeName + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.costCentre + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eR_Title + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eR_Purpose + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eD_ClaimExpense + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eD_TotalAmount + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.reimburseAmount + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eT_Desc + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eD_City + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eD_PayType + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eD_ReceiptStatus + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eD_Currency + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.approvalStatus + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eT_GLAccount + ' </span></td>';
                    + '</tr>';
            });
            tab_text = tab_text + "</table>";

            var date = new Date();
            var link = document.createElement("A");
            link.href = 'data:text/application/vnd.ms-excel,' + encodeURIComponent(tab_text);
            link.download = "ExpDetailsQueue_" + date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + '-' + date.getTime() + ".xls";
            link.click();

        }
    </script>

    <div class="body_container p-0 corp-exp-module">

        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">

                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left">Expense Details Queue </h3>

                    </div>

                </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <a class="btn btn-gray mr-2 " onclick="window.location.href='expdetailsqueue.aspx'">CLEAR <i class="icon icon-close "></i></a>
                    <a class="btn btn-primary " id="showCardsList" onclick="GetScreenData('SR')">SEARCH <i class="icon icon-search "></i></a>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">
                        <div class="form-group col-6 col-md-2">
                            <label>From Date</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="From Date" ID="txtFromDate" />
                                <div class="input-group-append" onclick="showDatepicker('txtFromDate')">
                                    <span class="input-group-text" id="basic-addon1"><i class="icon icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-6 col-md-2">
                            <label>To Date</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="To Date" ID="txtToDate" />
                                <div class="input-group-append" onclick="showDatepicker('txtToDate')">
                                    <span class="input-group-text" id="basic-addon2"><i class="icon icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Employee</label>
                            <select class="form-control" ID="ddlProfile">
                                <option Value="0">All</option>
                            </select>

                        </div>
                        <div class="form-group col-md-2">
                            <label>Cost Center</label>
                            <select class="form-control" ID="ddlCostCenter">
                                <option Value="0">All</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Receipt Status</label>
                            <select class="form-control" ID="ddlReceiptStatus">
                                <option Value="0">All</option>
                            </select>

                        </div>
                        <div class="form-group col-md-2">
                            <label>Payment Type</label>
                            <select class="form-control" ID="ddlPaymentType">
                                <option Value="0">All</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Report Ref #</label>
                            <input type="text" class="form-control" ID="txtRefNo" placeholder="Reference No" />
                        </div>
                        <div class="form-group col-md-2">
                            <label>Report Title</label>
                            <input type="text" class="form-control" placeholder="Title" ID="txtTitle" />
                        </div>
                        <div class="form-group col-md-2">
                            <label>Approver Status</label>
                            <select class="form-control" ID="ddlApproverStatus">
                                <option Value="All">All</option>
                                <option Value="O">Open</option>
                                <option Value="S">Sent For approval</option>
                                <option Value="PA">Partial approved</option>
                                <option Value="A">Approved</option>
                                <option Value="RV">Revise</option>
                                <option Value="RJ">Rejected</option>
                                <option Value="I">Payment Pending</option>
                                <option Value="R">Reiumbursed</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>

            <div class="exp-content-block" id="ExpDetailsList" style="overflow-x:scroll">

                <h5 class="mb-3 float-left">Expense Details</h5>
                <div class="button-controls text-right">
                    <a class="btn btn-primary " id="lnkPay" style="display:none" onclick="GetScreenData('PY')">PAY </a>
                    <a class="btn btn-primary " id="excelReport" onclick="GenerateExcel()">Export To Excel</a>
                </div>
                <div class="clear"></div>
                <div class="table-responsive">                    
                    <div id="divApproverGridInfo"></div>
                </div>
            </div>

        </div>
        <div class="clear "></div>
    </div>

</asp:Content>
