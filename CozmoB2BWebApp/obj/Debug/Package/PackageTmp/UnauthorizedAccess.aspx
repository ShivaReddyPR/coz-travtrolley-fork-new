<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="UnauthorizedAccess.aspx.cs" Inherits="UnauthorizedAccessUI" %>

    <!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

    <html xmlns="http://www.w3.org/1999/xhtml">

    <head runat="server">
        <title>UnAuthorized Access</title>
        <link href="https://fonts.googleapis.com/css?family=Quicksand:300,400" rel="stylesheet">
    </head>

    <body>
        <style>
            body {
                font-family: 'Quicksand', sans-serif;
            }
            
            #unsupported-message {}
            
            #unsupported-overlay,
            #unsupported-modal {
                display: block;
                position: absolute;
                position: fixed;
                left: 0;
                right: 0;
                z-index: 9999;
                background: #e9e9e9;
                color: #000000;
            }
            
            #unsupported-overlay {
                top: 0;
                bottom: 0;
            }
            
            #unsupported-modal {
                top: 80px;
                margin: auto;
                width: 90%;
                max-width: 520px;
                max-height: 90%;
                padding: 40px 20px;
                box-shadow: 0 10px 30px #e7e7e7;
                text-align: center;
                overflow: hidden;
                overflow-y: auto;
                border: solid 2px #ccc8b9;
                background-color: #fff;
            }
            
            #unsupported-message :last-child {
                margin-bottom: 0;
            }
            
            #unsupported-message h2 {
                font-size: 34px;
                color: #000;
                margin-bottom: 20px;
            }
            
            #unsupported-message h3 {
                font-size: 20px;
                color: #000;
            }
            
            body.hide-unsupport {
                padding-top: 24px;
            }
            
            body.hide-unsupport #unsupported-message {
                visibility: hidden;
            }
            
            #unsupported-banner {
                display: block;
                position: absolute;
                top: 0;
                left: 0;
                right: 0;
                background: #ccc8b9;
                color: #000;
                font-size: 14px;
                padding: 2px 5px;
                line-height: 1.5;
                text-align: center;
                visibility: visible;
                z-index: 100000;
            }
            
            @media (max-width: 800px),
            (max-height: 500px) {
                #unsupported-message .modal p {
                    /* font-size: 12px; */
                    line-height: 1.2;
                    margin-bottom: 1em;
                }
                #unsupported-modal {
                    position: absolute;
                    top: 20px;
                }
                #unsupported-message h1 {
                    font-size: 22px;
                }
                body.hide-unsupport {
                    padding-top: 0px;
                }
                #unsupported-banner {
                    position: static;
                }
                #unsupported-banner strong,
                #unsupported-banner u {
                    display: block;
                }
            }
        </style>
        <form id="form1" runat="server">

            <div id="unsupported-message">
                <a id="unsupported-overlay" href="#unsupported-modal"></a>
                <div id="unsupported-modal" role="alertdialog" aria-labelledby="unsupported-title">
                    <img src="https://travtrolley.com/images/logo.jpg" alt="" style="display:none">
                    <h2 id="unsupported-title">⚠ UnAuthorized Access !!! ⚠</h2>
                    <p> <a target="_blank" href="javascript:void(0)" onclick="window.history.go(-1); return false;">Go Back</a></p>

                    <p>Your account does not have access to this page.If you think you're seeing this message in error, <a href="mailto:helpdesk@cozmotravel.com">email us at helpdesk@cozmotravel.com</a></p>
                </div>
                <a id="unsupported-banner">
                    <asp:Label CssClass="lblMsg" ID="lblErrMsg" Text="" runat="server"></asp:Label>
                </a>
            </div>



        </form>
    </body>

    </html>