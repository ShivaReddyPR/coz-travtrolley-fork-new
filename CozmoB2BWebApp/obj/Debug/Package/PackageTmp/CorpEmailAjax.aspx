﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="CorpEmailAjax" Codebehind="CorpEmailAjax.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="System.Collections.Generic" %>
<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>

   <div id='EmailDiv' runat='server' style='width: 100%; display: none;'>
        <%if (isBookingSuccess)
          {
              List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();

              if (ticketList != null && ticketList.Count > 0)
              {
                  ptcDetails = ticketList[0].PtcDetail;
              }
              else// For Hold Bookings
              {
                  ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(flightItinerary.FlightId);
              }

              %>
              <%//Show the Agent details for First Email only. In case of Corporate booking second & third 
              //emails need not show Agent details in the email body
              if (emailCounter <= 1)
                { %>
        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                  <asp:Image ID='imgLogo' runat='server'  />
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
         <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                    <td style="width:70%;"></td>
                     <td align="right"><label style="padding-right: 20%"><strong>Agent Name:</strong></label></td>
                     <td align="left"><label style="padding-right: 10%"><strong><%=agency.Name%></strong></label></td>
                     </tr>
                        <tr> <td style="width:70%;"></td>
                         <td align="right"><label style="padding-right: 20%"><strong>Phone No:</strong></label></td>
                        <td align="left"><label style="padding-right: 10%"><strong><%=agency.Phone1%></strong></label></td>
                        </tr>
                    </table>
        <% }
           for (int count = 0; count < flightItinerary.Segments.Length; count++)
           {
               int paxIndex = 0;
               if (Request["paxId"] != null)
               {
                   paxIndex = Convert.ToInt32(Request["paxId"]);
               }

               List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
               ptcDetail = ptcDetails.FindAll(delegate(SegmentPTCDetail ptc) { return ptc.SegmentId == flightItinerary.Segments[count].SegmentId; });
               %>
        <table style='font-size: 12px;' width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>
                    <div style='border: solid 1px #000'>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                            <tr>
                                <td width='25%' style='background: #1C498A; height: 34px; line-height: 34px; padding-left: 20px;'>
                                    <font color='white'>
                                        <strong>Flight :
                                            <%=flightItinerary.Segments[count].Origin.CityName%>
                                            to
                                            <%=flightItinerary.Segments[count].Destination.CityName%></strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px;
                                    padding-left: 20px;'>
                                    <font color='white'>
                                        <strong>RESERVATION</strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px;
                                    padding-left: 20px;'>
                                    <font color='white'>
                                        <strong>
                                            <%=flightItinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%></strong></font>
                                </td>
                                <td width='25%' align='right' style='background: #1C498A; height: 34px; line-height: 34px;
                                            padding-left: 20px;'>
                                            <font color='white'><strong>Airline Ref : 
                                                <%=(flightItinerary.Segments[count].AirlinePNR == null || flightItinerary.Segments[count].AirlinePNR == "" ? (flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR) : (flightItinerary.Segments[count].AirlinePNR.Split('|').Length > 1 ? flightItinerary.Segments[count].AirlinePNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.Segments[count].AirlinePNR)) %>
                                                </strong></font>
                                        </td>
                            </tr>
                        </table>
                    </div>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='width: 100%;'>
                        <tr>
                            <td style='width: 57%;'>
                                <table style='background: #f3f6f8; width: 100%; float: left; border: solid 1px #cdcdcd;
                                    margin-top: 2px;'>
                                    <tr>
                                        <td style='width: 50%; float: left; height: 20px; line-height: 20px;'>
                                            <label style='padding-left: 20px'>
                                                <strong>Passenger Name's :</strong></label>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px;'>
                                            <%if(ticketList != null && ticketList.Count > 0){ %>
                                            <strong>Ticket No </strong>
                                            <%}else { // For Hold Bookings%>
                                            <strong>PNR </strong>
                                            <%} %>

                                        </td>
                                        <td style='width: 25%; float: left; height: 20px; line-height: 20px;'>
                                            <strong>Baggage </strong>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 43%;'>
                                <table style='background: #f3f6f8; width: 100%; float: right; border: solid 1px #cdcdcd;
                                    margin-top: 2px; height: 20px; line-height: 20px;'>
                                    <tr>
                                        <td>
                                            <label style='padding-left: 20px'>                                                
                                                <%if (ticketList != null && ticketList.Count > 0)
                                                  { %>
                                                <strong>Ticket Date:</strong>
                                                <%=ticketList[0].IssueDate.ToString("dd/MM/yyyy")%>
                                                <%}
                                                  else
                                                  {//for Corporate HOLD Booking %>
                                                <strong>Booking Date:</strong>
                                                <%=flightItinerary.CreatedOn.ToString("dd/MM/yyyy")%>
                                                <%} %>
                                                </label>
                                        </td>
                                        <td>
                                            <label style='padding-left: 20px'>
                                                <strong>Reference No :</strong>
                                                <%=booking.BookingId%></label>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%for (int j = 0; j < flightItinerary.Passenger.Length; j++)
              { %>
            <tr>
                <td>
                    <table style='width: 100%;'>
                        <tr>
                            <td style='width: 57%;'>
                                <table style='background: #f3f6f8; width: 100%; float: left; border: solid 1px #cdcdcd;
                                    margin-top: 2px;'>
                                    <tr>
                                        <td style='width: 50%; float: left; height: 20px; line-height: 20px;'>
                                            <label style='margin-left: 20px; padding: 2px; background: #EBBAD9;'>
                                                <strong>
                                                    <%=flightItinerary.Passenger[j].Title + " " + flightItinerary.Passenger[j].FirstName + " " + flightItinerary.Passenger[j].LastName%>
                                                    </strong></label>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px;'>
                                        <%if(ticketList != null && ticketList.Count > 0){ %>
                                            <%=(ticketList[j].TicketNumber.Split('|').Length > 1 ? ticketList[j].TicketNumber.Split('|')[flightItinerary.Segments[count].Group] : ticketList[j].TicketNumber) %>
                                            <%}else{ //for Corporate HOLD Booking%>
                                             <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR)%>
                                            <%} %>
                                        </td>
                                        <td style='width: 20%; float: left; height: 20px; line-height: 20px;text-align:right;'>
                                                
                                                 <%if (flightItinerary.FlightBookingSource == BookingSource.UAPI || (flightItinerary.FlightBookingSource == BookingSource.TBOAir && !(flightItinerary.IsLCC)))
                                                   {
                                                       if (Request["bkg"] == null || Request["bkg"] != null)
                                                       {
                                                           if (ptcDetail.Count > 0)
                                                           {
                                                               string baggage = "";
                                                               foreach (SegmentPTCDetail ptc in ptcDetail)
                                                               {
                                                                   switch (flightItinerary.Passenger[j].Type)
                                                                   {
                                                                       case PassengerType.Adult:
                                                                           if (ptc.PaxType == "ADT")
                                                                           {
                                                                               baggage = ptc.Baggage;
                                                                           }
                                                                           break;
                                                                       case PassengerType.Child:
                                                                           if (ptc.PaxType == "CNN")
                                                                           {
                                                                               baggage = ptc.Baggage;
                                                                           }
                                                                           break;
                                                                       case PassengerType.Infant:
                                                                           if (ptc.PaxType == "INF")
                                                                           {
                                                                               baggage = ptc.Baggage;
                                                                           }
                                                                           break;
                                                                   }
                                                               }

                                                               if (baggage.Length > 0 && !baggage.Contains("Bag") && !baggage.ToLower().Contains("piece") && !baggage.ToLower().Contains("unit") && !baggage.ToLower().Contains("units"))
                                                               {%>
                                                            <%=(baggage.ToLower().Contains("kg") ? baggage : baggage + " Kg")%> 
                                                         
                                                          <%}
                                                               else
                                                               {%>
                                                            <%=baggage%>
                                                          <%}
                                                           }//End PtcDetail.Count
                                                       }//End Request("bkg"]
                                                   }
                                                   else if (flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
                                                   {
                                                       if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
                                                       { %>
                                                            <%=flightItinerary.Passenger[j].BaggageCode.Split(',')[flightItinerary.Segments[count].Group]%> 
                                                     <%}
                                                   }
                                                   else if (flightItinerary.FlightBookingSource == BookingSource.AirArabia)
                                                   {
                                                       if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
                                                       {%>
                                                            <%=flightItinerary.Passenger[j].BaggageCode.Split(',')[flightItinerary.Segments[count].Group]%>
                                                     <%}
                                                   }
                                                   else if (flightItinerary.FlightBookingSource == BookingSource.TBOAir && (flightItinerary.IsLCC))
                                                   {
                                                       if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
                                                       {
                                                           string strBaggage = string.Empty;
                                                           if (flightItinerary.Passenger[j].BaggageCode != string.Empty && flightItinerary.Passenger[j].BaggageCode.Split(',').Length > 1)
                                                           {
                                                               strBaggage = flightItinerary.Passenger[j].BaggageCode.Split(',')[flightItinerary.Segments[count].Group];
                                                           }
                                                           else if (flightItinerary.Passenger[j].BaggageCode.Split(',').Length <= 1)
                                                           {
                                                               strBaggage = flightItinerary.Passenger[j].BaggageCode;
                                                           }
                                                           else
                                                           {
                                                               strBaggage = "Airline Norms";
                                                           }
                                                           %>
                                                      <%=strBaggage%> 
                                                    <%}
                                                   } %>
                                            
                                                </td>
                                    </tr>
                                </table>
                            </td>
                            <td style='width: 43%;'>
                                <table style='background: #f3f6f8; width: 100%; float: right; border: solid 1px #cdcdcd;
                                    margin-top: 2px; height: 20px; line-height: 20px'>
                                    <tr>
                                        <td style='width:50%'>
                                            <label style='padding-left: 20px'>
                                                <strong>PNR No : </strong>
                                                <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR)%></label>
                                        </td>
                                        <%if(!string.IsNullOrEmpty(flightItinerary.TripId)){ %>
                                        <td style='width:50%;text-align:right'>                                        
                                        <strong>Corporate Booking Code</strong>
                                        <%=flightItinerary.TripId %>
                                        </td>
                                        <%} %>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%} %>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left'>
                                <label style='padding-left: 20px'>
                                    Date:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].DepartureTime.ToString("MMM dd yyyy (ddd)")%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Status:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 40%; float: left;'>
                                <label style='padding-left: 20px; color: #08bd48;'>
                                    <strong>
                                        <%=booking.Status.ToString()%></strong></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Departs:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].DepartureTime.ToString("hh:mm tt")%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Arrives:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].ArrivalTime.ToString("hh:mm tt")%>
                                </label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airline:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%try
                                      {
                                          CT.Core.Airline airline = new CT.Core.Airline();
                                          airline.Load(flightItinerary.Segments[count].Airline);%>
                                    <%=airline.AirlineName%>
                                    <%}
                                      catch { } %>
                                </label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Flight:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Airline + " " + flightItinerary.Segments[count].FlightNumber%>
                                     <%try
                                       {  //Loading Operating Airline and showing
                                           if (!flightItinerary.IsLCC && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0)
                                           {
                                               string opCarrier = flightItinerary.Segments[count].OperatingCarrier;
                                               CT.Core.Airline opAirline = new CT.Core.Airline();
                                               if (opCarrier.Split('|').Length > 1)
                                               {
                                                   opAirline.Load(opCarrier.Split('|')[0]);
                                               }
                                               else
                                               {
                                                   opAirline.Load(opCarrier.Substring(0, 2));
                                               } %>
                                                        (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                       }
                                       catch { } %>
                                </label>
                            </td>
                        </tr>
                    </table>
                    
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    From:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Origin.CityName + ", " + flightItinerary.Segments[count].Origin.CountryName%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airport:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Origin.AirportName + ", " + flightItinerary.Segments[count].DepTerminal%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    To:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Destination.CityName + ", " + flightItinerary.Segments[count].Destination.CountryName%></label>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Airport:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Destination.AirportName + ", " + flightItinerary.Segments[count].ArrTerminal%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Class:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].CabinClass%></label>
                                    <%if (flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
                                      {%>                                    
                                      <label style='padding-left: 20px'><label style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                        width: 25%; '>Fare Type : </label> <%=flightItinerary.Segments[count].SegmentFareType%></label>                                                                             
                                      <%} %>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Duration:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].Duration%></label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <%--<tr>
                <td>
                    <table style='background: #fff; border: solid 1px #cdcdcd;' width='100%' cellspacing='0'
                        cellpadding='0'>
                        <tr style='height: 20px; line-height: 20px; margin-top: 2px;'>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Class:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 38%; float: left;'>
                                <label style='padding-left: 20px'>
                                    <%=flightItinerary.Segments[count].CabinClass%></label>
                                    <%if (flightItinerary.FlightBookingSource == BookingSource.FlyDubai)
                                               {%>                                    
                                      <label style='padding-left: 20px'><label style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                        width: 25%; '>Fare Type : </label> <%=flightItinerary.Segments[count].SegmentFareType%></label>                                                                             
                                      <%} %>
                            </td>
                            <td style='background: #edf5fb; height: 20px; line-height: 20px; font-weight: bold;
                                width: 10%; float: left;'>
                                <label style='padding-left: 20px'>
                                    Baggage:</label>
                            </td>
                            <td style='height: 20px; line-height: 20px; width: 42%; float: left;'>
                                <label style='padding-left: 20px'>
                                <%if (flightItinerary.FlightBookingSource == BookingSource.UAPI)
                                  {
                                      if (ptcDetail.Count > 0)
                                      {
                                          string baggage = "";
                                          foreach (SegmentPTCDetail ptc in ptcDetail)
                                          {
                                              if (baggage.Length > 0)
                                              {
                                                  baggage += "," + ptc.Baggage;
                                              }
                                              else
                                              {
                                                  baggage = ptc.Baggage;
                                              }
                                          }%>
                                                            <%=baggage%>
                                     <%}
                                      else
                                      {%>
                                          30Kg
                                   <% }%>
                                        <%}
                                  else if (flightItinerary.FlightBookingSource == BookingSource.FlyDubai)
                                      {
                                          if (flightItinerary.Segments[count].Group == 0)
                                          {%>
                                                    <%=inBaggage%>
                                                    <%}
                                          else
                                          { %>
                                                   <%=outBaggage%>
                                                    <%}
                                      }
                                      else
                                      {
                                          if (flightItinerary.Segments.Length > 2 && flightItinerary.FlightBookingSource == BookingSource.AirArabia)
                                          {%>
                                    <%if ((count + 1) <= 2)
                                      {%>
                                    <%=inBaggage%>
                                    <%}
                                      else
                                      { %>
                                    <%=outBaggage%>
                                    <%}
                                          }
                                      else
                                      { %>
                                    <%if ((count + 1) <= 1)
                                      {%>
                                    <%=inBaggage%>
                                    <%}
                                      else
                                      { %>
                                    <%=outBaggage%>
                                    <%}
                                          } %>
                                    <%} %>
                                </label>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>--%>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <%} %>
        </table>
        <table style='font-size: 12px;' width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0;

                      if (ticketList != null && ticketList.Count > 0)
                      {
                          for (int k = 0; k < ticketList.Count; k++)
                          {
                              AirFare += ticketList[k].Price.PublishedFare;
                              Taxes += ticketList[k].Price.Tax + ticketList[k].Price.Markup;
                              if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                              {
                                  Taxes += ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.OtherCharges + ticketList[k].Price.SServiceFee + ticketList[k].Price.TransactionFee;
                              }
                              Baggage += ticketList[k].Price.BaggageCharge;
                              MarkUp += ticketList[k].Price.Markup;
                              Discount += ticketList[k].Price.Discount;
                              AsvAmount += ticketList[k].Price.AsvAmount;
                          }
                          if (ticketList[0].Price.AsvElement == "BF")
                          {
                              AirFare += AsvAmount;
                          }
                          else if (ticketList[0].Price.AsvElement == "TF")
                          {
                              Taxes += AsvAmount;
                          }
                      }
                      else
                      {
                          for (int k = 0; k < flightItinerary.Passenger.Length; k++)
                          {
                              AirFare += flightItinerary.Passenger[k].Price.PublishedFare;
                              Taxes += flightItinerary.Passenger[k].Price.Tax + flightItinerary.Passenger[k].Price.Markup;
                              if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                              {
                                  Taxes += flightItinerary.Passenger[k].Price.AdditionalTxnFee + flightItinerary.Passenger[k].Price.OtherCharges + flightItinerary.Passenger[k].Price.SServiceFee + flightItinerary.Passenger[k].Price.TransactionFee;
                              }
                              Baggage += flightItinerary.Passenger[k].Price.BaggageCharge;
                              MarkUp += flightItinerary.Passenger[k].Price.Markup;
                              Discount += flightItinerary.Passenger[k].Price.Discount;
                              AsvAmount += flightItinerary.Passenger[k].Price.AsvAmount;
                              if (flightItinerary.Passenger[k].Price.AsvElement == "BF")
                              {
                                  AirFare += AsvAmount;
                              }
                              else if (flightItinerary.Passenger[k].Price.AsvElement == "TF")
                              {
                                  Taxes += AsvAmount;
                              }
                          }
                      }
                    %>
                     <% if(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId<=1)
                              {
                                  
                                  %>
                        <table width='100%' border='0' cellspacing='0' cellpadding='0'>
                        <tr>
                            <td>
                                <table width='100%' cellspacing='0' cellpadding='0' border='0'>
                                    <tbody>
                                        <tr>
                                            <td width='41%'>
                                            </td>
                                            <td>
                                                <table width='100%' class=''>
                                                    <tbody>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Air Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=AirFare.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Taxes & Fees</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Taxes.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.TBOAir && Baggage > 0 || flightItinerary.IsLCC ||flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp  )
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Baggage Fare</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Baggage.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <%if (Discount > 0)
                                                          { %>
                                                        <tr>
                                                            <td style='text-align: left!important;'>
                                                                <b>Discount</b>
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td style='text-align: right!important;'>
                                                                <%=Discount.ToString("N"+agency.DecimalValue)%>
                                                                <%=agency.AgentCurrency %>
                                                            </td>
                                                        </tr>
                                                        <%} %>
                                                        <tr>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: left!important;'>
                                                                <b>Total Air Fare</b>
                                                            </td>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                :
                                                            </td>
                                                            <td bgcolor='#F9F9F9' style='border-top: 1px solid #aaa!important; text-align: right!important;'>
                                                                <b>
                                                                   <%if (flightItinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                                                                     { %>
                                                                    <%=Math.Ceiling((AirFare + Taxes + Baggage) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                    <%}
                                                                     else
                                                                     { %>
                                                                     <%=((AirFare + Taxes + Baggage) - Discount).ToString("N" + agency.DecimalValue)%>
                                                                     <%} %>
                                                                    <%=agency.AgentCurrency%></b>
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                    </table>
                    
                      <%} %>
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
        <%} %>
    </div>

</body>
</html>
