﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="DealSheetGUI" Title="Deal Sheet" Codebehind="DealSheet.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<style>
.alternateRow 
{
	background:#f0f0f0;
	font-size:small;
	height:auto;
}
</style>
    <asp:GridView ID="gvNotice" runat="server" AutoGenerateColumns="False" AllowPaging="true"
        PageSize="15" CellPadding="0" Width="100%" CellSpacing="0" Height="137px" OnPageIndexChanging="gvNotice_PageIndexChanging" HeaderStyle-Font-Size="Medium" HeaderStyle-Font-Bold="true" AlternatingRowStyle-BackColor="#f0f0f0">
        <Columns>
            <asp:TemplateField HeaderText="S.No">
                <ItemTemplate>
                    <%# ((GridViewRow)Container).RowIndex + 1%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="ReceivedOn" HeaderText="Received On" ControlStyle-Width="15%"
                ItemStyle-Width="15%"></asp:BoundField>
            <asp:BoundField DataField="subject" HeaderText="Subject" ControlStyle-Width="25%"
                ItemStyle-Width="25%" ControlStyle-Font-Bold="true"></asp:BoundField>
            <asp:BoundField DataField="message" HeaderText="Notice" ControlStyle-Width="60%"
                ItemStyle-Width="60%"></asp:BoundField>
        </Columns>
       <HeaderStyle CssClass="gvHeader" />
        
    </asp:GridView>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

