﻿<%@ Page Language="C#" MasterPageFile="~/Transaction.master" AutoEventWireup="true" Inherits="Register" Title="Register" Codebehind="Register.aspx.cs" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
 <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script type="text/javascript" src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <title>Register</title>
    <link href="css/CTStyle.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <link href="css/override.css" rel="stylesheet" />
    <link href="App_Themes/Ibyta/Default.css" rel="stylesheet" type="text/css" />
     <script src="https://www.google.com/recaptcha/api.js?onload=onloadCallback&render=explicit"
        async defer>
    </script>
    <script src="scripts/Captcha.js" type="text/javascript"></script>

    <style type="text/css">
.required:after 
{
    content: "*";
    font-weight: bold;
    color: red; 
}
#ctl00_cphTransaction_txtAddress{
display: block;
    width: 100%;
  
    padding: 6px 6px;
    font-size: 14px;
    line-height: 1.42857143;
    color: #555;
    background-color: #fff;
    background-image: none;
    border: 1px solid #ccc;
    border-radius: 4px;
    -webkit-box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    box-shadow: inset 0 1px 1px rgba(0,0,0,.075);
    -webkit-transition: border-color ease-in-out .15s,-webkit-box-shadow ease-in-out .15s;
    -o-transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    transition: border-color ease-in-out .15s,box-shadow ease-in-out .15s;
    
}
.container.header_top{
    display:none !important;
}
.ibyta--top-header {
    background-color: #ec0b43;
    padding: 5px 0 5px 0;
}
</style>
 <script type="text/javascript">
     var widgetId1;     
    
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function checkEmail(inputvalue) {
            var pattern = /^([a-zA-Z0-9_.-])+@([a-zA-Z0-9_.-])+\.([a-zA-Z])+([a-zA-Z])+/;
            if (pattern.test(inputvalue)) {
                return true;
            }
            else {
                return false;
            }
        }
        
     function Save() {
         var valid = false;
         var ValidationMessage = "";
         //var CheckTerms = false;
         var Agency = document.getElementById('<%= txtAgency.ClientID %>').value
         var Address = document.getElementById('<%= txtAddress.ClientID %>').value
         var PhoneNo = document.getElementById('<%= txtTelephone.ClientID %>').value;
         var Email = document.getElementById('<%= txtEmail.ClientID %>').value
         var ValidEmail = checkEmail(document.getElementById('<%= txtEmail.ClientID %>').value)
         var Country = document.getElementById('<%=ddlCountry.ClientID%>').value;
         //var CheckTerms = document.getElementById('chkAgrement').checked;

         if (Agency.length == 0) {
             ValidationMessage += " Agency Cannot be blank  \n";
         }
         if (Address.length == 0) {
             ValidationMessage += " Address Cannot be blank \n";
         }
         if (PhoneNo.length == 0) {
             ValidationMessage += " Phone No. Cannot be Blank \n";
         }
         if (PhoneNo.length < 10) {
             ValidationMessage += " Please enter valid Phone number \n";
         }
         if (Email.length == 0) {
             ValidationMessage += " Please Enter Email  \n";
         }
         if (Email.length > 0) {
             if (!ValidEmail) {
                 ValidationMessage += " Please Enter Valid E-mail  \n";
             }
         }
         if (Country == -1) { ValidationMessage += " Please select country from the list! \n"; }

         if (Agency.length == 0 || PhoneNo.length == 0 || Email == 0 || Address.length == 0 || (PhoneNo.length < 10) || Country == -1) {
             alert(ValidationMessage);
             return valid;
         }
         else {
             var resp = grecaptcha.getResponse(widgetId1);
             if (resp != "" && resp.length > 0) {
                 $('#ctl00_cphTransaction_hdnreCaptchaResponse').val(resp);
                 grecaptcha.reset(widgetId1);
                 return true;
             }
             else {
                 alert('Please verify that you are not robot by checking the checkbox!');                 
                 return false;
             }
         }
     }
    </script>
 <div class="body_container paramform">
     <div class="ibyta--top-header">
			<div class="container">
				<div class="row">
					<div class="col-xs-6">
						<img src="images/ibyta-logo.jpg" class="img-responsive" style="max-width: 130px;margin-left: -15px;">
					</div>
				</div>
			</div>
		</div>  
     <asp:HiddenField ID="hdnreCaptchaResponse" runat="server" />
     

        <div>
            
            <div> 
                <div class="col-md-12">
                    <h4>
                        Register</h4>
                </div>
                <div class="marbot_10">
                    <div class=" col-md-3">
                        Name of Agency:<span class='required'> </span>
                    </div>
                    <div class=" col-md-6">
                        <asp:TextBox ID="txtAgency" runat="server"    class=" form-control"> </asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="marbot_10">
                    <div class=" col-md-3">
                        Address:<span class='required'></span>
                    </div>
                    <div class=" col-md-6">
                        <asp:TextBox ID="txtAddress"  Rows="4" Columns="40" TextMode="MultiLine"   runat="server"></asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="marbot_10">
                    <div class=" col-md-3">
                        Post Box No:
                    </div>
                    <div class=" col-md-6">
                        <asp:TextBox ID="txtPostBoxNo" runat="server" onkeypress = "return isNumber(event);"    class=" form-control"></asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                
                <div class="marbot_10">
                    <div class=" col-md-3">
                        Nature of Business:
                    </div>
                    <div class=" col-md-6">
                        <asp:TextBox ID="txtNatureOfBussiness" runat="server"   class=" form-control"></asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="marbot_10">
                    <div class=" col-md-3">
                        Name of Owners:
                    </div>
                    <div class=" col-md-6">
                        <asp:TextBox ID="txtNameOfOwners" runat="server"  class=" form-control"></asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="marbot_10">
                    <div class=" col-md-3">
                        Phone Number:<span class='required'></span>
                    </div>
                    <div class=" col-md-6">
                        <asp:TextBox ID="txtTelephone" runat="server"  onkeypress = "return isNumber(event);" class=" form-control"></asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="marbot_10">
                    <div class=" col-md-3">
                        Email:<span class='required'></span>
                    </div>
                    <div class=" col-md-6">
                        <asp:TextBox ID="txtEmail" runat="server" class=" form-control"></asp:TextBox>
                                
                    </div>
                    <div class="clearfix">
                    </div>
                </div>

                 <div class="marbot_10">
                    <div class=" col-md-3">
                        Website:
                    </div>
                    <div class=" col-md-6">
                        <asp:TextBox ID="txtWebsite" runat="server" class=" form-control"></asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                <div class="marbot_10">
                    <div class=" col-md-3">
                        Trade License No:
                    </div>
                    <div class=" col-md-6">
                        <asp:TextBox ID="txtTradeLicenseNo" runat="server"   class=" form-control"></asp:TextBox>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
               
                <div class="marbot_10">
                    <div class=" col-md-3">
                        Exp. Date:
                    </div>
                    <div class=" col-md-6">
                        <uc1:DateControl ID="dcLicExpDate" runat="server" BaseYearLimit="0" DateFormat="DDMMMYYYY"
                            DateOnly="true" DropDownYears="10" Enabled="true" HorizontalAlignment="Left"
                            OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down"
                            WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday">
                        </uc1:DateControl>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>
                 <div class="marbot_10">
                    <div class=" col-md-3">
                       Country :<span class='required'></span>
                    </div>
                    <div class=" col-md-6">
                        <asp:DropDownList runat="server" ID="ddlCountry" CssClass="inputDdlEnabled form-control"></asp:DropDownList>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>

              <%--  <div class="marbot_10">
                    <div class="col-md-3">
                    </div>
                    <div class=" col-md-6">
                        <label>
                            <input type="checkbox" id="chkAgrement"  />
                            I accept <a target="_blank" href="terms.aspx">terms & conditions</a>.</label>
                    </div>
                    <div class="clearfix">
                    </div>
                </div>--%>
                <div class="marbot_10">
                    <div class=" col-md-9">
                   <div id="captchaDiv"></div>
                    <div>
                        <asp:Button ID="btnRegister" CssClass="btn but_b pull-right marleft_10" runat="server" Text="Register" OnClientClick="return Save();"
                            OnClick="btnRegister_Click" />
                        <asp:Button ID="btnClear" CssClass="btn but_b pull-right marleft_10" runat="server" Text="Cancel" OnClick="btnClear_Click" />
                        <div class="clearfix">
                        </div>
                    </div>
                    <asp:Label ID="lblSuccessMsg" runat="server" Text=""></asp:Label>
                    <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
                    <div class="clearfix">
                    </div>
                </div>
           
            <div class="clearfix">
            </div>
        </div>
    </div>
    
    
    </div>
    
    </div>
</asp:Content>

