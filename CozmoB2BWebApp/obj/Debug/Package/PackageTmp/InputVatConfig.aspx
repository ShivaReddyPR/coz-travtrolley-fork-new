﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="InputVatConfigGUI" MasterPageFile="~/TransactionBE.master" Title="InputVat Config" Codebehind="InputVatConfig.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<asp:HiddenField runat="server" ID="hdfInvatId" Value="0"></asp:HiddenField>
 <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="lblCountry" Text="Country:" runat="server"></asp:Label><sup style="color:Red">*</sup>
                </div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="inpuTddlEnabled form-control" >
                    </asp:DropDownList>
                </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="lblSupplierCountry" Text="Supplier Country:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlSupplierCountry" runat="server" CssClass="inpuTddlEnabled form-control" >
                    </asp:DropDownList>
                </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="lblDestinationType" Text="Destination Type:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                           <asp:RadioButton GroupName="DestinationType" value="1" ID="rbInternational" runat="server" Text="InterNational" />
                           <asp:RadioButton GroupName="DestinationType" value="0" ID="rbDomestic" runat="server" Text="Domestic" />
                </div>
                </div>
                 <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="lblProduct" Text="Invat Product:"  runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlProduct" runat="server" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true" CssClass="inpuTddlEnabled form-control" >
                   
                    </asp:DropDownList>
                </div>
                </div>
                 <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="lblInvatModule" Text="Invat Module:"  runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlModule" runat="server" onchange="ValidateModule()"   CssClass="inpuTddlEnabled form-control" >
                    
                    </asp:DropDownList>
                </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="lblInvatApplied" Text="Invat Applied:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                <asp:RadioButton ID="rbInvatApplied" runat="server" Text="True" GroupName="InvatApplied" />
                <asp:RadioButton ID="rbInvatNotApplied" runat="server" Text="False" GroupName="InvatApplied" />
                </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="Label1" Text="Invat Value:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                           <asp:TextBox ID="txtInvatvalue" MaxLength="2" onkeypress="return isNumber(event);" runat="server" CssClass="inputEnabled form-control"></asp:TextBox>
                </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="lblInvatCostIncluded" Text="Invat Cost Included:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                <asp:RadioButton ID="rbInvatCostIncluded" runat="server" Text="True" GroupName="InvatCostIncluded" />
                <asp:RadioButton ID="rbInvatCostNotIncluded" runat="server" Text="False" GroupName="InvatCostIncluded" />
                          
                </div>
                </div>
                <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="lblInvatCalculate" Text="Invat Calculate:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                 <asp:RadioButton ID="rbInvatCalculate" runat="server" Text="True" GroupName="InvatCalculate" />
                <asp:RadioButton ID="rbInvatNotCalculate" runat="server" Text="False" GroupName="InvatCalculate" />
                </div>
                </div>
                 <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-2">
                    <asp:Label ID="lblInvatRemarks" Text="Invat Remarks:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                           <asp:TextBox ID="txtRemarks" runat="server" CssClass="inputEnabled form-control" TextMode="MultiLine" Height="80px"></asp:TextBox>
                </div>
                </div>
            <asp:Button ID="btnSave" CssClass="button" Text="Save" runat="server" Style="display: inline;" OnClientClick="return Save();" OnClick="btnSave_Click"  />&nbsp;&nbsp;
                                    <asp:Button ID="btnClear" CssClass="button" Text="Clear" runat="server" Style="display: inline;" OnClick="btnClear_Click" />&nbsp;&nbsp;
                                    <asp:Button ID="btnSearch" CssClass="button" Text="Search" runat="server"  Style="display: inline;" OnClick="btnSearch_Click" />
            <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>
            
            <script type="text/javascript">
                function Save() {
                    if (getElement('ddlCountry').selectedIndex <= 0) addMessage('Please select Country from the list!', '');
                    if (getElement('ddlSupplierCountry').selectedIndex <= 0) addMessage('Please select Supplier Country from the list!', '');
                    if (getElement('rbInternational').checked == false && getElement('rbDomestic').checked == false) addMessage('Please select Destination Type!', '');
                    if (getElement('ddlProduct').selectedIndex <= 0) addMessage('Please select Product from the list!', '');
                    if (getElement('ddlModule').selectedIndex <= 0) addMessage('Please select Module from the list!', '');
                    if (getElement('rbInvatApplied').checked == false && getElement('rbInvatNotApplied').checked == false) addMessage('Please select Vat Applied!', '');
                    if (getElement('txtInvatvalue').value == '') addMessage('Vat Value cannnot be blank!', '');
                    if (getElement('rbInvatCostIncluded').checked == false && getElement('rbInvatCostNotIncluded').checked == false) addMessage('Please select Cost Included!', '');
                    if (getElement('rbInvatCalculate').checked == false && getElement('rbInvatNotCalculate').checked == false) addMessage('Please select Vat Calculate!', '');
                    if (getMessage() != '') {

                        alert(getMessage()); clearMessage();
                        return false;
                    }
                }
                function isNumber(evt) {
                    evt = (evt) ? evt : window.event;
                    var charCode = (evt.which) ? evt.which : evt.keyCode;
                    if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                        return false;
                    }
                    return true;
                }
                function ValidateModule() {
                    var module = document.getElementById("<%=ddlModule.ClientID%>");
                    var modulevalue = module.options[module.selectedIndex].text;
                    var product = document.getElementById("<%=ddlProduct.ClientID%>");
                    var productvalue = product.options[product.selectedIndex].text;
                    if (productvalue == 'Flight') {
                        if (modulevalue != 'Ticket' && modulevalue != 'Seat' && modulevalue != 'Meal') {
                            alert("Module is not exist in Product");
                            document.getElementById("<%=ddlModule.ClientID%>").value = -1;
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                    if (productvalue == 'Hotel') {
                        if (modulevalue != 'Hotel') {
                            alert("Module is not exist in Product");
                            document.getElementById("<%=ddlModule.ClientID%>").value = -1;
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                    if (productvalue == 'Packages') {
                        if (modulevalue != 'Package') {
                            alert("Module is not exist in Product");
                            document.getElementById("<%=ddlModule.ClientID%>").value = -1;
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                    if (productvalue == 'Visa') {
                        if (modulevalue != 'Visa') {
                            alert("Module is not exist in Product");
                            document.getElementById("<%=ddlModule.ClientID%>").value = -1;
                            return false;
                        }
                        else {
                            return true;
                        }
                    }
                    else {
                        return true;
                    }
                }
            </script>
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
<asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="invat_id"
        EmptyDataText="No Data Found!" AutoGenerateColumns="false" PageSize="10" GridLines="none" CssClass="grdTable"  CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged"
       >
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />" ControlStyle-CssClass="label" ShowSelectButton="True" />
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtCountry" Width="100px" CssClass="inputEnabled" HeaderText="Country" OnClick="FilterSearch_Click"  runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblCountry" runat="server" Text='<%# Eval("invatcountrycode") %>' CssClass="label grdof">' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtSuppCountry" Width="100px" CssClass="inputEnabled" HeaderText="Supplier Country"  runat="server" OnClick="FilterSearch_Click" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblSuppCountry" runat="server" Text='<%# Eval("invatsuppliercountrycode") %>' CssClass="label grdof">' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtDestinationType" Width="100px" CssClass="inputEnabled" HeaderText="Destination Type"  runat="server" OnClick="FilterSearch_Click" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblDestType" runat="server" Text='<%# Eval("invat_destination_type") %>' CssClass="label grdof">' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtProductType" Width="100px" CssClass="inputEnabled" HeaderText="Product Type"  runat="server" OnClick="FilterSearch_Click" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblProductType" runat="server" Text='<%# Eval("invat_product") %>' CssClass="label grdof">' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Module">
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtModule" Width="100px" CssClass="inputEnabled" HeaderText="Module"  runat="server" OnClick="FilterSearch_Click" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblModule" runat="server" Text='<%# Eval("invat_module") %>' CssClass="label grdof">' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
           </Columns>
           </asp:GridView>
</asp:content>

