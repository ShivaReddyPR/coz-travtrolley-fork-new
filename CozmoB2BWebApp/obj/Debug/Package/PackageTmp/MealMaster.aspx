<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="MealMasterUI" Title="Meal Master" Codebehind="MealMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">


<style> 

.tbl2 input, select  { width:200px; height:24px; border: solid 1px #ccc; }

.tbl2 td  { line-height:30px; }

.gray { color:#ccc; }


textarea { width:240px; border: solid 1px #ccc; height:50px; } 
.select2-container {
    width: 100%;
}

</style>

<br /> 
<div class="table-responsive">


<table class="tbl2 table my-0 mx-auto table-bordered" style="    width: 50% !important;
    float: none;">
  <tr>
    <td width="24%" align="right">Agent</td>
    <td width="76%"><select name="select" id="select">
      <option> Select</option>
      <option>------------</option>
    </select></td>
  </tr>
 
  <tr>
    <td align="right">Meal name:</td>
    <td>
    
    
    
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="8%">
        <input type="text" name="textfield" id="textfield" />
        
        </td>
        <td width="92%"> &nbsp;<span class="gray"> Example:</span> Chieken Biriyani</td>
      </tr>
    </table>
    
    
    </td>
  </tr>
  <tr>
    <td align="right">Meal Type:</td>
    <td>
    <select name="select2" id="select1">
          
          <option>Dinner</option>
          <option>Launch</option>
           <option>Breakfast</option>
            
          
          <option selected="selected">Select</option>
          <option>------------</option>
        </select>
        
    </td>
  </tr>
  
  
  
  
  
  
  <tr>
    <td align="right">Rigion: </td>
    <td><table width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr>
        <td width="50%"><select name="select2" id="select2">
          
          <option>Asia</option>
          <option>Middle East</option>
          
          
          <option selected="selected">Select</option>
          
          <option>------------</option>
        </select></td>
        <td width="50%"> &nbsp;<span class="gray"> Example:</span> Asia</td>
      </tr>
    </table></td>
  </tr>
  
  
    <tr>
    <td align="right">City: </td>
    <td> <select name="select2" id="select4">
          
         <option selected="selected">All</option>
          <option>Bandar Seri Begawan</option>
          <option>Bangkok</option>
          
          <option>Dubai</option>
           <option>London</option>
           
          
          
        </select></td>
  </tr>
  
  
  <tr>
    <td align="right">Currency</td>
    <td>
    <select name="select3" id="select3">
          
           
          <option>BND</option>
          <option>AED</option>
          <option>USD</option>
          
          <option selected="selected">Select</option>
          <option>------------</option>
        </select>
    
    </td>
  </tr>

  <tr>
    <td align="right">Fee</td>
    <td><input type="text" name="textfield6" id="textfield6" /></td>
  </tr>

  

  <tr>
    <td align="right">&nbsp;</td>
    <td height="40"><input style="height:30px; width:auto"; class="button-normal" type="submit" name="button" id="button" value="Submit" /></td>
  </tr>
</table>

    </div>
</asp:Content>

