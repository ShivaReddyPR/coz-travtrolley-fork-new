﻿<%@ Page Title="Reimburse Report" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpReimReport.aspx.cs" Inherits="CozmoB2BWebApp.ExpReimReport" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <style>
        .body_container {
            padding: 0 !important;
        }
    </style>
    
    <link href="css/toastr.css" rel="stylesheet" type="text/css" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />

    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="scripts/Common/ExpenseObjects.js" type="text/javascript"></script>    
    <script src="scripts/jquery-ui.js"></script>

    <script type="text/javascript">

        var scAction = '', apiAgentInfo = {}, reimburseData = [];

        try {

            $(document).ready(function () {
                $("#ctl00_upProgress").hide();

                /* Date Control for From Date*/
                var FromDate = new Date();

                $("#txtFromDate").datepicker(
                    {
                        changeYear: true,
                        changeMonth: true,
                        minDate: -300,
                        dateFormat: 'dd/mm/yy',
                        onSelect: function (dateText, inst) {
                            var selectedDate = new Date(ConvertDate(dateText));
                            selectedDate.setDate(selectedDate.getDate() + 1);
                            $("#txtToDate").datepicker("option", "minDate", selectedDate);
                        }
                    }
                ).datepicker("setDate", FromDate);

                /* Date Control for To Date*/
                $("#txtToDate").datepicker(
                    {
                        changeYear: true,
                        changeMonth: true,
                        dateFormat: 'dd/mm/yy'
                    }).datepicker("setDate", FromDate);

                $("#txtFromDate").change(function () {
                    var dtToday = new Date($("#txtFromDate").val());
                    $("#txtToDate").datepicker(
                        {
                            changeYear: true,
                            changeMonth: true,
                            minDate: dtToday,
                            dateFormat: 'dd/mm/yy'
                        });
                });

                /* Prepare agent and login info for expense web api request call and assign to global variable */
                GetAgentInfo();

                /* Check the session info and revert if expired */
                if (IsEmpty(apiAgentInfo)) {

                    alert('Session expired, please login once again.');
                    return;
                }

                /* Prepare expense api host url and assign to global variable */
                GetExpenseApiHost();

                /* Load screen data */
                GetScreenData('SL');

                $("#ctl00_upProgress").hide();
            });

            /* To get expense reimburse data */
            function GetScreenData(action) {

                var fromDate = new Date($("#txtFromDate").datepicker("getDate"));
                var toDate = new Date($("#txtToDate").datepicker("getDate"));

                if (fromDate > toDate || toDate == undefined || toDate == '') {

                    alert('From Date must be less than to Date');
                    return false;
                } 

                scAction = action;

                screenFilters = GetExpApproverQueueObj(ConvertDate($('#txtFromDate').val()) + " 00:00:01", ConvertDate($('#txtToDate').val()) + " 23:59:59",
                        apiAgentInfo.AgentId, $('#ddlProfile').val(), $('#ddlCostCenter').val(), $('#ddlReceiptStatus').val(), $('#ddlPaymentType').val(),
                        (!IsEmpty($('#txtTitle').val()) ? $('#txtTitle').val().trim() : null), (!IsEmpty($('#txtRefNo').val()) ? $('#txtRefNo').val().trim() : null),
                        $('#ddlApproverStatus').val());

                var reqData = { AgentInfo: apiAgentInfo, ApproverQueue: screenFilters, type: 'RR' };
                var apiUrl = apiHost.trimRight('/') + '/' + 'api/expTransactions/getExpenseApproverQueue';
                WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);
            }

            /* To bind screen drop downs and grid */
            function BindScreenData(screenData) {

                if (scAction == 'SL')
                    BindDropDown(screenData.dtProfiles, screenData.dtPaymentType, screenData.dtcostCenters);

                BindReimburseData(screenData.dtApprovalReceipts);
            }

            /* To bind expense receipt status, payment type, cost center and corp profiles */
            function BindDropDown(dtProfiles, dtPaymentType, dtcostCenters) {
                
                var options = GetddlOption('0', 'All');

                options += GetddlOption('RU', 'Receipt Uploaded');
                options += GetddlOption('NR', 'No Receipt');

                $('#ddlReceiptStatus').empty();
                $('#ddlReceiptStatus').append(options);
                $("#ddlReceiptStatus").select2("val", '0');

                DropDownBind('ddlProfile', dtProfiles, 'profileId', 'employeeId-surName name', '-', 'profileId');
                DropDownBind('ddlPaymentType', dtPaymentType, 'pT_ID', 'pT_Desc', '', 'pT_ID');
                DropDownBind('ddlCostCenter', dtcostCenters, 'cC_Id', 'cC_Name', '', 'cC_Id');
            }

            /* To Bind drop downs data */
            function DropDownBind(ddlId, dataEntity, valColNames, textColNames, separator, defSelVal) {

                DdlBind(ddlId, (IsEmpty(dataEntity) ? [] : dataEntity), valColNames, textColNames, separator,
                    '0', 'All', ((IsEmpty(dataEntity) || dataEntity.length != 1) ? '0' : dataEntity[0][defSelVal]));

                if (!IsEmpty(dataEntity) && dataEntity.length == 1) 
                    $('#' + ddlId).attr('disabled', 'disabled');
            }

            /* To Bind the reimburse data to grid */
            function BindReimburseData(reports) {
                                
                ClearDetails();

                if (reports.length == 0) {

                    if (scAction != 'SL')
                        ShowError('No records found.');
                    return;
                }

                reimburseData = reports;
                var headerColumns = 'DATE OF REIMBURSEMENT|REIMBURSED BY|DATE|EMPLOYEE ID|EMPLOYEE NAME|EXPENSE TITLE|EXPENSE REF|EXPENSE CATEGORY|EXPENSE TYPE|CLAIM EXPENSE|CURRENCY|CLAIM AMOUNT|PAYMENT TYPE|';
                headerColumns += 'BUSINESS PURPOSE|COST CENTER|GL ACCOUNT|CITY|TOTAL REIM. AMOUNT|BALANCE|STATUS';
                var dataColumns = 'reimdate|reimby|date|emP_ID|emP_NAME|eR_Title|eR_RefNo|exP_CATEGORY|eT_Desc|eD_ClaimExpense|eD_Currency|eD_TotalAmount|pT_Desc|eR_Purpose|costCentre|eT_GLAccount|eD_City';
                dataColumns += '|totalReimbursementAmount|balance|approvalStatus';

                var gridProperties = GetGridPropsEntity();
                gridProperties.headerColumns = headerColumns.split('|');
                gridProperties.displayColumns = dataColumns.split('|');
                gridProperties.pKColumnNames = ('eD_Id').split('|');
                gridProperties.dataEntity = reimburseData;
                gridProperties.divGridId = 'divReimGridInfo';
                gridProperties.headerPaging = true;
                gridProperties.selectType = 'N';
                gridProperties.recordsperpage = 10;

                gridProperties.columnproperties = {};

                /* Assigning the grid column custom controls, events, attributes */
                var gridColumnproperties = {};

                for (var i = 0; i < gridProperties.displayColumns.length; i++) {

                    var columnproperties = GetGridColumnProperties();
                    columnproperties.columnName = gridProperties.displayColumns[i];

                    if (gridProperties.displayColumns[i] == 'eR_RefNo') {

                        columnproperties.columnControl = 'link';
                        columnproperties.columnAttributes = { 'style': 'font-weight:bold', 'href': 'javascript:void(0)' };
                        columnproperties.columnEvents = { 'onclick': 'ShowReferenceDetails' };
                        columnproperties.columnEventParams = { 'onclick': 'eD_ER_Id' }
                    }

                    if (gridProperties.displayColumns[i] == 'eR_Title') {

                        columnproperties.columnControl = 'link';
                        columnproperties.columnAttributes = { 'style': 'font-weight:bold', 'href': 'javascript:void(0)' };
                        columnproperties.columnEvents = { 'onclick': 'OpenExpReport' };
                        columnproperties.columnEventParams = { 'onclick': 'eD_ER_Id|eD_Id' };
                    }

                    if (gridProperties.displayColumns[i] == 'eT_Desc') {

                        columnproperties.columnControl = 'link';
                        columnproperties.columnAttributes = { 'style': 'font-weight:bold', 'href': 'javascript:void(0)' };
                        columnproperties.columnEvents = { 'onclick': 'OpenExpCreate' };
                        columnproperties.columnEventParams = { 'onclick': 'eD_ER_Id|eD_Id' };
                    }
                     
                    gridColumnproperties[gridProperties.displayColumns[i]] = columnproperties;
                }

                gridProperties.columnproperties = gridColumnproperties;
                EnablePagingGrid(gridProperties);
                $('#ExpReimList').show();
            }

            /* To clear reimburse details and hide the div */
            function ClearDetails() {

                RemoveGrid();
                $('#ExpReimList').hide();
            }

            /* To get agent and login info */
            function GetAgentInfo() {

                try {

                    var loginInfo = '<%=Settings.LoginInfo == null%>';

                    if (loginInfo == true)
                        return apiAgentInfo;

                    var agentId = '<%=Settings.LoginInfo.AgentId%>';
                    var loginUser = '<%=Settings.LoginInfo.UserID%>';
                    var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                    var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                    var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                    apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
                }
                catch (excp) {
                    var exception = excp;
                }
                return apiAgentInfo;
            }

            /* To get expense api host url */
            function GetExpenseApiHost() {

                apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';

                if (IsEmpty(apiHost))
                    apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";

                return apiHost;
            }

            /* Show the Date controls*/
            function showDatepicker(id) {
                $('#' + id).focus();
            }

            /* Getting the Date from Date controls*/
            function ConvertDate(selector) {
                // To matach with Main search Date Format
                var from = selector.split("/");
                var Date = from[1] + "/" + from[0] + "/" + from[2];
                return Date;
            }
          
            /* Getting the Report reference Datails */
            function ShowReferenceDetails(expDtlId) {

                if (parseInt(expDtlId) > 0) {

                    AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpReportPrint', 'sessionData':'" + JSON.stringify({ expDetailId: expDtlId }) + "', 'action':'set'}");
                    window.open("ExpReportPrint.aspx", "Expense Report", "width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50");
                }
            }
          
            /* Open expense report screen */
            function OpenExpReport(expRepId, expDetailId) {

                if (parseInt(expRepId) > 0 && parseInt(expDetailId) > 0) {

                    AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpReport', 'sessionData':'" + JSON.stringify({ ExpRepId: expRepId, ExpDtlId: expDetailId }) + "', 'action':'set'}");
                    window.location.href = "ExpReport.aspx";
                }
            }
          
            /* Open expense create screen */
            function OpenExpCreate(expRepId, expDetailId) {

                if (parseInt(expRepId) > 0 && parseInt(expDetailId) > 0) {

                    AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpCreate', 'sessionData':'" + JSON.stringify({ ExpRepId: expRepId, ExpDtlId: expDetailId }) + "', 'action':'set'}");
                    window.location.href = "ExpCreate.aspx";
                }
            }

        } catch (e) {

            var msg = JSON.stringify(e.stack);
            exceptionSaving(msg);
        }

        /* To generate excel from the grid */
        function GenerateExcel() {
                                        
            var tab_text = "<table  border='1'>";
            tab_text += '<tr style="font-weight:bold;text-align:center">'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">DATE OF REIMBURSEMENT</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">REIMBURSED BY</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">REPORT DATE</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">EMPLOYEE ID</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">EMPLOYEE NAME</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">EXPENSE TITLE</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">EXPENSE REF</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">EXPENSE CATEGORY</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">EXPENSE TYPE</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">CLAIM EXPENSE</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">CLAIM AMOUNT</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">PAYMENT TYPE</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">BUSINESS PURPOSE</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">COST CENTER</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">GL ACCOUNT</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">CITY</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">CURRENCY</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">PAY AMOUNT</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">TOTAL REIM. AMOUNT</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">BALANCE</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">STATUS</span></td>'
                + '<td style="width:150px;background-color:Black;"><span style="color: White;">EXPENSE DATE</span></td>'
                + '</tr>';

            $.each(reimburseData, function (key, col) {

                tab_text += '<tr>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.reimdate + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.reimby + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.date + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.emP_ID + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.emP_NAME + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eR_Title + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eR_RefNo + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.exP_CATEGORY + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eT_Desc + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eD_ClaimExpense + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eD_TotalAmount + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.pT_Desc + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + (IsEmpty(col.eR_Purpose) ? '' : col.eR_Purpose) + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.costCentre + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + (IsEmpty(col.eT_GLAccount) ? '' : col.eT_GLAccount) + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + (IsEmpty(col.eD_City) ? '' : col.eD_City) + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eD_Currency + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eD_TotalAmount + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.totalReimbursementAmount + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.balance + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.approvalStatus + ' </span></td>'
                    + '<td style="width:150px;background-color:White;"><span style="color: Black;">' + col.eD_TransDate + ' </span></td>'
                    + '</tr>';

            });

            tab_text = tab_text + "</table>";
            var date = new Date();
            var link = document.createElement("A");
            link.href = 'data:text/application/vnd.ms-excel,' + encodeURIComponent(tab_text);
            link.download = "ExpReimbursementReport_" + date.getDate() + '-' + (date.getMonth() + 1) + '-' + date.getFullYear() + '-' + date.getTime() + ".xls";
            link.click();            
        }
    </script>

    <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">
                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left"> Expense Reimburse Report </h3>
                    </div>
                </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <a class="btn btn-gray mr-2 " onclick="window.location.href='expdetailsqueue.aspx'">CLEAR <i class="icon icon-close "></i></a>
                    <a class="btn btn-primary " id="showCardsList" onclick="GetScreenData('SR')">SEARCH <i class="icon icon-search "></i></a>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">
                        <div class="form-group col-6 col-md-2">
                            <label>From Date</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="From Date" ID="txtFromDate" />
                                <div class="input-group-append" onclick="showDatepicker('txtFromDate')">
                                    <span class="input-group-text" id="basic-addon1"><i class="icon icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-6 col-md-2">
                            <label>To Date</label>
                            <div class="input-group mb-3">
                                <input type="text" class="form-control" placeholder="To Date" ID="txtToDate" />
                                <div class="input-group-append" onclick="showDatepicker('txtToDate')">
                                    <span class="input-group-text" id="basic-addon2"><i class="icon icon-calendar"></i></span>
                                </div>
                            </div>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Employee</label>
                            <select class="form-control" ID="ddlProfile">
                                <option Value="0">All</option>
                            </select>

                        </div>
                        <div class="form-group col-md-2">
                            <label>Cost Center</label>
                            <select class="form-control" ID="ddlCostCenter">
                                <option Value="0">All</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Receipt Status</label>
                            <select class="form-control" ID="ddlReceiptStatus">
                                <option Value="0">All</option>
                            </select>

                        </div>
                        <div class="form-group col-md-2">
                            <label>Payment Type</label>
                            <select class="form-control" ID="ddlPaymentType">
                                <option Value="0">All</option>
                            </select>
                        </div>
                        <div class="form-group col-md-2">
                            <label>Report Ref #</label>
                            <input type="text" class="form-control" ID="txtRefNo" placeholder="Reference No" />
                        </div>
                        <div class="form-group col-md-2">
                            <label>Report Title</label>
                            <input type="text" class="form-control" placeholder="Title" ID="txtTitle" />
                        </div>
                        <div class="form-group col-md-2">
                            <label>Expense Status</label>
                            <select class="form-control" ID="ddlApproverStatus">
                                <option Value="All">All</option>
                                <%--<option Value="O">Open</option>
                                <option Value="S">Sent For approval</option>
                                <option Value="PA">Partial approved</option>
                                <option Value="A">Approved</option>
                                <option Value="RV">Revise</option>
                                <option Value="RJ">Rejected</option>--%>
                                <option Value="I">Payment Pending</option>
                                <option Value="R">Reiumbursed</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="exp-content-block w-100" id="ExpReimList">
                <h5 class="mb-3 float-left">Reimburse Details</h5>
                <div class="button-controls text-right">
                    <a class="btn btn-primary " id="excelReport" onclick="GenerateExcel()">Export To Excel</a>
                </div>
                <div class="clear"></div>
                <div class="table-responsive">                    
                    <div id="divReimGridInfo"></div>
                </div>
            </div>
        </div>
        <div class="clear "></div>
    </div>
</asp:Content>
