﻿<%@ WebHandler Language="C#" Class="UploadHandler" %>

using System;
using System.Web;
using System.Net;
using System.Configuration;
using System.IO;
using CT.GlobalVisa;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
public class UploadHandler : IHttpHandler, System.Web.SessionState.IReadOnlySessionState
{
    private int completed;
    CorporateProfile cropProfileDetails;
    public void ProcessRequest(HttpContext context)
    {
        try
        {
            var fileName = context.Request["qqfilename"];
            context.Response.ContentType = "text/plain";
            if (context.Session["GVSession"] != null)
            {
                GlobalVisaSession visaSession = context.Session["GVSession"] as GlobalVisaSession;
                if (visaSession != null && visaSession.CorpProfileDetails != null)
                {
                    cropProfileDetails = visaSession.CorpProfileDetails as CorporateProfile;
                }
            }
            if (cropProfileDetails != null)
            {
                var tempPath = ConfigurationManager.AppSettings["ProfileImage"];
                tempPath = tempPath + "/" + cropProfileDetails.ProfileId;
                var dirFullPath = context.Server.MapPath(tempPath);
                if (!Directory.Exists(dirFullPath))
                {
                    Directory.CreateDirectory(dirFullPath);
                }
                foreach (string s in context.Request.Files)
                {
                    var file = context.Request.Files[s];
                    var fileExtension = file.ContentType;
                    if (string.IsNullOrEmpty(fileName)) continue;
                    CorpProfileDocuments profileDoc = new CorpProfileDocuments();
                    profileDoc.ProfileId = cropProfileDetails.ProfileId;

                    profileDoc.DocTypeName = fileName.Split('.')[0];
                    profileDoc.DocFileName = fileName;
                    profileDoc.DocFilePath = dirFullPath;
                    profileDoc.CreatedBy = (int)Settings.LoginInfo.UserID;
                    profileDoc.Save();
                    if (profileDoc.RetDocId > 0)
                    {
                        var pathToSave_100 = HttpContext.Current.Server.MapPath(tempPath) + "\\" + fileName;
                        file.SaveAs(pathToSave_100);
                    }
                    else
                    {
                        throw new Exception("File Not Uploaded");
                    }
                }
                context.Response.StatusCode = (int)HttpStatusCode.OK;
                context.Response.Write("{\"success\":true}");
            }
            else
            {
                context.Response.Redirect("AbandonSession.aspx", false);
            }
        }
        catch (Exception ex)
        {
            context.Response.Write("Error: " + ex.Message);
        }
    }
    public bool IsReusable
    {
        get
        {
            return false;
        }
    }
}