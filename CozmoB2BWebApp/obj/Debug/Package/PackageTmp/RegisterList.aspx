<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="RegisterListGUI" Title="Agent Register List" Codebehind="RegisterList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
   <%-- <div class="normal-heading" style='margin: 1%'>
        <h1>
            Registered Agents List</h1>
    </div>--%>
    <div style="margin: 0.5%; border: 1px solid black">
       <%-- <asp:HiddenField ID="hdfRLDetailsMode" runat="server"></asp:HiddenField>--%>
        <asp:GridView ID="gvRegisterList" runat="server" AutoGenerateColumns="False" Width="100%"
            EmptyDataText="No Agency List!" AllowPaging="true" PageSize="10" DataKeyNames="reg_id"
            CellPadding="0" ShowFooter="true" CellSpacing="0" GridLines="none" OnPageIndexChanging="gvRegisterList_PageIndexChanging"
            OnSelectedIndexChanged="gvRegisterList_SelectedIndexChanged" Caption="Registerd Agency List" >
            <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
            <Columns>
                <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                    ControlStyle-CssClass="label" ShowSelectButton="True" />
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:ImageButton runat="server" OnClick="DeleteRegisteredUser" OnClientClick="return confirm('Do you want to delete?')"
                            ImageUrl="Images/grid/wg_Delete.gif" CommandArgument='<%# Eval("reg_id")%>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter OnClick="FilterSearch_Click" ID="RLtxtAgentName" Width="70px" HeaderText="Name"
                            CssClass="inputEnabled" runat="server" /> 
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentName" runat="server" Text='<%# Eval("reg_name") %>' CssClass="labelDtl grdof"
                            ToolTip='<%# Eval("reg_name") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter OnClick="FilterSearch_Click" ID="RLtxtAgentAddress" Width="70px" HeaderText="Address"
                            CssClass="inputEnabled" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentAddress" runat="server" Text='<%# Eval("reg_address") %>'
                            CssClass="labelDtl grdof" ToolTip='<%# Eval("reg_address") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter OnClick="FilterSearch_Click" ID="RLtxtAgentPhone1" Width="70px" HeaderText="Phone"
                            CssClass="inputEnabled" runat="server" /> 
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentPhone1" runat="server" Text='<%# Eval("reg_telephone") %>'
                            CssClass="labelDtl grdof" ToolTip='<%# Eval("reg_telephone") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                    <cc1:Filter OnClick="FilterSearch_Click" ID="RLtxtAgentEmail1" Width="70px" HeaderText="Email"
                            CssClass="inputEnabled" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentEmail1" runat="server" Text='<%# Eval("reg_email") %>'
                            CssClass="labelDtl grdof" ToolTip='<%# Eval("reg_email") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
