<%@ Page AutoEventWireup="true" Inherits="ManualTicket"
    Language="C#" MasterPageFile="~/TransactionBE.master" Codebehind="ManualTicket.aspx.cs" %>

<%@ Import Namespace="CT.BookingEngine" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">

    <script src="Scripts/jsBE/ManualTicket.js" type="text/javascript"></script>

    <script type="text/javascript">
        var rowNumber = <% = itinerary.Segments.Length %>;
        
        function SupplierChange()
        {
             selectedIndex=document.getElementById('SupplierList').selectedIndex;
             var supplierid=document.getElementById('SupplierList').options[selectedIndex].value;
             document.getElementById('supplierId').value=supplierid
        }
    </script>

    
        <div >
            <div >
                <div >
                    <div >
                        <% = agency.Name %>
                    </div>
                    <div >
                        <span class="fleft">
                            <img alt="TB Online" src="Images/alert.gif" /></span>
                        <div class="fleft font-16 bold margin-left-5" style="width: 190px;">
                            <span>PNR: </span><span class="margin-left-10">
                                <% = itinerary.PNR %>
                            </span>
                        </div>
                        <%  if (booking.LockedBy == loggedInMember.ID)
                            { %>
                        <span class="fleft width-100">This booking has been locked for you</span>
                        <%  } %>
                    </div>
                </div>
                <div class="fleft width-100 margin-top-10">
                    <div class="fleft">
                        Booking Date: <span class="bold">
                            <%  TimeSpan tspan = DateTime.Now.ToUniversalTime() - booking.CreatedOn;
                            if (tspan.Days == 0){ %>
                            Today
                            <% } %>
                            <% = Util.UTCToIST(booking.CreatedOn).ToString("dd/MM/yyyy") %>
                        </span>
                    </div>
                    <div class="margin-left-15 fleft">
                        Travel Date: <span class="bold">
                            <% = itinerary.Segments[0].DepartureTime.ToString("dd/MM/yyyy") %>
                        </span>
                    </div>
                </div>
            </div>
            <div id="ErrorBlock" class="fleft" style="color:Red">
            </div>
            <% if (errorEticket == true) { %>
            <%--<div class="fleft locked-pnr-parent-width margin-top-10 margin-left-10 font-red"> --%>
            <div class="fleft font-red padding-5 yellow-back width-100 center margin-top-5 center margin-top-5"
                style="width: 740px;">
                <%= errorMessage %>
            </div>
            <% } %>
            <div class="fleft margin-top-10 form-parent-width">
                <div class="fleft padding-3 form-child-left-right-width">
                    <span class="fleft text-right form-content-heading-width">Pax Name: </span><span
                        class="fleft margin-left-5 width-260">
                        <input id="title" class="text width-40" maxlength="10" name="title" 
                            type="text" value="<% = itinerary.Passenger[paxIndex].Title %>" />
                        <input id="FirstName" class="text width-80" name="firstName" 
                            type="text" value="<% = itinerary.Passenger[paxIndex].FirstName %>" />
                        <input id="LastName" class="text width-80 margin-left-5" name="lastName"
                             type="text" value="<% = itinerary.Passenger[paxIndex].LastName %>" />
                        /
                        <% = itinerary.Passenger[paxIndex].Type.ToString() %>
                    </span><span class="fleft text-right margin-top-5 form-content-heading-width">
                    Endorsement / Restrictions: </span><span class="fleft margin-left-5 margin-top-5 width-260">
                            <textarea id="Endorsement" class="text width-250" cols="30" name="endorsement" rows="2"><% = editEndorsement %></textarea>
                        </span>
                </div>
                <div class="fleft padding-3 margin-left-3 form-child-left-right-width">
                    <div class="fleft width-100">
                        <span class="fleft text-right width-70">PNR: </span><span class="fleft margin-left-5 form-right-box-input-width">
                            <input id="PNR" class="text form-right-box-input-width readonly" name="pnr" readonly="readonly"
                                type="text" value="<% = itinerary.PNR %>" />
                        </span><span class="fleft text-right margin-left-8 width-70">Origin / 
                        Destination</span>
                        <span class="fleft margin-left-5 form-right-box-input-width">
                            <% string ond = itinerary.Origin + (itinerary.Segments[itinerary.Segments.Length - 1].Destination.AirportCode); %>
                            <input id="OND" class="text form-right-box-input-width readonly" name="ond" readonly="readonly"
                                type="text" value="<% = ond %>" />
                        </span>
                    </div>
                    <div class="fleft width-100 margin-top-5">
                        <span class="fleft text-right width-70">Issue Date:<span class="font-red">*</span> </span><span class="fleft margin-left-5 form-right-box-input-width">
                            <input id="IssueDate" class="text form-right-box-input-width" name="issueDate" type="text"
                                value="<%=DateTime.UtcNow.ToString("dd MMM yyyy HH:mm:ss")%>" />
                        </span><span class="fleft text-right margin-left-8 width-70">Issued in exchange</span>
                        <span class="fleft margin-left-5 form-right-box-input-width">
                            <input id="IssueInExchange" class="text form-right-box-input-width" name="issueInExchange"
                                type="text" value="<% = editIssueInExchange %>" />
                        </span>
                    </div>
                </div>
                <div>
                    <div class="fleft width-400">
                        <div class="fleft width-125">
                            Ticket Number :<span class="font-red">*</span></div>
                        <input id="TicketNumber" class="text width-100px" name="ticketNumber" type="text"
                            value="<% = editTicketNumber %>" />
                        -
                        <input id="ConjunctionNo" class="text width-35" name="conjunctionNo" type="text"
                            value="<% = editConjunctionNumber %>" />
                    </div>
                    <div>
                        <div class="fleft width-125">
                            Ticket Designator :</div>
                        <input class="fleft" id="TicketDesignator" class="text width-100px" name="ticketDesignator" type="text"
                            value="<% = editTicketDesignator %>" /></div>
                </div>
            </div>
            <div class="fleft width-100 margin-top-15 font-11">
                <div class="fleft width-30">
                    X/O</div>
                <div class="fleft margin-left-3 form-middle-passage-input-width">
                    Good for Pax from</div>
                <div class="fleft margin-left-5 width-40">
                    Carrier</div>
                <div class="fleft margin-left-3 center width-70">
                    Flight</div>
                <div class="fleft margin-left-3 text-right width-30">
                    Class</div>
                <div class="fleft margin-left-5 center width-130">
                    Travel Date</div>
                
                <div class="fleft margin-left-3 center width-60">
                    Status</div>
                <div class="fleft margin-left-3 text-right width-60">
                    Fare Basis</div>
                <div class="fleft margin-left-3 center width-60">
                    NVB</div>
                <div class="fleft margin-left-3 center width-60">
                    NVA</div>
                <div class="fleft margin-left-3 center width-60">
                    Baggage</div>
            </div>
            <div id="DestinationInfo">
                <%  for (int i = 0; i < itinerary.Segments.Length; i++)
                { %>
                <div class="fleft width-100 margin-top-5 font-11">
                    <% string stopOver = "X"; if (itinerary.Segments[i].StopOver) { stopOver = "O"; } %>
                    <div class="fleft width-30">
                        <input id="XO<% = i %>" class="text width-25" name="xo<% = i %>" type="text" value="<% = stopOver %>" />
                    </div>
                    <div class="fleft margin-left-3 width-45">
                        <input id="Org<% = i %>" class="text width-40" name="Org<% = i %>"
                            type="text" value="<% = itinerary.Segments[i].Origin.AirportCode %>" />
                    </div>
                    <div class="fleft margin-left-5 width-45">
                        <input id="Des<% = i %>" class="text width-40" name="des<% = i %>" 
                            type="text" value="<% = itinerary.Segments[i].Destination.AirportCode %>" />
                    </div>
                    <div class="fleft margin-left-5 width-40">
                        <input id="Airline<% = i %>" class="text width-35" name="airline<% = i %>"
                             type="text" value="<% = itinerary.Segments[i].Airline %>" />
                    </div>
                    <div class="fleft margin-left-3 width-70">
                        <input id="FlightNumber<% = i %>" class="text width-65 " name="flightNumber<% = i %>"
                             type="text" value="<% = itinerary.Segments[i].FlightNumber %>" />
                    </div>
                    <div class="fleft margin-left-3 width-30">
                        <input id="Class<% = i %>" class="text width-25 " name="class<% = i %>" 
                            type="text" value="<% = itinerary.Segments[i].BookingClass %>" />
                    </div>
                    <div class="fleft margin-left-5 width-130">
                        <input id="Date<% = i %>" class="text width-120" name="date<% = i %>" 
                            type="text" value="<% = itinerary.Segments[i].DepartureTime.ToString("dd MMM yyyy hh:mm:ss tt") %>" />
                    </div>
                   <%-- <div class="fleft margin-left-3 width-60">
                        <input id="Time<% = i %>" class="text width-55" name="time<% = i %>" 
                            type="text" value="<% = itinerary.Segments[i].DepartureTime.ToString("hh:mm tt") %>" />
                    </div>--%>
                    <div class="fleft margin-left-3 width-60">
                        <input id="Status<% = i %>" class="text width-50" name="status<% = i %>" type="text"
                            value="<% = itinerary.Segments[i].Status %>" />
                    </div>
                    <div class="fleft margin-left-3 width-60">
                        <%  string tempFareBasis = string.Empty;
                        string tempNVA = string.Empty;
                        string tempNVB = string.Empty;
                        string tempBaggage = string.Empty;

                        if (ETicketMode && (errorEticket == false))
                        {
                            //if ((itinerary.Segments[i].Origin.AirportCode == eTicket[paxIndex].))
                            tempFareBasis = eTicket[paxIndex].PtcDetail[i].FareBasis;
                            tempNVA = eTicket[paxIndex].PtcDetail[i].NVA;
                            tempNVB = eTicket[paxIndex].PtcDetail[i].NVB;
                            tempBaggage = eTicket[paxIndex].PtcDetail[i].Baggage;
                        }
                        else if(ticket.PtcDetail != null)
                        {
                            for (int j = 0; j < ticket.PtcDetail.Count; j++)
                            {
                                if (itinerary.Passenger[paxIndex].Type == FlightPassenger.GetPassengerType(ticket.PtcDetail[j].PaxType) && itinerary.Segments[i].SegmentId == ticket.PtcDetail[j].SegmentId)
                                {
                                    tempFareBasis = ticket.PtcDetail[j].FareBasis;
                                    tempNVA = ticket.PtcDetail[j].NVA;
                                    tempNVB = ticket.PtcDetail[j].NVB;
                                    tempBaggage = ticket.PtcDetail[j].Baggage;
                                    break;
                                }
                            }
                        }
                        %>
                        <input id="FareBasis<% = i %>" class="text width-55" name="fareBasis<% = i %>" type="text"
                            value="<% = tempFareBasis %>" />
                    </div>
                    <div class="fleft margin-left-3 width-60">
                        <input id="NVB<% = i %>" class="text width-55" name="nvb<% = i %>" type="text" value="<% = tempNVB %>" />
                    </div>
                    <div class="fleft margin-left-3 width-60">
                        <input id="NVA<% = i %>" class="text width-55" name="nva<% = i %>" type="text" value="<% = tempNVA %>" />
                    </div>
                    <div class="fleft margin-left-3 width-60">
                        <input id="Allow<% = i %>" class="select width-50" name="allow<% = i %>" type="text"
                            value="<% = tempBaggage %>" />
                    </div>
                </div>
                <%  } %>
            </div>
            <%--            <div class="fleft width-100 margin-top-5">
                <a href="javascript:AddRow()">Add Row</a>
            </div>--%>
            <div class="fleft form-parent-width margin-top-20">
                <div class="fleft padding-3 form-child-left-right-width">
                    <div>
                        <span class="fleft text-right form-content-heading-width">Fare </span><span class="fleft margin-left-5 width-260">
                            <span class="fleft">
                                <% //TODO: Change the Hardcoded currency %>
                                <input id="BaseCurrency" class="select-text width-52 dummy" name="baseCurrency" readonly="readonly"
                                    type="text" value="<%=CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"]  %>" />
                            </span><span class="fleft margin-left-5">
                                <input id="BaseFare" class="text width-195 readonly" name="baseFare" readonly="readonly"
                                    type="text" value="<% = (itinerary.Passenger[paxIndex].Price.PublishedFare - itinerary.Passenger[paxIndex].Price.AgentCommission - itinerary.Passenger[paxIndex].Price.AgentPLB).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) %>" />
                            </span></span>
                    </div>
                    <div id="TaxBlock">
                        <%  if (editMode)
                        {
                            int taxRowNumber = 0;
                            for (int i = 0; i < ticket.TaxBreakup.Count; i++, taxRowNumber++)
                            {   %>
                        <div id="Tax<% = i %>">
                            <span class="fleft text-right margin-top-5 form-content-heading-width">
                             <% = i<2?"<span class=\"font-red\">*</span>":  string.Empty%>
                            Tax<% = (i + 1)%>
                            </span><span class="fleft margin-left-5 margin-top-5 width-260"><span>
                                <input id="TaxValue<% = i %>" class="text text-right width-80" name="taxValue<% = i %>"
                                    type="text" value="<% = ticket.TaxBreakup[i].Value.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) %>" /></span>
                                <span>Type: </span><span>
                                    <input id="TaxType<% = i %>" class="text width-35" name="taxType<% = i %>" type="text"
                                        value="<% = ticket.TaxBreakup[i].Key %>" /></span> </span>
                        </div>
                        <%      }   %>

                        <script type="text/javascript">var taxRowNumber = <% = taxRowNumber %>;</script>

                        <%  }
                        else if (ETicketMode && (errorEticket == false))
                        {
                            int taxRowNumber = 0;
                            for (int i = 0; i < eTicket[paxIndex].TaxBreakup.Count; i++, taxRowNumber++)
                            {   %>
                        <div id="Div1">
                            <span class="fleft text-right margin-top-5 form-content-heading-width">
                            <% = i<2?"<span class=\"font-red\">*</span>":  string.Empty%>
                            Tax<% = (i + 1)%>
                            </span><span class="fleft margin-left-5 margin-top-5 width-260"><span>
                                <input id="Text1" class="text text-right width-80" name="taxValue<% = i %>" type="text"
                                    value="<% = eTicket[paxIndex].TaxBreakup[i].Value.ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) %>" /></span>
                                <span>Type: </span><span>
                                    <input id="Text2" class="text width-35" name="taxType<% = i %>" type="text" value="<% = eTicket[paxIndex].TaxBreakup[i].Key %>" /></span>
                            </span>
                        </div>
                        <%      }   %>

                        <script type="text/javascript">var taxRowNumber = <% = taxRowNumber %>;</script>

                        <%  }
                        else
                        {   %>
                        <div id="Tax0">
                            <span class="fleft text-right margin-top-5 form-content-heading-width"><span class="font-red">
                            *</span>Tax1 </span>
                            <span class="fleft margin-left-5 margin-top-5 width-260"><span>
                                <input id="TaxValue0" class="text width-80" name="taxValue0" type="text" value='<%=itinerary.Passenger[paxIndex].Price.YQTax%>' readonly="readonly" /></span>
                                <span>Type: </span><span>
                                    <input id="TaxType0" class="text width-35" name="taxType0" type="text" value="YQ" readonly="readonly"/></span>
                            </span>
                        </div>
                        <div id="Tax1">
                            <span class="fleft text-right margin-top-5 form-content-heading-width"><span class="font-red">
                            *</span>Tax2 </span>
                            <span class="fleft margin-left-5 margin-top-5 width-260"><span>
                                <input id="TaxValue1" class="text width-80" name="taxValue1" type="text" /></span>
                                <span>Type: </span><span>
                                    <input id="TaxType1" class="text width-35" name="taxType1" type="text" /></span>
                            </span>
                        </div>

                        <script type="text/javascript">var taxRowNumber = 2;</script>

                        <%  } %>
                    </div>
                    <span><a href="Javascript:AddTaxRow()">Add More</a></span>
                    <div>
                        <span class="fleft text-right form-content-heading-width margin-top-5">Total </span>
                        <span class="fleft margin-left-5 width-260 margin-top-5"><span class="fleft">
                            <input id="TotalCurrency" class="select-text width-52 dummy" name="TotalCurrency"
                                readonly="readonly" type="text" value="<%=CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"]  %>" />
                        </span><span class="fleft margin-left-5">
                            <input id="TotalFare" class="text width-195 readonly" name="totalFare" readonly="readonly"
                                type="text" value="<% = (itinerary.Passenger[paxIndex].Price.PublishedFare - itinerary.Passenger[paxIndex].Price.AgentCommission - itinerary.Passenger[paxIndex].Price.AgentPLB + itinerary.Passenger[paxIndex].Price.Tax).ToString( CT.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"] ) %>" />
                        </span></span>
                    </div>
                    
                    <div>
                        <span class="fleft text-right form-content-heading-width margin-top-10">Supplier </span>
                        <span class="fleft margin-left-5 width-260 margin-top-10"><span class="fleft">
                            <select id="SupplierList" name="SupplierList" style="width:150px" onchange="SupplierChange()">
                             <option value="0" > -- select Supplier--</option>
                             <% 
                                               
                                                 foreach (System.Collections.Generic.KeyValuePair<int, string> kvpPair in supplierList)
                                                {
                                                %>
                                                     
                                                        <option value="<%=kvpPair.Key %>" >
                                                            <%
                                                                Response.Write( kvpPair.Value);
                                                           %>
                                                        </option>
                                               <%} %>
                          </select>
                        </span></span>
                    </div>
                    
                </div>
                <script type="text/javascript">
                var element= document.getElementById('SupplierList');
                var source='<%=ticket.SupplierId%>';
                for(var i=0;i<element.options.length;i++)
                {
                if(element.options[i].value==source)
                {
                    element.options[i].selected=true;
                    break;
                }
            }
                </script>
                
                <div class="fleft padding-3 margin-left-3 form-child-left-right-width">
                    <div class="fleft width-100">
                        <span class="fleft text-right width-70">Fare Calculation </span><span class="fleft margin-left-5 form-right-box-input-width">
                            <textarea id="FareCalculation" class="text width-284" cols="30" name="fareCalculation"
                                rows="2"><% = editFareCalculation %></textarea>
                        </span>
                    </div>
                    <div class="fleft width-100 margin-top-5">
                        <span class="fleft text-right width-70">Form of Payment</span> <span class="fleft margin-left-5 form-right-box-input-width margin-top-5">
                            <input id="FOP" class="text width-284" name="fop" type="text" value="<% = editFOP %>" />
                        </span>
                    </div>
                    <div class="fleft width-100 margin-top-5">
                        <span class="fleft text-right width-70">Tour Code </span><span class="fleft margin-left-5 form-right-box-input-width">
                            <input id="TourCode" class="text width-100px" name="tourCode" type="text" value="<% = editTourCode %>" />
                        </span><span class="fleft text-right margin-left-8 width-70">Orignal issue</span>
                        <span class="fleft margin-left-5 form-right-box-input-width">
                            <input id="OriginalIssue" class="text width-100px" name="originalIssue" type="text"
                                value="<% = editOriginalIssue %>" />
                        </span>
                    </div>
                    <div class="fleft width-100 margin-top-5">
                        <span class="fleft text-right width-70">Validating Airline</span><span class="font-red">*</span> <span class="fleft margin-left-5 form-right-box-input-width">
                            <input id="ValidatingAirlineCode" class="text width-100px" name="validatingAirlineCode"
                                type="text" value="<% = editValidatingAirline %>" />
                        </span>
                    </div>
                </div>
            </div>
            <input name="bookingId" type="hidden" value="<% = bookingId %>" />
            <input name="paxId" type="hidden" value="<% = paxId %>" />
            <input name="postBack" type="hidden" value="true" />
            <input id="TaxRowCount" name="taxRowCount" type="hidden" />
            <input name="editMode" type="hidden" value="<% = editMode.ToString() %>" />
            <input name="ETicketMode" type="hidden" value="<% = eTicketMode %>" />
            <input id="supplierId" name="supplierId" type="hidden" />
          
            <%  if (editMode)
            {   %>
            <input name="ticketId" type="hidden" value="<% = ticket.TicketId %>" />
            <%  }
            else
            {   %>
            <input name="ticketId" type="hidden" value="0" />
            <%  }   %>

            <script type="text/javascript">document.getElementById('TaxRowCount').value = taxRowNumber;</script>

            <div class="fright">
                <input type="submit" value=" Save " onclick="return checkTicketForm()" /></div>
        </div>
    
</asp:Content>

