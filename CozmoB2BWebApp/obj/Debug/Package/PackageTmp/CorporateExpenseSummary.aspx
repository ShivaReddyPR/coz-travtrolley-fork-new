﻿<%@ Page MasterPageFile="~/TransactionVisaTitle.master" Language="C#" AutoEventWireup="true" Inherits="CorporateExpenseSummaryUI"
    Title="Corporate Expense Summary" Codebehind="CorporateExpenseSummary.aspx.cs" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript">

//corpProfileUserId -  based upon this Id we will pass an ajax request when the DOM is ready and bind all the booking details from the table BKE_FLIGHT_ITINERARY where the TripId is not null and load all the details.
//By default we will load only one month bookings.
var corpProfileUserId =  <% =corpProfileUserId %>;


    function Validate() {

        var valid = false;
        document.getElementById('errMess').style.display = "none";

        if (document.getElementById('ctl00_cphTransaction_dcExpFromDate_Date').value.length == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please select from date .";
        }
        else if (document.getElementById('ctl00_cphTransaction_dcExpToDate_Date').value.length == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please select to date.";
        }

        else {
            valid = true;
            var fromdate = document.getElementById('ctl00_cphTransaction_dcExpFromDate_Date').value;
            var toDate = document.getElementById('ctl00_cphTransaction_dcExpToDate_Date').value;
            getExpenseSummaryBookingsWithinDates(fromdate, toDate);
        }

        return valid;
    }

    var Ajax;
    if (window.XMLHttpRequest) {
        Ajax = new window.XMLHttpRequest();
    }
    else {
        Ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }

        function getExpenseSummaryBookings() {

        var paramList = 'requestSource=getExpenseSummaryBookings&Type=T&profileid='+ document.getElementById('ctl00_cphTransaction_ddlEmployee').value;
        var url = "CorportatePoliciesExpenseAjax";
        Ajax.onreadystatechange = bindExpenseSummaryBookings;
        Ajax.open('POST', url, false);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send(paramList);
    }

        function getExpenseSummaryBookingsWithinDates(fromdate, toDate) {
        
        var paramList = 'requestSource=getExpenseSummaryBookings&Type=T&profileid=' + document.getElementById('ctl00_cphTransaction_ddlEmployee').value+'&fromDate='+ fromdate + '&toDate='+toDate;
        var url = "CorportatePoliciesExpenseAjax";
        Ajax.onreadystatechange = bindExpenseSummaryBookingsWithinDateRange;
        Ajax.open('POST', url, false);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send(paramList);
    }


    function getExpenseSummaryBookingsHtml(bookingRecords) {
        var paramList = 'requestSource=getExpenseSummaryHtml&Type=T&bookingRecords=' + bookingRecords;
        var url = "CorportatePoliciesExpenseAjax";
        Ajax.onreadystatechange = bindSummaryBookingsHtml;
        Ajax.open('POST', url, false);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send(paramList);
    }


    function bindSummaryBookingsHtml() {
        if (Ajax.readyState == 4) {
            if (Ajax.status == 200) {
                if (Ajax.responseText.length > 0) {
                    $("#bookingRecords:last").append(Ajax.responseText);
                }
            }
        }
    }
    function bindExpenseSummaryBookings() {
        if (Ajax.readyState == 4) {
            if (Ajax.status == 200) {
                if (Ajax.responseText.length > 0) {
                    getExpenseSummaryBookingsHtml(Ajax.responseText);
                     $("#tblBookingRecords").show();
                     document.getElementById('errMess').style.display = "none";
                }
                else{
                 document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "No matching records found !";
                $("#tblBookingRecords").hide();
                 
                }
            }
        }
    }


    
    function bindExpenseSummaryBookingsWithinDateRange() {
    
        if (Ajax.readyState == 4) {
            if (Ajax.status == 200) {
                if (Ajax.responseText.length > 0) {
 $("#tblBookingRecords").show();
                    $("#tblBookingRecords").find("tr:gt(0)").remove();
                    getExpenseSummaryBookingsHtml(Ajax.responseText);
                     document.getElementById('errMess').style.display = "none";
                     

                }
                else{
                 document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "No matching records found !";
                $("#tblBookingRecords").hide();
                 
                }
            }
        }
    }
    
    

    $(document).ready(function() {
    
        //alert(corpProfileUserId);
        getExpenseSummaryBookings();

    })
    
    
    
    
    </script>

    <div class="body_container">
        <br />
        <div class="b2b-corp-exp-summary">
            <div class="col-md-4">
                <h2>
                    Expense Summary</h2>
            </div>
            <div id="errMess" style="display: none; color: Red; font-weight: bold; text-align: center;">
            </div>
            <div class="col-md-2">
                <div class="well">
                    <span><strong>Total Spend:</strong>
                        <% = currency%>
                        <% = amountSpend%></span>
                    <!-- Need to change this -->
                </div>
            </div>
            <div class="clearfix">
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>
                        From Date</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <uc1:DateControl ID="dcExpFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                        </uc1:DateControl>
                    </div>
                </div>
            </div>
            <div class="col-md-2">
                <div class="form-group">
                    <label>
                        To Date</label>
                    <div class="input-group">
                        <div class="input-group-addon">
                            <span class="glyphicon glyphicon-calendar"></span>
                        </div>
                        <uc1:DateControl ID="dcExpToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                        </uc1:DateControl>
                    </div>
                </div>
            </div>
            
                 <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Select Employee</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlEmployee" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
            
            
            <div class="col-md-2">
                <label class="center-block">
                    &nbsp;</label>
                <!--  <asp:Button OnClientClick="return Validate();" CssClass="btn btn-primary btn-blue" runat="server" Text ="Search" ID="btnSearch" /> -->
                <input type="button" value="Search" onclick="return Validate()" class="btn btn-primary btn-blue"
                    id="btnSearch" />
            </div>
            <div class="col-md-12">
                <table class="table table-bordered b2b-corp-table" id="tblBookingRecords">
                    <tbody id="bookingRecords">
                        <tr>
                            <th>
                                Date of Booking
                            </th>
                            <th>
                                Reference No.
                            </th>
                            <th>
                                Employee ID.
                            </th>
                            <th>
                                Employee Name.
                            </th>
                            <th>
                                Travel Date
                            </th>
                            <th>
                                Airline
                            </th>
                            <th>
                                Ticket No.
                            </th>
                            <th>
                                Route (sector details)
                            </th>
                            <th>
                                Total Fare
                            </th>
                            <th>
                                Create Expense
                            </th>
                        </tr>
                    </tbody>
                </table>
                <asp:Button OnClick="btnCreateExpense_Click" CssClass="btn btn-primary btn-blue"
                    runat="server" ID="a" Text="Create Expense" />
                <br />
                <br />
            </div>
            <div class="clearfix">
            </div>
        </div>
    </div>
</asp:Content>

