﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="OfflineBookingListGUI" Title="Offline Booking List" Codebehind="OfflineBookingList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<script type="text/javascript" src="Scripts/Jquery/jquery.min.js"></script>
<script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js" ></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js" ></script>
     
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    
<script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
<script type="text/javascript" language="javascript">
function init() {

        //    showReturn();
        var today = new Date();
        // For making dual Calendar use CalendarGroup  for single Month use Calendar     
        cal1 = new YAHOO.widget.Calendar("cal1", "container1");
        //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
        cal1.cfg.setProperty("title", "Select From Date");
        cal1.cfg.setProperty("close", true);
        cal1.selectEvent.subscribe(setDate1);
        cal1.render();

        cal2 = new YAHOO.widget.Calendar("cal2", "container2");
        cal2.cfg.setProperty("title", "Select To Date");
        cal2.selectEvent.subscribe(setDate2);
        cal2.cfg.setProperty("close", true);
        cal2.render();
    }

    function showCalendar1() {
       document.getElementById('container2').style.display = "none";
       document.getElementById('container1').style.display = "block";

    }

    function showCalendar2() {
        document.getElementById('container1').style.display = "none";
        cal1.hide();
        // setting Calender2 min date acoording to calendar1 selected date
        var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
        //var date1=new Date(tempDate.getDate()+1);

        if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            var depDateArray = date1.split('/');
            cal2.render();
        }
        document.getElementById('container2').style.display = "block";
    }


    function setDate1() {
        var date1 = cal1.getSelectedDates()[0];

        document.getElementById('IShimFrame').style.display = "none";
        this.today = new Date();
        var thisMonth = this.today.getMonth();
        var thisDay = this.today.getDate();
        var thisYear = this.today.getFullYear();
        var thisTime = this.today.getHours();
        var thisMinutes = this.today.getMinutes();
        var todaydate = new Date(thisYear, thisMonth, thisDay);
        var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
        var difference = (depdate.getTime() - todaydate.getTime());
         
        departureDate = cal1.getSelectedDates()[0];
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";
        //			
        var month = date1.getMonth() + 1;
        var day = date1.getDate();
        var time = "";
        var timePM = "";

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }
        if (thisMinutes < 10) {
            thisMinutes = "0" + thisMinutes
        }
        if (thisTime > 11) {
            timePM = ("PM")
        } else {
            timePM = ("AM")
        }
        time = (thisTime + ":" + thisMinutes + timePM + " ");

        document.getElementById("<%=txtFrom.ClientID%>").value = day + "/" + (month) + "/" + date1.getFullYear();
         cal1.hide();

    }
    function setDate2() {
        var date1 = document.getElementById("<%=txtFrom.ClientID%>").value;
        if (date1.length == 0 || date1 == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "First select from date.";
            return false;
        }

        var date2 = cal2.getSelectedDates()[0];

        var depDateArray = date1.split('/');

        // checking if date1 is valid		    
        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = " Invalid To Date";
            return false;
        }
        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        // Note: Date()	for javascript take months from 0 to 11
        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
        var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
        var difference = returndate.getTime() - depdate.getTime();
        //var thisTime = this.today.getHours();
        //var thisMinutes = this.today.getMinutes();
        if (difference < 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "To Date should be greater than  or equal to date of From (" + date1 + ")";
            return false;
        }

        document.getElementById('errMess').style.display = "none";
        document.getElementById('errorMessage').innerHTML = "";

        var month = date2.getMonth() + 1;
        var day = date2.getDate();

        if (month.toString().length == 1) {
            month = "0" + month;
        }

        if (day.toString().length == 1) {
            day = "0" + day;
        }

        document.getElementById("<%=txtTo.ClientID%>").value = day + "/" + month + "/" + date2.getFullYear();


        cal2.hide();
    }
    YAHOO.util.Event.addListener(window, "load", init);
    YAHOO.util.Event.addListener(window, "click", init);
     
    </script>
    
    
    
    <div> 
    
    
         
       
                                        
                                        
    <div class=" paramcon" title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true" >
        
        
        <div class="marbot_10"> 
        
        <div class="col-md-2"><asp:Label ID="lblFromDate" Text="From Date:" runat="server" ></asp:Label> </div>
        
          <div class="col-md-2"> <p class="fleft car-search">
                                <span class="fleft margin-top-3 margin-left-5">
                                <asp:TextBox ID="txtFrom" CssClass="form-control" runat="server" Width="80"></asp:TextBox>
                                 </span>
                                 <a href="javascript:void()" onclick="showCalendar1()"><img src="images/call-cozmo.png" class="margin-top-8 margin-left-5" alt="Pick Date" /></a>
</p>
                  <div id="container1" style="position: absolute;top: 33px;left: 20px; display: none;"> </div>

          </div>
           
           
            <div class="col-md-2"> <asp:Label ID="lblToDate"  Text="To Date:" runat="server"></asp:Label> </div>
              
              <div class="col-md-2"> <p class="fleft car-search">
            <span class="fleft margin-top-3 margin-left-5">
                  <asp:TextBox ID="txtTo" CssClass="form-control" runat="server" Width="80"></asp:TextBox>
            </span>
            <a href="javascript:void()" onclick="showCalendar2()"><img src="images/call-cozmo.png" class="margin-top-8 margin-left-5" alt="Pick Date" /></a>
</p>
                    <div id="container2" style="position: absolute;top: 33px;left: 20px;display: none;">
                                            </div>
              </div>


      <div class="col-md-2"> <asp:Label ID="lblAgent" runat="server" Text="Agent"></asp:Label></div>
               <div class="col-md-2"> <asp:DropDownList CssClass="form-control" ID="ddlAgent" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged">
                                       
                                    </asp:DropDownList></div>
        
        <div class="clearfix"> </div>
       
        </div>
        
        
        
        
               <div class="marbot_10"> 
               
         
               <div class="col-md-2"><asp:Label ID="lblB2BAgent" Text="B2BAgent:" runat="server"></asp:Label> </div>
             
     
             
             
             
               <div class="col-md-2">         <asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="form-control inputDdlEnabled"
                                      OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList></div>
               
               <div class="col-md-2"> <asp:Label ID="lblB2B2BAgent" Text="B2B2BAgent:" runat="server"></asp:Label></div>
               
               <div class="col-md-2"> 
               
               
               
               <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control" OnSelectedIndexChanged="ddlB2B2BAgent_SelectedIndexChanged" AutoPostBack="true"> </asp:DropDownList>
               
               </div>
               
                <div class="col-md-4"> 
               <asp:Button runat="server" ID="btnSearch" CssClass="button pull-right" Text="Search" OnClick="btnSearch_OnClick"/>
               </div>
               
               <div class="clearfix"> </div>
               </div>
        
        
        
        
        </asp:Panel>


    </div>
    
    
    
    
    <div id="errMess" class="error_module" style="display:none;"> <div id="errorMessage" style="float:left; color:Red;" class="padding-5 yellow-back width-100 center margin-top-5"> </div></div>
    
    
    
        <div> <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click">First</asp:LinkButton>
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" />
                            <asp:Label ID="lblCurrentPage" runat="server" ></asp:Label>
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" />
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click" >Last</asp:LinkButton></div>
    
    <div> 
    
        <div title="Desc" id="divDesc" style="position:absolute;display:none;right:355px;top:250px;"> </div>
    
    <div id="Div1"  runat="server">
   
    <asp:DataList  ID="dlOfflineBooking" runat="server" AutoGenerateColumns="false"
                 CellPadding="0" CellSpacing="0"
                Width="100%" ShowHeader="true" DataKeyField="OfflineBookingId">
                <HeaderStyle Font-Bold="false"   HorizontalAlign="Left"/>
                
                
               
      <HeaderTemplate >
       <table width="100%" class="datagrid">
                                        <tr>
                                           
                                            <td width="100px" align="left" class="themecol1">
                                                Agent Name
                                            </td>
                                           
                                             <td width="100px" align="left" class="themecol1">
                                               User Name
                                            </td>
                                            <td width="150px" align="left" class="themecol1">
                                               Departure Airport
                                            </td>
                                            <td width="150px" align="left" class="themecol1">
                                                Departure Date
                                            </td>
                                            <td width="150px" align="left" class="themecol1" >
                                                Arrival Airport
                                            </td>
                                             <td width="150px" align="left" class="themecol1">
                                                 Arrival Date
                                            </td>
                                             <td width="100px" align="left" class="themecol1">
                                               Location
                                            </td>
                                             <td width="100px" align="left" class="themecol1">
                                                View Details
                                            </td>
                                             
                                        </tr>
                                        
                                        
                                         </table>      
                                  
    </HeaderTemplate>
    <ItemTemplate>
    <table width="100%" class="datagrid">
    
                                        <tr>
                                            <td width="100px" align="left">
                                             <%#Eval("AgentName")%>
                                            </td>
                                            <td width="100px" align="left">
                                                <%#Eval("UserName")%>
                                            </td>
                                            <td width="150px" align="left">
                                                <%#Eval("DepartureAirport")%>
                                            </td>
                                             <td width="150px" align="left">
                                                <%#Eval("DepartureDate")%>
                                            </td>
                                            <td width="150px" align="left">
                                                <%#Eval("ArrivalAirport")%>
                                            </td>
                                            <td width="150px" align="left">
                                                <%#Eval("ArrivalDate")%>
                                            </td>
                                             <td width="100px" align="left">
                                                <%#Eval("Location")%>
                                            </td>
                                            <td width="100px">
                                               <a style=" cursor:pointer" href="OfflineBooking.aspx?OfflineBookingId=<%# Eval("OfflineBookingId") %>">View Details</a>                                              </td>
                                        </tr>
                                        
                                        </table>      
                             
    </ItemTemplate>
     </asp:DataList>
     
     
      <asp:Label ID="lblMessage" runat="server"></asp:Label>
     </div>
    
    </div>
    
    
    
    
    
    <div class="clearfix"> </div>
    
     </div>
    
    
  


      

    

    

    
    
    


   <iframe id="IShimFrame" style="position:absolute; display:none;" frameborder="0"></iframe>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

