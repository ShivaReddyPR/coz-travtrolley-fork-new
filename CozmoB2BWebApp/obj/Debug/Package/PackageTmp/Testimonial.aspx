﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="TestimonialGUI" Title="Testimonial" Codebehind="Testimonial.aspx.cs" %>
<%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<asp:HiddenField runat="server" ID="hdfTestimonialImg" Value="0" />
<asp:HiddenField runat="server" ID="hdfCount" Value="0" />
<asp:HiddenField runat="server" ID="hdfImgPath" Value="0" />
<div class="body_container">
<div class="col-md-12 padding-0 marbot_10">      
<div class="col-md-1"><asp:Label ID="lblName" Text="Name:" runat="server"></asp:Label> </div>
    
    
    <div class="col-md-4"> <asp:TextBox ID="txtName"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox></div>
    <div class="clearfix"></div>
   </div>
   <div class="col-md-12 padding-0 marbot_10">      
<div class="col-md-1"><asp:Label ID="lblTitle" Text="Title:" runat="server"></asp:Label> </div>
    
    
    <div class="col-md-4"> <asp:TextBox ID="txtTitle"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox></div>
    <div class="clearfix"></div>
   </div>
   <div class="col-md-12 padding-0 marbot_10">      
<div class="col-md-1"><asp:Label ID="lblDescription" Text="Description:" runat="server"></asp:Label> </div>
    
    
    <div class="col-md-4"> <asp:TextBox ID="txtDescription" TextMode="MultiLine"  runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox></div>
    <div class="clearfix"></div>
   </div>
   <div class="col-md-12 padding-0 marbot_10">
   <div class="col-md-1">
              <asp:Label ID="lblStatus" Text="Status:" runat="server" CssClass="label"
                 ></asp:Label>
          </div>
          <div class="col-md-4">
              <asp:DropDownList ID="ddlStatus" CssClass="inputDdlEnabled form-control" runat="server">
                  <asp:ListItem Text="Active" Value="A"></asp:ListItem>
                  <asp:ListItem Text="DeActive" Value="D"></asp:ListItem>
              </asp:DropDownList>
          </div>
          <div class="clearfix"></div>
   </div>
   
     <div class="col-md-12 padding-0 marbot_10">   
    <div class="col-md-2">image:</div>
    <div class="col-md-1"> <CT:DocumentManager ID="ImageTestimonial" runat="server" DefaultImageUrl="~/images/common/no_preview.png"
                    DocumentImageUrl="~/images/common/Preview.png"   ShowActualImage="false"  SessionName="crenterlic"
                    DefaultToolTip=""  /></div>
                   <div class="col-md-1"><asp:LinkButton ID="lnkView" runat="server" Text="View&nbsp;Image" Visible="true" OnClientClick="return View();"></asp:LinkButton></div>
         <div class="col-md-2">
            <asp:Image ID="imgPreview" Style="display: none;" runat="server" Height="70px" Width="80px" /> 
             <asp:Label ID="lblUploadMsg" runat="server"></asp:Label>
             <asp:Label ID="lblUpload1" runat="server" Visible="false"></asp:Label>
         </div>
     <div class="clearfix"></div>
    </div>
      <div class="col-md-12 padding-0 marbot_10">        
         
         
         <div class="col-md-12">
                 <label  style=" padding-left:100px;"><asp:Button  ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();" CssClass="btn but_b"  OnClick ="btnSave_Click" ></asp:Button></label>
                 
                 
                    <label  > <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b"   OnClick="btnClear_Click"></asp:Button></label>
                    
                    
                    <label > <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="btn but_b"   OnClick="btnSearch_Click"></asp:Button></label>
     
      </div>
         <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
         </div>
</div>

<script>
    function View() {
        var count = getElement('hdfCount').value;
        if (count == 1) {

            getElement('hdfCount').value = 0;
            document.getElementById('<%=imgPreview.ClientID %>').style.display = "none";
            getElement('lnkView').innerHTML = "View Image";
            return false;
        }
        else {
            document.getElementById('<%=imgPreview.ClientID %>').style.display = "block";
            document.getElementById('<%=imgPreview.ClientID %>').src = getElement('hdfImgPath').value;
            getElement('lnkView').innerHTML = "Hide";
            getElement('hdfCount').value = 1;
            return false;
        }
    }
    function Save() {
        if (getElement('txtName').value == '') addMessage('Name cannot be blank!', '');
        if (getElement('txtTitle').value == '') addMessage('Title cannot be blank!', '');
        if (getElement('txtDescription').value == '') addMessage('Description cannot be blank!', '');
        if (getMessage() != '') {
            alert(getMessage()); clearMessage(); return false;
        }
    }
</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
<asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="ID" 
    EmptyDataText="No Testimonial List!" AutoGenerateColumns="false" PageSize="17" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging" >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
    
     <asp:TemplateField>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc1:Filter   ID="HTtxtName" Width="70px" HeaderText="Name" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblname" runat="server" Text='<%# Eval("NAME") %>' CssClass="label grdof" ToolTip='<%# Eval("NAME") %>' Width="70px"></asp:Label>
    </ItemTemplate>    
    
    </asp:TemplateField>      
      
      <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtTitle"  Width="100px" CssClass="inputEnabled" HeaderText="Title" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblTitle" runat="server" Text='<%# Eval("TITLE") %>' CssClass="label grdof"  ToolTip='<%# Eval("TITLE") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtStatus"  Width="150px" CssClass="inputEnabled" HeaderText="Status" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("StatusName") %>' CssClass="label grdof"  ToolTip='<%# Eval("StatusName") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>        
    </Columns>           
    </asp:GridView>
</asp:Content>

