﻿<%@ Page Language="C#" %>
	<!DOCTYPE html>
	<html>

	<head id="Head1" runat="server">
		<title>Hotels</title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.ico">


		<link href="yui/build/reset/reset-min.css" rel="stylesheet" type="text/css" />
		<link href="css/RegisterStyle.css" rel="stylesheet" type="text/css" />
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<!-- manual css -->

		<link href="//fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
		<link href="build/css/main.min.css" rel="stylesheet" type="text/css" />

	</head>

	<body>
		<div class="topHeader2">
			<div class="container">
				<div class="row">
					<div class="col-md-12">
						<a href="bizzlogin.aspx"><img src="images/logoc.jpg"></a>
					</div>
				</div>
			</div>
		</div>
		<div class="heading_bizz">
			<div class="container">
				<h2> Hotels</h2>
			</div>
		</div>
		<form runat="server" id="form1">
			<div class="container">
				<div class="innerPage2">
					<div class="row">
						<div class="col-12">
							<p>We make you feel at home, no matter where you stay in the world. CCTM has exclusive contracts with top business hotels and relationships with boutique properties, so you feel welcome and especially cared for. Whether it is negotiating on behalf of our clients to secure a favorable rate or easing claim management via expense cards, CCTM is the trusted partner you need to manage corporate bookings and accommodation.
								<br />
								<br />We take customer satisfaction very seriously and work with our network partners, in India, the Gulf region, and across the globe, to extend a superior quality of service. From remembering your dietary preferences, accommodating a special request, to organizing group stay during a conference, offsite or sales meet, CCTM goes the extra mile and includes thoughtful touches to make your visit memorable.</p>
						</div>
					</div>
				</div>
			</div>
		</form>
		<div class="footer_sml_bizz">Copyright 2019 Cozmo Travel World, India. All Rights Reserved.</div>

	</body>

	</html>
