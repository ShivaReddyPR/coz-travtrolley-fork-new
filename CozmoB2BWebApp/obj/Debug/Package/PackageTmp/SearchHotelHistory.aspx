﻿<%@ Page Title="HotelSearch History" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="SearchHotelHistory.aspx.cs" Inherits="SearchHotelHistory" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>

    <script type="text/javascript" src="yui/build/event/event-min.js"></script>

    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>

    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>

    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>

    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>

    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>

    <%--<script type="text/javascript" src="js/Search.js"></script>--%>
    <script src="scripts/jsBE/Search.js"></script>

    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>

    <script src="yui/build/container/container-min.js" type="text/javascript"></script>

    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />
    <link href="build/css/jquery-ui.css" rel="stylesheet" />
    <script src="scripts/jquery-ui.js"></script>
    <link href="css/style.css" rel="stylesheet" type="text/css" />

    <script src="scripts/jsGrid/jsgrid.min.js"></script>
    <link href="scripts/jsGrid/jsgrid.min.css" rel="stylesheet" />
    <link href="scripts/jsGrid/jsgrid-theme.min.css" rel="stylesheet" />

    <style>
        .ins-item {
        }

        .ins-cell {
            padding: 5px 15px;
            width: 150px;
        }

        .hide {
            display: none;
        }
    </style>

    <script>
        $(document).ready(function () {
            BindHotelSearchGrid();
        });
    </script>

    <script>
        //--------------------------Calender control start-------------------------------
        var cal1;
        var cal2;

        function init() {

            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) / " + today.getDate() /" + today.getFullYear());
            //cal1.cfg.setProperty("title", "Select CheckIn date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDates1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            //cal2.cfg.setProperty("title", "Select CheckOut date");
            cal2.selectEvent.subscribe(setDates2);
            cal2.cfg.setProperty("close", true);
            cal2.render();
        }

        function showCal1() {
            init();
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            cal1.show();
            cal2.hide();
            document.getElementById('container1').style.display = "block";
        }

        var departureDate = new Date();
        function showCal2() {
            $('container1').context.styleSheets[0].display = "none";
            cal1.hide();
            init();
            // setting Calender2 min date acoording to calendar1 selected date
            var date1 = document.getElementById('<%= CheckIn.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
                var depDateArray = date1.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);

                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + (arrMinDate.getDate()) + "/" + arrMinDate.getFullYear());
                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal2.render();
            }
            document.getElementById('container2').style.display = "block";
        }

        function setDates1() {
            var date1 = cal1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            //if (difference < 0) {
            //    document.getElementById('errMess').style.display = "block";
            //    document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
            //    return false;
            //}

            departureDate = cal1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= CheckIn.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            cal1.hide();

        }

        function setDates2() {
            var date1 = document.getElementById('<%=CheckIn.ClientID %>').value;
            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
                return false;
            }

            var date2 = cal2.getSelectedDates()[0];

            var depDateArray = date1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 1) {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
                return false;
            }

            if (difference == 0) {

                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
                return false;
            }

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=CheckOut.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init);
    </script>

    <script>
        //--------------------------Calender control start-------------------------------
        var call1;
        var call2;

        function init1() {

            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            call1 = new YAHOO.widget.Calendar("call1", "Fcontainer1");
            //cal1.cfg.setProperty("minDate", (today.getMonth() + 1) /" + today.getDate() /" + today.getFullYear());
            //call1.cfg.setProperty("title", "Select From date");
            call1.cfg.setProperty("close", true);
            call1.selectEvent.subscribe(FsetDates1);
            call1.render();

            call2 = new YAHOO.widget.Calendar("call2", "Tcontainer2");
            //call2.cfg.setProperty("title", "Select To date");
            call2.selectEvent.subscribe(TsetDates2);
            call2.cfg.setProperty("close", true);
            call2.render();
        }

        function FshowCal1() {
            init1();
            $('Tcontainer2').context.styleSheets[0].display = "none";
            $('Fcontainer1').context.styleSheets[0].display = "block";
            call1.show();
            call2.hide();
            document.getElementById('Fcontainer1').style.display = "block";
        }

        var departureDt = new Date();
        function TshowCal2() {
            $('Fcontainer1').context.styleSheets[0].display = "none";
            call1.hide();
            init1();
            // setting Calender2 min date acoording to calendar1 selected date
            var dt1 = document.getElementById('<%=FromDate.ClientID%>').value;           

            if (dt1.length != 0 && dt1 != "DD/MM/YYYY") {
                var depDateArray = dt1.split('/');

                var arrMinDate = new Date(departureDt.getFullYear(), departureDt.getMonth(), departureDt.getDate());

                call2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                call2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                call2.render();
            }
            document.getElementById('Tcontainer2').style.display = "block";
        }

        function FsetDates1() {
            var dt1 = call1.getSelectedDates()[0];

            $('IShimFrame').context.styleSheets[0].display = "none";
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            var depdate = new Date(dt1.getFullYear(), dt1.getMonth(), dt1.getDate());
            var difference = (depdate.getTime() - todaydate.getTime());

            departureDt = call1.getSelectedDates()[0];
            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = dt1.getMonth() + 1;
            var day = dt1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= FromDate.ClientID %>').value = day + "/" + (month) + "/" + dt1.getFullYear();

            call1.hide();
        }

        function TsetDates2() {
            var dt1 = document.getElementById('<%=FromDate.ClientID %>').value;
            if (dt1.length == 0 || dt1 == "DD/MM/YYYY") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "First select From date.";
                return false;
            }

            var dt2 = call2.getSelectedDates()[0];

            var depDateArray = dt1.split('/');

            // checking if date1 is valid		    
            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
                return false;
            }

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            // Note: Date()	for javascript take months from 0 to 11
            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(dt2.getFullYear(), dt2.getMonth(), dt2.getDate());
            var difference = returndate.getTime() - depdate.getTime();

            document.getElementById('errMess').style.display = "none";
            document.getElementById('errorMessage').innerHTML = "";

            var month = dt2.getMonth() + 1;
            var day = dt2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=ToDate.ClientID %>').value = day + "/" + month + "/" + dt2.getFullYear();
            call2.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init1);
    </script>

    <script>

        function validate() {
            if (BetweenDates() && checkDates()) {
            }
            else {
                return false;
            }
        }

        function BetweenDates() {

            document.getElementById('errMess').style.display = "none";
            var dt1 = document.getElementById('<%= FromDate.ClientID %>').value;
            var dt2 = document.getElementById('<%= ToDate.ClientID %>').value;
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);
            if (dt1 != null && (dt1 == "DD/MM/YYYY" || dt1 == "")) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select From Date";
                return false;
            }
            var depDateArray = dt1.split('/');

            if (dt2 != null && (dt2 == "DD/MM/YYYY" || dt2 == "")) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please Select To Date";
                return false;
            }
            var retDateArray = dt2.split('/');

            // checking if date2 is valid	
            if (!CheckValidDate(retDateArray[0], retDateArray[1], retDateArray[2])) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = " Invalid To Date";
                return false;
            }

            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "To date should be greater than or equal to From date";
                return false;
            }
            return true;
        }

        function checkDates() {
            document.getElementById('errMess').style.display = "none";
            var date1 = document.getElementById('<%= CheckIn.ClientID %>').value;
            var date2 = document.getElementById('<%= CheckOut.ClientID %>').value;
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            var todaydate = new Date(thisYear, thisMonth, thisDay);

            var depDateArray = date1.split('/');

            if ((date1 != null && date1 != "")) {
                if (date2 == "") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Check Out Date";
                    return false;
                }
                else {
                    document.getElementById('errMess').style.display = "none";
                }
            }
            if ((date2 != null && date2 != "")) {
                if (date1 == "") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Select Check In Date";
                    return false;
                }
                else {
                    document.getElementById('errMess').style.display = "none";
                }
            }

            var retDateArray = date2.split('/');

            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
            var returndate = new Date(retDateArray[2], retDateArray[1] - 1, retDateArray[0]);
            var difference = returndate.getTime() - depdate.getTime();

            if (difference < 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "CheckOut date should be greater than or equal to CheckIn date";
                return false;
            }
            if (difference == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "CheckIn date and CheckOut date could not be same";
                return false;
            }
            return true;
        }

    </script>

    <script>
        function BindHotelSearchGrid() {
            try {
                validate();

                let fromDateVal = $('#<%= FromDate.ClientID %>').val();
                let toDateVal = $('#<%= ToDate.ClientID %>').val();
                let agentId = $('#<%= ddlAgents.ClientID %>').val();
                let locationId = $('#<%= ddlLocations.ClientID %>').val();
                let checkInVal = $('#<%= CheckIn.ClientID %>').val();
                let checkOutVal = $('#<%= CheckOut.ClientID %>').val();
                let cityNameVal = $('#<%= txtCityName.ClientID %>').val();
                let NationalityVal = $('#<%= ddlNationality.ClientID %>').val();               
                let UserVal;              
                if ($('#ctl00_cphTransaction_ddlConsultant').val() == "" || $('#ctl00_cphTransaction_ddlConsultant').val() == null) {
                    UserVal = 0;
                }
                else {                    
                    UserVal = $('#<%= ddlConsultant.ClientID %>').val();
                }

                let htlParams = {
                    fromDate: fromDateVal,
                    toDate: toDateVal,
                    agent: agentId,
                    location: locationId,
                    user: UserVal,
                    checkIn: checkInVal,
                    checkOut: checkOutVal,
                    cityName: cityNameVal,
                    Nationality: NationalityVal

                };
                let inputData = JSON.stringify(htlParams);
                $("#jsGrid").jsGrid({
                    height: "auto",
                    width: "100%",
                    sorting: false,
                    paging: true,
                    filtering: true,
                    autoload: true,
                    pageSize: 15,
                    pageButtonCount: 5,
                    controller: {
                        loadData: function (filter) {
                            let result = [];
                            var data = $.Deferred();
                            $.ajax({
                                type: "POST",
                                contentType: "application/json; charset=utf-8",
                                url: "SearchHotelHistory.aspx/BindGrid",
                                data: inputData,
                                dataType: "json"
                            }).done(function (response) {
                                try {
                                    //var result = JSON.parse(response.d);
                                    result = JSON.parse(response.d);
                                    //console.log(response.d);
                                    result = $.grep(result, function (row) {
                                        return (row.StartDate.indexOf(filter.StartDate) >= 0)
                                            && (row.EndDate.indexOf(filter.EndDate) >= 0)
                                            && (row.AgentName.toLowerCase().indexOf(filter.AgentName.toLowerCase()) >= 0)
                                            && (row.UserName.toLowerCase().indexOf(filter.UserName.toLowerCase()) >= 0)
                                            && (row.LocationName.toLowerCase().indexOf(filter.LocationName.toLowerCase()) >= 0)
                                            && (row.CityName.toLowerCase().indexOf(filter.CityName.toLowerCase()) >= 0)
                                            && (row.CountryName.toLowerCase().indexOf(filter.CityName.toLowerCase()) >= 0)
                                            && (row.PassengerNationality.toLowerCase().indexOf(filter.TransType.toLowerCase()) >= 0)
                                            && (row.NoOfRooms.toString().indexOf(filter.NoOfRooms.toString()) >= 0)
                                            && (row.MinRating.toString().indexOf(filter.MinRating.toString()) >= 0)
                                            && (row.MaxRating.toString().indexOf(filter.MaxRating.toString()) >= 0)
                                            && (row.Sources.toLowerCase().indexOf(filter.Sources.toLowerCase()) >= 0)
                                            && (row.SearchDateTime.indexOf(filter.SearchDateTime) >= 0)
                                            && (row.TransType.toLowerCase().indexOf(filter.TransType.toLowerCase()) >= 0)                                            
                                            && (row.HistoryId.toString().indexOf(filter.HistoryId.toString()) >= 0);
                                    });
                                    data.resolve(result);
                                }
                                catch{
                                    alert('Error occured. Please try again.');
                                }
                            });
                            return data.promise();
                        }
                    },
                    fields: [
                        { type: "control", width: 150, editButton: false, deleteButton: false },
                        { name: "SearchDateTime", title: "Search DateTime", type: "text", width: 150 },
                        { name: "StartDate", title: "Check In", type: "text", width: 150 },
                        { name: "EndDate", title: "Check Out", type: "text", width: 150 },
                        { name: "AgentName", title: "Agent Name", type: "text", width: 150 },
                        { name: "UserName", title: "User Name", type: "text", width: 150 },
                        { name: "LocationName", title: "Location Name", type: "text", width: 150 },
                        { name: "CityName", title: "City Name", type: "text", width: 150 },
                        { name: "CountryName", title: "Country Name", type: "text", width: 150 },
                        { name: "PassengerNationality", title: "Nationality", type: "text", width: 150 },    
                        { name: "NoOfRooms", title: "NoOfRooms", type: "text", width: 150 },
                        { name: "MinRating", title: "Min Rating", type: "text", width: 150 },
                        { name: "MaxRating", title: "Max Rating", type: "text", width: 150 },
                        { name: "Sources", title: "Sources", type: "text", width: 150 },                        
                        { name: "TransType", title: "Trans Type", type: "text", width: 150 },                            
                        { name: "HistoryId", width: 0, css: "hide" }

                    ],
                    rowRenderer: function (item) {                        
                        let $tdAttrs = { class: "ins-cell" };
                        let $button = $("<td>").attr($tdAttrs);
                        $button = $("<td>").attr($tdAttrs).append($("<button>Guest Details</button>").attr({ class: "btn btn-control btn-default pl-4" }))
                            .click(function (e) {
                                ShowPaxDetails(item.HistoryId);
                                BindHotelSearchGrid();
                                e.stopPropagation();
                            });
                        let SDate = new Date(item.StartDate);
                        let CheckIn = ('0' + SDate.getDate()).slice(-2) + '-' + ('0' + (SDate.getMonth() + 1)).slice(-2) + '-' + SDate.getFullYear();
                        let EDate = new Date(item.EndDate);
                        let CheckOut = ('0' + EDate.getDate()).slice(-2) + '-' + ('0' + (EDate.getMonth() + 1)).slice(-2) + '-' + EDate.getFullYear();
                        let $row = $("<tr>")
                            .append($button).attr($tdAttrs)
                            .append($("<td>").attr($tdAttrs).append(item.SearchDateTime))
                            .append($("<td>").attr($tdAttrs).append(CheckIn))
                            .append($("<td>").attr($tdAttrs).append(CheckOut))
                            .append($("<td>").attr($tdAttrs).append(item.AgentName))
                            .append($("<td>").attr($tdAttrs).append(item.UserName))
                            .append($("<td>").attr($tdAttrs).append(item.LocationName))
                            .append($("<td>").attr($tdAttrs).append(item.CityName))
                            .append($("<td>").attr($tdAttrs).append(item.CountryName))
                            .append($("<td>").attr($tdAttrs).append(item.PassengerNationality))
                            .append($("<td>").attr($tdAttrs).append(item.NoOfRooms))                           
                            .append($("<td>").attr($tdAttrs).append(item.MinRating))
                            .append($("<td>").attr($tdAttrs).append(item.MaxRating))
                            .append($("<td>").attr($tdAttrs).append(item.Sources))                            
                            .append($("<td>").attr($tdAttrs).append(item.TransType))
                            .append($("<td style='display: none;'>").attr($tdAttrs).append(item.HistoryId));

                        return $row;
                    }
                });
                return false;
            }
            catch{
                alert('Error occured. Please try again..')
            }
        }
    </script>

    <script>
        function ShowPaxDetails(HistoryId) {
            $.ajax({
                type: 'POST',
                url: "SearchHotelHistory.aspx/GetRoomGuestDetails",
                dataType: "json",
                contentType: "application/json",
                data: "{'HistoryId':'" + HistoryId + "'}",
                success: function (data) {
                    var d = JSON.parse(data.d);
                    $("#PaxHistory").html('');
                    var items = '';
                    items = '<table class="table-standard table table-condensed table-bordered mb-0" cellspacing="0" width="100%" border="0">'
                    items += '<thead><tr><th scope="col">Adults</th><th scope="col">Childs</th><th scope="col">ChildAge1</th><th scope="col">ChildAge2</th><th scope="col">ChildAge3</th><th scope="col">ChildAge4</th><th scope="col">ChildAge5</th><th scope="col">ChildAge6</th></tr></thead><tbody>'
                    $.each(d, function (i, val) {
                        items += '<tr><td>' + val.Adults + '</td><td>' + val.childs + '</td><td>' + val.ChildAge1 + '</td><td>' + val.ChildAge2 + '</td><td>' + val.ChildAge3 + '</td><td>' + val.ChildAge4 + '</td><td>' + val.ChildAge5 + '</td><td>' + val.ChildAge6 + '</td></tr>'
                    });
                    items += '</tbody></table>';
                    $("#PaxHistory").html(items);
                }
            })
            $("#PaxDetailsPop").modal("show");
        }
    </script>

    <div id="errMess" class="error_module" style="display: none;">
        <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
        </div>
    </div>

    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0"></iframe>
    <div class="clear" style="margin-left: 25px">
        <div id="container1" style="position: absolute; z-index: 9999; top: 120px; left: 8%; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container2" style="position: absolute; z-index: 9999; top: 120px; left: 40%; display: none;">
        </div>
    </div>

    <div class="clear" style="margin-left: 25px">
        <div id="Fcontainer1" style="position: absolute; z-index: 9999; top: 120px; left: 8%; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="Tcontainer2" style="position: absolute; z-index: 9999; top: 120px; left: 40%; display: none;">
        </div>
    </div>
    <div class="margin-top-5">
        <div class="col-md-2">FromDate:<span style="color: red">*</span></div>
        <div class="col-md-2">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <asp:TextBox ID="FromDate" runat="server" CssClass="form-control" Width="120px"></asp:TextBox>
                    </td>
                    <td>
                        <a href="javascript:void(null)" onclick="FshowCal1()">
                            <img id="dateLink2" src="images/call-cozmo.png" alt="Pick Date" />
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </div>
    <div class="margin-top-5">
        <div class="col-md-2">ToDate:<span style="color: red">*</span></div>
        <div class="col-md-2">
            <table border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <asp:TextBox ID="ToDate" CssClass="form-control" runat="server" Width="120px"></asp:TextBox>
                    </td>
                    <td>
                        <a href="javascript:void(null)" onclick="TshowCal2()">
                            <img id="Img2" src="images/call-cozmo.png" alt="Pick Date" />
                        </a>
                    </td>
                    <td>
                </tr>
            </table>
        </div>
    </div>
    <div class="margin-top-5">
        <div class="col-md-2">Agent: </div>
        <div class="col-md-2">
            <asp:DropDownList ID="ddlAgents" CssClass="form-control" runat="server" AppendDataBoundItems="true" AutoPostBack="true" OnSelectedIndexChanged="ddlAgents_SelectedIndexChanged">
            </asp:DropDownList>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="margin-top-5">
        <div class="col-md-2">User:</div>
        <div class="col-md-2">
            <asp:DropDownList ID="ddlConsultant" CssClass="form-control" runat="server"></asp:DropDownList>
        </div>
    </div>
    <div class="margin-top-5">
        <div class="col-md-2">Location: </div>
        <div class="col-md-2">
            <asp:DropDownList ID="ddlLocations" CssClass="form-control" runat="server" AppendDataBoundItems="true" Enabled="false">
                <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
            </asp:DropDownList>
        </div>
    </div>
    <div class="margin-top-5">
        <div class="col-md-2">Nationality: </div>
        <div class="col-md-2" id="divcitynationality">
            <div class="form-control-holder">
                <asp:DropDownList CssClass="form-control" ID="ddlNationality" runat="server"
                    DataTextField="CountryName" DataValueField="CountryCode"
                    AppendDataBoundItems="True">                    
                </asp:DropDownList>
            </div>
        </div>
    </div>
    <div class="col-md-2">CheckIn:  </div>
    <div class="col-md-2">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <asp:TextBox ID="CheckIn" runat="server" CssClass="form-control" Width="120px"></asp:TextBox>
                </td>
                <td>
                    <a href="javascript:void(null)" onclick="showCal1()">
                        <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                    </a>
                </td>
            </tr>
        </table>
    </div>

    <div class="col-md-2">CheckOut: </div>
    <div class="col-md-2">
        <table border="0" cellspacing="0" cellpadding="0">
            <tr>
                <td>
                    <asp:TextBox ID="CheckOut" CssClass="form-control" runat="server" Width="120px"></asp:TextBox>
                </td>
                <td>
                    <a href="javascript:void(null)" onclick="showCal2()">
                        <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" />
                    </a>
                </td>
                <td>
            </tr>
        </table>
    </div>

    <div class="col-md-2">City Name: </div>
    <div class="col-md-2">
        <asp:TextBox ID="txtCityName" runat="server" CssClass="inputEnabled form-control"></asp:TextBox>
    </div>

    <div class="clearfix"></div>
    <div class="margin-top-5">
        <div class="col-md-10"></div>
        <div class="col-md-2">
            <asp:Button ID="btnSubmit" runat="server" class="btn btn-primary" Text="Search" OnClientClick="return BindHotelSearchGrid();" />            
            <asp:Button ID="btnCancel" runat="server" class="btn btn-default" Text="Cancel" OnClick="btnCancel_Click"/>
        </div>
    </div>

    <div class="clearfix"></div>

    <div id="jsGrid"></div>
       
    <div class="modal fade in farerule-modal-style" data-backdrop="static" id="PaxDetailsPop" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel" style="display: none;">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" id="btnModelC">&times;</button>
                    <h4 class="modal-title">RoomGuest Details</h4>
                </div>
                <div class="modal-body">
                    <p class="Ruleshdr" id="PaxHistory"></p>
                </div>
            </div>
        </div>
    </div>

    <asp:Label Style="color: red; padding-left: 500px;" ID="lblError" runat="server"></asp:Label>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
