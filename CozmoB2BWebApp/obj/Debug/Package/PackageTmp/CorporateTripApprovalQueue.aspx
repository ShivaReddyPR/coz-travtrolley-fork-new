﻿<%@ Page Language="C#"  MasterPageFile="~/TransactionVisaTitle.master"  AutoEventWireup="true" Title="Corporate Trip Approval Queue" Inherits="CorporateTripApprovalQueue" Codebehind="CorporateTripApprovalQueue.aspx.cs" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<script type="text/javascript">

    function Validate() {

        var valid = false;
        document.getElementById('errMess').style.display = "none";

        if (document.getElementById('ctl00_cphTransaction_dcReimFromDate_Date').value.length == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please select from date .";
        }
        else if (document.getElementById('ctl00_cphTransaction_dcReimToDate_Date').value.length == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please select to date.";
        }

        else {
            valid = true;
        }

        return valid;
    }

    //Validating Hotel Tab feilds
    function ValidateHotelTab() {

        var valid = false;
        document.getElementById('errMess').style.display = "none";

        if (document.getElementById('ctl00_cphTransaction_dcHotelFromDate_Date').value.length == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please select from date .";
        }
        else if (document.getElementById('ctl00_cphTransaction_dcHotelToDate_Date').value.length == 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errMess').innerHTML = "Please select to date.";
        }

        else {
            valid = true;
        }

        return valid;
    }

    //Tab click function 
    function SetTabSelection(tabIndex) {
         document.getElementById('<%=hdnBindData.ClientID%>').value = "true";
        if (tabIndex == "1") {
            $('#hotelTab').removeClass('active');
            $('#flightTab').addClass('active');
            document.getElementById('<%=hdnProductType.ClientID %>').value = "1";
            document.forms[0].submit();
        }
        else {
            $('#flightTab').removeClass('active');
            $('#hotelTab').addClass('active');
            document.getElementById('<%=hdnProductType.ClientID %>').value = "2";
            document.forms[0].submit();
        }

    }

    // setting tab as active after post back 
    function SetActiveTab() {

        if (document.getElementById('<%=hdnProductType.ClientID %>').value == "2") {
            $('#flightTab').removeClass('active');
            $('#hotelTab').addClass('active');
            $('#divFlightTab').removeClass('active');
            $('#divHotelTab').addClass('active');
            $('#liflightTab').removeClass('active');
             $('#liHotelTab').addClass('active');
        }
        else {
            $('#hotelTab').removeClass('active');
            $('#flightTab').addClass('active');
            $('#divHotelTab').removeClass('active');
            $('#divFlightTab').addClass('active');
            $('#liHotelTab').removeClass('active');
            $('#liflightTab').addClass('active');

        }
    }

    function ViewHotelSummary(ConfNo, hotelId, empId, empName, status, Category) {
        AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'CorpHotelSummary', 'sessionData':'" + (ConfNo + '|' + hotelId + '|' + empId + '|' + empName + '|' + status + '|' + Category) + "', 'action':'set'}");
            window.open("CorpHotelSummary.aspx", "CorpHotelSummary", '_blank');
            return false;
        }


</script>

<div class="body_container">
        <div class="col-md-12 CorpTrvl-tabbed-panel">
            <h2 style="color: #60c231;
    text-align: center;
    font-weight: bold;
    font-size: 15px;
    text-decoration: underline;
    text-transform: uppercase;">
                Trip Approvers Queue</h2>

                
                <ul class="nav nav-tabs responsive" role="tablist">
                <li class="" role="presentation" id="liflightTab"><a href="#FlightTab" aria-controls="destinations-policy" id="flightTab"
                    role="tab" data-toggle="tab" aria-expanded="true" onclick="SetTabSelection(1);" >Flight</a></li>
                <li role="presentation" class="" id="liHotelTab"><a href="#HotelTab" aria-controls="air-policy" id="hotelTab"
                    role="tab" data-toggle="tab" aria-expanded="false" onclick="SetTabSelection(2);">Hotel</a></li>
               </ul>
                 <div id="errMess" style="display: none; color: Red; font-weight: bold; text-align: center;">
                 </div>
             <asp:HiddenField ID="hdnProductType" runat="server" Value="1" />
            <asp:HiddenField ID="hdnBindData" runat="server" Value="false" />
            <div class="tab-content responsive">
                <div role="tabpanel" class="tab-pane active" id="divFlightTab">
                  <div style="background: #fff; padding: 10px;">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    From Date <span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcReimFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                    </uc1:DateControl>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    To Date <span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcReimToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Select Employee</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlEmployee" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                      <%--<div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Select Booking Type</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlBookingType" runat="server">                                    
                                    <asp:ListItem Selected="True" Value="Normal" Text="Normal"></asp:ListItem>
                                    <asp:ListItem Value="Routing" Text="Routing"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>--%>
                        
                        <div class="col-md-2">
                            <label class="center-block">
                                &nbsp;</label>
                            <asp:Button OnClientClick="return Validate();" runat="server" ID="btnSearch" OnClick="btnSearch_Click" CssClass="btn but_d btn_xs_block cursor_point mar-5"
                                Text="Search" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="CorpTrvl-tabbed-panel">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#Pending" aria-controls="Pending"
                                        role="tab" data-toggle="tab" aria-expanded="true">Pending</a></li>
                                
                                    
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="Pending" style="overflow-x:scroll">
                                         
                                           <div class="table table-responsive mb-0">
                            <asp:GridView OnRowDataBound="gvPending_RowDataBound"  ID="gvPending" Width="100%" runat="server" AllowPaging="true" DataKeyNames="TRIP_ID"
                                EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvPending_PageIndexChanging">
                                <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                <Columns>
                                    <asp:TemplateField HeaderText="Details">
                                        <ItemTemplate>
                                            <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" 
                                                Text='View Details'/>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMP_ID" Width="100px" HeaderText="Employee ID" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMP_ID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_ID") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMP_NAME" Width="100px" HeaderText="Employee Name" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMP_NAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_NAME") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtTRIP_ID" Width="150px" HeaderText="Reference No" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblTRIP_ID" runat="server" Text='<%# Eval("UNIQUE_TRIP_ID") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("UNIQUE_TRIP_ID") %>' Width="150px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtBOOKING_DATE" Width="100px" HeaderText="Booking Date" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblBOOKING_DATE" runat="server" Text='<%# Eval("BOOKING_DATE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("BOOKING_DATE") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtTRAVEL_DATE" Width="100px" HeaderText="Travel Date" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblTRAVEL_DATE" runat="server" Text='<%# Eval("TRAVEL_DATE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("TRAVEL_DATE") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtRETURN_DATE" Width="100px" HeaderText="Return Date" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblRETURN_DATE" runat="server" Text='<%# Eval("RETURN_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("RETURN_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtAIRLINE_CODE" Width="200px" HeaderText="Airline" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAIRLINE_CODE" runat="server" Text='<%# Eval("AIRLINE_CODE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("AIRLINE_CODE") %>' Width="200px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtROUTE" Width="300px" HeaderText="Route" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblROUTE" runat="server" Text='<%# Eval("ROUTE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("ROUTE") %>' Width="300px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtREASON_OF_TRAVEL" Width="100px" HeaderText="Reason Of Travel" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblREASON_OF_TRAVEL" runat="server" Text='<%# Eval("REASON_OF_TRAVEL") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("REASON_OF_TRAVEL") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtTOTAL_FARE" Width="120px" HeaderText="Fare" CssClass="form-control"
                                                OnClick="FilterSearch1_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblTOTAL_FARE" runat="server" Text='<%# Eval("BOOKING_TYPE") == "Routing" ? GetRoundedAmount(Convert.ToDecimal(Eval("TOTAL_FARE")), Eval("Source").ToString()) : Eval("TOTAL_FARE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("BOOKING_TYPE") == "Routing" ? GetRoundedAmount(Convert.ToDecimal(Eval("TOTAL_FARE")), Eval("Source").ToString()) : Eval("TOTAL_FARE") %>' Width="120px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>                                           
                                    
                                </Columns>
                            </asp:GridView>
                                          </div>
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                    </div>
                   
                </div>
                </div>

                <div role="tabpanel" class="tab-pane" id="divHotelTab">
                    <div style="background: #fff; padding: 10px;">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>
                                        From Date <span class="fcol_red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                        <uc1:DateControl ID="dcHotelFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                        </uc1:DateControl>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>
                                        To Date <span class="fcol_red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                        <uc1:DateControl ID="dcHotelToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>
                                        Select Employee</label>
                                    <asp:DropDownList CssClass="form-control" ID="ddlHtlEmployees" runat="server">
                                        <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label class="center-block">
                                    &nbsp;</label>
                                <asp:Button OnClientClick="return ValidateHotelTab();" runat="server" ID="btnHotelSearch"   OnClick="btnHotelSearch_Click"
                                    CssClass="btn but_d btn_xs_block cursor_point mar-5" Text="Search" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="CorpTrvl-tabbed-panel">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#HTPending" aria-controls="Pending"
                                            role="tab" data-toggle="tab" aria-expanded="true">Pending</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="HTPending">
                                            <div class="table table-responsive mb-0">
                                                <asp:GridView ID="gvHTPending" Width="100%" runat="server" AllowPaging="true" DataKeyNames="ROWID"
                                                    EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                                    GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvHTPending_PageIndexChanging" >
                                                    <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                                    <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                                    <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtEMP_ID" Width="100px" HeaderText="Employee Id" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblEMP_ID" runat="server" Text='<%# Eval("EmpId") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("EmpId") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtEMP_NAME" Width="70px" HeaderText="Employee Name" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblEMP_NAME" runat="server" Text='<%# Eval("EmpName") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("EmpName") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtRefenceNo" Width="70px" HeaderText="Reference No" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblCOUNTRY" runat="server" Text='<%# Eval("Reference_No") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("Reference_No") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtBookingDate" Width="70px" HeaderText="booking Date" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblBookingDate" runat="server" Text='<%# Eval("BookingDate") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("BookingDate") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtCheckinDate" Width="70px" HeaderText="Checkin Date" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblCheckinDate" runat="server" Text='<%# Eval("CheckinDate") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("CheckinDate") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtCheckoutDate" Width="70px" HeaderText="Checkout Date" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblCheckoutDate" runat="server" Text='<%# Eval("CheckoutDate") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("CheckoutDate") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                         <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtHotelName" Width="70px" HeaderText="Hotel Name" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblHotelname" runat="server" Text='<%# Eval("HotelName") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("HotelName") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                         <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtCity" Width="70px" HeaderText="City" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblCity" runat="server" Text='<%# Eval("City") %>' CssClass="label grdof" style="text-align: left"
                                                                    ToolTip='<%# Eval("City") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtPrice" Width="100px" HeaderText="Price" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblPrice" runat="server" Text='<%# GetPriceRoundOff(Eval("Price")) %>' CssClass="label grdof"
                                                                    ToolTip='<%# GetPriceRoundOff(Eval("Price")) %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                         <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtReasonOfTravel" Width="70px" HeaderText="Reason Of Travel" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblReasonOfTravel" runat="server" Text='<%# Eval("ReasonOfTravel") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("ReasonOfTravel") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Details">
                                                            <ItemTemplate>
                                                                <%--<asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# string.Format("~/CorpHotelSummary.aspx?detailID={0}&empId={1}&empName={2}&status=P&Category=L",
Eval("HotelId"),Eval("EmpId"),Eval("EmpName")) %>'
                                                                    Text='View Details' />--%>
                                                                <a href="#" onclick='<%# "ViewHotelSummary(" +Eval("HotelId").ToString() + ",\""+Eval("EmpId").ToString()+"\",\""+Eval("EmpName")+"\",\""+"P"+"\",\""+"L"+"\" );" %>'>View Details </a>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>



</asp:Content>

