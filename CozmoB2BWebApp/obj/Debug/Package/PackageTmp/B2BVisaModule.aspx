﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="B2BVisaModuleUI" EnableEventValidation="true"
    Title="B2BVisa" Codebehind="B2BVisaModule.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.BookingEngine" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <style>
        .iframe-wrapper {
            position: relative;
            overflow: hidden;
            border: 1px solid #e6e9ed;
        }
       /* .iframe-wrapper:before,.iframe-wrapper:after {
            content: '';
            position: absolute;
            background-color: transparent;
            height: 36px;
            width: 30px;
            right: 21px;
            top: 0;
            z-index: 10000;
        }*/
        .iframe-wrapper:after {
            background-color: #fafafa;
            height: 28px;
            width: 27px;
        }
        .iframe-wrapper iframe{
            width:100%;
            border:0;
            height:600px;
            /*margin-top: -62px;*/
        }
    </style>
    <div class="iframe-wrapper">
        <%--<%=System.Configuration.ConfigurationSettings.AppSettings["VisaIframeUrl"].ToString() %>?b2bagentId=<%=b2bagentId %>&userId=<%=userId %>&userLoginName=<%=userName%>&product=<%=product%>&password=<%=password%>--%>
        <iframe  id="iframeVisa" src="<%=System.Configuration.ConfigurationSettings.AppSettings["VisaIframeUrl"].ToString() %>?b2bagentId=<%=b2bagentId %>&userId=<%=userId %>&userLoginName=<%=userName%>&product=<%=product%>&password=<%=password%>">
        </iframe>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
