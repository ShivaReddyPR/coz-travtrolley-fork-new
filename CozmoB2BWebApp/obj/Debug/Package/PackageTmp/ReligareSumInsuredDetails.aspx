﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="ReligareSumInsuredDetails.aspx.cs" Inherits="CozmoB2BWebApp.ReligareSumInsuredDetails" %>

<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Src="~/DocumentManager.ascx" TagPrefix="CT" TagName="DocumentManager" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <div>
        <h3>Religare Sum Insured Details Master</h3>
    </div>
    <div class="body_container" style="height: 200px">
        <div class="col-md-12 padding-0 marbot_10">


            <div class="col-md-2">
                <asp:Label ID="lblProductTypeName" runat="server" Text="Product Types:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:DropDownList CssClass="form-control" ID="DdlProductTypeName" runat="server" OnSelectedIndexChanged="DdlProductTypeName_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="col-md-2">
                <asp:Label ID="lblProductDetailName" runat="server" Text="Product Details:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:DropDownList CssClass="form-control" ID="DdlProdcutDetailsId" runat="server" OnSelectedIndexChanged="DdlProdcutDetailsId_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList>
            </div>
            <div class="col-md-2">
                <asp:Label ID="lblSumInsuredId" runat="server" Text="Range Description:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:DropDownList CssClass="form-control" ID="DdlSumInsuredId" runat="server" AutoPostBack="true"></asp:DropDownList>
            </div>
            <div class="clearfix">
            </div>
        </div>

        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                <asp:Label ID="lblAgeBand" runat="server" Text="Age Band:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtAgeBand" runat="server"></asp:TextBox>
            </div>

            <div class="col-md-2">
                <asp:Label ID="lblPlan" runat="server" Text="Plan:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtPlan" runat="server" onKeyPress="return onlyAlphabets(event)"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Label ID="lblPed" runat="server" Text="PED:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:RadioButton ID="rbPed" GroupName="PED" Text="Yes" runat="server" />
                <asp:RadioButton ID="rbPed1" GroupName="PED" Text="No" runat="server" />
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                <asp:Label ID="lblSumInsured" runat="server" Text="Sum Insured:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtSumInsured" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Label ID="lblTripType" runat="server" Text="Trip Type:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtTripType" runat="server" onKeyPress="return onlyAlphabets(event)"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Label ID="lblMemberInAgeBand" runat="server" Text="Member In Age Band:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtMemberInAgeBand" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                <asp:Label ID="lblTerm" runat="server" Text="Term:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtTerm" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>

                <asp:DropDownList ID="ddlTerm" CssClass="form-control" Style="width: 150px;"
                    runat="server">
                    <asp:ListItem Value="0">-- Select Term --</asp:ListItem>
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="6">6</asp:ListItem>
                    <asp:ListItem Value="9">9</asp:ListItem>
                    <asp:ListItem Value="12">12</asp:ListItem>
                    <asp:ListItem Value="15">15</asp:ListItem>
                    <asp:ListItem Value="18">18</asp:ListItem>
                    <asp:ListItem Value="24">24</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="col-md-2">
                <asp:Label ID="lblPremium" runat="server" Text="Premium:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtPremium" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Label ID="lblPremiumRoundOff" runat="server" Text="Premium RoundOff:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtPremiumRoundOff" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>
            </div>
            <div class="clearfix">
            </div>
        </div>
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
                <asp:Label ID="lblPremiumWithGst" runat="server" Text="Premium With GST:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtPremiumWithGst" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>
            </div>
            <div class="col-md-2">
                <asp:Label ID="lblPremiumRoundOfWithGst" runat="server" Text="Premium RoundOffWith GST:" onKeyPress="return isNumber(event)"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtPremiumRoundOfWihtGst" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>
            </div>
            <div class="clearfix"></div>
        </div>

        <div class="col-md-12">
            <label style="padding-right: 5px" class="f_R">
                <asp:Button ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();" CssClass="btn but_b" OnClick="btnSave_Click"></asp:Button></label>
            <label style="padding-right: 5px" class="f_R">
                <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b" OnClick="btnClear_Click"></asp:Button></label>
            <label style="padding-right: 5px" class="f_R">
                <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="btn but_b" OnClick="btnSearch_Click"></asp:Button></label>
        </div>

    </div>
    <div class="col-md-10">
        <asp:Label ID="lblSuccessMsg" runat="server" Text=""></asp:Label>
        <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
    </div>
    <asp:HiddenField runat="server" ID="hdfMeid" Value="0" />
    <script type="text/javascript">
        function Save() {
            if (getElement('DdlSumInsuredId').value == -1) addMessage('Prodcut Type cannot be blank!', '');
            if (getElement('txtAgeBand').value == '') addMessage('Age Band cannot be blank!', '');
            if (getElement('txtPlan').value == '') addMessage('Plan cannot be blank!', '');
            if (getElement('txtTerm').value == '') addMessage('Term cannot be blank!', '')
            //if (getElement('ddlTerm').value == 0) addMessage('Plan cannot be blank!', '')


            if (getMessage() != '') {
                //alert(getMessage());
                alert(getMessage()); clearMessage();
                return false;
            }
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
                return false;
            }
            return true;
        }

        //function onlyAlphabets(e) {
        //    return (e.charCode > 64 &&
        //        e.charCode < 91) || (e.charCode > 96 && e.charCode < 123)
        //}


    </script>

</asp:Content>

<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">

    <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="SUM_INSURED_DETAIL_ID"
        emptydatalist="No Location List!" AutoGenerateColumns="false" PageSize="15" GridLines="None"
        CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4"
        CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">


        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />" ControlStyle-CssClass="label" ShowSelectButton="True" />
            <asp:TemplateField HeaderText ="SL.NO">
        <ItemTemplate>
             <%#Container.DataItemIndex+1 %>
        </ItemTemplate>
    </asp:TemplateField>
             <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="ITlblAgeBand" runat="server" Text='<%# Eval("PRODUCT_TYPE") %>' CssClass="label grdof" ToolTip='<%# Eval("PRODUCT_TYPE") %>'></asp:Label>

                </ItemTemplate>
                <HeaderTemplate>
                    Prodcu Type
                </HeaderTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="ITlblAgeBand" runat="server" Text='<%# Eval("PRODUCT_NAME") %>' CssClass="label grdof" ToolTip='<%# Eval("PRODUCT_NAME") %>'></asp:Label>

                </ItemTemplate>
                <HeaderTemplate>
                    Prodcut Name
                </HeaderTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="ITlblAgeBand" runat="server" Text='<%# Eval("RANGE_DESCRIPTION") %>' CssClass="label grdof" ToolTip='<%# Eval("RANGE_DESCRIPTION") %>'></asp:Label>

                </ItemTemplate>
                <HeaderTemplate>
                    Range Description
                </HeaderTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="ITlblAgeBand" runat="server" Text='<%# Eval("AGE_BAND") %>' CssClass="label grdof" ToolTip='<%# Eval("AGE_BAND") %>'></asp:Label>

                </ItemTemplate>
                <HeaderTemplate>
                    Age Band
                </HeaderTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="ITlblAgeBand" runat="server" Text='<%# Eval("TERM") %>' CssClass="label grdof" ToolTip='<%# Eval("TERM") %>'></asp:Label>

                </ItemTemplate>
                <HeaderTemplate>
                    Term
                </HeaderTemplate>
            </asp:TemplateField>
             <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="ITlblPlan" runat="server" Text='<%# Eval("PED") %>' CssClass="label grdof" ToolTip='<%# Eval("PED") %>'></asp:Label>

                </ItemTemplate>
                <HeaderTemplate>
                    Ped
                </HeaderTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <ItemTemplate>
                    <asp:Label ID="ITlblPlan" runat="server" Text='<%# Eval("PLANS") %>' CssClass="label grdof" ToolTip='<%# Eval("PLANS") %>'></asp:Label>

                </ItemTemplate>
                <HeaderTemplate>
                    Plan
                </HeaderTemplate>
            </asp:TemplateField>

        </Columns>

    </asp:GridView>


</asp:Content>


