<%@ Page MasterPageFile="~/TransactionBE.master" Language="C#" AutoEventWireup="true" Inherits="CorporateTripAnalysisUI" Codebehind="CorporateTripAnalysis.aspx.cs" Title="Corporate Trip Approval" %>

<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.Corporate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
<asp:HiddenField runat="server" ID="hdnHLevel" />

 <script type="text/javascript">

     function GetQueryStringByParameter(name) {
         name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
         var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
         return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
     }

     $(document).ready(function () {
         var flightId = GetQueryStringByParameter('flightId');
         $('#<%=hdfExpDetailId.ClientID %>').val(flightId);
         <%--document.getElementById(flightId).checked = true;
         var radios = document.getElementsByTagName('input');

         var Hlevel = document.getElementById('<%=hdnHLevel.ClientID %>').value;

         if (parseInt(Hlevel) > 1) {
             for (i = 0; i < radios.length; i++) {
                 if (radios[i].type == 'radio') {
                     radios[i].disabled = true;
                 }
             }
         }--%>

     });

    

     
 </script>


    <script type="text/javascript">
        function updateStatus() {
            var valid = false;
            <%if (detail.ProfileApproversList.Exists(x => x.ApproverId == user_corp_profile_id && x.ApprovalStatus == "Awaiting Approval"))
        {%>
            if ($('#hdnConfirmed').val() == 'no') {
                valid = confirmRequest('Approve');                
            }
            else
                valid = true;            
                document.getElementById('errMsg').style.display = "none";                
               <%}%>
            return valid;
        }

         function confirmRequest(action) {
            var valid = confirm('Are you sure you want to ' + action + '?');
            if (valid) {
                $('#hdnConfirmed').val('yes');
            }
            else {
                $('#hdnConfirmed').val('no');
             }
            return valid;
        }


        function displayRemarks() {
            var valid = true;
            document.getElementById('errMsg').style.display = "none";

            //var count = 0;
            //var radios = document.getElementsByTagName('input');
            //for (i = 0; i < radios.length; i++) {
            //    if (radios[i].type == 'radio' && radios[i].checked) {
            //        count++;
            //    }
            //}
            //if (count > 0) {
            //    valid = true;
                document.getElementById('remarksTable').style.display = "block";
                document.getElementById('btnReject1').style.display = "none";
            //}
            //else {
            //    document.getElementById('errMsg').style.display = "block";
            //    document.getElementById('errMsg').innerHTML = "Please Select any trip !";
            //}

            return valid;
        }


        function verifyRemarks() {
            document.getElementById('errMsg').style.display = "none";
            var valid = false;
            
                var count = 0;
                //var radios = document.getElementsByTagName('input');
                //for (i = 0; i < radios.length; i++) {
                //    if (radios[i].type == 'radio' && radios[i].checked) {
                //        count++;
                //    }
                //}
                //if (count == 0) {
                //    document.getElementById('errMsg').style.display = "block";
                //    document.getElementById('errMsg').innerHTML = "Please Select any trip !";
                //}

            if (document.getElementById('ctl00_cphTransaction_txtRemarks').value.length == 0) {
                document.getElementById('errMsg').style.display = "block";
                document.getElementById('errMsg').innerHTML = "Please enter remarks!";
            }

            else {
                if ($('#hdnConfirmed').val() == 'no') {
                    valid = confirmRequest('Reject');
                    
                }
                else
                    valid = true;                
            }
            
            return valid;
        }
        
        
        
        function processTrip(flightId) {
            
        }



    </script>
    <input type="hidden" id="hdnConfirmed" value="no" />
    <asp:HiddenField ID="hdfExpDetailId" runat="server" />
    <asp:HiddenField ID="hdnStatus" runat="server" Value="P"/>
    
    <asp:Label runat="server" ID="lblSuccessMsg"></asp:Label>
    <%detail = new CorporateProfileTripDetails(Convert.ToInt32(Request["flightId"]));
        List<FlightPolicy> flightPolicies = new List<FlightPolicy>();
        if (listFlightItinerary != null && listFlightItinerary.Count > 0)
        {
            flightItinerary = listFlightItinerary[0];
            int fc = 0;
            AgentAppConfig appConfig = new AgentAppConfig();
            appConfig.AgentID = Settings.LoginInfo.AgentId;
            appConfig.ProductID = 1;
            List<AgentAppConfig> appConfigs = appConfig.GetConfigData();
            double deadline = 5;//Default 5 Hours
            if (appConfigs.Count > 0)
            {
                AgentAppConfig config = appConfigs.Find(ac => ac.AppKey == "CorpApprovalDeadline");
                if (config != null)
                    deadline = Convert.ToDouble(config.AppValue);
            }

            lblSuccessMsg.Text = ""; %>
    
     <div class="body_container" >            
            <span class="preheader" style="color: #f3f3f3; display: none !important; font-size: 1px; line-height: 1px; max-height: 0px; max-width: 0px; mso-hide: all !important; opacity: 0; overflow: hidden; visibility: hidden"></span>
    <table class="body" style="Margin: 0; background: #fff !important; border-collapse: collapse; border-spacing: 0; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
        <tr style="padding: 0; text-align: left; vertical-align: top">
            <td class="center" align="center" valign="top" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                <center data-parsed="" style="min-width: 700px; width: 100%">
                    <link href="https://fonts.googleapis.com/css?family=Open+Sans" rel="stylesheet" align="center" class="float-center" id="lnkFontfs" />
                    <h2>Trip Analysis</h2>
                    <table align="center" class="container bc-bookinginfo-wrapper float-center" style="Margin: 0 auto; background: #f7f7f7; border: solid #d0cfcf 1px; border-collapse: collapse; border-spacing: 0; float: none; margin: 0 auto; padding: 0; text-align: center; vertical-align: top; width: 700px">
                        <tbody>
                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">

                                    <table class="main-header" width="100%" cellspacing="0" cellpadding="4" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                        <tbody>
                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <strong style="font-weight: 600">Traveler:</strong>
                                                </td>
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">                                                  
                                                 <%=flightItinerary.CreatedOn.ToString("dd MMM yyyy") %> Trip <%=status + " - "%>  <%=flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName + " - "%> <%=flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName %>                                                  
                                                </td>
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    

                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>

                                    <table width="100%" cellspacing="0" cellpadding="4" border="0" class="table-bg-color" style="background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                        <tbody>
                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table>
                                                    <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                        <tbody>


                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Trip Reference:</strong>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=flightItinerary.TripId %>
                                                                </td>
                                                            </tr>
                                                           
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Employee ID</strong>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=detail.EmpId %>
                                                                </td>
                                                            </tr>
                                                            <%string rejectReason = CorpProfileApproval.GetRejectionReason(flightItinerary.FlightId);
                                                                if (status == "Rejected" && !string.IsNullOrEmpty(rejectReason))
                                                                { %>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Rejection Reason:</strong>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%= rejectReason%>
                                                                </td>
                                                            </tr>
                                                            <%} %>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Approval Deadline</strong>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=DateTime.Now.AddHours(Convert.ToDouble(deadline)).ToString("dd-MMM-yyyy hh:mm:ss tt") %>
                                                                </td>
                                                            </tr>
                                                            <%string leadApproverStatus = string.Empty;
                                                                string email = string.Empty, approverStatus = string.Empty,approverId=string.Empty;
                                                                foreach (FlightItinerary itinerary in listFlightItinerary)
                                                                {
                                                                    FlightPolicy flPolicy = new FlightPolicy();
                                                                    flPolicy.Flightid = itinerary.FlightId;
                                                                    flPolicy.GetPolicyByFlightId();
                                                                    if (!flPolicy.SelectedTrip)
                                                                    { continue; }
                                                                    CorporateProfileTripDetails tripDetails = new CorporateProfileTripDetails(itinerary.FlightId);
                                                                    List<CorpProfileApproval> approvalsCount = CorpProfileApproval.RemoveDuplicatesIterative(tripDetails.ProfileApproversList);
                                                                    for (int i = 0; i < approvalsCount.Count; i++)
                                                                    {

                                                                        string appstatus = approvalsCount[i].ApprovalStatus;
                                                                        //If approver profileid and traveller profile id is same then do not show Approver label
                                                                        if (!string.IsNullOrEmpty(approvalsCount[i].ApproverEmail) && approvalsCount[i].Hierarchy > 0 && !email.Contains(approvalsCount[i].ApproverEmail))
                                                                        {
                                                                            approverId += approverId.Length > 0 ? "~" + approvalsCount[i].ApproverId : approvalsCount[i].ApproverId.ToString();
                                                                            email += email.Length > 1 ? "~" + approvalsCount[i].ApproverEmail : approvalsCount[i].ApproverEmail;
                                                                            approverStatus += approverStatus.Length > 1 ? "~" + appstatus : appstatus;
                                                                            leadApproverStatus += leadApproverStatus.Length > 1 ? "~" + appstatus + "-" + approvalsCount[i].Hierarchy : appstatus + "-" + approvalsCount[i].Hierarchy;
                                                                        }
                                                                    }

                                                                    //for (int i = 0; i < approvalsCount.Count; i++)
                                                                    //{
                                                                    //    string appstatus = approvalsCount[i].ApprovalStatus;
                                                                    //    //If approver profileid and traveller profile id is same then do not show Approver label
                                                                    //    if (!string.IsNullOrEmpty(approvalsCount[i].ApproverEmail) && appstatus != "Rejected" && !approverId.Contains(approvalsCount[i].ApproverId.ToString()))
                                                                    //    {
                                                                    //        approverId += approverId.Length > 0 ? "," + approvalsCount[i].ApproverId : approvalsCount[i].ApproverId.ToString();
                                                                    //        email += email.Length > 1 ? "," + approvalsCount[i].ApproverEmail : approvalsCount[i].ApproverEmail;
                                                                    //        approverStatus += approverStatus.Length > 1 ? "," + appstatus : appstatus;
                                                                    //    }
                                                                    //}
                                                                }%>
                                                         
                                                               <%if (!string.IsNullOrEmpty(email))
    {
        for (int i = 0; i < email.Split('~').Length; i++)
        {
            string appemail = email.Split('~')[i];
            string appstatus = approverStatus.Split('~')[i];%>
                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                        <strong style="font-weight: 600">Approver <%=i + 1 %></strong>
                                                                    </td>
                                                                    <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                        <%=appemail%>&nbsp;&nbsp;&nbsp;&nbsp; <span style="color:green">Status: <strong><%=appstatus%></strong></span>
                                                                    </td>
                                                                </tr>                                                               
                                                               
                                                                <%}
                                                                    }
                                                                    //break;


                                                                    string fallBackApprover = string.Empty;
                                                                    List<CorpProfileApproval> approvalCount = CorpProfileApproval.RemoveDuplicatesIterative(detail.ProfileApproversList);
                                                                    List<CorpProfileApproval> fallBackApprovers = detail.ProfileApproversList.Except(approvalCount).ToList();
                                                                    fallBackApprovers = (from r in fallBackApprovers orderby r.Hierarchy select r).ToList();
                                                                    int c = 0;
                                                                    foreach (CorpProfileApproval corpProfile in fallBackApprovers)
                                                                    {
                                                                        fallBackApprover = corpProfile.ApproverEmail;
                                                                    %>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Fall Back Approver <%=corpProfile.Hierarchy %> - <%=(c+1) %></strong>
                                                                </td>
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <%=fallBackApprover %>&nbsp;&nbsp;&nbsp;&nbsp; <span style="color:green">Status: <strong><%= leadApproverStatus.Split('~').ToList().Find(x=>x.Split('-')[1] == corpProfile.Hierarchy.ToString()).Split('-')[0]%></strong></span>
                                                                </td>
                                                            </tr>
                                                            <%c++;
                                                                } %>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <hr size="1" />
                                                                </td>
                                                            </tr>


                                                        </tbody>
                                                    </table>
                                                    <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                        <tbody>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td colspan="1" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <span class="image-label" style="background-color: #656565; color: #fff; font-size: 11px; font-weight: bold; padding: 1px 6px; text-transform: uppercase">Trip Information</span>
                                                                </td>                                                                                                                                
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: green; font-family: 'Open Sans', Arial, sans-serif; font-size: 16px; font-weight: bold; ">                                                                    
                                                                
                                                                </td>
                                                            </tr>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td height="5" colspan="2" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word"></td>
                                                            </tr>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">
                                                                        Reason for Travel:
                                                                        <%
                                                                            FlightPolicy flightPolicy = new FlightPolicy();
                                                                            flightPolicy.Flightid = flightItinerary.FlightId;

                                                                            flightPolicy.GetPolicyByFlightId();
                                                                            flightPolicies.Add(flightPolicy);

                                                                            CorporateTravelReason travelReason = new CorporateTravelReason();
                                                                            if (flightPolicies.Count > 0)
                                                                                travelReason = new CorporateTravelReason(flightPolicies[0].TravelReasonId); %>
                                                                    </strong><%=string.IsNullOrEmpty(travelReason.Description) ? "" : travelReason.Description %>

                                                                </td>
                                                            </tr>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">
                                                                        Company:
                                                                    </strong><%=Settings.LoginInfo.AgentName %>

                                                                </td>
                                                            </tr>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">
                                                                        Policy Compliance:
                                                                    </strong><%=flightPolicies.Count > 0 ? flightPolicies[0].IsUnderPolicy ? "Inside Policy" : "Outside Policy - " + flightPolicies[0].PolicyBreakingRules : "" %>

                                                                </td>
                                                            </tr>
                                                            <%CorporateTravelReason policyReason = new CorporateTravelReason();
                                                                if (flightPolicies.Count > 0) policyReason = new CorporateTravelReason(flightPolicies[0].PolicyReasonId);
                                                                if (flightPolicies.Count > 0 && !flightPolicies[0].IsUnderPolicy)
                                                                { %>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">
                                                                        Violation Reason:                                                                        
                                                                    </strong> <%= !string.IsNullOrEmpty(policyReason.Description) ? policyReason.Description : string.Empty %>
                                                                </td>
                                                            </tr>
                                                            <%} %>
                                                        </tbody>
                                                    </table>
                                                    <% foreach (FlightItinerary fItinerary in listFlightItinerary)
                                                        {
                                                            FlightPolicy policy = new FlightPolicy();
                                                             policy.Flightid = fItinerary.FlightId;
                                                             policy.GetPolicyByFlightId();
                                                            string appStatus = FlightPolicy.GetTripApprovalStatus(fItinerary.FlightId, (int)ProductType.Flight);
                                                            if (!string.IsNullOrEmpty(appStatus))
                                                            {

                                                                switch (appStatus)
                                                                {
                                                                    case "A":
                                                                        status = "Approved";
                                                                        break;
                                                                    case "P":
                                                                        status = "Pending";
                                                                        break;
                                                                    case "R":
                                                                        status = "Rejected";
                                                                        break;
                                                                    default:
                                                                        status = "Ticketed";
                                                                        break;
                                                                }
                                                            }


                                                            if (fc == 0)
                                                            { %>
            <div class="showMsgHeading" style="color:white;font-size:16px;width:100%;" >                  
                <div class="col-md-12" style="text-align:right">
                <div class="col-md-4" style="text-align:left">
                    <span>Employee Selected</span>                    
                   &nbsp; PNR : <%=fItinerary.PNR %>
                </div>
                <div class="col-md-4" style="font-weight: 600;font-size:16px">
             <%if (status == "Pending")
    {
        bool selected = false;
        if (Request["appLoginName"] != null && Request["flightId"] != null && Convert.ToInt32(Request["flightId"]) == fItinerary.FlightId)
            selected = true;
        else if (fc == 0)
            selected = true;%>
                                                                 <span>   <input type="radio" id="chkOption<%=fc %>"  name="FlightAction" <%=selected ? "checked=\"checked\"" : "" %> onchange="SelectFlight('<%=fc %>')"/> <label for="chkOption<%=fc %>">  Select for Approval </label></span>
                                                                    <%}
    else
    { %><span>Approval Status : <%=status %></span>
                    <%} %>
                    </div>
                    <div class="col-md-4" style="text-align:right">
                         Policy Compliance : <strong style="font-weight: 600"><%=policy.IsUnderPolicy ? "Inside Policy":"Outside Policy" %></strong>
                     </div>
                    </div>
                <%}
                    else
                    { %>
                <br />
        <div class="ns-h3" style="color:white;font-size:16px;">
             <div class="col-md-12" style="text-align:right">
                <div class="col-md-4" style="text-align:left">
                    <span >Option <%=fc %></span>                 
                   &nbsp; PNR : <%=fItinerary.PNR %>
                </div>
                <div class="col-md-4" style="font-weight: 600;font-size:16px">
             <%if (status == "Pending")
    {
        bool selected = false;
        if (Request["appLoginName"] != null && Request["flightId"] != null && Convert.ToInt32(Request["flightId"]) == fItinerary.FlightId)
            selected = true;
        else if (fc == 0)
            selected = true;%>
                                                                   <span> <input type="radio" id="chkOption<%=fc %>"  name="FlightAction" <%=selected ? "checked=\"checked\"" : "" %> onchange="SelectFlight('<%=fc %>')"/> <label  for="chkOption<%=fc %>">  Select for Approval </label></span>
                                                                    <%}
    else
    { %>
                    <span>Approval Status : <%=status %></span>
                    <%} %>
                    </div>
                 <div class="col-md-4" style="text-align:right">
                         Policy Compliance : <strong style="font-weight: 600"><%=policy.IsUnderPolicy ? "Inside Policy":"Outside Policy" %></strong>
                     </div>
                    </div>
                
            
                    <%} %>
           
            <input type="hidden" id="hdnFlightId<%=fc %>" value="<%=fItinerary.FlightId %>" />
                                                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table>
                                                    <table class="flight-table" style="border-bottom: 1px solid #e4e4e4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%">
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left">Airline</th>
                                                            <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left">Departure</th>
                                                            <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left">Arrival</th>
                                                            <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left">Stops</th>
                                                            <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left">Duration</th>
                                                            <th style="Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left">Price</th>
                                                        </tr>
                                                        <% string currency = fItinerary.Passenger[0].Price.Currency;
                                                            int decimalPoint = fItinerary.Passenger[0].Price.DecimalPoint;
                                                            List<string> meals = new List<string>();
                                                            List<decimal> mealPrices = new List<decimal>();
                                                            List<string> baggages = new List<string>();
                                                            List<decimal> baggagePrices = new List<decimal>();
                                                            List<string> seats = new List<string>();
                                                            List<decimal> seatPrices = new List<decimal>();
                                                            int segments = fItinerary.Segments.Length;
                                                            segments += 5;//For SSR values

                                                            fItinerary.Passenger.ToList().ForEach(p =>
                                                            {
                                                                string meal = string.Empty, seat = string.Empty, bag = string.Empty;
                                                                for (int i = 0; i < fItinerary.Segments.Length; i++)
                                                                {
                                                                    meal += meal.Length > 0 ? ", No Meal Booked" : " No Meal Booked";
                                                                    seat += seat.Length > 0 ? ", No Seat Booked" : " No Seat Booked";
                                                                    bag += bag.Length > 0 ? ", No Bag" : " No Bag";
                                                                }
                                                                meals.Add(string.IsNullOrEmpty(p.MealDesc) ? meal : p.MealDesc);
                                                                mealPrices.Add(p.Price.MealCharge);
                                                                baggages.Add(string.IsNullOrEmpty(p.BaggageCode) ? bag : p.BaggageCode);
                                                                baggagePrices.Add(p.Price.BaggageCharge);
                                                                seats.Add(string.IsNullOrEmpty(p.Seat.Code) ? seat : p.Seat.Code);
                                                                seatPrices.Add(p.Price.SeatPrice);
                                                            });
                                                            for (int i = 0; i < fItinerary.Segments.Length; i++)
                                                            {
                                                                FlightInfo flight = fItinerary.Segments[i];%>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                <%Airline airline = new Airline(); airline.Load(flight.Airline); %><%=airline.AirlineName %><br /><%=flight.Airline %> <%=flight.FlightNumber %>
                                                            </td>
                                                            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                <strong><%=flight.Origin.CityName %></strong><br /><%=flight.DepartureTime.ToString("dd MMM yyyy") %><br /><%=flight.DepartureTime.ToString("(ddd), hh:mm tt") %>,<br />Airport:<%=flight.Origin.AirportName %>, Terminal: <%=flight.DepTerminal %>
                                                            </td>
                                                            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                <strong><%=flight.Destination.CityName %></strong><br /><%=flight.ArrivalTime.ToString("dd MMM yyyy") %><br /><%=flight.ArrivalTime.ToString("(ddd), hh:mm tt") %>,<br />Airport:<%=flight.Destination.AirportName %>, Terminal: <%=flight.ArrTerminal %>
                                                            </td>
                                                            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word"><%=flight.Stops == 0 ? "Non Stop" : flight.Stops == 1 ? "One Stop" : "Two Stops" %></td>
                                                            <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word"><%=flight.Duration %></td>
                                                            <%if (i == 0)
                                                                { %>
                                                            <td rowspan="<%=segments %>" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; border-left: 1px solid #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: center; vertical-align: middle; word-wrap: break-word">
                                                                <strong><%=currency %> <%=fItinerary.Passenger.ToList().Sum(p => p.Price.PublishedFare + p.Price.Tax + p.Price.Markup + p.Price.BaggageCharge + p.Price.OtherCharges + p.Price.HandlingFeeAmount + p.Price.OutputVATAmount +
                                                                                                                                                p.Price.MealCharge + p.Price.SeatPrice + p.Price.AsvAmount - p.Price.Discount).ToString("N" + decimalPoint) %></strong></td>
                                                            <%} %>
                                                        </tr>                                                        
                                                        <tr>
                                                            <td colspan="5" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top;width:100%">
                                                                        <tbody>
                                                                            
                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                <%if (mealPrices.Sum() > 0)
                                                                                    {
                                                                                        for (int j = 0; j < meals.Count; j++)
                                                                                        { %>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <strong style="font-weight: 600">
                                                                                        Meal:
                                                                                    </strong> <%=meals[j] %> [<%=currency %> <%=mealPrices[j].ToString("N" + decimalPoint) %>]
                                                                                </td>
                                                                                <%}
                                                                                    } %>
                                                                            </tr>
                                                                            
                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                <%if (seatPrices.Sum() > 0)
                                                                                    {
                                                                                        for (int k = 0; k < seats.Count; k++)
                                                                                        { %>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <strong style="font-weight: 600">
                                                                                        Seat:
                                                                                    </strong> <%=seats[k] %>  [<%=currency %> <%=seatPrices[k].ToString("N" + decimalPoint) %>]
                                                                                </td>
                                                                                <%}
                                                                                    } %>
                                                                            </tr>
                                                                            
                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                <%if (baggagePrices.Sum() > 0)
                                                                                    {
                                                                                        for (int l = 0; l < baggages.Count; l++)
                                                                                        { %>
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <strong style="font-weight: 600">
                                                                                        Additional Baggage
                                                                                    </strong> <%=baggages[l] %>   [<%=currency %> <%=baggagePrices[l].ToString("N" + decimalPoint) %>]
                                                                                </td>
                                                                                <%}
                                                                                    } %>
                                                                            </tr>

                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="2px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 2px; font-weight: normal; hyphens: auto; line-height: 2px; margin: 0; mso-line-height-rule: exactly; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table>
                                                                                </td>
                                                                            </tr>

                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table>
                                                                                </td>
                                                                            </tr>

                                                                        </tbody>
                                                                    </table>
                                                                </td>
                                                        </tr>
                                                        <%} %>
                                                    </table>
                                                    <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%"><tbody><tr style="padding: 0; text-align: left; vertical-align: top"><td height="10px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">&#xA0;</td></tr></tbody></table>
                                                    <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                        <tbody>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word">

                                                                    <table class="table-main-content" width="100%" cellspacing="0" cellpadding="0" border="0" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                                        <tbody>

                                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: bottom; word-wrap: break-word">
                                                                                    <strong style="font-weight: 600;color:white;">
                                                                                        Total:
                                                                                    </strong>
                                                                                    <strong style="font-size: 14px; font-weight: 600;color:white;">  <%=currency %>  <%=fItinerary.Passenger.ToList().Sum(p => p.Price.PublishedFare + p.Price.Tax + p.Price.Markup + p.Price.BaggageCharge + p.Price.OtherCharges + p.Price.HandlingFeeAmount + p.Price.OutputVATAmount +
                                                                                                p.Price.MealCharge + p.Price.SeatPrice + p.Price.AsvAmount - p.Price.Discount).ToString("N" + decimalPoint) %></strong>

                                                                                </td>
                                                                            </tr>

                                                                        </tbody>
                                                                    </table>

                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>

            </div>
              <%   fc++;
                  }%>



                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <%if (flightItinerary.Passenger[0].FlexDetailsList != null && flightItinerary.Passenger[0].FlexDetailsList.Count > 0)
                                        { %>
                                    <table class="" width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                        <tbody>
                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                <td colspan="1" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <table class="main-header" width="100%" border="0" cellspacing="0" cellpadding="4" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                        <tbody>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">Reporting Fields:</strong>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="bottom-table-style" width="100%" border="0" cellspacing="0" cellpadding="2" style="background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                        <tbody>
                                            <%foreach (FlightFlexDetails flex in flightItinerary.Passenger[0].FlexDetailsList)
                                                { %>
                                            <tr class="table-cnt-inner" style="padding: 0; text-align: left; vertical-align: top">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <%=flex.FlexLabel %>:
                                                </td>
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <%=flex.FlexData %>
                                                </td>
                                            </tr>
                                            <%} %>
                                        </tbody>
                                    </table>
                                    <%} %>

                                    
                                      <table class="" width="100%" border="0" cellspacing="0" cellpadding="2" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                        <tbody>
                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                <td colspan="1" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    <table class="main-header" width="100%" border="0" cellspacing="0" cellpadding="4" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                                        <tbody>
                                                            <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                    <strong style="font-weight: 600">PNR:</strong>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                    <table class="bottom-table-style" width="100%" border="0" cellspacing="0" cellpadding="2" style="background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                        <tbody>
                                           
                                            <tr class="table-cnt-inner" style="padding: 0; text-align: left; vertical-align: top">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    PNR : <%=flightItinerary.PNR %>
                                                </td>
                                            </tr>
                                            
                                            <%if (flightItinerary.IsLCC && flightItinerary.AirLocatorCode != null)
                                                {%>
                                            <tr class="table-cnt-inner" style="padding: 0; text-align: left; vertical-align: top">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    GDS PNR : <%=flightItinerary.AirLocatorCode %>
                                                </td>
                                            </tr>
                                            <%}%>
                                        </tbody>
                                    </table>
                                    <table class="bottom-table-style" width="100%" border="0" cellspacing="0" cellpadding="2" style="background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top">
                                        <tbody>
                                    <tr class="table-cnt-inner" style="padding: 0; text-align: left; vertical-align: top">
                                                <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word">
                                                    Created On : <%=flightItinerary.CreatedOn.ToString() %> Created By :<%CT.TicketReceipt.BusinessLayer.UserMaster user = new UserMaster(flightItinerary.CreatedBy); %> <%= user.LoginName %>
                                                </td>
                                            </tr>
                                            </tbody>
                                         </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </center>
            </td>
        </tr>
    </table>
       
         
    <div class="clearfix"></div> 
                   </div>
                                      
                    
        
    <%}
        else
        {            
            int flightId = !string.IsNullOrEmpty(hdfExpDetailId.Value) ? Convert.ToInt32(hdfExpDetailId.Value): Convert.ToInt32(Request["flightid"]);
            CorporateProfileTripDetails details = new CorporateProfileTripDetails(flightId);
            CorpProfileApproval approval = details.ProfileApproversList.ToList().Find(i => i.ApproverId == user_corp_profile_id);
            if (approval != null && lblSuccessMsg.Text.Length <= 0)
            {
                btnApprove.Visible = false;
                btnReject.Visible = false;
                if (approval.ApprovalStatus == "Approved")
                {%>
     <span style="color:green">You have <strong>Approved</strong> this Trip</span>
    <%}
        if (approval.ApprovalStatus == "Rejected")
        {
            btnApprove.Visible = false;
            btnReject.Visible = false;%>
    <span style="color:red">You have <strong>Rejected</strong> this Trip</span>
        <%}
                }
                else if (lblSuccessMsg.Text.Contains("Trip Rejected") || lblSuccessMsg.Text.Contains("Trip Approved"))
                {
                    btnApprove.Visible = false;
                    btnReject.Visible = false;
                }
            }%>
    

    <div>
    <input type="button" id="btnReject1" onclick="return displayRemarks()" class="btn but_d btn_xs_block cursor_point pull-right mar-5" value="Reject" />
     
        <asp:Button Text="APPROVE" OnClientClick="return updateStatus();" runat="server" ID="btnApprove"
            CssClass="btn but_d btn_xs_block cursor_point pull-right mar-5" OnClick="btnApprove_Click" />
        <div class="clearfix">
        </div>
        
        <div id="errMsg" style="color:red"></div>
        <table id="remarksTable" style="display:none;">
       <tr>
       <td>
      Please enter the reason for rejection
       </td>
       <td>
       <asp:TextBox TextMode="MultiLine" Columns="40" runat="server" ID="txtRemarks">
       
       </asp:TextBox>
       </td>
       <td>
       <asp:Button Text="REJECT" OnClientClick="return verifyRemarks()" runat="server" ID="btnReject"
            CssClass="btn but_d btn_xs_block cursor_point pull-right mar-5" OnClick="btnReject_Click" />
       
       </td>
       
       
       </tr>
        
       
        </table>
        
        
    </div>
   <script>
        
          <%if (Request["bookingType"] != null)
        {%>
       $(document).ready(function () {
            $('span').removeClass('glyphicon glyphicon-arrow-down');            
        });
            <%}if(Request.QueryString["appStatus"] != null) {
           if (needToApprove)
           {%>
       //alert('need to approve');
        $('#btnReject1').hide();
        $('#<%=btnApprove.ClientID%>').click();
        <%}
       else
       {%>
        $('#btnReject1').click();
        <%}
       }%>

        function SelectFlight(id) {
            var flightId = $('#hdnFlightId' + id).val();
            $('#<%=hdfExpDetailId.ClientID %>').val(flightId);
        }
        function HideButtons() {
            <% bool isPending = false;
        foreach (FlightItinerary f in listFlightItinerary) {
           CorporateProfileTripDetails tripDetails = new CorporateProfileTripDetails(f.FlightId);
            string appStatus = tripDetails.ProfileApproversList.Find(x=>x.ApproverId == user_corp_profile_id).ApprovalStatus;
            if (!string.IsNullOrEmpty(appStatus))
            {
                if (appStatus == "Awaiting Approval")
                {
                    isPending = true;
                    break;
                }
            }
        }%>

           if (!<%=isPending.ToString().ToLower()%>) {
               $('#btnReject1').hide();
               $('#ctl00_cphTransaction_btnApprove').hide();
           }
           else if(<%=isPending.ToString().ToLower()%>){
               $('#btnReject1').show();
               $('#ctl00_cphTransaction_btnApprove').show();
           }
       }
       window.onload = HideButtons();
        
         (function($) {             
              SelectFlight('0');
             
         }(jQuery));

        
    </script>
</asp:Content>
