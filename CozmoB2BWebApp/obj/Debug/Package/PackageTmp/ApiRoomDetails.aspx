﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ApiRoomDetails.aspx.cs" Inherits="CozmoB2BWebApp.ApiRoomDetails" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">


    <%--   <script type="text/javascript" src="Scripts/Menu/query2.js"></script>--%>
    <!-- New BE js -->



    <!-- end new Js -->

    <!-- end -->

    <!-- jQuery -->
    <!-- Bootstrap Core JavaScript -->
<%--    <link href="App_Themes/Default/BookingStyle.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/cozmovisa-style.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/Default.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/main-style.css" type="text/css" rel="stylesheet" />
    <link href="App_Themes/Default/style.css" type="text/css" rel="stylesheet" />--%>

    <!-- Required styles for Hotel Listing-->
    <script src="scripts/LINQ_JS_MIN.js"></script>
    <link href="build/css/royalslider.css" rel="stylesheet" type="text/css" />
    <link href="build/css/rs-default.css" rel="stylesheet" type="text/css" />
    <style>
        .room-number-label {
            position: absolute;
            background-color: #000000;
            font-size: 11px;
            text-transform: uppercase;
            padding: 5px 4px 4px 5px;
            top: 0;
            left: 0;
            color: #fff;
            font-weight: bold;
            z-index: 100;
            display: block;
        }

        .rate-breakup-div {
            position: absolute;
            right: 0px;
            width: 430px;
            margin-top: 10px;
            z-index: 10000;
            background-color: #fff;
        }

        .rate-breakup-close {
            top: 0px;
            right: 5px;
            color: #000;
            font-size: 16px;
        }

        .Ruleshdr { 
            float:left;font-style:normal;	
            background:#dadada;color:black;padding:1px 2px 0 2px;
            margin-top:-1px;margin-left:0;margin-right:5px;border-radius:3px; 
        }

        .Rulesdtl { border:1px solid #18407B;margin-right:10px;padding:10px; }
        .divdisable { pointer-events:none;color:lightgray; }
         .isPackage-label{
            position: absolute;
            right: -9px;
            top: 0;
            background-color: #fedf00;
            color: #9b8a0d;
            text-transform: uppercase;
            font-size: 10px;
            font-weight: bold;
            padding: 2px 7px;
       }
       .isPackage-label:before{
            content: '';
            position: absolute;
            right: 0;
            bottom: -8px;
            width: 0;
            height: 0;
            border-top: 8px solid #c4ae0e;
            border-right: 9px solid transparent;
       }

    </style>
    <input type="hidden" id="hdnObjRequest" runat="server" />
    <input type="hidden" id="hdnUserId" runat="server" />
    <input type="hidden" id="hdnAgentId" runat="server" />
    <input type="hidden" id="hdnBehalfLocation" runat="server" />
    <input type="hidden" id="hdnHotelCode" runat="server" />
    <input type="hidden" id="hdnSessionId" runat="server" />
    <input type="hidden" id="hdnRoomTypeCode" runat="server" />
    <input type="hidden" id="hdnRoomResult" runat="server" />
    <input type="hidden" id="hdnIsAgent" runat="server" /><!-- to restrict Supplire name 0=b2b|b2b2b , 1 =agent|base -->
    <input type="hidden" id="hdnisCorporate" runat="server" />
    <input type="hidden" id="hdnSelectRoomId" runat="server" value="0" />   
    <input type="hidden" id="hdnTokenId" runat="server" />
    <input type="hidden" id="hdnSourceId" runat="server" />
    <div>


        <div class="ui-listing-page loading-progress htldetail-page">

            <div class="results-list-wrapper pt-3">
                <div class="row custom-gutter">
                    <div class="col-12 col-lg-8">
                        <div class="ui-bg-wrapper h-100">
                            <h3 id="h3HotelName"></h3>
                            <p class="text-address mb-4" id="pAddress"></p>

                            <!--Gallery-->
                            <div class="royalSlider rsUni hotel-detail-slider zoom-gallery" id="imgGallery">
                            </div>

                        </div>

                    </div>
                    <div class="col-12 col-lg-4" id="hotelpriceinfowrapper">
                        <div class="hotel-price-info-wrapper h-100 bg-white">
                            <div class="header d-none">SELECTED ROOM/S</div>
                            <div class="ui-hotel-map">
                                <button type="button" class="btn" data-toggle="modal" data-target="#hotelMap">
                                    <span class="icon icon-map-pin"></span>VIEW MAP
                                </button>
                            </div>

                         <%--   <div class="ui-hotel-review ui-bg-wrapper mb-2" style="display: none">
                                <div class="row custom-gutter review-rating">
                                    <div class="col-4">
                                        <span class="rate primary-bgcolor">3.5</span>
                                    </div>
                                    <div class="col-8 text-right">
                                        <span class="review-word primary-color">Excellent</span>
                                        <p>Based on 1988 Reviews</p>
                                        <p>Reviews by<img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo"></p>
                                    </div>
                                </div>
                            </div>--%>

                            <div class="ui-bg-wrapper room-selection-details p-2">
                            

                                    <%--For room summary--%>
                                    <ul class="ui-listing-wrapper list-unstyled" id="list-group-roomsummary">
                                    </ul>
                                    <%--End -  For room summary--%>
                                    <div class="row">
                                    <div class="col-md-12 mt-2">
                                        <div class="row hotel-bookingdtl-dateinfo checkInInfo">
                                            <div class="col-md-12">
                                                <span class="float-left text-gray-light"><i class="icon icon-calendar"></i>Check-in</span>
                                                <span class="float-right text-gray-dark text-weight-400" id="startDate"></span>
                                            </div>
                                            <div class="col-md-12">
                                                <span class="float-left text-gray-light"><i class="icon icon-calendar"></i>Check-out</span>
                                                <span class="float-right text-gray-dark text-weight-400" id="endDate"></span>
                                            </div>

                                        </div>
                                    </div>
                                    <div class="col-md-12 mt-2">
                                        <div class="row no-gutter hotel-bookingdtl-price mb-3 py-3">
                                            <div class="col-sm-8 col-lg-12 col-xl-6">
                                                <span class="text pt-3" id="spanTotalPrice"></span>
                                            </div>
                                            <div class="col-sm-4 col-lg-12 col-xl-6">
                                                <div class="ui-list-price text-right">
                                                    <span class="price d-inline-block d-sm-block strikeout" id="beforeTotalPrice"></span>
                                                    <span class="price d-inline-block d-sm-block" id="TotalPrice"></span>
                                                </div>
                                            </div>
                                        </div>
                                        <div id="dvTaxDetails" style="display:none">
                                             <%--For tax breakup summary--%>
                                    <ul class="baggage-wrapper small mt-3 pb-3" id="list-group-taxsummary" style="display:none;">
                                    </ul>
                                     <div class="baggage-wrapper small mt-3 pb-3" id="taxsummaryListWrapper" style="display:none;">
                                         <ul id="list-group-taxsummary-Excl"> </ul>
                                         <div id="totPayatHotel"></div>
                                     </div>
                                    
                                            <%--  template room summary--%>
                                    <div id="templateTaxSummary" style="display: none;">
        
                                        <span class="text pt-3 d-flex" id="spanTaxBreakup" ></span>
        
                                    </div>
                                   <div id="templateTaxSummaryExcl" style="display: none;">
        
                                        <span class="text pt-3 d-flex" id="spanTaxBreakupExcl"></span>
        
                                    </div>
                                    <%--End -  For tax breakup--%>
                                            
                                        </div>
                                        <div class="row no-gutter">
                                       
                                        


                                            <div class="col-md-6"> 
                                                <a href="javascript:void(0);" class="btn btn-link p-0" onclick="window.location.href='ApiHotelResults.aspx'">Check other Hotel rooms</a>

                                        </div>

                                        <div class="col-md-2"> 

    <div id="DivInOut"  class="dropdown flightResultDropdown" style="display:none">
                                                      <button type="button" class="corp-icon-wrapper" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                          <%--<span id="SpanInOut" runat="server" class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>--%>
                                                        <%--  <asp:Image ID="ImgInOutPolicy1"/>--%>
                                                        <image id="ImgInOutPolicy"/>
                                                      </button>
                                                  
                                              </div>

                                        </div>
                                        
                                        <div class="col-md-4 text-right book-btn-wrapper">

                                            <input type="button" class="btn btn-primary font-weight-bold" id="btnBookNow" onclick="BookRoom();" value="BOOK NOW" />
                                        </div>
                                    </div>
                                     <div class="col-sm-8 col-lg-12 col-xl-6">
                                            <span class="text pt-3" id="spanBreakRules"></span>
                                        </div>
                                    <div>
                                        <ul id='divTravelReason' class="dropdown-menu" aria-labelledby="dLabel">
                                            </ul>
                                    </div>
                                </div>

                                    </div>
                              
                            </div>

                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
                <div class="row no-gutter loadingHide" style="display: none">
                    <div class="col">
                        <h3 class="pt-5">Room Choices</h3>
                        <%--For room type selection--%>
                        <ul class="ui-listing-wrapper list-unstyled" id="list-group-rooms">
                        </ul>
                    </div>
                </div>
                <%--  Hotel Over View--%>
                <div class="row custom-gutter loadingHide" style="display: none">
                    <div class="col-12">
                        <h3 class="pt-5 pb-3">Hotel Overview</h3>
                        <div class="ui-bg-wrapper">
                            <h6 class="pl-3 mt-3 mb-0 font-weight-bold">Amenities</h6>
                            <%--For Hotel Amenities--%>

                            <ul class="amenities-list" id="Amenities">
                            </ul>
                            <div class="clearfix"></div>
                            <h6 class="mt-3 mb-2 px-3 font-weight-bold">General Description</h6>
                            <div class="hotel-description px-3" id="HotelDescription">
                            </div>
                            <div class="hotel-description px-3" style="display: none">

                                <p>
                                    Check-in: From 03:00 PM<br>
                                    Check-out: Until 12:00 PM
                                </p>
                            </div>
                            <h6 class="mt-3 mb-2 px-3 font-weight-bold">Location</h6>
                            <div class="hotel-description px-3">
                                <p id="pLocation"></p>
                            </div>
                            <h6 class="mt-3 mb-2 px-3 font-weight-bold" id="h6Rooms" style="display: none">Rooms </h6>
                            <div class="hotel-description px-3" id="roomLevelAmenities">
                            </div>


                        </div>
                    </div>
                </div>
                <%--  End - Hotel  Over View--%>
                <%--  Hotel Reviews--%>
                <div class="row custom-gutter" style="display: none">
                    <div class="col-12 mb-5">
                        <h3 class="pt-5 pb-3 mb-2 heading-h3">Reviews by TrustYou</h3>
                        <div class="ui-bg-wrapper">
                            <div class="row custom-gutter top-reviews mt-2">
                                <div class="col-md-4 col-xs-12">
                                    <span class="excellent rating text-weight-500">3.5</span>
                                    <span class="category text-weight-500 pl-2">Excellent</span>
                                </div>
                                <div class="col-md-8 text-right">
                                    <p>
                                        Verified reviews by
								<%--<img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">--%>
                                    </p>
                                </div>
                            </div>
                            <div class="row mt-4 top-reviews">
                                <div class="col-md-7 py-4">
                                    <h3 class="heading-h3 mb-3">Top Review </h3>
                                    <p>
                                        <span class="icon-quotes-left"></span>stay in mercure is a nice experiance for me. my room always well cleaned by staff muhammad fezan, vinogen and supervisor Jagdish. <span class="icon-quotes-right"></span>
                                    </p>
                                    <hr>
                                    <p>
                                        <span class="icon-quotes-left"></span>was in Mercure room 1031, Suresa from concierge make us feel at home and wonderful stay and im flattered with Suresa service and kindness. I enjoyed Dubai very much, nice people nice food very good service <span class="icon-quotes-right"></span>
                                    </p>
                                </div>
                                <div class="col-md-1"></div>
                                <div class="col-md-4 badges  py-4">
                                    <h3 class="heading-h3 mb-3">Badges</h3>
                                    <div class="row">
                                        <div class="col-md-12 mb-3">
                                            <p class="text-weight-500"><span class="icon-medal badges-rating"></span>Excellent wellness hotel</p>
                                            <p>
                                                <span class="text-muted text-small">Top 7% in city </span>
                                            </p>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <p class="text-weight-500"><span class="icon-medal badges-rating"></span>Great Family Hotel</p>
                                            <p>
                                                <span class="text-muted text-small">Top 12% in city </span>
                                            </p>
                                        </div>
                                        <div class="col-md-12 mb-3">
                                            <p class="text-weight-500"><span class="icon-medal badges-rating"></span>Excellent business hotel</p>
                                            <p>
                                                <span class="text-muted text-small">Top 9% in city </span>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <%--  End - Hotel Reviews--%>
            </div>

        </div>


        <!-- Hotel Map -->
        <div class="modal fade" id="hotelMap" tabindex="-1" role="dialog" aria-labelledby="hotelMap" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered" role="document">
                <div class="modal-content">

                    <button type="button" class="modal-close" data-dismiss="modal" aria-label="Close">
                        <span class="icon icon-close"></span>
                    </button>
                    <div class="modal-body">
                        <div id="map" style="height: 500px;"></div>
                    </div>
                </div>
            </div>
        </div>

        <%-- Cancellation popup--%>
        <div class="modal fade" id="CancelPolicyModal1" role="dialog">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" id="btnModelClose1">&times;</button>
                        <h4 class="modal-title" id="headRoomTypeName1"></h4>
                    </div>
                    <div class="modal-body">
                        <p>
                            <b id="txtBold">Cancellation Policy :
                                <br />
                            </b>
                        </p>
                        <div class="hotel-description px-3" id="h2CnlPolicy1">
                        </div>
                        <%--<p id="h2CnlPolicy"></p>--%>
                    </div>
                    <div class="modal-footer" id="btnCancel1" style="display: none;">
                        <input type="button" class="btn btn-success" value="Select another Hotel" onclick="window.location.href = 'ApiHotelResults.aspx'" />
                        <input type="button" class="btn btn-success" value="Select Another Room" data-dismiss="modal" id="btnAnotherRoom1" />
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade in farerule-modal-style" data-backdrop="static" id="CancelPolicyModal" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" id="btnModelClose">&times;</button>
                        <h4 class="modal-title" id="headRoomTypeName"></h4>
                    </div>
                    <div class="modal-body">                                          
                        <div class="bg_white" id="h2CnlPolicy" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;"></div>
                        <%-- <p class="Ruleshdr" id="DisInfo">Disclaimer Information</p>
                        <div class="Rulesdtl" id="Disclaimer" style="padding:10px; width: 100%; font-size:13px;height:92%;overflow:auto;"></div>--%>
                    </div>
                    <div class="modal-footer" id="btnCancel" style="display: none;">
                        <input type="button" class="btn btn-success" value="Select another Hotel" onclick="window.location.href = 'ApiHotelResults.aspx'" />
                        <input type="button" class="btn btn-success" value="Select Another Room" data-dismiss="modal" id="btnAnotherRoom" />
                    </div>
                </div>
            </div>
        </div>


    </div>

    <%--  template room summary--%>
    <div id="templateRoomSummary" style="display: none;">
        <div class="mt-2">
            <h5 id="h5RoomName"></h5>
        </div>
        <div class="">
            <ul class="ui-hotel-addons-list">
                <li class="with-icon" id="mealPlanDesc"></li>
                <%--<li class="with-icon"  >Room only</li>
								<li class="with-icon">FREE Breakfast</li>
								<li class="with-icon">FREE Cancellation</li>--%>
            </ul>

        </div>
        <div class="mt-2">
            <div class="row hotel-bookingdtl-dateinfo">
                <div class="col-md-12">
                    <span class="float-left text-gray-light" id="roomGuests"><i class="icon icon-group"></i>Room Guests</span>
                    <span class="float-right text-gray-dark text-weight-400" id="roomGuestsCount">1 Room & 2 Adults</span>
                </div>
            </div>
        </div>
       <div class="mt-2">
            <div class="row hotel-bookingdtl-dateinfo">
                <div class="col-md-12" id="ChildAgeInfo">
                    <span class="float-left text-gray-light" id="ch"><i class="icon icon-group"></i>Child Age</span>
                    <span class="float-right text-gray-dark text-weight-400" id="ChildAge"></span>
                </div>
            </div>
        </div>
    </div>
    <%-- template for Rooms selection--%>
    <div id="templateRooms" style="display: none;">

        <div class="ui-room-details-table position-relative mt-4">
            <h5 class="room-number-label" id="h5RoomNumber"><span class="text"></span></h5>
            <div class="row no-gutters room-head d-none d-lg-flex">
                <div class="col-lg-6 room-col">Room type</div>
                <div class="col-lg-3 room-col">Options</div>
                <div class="col-lg-3 room-col" id="nightsAvgRate">Room Price</div>
            </div>
            <ul class="ui-listing-wrapper list-unstyled" id="list-group-roomtypes">
            </ul>
        </div>

    </div>
    <%-- template for Room types selection--%>
    <div class="row no-gutters room-body" id="templateRoomTypes" style="display: none;">
        <div class="col-12 col-lg-6 room-col">
            <%-- <img src="build/img/hotel-thumb-demo.jpg" alt="" class="hotel-image pr-3">--%>
            <div class="">
                <h4 id="h4RoomTypeName"></h4>
                <%-- <p id="pRoomType"></p>--%>
                <input type="button" class="btn btn-link p-0" id="roomTypeCancelPolicy" value="Cancellation Policy" />
                <%-- <a id="roomTypeCancelPolicy" class="small" href="#"></a>--%>
                <h5 id="BookingSource"></h5>
                 <h5 id="isPackage"></h5>
            </div>
        </div>
        <div class="col-6 col-lg-3 room-col">
            <ul class="ui-hotel-addons-list">
                <li id="roomTypeRefund"></li>
                <%-- <li class="with-icon">Special Deal</li>--%>
                <li class="with-icon" id="roomTypeMealPlan"></li>
            </ul>
        </div>
        <div class="col-6 col-lg-3 room-col">
            <div class="ui-list-price text-center position-relative">
                <span class="price d-inline-block d-sm-block" id="roomTypeAvgRate"></span>
                <select id="ddlRooms" class="ddlshow" style="display:none;"></select>
                <input type="button" class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0 select" id="btnSelect" value="SELECT" />

                <%-- <a tabindex="0"   class="price-breakup btn btn-link tooltip-btn position-absolute" role="button" 
                                       data-toggle="popover" data-trigger="focus" data-html="true" data-placement="bottom"  
                                       title="Price Breakup" data-content="<strong>And </strong>here's some amazing content. It's very engaging. Right?">
                                    <span class="icon icon-info"></span>
                                    </a>--%>

                <button class="price-breakup btn btn-link tooltip-btn position-absolute" type="button"
                    data-toggle="collapse" data-target="#collapseRateBreakup" aria-expanded="false"
                    aria-controls="collapseRateBreakup" id="btnRateBreakup" >
                    <span class="icon icon-info"></span>
                </button>
               
                <div class="collapse rate-breakup-div" id="collapseRateBreakup" >
                    <table class="table table-bordered table-condensed b2b-corp-table mb-0" id="tblRateBreakUp">
                        <tbody>
                            <tr>
                                <th colspan="8" class="showMsgHeading">Rate Breakup 
												<a class="rate-breakup-close position-absolute" href="javascript:void(0);"><span class="icon icon-close"></span></a>
                                </th>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                                <td><strong>Sun</strong></td>
                                <td><strong>Mon</strong></td>
                                <td><strong>Tue</strong></td>
                                <td><strong>Wed</strong></td>
                                <td><strong>Thu</strong></td>
                                <td><strong>Fri</strong></td>
                                <td><strong>Sat</strong></td>
                            </tr>
                            <%--  <tr>
                                    <td>Week 2</td>
                                    <td>131.38</td>
                                    <td>131.38</td>
                                    <td>131.38</td>
                                    <td>131.38</td>
                                    <td>131.38</td>
                                    <td>131.38</td>
                                    <td>131.38</td>
                                </tr>
                                <tr>
                                    <td colspan="8">Total : <b>4,467.07</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8">Extra Guest Charge : <b>0.00</b>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="8">Total Price : <b>4,467.07</b>
                                    </td>
                                </tr>--%>
                        </tbody>
                    </table>
                </div>

            </div>
             <div id="DivRoomInOut"  class="dropdown flightResultDropdown d-none">
                        
                                                  <button type="button" class="corp-icon-wrapper" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                      <%--<span id="SpanInOut" runat="server" class="glyphicon glyphicon-briefcase" aria-hidden="true"></span>--%>
                                                    <%--  <asp:Image ID="ImgInOutPolicy1"/>--%>
                                                    <image id="ImgRoomInOutPolicy"/>
                                                  </button>
                                                  
                                              </div>
             <div>
                                            <span class="text pt-3" id="spanRoomBreakRules"></span>
                                        </div>


        </div>
    </div>

    <script src="build/js/jquery.royalslider.min.js"></script>
    <script src="build/js/jquery.magnific-popup.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB_RGvSBnxHX_IPvIrlEg1TGy3YaIwvwMk&sensor=true" type="text/javascript"></script>
    <script type="text/javascript" src="Scripts/Jquery/popup_window.js"></script>
    <%--<script src="<%=Request.Url.Scheme%>://maps.googleapis.com/maps/api/js?key=AIzaSyCtd7RZ0Mbe3DTvLjqvbQnsOGk37VhPjI8&libraries=places&callback=initMap" async defer></script>--%>

    <script type="text/javascript">            
        var request;
        var userId;
        var agentId;
        var behalfLocation;
        var decimalValue;
        //var index;
        //var totFare;
        var hotelCode;
        var sessionId;
        var DataObject;
        var resultObj;
        var lstImages;
        var TypeCode;//RoomTypeCode is for getting cancellation policy when we click on Book now
        var lstRoomTypes, lstRoomTypeCodes;
        var isCorporate;
        var selectroomid;
        var firstRoomid;
        var secondRoomid;
        var thirdRoomid;
        var fourthRoomId;
        var TotalPax = 0; var TotalRooms = 0; var liOutPolicy = [];
        var selectRooms;
        var commonRoomKey;
        var region;
        var selectedSource;
        function pageLoad() {
            GetRoomDetails();
        }
              var Ajax;
           if (window.XMLHttpRequest) {
               Ajax = new XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }
        function GetRoomDetails() {
            request = JSON.parse(document.getElementById('<%=hdnObjRequest.ClientID %>').value);
            userId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
            agentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
            behalfLocation = JSON.parse(document.getElementById('<%=hdnBehalfLocation.ClientID %>').value);
               <%--  index = JSON.parse(document.getElementById('<%=hdnIndex.ClientID %>').value);
                 totFare = JSON.parse(document.getElementById('<%=hdnTotFare.ClientID %>').value);--%>

            var pageParams = JSON.parse(AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'ApiRoomDetails', 'sessionData':'', 'action':'get'}"));
            var sourceId = "";
            if (!IsEmpty(pageParams)) {
                hotelCode = !IsEmpty(pageParams.hotelCode) ? pageParams.hotelCode : "";
                sessionId = !IsEmpty(pageParams.sessionId) ? pageParams.sessionId : "";
                sourceId = !IsEmpty(pageParams.sourceId) ? pageParams.sourceId : "";
            }
           

            //hotelCode = JSON.parse(document.getElementById('<%=hdnHotelCode.ClientID %>').value);
            //sessionId = JSON.parse(document.getElementById('<%=hdnSessionId.ClientID %>').value);
            isCorporate = JSON.parse(document.getElementById('<%=hdnisCorporate.ClientID %>').value);
            //var sourceId = JSON.parse(document.getElementById('<%=hdnSourceId.ClientID %>').value);
            var data = { request: request, userId: userId, agentId: agentId, behalfLocation: behalfLocation, hotelCode: hotelCode, sessionId: sessionId, sourceId: sourceId };
            var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';
            if (apiUrl == null || apiUrl == '') {
                    apiUrl = DynamicAPIURL();<%--'<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';--%>
                }
            $.ajax({
                url: apiUrl + '/api/HotelRooms/GetRoomsResult',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(data),
                 beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', "Bearer " + JSON.parse(document.getElementById('<%=hdnTokenId.ClientID %>').value));
                    },
                success: function (data) {

                    DataObject = JSON.parse(data);
                    resultObj = DataObject.ResultObj;
                    document.getElementById('<%=hdnRoomResult.ClientID %>').value = resultObj;
                    decimalValue = DataObject.decimalValue;
                    if (resultObj !=null && resultObj.RoomDetails !=null && resultObj.RoomDetails.length > 0)               
                    {
                        LoadRoomDetails(resultObj);
                        if (isCorporate == 'Y') {
                            //updateHotelDetails('save');// For storing hotel result object  in Session
                            }
                    }
                    else {
                        var msg = "No Room(s) is/are avaliable for selected Hotel, please Select another Hotel !";
                        $("#txtBold").text("");
                        $("#btnModelClose").hide();
                        $("#btnAnotherRoom").hide();
                        $("#btnCancel").show();
                        $("#headRoomTypeName").text("");
                        $("#h2CnlPolicy").text(msg);
                        $("#CancelPolicyModal").modal("show");
                    }
                },
                complete: function () {
                    //Done with Loading
                    NProgress.done();
                },
                error: function (xhr, textStatus, errorThrown) {

                    // alert('Error in Operation');
                }
            });
        }
        // main Rooms load function
        function LoadRoomDetails(hotelResult) {

            $('#h3HotelName').text(hotelResult.HotelName);
            $('#pAddress').text(hotelResult.HotelAddress);
            hotelResult.HotelMap!=null?initialize(hotelResult.HotelMap):"";
            LoadImages(hotelResult);
            LoadRoomSummary(hotelResult);
            LoadRoomTypes(hotelResult);
            LoadHotelOverview(hotelResult);

        }
        //for load image urls
        function LoadImages(hotelResult) {

            lstImages = new Array();
            var roomDetails = hotelResult.RoomDetails;
            var noofRooms = hotelResult.RoomGuest;
            if (roomDetails != null && roomDetails.length > 0 && noofRooms != null && noofRooms.length > 0) {
                for (var i = 0; i < roomDetails.length; i++) {
                    var lstImgs = roomDetails[i].Images;
                    if (lstImgs != null && lstImgs.length > 0) {
                        for (var j = 0; j < lstImgs.length; j++) {
                            if (imgUrlContains(lstImages, lstImgs[j]))
                                lstImages.push(lstImgs[j]);
                        }
                    }
                }
            }
            if (lstImages == null || lstImages.length < 1) {
                if (hotelResult.HotelImages != null && hotelResult.HotelImages.length > 0) {
                    for (var k = 0; k < hotelResult.HotelImages.length; k++) {
                        lstImages.push(hotelResult.HotelImages[k]);
                    }
                }
            }

            var images = "";
            for (var l = 0; l < lstImages.length; l++) {
                images += '<a class="rsImg" data-rsbigimg="build" href="' + lstImages[l] + '" data-rsw="700" data-rsh="300"><img width="96" height="72" class="rsTmb" src="' + lstImages[l] + '" /></a>';
            }

          
            $('#imgGallery').html(images);             
            //CAROUSEL HOTEL GALLERY	
            var sliderOptions = {
                controlNavigation: 'thumbnails',
                thumbs: {
                    orientation: 'vertical',
                    paddingBottom: 4,
                    appendSpan: true
                },
                transitionType: 'move',
                autoScaleSlider: true,
                autoScaleSliderWidth: 800,
                autoScaleSliderHeight: 400,
                loop: true,
                arrowsNav: true,
                keyboardNavEnabled: true,
                navigateByClick: true,
                imageScaleMode: 'fill'

            };

            $('#imgGallery').royalSlider(sliderOptions);

            
          
        }
        //Find to duplecate Room level image urls
        function imgUrlContains(arr, element) {
            for (var c = 0; c < arr.length; c++) {
                if (arr[c] == element) {
                    return false;
                }
            }
            return true;
        }
        //Loading for map
        function initialize(Map) {

            var data = Map.split('||');
            myCenter = new google.maps.LatLng(data[0], data[1]);
            var mapProp = {
                center: myCenter,
                zoom: 18,
                panControl: true,
                zoomControl: true,
                mapTypeControl: true,
                scaleControl: true,
                streetViewControl: true,
                overviewMapControl: true,
                rotateControl: true,
                mapTypeId: google.maps.MapTypeId.ROADMAP
            };
            var map = new google.maps.Map(document.getElementById("map"), mapProp);


            marker = new google.maps.Marker({
                position: myCenter,
                animation: google.maps.Animation.BOUNCE
            });

            marker.setMap(map);
        }
        //for load Room(s) Detail summary
        function LoadRoomSummary(hotelResult) {

            $('#list-group-roomsummary').children().remove();
            var templateRoomSummary = $('#templateRoomSummary').html();
            $('#startDate').text(new Date(hotelResult.StartDate).toDateString());
            $('#endDate').text(new Date(hotelResult.EndDate).toDateString());

            $('#beforeTotalPrice').append('<del id="beforeTotalPriceDel"><em id="beforeTotalPriceCurrency" class="currency">' + hotelResult.Currency + ' </em>' + ' ' + Math.ceil(hotelResult.TotalPrice).toFixed(decimalValue)) + ' </del>';
            $('#TotalPrice').append('<em id="totalPriceCurrency" class="currency">' + hotelResult.Currency + ' </em>' + ' ' + Math.ceil(hotelResult.TotalPrice - hotelResult.Price.Discount).toFixed(decimalValue));
             if ( hotelResult.TotalPrice.toFixed(decimalValue) == (hotelResult.TotalPrice - hotelResult.Price.Discount).toFixed(decimalValue)) {
                $('#beforeTotalPrice').children().hide();
             }
             else {
                $('#beforeTotalPrice').children().show();
            }

            var noofAdults = 0;
            var noofChilds = 0;
            var noofRooms = hotelResult.RoomGuest;
            if (noofRooms != null && noofRooms.length > 0) {

                for (var m = 0; m < noofRooms.length; m++) {
                    $('#list-group-roomsummary').append('<li id="List' + m + '" class="item">' + templateRoomSummary + '</li>');
                    $('#h5RoomName').attr('id', 'h5RoomName-' + m);
                    $('#h5RoomName-' + m).text(hotelResult.RoomDetails[m].RoomTypeName);
                    $('#mealPlanDesc').attr('id', 'mealPlanDesc-' + m);
                    $('#mealPlanDesc-' + m).text(!IsEmpty(hotelResult.RoomDetails[m].mealPlanDesc) ? hotelResult.RoomDetails[m].mealPlanDesc : '');
                    $('#roomGuestsCount').attr('id', 'roomGuestsCount-' + m);
                    $('#roomGuestsCount-' + m).text(hotelResult.RoomGuest[m].noOfAdults + " Adult(s) & " + hotelResult.RoomGuest[m].noOfChild + " Child(s)");
                    noofAdults += hotelResult.RoomGuest[m].noOfAdults;
                    noofChilds += hotelResult.RoomGuest[m].noOfChild;
                    TotalPax += noofAdults + noofChilds;
                    //Display ChildAge 
                    $('#List' + m).find('#ChildAgeInfo').attr('id', 'ChildAgeInfo-' + m);
                    if (hotelResult.RoomGuest[m].noOfChild != 0) {
                        $('#ChildAgeInfo-' + m).show();
                        $('#List' + m).find('#ChildAge').attr('id', 'ChildAge-' + m);
                        $('#ChildAge-' + m).text(hotelResult.RoomGuest[m].childAge + "Year(s)");
                    }
                    else {
                        $('#ChildAgeInfo-' + m).hide();
                    }
                    if (m == 0 && hotelResult.RoomDetails[0].TaxBreakups != null) {
                        var region = AjaxCall('ApiRoomDetails.aspx/GetRegion', "{'CountryName':'" + request.CountryName + "'}");
                        if (hotelResult.RoomDetails[0].TaxBreakups != null) {
                            $('#dvTaxDetails').show();
                            BindTaxBreakUps(hotelResult.RoomDetails[0].TaxBreakups, hotelResult.Currency, region);
                        }
                        else {
                            $('#dvTaxDetails').hide();
                            }
                    }
                }
            }
            $('#spanTotalPrice').text("");
            $('#spanTotalPrice').text("Total price for " + noofAdults + " Adult(s), " + noofChilds + " Child(s)");
            
            
        }

        function BindTaxBreakUps(TaxBreakups, Currency, region) {
            $('#list-group-taxsummary').children().remove();
              $('#list-group-taxsummary-Excl').children().remove();
            var templateTaxSummary = $('#templateTaxSummary').html();
            var templateTaxSummaryExcl = $('#templateTaxSummaryExcl').html();
            //  var groupTaxes = [];
            var TaxBreakups = JSON.parse(AjaxCall('ApiRoomDetails.aspx/GetGroupTaxes', "{'taxLists':'" + JSON.stringify(TaxBreakups) + "'}"));
            $('#list-group-taxsummary').append('<li class="item mb-2"><u><strong>Tax Details</strong></u></li>');
            $('#list-group-taxsummary-Excl').append('<li class="item mb-2"><u><strong>Due @ Hotel (Pay @ Hotel) </strong></u></li>');
            var totIncl = 0;
            $('#totPayatHotel').hide();
            $('#taxsummaryListWrapper').hide();
            //$('#list-group-taxsummary-Excl').hide();
             $('#list-group-taxsummary').hide();
            for (var k = 0; k < TaxBreakups.length; k++) {
                var breakups = TaxBreakups[k];
                if (breakups.IsIncluded == true) {
                      $('#list-group-taxsummary').show();
                    $('#list-group-taxsummary').append('<li id="ListTax' + k + '" class="item mb-1">' + templateTaxSummary + '</li>');

                    $('#spanTaxBreakup').attr('id', 'spanTaxBreakup-' + k);
                    //$('#spanTaxBreakup-' + k).html("<span class='flex-fill'>" + ((breakups.TaxTitle == "tax_and_service_fee") && region == 'EU' ? "Tax Recover Charges" : (breakups.TaxTitle == "tax_and_service_fee" ? "Tax Recovery Charges & ServiceFee" : breakups.TaxTitle.replace(/_/g, " "))) + " -" + breakups.UnitType + "-Incl :</span><span><strong>" + Currency + " " + breakups.Value.toFixed(decimalValue) + "</strong> <br>/ " + breakups.FrequencyType + "</span>");
                    $('#spanTaxBreakup-' + k).html("<span class='flex-fill'>" + ((breakups.TaxTitle == "tax_and_service_fee") && region == 'EU' ? "Tax Recover Charges" : (breakups.TaxTitle == "tax_and_service_fee" ? "Tax Recovery Charges & ServiceFee" : breakups.TaxTitle.replace(/_/g, " "))) + " -" + breakups.UnitType + "-Incl :</span><span><strong>" + Currency + " " + breakups.Value.toFixed(decimalValue) + "</strong> </span>");
                } else {
                    $('#totPayatHotel').show();
                    $('#taxsummaryListWrapper').show();
                    // Excluded Tax
                    $('#list-group-taxsummary-Excl').append('<li id="ListTax' + k + '" class="item mb-1">' + templateTaxSummaryExcl + '</li>');
                    $('#spanTaxBreakupExcl').attr('id', 'spanTaxBreakupExcl-' + k);
                    //$('#spanTaxBreakupExcl-' + k).html("<span class='flex-fill'>" + ((breakups.TaxTitle == "tax_and_service_fee") && region == 'EU' ? "Tax Recover Charges" : (breakups.TaxTitle == "tax_and_service_fee" ? "Tax Recovery Charges & ServiceFee" : breakups.TaxTitle.replace(/_/g, " "))) + "-" + breakups.UnitType + "-Excl (Pay @ Hotel) :</span><span>" + " <strong>" + Currency + " " + breakups.Value.toFixed(decimalValue) + "</strong><br>/ " + breakups.FrequencyType + "</span>");
                    $('#spanTaxBreakupExcl-' + k).html("<span class='flex-fill'>" + ((breakups.TaxTitle == "tax_and_service_fee") && region == 'EU' ? "Tax Recover Charges" : (breakups.TaxTitle == "tax_and_service_fee" ? "Tax Recovery Charges & ServiceFee" : breakups.TaxTitle.replace(/_/g, " "))) + "-" + breakups.UnitType + "-Excl (Pay @ Hotel) :</span><span>" + " <strong>" + Currency + " " + breakups.Value.toFixed(decimalValue) + "</strong></span>");
                     totIncl += parseFloat(breakups.Value);
                }
            }
            $('#totPayatHotel').html("<div class='d-flex mt-2'><span class='flex-fill'>Total (pay @ Hotel) :</span><span><strong>" + Currency + " " + totIncl.toFixed(decimalValue) + "</strong></span></div>");
            
       }
           
        // for load Room types (Room Details)
        function LoadRoomTypes(hotelResult) {

            //var  noofNights= parseInt((new Date(hotelResult.EndDate) - new Date(hotelResult.StartDate)) / (1000 * 60 * 60 * 24));
            lstRoomTypes = [];
            lstRoomTypeCodes = [];
            //var roomDetails = hotelResult.RoomDetails;
            var noofRooms = hotelResult.RoomGuest;
            if (hotelResult.RoomDetails != null && hotelResult.RoomDetails.length > 0) {
                TypeCode = hotelResult.RoomDetails[0].RoomTypeCode;
                commonRoomKey = hotelResult.RoomDetails[0].CommonRoomKey;
                for (var n = 0; n < hotelResult.RoomDetails.length; n++) {
                    if (hotelResult.RoomDetails[n].SupplierId!='23'&&hotelResult.RoomDetails[n].IsIndividualSelection == false && roomTypeContains(lstRoomTypeCodes, hotelResult.RoomDetails[n].RoomTypeCode.split("||")[1])) {
                        lstRoomTypeCodes.push(hotelResult.RoomDetails[n].RoomTypeCode.split("||")[1]);                  
                    }
                    if (hotelResult.RoomDetails[n].SupplierId=='23'||hotelResult.RoomDetails[n].IsIndividualSelection ==true)
                    {
                         lstRoomTypeCodes.push(hotelResult.RoomDetails[n].RoomTypeCode);
                    }
                          lstRoomTypes.push(hotelResult.RoomDetails[n]);
                }
                // if (lstRoomTypes[0].IsIndividualSelection) { 
                sourceWiseSelectedRooms(lstRoomTypes[0].SupplierName);
               
                //}
            }

            if (lstRoomTypes != null && lstRoomTypes.length > 0) {
                $('#list-group-roomtype').children().remove();
                var templateRooms = $('#templateRooms').html();

                for (var p = 0; p < noofRooms.length; p++) {
                    $('#list-group-rooms').append('<li id="List' + p + '" class="">' + templateRooms + '</li>');
                    $('#h5RoomNumber').attr('id', 'h5RoomNumber-' + p);
                    $('#h5RoomNumber-' + p).text("Room - " + (p + 1));

                    //$('#nightsAvgRate').attr('id', 'nightsAvgRate-' + p);
                    //$('#nightsAvgRate-' + p).text("PRICE FOR " + noofNights +" NIGHT(S)");

                    var templateRoomTypes = $('#templateRoomTypes').html();
                    $('#list-group-rooms').find('#list-group-roomtypes').attr('id', 'list-group-roomtypes-' + p);
                    for (var q = 0; q < lstRoomTypes.length; q++) {
                         if (lstRoomTypes[q].SequenceNo!=p + 1 && lstRoomTypes[q].SupplierId!='23') {
                              continue;
                        }
                        $('#list-group-rooms').find('#list-group-roomtypes-' + p).append('<li id="ListTypes' + p + q + '" class="row no-gutters room-body">' + templateRoomTypes + '</li>');

                        //Room Type
                        $('#h4RoomTypeName').attr('id', 'h4RoomTypeName-' + p + q);
                        $('#h4RoomTypeName-' + p + q).text(lstRoomTypes[q].RoomTypeName);
                        //$('#pRoomType').attr('id', 'pRoomType-' + p + q);
                        //$('#pRoomType-' + p + q).text(lstRoomTypes[q].mealPlanDesc);
                        //cancel policy link -- To Do                             
                        $('#roomTypeCancelPolicy').attr('id', 'roomTypeCancelPolicy-' + p + q);
                        var RoomTypName = lstRoomTypes[q].RoomTypeName.replace(/(\r\n|\n|\r)/gm, "");
                        $('#roomTypeCancelPolicy-' + p + q).attr("onClick", "GetRoomCancelPolicy('" + lstRoomTypes[q].RoomTypeCode + "','" + RoomTypName + "','Cancel','"+ eval(p+1) +"','"+lstRoomTypes[q].SupplierName+"')");
                       
                        $('#BookingSource').attr('id', 'BookingSource-' + p + q);  // for Booking source
                         $('#isPackage').attr('id', 'isPackage-' + p + q);
                        if (lstRoomTypes[q].SupplierName == "GIMMONIX" && (lstRoomTypes[q].RoomTypeCode.split('||')[3].includes('EPS_') )) {
                            var EpsSupplier = lstRoomTypes[q].RoomTypeCode.split('||')[3];                                 
                            $('#isPackage-' + p + q).addClass('isPackage').append('<div class="isPackage-label">' + (EpsSupplier=='EPS_Package' ? 'Package' : (EpsSupplier == 'EPS_Memberonly' ? 'Loyalty' : 'Hotel only'))+'</div>');
                        }
                        if (lstRoomTypes[q].SupplierName == "Illusions" && lstRoomTypes[q].DynamicInventory == "Y") {
                            $('#isPackage-' + p + q).addClass('isPackage').append('<div class="isPackage-label">DynamicInventory</div>');
                        }
                        if (document.getElementById('<%=hdnIsAgent.ClientID %>').value == "1") {
                           
 
                            $('#BookingSource-' + p + q).text("Source : " + (lstRoomTypes[q].SupplierName=="GIMMONIX"?lstRoomTypes[q].RoomTypeCode.split('||')[3]:lstRoomTypes[q].SupplierName));
                        }
                        //Options

                        $('#roomTypeRefund').attr('id', 'roomTypeRefund-' + p + q);
                        if (lstRoomTypes[q].RoomTypeName.split('-')[1] == "unknown") {
                             $('#roomTypeRefund-' + p + q).text('Nonrefundable');
                        }
                        else {
                             $('#roomTypeRefund-' + p + q).text(lstRoomTypes[q].RoomTypeName.split('-')[1]);
                        }
                       
                        $('#roomTypeMealPlan').attr('id', 'roomTypeMealPlan-' + p + q);
                        $('#roomTypeMealPlan-' + p + q).text(!IsEmpty(lstRoomTypes[q].mealPlanDesc) ? lstRoomTypes[q].mealPlanDesc : '');
                        // Special Deal  -- To Do

                        //Avg per night
                        $('#roomTypeAvgRate').attr('id', 'roomTypeAvgRate-' + p + q);
                        $('#roomTypeAvgRate-' + p + q).append('<em class="currency">' + hotelResult.Currency + ' </em>' + ' ' + parseFloat(lstRoomTypes[q].TotalPrice - lstRoomTypes[q].Discount).toFixed(decimalValue));
                        //var selectedRT = JSON.parse(lstRoomTypes[q]);

                        /* To enable no of rooms selection for UAPI hotels supplier*/
                        if (resultObj.BookingSource == '23') {
                            
                            $('#ddlRooms').attr('id', 'ddlRooms-' + p + q); document.getElementById('ddlRooms-' + p + q).innerHTML = Createrooms(TotalPax);
                            $('#ddlRooms-' + p + q).attr('onchange', "NoOfRoomschange('" + hotelResult.Currency + "','" + q + "', this)");
                        }

                        $('#btnSelect').attr('id', 'btnSelect-' + p + q);
                        $('#btnSelect-' + p + q).attr("onClick", "SelectRoomOption('" + hotelResult.Currency + "','" + lstRoomTypes[q].RoomTypeCode + "','" + q + "','" + noofRooms.length + "',this.id,'"+eval(p+1)+"','"+lstRoomTypes[q].IsIndividualSelection+"','"+lstRoomTypes[q].CommonRoomKey+"')");                       
                        //if (lstRoomTypes[q].IsIndividualSelection) {
                            q == firstRoomid ? $('#btnSelect-' + p + firstRoomid).prop({'disabled': true,'value':'SELECTED'}) : q == secondRoomid ? $('#btnSelect-' + p + secondRoomid).prop({'disabled': true,'value':'SELECTED'}) : q == thirdRoomid ? $('#btnSelect-' + p + thirdRoomid).prop({'disabled': true,'value':'SELECTED'}) : q == fourthRoomId ? $('#btnSelect-' + p + fourthRoomId).prop({'disabled': true,'value':'SELECTED'}):'';
                       // }                       
                        //For corporate policy , Displaying IN/Out policy Icon
                        if (isCorporate == 'Y') {
                            $('#DivRoomInOut').removeClass('d-none');
                            $('#DivRoomInOut').attr('id', 'DivRoomInOut-' + p + q);
                            $('#ImgRoomInOutPolicy').attr('id', 'ImgRoomInOutPolicy-' + p + q);
                            $('#spanRoomBreakRules').attr('id', 'spanRoomBreakRules-' + p + q);
                            document.getElementById("DivInOut").style.display = "none";
                            if (lstRoomTypes[q].TravelPolicyResult != null && (lstRoomTypes[q].TravelPolicyResult.IsUnderPolicy == false)) {
                                if (document.getElementById("DivRoomInOut-" + p + q) != null) {
                                    //for breakingrules 
                                    var PolicyBreakingRules = lstRoomTypes[q].TravelPolicyResult.PolicyBreakingRules;
                                    var BreakingRules = '';
                                    $('#DivRoomInOut-' + p + q).attr('title', "Out Side the policy");
                                    $('#ImgRoomInOutPolicy-' + p + q).attr("src", "/images/icon-outside-policy.png");
                                    $('#ImgRoomInOutPolicy-' + p + q).attr("alt", "Outside Policy");
                                    document.getElementById('DivRoomInOut-' + p + q).style.display = "block";
                                    $.each( PolicyBreakingRules, function( key, value ) {
                                        
                                        BreakingRules += BreakingRules == '' ? value : ' ,' + value;                                        
                                    });
                           
                                    $('#spanRoomBreakRules-' + p + q).text("");
                                    $('#spanRoomBreakRules-' + p + q).text(BreakingRules);
                                }
                            }
                            else {
                                if (document.getElementById("DivRoomInOut-" + p + q) != null) {
                                    $('#DivRoomInOut-' + p + q).attr('title', "InSide the policy ");
                                    $('#ImgRoomInOutPolicy-' + p + q).attr("src", "/images/icon-inside-policy.png");
                                    $('#ImgRoomInOutPolicy-' + p + q).attr("alt", "Inside Policy");
                                    document.getElementById('DivRoomInOut-' + p + q).style.display = "block";
                                    
                                     
                                         $('#spanRoomBreakRules-' + p + q).text("");
                        $('#spanRoomBreakRules-' + p + q).text("In Policy");
                                }
                            }
                        }

                        
                        // for  RateBreakup
                        $('#collapseRateBreakup').attr('id', 'collapseRateBreakup-' + p + q);
                        $('#btnRateBreakup').attr('data-target', '#collapseRateBreakup-' + p + q);
                        $('#btnRateBreakup').attr('id', 'btnRateBreakup-' + p + q);

                        $('#tblRateBreakUp').attr('id', 'tblRateBreakUp-' + p + q);
                        rows = tblContent(parseFloat(lstRoomTypes[q].TotalPrice - lstRoomTypes[q].Discount).toFixed(decimalValue), lstRoomTypes[q].RoomTypeCode);
                        $('#tblRateBreakUp-' + p + q).append(rows);
                        $(".htldetail-page").removeClass("loading-progress");
                        $(".loadingHide").show();
                    }

                    if (isCorporate == 'Y') { // Display Corporate Policy In/Out Icon for Booking Details Section
                        if (lstRoomTypes[0].TravelPolicyResult != null && (lstRoomTypes[0].TravelPolicyResult.IsUnderPolicy == false)) {
                             //for breakingrules 
                             var  PolicyBreakingRules = lstRoomTypes[0].TravelPolicyResult.PolicyBreakingRules;
                                    var BreakingRules = '';
                            if (document.getElementById("DivInOut") != null) {
                                $('#DivInOut').attr('title', "Out Side the policy -Booking Amount ");
                                $('#ImgInOutPolicy').attr("src", "/images/icon-outside-policy.png");
                                $('#ImgInOutPolicy').attr("alt", "Outside Policy");
                                document.getElementById('DivInOut').style.display = "block";
                                 $.each( PolicyBreakingRules, function( key, value ) {
                                        BreakingRules += BreakingRules == '' ? value : ' ,' + value;
                                    });
                           $('#spanBreakRules').text("");
                        $('#spanBreakRules').text(BreakingRules);
                            }
                        }
                        else {
                            if (document.getElementById("DivInOut") != null) {
                                $('#DivInOut').attr('title', "InSide the policy ");
                                $('#ImgInOutPolicy').attr("src", "/images/icon-inside-policy.png");
                                $('#ImgInOutPolicy').attr("alt", "Inside Policy");
                                document.getElementById('DivInOut').style.display = "block";
                                   $('#spanBreakRules').text("");
                        $('#spanBreakRules').text("In Policy");
                            }
                        }
                    }
                }

            }
            /* To create and show bootstrap drop down for rooms drop down */
            if (resultObj.BookingSource == '23') {

                $('select').select2();
                noofRooms = TotalRooms = resultObj.RoomGuest.length;           
                for (var q = 0; q < lstRoomTypes.length; q++) {

                    $('#s2id_ddlRooms-0' + q).attr('style', '');

                    /* Validate and store the list of rooms which has out of policy to avoid API call on room change for UAPI hotel */
                    if (lstRoomTypes[q].TravelPolicyResult != null && lstRoomTypes[q].TravelPolicyResult.IsUnderPolicy == false)
                        liOutPolicy.push(q);
                }
                $('#s2id_ddlRooms-00').addClass('divdisable');
            }
        }
        function sourceWiseSelectedRooms(sourceName) {
            selectedSource = sourceName;
            for (var p = 0; p < request.NoOfRooms; p++) {
                 for (var q = 0; q < lstRoomTypes.length; q++) {
                     if (lstRoomTypes[q].SequenceNo == p + 1 && lstRoomTypes[q].SupplierName == sourceName) {
                                 if (p == 0) {
                                     firstRoomid = q;
                                     break;
                                 }
                                 if (p == 1) {
                                     secondRoomid = q;
                                     break;
                                 }
                                 if (p == 2) {
                                     thirdRoomid = q;
                                     break;
                                 }
                                 if (p == 3) {
                                     fourthRoomId = q;
                                     break;
                                 }                          
                             }
                           
                        }
                    }
        }

        /* To append no of rooms options based on the no of total pax */
        function Createrooms(count) {

            var html = '<option selected="selected" value="1">1</option>';
            for (var i = 2; i <= count; i++) {

                html += '<option value="' + i + '">' + i + '</option>';
            }
            return html;
        }

        //rate breake up table content

        // Create Dynamic Table rows of Rate Breakup
        function tblContent(totalPrice, roomtypecode) {

            var roomtypes = lstRoomTypes;
            var room = roomtypes.filter(function (item) {
                return (item.RoomTypeCode == roomtypecode);
            });
            var rows;
            var tr;
            var weekdays = new Array("Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday");
            var d1 = new Date(request.StartDate);
            var d2 = new Date(request.EndDate);
            d2.setDate(d2.getDate() - 1);
            // var Difftime = d2.getTime() - d1.getTime();
            var result = 0;
            // Get first full week
            var firstDayOfFirstFullWeek = d1;
            if (weekdays[d1.getDay()] != "Sunday") {
                result += 1;
                firstDayOfFirstFullWeek = d1.setDate(d1.getDate() + (7 - (d1.getDay())));
            }
            // Get last full week
            var lastDayOfLastFullWeek = d2;
            if (weekdays[d2.getDay()] != "Saturday") {
                result += 1;
                lastDayOfLastFullWeek = d2.setDate(d2.getDate() + (-(d2.getDay()) - 1));
            }
            var Difftime = lastDayOfLastFullWeek - firstDayOfFirstFullWeek;
            var diffResult = (Difftime / (1000 * 60 * 60 * 24));

            // Add number of full weeks
            result += parseInt((diffResult + 1) / 7);
            d1 = new Date(request.StartDate);
            d2 = new Date(request.EndDate);
            //      var days = parseInt((new Date(d2) - new Date(d1)) / (1000 * 60 * 60 * 24));

            var totalRoomSellRate = 0;
            var Count = result;
            for (var i = 0, j = 0; i < result; i++) {
                var week = i + 1;
                tr += "<tr>";
                tr += "<td >Week " + week + "</td>";
                var l = 0;
                if (weekdays[d1.getDay()] != "Sunday") {
                    var predeser = d1.getDay();
                    for (; l < predeser; l++) {
                        tr += "<td>&nbsp;</td>";
                    }
                }
                var rmInfo = room[0].Rates;
                if (i == Count--) {
                    d2.setDate(d2.getDate() - 1);
                }
                var k = 0;
                for (; l < 7 && d1 <= d2; l++) {



                    if (rmInfo.length > k) {
                        var rate = rmInfo[k].SellingFare;

                        tr += "<td>" + (parseFloat(totalPrice / rmInfo.length).toFixed(decimalValue)) + "</td>";
                        totalRoomSellRate += rate;
                    }
                    else {
                        tr += "<td>&nbsp;</td>";
                    }
                    d1.setDate(d1.getDate() + 1);
                    k++;
                }
                tr += "</tr>";
            }

            rows = tr + "<tr><td colspan='8'>" + "Total : " + "<b>" + totalPrice + "</b> </td> </tr>"
               // + "<tr> <td colspan='8'>" + "Extra Guest Charge : " + "<b>" + "0.00" + "</b></td></tr>" // Temp Commented as vinay req
                + "<tr><td colspan='8'>" + "Total Price :" + "<b>" + totalPrice + "</b></td></tr>";

            return rows;

        }

        function roomTypeContains(arr, element) {
            for (var c = 0; c < arr.length; c++) {
                if (arr[c] === element) {
                    return false;
                }
            }
            return true;
        }

        /* To calculate the price based on no of rooms selected and validate corporate policy for UAPI hotel */
        function NoOfRoomschange(currency, id, cid) {

            try {
                
                var Total = Math.ceil(cid.value) * (parseFloat(lstRoomTypes[id].TotalPrice) - parseFloat(lstRoomTypes[id].Discount));
                document.getElementById(cid.id.replace('ddlRooms', 'roomTypeAvgRate')).innerHTML = '';
                $('#' + cid.id.replace('ddlRooms', 'roomTypeAvgRate')).append('<em class="currency">' + currency + ' </em>' + ' ' + parseFloat(Total).toFixed(decimalValue));

                /* Check corporate policy if total no of rooms is not equal to total out of policy rooms list */
                if (resultObj.BookingSource == '23' && !IsEmpty(request.Corptraveler) && !IsEmpty(request.Corptravelreason) &&
                    liOutPolicy.indexOf(id) == -1 && resultObj.RoomDetails.length != liOutPolicy.length) {

                    $("#ctl00_upProgress").show();                    
                    request.NoOfRooms = Math.ceil(cid.value);

                    var Inputdata = { request: request, resultObj: resultObj, roomTypeCode: lstRoomTypes[id].RoomTypeCode };

                    var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';

                    apiUrl = !IsEmpty(apiUrl) ? apiUrl : DynamicAPIURL();
                    var tokenId = document.getElementById('<%=hdnTokenId.ClientID %>').value;
                    var Webresult = WebAPICall(apiUrl + '/api/HotelRooms/GetTravelPolicy', Inputdata, 'POST',tokenId);

                    if (!IsEmpty(Webresult)) {

                        var DtlsRsp = JSON.parse(Webresult);
                        resultObj.RoomDetails[id].TravelPolicyResult = !IsEmpty(DtlsRsp.ResultObj) ?
                            DtlsRsp.ResultObj.RoomDetails[id].TravelPolicyResult : resultObj.RoomDetails[id].TravelPolicyResult;
                        SetCorpPolicy(id, cid.id.replace('ddlRooms-', ''));
                    }                                       
                        
                    request.NoOfRooms = 1;
                    $("#ctl00_upProgress").hide();
                }
            }
            catch (exception) {
                var ex = exception;
            }
        }

        /* To set corporate policy */
        function SetCorpPolicy(id, cntrlid) {                                    
            
            if (lstRoomTypes[id].TravelPolicyResult != null && (lstRoomTypes[id].TravelPolicyResult.IsUnderPolicy == false)) {

                if (document.getElementById("DivRoomInOut-" + cntrlid) != null) {
                    
                    var PolicyBreakingRules = lstRoomTypes[id].TravelPolicyResult.PolicyBreakingRules;
                    var BreakingRules = '';

                    /* Set selected room policy */
                    $('#DivRoomInOut-' + cntrlid).attr('title', "Out Side the policy -Booking Amount ");
                    $('#ImgRoomInOutPolicy-' + cntrlid).attr("src", "/images/icon-outside-policy.png");
                    $('#ImgRoomInOutPolicy-' + cntrlid).attr("alt", "Outside Policy");
                    document.getElementById('DivRoomInOut-' + cntrlid).style.display = "block";

                    $.each(PolicyBreakingRules, function (key, value) {

                        BreakingRules += BreakingRules == '' ? value : ' ,' + value;
                    });

                    $('#spanRoomBreakRules-' + cntrlid).text("");
                    $('#spanRoomBreakRules-' + cntrlid).text(BreakingRules);

                    /* Populate selected room policy at room summary level */
                    if (document.getElementById("DivInOut") != null) {

                        $('#DivInOut').attr('title', "Out Side the policy -Booking Amount ");
                        $('#ImgInOutPolicy').attr("src", "/images/icon-outside-policy.png");
                        $('#ImgInOutPolicy').attr("alt", "Outside Policy");
                        document.getElementById('DivInOut').style.display = "block";
                        $('#spanBreakRules').text("");
                        $('#spanBreakRules').text(BreakingRules);
                    }
                }
            }
            else {

                /* Set selected room policy */
                if (document.getElementById("DivRoomInOut-" + cntrlid) != null) {

                    $('#DivRoomInOut-' + cntrlid).attr('title', "InSide the policy ");
                    $('#ImgRoomInOutPolicy-' + cntrlid).attr("src", "/images/icon-inside-policy.png");
                    $('#ImgRoomInOutPolicy-' + cntrlid).attr("alt", "Inside Policy");
                    document.getElementById('DivRoomInOut-' + cntrlid).style.display = "block";
                    $('#spanRoomBreakRules-' + cntrlid).text("");
                    $('#spanRoomBreakRules-' + cntrlid).text("In Policy");
                }

                /* Populate selected room policy at room summary level */
                if (document.getElementById("DivInOut") != null) {

                    $('#DivInOut').attr('title', "InSide the policy ");
                    $('#ImgInOutPolicy').attr("src", "/images/icon-inside-policy.png");
                    $('#ImgInOutPolicy').attr("alt", "Inside Policy");
                    document.getElementById('DivInOut').style.display = "block";
                    $('#spanBreakRules').text("");
                    $('#spanBreakRules').text("In Policy");
                }
            }                        
        }

        //Room type option selection
        function SelectRoomOption(currency, RoomTypeCode, id, noofRooms, cid, sequenceNo, isIndividualSelection, roomCommonKey) {

            selectroomid = document.getElementById('<%=hdnSelectRoomId.ClientID %>').value = id;

            if (resultObj.SimilarHotels != null && resultObj.SimilarHotels.length > 1 && selectedSource!=lstRoomTypes[id].SupplierName)
                sourceWiseSelectedRooms(lstRoomTypes[id].SupplierName);
            /* Set no of rooms and enable previous selected room drop down for UAPI hotels */            
            if (resultObj.BookingSource == '23') {
            
                $('.divdisable').removeClass('divdisable');
                TotalRooms = noofRooms = $('#' + cid.replace('btnSelect', 'ddlRooms')).val();                
                $('#s2id_ddlRooms-0' + id).addClass('divdisable');
            }                

            TypeCode = RoomTypeCode;
            commonRoomKey = roomCommonKey;
            var total = 0;
            var discount = 0;
            $('.ui-list-price .select').attr('disabled', false);
            $('.ui-list-price .select').val('SELECT');
            $('.room-body').removeClass('selected-room-div')
            var RoomId = '';
            for (var i = 0; i < lstRoomTypes.length; i++) {
                if (isIndividualSelection == "false") {
                    // RoomId = (lstRoomTypes[i].RoomTypeCode.split('||')[1].split('^')[0] == RoomTypeCode.split('||')[1].split('^')[0]) ?RoomId +i +"^": RoomId;
                    RoomId = ((lstRoomTypes[i].CommonRoomKey == roomCommonKey) ? RoomId + i + "^" : RoomId);
                }
            }

                // For disable selected room type in each room and To show total amount in summary section for selcted room type
                for (var k = 0; k < noofRooms; k++) {
                    if (isIndividualSelection == "true") {
                        if (k == 0) {
                            id = firstRoomid = (eval(k + 1) == sequenceNo) ? selectroomid : firstRoomid;
                        }
                        if (k == 1) {
                            id = secondRoomid = (eval(k + 1) == sequenceNo) ? selectroomid : secondRoomid;
                        }
                        if (k == 2) {
                            id = thirdRoomid = (eval(k + 1) == sequenceNo) ? selectroomid : thirdRoomid;
                        }
                        if (k == 3) {
                            id = fourthRoomId = (eval(k + 1) == sequenceNo) ? selectroomid : fourthRoomId;
                        }
                    }
                    if (isIndividualSelection == "false") {
                        selectRooms = [request.NoOfRooms];
                        for (var l = 0; l < RoomId.split('^').length - 1; l++) {
                            if (lstRoomTypes[RoomId.split('^')[l]].SequenceNo == eval(k + 1)) {
                                selectRooms[k] = RoomId.split('^')[l];
                                id = selectRooms[k];
                            }
                        }
                    }

                    $('#h5RoomName-' + k).text($('#h4RoomTypeName-' + k + id).text());
                    $('#mealPlanDesc-' + k).text($('#roomTypeMealPlan-' + k + id).text());

                    $('#btnSelect-' + k + id).attr('disabled', true);
                    $('#btnSelect-' + k + id).val('SELECTED');
                    $('#btnSelect-' + k + id).closest('.room-body').addClass('selected-room-div')
                    total += parseFloat(lstRoomTypes[id].TotalPrice);
                    discount += parseFloat(lstRoomTypes[id].Discount);
                }
                $('#beforeTotalPrice').children().remove();
                $('#TotalPrice').children().remove();
                $('#beforeTotalPrice').text("");
                $('#TotalPrice').text("");
                $('#beforeTotalPrice').append('<del><em class="currency">' + currency + ' </em>' + ' ' + Math.ceil(total).toFixed(decimalValue)) + ' </del>';
                $('#TotalPrice').append('<em class="currency">' + currency + ' </em>' + ' ' + Math.ceil(total - discount).toFixed(decimalValue));
                if (total.toFixed(decimalValue) == (total - discount).toFixed(decimalValue)) {
                    $('#beforeTotalPrice').children().hide();
                }
                else {
                    $('#beforeTotalPrice').children().show();
                }
                //for Hotel Overview --  Rooms (Room level amenities) 
                var roomAmenities = lstRoomTypes[id].Amenities;;
                var amenities = "";
                document.getElementById("h6Rooms").style.display = "none";
                if (roomAmenities != null) {
                    document.getElementById("h6Rooms").style.display = "block";

                for (var i = 0; i < roomAmenities.length; i++) {
                    amenities += roomAmenities[i] + ", ";
                }
            }
            $('#roomLevelAmenities').children().remove();
            $('#roomLevelAmenities').text("");
            $('#roomLevelAmenities').append('<p>' + amenities.replace(/,\s*$/, "") + '</p>');
            document.getElementById("DivInOut").style.display = "none";
            if (lstRoomTypes[id].TaxBreakups != null) {
                $('#dvTaxDetails').show();
                BindTaxBreakUps(lstRoomTypes[id].TaxBreakups, currency, region);
            }
            else {
                $('#dvTaxDetails').hide();
            }
            if (isCorporate == 'Y') {
                if (lstRoomTypes[id].TravelPolicyResult != null && lstRoomTypes[id].TravelPolicyResult.IsUnderPolicy == false) {
                    //for breakingrules 
                                    var PolicyBreakingRules = lstRoomTypes[id].TravelPolicyResult.PolicyBreakingRules;
                                    var BreakingRules = '';
                    if (document.getElementById("DivInOut") != null) {
                        $('#DivInOut').attr('title', "Out Side the policy -Booking Amount ");
                        $('#ImgInOutPolicy').attr("src", "/images/icon-outside-policy.png");
                        $('#ImgInOutPolicy').attr("alt", "Outside Policy");
                        document.getElementById("DivInOut").style.display = "block";
                         $.each( PolicyBreakingRules, function( key, value ) {
                                        BreakingRules += BreakingRules == '' ? value : ' ,' + value;
                                    });
                           $('#spanBreakRules').text("");
                        $('#spanBreakRules').text(BreakingRules);
                    }
                }
                else {
                    if (document.getElementById("DivInOut") != null) {
                        $('#DivInOut').attr('title', "InSide the policy ");
                        $('#ImgInOutPolicy').attr("src", "/images/icon-inside-policy.png");
                        $('#ImgInOutPolicy').attr("alt", "InSide Policy");
                        document.getElementById("DivInOut").style.display = "block";
                            $('#spanBreakRules').text("");
                        $('#spanBreakRules').text("In Policy");
                    }
                }
            }
           
            $("#btnBookNow").focus();
            topFunction();
        }
        // When the user clicks on the button, scroll to the top of the document
        function topFunction() {
            document.body.scrollTop = 0; // For Safari
            document.documentElement.scrollTop = 0; // For Chrome, Firefox, IE and Opera
        }
        //Book Room
        function BookRoom() {
           // $("#ctl00_upProgress").show();
            selectroomid = document.getElementById('<%=hdnSelectRoomId.ClientID %>').value;
            if (isCorporate == 'Y' && lstRoomTypes[selectroomid].TravelPolicyResult!=null && lstRoomTypes[selectroomid].TravelPolicyResult.IsUnderPolicy == false) {
                updateHotelDetails('save');
                var data = JSON.stringify(TypeCode);

                var passData = "id=" + TypeCode;

                Ajax.onreadystatechange = ShowTravelReason
                Ajax.open("POST", "CorpProfileBookingAjax");
                Ajax.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
                Ajax.send(passData);

                
            }
            else {
                GetRoomCancelPolicy(TypeCode, "", "Book",1,lstRoomTypes[selectroomid].SupplierName);
            }
           // GetRoomCancelPolicy(TypeCode, "", "Book");

        }
          function ShowTravelReason() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        document.getElementById('divTravelReason').style.display = 'block';
                        document.getElementById('divTravelReason').innerHTML = Ajax.responseText;
                        
                    }
                    else {
                        //ShowLoader();
                       // window.location.href = "PassengerDetails.aspx?id=" + ID;
                    }
                }
            }
        }
         function HideTravelReason() {
            document.getElementById('divTravelReason' ).style.display = 'none';
            document.getElementById("errorReason").style.display = "none";
            
        }
        //update HotelDetails to Session

        function updateHotelDetails(type) { // 'book' - is for normal flow, 'sve' - is for corporate flow
            var data = JSON.stringify(resultObj).replace('&', '').replace('#', '').replace('<', '').replace('>', '').replace(/'/g, '').replace(/\\"/g, '').replace('\\', '');    
            $.ajax({
                type: "POST",
                url: "ApiRoomDetails.aspx/HotelResult",
                dataType: "json",
                contentType: "application/json; charset=utf-8",
                data: "{'data':'" + data + "' }",
                success: function (response) {
                    if (type == 'Book') {
                        var tripid = 0;
                        if (document.getElementById("ddlTravelReason") != null && document.getElementById("ddlTravelReason").value.split('~')[0] > 0) {
                            tripid = document.getElementById("ddlTravelReason").value.split('~')[0];
                        }
                        if (resultObj.RoomDetails[selectroomid].SupplierId != '23') { 
                            if (resultObj.RoomDetails[selectroomid].IsIndividualSelection == false) {
                                TypeCode = commonRoomKey;
                            }
                        }

                        AjaxCall('CommonWebMethods.aspx/GetSetPageParams', "{'sessionKey':'ApiGuestDetails', 'sessionData':'" + (TypeCode + '|' + sessionId + '|' + tripid + '|' + TotalRooms) + "', 'action':'set'}");

                        window.location.href = "ApiGuestDetails.aspx";

                        //window.location.href = "ApiGuestDetails.aspx?RoomType=" + TypeCode + "&sessionId=" + sessionId + "&trid=" + tripid+ "&NOR=" + TotalRooms;
                        }
                },
                error: function (xhr, textStatus, errorThrown) {
                    $("#ctl00_upProgress").hide();
                    // alert(xhr.responseText + " --- " + errorThrown);
                }
            });
        }
        //Get Room level Cancellation policy
        function GetRoomCancelPolicy(roomTypeCode, roomTypeName, type,roomNo,sourceName) {

            if (type == "Book" && !IsEmpty(selectroomid) && isCorporate == 'Y' && lstRoomTypes[selectroomid].TravelPolicyResult != null
                && lstRoomTypes[selectroomid].TravelPolicyResult.IsUnderPolicy == false) {

                if (document.getElementById("ddlTravelReason" ).value != "-1") {
                    document.getElementById("errorReason").style.display = "none";                
                }
                else {
                    document.getElementById("errorReason").style.display = "block";
                    return;
                }
            }
            if (type == "Book" && (lstRoomTypes[selectroomid].IsIndividualSelection)) {
                            var roomsids = '';
                            if (!IsEmpty(firstRoomid) || firstRoomid>=0)
                            {
                                roomsids = roomsids+lstRoomTypes[firstRoomid].RoomTypeCode+ "^";
                }
                if (!IsEmpty(secondRoomid) || secondRoomid >= 0)
                            {
                                roomsids = roomsids+lstRoomTypes[secondRoomid].RoomTypeCode + "^";
                }
                if (!IsEmpty(thirdRoomid) || thirdRoomid >= 0)
                            {
                                roomsids = roomsids+lstRoomTypes[thirdRoomid].RoomTypeCode + "^";
                }
                if (!IsEmpty(fourthRoomId) || fourthRoomId>=0)
                            {
                                roomsids = roomsids+lstRoomTypes[fourthRoomId].RoomTypeCode + "^";
                            }
                            
                   //TypeCode = (!IsEmpty(firstRoomid) ? (lstRoomTypes[firstRoomid].RoomTypeCode + '||' + (!IsEmpty(secondRoomid) ? (lstRoomTypes[secondRoomid].RoomTypeCode + '||' + (!IsEmpty(thirdRoomid) ? (lstRoomTypes[thirdRoomid].RoomTypeCode + '||' + (!IsEmpty(fourthRoomId) ? (lstRoomTypes[fourthRoomId].RoomTypeCode + '||') : '' + '||')) : '' + '||')) : '' + '||')) : '')
                   roomsids = (IsEmpty(secondRoomid)&& secondRoomid < 0 )? roomsids.replace('^', ''):roomsids.slice(0, roomsids.lastIndexOf('^'));
                   TypeCode = roomTypeCode = roomsids;
                                      
                            //TypeCode = TypeCode.slice(0, TypeCode.lastIndexOf('||'))
                           
                        }
             $("#ctl00_upProgress").show();
            if (type == "Cancel") {
                //$("#ctl00_upProgress").show();
            }

            var userId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
            var agentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
            var behalfLocation = JSON.parse(document.getElementById('<%=hdnBehalfLocation.ClientID %>').value);
            request = JSON.parse(document.getElementById('<%=hdnObjRequest.ClientID %>').value);
            resultObj.HotelDescription = !IsEmpty(resultObj.HotelDescription) ? resultObj.HotelDescription.replace('&', '').replace('#', '').replace('<', '').replace('>', '').replace('%', '').replace(/'/g, '').replace(/\\"/g, '').replace('\\', '').replace(/<br\s*\/?>/gi, ' ').replace('br/', '') : '';
            var data = { roomTypeCode: roomTypeCode, resultObj: resultObj, sessionId: sessionId, userId: userId, agentId: agentId, behalfLocation: behalfLocation, hrequest: request, roomNo: roomNo, sourceName: sourceName};
            //var data = { roomtypecode: roomtypecode, resultobj: resultobj, sessionid: sessionid, userid: userid, agentid: agentid, behalflocation: behalflocation, hrequest: request, roomno: roomno, sourcename: sourcename };

            var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiHotelUrl"]%>';
            
            if (apiUrl == null || apiUrl == '')
                apiUrl = DynamicAPIURL();
            var noPolicies = false;
            $.ajax({
                url: apiUrl + '/api/HotelRooms/GetRoomsCancelPolicy',
                type: 'POST',
                contentType: 'application/json; charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(data),
                 beforeSend: function (xhr) {
                        xhr.setRequestHeader('Authorization', "Bearer " + JSON.parse(document.getElementById('<%=hdnTokenId.ClientID %>').value));
                    },
                success: function (data) {
                    var rslt = JSON.parse(data);
                    resultObj = rslt.ResultObj;
                    var cnlPolicy = rslt.CancelPolicy.replace("|", ". <br>");
                    
                    if (IsEmpty(cnlPolicy)) {
                        noPolicies = true;                      
                    }
            if(type == "Book" && lstRoomTypes[selectroomid].IsIndividualSelection){
              cnlPolicy = cnlPolicy.replace("@@", ". <br>");
                   // if (type == "Book") {// For Source HIS
                        var policies = rslt.CancelPolicy.split('|');
                        if (policies.length == request.NoOfRooms) {
                            $.each(policies, function (index, value) {
                                if (IsEmpty(policies)) {
                                    noPolicies = true;                                    
                                }
                            });
                        }
                        else {
                            noPolicies = true;
                        }
                   // }
                 }
                    if (noPolicies) {
                         $("#btnCancel").show();
                        $("#headRoomTypeName").text("Cancellation Policy");
                        $("#h2CnlPolicy").text("No Cancellation Policy is avaliable for Selected Rooms So please Select another Room or Hotel");
                        $("#CancelPolicyModal").modal("show");
                        $("#ctl00_upProgress").hide();
                        return;
                    }

                    if (type == "Cancel")
                        BindRules(resultObj.BookingSource, cnlPolicy, roomTypeName,sourceName);
                    else
                        updateHotelDetails(type);

                },
                error: function (xhr, textStatus, errorThrown) {
                    // alert('Error in cancellation policy Operation');
                    $("#ctl00_upProgress").hide();
                }
            });
        }

        /* To display hotel rules and cancel policy */
        function BindRules(Source, cnlPolicy, roomTypeName,sourceName) {

            $("#btnCancel").hide();
            $("#headRoomTypeName").text(roomTypeName);
            $('#h2CnlPolicy').children().remove();
            $('#h2CnlPolicy').text("");

            $('#h2CnlPolicy').append('<div id="divRules" class="padding-10 refine-result-bg-color"></div>');
            var Ruleshtml = ''; var hdnIsAgent = document.getElementById('<%=hdnIsAgent.ClientID %>').value == '1';

            if (Source == '23') {

                var RulesInfo = JSON.parse(cnlPolicy); var canhtml = '';
                var Restrict = [];
                Restrict.push('Rate description'); Restrict.push('Rate amount');Restrict.push('Room rate'); Restrict.push('Total Amount');

                $.each(RulesInfo, function (key, col) {

                    if (!IsEmpty(col.name) && col.FormattedText != null && col.FormattedText.length > 0) {
                                                
                        if (col.name == 'Cancellation')
                            canhtml = '<p class="Ruleshdr">Cancellation policy</p><br />' + '<p class="Rulesdtl">' + col.FormattedText[0].Value + '</p><br />';
                        else
                            Ruleshtml += (Restrict.indexOf(col.name) > -1 && !hdnIsAgent) ? '' : '<p class="Ruleshdr">' + col.name + '</p><br />' + '<p class="Rulesdtl">' + col.FormattedText[0].Value + '</p><br />';
                    }
                });         
                Ruleshtml = canhtml + Ruleshtml;
            }
            else {
                var policies = cnlPolicy.split('~');
               // Ruleshtml = '<p class="Ruleshdr">Cancellation policy</p><br />' + '<p class="Rulesdtl">' + policies[0] + '</p>';
                if (sourceName== 'GIMMONIX') {
                    Ruleshtml = '<p class="Ruleshdr">Cancellation policy</p><br />' + '<p class="Rulesdtl">' + policies[0].replace('^', "<br>") + '</p>';
                }
                else {
                    Ruleshtml = '<p class="Ruleshdr">Cancellation policy</p><br />' + '<p class="Rulesdtl">' + policies[0] + '</p>';
                    }
                if (policies != null && policies.length > 1 && !IsEmpty(policies[1])) {
                    var plcs = policies[1].split('^');
                    Ruleshtml += '<p class="Ruleshdr">Important Information</p><br />' + '<div class="Rulesdtl">' + plcs[0].replace('*', "<br>*") + '</div>';
 
                        if (sourceName== 'GIMMONIX') {
            
                        Ruleshtml += '<p class="Ruleshdr">Disclaimer</p><br />' + '<div class="Rulesdtl">';
                        if (!IsEmpty(plcs[1])) {
                          
                            Ruleshtml +=   plcs[1].replace('*', "<br>*") + '<br/><br/>';
                        }
                        
                        Ruleshtml +=   '<label class="bold"> Bed type availabilities depends @ Check-In time </label> </div>';
                }
                }
                
            }
           
            $('#divRules').append(Ruleshtml);
            $("#CancelPolicyModal").modal();
            $("#ctl00_upProgress").hide();
        }

        //For load Hotel Overview
        function LoadHotelOverview(hotelResult) {
            // For Amenities        

            if (hotelResult.HotelFacilities != null && hotelResult.HotelFacilities.length > 0) {
                for (var i = 0; i < hotelResult.HotelFacilities.length; i++) {

                    var amenity = hotelResult.HotelFacilities[i].trim().toLowerCase();
                    amenity = amenity.replace(" ", "-");
                    $('#Amenities').append(' <li><span class="icon icon-' + amenity + '"></span><span class="text">' + hotelResult.HotelFacilities[i] + '</span></li>');

                }
            }
            // For Description 
            $('#HotelDescription').children().remove();
            $('#HotelDescription').text("");
            $('#HotelDescription').append('<p>' + IsEmpty(hotelResult.HotelDescription) ? hotelResult.HotelName : hotelResult.HotelDescription + '</p>');
            // for Location
            $('#pLocation').text(hotelResult.HotelName + " , " + hotelResult.HotelAddress);

            //for Rooms (Room level amenities) 
            var roomAmenities = hotelResult.RoomDetails[0].Amenities;
            var amenities = "";
            document.getElementById("h6Rooms").style.display = "none";
            if (roomAmenities != null) {
                document.getElementById("h6Rooms").style.display = "block";
                for (var i = 0; i < roomAmenities.length; i++) {
                    amenities += roomAmenities[i] + ", ";
                }
                $('#roomLevelAmenities').children().remove();
                $('#roomLevelAmenities').text("");
                $('#roomLevelAmenities').append('<p>' + amenities.replace(/,\s*$/, "") + '</p>');
            }


        }
    </script>
    <script>
        //Loading Starts
        $(function () {
            NProgress.start();
        })
       


     

        //For RateBreakup
        $('.ui-listing-wrapper').on('click', '.rate-breakup-close', function () {
            $('.rate-breakup-div').removeClass('in');
        })
        $(document).mouseup(function (e) {
            var MenuContainer = $('.rate-breakup-div');
            if (!MenuContainer.is(e.target) && MenuContainer.has(e.target).length === 0) {
                MenuContainer.removeClass('in');
            }
        });
        function DynamicAPIURL() {
                //debugger;
                var url;
                var Secure ='<%=Request.IsSecureConnection%>';
            if (Secure=='True') {
                url = '<%=Request.Url.Scheme%>'+"://" +'<%=Request.Url.Host%>'+"/HotelWebApi";
            }
            else {
                 url = 'https'+"://" +'<%=Request.Url.Host%>'+"/HotelWebApi";
                 // url = '<%=Request.Url.Scheme%>'+"://" +'<%=Request.Url.Host%>'+"/HotelWebApi";
            }
            return url;
        }

        //scrollToFixed
        //document.addEventListener('DOMContentLoaded', function () {       
        //    $('.hotel-price-info-wrapper').scrollToFixed();
        //});
    

    </script>
    <script src="build/js/jquery-ui.js"></script>
    <script src="build/js/nprogress.js"></script>
    <script src="build/js/jquery-scrolltofixed-min.js"></script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
