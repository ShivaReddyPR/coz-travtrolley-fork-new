﻿<%@ Page Language="C#" AutoEventWireup="true" Title="FeedBack from Corporates" EnableEventValidation="false"  Inherits="FeedbackCorp" Codebehind="FeedbackCorp.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head runat="server">
    <title>FeedBack from Corporates</title>
      <script language="javascript" type='text/javascript'>
          function popupopen(popup_name) {
              document.getElementById(popup_name).style.display = 'block';
          }

          function popupclose(popup_name) {
              document.getElementById(popup_name).style.display = 'none';
          }
       
    </script>
    
    
    <script>
        function PrintPage() {
            window.print();
        }
</script>

    <style type="text/css">
        #firstPopupDivOuter
        {
            position: absolute;
            top: 50px;
            left: 100px;
            width: 400px;
            height: 300px;
            display: none;
            background-color:#2BA9D9;
            color:#FFF;
        }
        #firstPopupDivInner
        {
            width: 378px;
            height: 268px;
            background: url(images/thankuwindow.png) no-repeat;
            vertical-align: middle;
            margin: 160px auto;
        }
        
        .font_red
        {
            color: Red;
        }
        .feedback-corp label
        {
            padding-right: 30px;
        }
        .feedback-corp td
        {
            padding-left: 5px;
            padding-right: 5px;
        }
        .feedback-corp input[type=text], select, textarea
        {
            width: 100%;
            color: #000;
            border-style: solid;
            border-width: 1px;
            border-color: #ccc;
            border-radius: 3px;
            -moz-border-radius: 3px;
            -khtml-border-radius: 3px;
            -webkit-border-radius: 3px;
            box-shadow: -1px 1px 3px #d8d8d8 inset;
            font: 13px Arial, Helvetica, sans-serif;
            margin: 0;
            padding: 0px 0px 0px 5px;
            line-height: 26px;
            min-height: 26px;
        }
        .feedback-corp strong
        {
            font-size: 15px;
        }
        .feedback-corp
        {
            background: url(images/feedbackbg.jpg);
            margin-top: 10px;
            padding-top: 10px;
            font-family: Arial, Helvetica, sans-serif;
            font-size: 13px;
        }
        .div_container
        {
            border-bottom: solid 2px #f3f2f2;
            padding: 10px 40px 20px 40px;
            margin-bottom: 10px;
            width: 90%;
        }
        .t_area
        {
            height: 60px;
        }
        /* RADIOS & CHECKBOXES STYLES *//* base styles */input[type="radio"]
        {
            height: 1.2em;
            width: 1.2em;
            vertical-align: middle;
            margin: 0 0.4em 0.4em 0;
            border: 1px solid rgba(0, 0, 0, 0.3);
            background: -webkit-linear-gradient(#FCFCFC, #DADADA);
            -webkit-appearance: none;
            -webkit-transition: box-shadow 200ms;
            -moz-appearance: none;
            -moz-transition: box-shadow 200ms;
            box-shadow: inset 1px 1px 0 #fff, 0 1px 1px rgba(0,0,0,0.1);
        }
        /* border radius for radio*/input[type="radio"]
        {
            -webkit-border-radius: 100%;
            -moz-border-radius: 100%;
            border-radius: 100%;
        }
        /* hover state */input[type="radio"]:not(:disabled):hover
        {
            border-color: rgba(0,0,0,0.5);
            box-shadow: inset 1px 1px 0 #fff, 0 0 4px rgba(0,0,0,0.3);
        }
        /* active state */input[type="radio"]:active:not(:disabled), input[type="checkbox"]:active:not(:disabled)
        {
            background-image: -webkit-linear-gradient(#C2C2C2, #EFEFEF);
            box-shadow: inset 1px 1px 0 rgba(0,0,0,0.2), inset -1px -1px 0 rgba(255,255,255,0.6);
            border-color: rgba(0,0,0,0.5);
        }
        /* focus state */input[type="radio"]:focus, input[type="checkbox"]:focus
        {
            outline: none;
            box-shadow: 0 0 1px 2px rgba(0, 240, 255, 0.4);
        }
        /* input checked border color */input[type="radio"]:checked, input[type="checkbox"]:checked
        {
            border-color: rgba(0, 0, 0, 0.5);
        }
        /* radio checked */input[type="radio"]:checked:before
        {
            display: block;
            height: 0.3em;
            width: 0.3em;
            position: relative;
            left: 0.3em;
            top: 0.3em;
            background: rgba(0, 0, 0, 0.7);
            border-radius: 100%;
            content: '';
        }
        /* checkbox checked */input[type="checkbox"]:checked:before
        {
            font-weight: bold;
            color: rgba(0, 0, 0, 0.7);
            content: '\2713';
            -webkit-margin-start: 0;
            -moz-border-radius: 100%;
            margin-left: 2px;
            font-size: 0.9em;
        }
        /* disabled input */input:disabled
        {
            opacity: .6;
            box-shadow: none;
            background: rgba(0, 0, 0, 0.1);
            box-shadow: none;
        }
        /* style label for disabled input */input:disabled + label
        {
            opacity: .6;
            cursor: default;
            -webkit-user-select: none;
            -moz-user-select: none;
        }
    </style>
</head>
<body>
  
  
    
     <%-- <form id="form1" runat="server">--%>
    <div class="feedback-corp" id="divFeed" runat="server" style="display:block" >
        <div class="div_container">
           
           
           <table width="100%" border="0" cellspacing="0" cellpadding="0">
           
           <tr> 
           <td width="20%"> 
           
           <img src="images/logo.jpg" />
           
           </td>
           
          <td width="60%">   <center>
                <h1>
                    Feedback form
                </h1>
            </center></td>
            
           <td width="20%" align="right"> 
           
           <a href="#" onClick="PrintPage()" class="button">  
           Print
           
           </a>
           
           </td>
           
           
           </tr>
           
           </table>
           
           
            
        </div>
        <div class="div_container">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                         <strong>Name:</strong>
                    </td>
                    <td>
                       <strong>Email:</strong>
                    </td>
                    <td>
                        <strong>Your Company:</strong>
                    </td>
                </tr>
                <tr>
                    <td>
                      <%-- <%=corpFeedback.Name %>--%>
                       <%=dr["CFName"] %>
                    </td>
                    <td>
                        <%=dr["CFEmail"] %>
                    </td>
                    <td>
                        <%=dr["CFCompany"]%>
                    </td>
                </tr>
            </table>
        </div>
        <div class="div_container">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <strong>How easily accessible and welcoming are our consultants during normal working
                            hours and during after hours?</strong>
                    </td>
                </tr>
                <tr>
                    <td height="20">
                    </td>
                </tr>
                
                <tr>
                
                    <td>
                       <%=dr["CFAccessible"]%>
                    </td>
                </tr>
                
                </table>
                </div>
                 <div class="div_container">
                <table>
                <tr>
                    <td>
                       <strong> Comments</strong>
                    </td>
                </tr>
                 <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=dr["CFAccessibleComments"]%>
                    </td>
                </tr>
            </table>
        </div>
        <div class="div_container">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <strong>Your travel queries are handled professionally and accurately with proper documentation?</strong>
                    </td>
                </tr>
                <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=dr["CFTravelQuery"]%>
                    </td>
                </tr>
               
                </table>
                </div>
                 <div class="div_container">
                <table>
                <tr>
                    <td>
                      <strong> Comments</strong>
                    </td>
                </tr>
                 <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=dr["CFTravelQueryComments"]%>
                    </td>
                </tr>
            </table>
        </div>
        <div class="div_container">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <strong>How informative have we been in our responses to your queries in our daily operations?</strong>
                    </td>
                </tr>
                <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr>
                    <td style="width: 100%">
                        <%=dr["CFDailyOps"]%>
                    </td>
                </tr>
               
                </table>
                </div>
                 <div class="div_container">
                <table>
                <tr>
                    <td>
                     <strong> Comments</strong>
                    </td>
                </tr>
                 <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=dr["CFDailyOpsComments"]%>
                    </td>
                </tr>
            </table>
        </div>
        <div class="div_container">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <strong>How well do our operational services meet your needs and expectations?</strong>
                    </td>
                </tr>
                <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=dr["CFOperServices"]%>
                    </td>
                </tr>
                
                </table>
                </div>
                 <div class="div_container">
                <table>
                <tr>
                    <td>
                      <strong> Comments</strong>
                    </td>
                </tr>
                 <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=dr["CFOperServicesComments"]%>
                    </td>
                </tr>
            </table>
        </div>
        <div class="div_container">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <strong>How would you rate the value for money for our airfares, hotel rates, visa charges
                            and other ancillaries?</strong>
                    </td>
                </tr>
                <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=dr["CFValueForMoney"] %>
                    </td>
                </tr>
                
                </table>
                </div>
                 <div class="div_container">
                <table>
                <tr>
                    <td>
                       <strong> Comments</strong>
                    </td>
                </tr>
                 <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=dr["CFValueForMoneyComments"] %>
                    </td>
                </tr>
            </table>
        </div>
        <div class="div_container">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <strong>How long have you been a customer of our company?</strong>
                    </td>
                </tr>
                <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=dr["CFCustRelation"] %>
                    </td>
                </tr>
            </table>
        </div>
        <div class="div_container">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <strong>How likely is that you would recommend our company and our services to associates
                            and colleagues in your industry?(0-10)</strong>
                    </td>
                </tr>
                 <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=dr["CFRecommend"]%>
                    </td>
                </tr>
            </table>
        </div>
        <div class="div_container">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td>
                        <strong>Do you have any other comments, recommendations, suggestions for improvements
                            or concerns?</strong>
                    </td>
                </tr>
                 <tr>
                    <td height="20">
                    </td>
                </tr>
                <tr>
                    <td>
                        <%=dr["CFRecommendComments"]%>
                    </td>
                </tr>
            </table>
        </div>
       
    </div>
   <%-- </form>--%>
</body>
</html>
