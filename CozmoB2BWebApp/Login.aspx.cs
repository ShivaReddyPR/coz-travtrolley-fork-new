using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

public partial class LoginUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        try
        {
           
        string hostName = BookingUtility.ExtractSiteName(Request["HTTP_HOST"]);
        if (ConfigurationManager.AppSettings["TestMode"] == "True")
        {
            hostName = "ibyta.com";
        }
        //Request["HTTP_X_FORWARDED_FOR"]
        //    REMOTE_ADDR 
        //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Host Name" +hostName, "");
        string[] themeDetails = AgentMaster.GetThemByDoamin(hostName);
        if (!string.IsNullOrEmpty(themeDetails[1]))
            Session["themeName"] = themeDetails[1];
        else Session["themeName"] = "Default";
        Label lblCopyRight = (Label)this.Master.FindControl("lblCopyRight");
            //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "test: " + themeDetails[2], "");
            if (!string.IsNullOrEmpty(themeDetails[2]) && (hostName == "cozmobizz.com" || hostName=="petrofac.cozmobizz.com"))
            {
                // CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, hostName+"1 zi", "");
                lblCopyRight.Text = themeDetails[2];
                Response.Redirect("bizzlogin.aspx");

            }
            //else if (!string.IsNullOrEmpty(themeDetails[2]) && (hostName == "ibyta.com" ))
            else if ((hostName == "ibyta.com"))
            {
                // CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, hostName+"1 zi", "");
                lblCopyRight.Text = themeDetails[2];
                Response.Redirect("ibytalogin");

            }
            //else if (!string.IsNullOrEmpty(themeDetails[2]) && hostName == "cozmobizz.com")
            //{
            //    // CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, hostName+"1 zi", "");
            //    lblCopyRight.Text = themeDetails[2];
            //    Response.Redirect("bizzlogin.aspx");

            //}
            else if (!string.IsNullOrEmpty(themeDetails[2]))
            {
                CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, hostName + "2", "");
                lblCopyRight.Text = themeDetails[2];
                Response.Redirect("AGLogin");

            }
        Image imgLogo = (Image)this.Master.FindControl("imgLogo");

        //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, hostName + "2 zi", "");
        if (!string.IsNullOrEmpty(themeDetails[0]))
        {
            string logoPath = ConfigurationManager.AppSettings["AgentImage"] + themeDetails[0];

            imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + logoPath;

        }

        string themeName = (string)Session["themeName"];
        if (themeName != null)
        {
            this.Page.Theme = themeName;
        }
        else
        {
            this.Page.Theme = "Default";
        }
        }

        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Dom Error:" + ex.ToString(), "");
            Utility.WriteLog(ex, this.Title);
            lblError.Text = ex.Message;
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //if (ex.InnerException != null) lblMasterError.Text = ex.InnerException.ToString();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            
            lblForgotPwd.Text = string.Empty;
            if (!IsPostBack) InitializeControls();
            lblError.Text = string.Empty;
            if (Request.QueryString["errMessage"] != null)
            {
                lblForgotPwd.Text = Request.QueryString["errMessage"];
            }
        }

        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            lblError.Text = ex.Message;
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //if (ex.InnerException != null) lblMasterError.Text = ex.InnerException.ToString();
        }
        
    }
    private void InitializeControls()
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                Session.Abandon();
                //Response.Redirect("Login.aspx");
            }
            // string hostName = Request["HTTP_HOST"];
            //string[] themeDetails = AgentMaster.GetThemByDoamin(hostName);


            //if (!string.IsNullOrEmpty(themeDetails[0]))
            //    Session["themeName"] = themeDetails[0];
            //else Session["themeName"] = "Default";
            //Image imgLogo = (Image)this.Master.FindControl("imgLogo");

            //if (!string.IsNullOrEmpty(themeDetails[1]))
            //{
            //    string logoPath = ConfigurationManager.AppSettings["AgentImage"] + themeDetails[1];

            //    imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + logoPath;

            //}
            
          

           
          


            //ddlCompany.DataSource = CompanyMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            //ddlCompany.DataValueField = "company_id";
            //ddlCompany.DataTextField = "company_name";
            //ddlCompany.DataBind();
            //ddlCompany.Items.Insert(0, new ListItem("--Select Company--", "-1"));
            //ddlCompany.SelectedValue = "2";
            // clearControls();
        }
        catch { throw; }

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {

            bool isAuthenticated = UserMaster.IsAuthenticatedUser(txtLoginName.Text.Trim(), txtPassword.Text, Utility.ToInteger(ddlCompany.SelectedValue));
            if (isAuthenticated)
            {
                //Session["agencyId"] = 1;
                //Response.Redirect("HotelSearch.aspx", false);
                if (!string.IsNullOrEmpty(Settings.LoginInfo.AgentTheme))
                    Session["themeName"] = Settings.LoginInfo.AgentTheme;
                else Session["themeName"] = "Default";

                //Response.Redirect("Index.aspx", false);
                //Response.Redirect("Index.aspx", false);

                string[] agentLeadProduct = Settings.LoginInfo.AgentProduct.Split(',');//, StringSplitOptions.RemoveEmptyEntries);
                if (agentLeadProduct.Length > 0)
                {

                    Array.Sort(agentLeadProduct);

                    if (!string.IsNullOrEmpty(agentLeadProduct[0]))
                    {
                        switch (agentLeadProduct[0])
                        {
                            case "1":
                                if (Settings.roleFunctionList.Exists(r => r.ToLower().Contains("hotelsearch?source=flight")))
                                    Response.Redirect("HotelSearch?source=Flight", false);
                                else
                                    Response.Redirect("Index", false);
                                    //Response.Redirect("UnauthorizedAccess", false);
                                
                                break;
                            case "2":
                                if (Settings.LoginInfo.Currency == "INR" && Settings.roleFunctionList.Exists(r => r.ToLower().Contains("hotelsearch?source=hotel")))
                                    Response.Redirect("HotelSearch?source=Hotel", false);
                                else if (Settings.roleFunctionList.Exists(r => r.ToLower().Contains("findhotels")))
                                    Response.Redirect("findHotels", false);
                                else
                                    Response.Redirect("Index", false);
                                break;
                            case "3":
                                if (Settings.roleFunctionList.Exists(r => r.ToLower().Contains("packageMaster")))
                                    Response.Redirect("PackageMaster", false);
                                else
                                    Response.Redirect("Index", false);
                                break;
                            case "4":
                                if (Settings.roleFunctionList.Exists(r => r.ToLower().Contains("activitymaster")))
                                    Response.Redirect("ActivityMaster", false);
                                else
                                    Response.Redirect("Index", false);
                                break;
                            case "5":
                                if (Settings.roleFunctionList.Exists(r => r.ToLower().Contains("insurance")))
                                    Response.Redirect("Insurance", false);
                                else
                                    Response.Redirect("Index", false);
                                break;
                            default:
                                Response.Redirect("Index", false);// Default landing page for All roles
                                break;

                        }
                    }
                    else Response.Redirect("Index", false);// Default landing page for All roles
                }
                else Response.Redirect("Index", false);// Default landing page for All roles
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            lblError.Text = ex.Message;
            lblError.Visible = true;
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //if(ex.InnerException!=null)lblMasterError.Text = ex.InnerException.ToString();
            
        }

    }
    protected void btnGetPassword_Click(object sender, EventArgs e)
    {
        try
        {
            string pagePath =Request.Url.Scheme+ "://" + Request.ServerVariables["HTTP_HOST"] + Page.Request.Path;
            string[] pageName = pagePath.Split('/');
            pagePath = string.Empty;
            for (int i = 0; i < pageName.Length - 1; i++)
            {
                pagePath = pagePath + pageName[i] + "/";
            }
            string guid = UserMaster.RequestPasswordChange(txtEmailId.Text.Trim());
            pagePath = pagePath + "ResetPassword?requestId=" + guid;

            //lblForgotPwd.ForeColor = Color.Black;
            
            //DataTable dtUser = CT.TicketReceipt.BusinessLayer1.UserMaster.GetUserByEmailId(txtEmailId.Text.Trim());
            //CT.TicketReceipt.BusinessLayer1.UserMaster member;
            //if (dtUser != null && dtUser.Rows.Count > 0)
            //{
            //    long userId = Convert.ToInt64(dtUser.Rows[0]["user_id"]);
            //     member = new CT.TicketReceipt.BusinessLayer1.UserMaster(userId);
            //}
            #region Sending Email
            //if (Convert.ToBoolean(ConfigurationManager.AppSettings["isSendEmail"]))
            //{

                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                //if (member.IsPrimaryAgent)
                //{
                    toArray.Add(txtEmailId.Text.Trim());
                //}
                //else
                //{
                //    string primaryAgentLogin = member.LoginName.Substring(member.LoginName.IndexOf('@') + 1);
                //    Member primaryMember = new Member();
                //    primaryMember.Load(primaryAgentLogin);
                //    toArray.Add(primaryMember.Email);
                //}
                string link = "<a href='" + pagePath + "'>" + pagePath + "</a>";
                string message = ConfigurationManager.AppSettings["changePassowrdMessage"].Replace("%link%", link);
                try
                {
                   // CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Change your password", message, null);
                    lblForgotPwd.Text = "An email has been sent to you with a link to reset your password!";
                }
                catch (System.Net.Mail.SmtpException)
                {
                    //CT.Core1.Audit.Add(CoreLogic.EventType.Email, CoreLogic.Severity.Normal, 0, "Smtp is unable to send the message", "");
                }
            //}
            #endregion
            
        }
        catch(Exception ex)
        {
            lblForgotPwd.Text = ex.Message.ToString();
        }
    }
}
