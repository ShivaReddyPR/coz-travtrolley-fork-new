﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="IRQVisaList" Codebehind="IRQVisaList.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="ash.js"></script>
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        //Yahoo calendar start
        function init1() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal3 = new YAHOO.widget.Calendar("cal3", "container3");
            cal3.cfg.setProperty("minDate", (1) + "/" + 1 + "/" + (today.getFullYear() - 97));
            cal3.cfg.setProperty("maxDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal3.cfg.setProperty("title", "Select From Date");
            cal3.cfg.setProperty("close", true);
            cal3.selectEvent.subscribe(setDate3);
            cal3.render();

            cal4 = new YAHOO.widget.Calendar("cal4", "container4");
            cal4.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + (today.getDate() + 1) + "/" + today.getFullYear());
            cal4.cfg.setProperty("title", "Select ToDate");
            cal4.selectEvent.subscribe(setDate4);
            cal4.cfg.setProperty("close", true);
            cal4.render();
        }
        function showCalendar3() {
            init1();
            cal3.show();
            document.getElementById('container4').style.display = "none";
            document.getElementById('container3').style.display = "block";
        }
        function showCalendar4() {
            document.getElementById('container3').style.display = "none";
            //cal3.hide();
            this.today = new Date();
            var thisMonth = this.today.getMonth();
            var thisDay = this.today.getDate();
            var thisYear = this.today.getFullYear();

            cal4.cfg.setProperty("minDate", (thisMonth + 1) + "/" + thisDay + "/" + thisYear);
            cal4.cfg.setProperty("pageDate", (thisMonth + 1) + "/" + thisYear);
            cal4.render();

            document.getElementById('container4').style.display = "block";

        }

        function setDate3() {
            var date1 = cal3.getSelectedDates()[0];
            //alert(date1);
            document.getElementById('IShimFrame').style.display = "none";

            var month = date1.getMonth() + 1;
            var day = date1.getDate();


            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('ctl00_cphTransaction_txtFromDate').value = day + "/" + (month) + "/" + date1.getFullYear();
            cal3.hide();

        }
        function setDate4() {
            var toDate = cal4.getSelectedDates()[0];

            var passExpiryDate = new Date(toDate.getFullYear(), toDate.getMonth(), toDate.getDate());

            var month = toDate.getMonth() + 1;
            var day = toDate.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('ctl00_cphTransaction_txtToDate').value = day + "/" + month + "/" + toDate.getFullYear();

            cal4.hide();
        }
        YAHOO.util.Event.addListener(window, "load", init1);

        //Yahoo calendar end


        function checkUpdate(id) {
            if (document.getElementById('ddlApplicantStatus_' + id).selectedValue != '-1') {
                document.getElementById('hdnUpdatedId').value = id + "_" + document.getElementById('ddlApplicantStatus_' + id).selectedValue;
            }
            else {
                document.getElementById('lblErrorMessage' + id).style.display = "block";
                document.getElementById('lblErrorMessage' + id).innerHTML = "Please select the Visa Status";
            }
        }
    </script>

    <input type="hidden" id="hdnUpdateId" value="" runat="server" />
    <asp:HiddenField ID="PageNoString" runat="server" Value="1" />
    <h3>Quick Access </h3>
    <div class="clear" style="margin-left: 30px">
        <div id="container3" style="position: absolute; top: 250px; left: 2%; z-index: 9999; display: none;">
        </div>
    </div>
    <div class="clear" style="margin-left: 30px">
        <div id="container4" style="position: absolute; top: 250px; left: 15%; z-index: 9999; display: none;">
        </div>
    </div>
    <div class="bggray bor_gray paddingtop_10 marbot_20">
        <div class="col-md-2">
            <div class="form-group">
                <label>From Date:   </label>
                <div class='input-group date' id='Div1'>
                    <input placeholder="dd/mm/yyyy" type='text' id="txtFromDate" runat="server" onkeypress="isNumber(event)" onclick="showCalendar3()" class="form-control" />
                    <%--<asp:TextBox id="txtFromDate" runat="server" onkeypress="isNumber(event)" onclick="showCalendar3()" CssClass="form-control" />--%>
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>To Date:   </label>
                <div class='input-group date' id='datetimepicker1'>
                    <input type='text' placeholder="dd/mm/yyyy" id="txtToDate" runat="server" onkeypress="isNumber(event)" onclick="showCalendar4()" class="form-control" />
                    <span class="input-group-addon">
                        <span class="glyphicon glyphicon-calendar"></span>
                    </span>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Trans. ID (Visa):   </label>
                <input type="text" id="txtDocNumber" runat="server" class="form-control" />
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Status: </label>
                <asp:DropDownList ID="ddlVisaStatus" runat="server">
                    <asp:ListItem Text="Select" Value="-1" Selected="True" />
                    <asp:ListItem Text="InProgress" Value="I" />
                    <asp:ListItem Text="Rejected" Value="R" />
                    <asp:ListItem Text="Approved" Value="C" />
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group">
                <label>Visa Type: </label>
                <asp:DropDownList ID="ddlVisaType" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-2">
            <div class="form-group" id="divAgentNames" runat="server" style="display: none">
                <label>Agent: </label>
                <asp:DropDownList ID="ddlB2BAgent" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div class="col-md-12">
            <div class="form-group">
                <div>
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" /> <%--OnClientClick="return validateSearch();"--%>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>

        <div class="clearfix"></div>
    </div>

    <h3>Visa List </h3>
    <asp:DataList ID="dlVisaBookings" runat="server" Width="100%" OnItemDataBound="dlVisaBookings_ItemDataBound" OnItemCommand="dlVisaBookings_ItemCommand">
        <ItemStyle VerticalAlign="Top" />
        <ItemTemplate>
            <div class="pull-right">
                <%=show %>
            </div>
            <div class="tbl">

                <div class="col-md-12 padding-0 ">
                    <div class="col-md-4">
                        <strong>COZMO B2C UAE</strong>
                        <br />
                        <strong>Visa Type:  </strong>
                        <asp:Label ID="lblVisaType" runat="server" Text='<%#Eval("VisaType") %>' /><br />
                        Price:  <strong>
                            <asp:Label ID="lblVisaPrice" runat="server" Text='<%#Eval("VisaPrice") %>' /></strong>
                        <asp:Label ID="lblVisaCurrency" runat="server" Text='<%#Eval("Currency") %>' /><br />
                    </div>
                    <div class="col-md-5">
                        <span class="text-success">
                            <asp:Label ID="lblVisaStatus" runat="server" Text='<%#Eval("VisaStatusName") %>' />
                        </span>
                        <br />
                        <asp:Label ID="lblLocationName" runat="server" Text='<%#Eval("location_Name") %>' />
                        <br />
                    </div>
                    <div class="col-md-3">
                        <strong>
                            <asp:Label ID="lblDocNumber" runat="server" Text='<%#Eval("DocNumber") %>' />
                        </strong>
                        <br />
                        <asp:Label ID="lblPaxName" runat="server" Text='<%#Eval("ApplicantName") %>' />
                        <br />
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="col-md-12 padding-0 ">
                    <div class="col-md-9">
                    </div>
                    <div class="col-md-3" id="divUpdateStatus" runat="server">
                        <table width="100%">
                            <tr>
                                <td>Update Status: </td>
                                <td>
                                    <asp:DropDownList ID="ddlApplicantStatus" runat="server">
                                        <asp:ListItem Text="Select" Value="-1" Selected="True" />
                                        <%--<asp:ListItem Text="InProgress" Value="I" />--%>
                                        <asp:ListItem Text="Rejected" Value="R" />
                                        <asp:ListItem Text="Approved" Value="C" />
                                    </asp:DropDownList>
                                </td>
                                <td width="10"></td>
                                <td>
                                    <asp:Button ID="btnUpdate" CssClass="button" CommandName="Update" CommandArgument='<%#Eval("ApplicantId") %>' runat="server" Text="Update" Visible="false" />
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="pull-right">
                <%=show %>
            </div>
        </ItemTemplate>
    </asp:DataList>
    
    <iframe id="IShimFrame" style="position: absolute; display: none;" frameborder="0"></iframe>
</asp:Content>

