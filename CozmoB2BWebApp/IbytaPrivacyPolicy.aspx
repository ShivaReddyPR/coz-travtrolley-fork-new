﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IbytaPrivacyPolicy.aspx.cs" Inherits="IbytaPrivacyPolicy" %>
	<!DOCTYPE html>
	<html>

	<head runat="server">
		<title>Ibyta - Privacy Policy </title>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="icon" href="images/favicon.ico">	
		<!-- Bootstrap Core CSS -->
		<link href="css/bootstrap.min.css" rel="stylesheet" />
		<!-- manual css -->

		<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet" />
		<link href="build/css/main.min.css" rel="stylesheet" type="text/css" /> 
        <link href="css/override.css" rel="stylesheet">     
        
		<%--<script type="text/javascript" src="Scripts/Menu/query2.js"></script>--%>
     
    <%--jQuery 2.2.2 + Migrate 1.4.1 - Dec2020 Upgrade--%>

     <script src="build/js/jquery-2.2.4.min.js" type="text/javascript"></script>        
     <script type="text/javascript" src="build/js/jquery-migrate-1.4.1.min.js"></script>
       <script>
           jQuery.ajaxPrefilter(function(s){
            if(s.crossDomain){
                s.contents.script = false;
             }
           })
     </script>

	</head>
<body>
     <div id="unsupported-message">
	        <a id="unsupported-overlay" href="#unsupported-modal"></a>
	        <div id="unsupported-modal" role="alertdialog" aria-labelledby="unsupported-title">
		        <h2 id="unsupported-title">⚠ Unsupported Browser ⚠</h2>
		        <p>Travtrolley probably won't work great in Internet Explorer. We generally only support the recent versions of major browsers like <a target="_blank" href="https://www.google.com/chrome/">Chrome</a>, <a target="_blank" href="https://www.mozilla.org/en-US/firefox/new/">Firefox</a>, <a target="_blank" href="https://support.apple.com/downloads/safari">Safari.</a>!</p>

		        <p>If you think you're seeing this message in error, <a href="mailto:tech@cozmotravel.com">email us at tech@cozmotravel.com</a> and let us know your browser and version.</p>
	        </div>
	        <a id="unsupported-banner">
	        <strong> ⚠ Unsupported Browser ⚠ </strong>
		        Travtrolley may not work properly in this browser.
	        </a>
          </div>
         <script>
            // Get IE or Edge browser version
            function detectIE() {
                var ua = window.navigator.userAgent,
                    msie = ua.indexOf('MSIE '),
                    trident = ua.indexOf('Trident/'),
                    edge = ua.indexOf('Edge/');              
                 if (msie > 0 || trident > 0 ) {
                    $('#unsupported-message').css('display', 'block');
                    $('body').css('overflow', 'hidden');
                }
            }
            detectIE();         

        </script>
	    <style>
	            #unsupported-message {
                     display: none;
	            }
		        #unsupported-overlay,
		            #unsupported-modal {
		              display: block;
		              position: absolute;
		              position: fixed;
		              left: 0;
		              right: 0;
		              z-index: 9999;
		              background: #000;
		              color: #D5D7DE;
		            }
		
		            #unsupported-overlay {
		              top: 0;
		              bottom: 0;
		            }
		
		            #unsupported-modal {
		              top: 80px;
		              margin: auto;
		              width: 90%;
		              max-width: 520px;
		              max-height: 90%;
		              padding: 40px 20px;
		              box-shadow: 0 10px 30px #000;
		              text-align: center;
		              overflow: hidden;
		              overflow-y: auto;
		              border:solid 2px #ccc8b9;
		            }
		
		            #unsupported-message :last-child { margin-bottom: 0; }
		
		            #unsupported-message h2 {
		              font-size: 34px;
		              color: #FFF;
                      margin-bottom: 20px;
		            }
		
		            #unsupported-message h3 {
		              font-size: 20px;
		              color: #FFF;
		            }
		
		            body.hide-unsupport { padding-top: 24px; }
		            body.hide-unsupport #unsupported-message { visibility: hidden; }
		
		            #unsupported-banner {
		              display: block;
		              position: absolute;
		              top: 0;
		              left: 0;
		              right: 0;
		              background:#ccc8b9;
		              color: #000;
		              font-size: 14px;
		              padding: 2px 5px;
		              line-height: 1.5;
		              text-align: center;
		              visibility: visible;
		              z-index: 100000;
		            }
		
		
		
		            @media (max-width: 800px), (max-height: 500px) {
		              #unsupported-message .modal p {
		                /* font-size: 12px; */
		                line-height: 1.2;
		                margin-bottom: 1em;
		              }
		
		              #unsupported-modal {
		                position: absolute;
		                top: 20px;
		              }
		              #unsupported-message h1 { font-size: 22px; }
		              body.hide-unsupport { padding-top: 0px; }
		              #unsupported-banner { position: static; }
		              #unsupported-banner strong,
		              #unsupported-banner u { display: block; }
		            }
                    html,body{
                        min-height: 100%;
                        background-color: #ebf1fb;
                    }
                    body{
                        display:flex;
                        flex-direction:column;
                    }                     
                    .ibyta--top-header {
	                    background-color:#ec0b43;
	                    padding:5px 0 5px 0
                    }
                    .ibyta-footer {
                        bottom: 0;
                        left: 0;
                        right: 0;
                        z-index: 1000;
                        position: relative;
                        flex: 1;
                        display: flex;
                        flex-direction: column;
                        justify-content: flex-end;
                    }
                    .ibyta-footer .copyright {
	                    color:#fff;
	                    text-align:center;
	                    background-color:#000;
	                    padding:4px 0;
                        font-size: 12px;
                    }
                    .ibyta-footer .contact-info{
                        text-align: center;
                        padding-bottom: 5px;
                        color: #ec0b43;
                        background-color: #f5f4f4;
                        padding: 6px 0;
                    }
                    .ibyta-footer .contact-info a{
                        color: #ec0b43;
                    }
                    .ibyta-footer .contact-info em{
                        color:#383838;
                        font-style:normal;
                    }
                    .services-list{
                        text-align: center;
                        padding: 10px 2px;
                          background: #fff;
                          box-shadow: 0 0 5px -2px rgba(0, 0, 0, 0.32);
                    }
                    .services-list li{
                        color: #696969;
                        font-size: 13px;
                        font-weight: 600;
                        padding: 0 20px;
                    }
                    .services-list li:before{
                        content:"";
                        background:url(build/img/ibyta-services.svg) no-repeat;
                        background-position: -124px -60px;
                        width: 38px;
                        height: 30px;
                        display: block;
                        margin:0 auto 8px auto;
                        opacity:.1;
                    }
                    .services-list li:nth-child(2):before{
                        background-position:0 -122px;
                    }
                    .services-list li:nth-child(3):before{
                        background-position:6px -60px;
                    }
                    .services-list li:nth-child(4):before{
                        background-position:-63px -60px;
                    }
                    .services-list li:nth-child(5):before{
                        background-position:9px 0px;
                    }
                    .services-list li:nth-child(6):before{
                        background-position:-187px -60px;
                    }
                    @media(min-width:768px){.ibyta-footer{}}

	        </style>
    
    	<div class="ibyta--top-header">
			<div class="cz-container" style="box-shadow: none;">
				<div class="row px-5 px-lg-0">
					<div class="col-xs-6">
						<a href="ibytalogin.aspx"><img src="images/ibyta-logo.jpg" class="img-responsive" style="max-width: 130px;margin-left: -15px;"></a>
                        <p class="text-white"><small>An AirArabia Group Company</small> </p>
					</div>
					<div class="col-xs-6">
						<a href="Register.aspx" class="float-right mt-3" style="color:#fff;font-size:18px;"><span class="icon-user2"></span> Register</a>
					</div>
				</div>
			</div>
		</div>
        <div class="cz-container">
            <div class="body_container">
                <h2>Privacy Policy</h2>
                <div class="clearfix"></div>

                <h5 class="pt-4">General</h5>
                <p>This privacy policy applies to all services provided by IBYTA TRAVELS LLC and its subsidiaries, who shall be collectively referred to as “we” or “us” herein this notice.</p>

                <p>As a customer, you are in charge of your travel-planning. We know you want to remain in charge of your personal data as well. We value your trust and make it a high priority to ensure the security and confidentiality of the personal data you provide to us. It is a responsibility we take very seriously.</p>

                <p>We strive to secure your data and ensure it is not used for any unlawful activities and/or purposes other than those you have given permission for. We do so by enforcing data privacy and protection in our design phase. This concept is endorsed by the leadership group and the entire team. We recommend you review this Notice and become familiar with it.</p>

                <p>By providing personal data to us, you agree that this Notice will apply to how we handle your personal data. Furthermore, you consent to us collecting, processing and disclosing your personal data where the lawful basis for the same is contractual necessity. Under any other circumstance, we shall collect your consent before collecting, processing or sharing your data. If you do not agree with any part of this Notice, you must not provide your personal data to us.</p>

                <p>If you do not provide us with your personal data, or if you withdraw a consent that you have given under this Notice, this may affect our ability to provide services to you or negatively impact the services we can provide to you.</p>

                <p>With respect to General Data Protection Regulation 2016/679 (“GDPR”), we have taken the relevant steps to ensure compliance. We are committed to upholding data privacy rights of our customers who are residents of the European Union by ensuring compliance. </p>

                
                <h5 class="pt-4">What data do we collect and how is it used</h5>
                <p> In order to process the service requested and to ensure that your travel arrangements meet your requirements we need to use the information you provide. We collect data whenever you avail a service from us.</p>

                <p>We take full responsibility for ensuring that proper security measures are in place to protect your information. Under GDPR, the lawful basis for our processing activities fall under contractual necessity or legitimate interest. In situations where you share information regarding health concerns, the lawful basis is vital interests. Your consent, our processing activities and the data involved is clearly documented.</p>

                <p>We predominantly process the following types of personal data about you:</p>

                <ul>
                    <li>Contact information (such as name, telephone number, email address, residential/mailing address);</li>
                    <li>Passport details</li>
                    <li>Loyalty program / frequent flyer details</li>
                    <li>Information about your dietary requirements and health issues</li>
                    <li>Other details relevant to your travel arrangements or to the relevant travel service provider(s)</li>
                    <li>Information from our website such as device model, IP address, the type of browser being used, usage pattern through cookies and browser settings, query logs and product usage logs.</li>
                </ul>
                <p>The purposes for which we collect personal data include:</p>


                <h5 class="pt-4">Service Fulfilment:</h5>
                <ul>
                    <li>To fulfil  the services requested</li>
                    <li>To send you requested information about the Service(s)</li>
                    <li>To respond to customer service requests, questions and concerns;</li>
                    <li>For identification of fraud or error, regulatory reporting and compliance;</li>
                    <li>For internal accounting and administration;</li>
                    <li>To comply with our legal obligations and any applicable customs/immigration requirements</li>
                    <li>For other purposes as authorized or required by law</li>
                </ul>


                <h5 class="pt-4">Marketing:</h5>
                <ul>
                    <li>For servicing our relationship with you and maintaining our brands to service you better</li>
                    <li>For presenting options on our website, we think may interest you</li>
                    <li>To facilitate your participation in loyalty programs;</li>
                    <li>Use your personal data for customer engagement and to send electronic marketing materials to You including e-newsletters, email, SMS, social media channels etc. if you have opted-in to receive them.</li>
                </ul>


                <h5 class="pt-4">Analytics:</h5>
                <p>We collect certain personal data of yours when you visit our websites. We use such data to</p>
                <ul>
                    <li>Assess needs of your business to determine or suggest suitable Service(s) (e.g.: redirect to corresponding domain as per customer location)</li>
                    <li>For analytical purposes based on use of our websites </li>
                    <li>For research and analysis in relation to our business and services, including trends and preferences in sales and travel destinations </li>
                    <li>Involve you in market research, evaluate customer satisfaction and seek feedback regarding our relationship and the service we have provided </li>
                </ul>


                <h5 class="pt-4">With whom is your personal data shared</h5>
                <p>
                    We do not and will not sell, rent out or trade your personal data. We must pass the information on to our partners such as airlines, hotels, transport companies immigration departments, government authorities etc. depending on the service provided to you. Our partners are professional and reputed firms that are leaders in their respective domains. Where data is shared, we take necessary steps to ensure confidentiality by implementing measures such as data pseudonymizing,wherever feasible. The information may also be provided to security or credit checking companies, public authorities such as customs/immigration if required by them, or as required by law.
                </p>


                <h5 class="pt-4">What are cookies and do we use them</h5>
                <p>A cookie is a text-only string of information that a website transfers to the cookie file of the browser on your computer's hard disk so that the website can remember who you are.<br />
                The website uses cookies for the following general purposes:</p>

                <ul>
                    <li>To help recognize your browser as a previous visitor and save and remember any preferences that may have been set while your browser was visiting our site.</li>
                    <li>To help us customize the content and advertisements provided to you on this website and on other sites across the Internet.</li>
                </ul>
                <p>We cannot accept responsibility for any issues concerning your data collected by websites that are not directly under our control. Please also be aware that individual organizations operate their own policies regarding the use and sale of personal data and the use of cookies. If you have a concern regarding the way your personal data will be used on other sites then you are advised to read the relevant privacy statement.</p>


                 <h5 class="pt-4">How to control and delete cookies</h5>
                <p>If you would like to restrict or block the cookies which are set by us, or any other website, you can do this through your browser settings. You can opt-in or opt-out of our cookies by selecting the appropriate options on our cookie pop-up that appears on the website. For information on how to delete or restrict cookies on your mobile phone refer to your handset manual. Please be aware that restricting cookies may impact the functionality of our website.</p>


                <h5 class="pt-4">How we protect your information</h5>
                <p>We want you to feel confident about working with us to plan and purchase your travel, so we are committed to protecting the information we collect. We have detailed policies and procedures in place to ensure confidentiality and rightful use of the data. We have implemented appropriate administrative, technical, and physical security procedures. We review our information collection, storage and processing practices periodically.We have physical security measures and access control systems to guard against unauthorized access. We restrict access to personal data to those who require access for fulfilment of service. Access permissions are subject to strict contractual confidentiality obligations</p>


                <h5 class="pt-4">Data Retention</h5>
                <p>We retain your data for the duration of your association with us as the data that we retain is necessary for completion of our services and improving your experience as a customer. You can request the erasure of your data once you wish to end your association with us. At the point, your personal data will be removed from our internal servers and storage</p>




                <h5 class="pt-4">Data Breaches</h5>
                <p>Under GDPR, we liable to report a significant data breach to the concerned parties within 72 hours of discovery. We ensure that security measures are in place in order to ensure that a data breach is identified and the necessary mitigation actions are implemented immediately. We have defined policies and standard operating procedures with respect to data breach identification, mitigation and reporting.</p>

                <h5 class="pt-4">Children’s Privacy</h5>
                <p>Should a child whom we know to be under 16 send personal data to us, we will use that information only to respond directly to that child to inform him or her that we must have parental consent or consent from a guardian in order to fulfil  their request</p>


                <h5 class="pt-4">External Links</h5>
                <p>If any part of our website links you to other websites, those websites do not operate under our Privacy Policy. We recommend you examine the privacy statements posted on those websites to understand their procedures for collecting, using, and disclosing personal data.</p>

                
                <h5 class="pt-4">Third Parties</h5>
                <p>We do not intend our third-party service providers to use your personal data for their own purposes. We only permit them to process your personal data for specified purposes in accordance to our contractual terms. We do not collect or store any of your credit card information. All payments you make for the purchase of our services using your credit card are securely processed by the concerned banks/payment gateway.</p>


                
                <h5 class="pt-4">Your rights in relation to the personal data we collect</h5>
                <p>As Data Subjects, under GDPR, you are entitled to:</p>
                
                <ul>
                    <li>Access i.e. to update, rectify, and erase your personal data held by us</li>
                    <li>Restrict us from processing or profiling any of your personal data,</li>
                    <li>Request to get your personal data in a machine-readable language</li>
                    <li>A responsible point of contact within the company i.e. a Data Protection Officer for any queries regarding your data and privacy</li>
                    <li>Withdraw consent for any processing activities you have consented to before </li>
                </ul>
                <p>You can request any of these features and any other queries regarding your data by emailing us at legal@ibyta.com. We try to respond to all legitimate requests within one month. Occasionally it may take us longer than a month if your request is particularly complex or you have made a number of requests.</p>
                <p>We reserve the right to deny you access for any reason permitted under applicable laws. Such exemptions may include national security, corporate finance and confidential references.</p>
                <p>Pertaining to your right to be forgotten i.e. erasure of your personal data, we will remove your personal data from our storage sources. However, logs of transactions between you and the services we provide will be retained for internal audits and similar purposes.</p>
                <p>You must always provide accurate information and you agree to update it whenever necessary. You also agree that, in the absence of any update, we can assume that the information submitted to us is correct.</p>
                <p>In any of the situations listed above, we may request that you prove your identity by providing us with a copy of a valid means of identification for us to comply with our security obligations and to prevent unauthorized disclosure of personal data.</p>


                     
                <h5 class="pt-4">Hiring Disclaimer:</h5>
                <p>We are aware of some individuals and organizations who are not associated with Ibyta Travels LLC, sending emails or otherwise contacting candidates to make fraudulent offers of employment with Ibyta Travels LLC. These people and organizations may request personal data or money from you in order to process the application. We would strongly advise you not to provide your bank account or credit card details as part of an employment application.</p>
                <p>Our recruitment department will never email you from personal email accounts such as Hotmail, Yahoo or Gmail. Ibyta Travels LLC’s email addresses always end in "@ibyta.com. Ibyta Travels LLC never charges fees for recruitment or visa processing or recruitment deposit. Ibyta Travels LLC will never make a job offer without meeting a candidate face to face or via video conference / Skype. If you are asked for money as part of any Ibyta Travels LLC recruitment, please do not pay and report it to <a href="mailto:legal@ibyta.com">legal@ibyta.com</a> with all accompanying e-mails and documents.</p>


                     
                <h5 class="pt-4">Changes to this Privacy Policy</h5>
                <p>By visiting this website, you are accepting the practices described herein. Please note that we review our Privacy Policy from time to time, therefore, you may wish to periodically review this page to make sure that you have the current version.<br />
Thank you for using the services of Ibyta Travels LLC and its subsidiaries !</p>


            </div>
        </div>
         <div class="ibyta-footer">
            <div class="services-list">
                <ul class="list-unstyled text-center mb-0">
                    <li class="d-inline-block">Hotels</li>
                    <li class="d-inline-block">Apartments</li>
                    <li class="d-inline-block">Tours</li>
                    <li class="d-inline-block">Transfers</li>
                    <li class="d-inline-block">Insurance</li>
                    <li class="d-inline-block">Visas</li>
                </ul>                       
            </div>  
            <div class="contact-info">
                    Tel : +97144065891 + 9714 4065892 <em> | </em> suppliers : <a href="mailto:contracting@ibyta.com">contracting@ibyta.com</a> <em> | </em> customers : <a href="mailto:sales@ibyta.com">sales@ibyta.com</a> 
                    <div class="small pt-2 text-secondary">
                    <a class="text-secondary" href="IbytaTerms.aspx">Terms and Conditions</a> | <a class="text-secondary" href="IbytaPrivacyPolicy.aspx">Privacy Policy</a> | <a class="text-secondary" href="IbytaContact.aspx">Contact Us</a>
                    </div>
            </div>
            <div class="copyright">
                    Copyright 2019 Ibyta, UAE . All Rights Reserved.
            </div>          
         </div>
</body>
</html>
