using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using CT.GlobalVisa;
using CT.Core;
using CT.GlobasVisa;

public partial class RequirementMasterUI : CT.Core.ParentPage
{
  
    private string REQUIREMENT_SESSION = "_requirementSession";

    private string REQUIREMENT_SEARCH_SESSION = "_requirementSearchList";
    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
                if (Session["getGuid"] == null)
                {
                    Session["getGuid"] = Guid.NewGuid();
                }
            }
            lblErrorMsg.Text = string.Empty;
            lblSuccessMsg.Text = string.Empty;
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    private void InitializePageControls()
    {
        try
        {
            BindCountryList();
            BindNationalityList();
            BindVisaType(ddlCountry.SelectedValue);
            BindResidence();
        }
        catch
        {
            throw;
        }
    }
    #endregion

    #region gvLocation Events
    private void BindGrid()
    {
        
        try
        {
            CommonGrid grid = new CommonGrid();
           // grid.BindGrid(gvLocation, LocationMaster.GetList(ListStatus.Short, RecordStatus.Activated));
           // setRowId();
        }
        catch
        {
            throw;
        }
    }

   
    private void BindCountryList()
    {
        try
        {
            ddlCountry.DataSource = Country.GetCountryList();
            ddlCountry.DataValueField = "Value";
            ddlCountry.DataTextField = "Key";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "-1"));

        }
        catch { throw; }
    }

    private void BindResidence()//Field Master
    {
        try
        {
            ddlResidence.DataSource = Country.GetCountryList();
            ddlResidence.DataTextField = "Key";
            ddlResidence.DataValueField = "Value";
            ddlResidence.DataBind();
            ddlResidence.Items.Insert(0, new ListItem("--Select Residence--", "-1"));
            ddlResidence.Items.Insert(1,new ListItem("All","0"));
        }
        catch { throw; }
    }
    private void BindNationalityList()
    {
        try
        {
            ddlNationality.DataSource = Country.GetNationalityList();
            ddlNationality.DataTextField = "Key";
            ddlNationality.DataValueField = "Value";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("--Select Nationality--", "-1"));
            ddlNationality.Items.Insert(1, new ListItem("All", "0"));
        }
        catch { throw; }
    }

    private void BindVisaType(string countryCode)
    {
        ddlVisaType.DataSource = VisaTypeMaster.GetListBYCountryCode(ListStatus.Short, RecordStatus.Activated, countryCode);
        ddlVisaType.DataTextField = "VISA_TYPE_NAME";
        ddlVisaType.DataValueField = "VISA_TYPE_ID";
        ddlVisaType.DataBind();
        ddlVisaType.Items.Insert(0, new ListItem("--Select Visa Type--", "-1"));
        ddlVisaType.Items.Insert(1, new ListItem("All", "0"));
    }
    
  
    private void bindSearch()
    {
        try
        {
            LoginInfo loginfo = Settings.LoginInfo;
            DataTable dt = GVRequirementMaster.GetList(ListStatus.Long, RecordStatus.All);
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch { throw; }
    }

    

      protected void btnSave_Click(object sender, EventArgs e)
      {
          try
          {
            Save();
          }
          catch (Exception ex)
          {
              Utility.WriteLog(ex, this.Title);
              Label lblMasterError = (Label)this.Master.FindControl("lblError");
              lblMasterError.Visible = true;
              lblMasterError.Text = ex.Message;
              Utility.Alert(this.Page, ex.Message);
          }
      }
      protected void btnCancel_Click(object sender, EventArgs e)
      {
          try
          {
              //chkCopyFrom.Checked = false;
              Clear();                           
          }
          catch (Exception ex)
          {
              Utility.WriteLog(ex, this.Title);
              Label lblMasterError = (Label)this.Master.FindControl("lblError");
              lblMasterError.Visible = true;
              lblMasterError.Text = ex.Message;
          }
      }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
         
            this.Master.ShowSearch("Search");
            Clear();
            bindSearch();                     
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
   
    private void Clear()
    {
        try
        {
            RequirementMaster  terms = new RequirementMaster ();
            txtHeader.Text = string.Empty;
            txtDescription.Text = string.Empty;
           // if (!chkCopyFrom.Checked)
            {
                ddlCountry.SelectedIndex = 0;
                ddlNationality.SelectedIndex = 0;
                ddlResidence.SelectedIndex = 0;
                
                ddlVisaType.Items.Clear();
                ddlVisaType.Items.Insert(0, new ListItem("--Select Visa Type--", "-1"));
                ddlVisaType.Items.Insert(1, new ListItem("All", "0"));
                ddlVisaType.SelectedIndex = 0;
            }           
            btnSave.Text = "Save";
            CurrentObject = null;
            Session["getGuid"] = null;
        }
        catch { throw; }      
    
    }

    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[REQUIREMENT_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["REQ_ID"] };
            Session[REQUIREMENT_SEARCH_SESSION] = value;
        }
    }

    private GVRequirementMaster  CurrentObject
    {
        get
        {
            return (GVRequirementMaster)Session[REQUIREMENT_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(REQUIREMENT_SESSION);
            }
            else
            {
                Session[REQUIREMENT_SESSION] = value;
            }

        }

    }
    private void Save()
    {

        try
        {
            GVRequirementMaster  requirement;
            if (CurrentObject == null)
            {
                requirement = new GVRequirementMaster ();
            }
            else
            {
                requirement  = CurrentObject;
            }

            if (!string.IsNullOrEmpty(txtHeader.Text))
            {
                requirement.Header = txtHeader.Text.ToString();
            }
            else {
                throw new Exception("Please enter the Header");
            }
            if (!string.IsNullOrEmpty(txtDescription.Text))
            {
                requirement.Description = txtDescription.Text.ToString();
            }
            else {
                throw new Exception("Please enter the Description");
            }

            //requirement.Path = ConfigurationManager.AppSettings["PROPERITY_DOC_PATH"];
            if (!string.IsNullOrEmpty(ddlCountry.SelectedValue.ToString()) && ddlCountry.SelectedValue.ToString() != "-1")
            {
                requirement.CountryID = ddlCountry.SelectedItem.Value;
            }
            else {
                throw new Exception("Please select country");
            }
            if (!string.IsNullOrEmpty(ddlNationality.SelectedValue.ToString()) && ddlNationality.SelectedValue.ToString() != "-1")
            {
                requirement.NationalityId = ddlNationality.SelectedItem.Value;
            }
            else {
                throw new Exception("Please select the Nationality");
            }
            if (!string.IsNullOrEmpty(ddlVisaType.SelectedValue.ToString()) && ddlVisaType.SelectedValue.ToString() != "-1")
            {
                requirement.VisaTypeId = Utility.ToLong(ddlVisaType.SelectedItem.Value);
            }
            else {
                throw new Exception("Please Select visaType");
            }
            if (!string.IsNullOrEmpty(ddlResidence.SelectedValue.ToString()) && ddlResidence.SelectedValue.ToString() != "-1")
            {
                requirement.ResidenceId = ddlResidence.SelectedItem.Value;
            }
            else {
                throw new Exception("Please Select Residence");
            }
            
            requirement.Status = Settings.ACTIVE;
            requirement.CreatedBy = Settings.LoginInfo.UserID;
            requirement.Save();                
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage("requirement details are", "", (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            Clear();
       }

       catch
       { throw; }
    }

    private void Edit(long id)
    {
        try
        {
            GVRequirementMaster requirement = new GVRequirementMaster(id);
            CurrentObject = requirement;
            Session["getGuid"] = CurrentObject.TransactionId;
            txtHeader.Text=Utility.ToString(requirement.Header);
            txtDescription.Text=Utility.ToString(requirement.Description);
            ddlCountry.SelectedValue=Utility.ToString(requirement.CountryID);
            BindVisaType(ddlCountry.SelectedItem.Value);
            ddlNationality.SelectedValue =Utility.ToString(requirement.NationalityId);
            ddlVisaType.SelectedValue = Utility.ToString(requirement.VisaTypeId);
            ddlResidence.SelectedValue = Utility.ToString(requirement.ResidenceId);                       
            btnSave.Text = "Update";
            //if (chkCopyFrom.Checked)
            //{
            //    txtHeader.Text = string.Empty;
            //    txtDescription.Text = string.Empty;
            //    btnSave.Text = "Save";
            //}
            // BindGrid();
        }
        catch
        {
            throw;
        }
    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            long requirementId = Utility.ToLong(gvSearch.SelectedValue);
            Edit(requirementId);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }

    }

  
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{ "HTtxtHeader", "req_header" },
            { "HTtxtCountry", "req_country_name" },{ "HTtxtNationality", "req_nationality_name" },{"HTxtVisaType","req_visa_type_name"},
            {"HTtxtResidence","req_residence_name"}};
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
            //{"HTtxtDescription", "req_description" }, 
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    #endregion

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindVisaType(ddlCountry.SelectedValue);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }
}
