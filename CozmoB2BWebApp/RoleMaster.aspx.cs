﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;

public partial class RoleMasterGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    private string ROLE_DETAILS_SESSION = "_TranxRoleDetails";

    private string ROLE_DETAILS_LIST_SESSION = "_RoleDetailsList";

    //private string ROLE_SEARCH_SESSION = "_RoleMasterSearchList";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            // Session["RCPT_TICKET_ID"] = "aaaa";
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {

                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
                //Clear();
                lblSuccessMsg.Text = string.Empty;

            }
            //StartupScript(this.Page, " showHidMode(" + ddlSettlementMode.SelectedItem.Value + ")", " showHidMode");
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);

        }
    }

    private void InitializePageControls()
    {
        try
        {

            Clear();
            // BindGrid();


        }
        catch { throw; }
    }

    # region Properties
    private DataTable RoleDetails
    {
        get
        {
            return (DataTable)Session[ROLE_DETAILS_LIST_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["rd_func_id"] };
            Session[ROLE_DETAILS_LIST_SESSION] = value;
        }
    }
    private RoleMaster CurrentObject
    {
        get
        {
            return (RoleMaster)Session[ROLE_DETAILS_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(ROLE_DETAILS_SESSION);
            }
            else
            {
                Session[ROLE_DETAILS_SESSION] = value;
            }

        }

    }
    # endregion
    //    # region private Methods
    private void Clear()
    {
        try
        {
            gvRoleDetails.EditIndex = -1;
            RoleMaster tempRoles = new RoleMaster();
            RoleDetails = tempRoles.RoleDetails;

            txtRole.Text = string.Empty;
            lblSuccessMsg.Text = string.Empty;
            lblSuccessMsg.Visible = false;
            btnSave.Text = "Save";
            CurrentObject = null;
            // hdfmapingDetailsMode.Value = "0";
            BindGrid();

        }
        catch
        {
            throw;
        }
    }

    //private DataTable SearchList
    //{
    //    get
    //    {
    //        return (DataTable)Session[ROLE_SEARCH_SESSION];
    //    }
    //    set
    //    {
    //        value.PrimaryKey = new DataColumn[] { value.Columns["LOCATION_ID"] };
    //        Session[ROLE_SEARCH_SESSION] = value;
    //    }
    //}

    private void Save()
    {
        try
        {
            RoleMaster roles;
            if (CurrentObject == null)
            {
                roles = new RoleMaster();
            }
            else
            {
                roles = CurrentObject;
            }

            roles.RoleName = txtRole.Text.Trim();
            roles.Status = Settings.ACTIVE;
            string roleName = txtRole.Text.Trim();

            roles.CreatedBy = Settings.LoginInfo.UserID;

            roles.RoleDetails = RoleDetails;
            roles.Save();
            Clear();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage("Role details of ", roleName, (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
        }
        catch
        {
            throw;
        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            bindSearch();
            //Clear();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    private void BindGrid()
    {

        try
        {
            CommonGrid grid = new CommonGrid();
            grid.BindGrid(gvRoleDetails, RoleDetails);
            //setRowId();
        }
        catch
        {
            throw;
        }
    }



    protected void HTchkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
            CheckBox chkSelectAll = (CheckBox)gvHdrRow.FindControl("HTchkSelectAll");


            foreach (GridViewRow gvRow in gvRoleDetails.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                chkSelect.Checked = chkSelectAll.Checked;
                long funcId = Utility.ToLong(gvRoleDetails.DataKeys[gvRow.RowIndex].Value);
                SetRowStatus(funcId, chkSelect.Checked);

            }
        }
        catch { throw; }
    }

    protected void ITchkSelect_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
            CheckBox chkSelectOne = (CheckBox)gvHdrRow.FindControl("ITchkSelect");
            long functionId = Utility.ToLong(gvRoleDetails.DataKeys[gvHdrRow.RowIndex].Value);
            SetRowStatus(functionId, chkSelectOne.Checked);
            //BindGrid();

        }
        catch { throw; }
    }

    private void SetRowStatus(long functionId, Boolean check)
    {
        try
        {
            DataRow dr = RoleDetails.Rows.Find(functionId);
            if (dr != null)
            {
                if (check)
                {
                    RoleDetails.Rows.Find(functionId)["RD_STATUS"] = (char)RecordStatus.Activated;
                    RoleDetails.Rows.Find(functionId)["checked_status"] = (char)RecordStatus.Activated;
                }
                else
                {
                    RoleDetails.Rows.Find(functionId)["RD_STATUS"] = (char)RecordStatus.Deactivated;
                    RoleDetails.Rows.Find(functionId)["checked_status"] = (char)RecordStatus.Deactivated;
                }
            }

            if (Utility.ToLong(RoleDetails.Rows.Find(functionId)["RD_ID"]) > 0)
            {
                dr.AcceptChanges();
                dr.SetModified();
            }
            else
            {
                dr.AcceptChanges();
                dr.SetAdded();
            }


        }
        catch { throw; }

    }
    protected void gvRoleDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvRoleDetails.PageIndex = e.NewPageIndex;
            gvRoleDetails.EditIndex = -1;
            BindGrid();

        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void Filter_Click(object sender, EventArgs e)
    {
        try
        {
            // selectedItem();
            //BindGrid(); 
            string[,] textboxesNColumns = { { "HTtxtPageName", "PageName" }, { "HTtxtParentMenu", "ParentMenu" } };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvRoleDetails, RoleDetails.Copy(), textboxesNColumns);

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    # region Content Search
    private void bindSearch()
    {
        try
        {
            LoginInfo loginfo = Settings.LoginInfo;
            DataTable dt = RoleMaster.GetList(ListStatus.Long, RecordStatus.All);
            //SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch { throw; }
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Utility.WriteLog("ziya1", this.Title);
            long roleId = Utility.ToLong(gvSearch.SelectedValue);
            Utility.WriteLog("ziya2", this.Title);
            Edit(roleId);
            Utility.WriteLog("ziya3", this.Title);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{ "HTtxtRole", "role_name" },{"HTtxtStatus", "role_status" } };
            DataTable dt = RoleMaster.GetList(ListStatus.Long, RecordStatus.All);
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, dt, textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    private void Edit(long id)
    {
        try
        {
            RoleMaster roles = new RoleMaster(id);
            CurrentObject = roles;
            txtRole.Text = Utility.ToString(roles.RoleName);
            RoleDetails = roles.RoleDetails;
            // if (RoleDetails.Rows.Count>0 )txtMapLocation.Text = Utility.ToString(RoleDetails.Rows[0]["map_location_code"]);    

            btnSave.Text = "Update";
            btnClear.Text = "Clear";
            BindGrid();
        }
        catch
        {
            throw;
        }
    }
    # endregion


}
