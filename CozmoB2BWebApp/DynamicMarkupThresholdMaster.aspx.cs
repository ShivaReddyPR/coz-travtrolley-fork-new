﻿using CT.TicketReceipt.Common;
using System;
using CT.BookingEngine;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using CT.TicketReceipt.BusinessLayer;

namespace CozmoB2BWebApp
{
    public partial class DynamicMarkupThresholdMaster :CT.Core.ParentPage
    {
        int dynId;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Request.QueryString["dynId"] != null && Request.QueryString["dynId"] != string.Empty)
            {
                dynId = Convert.ToInt32(Request.QueryString["dynId"]);
            }

        }
        #region methods
        private void Save()
        {
            try
            {
                DynamicMarkupThreshold markupThreshold = new DynamicMarkupThreshold();  
                if (Utility.ToInteger(hdfId.Value)>0)
                {
                    markupThreshold.ThresholdId = Utility.ToInteger(hdfId.Value);
                }
                else
                {
                    markupThreshold.ThresholdId = -1;
                }
                markupThreshold.FromAmount =Utility.ToDecimal (txtFromAmount.Text);
                markupThreshold.ToAmount = Utility.ToDecimal(txtToAmount.Text);
                markupThreshold.MarkupValue = Utility.ToDecimal(txtMarkupValue.Text);
                markupThreshold.MarkupType = Utility.ToString(ddlMarkupType.SelectedItem.Value);
                markupThreshold.Created_by = Settings.LoginInfo.UserID;
                markupThreshold.Save(dynId);
                lblSucessMsg.Visible = false;
                lblSucessMsg.Text = Formatter.ToMessage("Details Successfully", "", hdfId.Value == "0" ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);
                btnSave.Text = "Save";
                Clear();
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        private void Clear()
        {
            ddlMarkupType.SelectedIndex = -1;
            txtFromAmount.Text = string.Empty;
            txtMarkupValue.Text = string.Empty;
            txtToAmount.Text = string.Empty;
        }
        private void Edit(int ThresholdId)
        {
            try
            {
                DynamicMarkupThreshold markupThreshold = new DynamicMarkupThreshold(ThresholdId);
                hdfId.Value = Utility.ToString(markupThreshold.ThresholdId);
                markupThreshold.DynId = Utility.ToInteger(dynId);
                txtFromAmount.Text =Utility.ToString (markupThreshold.FromAmount);
                txtToAmount.Text = Utility.ToString(markupThreshold.ToAmount);
                txtMarkupValue.Text = Utility.ToString(markupThreshold.MarkupValue);
                ddlMarkupType.SelectedValue = Utility.ToString(markupThreshold.MarkupType);
                btnSave.Text = "Update";
                btnClear.Text = "Cancel";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindSearch()
        {
            try
            {
                DataTable dt = DynamicMarkupThreshold.GetDynamicMarkupThresholdMasterDetails();
                if (dt.Rows.Count > 0)
                {
                    gvSearch.DataSource = dt;
                    gvSearch.DataBind();
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Button Clicking events
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.ShowSearch("Search");
                BindSearch();

            }
            catch(Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        #endregion
        #region gridview events
        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                int ThresholdId = Utility.ToInteger(gvSearch.SelectedValue);
                Edit(ThresholdId);
                this.Master.HideSearch();

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                BindSearch();

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        #endregion

    }
}