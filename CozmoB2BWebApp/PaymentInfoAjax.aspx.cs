﻿using System;
using CT.AccountingEngine;
using System.Text;
using System.Data;

public partial class PaymentInfoAjax : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        StringBuilder paymentInfoBlock;
        string blockId = string.Empty;
        if (Request["isPaymentInfo"] == "true")
        {
            try
            {
                int bookingId = 0;
                blockId = Convert.ToString(Request["blockId"]);

                bool isNumerice = int.TryParse(Request["bookingId"], out bookingId);
                if (isNumerice)
                {
                    CreditCardPaymentInformation paymentInfo = new CreditCardPaymentInformation();
                    paymentInfo.Load(bookingId);
                    if (paymentInfo.PaymentId != null)
                    {
                        paymentInfoBlock = new StringBuilder();
                        paymentInfoBlock.Append("<div class=\"fleft visa_PaymentInfo_top\">");
                        paymentInfoBlock.Append("<span class=\"fleft\">Payment Information</span>");
                        paymentInfoBlock.Append("<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" style=\" color:#ffffff;\" onclick=\"document.getElementById('PaymentInfo-" + blockId + "').style.display='none'\">Close</a></label></div>");

                        paymentInfoBlock.Append("<div style='width:680px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">");
                        paymentInfoBlock.Append("<span class=\"fleft width-90 margin-right-5\" style='width:180px;'><b>Order Id</b></span>");
                        paymentInfoBlock.Append("<span class=\"fleft width-150 margin-right-5\" style='width:130px;'><b>Payment Id</b></span>");
                        paymentInfoBlock.Append("<span class=\"fleft width-90 margin-right-5\" style='width:100px;'><b>Amount</b></span>");
                        paymentInfoBlock.Append("<span class=\"fleft width-60 margin-right-5\" style='width:100px;'><b>Charges</b></span>");
                        paymentInfoBlock.Append("<span class=\"fleft width-60px \" style='width:130px;'><b>Payment Sources</b></span>");
                        paymentInfoBlock.Append("</div>");

                        paymentInfoBlock.Append("<div style='width:680px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">");
                        paymentInfoBlock.Append("<span class=\"fleft width-90  margin-right-5\" style='width:180px;'>" + paymentInfo.TrackId + "</span>");
                        paymentInfoBlock.Append("<span class=\"fleft width-150 margin-right-5\" style='width:130px;'>" + paymentInfo.PaymentId + "</span>");
                        paymentInfoBlock.Append("<span class=\"fleft width-90 margin-right-5\" style='width:100px;'>" + paymentInfo.Amount.ToString("#0.00") + "</span>");
                        paymentInfoBlock.Append("<span class=\"fleft width-90 margin-right-5\" style='width:100px;'>" + paymentInfo.Charges.ToString("#0.00") + "</span>");
                        paymentInfoBlock.Append("<span class=\"fleft width-60px \" style='width:130px;'>" + ((PaymentGatewaySource)paymentInfo.PaymentGateway + "</span>"));
                        paymentInfoBlock.Append("</div></div>");

                        Response.Write(paymentInfoBlock.ToString());
                    }
                    else
                    {
                        paymentInfoBlock = new StringBuilder();
                        paymentInfoBlock.Append("<div class=\"fleft visa_PaymentInfo_top\">");
                        paymentInfoBlock.Append("<span style='padding-left:250px; color:#ffffff; font-weight:bold;'> Payment Information Not Available.</span>");
                        paymentInfoBlock.Append("<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" style=\" color:#ffffff;\" onclick=\"document.getElementById('PaymentInfo-" + blockId + "').style.display='none'\">Close</a></label>");
                        paymentInfoBlock.Append("</div>");

                        Response.Write(paymentInfoBlock.ToString());
                    }
                }

            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.Normal, 0, "Failed to load Payament Details. Error: " + ex.Message, "");

                paymentInfoBlock = new StringBuilder();
                paymentInfoBlock.Append("<div class=\"fleft visa_PaymentInfo_top\">");
                paymentInfoBlock.Append("<span style='padding-left:250px; color:#ffffff; font-weight:bold;'>" + ex.Message + "</span>");
                paymentInfoBlock.Append("<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" style=\" color:#ffffff;\" onclick=\"document.getElementById('PaymentInfo-" + blockId + "').style.display='none'\">Close</a></label>");
                paymentInfoBlock.Append("</div>");

                Response.Write(paymentInfoBlock.ToString());
            }

        }
        else if (Request["isInsurancePaymentInfo"] == "true")
        {
            try
            {
                int planId = Convert.ToInt32(Request["planId"]);
                blockId = Convert.ToString(Request["blockId"]);
                DataTable dtPaymentInfo = CT.BookingEngine.InsuranceQueue.Load(planId);

                if (dtPaymentInfo.Rows.Count > 0)
                {
                    DataRow dr = dtPaymentInfo.Rows[0];
                    paymentInfoBlock = new StringBuilder();
                    paymentInfoBlock.Append("<div class=\"fleft visa_PaymentInfo_top\">");
                    paymentInfoBlock.Append("<span class=\"fleft\">Payment Information</span>");
                    paymentInfoBlock.Append("<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" style=\" color:#ffffff;\" onclick=\"document.getElementById('PaymentInfo-" + blockId + "').style.display='none'\">Close</a></label></div>");

                    paymentInfoBlock.Append("<div style='width:680px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">");
                    paymentInfoBlock.Append("<span class=\"fleft width-90 margin-right-5\" style='width:130px;'><b>Order Id</b></span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-150 margin-right-5\" style='width:130px;'><b>Payment Id</b></span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-90 margin-right-5\" style='width:130px;'><b>Amount</b></span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-60 margin-right-5\" style='width:130px;'><b>Charges</b></span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-60px \" style='width:130px;'><b>Payment Sources</b></span>");
                    paymentInfoBlock.Append("</div>");

                    paymentInfoBlock.Append("<div style='width:680px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">");
                    paymentInfoBlock.Append("<span class=\"fleft width-90  margin-right-5\" style='width:130px;'>" + (dr["orderId"] == DBNull.Value ? "N/A" : Convert.ToString(dr["orderId"])) + "</span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-150 margin-right-5\" style='width:130px;'>" + (Convert.ToString(dr["paymentId"])) + "</span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-90 margin-right-5\" style='width:130px;'>" + (Convert.ToDecimal(dr["paymentAmount"])).ToString("#0.00") + "</span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-90 margin-right-5\" style='width:130px;'>" + (dr["CCCharges"] == DBNull.Value ? "0.00" : Convert.ToDecimal(dr["CCCharges"]).ToString("#0.00")) + "</span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-60 margin-right-5\" style='width:130px;'>" + (PaymentGatewaySource)Convert.ToInt32(dr["paymentGatewaySourceId"]) + "</span>");
                    paymentInfoBlock.Append("</div></div>");

                    Response.Write(paymentInfoBlock.ToString());


                }
                else
                {
                    paymentInfoBlock = new StringBuilder();
                    paymentInfoBlock.Append("<div class=\"fleft visa_PaymentInfo_top\">");
                    paymentInfoBlock.Append("<span style='padding-left:250px; color:#ffffff; font-weight:bold;'>Payment Information Not Available. </span>");
                    paymentInfoBlock.Append("<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" style=\" color:#ffffff;\" onclick=\"document.getElementById('PaymentInfo-" + blockId + "').style.display='none'\">Close</a></label>");
                    paymentInfoBlock.Append("</div>");

                    Response.Write(paymentInfoBlock.ToString());
                }

            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.Normal, 0, "Failed to load Payament Details. Error: " + ex.Message, "");
                paymentInfoBlock = new StringBuilder();
                paymentInfoBlock.Append("<div class=\"fleft visa_PaymentInfo_top\">");
                paymentInfoBlock.Append("<span style='padding-left:250px; color:#ffffff; font-weight:bold;'>" + ex.Message + "</span>");
                paymentInfoBlock.Append("<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" style=\" color:#ffffff;\" onclick=\"document.getElementById('PaymentInfo-" + blockId + "').style.display='none'\">Close</a></label>");
                paymentInfoBlock.Append("</div>");

                Response.Write(paymentInfoBlock.ToString());

            }


        }
       //Added by Harish Regarding Package Payment Information
        else if (Request["isPackagePaymentInfo"] == "true")
        {
           
            if (Request["orderId"] != null)
            {
                CreditCardPaymentInformation paymentInfo = new CreditCardPaymentInformation();
                paymentInfo.getPaymentDetailsByOrderId(Request["orderId"].ToString());
                if (paymentInfo.PaymentId != null)
                {
                    paymentInfoBlock = new StringBuilder();
                    paymentInfoBlock.Append("<div class=\"fleft visa_PaymentInfo_top\">");
                    paymentInfoBlock.Append("<span class=\"fleft\">Payment Information</span>");
                    paymentInfoBlock.Append("<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" style=\" color:#ffffff;\" onclick=\"document.getElementById('PaymentInfo-" + Convert.ToString(Request["blockId"]) + "').style.display='none'\">Close</a></label></div>");

                    paymentInfoBlock.Append("<div style='width:680px;margin-top:30px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">");
                    paymentInfoBlock.Append("<span class=\"fleft width-90 margin-right-5\" style='width:180px;'><b>Order Id</b></span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-150 margin-right-5\" style='width:130px;'><b>Payment Id</b></span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-90 margin-right-5\" style='width:100px;'><b>Amount</b></span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-60 margin-right-5\" style='width:100px;'><b>Charges</b></span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-60px \" style='width:130px;'><b>Payment Sources</b></span>");
                    paymentInfoBlock.Append("</div>");

                    paymentInfoBlock.Append("<div style='width:680px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">");
                    paymentInfoBlock.Append("<span class=\"fleft width-90  margin-right-5\" style='width:180px;'>" + (!string.IsNullOrEmpty(paymentInfo.TrackId) ? paymentInfo.TrackId : "") + "</span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-150 margin-right-5\" style='width:130px;'>" + (!string.IsNullOrEmpty(paymentInfo.PaymentId) ? paymentInfo.PaymentId : "") + "</span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-90 margin-right-5\" style='width:100px;'>" + (!string.IsNullOrEmpty(paymentInfo.Amount.ToString("#0.00")) ? Math.Ceiling(paymentInfo.Amount).ToString("#0.00") : "") + "</span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-90 margin-right-5\" style='width:100px;'>" + (!string.IsNullOrEmpty(paymentInfo.Charges.ToString("#0.00")) ? Math.Ceiling(paymentInfo.Charges).ToString("#0.00") : "") + "</span>");
                    paymentInfoBlock.Append("<span class=\"fleft width-60px \" style='width:130px;'>" + ((PaymentGatewaySource)paymentInfo.PaymentGateway + "</span>"));
                    paymentInfoBlock.Append("</div></div>");

                    Response.Write(paymentInfoBlock.ToString());
                }
                else
                {
                    paymentInfoBlock = new StringBuilder();
                    paymentInfoBlock.Append("<div class=\"fleft visa_PaymentInfo_top\">");
                    paymentInfoBlock.Append("<span style='padding-left:250px; color:#ffffff; font-weight:bold;'> Payment Information Not Available.</span>");
                    paymentInfoBlock.Append("<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" style=\" color:#ffffff;\" onclick=\"document.getElementById('PaymentInfo-" + Convert.ToString(Request["blockId"]) + "').style.display='none'\">Close</a></label>");
                    paymentInfoBlock.Append("</div>");

                    Response.Write(paymentInfoBlock.ToString());
                }
            }
        }


    }
}
