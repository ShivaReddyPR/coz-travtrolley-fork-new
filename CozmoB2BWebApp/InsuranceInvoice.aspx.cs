﻿using System;
using System.Data;
using CT.BookingEngine;
using CT.Core;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine.Insurance;

public partial class InsuranceInvoiceGUI :CT.Core.ParentPage// System.Web.UI.Page
{

    protected List<InsurancePlan> plans;
    protected Invoice invoice = new Invoice();
    protected AgentMaster agency;
    protected string agencyAddress;
    protected RegCity agencyCity = new RegCity();
    protected UserMaster loggedMember = new UserMaster();
    protected int insHdrId;
    protected int agencyId;
    protected int cityId = 0;
    protected string remarks = string.Empty;
    protected string pnr;
    protected DateTime traveldate = new DateTime();
    //protected ZEUSIns  Insurance = new ZEUSIns();
    protected InsuranceHeader header = new InsuranceHeader();
    protected bool isServiceAgency;
    protected bool paymentDoneAgainstInvoice = false;
    protected string supplierName = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
        loggedMember = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
        int invoiceNumber = 0;
        //checking if the page is reached with proper data
        if (Request["insHdrId"] != null)
        {
            insHdrId = Convert.ToInt32(Request["insHdrId"]);
        }
        else
        {
            invoiceNumber = Convert.ToInt32(Request["invoiceNumber"]);
        }
        header = new InsuranceHeader();
        header.RetrieveConfirmedPlan(insHdrId);
        pnr = header.PNR;
        agency = new AgentMaster(header.AgentId);
        DataTable dtCities = CityMaster.GetList(agency.Country, ListStatus.Short, RecordStatus.Activated);
        DataRow[] cities = dtCities.Select("City_Name like '%" + agency.City + "%'");

        if (cities != null && cities.Length > 0)
        {
            cityId = Convert.ToInt32(cities[0]["city_id"]);
        }
        // Reading agency information.
        
        isServiceAgency = true;// agency.AgencyTypeId == Convert.ToInt32(ConfigurationSystem.Core["ServiceAgencyTypeId"]);
        if (agency.City.Length == 0)
        {
            agencyCity.CityName = agency.City;
        }
        else
        {
            agencyCity = RegCity.GetCity(cityId);
        }
        // Formatting agency address for display.
        agencyAddress = agency.Address;
        agencyAddress.Trim();
        if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
        {
            agencyAddress += ",";
        }
        if (agency.Address != null && agency.Address.Length > 0)
        {
            agencyAddress += agency.Address;
        }
        agencyAddress.Trim();
        if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
        {
            agencyAddress += ",";
        }
        // Getting ticket list from DB
        

        traveldate = Convert.ToDateTime(header.DepartureDateTime);
        plans = header.InsPlans;
        // Generating invoice.
        if (plans.Count > 0)
        {
            if (invoiceNumber == 0)
            {
                invoiceNumber = Invoice.isInvoiceGenerated(header.Id, CT.BookingEngine.ProductType.Insurance);
            }
        }
        if (invoiceNumber > 0)
        {
            invoice = new Invoice();
            invoice.Load(invoiceNumber);
            supplierName = Invoice.GetSupplierByInvoiceNumber(invoiceNumber);
        }
        else
        {
            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(header.Id, string.Empty, (int)loggedMember.ID, ProductType.Insurance, 1);
            invoice.Load(invoiceNumber);
        }
        paymentDoneAgainstInvoice = Invoice.IsPaymentDoneAgainstInvoice(invoiceNumber);
        LocationMaster location = new LocationMaster(header.LocationId);
        lblLocation.Text = location.Name;
        //Fare calculations;
        decimal TotalPremiumAmt = 0;
        decimal Markup = 0;
        decimal Discount = 0;
        decimal B2CMarkup = 0;
        decimal outputVat = 0;


        foreach (InsurancePlan pax in plans)
        {
            if (!string.IsNullOrEmpty(pax.PolicyNo) && pax.ProposalState == InsuranceBookingStatus.Confirmed.ToString().ToUpper())
            {
                TotalPremiumAmt += Convert.ToDecimal(pax.NetAmount) + Convert.ToDecimal(pax.InputVATAmount);
                Markup += pax.Markup;
                Discount += pax.Discount;
                B2CMarkup += pax.B2CMarkup;
                outputVat += pax.OutputVATAmount;
            }
        }
        if (Settings.LoginInfo.AgentId > 1)
        {
            lblMarkup.Visible = false;
            lblMarkupVal.Visible = false;
            lblPremiumAmount.Text = header.Currency + " " + (TotalPremiumAmt + Markup-Discount+ B2CMarkup).ToString("N" + agency.DecimalValue); //Added only B2C Markup by chandan
        }
        else
        {
            lblMarkup.Visible = true;
            lblMarkupVal.Visible = true;
            lblPremiumAmount.Text = header.Currency + " " + TotalPremiumAmt.ToString("N" + agency.DecimalValue);
            lblMarkupVal.Text = header.Currency + " " + (Markup + B2CMarkup).ToString("N" + agency.DecimalValue);//Added only B2C Markup by chandan
        }
        if (Discount > 0)
        {
            lblDiscount.Visible = true;
            lblDiscountVal.Visible = true;
            lblDiscountVal.Text = header.Currency + " " + Discount.ToString("N" + agency.DecimalValue);
        }
        if (outputVat > 0)
        {
            lblvat.Visible = true;
            lblvatAmount.Visible = true;
            lblvatAmount.Text = agency.AgentCurrency + " " + outputVat.ToString("N" + agency.DecimalValue);
        }
        lblTotal.Text = header.Currency  + " " + (TotalPremiumAmt+ outputVat + Markup + B2CMarkup - Discount).ToString("N" + agency.DecimalValue);//Added only B2C Markup by chandan
    }

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
