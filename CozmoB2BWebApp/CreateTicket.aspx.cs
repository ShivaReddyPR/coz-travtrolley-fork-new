using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.Core;
using CT.AccountingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.MetaSearchEngine;
using CT.TicketReceipt.Common;
using CCA.Util;
using System.Collections.Specialized;
using SafexPay;
using Encryption.AES;
using System.Linq;

public partial class CreateTicket :CT.Core.ParentPage// System.Web.UI.Page
{
    protected UserMaster loggedMember = new UserMaster();
    protected List<Ticket> ticketList;
    protected FlightItinerary itinerary;
    protected BookingDetail booking;
    protected AgentMaster agency;
    protected UserMaster agent;
    protected int bookingId = 0;
    protected string lockMessage = string.Empty;
    protected string routing = string.Empty;
    protected bool ticketed;
    protected string errorMessage = string.Empty;
    protected bool eTicketEligible = false;
    protected bool airlineEligible = true;
    protected int lineItemCount;
    protected bool isDomestic = true;
    protected TicketingResponse response;
    Dictionary<string, string> ticketData = new Dictionary<string, string>();
    protected string ipAddr = string.Empty;

    protected bool isBookingSuccess = false;
    
    protected FlightItinerary flightItinerary;
    protected string IPAddr = string.Empty;
    protected string inBaggage = "", outBaggage = "";
    protected Airline airline;
    protected string AirlineLogoPath = ConfigurationManager.AppSettings["AirlineLogoPath"];

    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Create Ticket";
        AuthorizationCheck();
        if (Session["FlightItinerary"] == null)
        {
            loggedMember = new UserMaster(Settings.LoginInfo.UserID);
            {
                if (Request.QueryString["bookingId"] != null)
                {
                    bookingId = Convert.ToInt32(Request.QueryString["bookingId"]);
                }
                else
                {
                    Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + bookingId + "&fromAgent=true", true);
                }
            }
        }
        else
        {
            itinerary = Session["FlightItinerary"] as FlightItinerary;
            loggedMember = new UserMaster(Settings.LoginInfo.UserID);
        }
        if (Request["errorMessage"] != null)
        {
            errorMessage = Request["errorMessage"];
        }
        if (itinerary == null)
        {
            Product[] products = BookingDetail.GetProductsLine(bookingId);
            booking = new BookingDetail(bookingId);

            agency = new AgentMaster(booking.AgencyId);
            agent = new UserMaster(booking.CreatedBy);

            //TODO: what to do for this check.
            // Added a check as in the code below booking as getting locked and its status is getting updated again to inProgress
            //if (booking.Status != BookingStatus.Hold && booking.Status != BookingStatus.Ready && booking.Status != BookingStatus.InProgress)
            //{
            //    Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + bookingId.ToString() + "&message=This ticket cannot be saved.", true);
            //}
            for (int i = 0; i < products.Length; i++)
            {
                if (products[i].ProductTypeId == (int)ProductType.Flight)
                {
                    itinerary = new FlightItinerary(products[i].ProductId);
                    hdfFlightId.Value = itinerary.FlightId.ToString();
                    break;
                }
            }
            Session["FlightItinerary"] = itinerary;
            
        }
        hdfFlightId.Value = itinerary.FlightId.ToString();
        //Load the payment details for the current booking
        CreditCardPaymentInformation ccPayment = new CreditCardPaymentInformation();
        ccPayment.Load(itinerary.BookingId);

        //If Payment is not done earlier then redirect to payment gateway
        //otherwise 
        if (ccPayment.PaymentStatus < 1)
        {
            if (Request.QueryString["mode"] != null)
            {
                Response.Redirect("PaymentProcessing.aspx?paymentGateway=" + Request["pg"] + "&redirect_url=CreateTicket.aspx");
            }
        }

        // checking for flight domestic or international
        for (int i = 0; i < itinerary.Segments.Length; i++)
        {
            if (itinerary.Segments[i].Origin.CountryCode != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "" || itinerary.Segments[i].Destination.CountryCode != "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + "")
            {
                isDomestic = false;
                break;
            }
        }
        for (int i = 0; i < itinerary.Segments.Length; i++)
        {
            Airline airline = new Airline();
            airline.Load(itinerary.Segments[i].Airline);
            if (!airline.ETicketEligible)
            {
                airlineEligible = false;
                break;
            }
        }
        booking = new BookingDetail(itinerary.BookingId);

        agency = new AgentMaster(booking.AgencyId);
        agent = new UserMaster(booking.CreatedBy);


        ticketList = Ticket.GetTicketList(itinerary.FlightId);
        ticketed = FlightItinerary.IsTicketed(itinerary);
        if (!ticketed)
        {
            if (booking.Status == BookingStatus.Released)
            {
                Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + bookingId, true);
            }
            // Checking the lock status. Lock if not already locked and member is logged in.
        }
        //TODO: check for sequence of destinations.
        routing = itinerary.Segments[0].Origin.CityCode;
        eTicketEligible = true;
        for (int i = 0; i < itinerary.Segments.Length; i++)
        {
            routing += "-" + itinerary.Segments[i].Destination.CityCode;
            if (itinerary.Segments[i].ETicketEligible == false)
            {
                eTicketEligible = false;
            }
        }

        InitializeControls();


        if (Request.QueryString["ErrorPG"] != null)//Error returned from Payment Gateway
        {
            Utility.Alert(this, Request.QueryString["ErrorPG"]);
        }
        else if (Request.Form["encResp"] != null)//Payment returned from CCAvenue
        {
            string orderId = string.Empty;
            string paymentId = string.Empty;
            string statusMessage = string.Empty;

            string workingKey = ConfigurationManager.AppSettings["CCA_Encrypt_Key"];
            CCACrypto ccaCrypto = new CCACrypto();
            string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], workingKey);
            Audit.Add(EventType.Book, Severity.Low, 1, "(Import PNR)CCAvenue response data : " + encResponse, Request["REMOTE_ADDR"]);
            NameValueCollection Params = new NameValueCollection();
            string[] segments = encResponse.Split('&');
            foreach (string seg in segments)
            {
                string[] parts = seg.Split('=');
                if (parts.Length > 0)
                {
                    string Key = parts[0].Trim();
                    string Value = parts[1].Trim();
                    Params.Add(Key, Value);
                }
            }


            int paymentInformationId = 0;
            if (Session["PaymentInformationId"] != null)
            {
                paymentInformationId = Convert.ToInt32(Session["PaymentInformationId"]);
            }


            if (Params["order_status"] == "Success")
            {
                orderId = Params["order_id"];
                //paymentId = Params["payment_id"];
                paymentId = Params["tracking_id"];
                statusMessage = "Successful";

                try
                {
                    CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                    creditCard.PaymentId = paymentId;
                    creditCard.TrackId = orderId;
                    creditCard.PaymentInformationId = paymentInformationId;
                    creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                    creditCard.ReferenceId = itinerary.BookingId;
                    creditCard.Remarks = statusMessage;
                    creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.CCAvenue;
                    creditCard.UpdatePaymentDetails();

                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                }
                finally
                {
                    Session["PaymentInformationId"] = null;//clear the credit card id stored in session                    
                    Session["FlightItinerary"] = null;//Clear Itinerary used for Payment Processing
                }


                GenerateTicket();
            }
            else
            {
                if (Params["failure_message"] != null && Params["failure_message"].Length > 0)
                {
                    statusMessage = Params["failure_message"];
                }
                else if (Params["status_message"] != null && Params["status_message"].Length > 0)
                {
                    statusMessage = Params["status_message"];
                }
                else
                {
                    statusMessage = "Payment declined due to invalid details or due to technical error.";
                }

                try
                {
                    CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                    creditCard.PaymentId = paymentId;
                    creditCard.TrackId = orderId;
                    creditCard.PaymentInformationId = paymentInformationId;
                    creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                    creditCard.ReferenceId = itinerary.BookingId;
                    creditCard.Remarks = statusMessage;
                    creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.CCAvenue;
                    creditCard.UpdatePaymentDetails();

                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                }
                finally
                {
                    Session["PaymentInformationId"] = null;//clear the credit card id stored in session                    
                    Session["FlightItinerary"] = null;//Clear Itinerary used for Payment Processing
                }

                errorMessage = "Payment declined due to technical error or due to invalid details";
                MultiView1.ActiveViewIndex = 1;
            }

            
        }
        #region SafexPay 
        else if (Request.Params.Count > 0 && !string.IsNullOrEmpty(Request.Params["txn_response"]))//Payment returned from SafexPay
        {
            string orderId = string.Empty;
            string paymentId = string.Empty;
            string statusMessage = string.Empty;
            SafexPayService safexPay = new SafexPayService();
            CryptoClass aes = new CryptoClass();
            #region response parameters

            try
            {
                aes.enc_txn_response = (!String.IsNullOrEmpty(Request.Params["txn_response"])) ? Request.Params["txn_response"] : string.Empty;
                aes.enc_pg_details = (!String.IsNullOrEmpty(Request.Params["pg_details"])) ? Request.Params["pg_details"] : string.Empty;
                aes.enc_fraud_details = (!String.IsNullOrEmpty(Request.Params["fraud_details"])) ? Request.Params["fraud_details"] : string.Empty;
                aes.enc_other_details = (!String.IsNullOrEmpty(Request.Params["other_details"])) ? Request.Params["other_details"] : string.Empty;

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, "Error message :" + ex.Message + ex.StackTrace, Request["REMOTE_ADDR"]);
            }
            #endregion
            #region Txn_Resonse
            try
            {

                string txn_response = aes.decrypt(aes.enc_txn_response, safexPay.txnMerchantKey);
                string[] txn_response_arr = txn_response.Split('|');

                safexPay.ag_id = (!String.IsNullOrEmpty(txn_response_arr[0])) ? txn_response_arr[0] : string.Empty;
                safexPay.me_id = (!String.IsNullOrEmpty(txn_response_arr[1])) ? txn_response_arr[1] : string.Empty;
                safexPay.order_no = (!String.IsNullOrEmpty(txn_response_arr[2])) ? txn_response_arr[2] : string.Empty;
                safexPay.Amount = (!String.IsNullOrEmpty(txn_response_arr[3])) ? txn_response_arr[3] : string.Empty;
                safexPay.Country = (!String.IsNullOrEmpty(txn_response_arr[4])) ? txn_response_arr[4] : string.Empty;
                safexPay.Currency = (!String.IsNullOrEmpty(txn_response_arr[5])) ? txn_response_arr[5] : string.Empty;
                safexPay.txn_date = (!String.IsNullOrEmpty(txn_response_arr[6])) ? txn_response_arr[6] : string.Empty;
                safexPay.txn_time = (!String.IsNullOrEmpty(txn_response_arr[7])) ? txn_response_arr[7] : string.Empty;
                safexPay.ag_ref = (!String.IsNullOrEmpty(txn_response_arr[8])) ? txn_response_arr[8] : string.Empty;
                safexPay.pg_ref = (!String.IsNullOrEmpty(txn_response_arr[9])) ? txn_response_arr[9] : string.Empty;
                safexPay.status = (!String.IsNullOrEmpty(txn_response_arr[10])) ? txn_response_arr[10] : string.Empty;
                safexPay.res_code = (!String.IsNullOrEmpty(txn_response_arr[11])) ? txn_response_arr[11] : string.Empty;
                safexPay.res_message = (!String.IsNullOrEmpty(txn_response_arr[12])) ? txn_response_arr[12] : string.Empty;

                Audit.Add(EventType.Book, Severity.Low, 1, "(Flight)SafexPay response data : " + txn_response, Request["REMOTE_ADDR"]);

                try
                {
                    //response Log in decrypt format
                    safexPay.WriteResonse(txn_response);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
                }

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SafexPay, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
            }
            #endregion

            int paymentInformationId = 0;

            if (Session["PaymentInformationId"] != null)
            {
                paymentInformationId = Convert.ToInt32(Session["PaymentInformationId"]);
            }

            if (safexPay.status == "Successful")
            {
                orderId = safexPay.order_no;
                //paymentId = Params["payment_id"];
                paymentId = safexPay.pg_ref;
                statusMessage = "Successful";

                try
                {
                    CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                    creditCard.PaymentId = paymentId;
                    creditCard.TrackId = orderId;
                    creditCard.PaymentInformationId = paymentInformationId;
                    creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                    creditCard.ReferenceId = itinerary.BookingId;
                    creditCard.Remarks = statusMessage;
                    creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.SafexPay;
                    creditCard.UpdatePaymentDetails();
                    Session["PaymentInformationId"] = null;//clear the credit card id stored in session
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                }

                GenerateTicket();
            }
            else
            {
                statusMessage = safexPay.status;
                try
                {
                    CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                    creditCard.PaymentId = paymentId;
                    creditCard.TrackId = orderId;
                    creditCard.PaymentInformationId = paymentInformationId;
                    creditCard.PaymentStatus = (statusMessage == "Successful" ? 1 : 0);
                    creditCard.ReferenceId = itinerary.BookingId;
                    creditCard.Remarks = statusMessage;
                    creditCard.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.SafexPay;
                    creditCard.UpdatePaymentDetails();
                    Session["PaymentInformationId"] = null;//clear the credit card id stored in session
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                }
                errorMessage = "Payment declined due to technical error or due to invalid details";
                MultiView1.ActiveViewIndex = 1;
            }

            
        }
        #endregion
        else
        {
            GenerateTicket();

            if (itinerary.PaymentMode == ModeOfPayment.CreditCard)
            {
                try
                {
                    CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                    creditCard.Load(itinerary.BookingId);
                    creditCard.PaymentId = creditCard.PaymentId;
                    creditCard.PaymentInformationId = creditCard.PaymentInformationId;
                    creditCard.PaymentStatus = 1; //Success
                    creditCard.ReferenceId = itinerary.BookingId;
                    creditCard.Remarks = errorMessage;
                    creditCard.UpdatePaymentDetails();

                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                }
                finally
                {
                    Session["PaymentInformationId"] = null;//clear the credit card id stored in session                    
                    Session["FlightItinerary"] = null;//Clear Itinerary used for Payment Processing
                }
            }
        }
    }

    /// <summary>
    /// Checks if the ticketing is done for given passenger or not.
    /// </summary>
    /// <param name="pId">paxId of the passenger</param>
    /// <param name="tList">List of ticket</param>
    /// <returns>True if ticketed</returns>
    protected bool IsTicketed(int pId, List<Ticket> tList)
    {
        bool retVal = false;
        foreach (Ticket ticket in tList)
        {
            if (ticket.PaxId == pId)
            {
                retVal = true;
                break;
            }
        }
        return retVal;
    }

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx", true);
        }

    }
    private void InitializeControls()
    {
        lblAgeny.Text = agency.Name;
        lblPNRNo.Text = itinerary.PNR;
        if (agency.ID <= 0)
        {
            lblAgentName.Text = agent.FirstName + ' ' + agent.LastName;
        }
        if (booking.LockedBy > 0)
        {
            //imgAlert.Visible = true;
            //lblAlertmsg.Text = "This booking has been locked for you";
        }
        TimeSpan tspan = Util.UTCToIST(DateTime.Now) - Util.UTCToIST(booking.CreatedOn);
        if (tspan.Days == 0)
        {
            lblBkgDate.Text = "Today ";
        }
        lblBkgDate.Text += Util.UTCToIST(booking.CreatedOn).ToString("dd/MM/yyyy");
        lblTravelDate.Text = itinerary.Segments[0].DepartureTime.ToString("dd/MM/yyyy");
        lblErrMsg.Text = errorMessage;
        if (!ticketed)
        {
            //Image1.Visible = true;
            //lblAltMsg.Text = "Please make the ticket in Go Res! and click on 'Create Ticket' link, when you are finished.";
        }
    }
    private void GenerateTicket()
    {        
        response = new TicketingResponse();
        MetaSearchEngine mse = new MetaSearchEngine();
        mse.SettingsLoginInfo = Settings.LoginInfo;
        if (Session["sessionId"] == null)
        {
            mse.SessionId = Guid.NewGuid().ToString();
        }
        else
        {
            mse.SessionId = Session["sessionId"].ToString();
        }
        mse.AppUserId = (int)Settings.LoginInfo.UserID;
        int agencyId = 0;
        if (Settings.LoginInfo.IsOnBehalfOfAgent)
        {
            agencyId = Settings.LoginInfo.OnBehalfAgentID;
        }
        else
        {
            agencyId = Settings.LoginInfo.AgentId;
        }
        FlightItinerary itinerary = new FlightItinerary();
        AgentMaster agency = new AgentMaster(agencyId);
        try
        {
            if (hdfFlightId.Value.Length > 0) // We can not proceed without pnr.
            {
                itinerary = new FlightItinerary(Convert.ToInt32(hdfFlightId.Value));
                booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(itinerary.FlightId));
                List<Ticket> tickets= Ticket.GetTicketList(itinerary.FlightId);
                if (tickets != null && tickets.Count ==0)
                {
                    if (booking.LockedBy <= 0)
                    {
                        booking.Lock(BookingStatus.InProgress, (int)loggedMember.ID);
                    }
                    else if (booking.LockedBy != loggedMember.ID)
                    {
                        //Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + bookingId, true);
                        UserMaster user = new UserMaster(booking.LockedBy);
                        errorMessage = "Booking Locked by <b>" + user.FirstName + " " + user.LastName + "</b>";
                        MultiView1.ActiveViewIndex = 1;
                        return;
                    }
                    string corporateCode = string.Empty;
                    if (Request.QueryString["corporateCode"] != null)
                    {
                        ticketData.Add("corporateCode", Request.QueryString["corporateCode"].ToString());
                    }
                    if (Request.QueryString["tourCode"] != null)
                    {
                        ticketData.Add("tourCode", Request.QueryString["tourCode"].ToString());
                    }
                    if (Request.QueryString["endorsement"] != null)
                    {
                        ticketData.Add("endorsement", Request.QueryString["endorsement"].ToString());
                    }
                    if (Request.QueryString["remarks"] != null)
                    {
                        ticketData.Add("remarks", Request.QueryString["remarks"].ToString());
                    }
                    loggedMember = new UserMaster(Settings.LoginInfo.UserID);    // picking memberId of the logged in user from Session.
                    itinerary.BookUserIP = ipAddr = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];
                    try
                    {
                        agency.UpdateBalance(0);
                        if (agency.CurrentBalance > 0)
                        {
                            mse.clsURImpresp = Session["UAPIURImpresp"] != null ? (UAPIdll.Universal46.UniversalRecordImportRsp)Session["UAPIURImpresp"] : mse.clsURImpresp;
                            mse.clsUAPIRepriceResp = Session["UAPIReprice"] != null ? (UAPIdll.Air46.AirPriceRsp)Session["UAPIReprice"] : mse.clsUAPIRepriceResp;
                            response = mse.Ticket(itinerary.PNR, loggedMember, ticketData, ipAddr);
                        }
                        else
                        {
                            response.Status = TicketingResponseStatus.NotAllowed;
                            response.Message = "Insufficient agent balance.";
                            errorMessage = "Insufficient agent balance.";
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, "");
                        if (response.Status == TicketingResponseStatus.InProgress)
                        {
                            errorMessage = "Ticketing is already in progress.";
                        }
                        else if (response.Status == TicketingResponseStatus.NotAllowed)
                        {
                            errorMessage = "<span class=\"bold border-bottom\">Auto ticketing is not allowed.</span><br /><br /> ";
                            errorMessage += response.Message + "<br />";
                        }
                        else if (response.Status == TicketingResponseStatus.NotCreated || response.Status == TicketingResponseStatus.OtherError)
                        {
                            errorMessage = "Ticket could not be generated.";
                        }
                        else if (response.Status == TicketingResponseStatus.NotSaved)
                        {
                            errorMessage = "Ticket has been generated but could not be saved in our system.";
                        }
                        else if (response.Status == TicketingResponseStatus.TicketedWS)
                        {
                            errorMessage = "Ticket is already generaged.";
                        }
                        else if (response.Status == TicketingResponseStatus.PriceChanged)
                        {
                            errorMessage = "Pricing has changed for this booking.";
                        }
                        //else if (response.Status == TicketingResponseStatus.NotSaved)
                        //{
                        //    errorMessage = "Ticket generated but could not be saved in our system.";
                        //}
                        else
                        {
                            errorMessage = "Ticket could not be generated.";
                        }
                        MultiView1.ActiveViewIndex = 1;
                    }
                }
                else
                {                   
                    response.Status = TicketingResponseStatus.Successful;
                    errorMessage = "Already Ticketed for this PNR";
                    Response.Redirect("Eticket.aspx?FlightId=" + itinerary.FlightId, true);
                }
            }
            else
            {
                response.Status = TicketingResponseStatus.OtherError;
                //ErrorView.ActiveViewIndex = 1;
            }


            if (response.Status == TicketingResponseStatus.Successful || response.Status == TicketingResponseStatus.TicketedDB)
            {
                BookingUtility.AgencyLccBal.Remove(booking.AgencyId);
                //Dictionary<int, decimal> agencyLccBal = (Dictionary<int, decimal>)Technology.MemCache.Cache.Get("AgencyLccBal");
                //if (agencyLccBal != null)
                //{
                //    agencyLccBal.Remove(booking.AgencyId);
                //    Technology.MemCache.Cache.Replace("AgencyLccBal", (object)agencyLccBal);
                //}
                //decimal amountToCheck = 0;
                //List<BookingDetail> insuranceBooking = BookingDetail.GetChildBookings(booking.BookingId);
                //agency.Load(Convert.ToInt32(booking.AgencyId));
                //for (int i = 0; i < insuranceBooking.Count; i++)
                //{
                //    Product[] product = insuranceBooking[i].ProductsList;
                //    for (int j = 0; j < product.Length; j++)
                //    {
                //        if (product[j].ProductType == ProductType.Insurance)
                //        {
                //            Insurance insurance = new Insurance();
                //            insurance.Load(product[j].ProductId);
                //            mse.CreatePolicy(itinerary, insurance, agency, loggedMember);
                //        }
                //    }
                //}
                /* SAI              decimal ticketAmount = 0;
                              foreach (FlightPassenger pax in itinerary.Passenger)
                              {
                                  if (itinerary.Passenger[0].Price.NetFare > 0)
                                  {
                                      ticketAmount = pax.Price.NetFare + pax.Price.Tax + pax.Price.Markup;
                                  }
                                  else
                                  {
                                      ticketAmount = pax.Price.PublishedFare + pax.Price.Tax;
                                  }
                              }

                              //Updating Agent Balance
                              Settings.LoginInfo.AgentBalance -= ticketAmount;
                              AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
                              agent.CreatedBy = Settings.LoginInfo.UserID;
                              agent.UpdateBalance(-ticketAmount);       */

                if (booking.AgencyId == Settings.LoginInfo.AgentId && itinerary.PaymentMode == ModeOfPayment.Credit)
                {
                    decimal currentAgentBalance = 0;
                    AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
                    agent.CreatedBy = Settings.LoginInfo.UserID;
                    if (itinerary.FlightBookingSource != BookingSource.TBOAir)
                    {
                        currentAgentBalance = agent.UpdateBalance(0);
                    }
                    else //For TBO we are ceiling the Total Amount and need to deduct the same from Agent balance
                    {
                        //For all other sources we are deducting the balance from Add_Ticket procedure passenger wise i.e
                        //if 6 pax are there six times we are deducting the balance with pax wise total amount. So in case
                        //of TBO Air if we do pax wise ceiling TotalAmount will be higher than the original amount.
                        decimal ticketAmount = 0;
                        foreach (FlightPassenger pax in itinerary.Passenger)
                        {
                            ticketAmount += pax.Price.PublishedFare + pax.Price.Tax + pax.Price.Markup + pax.Price.OtherCharges + pax.Price.BaggageCharge + 
                                pax.Price.AdditionalTxnFee + pax.Price.SServiceFee + pax.Price.TransactionFee - pax.Price.Discount +
                                (Settings.LoginInfo.IsOnBehalfOfAgent ? pax.Price.AsvAmount : 0);
                        }
                        currentAgentBalance = agent.UpdateBalance(-Math.Ceiling(ticketAmount));
                    }
                    Settings.LoginInfo.AgentBalance = currentAgentBalance;
                    Label lblAgentBalance = (Label)Master.FindControl("lblAgentBalance");
                    lblAgentBalance.Text = CT.TicketReceipt.Common.Formatter.ToCurrency(currentAgentBalance);
                }
                isBookingSuccess = true;
                try
                {
                    Invoice invoice = new Invoice();
                    int invoiceNumber = Invoice.isInvoiceGenerated(Ticket.GetTicketList(itinerary.FlightId)[0].TicketId, ProductType.Flight);

                    if (invoiceNumber > 0)
                    {

                        invoice.Load(invoiceNumber);
                    }
                    else
                    {
                        invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.FlightId, "", (int)Settings.LoginInfo.UserID, ProductType.Flight, 1);
                        if (invoiceNumber > 0)
                        {
                            invoice.Load(invoiceNumber);
                            invoice.Status = InvoiceStatus.Paid;
                            invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                            invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                            invoice.UpdateInvoice();
                        }
                    }
                    isBookingSuccess = true;
                    BindTicketInfo(itinerary.FlightId);

                    errorMessage = "Create Ticket Successful";                   
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, Request.ServerVariables["REMOTE_ADDR"]);
                }
                SendFlightTicketEmail(itinerary);
                Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + booking.BookingId, false);
            }
            else
            {
                if (response.Status == TicketingResponseStatus.InProgress)
                {
                    errorMessage = "Ticketing is already in progress.";
                }
                else if (response.Status == TicketingResponseStatus.NotAllowed)
                {
                    errorMessage = "<span class=\"bold border-bottom\">Auto ticketing is not allowed.</span><br /><br /> ";
                    errorMessage += response.Message + "<br />";
                }
                else if (response.Status == TicketingResponseStatus.NotCreated || response.Status == TicketingResponseStatus.OtherError)
                {
                    errorMessage = "Ticket could not be generated.";
                }
                else if (response.Status == TicketingResponseStatus.NotSaved)
                {
                    errorMessage = "Ticket has been generated but could not be saved in our system.";
                }
                else if (response.Status == TicketingResponseStatus.TicketedWS)
                {
                    errorMessage = "Ticket is already generaged.";
                }
                else if (response.Status == TicketingResponseStatus.PriceChanged)
                {
                    errorMessage = "Pricing has changed for this booking.";
                }
                else if (response.Status == TicketingResponseStatus.NotSaved)
                {
                    errorMessage = "Ticket generated but could not be saved in our system.";
                }
                else
                {
                    errorMessage = "Ticket could not be generated.";
                }
                MultiView1.ActiveViewIndex = 1;
            }
        }
        catch (Exception ex)
        {
            
        }
    }
    protected void btnGenerateTicket_Click(object sender, EventArgs e)
    {
        GenerateTicket();
    }

    protected void BindTicketInfo(int FlightId)
    {
        try
        {
            if (FlightId > 0)
            {
                flightItinerary = new FlightItinerary(FlightId);
                booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(FlightId));
                ticketList = Ticket.GetTicketList(FlightId);
            }
            if (flightItinerary.FlightBookingSource == BookingSource.AirArabia)
            {
                foreach (FlightPassenger pax in flightItinerary.Passenger)
                {
                    if (inBaggage.Length > 0)
                    {
                        inBaggage += ", " + pax.BaggageCode.Split(',')[0];
                    }
                    else
                    {
                        inBaggage = pax.BaggageCode.Split(',')[0];
                    }
                    if (pax.BaggageCode.Split(',').Length > 1)
                    {
                        if (outBaggage.Length > 0)
                        {
                            outBaggage += ", " + pax.BaggageCode.Split(',')[1];
                        }
                        else
                        {
                            outBaggage = pax.BaggageCode.Split(',')[1];
                        }
                    }
                }
            }
            airline = new Airline();
            airline.Load(flightItinerary.ValidatingAirlineCode);

            if (airline.LogoFile.Trim().Length != 0)
            {
                string[] fileExtension = airline.LogoFile.Split('.');
                AirlineLogoPath = AirlineLogoPath + flightItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void SendFlightTicketEmail(FlightItinerary itinerary)
    {
        try
        {
            string logoPath = "";
            int agentId = 0;

            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agentId = Settings.LoginInfo.OnBehalfAgentID;
            }
            else
            {
                agentId = Settings.LoginInfo.AgentId;
            }

            if (agentId > 1)
            {
                logoPath =  Request.Url.Scheme +"://www.ctb2b.cozmotravel.com" + (ConfigurationManager.AppSettings["AgentImage"].StartsWith("/") ? ConfigurationManager.AppSettings["AgentImage"] : "/" + ConfigurationManager.AppSettings["AgentImage"]) + new AgentMaster(agentId).ImgFileName;
                imgLogo.ImageUrl = logoPath;
            }
            else
            {
                imgLogo.ImageUrl = Request.Url.Scheme +"://www.ctb2b.cozmotravel.com/images/logo.jpg";
            }

            ticketList = Ticket.GetTicketList(itinerary.FlightId);

            string myPageHTML = "";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            EmailDiv.RenderControl(htw);
            myPageHTML = sw.ToString();
            myPageHTML = myPageHTML.Replace("none", "block");//Set Email <Div> Visible in the email
            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            //Send email to Location email if FLIGHTTRIPSUMMARY_LOCATION_EMAIL set to true otherwise send to pax email
            try
            {
                AgentAppConfig clsAppCnf = new AgentAppConfig();
                clsAppCnf.AgentID = Settings.LoginInfo.AgentId;
                string isLocationEmail = clsAppCnf.GetConfigData().Where(x => x.AppKey.ToUpper() == "FLIGHTTRIPSUMMARY_LOCATION_EMAIL" && x.ProductID == 1).Select(y => y.AppValue).FirstOrDefault();
                if (Settings.LoginInfo.IsCorporate == "Y")
                {
                    if (!string.IsNullOrEmpty(isLocationEmail) && isLocationEmail.ToUpper() == "TRUE")
                    {
                        LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.LocationID);
                        AddLocationEmailsForTicketEmail(ref toArray, locationMaster);                        
                    }
                    else toArray.Add(itinerary.Passenger[0].Email);
                }
                else
                {
                    toArray.Add(itinerary.Passenger[0].Email);
                }
            }
            catch { }
            
            if (ViewState["MailSent"] == null)
            {
                ViewState["MailSent"] = true;
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Ticket Confirmation - " + itinerary.PNR, myPageHTML, new Hashtable());

            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Send Flight E-ticket Email: reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }

    }

    /// <summary>
    /// This function is used to add location emails for corporate agents in order to send ticket voucher. If location emails are not null then mails will be added
    /// </summary>
    /// <param name="lstToMails">List of string with email collection to be sent</param>
    /// <param name="locationMaster">LocationMaster object for adding emails</param>
    private void AddLocationEmailsForTicketEmail(ref List<string> lstToMails, LocationMaster locationMaster)
    {
        List<string> toArray = lstToMails;
        if (!string.IsNullOrEmpty(locationMaster.LocationEmail) && locationMaster.LocationEmail.Contains("@"))
        {
            if (locationMaster.LocationEmail.Contains(","))
            {
                string[] emails = locationMaster.LocationEmail.Split(',');
                emails.ToList().ForEach(e => toArray.Add(e));
            }
            else
            {
                toArray.Add(locationMaster.LocationEmail);
            }
        }
    }

    //Added by lokesh on 4-July-2018
    /// <summary>
    /// This method returns the baggage for GDS suppliers from table BKE_Segment_PTC_Detail
    /// </summary>
    /// <param name="flightId"></param>
    /// <returns></returns>
    protected string GetBaggageForGDS(int flightId, PassengerType paxType)
    {
        string gdsBaggage = string.Empty;
        try
        {
            List<SegmentPTCDetail> segmentPTCDetails = new List<SegmentPTCDetail>();
            List<SegmentPTCDetail> paxTypeSegmentPTCDetails = new List<SegmentPTCDetail>();
            string paxCode = string.Empty;
            switch (paxType)
            {
                case PassengerType.Adult:
                    paxCode = "ADT";
                    break;
                case PassengerType.Child:
                      paxCode = "CNN";
                    break;
                case PassengerType.Infant:
                    paxCode = "INF";
                    break;
            }

            segmentPTCDetails = SegmentPTCDetail.GetSegmentPTCDetail(flightId);
            paxTypeSegmentPTCDetails = segmentPTCDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.PaxType.ToLower().Equals(paxCode.ToLower()); });
            if (paxTypeSegmentPTCDetails != null && paxTypeSegmentPTCDetails.Count > 0)
            {
                for (int i = 0; i < paxTypeSegmentPTCDetails.Count; i++)
                {
                    if (!string.IsNullOrEmpty(paxTypeSegmentPTCDetails[i].Baggage))
                    {
                        if (string.IsNullOrEmpty(gdsBaggage))
                        {
                            gdsBaggage = !paxTypeSegmentPTCDetails[i].Baggage.ToLower().Contains("piece") ? (paxTypeSegmentPTCDetails[i].Baggage.ToLower().Contains("kg") ? paxTypeSegmentPTCDetails[i].Baggage : paxTypeSegmentPTCDetails[i].Baggage + "Kg") : paxTypeSegmentPTCDetails[i].Baggage;
                        }
                        else
                        {
                            gdsBaggage += "," + (!paxTypeSegmentPTCDetails[i].Baggage.ToLower().Contains("piece") ? (paxTypeSegmentPTCDetails[i].Baggage.ToLower().Contains("kg") ? paxTypeSegmentPTCDetails[i].Baggage : paxTypeSegmentPTCDetails[i].Baggage + "Kg") : paxTypeSegmentPTCDetails[i].Baggage);
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Failed to read baggage" + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        if (string.IsNullOrEmpty(gdsBaggage) && paxType != PassengerType.Infant)
        {
            gdsBaggage = "Airline Norms";
        }
        return gdsBaggage;
    }
}
