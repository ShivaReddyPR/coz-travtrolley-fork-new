﻿#region NameSpace
using System;
using System.Data;
using System.Web.UI;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.IO;
using CT.BookingEngine;
using System.Web.UI.WebControls;
#endregion

public partial class HalaServiceQueueGUI : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblSuccessMsg.Text = string.Empty;
            if (Settings.LoginInfo != null)
            {
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.btnExport);
                hdfParam.Value = "1";
                Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
               
                if (!IsPostBack)
                {
                   
                    txtFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    hdfParam.Value = "0";
                    LoadQueue();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "HalaServiceQueue Page_Load Event Error Raised:" + ex.ToString(), "");
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            LoadQueue();
            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "HalaServiceQueue btnSearch_Click Event Error Raised:" + ex.ToString(), "");
        }

    }
   
    private DataTable GetHalaList()
    {
        try
        {
            DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
            if (txtFromDate.Text != string.Empty)
            {
                try
                {
                    startDate = Convert.ToDateTime(txtFromDate.Text, provider);
                }
                catch { }
            }
            else
            {
                startDate = DateTime.Now;
            }

            if (txtToDate.Text != string.Empty)
            {
                try
                {
                    endDate = Convert.ToDateTime(Convert.ToDateTime(txtToDate.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
                }
                catch { }
            }
            else
            {
                endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
            }
           int isUsed = Utility.ToInteger(ddlIsUsed.SelectedItem.Value);

           HalaRecordsData = ItineraryAddServiceDetails.GetHalaQueueItems(startDate, endDate, isUsed);
           
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return HalaRecordsData;

    }

    private void LoadQueue()
    {
        try
        {
            DataTable dtRecords = GetHalaList();
            if (dtRecords != null)
            {
                gvHalaList.DataSource = dtRecords;
                gvHalaList.DataBind();
                if (dtRecords.Rows.Count > 0)
                {
                    btnExport.Visible = true;
                    btnUpdateStatus.Visible = true;
                }
                else
                {
                    btnExport.Visible = false;
                    btnUpdateStatus.Visible = false;
                }
                
            }
            //Added by Ganesh on 27-Apr-2018, displaying checkbox based on the member typessss
            if (Settings.LoginInfo.MemberType == MemberType.HALASERVICEUSER)
            {
                gvHalaList.Columns[0].Visible = true;
                btnUpdateStatus.Visible = true;
            }
            else
            {
                gvHalaList.Columns[0].Visible = false;
                btnUpdateStatus.Visible = false;
            }

            
            

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void selectedItem()
    {
        try
        {
            foreach (System.Data.DataColumn col in HalaRecordsData.Columns) col.ReadOnly = false;
            foreach (GridViewRow gvrow in gvHalaList.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvrow.FindControl("chkstatus");
                HiddenField hdfAddid = (HiddenField)gvrow.FindControl("IThdfAddId");
                foreach (DataRow dr in HalaRecordsData.Rows)
                {
                    if (dr != null)
                    {
                        dr.BeginEdit();
                        if (Utility.ToString(dr["add_id"]) == hdfAddid.Value)
                        {
                            if (Utility.ToString(dr["isUsed"]) != "U") dr["isUsed"] = chkSelect.Checked ? "Y" : "N";
                        }
                        dr.EndEdit();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    private void isTrackingEmpty()
    {
        try
        {
            bool _selected = false;
            foreach (DataRow dr in HalaRecordsData.Rows)
            {
                if (dr != null)
                {
                    bool isUpdated = Utility.ToString(dr["isUsed"]) == "U" ? true : false; 
                    if (!isUpdated && Utility.ToString(dr["isUsed"]) == "Y")
                    {
                        _selected = true;
                        return;
                    }
                }
            }
            string strMsg = "Please Select atleast one Item ! ";
            if (!_selected)
                Utility.Alert(this.Page, strMsg);
        }
        catch { throw; }
    }            
    private DataTable HalaRecordsData
    {
        get
        {
            return (DataTable)Session["HalaRecordsData"];
        }
        set
        {
            if (value == null)
            {
                Session.Remove("HalaRecordsData");
            }
            else
            {
                Session["HalaRecordsData"] = value;
            }

        }
    }
   
  
    protected void gvHalaList_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvHalaList.PageIndex = e.NewPageIndex;
            LoadQueue();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "HalaServiceQueue gvHalaList_OnPageIndexChanging Event Error Raised:" + ex.ToString(), "");

        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            string attachment = "attachment; filename=HalaAcctReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dgHalaSerAcctReportList.AllowPaging = false;
            dgHalaSerAcctReportList.DataSource = GetHalaList();
            dgHalaSerAcctReportList.DataBind();
            dgHalaSerAcctReportList.RenderControl(htw);
            Response.Write(sw.ToString());
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "HalaServiceQueue btnExport_Click Event Error Raised:" + ex.ToString(), "");

        }
        finally
        {
            Response.End();
        }
    }
    protected void btnUpdateStatus_Click(object sender, EventArgs e)
    {
        try
        {
            selectedItem();
            isTrackingEmpty();
            foreach (DataRow dr in HalaRecordsData.Rows)
            {
                if (dr != null)
                {
                    bool selected = Utility.ToString(dr["isUsed"])=="Y"? true : false;
                    long addId = Utility.ToLong(dr["add_id"]);
                    bool isUpdated = Utility.ToString(dr["isUsed"]) == "U" ? true : false;
                    if (selected && !isUpdated)
                    {
                        ItineraryAddServiceDetails.UpdateFlightAcctStatus(addId, (int)Settings.LoginInfo.UserID);
                        lblSuccessMsg.Visible = true;
                        lblSuccessMsg.Text = "Updated Successfully";
                    }
                }
            }
            LoadQueue();
            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "HalaServiceQueue btnUpdateStatus_Click Event Error Raised:" + ex.ToString(), "");
        }
    }

}

