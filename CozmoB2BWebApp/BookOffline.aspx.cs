﻿using CT.BookingEngine;
using CT.Core;
using CT.Corporate;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web.Services;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class BookOffline : CT.Core.ParentPage
    {
        public AgentMaster clsAgentMaster;
        public OfflineItinerary clsOFI;
        public OfflineHotelItinerary clsOHI;
        public int iAgentId;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.Master.PageRole = true;
                AuthorizationCheck();
                if (!IsPostBack)
                {
                    ddlHotelCountry.DataSource = Country.GetCountryList();
                    ddlHotelCountry.DataTextField = "Key";
                    ddlHotelCountry.DataValueField = "Value";
                    ddlHotelCountry.DataBind();
                    if (ddlHotelCountry.Items.Count > 1)
                        ddlHotelCountry.Items.Insert(0, "--Select--");
                    txtCountries.Text = JsonConvert.SerializeObject(Country.GetCountryList());

                    iAgentId = Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId;
                    hdnUserId.Value = Settings.LoginInfo.UserID.ToString();
                    clsAgentMaster = new AgentMaster(iAgentId);

                    if (clsAgentMaster.RequiredFlexFields || Settings.LoginInfo.IsCorporate == "Y")
                    {
                        DataTable dtFlexFields = CorporateProfile.GetAgentFlexDetailsByProduct(iAgentId, 1);
                        txtFlexInfo.Text = JsonConvert.SerializeObject(dtFlexFields);
                        hdnFlexConfig.Value = Convert.ToString(ConfigurationManager.AppSettings["TRAVEL_REASON_FLEX_PAIR"]);
                    }

                    hdnIsCorp.Value = Settings.LoginInfo.IsCorporate;
                    if (hdnIsCorp.Value == "Y")
                    {
                        try
                        {
                            DataTable dtTravelReasons = TravelUtility.GetTravelReasonList("S", Settings.LoginInfo.AgentId, ListStatus.Short);
                            ddlFlightTravelReasons.DataSource = dtTravelReasons;
                            ddlFlightTravelReasons.DataTextField = "Description";
                            ddlFlightTravelReasons.DataValueField = "ReasonId";
                            ddlFlightTravelReasons.DataBind();
                            if (ddlFlightTravelReasons.Items.Count > 1)
                                ddlFlightTravelReasons.Items.Insert(0, "--Select Reason--");

                            DataTable dtProfiles = TravelUtility.GetProfileList(Settings.LoginInfo.CorporateProfileId, Settings.LoginInfo.AgentId, ListStatus.Short);
                            ddlFlightEmployee.DataSource = dtProfiles;
                            ddlFlightEmployee.DataTextField = "ProfileName";
                            ddlFlightEmployee.DataValueField = "Profile";
                            ddlFlightEmployee.DataBind();
                            if (ddlFlightEmployee.Items.Count > 1)
                                ddlFlightEmployee.Items.Insert(0, "--Select Traveller--");
                            if (Settings.LoginInfo.CorporateProfileId > 0)
                            {
                                CorporateProfile corpProfile = new CorporateProfile(Settings.LoginInfo.CorporateProfileId, Settings.LoginInfo.AgentId);
                                if (corpProfile.ProfileType != "TRAVELCORDINATOR" && corpProfile.ProfileType != "TVLCO")
                                {
                                    txtCorpProfile.Text = JsonConvert.SerializeObject(corpProfile);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Search, Severity.High, 0, "Failed to Bind Corporate details for offline booking. Reason :" + ex.ToString(), Request["REMOTE_ADDR"]);
                        }
                    }

                    clsOFI = new OfflineItinerary();
                    clsOFI.AgencyId = iAgentId;
                    clsOFI.Passenger = Enumerable.Repeat(new OfflinePaxInfo(), 10).ToArray();
                    clsOFI.Passenger.ToList().ForEach(x => { x.FlexDetailsList = Enumerable.Repeat(new FlightFlexDetails(), 40).ToList(); });
                    clsOFI.Segments = Enumerable.Repeat(new OfflineSegInfo(), 6).ToArray();
                    txtOffFltInfo.Text = JsonConvert.SerializeObject(clsOFI);

                    clsOHI = new OfflineHotelItinerary();
                    clsOHI.Roomtype = Enumerable.Repeat(new OfflineHotelRoom(), 5).ToArray();
                    clsOHI.Roomtype.ToList().ForEach(x => { x.PassenegerInfo = Enumerable.Repeat(new OfflinePaxInfo(), 10).ToList();
                        x.PassenegerInfo.ForEach(y => { y.FlexDetailsList = Enumerable.Repeat(new FlightFlexDetails(), 40).ToList(); });
                    });
                    txtOffHotelInfo.Text = JsonConvert.SerializeObject(clsOHI);
                }
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                Utility.Alert(this.Page, ex.Message);
            }
        }

        private void AuthorizationCheck()
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
        }
                
        //saving offline Booking
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
              
            }
            catch (Exception ex)
            {
               
            }
        }
        
        /// <summary>This web Method will call from client side ajax to get the list of passengers available based on given filters. 
        /// </summary>

        [WebMethod(EnableSession = true)]
        public static string GetFlexddlData(string sddlQuery)
        {
            string JSONString = string.Empty;
            try
            {
                DataTable dt = CorporateProfile.FillDropDown(sddlQuery.Replace("^", "'"));

                if (dt != null && dt.Rows.Count > 0)
                    JSONString = JsonConvert.SerializeObject(dt);
                return JSONString;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(GetFlexddlData)Failed to get flex field ddl data. Reason : " + ex.ToString(), "");
                return JSONString;
            }
        }

        /// <summary>This web Method will call from client side ajax to get passenger profile info based on profile id. 
        /// </summary>

        [WebMethod(EnableSession = true)]
        public static string GetCorpProfData(string sProfileId)
        {
            string JSONString = string.Empty;
            try
            {
                CorporateProfile corpProfile = new CorporateProfile(Convert.ToInt32(sProfileId.Split('~')[0]), Settings.LoginInfo.AgentId);

                if (corpProfile != null && corpProfile.DtProfileFlex != null && corpProfile.DtProfileFlex.Rows.Count > 0)
                    corpProfile.DtProfileFlex = corpProfile.DtProfileFlex.Select("ISNULL(FlexProductId, '1') = '1'").CopyToDataTable();

                JSONString = JsonConvert.SerializeObject(corpProfile);
                return JSONString;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(GetFlexddlData)Failed to get flex field ddl data. Reason : " + ex.ToString(), "");
                return JSONString;
            }
        }
        
        /// <summary>
        /// This method will save the offline booking details and send email to agent with all the saved booking details for both flight and hotel
        /// </summary>
        /// <param name="sFltitinerary"></param>
        /// <param name="sHotelitinerary"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string SaveOfflineBooking(string sFltitinerary, string sHotelitinerary, string hostName)
        {
            string JSONString = string.Empty;
            try
            {
                if (Settings.LoginInfo == null)
                    JSONString = "Session expired, please login once again";
                else
                {
                    MetaSearchEngine clsMSE = new MetaSearchEngine();
                    clsMSE.AppUserId = (int)Settings.LoginInfo.UserID; clsMSE.SettingsLoginInfo = Settings.LoginInfo;
                    JSONString = clsMSE.CreateOfflineItinerary(sFltitinerary.Replace("^", "'"), sHotelitinerary.Replace("^", "'"), hostName);
                }

                return JSONString;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(SaveOfflineBooking)Failed to save offline booking. Reason : " + ex.ToString(), "");
                return JSONString = ex.ToString();
            }
        }
    }
}
