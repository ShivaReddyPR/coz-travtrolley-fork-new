﻿using CT.BookingEngine;
using CT.CMS;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using ClosedXML.Excel;
using System.IO;
using System.Configuration;
using System.Web.Script.Services;
using System.Web.Services;
using Newtonsoft.Json;

namespace CozmoB2BWebApp
{
    public partial class CorporateExcelDownload : System.Web.UI.Page
    {
        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Settings.LoginInfo != null) //Authorisation Check -- if success
                {
                    if (!IsPostBack)
                    {
                        IntialiseControls();

                    }

                    if (hdnSubmit.Value == "ExporttoExcel")
                    {
                        ExporttoExcel();
                    }
                }
                else//Authorisation Check -- if failed
                {
                    Response.Redirect("AbandonSession.aspx");
                }
            }
            catch
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateExcelDownload) Page Load Event Error: " + ex.Message, "0");
            }
        }

        private void IntialiseControls()
        {
            try
            {
                dcFromDate.Value = DateTime.Now;
                dcToDate.Value = DateTime.Now;

                int id = 0;
                HtmlTableRow row = new HtmlTableRow();

                HtmlTableCell cell1 = new HtmlTableCell();
                CheckBox chkAgent1 = new CheckBox();
                chkAgent1.ID = "chkAgent" + id.ToString();
                chkAgent1.Text = "All";
                chkAgent1.Checked = true;
                chkAgent1.Attributes.Add("onclick", "setCheck('ctl00_cphTransaction_" + chkAgent1.ID + "')");
                cell1.Controls.Add(chkAgent1);

                row.Cells.Add(cell1);
                id++;
                DataTable dtAgents = AgentMaster.PetrofacAgentlist();
                foreach (DataRow dr in dtAgents.Rows)
                {
                    HtmlTableCell cell = new HtmlTableCell();
                    CheckBox chkAgent = new CheckBox();
                    chkAgent.ID = "chkAgent" + id.ToString();
                    chkAgent.Text = Convert.ToString(dr["Name"]);
                    HiddenField hiddenField = new HiddenField();
                    hiddenField.ID = "hdfAgentId" + id.ToString();
                    hiddenField.Value = Convert.ToString(dr["Id"]);
                    hdnSelectedAgents.Value += Convert.ToString(dr["Id"]) + ",";
                    
                    chkAgent.Checked = true;

                    chkAgent.Attributes.Add("onclick", "setCheck('ctl00_cphTransaction_" + chkAgent.ID + "')");
                    cell.Controls.Add(chkAgent);
                    cell.Controls.Add(hiddenField);
                    row.Cells.Add(cell);
                    id++;
                }

                hdnSelectedAgents.Value = hdnSelectedAgents.Value.TrimEnd(',');
                hdnActiveAgents.Value = id.ToString();

                tblAgents.Rows.Add(row);

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        
        void ExporttoExcel()
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

            DateTime FromDate = Convert.ToDateTime(hdnFromdate.Value, dateFormat);
            DateTime ToDate = Convert.ToDateTime(hdnTodate.Value, dateFormat);
            string AgentIds = hdnSelectedAgents.Value;

            DataSet ds = new DataSet();
            var SPNames = new List<string>() { "usp_GetAirOffline_data-AirOffline", "usp_GetAirOnline_data-AirOnline", "usp_GetHotelOffline_data-HotelOffline", "usp_GetHotelOnline_data-HotelOnline" };


            foreach (var SPName in SPNames)
            {
                DataTable dt = new DataTable();
                dt = TravelUtility.GetExcelDatabySPName(FromDate, ToDate, AgentIds, SPName.Split(new Char[] { '-' })[0]);
                if (dt.Rows.Count > 0)
                { 
                    dt.TableName = SPName.Split(new Char[] { '-' })[1];
                    ds.Tables.Add(dt);
                }
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(ds);
                wb.Style.Alignment.Horizontal = XLAlignmentHorizontalValues.Center;
                wb.Style.Font.Bold = true;
                Response.Clear();
                Response.Buffer = true;
                Response.Charset = "";
                Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                Response.AddHeader("content-disposition", "attachment;filename= CorpExcels.xlsx");
                using (MemoryStream MyMemoryStream = new MemoryStream())
                {
                    wb.SaveAs(MyMemoryStream);
                    MyMemoryStream.WriteTo(Response.OutputStream);
                    Response.Flush();
                    Response.End();
                }
            }

        }
    }
}