﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="True" CodeBehind="AgentMarkupUpdate.aspx.cs" Inherits="AgentMarkupUpdateGUI" Title="Agent Markup Update" %>
  <%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<%--   <script type="test/css">
    .tblpax {font-size:13px; border: solid 1px #ccc; }
    .heading {background:#DADADA; font-size:13px; margin-left:10px;  font-weight:bold;}
    </script>--%>
      <style type="text/css">
    .chkBoxList td
    {
        width:160px;
    }
    </style>
<script type="text/javascript">

       

    function validateDtlAddUpdate(action, id, product) {
        try {

            var msg = '';
            var rowId = id.substring(0, id.lastIndexOf('_'));
            
            if (action == "I" || action == "E")//Edit
            {
                if (product == "Flight") {
                    //if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
                    if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
                    if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
                    if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
                    if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
                    if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
                    if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
                    if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
                }
                else if (product == "Hotel") {
                    if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
                    //if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
                    if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
                    if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
                    if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
                    if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
                    if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
                    if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
                }
                else if (product == "Package") {
                    if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
                    if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
                    //if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
                    if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
                    if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
                    if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
                    if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
                    if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
                }
                else if (product == "Activity") {
                    if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
                    if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
                    if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
                    //if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
                    if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
                    if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
                    if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
                    if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
                }
                else if (product == "Insurance") {
                    if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
                    if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
                    if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
                    if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
                    //if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
                    if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
                    if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
                    if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
                }
                else if (product == "Car") {
                    if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
                    if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
                    if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
                    if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
                    if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
                    //if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
                    if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
                    if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
                }
                else if (product == "FixedDeparture") {
                    if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
                    if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
                    if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
                    if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
                    if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
                    if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
                    // if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
                    if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
                }
                else if (product == "Sight") {
                    if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
                    if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
                    if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
                    if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
                    if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
                    if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
                     if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
                    //if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
                }
            }
            
                    if (action == 'I')//Insert
                     {
                        if (product != "Package" && product != "Activity" ) 
                            if (document.getElementById(rowId + '_FTddlSource').selectedIndex <= 0) addMessage('Please select Source from the list !','');

                        if (document.getElementById(rowId + '_FTddlMarkupType').selectedIndex <= 0) addMessage('Please select Markup Type from the list !', '');                        
                        if (document.getElementById(rowId + '_FTtxtAgentMarkup').value == '') addMessage('Agent Markup cannot be blank !', '');
                        
                        if (document.getElementById(rowId + '_FTtxtOurCommission').value == '') addMessage('Our Commission cannot be blank !', '');
                        //Added by Anji on 22/01/2020 for Discount 
                        if (document.getElementById(rowId + '_FTtxtDiscount').value == '') addMessage('Our Discount cannot be blank !','');
                        //End
                        //if (product == "Insurance" || product == "Sight" || product == "Transfer")
                            if (document.getElementById(rowId + '_FTddlDiscountType').selectedIndex <= 0) addMessage('Please select Discount Type from the list !','');
                       
                        
                    }
                    else if (action == 'U')//Update
                    {
                        if (product != "Package" && product != "Activity")
                            if (document.getElementById(rowId + '_EITddlSource').selectedIndex <= 0) addMessage('Please select Source from the list !','');

                        if (document.getElementById(rowId + '_EITddlMarkupType').selectedIndex <= 0) addMessage('Please select Markup Type from the list !','');
                        if (document.getElementById(rowId + '_EITtxtAgentMarkup').value == '') addMessage('Agent Markup cannot be blank !', '');
                        if (document.getElementById(rowId + '_EITtxtOurCommission').value == '') addMessage('Our Commission cannot be blank !', '');
                        //Added by Anji on 22/01/2020 for Discount 
                         if (document.getElementById(rowId + '_EITtxtDiscount').value == '') addMessage('Our Discount cannot be blank !','');
                        //End
                        //if (product == "Insurance" || product == "Sight" || product == "Transfer")
                            if (document.getElementById(rowId + '_EITddlDiscountType').selectedIndex <= 0) addMessage('Please select Discount Type from the list !','');
                        //if (document.getElementById(rowId + '_EITddlDiscountType').selectedIndex <= 0) {
                        //    $('#' + rowId + '_EITddlDiscountType').select2('val', 'P');//Select Location
                        //}
                    }
              
            if (getMessage() != '') {
                //alert(getMessage()); 
                alert(getMessage());
                clearMessage(); return false;
            }
            return true;
        }
        catch (e) {
            //alert(e.description);
            return true;
        }
    }
    function ShowMarkups(product)
    {
        if (product == "Flight")
        {            
            $('.nav-tabs li:eq(0) a').tab('show');
            document.getElementById('flight-details').className = "tab-pane active";
            document.getElementById('hotel-details').className = "tab-pane";            
            document.getElementById('package-details').className = "tab-pane";
            document.getElementById('activity-details').className = "tab-pane";
            document.getElementById('insurance-details').className = "tab-pane";
            document.getElementById('car-details').className = "tab-pane";
            document.getElementById('sight-details').className = "tab-pane";
            document.getElementById('transfer-details').className = "tab-pane";
        }
        else if (product == "Hotel")
        {
            $('.nav-tabs li:eq(1) a').tab('show');
            document.getElementById('flight-details').className = "tab-pane";
            document.getElementById('hotel-details').className = "tab-pane active";            
            document.getElementById('package-details').className = "tab-pane";
            document.getElementById('activity-details').className = "tab-pane";
            document.getElementById('insurance-details').className = "tab-pane";
            document.getElementById('car-details').className = "tab-pane";
            document.getElementById('sight-details').className = "tab-pane";
            document.getElementById('transfer-details').className = "tab-pane";
        }
        else if (product == "Package") {
            $('.nav-tabs li:eq(2) a').tab('show');
            document.getElementById('flight-details').className = "tab-pane";
            document.getElementById('hotel-details').className = "tab-pane";
            document.getElementById('package-details').className = "tab-pane active";
            document.getElementById('activity-details').className = "tab-pane";
            document.getElementById('insurance-details').className = "tab-pane";
            document.getElementById('car-details').className = "tab-pane";
            document.getElementById('sight-details').className = "tab-pane";
            document.getElementById('transfer-details').className = "tab-pane";
        }
        else if (product == "Activity") {
            $('.nav-tabs li:eq(3) a').tab('show');
            document.getElementById('flight-details').className = "tab-pane";
            document.getElementById('hotel-details').className = "tab-pane";
            document.getElementById('package-details').className = "tab-pane";
            document.getElementById('activity-details').className = "tab-pane active";
            document.getElementById('insurance-details').className = "tab-pane";
            document.getElementById('car-details').className = "tab-pane";
            document.getElementById('sight-details').className = "tab-pane";
            document.getElementById('transfer-details').className = "tab-pane";
        }
        else if (product == "Insurance") {
            $('.nav-tabs li:eq(4) a').tab('show');
            document.getElementById('flight-details').className = "tab-pane";
            document.getElementById('hotel-details').className = "tab-pane";
            document.getElementById('package-details').className = "tab-pane";
            document.getElementById('activity-details').className = "tab-pane";
            document.getElementById('insurance-details').className = "tab-pane active";
            document.getElementById('car-details').className = "tab-pane";
            document.getElementById('sight-details').className = "tab-pane";
            document.getElementById('transfer-details').className = "tab-pane";
        }
        else if (product == "Car") {
            $('.nav-tabs li:eq(5) a').tab('show');
            document.getElementById('flight-details').className = "tab-pane";
            document.getElementById('hotel-details').className = "tab-pane";
            document.getElementById('package-details').className = "tab-pane";
            document.getElementById('activity-details').className = "tab-pane";
            document.getElementById('insurance-details').className = "tab-pane";
            document.getElementById('car-details').className = "tab-pane active";
            document.getElementById('sight-details').className = "tab-pane";
            document.getElementById('transfer-details').className = "tab-pane";
        }
        else if (product == "Sight") {
            $('.nav-tabs li:eq(6) a').tab('show');
            document.getElementById('flight-details').className = "tab-pane";
            document.getElementById('hotel-details').className = "tab-pane";
            document.getElementById('package-details').className = "tab-pane";
            document.getElementById('activity-details').className = "tab-pane";
            document.getElementById('insurance-details').className = "tab-pane";
            document.getElementById('car-details').className = "tab-pane";
            document.getElementById('sight-details').className = "tab-pane active";
            document.getElementById('transfer-details').className = "tab-pane";
        }
        else if (product == "Transfer") {
            $('.nav-tabs li:eq(7) a').tab('show');
            document.getElementById('flight-details').className = "tab-pane";
            document.getElementById('hotel-details').className = "tab-pane";
            document.getElementById('package-details').className = "tab-pane";
            document.getElementById('activity-details').className = "tab-pane";
            document.getElementById('insurance-details').className = "tab-pane";
            document.getElementById('car-details').className = "tab-pane";
            document.getElementById('sight-details').className = "tab-pane";
            document.getElementById('transfer-details').className = "tab-pane active";
        }


    }

    function CalculateTotalMarkup(action, id)
    {
        var rowId = id.substring(0, id.lastIndexOf('_'));
    
        var agentMarkup = 0;
        var ourCommission = 0;
        var totalMarkup = 0;
      
        if(action=="I")
        {           

            agentMarkup = parseFloat(isNaN(document.getElementById(rowId + '_FTtxtAgentMarkup').value)) ? 0 : document.getElementById(rowId + '_FTtxtAgentMarkup').value;
            ourCommission = parseFloat(isNaN(document.getElementById(rowId + '_FTtxtOurCommission').value)) ? 0 : document.getElementById(rowId + '_FTtxtOurCommission').value;
            if (agentMarkup == '') agentMarkup = 0;
            if (ourCommission == '') ourCommission = 0;
            totalMarkup =   parseFloat(agentMarkup) + parseFloat(ourCommission);            
            document.getElementById(rowId + '_FTtxtMarkup').value = totalMarkup;
        }
        else if (action == "U") {
            agentMarkup = parseFloat(isNaN(document.getElementById(rowId + '_EITtxtAgentMarkup').value)) ? 0 : document.getElementById(rowId + '_EITtxtAgentMarkup').value;
            ourCommission = parseFloat(isNaN(document.getElementById(rowId + '_EITtxtOurCommission').value)) ? 0 : document.getElementById(rowId + '_EITtxtOurCommission').value;
            if (agentMarkup == '') agentMarkup = 0;
            if (ourCommission == '') ourCommission = 0;
            totalMarkup = parseFloat(agentMarkup) + parseFloat(ourCommission);
            document.getElementById(rowId + '_EITtxtMarkup').value = totalMarkup;
        }
    }
    function CheckEventMode(product)
    {
        try {
            if (document.getElementById('ddlAgent').selectedIndex == 0) addMessage('Please select agent from the list !', '');
            else if (product == "Flight") {
            //if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
            if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
            if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
            if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
            if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
            if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
            if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
            if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
            if (getElement('hdfTransferMode').value != "0") addMessage('Transfer Markup Details in Edit Mode!', '');
            
        }
        else if (product == "Hotel") {
            if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
            //if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
            if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
            if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
            if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
            if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
            if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
            if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
            if (getElement('hdfTransferMode').value != "0") addMessage('Transfer Markup Details in Edit Mode!', '');
            
        }
        else if (product == "Package") {
            if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
            if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
            //if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
            if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
            if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
            if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
            if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
            if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
            if (getElement('hdfTransferMode').value != "0") addMessage('Transfer Markup Details in Edit Mode!', '');
        }
        else if (product == "Activity") {
            if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
            if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
            if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
            //if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
            if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
            if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
            if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
            if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
            if (getElement('hdfTransferMode').value != "0") addMessage('Transfer Markup Details in Edit Mode!', '');
        }
        else if (product == "Insurance") {
            if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
            if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
            if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
            if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
            //if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
            if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
            if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
            if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
            if (getElement('hdfTransferMode').value != "0") addMessage('Transfer Markup Details in Edit Mode!', '');
        }
        else if (product == "Car") {
            if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
            if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
            if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
            if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
            if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
            //if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
            if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
            if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
            if (getElement('hdfTransferMode').value != "0") addMessage('Transfer Markup Details in Edit Mode!', '');
        }
        else if (product == "FixedDeparture") {
            if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
            if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
            if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
            if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
            if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
            if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
            // if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
            if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
            if (getElement('hdfTransferMode').value != "0") addMessage('Transfer Markup Details in Edit Mode!', '');
        }
        else if (product == "Sight") {
            if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
            if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
            if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
            if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
            if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
            if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
            if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
            //if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
            if (getElement('hdfTransferMode').value != "0") addMessage('Transfer Markup Details in Edit Mode!', '');
        }
        else if (product == "Transfer") {
            if (getElement('hdfFlightMode').value != "0") addMessage('Flight Markup Details in Edit Mode!', '');
            if (getElement('hdfHotelMode').value != "0") addMessage('Hotel Markup Details in Edit Mode!', '');
            if (getElement('hdfPackageMode').value != "0") addMessage('Package Markup Details in Edit Mode!', '');
            if (getElement('hdfActivityMode').value != "0") addMessage('Ativity Markup Details in Edit Mode!', '');
            if (getElement('hdfInsurancemode').value != "0") addMessage('Insurance Markup Details in Edit Mode!', '');
            if (getElement('hdfCarMode').value != "0") addMessage('Car Markup Details in Edit Mode!', '');
            if (getElement('hdfFixedDepMode').value != "0") addMessage('Fixed Departure Markup Details in Edit Mode!', '');
            if (getElement('hdfSightMode').value != "0") addMessage('Sight Seeing Markup Details in Edit Mode!', '');
            //if (getElement('hdfTransferMode').value != "0") addMessage('Transfer Markup Details in Edit Mode!', '');
        }

             if (getMessage() != '') {
                 //alert(getMessage()); 
                 alert(getMessage());
                 clearMessage(); return false;
             }
             return true;
         }
         catch (e) {
             //alert(e.description);
             return true;
         }

    }
</script>

    <div class="">
       
        <div class="ns-h3"> Update Markup</div>
        <asp:HiddenField ID="hdnCount" runat="server" />
        <asp:HiddenField id="hdfFlightMKupRowId" runat="server"></asp:HiddenField>
        <asp:HiddenField id="hdfHotelMKupRowId" runat="server"></asp:HiddenField>
        <asp:HiddenField id="hdfPackageMKupRowId" runat="server"></asp:HiddenField>
        <asp:HiddenField id="hdfActivityMKupRowId" runat="server"></asp:HiddenField>
        <asp:HiddenField id="hdfInsuranceMKupRowId" runat="server"></asp:HiddenField>
        <asp:HiddenField id="hdfFixedDepMKupRowId" runat="server"></asp:HiddenField>
        <asp:HiddenField id="hdfCarMKupRowId" runat="server"></asp:HiddenField>
        <asp:HiddenField id="hdfSightMKupRowId" runat="server"></asp:HiddenField>
        <asp:HiddenField id="hdfTransferMKupRowId" runat="server"></asp:HiddenField>
       <div class="paramcon bg_white bor_gray pad_10">
       
       
      
                  <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-1 col-xs-2 marbot_10"> <asp:Label ID="lblAgent" runat="server" Text="Agent:" Font-Bold="true"></asp:Label></div>
    
    
    <div class="col-md-2 col-xs-10 marbot_10"><asp:DropDownList CssClass="form-control" ID="ddlAgent" runat="server" Style="display: block;" AutoPostBack="true" 
                                OnSelectedIndexChanged="ddlAgent_OnSelectedIndexChanged">
                                </asp:DropDownList> 
                                <div id="errMess" runat="server" class="error_module" style="display: none;">
                                    <div id="errorMessage" runat="server" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                                    </div>
                                </div>
                                
                                
                                </div>
                                
                                
    <div class="col-md-6 col-xs-12"> <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ForeColor="Green"></asp:Label> <asp:Button ID="btnUpdate" CssClass="btn but_b" Visible="false" runat="server" Text="Update"
                        OnClientClick="return validation();" />
                        
                        
                        
                  <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn but_b"  OnClick="btnClear_OnClick"/>      
                        </div>



    <div class="clearfix"></div>
    </div>

 <div class="CorpTrvl-tabbed-panel">
                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs responsive" role="tablist">
                        <%--<li role="presentation" class="active"><a href="#flight-details">Flight</a></li>--%>
                        <li role="presentation" class="active"><asp:LinkButton  runat="server"  ID="lnkFlight" OnClientClick="return CheckEventMode('Flight');" OnClick="lnkFlight_Click" >Flight</asp:LinkButton></li>
                        <%--<li role="presentation" class=""><a href="#hotel_details" aria-controls="hotel-details" role="tab" data-toggle="tab" aria-expanded="true">Hotel</a></li>--%>
                        <li role="presentation" class=""><asp:LinkButton  runat="server"  ID="lnkHotel" OnClientClick="return CheckEventMode('Hotel');" OnClick="lnkHotel_Click" >Hotel</asp:LinkButton></li>
                        <%--<li role="presentation" class=""><a href="#package-details" aria-controls="login-info" role="tab" data-toggle="tab" aria-expanded="true">Packages</a></li>--%>
                        <li role="presentation" class=""><asp:LinkButton  runat="server"  ID="lnkPackage" OnClientClick="return CheckEventMode('Package');" OnClick="lnkPackage_Click" >Package</asp:LinkButton></li>
                        <%--<li role="presentation" class=""><a href="#activity-details" aria-controls="login-info" role="tab" data-toggle="tab" aria-expanded="true">Activity</a></li>--%>
                        <li role="presentation" class=""><asp:LinkButton  runat="server"  ID="lnkActivity" OnClientClick="return CheckEventMode('Activity');" OnClick="lnkActivity_Click" >Activity</asp:LinkButton></li>
                        <%--<li role="presentation" class=""><a href="#insurance-details" aria-controls="login-info" role="tab" data-toggle="tab" aria-expanded="true">Insurance</a></li>--%>
                        <li role="presentation" class=""><asp:LinkButton  runat="server"  ID="lnkInsurance" OnClientClick="return CheckEventMode('Insurance');" OnClick="lnkInsurance_Click" >Insurance</asp:LinkButton></li>

                        <%--<li role="presentation" class=""><a href="#car-details" aria-controls="car-details" role="tab" data-toggle="tab" aria-expanded="true">Car</a></li>--%>
                        <li role="presentation" class=""><asp:LinkButton  runat="server"  ID="lnkCar" OnClientClick="return CheckEventMode('Car');" OnClick="lnkCar_Click" >Car</asp:LinkButton></li>

                        <%--<li role="presentation" class="active"><a href="#fixedDep-details" aria-controls="fixedDep-details" role="tab" data-toggle="tab" aria-expanded="true">Fixed Departure</a></li>--%>
                         <li role="presentation" class=""><asp:LinkButton  runat="server"  ID="lnkSight" OnClientClick="return CheckEventMode('Sight');" OnClick="lnkSight_Click" >Sight Seeing</asp:LinkButton></li>
                         <li role="presentation" class=""><asp:LinkButton  runat="server"  ID="lnkTransfer" OnClientClick="return CheckEventMode('Transfer');" OnClick="lnkTransfer_Click" >Transfer</asp:LinkButton></li>
                        

                        
                        
                    </ul>
    <div class="tab-content responsive">
                        <div role="tabpanel" class="tab-pane active" id="flight-details">
                        <asp:HiddenField id="hdfFlightMode" runat="server"></asp:HiddenField>
                         <div class="table table-responsive table-max-height"> 
                            <asp:GridView ID="gvFlightMkp" runat="server" CssClass="table table-bordered b2b-corp-table"  AutoGenerateColumns="False" Width="100%"
                        AllowPaging="true" PageSize="20" DataKeyNames="MRDId" CellPadding="0" ShowFooter="true"
                                    CellSpacing="0"  GridLines="none" 
                        OnPageIndexChanging="gvFlightMkp_PageIndexChanging" 
                        OnRowCommand="gvFlightMkp_RowCommand" OnRowEditing="gvFlightMkp_RowEditing" 
                        OnRowCancelingEdit="gvFlightMkp_RowCancelingEdit" OnRowUpdating="gvFlightMkp_RowUpdating" OnRowDeleting="gvFlightMkp_RowDeleting">
                                   
                                        <PagerStyle CssClass="pagination-style" />  
                                    <Columns>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" CssClass="thead" />
                                        <HeaderTemplate>
                                        Source
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblSource" runat="server" Text='<%# Eval("SourceId") %>' ToolTip='<%# Eval("SourceId") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:DropDownList Id="EITddlSource" style="width:150px;" runat="server" ></asp:DropDownList>
                                            <asp:HiddenField Id="EITHdfMRId"   Value='<%# Eval("MRId") %>'  runat="server" ></asp:HiddenField>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:DropDownList Id="FTddlSource" style="width:150px;" runat="server" ></asp:DropDownList >
                                             <asp:HiddenField Id="FTHdfMRId"   Value="-1"  runat="server" ></asp:HiddenField>
                                            </FooterTemplate>
                                    </asp:TemplateField>
                                       <%-- Added by Somasekhar on 19-02-2019 for Booking Type (N-Normal , I-Import)--%>
                                        <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        <label>Booking  Type</label> 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblBookingType" runat="server" Text='<%# Eval("BookingTypeName") %>' ToolTip='<%# Eval("BookingTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlBookingType" Text='<%# Eval("BookingType") %>' runat="server" >
                                                    <asp:ListItem Text="Normal" Value="N"></asp:ListItem>
                                                    <asp:ListItem Text="Import" Value="I"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlBookingType" runat="server" > 
                                                    <asp:ListItem Text="Nomal" Value="N"></asp:ListItem>
                                                    <asp:ListItem Text="Import" Value="I"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                       <%-- -------%>
                                     
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            <label>Flight Type</label>
                                            </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblFlightType" runat="server" Text='<%# Eval("FlightTypeDesc") %>' ToolTip='<%# Eval("FlightTypeDesc") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                            <asp:DropDownList Id="EITddlFlightType" Text='<%# Eval("FlightType") %>' runat="server" >
                                                <%--<asp:ListItem Text="--Select Flight Type--" Value="-1"></asp:ListItem>--%>
                                                <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                                <asp:ListItem Text="Domestic" Value="DOM"></asp:ListItem>
                                                <asp:ListItem Text="International" Value="INT"></asp:ListItem>
                                                
                                            </asp:DropDownList>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlFlightType" runat="server" >
                                                    <%--<asp:ListItem Text="--Select Flight Type--" Value="-1"></asp:ListItem>--%>
                                                    <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                                    <asp:ListItem Text="Domestic" Value="DOM"></asp:ListItem>
                                                    <asp:ListItem Text="International" Value="INT"></asp:ListItem>
                                                </asp:DropDownList>
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                        
                                         <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        <label>Journey Type</label>
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblJourneyTpe" runat="server" Text='<%# Eval("JourneyTypeDesc") %>' ToolTip='<%# Eval("JourneyTypeDesc") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlJourneyType" Text='<%# Eval("JourneyType") %>' runat="server" >
                                                    <%--<asp:ListItem Text="--Select Journey Type--" Value="-1"></asp:ListItem>--%>
                                                    <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                                    <asp:ListItem Text="Return" Value="RET"></asp:ListItem>
                                                    <asp:ListItem Text="Onward" Value="ONW"></asp:ListItem>
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlJourneyType" runat="server" >
                                                    <%--<asp:ListItem Text="--Select Journey Type--" Value="-1"></asp:ListItem>--%>
                                                    <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                                    <asp:ListItem Text="Return" Value="RET"></asp:ListItem>
                                                    <asp:ListItem Text="Onward" Value="ONW"></asp:ListItem>                                   
                                    
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>

                                         <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        <label>Carrier Type</label>
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblCarrierType" runat="server" Text='<%# Eval("CarrierType") %>' ToolTip='<%# Eval("CarrierType") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlCarrierType" Text='<%# Eval("CarrierType") %>' runat="server" >
                                                    <%--<asp:ListItem Text="--Select Carrier Type--" Value="-1"></asp:ListItem>--%>
                                                    <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                                    <asp:ListItem Text="LCC" Value="LCC"></asp:ListItem>
                                                    <asp:ListItem Text="GDS" Value="GDS"></asp:ListItem>
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlCarrierType" runat="server" >
                                                    <%--<asp:ListItem Text="--Select Carrier Type--" Value="-1"></asp:ListItem>--%>
                                                   <asp:ListItem Text="All" Value="All"></asp:ListItem>
                                                    <asp:ListItem Text="LCC" Value="LCC"></asp:ListItem>
                                                    <asp:ListItem Text="GDS" Value="GDS"></asp:ListItem>                               
                                    
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>

                                    
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            <label>Agent Markup</label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAgentMarkup" runat="server" Text='<%# Eval("AgentMarkup") %>' ToolTip='<%# Eval("AgentMarkup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtAgentMarkup" runat="server" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("AgentMarkup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtAgentMarkup" Text="0.00" style="text-transform:capitalize;"  onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>   
                                        
                                      <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            <label>Our Commission</label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblOurCommission" runat="server" Text='<%# Eval("OurCommission") %>' ToolTip='<%# Eval("OurCommission") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtOurCommission" runat="server" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("OurCommission") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtOurCommission" Text="0.00" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8"  runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>                                    
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            <label>Total Markup</label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblMarkup" runat="server" Text='<%# Eval("Markup") %>' ToolTip='<%# Eval("Markup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtMarkup" runat="server" style="text-transform:capitalize;" Enabled="false" Maxlength="50" Text='<%# Eval("Markup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtMarkup" Text="0.00" style="text-transform:capitalize;" Enabled="false"  runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        <label>Markup  Type</label>
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblMarkupType" runat="server" Text='<%# Eval("MarkupTypeName") %>' ToolTip='<%# Eval("MarkupTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlMarkupType" Text='<%# Eval("MarkupType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlMarkupType" runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                       <%-- Added by Anji on 21-01-2020 For Discount & Discount Type--%>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            <label>Discount</label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDiscount" runat="server" Text='<%# Eval("Discount") %>' ToolTip='<%# Eval("Discount") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtDiscount" runat="server" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');"  Maxlength="8" Text='<%# Eval("Discount") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtDiscount" Text="0.00" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');"  Maxlength="8"  runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField> 
                                <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        <label>Discount Type</label>
                                     
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblDiscountType" runat="server" Text='<%# Eval("DiscountTypeName") %>' ToolTip='<%# Eval("DiscountTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlDiscountType" Text='<%# Eval("DiscountType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlDiscountType" runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                        <%--End by Anji--%>
                                    <asp:TemplateField>
                                        <HeaderTemplate><asp:Label runat="server" ID="id"></asp:Label></HeaderTemplate>
                                        <ItemTemplate >
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkEdit"  CssClass="btn btn-control btn-default btn-sm" data-icon="fa fa-edit"  runat="server" Text="Edit" OnClientClick="return validateDtlAddUpdate('E',this.id,'Flight')"  commandname="Edit">
                                            </asp:LinkButton>
                                          <%--<div  class="action-btn-wrap">
                                            <asp:LinkButton ID = "ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the Pax Details?');" ></asp:LinkButton>
                                              <a href class="btn btn-control btn-cancel btn-sm" data-icon="fa fa-trash">DELETE</a>
                                          </div>--%>
                                            </div>
                                        </ItemTemplate>  
                                        
                                        <EditItemTemplate>
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkUpdate"  runat="server" Text="Update" CssClass="btn btn-control btn-save btn-sm" data-icon="vms-icon-check"  commandname="Update" OnClientClick="return validateDtlAddUpdate('U',this.id,'Flight')" ></asp:LinkButton>
                                            <asp:LinkButton ID="lnkCancel"  runat="server" Text="Cancel" CssClass="btn btn-control btn-clear btn-sm" data-icon="icon fa fa-times"  commandname="Cancel" ></asp:LinkButton></div>
                                            
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" />
                                        <FooterTemplate>
                                        
                                                <asp:LinkButton ID="lnkAdd" commandname="Add" runat="server" CssClass="btn btn-control btn-save btn-sm" data-icon="fa fa-plus" Text="Add" OnClientClick="return validateDtlAddUpdate('I',this.id,'Flight')"></asp:LinkButton>
                                     
                                        </FooterTemplate>  
                                                                            
                                        
                                        </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gvHeader01" />
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />                                    
                                </asp:GridView>    
                       </div>
                        </div>       

                        <div role="tabpanel" class="tab-pane" id="hotel-details">
                             <asp:HiddenField id="hdfHotelMode" runat="server"></asp:HiddenField>
                         <div class="table table-responsive table-max-height"> 
                            <asp:GridView ID="gvHotelMkp" runat="server" CssClass="table table-bordered b2b-corp-table"  AutoGenerateColumns="False" Width="100%"
                        AllowPaging="true" PageSize="20" DataKeyNames="MRDId" CellPadding="0" ShowFooter="true"
                                    CellSpacing="0"  GridLines="none" 
                        OnPageIndexChanging="gvHotelMkp_PageIndexChanging" 
                        OnRowCommand="gvHotelMkp_RowCommand" OnRowEditing="gvHotelMkp_RowEditing" 
                        OnRowCancelingEdit="gvHotelMkp_RowCancelingEdit" OnRowUpdating="gvHotelMkp_RowUpdating" OnRowDeleting="gvHotelMkp_RowDeleting">
                                   
                                        <PagerStyle CssClass="pagination-style" />  
                                    <Columns>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" CssClass="thead" />
                                        <HeaderTemplate>
                                        Source
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblSource" runat="server" Text='<%# Eval("SourceId") %>' ToolTip='<%# Eval("SourceId") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:DropDownList Id="EITddlSource" style="width:150px;" runat="server" ></asp:DropDownList>
                                            <asp:HiddenField Id="EITHdfMRId"  Value='<%# Eval("MRId") %>' runat="server" ></asp:HiddenField>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:DropDownList Id="FTddlSource" style="width:150px;" runat="server" ></asp:DropDownList >
                                             <asp:HiddenField Id="FTHdfMRId"   Value="-1"  runat="server" ></asp:HiddenField>
                                            </FooterTemplate>
                                    </asp:TemplateField>

                                     <%-- Added by Anji on 17-03-2020 for Booking Type (N-Normal , C-Cancelation)--%>
                                        <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        <label>Booking  Type</label> 
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblBookingType" runat="server" Text='<%# Eval("BookingTypeName") %>' ToolTip='<%# Eval("BookingTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlBookingType" Text='<%# Eval("BookingType") %>' runat="server" >
                                                    <asp:ListItem Text="Normal" Value="N"></asp:ListItem>
                                                    <asp:ListItem Text="Cancelation" Value="C"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlBookingType" runat="server" > 
                                                    <asp:ListItem Text="Nomal" Value="N"></asp:ListItem>
                                                    <asp:ListItem Text="Cancelation" Value="C"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                       <%-- -------%>
                                                                       
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Agent Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAgentMarkup" runat="server" Text='<%# Eval("AgentMarkup") %>' ToolTip='<%# Eval("AgentMarkup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtAgentMarkup" runat="server" style="text-transform:capitalize;" Maxlength="8" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Text='<%# Eval("AgentMarkup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtAgentMarkup" Text="0.00" style="text-transform:capitalize;"  onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8"  runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>   
                                        
                                      <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Our Commission
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblOurCommission" runat="server" Text='<%# Eval("OurCommission") %>' ToolTip='<%# Eval("OurCommission") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtOurCommission" runat="server" style="text-transform:capitalize;"  onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("OurCommission") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtOurCommission" Text="0.00" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>                                    
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Total Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblMarkup" runat="server" Text='<%# Eval("Markup") %>' ToolTip='<%# Eval("Markup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtMarkup" runat="server" style="text-transform:capitalize;"  Enabled="false" Maxlength="50" Text='<%# Eval("Markup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtMarkup" Text="0.00" style="text-transform:capitalize;" Enabled="false" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        Markup  Type
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblMarkupType" runat="server" Text='<%# Eval("MarkupTypeName") %>' ToolTip='<%# Eval("MarkupTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlMarkupType" Text='<%# Eval("MarkupType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlMarkupType" runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                         <%-- Added by Anji on 21-01-2020 For Discount & Discount Type--%>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            <label>Discount</label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDiscount" runat="server" Text='<%# Eval("Discount") %>' ToolTip='<%# Eval("Discount") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtDiscount" runat="server" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');"  Maxlength="8"  Text='<%# Eval("Discount") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtDiscount" Text="0.00" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');"  Maxlength="8"   runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField> 
                                <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        <label>Discount Type</label>
                                     
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblDiscountType" runat="server" Text='<%# Eval("DiscountTypeName") %>' ToolTip='<%# Eval("DiscountTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlDiscountType" Text='<%# Eval("DiscountType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlDiscountType" runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                    <%--End by Anji--%>
                                       <asp:TemplateField>
                                            <HeaderStyle HorizontalAlign="left" />
                                            <HeaderTemplate>
                                                MF Discount
                                            </HeaderTemplate>
                                            <ItemTemplate>
                                                <asp:Label ID="ITlblMFDiscount" runat="server" Text='<%# Eval("MFDiscount") %>' ToolTip='<%# Eval("MFDiscount") %>' Width="110px"></asp:Label>
                                            </ItemTemplate>
                                            <EditItemTemplate>                                            
                                                <EditRowStyle VerticalAlign="Top" />
                                                <asp:TextBox ID="EITtxtMFDiscount" runat="server" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" CssClass="form-control" Text='<%# Eval("MFDiscount") %>' 
                                                Width="120px"></asp:TextBox>
                                            </EditItemTemplate>
                                            <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                            <FooterTemplate>
                                                <asp:TextBox ID="FTtxtMFDiscount" Text="0.00" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" CssClass="form-control" runat="server"></asp:TextBox>                                                
                                            </FooterTemplate>
                                        </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate><asp:Label runat="server" ID="id"></asp:Label></HeaderTemplate>
                                        <ItemTemplate >
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkEdit"  CssClass="btn btn-control btn-default btn-sm" data-icon="fa fa-edit"  runat="server" Text="Edit"  OnClientClick="return validateDtlAddUpdate('E',this.id,'Hotel')"  commandname="Edit">
                                            </asp:LinkButton>
                                         <%-- <div class="action-btn-wrap">
                                            <asp:ImageButton ID = "ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the Pax Details?');" ></asp:ImageButton>
                                              <a href class="btn btn-control btn-cancel btn-sm" data-icon="fa fa-trash">DELETE</a>
                                          </div>--%>
                                            </div>
                                        </ItemTemplate>  
                                        
                                        <EditItemTemplate>
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkUpdate"  runat="server" Text="Update" CssClass="btn btn-control btn-save btn-sm" data-icon="vms-icon-check"  commandname="Update" OnClientClick="return validateDtlAddUpdate('U',this.id,'Hotel')" ></asp:LinkButton>
                                            <asp:LinkButton ID="lnkCancel"  runat="server" Text="Cancel" CssClass="btn btn-control btn-clear btn-sm" data-icon="icon fa fa-times"  commandname="Cancel" ></asp:LinkButton></div>
                                            
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" />
                                        <FooterTemplate>
                                        
                                                <asp:LinkButton ID="lnkAdd" commandname="Add" runat="server" CssClass="btn btn-control btn-save btn-sm" data-icon="fa fa-plus" Text="Add" OnClientClick="return validateDtlAddUpdate('I',this.id,'Hotel')"></asp:LinkButton>
                                     
                                        </FooterTemplate>  
                                                                            
                                        
                                        </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gvHeader01" />
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />                                    
                                </asp:GridView>    
                       </div>
                        </div>                  
                        
                         <div role="tabpanel" class="tab-pane" id="package-details">
                            <asp:HiddenField id="hdfPackageMode" runat="server"></asp:HiddenField>
                                <div class="table table-responsive table-max-height"> 
                                    <asp:GridView ID="gvPackageMkp" runat="server" CssClass="table table-bordered b2b-corp-table"  AutoGenerateColumns="False" Width="100%"
                        AllowPaging="true" PageSize="20" DataKeyNames="MRDId" CellPadding="0" ShowFooter="true"
                                    CellSpacing="0"  GridLines="none" 
                        OnPageIndexChanging="gvPackageMkp_PageIndexChanging" 
                        OnRowCommand="gvPackageMkp_RowCommand" OnRowEditing="gvPackageMkp_RowEditing" 
                        OnRowCancelingEdit="gvPackageMkp_RowCancelingEdit" OnRowUpdating="gvPackageMkp_RowUpdating" OnRowDeleting="gvPackageMkp_RowDeleting">
                                   
                                        <PagerStyle CssClass="pagination-style" />  
                                    <Columns>                          
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Agent Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAgentMarkup" runat="server" Text='<%# Eval("AgentMarkup") %>' ToolTip='<%# Eval("AgentMarkup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtAgentMarkup" runat="server" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("AgentMarkup") %>' 
                                            Width="120px"></asp:TextBox>
                                             <asp:HiddenField Id="EITHdfMRId"  Value='<%# Eval("MRId") %>' runat="server" ></asp:HiddenField>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtAgentMarkup" Text="0.00" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>        
                                            <asp:HiddenField Id="FTHdfMRId"   Value="-1"  runat="server" ></asp:HiddenField>                                        
                                        </FooterTemplate>
                                    </asp:TemplateField>   
                                        
                                      <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Our Commission
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblOurCommission" runat="server" Text='<%# Eval("OurCommission") %>' ToolTip='<%# Eval("OurCommission") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtOurCommission" runat="server" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("OurCommission") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtOurCommission" Text="0.00" style="text-transform:capitalize;"  onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8"  runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>                                    
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Total Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblMarkup" runat="server" Text='<%# Eval("Markup") %>' ToolTip='<%# Eval("Markup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtMarkup" runat="server" style="text-transform:capitalize;" Enabled="false" Maxlength="50" Text='<%# Eval("Markup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtMarkup"  Text="0.00" style="text-transform:capitalize;" Enabled="false" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        Markup  Type
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblMarkupType" runat="server" Text='<%# Eval("MarkupTypeName") %>' ToolTip='<%# Eval("MarkupTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlMarkupType" Text='<%# Eval("MarkupType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlMarkupType" runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                    <%-- Added by Anji on 21-01-2020 For Discount & Discount Type--%>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Discount
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDiscount" runat="server" Text='<%# Eval("Discount") %>' ToolTip='<%# Eval("Discount") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtDiscount" runat="server" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("Discount") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtDiscount" Text="0.00" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        Discount Type
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblDiscountType" runat="server" Text='<%# Eval("DiscountTypeName") %>' ToolTip='<%# Eval("DiscountTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlDiscountType" Text='<%# Eval("DiscountType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlDiscountType" runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                       <%-- End by Anji--%>
                                    <asp:TemplateField>
                                        <HeaderTemplate><asp:Label runat="server" ID="id"></asp:Label></HeaderTemplate>
                                        <ItemTemplate >
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkEdit"  CssClass="btn btn-control btn-default btn-sm" data-icon="fa fa-edit"  runat="server" Text="Edit" OnClientClick="return validateDtlAddUpdate('E',this.id,'Package')"   commandname="Edit">
                                            </asp:LinkButton>
                                         <%-- <div class="action-btn-wrap">
                                            <asp:ImageButton ID = "ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the Pax Details?');" ></asp:ImageButton>
                                              <a href class="btn btn-control btn-cancel btn-sm" data-icon="fa fa-trash">DELETE</a>
                                          </div>--%>
                                            </div>
                                        </ItemTemplate>  
                                        
                                        <EditItemTemplate>
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkUpdate"  runat="server" Text="Update" CssClass="btn btn-control btn-save btn-sm" data-icon="vms-icon-check"  commandname="Update" OnClientClick="return validateDtlAddUpdate('U',this.id,'Package')" ></asp:LinkButton>
                                            <asp:LinkButton ID="lnkCancel"  runat="server" Text="Cancel" CssClass="btn btn-control btn-clear btn-sm" data-icon="icon fa fa-times"  commandname="Cancel" ></asp:LinkButton></div>
                                            
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" />
                                        <FooterTemplate>
                                        
                                                <asp:LinkButton ID="lnkAdd" commandname="Add" runat="server" CssClass="btn btn-control btn-save btn-sm" data-icon="fa fa-plus" Text="Add" OnClientClick="return validateDtlAddUpdate('I',this.id,'Package')"></asp:LinkButton>
                                     
                                        </FooterTemplate>  
                                                                            
                                        
                                        </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gvHeader01" />
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />                                    
                                </asp:GridView>    
                                </div>
                        </div> 

                        <div role="tabpanel" class="tab-pane" id="activity-details">
                             <asp:HiddenField id="hdfActivityMode" runat="server"></asp:HiddenField>
                                <div class="table table-responsive table-max-height"> 
                                    <asp:GridView ID="gvActivityMkp" runat="server" CssClass="table table-bordered b2b-corp-table"  AutoGenerateColumns="False" Width="100%"
                        AllowPaging="true" PageSize="20" DataKeyNames="MRDId" CellPadding="0" ShowFooter="true"
                                    CellSpacing="0"  GridLines="none" 
                        OnPageIndexChanging="gvActivityMkp_PageIndexChanging" 
                        OnRowCommand="gvActivityMkp_RowCommand" OnRowEditing="gvActivityMkp_RowEditing" 
                        OnRowCancelingEdit="gvActivityMkp_RowCancelingEdit" OnRowUpdating="gvActivityMkp_RowUpdating" OnRowDeleting="gvActivityMkp_RowDeleting">
                                   
                                        <PagerStyle CssClass="pagination-style" />  
                                    <Columns>                          
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Agent Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAgentMarkup" runat="server" Text='<%# Eval("AgentMarkup") %>' ToolTip='<%# Eval("AgentMarkup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtAgentMarkup" runat="server" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("AgentMarkup") %>' 
                                            Width="120px"></asp:TextBox>
                                            <asp:HiddenField Id="EITHdfMRId"  Value='<%# Eval("MRId") %>' runat="server" ></asp:HiddenField>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtAgentMarkup" Text="0.00" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                            <asp:HiddenField Id="FTHdfMRId"   Value="-1"  runat="server" ></asp:HiddenField>                                        
                                        </FooterTemplate>
                                    </asp:TemplateField>   
                                        
                                      <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Our Commission
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblOurCommission" runat="server" Text='<%# Eval("OurCommission") %>' ToolTip='<%# Eval("OurCommission") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtOurCommission" runat="server" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("OurCommission") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtOurCommission" Text="0.00" style="text-transform:capitalize;"  onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>                                    
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Total Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblMarkup" runat="server" Text='<%# Eval("Markup") %>' ToolTip='<%# Eval("Markup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtMarkup" runat="server" style="text-transform:capitalize;" Enabled="false" Maxlength="50" Text='<%# Eval("Markup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtMarkup" Text="0.00" style="text-transform:capitalize;"  Enabled="false" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        Markup  Type
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblMarkupType" runat="server" Text='<%# Eval("MarkupTypeName") %>' ToolTip='<%# Eval("MarkupTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlMarkupType" Text='<%# Eval("MarkupType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlMarkupType" runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                        <%-- Added by Anji on 21-01-2020 For Discount & Discount Type--%>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            <label>Discount</label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDiscount" runat="server" Text='<%# Eval("Discount") %>' ToolTip='<%# Eval("Discount") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtDiscount" runat="server" style="text-transform:capitalize;"  onkeypress="return restrictNumeric(this.id,'2');"  Maxlength="8" Text='<%# Eval("Discount") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtDiscount" Text="0.00" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');"  Maxlength="8"  runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField> 
                                <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        <label>Discount Type</label>
                                     
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblDiscountType" runat="server" Text='<%# Eval("DiscountTypeName") %>' ToolTip='<%# Eval("DiscountTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlDiscountType" Text='<%# Eval("DiscountType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlDiscountType" runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                    <%--End by Anji--%>
                                    <asp:TemplateField>
                                        <HeaderTemplate><asp:Label runat="server" ID="id"></asp:Label></HeaderTemplate>
                                        <ItemTemplate >
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkEdit"  CssClass="btn btn-control btn-default btn-sm" data-icon="fa fa-edit"  runat="server" Text="Edit"  OnClientClick="return validateDtlAddUpdate('E',this.id,'Activity')"  commandname="Edit">
                                            </asp:LinkButton>
                                          <%--<div class="action-btn-wrap">
                                            <asp:ImageButton ID = "ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the Pax Details?');" ></asp:ImageButton>
                                              <a href class="btn btn-control btn-cancel btn-sm" data-icon="fa fa-trash">DELETE</a>
                                          </div>--%>
                                            </div>
                                        </ItemTemplate>  
                                        
                                        <EditItemTemplate>
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkUpdate"  runat="server" Text="Update" CssClass="btn btn-control btn-save btn-sm" data-icon="vms-icon-check"  commandname="Update" OnClientClick="return validateDtlAddUpdate('U',this.id,'Activity')" ></asp:LinkButton>
                                            <asp:LinkButton ID="lnkCancel"  runat="server" Text="Cancel" CssClass="btn btn-control btn-clear btn-sm" data-icon="icon fa fa-times"  commandname="Cancel" ></asp:LinkButton></div>
                                            
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" />
                                        <FooterTemplate>
                                        
                                                <asp:LinkButton ID="lnkAdd" commandname="Add" runat="server" CssClass="btn btn-control btn-save btn-sm" data-icon="fa fa-plus" Text="Add" OnClientClick="return validateDtlAddUpdate('I',this.id,'Activity')"></asp:LinkButton>
                                     
                                        </FooterTemplate>  
                                                                            
                                        
                                        </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gvHeader01" />
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />                                    
                                </asp:GridView>    
                                </div>
                        </div> 

                         <div role="tabpanel" class="tab-pane" id="insurance-details">
                             <asp:HiddenField id="hdfInsurancemode" runat="server"></asp:HiddenField>
                                <div class="table table-responsive table-max-height"> 
                                    <asp:GridView ID="gvInsuranceMkp" runat="server" CssClass="table table-bordered b2b-corp-table"  AutoGenerateColumns="False" Width="100%"
                        AllowPaging="true" PageSize="20" DataKeyNames="MRDId" CellPadding="0" ShowFooter="true"
                                    CellSpacing="0"  GridLines="none" 
                        OnPageIndexChanging="gvInsuranceMkp_PageIndexChanging" 
                        OnRowCommand="gvInsuranceMkp_RowCommand" OnRowEditing="gvInsuranceMkp_RowEditing" 
                        OnRowCancelingEdit="gvInsuranceMkp_RowCancelingEdit" OnRowUpdating="gvInsuranceMkp_RowUpdating" OnRowDeleting="gvInsuranceMkp_RowDeleting">
                                   
                                        <PagerStyle CssClass="pagination-style" />  
                                    <Columns>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" CssClass="thead" />
                                        <HeaderTemplate>
                                        Source
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblSource" runat="server" Text='<%# Eval("SourceId") %>' ToolTip='<%# Eval("SourceId") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:DropDownList Id="EITddlSource" style="width:150px;" runat="server" ></asp:DropDownList>
                                            <asp:HiddenField Id="EITHdfMRId"  Value='<%# Eval("MRId") %>' runat="server" ></asp:HiddenField>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:DropDownList Id="FTddlSource" style="width:150px;" runat="server" ></asp:DropDownList >
                                             <asp:HiddenField Id="FTHdfMRId"   Value="-1"  runat="server" ></asp:HiddenField>                                        
                                            </FooterTemplate>
                                    </asp:TemplateField>
                                                                       
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Agent Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAgentMarkup" runat="server" Text='<%# Eval("AgentMarkup") %>' ToolTip='<%# Eval("AgentMarkup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtAgentMarkup" runat="server" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("AgentMarkup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtAgentMarkup" Text="0.00" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>   
                                        
                                      <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Our Commission
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblOurCommission" runat="server" Text='<%# Eval("OurCommission") %>' ToolTip='<%# Eval("OurCommission") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />  
                                            <asp:TextBox ID="EITtxtOurCommission" runat="server" style="text-transform:capitalize;"  onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("OurCommission") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtOurCommission" Text="0.00" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>                                    
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Total Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblMarkup" runat="server" Text='<%# Eval("Markup") %>' ToolTip='<%# Eval("Markup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtMarkup" runat="server" style="text-transform:capitalize;" Enabled="false" Maxlength="50" Text='<%# Eval("Markup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtMarkup" Text="0.00" style="text-transform:capitalize;" Enabled="false" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        Markup  Type
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblMarkupType" runat="server" Text='<%# Eval("MarkupTypeName") %>' ToolTip='<%# Eval("MarkupTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlMarkupType" Text='<%# Eval("MarkupType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlMarkupType" runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Discount
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDiscount" runat="server" Text='<%# Eval("Discount") %>' ToolTip='<%# Eval("Discount") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtDiscount" runat="server" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("Discount") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtDiscount" Text="0.00" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        Discount Type
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblDiscountType" runat="server" Text='<%# Eval("DiscountTypeName") %>' ToolTip='<%# Eval("DiscountTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlDiscountType" Text='<%# Eval("DiscountType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlDiscountType" runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                    
                                    <asp:TemplateField>
                                        <HeaderTemplate><asp:Label runat="server" ID="id"></asp:Label></HeaderTemplate>
                                        <ItemTemplate >
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkEdit"  CssClass="btn btn-control btn-default btn-sm" data-icon="fa fa-edit"  runat="server" Text="Edit" OnClientClick="return validateDtlAddUpdate('E',this.id,'Insurance')"   commandname="Edit">
                                            </asp:LinkButton>
                                         <%-- <div class="action-btn-wrap">
                                            <asp:ImageButton ID = "ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the Pax Details?');" ></asp:ImageButton>
                                              <a href class="btn btn-control btn-cancel btn-sm" data-icon="fa fa-trash">DELETE</a>
                                          </div>--%>
                                            </div>
                                        </ItemTemplate>  
                                        
                                        <EditItemTemplate>
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkUpdate"  runat="server" Text="Update" CssClass="btn btn-control btn-save btn-sm" data-icon="vms-icon-check"  commandname="Update" OnClientClick="return validateDtlAddUpdate('U',this.id,'Insurance')" ></asp:LinkButton>
                                            <asp:LinkButton ID="lnkCancel"  runat="server" Text="Cancel" CssClass="btn btn-control btn-clear btn-sm" data-icon="icon fa fa-times"  commandname="Cancel" ></asp:LinkButton></div>
                                            
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" />
                                        <FooterTemplate>
                                        
                                                <asp:LinkButton ID="lnkAdd" commandname="Add" runat="server" CssClass="btn btn-control btn-save btn-sm" data-icon="fa fa-plus" Text="Add" OnClientClick="return validateDtlAddUpdate('I',this.id,'Insurance')"></asp:LinkButton>
                                     
                                        </FooterTemplate>  
                                                                            
                                        
                                        </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gvHeader01" />
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />                                    
                                </asp:GridView>    
                                </div>
                        </div> 


                          <div role="tabpanel" class="tab-pane" id="car-details">
                             <asp:HiddenField id="hdfCarMode" runat="server"></asp:HiddenField>
                                <div class="table table-responsive table-max-height"> 
                                         <asp:GridView ID="gvCarMkp" runat="server" CssClass="table table-bordered b2b-corp-table"  AutoGenerateColumns="False" Width="100%"
                        AllowPaging="true" PageSize="20" DataKeyNames="MRDId" CellPadding="0" ShowFooter="true"
                                    CellSpacing="0"  GridLines="none" 
                        OnPageIndexChanging="gvCarMkp_PageIndexChanging" 
                        OnRowCommand="gvCarMkp_RowCommand" OnRowEditing="gvCarMkp_RowEditing" 
                        OnRowCancelingEdit="gvCarMkp_RowCancelingEdit" OnRowUpdating="gvCarMkp_RowUpdating" OnRowDeleting="gvCarMkp_RowDeleting">
                                   
                                        <PagerStyle CssClass="pagination-style" />  
                                    <Columns> 
                                        <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" CssClass="thead" />
                                        <HeaderTemplate>
                                        Source
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblSource" runat="server" Text='<%# Eval("SourceId") %>' ToolTip='<%# Eval("SourceId") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:DropDownList Id="EITddlSource" style="width:150px;" runat="server" ></asp:DropDownList>
                                            <asp:HiddenField Id="EITHdfMRId"  Value='<%# Eval("MRId") %>' runat="server" ></asp:HiddenField>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:DropDownList Id="FTddlSource" style="width:150px;" runat="server" ></asp:DropDownList >
                                             <asp:HiddenField Id="FTHdfMRId"   Value="-1"  runat="server" ></asp:HiddenField>
                                            </FooterTemplate>
                                    </asp:TemplateField>
                                                                 
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Agent Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAgentMarkup" runat="server" Text='<%# Eval("AgentMarkup") %>' ToolTip='<%# Eval("AgentMarkup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtAgentMarkup" runat="server" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("AgentMarkup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtAgentMarkup" Text="0.00" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>   
                                        
                                      <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Our Commission
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblOurCommission" runat="server" Text='<%# Eval("OurCommission") %>' ToolTip='<%# Eval("OurCommission") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtOurCommission" runat="server" style="text-transform:capitalize;"  onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("OurCommission") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtOurCommission" Text="0.00" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>                                    
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Total Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblMarkup" runat="server" Text='<%# Eval("Markup") %>' ToolTip='<%# Eval("Markup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtMarkup" runat="server" style="text-transform:capitalize;" Enabled="false" Maxlength="50" Text='<%# Eval("Markup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtMarkup" Text="0.00" style="text-transform:capitalize;" Enabled="false" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        Markup  Type
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblMarkupType" runat="server" Text='<%# Eval("MarkupTypeName") %>' ToolTip='<%# Eval("MarkupTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlMarkupType" Text='<%# Eval("MarkupType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlMarkupType" runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                        <%-- Added by Anji on 21-01-2020 For Discount & Discount Type--%>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            <label>Discount</label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDiscount" runat="server" Text='<%# Eval("Discount") %>' ToolTip='<%# Eval("Discount") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtDiscount" runat="server" style="text-transform:capitalize;"  onkeypress="return restrictNumeric(this.id,'2');"  Maxlength="8" Text='<%# Eval("Discount") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtDiscount" Text="0.00" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');"  Maxlength="8"   runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField> 
                                <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        <label>Discount Type</label>
                                     
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblDiscountType" runat="server" Text='<%# Eval("DiscountTypeName") %>' ToolTip='<%# Eval("DiscountTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlDiscountType" Text='<%# Eval("DiscountType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlDiscountType" runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                    <%--End by Anji--%>
                                    <asp:TemplateField>
                                        <HeaderTemplate><asp:Label runat="server" ID="id"></asp:Label></HeaderTemplate>
                                        <ItemTemplate >
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkEdit"  CssClass="btn btn-control btn-default btn-sm" data-icon="fa fa-edit"  runat="server" Text="Edit" OnClientClick="return validateDtlAddUpdate('E',this.id,'Car')"  commandname="Edit">
                                            </asp:LinkButton>
                                      <%--    <div class="action-btn-wrap">
                                            <asp:ImageButton ID = "ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the Pax Details?');" ></asp:ImageButton>
                                              <a href class="btn btn-control btn-cancel btn-sm" data-icon="fa fa-trash">DELETE</a>
                                          </div>--%>
                                            </div>
                                        </ItemTemplate>  
                                        
                                        <EditItemTemplate>
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkUpdate"  runat="server" Text="Update" CssClass="btn btn-control btn-save btn-sm" data-icon="vms-icon-check"  commandname="Update" OnClientClick="return validateDtlAddUpdate('U',this.id,'Car')" ></asp:LinkButton>
                                            <asp:LinkButton ID="lnkCancel"  runat="server" Text="Cancel" CssClass="btn btn-control btn-clear btn-sm" data-icon="icon fa fa-times"  commandname="Cancel" ></asp:LinkButton></div>
                                            
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" />
                                        <FooterTemplate>
                                        
                                                <asp:LinkButton ID="lnkAdd" commandname="Add" runat="server" CssClass="btn btn-control btn-save btn-sm" data-icon="fa fa-plus" Text="Add" OnClientClick="return validateDtlAddUpdate('I',this.id,'Car')"></asp:LinkButton>
                                     
                                        </FooterTemplate>  
                                                                            
                                        
                                        </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gvHeader01" />
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />                                    
                                </asp:GridView>    
                                </div>
                        </div> 

                        <div role="tabpanel" class="tab-pane" id="fixedDep-details">
                             <asp:HiddenField id="hdfFixedDepMode" runat="server"></asp:HiddenField>
                                 <div class="table table-responsive table-max-height"> 
                                        <asp:GridView ID="gvFixedDepMkp" runat="server" CssClass="table table-bordered b2b-corp-table"  AutoGenerateColumns="False" Width="100%"
                        AllowPaging="true" PageSize="20" DataKeyNames="MRDId" CellPadding="0" ShowFooter="true"
                                    CellSpacing="0"  GridLines="none" 
                        OnPageIndexChanging="gvFixedDepMkp_PageIndexChanging" 
                        OnRowCommand="gvFixedDepMkp_RowCommand" OnRowEditing="gvFixedDepMkp_RowEditing" 
                        OnRowCancelingEdit="gvFixedDepMkp_RowCancelingEdit" OnRowUpdating="gvFixedDepMkp_RowUpdating" OnRowDeleting="gvFixedDepMkp_RowDeleting">
                                   
                                        <PagerStyle CssClass="pagination-style" />  
                                    <Columns>    
                                        
                                         <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" CssClass="thead" />
                                        <HeaderTemplate>
                                        Source
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblSource" runat="server" Text='<%# Eval("SourceId") %>' ToolTip='<%# Eval("SourceId") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:DropDownList Id="EITddlSource" style="width:150px;" runat="server" ></asp:DropDownList>
                                            <asp:HiddenField Id="EITHdfMRId"  Value='<%# Eval("MRId") %>' runat="server" ></asp:HiddenField>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:DropDownList Id="FTddlSource" style="width:150px;" runat="server" ></asp:DropDownList >
                                             <asp:HiddenField Id="FTHdfMRId"   Value="-1"  runat="server" ></asp:HiddenField>
                                            </FooterTemplate>
                                    </asp:TemplateField>
                                                              
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Agent Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAgentMarkup" runat="server" Text='<%# Eval("AgentMarkup") %>' ToolTip='<%# Eval("AgentMarkup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtAgentMarkup" runat="server" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("AgentMarkup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtAgentMarkup" Text="0.00" style="text-transform:capitalize;"  onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>   
                                        
                                      <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Our Commission
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblOurCommission" runat="server" Text='<%# Eval("OurCommission") %>' ToolTip='<%# Eval("OurCommission") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtOurCommission" runat="server" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("OurCommission") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtOurCommission" Text="0.00" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8"  runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>                                    
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Total Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblMarkup" runat="server" Text='<%# Eval("Markup") %>' ToolTip='<%# Eval("Markup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtMarkup" runat="server" style="text-transform:capitalize;" Enabled="false" Maxlength="50" Text='<%# Eval("Markup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtMarkup" Text="0.00" style="text-transform:capitalize;" Enabled="false" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        Markup  Type
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblCarrierType" runat="server" Text='<%# Eval("MarkupTypeName") %>' ToolTip='<%# Eval("MarkupTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlCarrierType" Text='<%# Eval("MarkupType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlCarrierType" runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                        <%-- Added by Anji on 21-01-2020 For Discount & Discount Type--%>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            <label>Discount</label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDiscount" runat="server" Text='<%# Eval("Discount") %>' ToolTip='<%# Eval("Discount") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtDiscount" runat="server" style="text-transform:capitalize;"  onkeypress="return restrictNumeric(this.id,'2');"  Maxlength="8" Text='<%# Eval("Discount") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtDiscount" Text="0.00" style="text-transform:capitalize;"  onkeypress="return restrictNumeric(this.id,'2');"  Maxlength="8"  runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField> 
                                <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        <label>Discount Type</label>
                                     
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblDiscountType" runat="server" Text='<%# Eval("DiscountTypeName") %>' ToolTip='<%# Eval("DiscountTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlDiscountType" Text='<%# Eval("DiscountType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlDiscountType" runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                     <%--End by Anji--%>
                                    <asp:TemplateField>
                                        <HeaderTemplate><asp:Label runat="server" ID="id"></asp:Label></HeaderTemplate>
                                        <ItemTemplate >
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkEdit"  CssClass="btn btn-control btn-default btn-sm" data-icon="fa fa-edit"  runat="server" Text="Edit"  commandname="Edit">
                                            </asp:LinkButton>
                                         <%-- <div class="action-btn-wrap">
                                            <asp:ImageButton ID = "ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the Pax Details?');" ></asp:ImageButton>
                                              <a href class="btn btn-control btn-cancel btn-sm" data-icon="fa fa-trash">DELETE</a>
                                          </div>--%>
                                            </div>
                                        </ItemTemplate>  
                                        
                                        <EditItemTemplate>
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkUpdate"  runat="server" Text="Update" CssClass="btn btn-control btn-save btn-sm" data-icon="vms-icon-check"  commandname="Update" OnClientClick="return validateDtlAddUpdate('U',this.id,'FixedDeparture')" ></asp:LinkButton>
                                            <asp:LinkButton ID="lnkCancel"  runat="server" Text="Cancel" CssClass="btn btn-control btn-clear btn-sm" data-icon="icon fa fa-times"  commandname="Cancel" ></asp:LinkButton></div>
                                            
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" />
                                        <FooterTemplate>
                                        
                                                <asp:LinkButton ID="lnkAdd" commandname="Add" runat="server" CssClass="btn btn-control btn-save btn-sm" data-icon="fa fa-plus" Text="Add" OnClientClick="return validateDtlAddUpdate('I',this.id,'FixedDeparture')"></asp:LinkButton>
                                     
                                        </FooterTemplate>  
                                                                            
                                        
                                        </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gvHeader01" />
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />                                    
                                </asp:GridView>    
                                </div>
                        </div> 

                      
          <div role="tabpanel" class="tab-pane" id="sight-details">
                             <asp:HiddenField id="hdfSightMode" runat="server"></asp:HiddenField>
                         <div class="table table-responsive table-max-height"> 
                            <asp:GridView ID="gvSightMkp" runat="server" CssClass="table table-bordered b2b-corp-table"  AutoGenerateColumns="False" Width="100%"
                        AllowPaging="true" PageSize="20" DataKeyNames="MRDId" CellPadding="0" ShowFooter="true"
                                    CellSpacing="0"  GridLines="none" 
                        OnPageIndexChanging="gvSightMkp_PageIndexChanging" 
                        OnRowCommand="gvSightMkp_RowCommand" OnRowEditing="gvSightMkp_RowEditing" 
                        OnRowCancelingEdit="gvSightMkp_RowCancelingEdit" OnRowUpdating="gvSightMkp_RowUpdating" OnRowDeleting="gvSightMkp_RowDeleting">
                                   
                                        <PagerStyle CssClass="pagination-style" />  
                                    <Columns>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" CssClass="thead" />
                                        <HeaderTemplate>
                                        Source
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblSource" runat="server" Text='<%# Eval("SourceId") %>' ToolTip='<%# Eval("SourceId") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:DropDownList Id="EITddlSource" style="width:150px;" runat="server" ></asp:DropDownList>
                                            <asp:HiddenField Id="EITHdfMRId"  Value='<%# Eval("MRId") %>' runat="server" ></asp:HiddenField>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:DropDownList Id="FTddlSource" style="width:150px;" runat="server" ></asp:DropDownList >
                                             <asp:HiddenField Id="FTHdfMRId"   Value="-1"  runat="server" ></asp:HiddenField>
                                            </FooterTemplate>
                                    </asp:TemplateField>
                                                                       
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Agent Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAgentMarkup" runat="server" Text='<%# Eval("AgentMarkup") %>' ToolTip='<%# Eval("AgentMarkup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtAgentMarkup" runat="server" style="text-transform:capitalize;" Maxlength="8" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Text='<%# Eval("AgentMarkup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtAgentMarkup" Text="0.00" style="text-transform:capitalize;"  onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8"  runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>   
                                        
                                      <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Our Commission
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblOurCommission" runat="server" Text='<%# Eval("OurCommission") %>' ToolTip='<%# Eval("OurCommission") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtOurCommission" runat="server" style="text-transform:capitalize;"  onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("OurCommission") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtOurCommission" Text="0.00" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');"  MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>                                    
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Total Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblMarkup" runat="server" Text='<%# Eval("Markup") %>' ToolTip='<%# Eval("Markup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtMarkup" runat="server" style="text-transform:capitalize;"  Enabled="false" Maxlength="50" Text='<%# Eval("Markup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtMarkup"  Text="0.00" style="text-transform:capitalize;" Enabled="false" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        Markup  Type
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblMarkupType" runat="server" Text='<%# Eval("MarkupTypeName") %>' ToolTip='<%# Eval("MarkupTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlMarkupType" Text='<%# Eval("MarkupType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlMarkupType" runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                    
                                         <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Discount
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDiscount" runat="server" Text='<%# Eval("Discount") %>' ToolTip='<%# Eval("Discount") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtDiscount" runat="server" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("Discount") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtDiscount" Text="0.00" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        Discount Type
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblDiscountType" runat="server" Text='<%# Eval("DiscountTypeName") %>' ToolTip='<%# Eval("DiscountTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlDiscountType" Text='<%# Eval("DiscountType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlDiscountType" runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate><asp:Label runat="server" ID="id"></asp:Label></HeaderTemplate>
                                        <ItemTemplate >
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkEdit"  CssClass="btn btn-control btn-default btn-sm" data-icon="fa fa-edit"  runat="server" Text="Edit"  OnClientClick="return validateDtlAddUpdate('E',this.id,'Sight')"  commandname="Edit">
                                            </asp:LinkButton>
                                         <%-- <div class="action-btn-wrap">
                                            <asp:ImageButton ID = "ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the Pax Details?');" ></asp:ImageButton>
                                              <a href class="btn btn-control btn-cancel btn-sm" data-icon="fa fa-trash">DELETE</a>
                                          </div>--%>
                                            </div>
                                        </ItemTemplate>  
                                        
                                        <EditItemTemplate>
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkUpdate"  runat="server" Text="Update" CssClass="btn btn-control btn-save btn-sm" data-icon="vms-icon-check"  commandname="Update" OnClientClick="return validateDtlAddUpdate('U',this.id,'Sight')" ></asp:LinkButton>
                                            <asp:LinkButton ID="lnkCancel"  runat="server" Text="Cancel" CssClass="btn btn-control btn-clear btn-sm" data-icon="icon fa fa-times"  commandname="Cancel" ></asp:LinkButton></div>
                                            
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" />
                                        <FooterTemplate>
                                        
                                                <asp:LinkButton ID="lnkAdd" commandname="Add" runat="server" CssClass="btn btn-control btn-save btn-sm" data-icon="fa fa-plus" Text="Add" OnClientClick="return validateDtlAddUpdate('I',this.id,'Sight')"></asp:LinkButton>
                                     
                                        </FooterTemplate>  
                                                                            
                                        
                                        </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gvHeader01" />
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />                                    
                                </asp:GridView>    
                       </div>
                        </div>
        
           <div role="tabpanel" class="tab-pane" id="transfer-details">
                             <asp:HiddenField id="hdfTransferMode" runat="server"></asp:HiddenField>
                         <div class="table table-responsive table-max-height"> 
                            <asp:GridView ID="gvTransferMkp" runat="server" CssClass="table table-bordered b2b-corp-table"  AutoGenerateColumns="False" Width="100%"
                        AllowPaging="true" PageSize="20" DataKeyNames="MRDId" CellPadding="0" ShowFooter="true"
                                    CellSpacing="0"  GridLines="none" 
                        OnPageIndexChanging="gvTransferMkp_PageIndexChanging" 
                        OnRowCommand="gvTransferMkp_RowCommand" OnRowEditing="gvTransferMkp_RowEditing" 
                        OnRowCancelingEdit="gvTransferMkp_RowCancelingEdit" OnRowUpdating="gvTransferMkp_RowUpdating" OnRowDeleting="gvTransferMkp_RowDeleting">
                                   
                                        <PagerStyle CssClass="pagination-style" />  
                                    <Columns>
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" CssClass="thead" />
                                        <HeaderTemplate>
                                        Source
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblSource" runat="server" Text='<%# Eval("SourceId") %>' ToolTip='<%# Eval("SourceId") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:DropDownList Id="EITddlSource" style="width:150px;" runat="server" ></asp:DropDownList>
                                            <asp:HiddenField Id="EITHdfMRId"  Value='<%# Eval("MRId") %>' runat="server" ></asp:HiddenField>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:DropDownList Id="FTddlSource" style="width:150px;" runat="server" ></asp:DropDownList >
                                             <asp:HiddenField Id="FTHdfMRId"   Value="-1"  runat="server" ></asp:HiddenField>
                                            </FooterTemplate>
                                    </asp:TemplateField>
                                                                       
                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Agent Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAgentMarkup" runat="server" Text='<%# Eval("AgentMarkup") %>' ToolTip='<%# Eval("AgentMarkup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtAgentMarkup" runat="server" style="text-transform:capitalize;" Maxlength="8" onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Text='<%# Eval("AgentMarkup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtAgentMarkup"  Text="0.00" style="text-transform:capitalize;"  onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8"  runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>   
                                        
                                      <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Our Commission
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblOurCommission" runat="server" Text='<%# Eval("OurCommission") %>' ToolTip='<%# Eval("OurCommission") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtOurCommission" runat="server" style="text-transform:capitalize;"  onChange="CalculateTotalMarkup('U',this.id)" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("OurCommission") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtOurCommission"  Text="0.00" style="text-transform:capitalize;" onChange="CalculateTotalMarkup('I',this.id)" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>                                    
                                    
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Total Markup
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblMarkup" runat="server" Text='<%# Eval("Markup") %>' ToolTip='<%# Eval("Markup") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtMarkup" runat="server" style="text-transform:capitalize;"  Enabled="false" Maxlength="50" Text='<%# Eval("Markup") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtMarkup" Text="0.00" style="text-transform:capitalize;" Enabled="false" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        Markup  Type
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblMarkupType" runat="server" Text='<%# Eval("MarkupTypeName") %>' ToolTip='<%# Eval("MarkupTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlMarkupType" Text='<%# Eval("MarkupType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlMarkupType" runat="server" >
                                                    <asp:ListItem Text="--Select Markup Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                    
                                         <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                            Discount
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDiscount" runat="server" Text='<%# Eval("Discount") %>' ToolTip='<%# Eval("Discount") %>'   Width="110px"></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>                                            
                                            <EditRowStyle VerticalAlign="Top" />
                                            <asp:TextBox ID="EITtxtDiscount" runat="server" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');" Maxlength="8" Text='<%# Eval("Discount") %>' 
                                            Width="120px"></asp:TextBox>
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top"/>
                                        <FooterTemplate>
                                            <asp:TextBox ID="FTtxtDiscount" Text="0.00" style="text-transform:capitalize;" onkeypress="return restrictNumeric(this.id,'2');" MaxLength="8" runat="server"></asp:TextBox>                                                
                                        </FooterTemplate>
                                    </asp:TemplateField>  

                                     <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                         <HeaderTemplate>
                                        Discount Type
                                     <%-- <cc1:Filter ID="HTtxtSettleMode" runat="server" OnClick="FilterSearch_Click" HeaderText="Nationality"
                                        Width="150px" />--%>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                                <asp:Label ID="ITlblDiscountType" runat="server" Text='<%# Eval("DiscountTypeName") %>' ToolTip='<%# Eval("DiscountTypeName") %>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>
                                                <asp:DropDownList Id="EITddlDiscountType" Text='<%# Eval("DiscountType") %>' runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value=""></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>                                                    
                                                </asp:DropDownList>                              
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" VerticalAlign="top" />
                                        <FooterTemplate>
                                                <asp:DropDownList Id="FTddlDiscountType" runat="server" >
                                                    <asp:ListItem Text="--Select Discount Type--" Value="-1"></asp:ListItem>
                                                    <asp:ListItem Text="Fixed" Value="F"></asp:ListItem>
                                                    <asp:ListItem Text="Percentage" Value="P"></asp:ListItem>  
                                                </asp:DropDownList >
                                        </FooterTemplate>
                                        </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderTemplate><asp:Label runat="server" ID="id"></asp:Label></HeaderTemplate>
                                        <ItemTemplate >
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkEdit"  CssClass="btn btn-control btn-default btn-sm" data-icon="fa fa-edit"  runat="server" Text="Edit"  OnClientClick="return validateDtlAddUpdate('E',this.id,'Transfer')"  commandname="Edit">
                                            </asp:LinkButton>
                                         <%-- <div class="action-btn-wrap">
                                            <asp:ImageButton ID = "ibtnDelete" runat="server" ImageUrl="~/Images/grid/wg_Delete.gif" CausesValidation ="False" CommandName="Delete" Text="Delete" OnClientClick="return confirm('Do you want to delete the Pax Details?');" ></asp:ImageButton>
                                              <a href class="btn btn-control btn-cancel btn-sm" data-icon="fa fa-trash">DELETE</a>
                                          </div>--%>
                                            </div>
                                        </ItemTemplate>  
                                        
                                        <EditItemTemplate>
                                            <div style="width: 160px;">
                                            <asp:LinkButton ID="lnkUpdate"  runat="server" Text="Update" CssClass="btn btn-control btn-save btn-sm" data-icon="vms-icon-check"  commandname="Update" OnClientClick="return validateDtlAddUpdate('U',this.id,'Transfer')" ></asp:LinkButton>
                                            <asp:LinkButton ID="lnkCancel"  runat="server" Text="Cancel" CssClass="btn btn-control btn-clear btn-sm" data-icon="icon fa fa-times"  commandname="Cancel" ></asp:LinkButton></div>
                                            
                                        </EditItemTemplate>
                                        <FooterStyle HorizontalAlign="left" />
                                        <FooterTemplate>
                                        
                                                <asp:LinkButton ID="lnkAdd" commandname="Add" runat="server" CssClass="btn btn-control btn-save btn-sm" data-icon="fa fa-plus" Text="Add" OnClientClick="return validateDtlAddUpdate('I',this.id,'Transfer')"></asp:LinkButton>
                                     
                                        </FooterTemplate>  
                                                                            
                                        
                                        </asp:TemplateField>
                                </Columns>
                                <HeaderStyle CssClass="gvHeader01" />
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />                                    
                                </asp:GridView>    
                       </div>
                        </div>      
                    </div>
                            
</div>                        
                    
            <div class="tab-content pb-0">
                    <%--<div role="tabpanel" class="tab-pane active" id="flight-details" title="flightDetails">
                        
                    </div>--%>



              



              



            
                
                
                                                    
                                             

         



               
<div> 



            </div>
          
            
            
            
                       

       




</div>

 
 
 <div> 
 
<%-- <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click">First</asp:LinkButton>
                            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" />
                            <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
                            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" />
                            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click">Last</asp:LinkButton>--%>
 
 </div>       
        
        
    </div>
    
    
    
    
    <div> 
    
    
    
    
    </div>
    
    
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

