﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Corporate;
using System.Collections.Generic;
using System.Web.Services;
public partial class CorporateCreateExpenseReportUI : CT.Core.ParentPage
{

    protected long corpProfileUserId;
    protected List<CorporateProfileExpenseDetails> _profileExpenseDetailsList = new List<CorporateProfileExpenseDetails>();
    protected string approverNames = string.Empty;
    protected int approverId = 0;
    

    [WebMethod]  //If the user removes the uploaded file then remove it from session too.
    public static void RemoveSession(string response)
    {
        List<HttpPostedFile> files = new List<HttpPostedFile>();
        if (HttpContext.Current.Session["Files"] != null)
        {
            files = HttpContext.Current.Session["Files"] as List<HttpPostedFile>;
        }
        foreach (HttpPostedFile file in files)
        {
            if (file.FileName == response)
            {
                files.Remove(file);
                break;
            }
        }
        HttpContext.Current.Session["Files"] = files;
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo!= null) //Authorisation Check -- if success
            {
                this.corpProfileUserId = Settings.LoginInfo.UserID;

                if (!IsPostBack)
                {
                    if (Request.QueryString["travelMode"] != null || Request.QueryString["type"] != null)
                    {
                        IntialiseControls();

                    }
                    else
                    {
                        Response.Redirect("AbandonSession.aspx");
                    }
                }
            }
            else
            {
                //Authorisation Check -- if failed
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateCreateExpenseReport) page Load Error: " + ex.Message, "0");
        }

    }

    private void IntialiseControls()
    {
        try
        {
            BindCountryList();
            BindExpenseTypes();
            BindCostCenters();

            ListItem item = new ListItem();
            item.Text = Settings.LoginInfo.Currency;
            item.Value = Settings.LoginInfo.Currency;

            ddlCurrency.Items.Add(item);
            txtEmployeeName.Text = Convert.ToString(Request["en"]);//employee name

            ddlCostCenter.SelectedValue = Convert.ToString(Request["cc"]); //cost center


        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    private void BindExpenseTypes()
    {
        try
        {


            ddlExpenseType.DataSource = PolicyExpenseDetails.GetAllExpenseTypes('A', "ET");
            ddlExpenseType.DataValueField = "SetupId";
            ddlExpenseType.DataTextField = "Name";
            ddlExpenseType.AppendDataBoundItems = true;
            ddlExpenseType.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindCostCenters()
    {
        try
        {


            ddlCostCenter.DataSource = PolicyExpenseDetails.GetAllExpenseTypes('A', "CS");
            ddlCostCenter.DataValueField = "SetupId";
            ddlCostCenter.DataTextField = "Name";
            ddlCostCenter.AppendDataBoundItems = true;
            ddlCostCenter.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }



    private void BindCountryList()
    {
        try
        {
       

              DataTable dtCountries =CorporateProfileExpenseDetails.GetPolicyExpenseCountryList();
              ddlCountry.DataSource = dtCountries;
               ddlCountry.DataValueField = "COUNTRYCODE";
               ddlCountry.DataTextField = "COUNTRYNAME";
               ddlCountry.AppendDataBoundItems = true;
               ddlCountry.DataBind();




        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    protected void btnCreateExpense_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(hdnExpDetails.Value))
            {
                string[] expDetails = hdnExpDetails.Value.Split('|');
                foreach (string detail in expDetails)
                {
                    // -1^E@08-06-2017@AF@CCN@6@1@REf@Desc@AED@120@Comment

                    CorporateProfileExpenseDetails ped = new CorporateProfileExpenseDetails();
                    ped.ExpDetailId = Convert.ToInt32(detail.Split('^')[0]);

                    //Expense Type
                    ped.Type = detail.Split('^')[1].Split('@')[0];
                    //Date
                    IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                    ped.Date = Convert.ToDateTime(detail.Split('^')[1].Split('@')[1].Replace('-', '/'), dateFormat);

                    //Agent Id
                    ped.AgentId = Settings.LoginInfo.AgentId;
                    //Profile id
                    ped.ProfileId = (int)Settings.LoginInfo.UserID;

                    //Country Code
                    ped.CountryCode = detail.Split('^')[1].Split('@')[2];

                    //City Code
                    ped.CityCode = detail.Split('^')[1].Split('@')[3];

                    //Cost center Id
                    ped.CostCenterId = Convert.ToInt32(detail.Split('^')[1].Split('@')[4]);
                    //Exp Type Id

                    ped.Exptypeid = Convert.ToInt32(detail.Split('^')[1].Split('@')[5]);

                    //Reference code

                    ped.ReferenceCode = detail.Split('^')[1].Split('@')[6];

                    //Description
                    ped.Description = detail.Split('^')[1].Split('@')[7];

                    //Currency
                    ped.Currency = detail.Split('^')[1].Split('@')[8];

                    //Amount

                    ped.Amount = Convert.ToDecimal(detail.Split('^')[1].Split('@')[9]);

                    //Comment
                    ped.Comment = detail.Split('^')[1].Split('@')[10];

                    //docno
                    ped.DocNo = "";
                    //type
                    if (Request.QueryString["type"] != null)
                    {
                        ped.Expmode = Convert.ToString(Request.QueryString["type"]); //Travel mode(T)
                    }
                    else
                    {
                        ped.Expmode = Convert.ToString(Request.QueryString["travelMode"]);//Non travel mode(N)
                    }
                    //reference id
                    if (Request.QueryString["type"] == "T" && Request.QueryString["refid"] != null)
                    {
                        ped.RefId = Convert.ToString(Request.QueryString["refid"]);
                    }
                    ped.ApprovalHierarachy = "";
                    ped.ApprovalStatus = "";
                    ped.Status = "A";
                    ped.ApprovalStatus = "P"; //Initially Pending state.
                    ped.CreatedBy = (int)Settings.LoginInfo.UserID;

                    //Save the uploaded files into the folder.
                    if (Session["files"] != null)
                    {

                        List<HttpPostedFile> filesList = Session["files"] as List<HttpPostedFile>;
                        ped.ProfileExpenseUploadedFilesList = filesList;

                    }

                    _profileExpenseDetailsList.Add(ped);


                }

                if (_profileExpenseDetailsList != null && _profileExpenseDetailsList.Count > 0)
                {
                    CorporateProfileExpenseDetails ped = new CorporateProfileExpenseDetails();
                    string refNum = ped.Save(_profileExpenseDetailsList);
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ClearAll();", "SCRIPT");
                    Label lblMasterError = (Label)this.Master.FindControl("lblError");
                    lblMasterError.Visible = true;
                    lblMasterError.Text = "Details saved !Ref.No:" + refNum;

                    //clearing existing docs
                    List<HttpPostedFile> files = new List<HttpPostedFile>();
                    //if (HttpContext.Current.Session["Files"] != null)
                    //{
                    //    files = HttpContext.Current.Session["Files"] as List<HttpPostedFile>;
                    //}
                    //foreach (HttpPostedFile file in files)
                    //{
                    //        files.Remove(file);
                           
                    //}
                    HttpContext.Current.Session["Files"] = files;



                }

                //Once the expense details are created we need to trigger an email to the employee as well as to the all the approvers with hierarchy level 1
                //As the trans type is always expense so always the email will be send to the expense approvers.

                if (_profileExpenseDetailsList != null && _profileExpenseDetailsList.Count > 0)
                {
                    try
                    {
                        SendEmail(_profileExpenseDetailsList, "E");//To the employee.
                        SendEmail(_profileExpenseDetailsList, "A");//To the list of approvers.
                    }
                    catch { }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateCreateExpenseReport) btnCreateExpense_Click Error: " + ex.ToString(), "0");
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "ClearAll();", "SCRIPT");
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = "Error occured while saving:" + ex.ToString();
        }
    }


    protected string SendEmail(List<CorporateProfileExpenseDetails> expRequests, string toEmpOrApp)
    {
        string myPageHTML = string.Empty;
        System.IO.StringWriter sw = new System.IO.StringWriter();
        HtmlTextWriter htw = new HtmlTextWriter(sw);
        try
        {           
            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            if (toEmpOrApp =="A")//To the approver
            {               
                //From the submitted expense details we will get all the approvers list with hierarchy level 1
                //Then send an email.
                List<CorpProfileApproval> approversList = new List<CorpProfileApproval>();
                List<int> approverIds = new List<int>();               
                foreach (CorporateProfileExpenseDetails ped in expRequests)
                {
                    foreach (CorpProfileApproval approver in ped.ProfileApproversList)
                    {
                        if (approver.Hierarchy == 1 && !approverIds.Contains(approver.ApproverId))
                        {
                            approversList.Add(approver);
                            approverIds.Add(approver.ApproverId);
                        }
                    }
                }
                foreach (CorpProfileApproval approver in approversList)
                {
                    approverNames = approver.ApproverName;
                    approverId = approver.ApproverId;
                    EmailDivApprover.RenderControl(htw);
                    myPageHTML = sw.ToString();
                    myPageHTML = myPageHTML.Replace("none", "block");
                    toArray.Add(approver.ApproverEmail);
                    string subject = "Expense Approval Request";
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                    toArray.Clear();
                }                 
            }
            else //To the employee
            {
                
                if (!string.IsNullOrEmpty(expRequests[0].EmpEmail))
                {
                    toArray.Add(expRequests[0].EmpEmail);
                }
                EmailDivEmployee.RenderControl(htw);
                myPageHTML = sw.ToString();
                myPageHTML = myPageHTML.Replace("none", "block");
                if (toArray != null && toArray.Count > 0)
                {
                    string subject = "Expense Approval Request";
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                }
            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateCreateExpenseReport.aspx)Failed to Send Email For Employee and Approvers: Reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        return myPageHTML;
    }


}
