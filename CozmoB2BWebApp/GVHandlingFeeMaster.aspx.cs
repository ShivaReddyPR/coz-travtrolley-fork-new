﻿
using CT.TicketReceipt.Common;
using CT.Core;
using CT.GlobalVisa;
using CT.GlobasVisa;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Web.UI.Controls;

public partial class GVHandlingFeeMasterUI :CT.Core.ParentPage
{
    private string GVHANDLINGFEE_SESSION = "_GVvisaHandlingFeeSession";
    private string GV_HANDLING_FEE_SEARCH_SESSION = "_GVvisaHandlingFeeSearchList";
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            this.Master.PageRole = true;
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
            }
            BindVisaType();
            lblErrorMsg.Text = string.Empty;
            lblSuccessMsg.Text = string.Empty;
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
        private void InitializePageControls()
    {
        try
        {
           // BindAgent();
            BindCountryList();
            BindNationalityList();
            BindVisaCategory();
            BindResidence();
        }
        catch
        {
            throw;
        }
    }
    //private void BindAgent()
    //{
    //    try
    //    {
    //        int agentId = 0;
    //        if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
    //        ddlAgent.DataSource = AgentMaster.GetList(1, "ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
    //        ddlAgent.DataValueField = "agent_Id";
    //        ddlAgent.DataTextField = "agent_Name";
    //        ddlAgent.DataBind();
    //        ddlAgent.Items.Insert(0, new ListItem("Select Agent", "-1"));
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}
    private void BindCountryList()
    {
        try
        {
            ddlCountry.DataSource = Country.GetCountryList();
            ddlCountry.DataValueField = "Value";
            ddlCountry.DataTextField = "Key";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "-1"));
        }
        catch (Exception ex) { throw ex; }
    }
    private void BindResidence()//Field Master
    {
        try
        {
            ddlResidence.DataSource = Country.GetCountryList();
            ddlResidence.DataTextField = "Key";
            ddlResidence.DataValueField = "Value";
            ddlResidence.DataBind();
            ddlResidence.Items.Insert(0, new ListItem("--Select Residence--", "-1"));
        }
        catch (Exception ex) { throw ex; }
    }
    private void BindNationalityList()
    {
        try
        {
            ddlNationality.DataSource = Country.GetNationalityList();
            ddlNationality.DataTextField = "Key";
            ddlNationality.DataValueField = "Value";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("--Select Nationality--", "-1"));
            ddlNationality.Items.Insert(1, new ListItem("All", "0"));
        }
        catch (Exception ex) { throw ex; }
    }
    private void BindVisaType()
    {
        try
        {
            DataTable dtVisaTypes = VisaTypeMaster.GetListBYCountryCode(ListStatus.Short, RecordStatus.Activated, ddlCountry.SelectedValue);
            if (dtVisaTypes != null && dtVisaTypes.Rows.Count > 0)
            {
                foreach (DataRow dr in dtVisaTypes.Rows)
                {
                    HtmlTableRow tr = new HtmlTableRow();
                    HtmlTableCell tc = new HtmlTableCell();
                    Label label = new Label();
                    label.Text = Convert.ToString(dr["VISA_TYPE_NAME"]);
                    label.ID = "lbl_" + Convert.ToString(dr["VISA_TYPE_ID"]);
                    label.Width = new Unit(120, UnitType.Pixel);

                    tc.Controls.Add(label);
                    TextBox txt = new TextBox();
                    txt.ID = "txt_" + Convert.ToString(dr["VISA_TYPE_ID"]);
                    txt.Text = "0.00";
                    txt.Attributes["onkeypress"]= "return restrictNumeric(this.id,'1');";
                    txt.Attributes["onfocus"]= "Check(this.id);";
                    txt.Attributes["onBlur"]= "setToFixed(this.id);";
                    tc.Controls.Add(txt);

                    tr.Cells.Add(tc);
                    tblVisaTypes.Controls.Add(tr);
                }
            }
        }catch(Exception ex)
        {
            throw ex;
        }
    }
    private void BindVisaCategory()//Field Master
    {
        try
        {
            ddlVisaCategory.DataSource = CachedMaster.GetCommonList("ob_visa_type");
            ddlVisaCategory.DataTextField = "field_text";
            ddlVisaCategory.DataValueField = "field_value";
            ddlVisaCategory.DataBind();
            ddlVisaCategory.Items.Insert(0, new ListItem("--Select Visa Category--", "-1"));
        }
        catch (Exception ex) { throw ex; }
    }
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
    //    if(ddlAgent.SelectedIndex==0)
    //    {
    //        Utility.StartupScript(this.Page, "alert('please select Agent');", "myalert");
    //        ddlCountry.SelectedIndex = 0;
    //    }
    //    //BindVisaType();//ddlCountry.SelectedValue,Convert.ToInt32( ddlAgent.SelectedValue)
    }
    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[GV_HANDLING_FEE_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["HAND_ID"] };
            Session[GV_HANDLING_FEE_SEARCH_SESSION] = value;
        }
    }

    private GVHandlingFeeMaster CurrentObject
    {
        get
        {
            return (GVHandlingFeeMaster)Session[GVHANDLINGFEE_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(GVHANDLINGFEE_SESSION);
            }
            else
            {
                Session[GVHANDLINGFEE_SESSION] = value;
            }
        }
    }
    private void Save()
    {
        try
        {
            GVHandlingFeeMaster gvVisaHandlingFee;
            DataTable dtvisatypes = VisaTypeMaster.GetListBYCountryCode(ListStatus.Short, RecordStatus.Activated, ddlCountry.SelectedValue);
            DataTable dtvisa = (DataTable)ViewState["dttypes"];
            if (dtvisatypes.Rows.Count > 0 && dtvisatypes.Rows != null)
            {
                if (CurrentObject == null)
                {
                    gvVisaHandlingFee = new GVHandlingFeeMaster();
                }
                else
                {
                    gvVisaHandlingFee = CurrentObject;
                }
                
                    for (int i = 0; i < dtvisatypes.Rows.Count; i++)
                    {

                        Label lablid = (Label)tblVisaTypes.FindControl("lbl_" + Utility.ToString(dtvisatypes.Rows[i]["VISA_TYPE_ID"]));
                        TextBox txtId = (TextBox)tblVisaTypes.FindControl("txt_" + Utility.ToString(dtvisatypes.Rows[i]["VISA_TYPE_ID"]));
                        if ((lablid != null) && (txtId != null))
                        {
                            gvVisaHandlingFee.CountryID = Utility.ToString(ddlCountry.SelectedItem.Value);
                            gvVisaHandlingFee.NationalityId = Utility.ToString(ddlNationality.SelectedItem.Value);
                            gvVisaHandlingFee.ResidenceId = Utility.ToString(ddlResidence.SelectedItem.Value);
                            gvVisaHandlingFee.VisaCategory = Utility.ToString(ddlVisaCategory.SelectedItem.Value);
                            gvVisaHandlingFee.VisaTypeId = Utility.ToLong(dtvisatypes.Rows[i]["VISA_TYPE_ID"]);
                            gvVisaHandlingFee.HandlingFee = Utility.ToDecimal(txtId.Text);
                        gvVisaHandlingFee.Status = Settings.ACTIVE;
                        gvVisaHandlingFee.CreatedBy = Settings.LoginInfo.UserID;
                        if (dtvisa!=null)
                        {
                            if (i+1 > dtvisa.Rows.Count)
                            {
                                CurrentObject = null;
                                gvVisaHandlingFee.TransactionId = -1;
                            }
                                
                        }
                        if (CurrentObject != null)
                            {

                                for (int j = i; j < dtvisa.Rows.Count; j++)
                                {
                                    gvVisaHandlingFee.TransactionId = Utility.ToLong(dtvisa.Rows[j]["hand_id"]);
                                    break;
                                }
                        }
                            gvVisaHandlingFee.Save();
                        }
                    }
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage(" Visa Handling Fee Details are", "", (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
                Clear();
            }
            else
            {
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = "No Visa Types For selected Country!!!!";
            }
        }
        catch (Exception ex) { throw ex; }
    }
    private void Clear()
    {
        try
        {
            ddlCountry.SelectedIndex = 0;
            ddlCountry.Enabled = true;
            ddlNationality.SelectedIndex = 0;
            ddlResidence.SelectedIndex = 0;
            ddlVisaCategory.SelectedIndex = 0;
            ViewState["dttypes"] = null;
            CurrentObject = null;
            tblVisaTypes.Controls.Clear();
            btnSave.Text = "Submit";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void Edit(long id)
    {
        try
        {
            GVHandlingFeeMaster gvVisaHandlingFee = new GVHandlingFeeMaster(id);
            CurrentObject = gvVisaHandlingFee;
            string Country = Utility.ToString(gvVisaHandlingFee.CountryID);
            ddlCountry.SelectedValue = Country;
            ddlCountry.Enabled = false;
            string Nationality = Utility.ToString(gvVisaHandlingFee.NationalityId);
            ddlNationality.SelectedValue = Nationality;
            string Residence = Utility.ToString(gvVisaHandlingFee.ResidenceId);
            ddlResidence.SelectedValue = Residence;
            string visaCategory = Utility.ToString(gvVisaHandlingFee.VisaCategory);
            ddlVisaCategory.SelectedValue = visaCategory;
            DataTable dttypes = GVHandlingFeeMaster.GetVisaTypesByCombination(Country, Nationality, Residence, visaCategory);
            ViewState["dttypes"] = dttypes;
            DataTable dtvisatypes = VisaTypeMaster.GetListBYCountryCode(ListStatus.Short, RecordStatus.Activated,Country);
            if (dtvisatypes != null && dtvisatypes.Rows.Count > 0)
            {
                    for (int j = 0; j < dtvisatypes.Rows.Count; j++)
                    {
                        HtmlTableRow tr = new HtmlTableRow();
                        HtmlTableCell tc = new HtmlTableCell();
                        Label label = new Label();
                        label.Text = Convert.ToString(dtvisatypes.Rows[j]["VISA_TYPE_NAME"]);
                        label.ID = "lbl_" + Convert.ToString(dtvisatypes.Rows[j]["VISA_TYPE_ID"]);
                        label.Width = new Unit(120, UnitType.Pixel);

                        tc.Controls.Add(label);
                        TextBox txt = new TextBox();
                        txt.ID = "txt_" + Convert.ToString(dtvisatypes.Rows[j]["VISA_TYPE_ID"]);
                    txt.Text = "0.00";
                    for (int i =j; i < dttypes.Rows.Count; i++)
                    {
                        txt.Text = Formatter.ToCurrency(dttypes.Rows[i]["hand_res_fee"]);
                        break;
                    }
                        txt.Attributes["onkeypress"] = "return restrictNumeric(this.id,'1');";
                        txt.Attributes["onfocus"] = "Check(this.id);";
                        txt.Attributes["onBlur"] = "setToFixed(this.id);";
                        tc.Controls.Add(txt);
                        tc.Controls.Add(txt);
                        tr.Cells.Add(tc);
                        tblVisaTypes.Controls.Add(tr);
                    }
                }
            btnSave.Text = "Update";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void bindSearch()
    {
        try
        {
            LoginInfo loginfo = Settings.LoginInfo;
            DataTable dt = GVHandlingFeeMaster.GetList(ListStatus.Long, RecordStatus.Activated);
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch (Exception ex) { throw ex; }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            Clear();
            bindSearch();
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            long gvVisaFeeId = Utility.ToLong(gvSearch.SelectedValue);
           Edit(gvVisaFeeId);
            this.Master.HideSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{ "HTtxtVisaCategory", "charge_visa_category_Name" },
            { "HTtxtCountry", "hand_country_Name" },{ "HTtxtNationality", "hand_nationality_Name" },{"HTxtVisaType","hand_visa_type_Name"},
            {"HTtxtResidence","hand_residence_Name"}};
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
}
