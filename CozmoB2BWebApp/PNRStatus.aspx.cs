﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
public partial class PNRStatusGUI : CT.Core.ParentPage
{ 
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null)//Authorisation Check
            {               
                lblStatus.Text = String.Empty;
                if (!IsPostBack)
                {
                    dvResult.Visible = false;
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Audit.Add(EventType.Exception, Severity.High, 1, "PNRStatusGUI Page_Load Event error.Reason: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dtresult = FlightItinerary.GetInProgressPNR(txtPNR.Text);            
            if (dtresult.Rows.Count > 0)
            {
              foreach(DataRow dr in dtresult.Rows)
                {
                    ddlStatus.Items.Clear();
                    dvResult.Visible = true;
                    ListItem liststatus = new ListItem("Release", dr["bookingId"].ToString());
                    ddlStatus.Items.Add(liststatus);
                    liststatus=new ListItem("Void", dr["bookingId"].ToString());
                    ddlStatus.Items.Add(liststatus);
                    ddlStatus.Items.Insert(0, new ListItem("--Select--", "0"));
                  
                }
                //ddlStatus.Items.Clear();
                //dvResult.Visible = true;
                //ddlStatus.DataSource = dtresult;
                //ddlStatus.DataTextField = "Status";
                //ddlStatus.DataValueField = "bookingId";
                //ddlStatus.Items.Insert(0, new ListItem("--Select--", "0"));
                //ddlStatus.DataBind();
            }
            else
            {
                lblStatus.Text = "No Data Found";
            }

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Audit.Add(EventType.Exception, Severity.High, 1, "PNRStatusGUI btnSearch_Click Event error.Reason: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
    protected void btnChangeStatus_Click(object sender, EventArgs e)
    {
        try
        {
            int count = 0;
             BookingDetail bookingdetails = new BookingDetail(Convert.ToInt32(ddlStatus.SelectedValue));
            
            if (ddlStatus.Items[Convert.ToInt32(hdfStatus.Value)].Text == "Release")
            {
                count = bookingdetails.Unlock(BookingStatus.Released, (int)Settings.LoginInfo.UserID);
            }
            else
            {
                count = bookingdetails.Unlock(BookingStatus.Void, (int)Settings.LoginInfo.UserID);
            }
             
            if (count == 1)
            {
                dvResult.Visible = false;
                lblStatus.Text = "Successfully " + txtPNR.Text + " Updated";
                txtPNR.Text = string.Empty;
            }
            else
            {
                lblStatus.Text = "PNR is not Updated ";
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Audit.Add(EventType.Exception, Severity.High, 1, "PNRStatusGUI btnChangeStatus_Click Event error. Reason " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
}
