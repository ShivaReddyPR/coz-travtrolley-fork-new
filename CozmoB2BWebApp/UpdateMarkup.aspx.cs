﻿using System;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;

public partial class UpdateMarkupGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    DataTable dtProducts;
    DataTable dtMarkupList;
    PagedDataSource pagedData = new PagedDataSource();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            AuthorizationCheck();
            if (!IsPostBack)
            {
                InitializePageControls();
            }
            else
            {
                BindControls();
            }
            lblSuccessMsg.Text = string.Empty;
            errMess.Style.Add("display", "none");
            errorMessage.InnerHtml = string.Empty;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #region Private methods

    private void InitializePageControls()
    {
        try
        {
            BindAgent();
            BindProcduct();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindAgent()
    {
        try
        {
            int agentId = Convert.ToInt32(Settings.LoginInfo.AgentId);
            //AgentMaster1 agent = new AgentMaster1(agentId);
            DataTable dtAgents = null;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                dtAgents = AgentMaster.GetList(1, "ALL", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                dtAgents = AgentMaster.GetList(1, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agency--", "0"));
            ddlAgent.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindProcduct()
    {
        try
        {
            Session["dtProducts"] = null;
            
            dtProducts = UpdateMarkup.ProductGetList(ListStatus.Short, RecordStatus.Activated,Settings.LoginInfo.AgentId);
            chkProduct.DataSource = dtProducts;
            chkProduct.DataTextField = "productType";
            chkProduct.DataValueField = "productTypeId";
            chkProduct.DataBind();
            BindControls();
            DisableDataList();
            //BindMarkupList();
            Session["dtProducts"] = dtProducts;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindControls()
    {
        if (Session["dtProducts"] != null)
        {
            dtProducts = (DataTable)Session["dtProducts"];
        }
        //List<string> HotelSource = new List<string>();
        //List<string> FlightSource = new List<string>();
        //System.Collections.Generic.Dictionary<string, string> sourcesConfig = CT.Configuration.ConfigurationSystem.ActiveSources;
        //foreach (System.Collections.Generic.KeyValuePair<string, string> pair in sourcesConfig)
        //{
        //    if (pair.Value.Split(',')[0].ToLower() == "hotel")
        //    {
        //        HotelSource.Add(pair.Key);
        //    }
        //    else if (pair.Value.Split(',')[0].ToLower() == "flight")
        //    {
        //        FlightSource.Add(pair.Key);
        //    }
        //}
        int agentId = Convert.ToInt32(ddlAgent.SelectedValue);
        HtmlTableRow hr = new HtmlTableRow();
        for (int i = 0; i < dtProducts.Rows.Count; i++) //Binding all Controls
        {
            //13.10.2014 Added by brahmam
            Label lblAgentMarkup = new Label();
            lblAgentMarkup.ID = "lblAgentMarkup_" + i.ToString();
            lblAgentMarkup.Text = "Agent Markup";
            lblAgentMarkup.Width = new Unit(90, UnitType.Pixel);
            lblAgentMarkup.Style.Add("display", "none");
            TextBox txtAgentMarkup = new TextBox();
            txtAgentMarkup.ID = "txtAgentMarkup_" + i.ToString();
            txtAgentMarkup.Text = "0.0000";
            txtAgentMarkup.Width = new Unit(80, UnitType.Pixel);
            txtAgentMarkup.Style.Add("display", "none");
            txtAgentMarkup.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'0');");
            txtAgentMarkup.Attributes.Add("onchange", "SetValue();");
            txtAgentMarkup.Attributes.Add("onfocus", "Check(this.id);");
            txtAgentMarkup.Attributes.Add("onBlur", "Set(this.id);");
            Label lblOurComm = new Label();
            lblOurComm.ID = "lblOurComm_" + i.ToString();
            lblOurComm.Text = "Our Commission";
            lblOurComm.Width = new Unit(80, UnitType.Pixel);
            lblOurComm.Style.Add("display", "none");
            TextBox txtOurComm = new TextBox();
            txtOurComm.ID = "txtOurComm_" + i.ToString();
            txtOurComm.Text = "0.0000";
            txtOurComm.Width = new Unit(80, UnitType.Pixel);
            txtOurComm.Style.Add("display", "none");
            txtOurComm.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'4');");
            txtOurComm.Attributes.Add("onchange", "SetValue();");
            txtOurComm.Attributes.Add("onfocus", "Check(this.id);");
            txtOurComm.Attributes.Add("onBlur", "Set(this.id);");
            
            Label lblMarkup = new Label();
            lblMarkup.ID = "lblMarkUp_" + i.ToString();
            lblMarkup.Text = "Markup";
            lblMarkup.Width = new Unit(80, UnitType.Pixel);
            lblMarkup.Style.Add("display", "none");
            TextBox txtMark = new TextBox();
            txtMark.ID = "txtMarkUp_" + i.ToString();
            txtMark.Text = "0.0000";
            txtMark.Width = new Unit(80, UnitType.Pixel);
            txtMark.Style.Add("display", "none");
            txtMark.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'0');");
            txtMark.Attributes.Add("onchange", "setToFixedThis(this.id);");
            txtMark.Attributes.Add("onfocus", "Check(this.id);");
            txtMark.Attributes.Add("onBlur", "Set(this.id);");
            txtMark.Enabled = false;
            Label lblmarkUpType = new Label();
            lblmarkUpType.ID = "lblmarkUpType_" + i.ToString();
            lblmarkUpType.Text = "MarkupType";
            lblmarkUpType.Width = new Unit(80, UnitType.Pixel);
            lblmarkUpType.Style.Add("display", "none");
            DropDownList ddlMarkupType = new DropDownList();
            ddlMarkupType.ID = "ddlMarkupType_" + i.ToString();
            ddlMarkupType.Items.Insert(0, new ListItem("Fixed", "F"));
            ddlMarkupType.Items.Insert(0, new ListItem("Percentage", "P"));
            ddlMarkupType.Width = new Unit(80, UnitType.Pixel);
            ddlMarkupType.Style.Add("display", "none");
            HtmlTableCell tc = new HtmlTableCell();
            TextBox txtDiscount = new TextBox();
            txtDiscount.Width = new Unit(80, UnitType.Pixel);
            txtDiscount.ID = "txtDiscount_" + i.ToString();
            txtDiscount.Text = "0.0000";
            txtDiscount.Style.Add("display", "none");
            txtDiscount.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'0');");
            txtDiscount.Attributes.Add("onchange", "setToFixedThis(this.id);");
            txtDiscount.Attributes.Add("onfocus", "Check(this.id);");
            txtDiscount.Attributes.Add("onBlur", "Set(this.id);");
            txtDiscount.Visible = true;
            Label lblDiscountType = new Label();
            lblDiscountType.ID = "lblDiscountType_" + i.ToString();
            lblDiscountType.Text = "Disc Type";
            lblDiscountType.Style.Add("display", "none");
            lblDiscountType.Width = new Unit(80, UnitType.Pixel);
            lblDiscountType.Visible = true;
            DropDownList ddlDiscountType = new DropDownList();
            ddlDiscountType.ID = "ddlDiscountType_" + i.ToString();
            ddlDiscountType.Style.Add("display", "none");
            ddlDiscountType.Items.Insert(0, new ListItem("Fixed", "F"));
            ddlDiscountType.Items.Insert(0, new ListItem("Percentage", "P"));
            ddlDiscountType.Width = new Unit(80, UnitType.Pixel);
            ddlDiscountType.Visible = true;
            Label lblDiscount = new Label();
            lblDiscount.ID = "lblDiscount_" + i.ToString();
            lblDiscount.Text = "Discount";
            lblDiscount.Style.Add("display", "none");
            lblDiscount.Width = new Unit(80, UnitType.Pixel);
            lblDiscount.Visible = true;



            // Modified by  brahmam 05 sep 2014
            if (i == 4 || i==9)  //Insurance and Car only showing Discount controls
            {
                txtDiscount.Visible = true;
                lblDiscountType.Visible = true;
                ddlDiscountType.Visible = true;
                lblDiscount.Visible = true;
            }
                      

            if (i == 0)
            {
                Label lblSource = new Label();
                lblSource.ID = "lblSource_" + i.ToString();
                lblSource.Text = "Sources";
                lblSource.Style.Add("display", "none");
                lblSource.Width = new Unit(100, UnitType.Pixel);
                tc.Controls.Add(lblSource);
                DropDownList ddlSource = new DropDownList();
                ddlSource.ID = "ddlSource_" + i.ToString();
                if (agentId > 0)
                {
                    //ddlSource.DataSource = AgentMaster.GetAgentSources(agentId, (int)ProductType.Flight);
                    ddlSource.DataSource = AgentMaster.GetAgentSourcesAll(agentId, (int)ProductType.Flight,2);
                    
                    ddlSource.DataValueField = "Id";
                    ddlSource.DataTextField = "Name";
                    ddlSource.DataBind();
                }
                ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
                ddlSource.Width = new Unit(100, UnitType.Pixel);
                ddlSource.Style.Add("display", "none");
                ddlSource.AutoPostBack = true;
                ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);

                Label lblFlightType = new Label();
                lblFlightType.ID = "lblFlightType_" + i.ToString();
                lblFlightType.Text = "Flight Type";
                lblFlightType.Style.Add("display", "none");
                lblFlightType.Width = new Unit(80, UnitType.Pixel);
                
                DropDownList ddlFlightType = new DropDownList();
                ddlFlightType.ID = "ddlFlightType_" + i;
                ddlFlightType.Style.Add("display", "none");
                ddlFlightType.Items.Add(new ListItem("All", "All"));
                ddlFlightType.Items.Add(new ListItem("International", "INT"));
                ddlFlightType.Items.Add(new ListItem("Domestic", "DOM"));
                ddlFlightType.Width = new Unit(120, UnitType.Pixel);
                ddlFlightType.SelectedIndexChanged += DdlFlightType_SelectedIndexChanged;
                ddlFlightType.AutoPostBack = true;                 // added by arun, 05/07/2018

                Label lblJourneyType = new Label();
                lblJourneyType.ID = "lblJourneyType_" + i.ToString();
                lblJourneyType.Text = "Journey Type";
                lblJourneyType.Style.Add("display", "none");
                lblJourneyType.Width = new Unit(100, UnitType.Pixel);
                
                DropDownList ddlJourneyType = new DropDownList();
                ddlJourneyType.ID = "ddlJourneyType_" + i;
                ddlJourneyType.Style.Add("display", "none");
                ddlJourneyType.Items.Add(new ListItem("All", "All"));
                ddlJourneyType.Items.Add(new ListItem("Onward", "ONW"));
                ddlJourneyType.Items.Add(new ListItem("Return", "RET"));
                ddlJourneyType.Width = new Unit(100, UnitType.Pixel);
                ddlJourneyType.SelectedIndexChanged += DdlJourneyType_SelectedIndexChanged;
                ddlJourneyType.AutoPostBack = true;

                Label lblCarrierType = new Label();
                lblCarrierType.ID = "lblCarrierType_" + i.ToString();
                lblCarrierType.Text = "Carrier Type";
                lblCarrierType.Style.Add("display", "none");
                lblCarrierType.Width = new Unit(80, UnitType.Pixel);
                
                DropDownList ddlCarrierType = new DropDownList();
                ddlCarrierType.ID = "ddlCarrierType_" + i;
                ddlCarrierType.Style.Add("display", "none");
                ddlCarrierType.Items.Add(new ListItem("All", "All"));
                ddlCarrierType.Items.Add(new ListItem("GDS", "GDS"));
                ddlCarrierType.Items.Add(new ListItem("LCC", "LCC"));
                ddlCarrierType.Width = new Unit(80, UnitType.Pixel);
                ddlCarrierType.SelectedIndexChanged += DdlCarrierType_SelectedIndexChanged;
                ddlCarrierType.AutoPostBack = true;

                tc.Controls.Add(ddlSource);
                tc.Controls.Add(lblFlightType);
                tc.Controls.Add(ddlFlightType);
                tc.Controls.Add(lblJourneyType);
                tc.Controls.Add(ddlJourneyType);
                tc.Controls.Add(lblCarrierType);
                tc.Controls.Add(ddlCarrierType);

            }
            else if (i == 1)
            {
                Label lblSource = new Label();
                lblSource.ID = "lblSource_" + i.ToString();
                lblSource.Text = "Sources";
                lblSource.Style.Add("display", "none");
                lblSource.Width = new Unit(80, UnitType.Pixel);
                tc.Controls.Add(lblSource);
                DropDownList ddlSource = new DropDownList();
                ddlSource.ID = "ddlSource_" + i.ToString();
                if (agentId > 0)
                {
                    ddlSource.DataSource = AgentMaster.GetAgentSources(agentId, (int)ProductType.Hotel);
                    ddlSource.DataValueField = "Id";
                    ddlSource.DataTextField = "Name";
                    ddlSource.DataBind();
                }
                ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
                ddlSource.Width = new Unit(100, UnitType.Pixel);
                ddlSource.Style.Add("display", "none");
                ddlSource.AutoPostBack = true;
                ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
                tc.Controls.Add(ddlSource);
            }
            else if (i == 4)
            {
                 Label lblSource = new Label();
                lblSource.ID = "lblSource_" + i.ToString();
                lblSource.Text = "Sources";
                lblSource.Style.Add("display", "none");
                lblSource.Width = new Unit(80, UnitType.Pixel);
                tc.Controls.Add(lblSource);
                DropDownList ddlSource = new DropDownList();
                ddlSource.ID = "ddlSource_" + i.ToString();
                if (agentId > 0)
                {
                    ddlSource.DataSource = AgentMaster.GetAgentSources(agentId, (int)ProductType.Insurance);
                    ddlSource.DataValueField = "Id";
                    ddlSource.DataTextField = "Name";
                    ddlSource.DataBind();
                }
                ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
                ddlSource.Width = new Unit(80, UnitType.Pixel);
                ddlSource.Style.Add("display", "none");
                ddlSource.AutoPostBack = true;
                ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
                tc.Controls.Add(ddlSource);
            }
            else if (i == 5)
            {
                Label lblSource = new Label();
                lblSource.ID = "lblSource_" + i.ToString();
                lblSource.Text = "Sources";
                lblSource.Style.Add("display", "none");
                lblSource.Width = new Unit(80, UnitType.Pixel);
                tc.Controls.Add(lblSource);
                DropDownList ddlSource = new DropDownList();
                ddlSource.ID = "ddlSource_" + i.ToString();
                if (agentId > 0)
                {
                    ddlSource.DataSource = AgentMaster.GetAgentSources(agentId, (int)ProductType.SightSeeing);
                    ddlSource.DataValueField = "Id";
                    ddlSource.DataTextField = "Name";
                    ddlSource.DataBind();
                }
                ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
                ddlSource.Width = new Unit(80, UnitType.Pixel);
                ddlSource.Style.Add("display", "none");
                ddlSource.AutoPostBack = true;
               ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
                tc.Controls.Add(ddlSource);
            }
                //@@@@ Added by chandan on 18022016
            else if (i == 6)
            {
                Label lblSource = new Label();
                lblSource.ID = "lblSource_" + i.ToString();
                lblSource.Text = "Sources";
                lblSource.Style.Add("display", "none");
                lblSource.Width = new Unit(80, UnitType.Pixel);
                tc.Controls.Add(lblSource);
                DropDownList ddlSource = new DropDownList();
                ddlSource.ID = "ddlSource_" + i.ToString();
                if (agentId > 0)
                {
                    ddlSource.DataSource = AgentMaster.GetAgentSources(Convert.ToInt32(ddlAgent.SelectedValue), (int)ProductType.Transfers);// '9' is for Transfer
                    ddlSource.DataValueField = "Id";
                    ddlSource.DataTextField = "Name";
                    ddlSource.DataBind();
                }
                ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
                ddlSource.Width = new Unit(80, UnitType.Pixel);
                ddlSource.Style.Add("display", "none");
                ddlSource.AutoPostBack = true;
               ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
                tc.Controls.Add(ddlSource);
            }
            else if (i == 9)
            {
                Label lblSource = new Label();
                lblSource.ID = "lblSource_" + i.ToString();
                lblSource.Text = "Sources";
                lblSource.Style.Add("display", "none");
                lblSource.Width = new Unit(80, UnitType.Pixel);
                tc.Controls.Add(lblSource);
                DropDownList ddlSource = new DropDownList();
                ddlSource.ID = "ddlSource_" + i.ToString();
                if (agentId > 0)
                {
                    ddlSource.DataSource = AgentMaster.GetAgentSources(Convert.ToInt32(ddlAgent.SelectedValue), (int)ProductType.Car);// '9' is for Car
                    ddlSource.DataValueField = "Id";
                    ddlSource.DataTextField = "Name";
                    ddlSource.DataBind();
                }
                ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
                ddlSource.Width = new Unit(80, UnitType.Pixel);
                ddlSource.Style.Add("display", "none");
                ddlSource.AutoPostBack = true;
          //      ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
                tc.Controls.Add(ddlSource);
            }
            else if (i == 10)
            {
                Label lblSource = new Label();
                lblSource.ID = "lblSource_" + i.ToString();
                lblSource.Text = "Sources";
                lblSource.Style.Add("display", "none");
                lblSource.Width = new Unit(80, UnitType.Pixel);
                tc.Controls.Add(lblSource);
                DropDownList ddlSource = new DropDownList();
                ddlSource.ID = "ddlSource_" + i.ToString();
                if (agentId > 0)
                {
                    ddlSource.DataSource = AgentMaster.GetAgentSources(Convert.ToInt32(ddlAgent.SelectedValue), (int)ProductType.BaggageInsurance);// '9' is for Car
                    ddlSource.DataValueField = "Id";
                    ddlSource.DataTextField = "Name";
                    ddlSource.DataBind();
                }
                ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
                ddlSource.Width = new Unit(80, UnitType.Pixel);
                ddlSource.Style.Add("display", "none");
                ddlSource.AutoPostBack = true;
                //      ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
                tc.Controls.Add(ddlSource);
            }
            else
            {
            }
            tc.Controls.Add(lblAgentMarkup);
            tc.Controls.Add(txtAgentMarkup);
            tc.Controls.Add(lblOurComm);
            tc.Controls.Add(txtOurComm);
            tc.Controls.Add(lblMarkup);
            tc.Controls.Add(txtMark);
            tc.Controls.Add(lblmarkUpType);
            tc.Controls.Add(ddlMarkupType);
            tc.Controls.Add(lblDiscount);
            tc.Controls.Add(txtDiscount);
            tc.Controls.Add(lblDiscountType);
            tc.Controls.Add(ddlDiscountType);

            tc.Width = "100px";
            tc.VAlign = "top";
            hr.Cells.Add(tc);
        }
        tblMarkup.Rows.Add(hr);
    }

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx", true);
        }
    }

    private void LoadControls()
    {
        Label lblFlightType = (Label)tblMarkup.FindControl("lblFlightType_0");
        DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_0");
        Label lblJourneyType = (Label)tblMarkup.FindControl("lblJourneyType_0");
        DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_0");
        Label lblCarrierType = (Label)tblMarkup.FindControl("lblCarrierType_0");
        DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_0");
        DropDownList ddlsource = (DropDownList)tblMarkup.FindControl("ddlSource_0");
         
        //ddlsource.ClearSelection();       // To clear Selectin on Unchecking Checkbox
        //ddlFlightType.ClearSelection();
        //ddlJourneyType.ClearSelection();
        //ddlCarrierType.ClearSelection();


        dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
        for (int i = 0; i < chkProduct.Items.Count; i++)
        {
            Label lblAgentMarkup = (Label)tblMarkup.FindControl("lblAgentMarkup_" + i.ToString());
            TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + i.ToString());
            Label lblOurComm = (Label)tblMarkup.FindControl("lblOurComm_" + i.ToString());
            TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + i.ToString());
            Label lblMarkup = (Label)tblMarkup.FindControl("lblMarkUp_" + i.ToString());
            TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + i.ToString());
            Label lblMarkupType = (Label)tblMarkup.FindControl("lblmarkUpType_" + i.ToString());
            DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + i.ToString());
            Label lblDiscount = (Label)tblMarkup.FindControl("lblDiscount_" + i.ToString());
            TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + i.ToString());
            Label lblDiscountType = (Label)tblMarkup.FindControl("lblDiscountType_" + i.ToString());
            DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + i.ToString());
            Label lblSource = (Label)tblMarkup.FindControl("lblSource_" + i.ToString());
            DropDownList ddlSource = (DropDownList)tblMarkup.FindControl("ddlSource_" + i.ToString());

            txtAgentMarkup.Text = "";
            txtMarkup.Text = "";             // to clear when checkbox unchecked
            txtOurComm.Text = "";


            if (chkProduct.Items[i].Selected)
            {
                lblAgentMarkup.Style.Add("display", "block");
                txtAgentMarkup.Style.Add("display", "block");
                lblOurComm.Style.Add("display", "block");
                txtOurComm.Style.Add("display", "block");
                lblMarkup.Style.Add("display", "block");
                txtMarkup.Style.Add("display", "block");
                lblMarkupType.Style.Add("display", "block");
                ddlMarkupType.Style.Add("display", "block");
                lblDiscount.Style.Add("display", "block");
                txtDiscount.Style.Add("display", "block");
                lblDiscountType.Style.Add("display", "block");
                ddlDiscountType.Style.Add("display", "block");
                if (i == 0)
                {
                   


                    lblFlightType.Style.Add("display", "block");
                    ddlFlightType.Style.Add("display", "block");
                    lblJourneyType.Style.Add("display", "block");
                    ddlJourneyType.Style.Add("display", "block");
                    lblCarrierType.Style.Add("display", "block");
                    ddlCarrierType.Style.Add("display", "block");
                }
                if (i < 2 || i == 4 || i == 5 || i == 6 || i == 9 || i==10) // Sources is there Just displaying
                {
                    //ddlSource.DataSource = AgentMaster.GetAgentSources(Convert.ToInt32(ddlAgent.SelectedValue), Convert.ToInt32(i));
                    //ddlSource.DataValueField = "Id";
                    //ddlSource.DataTextField = "Name";
                    //ddlSource.DataBind();
                    //ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
                    //ddlSource.Width = new Unit(100, UnitType.Pixel);
                    //ddlSource.Style.Add("display", "none");
                    //ddlSource.AutoPostBack = true;
                    //ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);

                    lblSource.Style.Add("display", "block");
                    ddlSource.Style.Add("display", "block");
                }
                if (i == 2 || i == 3 || i == 7 || i == 8) // Here source is not there Loading markup directly
                {
                    

                    int agent = 0;
                    string find = string.Empty;
                    int productId = Convert.ToInt32(chkProduct.Items[i].Value);
                    if (ddlAgent.SelectedItem.Value != "0")
                    {
                        agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                    if (agent == 0)
                    {
                        find = "ProductId='" + productId + "' AND AgentId IS NULL";
                    }
                    else
                    {
                        find = "ProductId='" + productId + "' AND AgentId = '" + agent + "'  AND transType='B2B'";
                    }
                    DataRow[] foundRows = dtMarkupList.Select(find);
                    //DataTable dtMarkupDetails = UpdateMarkup.GetMarkupDetails(productId, agent);
                    if (foundRows.Length > 0)
                    {
                        if (foundRows[0]["AgentMarkup"] != DBNull.Value)
                        {
                            txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
                        }
                        else
                        {
                            txtAgentMarkup.Text = "0.00";
                        }
                        if (foundRows[0]["OurCommission"] != DBNull.Value)
                        {
                            txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
                        }
                        else
                        {
                            txtOurComm.Text = "0.00";
                        }
                        txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
                        string markuptype = Utility.ToString(foundRows[0]["MarkupType"]);

                        if (!string.IsNullOrEmpty(markuptype))
                            ddlMarkupType.SelectedValue = markuptype;
                        //  ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
                        txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
                        ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
                    }
                    else
                    {
                        txtAgentMarkup.Text = "0.00";
                        txtOurComm.Text = "0.00";
                        txtMarkup.Text = "0.00";
                        txtDiscount.Text = "0.00";
                    }
                }
            }
            else
            {
                lblAgentMarkup.Style.Add("display", "none");
                txtAgentMarkup.Style.Add("display", "none");
                lblOurComm.Style.Add("display", "none");
                txtOurComm.Style.Add("display", "none");
                lblMarkup.Style.Add("display", "none");
                txtMarkup.Style.Add("display", "none");
                lblMarkupType.Style.Add("display", "none");
                ddlMarkupType.Style.Add("display", "none");
                lblDiscount.Style.Add("display", "none");
                txtDiscount.Style.Add("display", "none");
                lblDiscountType.Style.Add("display", "none");
                ddlDiscountType.Style.Add("display", "none");
                if (i == 0)
                {
                    lblFlightType.Style.Add("display", "none");
                    ddlFlightType.Style.Add("display", "none");
                    lblJourneyType.Style.Add("display", "none");
                    ddlJourneyType.Style.Add("display", "none");
                    lblCarrierType.Style.Add("display", "none");
                    ddlCarrierType.Style.Add("display", "none");
                }
                if (i < 2 || i == 4 || i == 5 || i == 6 || i == 9) //Checking only sources products only
                {
                    lblSource.Style.Add("display", "none");
                    ddlSource.Style.Add("display", "none");
                }
            }
        }
    }

    private void BindMarkupList()
    {
        try
        {
            string agentType = string.Empty;

            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                if (agentId <= 0)
                {
                    agentType = "B2B";
                    agentId = Settings.LoginInfo.AgentId;
                }
            }
            if (agentId > 0)
            {
                dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, agentId, agentType, string.Empty, string.Empty, string.Empty);
                if (dtMarkupList != null && dtMarkupList.Rows.Count > 0)
                {
                    dlMarkup.Visible = true;
                    lblMessage.Visible = false;
                    dlMarkup.DataSource = dtMarkupList;
                    ViewState["Markup"] = dtMarkupList;
                    dlMarkup.DataBind();
                    doPaging();
                }
                else
                {
                    DisableDataList();
                }
            }
            else
            {
                DisableDataList();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.Low, 0, ex.Message, "0");
        }
    }
    private void DisableDataList()
    {
        dlMarkup.Visible = false;
        btnPrev.Visible = false;
        btnFirst.Visible = false;
        btnLast.Visible = false;
        btnNext.Visible = false;
        lblMessage.Visible = true;
        lblCurrentPage.Text = string.Empty;
        lblMessage.Text = "No Results Found.";
    }

    #endregion private

    #region dropdown events

    private void DdlCarrierType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddlSource = sender as DropDownList;
            int agent = 0;
            int rowIndex = 0;
            string find = string.Empty;
            rowIndex = Convert.ToInt32(ddlSource.ID.Split('_')[1]);
            if (ddlAgent.SelectedItem.Value != "0")
            {
                agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            }

            string source = ddlSource.SelectedItem.Text;
            TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + rowIndex.ToString());
            TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + rowIndex.ToString());
            TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + rowIndex.ToString());
            DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + rowIndex.ToString());
            TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + rowIndex.ToString());
            DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + rowIndex.ToString());
            DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_" + rowIndex.ToString());
            DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_" + rowIndex.ToString());
            DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_" + rowIndex.ToString());
            DropDownList ddlSourcelist = (DropDownList)tblMarkup.FindControl("ddlSource_" + rowIndex.ToString());

            int productId = Convert.ToInt32(chkProduct.Items[rowIndex].Value);

            //DataTable dtMarkUp = UpdateMarkup.GetMarkupRulesBySouceId(source, agent);
            dtMarkupList = UpdateMarkup.GetMarkupList(-1, ddlSourcelist.SelectedItem.Text, -1, string.Empty,ddlFlightType.SelectedValue,ddlJourneyType.SelectedValue,ddlCarrierType.SelectedValue);
            if (agent == 0)
            {
                find = "SourceId='" + source + "' AND AgentId IS NULL";
            }
            else
            {
                find = "ProductId='" + productId + "'AND CarrierType='" + source + "' AND AgentId = '" + agent + "' AND transType='B2B'";
                DataTable dtAgentSources = AgentMaster.GetAgentSources(agent, productId);
                //DataRow[] sources = dtAgentSources.Select("Name='" + source + "'");

                //if (sources != null && productId == 1 && ddlCarrierType != null)
                //{
                //    ddlCarrierType.SelectedValue = sources[0]["SourceType"].ToString();
                //}
            }
            DataRow[] foundRows = dtMarkupList.Select(find);
            if (foundRows.Length > 0)
            {
                if (foundRows[0]["AgentMarkup"] != DBNull.Value)
                {
                    txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
                }
                else
                {
                    txtAgentMarkup.Text = "0.00";
                }
                if (foundRows[0]["OurCommission"] != DBNull.Value)
                {
                    txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
                }
                else
                {
                    txtOurComm.Text = "0.00";
                }
                txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
                string markuptype = Utility.ToString(foundRows[0]["MarkupType"]);

                if (!string.IsNullOrEmpty(markuptype))
                    ddlMarkupType.SelectedValue = markuptype;
                // ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
                txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
                ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
                if (productId == 1)
                {
                    if (foundRows[0]["CarrierType"] != DBNull.Value)
                    {
                        ddlCarrierType.SelectedValue = Convert.ToString(foundRows[0]["CarrierType"]);
                    }
                    if (foundRows[0]["FlightType"] == DBNull.Value)
                    {
                        ddlFlightType.SelectedValue = "All";
                    }
                    else
                    {
                        switch (Convert.ToString(foundRows[0]["FlightType"]))
                        {
                            case "All":
                                ddlFlightType.SelectedValue = Convert.ToString(foundRows[0]["FlightType"]);
                                break;
                            case "Domestic":
                                ddlFlightType.SelectedValue = "DOM";
                                break;
                            case "International":
                                ddlFlightType.SelectedValue = "INT";
                                break;
                        }
                    }
                    if (foundRows[0]["JourneyType"] == DBNull.Value)
                    {
                        ddlJourneyType.SelectedValue = "All";
                    }
                    else
                    {
                        switch (Convert.ToString(foundRows[0]["JourneyType"]))
                        {
                            case "All":
                                ddlJourneyType.SelectedValue = Convert.ToString(foundRows[0]["JourneyType"]);
                                break;
                            case "Onward":
                                ddlJourneyType.SelectedValue = "ONW";
                                break;
                            case "Return":
                                ddlJourneyType.SelectedValue = "RET";
                                break;
                        }
                    }
                }
            }
            else
            {
                txtAgentMarkup.Text = "0.00";
                txtOurComm.Text = "0.00";
                txtMarkup.Text = "0.00";
                txtDiscount.Text = "0.00";
            }

        }
        catch 
        {

        }
    }

    private void DdlJourneyType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddlSource = sender as DropDownList;
            int agent = 0;
            int rowIndex = 0;
            string find = string.Empty;
            rowIndex = Convert.ToInt32(ddlSource.ID.Split('_')[1]);
            if (ddlAgent.SelectedItem.Value != "0")
            {
                agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            }

            string source = ddlSource.SelectedItem.Text;
            TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + rowIndex.ToString());
            TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + rowIndex.ToString());
            TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + rowIndex.ToString());
            DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + rowIndex.ToString());
            TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + rowIndex.ToString());
            DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + rowIndex.ToString());
            DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_" + rowIndex.ToString());
            DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_" + rowIndex.ToString());
            DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_" + rowIndex.ToString());
            DropDownList ddlSourcelist = (DropDownList)tblMarkup.FindControl("ddlSource_" + rowIndex.ToString());

            int productId = Convert.ToInt32(chkProduct.Items[rowIndex].Value);

            //DataTable dtMarkUp = UpdateMarkup.GetMarkupRulesBySouceId(source, agent);
            dtMarkupList = UpdateMarkup.GetMarkupList(-1, ddlSourcelist.SelectedItem.Text, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
            if (agent == 0)
            {
                find = "SourceId='" + source + "' AND AgentId IS NULL";
            }
            else
            {
                find = "ProductId='" + productId + "'AND JourneyType='" + source + "' AND AgentId = '" + agent + "' AND transType='B2B'";
               // DataTable dtAgentSources = AgentMaster.GetAgentSources(agent, productId);
               // DataRow[] sources = dtAgentSources.Select("Name='" + source + "'");

               // if (sources != null && productId == 1 && ddlCarrierType != null)
               // {
                //    ddlCarrierType.SelectedValue = sources[0]["SourceType"].ToString();
               // }
            }
            DataRow[] foundRows = dtMarkupList.Select(find);
            if (foundRows.Length > 0)
            {
                if (foundRows[0]["AgentMarkup"] != DBNull.Value)
                {
                    txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
                }
                else
                {
                    txtAgentMarkup.Text = "0.00";
                }
                if (foundRows[0]["OurCommission"] != DBNull.Value)
                {
                    txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
                }
                else
                {
                    txtOurComm.Text = "0.00";
                }
                txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
                string markuptype = Utility.ToString(foundRows[0]["MarkupType"]);

                if (!string.IsNullOrEmpty(markuptype))
                    ddlMarkupType.SelectedValue = markuptype;

                // ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
                txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
                ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
                if (productId == 1)
                {
                    if (foundRows[0]["CarrierType"] != DBNull.Value)
                    {
                        ddlCarrierType.SelectedValue = Convert.ToString(foundRows[0]["CarrierType"]);
                    }
                    if (foundRows[0]["FlightType"] == DBNull.Value)
                    {
                        ddlFlightType.SelectedValue = "All";
                    }
                    else
                    {
                        switch (Convert.ToString(foundRows[0]["FlightType"]))
                        {
                            case "All":
                                ddlFlightType.SelectedValue = Convert.ToString(foundRows[0]["FlightType"]);
                                break;
                            case "Domestic":
                                ddlFlightType.SelectedValue = "DOM";
                                break;
                            case "International":
                                ddlFlightType.SelectedValue = "INT";
                                break;
                        }
                    }
                    if (foundRows[0]["JourneyType"] == DBNull.Value)
                    {
                        ddlJourneyType.SelectedValue = "All";
                    }
                    else
                    {
                        switch (Convert.ToString(foundRows[0]["JourneyType"]))
                        {
                            case "All":
                                ddlJourneyType.SelectedValue = Convert.ToString(foundRows[0]["JourneyType"]);
                                break;
                            case "Onward":
                                ddlJourneyType.SelectedValue = "ONW";
                                break;
                            case "Return":
                                ddlJourneyType.SelectedValue = "RET";
                                break;
                        }
                    }
                }
            }
            else
            {
                txtAgentMarkup.Text = "0.00";
                txtOurComm.Text = "0.00";
                txtMarkup.Text = "0.00";
                txtDiscount.Text = "0.00";
            }

        }
        catch 
        {

        }
    }

    private void DdlFlightType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddlSource = sender as DropDownList;
            int agent = 0;
            int rowIndex = 0;
            string find = string.Empty;
            rowIndex = Convert.ToInt32(ddlSource.ID.Split('_')[1]);
            if (ddlAgent.SelectedItem.Value != "0")
            {
                agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            }

            string source = ddlSource.SelectedItem.Text;
            TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + rowIndex.ToString());
            TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + rowIndex.ToString());
            TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + rowIndex.ToString());
            DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + rowIndex.ToString());
            TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + rowIndex.ToString());
            DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + rowIndex.ToString());
            DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_" + rowIndex.ToString());
            DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_" + rowIndex.ToString());
            DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_" + rowIndex.ToString());
            DropDownList ddlSourcelist = (DropDownList)tblMarkup.FindControl("ddlSource_" + rowIndex.ToString());

            int productId = Convert.ToInt32(chkProduct.Items[rowIndex].Value);

            //DataTable dtMarkUp = UpdateMarkup.GetMarkupRulesBySouceId(source, agent);
            dtMarkupList = UpdateMarkup.GetMarkupList(-1, ddlSourcelist.SelectedItem.Text, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
            if (agent == 0)
            {
                find = "SourceId='" + source + "' AND AgentId IS NULL";
            }
            else
            {
                find = "ProductId='" + productId + "'AND FlightType='" + source + "' AND AgentId = '" + agent + "' AND transType='B2B'";
                //DataTable dtAgentSources = AgentMaster.GetAgentSources(agent, productId);
                //DataRow[] sources = dtAgentSources.Select("Name='" + source + "'");

                //if (sources != null && productId == 1 && ddlCarrierType != null)
                //{
                //    ddlCarrierType.SelectedValue = sources[0]["SourceType"].ToString();
                //}
            }
            DataRow[] foundRows = dtMarkupList.Select(find);
            if (foundRows.Length > 0)
            {
                if (foundRows[0]["AgentMarkup"] != DBNull.Value)
                {
                    txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
                }
                else
                {
                    txtAgentMarkup.Text = "0.00";
                }
                if (foundRows[0]["OurCommission"] != DBNull.Value)
                {
                    txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
                }
                else
                {
                    txtOurComm.Text = "0.00";
                }
                txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
                string markuptype = Utility.ToString(foundRows[0]["MarkupType"]);

                if (!string.IsNullOrEmpty(markuptype))
                    ddlMarkupType.SelectedValue = markuptype;
                // ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
                txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
                ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
                if (productId == 1)
                {
                    if (foundRows[0]["CarrierType"] != DBNull.Value)
                    {
                        ddlCarrierType.SelectedValue = Convert.ToString(foundRows[0]["CarrierType"]);
                    }
                    if (foundRows[0]["FlightType"] == DBNull.Value)
                    {
                        ddlFlightType.SelectedValue = "All";
                    }
                    else
                    {
                        switch (Convert.ToString(foundRows[0]["FlightType"]))
                        {
                            case "All":
                                ddlFlightType.SelectedValue = Convert.ToString(foundRows[0]["FlightType"]);
                                break;
                            case "Domestic":
                                ddlFlightType.SelectedValue = "DOM";
                                break;
                            case "International":
                                ddlFlightType.SelectedValue = "INT";
                                break;
                        }
                    }
                    if (foundRows[0]["JourneyType"] == DBNull.Value)
                    {
                        ddlJourneyType.SelectedValue = "All";
                    }
                    else
                    {
                        switch (Convert.ToString(foundRows[0]["JourneyType"]))
                        {
                            case "All":
                                ddlJourneyType.SelectedValue = Convert.ToString(foundRows[0]["JourneyType"]);
                                break;
                            case "Onward":
                                ddlJourneyType.SelectedValue = "ONW";
                                break;
                            case "Return":
                                ddlJourneyType.SelectedValue = "RET";
                                break;
                        }
                    }
                }
            }
            else
            {
                txtAgentMarkup.Text = "0.00";
                txtOurComm.Text = "0.00";
                txtMarkup.Text = "0.00";
                txtDiscount.Text = "0.00";
            }

        }
        catch 
        {

        }
    }

    protected void ddlSource_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
           
            DropDownList ddlSource = sender as DropDownList;
            int agent = 0;
            int rowIndex = 0;
            string find = string.Empty;
            rowIndex = Convert.ToInt32(ddlSource.ID.Split('_')[1]);
            if (ddlAgent.SelectedItem.Value != "0")
            {
                agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            }

            int productId = Convert.ToInt32(chkProduct.Items[rowIndex].Value);
            string source = ddlSource.SelectedItem.Text;
            TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + rowIndex.ToString());
            TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + rowIndex.ToString());
            TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + rowIndex.ToString());
            DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + rowIndex.ToString());
            TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + rowIndex.ToString());

            DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + rowIndex.ToString());
            if (productId == 1)
            {
                DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_" + rowIndex.ToString());
                DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_" + rowIndex.ToString());
                DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_" + rowIndex.ToString());

                ddlCarrierType.Enabled = true;//Enable by default
                ddlFlightType.Enabled = true;
                ddlJourneyType.Enabled = true;

                ddlFlightType.ClearSelection();       // To clear previously selected data on changing sourcedropdown(ddlSource)
                ddlJourneyType.ClearSelection();       // added by arun.                      5/june/2018  
                ddlCarrierType.ClearSelection();

                ddlMarkupType.ClearSelection();
                txtAgentMarkup.Text = "";
                txtDiscount.Text = "";
                txtMarkup.Text = "";
                txtOurComm.Text = "";


                //DataTable dtMarkUp = UpdateMarkup.GetMarkupRulesBySouceId(source, agent);
                // dtMarkupList = UpdateMarkup.GetMarkupList(-1, source, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
                if (agent == 0)
                {
                    find = "SourceId='" + source + "' AND AgentId IS NULL";
                }
                else
                {
                    find = "ProductId='" + productId + "'AND SourceId='" + source + "' AND AgentId = '" + agent + "' AND transType='B2B'";
                    DataTable dtAgentSources = AgentMaster.GetAgentSources(agent, productId);
                    DataRow[] sources = dtAgentSources.Select("Name='" + source + "'");

                    if (sources != null && productId == 1 && ddlCarrierType != null && sources.Length > 0)
                    {
                        ddlCarrierType.SelectedValue = sources[0]["SourceType"].ToString();
                    }
                }


                dtMarkupList = UpdateMarkup.GetMarkupList(-1, source, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);


                DataRow[] foundRows = dtMarkupList.Select(find);
                if (foundRows != null && foundRows.Length > 0)
                {
                    if (foundRows[0]["AgentMarkup"] != DBNull.Value)
                    {
                        txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
                    }
                    else
                    {
                        txtAgentMarkup.Text = "0.00";
                    }
                    if (foundRows[0]["OurCommission"] != DBNull.Value)
                    {
                        txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
                    }
                    else
                    {
                        txtOurComm.Text = "0.00";
                    }
                    if (foundRows[0]["Markup"] != DBNull.Value)
                    {
                        txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
                    }
                    else
                    {
                        txtMarkup.Text = "0.00";
                    }
                    string markuptype = Utility.ToString(foundRows[0]["MarkupType"]);
                    if (!string.IsNullOrEmpty(markuptype))
                        ddlMarkupType.SelectedValue = markuptype;

                    txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
                    ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
                    if (productId == 1)
                    {
                        if (foundRows[0]["CarrierType"] != DBNull.Value)
                        {
                            ddlCarrierType.SelectedValue = Convert.ToString(foundRows[0]["CarrierType"]);                            
                        }
                        else
                        {
                            ddlCarrierType.Enabled = false;//Disable for null saving
                        }
                        if (foundRows[0]["FlightType"] == DBNull.Value)
                        {
                            ddlFlightType.SelectedValue = "All";
                            ddlFlightType.Enabled = false;//Disable for null saving
                        }
                        else
                        {                            
                            switch (Convert.ToString(foundRows[0]["FlightType"]))
                            {
                                case "All":
                                    ddlFlightType.SelectedValue = Convert.ToString(foundRows[0]["FlightType"]);
                                    break;
                                case "Domestic":
                                    ddlFlightType.SelectedValue = "DOM";
                                    break;
                                case "International":
                                    ddlFlightType.SelectedValue = "INT";
                                    break;
                            }
                        }
                        if (foundRows[0]["JourneyType"] == DBNull.Value)
                        {
                            ddlJourneyType.SelectedValue = "All";
                            ddlJourneyType.Enabled = false;//Disable for null saving
                        }
                        else
                        {                            
                            switch (Convert.ToString(foundRows[0]["JourneyType"]))
                            {
                                case "All":
                                    ddlJourneyType.SelectedValue = Convert.ToString(foundRows[0]["JourneyType"]);
                                    break;
                                case "Onward":
                                    ddlJourneyType.SelectedValue = "ONW";
                                    break;
                                case "Return":
                                    ddlJourneyType.SelectedValue = "RET";
                                    break;
                            }
                        }
                        
                    }
                }
                else
                {
                    
                    txtAgentMarkup.Text = "0.00";
                    txtOurComm.Text = "0.00";
                    txtMarkup.Text = "0.00";
                    txtDiscount.Text = "0.00";
                }
            }
            else
            {
                dtMarkupList = UpdateMarkup.GetMarkupList(-1, source, -1, string.Empty, string.Empty, string.Empty, string.Empty);
                if (agent == 0)
                {
                    find = "SourceId='" + source + "' AND AgentId IS NULL";
                }
                else
                {
                    find = "ProductId='" + productId + "'AND SourceId='" + source + "' AND AgentId = '" + agent + "' AND transType='B2B'";
                    DataTable dtAgentSources = AgentMaster.GetAgentSources(agent, productId);
                    DataRow[] sources = dtAgentSources.Select("Name='" + source + "'");


                    DataRow[] foundRows = dtMarkupList.Select(find);
                    if (foundRows != null && foundRows.Length > 0)
                    {
                        if (foundRows[0]["AgentMarkup"] != DBNull.Value)
                        {
                            txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
                        }
                        else
                        {
                            txtAgentMarkup.Text = "0.00";
                        }
                        if (foundRows[0]["OurCommission"] != DBNull.Value)
                        {
                            txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
                        }
                        else
                        {
                            txtOurComm.Text = "0.00";
                        }
                        txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
                        string markuptype = Utility.ToString(foundRows[0]["MarkupType"]);
                        if (!string.IsNullOrEmpty(markuptype))
                            ddlMarkupType.SelectedValue = markuptype;

                        txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
                        ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);


                    }
                    else
                    {
                        txtAgentMarkup.Text = "0.00";
                        txtOurComm.Text = "0.00";
                        txtMarkup.Text = "0.00";
                        txtDiscount.Text = "0.00";
                    }

                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ddlAgent_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (ListItem item in chkProduct.Items)
            {
                item.Selected = false;
            }
            tblMarkup.Rows.Clear();
            BindMarkupList();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    //protected void btnUpdate_OnClick(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        DropDownList ddlFlightType = tblMarkup.Rows[0].Cells[0].FindControl("ddlFlightType_0") as DropDownList;
    //        DropDownList ddlJourneyType = tblMarkup.Rows[0].Cells[0].FindControl("ddlJourneyType_0") as DropDownList;
    //        DropDownList ddlCarrierType = tblMarkup.Rows[0].Cells[0].FindControl("ddlCarrierType_0") as DropDownList;
    //        dtMarkupList = UpdateMarkup.GetMarkupList(-1,string.Empty,-1,string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
    //        for (int i = 0; i < chkProduct.Items.Count; i++)
    //        {
    //            if (chkProduct.Items[i].Selected)
    //            {
    //                int agent = 0;
    //                string find = string.Empty;
    //                string source = string.Empty;
    //                int productId = Convert.ToInt32(chkProduct.Items[i].Value);
    //                if (i < 2 || i == 4 || i == 5 || i == 6 || i == 9) //sources binding based on product
    //                {
    //                    DropDownList ddlSource = tblMarkup.Rows[0].Cells[i].FindControl("ddlSource_" + i.ToString()) as DropDownList;
    //                    source = ddlSource.SelectedItem.Text;
    //                    if (string.IsNullOrEmpty(source))
    //                    {
    //                        throw new Exception("Please select a source");
    //                    }
    //                }
    //                if (ddlAgent.SelectedItem.Value != "0")
    //                {
    //                    agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //                }
    //                TextBox txtAgentMarkup = tblMarkup.Rows[0].Cells[i].FindControl("txtAgentMarkup_" + i.ToString()) as TextBox;
    //                TextBox txtOurComm = tblMarkup.Rows[0].Cells[i].FindControl("txtOurComm_" + i.ToString()) as TextBox;
    //                TextBox txtMarkup = tblMarkup.Rows[0].Cells[i].FindControl("txtMarkUp_" + i.ToString()) as TextBox;
    //                DropDownList ddlMarkUp = tblMarkup.Rows[0].Cells[i].FindControl("ddlMarkupType_" + i.ToString()) as DropDownList;
    //                TextBox txtDiscount = tblMarkup.Rows[0].Cells[i].FindControl("txtDiscount_" + i.ToString()) as TextBox;
    //                DropDownList ddlDiscount = tblMarkup.Rows[0].Cells[i].FindControl("ddlDiscountType_" + i.ToString()) as DropDownList;


    //                //DataTable dtMarkUp = UpdateMarkup.GetMarkupRules(productId, source,agent);
    //                if (agent == 0)
    //                {
    //                    find = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId IS NULL";
    //                }
    //                else
    //                {
    //                    find = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId='" + agent + "' AND transType='B2B'";
    //                }

    //                DataRow[] foundRows = dtMarkupList.Select(find);
    //                UpdateMarkup objUpdateMarkup = new UpdateMarkup();
    //                if (foundRows.Length > 0)
    //                {
    //                    objUpdateMarkup.Id = Utility.ToInteger(foundRows[0]["MRId"]);
    //                    objUpdateMarkup.MrDId = Utility.ToInteger(foundRows[0]["MRDId"]);
    //                    objUpdateMarkup.HandlingFee = Utility.ToDecimal(foundRows[0]["HandlingFeeValue"]);
    //                   string handlingType = Utility.ToString(foundRows[0]["HandlingFeeType"]);
    //                    if (handlingType == "Percentage")
    //                        handlingType = "P";
    //                    else if (handlingType == "Fixed")
    //                         handlingType = "F";
    //                    objUpdateMarkup.HandlingType = handlingType;

    //                }
    //                objUpdateMarkup.ProductId = productId;
    //                objUpdateMarkup.SourceId = source;

    //                objUpdateMarkup.AgentId = agent;
    //                objUpdateMarkup.AgentMarkup = Utility.ToDecimal(txtAgentMarkup.Text);
    //                objUpdateMarkup.OurCommission = Utility.ToDecimal(txtOurComm.Text);
    //                objUpdateMarkup.Markup = Utility.ToDecimal(txtMarkup.Text);
    //                objUpdateMarkup.MarkupType = ddlMarkUp.SelectedItem.Value;
    //                objUpdateMarkup.Discount = Utility.ToDecimal(txtDiscount.Text);
    //                objUpdateMarkup.DiscountType = ddlDiscount.SelectedItem.Value;

    //                if(productId == 1)
    //                {
    //                    objUpdateMarkup.FlightType = ddlFlightType.SelectedValue;
    //                    objUpdateMarkup.JourneyType = ddlJourneyType.SelectedValue;
    //                    objUpdateMarkup.CarrierType = ddlCarrierType.SelectedValue;
    //                }

    //                objUpdateMarkup.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
    //                objUpdateMarkup.UserId = 0;// to insert null for b2b user
    //                objUpdateMarkup.Save();
    //                chkProduct.Items[i].Selected = false;
    //            }
    //        }
    //        lblSuccessMsg.Text = "Updated successfully";
    //        //rblType.SelectedIndex = 0;
    //        //ddlAgent.SelectedIndex = 0;
    //        //ddlAgent.Style.Add("display", "none");
    //        tblMarkup.Rows.Clear();
    //        BindMarkupList();

    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
    //        errorMessage.InnerHtml = ex.Message;
    //        errMess.Style["display"] = "block";
    //    }
    //}

    //protected void btnClear_OnClick(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        //rblType.SelectedIndex = 0;
    //        ddlAgent.SelectedIndex = 0;
    //        ddlAgent.Style.Add("display", "block");
    //        tblMarkup.Rows.Clear();
    //        chkProduct.ClearSelection();
    //        BindMarkupList();
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.Add(EventType.Exception, Severity.Low, 0, ex.Message, "0");
    //    }
    //}

    protected void chkProduct_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //if (rblType.SelectedItem.Value == "A")
            //{
                if (ddlAgent.SelectedIndex != 0)
                {
                    LoadControls();
                }
                else
                {
                    errMess.Style.Add("display", "block");
                    errorMessage.InnerHtml = "Please select Agent!";
                }
            //}
            //else
            //{
            //    LoadControls();
            //}
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    //private void LoadControls()
    //{
    //    Label lblFlightType = (Label)tblMarkup.FindControl("lblFlightType_0");
    //    DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_0");
    //    Label lblJourneyType = (Label)tblMarkup.FindControl("lblJourneyType_0");
    //    DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_0");
    //    Label lblCarrierType = (Label)tblMarkup.FindControl("lblCarrierType_0");
    //    DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_0");

    //    dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
    //    for (int i = 0; i < chkProduct.Items.Count; i++)
    //    {
    //        Label lblAgentMarkup = (Label)tblMarkup.FindControl("lblAgentMarkup_" + i.ToString());
    //        TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + i.ToString());
    //        Label lblOurComm = (Label)tblMarkup.FindControl("lblOurComm_" + i.ToString());
    //        TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + i.ToString());
    //        Label lblMarkup = (Label)tblMarkup.FindControl("lblMarkUp_" + i.ToString());
    //        TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + i.ToString());
    //        Label lblMarkupType = (Label)tblMarkup.FindControl("lblmarkUpType_" + i.ToString());
    //        DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + i.ToString());
    //        Label lblDiscount = (Label)tblMarkup.FindControl("lblDiscount_" + i.ToString());
    //        TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + i.ToString());
    //        Label lblDiscountType = (Label)tblMarkup.FindControl("lblDiscountType_" + i.ToString());
    //        DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + i.ToString());
    //        Label lblSource = (Label)tblMarkup.FindControl("lblSource_" + i.ToString());
    //        DropDownList ddlSource = (DropDownList)tblMarkup.FindControl("ddlSource_" + i.ToString());




    //        if (chkProduct.Items[i].Selected)
    //        {
    //            lblAgentMarkup.Style.Add("display", "block");
    //            txtAgentMarkup.Style.Add("display", "block");
    //            lblOurComm.Style.Add("display", "block");
    //            txtOurComm.Style.Add("display", "block");
    //            lblMarkup.Style.Add("display", "block");
    //            txtMarkup.Style.Add("display", "block");
    //            lblMarkupType.Style.Add("display", "block");
    //            ddlMarkupType.Style.Add("display", "block");
    //            lblDiscount.Style.Add("display", "block");
    //            txtDiscount.Style.Add("display", "block");
    //            lblDiscountType.Style.Add("display", "block");
    //            ddlDiscountType.Style.Add("display", "block");
    //            if (i == 0)
    //            {
    //                lblFlightType.Style.Add("display", "block");
    //                ddlFlightType.Style.Add("display", "block");
    //                lblJourneyType.Style.Add("display", "block");
    //                ddlJourneyType.Style.Add("display", "block");
    //                lblCarrierType.Style.Add("display", "block");
    //                ddlCarrierType.Style.Add("display", "block");
    //            }
    //            if (i < 2 || i == 4 || i == 5 || i == 6 || i == 9) // Sources is there Just displaying
    //            {
    //                lblSource.Style.Add("display", "block");
    //                ddlSource.Style.Add("display", "block");
    //            }
    //            if (i == 2 || i == 3 || i == 7 || i == 8) // Here source is not there Loading markup directly
    //            {
    //                int agent = 0;
    //                string find = string.Empty;
    //                int productId = Convert.ToInt32(chkProduct.Items[i].Value);
    //                if (ddlAgent.SelectedItem.Value != "0")
    //                {
    //                    agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //                }
    //                if (agent == 0)
    //                {
    //                    find = "ProductId='" + productId + "' AND AgentId IS NULL";
    //                }
    //                else
    //                {
    //                    find = "ProductId='" + productId + "' AND AgentId = '" + agent + "'  AND transType='B2B'";
    //                }
    //                DataRow[] foundRows = dtMarkupList.Select(find);
    //                //DataTable dtMarkupDetails = UpdateMarkup.GetMarkupDetails(productId, agent);
    //                if (foundRows.Length > 0)
    //                {
    //                    txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
    //                    txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
    //                    txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
    //                    ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
    //                    txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
    //                    ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
    //                }
    //                else
    //                {
    //                    txtAgentMarkup.Text = "0.00";
    //                    txtOurComm.Text = "0.00";
    //                    txtMarkup.Text = "0.00";
    //                    txtDiscount.Text = "0.00";
    //                }
    //            }
    //        }
    //        else
    //        {
    //            lblAgentMarkup.Style.Add("display", "none");
    //            txtAgentMarkup.Style.Add("display", "none");
    //            lblOurComm.Style.Add("display", "none");
    //            txtOurComm.Style.Add("display", "none");
    //            lblMarkup.Style.Add("display", "none");
    //            txtMarkup.Style.Add("display", "none");
    //            lblMarkupType.Style.Add("display", "none");
    //            ddlMarkupType.Style.Add("display", "none");
    //            lblDiscount.Style.Add("display", "none");
    //            txtDiscount.Style.Add("display", "none");
    //            lblDiscountType.Style.Add("display", "none");
    //            ddlDiscountType.Style.Add("display", "none");
    //            if (i == 0)
    //            {
    //                lblFlightType.Style.Add("display", "none");
    //                ddlFlightType.Style.Add("display", "none");
    //                lblJourneyType.Style.Add("display", "none");
    //                ddlJourneyType.Style.Add("display", "none");
    //                lblCarrierType.Style.Add("display", "none");
    //                ddlCarrierType.Style.Add("display", "none");
    //            }
    //            if (i < 2 || i == 4 || i == 5 || i == 6 || i == 9) //Checking only sources products only
    //            {
    //                lblSource.Style.Add("display", "none");
    //                ddlSource.Style.Add("display", "none");
    //            }
    //        }
    //    }
    //}

    //private void BindMarkupList()
    //{
    //    try
    //    {
    //        string agentType = string.Empty;

    //        int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
    //        if (Settings.LoginInfo.AgentType == AgentType.B2B)
    //        {
    //            if (agentId <= 0)
    //            {
    //                agentType = "B2B";
    //                agentId = Settings.LoginInfo.AgentId;
    //            }
    //        }
    //        if (agentId > 0)
    //        {
    //            dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, agentId, agentType, string.Empty,string.Empty,string.Empty);
    //            if (dtMarkupList != null && dtMarkupList.Rows.Count > 0)
    //            {
    //                dlMarkup.Visible = true;
    //                lblMessage.Visible = false;
    //                dlMarkup.DataSource = dtMarkupList;
    //                ViewState["Markup"] = dtMarkupList;
    //                dlMarkup.DataBind();
    //                doPaging();
    //            }
    //            else
    //            {
    //                DisableDataList();
    //            }
    //        }
    //        else
    //        {
    //            DisableDataList();
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.Add(EventType.Exception, Severity.Low, 0, ex.Message, "0");
    //    }
    //}
    //private void DisableDataList()
    //{
    //    dlMarkup.Visible = false;
    //    btnPrev.Visible = false;
    //    btnFirst.Visible = false;
    //    btnLast.Visible = false;
    //    btnNext.Visible = false;
    //    lblMessage.Visible = true;
    //    lblCurrentPage.Text = string.Empty;
    //    lblMessage.Text = "No Results Found.";
    //}

    //protected void ddlSource_OnSelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        DropDownList ddlSource = sender as DropDownList;
    //        int agent = 0;
    //        int rowIndex = 0;
    //        string find = string.Empty;
    //        rowIndex = Convert.ToInt32(ddlSource.ID.Split('_')[1]);
    //        if (ddlAgent.SelectedItem.Value != "0")
    //        {
    //            agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //        }
            
    //        string source = ddlSource.SelectedItem.Text;
    //        TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtAgentMarkup_" + rowIndex.ToString());
    //        TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + rowIndex.ToString());
    //        TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + rowIndex.ToString());
    //        DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + rowIndex.ToString());
    //        TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + rowIndex.ToString());
    //        DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + rowIndex.ToString());
    //        DropDownList ddlCarrierType = (DropDownList)tblMarkup.FindControl("ddlCarrierType_" + rowIndex.ToString());
    //        DropDownList ddlFlightType = (DropDownList)tblMarkup.FindControl("ddlFlightType_" + rowIndex.ToString());
    //        DropDownList ddlJourneyType = (DropDownList)tblMarkup.FindControl("ddlJourneyType_" + rowIndex.ToString());
    //        int productId = Convert.ToInt32(chkProduct.Items[rowIndex].Value);
            
    //        //DataTable dtMarkUp = UpdateMarkup.GetMarkupRulesBySouceId(source, agent);
    //        dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1,string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
    //        if (agent == 0)
    //        {
    //            find = "SourceId='" + source + "' AND AgentId IS NULL";
    //        }
    //        else
    //        {
    //            find = "ProductId='" + productId + "'AND SourceId='" + source + "' AND AgentId = '" + agent + "' AND transType='B2B'";
    //            DataTable dtAgentSources = AgentMaster.GetAgentSources(agent, productId);
    //            DataRow[] sources = dtAgentSources.Select("Name='" + source + "'");

    //            if (sources != null && productId == 1 && ddlCarrierType != null)
    //            {
    //                ddlCarrierType.SelectedValue = sources[0]["SourceType"].ToString();
    //            }
    //        }
    //        DataRow[] foundRows = dtMarkupList.Select(find);
    //        if (foundRows.Length > 0)
    //        {
    //            txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
    //            txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
    //            txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
    //            ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
    //            txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
    //            ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
    //            if (productId == 1)
    //            {
    //                if (foundRows[0]["CarrierType"] != DBNull.Value)
    //                {
    //                    ddlCarrierType.SelectedValue = Convert.ToString(foundRows[0]["CarrierType"]);
    //                }
    //                if (foundRows[0]["FlightType"] == DBNull.Value)
    //                {
    //                    ddlFlightType.SelectedValue = "All";
    //                }
    //                else
    //                {
    //                    switch (Convert.ToString(foundRows[0]["FlightType"]))
    //                    {
    //                        case "All":
    //                            ddlFlightType.SelectedValue = Convert.ToString(foundRows[0]["FlightType"]);
    //                            break;
    //                        case "Domestic":
    //                            ddlFlightType.SelectedValue = "DOM";
    //                            break;
    //                        case "International":
    //                            ddlFlightType.SelectedValue = "INT";
    //                            break;
    //                    }
    //                }
    //                if(foundRows[0]["JourneyType"] == DBNull.Value)
    //                {
    //                    ddlJourneyType.SelectedValue = "All";
    //                }
    //                else
    //                {
    //                    switch (Convert.ToString(foundRows[0]["JourneyType"]))
    //                    {
    //                        case "All":
    //                            ddlJourneyType.SelectedValue = Convert.ToString(foundRows[0]["JourneyType"]);
    //                            break;
    //                        case "Onward":
    //                            ddlJourneyType.SelectedValue = "ONW";
    //                            break;
    //                        case "Return":
    //                            ddlJourneyType.SelectedValue = "RET";
    //                            break;
    //                    }
    //                }                        
    //            }
    //        }
    //        else
    //        {
    //            txtAgentMarkup.Text = "0.00";
    //            txtOurComm.Text = "0.00";
    //            txtMarkup.Text = "0.00";
    //            txtDiscount.Text = "0.00";
    //        }

    //    }
    //    catch (Exception ex)
    //    {

    //    }
    //}

    //protected void rblType_OnSelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        if (rblType.SelectedItem.Value == "A")
    //        {
    //            ddlAgent.Style.Add("display", "block");
    //            tblMarkup.Rows.Clear();
    //            foreach (ListItem item in chkProduct.Items)
    //            {
    //                item.Selected = false;
    //            }
    //        }
    //        else
    //        {
    //            ddlAgent.Style.Add("display", "none");
    //            ddlAgent.SelectedIndex = 0;
    //            tblMarkup.Rows.Clear();
    //            foreach (ListItem item in chkProduct.Items)
    //            {
    //                item.Selected = false;
    //            }
    //        }
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}
    //protected void ddlAgent_OnSelectedIndexChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        foreach (ListItem item in chkProduct.Items)
    //        {
    //            item.Selected = false;
    //        }
    //        tblMarkup.Rows.Clear();
    //        BindMarkupList();
    //    }
    //    catch (Exception ex)
    //    {
    //        throw ex;
    //    }
    //}

    //private void AuthorizationCheck()
    //{
    //    if (Settings.LoginInfo == null)
    //    {
    //        Response.Redirect("AbandonSession.aspx", true);
    //    }
    //}

    void doPaging()
    {
        DataTable pdt = (DataTable)ViewState["Markup"];
        if (pdt != null && pdt.Rows.Count > 0)
        {
            DataTable dt = (DataTable)ViewState["Markup"];
            pagedData.DataSource = dt.DefaultView;
        }
        pagedData.AllowPaging = true;
        pagedData.PageSize = 10;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        dlMarkup.DataSource = pagedData;
        dlMarkup.DataBind();
    }

    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }
    #region button events

    protected void btnUpdate_OnClick(object sender, EventArgs e)
    {
        try
        {
            DropDownList ddlFlightType = tblMarkup.Rows[0].Cells[0].FindControl("ddlFlightType_0") as DropDownList;
            DropDownList ddlJourneyType = tblMarkup.Rows[0].Cells[0].FindControl("ddlJourneyType_0") as DropDownList;
            DropDownList ddlCarrierType = tblMarkup.Rows[0].Cells[0].FindControl("ddlCarrierType_0") as DropDownList;
            dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty, ddlFlightType.SelectedValue, ddlJourneyType.SelectedValue, ddlCarrierType.SelectedValue);
            for (int i = 0; i < chkProduct.Items.Count; i++)
            {
                if (chkProduct.Items[i].Selected)
                {
                    int agent = 0;
                    string find = string.Empty;
                    string source = string.Empty;
                    int productId = Convert.ToInt32(chkProduct.Items[i].Value);
                    if (i < 2 || i == 4 || i == 5 || i == 6 || i == 9 || i==10) //sources binding based on product
                    {
                        DropDownList ddlSource = tblMarkup.Rows[0].Cells[i].FindControl("ddlSource_" + i.ToString()) as DropDownList;
                        source = ddlSource.SelectedItem.Text;
                        if (string.IsNullOrEmpty(source) || ddlSource.SelectedIndex<1)
                        {
                            throw new Exception("Please select a source");
                        }
                    }
                    if (ddlAgent.SelectedItem.Value != "0")
                    {
                        agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                    TextBox txtAgentMarkup = tblMarkup.Rows[0].Cells[i].FindControl("txtAgentMarkup_" + i.ToString()) as TextBox;
                    TextBox txtOurComm = tblMarkup.Rows[0].Cells[i].FindControl("txtOurComm_" + i.ToString()) as TextBox;
                    TextBox txtMarkup = tblMarkup.Rows[0].Cells[i].FindControl("txtMarkUp_" + i.ToString()) as TextBox;
                    DropDownList ddlMarkUp = tblMarkup.Rows[0].Cells[i].FindControl("ddlMarkupType_" + i.ToString()) as DropDownList;
                    TextBox txtDiscount = tblMarkup.Rows[0].Cells[i].FindControl("txtDiscount_" + i.ToString()) as TextBox;
                    DropDownList ddlDiscount = tblMarkup.Rows[0].Cells[i].FindControl("ddlDiscountType_" + i.ToString()) as DropDownList;


                    //DataTable dtMarkUp = UpdateMarkup.GetMarkupRules(productId, source,agent);
                    if (agent == 0)
                    {
                        find = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId IS NULL";
                    }
                    else
                    {
                        find = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId='" + agent + "' AND transType='B2B'";
                    }

                    DataRow[] foundRows = dtMarkupList.Select(find);
                    UpdateMarkup objUpdateMarkup = new UpdateMarkup();
                    if (foundRows.Length > 0)
                    {
                        objUpdateMarkup.Id = Utility.ToInteger(foundRows[0]["MRId"]);
                        objUpdateMarkup.MrDId = Utility.ToInteger(foundRows[0]["MRDId"]);
                        objUpdateMarkup.HandlingFee = Utility.ToDecimal(foundRows[0]["HandlingFeeValue"]);
                        if (!string.IsNullOrEmpty(Convert.ToString(foundRows[0]["HandlingFeeType"])))
                            objUpdateMarkup.HandlingType = Utility.ToString(foundRows[0]["HandlingFeeType"]).Substring(0, 1);
                        else
                            objUpdateMarkup.HandlingType = "P";
   
                    }
                    else
                    { objUpdateMarkup.HandlingType = "P"; }
                    dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty, string.Empty, string.Empty, string.Empty);
                    DataRow[] Mrid = dtMarkupList.Select(find);
                    if (Mrid.Length > 0)
                    {
                        objUpdateMarkup.Id = Utility.ToInteger(Mrid[0]["MRId"]);
                    }
                    objUpdateMarkup.ProductId = productId;
                 
                    objUpdateMarkup.SourceId = source;

                    objUpdateMarkup.AgentId = agent;
                    objUpdateMarkup.AgentMarkup = Utility.ToDecimal(txtAgentMarkup.Text);
                    objUpdateMarkup.OurCommission = Utility.ToDecimal(txtOurComm.Text);
                    objUpdateMarkup.Markup = Utility.ToDecimal(txtMarkup.Text);
                    objUpdateMarkup.MarkupType = ddlMarkUp.SelectedItem.Value;
                    objUpdateMarkup.Discount = Utility.ToDecimal(txtDiscount.Text);
                    objUpdateMarkup.DiscountType = ddlDiscount.SelectedItem.Value;

                    if (productId == 1)
                    {
                        objUpdateMarkup.FlightType = ddlFlightType.SelectedValue;
                        objUpdateMarkup.JourneyType = ddlJourneyType.SelectedValue;
                        objUpdateMarkup.CarrierType = ddlCarrierType.SelectedValue;
                    }

                    objUpdateMarkup.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
                    objUpdateMarkup.UserId = 0;// to insert null for b2b user
                    objUpdateMarkup.Save();
                    chkProduct.Items[i].Selected = false;
                }
            }
            lblSuccessMsg.Text = "Updated successfully";
            //rblType.SelectedIndex = 0;
            //ddlAgent.SelectedIndex = 0;
            //ddlAgent.Style.Add("display", "none");
            tblMarkup.Rows.Clear();
            BindMarkupList();

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
            errorMessage.InnerHtml = ex.Message;
            errMess.Style["display"] = "block";
        }
    }

    protected void btnClear_OnClick(object sender, EventArgs e)
    {
        try
        {
            //rblType.SelectedIndex = 0;
            ddlAgent.SelectedIndex = 0;
            ddlAgent.Style.Add("display", "block");
            tblMarkup.Rows.Clear();
            chkProduct.ClearSelection();
            BindMarkupList();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.Low, 0, ex.Message, "0");
        }
    }


    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }
    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();

    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }
    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }

    #endregion

}
