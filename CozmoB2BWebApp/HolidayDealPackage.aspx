<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="HolidayDealPackageGUI" EnableEventValidation="false" Title="Holiday Deal Package" ValidateRequest="false" Codebehind="HolidayDealPackage.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<style>
hr.line  { border: none;   background-color: #ccc;  color: #ccc;  height: 1px;  }
</style>
  <script type="text/javascript" src="Scripts/Common/Common.js"></script>

  <script language="javascript" src="tinymce/jscripts/tiny_mce/tiny_mce.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript">
    tinyMCE.init({
        mode: "textareas",
        theme: "advanced",
        width: '640px',
        theme_advanced_buttons1_add: "bullist,numlist,outdent,indent,undo,redo,fontselect,fontsizeselect",
        theme_advanced_buttons2: "",
        theme_advanced_toolbar_location: "top",
        theme_advanced_disable: "styleselect,anchor,formatselect,justifyfull,help,cleanup",
        convert_fonts_to_spans: true,
        content_css: "/style.css"
    });

   // function $(id) { return document.getElementById(id); }
    function trim(stringToTrim) {
        return stringToTrim.replace(/^\s+|\s+$/g, "");
    }
    function Remove(id, val) {

        if (id.split('-')[1] != 1) {
            document.getElementById(id).value = "";
            document.getElementById(id).style.display = "none";
            if (val == "text") {

                var count = eval(id.split('-')[1]);
                var idSplit = id.split('-')[0];
                if (eval(count - 1) != 1) {
                    document.getElementById(idSplit + "Img-" + eval(count - 1)).style.display = "block";
                }
                var idCurrent = eval(document.getElementById(id.split('-')[0] + 'Current').value);
                idCurrent--;
                document.getElementById(id.split('-')[0] + 'Current').value = idCurrent;

                if (idSplit == "Inclusions") {
                    document.getElementById("<%=hdfinclCounter.ClientID%>").value = idCurrent;
                }
                else if (idSplit == "Exclusions") {
                    document.getElementById("<%=hdfExclCounter.ClientID%>").value = idCurrent;
                }
                else if (idSplit == "PriceInclude") {
                document.getElementById("<%=hdfpriceExcludeCounter.ClientID%>").value = idCurrent;
                }
                else if (idSplit == "T&C") {
                document.getElementById("<%=hdftncCounter.ClientID%>").value = idCurrent;
                }
                
            }
        }
    }

    function AddTextBox(divName) {
       // alert(divName);
        var idCount = document.getElementById(divName + 'Counter').value;
        var idCurrent = document.getElementById(divName + 'Current').value;
       
        if (document.getElementById(divName + "-" + idCurrent)) {
            document.getElementById(divName + "-" + idCurrent).style.display = "block";
            document.getElementById(divName + "Img-" + idCurrent).style.display = "block";
            Remove(divName + "Img-" + eval(idCurrent - 1), 'img');
            idCurrent++;
            document.getElementById(divName + 'Current').value = idCurrent;
        }
        else {
       
           // var idPrevText = document.getElementById("InclusionsText"+ (idCount-1)).value;
           
                var stringHTML = "";
                var newdiv = document.createElement('div');
                if (divName == "Inclusions") {
                    stringHTML += "<p class=\"fleft width-100 padding-top-5\" style=\"padding-left:160px;\" id=\"" + divName + "-" + idCount + "\">";
                }
                if (divName == "PriceInclude" || divName == "T&C" || divName == "Exclusions") {
                    stringHTML += "<p class=\"fleft width-100 padding-top-5\" style=\"padding-left:160px;\" id=\"" + divName + "-" + idCount + "\">";
                }
                stringHTML += "<span class=\"fleft width-300\"><input type=\"text\" id=\"" + divName + "Text-" + idCount + "\" name=\"" + divName + "Text-" + idCount + "\" class=\"width-300 fleft\" /></span>";
                stringHTML += "<i class=\"fleft margin-left-10\" id=\"" + divName + "Img-" + idCount + "\"><img src=\"Images/delete.gif\" alt=\"Remove\" onclick=\"javascript:Remove('" + divName + "-" + idCount + "','text')\"/></i>";
                stringHTML += "</p>";
                newdiv.innerHTML = stringHTML;
                document.getElementById(divName + "Div").appendChild(newdiv)
                Remove(divName + "Img-" + eval(idCount - 1), 'img');

                idCount++;
                document.getElementById(divName + 'Counter').value = idCount;
                idCurrent++;
                document.getElementById(divName + 'Current').value = idCurrent;

                if (divName == "Inclusions") {
                    document.getElementById("<%=hdfinclCounter.ClientID%>").value = idCount;
                }
                else if (divName == "Exclusions") {
                    document.getElementById("<%=hdfExclCounter.ClientID%>").value = idCount;
                }
                else if (divName == "PriceInclude") {
                    document.getElementById("<%=hdfpriceExcludeCounter.ClientID%>").value = idCount;
                }
                else if (divName == "T&C") {
                    document.getElementById("<%=hdftncCounter.ClientID%>").value = idCount;
                }
            
        }
    }


//    function Save() {
//        if (isValid()) {
//            
//        }
//        else {
//            return false;
//        }
////        $('savePage').value = "true";         
////            document.forms[0].submit();
//        }
    function Save() {

        var regexStr = /<\S[^><]*>/g;
        var regexStr1 = /&nbsp;/g;
        var regexStr2 = /<BR>/g;
       // var changed = window.frames.mce_editor_0.document.body.innerHTML.replace(regexStr, '');
       // var changed = changed.replace(regexStr1, '');
        //var changed = changed.replace(regexStr2, ''); 
            document.getElementById('errMess').style.display = "none";
            if (document.getElementById("<%=ddlAgent.ClientID%>").selectedIndex == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select the Agent for the package!";
                return false;
            }
            if (document.getElementById("<%=chkCopy.ClientID%>").checked) {
                if (document.getElementById("<%=ddlPackages.ClientID%>").selectedIndex == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select the Package for the package!";
                    return false;
                }
                if (document.getElementById("<%=ddlAgentCopy.ClientID%>").selectedIndex == 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select the AgentCopy for the package!";
                    return false;
                }
            }
            
            if (document.getElementById("<%=txtPackage.ClientID%>").value.trim() == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Package name cannot be blank!"
                return false;
            }
            if (document.getElementById("<%=ddlNights.ClientID%>").selectedIndex == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select the nights for the package!";
                return false;
            }
            if (document.getElementById("<%=txtCity.ClientID%>").value.trim() == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter a valid city name!"
                return false;
            }
            if (document.getElementById("<%=txtCountry.ClientID%>").value.trim() == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter a valid country name!"
                return false;
            }
            if (document.getElementById("<%=hdfUploadYNImage1.ClientID%>").value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Main Image should be uploaded !";
                return false;
            }

            /*Add 02032016 */
            if (document.getElementById("<%=hdfUploadYNImage4.ClientID%>").value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Main Image1 should be uploaded !";
                return false;
            }


            /*Add 02032016 */
            if (document.getElementById("<%=hdfUploadYNImage5.ClientID%>").value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Main Image2 should be uploaded !";
                return false;
            }

            if (document.getElementById("<%=hdfUploadYNImage2.ClientID%>").value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Side Image should be uploaded !";
                return false;
            }

            if (document.getElementById("<%=hdfUploadYNImage3.ClientID%>").value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Thumbnail should be uploaded !";
                return false;
            }

           
            
            var chklist = document.getElementById('<%= chkTheme.ClientID %>');
            var chkListinputs = chklist.getElementsByTagName("input");
            var count = 0;
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    count = count + 1;
                }
            }
            if (count == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Atleast one theme should be selected !";
                return false;
            }
            if (document.getElementById("<%=txtDescription.ClientID%>").value.trim() == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter a short description for the deal!"
                return false;
            }
            //commented on 09/10/2015 by chandan @@@
            var changed = window.frames.mce_editor_0.document.body.innerHTML.replace(regexStr, '');
            var changed = changed.replace(regexStr1, '');
           var changed = changed.replace(regexStr2, '');

           if (trim(changed) == "") {
               document.getElementById('errMess').style.display = "block";
               document.getElementById('errorMessage').innerHTML = "Please enter overview for the deal!"
               return false;
           }
            if (document.getElementById("<%=ddlTourType.ClientID%>").selectedIndex == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please select a tour type for hotel deal!";
                return false;
            }
            var incValue = eval($('InclusionsCurrent').value);

            for (var i = 1; i < incValue; i++) {

                if (Trim($('InclusionsText-' + i).value) == "") {
                    if (i == 1) {
                        $('errorMessage').innerHTML = "Please enter Inclusions for HotelDeal.";
                        $('errMess').style.display = "block";
                        return false;
                    }
                    else {
                        $('errorMessage').innerHTML = "Please fill all Inclusions for HotelDeal.";
                        $('errMess').style.display = "block";
                        return false;
                    }
                }
            }
            if (document.getElementById("<%=ddlNights.ClientID%>").selectedIndex > 0) {
                var nights = parseInt(document.getElementById('<%= ddlNights.ClientID %>').value) + 1;
                for (var i = 0; i < nights; i++) {
                    //change ct100_contentPlaceHolder_txtDay to cphTransaction
                    if (document.getElementById('ctl00_cphTransaction_txtDay_' + i).value.trim() == "") {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Package Day '" + (parseInt(i) + 1) + "' cannot be blank!"
                        return false;
                    }
//                    if (document.getElementById('ctl00_ContentPlaceHolder1_txtDesc_' + i).value.trim() == "") {
//                        document.getElementById('errMess').style.display = "block";
                    //    document.getElementById('errorMessage').innerHTML = "Package Day '" + (parseInt(i) + 1) + "' cannot be blank!"
//                        return false;
//                    }
                    if (document.getElementById('ctl00_cphTransaction_txtMeals_' + i).value.trim() == "") {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errorMessage').innerHTML = "Package Day '" + (parseInt(i) + 1) + "' cannot be blank!"
                        return false;
                    }
//                    if (document.getElementById('ctl00_ContentPlaceHolder1_txtSuppNameEmail_' + i).value.trim() != "" && !checkEmail(document.getElementById('ctl00_ContentPlaceHolder1_txtSuppNameEmail_' + i).value.trim())) {
//                        document.getElementById('errMess').style.display = "block";
//                        document.getElementById('errorMessage').innerHTML = "Supplier Email '" +(parseInt(i)+1) + "' is not valid!"
//                        return false;
//                    }
                }
            }
//            var changed = window.frames.mce_editor_2.document.body.innerHTML.replace(regexStr, '');
//            var changed = changed.replace(regexStr1, '');
//            var changed = changed.replace(regexStr2, '');
//            if (trim(changed) == "") {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "Please enter double price for hotel for Room Rates.!"
//                return false;
//            }
            if (document.getElementById("<%=txtStartFrom.ClientID%>").value.trim() == "") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errorMessage').innerHTML = "Please enter proper value for double price of hotel for Room Rates.!"
                return false;
            }
            var exclValue = eval($('ExclusionsCurrent').value);  
            for (var i = 1; i < exclValue; i++) {
                if (Trim($('ExclusionsText-' + i).value) == "") {
                    if (i == 1) {
                        $('errorMessage').innerHTML = "Please enter Exclusions for the HotelDeal.";
                        $('errMess').style.display = "block";
                        return false;
                    }
                    else {
                        $('errorMessage').innerHTML = "Please fill all Exclusions for the HotelDeal.";
                        $('errMess').style.display = "block";
                        return false;
                    }
                }
            }

            var TCValue = eval($('T&CCurrent').value);
            for (var i = 1; i < TCValue; i++) {
                if (Trim($('T&CText-' + i).value) == "") {
                    if (i == 1) {
                        $('errorMessage').innerHTML = "Please enter Terms and condition for the HotelDeal.";
                        $('errMess').style.display = "block";
                        return false;
                    }
                    else {
                        $('errorMessage').innerHTML = "Please fill all Terms and condition for the HotelDeal.";
                        $('errMess').style.display = "block";
                        return false;
                    }
                }
            }
            return true;
        }
        function chkvalidate() {

            if (document.getElementById("<%=chkCopy.ClientID%>").checked) {
                document.getElementById('errMess').style.display = "none";
                if (document.getElementById("<%= ddlAgent.ClientID %>").selectedIndex <= 0) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please select a Agent for hotel deal!";
                    return false;
                }
                document.getElementById("<%= lblPackage.ClientID %>").style.display = "block";
                document.getElementById("<%= ddlPackages.ClientID %>").style.display = "block";
                document.getElementById("<%= lblcopyAgent.ClientID %>").style.display = "block";
                document.getElementById("<%= ddlAgentCopy.ClientID %>").style.display = "block";
            }
            else {
                document.getElementById("<%= lblPackage.ClientID %>").style.display = "none";
                document.getElementById("<%= ddlPackages.ClientID %>").style.display = "none";
                document.getElementById("<%= lblcopyAgent.ClientID %>").style.display = "none";
                document.getElementById("<%= ddlAgentCopy.ClientID %>").style.display = "none";
            }

        }
        function showimag1(id, imgid, hdfid, hideid) {
            var div = document.getElementById(id);
            if (div.style.display == "block") {
                div.style.display = "none";
            }
            else {
                div.style.display = "block"; 
            }
            //alert(document.getElementById(hdfid).value);
            document.getElementById(imgid).src = document.getElementById(hdfid).value;
            return false;
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }
        function showImage(stat, img) {

            if (stat == "S") {

                // alert(document.getElementById('<%=pnlImages.ClientID %>'));
                document.getElementById('<%=pnlImages.ClientID %>').style.display = "block";

                if (img == "1") {
                    document.getElementById('<%=img1PreView.ClientID %>').style.display = "block";
                    document.getElementById('<%=img2PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img3PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img4PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img5PreView.ClientID %>').style.display = "none";
                }
                else if (img == "2") {
                    document.getElementById('<%=img1PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img2PreView.ClientID %>').style.display = "block";
                    document.getElementById('<%=img3PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img4PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img5PreView.ClientID %>').style.display = "none";
                }
                else if (img == "3") {
                    document.getElementById('<%=img1PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img2PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img3PreView.ClientID %>').style.display = "block";
                    document.getElementById('<%=img4PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img5PreView.ClientID %>').style.display = "none";
                }
                else if (img == "4") {
                    document.getElementById('<%=img1PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img2PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img3PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img4PreView.ClientID %>').style.display = "block";
                    document.getElementById('<%=img5PreView.ClientID %>').style.display = "none";
                }
                else {
                    document.getElementById('<%=img1PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img2PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img3PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img4PreView.ClientID %>').style.display = "none";
                    document.getElementById('<%=img5PreView.ClientID %>').style.display = "block";
                }
            }
            else {
                document.getElementById('<%=pnlImages.ClientID %>').style.display = "none";
                document.getElementById('<%=img1PreView.ClientID %>').style.display = "none";
                document.getElementById('<%=img2PreView.ClientID %>').style.display = "none";
                document.getElementById('<%=img3PreView.ClientID %>').style.display = "none";
                document.getElementById('<%=img4PreView.ClientID %>').style.display = "none";
                document.getElementById('<%=img5PreView.ClientID %>').style.display = "none";
                return false;
            }

        }
        function showCopyControls() {
            if (document.getElementById("<%=chkCopy.ClientID%>").checked) {
                document.getElementById("<%= lblPackage.ClientID %>").style.display = "block";
                document.getElementById("<%= ddlPackages.ClientID %>").style.display = "block";
                document.getElementById("<%= lblcopyAgent.ClientID %>").style.display = "block";
                document.getElementById("<%= ddlAgentCopy.ClientID %>").style.display = "block";
            }
        }
</script>
<link href="Styles/Default.css" rel="stylesheet" type="text/css" />

<asp:HiddenField runat="server" ID="hdfImage1" Value="0" />
<asp:HiddenField runat="server" ID="hdfImage2" Value="0" />
<asp:HiddenField runat="server" ID="hdfImage3" Value="0" />
<asp:HiddenField runat="server" ID="hdfImage4" Value="0" /><!--02032016  MainImage1-->
<asp:HiddenField runat="server" ID="hdfImage5" Value="0" /><!--02032016  MainImage2-->

<asp:HiddenField runat="server" ID="hdfImage1Extn" Value="0" />
<asp:HiddenField runat="server" ID="hdfImage2Extn" Value="0" />
<asp:HiddenField runat="server" ID="hdfImage3Extn" Value="0" />
<asp:HiddenField runat="server" ID="hdfImage4Extn" Value="0" /> <!--02032016  MainImage1-->
<asp:HiddenField runat="server" ID="hdfImage5Extn" Value="0" /> <!--02032016  MainImage2-->

<asp:HiddenField runat="server" ID="hdfUploadYNImage1" Value="0" />
<asp:HiddenField runat="server" ID="hdfUploadYNImage2" Value="0" />
<asp:HiddenField runat="server" ID="hdfUploadYNImage3" Value="0" />
<asp:HiddenField runat="server" ID="hdfUploadYNImage4" Value="0" /> <!--02032016  MainImage1-->
<asp:HiddenField runat="server" ID="hdfUploadYNImage5" Value="0" /> <!--02032016  MainImage2-->

<asp:HiddenField runat="server" ID="hdfAgencyImagePath" Value="" />

<asp:HiddenField runat="server" ID="hdfOldImage1" Value="" />
<asp:HiddenField runat="server" ID="hdfOldImage2" Value="" />
<asp:HiddenField runat="server" ID="hdfOldImage3" Value="" />
<asp:HiddenField runat="server" ID="hdfOldImage4" Value="" /> <!--02032016 MainImage1 -->
<asp:HiddenField runat="server" ID="hdfOldImage5" Value="" /> <!--02032016 MainImage2 -->

<div style=" margin-top:10px;" class="ns-h3">Package </div>



<div class="bg_white bor_gray pad_10">

<div class="paramcon">
  
         <div class="col-md-12 padding-0 marbot_10">    
           
           <%-- <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.MemberType == CT.TicketReceipt.BusinessLayer.MemberType.ADMIN)
              { %>--%>
              <%--<a href="HolidayWhiteLabelSettings.aspx?memberId=<%= Request["memberId"] %>&agencyId=<%= Request["agencyId"] %>">Setting</a>
                
               <a href="HolidayWhiteLabelSiteLayout.aspx?memberId=<%= Request["memberId"] %>&agencyId=<%= Request["agencyId"] %>">Layout</a>--%>
                
               <a class="fcol_blue margin-right-5" href="HolidayDealPackage.aspx">Create Package</a>
            
               <a class="fcol_blue margin-right-5" href="HotelDealSetting.aspx">Package List</a>
               <%--<a href="PackageQueriesPanel.aspx?memberId=<%= Request["memberId"] %>&agencyId=<%= Request["agencyId"] %>">Order Tracking</a>
               <a href="PackageQueues.aspx?memberId=<%= Request["memberId"] %>&agencyId=<%= Request["agencyId"] %>">Package Queues</a>--%>
                
                
                <%--<%}
                    else
              { %>--%>
              
            <%--  <a href="HolidayDealPackage.aspx">Create Package </a>
             <a href="HotelDealSetting.aspx">Package Order</a>--%>
              
              <%--<a href="PackageQueriesPanel.aspx">Order Tracking</a>
              <a href="PackageQueues.aspx?memberId=<%= Request["memberId"] %>&agencyId=<%= Request["agencyId"] %>">Package Queues</a>--%>
              
             <%-- <%} %>--%>
            
          </div>
       
        
        
        <div> 
<input type="hidden" id = "savePage" name="savePage" value="false" />
<asp:label runat="server" style="color:Red" ID="lblError" Visible="false" Text=""></asp:label>
<asp:label runat="server" style="color:Green" ID="lblSucess" Visible="false" Text=""></asp:label>
<div id="errMess" class="error_module" style="display:none;"> 
           <div id="errorMessage" style="float:left; color:Red;" class="padding-5 yellow-back width-100 center margin-top-5"> </div>
           </div>
   </div>        
         
         
         
        <div class="col-md-12 marbot_10">   <h4> Create Package Deal</h4>      </div>
         
         <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblAgent" runat="server" Text="Agent" ></asp:Label></b><sup>*</sup></div>
    
    
    
    <div class="col-md-3"> 
     <asp:UpdatePanel ID="UP_ddlAgent" runat="server">
    <ContentTemplate>
   <asp:DropDownList ID="ddlAgent" CssClass="form-control" runat="server" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList> 
    </ContentTemplate>
    <Triggers>
    <asp:PostBackTrigger ControlID="ddlAgent" />
    </Triggers>
    </asp:UpdatePanel>
    
    </div>
    
    
    <div class="col-md-2"> <asp:CheckBox ID="chkCopy" Font-Bold="true" runat="server" Text="Copy" onclick="return chkvalidate();" /></div>
    
    
   


    <div class="clearfix"></div>
    </div>
    
    
    
       <div class="col-md-12 padding-0 marbot_10">                   
    
    
        <div class="col-md-2">
        
        <asp:Label ID="lblPackage" runat="server" Text="Package" style="display:none;"></asp:Label>
        
         </div>
    <div class="col-md-3"> 
    <asp:UpdatePanel ID="UP_ddlPackages" runat="server">
  <ContentTemplate>
    <asp:DropDownList ID="ddlPackages" runat="server" CssClass="form-control" style="display:none;" OnSelectedIndexChanged="ddlPackages_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
     </ContentTemplate>
  <Triggers>
  <asp:PostBackTrigger ControlID="ddlPackages" />
  </Triggers>
  </asp:UpdatePanel>
    
    </div>
    
    
     <div class="col-md-2">
    
    <asp:Label ID="lblcopyAgent" runat="server" Text="Copy to Agent:" style="display:none;"></asp:Label> </div>


  <div class="col-md-3"> <asp:UpdatePanel ID="UP_ddlAgentCopy" runat="server">
    <ContentTemplate>
    <asp:DropDownList ID="ddlAgentCopy" runat="server" CssClass="form-control" style="display:none;" OnSelectedIndexChanged="ddlAgentCopy_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
    </ContentTemplate>
    <Triggers>
    <asp:PostBackTrigger ControlID="ddlAgentCopy" />
    </Triggers>
    </asp:UpdatePanel></div>
  
  
  

    <div class="clearfix"></div>
    </div>
    
    
    
    
     <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblPackageName" runat="server" Text="Package Name:" > </asp:Label><sup>*</sup></div>
    <div class="col-md-3"> <asp:TextBox  ID="txtPackage" CssClass="form-control" runat="server"></asp:TextBox></div>
    <div class="col-md-2"> <asp:Label ID="lblNights" runat="server" Text="Nights:"> </asp:Label></div>
   
     <div class="col-md-3">
     <asp:UpdatePanel ID="UP_ddlNights" runat="server">
    <ContentTemplate>
    <asp:DropDownList ID="ddlNights"  CssClass="inputDdlEnabled form-control" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlNights_SelectedIndexChanged">
             <asp:ListItem Selected="True" Value="-1" Text="All"></asp:ListItem>
            </asp:DropDownList> 
    </ContentTemplate>
    <Triggers>
    <asp:PostBackTrigger ControlID="ddlNights" />
    </Triggers>
    </asp:UpdatePanel>
     
     
      </div>
     
     



    <div class="clearfix"></div>
    </div>
    
    
    
      
      
      
     <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblCity" runat="server" Text="City:"></asp:Label></div>
    
    <div class="col-md-3"><asp:TextBox  ID="txtCity"  CssClass="form-control" runat="server"></asp:TextBox> </div>
    
    <div class="col-md-2"> <asp:Label ID="lblCountry"  runat="server" Text="Country:"></asp:Label></div>
    
     <div class="col-md-3"><asp:TextBox  ID="txtCountry"  CssClass="form-control" runat="server"></asp:TextBox> </div>

    <div class="clearfix"></div>
    </div>


   <div class="col-md-12 marbot_10"> <asp:Label ID="lblTest" runat="server" Width="120px"></asp:Label> </div>        

   
   
   
     <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <b>Upload Main Image (624x270):</b><sup>*</sup></div>
    <div class="col-md-3"> 
    
    <asp:UpdatePanel ID="UpdatePanel1_fuImage1" runat="server">
       <ContentTemplate>
    <asp:FileUpload ID="fuImage1" runat="server" Width="200px"/>   
       </ContentTemplate>
       <Triggers>
       <asp:PostBackTrigger ControlID="btnUpload1" />
       </Triggers>
       </asp:UpdatePanel>
       
       
       
       </div>
       
       
    <div class="col-md-2"> 
    <asp:Button ID="btnUpload1" CssClass="btn bor_gray" runat="server" Text="Upload" 
           onclick="btnUpload1_Click" />
    
    </div>
    
    
     <div class="col-md-3"><asp:UpdatePanel ID="UP_lbUploadMsg" runat="server">
       <ContentTemplate>
   <asp:LinkButton ID="lbUploadMsg"   runat="server" OnClick="lbUploadMsg_Click"></asp:LinkButton>
    </ContentTemplate>
       <Triggers>
       <asp:PostBackTrigger ControlID="lbUploadMsg" />
       </Triggers>
       </asp:UpdatePanel> </div>
       
       
       
     <div class="col-md-2"> 
   <asp:Label ID="lblUpload1" runat="server" Visible="false"></asp:Label>
       <div id="dUpload1Msg"  style="display:none;" >
   <img  id="imgupload1" height="50px" width="50px" />
   </div></div>

    <div class="clearfix"></div>
    </div>









  <div class="col-md-12 padding-0 marbot_10">   
  
  
  
  
  <div id="pnlImages" runat="server" style="display: none; position:absolute; top:370px; z-index:9999; left:58%; top:-61px">
                                <%--<asp:Panel id="pnlImages" runat="server"  style="position:fixed;right:250px;display:none" >--%>
                                           <div class="thepet pdac ">
                                                <table border="0" style="color:Blue" ><tr>
                                                
                                                <td>
                                             <div class="ns-h3"> Preview</div> 
                                                
                                                
                                                </td>
                                                
                                                
                                                </tr>
                                            <tr><td>
                                                        <asp:Image  runat="server"  style="height:100px; width:150px;display:block" id="img1PreView"  />
                                                         <asp:Image  runat="server" style="height:100px; width:150px;display:block" id="img4PreView"  />
                                                        <asp:Image  runat="server" style="height:100px; width:150px;display:block" id="img5PreView"  />
                                                        <asp:Image  runat="server" style="height:100px; width:150px;display:block" id="img2PreView"  />
                                                        <asp:Image  runat="server" style="100px; width:150px;display:block" id="img3PreView"  />
                                                       
                                                        </td>
                                                        </tr>
                                                        <tr><td>
                                                        <asp:UpdatePanel ID="UP_btnClose" runat="server">
                                                        <ContentTemplate>
                                                        <asp:Button OnClientClick="showImage('H','')" runat="server" id="btnClose" Text="Close" CssClass="button" /> 
                                                        </ContentTemplate>
                                                        <Triggers>
                                                        <asp:PostBackTrigger ControlID="btnClose" />
                                                        </Triggers>
                                                        </asp:UpdatePanel>
                                                         
                                                         </td>
                                                         </tr>
                                                </table>
                                            </div>
                                            <%--</asp:Panel>--%>
                                            </div>                                   
    <div class="col-md-2"> <b>Upload Main Image1 (624x270):</b><sup>*</sup></div>
    <div class="col-md-3"><asp:UpdatePanel ID="UpdatePanel1_MainImage1" runat="server">
       <ContentTemplate>
    <asp:FileUpload ID="mainImage1" runat="server" Width="200px"/>   
       </ContentTemplate>
       <Triggers>
       <asp:PostBackTrigger ControlID="btnMainImg1" />
       </Triggers>
       </asp:UpdatePanel> </div>
    <div class="col-md-2"><asp:Button ID="btnMainImg1" CssClass="btn bor_gray" runat="server" Text="Upload" 
           onclick="btnMainImg1_Click" /> </div>
           
     <div class="col-md-3"><asp:UpdatePanel ID="UP_lbUploadMainImg1" runat="server">
       <ContentTemplate>
   <asp:LinkButton ID="lbUploadMainImg1"   runat="server" OnClick="lbUploadMainImg1_Click"></asp:LinkButton>
    </ContentTemplate>
       <Triggers>
       <asp:PostBackTrigger ControlID="lbUploadMainImg1" />
       </Triggers>
       </asp:UpdatePanel> </div>
       
       
        <div class="col-md-2"> <asp:Label ID="lblUploadMainImg1" runat="server" Visible="false"></asp:Label> </div>
      
       <div id="dUpload1MsgMain1"  style="display:none;" >
   <img  id="imguploadMainImg1" height="50px" width="50px" /> </div>
        
        

    <div class="clearfix"></div>
    </div>
    
    
        <div class="col-md-12 padding-0 marbot_10">                                      
   
   
    <div class="col-md-2">  <b>Upload Main Image2 (624x270):</b><sup>*</sup></div>
    <div class="col-md-3"> <asp:UpdatePanel ID="UpdatePanel1_MainImage2" runat="server">
       <ContentTemplate>
    <asp:FileUpload ID="mainImage2" runat="server" Width="200px"/>   
       </ContentTemplate>
       <Triggers>
       <asp:PostBackTrigger ControlID="btnMainImg2" />
       </Triggers>
       </asp:UpdatePanel></div>
       
    <div class="col-md-2"><asp:Button ID="btnMainImg2" CssClass="btn bor_gray" runat="server" Text="Upload" 
           onclick="btnMainImg2_Click" /> </div>
           
     <div class="col-md-3"> <asp:UpdatePanel ID="UP_lbUploadMainImg2" runat="server">
       <ContentTemplate>
   <asp:LinkButton ID="lbUploadMainImg2"   runat="server" OnClick="lbUploadMainImg2_Click"></asp:LinkButton>
    </ContentTemplate>
       <Triggers>
       <asp:PostBackTrigger ControlID="lbUploadMainImg2" />
       </Triggers>
       </asp:UpdatePanel></div>
   
     <div class="col-md-2"><asp:Label ID="lblUploadMainImg2" runat="server" Visible="false"></asp:Label> </div>
       <div id="dUpload1MsgMain2"  style="display:none;" >
   <img  id="imguploadMainImg2" height="50px" width="50px" /> </div>
    
    
    <div class="clearfix"></div>
    </div>


   
        <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"><b>Upload Side Image (146x123):</b><sup>*</sup> </div>
    <div class="col-md-3"> <asp:UpdatePanel ID="UpdatePanel2_fuImage2" runat="server">
       <ContentTemplate>
    <asp:FileUpload ID="fuImage2" runat="server" Width="200px"/>
    </ContentTemplate>
       <Triggers>
       <asp:PostBackTrigger ControlID="btnUpload2" />
       </Triggers>
       </asp:UpdatePanel></div>
    <div class="col-md-2"> <asp:Button ID="btnUpload2" CssClass="btn bor_gray" runat="server" Text="Upload" 
           onclick="btnUpload2_Click" /></div>
           
     <div class="col-md-3"> <asp:UpdatePanel ID="UP_lbUpload2Msg" runat="server">
       <ContentTemplate>
   <asp:LinkButton ID="lbUpload2Msg"  runat="server" OnClick="lbUpload2Msg_Click" ></asp:LinkButton>
   </ContentTemplate>
       <Triggers>
       <asp:PostBackTrigger ControlID="lbUpload2Msg" />
       </Triggers>
       </asp:UpdatePanel></div>
       
       
     <div class="col-md-2">   <asp:Label ID="lblUplaod2" runat="server" Visible="false" ></asp:Label> </div>
     
     
    <div class="col-md-3">  <div id="dUpload2Msg"  style="display:none;" >
   <img alt="" id="imgupload2" height="50px" width="50px" /> </div></div>
   
   
    <div class="clearfix"></div>
    </div>

   
   
   
       <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <b> Thumbnail (86x70):</b><sup>*</sup></div>
    <div class="col-md-3"><asp:UpdatePanel ID="UpdatePanel_fuImage3" runat="server">
       <ContentTemplate>
    <asp:FileUpload ID="fuImage3" runat="server" Width="200px"/>
    </ContentTemplate>
     <Triggers>
       <asp:PostBackTrigger ControlID="btnUplaod3" />
       </Triggers>
       </asp:UpdatePanel> </div>
    <div class="col-md-2"><asp:Button  CssClass="btn bor_gray" ID="btnUplaod3" runat="server" Text="Upload" 
           onclick="btnUpload3_Click" /> </div>
     <div class="col-md-3"> <asp:UpdatePanel ID="UP_lbUpload3Msg" runat="server">
   <ContentTemplate>
   <asp:LinkButton ID="lbUpload3Msg"  runat="server" OnClick="lbUpload3Msg_Click"></asp:LinkButton>
   </ContentTemplate>
   <Triggers>
   <asp:PostBackTrigger ControlID="lbUpload3Msg" />
   </Triggers>
   </asp:UpdatePanel></div>
     <div class="col-md-2">     
   <asp:Label ID="lblUpload3" runat="server" Visible="false"></asp:Label>
   <div id="dUpload3Msg"  style="display:none;" >
   <img id="imgupload3" height="50px" width="50px" /></div>
    
<div class="clearfix"></div>
    </div> 
        

   
   

    
    
         <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"><asp:Label ID="lblPackageIncludes" Font-Bold="true" runat="server" Text="Package Includes:"></asp:Label><sup>*</sup> </div>
    <div class="col-md-10"> <asp:CheckBoxList ID="chkPackageIncludes" runat="server" RepeatDirection="Horizontal" Width="500px">
   <asp:ListItem Text="AirFare" Value="AirFare"></asp:ListItem>
   <asp:ListItem Text="Hotel" Value="Hotel"></asp:ListItem>
   <asp:ListItem Text="SightSeeing" Value="SightSeeing"></asp:ListItem>
   <asp:ListItem Text="Meals" Value="Meals"></asp:ListItem>
   <asp:ListItem Text="Transport" Value="Transport"></asp:ListItem>
   </asp:CheckBoxList></div>

    
<div class="clearfix"></div>
    </div>
    
    
   
   
        <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblTheme" Font-Bold="true" runat="server" Text="Select Theme:"></asp:Label></div>
    <div class="col-md-10"><asp:CheckBoxList ID="chkTheme" runat="server" RepeatDirection="Horizontal" Width="400px"></asp:CheckBoxList> </div>

    
<div class="clearfix"></div>
    </div>
    
     
    
   
   
   
  
  <div class="clearfix"></div> 
</div> 

   
   
   
        <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"><asp:Label ID="lblDescription" Font-Bold="true" runat="server" Text="Description:"></asp:Label> </div>
    <div class="col-md-5"><asp:TextBox  ID="txtDescription"  CssClass=" form-control" runat="server"></asp:TextBox> </div>

    
<div class="clearfix"></div>
    </div>
    
    
   
   
        <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"><asp:Label ID="lblOverView" runat="server" Font-Bold="true" Text="OverView:"></asp:Label> </div>
 
    <div class="col-md-8"> <asp:TextBox  ID="txtOverView"  CssClass="" runat="server" Width="100%" TextMode="MultiLine" Height="100"></asp:TextBox></div>

    
<div class="clearfix"></div>
    </div>
    
    
    
    
         <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"> <asp:Label ID="lblTourType" Font-Bold="true" runat="server" Text="Tour Type:"></asp:Label><sup>*</sup></div>
    <div class="col-md-3"> <asp:DropDownList ID="ddlTourType" runat="server"  CssClass="inputDdlEnabled form-control" >
   <asp:ListItem Text="Select Tour Type" Value="S"></asp:ListItem>
   <asp:ListItem Text="Inbound Tour" Value="I"></asp:ListItem>
   <asp:ListItem Text="Outbound Tour" Value="O"></asp:ListItem>
   <asp:ListItem Text="Piligrim Tour" Value="P"></asp:ListItem>
   </asp:DropDownList></div>

    
<div class="clearfix"></div>
    </div>
    
    
    

   
   
   
   
      <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-2"><asp:Label ID="lblRoomRates" Font-Bold="true" runat="server" Text="Room Rates:"></asp:Label> </div>
    <div class="col-md-8"><asp:TextBox  ID="txtRoomRates"  CssClass="inp_22_1" runat="server" Width="100%" TextMode="MultiLine" Height="100"></asp:TextBox> </div>


<div class="clearfix"></div>
    </div>
    
    
    
          <div class="col-md-12 padding-0 marbot_10">                                      
   
 <div class="col-md-4"> 
 <table> 
 <tr> 
 <td> <asp:Label ID="lblCurrency" runat="server" Font-Bold="true" Text="Currency:"></asp:Label></td>
 
 <td><asp:DropDownList ID="ddlSupplierCurrency" Enabled="false" CssClass="inputDdlEnabled form-control" Width="100px" runat="server">
   
   </asp:DropDownList> </td>
 
 
 </tr>
 
 
 </table>
 
 
 
 </div>
 
 
    <div class="col-md-4">
       <table> 
        <tr> 
        <td><asp:Label ID="lblStartFrom" runat="server" Font-Bold="true" Text="Starting From:"></asp:Label> </td>
        
         <td><asp:TextBox  ID="txtStartFrom"  CssClass="form-control" runat="server" Width="100px" placeholder="0.0" onkeypress="return isNumber(event);"></asp:TextBox> </td>
</tr>
        
        
        </table>
    
    
     </div>


<div class="clearfix"></div>
    </div>
   
   
    
    
       <div>
   
   <table width="100%" border="0" cellpadding="0" cellspacing="0">
   <tr>
<td style="width: 10px"></td>
    <td colspan="5" style="height: 21px" width="700px">  
   <asp:HiddenField  runat="server" ID="hdfinclCounter" Value="2"/>
   <asp:HiddenField  runat="server" ID="hdfExclCounter" Value="2"/>
   <asp:HiddenField  runat="server" ID="hdfpriceExcludeCounter" Value="2"/>
   <asp:HiddenField  runat="server" ID="hdftncCounter" Value="2"/>
   
   
   <asp:HiddenField  runat="server" ID="hdfinclusion" Value=""/>
   <asp:HiddenField  runat="server" ID="hdfExclusions" Value=""/>
   <asp:HiddenField  runat="server" ID="hdfpriceExclude" Value=""/>
   <asp:HiddenField  runat="server" ID="hdftnc" Value=""/>
           
           
            <div style="display:none;">
                <input type="hidden" id="InclusionsCounter" name="InclusionsCounter"   value="<%=hotelVariables["inclusionsCount"] %>" />               
                <input type="hidden" id="InclusionsCurrent" name="InclusionsCurrent" value="<%=hotelVariables["inclusionsCount"] %>" />
              </div>
            
              <div class="fleft " style="border:solid 0px #ccc; width:700px; margin-top:25px; padding:10px 10px 10px 10px;">
                  <div id="InclusionsDiv">
                  <table>
                  <tr>
                  <td width="93px">
                  <b class="fleft">Inclusions :<sup>*</sup></b>
                  </td>
                  <td width="500px">
                  <%for (int i = 1; i < Convert.ToInt32(hotelVariables["inclusionsCount"]); i++)
                        {
                            if (i == 1)
                            { %>
                      <p class="fleft">
                        <span class="fleft"><input type="text" style="width:450px;"  id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" class="width-300 fleft" value="<%=hotelVariablesArray["inclusionsValue"][i - 1] %>" /></span>
                                     
                        
                      </p>
                      <%}
                        else
                        { %>
                        <%if (i < Convert.ToInt32(hotelVariables["inclusionsCount"]) - 1)
                          { %>
                    <p class="fleft width-100 padding-top-5" style="padding-left:160px" id="Inclusions-<%=i %>">
	                    <%--<span class="fleft"><input id="Text1" name="InclusionsText-<%=i %>" value='<%=Page.Request["InclusionsText-"+i] %>' class="width-300 fleft" type="text" value="<%=activityVariablesArray["inclusionsValue"][i - 1] %>" /></span>--%>
	                    <span class="fleft width-300""><input id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" value="<%=hotelVariablesArray["inclusionsValue"][i - 1] %>" class="width-300 fleft" type="text"  /></span>
	                    <i style="display: none;" class="fleft margin-left-10" id="InclusionsImg-<%=i %>"><img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Inclusions-<%=i %>','text')" /></i>
                    </p>
                          <%}
                            else
                            { %>
                    <p class="fleft width-100 padding-top-5" style="padding-left:160px"  id="Inclusions-<%=i %>">
	                    <%--<span class="fleft width-300"><input id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" value='<%=Page.Request["InclusionsText-"+i] %>' class="width-300 fleft" type="text" value="<%=activityVariablesArray["inclusionsValue"][i - 1] %>" /></span>--%>
	                    <span class="fleft width-300"><input id="InclusionsText-<%=i %>" name="InclusionsText-<%=i %>" value="<%=hotelVariablesArray["inclusionsValue"][i - 1] %>" class="width-300 fleft" type="text"  /></span>
	                    <i class="fleft margin-left-10" id="InclusionsImg-<%=i %>"><img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Inclusions-<%=i %>','text')" /></i>
</p>
                          <%} %>
                            <%} %>
                      <%} %>
                  </td>
                  
                      </tr>
                  </table>
                      
                  </div>
                 <table>
                    <tr>
                    <td>
                    
                    </td>
                    <td>
                  <div class="fleft" style="width:100px; text-align:right">
                   <p class="fleft width-100 padding-top-5" style="padding-left: 160px">
                   <span class="fleft"> 
                   <input type="button" value="add More" class="btn bor_gray"  onclick="javascript:AddTextBox('Inclusions')" />
                   </span>
                   </p>
                </div>
                  </td>
                    </tr>
                    </table>  
                
              </div>
        </td>           
   </tr>
   </table>
   <table id="tblitinerary" runat="server">
                    </table>
   
   
   <table width="100%">
     <tr>
      <td></td>
        <td colspan="5" style="height: 21px;" width="700px">        
        <div style="display:none;">
                <input type="hidden" id="ExclusionsCounter" value="<%=hotelVariables["exclusionsCount"] %>" />
                <input type="hidden" id="ExclusionsCurrent" name="ExclusionsCurrent" value="<%=hotelVariables["exclusionsCount"] %>" />
              </div>
              <div class="fleft " style="border:solid 0px #ccc; width:620px; margin-top:25px; padding:10px 10px 10px 10px;">
                  <div id="ExclusionsDiv">
                  <table>
                  <tr>
                  <td width="93px">
                  <b class="fleft" style="width:160px">Exclusions:<sup>*</sup></b>
                  </td>
                  <td width="500px">
                  <%for (int i = 1; i < Convert.ToInt32(hotelVariables["exclusionsCount"]); i++)
                        {
                            if (i == 1)
                            { %>
                      <p class="fleft">
                       <%-- <span style=" width:470px" class="fleft"><input style="width:460px" type="text" id="ExclusionsText-<%=i %>" value='<%=Page.Request["ExclusionsText-"+i] %>' name="ExclusionsText-<%=i %>" class="width-300 fleft" value="<%=activityVariablesArray["exclusionsValue"][i - 1] %>" /></span>--%>
                        <span class="fleft"><input style="width:450px;" type="text" id="ExclusionsText-<%=i %>"  name="ExclusionsText-<%=i %>" class="width-300 fleft" value="<%=hotelVariablesArray["exclusionsValue"][i - 1] %>" /></span>
                      </p>
                      <%}
                        else
                        { %>
                        <%if (i < Convert.ToInt32(hotelVariables["exclusionsCount"]) - 1)
                          { %>
                    <p class="fleft width-100 padding-top-5" style="padding-left:160px" id="Exclusions-<%=i %>">
	                    <span class="fleft"><input id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>"  class="width-300 fleft" type="text" value="<%=hotelVariablesArray["exclusionsValue"][i - 1] %>" /></span>
	                    <%--<span class="fleft width-300""><input id="Text1" name="ExclusionsText-<%=i %>" value='<%=Page.Request["ExclusionsText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
	                    <i style="display: none;" class="fleft margin-left-10" id="ExclusionsImg-<%=i %>"><img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Exclusions-<%=i %>','text')" /></i>
                    </p>
                          <%}
                            else
                            { %>
                    <p class="fleft width-100 padding-top-5" style="padding-left: 160px" id="Exclusions-<%=i %>">
	                    <span class="fleft width-300"><input id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>"   class="width-300 fleft" type="text" value="<%=hotelVariablesArray["exclusionsValue"][i - 1] %>" /></span>
	                    <%--<span class="fleft width-300"><input id="ExclusionsText-<%=i %>" name="ExclusionsText-<%=i %>"  value='<%=Page.Request["ExclusionsText-"+i] %>' class="width-300 fleft" type="text"  /></span>--%>
	                    <i class="fleft margin-left-10" id="ExclusionsImg-<%=i %>"><img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('Exclusions-<%=i %>','text')" /></i>
                    </p>
                          <%} %>
                            <%} %>
                      <%} %>
                  </td>
                  
                  </tr>
                  </table>
                   
                  </div>
                 <table>
                    <tr>
                    <td>
                    
                    </td>
                    <td>
                  <div class="fleft" style="width:100px; text-align:right">
                   <p class="fleft width-100 padding-top-5" style="padding-left: 160px"><span class="fleft"> <input class="btn bor_gray" type="button" value="add More"  onclick="javascript:AddTextBox('Exclusions')" /></span></p>
                </div>
                  </td>
                    </tr>
                    </table>  
                      
                
              </div>
        
     </td>
 
   </tr> 
     <tr>
<td></td>
    <td colspan="5" style="height:21px; width:700px;">
    <div style="display:none;">
                <input type="hidden" id="PriceIncludeCounter" value="<%=Convert.ToInt32(hotelVariables["priceExcludeCount"])%>" />
                <input type="hidden" id="PriceIncludeCurrent" name="PriceIncludeCurrent" value="<%=Convert.ToInt32(hotelVariables["priceExcludeCount"])%>" />
              </div>
        <div class="fleft " style="border:solid 0px #aaa; width:620px; margin-top:25px; padding:10px 10px 10px 10px;">
                  <div id="PriceIncludeDiv">
                  <table>
                  <tr>
                  <td width="93px">
                  <b class="fleft" style="width:160px;">Price does not include :</b>
                  </td>
                  <td width="500px">
                  <%for (int i = 1; i < Convert.ToInt32(hotelVariables["priceExcludeCount"]); i++)
                        {
                            if (i == 1)
                            { %>
                      <p class="fleft">
                        <span  class="fleft"><input style=" width:460px" type="text" id="PriceIncludeText-<%=i %>" name="PriceIncludeText-<%=i %>" class="width-300 fleft" value="<%=hotelVariablesArray["priceExcludeValue"][i - 1] %>" /></span>
                      </p>
                      <%}
                        else
                        { %>
                        <%if (i < Convert.ToInt32(hotelVariables["priceExcludeCount"]) - 1)
                          { %>
                    <p class="fleft width-100 padding-top-5" style="padding-left: 160px;" id="PriceInclude-<%=i %>">
	                    <span class="fleft width-300"><input id="PriceIncludeText-<%=i %>" name="PriceIncludeText-<%=i %>" class="width-300 fleft" type="text" value="<%=hotelVariablesArray["priceExcludeValue"][i - 1] %>" /></span>
	                    <i style="display: none;" class="fleft margin-left-10" id="PriceIncludeImg-<%=i %>"><img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('PriceInclude-<%=i %>','text')" /></i>
                    </p>
                          <%}
                            else
                            { %>
                    <p class="fleft width-100 padding-top-5" style="padding-left: 160px;" id="PriceInclude-<%=i %>">
	                    <span class="fleft width-300"><input id="PriceIncludeText-<%=i %>" name="PriceIncludeText-<%=i %>" class="width-300 fleft" type="text" value="<%=hotelVariablesArray["priceExcludeValue"][i - 1] %>" /></span>
	                    <i class="fleft margin-left-10" id="PriceIncludeImg-<%=i %>"><img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('PriceInclude-<%=i %>','text')" /></i>
</p>
                          <%} %>
                            <%} %>
                      <%} %>
                  </td>
                  
                  </tr>
                  </table>
                      
                      
                  </div>
                <table>
                    <tr>
                    <td>
                    
                    </td>
                    <td>
                  <div class="fleft" style="width:100px; text-align:right">
                   <p class="fleft width-100 padding-top-5" style="padding-left: 160px">
                   <span class="fleft"> 
                   <input type="button" value="add More" class="btn bor_gray"  onclick="javascript:AddTextBox('PriceInclude')" />
                   </span>
                   </p>
                </div>
                  </td>
                    </tr>
                    </table>  
              </div>
    </td>
    </tr>
    <tr>
<td></td>
    <td colspan="5" style="height: 21px; width:700px;"> 
    <div style="display:none;">
                <input type="hidden" id="T&CCounter" value="<%=Convert.ToInt32(hotelVariables["tncCount"])%>" />
                <input type="hidden" id="T&CCurrent" name="T&CCurrent" value="<%=Convert.ToInt32(hotelVariables["tncCount"])%>" />
              </div>
        <div class="fleft " style="width:100%; width:620px; border:solid 0px #aaa; margin-top:25px; padding:10px 10px 10px 10px;">
                  <div id="T&CDiv">
                  <table>
                  <tr>
                  <td width="93px">
                  <b class="fleft" style="width:160px;">Terms and Conditions<sup>*</sup> :</b>
                  </td>
                  <td width="500px">
                  <%for (int i = 1; i < Convert.ToInt32(hotelVariables["tncCount"]); i++)
                        {
                            if (i == 1)
                            { %>
                      <p class="fleft">
                        <span class="fleft"><input style=" width:460px" type="text" id="T&CText-<%=i %>" name="T&CText-<%=i %>" class="width-300 fleft" value="<%=hotelVariablesArray["tncValue"][i - 1] %>" /></span>
                      </p>
                      <%}
                        else
                        { %>
                        <%if (i < Convert.ToInt32(hotelVariables["tncCount"]) - 1)
                          { %>
                    <p class="fleft width-100 padding-top-5" style="padding-left: 160px;" id="T&C-<%=i %>">
	                    <span class="fleft width-300"><input id="T&CText-<%=i %>" name="T&CText-<%=i %>" class="width-300 fleft" type="text" value="<%=hotelVariablesArray["tncValue"][i - 1] %>" /></span>
	                    <i style="display: none;" class="fleft margin-left-10" id="T&CImg-<%=i %>"><img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('T&C-<%=i %>','text')" /></i>
                    </p>
                          <%}
                            else
                            { %>
                    <p class="fleft width-100 padding-top-5" style="padding-left: 160px;" id="T&C-<%=i %>">
	                    <span class="fleft width-300"><input id="T&CText-<%=i %>" name="T&CText-<%=i %>" class="width-300 fleft" type="text" value="<%=hotelVariablesArray["tncValue"][i - 1] %>" /></span>
	                    <i class="fleft margin-left-10" id="T&CImg-<%=i %>"><img src="Images/delete.gif" alt="Remove" onclick="javascript:Remove('T&C-<%=i %>','text')" /></i>
</p>
                          <%} %>
                            <%} %>
                      <%} %>
                  </td>
                 
                  </tr>
                  </table>
                      
                      
                  </div>
                 <table>
                    <tr>
                    <td>
                    
                    </td>
                    <td>
                  <div class="fleft" style="width:100px; text-align:right">
                   <p class="fleft width-100 padding-top-5" style="padding-left: 160px">
                   <span class="fleft"> 
                   <input type="button" value="add More" class="btn bor_gray" onclick="javascript:AddTextBox('T&C')" />
                   </span>
                   </p>
                </div>
                  </td>
                    </tr>
                    </table>  
              </div>
    </td>
    </tr>
    <tr>
    <td></td>
    <td>
    <asp:UpdatePanel ID="UP_btnSave" runat="server">
    <ContentTemplate>
    <asp:Button ID="btnSave" CssClass="btn but_b pull-right" runat="server" Text="Save Deal" OnClientClick="return Save();"  OnClick="btnSave_Click"/>
    </ContentTemplate>
    <Triggers>
    <asp:PostBackTrigger ControlID="btnSave" />
    </Triggers>
    </asp:UpdatePanel>
    
                                            
                                            </td>
    </tr>
    
    </table>
</div>
  

  
   </div>
   
   <div class="clearfix"></div>
   
   </div> 
   



   </td>
   </tr>
   </table>
  </td>
   </tr>
   </table>
  
   
   
   



</asp:Content>

