﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ActivityVoucherGUI" Title="Activity Voucher" Codebehind="ActivityVoucher.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
    <script>
        function printDiv(divName) {
            var printContents = document.getElementById(divName).innerHTML;
            var originalContents = document.body.innerHTML;

            document.body.innerHTML = printContents;

            window.print();

            document.body.innerHTML = originalContents;
        }
    </script>


    <div id="printableArea" runat="server">
                <table width="100%" cellspacing="0" cellpadding="0" border="0" bgcolor="#FFFFFF">
                    <tbody>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                    <tbody>
                                        <tr>
                                            <td width="21%">
                                                <%--<img width="200" height="71" src="<%=Request.Url.Scheme%>://cozmotravel.com/images/packageVoucherlogo.gif">--%>
                                                <asp:Image ID="imgHeaderLogo" runat="server" Width="200px" Height="60px" AlternateText="AgentLogo" ImageUrl="images/header_cozmovisa.jpg" />
                                            </td>
                                            <td width="79%" align="right">
                                                <div style="float: right;">
                                                    <a href="#" onclick="printDiv('<%=printableArea.ClientID %>')">Print &nbsp;
                                                        <img align="absmiddle" style="padding-left: 2;" src="<%=Request.Url.Scheme%>://cozmotravel.com/Images/print_blue.gif" title="Print"
                                                            alt="print"></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td height="25" class="themecol1">
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Voucher</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse" class="pd15 padd10">
                                    <tbody>
                                        <tr>
                                            <td  width="50%" height="25">
                                                <strong>Date of Issue:</strong>
                                                <asp:Label ID="lblBookingDate" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td  width="50%">
                                                <strong>Passenger(s): </strong>
                                                <asp:Label ID="lblPassengers" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="25">
                                                <strong>Booking Ref:</strong>
                                                <asp:Label ID="lblBookingRef" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Lead Passenger: </strong>
                                                <asp:Label ID="lblLeadPassenger" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="25" class="themecol1">
                                <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                    Guest Details</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <table width="100%" cellspacing="0" cellpadding="0" border="1" style="margin: auto;
                                    border: solid 1px; border-collapse: collapse" class="pd15 padd10">
                                    <tbody>
                                        <tr>
                                            <td width="50%" height="20" valign="top">
                                                <strong>Guest Name :</strong> 
                                                <asp:Label ID="lblGuestName" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td width="50%">
                                                <strong>Tour Name :</strong> 
                                                <asp:Label ID="lblActivityName" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="top">
                                                <strong>Nationality : </strong>
                                                <asp:Label ID="lblNationality" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Departure Date :</strong> 
                                                <asp:Label ID="lblTransactionDate" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="top">
                                                <strong>No of Adults </strong>: 
                                                <asp:Label ID="lblAdultCount" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Duration :</strong> 
                                                <asp:Label ID="lblDuration" runat="server" Text=""></asp:Label> &nbsp;hours
                                            </td>
                                        </tr>
                                        <tr>
                                            <td height="20" valign="top">
                                                <strong>No of Children</strong> : 
                                                <asp:Label ID="lblChildCount" runat="server" Text=""></asp:Label>
                                            </td>
                                           <%-- <td style="display:none;">
                                                <strong>Car/Bus Type :</strong> eded
                                                 <strong>Entrance Fees :</strong> AED 
                                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                            </td>--%>
                                              <td style="height: 20px">
                                                <strong>Entrance Fees :</strong> 
                                                <asp:Label ID="lblTotal" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr style="display:none;">
                                            <td height="20" valign="top">
                                                <strong>Hotel :</strong> Al Wahrami
                                            </td>
                                            <td>
                                                <strong>Guide Language :</strong> Arabic
                                            </td>
                                        </tr>
                                        <tr>
                                              <td height="20" valign="top">
                                                <strong>No of Infants</strong> : 
                                                <asp:Label ID="lblInfantCount" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Pick up point :</strong> 
                                                <asp:Label ID="lblPickupPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                        <td height="20" valign="top">
                                                <strong>Country :</strong> 
                                                <asp:Label ID="lblCountry" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Pick up Time :</strong> 
                                                <asp:Label ID="lblPickupTime" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                         <td height="20" valign="top">
                                                <strong>City :</strong> 
                                                <asp:Label ID="lblCity" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td>
                                                <strong>Drop Off point :</strong> 
                                                <asp:Label ID="lblDropPoint" runat="server" Text=""></asp:Label>
                                            </td>
                                        </tr>
                                        <tr>
                                          <td height="20" valign="top">
                                                <strong>Address :</strong> 
                                                <asp:Label ID="lblLocation" runat="server" Text=""></asp:Label>
                                            </td>
                                            <td valign="top" style="height: 20px">
                                                <strong>Tel number :</strong> 
                                                <asp:Label ID="lblPhone" runat="server" Text=""></asp:Label>
                                            </td>
                                          
                                        </tr>
                                        <tr>
                                            <td colspan="2">
                                                <div style="line-height: 25px; display:none;" >
                                                    <strong><span style="background: #FFFF00">Emergency Contact Details:</span></strong>
                                                    999999999</div>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>
                                            <table width="97%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td height="25" bgcolor="#03a7e6">
                                                        <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                                            Booked and Payable by
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="padd10" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td style="height: 20px">
                                                                    <strong>Cozmo Travel LLC </strong>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td>
                                            <table width="97%" border="0" align="right" cellpadding="0" cellspacing="0">
                                                <tr>
                                                    <td height="25" bgcolor="#03a7e6">
                                                        <label style="padding-left: 10px; color: #FFFFFF; font-size: 16px;">
                                                            Supplier / Driver contact if
                                                        </label>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td>
                                                        <table class="padd10" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td height="20">
                                                                    <strong>Contact no :</strong> 9312678087
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                           <td height="25" class="themecol1">
                                <label style="padding-left: 20px; color: #FFFFFF; font-size: 16px;">
                                    Terms &amp; Conditions</label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <%--<li style="margin-left: 30px; margin-bottom: 5px; margin-top: 5px;">Check in Counters
                                    open 3 hours prior to the scheduled flight departure time. Passenger must complete
                                    all check-in formalities 1 hour prior to the scheduled flight departure time. Check-in
                                    counters for all flights close 1 hour prior to the scheduled/revised flight departure
                                    time. Passenger(s) failing to check-in on time will not be accepted and they will
                                    be considered as no-shows, eligible to pay the applicable cancellation charges as
                                    a no-show passenger.</li>
                                <li style="margin-left: 30px; margin-bottom: 5px; margin-top: 5px;">Air Arabia does
                                    not accept children travelling unaccompanied under the age of 12 years. Infants
                                    under the age of 2 weeks are not accepted for travel.</li>
                                <li style="margin-left: 30px; margin-bottom: 5px; margin-top: 5px;">Free Baggage Allowance
                                    (FBA) per passenger (excluding infants) is 20-30 kgs per person depending upon the
                                    sector of travel (please check the FBA for your sector at the time of purchasing
                                    the package). Maximum weight permitted per individual piece of baggage is 32 kgs
                                    with total dimensions of 160 cms (W+D+L). Hand baggage per passenger should not
                                    exceed 7 kgs with dimensions within 55x40x20 cms.</li>
                                <li style="margin-left: 30px; margin-bottom: 5px; margin-top: 5px;">Items which you
                                    MUST NOT include food items, money, jewellery, medicine, business documents, passports
                                    or other identification documents and fragile or valuable items. Any liquid, aerosols,
                                    gels or pastes greater than 100 ml each must be packed in your check-in baggage.
                                    Any individual container over 100 ml in your hand baggage (even if there is less
                                    than 100 ml inside) will NOT be allowed on board. All such items must be carried
                                    in a separate clear plastic, zip top, re-sealable bag that is not bigger than 20x20
                                    cms or equivalent to 1 litre capacity.</li>
                                <li style="margin-left: 30px; margin-bottom: 5px; margin-top: 5px;">Modification or
                                    Cancellation of confirmed and paid booking will be subject to applicable charges
                                    as detailed in the Cancellation &amp; Amendment Policy document which you have read
                                    and signed at the time of payment of your holiday package.</li>
                                <li style="margin-left: 30px; margin-bottom: 5px; margin-top: 5px;">Passengers holding
                                    original visa do not require 'OK to Board' message. However, in case passenger is
                                    travelling from India, Pakistan, Bangladesh or Sri Lanka to UAE and holding a copy
                                    of UAE visa, the sponsor or representative of the passenger has to produce the original
                                    visa in person along with a fee of AED 10 at any of our sales shops in Sharjah or
                                    Dubai or at the Hala Service at Sharjah Airport between 0830 to 1500 hrs and it
                                    has to be done atleast 24 hours before departure of flight. These offices, after
                                    verifying with the authorities concerned, will authorize to accept the passenger
                                    by inserting an 'OK to Board' remark in the reservation. The failure to do so will
                                    lead in Air Arabia refusing to carry the passengers.</li>
                                <li style="margin-left: 30px; margin-bottom: 5px; margin-top: 5px;">Sharjah Airport
                                    has a charge of AED 50 for a wheelchair request for use at the airport. Please note
                                    that relevant wheelchair charges may apply to other cities based on the respective
                                    airport policy. For details, call your local Air Arabia office.</li>
                                <li style="margin-left: 30px; margin-bottom: 5px; margin-top: 5px;">By buying the ticket,
                                    the passenger confirms herewith that he/she has agreed to all the terms and conditions
                                    (without exception) as issued and amended by the carrier from time to time. In case
                                    of any dispute related to any/all of the services as provided by the Carrier and/or
                                    any of its authorized representatives, before, during, and/or after the provision
                                    of the service, such dispute shall be exclusively and solely raised, filed, submitted,
                                    registered and/ore presented in front of any of the legal courts operating in the
                                    Emirate of Sharjah in the United Arab Emirates.</li>
                                <li style="margin-left: 30px; margin-bottom: 5px; margin-top: 5px;">Common Queries:
                                    It is not necessary to reconfirm your flight provided you have a reservation confirmation.
                                    Food and drinks can be purchased on board at affordable prices during the flight.
                                    You are not permitted to carry food from outside on board the flight. For any enquiries,
                                    please call the number mentioned at the bottom of this document.</li>--%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                &nbsp;
                            </td>
                        </tr>
                        <tr>
                            <td height="40" bgcolor="" align="center">
                                <div style="border: solid 1px #ccc; padding: 10px; text-align: center">
                                    Cozmo Holidays Tel. No. :971 600524444 <br>
                                    Email : <a style="color: #ff7800; text-decoration: none;" href="mailto:holidaysproduct1@cozmotravel.com">
                                        holidaysproduct1@cozmotravel.com</a>
                                </div>
                            </td>
                        </tr>
                    </tbody>
                </table>
            
    </div>
    <%--</form>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

