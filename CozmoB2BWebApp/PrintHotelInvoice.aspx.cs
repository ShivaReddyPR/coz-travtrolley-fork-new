using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

public partial class PrintHotelInvoice :CT.Core.ParentPage// System.Web.UI.Page
{
    protected HotelRoom[] hRoomList = new HotelRoom[0];
    protected Invoice invoice = new Invoice();
    protected AgentMaster agency;
    protected string agencyAddress;
    protected RegCity agencyCity = new RegCity();
    protected UserMaster loggedMember = new UserMaster();
    protected int lineItemCount;
    protected int hotelId;
    protected int agencyId;
    protected int cityId = 0;
    protected string remarks = string.Empty;
    protected string confNo;
    protected string routing;
    protected string airlineCode = string.Empty;
    protected string plbString = string.Empty;
    protected HotelItinerary itinerary = new HotelItinerary();
    protected string billAddress;
    protected void Page_Load(object sender, EventArgs e)
    {
        loggedMember =  new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
        agency = new AgentMaster(Settings.LoginInfo.AgentId);
        # region Hotel Invoice

        if (Request["hotelId"] != null && Request["agencyId"] != null && Request["confNo"] != null)
        {
            hotelId = Convert.ToInt32(Request["hotelId"]);
            agencyId = Convert.ToInt32(Request["agencyId"]);
            confNo = Request["confNo"];
        }
        else
        {
            //TODO: Handle it
        }

        itinerary.Load(hotelId);
        HotelPassenger hPax = new HotelPassenger();
        hPax.Load(hotelId);
        itinerary.HotelPassenger = hPax;
        billAddress = Request["billingAddress"];
        BillingAddress.BorderStyle = BorderStyle.None;
        BillingAddress.ReadOnly = true;
        BillingAddress.Text = billAddress;
      
        if (Request["city"] != null)
        {
            routing = Request["city"];
        }

        if (Request["remarks"] != null)
        {
            remarks = Request["remarks"];
        }

        // Reading agency information.
        DataTable dtCities = CityMaster.GetList(agency.Country, ListStatus.Short, RecordStatus.Activated);
        DataRow[] cities = dtCities.Select("City_Name like '%" + agency.City + "%'");

        if (cities != null && cities.Length > 0)
        {
            cityId = Convert.ToInt32(cities[0]["city_id"]);
        }
        agency = new  AgentMaster(agencyId);
        if (agency.City.Length > 0)
        {
            agencyCity.CityName = agency.City;
        }
        else
        {
            agencyCity = RegCity.GetCity(cityId);
        }
        // Formatting agency address for display.
        agencyAddress = agency.Address;
        agencyAddress.Trim();
        if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
        {
            agencyAddress += ",";
        }
        //if (agency.Address.Line2 != null && agency.Address.Line2.Length > 0)
        //{
        //    agencyAddress += agency.Address.Line2;
        //}
        agencyAddress.Trim();
        if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
        {
            agencyAddress += ",";
        }

        // Getting ticket list from DB       
        HotelRoom hRoom = new HotelRoom();
        hRoomList = hRoom.Load(hotelId);
        //Price info


        int invoiceNumber = 0;
        // Generating invoice.
        if (hRoomList.Length > 0)
        {
            invoiceNumber = Invoice.isInvoiceGenerated(hRoomList[0].RoomId, ProductType.Hotel);
        }
        if (invoiceNumber > 0)
        {
            invoice = new Invoice();
            invoice.Load(invoiceNumber);
        }
        else
        {
            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(hotelId, string.Empty, (int)loggedMember.ID, ProductType.Hotel, 1);
            invoice.Load(invoiceNumber);
        }
        #endregion

    }
}
