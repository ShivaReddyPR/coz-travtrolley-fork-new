﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="ReligareDiscountMaster.aspx.cs" Inherits="CozmoB2BWebApp.ReligareDiscountMaster" %>
<%--<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" %>--%>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
 <%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
     
    <div><h3>Religare Discount Master</h3></div>
         <div class="body_container" style="height:100px">
        <div class="col-md-12 padding-0 marbot_10">


            <div class="col-md-2">
                <asp:Label ID="lblDiscountNumberId" runat="server" Text="Discount Number Id:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtDiscountNumberId" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>
            </div>

            <div class="col-md-2">
                <asp:Label ID="lblDiscountPercentage" runat="server" Text="Discount Percentage:" onKeyPress="return isNumber(event)"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtDiscountPercentage" runat="server" ></asp:TextBox>
            </div>

            <div class="col-md-2">
                <asp:Label ID="lblDiscountRemarks" runat="server" Text="Discount Remarks:"></asp:Label>
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtDiscountRemarks" runat="server"></asp:TextBox>
            </div>
            <div class="clearfix"></div>
            </div>
                   </div>
   

        <div class="col-md-12">
            <label style=" padding-right:5px" class="f_R"><asp:Button  ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();" CssClass="btn but_b"  OnClick ="btnSave_Click" ></asp:Button></label>
                 
                 
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b"   OnClick="btnClear_Click"></asp:Button></label>
                    
                    
                    <label style=" padding-right:5px" class="f_R"> <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="btn but_b"   OnClick="btnSearch_Click"></asp:Button></label>
            </div>

        
        <div class="col-md-10">
            <asp:Label ID="lblSuccessMsg" runat="server" Text=""></asp:Label>
            <asp:Label ID="lblError" runat="server" Text=""></asp:Label>
        </div>
    
    <script type="text/javascript">
        function Save() {
            if (getElement('txtDiscountNumberId').value == '') addMessage('Discount Member Id cannot be blank!', '');
            
            

        if (getMessage() != '') {
            //alert(getMessage());
            alert(getMessage()); clearMessage(); 
            return false;
           }
        }
        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57  || charCode==47) ) {
                return false;
            }
            return true;
        }
    </script>
    <asp:HiddenField runat="server" ID="hdfEMId" Value="0" />
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server"> 

     <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="DISCOUNT_ID"
      emptydatalist="No Location List!" AutoGenerateColumns="false" PageSize="15" GridLines="None"
     CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="5"
     CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">


     <Columns>
         <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
         <asp:TemplateField HeaderText ="SL.NO">
        <ItemTemplate>
             <%#Container.DataItemIndex+1 %>
        </ItemTemplate>
    </asp:TemplateField>
         <asp:TemplateField>
       
        <ItemTemplate >
    <asp:Label ID="ITlblDiscountPercentage" runat="server" Text='<%# Eval("DISCOUNT_PERCENTAGE") %>' CssClass="label grdof" ToolTip='<%# Eval("DISCOUNT_PERCENTAGE") %>' ></asp:Label>
   
    </ItemTemplate>  
             <HeaderTemplate>
                 Discount Percentage
             </HeaderTemplate>
         </asp:TemplateField>

         <asp:TemplateField>
        <ItemTemplate >
    <asp:Label ID="ITlblDiscountRemarks" runat="server" Text='<%# Eval("DICOUNT_REMARKS") %>' CssClass="label grdof" ToolTip='<%# Eval("DICOUNT_REMARKS") %>'></asp:Label>
   
    </ItemTemplate> 
             <HeaderTemplate>
                 Discount Remarks
             </HeaderTemplate>
         </asp:TemplateField>


     </Columns>

     </asp:GridView>
     

    </asp:Content>
