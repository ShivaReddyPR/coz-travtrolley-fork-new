using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.ComponentModel;

public partial class DateControl : System.Web.UI.UserControl
{
    #region Declaration
    string _businessName = "DatePicker";
    string _weekNumberDisplay = "true";
    string _allowDrag = "false";
    string _outOfMonthDisable = "false";
    string _outOfMonthHide = "false";
    string _dateInputSequence = "DMY";
    string _dateFormatString = "DD-MMM-YYYY";
    string _dateDelimiter = "-";

    char _timeDelimiter = ':';

    int _dateDelimiterCode = 45;
    int _timeDelimiterCode = 58;
    int _baseYearLimit = 20;
    int _dropDownYears = 30;
    int _weekStartDay = 0;
    int _weekNumberBaseDay = 0;
    int _calanderHorizontalAlignment = 0;
    int _calanderVerticalAlignment = 1;

    int _offsetLeftAlignment = 0;
    int _offsetTopAlignment = 0;

    double _universalTimeDiff = 0.0;

    bool _dateOnly = false;
    bool _timeOnly = false;
    DateTime _dateTime;
    IDDateFormat _dateFormat = IDDateFormat.DDMMMYYYY;
    IDTimeDelimiter _timeFormat = IDTimeDelimiter.Colon;
    #endregion

    #region Enum
    //public enum IDDateFormat
    //{
    //     Summary:
    //         Indicates DD-MM-YYYY.
    //    DDMMYYYY = 0,
        
    //     Summary:
    //         Indicates MM-DD-YYYY.
    //    MMDDYYYY = 1,
        
    //     Summary:
    //         Indicates DD-MMM-YYYY.
    //    DDMMMYYYY = 2,
        
    //     Summary:
    //         Indicates MMM-DD-YYYY.
    //    MMMDDYYYY = 3,

    //}
    public enum IDDateDelimiter
    {
        // Summary:
        //     Indicates '-'.
        Slash = 0,
        //
        // Summary:
        //     Indicates '/'.
        Hyfen = 1,
        //
        // Summary:
        //     Indicates ','.
        FullStop = 2,
        //
        // Summary:
        //     Indicates '.'.
        Comma = 3,
        //
        // Summary:
        //     Indicates ' '.
        Space = 4,

    }
    public enum IDTimeDelimiter
    {
        // Summary:
        //     Indicates ':'.
        Colon = 0,
        //
        // Summary:
        //     Indicates '-'.
        Hyfen = 1,
        //
        // Summary:
        //     Indicates '.'.
        FullStop = 2,
    }
    public enum IDDateSequence
    {
        // Summary:
        //     Indicates DMY.
        DMY = 0,
        //
        // Summary:
        //     Indicates MDY.
        MDY = 1,

    }
    public enum IDDateHorizontalAlignment
    {
        Left = 0,
        Right = 1,
        Center = 2,
    }
    public enum IDDateVerticalAlignment
    {
        Up = 0,
        Down = 1,
        Middle = 2,
    }

    public enum IDDateFormat
    {
        DDMMYYYY = 0,
        MMDDYYYY = 1,
        DDMMMYYYY = 2,
        MMMDDYYYY = 3,
    }
    #endregion

    #region PageLoad
    protected void Page_Load(object sender, EventArgs e)
    {
        SetForamt();
        //_dropDownYears = 30;
        //_baseYearLimit = 20;
        _dropDownYears = 110;//No of Items To be Shown
        _baseYearLimit = 90;// starting year from current date
        Page.ClientScript.RegisterClientScriptInclude("ClientScript", "Scripts/DateControl/DateControlScript.js");
        Date.Attributes.Add("onclick", "scwShow(this,this,'" + _businessName + "'," + _baseYearLimit + "," + _dropDownYears + "," + _weekStartDay + "," + _weekNumberDisplay + "," + _weekNumberBaseDay + "," + _allowDrag + "," + _outOfMonthDisable + "," + _outOfMonthHide + "," + _calanderHorizontalAlignment + "," + _calanderVerticalAlignment + "," + _offsetLeftAlignment + "," + _offsetTopAlignment + ");");
        Date.Attributes.Add("onFocus", "scwShow(this,this,'" + _businessName + "'," + _baseYearLimit + "," + _dropDownYears + "," + _weekStartDay + "," + _weekNumberDisplay + "," + _weekNumberBaseDay + "," + _allowDrag + "," + _outOfMonthDisable + "," + _outOfMonthHide + "," + _calanderHorizontalAlignment + "," + _calanderVerticalAlignment + "," + _offsetLeftAlignment + "," + _offsetTopAlignment + ");");
        Date.Attributes.Add("onBlur", "if(!isValidDate(this.value)){this.focus(); scwShow(this,this,'" + _businessName + "'," + _baseYearLimit + "," + _dropDownYears + "," + _weekStartDay + "," + _weekNumberDisplay + "," + _weekNumberBaseDay + "," + _allowDrag + "," + _outOfMonthDisable + "," + _outOfMonthHide + "," + _calanderHorizontalAlignment + "," + _calanderVerticalAlignment + "," + _offsetLeftAlignment + "," + _offsetTopAlignment + ");} else OnDateExit(this.id)");
        Date.Attributes.Add("onKeyDown", "scwHide();");

        Date.ToolTip = _dateFormatString;
        Time.ToolTip = "HH24" + _timeDelimiter.ToString() + "mm";

        Date.Attributes.Add("onKeypress", "keyPressDate(this.value," + (int)_dateFormat + "," + _dateDelimiterCode + ");");
        Date.Attributes.Add("onKeyUp", "keyUpDate(this,event," + (int)_dateFormat + "," + _dateDelimiterCode + ");");

        Time.Attributes.Add("onKeypress", "keyPressTime(this.value," + _timeDelimiterCode + ");");
        Time.Attributes.Add("onKeyUp", "keyUpTime(this,event," + _timeDelimiterCode + ");");
        Time.Attributes.Add("onBlur", "OnTimeExit(this.id)");

        string strScript = string.Empty;

        strScript = " scwDateDisplayFormat = '" + _dateFormatString + "';scwDateOutputFormat= '" + _dateFormatString + "';";
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "dateFormatString", strScript, true);

        strScript = " scwDateInputSequence = '" + _dateInputSequence + "';";
        Page.ClientScript.RegisterClientScriptBlock(this.GetType(), "dateInputSequence", strScript, true);

        

    }
    #endregion

    #region Properties

    public string BusinessName
    {
        set { _businessName = value; }
    }

    public DateTime Value
    {
        set
        {
            _dateTime = value.AddHours(_universalTimeDiff);
            Date.Text = GetDateString(_dateTime, _dateFormat);
            Time.Text = _dateTime.ToString("HH" + _timeDelimiter.ToString() + "mm");
        }
        get
        {
            if (_dateOnly) return GetDateFromString(Date.Text, _dateFormat).AddHours(-_universalTimeDiff);
            else if (_timeOnly) return Convert.ToDateTime(GetTimeFromString(Time.Text, _timeFormat)).AddHours(-_universalTimeDiff);
            else return GetDateFromString(Date.Text, _dateFormat, Time.Text).AddHours(-_universalTimeDiff);
        }
    }

    [DefaultValue(true)]
    public bool Enabled
    {
        set { if (Date.Visible) Date.Enabled = value; if (Time.Visible) Time.Enabled = value; }
        get { return Date.Enabled; }
    }

    [DefaultValue(false)]
    public bool DateOnly
    {
        set
        {
            _dateOnly = value;
            Time.Visible = !value;
        }
        get { return _dateOnly; }
    }
    [DefaultValue(false)]
    public bool TimeOnly
    {
        set
        {
            _timeOnly = value;
            if (value)
            {
                Time.Style.Clear();
                Time.Width = Unit.Pixel(50);
            }
            Date.Visible = !value;
        }
        get { return _timeOnly; }
    }

    [DefaultValue(10)]
    public int BaseYearLimit
    {
        set { _baseYearLimit = value; }
    }

    [DefaultValue(20)]
    public int DropDownYears
    {
        set { _dropDownYears = value; }
    }

    [DefaultValue(IDDateDelimiter.Hyfen)]
    public IDDateDelimiter DateDelimiter
    {
        set
        {
            switch (value)
            {
                case IDDateDelimiter.Slash:
                    {
                        _dateDelimiter = "/";
                        _dateDelimiterCode = 47;
                        break;
                    }
                case IDDateDelimiter.Hyfen:
                    {
                        _dateDelimiter = "-";
                        _dateDelimiterCode = 45;
                        break;
                    }
                case IDDateDelimiter.FullStop:
                    {
                        _dateDelimiter = ".";
                        _dateDelimiterCode = 46;
                        break;
                    }
                case IDDateDelimiter.Comma:
                    {
                        _dateDelimiter = ";";
                        _dateDelimiterCode = 59;
                        break;
                    }
                case IDDateDelimiter.Space:
                    {
                        _dateDelimiter = " ";
                        _dateDelimiterCode = 32;
                        break;
                    }
                default:
                    {
                        _dateDelimiter = "-";
                        _dateDelimiterCode = 45;
                        break;
                    }
            }
        }
    }

    [DefaultValue(IDTimeDelimiter.Colon)]
    public IDTimeDelimiter TimeDelimiter
    {
        set
        {
            switch (value)
            {
                case IDTimeDelimiter.Colon:
                    {
                        _timeDelimiter = ':';
                        _timeDelimiterCode = 58;
                        break;
                    }
                case IDTimeDelimiter.Hyfen:
                    {
                        _timeDelimiter = '-';
                        _timeDelimiterCode = 45;
                        break;
                    }
                case IDTimeDelimiter.FullStop:
                    {
                        _timeDelimiter = '.';
                        _timeDelimiterCode = 46;
                        break;
                    }
                default:
                    {
                        _timeDelimiter = ':';
                        _timeDelimiterCode = 58;
                        break;
                    }
            }
        }
    }

    [DefaultValue(IDDateFormat.DDMMMYYYY)]
    public IDDateFormat DateFormat
    {
        set { _dateFormat = value; SetForamt(); }
        get { return _dateFormat; }

    }

    [DefaultValue(DayOfWeek.Sunday)]
    public DayOfWeek WeekStartDate
    {
        set { _weekStartDay = (int)value; }
    }

    [DefaultValue(true)]
    public bool WeekNumberDisplay
    {
        set { _weekNumberDisplay = value.ToString().ToLower(); }
    }

    [DefaultValue(DayOfWeek.Tuesday)]
    public DayOfWeek WeekNumberBaseDay
    {
        set { _weekNumberBaseDay = (int)value; }
    }

    [DefaultValue(IDDateHorizontalAlignment.Left)]
    public IDDateHorizontalAlignment HorizontalAlignment
    {
        set { _calanderHorizontalAlignment = (int)value; }
    }

    [DefaultValue(IDDateVerticalAlignment.Down)]
    public IDDateVerticalAlignment VerticalAlignment
    {
        set { _calanderVerticalAlignment = (int)value; }
    }

    [DefaultValue(0)]
    public int OffsetLeft
    {
        set { _offsetLeftAlignment = value; }
    }

    [DefaultValue(0)]
    public int OffsetTop
    {
        set { _offsetTopAlignment = value; }
    }


    [DefaultValue(true)]
    public bool AllowDrag
    {
        set { _allowDrag = value.ToString().ToLower(); }
    }

    [DefaultValue(false)]
    public bool OutOfMonthDisable
    {
        set { _outOfMonthDisable = value.ToString().ToLower(); }
    }

    [DefaultValue(false)]
    public bool OutOfMonthHide
    {
        set { _outOfMonthHide = value.ToString().ToLower(); }
    }

    public double UniversalTimeDiff
    {
        set { _universalTimeDiff = value; }
    }

    #endregion

    #region Methods

    public void SetDateTime(DateTime universalDateTime)
    {
        _dateTime = universalDateTime.AddHours(_universalTimeDiff);
        Date.Text = GetDateString(_dateTime, _dateFormat);
        Time.Text = _dateTime.ToString("HH" + _timeDelimiter.ToString() + "mm");
    }
    public void SetDateTime(DateTime universalDateTime, double timeDiff)
    {
        _universalTimeDiff = timeDiff;
        _dateTime = universalDateTime.AddHours(_universalTimeDiff);
        Date.Text = GetDateString(_dateTime, _dateFormat);
        Time.Text = _dateTime.ToString("HH" + _timeDelimiter.ToString() + "mm");
    }

    public string GetDateClientID()
    {
        return Date.ClientID;
    }
    public string GetTimeClientID()
    {
        return Time.ClientID;
    }

    public string DateText()
    {
        return GetDateFromString(Date.Text, _dateFormat).ToString("dd-MMM-yyyy");
    }
    public string TimeText()
    {
        return Time.Text;
    }

    public void Clear()
    {
        Date.Text = "";
        Time.Text = "";
    }

    #endregion

    #region Private Methods
    private string GetTimeFromString(string timeValue, IDTimeDelimiter timeFormat)
    {
        try
        {
            return timeValue;

        }
        catch { throw; }
    }
    private DateTime GetDateFromString(string dateValue, IDDateFormat dateFormat)
    {
        try
        {
            string[] dateArray = dateValue.Split(_dateDelimiter.ToCharArray());
            DateTime returnValue = new DateTime();

            switch (dateFormat)
            {
                case IDDateFormat.DDMMYYYY:
                    {
                        returnValue = new DateTime(Convert.ToInt16(dateArray[2]), Convert.ToInt16(dateArray[1]), Convert.ToInt16(dateArray[0]));
                        break;
                    }
                case IDDateFormat.MMDDYYYY:
                    {
                        returnValue = new DateTime(Convert.ToInt16(dateArray[2]), Convert.ToInt16(dateArray[0]), Convert.ToInt16(dateArray[1]));
                        break;
                    }
                case IDDateFormat.DDMMMYYYY:
                    {
                        returnValue = new DateTime(Convert.ToInt16(dateArray[2]), GetMonth(dateArray[1].ToUpper()), Convert.ToInt16(dateArray[0]));
                        break;
                    }
                case IDDateFormat.MMMDDYYYY:
                    {
                        returnValue = new DateTime(Convert.ToInt16(dateArray[2]), GetMonth(dateArray[0].ToUpper()), Convert.ToInt16(dateArray[1]));
                        break;
                    }
            }
            return returnValue;
        }
        catch { return default(DateTime); }
    }
    private DateTime GetDateFromString(string dateValue, IDDateFormat dateFormat, string timeValue)
    {
        try
        {
            string[] timeArray;

            if (string.IsNullOrEmpty(timeValue.Trim()))
            {
                timeArray = new string[2];
                timeArray[0] = "00";
                timeArray[1] = "00";
            }
            else
                timeArray = timeValue.Split(_timeDelimiter);
            string[] dateArray = dateValue.Split(_dateDelimiter.ToCharArray());

            DateTime returnValue = new DateTime();

            switch (dateFormat)
            {
                case IDDateFormat.DDMMYYYY:
                    {
                        returnValue = new DateTime(Convert.ToInt16(dateArray[2]), Convert.ToInt16(dateArray[1]), Convert.ToInt16(dateArray[0]), Convert.ToInt16(timeArray[0]), Convert.ToInt16(timeArray[1]), 0);
                        break;
                    }
                case IDDateFormat.MMDDYYYY:
                    {

                        returnValue = new DateTime(Convert.ToInt16(dateArray[2]), Convert.ToInt16(dateArray[0]), Convert.ToInt16(dateArray[1]), Convert.ToInt16(timeArray[0]), Convert.ToInt16(timeArray[1]), 0);
                        break;
                    }
                case IDDateFormat.DDMMMYYYY:
                    {

                        returnValue = new DateTime(Convert.ToInt16(dateArray[2]), GetMonth(dateArray[1].ToUpper()), Convert.ToInt16(dateArray[0]), Convert.ToInt16(timeArray[0]), Convert.ToInt16(timeArray[1]), 0);
                        break;
                    }
                case IDDateFormat.MMMDDYYYY:
                    {

                        returnValue = new DateTime(Convert.ToInt16(dateArray[2]), GetMonth(dateArray[0].ToUpper()), Convert.ToInt16(dateArray[1]), Convert.ToInt16(timeArray[0]), Convert.ToInt16(timeArray[1]), 0);
                        break;
                    }

            }
            return returnValue;
        }
        catch { return default(DateTime); }
    }
    private string GetDateString(DateTime value, IDDateFormat dateFormat)
    {
        try
        {
            string dateString = string.Empty;
            switch (dateFormat)
            {
                case IDDateFormat.DDMMYYYY:
                    {
                        dateString = value.ToString("dd" + _dateDelimiter + "MM" + _dateDelimiter + "yyyy");
                        break;
                    }
                case IDDateFormat.MMDDYYYY:
                    {
                        dateString = value.ToString("MM" + _dateDelimiter + "dd" + _dateDelimiter + "yyyy");
                        break;
                    }
                case IDDateFormat.DDMMMYYYY:
                    {
                        dateString = value.ToString("dd" + _dateDelimiter + "MMM" + _dateDelimiter + "yyyy");
                        break;
                    }
                case IDDateFormat.MMMDDYYYY:
                    {
                        dateString = value.ToString("MMM" + _dateDelimiter + "dd" + _dateDelimiter + "yyyy");
                        break;
                    }

            }
            return dateString;
        }
        catch { return null; }
    }
    private int GetMonth(string monthName)
    {
        String[] monthNames = new string[] { "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC" };
        for (int i = 0; i < monthNames.Length; i++)
        {
            if (monthName == monthNames[i])
                return i + 1;
        }
        return 0;
    }
    private void SetForamt()
    {

        switch (_dateFormat)
        {
            case IDDateFormat.DDMMYYYY:
                {
                    _dateFormatString = "DD" + _dateDelimiter + "MM" + _dateDelimiter + "YYYY";
                    _dateInputSequence = "DMY";
                    Date.MaxLength = 10;
                    break;
                }
            case IDDateFormat.MMDDYYYY:
                {
                    _dateFormatString = "MM" + _dateDelimiter + "DD" + _dateDelimiter + "YYYY";
                    _dateInputSequence = "MDY";
                    Date.MaxLength = 10;
                    break;
                }
            case IDDateFormat.DDMMMYYYY:
                {
                    _dateFormatString = "DD" + _dateDelimiter + "MMM" + _dateDelimiter + "YYYY";
                    _dateInputSequence = "DMY";
                    Date.MaxLength = 11;
                    break;
                }
            case IDDateFormat.MMMDDYYYY:
                {
                    _dateFormatString = "MMM" + _dateDelimiter + "DD" + _dateDelimiter + "YYYY";
                    _dateInputSequence = "MDY";
                    Date.MaxLength = 11;
                    break;
                }
            default:
                {
                    _dateFormatString = "DD" + _dateDelimiter + "MM" + _dateDelimiter + "YYYY";
                    _dateInputSequence = "DMY";
                    Date.MaxLength = 10;
                    break;
                }
        }



    }
    #endregion
}
