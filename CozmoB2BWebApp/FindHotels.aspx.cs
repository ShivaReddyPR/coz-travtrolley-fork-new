﻿using CT.BookingEngine;
using CT.CMS;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class FindHotels : ParentPage
    {
        protected RoomGuestData[] guestInfo = new RoomGuestData[0];        
        protected HotelRequest reqObj = new HotelRequest();        
        protected string cityName = "Enter city name";
        protected string adults = string.Empty, childs = string.Empty, childAges = string.Empty ;        
        protected int agencyId;
        protected string userType;
        DataTable dtHotelSources;
        public CT.CMS.B2BCMSDetails clsCMSDet;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.Master.PageRole = true;
                if (Settings.LoginInfo == null)
                    Response.Redirect("AbandonSession.aspx");

                rbtnAgent.InputAttributes.Add("class", "tgl tgl-light");
                if (!IsPostBack)
                {
                    agencyId = Settings.LoginInfo.AgentId;
                    userType = Settings.LoginInfo.MemberType.ToString();

                    ClearSessions();
                    BindMessages();
                    GetCMSInfo();
                    GetBookingStatistics();
                    BindNationality();

                    DataTable dtAgents = AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
                    DataView dv = dtAgents.DefaultView;
                    dv.RowFilter = "agent_Id NOt IN('" + Settings.LoginInfo.AgentId + "')";
                    ddlAgents.AppendDataBoundItems = true;
                    ddlAgents.Items.Add("Select Client");
                    ddlAgents.DataSource = dv.ToTable();
                    ddlAgents.DataTextField = "agent_Name";
                    ddlAgents.DataValueField = "agent_Id";
                    ddlAgents.DataBind();

                    if (Settings.LoginInfo.IsCorporate == "Y")
                    {
                        try
                        {
                            DataTable dtTravelReasons = TravelUtility.GetTravelReasonList("S", Settings.LoginInfo.AgentId, ListStatus.Short);
                            ddlFlightTravelReasons.DataSource = dtTravelReasons;
                            ddlFlightTravelReasons.DataTextField = "Description";
                            ddlFlightTravelReasons.DataValueField = "ReasonId";
                            ddlFlightTravelReasons.DataBind();

                            DataTable dtProfiles = TravelUtility.GetProfileList(Settings.LoginInfo.CorporateProfileId, Settings.LoginInfo.AgentId, ListStatus.Short);
                            ddlFlightEmployee.DataSource = dtProfiles;
                            ddlFlightEmployee.DataTextField = "ProfileName";
                            ddlFlightEmployee.DataValueField = "Profile";
                            ddlFlightEmployee.DataBind();
                            ddlFlightEmployee.Items.Insert(0, "Select Traveller");
                            if (ddlFlightEmployee.Items.Count == 2)
                                ddlFlightEmployee.SelectedIndex = 1;
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Search, Severity.High, 0, "Failed to Bind Corporate details in find hotels screen page load. Reason :" + ex.ToString(), Request["REMOTE_ADDR"]);
                        }
                    }

                    if (Page.PreviousPage != null && Page.PreviousPage.IsCrossPagePostBack && Session["req"] != null)
                        FromPreviousPage();
                    else
                    {
                        Settings.LoginInfo.IsOnBehalfOfAgent = false;
                        Settings.LoginInfo.OnBehalfAgentLocation = 0;
                    }
                }

                if (hdnSubmit.Value == "search")
                    SearchHotels();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Search Hotel Details. Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        protected void gvMessages_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvMessages.PageIndex = e.NewPageIndex;
            gvMessages.EditIndex = -1;
            DataSet Messages = (DataSet)Session["Messages"];
            gvMessages.DataSource = Messages;
            gvMessages.DataBind();
        }

        /// <summary>
        /// To clear all the previous sessions before the search starts
        /// </summary>
        private void ClearSessions()
        {
            /* Hotel Ititnerary sessions */
            Session["hItinerary"] = Session["BookingResponse"] = Session["SearchResults"] =
                Session["cSessionId"] = Session["BookingAgencyID"] = Session["Result"] =
                Session["hCode"] = Session["rCode"] = Session["UserBookings"] =
                Session["BookingMade"] = Session["Warning"] = Session["MailSent"] =
                Session["TimesChanged"] = Session["FlightRequest"] = Session["GXResults"] = null;
        }

        /// <summary>
        /// To bind importand messages for user reference
        /// </summary>
        private void BindMessages()
        {
            try
            {
                int agencyId = (int)Settings.LoginInfo.AgentId;
                DataSet Messages = IMPMessages.GetMessages(agencyId);
                Session["Messages"] = Messages;
                gvMessages.DataSource = Messages;
                gvMessages.DataBind();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to load important messages in find hotel page load. Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        /// <summary>
        /// To get booking statistics of hotel
        /// </summary>
        private void GetBookingStatistics()
        {
            try
            {
                DataSet ds = BookingDetail.GetBookingCount(agencyId, Settings.LoginInfo.MemberType == MemberType.OPERATIONS ? Settings.LoginInfo.UserID : 0);

                if (ds == null || ds.Tables.Count == 0)
                    return;

                if (ds.Tables.Count > 0)
                {
                    lblHotelsBookingCountToday.Text = "(0)";
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        if (Enum.GetName(typeof(ProductType), Convert.ToInt32(ds.Tables[0].Rows[i]["productTypeId"].ToString())) == ProductType.Hotel.ToString())
                        {
                            lblHotelsBookingCountToday.Text = "(" + (ds.Tables[0].Rows[i]["bookingcount"]).ToString() + ")";
                            lblHotelsBookingCountToday.NavigateUrl += DateTime.Now.ToString("dd/MM/yyyy");
                        }
                    }
                }

                if (ds.Tables.Count > 1)
                {
                    lblHotelsBkgWeeklyCount.Text = "(0)";
                    for (int i = 0; i < ds.Tables[1].Rows.Count; i++)
                    {
                        if (Enum.GetName(typeof(ProductType), Convert.ToInt32(ds.Tables[1].Rows[i]["productTypeId"].ToString())) == ProductType.Hotel.ToString())
                        {
                            lblHotelsBkgWeeklyCount.Text = "(" + (ds.Tables[1].Rows[i]["bookingcount"]).ToString() + ")";
                            lblHotelsBkgWeeklyCount.NavigateUrl += DateTime.Now.AddDays(-7).ToString("dd/MM/yyyy");
                        }
                    }
                }

                if (ds.Tables.Count > 2)
                {
                    lblHotelsBkgMonthlyCount.Text = "(0)";
                    for (int i = 0; i < ds.Tables[2].Rows.Count; i++)
                    {
                        if (Enum.GetName(typeof(ProductType), Convert.ToInt32(ds.Tables[2].Rows[i]["productTypeId"].ToString())) == ProductType.Hotel.ToString())
                        {
                            lblHotelsBkgMonthlyCount.Text = "(" + (ds.Tables[2].Rows[i]["bookingcount"]).ToString() + ")";
                            lblHotelsBkgMonthlyCount.NavigateUrl += DateTime.Now.AddDays(-30).ToString("dd/MM/yyyy");
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get hotel booking statistics on find hotels page load. Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        /// <summary>
        /// To bind nationality drop down
        /// </summary>
        private void BindNationality()
        {
            DOTWCountry dotwC = new DOTWCountry();
            Dictionary<string, string> countryList = dotwC.GetAllCountries();

            ddlNationality.DataSource = countryList;
            ddlNationality.DataValueField = "key";
            ddlNationality.DataTextField = "value";
            ddlNationality.DataBind();

            if (Settings.LoginInfo.LocationCountryCode == "IN")
                ddlNationality.Items.FindByText("INDIA").Selected = true;
        }

        /// <summary>
        /// To prepare hotel search request object
        /// </summary>
        void SearchHotels()
        {
            // Reading Corporate Traveller Grade
            if (Settings.LoginInfo.IsCorporate == "Y" && !string.IsNullOrEmpty(ddlFlightEmployee.SelectedValue) && ddlFlightEmployee.SelectedValue.Split('~').Length > 1)
                Settings.LoginInfo.CorporateProfileGrade = ddlFlightEmployee.SelectedValue.Split('~')[1];
                        
            Session["error"] = null;

            Session["memberId"] = 1;            
            
            List<string> sources = new List<string>();

            if (Convert.ToDouble(hdnLongtitude.Value) != 0 && Convert.ToDouble(hdnlatitude.Value) != 0)
            {
                reqObj.Longtitude = Convert.ToDouble(hdnLongtitude.Value);
                reqObj.Latitude = Convert.ToDouble(hdnlatitude.Value);
                reqObj.Radius = Convert.ToInt32(hdnRadius.Value);
                var sCntryInfo = hdnPOICityName.Value.Split('|');
                reqObj.CountryName = sCntryInfo[0];
                reqObj.CountryCode = sCntryInfo.Length > 1 ? sCntryInfo[1] : string.Empty;
                reqObj.CityName = sCntryInfo.Length > 2 ? sCntryInfo[2] : string.Empty;
            }
            else
            {
                if (!string.IsNullOrEmpty(Request.Form["cityCode"]))
                {
                    reqObj.CityId = Convert.ToInt32(Request.Form["cityCode"]);
                    if (!string.IsNullOrEmpty(Request.Form["city"]) && Request.Form["city"].Split(',').Length > 2)
                    {
                        string[] multiplecities = Request.Form["city"].Split(',');
                        reqObj.CountryName = Request.Form["city"].Split(',')[multiplecities.Length - 1];
                    }
                    else
                    {
                        reqObj.CountryName = Request.Form["city"].Split(',')[1];
                    }

                }
                reqObj.CityName = Request.Form["City"].Split(',')[0];
                Session["city"] = Request.Form["City"];
                

            }

            Settings.LoginInfo.OnBehalfAgentID = rbtnAgent.Checked == true ? Convert.ToInt32(ddlAgents.SelectedValue) : Settings.LoginInfo.AgentId;

            try
            {
                if (rbtnAgent.Checked)
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = true;
                    AgentMaster agent = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                    Settings.LoginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
                    Settings.LoginInfo.OnBehalfAgentDecimalValue = agent.DecimalValue;
                    Settings.LoginInfo.OnBehalfAgentLocation = Convert.ToInt32(hdnAgentLocation.Value);
                    StaticData sd = new StaticData();
                    sd.BaseCurrency = agent.AgentCurrency;
                    Settings.LoginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                    Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(Settings.LoginInfo.OnBehalfAgentID);
                    Settings.LoginInfo.OnBehalfAgentSourceCredentials = AgentCredentials;
                }
                else
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                    Settings.LoginInfo.OnBehalfAgentID = 0;
                }
            }
            catch { }
                        
            sources = new List<string>();
            if (hdnSelectedSources.Value != "" && Convert.ToInt32(hdnActiveSources.Value) > 0)
            {
                if (hdnSelectedSources.Value.Split(',').Contains("UAH"))
                {
                    sources.Add("UAH");
                    reqObj.SupplierIds = null;
                }
                if (hdnSelectedSources.Value.Split(',').Contains("HIS") || hdnSelectedSources.Value.Split(',').Contains("RezLive")|| hdnSelectedSources.Value.Split(',').Contains("Illusions")||hdnSelectedSources.Value.Split(',').Contains("HotelExtranet"))
                {
                    reqObj.CityId = (reqObj.Longtitude == 0 && reqObj.Latitude == 0) ? HotelCity.GetCityIdFromCityName(reqObj.CityName) : 0;
                    reqObj.SupplierIds = null;
                    if (hdnSelectedSources.Value.Split(',').Contains("HIS"))
                    {
                        sources.Add("HIS");                      
                    }
                    if (hdnSelectedSources.Value.Split(',').Contains("RezLive"))
                    {
                        sources.Add("RezLive");
                    }
                    if (hdnSelectedSources.Value.Split(',').Contains("Illusions"))
                    {
                        sources.Add("Illusions");
                    }
                    if (hdnSelectedSources.Value.Split(',').Contains("HotelExtranet"))
                    {
                        sources.Add("HotelExtranet");
                    }
                }
                if(hdnSelectedSources.Value.Split(',').Contains("GIMMONIX"))
                {
                    //if source having Gimmonix then need to find out their subsuppliers and assign to request object.
                    sources.Add("GIMMONIX");
                    if (hdnSelectedSources.Value != "" && (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent))
                    {
                        hdnSelectedSources.Value = hdnSelectedSources.Value.Replace("HIS", "").Replace("UAH","").Replace("GIMMONIX","").Replace("RezLive","").Replace("Illusions", "").Replace("HotelExtranet", "");
                        string[] selSources = hdnSelectedSources.Value.Split(new string[] { ","},StringSplitOptions.RemoveEmptyEntries); 
                        List<int> SupplierIdsList = new List<int>();
                        if (selSources.Length > 0)
                        {
                            for (int i = 0; i < selSources.Length; i++)
                            {if( !string.IsNullOrEmpty(selSources[i]))
                                SupplierIdsList.Add(Convert.ToInt32(selSources[i]));
                            }
                            reqObj.SupplierIds = SupplierIdsList.ToArray();
                        }
                        else
                        {
                            reqObj.SupplierIds = null;
                        }
                    }
                }
            }
            reqObj.Sources = sources;            

            IFormatProvider format = new CultureInfo("en-GB", true);

            reqObj.StartDate = DateTime.Parse(CheckIn.Text, format);
            if (CheckOut.Text.Length > 0)
                reqObj.EndDate = DateTime.Parse(CheckOut.Text, format);

            reqObj.PassengerCountryOfResidence = ddlNationality.SelectedItem.Value;
            reqObj.PassengerNationality = ddlNationality.SelectedItem.Value;

            reqObj.MinRating = 0; reqObj.MaxRating = 5;
            if (!string.IsNullOrEmpty(rating.SelectedValue) && Convert.ToInt16(rating.SelectedValue) > 0)
                reqObj.MinRating = reqObj.MaxRating = Convert.ToInt16(rating.SelectedValue);

            reqObj.HotelName = txtHotelName.Text;

            reqObj.Corptravelreason = ddlFlightTravelReasons.SelectedIndex > 0 ? ddlFlightTravelReasons.SelectedValue : string.Empty;
            reqObj.Corptraveler = ddlFlightEmployee.SelectedIndex > 0 ? ddlFlightEmployee.SelectedValue : string.Empty;
            reqObj.URLocator = Request.QueryString["URLC"] != null ? Convert.ToString(Request.QueryString["URLC"]) : string.Empty;

            string rooms = Request["roomCount"];
            reqObj.NoOfRooms = Convert.ToInt16(rooms);
            if (reqObj.NoOfRooms > 1)
            {
                reqObj.IsMultiRoom = true;
            }

            guestInfo = new RoomGuestData[reqObj.NoOfRooms];
            
            for (int i = 0; i < reqObj.NoOfRooms; i++)
            {
                //Checks whether Number of children more than 1.
                string request = "chdRoom-" + (i + 1);
                if (Request[request] != "none" && Convert.ToInt16(Request[request]) > 0)
                {
                    guestInfo[i].noOfChild = Convert.ToInt16(Request[request]);
                    List<int> childInfo = new List<int>();
                    string numChild = string.Empty; ;
                    for (int j = 1; j <= guestInfo[i].noOfChild; j++)
                    {
                        numChild = "ChildBlock-" + (i + 1) + "-ChildAge-" + j;
                        childInfo.Add(Convert.ToInt16(Request[numChild]));
                    }
                    guestInfo[i].childAge = childInfo;
                }
                else
                {
                    guestInfo[i].noOfChild = 0;
                    guestInfo[i].childAge = new List<int>();
                }
                string adultStr = "adtRoom-" + (i + 1);
                guestInfo[i].noOfAdults = Convert.ToInt16(Request[adultStr]);   
            }
            
            reqObj.RoomGuest = guestInfo;
            Session["req"] = reqObj;
            if (reqObj.Sources.Count > 0)
                Response.Redirect("ApiHotelResults.aspx", false);
            else
            {
                Session["error"] = "Please change your search parameters and try!";
                Response.Redirect("findhotels.aspx", false);
            }            
        }

        /// <summary>
        /// To set search data on browser back button click
        /// </summary>
        private void FromPreviousPage()
        {
            try
            {
                reqObj = Session["req"] as HotelRequest;

                cityName = reqObj.CityName + "," + reqObj.CountryName;

                CheckIn.Text = reqObj.StartDate.ToString("dd/MM/yyyy");
                CheckOut.Text = reqObj.EndDate.ToString("dd/MM/yyyy");
                txtHotelName.Text = reqObj.HotelName;
                hdnRating.Value = ((int)reqObj.Rating).ToString();

                for (int i = 0; i < reqObj.NoOfRooms; i++)
                {
                    string sRoomNo = (i + 1).ToString();
                    adults += adults.Length > 0 ? "," + sRoomNo + "-" + reqObj.RoomGuest[i].noOfAdults : sRoomNo + "-" + reqObj.RoomGuest[i].noOfAdults;
                    childs += childs.Length > 0 ? "," + sRoomNo + "-" + reqObj.RoomGuest[i].noOfChild : sRoomNo + "-" + reqObj.RoomGuest[i].noOfChild;

                    if (reqObj.RoomGuest[i].childAge != null)
                    {
                        foreach (int age in reqObj.RoomGuest[i].childAge)
                        {
                            childAges += childAges.Length > 0 ? "," + age.ToString() : age.ToString();
                        }
                    }
                }

                ddlNationality.SelectedIndex = 0;
                if (reqObj.PassengerNationality != null && reqObj.PassengerNationality.Length > 0)
                    ddlNationality.Items.FindByValue(reqObj.PassengerNationality).Selected = true;

                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    rbtnAgent.Checked = true;
                    ddlAgents.SelectedValue = Settings.LoginInfo.OnBehalfAgentID.ToString();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to set hotel search data on browser back button click. Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        /// <summary>
        /// To get B2B CMS details and sliders
        /// </summary>
        void GetCMSInfo()
        {
            try
            {
                clsCMSDet = B2BCMSDetails.GetCMSData(Settings.LoginInfo.AgentId, Convert.ToInt32(Settings.LoginInfo.AgentType));
                clsCMSDet = clsCMSDet == null ? new B2BCMSDetails() : clsCMSDet;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get GetCMSInfo. Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }

        }
         [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string BindSuppliers( string agentId)
        {
            try
            {
                DataTable dtHotelSources = AgentMaster.GetAgentSources(Convert.ToInt32(agentId), 2, true);
                DataTable dtGimmonixSuppliers = null;
                // dtHotelSources = dtHotelSources.Select("Name='" + HotelBookingSource.GIMMONIX.ToString() + "'").Count() > 0 ?
                //        new HotelSource().LoadGimmonixSuppliers() : dtHotelSources;
                foreach (DataRow dr in dtHotelSources.Rows)
                {
                    if (Convert.ToString(dr["Name"]) == "GIMMONIX")
                    {
                        dtGimmonixSuppliers = new HotelSource().LoadGimmonixSuppliers();
                    }
                }
                Dictionary<string, Object> Data = new Dictionary<string, object>();
                Data.Add("Suppliers", dtHotelSources);
                Data.Add("ChildSuppliers", dtGimmonixSuppliers);
                string JsonString = JsonConvert.SerializeObject(Data);
                return JsonString;
            }
            catch
            {
                return null;
            }
        }
    }
}
