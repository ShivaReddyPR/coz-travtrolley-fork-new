﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="DynamicMarkupThresholdMaster.aspx.cs" Inherits="CozmoB2BWebApp.DynamicMarkupThresholdMaster" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
    <div class="col-md-12">
        <h3>Dynamic Markup Threshold Master</h3>
    </div>
    <div class="body_container" style="height:100px">
        <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2">
               From Amount  :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtFromAmount" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>
            </div>
            <div class="col-md-2">
                To Amount :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtToAmount" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>
            </div>
          
            <div class="clearfix"></div>
            </div>
        <div class="col-md-12 padding-0 marbot_10">
              <div class="col-md-2">
                Markup Value :
            </div>
            <div class="col-md-2">
                <asp:TextBox ID="txtMarkupValue" runat="server" onKeyPress="return isNumber(event)"></asp:TextBox>
            </div>
            <div class="col-md-2">
                Markup Type :
            </div>
            <div class="col-md-2">
                <asp:DropDownList ID="ddlMarkupType" runat="server" Width="150" AppendDataBoundItems="true">
                    <asp:ListItem Value="-1">Select Markup Type</asp:ListItem>
                    <asp:ListItem Value="P">Percentage</asp:ListItem>
                    <asp:ListItem Value="F">Fixed</asp:ListItem>
                </asp:DropDownList>
            </div>
            <div class="clearfix"></div>
        </div>
        <div class="col-md-12">
            <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="btn but_b" OnClientClick="return Save();" />
            <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn but_b" OnClick="btnSearch_Click" />
            <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn but_b" OnClick="btnClear_Click"/>
            <%--<asp:Button ID="btnClose" runat="server" Text="Close" CssClass="btn but_b" OnClientClick="return closeWindow()"/>--%>
        </div>
        
        <asp:HiddenField ID="hdfId" Value="0" runat="server" />
        <asp:Label ID="lblSucessMsg" runat="server" Visible="false"></asp:Label>
            </div>
    <script type="text/javascript">
    function closeWindow()
    {
        debugger;
        window.parent.close();
        }
    function Save() {
       if (getElement('ddlMarkupType').value == -1) addMessage('MarkupType cannot be blank!', '');
       if (getElement('ddlOperatorValue').value ==-1) addMessage('OperatorValue cannot be blank!', '');
            if (getMessage() != '') {
            alert(getMessage()); clearMessage(); 
            return false;
           }
        }
    function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57 || charCode == 47)) {
                return false;
            }
            return true;
        }
    </script>
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
     <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="ThresholdId"
      emptydatalist="No DynamicMarkup Threshold List!" AutoGenerateColumns="false" PageSize="15" GridLines="None"
     CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="5"
     CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">


     <Columns>
         <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
         <asp:TemplateField HeaderText ="SL.NO">
        <ItemTemplate>
             <%#Container.DataItemIndex+1 %>
        </ItemTemplate>
    </asp:TemplateField>
         <asp:TemplateField HeaderText="From Amount" >
       
        <ItemTemplate >
    <asp:Label ID="ITlblFAmount" runat="server" Text='<%# Eval("FromAmount") %>' CssClass="label grdof" ToolTip='<%# Eval("FromAmount") %>' ></asp:Label>
   
    </ItemTemplate>  
         </asp:TemplateField>

         <asp:TemplateField HeaderText="To Amount">
        <ItemTemplate >
    <asp:Label ID="ITlblTAmount" runat="server" Text='<%# Eval("ToAmount") %>' CssClass="label grdof" ToolTip='<%# Eval("ToAmount") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>
         <asp:TemplateField HeaderText="Markup Value">
        <ItemTemplate >
    <asp:Label ID="ITlblMarkupValue" runat="server" Text='<%# Eval("MarkupValue") %>' CssClass="label grdof" ToolTip='<%# Eval("MarkupValue") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>
          <asp:TemplateField HeaderText="Markup Type">
        <ItemTemplate >
    <asp:Label ID="ITlblMarkupType" runat="server" Text='<%# Eval("MarkupType") %>' CssClass="label grdof" ToolTip='<%# Eval("MarkupType") %>'></asp:Label>
   
    </ItemTemplate>
         </asp:TemplateField>
          
 </Columns>

     </asp:GridView>
</asp:Content>
