﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="TransferResults.aspx.cs" Inherits="CozmoB2BWebApp.TransferResults" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />
    <script src="build/js/jquery-ui.js"></script>
    <link rel="stylesheet" href="css/bootstrap-datepicker.css" /> 
    <script type="text/javascript" src="scripts/bootstrap-datepicker.js"></script>
    <style>
        .last-cancel-date{
            position:absolute;
            bottom:5px;
            text-align: center;
            font-size: 12px;
            color: #000;
            font-weight: bold;
        }
    </style>

        <input type="hidden" id="hdnUserId" runat="server" />
        <input type="hidden" id="hdnAgentId" runat="server" />
        <input type="hidden" id="hdnBehalfLocation" runat="server" />        
       <input type="hidden" id="hdnSession" runat="server" />
        <input type="hidden" id="hdndecimal" runat="server" />
        <input type="hidden" id="hdnGuestInfo" runat="server" />
    <input type="hidden" id="hdnSessionId" runat="server" />
     <input type="hidden" id="hdnAgentType" runat="server" />
   
        <input type="hidden" id="hdnCorpInfo" runat="server" />
        <div class="ui-listing-page">
            <!--Modify Panel-->
            
            <div class="ui-modify-wrapper">
                <div id="errMess" class="error_module" style="display: none;">
                    <div id="errorMessage" class="alert-messages alert-caution">
                    </div>
                </div>
                
                
                <div class="ui-hotel-modify-search">
                    <div class="row custom-gutter">
                        <div class="col-md-10">
                            <div class="row custom-gutter">
                                <div class="col-12" id="normalSearchFields">
                                    <div class="row custom-gutter">

                                        <div class="col-md-4 col-xl-4 mb-2 mb-xl-0">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon icon-location" id=""></span>
                                                </div>
                                                <input class="form-control ui-autocomplete-input" placeholder="" type="text" id="Picup" autocomplete="off" />   
                                                <input type="hidden" value="0" id="hdnPickUpLongtitude" />
                                                <input type="hidden" value="0" id="hdnPickUplatitude" />
                                            </div>
                                        </div>
                                        <div class="col-md-4 col-xl-4 mb-2 mb-xl-0">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon icon-location" id=""></span>
                                                </div>
                                                <input class="form-control ui-autocomplete-input" placeholder="" type="text" id="Destination" autocomplete="off" /> 
                                                <input type="hidden" value="0" id="hdnDropOffLatitude" />
                                                <input type="hidden" value="0" id="hdnDropOffLongtitude" />
                                            </div>
                                        </div>

                                        <div class="col-md-4  col-xl-4 mb-2 mb-xl-0">
                                            <div class="row no-gutter">
                                                <div class="col-7">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="icon icon-calendar"></span>
                                                        </div>
                                                        <input class="form-control" placeholder="" type="text" id="PickupDate" readonly="readonly" data-date-format="dd/mm/yyyy" />
                                                    </div>
                                                </div>
                                                <div class="col-5">
                                                    <div class="input-group">
                                                        <div class="input-group-prepend">
                                                            <span class="icon icon-clock"></span>
                                                        </div>
                                                        <select class="form-control select2" id="PicUpTime">
                                                            <option value="0:00">0:00</option>
                                                            <option value="0:15">0:15</option>
                                                            <option value="0:20">0:20</option>
                                                            <option value="0:25">0:25</option>
                                                            <option value="0:30">0:30</option>
                                                            <option value="0:45">0:45</option>
                                                            <option value="1:00">1:00</option>
                                                        </select>
                                                    </div>
                                                </div>
                                            </div>

                                        </div>

                                     

                                    </div>
                                </div>

                                <div class="col-12" id="advancedSearchFields" style="display:none" >
                                    <div class="row custom-gutter">
                                        <div class="col-12 col-lg-4 d-none" id="flightDiv">
                                            <label>Airline</label>
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon-location" aria-label="Airline"></span>
                                                </div>
                                                <select id="AirlineCode" class="form-control select2">

                                                </select>
                                            </div>          
                                         </div>
                                        <div class="col-12 col-lg-4 d-none" id="flightNoDiv">
                                              <label>Flight Number</label>
                                              <div class="input-group">
                                                 <div class="input-group-prepend">
                                                     <span class="icon-location" aria-label="Flight number"></span>
                                                 </div>
                                                 <input id="flightNumber" class="form-control" placeholder="Flight Number" />  
                                
                                             </div>
                                         </div>
                                        <div class="col-12 col-lg-4 d-none" id="departurCityDiv">
                                              <label>Diparture City</label>
                                              <div class="input-group">
                                                 <div class="input-group-prepend">
                                                     <span class="icon-location" aria-label="Diparture City"></span>
                                                 </div>
                                                 <input id="departureCity" class="form-control" placeholder="Departure City Name" />                                  
                                             </div>
                                         </div>
                                    </div>
                                    <div class="row custom-gutter">
                                        <div class="col-md-4">                                            
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon-group" aria-label=""></span>
                                                </div>  
                                                <select class="form-control select2" id="passengerCount">
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                </select>    
                                            </div>   
                                        </div>

                                        <div class="col-md-4">

                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon-baggage"></span>
                                                </div>                                               
                                                <select id="Luggage" class="form-control select2" >
                                                    <option value="0">0</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>

                                        </div>
                                        <div class="col-md-4">
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon-dog"></span>
                                                </div>                                               
                                                <select id="PetCount" class="form-control select2" >
                                                    <option value="0">0</option>
                                                    <option value="1">1</option>
                                                    <option value="2">2</option>
                                                    <option value="3">3</option>
                                                    <option value="4">4</option>
                                                    <option value="5">5</option>
                                                </select>
                                            </div>
                                        </div>
                                        
                                    </div> 

                                    <div class="row custom-gutter">
                                        <div class="col-md-4">                                            
                                            <div class="input-group">
                                                <div class="input-group-prepend">
                                                    <span class="icon-baggage" aria-label=""></span>
                                                </div>  
                                                <select class="form-control select2" id="SportsLuggage" title="Sports luggage">
                                                        <option value="0">0</option>
                                                        <option value="1">1</option>
                                                        <option value="2">2</option>
                                                        <option value="3">3</option>
                                                        <option value="4">4</option>
                                                        <option value="5">5</option>
                                                </select>    
                                            </div>   
                                        </div>
                                        <div class="col-4 col-lg-4">                                            
                                            <div class="custom-dropdown-wrapper">
                                   
                                                <a href="jaavscript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#travellerDropdown">
                                                    <div class="icon-holder">
 	                                                     <span class="icon-group"></span>
                                                    </div>
                                                    <span class="form-control-text" id=""><strong></strong> Child Seats</span>               
                                                </a>
                                                <div class="dropdown-content right-aligned p-4 d-none" id="travellerDropdown" style="max-width: 200px;">                                       
                                                    <div class="row no-gutters">                                             
                                                          <div class="col-12">
                                                               <span class="pt-2 d-block"> 3-6 years (15-25kg)</span>
                                                              <select  class="form-control no-select2 childSeat1" id="childSeat1">
                                                                  <option selected="selected" value="0">0</option>
	                                                                <option value="1">1</option>
	                                                                <option value="2">2</option>
	                                                                <option value="3">3</option>
	                                                                <option value="4">4</option>
	                                                                <option value="5">5</option>
	                                                                <option value="6">6</option>
	                                                                <option value="7">7</option>
	                                                                <option value="8">8</option>
	                                                                <option value="9">9</option>

                                                                </select>
                                                          </div>
                                                      </div> 
                                                    <div class="row no-gutters">
                                                          <div class="col-12">
                                                               <span class="pt-2 d-block"> 6-12 years (22-36kg)</span>
                                                              <select class="form-control no-select2 childSeat2" id="childSeat2">
	                                                            <option selected="selected" value="0">0</option>
	                                                            <option value="1">1</option>
	                                                            <option value="2">2</option>
	                                                            <option value="3">3</option>
	                                                            <option value="4">4</option>
	                                                            <option value="5">5</option>
	                                                            <option value="6">6</option>
	                                                            <option value="7">7</option>
	                                                            <option value="8">8</option>

                                                            </select>
                                                          </div>
                                                      </div> 
                                    
                                                      <a href="javascript:void(0);" class="mt-2 btn btn-primary btn-sm float-right dd-done-btn">DONE</a>       
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-4 col-lg-4">    
                                           
											<div class="input-group">                                                        
                                                <input id="VoucherCode" placeholder="Promo Code" class="form-control" type="text"/>
                                           </div>
                                       </div>
                                    </div>	
                                    
									<div class="row custom-gutter" id="supplierDiv">
                                       <div class="col-4 col-lg-4">       
                                            <div class="custom-dropdown-wrapper">
                                                <a href="javascript:void(0);" class="form-control-holder form-control-element with-custom-dropdown" data-dropdown="#TransferSupplierDropdown">
                                                    <div class="form-control-holder">
                                                        <div class="icon-holder">                                       
                                                                <span class="icon-global-settings"></span>                                     
                                                        </div>   
                                                        <span class="form-control-text" id="htlSelectedSuppliers">Search Suppliers</span>                                  
                                                    </div>  
                                                </a>
                                                <div class="dropdown-content d-none p-4" id="TransferSupplierDropdown">                         
                                                    <div class="row no-gutters">                                            
                                                        <div class="col-md-12">
                                                            <strong> Search Suppliers : </strong>
                                                        </div>     
                                                        <div class="col-md-12">                               
                                                            <table width="100%" id="tblSources" class="chkChoice custom-checkbox-table chk-Suppliers-table mt-3" enableviewstate="true" border="0" cellspacing="0" cellpadding="0"></table>  
                                                            <a href="javascript:void(0);" class="btn btn-primary btn-sm float-right dd-done-btn">DONE</a>
                                                        </div>                                             
                                                    </div>
                                                </div>
                                            </div>
                                       </div>
                                    </div>									
                                   
                                </div>
                                <div class="col-md-12">
                                   
                                </div>

                                <div class="col-md-12">   
                                   
                                </div>
                                        <div class="col-12" id="divCorpFields" style="display:none">
                                            <div class="row custom-gutter">
                                                <div class="col-md-6 mb-2 col-xl-3 mb-md-0">
                                                    <div class="input-group">
                                                        <div class="select2-container form-control" id="s2id_ddlTravelReasons">
                                                            <a href="javascript:void(0)" class="select2-choice" tabindex="-1"> <span class="select2-chosen" id="select2-chosen-52">&nbsp;</span><abbr class="select2-search-choice-close"></abbr> <span class="select2-arrow" role="presentation"><b role="presentation"></b></span></a>
                                                            <label for="s2id_autogen52" class="select2-offscreen"></label>
                                                            <input class="select2-focusser select2-offscreen" type="text" aria-haspopup="true" role="button" aria-labelledby="select2-chosen-52" id="s2id_autogen52">
                                                            <div class="select2-drop select2-display-none select2-with-searchbox">
                                                                <div class="select2-search">
                                                                    <label for="s2id_autogen52_search" class="select2-offscreen"></label>
                                                                    <input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="select2-input" role="combobox" aria-expanded="true" aria-autocomplete="list" aria-owns="select2-results-52" id="s2id_autogen52_search" placeholder=""> </div>
                                                                <ul class="select2-results" role="listbox" id="select2-results-52"> </ul>
                                                            </div>
                                                        </div>
                                                        <select class="form-control" id="ddlTravelReasons" tabindex="-1" title="" style="display: none;"></select>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 mb-2 col-xl-3 mb-md-0">
                                                    <div class="input-group">
                                                        <div class="select2-container form-control" id="s2id_ddlTravelers">
                                                            <a href="javascript:void(0)" class="select2-choice" tabindex="-1"> <span class="select2-chosen" id="select2-chosen-53">&nbsp;</span><abbr class="select2-search-choice-close"></abbr> <span class="select2-arrow" role="presentation"><b role="presentation"></b></span></a>
                                                            <label for="s2id_autogen53" class="select2-offscreen"></label>
                                                            <input class="select2-focusser select2-offscreen" type="text" aria-haspopup="true" role="button" aria-labelledby="select2-chosen-53" id="s2id_autogen53">
                                                            <div class="select2-drop select2-display-none select2-with-searchbox">
                                                                <div class="select2-search">
                                                                    <label for="s2id_autogen53_search" class="select2-offscreen"></label>
                                                                    <input type="text" autocomplete="off" autocorrect="off" autocapitalize="off" spellcheck="false" class="select2-input" role="combobox" aria-expanded="true" aria-autocomplete="list" aria-owns="select2-results-53" id="s2id_autogen53_search" placeholder=""> </div>
                                                                <ul class="select2-results" role="listbox" id="select2-results-53"> </ul>
                                                            </div>
                                                        </div>
                                                        <select class="form-control" id="ddlTravelers" tabindex="-1" title="" style="display: none;"></select>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        
                                   
                            </div>
                        </div>
                        <div class="col-md-2 d-flex align-content-center align-items-center justify-content-center">
                            <input type="button" class="btn btn-primary font-weight-bold w-100" style="height: 38px;" value="MODIFY" id="btnModify" />
                        </div>
                    </div>
                </div>

            </div>
            <div class="results-list-wrapper pt-3">
                
                <div class="row custom-gutter">
          
                    <div class="col-12 col-lg-3 d-none d-lg-block" id="filter-panel">
                        <a href="javascript:void(0);" class="filter-done-mobile close-btn d-block d-lg-none "><span class="icon icon-close"></span></a>
                        <div class="row">
                            <div class="col-md-12 filter-text">
                                <span>FILTER </span><a id="ClearFilter" href="javascript:void(0);" >Clear all</a>
                            </div>
                        </div>
                        <div class="ui-left-panel">

                            <div class="row">

                                <div class="col-md-12 mb-1 left-panel-heading">CAR MODEL </div>

                                <div class="col-md-12">

                                    <div class="input-group">

                                        <input class="form-control  ui-serch-by-car ui-autocomplete-input" placeholder="Search by Car Model" id="txtSearchVehicle" >

                                    </div>

                                </div>

                                <div class="col-md-12 left-panel-heading mb-1 mt-4">CAR TYPES </div>

                                <div class="col-md-12 filter-category">
                                    <div class="custom-checkbox-style">
                                        <input type="checkbox"  name="rating" value="All" class="CarTypes checkAll" checked="checked">
                                        <label for="CarTypes-0" class="CarType">All Types</label>
                                    </div>
                                   <%-- <div class="custom-checkbox-style">
                                        <input type="checkbox" name="rating" value="Economy MPV" class="CarTypes" checked="checked">
                                        <label for="CarTypes-1" class="CarType">Economy MPV</label>
                                    </div>
                                    <div class="custom-checkbox-style">
                                        <input type="checkbox" name="rating" value="Economy" class="CarTypes" checked="checked">
                                        <label for="CarTypes-2" class="CarType">Economy</label>
                                    </div>
                                    <div class="custom-checkbox-style">
                                        <input type="checkbox"  name="rating" value="Economy VAN" class="CarTypes" checked="checked">
                                        <label for="CarTypes-3" class="CarType">Economy VAN</label>
                                    </div>
                                    <div class="custom-checkbox-style">
                                        <input type="checkbox" name="rating" value="Business" class="CarTypes" checked="checked">
                                        <label for="CarTypes-4" class="CarType">Business</label>
                                    </div>                               
                                    <div class="custom-checkbox-style">
                                        <input type="checkbox" name="rating" value="First" class="CarTypes" checked="checked">
                                        <label for="CarTypes-5" class="CarType">First</label>
                                    </div>  --%>

                                </div>

                                <div class="col-md-12 left-panel-heading mb-1 mt-4">PRICE </div>

                                <div class="col-md-12">
                                    <div class="filter-item price-slider">

                                        <div class="price-slider-text">
                                            <div class="float-left">
                                                <small id="fromrangeCurrency"></small> 
                                                <span class="range" id="price-from-range"></span>
                                                <span style="display:none;" id="fromrange"></span>
                                            </div>
                                            <div class="float-right">

                                                <small id="torangeCurrency"></small>   
                                                <span class="range" id="price-to-range"></span>
                                                 <span style="display:none;" id="torange"></span>
                                            </div>
                                        </div>
                                        <div class="clearfix"></div>
                                        <div id="htlPriceRangeSlider" ></div>

                                    </div>

                                </div>

                            </div>

                        </div>
                        <button type="button" id="filterDoneMobile" class="filter-done-mobile pl-3 d-block d-lg-none btn-block btn-primary btn mt-3 font-weight-bold">
                            APPLY
                        </button>
                    </div>


                    <div class="col-12 col-lg-9" id="hotel-results-panel">
                        <div class="ui-result-header">
                            <div class="float-left mb-3" id="" style="">
                                <h2 class="float-left" id="">Transfers  <strong>Result</strong></h2>
                                <span class="primary-bgcolor ui-bg-highlighted float-left" ><span id="noOfResults">0</span> found</span>
                            </div>

                            <div class="float-left float-sm-right mb-3 mb-md-0">

                                <div class="ui-list-control-btn-group mt-2 float-left">
                                    <ul>
                                        <li class="normal-text">SORT BY</li>
                                        <li>
                                            <div class="btn-group" id="sortDropdown">                                              
                                                <select id="sortbyPrice" class="form-control">
                                                    <option value="1" selected="selected">Lowest Price</option>
                                                    <option value="2">Highest Price</option>
                                                </select>
                                            </div>
                                        </li>                                     
                                       
                                        <li id="filterBtnWrap">
                                            <a href="javascript:void(0);" id="filterBtnMobile">
                                                <span class="icon icon-filter"></span>
                                            </a>
                                        </li>
                                    </ul>
                                </div>

                            </div>
                        </div>

                        <div class="clearfix"></div>
                        <ul class="ui-listing-wrapper list-unstyled" id="StaticView">
							<li class="item">
								 <div>
                                    <div class="row no-gutters">
                                        <div class="col-2 col-sm image-wrapper">
                                            <%--<img src="123" alt="Hotel Name" class=" h-100">--%>
                                        </div>
                                        <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column text-content-wrapper">
                                            <h4 ></h4>
                                            <p class="address mb-0" ></p>
                                            <div class="mb-auto mt-2">
                                                <div ></div>
                                                <div class="review-rating float-left mt-2" style="display:none">
                                                    <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                                                    <span class="rate primary-bgcolor" ></span>
                                                </div>
                                            </div>
                                            <ul class="ui-list-features list-unstyled">
                                                <li>
                                                    <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" >
                                                        <i class="icon icon-images"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false">
                                                        <i class="icon icon-map-marked-alt"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false">
                                                        <i class="icon icon-more"></i>
                                                    </a>
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator price-wrapper">
                                            <div class="ui-list-price text-center">
                                                <%--<span class="price strikeout d-inline-block d-sm-block"><del><em class="currency">AED </em>1177</del></span>--%>
                                                <span class="price d-inline-block d-sm-block"></span>
                                                <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0">Book Transfer</button>
                                            </div>
                                        </div>
                                    </div>
                                </div> 
							</li>
							<li class="item">
								 <div >
                                    <div class="row no-gutters">
                                        <div class="col-2 col-sm image-wrapper">
                                            <img src="build/img/hotel-full-demo.jpg" alt="Hotel Name" class=" h-100">
                                        </div>
                                        <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column text-content-wrapper">
                                            <h4 ></h4>
                                            <p class="address mb-0" ></p>
                                            <div class="mb-auto mt-2">
                                                <div ></div>
                                                <div class="review-rating float-left mt-2" style="display:none">
                                                    <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                                                    <span class="rate primary-bgcolor" ></span>
                                                </div>
                                            </div>
                                            <ul class="ui-list-features list-unstyled">
                                                <li>
                                                    <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" >
                                                        <i class="icon icon-images"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false">
                                                        <i class="icon icon-map-marked-alt"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false">
                                                        <i class="icon icon-more"></i>
                                                    </a>
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator price-wrapper">
                                            <div class="ui-list-price text-center">
                                                <%--<span class="price strikeout d-inline-block d-sm-block"><del><em class="currency">AED </em>1177</del></span>--%>
                                                <span class="price d-inline-block d-sm-block"></span>
                                                <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0" >Book Transfer</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
							</li>		
                   			<li class="item">		
                                  <div >
                                    <div class="row no-gutters">
                                        <div class="col-2 col-sm image-wrapper">
                                            <img src="build/img/hotel-full-demo.jpg" alt="Hotel Name" class=" h-100">
                                        </div>
                                        <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column text-content-wrapper">
                                            <h4 ></h4>
                                            <p class="address mb-0" ></p>
                                            <div class="mb-auto mt-2">
                                                <div ></div>
                                                <div class="review-rating float-left mt-2" style="display:none">
                                                    <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                                                    <span class="rate primary-bgcolor" ></span>
                                                </div>
                                            </div>
                                            <ul class="ui-list-features list-unstyled">
                                                <li>
                                                    <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" >
                                                        <i class="icon icon-images"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false">
                                                        <i class="icon icon-map-marked-alt"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false">
                                                        <i class="icon icon-more"></i>
                                                    </a>
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator price-wrapper">
                                            <div class="ui-list-price text-center">
                                                <%--<span class="price strikeout d-inline-block d-sm-block"><del><em class="currency">AED </em>1177</del></span>--%>
                                                <span class="price d-inline-block d-sm-block"></span>
                                                <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0">Book Transfer</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>  
							</li>	
							<li class="item">
                               <div >
                                    <div class="row no-gutters">
                                        <div class="col-2 col-sm image-wrapper">
                                            <img src="build/img/hotel-full-demo.jpg" alt="Hotel Name" class=" h-100">
                                        </div>
                                        <div class="col-10 col-sm-7 col-md-6 pl-4 py-3 d-flex justify-content-between flex-column text-content-wrapper">
                                            <h4 ></h4>
                                            <p class="address mb-0" ></p>
                                            <div class="mb-auto mt-2">
                                                <div></div>
                                                <div class="review-rating float-left mt-2" style="display:none">
                                                    <img src="build/img/trustyou-logo-sm.jpg" alt="TrustYou Reviews" class="trust-logo">
                                                    <span class="rate primary-bgcolor" ></span>
                                                </div>
                                            </div>
                                            <ul class="ui-list-features list-unstyled">
                                                <li>
                                                    <a class="ui-list-btn" data-toggle="pill" href="#pills-gallery-tab-2" role="tab" aria-controls="pills-gallery-tab-1" aria-selected="true" >
                                                        <i class="icon icon-images"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="ui-list-btn" data-toggle="pill" href="#pills-location-tab-2" role="tab" aria-controls="pills-location-tab-1" aria-selected="false">
                                                        <i class="icon icon-map-marked-alt"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a class="ui-list-btn" data-toggle="pill" href="#pills-overview-tab-2" role="tab" aria-controls="pills-overview-tab-1" aria-selected="false">
                                                        <i class="icon icon-more"></i>
                                                    </a>
                                                </li>
                                            </ul>

                                        </div>
                                        <div class="col-12 col-sm-3 d-flex justify-content-center align-items-center dotted-separator price-wrapper">
                                            <div class="ui-list-price text-center">
                                                <%--<span class="price strikeout d-inline-block d-sm-block"><del><em class="currency">AED </em>1177</del></span>--%>
                                                <span class="price d-inline-block d-sm-block"></span>
                                                <button class="btn btn-primary font-weight-bold mb-3 ml-3 mb-md-0">Book Transfer</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>  								       
							</li>	
				
						</ul>
                        <div class="inn_wrap01" id="Noresults" style=" display:none;">
                            <div class="ns-h3">
                                No Results found
                            </div>
                            <div style="text-align: center; font-weight: bold">
                                Sorry we didn't find any transfer vehicle, please contact our Support Team.
                            </div>
                            <div class="clear">
                            </div>
                        </div>
                        <div class="ui-listing-wrapper" id="list-group-wrap">
                           <ul class="list-unstyled" id = "list-group" >
                            </ul>
                        </div>
                    
                    </div>
                </div>
            </div>
        </div>
     
     <script>
        var AgentId = JSON.parse(document.getElementById('<%=hdnAgentId.ClientID %>').value);
        var UserId = JSON.parse(document.getElementById('<%=hdnUserId.ClientID %>').value);
        var BehalfLocation = JSON.parse(document.getElementById('<%=hdnBehalfLocation.ClientID %>').value);
        var apiUrl = '<%=System.Configuration.ConfigurationManager.AppSettings["TransferWebApiUrl"]%>';
         $(function () {

             $('#normalSearchFields').on('click', function () {
                 var htlHddDiv = $('#advancedSearchFields');
                 if (htlHddDiv.css('display') == 'none') {
                     htlHddDiv.fadeIn();
                 }

                 if ($('#divCorpFields').css('display') == 'none' && document.getElementById('<%=hdnCorpInfo.ClientID %>').value != '') {
                     $('#divCorpFields').fadeIn();
                 }
             })
         });
         var agentType = document.getElementById('<%=hdnAgentType.ClientID %>').value;         
         if (agentType != "1" && agentType != "2") {
             $("#supplierDiv").hide();
         }
         
    </script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyC0mr4cq7HrZgJb9TZMLYWk-yh-_Ahg9Ro&libraries=places&callback=initAutocomplete" async defer></script>
    <script src="build/js/nprogress.js"></script>
    <script src="scripts/Transfers/transferRequest.js" type="text/javascript"></script>
    </asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
