using System;
using System.Data;
using System.Linq;
using System.Configuration;
using System.Web.UI;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;

public partial class TransactionBEUI : System.Web.UI.MasterPage
{
    private bool _pageRole = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //smTransaction.AsyncPostBackTimeout
            lblError.Visible = false;
            //Response.CacheControl = "no-cache";
            Response.AddHeader("Pragma", "no-cache");//over secure connection
            Response.Expires = -1;////over non-secure connection
            SetSessionExpires();
            LoginInfo loginInfo = Settings.LoginInfo;
            if (loginInfo == null) Response.Redirect("SessionExpired.aspx");
            //-------------------------------------------------------------------
            // Validating the user whether he is an authorized user or not
            //-------------------------------------------------------------------
            //lblAgentBalance.Text = loginInfo.Currency +" "+ loginInfo.AgentBalance.ToString("N" + loginInfo.DecimalValue);//Formatter.ToCurrency(loginInfo.AgentBalance);
            if (loginInfo.IsOnBehalfOfAgent)
            {
                AgentMaster OBAgent = null;
                //if (!IsPostBack)
                //{
                    OBAgent = new AgentMaster(loginInfo.OnBehalfAgentID);
                //}
                //commented by brahmam  17.08.2016
                lblOBAgentBalance.Visible = true;
                lblOBLocation.Visible = true;
                lblOBUserName.Visible = true;
                lblOBAgentBalance.Text = "Cr.Balance : " + OBAgent.AgentCurrency + " " + OBAgent.CurrentBalance.ToString("N" + loginInfo.OnBehalfAgentDecimalValue);
                lblOBLocation.Text = OBAgent.City;
                lblOBUserName.Text = "Booking on behalf of " + OBAgent.Name;
                
            }

            //if (loginInfo.AgentId > 0)// TODO ziya, this dall should be avoided from here as calling DB always
            //{
            //    //Loading Current Balance
            //    decimal currentBal = AgentMaster.UpdateAgentBalance(loginInfo.AgentId, 0, (int)loginInfo.UserID);
            //    //decimal currentBal = CT.Core.Supplier.UpdateAgentBalanceTemp(loginInfo.AgentId, 0, (int)loginInfo.UserID);// TEMP
            //    Settings.LoginInfo.AgentBalance = currentBal;
            //}
            lblLocation.Text = string.Format("{0} ", loginInfo.LocationName);
            lblUserName.Text = string.Format(" {0} / {1} ", loginInfo.AgentName, loginInfo.FullName);
            lblAgentBalance.Text = loginInfo.AgentBalance.ToString("N" + loginInfo.DecimalValue);
            lblCurrency.Text = loginInfo.Currency;

            if (!IsPostBack)
            {
                string sAccessFlag = Convert.ToString(ConfigurationManager.AppSettings["CheckAccess"]);
                //if (sAccessFlag == "NO" && Request.Url.AbsolutePath != "/ErrorPage.aspx" &&
                //    Settings.roleFunctionList.Where(x => ("/" + x).ToUpper().Contains(Request.Url.AbsolutePath.ToUpper())).Count() == 0)
                //    Response.Redirect("ErrorPage.aspx?Err=Access Denied", false);

                //lblGrandTotal.Visible=lblGrandText.Visible = false;
                //LoginInfo loginInfo = Settings.LoginInfo;
                if (loginInfo == null)
                {
                    Response.Redirect("SessionExpired.aspx");
                    // Response.Redirect(string.Format("ErrorPage.aspx?Err={0}", GetGlobalResourceObject("ErrorMessages", "INVALID_USER")));
                }
                else if (sAccessFlag == "Y" && _pageRole && !isValidPage() )
                {
                    //Response.Redirect("SessionExpired.aspx?auth=false");
                    Response.Redirect("UnauthorizedAccess.aspx");
                    
                }
                if (!string.IsNullOrEmpty(Settings.LoginInfo.CopyRight))
                {
                    lblCopyRight.Text = Settings.LoginInfo.CopyRight;
                }
                MenuList menu = new MenuList();
                ltrMenuList.Text = menu.ExecuteXSLTransformation();
                

                if (loginInfo.AgentId > 1)
                {
                    string logoPath = ConfigurationManager.AppSettings["AgentImage"] + Settings.LoginInfo.AgentLogoPath;
                    imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + logoPath;
                    
                    //System.Drawing.Image img = System.Drawing.Image.FromFile(logoPath, true);
                    //logoPath = new AgentMaster(Settings.LoginInfo.AgentId).ImgFileName;
                    //imgLogo.ImageUrl = new Uri(System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + logoPath, false).AbsoluteUri;

                    //if (img.Width > 996)
                    //{
                    //    imgLogo.Width = new Unit(996, UnitType.Pixel);
                    //}
                    //if (img.Height > 39)
                    //{
                    //    imgLogo.Height = new Unit(39, UnitType.Pixel);
                    //}
                }
               
                //lblAgentBalance.Text = Formatter.ToCurrency(loginInfo.AgentBalance);

                //lblUserName.Text = string.Format(" {0} {1} ", loginInfo.FirstName, loginInfo.LastName);
                //hyplVisaFeeMaster.Visible = true;
                //hyplVisaSales.Visible = true;
                //hyplVisaSalesList.Visible = true;
                //hypldispatch2SP.Visible = true;
                //hypldispatch2BO.Visible = true;
                //hypldispatch2OP.Visible = true;
                //hyplVisaApprove.Visible = true;
                //hyplVisaReceive.Visible = true;
                //hyplVisaHistory.Visible = true;
                //hyplEditVisa.Visible = true;
                //hypldispatch2AW.Visible = true;
                //hyplVisaQuery.Visible = true;
                

            //    switch (loginInfo.MemberType)
            //    {
            //        case MemberType.ADMIN:
            //            break;
            //        case MemberType.CASHIER:
            //            hyplVisaFeeMaster.Visible = false;
            //            //hyplVisaSales.Visible = false;
            //            //hyplVisaSalesList.Visible = false;
            //            hypldispatch2SP.Visible = false;
            //            hypldispatch2BO.Visible = false;
            //            hypldispatch2OP.Visible = false;
            //            hyplVisaApprove.Visible = false;
            //            hyplVisaReceive.Visible = false;
            //            hypldispatch2AW.Visible = false;
            //            //hyplVisaQuery.Visible = false;
                        

            //            break;
            //        case MemberType.OPERATIONS:
            //            hyplVisaFeeMaster.Visible = false;
            //            //hyplVisaSales.Visible = false;
            //            //hyplVisaSalesList.Visible = false;
            //            //hypldispatch2SP.Visible = false;
            //            hypldispatch2BO.Visible = false;
            //            hypldispatch2OP.Visible = false;
            //            hyplVisaApprove.Visible = false;
            //            //hyplVisaHistory.Visible = true;
            //            hypldispatch2AW.Visible = false;
            //            //hyplVisaQuery.Visible = false;
            //            break;
            //        case MemberType.BACKOFFICE:
            //            hyplVisaFeeMaster.Visible = false;
            //            hyplVisaSales.Visible = false;
            //            //hyplVisaSalesList.Visible = false;
            //            hypldispatch2SP.Visible = false;
            //            hypldispatch2BO.Visible = false;
            //            //hyplVisaQuery.Visible = false;
            //            //hypldispatch2OP.Visible = false;
            //            //hyplVisaApprove.Visible = false;
            //            //hyplVisaHistory.Visible = true;
            //            break;
            //        case MemberType.SUPERVISOR:
            //            hyplVisaFeeMaster.Visible = false;
            //            hypldispatch2AW.Visible = false;
            //            break;
            //    }

            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, "Transaction Visa Title");
        }
    }
    private void SetSessionExpires()
    {

        //time to redirect, 5 milliseconds before session ends
        int int_MilliSecondsTimeOut = (this.Session.Timeout * 60 * 1000) - 5;

        string str_Script = @"
            var myTimeOut;             
            clearTimeout(myTimeOut);            
            var sessionTimeout = " + int_MilliSecondsTimeOut.ToString() + ";" +
            "function doRedirect(){ window.location.href='SessionExpired.aspx'; }" + @"            
            myTimeOut=setTimeout('doRedirect()', sessionTimeout); ";

        ScriptManager.RegisterClientScriptBlock(this.Page, this.GetType(),
              "CheckSessionOut", str_Script, true);
    }

    # region Content Search
    public void butSearchClose_Click(object sender, EventArgs e)
    {
        pnlSearch.Visible = false;
        //cphSearch.Visible = false;
    }
    public void ShowSearch(string title)
    {
        //lblTitleSearch.Text = title;
        //cphSearch.Visible = true;
        pnlSearch.Visible = true;
    }
    public void HideSearch()
    {
        //cphSearch.Visible = false;
        pnlSearch.Visible = false;
    }
    # endregion


    # region Page Validation
    private bool isValidPage()
    {
        //int removeIndex = Page.AppRelativeVirtualPath.IndexOf("/") + 1;
        //string pageName = string.Empty;
        //if (removeIndex > 0)
        //{
        //    pageName = Page.AppRelativeVirtualPath.Substring(removeIndex, Page.AppRelativeVirtualPath.Length - removeIndex);
        //    if (!string.IsNullOrEmpty(Page.ClientQueryString))
        //    {
        //        pageName = pageName + "?" + Page.ClientQueryString;
        //    }
        //}
       // commenting by ziyad for temp

       string pageName = string.Empty;
       pageName = System.IO.Path.GetFileName(Request.Url.AbsolutePath).Replace(".aspx",string.Empty);//Checking only page name ignoring query string
        //return (Settings.roleFunctionList.Where(x => x.ToUpper().Contains(pageName.ToUpper())).Count() > 0);
        
        return pageName=="Errorpage"?true: (Settings.MenuList.Tables[0].AsEnumerable().Where(row => Utility.ToString(row["FUNC_PAGE_LINK"]).ToUpper().Contains(pageName.ToUpper())).Count() > 0);
       
    }
    private bool isValidPageORG()
    {
        //int removeIndex=Page.AppRelativeVirtualPath.IndexOf("/")+1;
        //string pageName=string.Empty;
        //if (removeIndex > 0)
        //{
        //    pageName = Page.AppRelativeVirtualPath.Substring(removeIndex, Page.AppRelativeVirtualPath.Length - removeIndex);
        //    if(!string.IsNullOrEmpty(Page.ClientQueryString))
        //    {
        //        pageName=pageName + "?" + Page.ClientQueryString;
        //    }
        //}
        // commenting by ziyad for temp
        //string pageName=string.Empty;
        //pageName=System.IO.Path.GetFileName(Request.Url.ToString());
        //if (Settings.roleFunctionList.Contains(pageName.ToUpper()))
        //    return true;
        //else
        //   return  false;
        return true;
    }
    public bool PageRole
    {
        get
        {
            return _pageRole;
        }
        set
        {
            _pageRole = value;
        }
    }

    # endregion
}
