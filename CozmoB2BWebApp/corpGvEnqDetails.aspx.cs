﻿using CT.TicketReceipt.Common;
using CT.GlobalVisa;
using System;
using System.Collections.Generic;

public partial class corpGvEnqDetailsGUI : CT.Core.ParentPage
{
    protected GVEnquiryRequest enqRequestSession = new GVEnquiryRequest();
    protected GVEnquiry gvEnquiryDet = new GVEnquiry();
    protected GVTermsMaster gvTerms = new GVTermsMaster();
    protected List<GVFeeMaster> gvFeeList = new List<GVFeeMaster>();
    protected List<GVHandlingFeeMaster> gvHandlingFee = new List<GVHandlingFeeMaster>();
    protected GVRequirementMaster reqMaster = new GVRequirementMaster();
    protected string errormessage = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                if (Session["GVEnqSession"] != null)
                {
                    enqRequestSession = Session["GVEnqSession"] as GVEnquiryRequest;
                    if (enqRequestSession != null)
                    {
                        gvEnquiryDet = GVVisaSale.GetEnquiryDetails(enqRequestSession.TravelToCountryCode, enqRequestSession.NationalityCode, Utility.ToInteger(enqRequestSession.VisaTypeId), enqRequestSession.ResidenseCode,CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId); 
                        if (gvEnquiryDet != null && gvEnquiryDet.GvFeeList != null && (gvEnquiryDet.GvRequirements != null || gvEnquiryDet.GvTerms != null))
                        {
                            if (gvEnquiryDet.GvRequirements != null)
                            {
                                reqMaster = gvEnquiryDet.GvRequirements;
                            }
                            if (gvEnquiryDet.GvTerms != null)
                            {
                                gvTerms = gvEnquiryDet.GvTerms;
                            }
                            if (gvEnquiryDet.GvFeeList != null)
                            {
                                gvFeeList = gvEnquiryDet.GvFeeList;
                            }
                            if (gvEnquiryDet.GVHandlFee != null)
                            {
                                gvHandlingFee = gvEnquiryDet.GVHandlFee;
                            }
                        }
                        else
                        {
                            errormessage = "Enquiry details Not Found!";
                        }
                    }
                    else
                    {
                        errormessage = "Enquiry details Not Found!";
                    }
                    Session["GVEnqSession"] = null;
                }
                else
                {
                    Response.Redirect("corpGvEnqRequest.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            errormessage = "Enquiry details Not Found! exception :" + ex.Message;
            Session["GVEnqSession"] = null;
        }
    }
}
