﻿using System;
using CT.Core;
using System.Web.UI;
using System.Web.UI.WebControls;
using Visa;
using System.IO;
using CT.TicketReceipt.BusinessLayer;

public partial class AddVisaType : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            AuthorizationCheck();
            if (!IsPostBack)
            {
                Page.Title = "Add New Visa Type";
                InitializePageControls();

                if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
                {
                    int Id;
                    bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out Id);
                    if (!isnumerice)
                    {
                        Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                        Response.End();

                    }
                    try
                    {
                        VisaType visaType = new VisaType(Id);
                        if (visaType != null)
                        {
                            txtVisaTypeName.Text = visaType.VisaTypeName;
                            txtDescription.Text = visaType.Description;
                            txtProcessingDays.Text = Convert.ToString(visaType.ProcessingDays);
                            txtvalidityDays.Text = Convert.ToString(visaType.ValidityDays);
                            txtLivingValidity.Text = Convert.ToString(visaType.LivingValidity);
                            ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(visaType.CountryCode));
                            txtVisaTypeNameAR.Text = visaType.VisaTypeNameAR;
                            txtDescriptionAR.Text = visaType.DescriptionAR;
                            btnSave.Text = "Update";
                            Page.Title = "Update Visa Type";
                            lblStatus.Text = "Update Visa Type";
                            //RequiredFieldValidator6.Enabled = false;
                            ddlAgent.SelectedValue = Convert.ToString(visaType.AgentID);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaType.aspx,Err:" + ex.Message, "");
                    }
                }
                else
                {
                    //ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByText("UAE"));
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa,Severity.High, 0, "AddVisaType Page:"+ex.Message, "");
        }
    }

    private void InitializePageControls()
    {
        try
        {
            BindCountry();
            BindAgent();
        }
        catch 
        {
            throw;
        }
    }

    private void BindCountry()
    {
        ddlCountry.DataSource = VisaCountry.GetActiveVisaCountryList();
        ddlCountry.DataTextField = "countryName";
        ddlCountry.DataValueField = "countryCode";
        ddlCountry.DataBind();
        ddlCountry.Items.Insert(0, new ListItem("--Select--", "-1"));
    }

    private void BindAgent()
    {
        ddlAgent.DataSource = AgentMaster.GetB2CAgentList();
        ddlAgent.DataTextField = "agent_name";
        ddlAgent.DataValueField = "agent_id";
        ddlAgent.DataBind();
        ddlAgent.Items.Insert(0, new ListItem("--Select--", "-1"));
    }


    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            string docPath = "";
            string appPath = "";
            string fileExt = "";
            VisaType visaType = new VisaType();
            visaType.VisaTypeName = txtVisaTypeName.Text.Trim();
            visaType.CountryCode = ddlCountry.SelectedValue;
            visaType.ValidityDays = Convert.ToInt32(txtvalidityDays.Text.Trim() != string.Empty ? txtvalidityDays.Text.Trim() : "0");
            visaType.LivingValidity = Convert.ToInt32(txtLivingValidity.Text.Trim() != string.Empty ? txtLivingValidity.Text.Trim() : "0");
            visaType.ProcessingDays = Convert.ToInt32(txtProcessingDays.Text.Trim());
            visaType.VisaTypeNameAR = txtVisaTypeNameAR.Text.Trim();
            visaType.DescriptionAR = txtDescriptionAR.Text.Trim();
            if (visaIconFileUpload.HasFile)
            {
                docPath = "VisaIcon";
                appPath = Server.MapPath(docPath);
                if (!Directory.Exists(appPath))
                {
                    Directory.CreateDirectory(appPath);
                }

                fileExt = Path.GetExtension(visaIconFileUpload.FileName).ToLower();
                // string fileName = Path.GetFileName(file.FileName);

                visaType.VisaIconPath = docPath + "/" + fileExt;
            }
            else
            {
                visaType.VisaIconPath = "DummyImage.jpg";
            }
            visaType.AgentID = Convert.ToInt32(ddlAgent.SelectedValue);

            visaType.Description = txtDescription.Text.Trim();

            if (btnSave.Text == "Update")
            {
                if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
                {

                    visaType.VisaTypeId = Convert.ToInt32(Request.QueryString[0]);
                    visaType.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                    visaType.Save();
                    if (visaType.VisaIconPath != "")
                    {
                        visaIconFileUpload.SaveAs(appPath + "/" + Request.QueryString[0] + fileExt);
                    }
                }
            }
            else
            {

                visaType.CreatedBy = (int)Settings.LoginInfo.UserID;
                visaType.IsActive = true;
                visaType.Save();
                visaIconFileUpload.SaveAs(appPath + "/" + visaType.VisaTypeId + fileExt);
            }

            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaType.aspx,Err:" + ex.Message, "");
        }
        Response.Redirect("VisaTypeList.aspx?Country=" + ddlCountry.SelectedValue);

    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
