using System;
using System.Configuration;
using System.Collections;
using CT.BookingEngine;
using CT.Core;
using System.Collections.Generic;

using CT.TicketReceipt.BusinessLayer;
using CT.MetaSearchEngine;

public partial class ReleaseSeat : System.Web.UI.Page
{
    protected UserMaster loggedMember;
    protected BookingDetail booking;
    private string from = ConfigurationManager.AppSettings["fromEmail"];
    private FlightItinerary itinerary;
    protected void Page_Load(object sender, EventArgs e)
    {
        
        int bookedBy = 0;
        string pnr = string.Empty;
        string result = string.Empty;
        string message = string.Empty;
        string remarks = string.Empty;
        bool released = false;
        bool alreadyCancelled = false;
        // Checking for logged in user and authentication
        //if (Session["roleId"] == null || Session["memberId"] == null)
        //{
        //    message = "Your login Session seems expired. Re-Login to continue.";
        //}
        //else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.CancelFlightBookings))
        //{
        //    message = "You do not have authority to Release Seat.";
        //}
        //else
        {
            try
            {
                pnr = Request["pnr"];
                loggedMember = new UserMaster(Settings.LoginInfo.UserID);
                int bookingId = Convert.ToInt32(Request["bookingId"]);
                if (Request["remarks"] != null)
                {
                    remarks = Request["remarks"];
                    if (remarks.ToUpper() == "BOOKEDOTHER")
                    {
                        Session.Remove("bookedOtherSessionAlive");
                    }
                }
                if (Request["setStatus"] != null && Convert.ToBoolean(Request["setStatus"]))
                {
                    CT.Core.Queue.SetStatus(QueueType.Booking, bookingId, QueueStatus.Suspended, (int)loggedMember.ID, remarks);
                }
                else
                {
                    booking = new BookingDetail(bookingId);
                    if (booking.LockedBy > 0)
                    {
                        string lockedByUser = "you";
                        if (booking.LockedBy != (int)loggedMember.ID)
                        {
                            lockedByUser = loggedMember.FirstName + " " + loggedMember.LastName;
                        }
                        message = pnr + " can not be released. Booking is locked by " + lockedByUser + ".";
                    }
                    else if (booking.Status != BookingStatus.Ready && booking.Status != BookingStatus.Hold)
                    {
                        message = pnr + " can not be released. The current status of booking has been changed. Please refresh the page";
                        //TODO: this error message is true till we allow release for Ready and Hold bookings only.
                    }
                    else if (booking.Status == BookingStatus.Released)
                    {
                        message = pnr + " has already been cancelled. Please refresh your page.";
                    }
                    else
                    {
                        List<Ticket> ticket = Ticket.GetTicketList(FlightItinerary.GetFlightId(pnr));
                        if (ticket.Count > 0)
                        {
                            message = pnr + " can not be cancelled. Ticketing already in progress.";
                        }
                        else
                        {
                            itinerary = new FlightItinerary(FlightItinerary.GetFlightId(pnr));
                            string response = string.Empty;
                            //if (itinerary.FlightBookingSource == BookingSource.UAPI)
                            //    response = MetaSearchEngine.CancelItinerary(itinerary.UniversalRecord, itinerary.FlightBookingSource);
                            //else
                            //    response = MetaSearchEngine.CancelItinerary(pnr, itinerary.FlightBookingSource);
                            MetaSearchEngine mse = new MetaSearchEngine(Session.SessionID);
                            mse.SettingsLoginInfo = Settings.LoginInfo;
                            //Implemented by brahmam
                            if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                            {                                
                                response = mse.CancelItinerary(itinerary.AirLocatorCode.TrimEnd('|') + "+" + itinerary.UniversalRecord, itinerary.FlightBookingSource);
                                if (response == itinerary.AirLocatorCode)
                                {
                                    response = pnr;
                                }
                                else
                                {
                                    if (!string.IsNullOrEmpty(response) )
                                    {
                                        if (!string.IsNullOrEmpty(pnr) )
                                        {
                                            response = pnr.Split('|')[0];
                                        }
                                    }
                                }
                            }
                            else if (itinerary.FlightBookingSource == BookingSource.PKFares)
                            {
                                PKFARES.API api = new PKFARES.API();
                                api.AppUserID = (int)Settings.LoginInfo.UserID;
                                api.SessionID = Guid.NewGuid().ToString();

                                if(api.CancelBooking(itinerary.TicketAdvisory, itinerary.PNR))
                                {
                                    response = itinerary.PNR;
                                }
                            }
                            //Added by lokesh on 30-05-2018.
                            //In order to cancel to the booking for amadeus flight source.
                            //This method is called when the booking happened but not ticketed.
                            else if (itinerary.FlightBookingSource == BookingSource.Amadeus)
                            {
                                pnr = itinerary.PNR;
                                response = mse.CancelItinerary(pnr, itinerary.FlightBookingSource);
                            }
                            else
                            {
                                response = mse.CancelItinerary(pnr, itinerary.FlightBookingSource);
                            }

                            BookingHistory bh = new BookingHistory();
                            bh.BookingId = booking.BookingId;
                            bh.EventCategory = EventCategory.Booking;
                            bh.Remarks = "Booked seat are released pnr: " + pnr + " (IP Address: " + Request["REMOTE_ADDR"] + ")";
                            bh.CreatedBy = (int)Settings.LoginInfo.UserID;
                            bh.Save();
                            if (response == pnr)
                            {
                                released = true;
                            }
                            if (released)
                            {
                                message = "Seat released successfully for " + pnr + ".";
                                UserPreference preference = new UserPreference();
                                string subject = "Booking Cancelled - PNR(" + pnr + ")";
                                string flightItinerary = string.Empty;
                                for (int i = 0; i < itinerary.Segments.Length; i++)
                                {
                                    flightItinerary = flightItinerary + "-" + itinerary.Segments[i].Origin.AirportCode;
                                }
                                try
                                {
                                    int agencyId = booking.AgencyId;
                                    bookedBy = (int)Settings.LoginInfo.UserID;
                                    preference = preference.GetPreference(bookedBy, preference.BookingCancel, preference.EmailNotification);
                                }
                                catch (ArgumentException)
                                {
                                    preference.Value = "False";
                                }
                                if (Convert.ToBoolean(preference.Value))
                                {
                                    if (Convert.ToBoolean(ConfigurationManager.AppSettings["isSendEmail"]))
                                    {
                                        Hashtable table = new Hashtable(3);
                                        table.Add("firstName", loggedMember.FirstName);
                                        table.Add("lastName", loggedMember.LastName);
                                        table.Add("time", DateTime.UtcNow.Add(new TimeSpan(4, 0, 0)).ToString("hh:mm:ss tt"));
                                        table.Add("date", DateTime.UtcNow.Add(new TimeSpan(4, 0, 0)).ToString("dd/MMM/yyyy"));
                                        table.Add("pnr", pnr);
                                        table.Add("paxName", itinerary.Passenger[0].FullName);
                                        table.Add("travelDate", itinerary.Segments[0].DepartureTime.ToString("dd/MMM/yyyy"));
                                        table.Add("flight", itinerary.Segments[0].Airline + itinerary.Segments[0].FlightNumber);
                                        table.Add("itinerary", flightItinerary);
                                        //preferene is getting wriiten again here
                                        preference = preference.GetPreference(bookedBy, preference.AlternateEmail, preference.AlternateEmail);
                                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                                        if (preference.Value != null)
                                        {
                                            toArray.Add(preference.Value);
                                        }
                                        else
                                        {
                                            toArray.Add(Settings.LoginInfo.AgentEmail);
                                        }
                                        try
                                        {
                                            Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, subject, ConfigurationManager.AppSettings["bookingCancel"], table);
                                        }
                                        catch (System.Net.Mail.SmtpException)
                                        {
                                            message += " Email not sent.";
                                            Audit.Add(EventType.Email, Severity.Normal, 0, "Smtp is unable to send the message", "");
                                        }
                                    }
                                }
                            }
                            else
                            {
                                message = response;
                                if (itinerary.FlightBookingSource == BookingSource.TBOAir && !string.IsNullOrEmpty(response))
                                {
                                    message = "Seat released only for " + response + ",other PNR is failed";
                                }
                                else
                                {
                                    message = "Releasing seats failed for " + pnr;
                                }
                            }
                            if (response == "ALREADY CANCELLED" || response.IndexOf("already been cancelled") != -1)
                            {
                                alreadyCancelled = true;
                                message += ". Booking is removed from Queue. PNR = " + pnr + ".";
                            }
                            if (released || alreadyCancelled)
                            {
                                // Alter Credit starts
                                int flightId = FlightItinerary.GetFlightId(pnr);
                                FlightPassenger[] flightPassenger = FlightPassenger.GetPassengers(flightId);
                                double totalPrice = 0;
                                foreach (FlightPassenger fp in flightPassenger)
                                {
                                    totalPrice = totalPrice + Convert.ToDouble(fp.Price.GetAgentPrice());
                                }
                                //if (bookingInstance.Status != BookingStatus.Hold)
                                //{
                                //    Agency.AlterCredit(totalPrice, booking.AgencyId, true);
                                //}
                                decimal ticketAmount = 0;
                                foreach (FlightPassenger pax in itinerary.Passenger)
                                {
                                    if (itinerary.Passenger[0].Price.NetFare > 0)
                                    {
                                        ticketAmount += pax.Price.NetFare + pax.Price.Tax + pax.Price.Markup;
                                    }
                                    else
                                    {
                                        ticketAmount += pax.Price.PublishedFare + pax.Price.Tax;
                                    }
                                }

                                //Updating Agent Balance
                                //Settings.LoginInfo.AgentBalance += ticketAmount;
                                //AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
                                //agent.CreatedBy = Settings.LoginInfo.UserID;
                                //agent.UpdateBalance(ticketAmount);
                                // Alter Credit ends
                                BookingDetail.SetBookingStatus(bookingId, BookingStatus.Released, (int)loggedMember.ID);
                            }
                        }
                    }
                }
            }
            catch (NullReferenceException)
            {
                message = "Releasing seat failed for " + pnr + ".";
            }
            catch (BookingEngineException)
            {
                message = "Releasing seat failed for " + pnr + ".";
            }
            catch (ArgumentException)
            {
                message = "Releasing seat failed for " + pnr + ".";
            }
        }
        if (Request["redirectUrl"] != null && Request["redirectUrlOnFail"] != null)
        {
            if (released)
            {
                string url = Request["redirectUrl"] + "?message=" + message;
                Response.Redirect(url, true);
            }
            else
            {
                string url = Request["redirectUrlOnFail"] + "?bookingId=" + booking.BookingId + "&message=" + message;
                Response.Redirect(url, true);
            }
        }
        else
        {
            if (released)
            {
                result = "1|";
            }
            else
            {
                result = "0|";
            }
            if (pnr.Contains("|"))
            {
                result += pnr.Replace('|', ',');
                if (message.Contains("|"))
                {
                    result += "|" + message.Replace('|', ',');
                }
                else
                {
                    result += "|" + message;
                }
            }
            else
            {
                result += pnr + "|" + message;
            }
            Response.Write(result);
        }
    }
}
