<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/TransactionBE.master" Inherits="AutoTicket" Codebehind="AutoTicket.aspx.cs" %>
<%@ Import Namespace="CT.Configuration" %>
<asp:Content ID="Content1" runat="Server" ContentPlaceHolderID="cphTransaction">
    <asp:MultiView ID="ErrorView" runat="server">
        <%-- View 0 --%>
        <asp:View ID="GeneralError" runat="server">
            <div class="width-720 margin-left-30" style="text-align: center">
                <br />
                <br />
                <br />
                <span>
                    <% = errorMessage %>
                </span>
                <br />
                <br />
                <%if (response.Status != CT.BookingEngine.TicketingResponseStatus.NotSaved)
                  { %>
                <span>
                    Go to <a href="<% = queuePage %>">Booking Queue</a> or <a href="ViewBookingForTicket.aspx?bookingId=<% = booking.BookingId %>">View Booking</a>
                    <% //TODO: Navigation links here. %>
                </span>
                <%}
                  else
                  { %>
                <span>Go to <a href="FailedBookingQueue.aspx">Pending Queue</a>
                        <% //TODO: Navigation links here. %>
                </span>
                <%} %>
                <br />
                <br />
                <span>
                    For further assistance contact <%= CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"]  %>.
                </span>
                <br />
                <br />
            </div>
            <div class="width-720 margin-left-30 center"><a href="javascript:history.go(-1);"><< Back to previous page</a></div>
            <br /><br />
       </asp:View>
        <%-- View 1 --%>
        <asp:View ID="UnhandledError" runat="server">
            <div class="width-720 margin-left-30" style="text-align: center">
                <br />
                <br />
                <br />
                <br />
                <span>
                    Unknown Error.
                </span>
                <br />
                <br />
                <span>
                    Go to <a href="<% = queuePage %>">Booking Queue</a>
                </span>
                <br />
                <br />
                <span>
                    For assistance contact <%= CT.Configuration.ConfigurationSystem.LocaleConfig["CompanyName"]  %>.
                </span>
                <br />
                <br />
            </div>
            <div class="width-720 margin-left-30 center"><a href="javascript:history.go(-1);"><< Back to previous page</a></div>
            <br /><br />
        </asp:View>
    </asp:MultiView>
</asp:Content>
