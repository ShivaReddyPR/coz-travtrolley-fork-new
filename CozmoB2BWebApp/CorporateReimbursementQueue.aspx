<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorporateReimbursementQueueUI"
    Title="Reimbursement Queue" Codebehind="CorporateReimbursementQueue.aspx.cs" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript">

        function isNumber(evt) {
            evt = (evt) ? evt : window.event;
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode > 31 && (charCode < 46 || charCode > 57)) {
                return false;
            }
            return true;
        }


        



        function getValue(rowId, action) {

            var id = rowId.substring(0, rowId.lastIndexOf('_') + 1);
            console.log(id);
            if (action == "U") {

                var amount = document.getElementById(id + 'txtReimAmount').value;
                var balance = document.getElementById(id + 'ITlblBALANCE').innerHTML;

                amount = parseInt(amount);
                balance = parseInt(balance);
                //console.log(balance);
                //console.log(amount);
                if (amount > balance) {
                    document.getElementById(id + 'txtReimAmount').value = '';
                    alert("Entered amount is greater than balance amount !");
                }


            }


        }


        function Validate() {

            var valid = false;
            document.getElementById('errMess').style.display = "none";

            if (document.getElementById('ctl00_cphTransaction_dcReimFromDate_Date').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select from date .";
            }
            else if (document.getElementById('ctl00_cphTransaction_dcReimToDate_Date').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select to date.";
            }

            else {
                valid = true;
            }

            return valid;
        }
    </script>

    <div class="body_container">
        <div class="col-md-12 CorpTrvl-tabbed-panel">
            <h2>
                Reimbursement Queue</h2>
            <div id="errMess" style="display: none; color: Red; font-weight: bold; text-align: center;">
            </div>
            <div class="tab-content responsive">
                <div style="background: #fff; padding: 10px;">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    From Date <span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcReimFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                    </uc1:DateControl>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    To Date<span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcReimToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Select Employee</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlEmployee" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Expense Category</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlExpCategory" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                    <asp:ListItem Value="T" Text="Travel Mode"></asp:ListItem>
                                    <asp:ListItem Value="N" Text="Non Travel Mode"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Expense Type</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlExpType" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="center-block">
                                &nbsp;</label>
                            <asp:Button OnClientClick="return Validate();" OnClick="btnSearch_Click" runat="server"
                                ID="btnSearch" CssClass="btn but_d btn_xs_block cursor_point mar-5" Text="Search" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="table table-responsive  mb-0">
                                 <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="EXP_DETAIL_ID"
                                EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging"
                                OnRowDataBound="gvSearch_RowDataBound">
                                <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="left" />
                                        <ItemTemplate>
                                            <asp:HiddenField ID="IThdfEXPDETAIL_ID" runat="server" Value='<%# Bind("EXP_DETAIL_ID") %>'>
                                            </asp:HiddenField>
                                            <asp:HiddenField ID="hdfClaimAmount" runat="server" Value='<%# Bind("AMOUNT") %>'>
                                            </asp:HiddenField>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="center" />
                                        <HeaderTemplate>
                                            <label style="color: Black">
                                                Select</label>
                                            <asp:CheckBox OnCheckedChanged="ITchkSelect_CheckedChanged" runat="server" AutoPostBack="true"
                                                ID="HTchkSelectAll"></asp:CheckBox>
                                        </HeaderTemplate>
                                        <ItemStyle />
                                        <ItemTemplate>
                                            <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" CssClass="label"></asp:CheckBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtDATE" Width="100px" HeaderText="DATE" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblDATE" runat="server" Text='<%# Eval("DATE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("DATE") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMPLOYEEID" Width="70px" HeaderText="EMPLOYEEID" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMPLOYEEID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_ID") %>' Width="70px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEMPLOYEENAME" Width="100px" HeaderText="EMPLOYEENAME" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEMPLOYEENAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EMP_NAME") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPREF" Width="100px" HeaderText="EXPENSE REF" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEXPREF" runat="server" Text='<%# Eval("EXP_REF") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EXP_REF") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPCAT" Width="100px" HeaderText="EXPENSE CATEGORY" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEXPCAT" runat="server" Text='<%# Eval("EXP_CATEGORY") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EXP_CATEGORY") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtEXPTYPE" Width="100px" HeaderText="EXPENSE TYPE" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblEXPTYPE" runat="server" Text='<%# Eval("EXP_TYPE") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("EXP_TYPE") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtAMOUNT" Width="100px" HeaderText="CLAIM AMOUNT" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblAMOUNT" runat="server" Text='<%# Eval("AMOUNT") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("AMOUNT") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="CURRENCY">
                                        <ItemTemplate>
                                            <asp:DropDownList ID="ddlCurrency" runat="server">
                                            </asp:DropDownList>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="PAY AMOUNT">
                                        <ItemTemplate>
                                            <asp:TextBox runat="server" onkeypress="return isNumber(event);" onChange="getValue(this.id,'U')"
                                                value="0"  ID="txtReimAmount"></asp:TextBox>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtTRAMOUNT" Width="100px" HeaderText="TOTAL REIM. AMOUNT" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblTRAMOUNT" runat="server" Text='<%# Eval("TotalReimbursementAmount") %>'
                                                CssClass="label grdof" ToolTip='<%# Eval("TotalReimbursementAmount") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <HeaderTemplate>
                                            <cc1:Filter ID="HTtxtBALANCE" Width="100px" HeaderText="BALANCE" CssClass="form-control"
                                                OnClick="FilterSearch_Click" runat="server" />
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblBALANCE" runat="server" Text='<%# Eval("Balance") %>' CssClass="label grdof"
                                                ToolTip='<%# Eval("Balance") %>' Width="100px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="STATUS">
                                        <HeaderStyle HorizontalAlign="Left" />
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblStatus" Text="Approved"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="DETAILS">
                                        <ItemTemplate>
                                            <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# Eval("EXP_DETAIL_ID", "~/CorporateViewExpenseDetails.aspx?ExpId={0}") %>'
                                                Text='View Details' />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="pad_10">
                <asp:Button OnClick="btnPay_Click" runat="server" ID="btnPay" CssClass="btn but_d btn_xs_block cursor_point mar-5"
                    Text="PAY NOW" />
                <div class="clearfix">
                </div>
            </div>
        </div>
    </div>
    <!-- Email DIV-->
    <div id='EmailDivEmployee' runat='server' style='width: 100%; display: none;'>
    <%if (detail != null) %>                                        
                          <%{  %>
        <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
        <html>
        <head>
            <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        </head>
        <body>
            <div style="margin: 0; background: #f3f3f3!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; min-width: 100%;
                padding: 0; text-align: left; width: 100%!important">
                <span style="color: #f3f3f3; display: none!important; font-size: 1px; line-height: 1px;
                    max-height: 0; max-width: 0; overflow: hidden"></span>
                <table style="margin: 0; background: #f3f3f3!important; border-collapse: collapse;
                    border-spacing: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                    font-weight: 400; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left;
                    vertical-align: top; width: 100%">
                    <tr style="padding: 0; text-align: left; vertical-align: top">
                        <td align="center" valign="top" style="margin: 0; border-collapse: collapse!important;
                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                            line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top;
                            word-wrap: break-word">
                            <center style="min-width: 580px; width: 100%">
                                <table style="margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0;
                                    float: none; margin: 0 auto; margin-top: 20px; padding: 10px; text-align: center;
                                    vertical-align: top; width: 580px" align="center">
                                    <tbody>
                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                            <td style="margin: 0; border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left;
                                                vertical-align: top; word-wrap: break-word">
                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                    vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <td height="16px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 16px;
                                                                margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0;
                                                    position: relative; text-align: left; vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <th style="margin: 0 auto; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0 auto; padding: 0;
                                                                padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left;
                                                                width: 564px">
                                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                    vertical-align: top; width: 100%">
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                            font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                            <img src="<%=Request.Url.Scheme%>://www.travtrolley.com/images/logo.jpg" style="clear: both; display: block;
                                                                                max-width: 100%; outline: 0; text-decoration: none; width: auto">
                                                                        </th>
                                                                        <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                            font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important; text-align: left;
                                                                            width: 0">
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                    vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <td height="16px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 16px;
                                                                margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                            </td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                                
                                                
                                                <table style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0;
                                                    position: relative; text-align: left; vertical-align: top; width: 100%">
                                                    <tbody>
                                                        <tr style="padding: 0; text-align: left; vertical-align: top">
                                                            <th style="margin: 0 auto; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0 auto; padding: 0;
                                                                padding-bottom: 16px; padding-left: 16px; padding-right: 16px; text-align: left;
                                                                width: 564px">
                                                                <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                    vertical-align: top; width: 100%">
                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                        <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                            font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                            <h1 style="margin: 0; margin-bottom: 10px; color: inherit; font-family: Helvetica,Arial,sans-serif;
                                                                                font-size: 20px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                padding: 0; text-align: left; word-wrap: normal">
                                                                                Dear  <%=detail.EmpName%>,</h1>
                                                                            <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                padding: 0; text-align: left">
                                                                                Below expense are reimbursed.</p>
                                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                vertical-align: top; width: 100%">
                                                                                <tbody>
                                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                        <td height="30px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                            font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400; line-height: 30px;
                                                                                            margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            
                                                                            <table style="background: #fff; border: 1px solid #167f92; border-collapse: collapse;
                                                                                border-radius: 10px; border-spacing: 0; color: #024457; font-size: 11px; margin: 1em 0;
                                                                                padding: 0; text-align: left; vertical-align: top; width: 100%">
                                                                                <tr style="background-color: #eaf3f3; border: 1px solid #d9e4e6; padding: 0; text-align: left;
                                                                                    vertical-align: top">
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Date
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Expense Ref
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Expense Category
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                                        Expense Type
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                                               Claim  Amount
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                           Reimbursed Amount
                                                                                    </th>
                                                                                    <th style="margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #fff;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0; padding: 7px 7px; text-align: left">
                                                           Balance Amount
                                                                                    </th>
                                                                                    
                                                                                </tr>
                                                                                
                                                                  
                          
                                                                                <tr style="border: 1px solid #d9e4e6; padding: 0; text-align: left; vertical-align: top">
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                   <% = Convert.ToString(detail.Date).Split(' ')[0]%>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                           <% = detail.DocNo%>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                     <% =detail.ExpCategoryText%>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                    <% =detail.ExpTypeText%>
                                                                                    </td>
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                                        <% =detail.ReimCurrency%> <% = detail.ClaimAmount%> 
                                                                                    </td>
                                                                                    
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                                                                                        <% =detail.ReimCurrency%> <% = detail.ReimAmount%> 
                                                                                    </td>
                                                                                    
                                                                                    <td style="margin: 0; border: 1px solid #476794; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400;
                                                                                        line-height: 1.3; margin: 0; padding: 7px 7px; text-align: left; vertical-align: top;
                                                                                        word-wrap: break-word">
                        <% =detail.ReimCurrency%> <% = detail.BalanceAmount%> 
                                                                                    </td>
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                    
                                                                                </tr>
                                                                                
                                                                                
                                                                                 
                              
                                                                                
                                                                            </table>
                                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                vertical-align: top; width: 100%">
                                                                                <tbody>
                                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                        <td height="30px" style="margin: 0; border-collapse: collapse!important; color: #0a0a0a;
                                                                                            font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400; line-height: 30px;
                                                                                            margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word">
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                            <table style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0;
                                                                                position: relative; text-align: left; vertical-align: top; width: 100%">
                                                                                <tbody>
                                                                                    <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                        <th style="margin: 0 auto; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                            font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0 auto; padding: 0;
                                                                                            padding-bottom: 16px; padding-left: 0!important; padding-right: 0!important;
                                                                                            text-align: left; width: 100%">
                                                                                            <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                                vertical-align: top; width: 100%">
                                                                                                <tr style="padding: 0; text-align: left; vertical-align: top">
                                                                                                    <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left">
                                                                                                        <p style="margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                            font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                                            padding: 0; text-align: left">
                                                                                                            Regards<br>
                                                                                                           
                                 
                                                                                                            <strong><% = detail.AgencyName%></strong></p>
                                                      
                                                                                                    </th>
                                                                                                    <th style="margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important; text-align: left;
                                                                                                        width: 0">
                                                                                                    </th>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </th>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </th>
                                                                    </tr>
                                                                </table>
                                                            </th>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </center>
                        </td>
                    </tr>
                </table>
                <div style="display: none; white-space: nowrap; font: 15px courier; line-height: 0">
                </div>
            </div>
        </body>
        </html>
        <%} %>
    </div>
</asp:Content>
