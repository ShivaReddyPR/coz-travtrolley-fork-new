﻿using System;
using CT.TicketReceipt.BusinessLayer;

public partial class ItineraryAdditionalServiceDetailsIframe : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
