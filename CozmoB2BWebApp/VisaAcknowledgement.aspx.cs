﻿using System;
using Visa;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;



public partial class VisaAcknowledgement : System.Web.UI.Page
{
    protected VisaApplication application;
    protected int Adult = 0;
    protected int Child = 0;
    protected int Infant = 0;
    protected string visaTypeName = string.Empty;
    protected int visaId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
       
        if (Request.QueryString.Count > 0)
        {
            bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out visaId);
            if (!isnumerice)
            {
                Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                Response.End();
            }
            try
            {
                application = new VisaApplication(visaId);
                VisaType visaType = new VisaType(application.VisaTypeId);
                visaTypeName = visaType.VisaTypeName;
                foreach (VisaPassenger Passenger in application.PassengerList)
                {
                    int yearPass = (Passenger.CreatedOn.Year - Passenger.Dob.Year);
                    if (Passenger.CreatedOn.Month < Passenger.Dob.Month || (Passenger.CreatedOn.Month == Passenger.Dob.Month && Passenger.CreatedOn.Day < Passenger.Dob.Day))
                    {
                        yearPass--;
                    }
                    int year = yearPass;
                    if (year >= 12)
                    {
                        Adult++;
                    }
                    else if (year < 12 && year >= 2)
                    {
                        Child++;
                    }
                    else
                    {
                        Infant++;
                    }


                }
                if (Session["visaPassengerId"] != null)
                {
                    Session.Remove("visaPassengerId");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Visa, Severity.High, (int)Session["memberId"], "Page:AddAcknowledgement.aspx,Err:" + ex.Message,"");
            }

        }
        else
        {
            Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
            Response.End();
        }

    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
        //if (Session["roleId"] == null)
        //{
        //    String values = "?errMessage=Login Required to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
        //else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.CozmoVisa))
        //{
        //    String values = "?errMessage=You are not authorised to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
    }
}
