﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

public partial class OfflineBookingListGUI : CT.Core.ParentPage
{
    PagedDataSource pagedData = new PagedDataSource();
    protected int agentFilter;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void InitializePageControls()
    {
        try
        {
            Clear();
            txtFrom.Text = "";
            txtFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtTo.Text = "";
            txtTo.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            
            BindAgent();
           
           
           
            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgent.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgent.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            BindB2BAgent(Convert.ToInt32(ddlAgent.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }


            BindSearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void Clear()
    {
        ddlAgent.SelectedIndex = 0;
       
    }

   
    private void BindSearch()
    {
        try
        {

            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            string StartFromDate = (txtFrom.Text);
            DateTime fromDate = Convert.ToDateTime(StartFromDate, dateFormat);
            string StartToDate = (txtTo.Text);
            DateTime toDate = Convert.ToDateTime(StartToDate, dateFormat);
            //int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            //string  agentType=

            string agentType = string.Empty;
            if (!IsPostBack)
            {
                agentFilter = Utility.ToInteger(Settings.LoginInfo.AgentId);
                agentType = Convert.ToString(Settings.LoginInfo.AgentType);
            }
            else
            {
                agentFilter = Utility.ToInteger(ddlAgent.SelectedItem.Value);
                if (agentFilter == 0)
                {
                    agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                    }
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = string.Empty;// null Means binding in list all BOOKINGS
                    }
                }
                //if (ddlAgent.SelectedIndex > 0)
                //{


                if (agentFilter > 0 && ddlB2BAgent.SelectedIndex > 0)
                {
                    if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
                    {
                        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                        {
                            agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                        }
                        else
                        {
                            agentFilter = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                        }
                    }
                    else
                    {
                        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                        {
                            agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                        }
                        agentFilter = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }

                }
                if (agentFilter > 0 && ddlB2B2BAgent.SelectedIndex > 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                    }
                    else
                    {
                        agentFilter = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                    }
                }
                if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                        {
                            agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                            agentFilter = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                        }
                    }
                }
            }

            DataTable dtOfflineBooking = FlightOfflineBooking.GetOfflineBookingList(fromDate, toDate, agentFilter, Settings.LoginInfo.UserID, agentType);
            BindOfflineBooking(dtOfflineBooking);
        }
        catch
        {
            throw;
        }
    }
    private void GetAgentType()
    {
       
    }
    private void BindOfflineBooking(DataTable dtOfflineBooking)
    {
        try
        {
            if (dtOfflineBooking.Rows.Count > 0)
            {
                dlOfflineBooking.Visible = true;
                lblMessage.Visible = false;
                dlOfflineBooking.DataSource = dtOfflineBooking;
                ViewState["OfflineBooking"] = dtOfflineBooking;
                dlOfflineBooking.DataBind();
                doPaging();
            }
            else
            {
                dlOfflineBooking.Visible = false;
                btnPrev.Visible = false;
                btnFirst.Visible = false;
                btnLast.Visible = false;
                btnNext.Visible = false;
                lblMessage.Visible = true;
                lblMessage.Text = "No Results Found.";
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void doPaging()
    {
        DataTable pdt = (DataTable)ViewState["OfflineBooking"];
        if (pdt != null && pdt.Rows.Count > 0)
        {
            DataTable dt = (DataTable)ViewState["OfflineBooking"];
            pagedData.DataSource = dt.DefaultView;
        }
        pagedData.AllowPaging = true;
        pagedData.PageSize = 10;//10; for checking
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        dlOfflineBooking.DataSource = pagedData;
        dlOfflineBooking.DataBind();
    }

    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }
    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }


    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }

    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }

   

    protected void btnSearch_OnClick(object sender, EventArgs e)
    {
        try
        {
            BindSearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Agents Filter
    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        int agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
        if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
        BindB2BAgent(agentId);
        BindB2B2BAgent(agentId);
        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
        {
            ddlB2B2BAgent.Enabled = false;
        }
        string type = string.Empty;
        if (agentId == 0)
        {
            type = "BASE";
        }
       
    }

    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(1, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agency--", "0"));
            //ddlAgent.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string type = string.Empty;
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            if (agentId == 0)
            {
                if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
                {
                    type = "AGENT";// AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
                else
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
            }
            else
            {
                if (agentId == -1)
                {
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    if (agentId == 0)
                    {
                        type = "BASE";// BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations
                    }
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    type = "BASEB2B";// BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations
                }
            }

           
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void ddlB2B2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "B2B2B";// B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                }
            }
            else if (agentId == -1)
            {
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
                //if (agentId == -1)
                //{
                //    type = "BASE";
                //}
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = string.Empty;
                    }
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                }
            }
            
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
