﻿using System;
using System.Data;
using System.Web.UI;
using CT.BookingEngine.WhiteLabel;

public partial class FeedbackCorp : System.Web.UI.Page
{
    protected Feedback corpFeedback = new Feedback();
    protected DataRow dr;
    protected void Page_Load(object sender, EventArgs e)
    {
        IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
        DataTable dt = Feedback.GetFeedbackDetailsById(Convert.ToInt32(Request.QueryString["Index"]));

        if (dt != null && dt.Rows.Count > 0)
        {
            int row = Convert.ToInt32(Request.QueryString["Index"]);
            //dr = dt.Rows[row];
            dr = dt.Rows[0];
             //dr= dt.Select("CFId="+row);
            
        }
    }

    

    private void StartupScript(Page page, string script, string key)
    {

        script = string.Format("{0};", script);
        if (page != null && !string.IsNullOrEmpty(script) && !page.ClientScript.IsStartupScriptRegistered(key))
        {
            if (ScriptManager.GetCurrent(page) != null && ScriptManager.GetCurrent(page).IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(page, page.GetType(), key, script, true);
            else
                page.ClientScript.RegisterStartupScript(page.GetType(), key, script, true);

        }
    }

}
