using System;
using System.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

public partial class AGLoginUI : CT.Core.ParentPage// System.Web.UI.Page
{
    protected void Page_PreInit(object sender, EventArgs e)
    {
        try
        {
           
        string hostName = BookingUtility.ExtractSiteName(Request["HTTP_HOST"]);
        //Request["HTTP_X_FORWARDED_FOR"]
        //    REMOTE_ADDR 
        //CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Host Name" +hostName, "");
        string[] themeDetails = AgentMaster.GetThemByDoamin(hostName);
        if (!string.IsNullOrEmpty(themeDetails[1]))
            Session["themeName"] = themeDetails[1];
        else Session["themeName"] = "Default";
        Label lblCopyRight = (Label)this.Master.FindControl("lblCopyRight");
        if (!string.IsNullOrEmpty(themeDetails[2]))
        {
           lblCopyRight.Text=themeDetails[2];

        }
        Image imgLogo = (Image)this.Master.FindControl("imgLogo");
        

        if (!string.IsNullOrEmpty(themeDetails[0]))
        {
            string logoPath = ConfigurationManager.AppSettings["AgentImage"] + themeDetails[0];

            imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + logoPath;

        }

        string themeName = (string)Session["themeName"];
        if (themeName != null)
        {
            this.Page.Theme = themeName;
        }
        else
        {
            this.Page.Theme = "Default";
        }
        }

        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Dom Error:" + ex.ToString(), "");
            Utility.WriteLog(ex, this.Title);
            lblError.Text = ex.Message;
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //if (ex.InnerException != null) lblMasterError.Text = ex.InnerException.ToString();
        }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            
            lblForgotPwd.Text = string.Empty;
            if (!IsPostBack) InitializeControls();
            lblError.Text = string.Empty;
            if (Request.QueryString["errMessage"] != null)
            {
                lblForgotPwd.Text = Request.QueryString["errMessage"];
            }
        }

        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            lblError.Text = ex.Message;
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //if (ex.InnerException != null) lblMasterError.Text = ex.InnerException.ToString();
        }
        
    }
    private void InitializeControls()
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                Session.Abandon();
                //Response.Redirect("Login.aspx");
            }
            // string hostName = Request["HTTP_HOST"];
            //string[] themeDetails = AgentMaster.GetThemByDoamin(hostName);


            //if (!string.IsNullOrEmpty(themeDetails[0]))
            //    Session["themeName"] = themeDetails[0];
            //else Session["themeName"] = "Default";
            //Image imgLogo = (Image)this.Master.FindControl("imgLogo");

            //if (!string.IsNullOrEmpty(themeDetails[1]))
            //{
            //    string logoPath = ConfigurationManager.AppSettings["AgentImage"] + themeDetails[1];

            //    imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + logoPath;

            //}
            
          

           
          


            //ddlCompany.DataSource = CompanyMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            //ddlCompany.DataValueField = "company_id";
            //ddlCompany.DataTextField = "company_name";
            //ddlCompany.DataBind();
            //ddlCompany.Items.Insert(0, new ListItem("--Select Company--", "-1"));
            //ddlCompany.SelectedValue = "2";
            // clearControls();
        }
        catch { throw; }

    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {

            bool isAuthenticated = UserMaster.IsAuthenticatedUser(txtLoginName.Text.Trim(), txtPassword.Text, Utility.ToInteger(ddlCompany.SelectedValue));
            if (isAuthenticated)
            {
                //Session["agencyId"] = 1;
                //Response.Redirect("HotelSearch.aspx", false);
                if (!string.IsNullOrEmpty(Settings.LoginInfo.AgentTheme))
                    Session["themeName"] = Settings.LoginInfo.AgentTheme;
                else Session["themeName"] = "Default";

                Response.Redirect("HotelSearch.aspx?source=Flight", false);
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            lblError.Text = ex.Message;
            lblError.Visible = true;
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //if(ex.InnerException!=null)lblMasterError.Text = ex.InnerException.ToString();
            
        }

    }
    protected void btnGetPassword_Click(object sender, EventArgs e)
    {
        try
        {
            string pagePath = Request.Url.Scheme+"://" + Request.ServerVariables["HTTP_HOST"] + Page.Request.Path;
            string[] pageName = pagePath.Split('/');
            pagePath = string.Empty;
            for (int i = 0; i < pageName.Length - 1; i++)
            {
                pagePath = pagePath + pageName[i] + "/";
            }
            string guid = UserMaster.RequestPasswordChange(txtEmailId.Text.Trim());
            pagePath = pagePath + "ResetPassword.aspx?requestId=" + guid;

            //lblForgotPwd.ForeColor = Color.Black;
            
            //DataTable dtUser = CT.TicketReceipt.BusinessLayer1.UserMaster.GetUserByEmailId(txtEmailId.Text.Trim());
            //CT.TicketReceipt.BusinessLayer1.UserMaster member;
            //if (dtUser != null && dtUser.Rows.Count > 0)
            //{
            //    long userId = Convert.ToInt64(dtUser.Rows[0]["user_id"]);
            //     member = new CT.TicketReceipt.BusinessLayer1.UserMaster(userId);
            //}
            #region Sending Email
            //if (Convert.ToBoolean(ConfigurationManager.AppSettings["isSendEmail"]))
            //{

                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                //if (member.IsPrimaryAgent)
                //{
                    toArray.Add(txtEmailId.Text.Trim());
                //}
                //else
                //{
                //    string primaryAgentLogin = member.LoginName.Substring(member.LoginName.IndexOf('@') + 1);
                //    Member primaryMember = new Member();
                //    primaryMember.Load(primaryAgentLogin);
                //    toArray.Add(primaryMember.Email);
                //}
                string link = "<a href='" + pagePath + "'>" + pagePath + "</a>";
                string message = ConfigurationManager.AppSettings["changePassowrdMessage"].Replace("%link%", link);
                try
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Change your password", message, null);
                    lblForgotPwd.Text = "An email has been sent to you with a link to reset your password!";
                }
                catch (System.Net.Mail.SmtpException)
                {
                    //CT.Core1.Audit.Add(CoreLogic.EventType.Email, CoreLogic.Severity.Normal, 0, "Smtp is unable to send the message", "");
                }
            //}
            #endregion
            
        }
        catch(Exception ex)
        {
            lblForgotPwd.Text = ex.Message.ToString();
        }
    }
}
