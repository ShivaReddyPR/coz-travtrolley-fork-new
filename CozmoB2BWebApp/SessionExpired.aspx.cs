using System;
using CT.TicketReceipt.Common;

public partial class SessionExpired :CT.Core.ParentPage// System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        Session.Abandon();
        if(Request["auth"]=="false")
            lblErrMsg.Text = Utility.ToString(GetGlobalResourceObject("ErrorMessages", "INVALID_USER"));
        else
            lblErrMsg.Text = Utility.ToString(GetGlobalResourceObject("ErrorMessages", "SESSION_EXPIRED"));

    }
}
