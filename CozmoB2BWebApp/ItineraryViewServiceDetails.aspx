﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="ItineraryViewServiceDetailsGUI" Codebehind="ItineraryViewServiceDetails.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme %>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="<%=Request.Url.Scheme %>://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>Itinerary View Service Details</title>
    <link href="css/cozmovisa-style.css" rel="stylesheet" type="text/css" />
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="css/Default.css" rel="stylesheet" type="text/css" />

    <script type="text/javascript">
        function print(addId) {
        var finalurl = "ItineraryPrintAddServiceDetails?addId=" + addId;
        window.open(finalurl, '_blank');  
        }
    </script>

</head>
<body>
    <form id="form1" runat="server">
    <div>
        <div class="body_container">
            <%--   <div style="text-align: center">
            <h4>
                Service Details</h4>
        </div>--%>
            <%if (dtRecords != null && dtRecords.Rows.Count > 0) %>
            <%{ %>
            <%for (int i = 0; i < dtRecords.Rows.Count; i++) %>
            <%{ %>
            <% if (i == 0) %>
            <%{ %>
            <div class="col-md-12 padding-0">
                <div class="col-md-8 padding-0 marbot_10">
                    Passenger Name:<span><%=Convert.ToString(dtRecords.Rows[i]["pax_name"])%></span>
                </div>
            </div>
            <div class="col-md-12 padding-0">
                <div class="col-md-8 padding-0 marbot_10">
                    Route Details:<span><%=Convert.ToString(dtRecords.Rows[i]["routing"])%></span>
                </div>
            </div>
            <div class="col-md-12 padding-0">
                <div class="col-md-8 padding-0 marbot_10">
                    Travel Date:<span><%=Convert.ToString(CZDateFormat(dtRecords.Rows[i]["traveldate"]))%></span>
                </div>
            </div>
             <div class="col-md-12 padding-0">
                <div class="col-md-8 padding-0 marbot_10">
                    PNR Number:<span><%=Convert.ToString(dtRecords.Rows[i]["pax_pnr_no"])%></span>
                </div>
            </div>
            <%} %>
            <%} %>
            <%} %>
            <div class=" paramcon" title="header" style="max-height: 400px; overflow: hidden;
                overflow-y: auto;">
                <asp:GridView ID="gvSearch" Width="100%" runat="server" DataKeyNames="add_id" EmptyDataText="No Records Found!"
                    OnRowDataBound="gvSearch_RowDataBound" AutoGenerateColumns="false" CssClass="table table-condensed table-bordered"
                    CellPadding="4" CellSpacing="0">
                    <Columns>
                        <asp:TemplateField HeaderText="Service Name">
                            <ItemTemplate>
                                <asp:Label ID="lblServiceName" runat="server" Text='<%# Bind("service_name") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Pax Nationality">
                            <ItemTemplate>
                                <asp:Label ID="lblPaxNationality" runat="server" Text='<%# Bind("nationality") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Remarks">
                            <ItemTemplate>
                                <asp:Label ID="lblRemarks" runat="server" Text='<%# Bind("remarks") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField HeaderText="Amount">
                            <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%# Bind("amount") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnPrint" CssClass="but_b" runat="server" Text="Print" />
                            </ItemTemplate>
                        </asp:TemplateField>
                    </Columns>
                </asp:GridView>
            </div>
        </div>
    </div>
    </form>
</body>
</html>
