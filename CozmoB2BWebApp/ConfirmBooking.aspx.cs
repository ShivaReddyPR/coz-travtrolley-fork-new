using System;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

public partial class ConfirmBooking :CT.Core.ParentPage// System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //
        BookingDetail booking = new BookingDetail();
        string pnr = string.Empty;
        UserMaster loggedMember = new UserMaster();
        AgentMaster agency = new  AgentMaster();
        string message = "Confirm failed !";
        FlightItinerary itinerary;
        bool isAdmin = false;
        bool confirmed = false;
        decimal price = 0;
        if (Settings.LoginInfo != null)
        {
            loggedMember = new UserMaster(Settings.LoginInfo.UserID);
            if (loggedMember.AgentId <= 0)
            {
                isAdmin = true;
            }
            if (Request["bookingId"] != null && Request["pnr"] != null)
            {
                booking = new BookingDetail(Convert.ToInt32(Request["bookingId"]));
                pnr = Request["pnr"];
                if (booking.Status != BookingStatus.Hold)
                {
                    message = "Status of this booking has been modified. Please refresh  the page.";
                }
                else
                {
                    Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
                    for (int i = 0; i < products.Length; i++)
                    {
                        if (products[i].ProductTypeId == (int)ProductType.Flight)
                        {
                            itinerary = new FlightItinerary(products[i].ProductId);
                            for (int j = 0; j < itinerary.Passenger.Length; j++)
                            {
                                price += itinerary.Passenger[j].Price.GetAgentPrice();
                            }

                            decimal effectiveCredit = Settings.LoginInfo.AgentBalance;
                            if (effectiveCredit >= price)
                            {
                                agency = new AgentMaster(booking.AgencyId);
                                //TODO Ziyad
                                //if (agency.AgencyTypeId != (int)Agencytype.Cash && agency.AgencyTypeId != (int)Agencytype.Service)
                                //{
                                //    Agency.AlterCredit(Convert.ToDouble(-price), booking.AgencyId, isAdmin);
                                //}

                                booking.SetBookingStatus(BookingStatus.Ready, (int)loggedMember.ID);
                                BookingHistory bh = new BookingHistory();
                                bh.BookingId = booking.BookingId;
                                bh.EventCategory = EventCategory.Booking;
                                bh.Remarks = "Booked seat are confirmed and in ready state";
                                bh.CreatedBy = (int)loggedMember.ID;
                                bh.Save();
                                confirmed = true;
                                break;
                            }
                            else
                            {
                                message = "Not enough credit";
                                break;
                            }
                        }
                    }
                }
                
            }
        }
        else
        {
            message = "Your login session seems expired ! Relogin to Confirm.";
        }
        if (confirmed)
        {
            message = "1|" + pnr + "|Confirm successful !";
        }
        else
        {
            message = "1|" + pnr + "|" + message;
        }
        Response.Write(message);
    }
}
