﻿<?xml version="1.0" encoding="utf-8"?>
<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:msxsl="urn:schemas-microsoft-com:xslt" exclude-result-prefixes="msxsl"
>
  <xsl:output method="xml" indent="yes" encoding="utf-8"/>

  <xsl:template match="/Menus"> 
      <ul>
        <xsl:attribute name="class">
          <xsl:text>dropdown_menu</xsl:text>
        </xsl:attribute>
        <xsl:attribute name="id">
          <xsl:text>nav</xsl:text>
        </xsl:attribute>
        <li>
          <a href="HotelSearch.aspx?source=Flight&amp;domain=yes" data-icon="Home">Home</a>
        </li>
        <xsl:call-template name="MenuListing" />
      </ul>   
  </xsl:template>
  <!-- Allow for recusive child node processing -->
  <xsl:template name="MenuListing">
    <xsl:apply-templates select="Menu" />
  </xsl:template>

  <xsl:template match="Menu">
    <li>
      <a>
        <!--   Convert Menu child elements to <li> <a> html tags and  add attributes inside <a> tag -->
        <xsl:attribute name="href">
          <xsl:value-of select="FUNC_PAGE_LINK"/>
        </xsl:attribute>
        <xsl:attribute name="data-icon">
          <xsl:value-of select="FUNC_NAME"/>
        </xsl:attribute>
        <xsl:value-of select="FUNC_NAME"/>
      </a>
      <!-- Call MenuListing if there are child Menu nodes -->
      <xsl:if test="count(Menu) > 0">
        <ul>
          <xsl:call-template name="MenuListing" />
        </ul>
      </xsl:if>
    </li>
  </xsl:template>
</xsl:stylesheet>
