﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="BaggageMasterGUI" MasterPageFile="~/TransactionBE.master" Title="Baggage Master" Codebehind="BaggageMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="content1" runat="server" ContentPlaceHolderID="cphTransaction">
    <asp:HiddenField Value="0" ID="hdfBaggageId" runat="server" />
    <div>
         <div class="col-md-12 padding-0 marbot_10"">
        <div class="col-md-2">
            <asp:Label ID="lblBaggageSource" runat="server" Text="Source:"></asp:Label> <sup style="color:Red">*</sup>           
        </div>
        <div class="col-md-2">
            <asp:DropDownList ID="ddlSource" runat="server" CssClass="inpuTddlEnabled form-control" ></asp:DropDownList>
        </div>
    </div>
    <div class="col-md-12 padding-0 marbot_10"">
        <div class="col-md-2">
            <asp:Label ID="lblCode" Text="Code:" runat="server"></asp:Label><sup style="color:Red">*</sup>
        </div>
        <div class="col-md-2">
            <asp:TextBox ID="txtCode" runat="server" MaxLength="10" onkeypress="return isAlphaNumeric(event);" CssClass="inputEnabled form-control"></asp:TextBox>
        </div>
    </div>
     <div class="col-md-12 padding-0 marbot_10"">
        <div class="col-md-2">
            <asp:Label ID="lblPrice" runat="server" Text="Price:"></asp:Label><sup style="color:Red">*</sup>
        </div>
        <div class="col-md-2">
            <asp:TextBox ID="txtPrice" MaxLength="10" runat="server" onkeypress="return isNumber(event);" CssClass="inputEnabled form-control"></asp:TextBox>
        </div>
    </div>
     <div class="col-md-12 padding-0 marbot_10"">
        <div class="col-md-2">
             <asp:Label ID="lblCurrency" runat="server" Text="Currency:"></asp:Label><sup style="color:Red">*</sup>
        </div>
        <div class="col-md-2">
            <asp:TextBox ID="txtCurrency" runat="server" CssClass="inputEnabled form-control" MaxLength="3"></asp:TextBox>
        </div>
    </div>
     <div class="col-md-12 padding-0 marbot_10"">
        <div class="col-md-2">
             <asp:Label ID="lblIsActive" runat="server" Text="isActive:"></asp:Label>
        </div>
        <div class="col-md-2">
            <asp:RadioButton ID="rbActive" runat="server" Text="Active" Checked="true" GroupName="Active" />
            <asp:RadioButton ID="rbInActive" runat="server" Text="InActive" GroupName="Active" />
           
        </div>
    </div>
     <div class="col-md-12 padding-0 marbot_10"">
        <div class="col-md-2">
             <asp:Label ID="lblDomestic" runat="server" Text="isDomestic:"></asp:Label>
        </div>
        <div class="col-md-2">
           <asp:RadioButton ID="rbDomestic" runat="server" Text="Domestic" Checked="true"  GroupName="Domestic" />
            <asp:RadioButton ID="rbInternational" runat="server" Text="International"  GroupName="Domestic" />
        </div>
    </div>
        <div style="padding:220px">
             <asp:Button ID="btnSave" CssClass="button" Text="Save" runat="server" Style="display: inline;"  OnClientClick="return Save();" OnClick="btnSave_Click"  />&nbsp;&nbsp;
                                    <asp:Button ID="btnClear" CssClass="button" Text="Clear" runat="server" Style="display: inline;" OnClick="btnClear_Click"  />&nbsp;&nbsp;
                                    <asp:Button ID="btnSearch" CssClass="button" Text="Search" runat="server"  Style="display: inline;" OnClick="btnSearch_Click" />
            <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>
        </div>
    </div>
   <script type="text/javascript">
      
       function isNumber(evt) {
           evt = (evt) ? evt : window.event;
           var charCode = (evt.which) ? evt.which : evt.keyCode;
           if (charCode > 31 && (charCode < 46 || charCode > 57)) {
               return false;
           }
           return true;
       }
       var specialKeys = new Array();
       specialKeys.push(8); //Backspace
       specialKeys.push(9); //Tab
       specialKeys.push(46); //Delete
       specialKeys.push(36); //Home
       specialKeys.push(35); //End
       specialKeys.push(37); //Left
       specialKeys.push(39); //Right
       function isAlphaNumeric(e) {
           var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
           var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (specialKeys.indexOf(e.keyCode) != -1 && e.charCode != e.keyCode) || keyCode == 32);
           return ret;
       }
       function Save() {   
           if (document.getElementById('<%=ddlSource.ClientID%>').selectedIndex <= 0)  addMessage('Please select Source from the list!', ''); 
           if (document.getElementById('<%=txtCode.ClientID%>').value == '') addMessage('Code cannnot be blank!', '');
           if (document.getElementById('<%=txtPrice.ClientID%>').value == '') addMessage('Price cannnot be blank!', '');
           if (document.getElementById('<%=txtCurrency.ClientID%>').value == '') addMessage('Currency cannnot be blank!', '');

           if (getMessage() != '') {

               alert(getMessage()); clearMessage();
               return false;
           }
       }

   </script>
</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
<asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="BaggageId"
        EmptyDataText="No Data Found!" AutoGenerateColumns="false" PageSize="10" GridLines="none" CssClass="grdTable"  CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged"
       >
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />" ControlStyle-CssClass="label" ShowSelectButton="True" />
            <asp:TemplateField>
               <HeaderTemplate>                                         
                 <asp:Label ID="lblhdrSource" CssClass="label" Width="100px"  runat="server" Text="Source"></asp:Label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblSource" runat="server" Width="100px"  Text='<%# Eval("BaggageSourceId") %>' CssClass="label grdof"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                   <asp:Label ID="lblhdrCode" CssClass="label" Width="100px"  runat="server" Text="Code"></asp:Label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblBaggageCode" runat="server" Width="100px" Text='<%# Eval("BaggageCode") %>' CssClass="label grdof"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField >
                <HeaderTemplate>
                   <asp:Label ID="lblhdrPrice" CssClass="label" Width="100px"  runat="server" Text="Price"></asp:Label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblBaggagePrice" runat="server" Width="100px" Text='<%# Eval("BaggagePrice") %>' CssClass="label grdof"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    <asp:Label ID="lblhdrCurrency" CssClass="label" Width="100px"  runat="server" Text="Currency"></asp:Label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblBaggageCurrency" runat="server" Width="100px" Text='<%# Eval("BaggageCurrency") %>' CssClass="label grdof"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="Module">
                <HeaderTemplate>
                   <asp:Label ID="lblhdrActive" CssClass="label" Width="100px"  runat="server" Text="Is Active"></asp:Label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblBaggageActive" runat="server" Width="100px" Text='<%# Eval("IsActive") %>' CssClass="label grdof"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
              <asp:TemplateField HeaderText="Module">
                <HeaderTemplate>
                   <asp:Label ID="lblhdrDomestic" CssClass="label" Width="100px"  runat="server" Text="Is Domestic"></asp:Label>
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="lblBaggageDomestic" runat="server" Width="100px" Text='<%# Eval("IsDomestic") %>' CssClass="label grdof"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
           </Columns>
           </asp:GridView>
</asp:content>

