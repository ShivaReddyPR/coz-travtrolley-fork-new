﻿using System;
using System.Data;
using System.Linq;
using CT.BookingEngine;
using CT.Core;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;

public partial class RoomDetailsAjax :System.Web.UI.Page
{
    protected HotelSearchResult resultObj = null;
    protected HotelRequest request = null;
    protected int index =0;
    protected decimal totFare = 0, resultTotalFare =0;
    protected decimal discount = 0;
    protected Dictionary<string, decimal> rateOfEx = new Dictionary<string, decimal>();
    protected int decimalValue=2;
    protected void Page_Load(object sender, EventArgs e)
    
    
    
    {
        request = Session["req"] as HotelRequest;
        string hotelCode = Request["hotelCode"];
        //resultObj = Session[hotelCode] as HotelSearchResult;
        index = Convert.ToInt32(Request["index"]);
        resultTotalFare = Convert.ToDecimal(Request["totFare"]);


       
        if (request == null )
        {
            tblRooms.InnerHtml = "<b>Your session seems to be expired!. Please search again.</b>";
        }
        else
        {   
            Dictionary<string, decimal> ExchangeRates = new Dictionary<string,decimal>();
            HotelSearchResult[] hotelInfo = Session["SearchResults"] as HotelSearchResult[];
            resultObj = hotelInfo.First<HotelSearchResult>(delegate(HotelSearchResult h) { return h.HotelCode == hotelCode; });
            int agencyId = 0;

            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                decimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                agencyId = Settings.LoginInfo.OnBehalfAgentID;
            }
            else
            {
                decimalValue = Settings.LoginInfo.DecimalValue;
                agencyId = Settings.LoginInfo.AgentId;
            }
            
            //CT.Core1.StaticData staticInfo = new CT.Core1.StaticData();
            //staticInfo.BaseCurrency = agent.AgentCurrency;
            //rateOfEx = staticInfo.CurrencyROE;
            #region DOTW GetRooms
            try
            {
                if (resultObj.BookingSource == HotelBookingSource.DOTW)
                {                   

                    DataTable dtMarkup = CT.BookingEngine.UpdateMarkup.Load(agencyId, resultObj.BookingSource.ToString(), (int)ProductType.Hotel);
                    
                    CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(Session["cSessionId"].ToString());
                    if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agencyId = Settings.LoginInfo.AgentId;
                        dotw.AgentCurrency = Settings.LoginInfo.Currency;
                        ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        dotw.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        dotw.DecimalPoint = Settings.LoginInfo.DecimalValue;
                    }
                    else
                    {
                        agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency\
                        dotw.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                        dotw.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        dotw.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                    }
                    //For VAT Implemetation 15.12.2017
                    try
                    {
                        Dictionary<string, string> activeSources = AgentMaster.GetActiveSourcesByProduct((int)ProductType.Hotel);
                        if (activeSources != null && activeSources.Count > 0)
                        {
                            dotw.SourceCountryCode = activeSources[HotelBookingSource.DOTW.ToString()];
                        }
                        else
                        {
                            dotw.SourceCountryCode = "AE";// TODO Ziyad
                        }
                    }
                    catch
                    {
                        dotw.SourceCountryCode = "AE";
                    }

                    decimal markup = 0; string markupType = string.Empty;

                    if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                    {
                        markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                        markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                    }
                    dotw.GetRooms(ref resultObj, request, markup, markupType);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.High, 1, "Failed to get room details for DOTW: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
            #endregion

            #region TBOHotel GetRooms

            if (resultObj.BookingSource == HotelBookingSource.TBOHotel)
            {
                try
                {
                    DataTable dtMarkup = CT.BookingEngine.UpdateMarkup.Load(agencyId, resultObj.BookingSource.ToString(), (int)ProductType.Hotel);

                    TBOHotel.HotelV10 tbo = new TBOHotel.HotelV10();
                    if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agencyId = Settings.LoginInfo.AgentId;
                        tbo.AgentCurrency = Settings.LoginInfo.Currency;
                        ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        tbo.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        tbo.DecimalPoint = Settings.LoginInfo.DecimalValue;
                    }
                    else
                    {
                        agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency\
                        tbo.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                        tbo.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        tbo.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                    }
                    //For VAT Implemetation 15.12.2017
                    try
                    {
                        Dictionary<string, string> activeSources = AgentMaster.GetActiveSourcesByProduct((int)ProductType.Hotel);
                        if (activeSources != null && activeSources.Count > 0)
                        {
                            tbo.SourceCountryCode = activeSources[HotelBookingSource.TBOHotel.ToString()];
                        }
                        else
                        {
                            tbo.SourceCountryCode = "AE";// TODO Ziyad
                        }
                    }
                    catch
                    {
                        tbo.SourceCountryCode = "AE";
                    }

                    decimal markup = 0; string markupType = string.Empty;

                    if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                    {
                        markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                        markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                    }
                    tbo.GetRooms(ref resultObj,request, markup, markupType);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HotelSearch, Severity.High, 1, "Failed to get room details for TBOHotel: " + ex.ToString(), Request["REMOTE_ADDR"]);
                }

            }
            #endregion

            #region Yatra GetRooms
            try
            {
                if (resultObj.BookingSource == HotelBookingSource.Yatra)
                {

                    DataTable dtMarkup = CT.BookingEngine.UpdateMarkup.Load(agencyId, resultObj.BookingSource.ToString(), (int)ProductType.Hotel);

                    CT.BookingEngine.GDS.Yatra yatra = new CT.BookingEngine.GDS.Yatra(Session["cSessionId"].ToString());
                    if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agencyId = Settings.LoginInfo.AgentId;
                        yatra.AgentCurrency = Settings.LoginInfo.Currency;
                        ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        yatra.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        yatra.DecimalPoint = Settings.LoginInfo.DecimalValue;
                        yatra.AppUserId = (int)Settings.LoginInfo.UserID;
                    }
                    else
                    {
                        agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency\
                        yatra.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                        yatra.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        yatra.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                        yatra.AppUserId = (int)Settings.LoginInfo.UserID;
                    }
                    //For VAT Implemetation 15.12.2017
                    try
                    {
                        Dictionary<string, string> activeSources = AgentMaster.GetActiveSourcesByProduct((int)ProductType.Hotel);
                        if (activeSources != null && activeSources.Count > 0)
                        {
                            yatra.SourceCountryCode = activeSources[HotelBookingSource.Yatra.ToString()];
                        }
                        else
                        {
                            yatra.SourceCountryCode = "AE";// TODO Ziyad
                        }
                    }
                    catch
                    {
                        yatra.SourceCountryCode = "AE";
                    }

                    decimal markup = 0; string markupType = string.Empty;

                    if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                    {
                        markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                        markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                    }
                    yatra.GetRooms(ref resultObj, request, markup, markupType);  //TODO
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to get room details for Yatra : " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
            #endregion

 #region Grn GetRooms
            if (resultObj.BookingSource == HotelBookingSource.GRN)
            {
                try
                {
                    if (!string.IsNullOrEmpty(resultObj.RoomDetails[0].RoomTypeCode.Split('|')[3]))
                    {
                        DataTable dtMarkup = CT.BookingEngine.UpdateMarkup.Load(agencyId, resultObj.BookingSource.ToString(), (int)ProductType.Hotel);
                        CT.BookingEngine.GDS.GRN GrnApi = new CT.BookingEngine.GDS.GRN();
                        GrnApi.GrnUserId = Settings.LoginInfo.UserID;
                        if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            agencyId = Settings.LoginInfo.AgentId;
                            GrnApi.AgentCurrency = Settings.LoginInfo.Currency;
                            ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            GrnApi.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            GrnApi.DecimalPoint = Settings.LoginInfo.DecimalValue;
                        }
                        else
                        {
                            agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency\
                            GrnApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            GrnApi.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                            ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                            GrnApi.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                        }

                        //For VAT Implemetation 
                        try
                        {
                            Dictionary<string, string> activeSources = AgentMaster.GetActiveSourcesByProduct((int)ProductType.Hotel);
                            if (activeSources != null && activeSources.Count > 0)
                            {
                                GrnApi.SourceCountryCode = activeSources[HotelBookingSource.GRN.ToString()];
                            }
                            else
                            {
                                GrnApi.SourceCountryCode = "AE";// TODO Ziyad
                            }
                        }
                        catch
                        {
                            GrnApi.SourceCountryCode = "AE";
                        }
                        decimal markup = 0; string markupType = string.Empty;
                        if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                        {
                            markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                            markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                        }

                        GrnApi.GetRoomRefetchDetails(ref resultObj, request, markup, markupType, resultObj.RoomDetails[0].RoomTypeCode.Split('|')[1]);                                               
                    }
                }
                catch (Exception ex)
                {
                    if(resultObj.HotelDescription== "Invalid search ID or it might have expired")
                    {
                        tblRooms.InnerHtml = "<b>Your session seems to be expired!. Please search again.</b>";
                        return;
                    }
                    else
                    {
                        Audit.Add(EventType.HotelSearch, Severity.High, 1, "Failed to get room details for GRNHotel: " + ex.ToString(), Request["REMOTE_ADDR"]);                    
                    }
                    throw ex;
                }
            }
            #endregion         

            #region OYO -- GetRooms, added by somasekhar on 13/12/2018
            try
            {
                if (resultObj.BookingSource == HotelBookingSource.OYO)
                {
                    DataTable dtMarkup = CT.BookingEngine.UpdateMarkup.Load(agencyId, resultObj.BookingSource.ToString(), (int)ProductType.Hotel);
                    CT.BookingEngine.GDS.OYO oyo = new CT.BookingEngine.GDS.OYO(Session["cSessionId"].ToString());
                    if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agencyId = Settings.LoginInfo.AgentId;
                        oyo.AgentCurrency = Settings.LoginInfo.Currency;
                        ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        oyo.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        oyo.DecimalPoint = Settings.LoginInfo.DecimalValue;
                        oyo.AppUserId = (int)Settings.LoginInfo.UserID;
                    }
                    else
                    {
                        agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency\
                        oyo.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                        oyo.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        oyo.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                        oyo.AppUserId = (int)Settings.LoginInfo.UserID;
                    }
                    //For VAT Implemetation 15.12.2017
                    try
                    {
                        Dictionary<string, string> activeSources = AgentMaster.GetActiveSourcesByProduct((int)ProductType.Hotel);
                        if (activeSources != null && activeSources.Count > 0)
                        {
                            oyo.SourceCountryCode = activeSources[HotelBookingSource.Yatra.ToString()];
                        }
                        else
                        {
                            oyo.SourceCountryCode = "AE";// TODO Ziyad
                        }
                    }
                    catch
                    {
                        oyo.SourceCountryCode = "AE";
                    }
                    decimal markup = 0; string markupType = string.Empty;
                    if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                    {
                        markup = Convert.ToDecimal(dtMarkup.Rows[0]["Markup"]);
                        markupType = dtMarkup.Rows[0]["MarkupType"].ToString();
                    }
                    oyo.GetRooms(ref resultObj, request, markup, markupType);  //TODO
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.OYOAvailSearch, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to get room details for OYO : " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
            #endregion
            try
            {

                #region Old Code

                //if (resultObj.BookingSource == HotelBookingSource.DOTW)
                //{
                    

                //    //Get the Markup for the Agent once for All Sources searched
                //    Dictionary<string, DataTable> agentMarkups = new Dictionary<string, DataTable>();
                //    //foreach (string source in request.Sources)
                //    {
                //        DataTable dtMarkup = CT.AccountingEngine.UpdateMarkup.Load(agencyId, resultObj.BookingSource.ToString(), (int)ProductType.Hotel);
                //        if (dtMarkup != null)
                //        {
                //            agentMarkups.Add(resultObj.BookingSource.ToString(), dtMarkup);
                //        }
                //    }
                //    for (int count = 0; count < resultObj.RoomDetails.Length; count++)
                //    {
                //        decimal totalSellingFare = 0;
                //        for (int j = 0; j < resultObj.RoomDetails[count].Rates.Length; j++)
                //        {
                //            PriceAccounts tempPrice = new PriceAccounts();

                //            decimal markup = 0;
                //            decimal discount = 0;
                //            string markupType = string.Empty; //  Commission Type Id, either percentage/Fixed    
                //            string discountType = string.Empty; // Fare Type Id, either Net/Published 

                //            if (resultObj.BookingSource == HotelBookingSource.DOTW)
                //            {
                //                DataTable dtMarkup = agentMarkups[resultObj.BookingSource.ToString()];
                //                if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                //                {
                //                    DataRow dr = dtMarkup.Rows[0];
                //                    markup = Convert.ToDecimal(dr["Markup"]);
                //                    discount = Convert.ToDecimal(dr["Discount"]);
                //                    markupType = Convert.ToString(dr["MarkupType"]); // Percentage, fixed
                //                    discountType = Convert.ToString(dr["DiscountType"]);// Percentage, fixed
                //                }
                //                tempPrice.MarkupType = markupType;
                //                tempPrice.MarkupValue = markup;
                //                tempPrice.DiscountType = discountType;
                //                tempPrice.DiscountValue = discount;
                //                //price for only sigle room so noOfRooms==1
                //                tempPrice.Markup = (markupType == "F" ? markup : Convert.ToDecimal(resultObj.RoomDetails[count].TotalPrice) * (markup / 100));//clari
                //                // tempPrice.Markup = (markupType == "F" ? markup * request.NoOfRooms : (Convert.ToDecimal(resultObj.RoomDetails[count].Rates[j].Amount / request.NoOfRooms) * (markup / 100)) * request.NoOfRooms);
                //                tempPrice.NetFare = Convert.ToDecimal(resultObj.RoomDetails[count].TotalPrice);
                //                tempPrice.Discount = (discountType == "F" ? discount : discount / 100);
                //            }
                //            //else if (resultObj.BookingSource == HotelBookingSource.RezLive)
                //            //{
                //            //    try
                //            //    {
                //            //        DataTable dtMarkup = agentMarkups[resultObj.BookingSource.ToString()];
                //            //        if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                //            //        {
                //            //            DataRow dr = dtMarkup.Rows[0];
                //            //            markup = Convert.ToDecimal(dr["Markup"]);
                //            //            discount = Convert.ToDecimal(dr["Discount"]);
                //            //            markupType = Convert.ToString(dr["MarkupType"]); // Percentage, fixed
                //            //            discountType = Convert.ToString(dr["DiscountType"]);// Percentage, fixed
                //            //        }
                //            //    }
                //            //    catch { }

                //            //    //For Rezlive Whole amount is given
                //            //    tempPrice.Markup = (markupType == "F" ? markup * request.NoOfRooms : (Convert.ToDecimal(resultObj.RoomDetails[count].Rates[j].Amount / request.NoOfRooms) * (markup / 100)) * request.NoOfRooms);
                //            //    tempPrice.NetFare = Convert.ToDecimal(resultObj.RoomDetails[count].TotalPrice);
                //            //    tempPrice.Discount = (discountType == "F" ? discount : Convert.ToDecimal(tempPrice.NetFare + tempPrice.Markup) * (discount / 100));
                //            //}
                //            //else if (resultObj.BookingSource == HotelBookingSource.LOH)
                //            //{
                //            //    try
                //            //    {
                //            //        DataTable dtMarkup = agentMarkups[resultObj.BookingSource.ToString()];
                //            //        if (dtMarkup != null && dtMarkup.Rows.Count > 0)
                //            //        {
                //            //            DataRow dr = dtMarkup.Rows[0];
                //            //            markup = Convert.ToDecimal(dr["Markup"]);
                //            //            discount = Convert.ToDecimal(dr["Discount"]);
                //            //            markupType = Convert.ToString(dr["MarkupType"]); // Percentage, fixed
                //            //            discountType = Convert.ToString(dr["DiscountType"]);// Percentage, fixed
                //            //        }
                //            //    }
                //            //    catch { }

                //            //    //For LOH Whole amount is given
                //            //    tempPrice.Markup = (markupType == "F" ? markup : (Convert.ToDecimal(resultObj.RoomDetails[count].SellingFare) * (markup / 100)));
                //            //    tempPrice.NetFare = Convert.ToDecimal(resultObj.RoomDetails[count].TotalPrice);
                //            //    tempPrice.Discount = (discountType == "F" ? discount : (Convert.ToDecimal(resultObj.SellingFare + (tempPrice.Markup * request.NoOfRooms))) * (discount / 100));
                //            //    tempPrice.Discount = Math.Round(tempPrice.Discount, 2);
                //            //}

                //            //if (resultObj.BookingSource != HotelBookingSource.HotelConnect)
                //            {
                //                if (resultObj.BookingSource == HotelBookingSource.DOTW)
                //                {
                //                    resultObj.RoomDetails[count].Rates[j].SellingFare = ((tempPrice.NetFare)) / resultObj.RoomDetails[count].Rates.Length;
                //                }
                //                //else if (resultObj.BookingSource != HotelBookingSource.DOTW)
                //                //{
                //                //    resultObj.RoomDetails[count].Rates[j].SellingFare = (tempPrice.NetFare);
                //                //}
                //                resultObj.RateType = RateType.Negotiated;
                //                resultObj.RoomDetails[count].DiscountType = discountType;
                //                resultObj.RoomDetails[count].Discount = tempPrice.Discount;
                //                resultObj.RoomDetails[count].Markup = tempPrice.Markup;
                //                resultObj.RoomDetails[count].MarkupType = tempPrice.MarkupType;
                //                resultObj.RoomDetails[count].MarkupValue = tempPrice.MarkupValue;
                //                resultObj.RoomDetails[count].DiscountType = tempPrice.DiscountType;
                //                resultObj.RoomDetails[count].DiscountValue = tempPrice.DiscountValue;
                //            }
                //            if (resultObj.BookingSource == HotelBookingSource.DOTW)
                //            {
                //                totalSellingFare += resultObj.RoomDetails[count].Rates[j].SellingFare;
                //            }
                //            //else if (resultObj.BookingSource != HotelBookingSource.DOTW)
                //            //{
                //            //    totalSellingFare = resultObj.RoomDetails[count].Rates[j].SellingFare;
                //            //}
                //        }

                //        resultObj.RoomDetails[count].SellingFare = totalSellingFare;
                //    }
                //} 
                #endregion

                Session["SelectedHotel"] = resultObj;

                //if (resultObj.BookingSource == HotelBookingSource.DOTW)
                {
                    totFare = 0;
                }
                string roomSeries = "", sameRooms = "", rooms = "", uniqueRooms = "";
                if (resultObj.BookingSource == HotelBookingSource.HotelBeds)
                {
                    

                List<RoomGuestData> roomsList = new List<RoomGuestData>();
                roomsList.AddRange(request.RoomGuest);

                Dictionary<RoomGuestData, int> filteredroomsList = new Dictionary<RoomGuestData, int>();
                foreach (var container in roomsList)
                {
                    List<RoomGuestData> duplicateRoom = roomsList.FindAll(delegate(RoomGuestData checkRoom)
                    { return (checkRoom.noOfAdults == container.noOfAdults && checkRoom.noOfChild == container.noOfChild); });
                    if (duplicateRoom.Count > 1 && !filteredroomsList.ContainsKey(duplicateRoom[0]))
                    {
                        int ind = roomsList.FindLastIndex(delegate(RoomGuestData g) { return g.noOfAdults == duplicateRoom[0].noOfAdults && g.noOfChild == duplicateRoom[0].noOfChild; });
                        if (uniqueRooms.Length > 0)
                        {
                            uniqueRooms += "," + ind;
                        }
                        else
                        {
                            uniqueRooms = ind.ToString();
                        }
                        filteredroomsList.Add(duplicateRoom[0], ind);
                    }
                }

                    int uniqueIndex = 0;
                    List<string> uniqueRoomList = new List<string>();
                    if (uniqueRooms.Length > 0)
                    {
                        uniqueIndex = Convert.ToInt32(uniqueRooms.Split(',')[0]);
                    }

                    for (int x = 0; x < request.NoOfRooms; x++)
                    {
                        foreach (KeyValuePair<RoomGuestData, int> pair in filteredroomsList)
                        {
                            if (pair.Key.noOfAdults == request.RoomGuest[x].noOfAdults && pair.Key.noOfChild == request.RoomGuest[x].noOfChild)
                            {
                                if (sameRooms.Length > 0)
                                {
                                    sameRooms += "," + (request.RoomGuest[x].noOfAdults + request.RoomGuest[x].noOfChild) + "-" + x.ToString();
                                }
                                else
                                {
                                    sameRooms = (request.RoomGuest[x].noOfAdults + request.RoomGuest[x].noOfChild) + "-" + x.ToString();
                                }
                            }
                            //else if(sameRooms.IndexOf(x.ToString()) < 0)
                            //{
                            //    if (rooms.Length > 0)
                            //    {
                            //        rooms += "," + x.ToString();
                            //    }
                            //    else
                            //    {
                            //        rooms = x.ToString();
                            //    }
                            //}
                        }


                        if (uniqueRooms.Length > 0 && uniqueRooms.IndexOf(x.ToString()) < 0)
                        {
                            if (request.RoomGuest[x].noOfAdults == request.RoomGuest[uniqueIndex].noOfAdults && request.RoomGuest[x].noOfChild == request.RoomGuest[uniqueIndex].noOfChild)
                            {
                                if (!uniqueRoomList.Contains(x.ToString()))
                                {
                                    uniqueRoomList.Add(x.ToString());
                                }
                            }
                        }
                    }
                    uniqueRoomList.Add(uniqueIndex.ToString());

                    Array.Sort(uniqueRoomList.ToArray());
                    uniqueRooms = "";
                    foreach (string val in uniqueRoomList)
                    {
                        if (uniqueRooms.Length > 0)
                        {
                            uniqueRooms += "," + val;
                        }
                        else
                        {
                            uniqueRooms = val.ToString();
                        }
                    }

                }

                for (int x = 0; x < request.NoOfRooms; x++)
                {
                    tblRooms.InnerHtml += "<tr><td colspan='3'>";
                    
                    tblRooms.InnerHtml += "<b>Select Room " + (x + 1) + ": " + request.RoomGuest[x].noOfAdults + " Adult ";
                    if (request.RoomGuest[x].noOfChild > 0)
                        tblRooms.InnerHtml += request.RoomGuest[x].noOfChild + " Child </b></td></tr>";
                    else
                        tblRooms.InnerHtml += "</b></td></tr>";

                    tblRooms.InnerHtml += "<tr><th width='50%' align='left'>Room Types</th><th width='24%' align='center'>Rates</th><th width='26%'>Cancellation Charges</th></tr>";

                    if (resultObj.BookingSource == HotelBookingSource.HotelBeds)
                    {
                        if (roomSeries.Length > 0)
                        {
                            roomSeries += "|";
                        }


                        if (uniqueRooms.Length > 0 && sameRooms.Split(',').Length < request.NoOfRooms && uniqueRooms.IndexOf(x.ToString()) < 0)
                        {
                            if (rooms.Length > 0)
                            {
                                rooms += "," + x.ToString();
                            }
                            else
                            {
                                rooms = x.ToString();
                            }
                        }
                    }

                   
                    int firstRoom = 0;
                    for (int y = 0; y < resultObj.RoomDetails.Length; y++)
                    {
                        # region DOTW
                        if (resultObj.BookingSource == HotelBookingSource.DOTW)
                        {
                            int nights = request.EndDate.Subtract(request.StartDate).Days;
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()) && resultObj.RoomDetails[y].Occupancy["minStay"] <= nights)
                            {
                                decimal totalSellRate = 0;                                
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                totalSellRate = (Convert.ToDecimal(roomResult.SellingFare + roomResult.Markup + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length) + roomResult.TotalTax));
                                if (resultObj.RoomDetails[y].Occupancy["minStay"] > 0 && request.EndDate.Subtract(request.StartDate).Days < resultObj.RoomDetails[y].Occupancy["minStay"])
                                {
                                    totalSellRate = totalSellRate * resultObj.RoomDetails[y].Occupancy["minStay"];
                                    

                                }
                                discount = roomResult.Discount;
                                //if (resultObj.RoomDetails[y].DiscountType == "P")
                                //{
                                //    totalSellRate = totalSellRate - resultObj.RoomDetails[y].Discount;
                                //}
                                //else
                                //{
                                //    decimal tmpDiscount=resultObj.RoomDetails[y].Discount/request.NoOfRooms;
                                //    totalSellRate =totalSellRate - tmpDiscount;

                                    

                                //}
                                tblRooms.InnerHtml += "<tr>";
                                if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                {
                                    tblRooms.InnerHtml += "<td>";
                                    tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                    if (firstRoom == 0)
                                        tblRooms.InnerHtml += "checked=\'checked\'";
                                    else
                                        tblRooms.InnerHtml += "";

                                    tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";
                                    tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";
                                }

                                if (resultObj.RoomDetails[y].Occupancy["minStay"] > 0)
                                {
                                    tblRooms.InnerHtml += "<table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/img_mandatory.gif' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += "Min Stay Nights: " + resultObj.RoomDetails[y].Occupancy["minStay"];
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }

                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin: 0px 0px 0px 0px;' id='AmenityDiv-" + index.ToString() + y + "'>";
                                tblRooms.InnerHtml += "<span id='amenityClo' title='close' style='font-size: 24px; cursor: pointer; position: absolute;right: 10px; top: 0px' onclick=\"hideAmenities('AmenityDiv-" + index.ToString() + y + "')\"><a class='closex' href='#'>X</a></span>";
                                if (resultObj.RoomDetails[y].Amenities != null && resultObj.RoomDetails[y].Amenities.Count > 0)
                                {
                                    tblRooms.InnerHtml += "<div class='showMsgHeading'>Room Amenities</div>";
                                    tblRooms.InnerHtml += "<div style='overflow:auto;height:170px'><ul>";

                                    foreach (string amenity in resultObj.RoomDetails[y].Amenities)
                                    {
                                        tblRooms.InnerHtml += "<li style='padding-left:10px; padding-top:3px'>" + amenity + "</li>";
                                    }
                                    tblRooms.InnerHtml += "</ul></div>";
                                }

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowRoomAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'>" + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:GetRoomInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + index + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span>";

                                #region Fare Breakup

                                tblRooms.InnerHtml += "<div class='links'>";

                                DateTime d1 = resultObj.StartDate;
                                DateTime d2 = resultObj.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;

                                int minStay = 1;
                                if (request.EndDate.Subtract(request.StartDate).Days < resultObj.RoomDetails[y].Occupancy["minStay"])
                                {
                                    minStay = resultObj.RoomDetails[y].Occupancy["minStay"];

                                }
                                //tblRooms.InnerHtml += "<table><tr><td colspan='3'>";
                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr ><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 0px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    tblRooms.InnerHtml += "<tr  style='width: 550px;font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td >Week " + (i + 1) + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";                                            
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        rmInfo = roomResult.Rates[j++];
                                        d1 = d1.AddDays(1);

                                        tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / (request.EndDate.Subtract(request.StartDate).Days), decimalValue).ToString("N" + decimalValue) + "</td>";


                                        for (int k = 1; k < minStay; k++)
                                        {
                                            tblRooms.InnerHtml += "<td>" + Math.Round(rmInfo.SellingFare + (roomResult.Markup / diffResult.Days), decimalValue).ToString("N" + decimalValue) + "</td>";
                                        }
                                        totalRoomSellRate += rmInfo.SellingFare;
                                    }

                                    tblRooms.InnerHtml += "</tr>";
                                }
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round((totalRoomSellRate + roomResult.Markup) * minStay, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                //tblRooms.InnerHtml += "</ul>";
                                //if (resultObj.Price.Discount > 0)
                                //{
                                //    tblRooms.InnerHtml += "<ul><li>Discount : ";
                                //    tblRooms.InnerHtml += "<b>" + (resultObj.Price.Discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //}
                                //tblRooms.InnerHtml += "<ul><li>Tax</li>";
                                //tblRooms.InnerHtml += " <li><b>" + (roomResult.TotalTax).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";

                                decimal extraGuestRate = 0;
                                if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                {
                                    extraGuestRate = roomResult.SellExtraGuestCharges * roomResult.Rates.Length;
                                }
                                else
                                {
                                    extraGuestRate = roomResult.SellExtraGuestCharges;
                                }

                                tblRooms.InnerHtml += " <b>" + Math.Round(extraGuestRate, 2).ToString("N" + decimalValue) + "</b></td></tr>";
                                if (resultObj.BookingSource == HotelBookingSource.TBOConnect)
                                {
                                    tblRooms.InnerHtml += " <ul><li>Child Charge</li>";
                                    tblRooms.InnerHtml += " <li><b>" + roomResult.ChildCharges.ToString("N" + decimalValue) + "</b></td></tr>";
                                }

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";


                                decimal tempTotalP = 0;

                                tempTotalP = (roomResult.SellingFare + roomResult.Markup + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length));

                                //tempTotalP -= resultObj.Price.Discount;

                                tblRooms.InnerHtml += " <b>" + Math.Round(tempTotalP * minStay, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'><div id='divMeal" + resultObj.RoomDetails[y].RoomTypeCode + "' ></div></td></tr>";

                                tblRooms.InnerHtml += "</table></div>";
                                #endregion

                                tblRooms.InnerHtml += "</td><td>";

                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                    tblRooms.InnerHtml += " <script type='text/javascript'>";                                   
                                    tblRooms.InnerHtml += " document.getElementById('rCode" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                    tblRooms.InnerHtml += " document.getElementById('rCodeDefault" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                    tblRooms.InnerHtml += " document.getElementById('priceTag" + index + "-" + x + "').value = '" + totalSellRate + "';";                                   
                                    tblRooms.InnerHtml += "</script>";
                                    totFare += totalSellRate;
                                }

                                firstRoom++;
                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";
                            }
                        }
                        # endregion
                        # region LOH
                        else if (resultObj.BookingSource == HotelBookingSource.LOH)
                        {
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Split(',').Length > 1)
                            {
                                for (int k = 0; k < resultObj.RoomDetails[y].SequenceNo.Split(',').Length; k++)
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo.Split(',')[k]) == (x + 1))
                                    {
                                        double totalSellRate = 0;
                                        double totalPubRate = 0;
                                        HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                        totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));

                                        //if (resultObj.Currency == "USD")
                                        //{
                                        //    totalSellRate = totalSellRate * (double)rateOfEx[resultObj.Currency];
                                        //}
                                        
                                        tblRooms.InnerHtml += "    <tr>";
                                        tblRooms.InnerHtml += " <td>";
                                        tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                        if (firstRoom == 0)
                                        {
                                            tblRooms.InnerHtml += "checked=\'checked\'";
                                            totFare += (decimal)totalSellRate;
                                            discount = roomResult.Discount;
                                        }
                                        else
                                            tblRooms.InnerHtml += "";

                                       // if (x > 0)
                                       // {
                                            //tblRooms.InnerHtml += " disabled='disabled'";
                                       // }

                                        tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";

                                        //if (roomResult.RoomTypeName.Split('|').Length > 0)
                                        //{
                                        //    tblRooms.InnerHtml += "<b>" + (roomResult.RoomTypeName.Contains("|") ? roomResult.RoomTypeName.Split('|')[x] : roomResult.RoomTypeName) + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                        //}
                                        //else
                                        {
                                            //tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + " - " + roomResult.RoomTypeCode + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                            tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? "</b> Incl: " + roomResult.mealPlanDesc : "") + "</br>";
                                        }

                                        if (roomResult.Occupancy != null && roomResult.Occupancy.ContainsKey("ExtraBed") && roomResult.Occupancy["ExtraBed"] == 1)
                                        {
                                            tblRooms.InnerHtml += " + &nbsp;&nbsp;<b>Extra Bed available</b>";
                                        }

                                        if (request.NoOfRooms == 1)
                                        {
                                            tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                        }
                                        else
                                        {
                                            tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                        }
                                        tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                        if (roomResult.PromoMessage != null)
                                        {
                                            //tblRooms.InnerHtml += "<div style='position: relative; top: 20px; left: 10px; display: block'>";
                                            tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                            tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                            tblRooms.InnerHtml += roomResult.PromoMessage;
                                            tblRooms.InnerHtml += "</label></td></tr></table>";
                                        }

                                        tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin: 0px 0px 0px 0px;' id='AmenityDiv-" + index.ToString() + y + "'>";

                                        tblRooms.InnerHtml += "</div>";
                                        tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                        tblRooms.InnerHtml += "</div></td>";
                                        tblRooms.InnerHtml += "<td align='center'><div class='style1'>" + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                        tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span>";

                                        #region Fare Breakup
                                        tblRooms.InnerHtml += "<div class='links'>";

                                        //tblRooms.InnerHtml += "</div></td></tr><tr>";

                                        DateTime d1 = request.StartDate;
                                        DateTime d2 = request.EndDate;
                                        d2 = d2.AddDays(-1);
                                        int result = 0;
                                        // Get first full week
                                        DateTime firstDayOfFirstFullWeek = d1;
                                        if (d1.DayOfWeek != DayOfWeek.Sunday)
                                        {
                                            result += 1;
                                            firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                        }
                                        // Get last full week
                                        DateTime lastDayOfLastFullWeek = d2;
                                        if (d2.DayOfWeek != DayOfWeek.Saturday)
                                        {
                                            result += 1;
                                            lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                        }
                                        System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                        // Add number of full weeks
                                        result += (diffResult.Days + 1) / 7;



                                        //tblRooms.InnerHtml += "</tr><tr><td colspan='3'>";
                                        tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                        tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 0px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                        tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                        tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                        d1 = resultObj.StartDate;
                                        d2 = resultObj.EndDate;
                                        decimal totalRoomSellRate = 0;
                                        int days = d2.Subtract(d1).Days;
                                        for (int i = 0, j = 0; i < result; i++)
                                        {
                                            int week = i + 1;
                                            tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                            tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                            int l = 0;
                                            if (d1.DayOfWeek != DayOfWeek.Sunday)
                                            {
                                                int predeser = Convert.ToInt16(d1.DayOfWeek);
                                                for (; l < predeser; l++)
                                                {
                                                    tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                                }
                                            }

                                            for (; l < 7 && d1 < d2; l++)
                                            {
                                                RoomRates rmInfo = new RoomRates();
                                                j++;
                                                if (roomResult.Rates.Length > j)
                                                {
                                                    rmInfo = roomResult.Rates[j];
                                                }
                                                else
                                                {
                                                    rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                                }
                                                d1 = d1.AddDays(1);
                                                //if (resultObj.BookingSource != HotelBookingSource.RezLive)
                                                //{
                                                //    tblRooms.InnerHtml += "  <li>" + Math.Round(rmInfo.SellingFare, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                                //    totalRoomSellRate += rmInfo.SellingFare;
                                                //}
                                                //else
                                                {

                                                    if (roomResult.RoomTypeName.Contains("|"))
                                                    {
                                                        string[] names = roomResult.RoomTypeName.Split('|');
                                                        decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                                        tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                                        totalRoomSellRate += rate;

                                                        //if (resultObj.Currency != "AED")
                                                        //{
                                                        //    totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                                        //}
                                                    }
                                                    else
                                                    {
                                                        tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                                        totalRoomSellRate += ((decimal)totalSellRate / days);

                                                        //if (resultObj.Currency != "AED")
                                                        //{
                                                        //    totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                                        //}
                                                    }
                                                }
                                            }


                                            tblRooms.InnerHtml += "</tr>";
                                        }
                                        //totalRoomSellRate += roomResult.Markup;
                                        tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                        tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                        tblRooms.InnerHtml += "</tr>";
                                        //if (roomResult.Discount > 0)
                                        //{
                                        //    tblRooms.InnerHtml += "<ul><li>Discount</li> ";
                                        //    tblRooms.InnerHtml += "<li><b>" + (roomResult.Discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                        //}
                                        //tblRooms.InnerHtml += "<ul><li>Tax</li>";
                                        //tblRooms.InnerHtml += " <li><b>" + (roomResult.TotalTax).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";

                                        tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                        decimal extraGuestRate = 0;


                                        extraGuestRate = roomResult.SellExtraGuestCharges;
                                        tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                        tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                        decimal tempTotalP = 0;
                                        if (resultObj.BookingSource == HotelBookingSource.LOH)
                                        {
                                            tempTotalP = (roomResult.SellingFare + roomResult.Markup + roomResult.TotalTax);
                                        }

                                        tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                        tblRooms.InnerHtml += "</table></div>";
                                        #endregion

                                        tblRooms.InnerHtml += "</td><td>";
                                        if (firstRoom == 0)
                                        {
                                            //tblRooms.InnerHtml += " <script type='text/javascript'>";
                                            //tblRooms.InnerHtml += " document.getElementById('rCode" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                            //tblRooms.InnerHtml += " document.getElementById('rCodeDefault" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                            //if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                            //{
                                            //    tblRooms.InnerHtml += " document.getElementById('priceTag" + index + "-" + x + "').value = '" + totalSellRate + "';";
                                            //}
                                            //else
                                            //{
                                            //    tblRooms.InnerHtml += " document.getElementById('priceTag" + index + "-" + x + "').value = '" + totalPubRate + "';";
                                            //}
                                            //tblRooms.InnerHtml += "</script>";
                                            tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                            tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                            tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                        }
                                        firstRoom++;

                                        string roomTypeCode = "";
                                        //if (roomResult.RoomTypeCode.Length <= 0)
                                        {
                                            roomTypeCode = index.ToString() + x.ToString();
                                        }
                                        //else
                                        //{
                                        //    roomTypeCode = roomResult.RoomTypeCode;
                                        //}

                                        tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomTypeCode + "-" + y.ToString() + "'></div> ";
                                        tblRooms.InnerHtml += "<span class='fright width-140' onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomTypeCode + "-" + y.ToString() + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></div></td>";
                                    }
                                }
                            }
                            else if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo) == (x + 1))
                            {
                                double totalSellRate = 0;
                                double totalPubRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));
                                
                                //if (resultObj.Currency == "USD")
                                //{
                                //    totalSellRate = totalSellRate * (double)rateOfEx[resultObj.Currency];
                                //}

                                tblRooms.InnerHtml += "    <tr>";
                                tblRooms.InnerHtml += " <td>";
                                tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "checked=\'checked\'";
                                    totFare += (decimal)totalSellRate;
                                    discount = roomResult.Discount;
                                }
                                else
                                    tblRooms.InnerHtml += "";

                                if (x > 0)
                                {
                                    //tblRooms.InnerHtml += " disabled='disabled'";
                                }
                                tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";

                                //if (roomResult.RoomTypeName.Split('|').Length > 0)
                                //{
                                //    tblRooms.InnerHtml += "<b>" + (roomResult.RoomTypeName.Contains("|") ? roomResult.RoomTypeName.Split('|')[x] : roomResult.RoomTypeName) + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                //}
                                //else
                                {
                                    //tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + " - " + roomResult.RoomTypeCode + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                    tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? "</b> Incl: " + roomResult.mealPlanDesc : "") + "</br>";
                                }

                                //if (roomResult.Occupancy != null && roomResult.Occupancy.ContainsKey("ExtraBed") && roomResult.Occupancy["ExtraBed"] == 1)
                                //{
                                //    tblRooms.InnerHtml += " + &nbsp;&nbsp;<b>Extra Bed available</b>";
                                //}

                                if (request.NoOfRooms == 1)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                if (roomResult.PromoMessage != null)
                                {
                                    //tblRooms.InnerHtml += "<div style='position: relative; top: 20px; left: 10px; display: block'>";
                                    tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += roomResult.PromoMessage;
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }

                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin:0px 0px 0px 100px;' id='AmenityDiv-" + index.ToString() + y + "'>";

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'> "+ resultObj.Currency+" " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span></div>";

                                #region Fare Breakup
                                tblRooms.InnerHtml += "<div class='links'>";

                                //tblRooms.InnerHtml += "</div></td></tr><tr>";

                                DateTime d1 = request.StartDate;
                                DateTime d2 = request.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;



                                //tblRooms.InnerHtml += "</tr><tr><td colspan='3'>";
                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 0px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                int days = d2.Subtract(d1).Days;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    int week = i + 1;
                                    tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);
                                        //if (resultObj.BookingSource != HotelBookingSource.RezLive)
                                        //{
                                        //    tblRooms.InnerHtml += "  <li>" + Math.Round(rmInfo.SellingFare, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                        //    totalRoomSellRate += rmInfo.SellingFare;
                                        //}
                                        //else
                                        {

                                            if (roomResult.RoomTypeName.Contains("|"))
                                            {
                                                string[] names = roomResult.RoomTypeName.Split('|');
                                                decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                                tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                                totalRoomSellRate += rate;

                                                //if (resultObj.Currency != "AED")
                                                //{
                                                //    totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                                //}
                                            }
                                            else
                                            {
                                                tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                                totalRoomSellRate += ((decimal)totalSellRate / days);

                                                //if (resultObj.Currency != "AED")
                                                //{
                                                //    totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                                //}
                                            }
                                        }
                                    }


                                    tblRooms.InnerHtml += "</tr>";
                                }
                                //totalRoomSellRate += roomResult.Markup;
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                tblRooms.InnerHtml += "</tr>";
                                //if (roomResult.Discount > 0)
                                //{
                                //    tblRooms.InnerHtml += "<ul><li>Discount</li> ";
                                //    tblRooms.InnerHtml += "<li><b>" + (roomResult.Discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //}
                                //tblRooms.InnerHtml += "<ul><li>Tax</li>";
                                //tblRooms.InnerHtml += " <li><b>" + (roomResult.TotalTax).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                decimal extraGuestRate = 0;


                                extraGuestRate = roomResult.SellExtraGuestCharges;
                                tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                decimal tempTotalP = 0;
                                if (resultObj.BookingSource == HotelBookingSource.LOH)
                                {
                                    tempTotalP = (roomResult.SellingFare + roomResult.Markup + roomResult.TotalTax);
                                }

                                tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "</table></div>";
                                #endregion

                                tblRooms.InnerHtml += "</td><td>";
                                if (firstRoom == 0)
                                {
                                    //tblRooms.InnerHtml += " <script type='text/javascript'>";
                                    //tblRooms.InnerHtml += " document.getElementById('rCode" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                    //tblRooms.InnerHtml += " document.getElementById('rCodeDefault" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                    //if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                    //{
                                    //    tblRooms.InnerHtml += " document.getElementById('priceTag" + index + "-" + x + "').value = '" + totalSellRate + "';";
                                    //}
                                    //else
                                    //{
                                    //    tblRooms.InnerHtml += " document.getElementById('priceTag" + index + "-" + x + "').value = '" + totalPubRate + "';";
                                    //}
                                    //tblRooms.InnerHtml += "</script>";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                }
                                firstRoom++;

                                string roomTypeCode = "";
                                //if (roomResult.RoomTypeCode.Length <= 0)
                                {
                                    roomTypeCode = index.ToString() + x.ToString();
                                }
                                //else
                                //{
                                //    roomTypeCode = roomResult.RoomTypeCode;
                                //}

                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomTypeCode + "-" + y.ToString() + "'></div> ";
                                tblRooms.InnerHtml += "<span class='fright width-140' onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomTypeCode + "-" + y.ToString() + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></div></td>";

                            }
                        }
                        # endregion
                        //Added by brahmam 26.09.2014
                        # region HotelBeds   
                        if (resultObj.BookingSource == HotelBookingSource.HotelBeds)
                        {
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                            {
                                decimal totalSellRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                totalSellRate = (Convert.ToDecimal(roomResult.SellingFare + roomResult.Markup + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length) + roomResult.TotalTax));
                                //if (resultObj.RoomDetails[y].Occupancy["minStay"] > 0 && request.EndDate.Subtract(request.StartDate).Days < resultObj.RoomDetails[y].Occupancy["minStay"])
                                //{
                                //    totalSellRate = totalSellRate * resultObj.RoomDetails[y].Occupancy["minStay"];


                                //}
                                discount = roomResult.Discount;
                                if (roomSeries.Length <= 0)
                                {
                                    roomSeries = (request.RoomGuest[x].noOfAdults + request.RoomGuest[x].noOfChild) + "-" + y.ToString();
                                }
                                else
                                {
                                    if (!roomSeries.EndsWith("|"))
                                    {
                                        roomSeries += "," + (request.RoomGuest[x].noOfAdults + request.RoomGuest[x].noOfChild) + "-" + y.ToString();
                                    }
                                    else
                                    {
                                        roomSeries += (request.RoomGuest[x].noOfAdults + request.RoomGuest[x].noOfChild) + "-" + y.ToString();
                                    }
                                }
                                tblRooms.InnerHtml += "<tr>";
                                //if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                {
                                    tblRooms.InnerHtml += "<td>";
                                    tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                    if (firstRoom == 0)
                                        tblRooms.InnerHtml += "checked=\'checked\'";
                                    else
                                        tblRooms.InnerHtml += "";

                                    if (uniqueRooms.Length > 0 && uniqueRooms.Contains(x.ToString()) && uniqueRooms.IndexOf(x.ToString()) > 0)
                                    {
                                        tblRooms.InnerHtml += " disabled='disabled' ";
                                    }

                                    tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode+"#"+(request.RoomGuest[x].noOfAdults+request.RoomGuest[x].noOfChild) + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";
                                    tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rTCode" + x + "-" + y + "' id='rTCode" + x + "-" + y + "' value='" + roomResult.RoomTypeCode.Split('|')[3] + "' /> ";

                                

                                if (request.NoOfRooms == 1)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";
                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin: 0px 0px 0px 0px;' id='AmenityDiv-" + index.ToString() + y + "'>";
                                tblRooms.InnerHtml += "<span id='amenityClo' title='close' style='font-size: 24px; cursor: pointer; position: absolute;right: 10px; top: 5px' onclick=\"hideAmenities('AmenityDiv-" + index.ToString() + y + "')\"><a class='closex' href='#'>X</a></span>";
                                if (resultObj.RoomDetails[y].Amenities != null && resultObj.RoomDetails[y].Amenities.Count > 0)
                                {
                                    tblRooms.InnerHtml += "<div class='showMsgHeading'>Room Amenities</div>";
                                    tblRooms.InnerHtml += "<div style='overflow:auto;height:170px'><ul>";

                                    foreach (string amenity in resultObj.RoomDetails[y].Amenities)
                                    {
                                        tblRooms.InnerHtml += "<li style='padding-left:10px; padding-top:3px'>" + amenity + "</li>";
                                    }
                                    tblRooms.InnerHtml += "</ul></div>";
                                }

                                if (roomResult.PromoMessage != null)
                                {
                                    //tblRooms.InnerHtml += "<div style='position: relative; top: 20px; left: 10px; display: block'>";
                                    tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += roomResult.PromoMessage;
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }

                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";
                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'>"+resultObj.Currency+" " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span>";

                                #region Fare Breakup

                                tblRooms.InnerHtml += "<div class='links'>";

                                DateTime d1 = resultObj.StartDate;
                                DateTime d2 = resultObj.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;


                                //tblRooms.InnerHtml += "<table><tr><td colspan='3'>";
                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr ><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 5px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    tblRooms.InnerHtml += "<tr  style='width: 550px;font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td >Week " + (i + 1) + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        rmInfo = roomResult.Rates[j++];
                                        d1 = d1.AddDays(1);

                                        tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / (request.EndDate.Subtract(request.StartDate).Days), decimalValue).ToString("N" + decimalValue) + "</td>";



                                        totalRoomSellRate += rmInfo.SellingFare;
                                    }

                                    tblRooms.InnerHtml += "</tr>";
                                }
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round((totalRoomSellRate + roomResult.Markup), decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                //tblRooms.InnerHtml += "</ul>";
                                //if (resultObj.Price.Discount > 0)
                                //{
                                //    tblRooms.InnerHtml += "<ul><li>Discount : ";
                                //    tblRooms.InnerHtml += "<b>" + (resultObj.Price.Discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //}
                                //tblRooms.InnerHtml += "<ul><li>Tax</li>";
                                //tblRooms.InnerHtml += " <li><b>" + (roomResult.TotalTax).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";

                                decimal extraGuestRate = 0;
                                if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                {
                                    extraGuestRate = roomResult.SellExtraGuestCharges * roomResult.Rates.Length;
                                }
                                else
                                {
                                    extraGuestRate = roomResult.SellExtraGuestCharges;
                                }

                                tblRooms.InnerHtml += " <b>" + Math.Round(extraGuestRate, 2).ToString("N" + decimalValue) + "</b></td></tr>";
                                if (resultObj.BookingSource == HotelBookingSource.TBOConnect)
                                {
                                    tblRooms.InnerHtml += " <ul><li>Child Charge</li>";
                                    tblRooms.InnerHtml += " <li><b>" + roomResult.ChildCharges.ToString("N" + decimalValue) + "</b></td></tr>";
                                }

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";


                                decimal tempTotalP = 0;

                                tempTotalP = (roomResult.SellingFare + roomResult.Markup + roomResult.TotalTax + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length));

                                //tempTotalP -= resultObj.Price.Discount;

                                tblRooms.InnerHtml += " <b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'><div id='divMeal" + resultObj.RoomDetails[y].RoomTypeCode + "' ></div></td></tr>";

                                tblRooms.InnerHtml += "</table></div>";
                                #endregion

                                tblRooms.InnerHtml += "</td><td>";

                                if (firstRoom == 0)
                                {
                                    //tblRooms.InnerHtml += " <script type='text/javascript'>";
                                    //tblRooms.InnerHtml += " document.getElementById('rCode" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                    //tblRooms.InnerHtml += " document.getElementById('rCodeDefault" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                    //tblRooms.InnerHtml += " document.getElementById('priceTag" + index + "-" + x + "').value = '" + totalSellRate + "';";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                    //tblRooms.InnerHtml += " <script type='text/javascript'>";
                                    //tblRooms.InnerHtml += " document.getElementById('rCode" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                    //tblRooms.InnerHtml += " document.getElementById('rCodeDefault" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                    //tblRooms.InnerHtml += " document.getElementById('priceTag" + index + "-" + x + "').value = '" + totalSellRate + "';";
                                    //tblRooms.InnerHtml += "</script>";
                                    totFare += totalSellRate;
                                }

                                firstRoom++;
                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";
                            }
                        }
                        # endregion

                        //Added by brahmam 17.11.2014
                        #region GTA
                        if (resultObj.BookingSource == HotelBookingSource.GTA)
                        {
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                            {
                                double totalSellRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));

                                tblRooms.InnerHtml += "    <tr>";
                                tblRooms.InnerHtml += " <td>";
                                tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "checked=\'checked\'";
                                    totFare += (decimal)totalSellRate;
                                    discount = roomResult.Discount;
                                }
                                else
                                    tblRooms.InnerHtml += "";

                                if (x > 0)
                                {
                                    //tblRooms.InnerHtml += " disabled='disabled'";
                                }
                                tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";

                                {
                                    tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";
                                    //tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? "</b> Incl: " + roomResult.mealPlanDesc : "") + "</br>";
                                }

                                if (request.NoOfRooms == 1)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                tblRooms.InnerHtml += "<input  type='hidden' name='NoOfCots" + index.ToString() + x + "-" + y + "' id='NoOfCots" + index.ToString() + x + "-" + y + "' value='" + resultObj.RoomDetails[y].NumberOfCots.ToString() + "' /> ";
                                tblRooms.InnerHtml += "<input  type='hidden' name='SharingBedding" + index.ToString() + x + "-" + y + "' id='SharingBedding" + index.ToString() + x + "-" + y + "' value='" + resultObj.RoomDetails[y].SharingBedding.ToString() + "' /> ";

                                if (roomResult.PromoMessage != null)
                                {
                                    //tblRooms.InnerHtml += "<div style='position: relative; top: 20px; left: 10px; display: block'>";
                                    tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += roomResult.PromoMessage;
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }

                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin: 0px 0px 0px 0px;' id='AmenityDiv-" + index.ToString() + y + "'>";

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'> " + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span></div>";

                                #region Fare Breakup
                                tblRooms.InnerHtml += "<div class='links'>";

                                //tblRooms.InnerHtml += "</div></td></tr><tr>";

                                DateTime d1 = request.StartDate;
                                DateTime d2 = request.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;



                                //tblRooms.InnerHtml += "</tr><tr><td colspan='3'>";
                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 5px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                int days = d2.Subtract(d1).Days;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    int week = i + 1;
                                    tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);
                                        if (roomResult.RoomTypeName.Contains("|"))
                                        {
                                            string[] names = roomResult.RoomTypeName.Split('|');
                                            decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                            tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += rate;
                                        }
                                        else
                                        {
                                            tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += ((decimal)totalSellRate / days);
                                        }
                                        
                                    }


                                    tblRooms.InnerHtml += "</tr>";
                                }
                                //totalRoomSellRate += roomResult.Markup;
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                tblRooms.InnerHtml += "</tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                decimal extraGuestRate = 0;


                                extraGuestRate = roomResult.SellExtraGuestCharges;
                                tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                decimal tempTotalP = 0;
                                if (resultObj.BookingSource == HotelBookingSource.LOH)
                                {
                                    tempTotalP = (roomResult.SellingFare + roomResult.Markup + roomResult.TotalTax);
                                }

                                tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "</table></div>";
                                #endregion

                                tblRooms.InnerHtml += "</td><td>";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                }
                                firstRoom++;

                                //string roomTypeCode = "";
                                ////if (roomResult.RoomTypeCode.Length <= 0)
                                //{
                                //    roomTypeCode = index.ToString() + x.ToString();
                                //}
                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";

                            }
                        }
                        #endregion

                        # region WST
                        if (resultObj.BookingSource == HotelBookingSource.WST) //modified by brahmam based on new code
                        {
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                            {
                                double totalSellRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));

                                tblRooms.InnerHtml += "    <tr>";
                                tblRooms.InnerHtml += " <td>";
                                tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "checked=\'checked\'";
                                    totFare += (decimal)totalSellRate;
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "";
                                }
                                tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";
                                tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";
                                if (roomResult.IsExtraBed)
                                {
                                    tblRooms.InnerHtml += " + &nbsp;&nbsp;<b>Extra Bed available</b>";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";
                               
                                ////getting promotion roomwise
                                //string[] rpCode = roomResult.RatePlanCode.Split('|');
                                //// RATEPLANCODE-------- mealcode|rmcatCode|rateType|promotion
                                //if (rpCode.Length > 2 && rpCode[3] == "1")
                                //{
                                //    CT.BookingEngine.GDS.WST wstApi = new CT.BookingEngine.GDS.WST();
                                //    roomResult.PromoMessage = wstApi.GetPromotion(roomResult.RatePlanCode, roomResult.RoomTypeCode, resultObj.HotelCode);
                                //    if (roomResult.PromoMessage != null)
                                //    {
                                //        tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                //        tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                //        tblRooms.InnerHtml += roomResult.PromoMessage;
                                //        tblRooms.InnerHtml += "</label></td></tr></table>";
                                //    }
                                //}
                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none; text-align: left;margin: 0px 0px 0px 0px;' id='AmenityDiv-" + index.ToString() + y + "'>";

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'> " + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span></div>";

                                #region Fare Breakup
                                tblRooms.InnerHtml += "<div class='links'>";
                                DateTime d1 = request.StartDate;
                                DateTime d2 = request.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;



                                //tblRooms.InnerHtml += "</tr><tr><td colspan='3'>";
                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 5px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                int days = d2.Subtract(d1).Days;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    int week = i + 1;
                                    tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);
                                        if (roomResult.RoomTypeName.Contains("|"))
                                        {
                                            string[] names = roomResult.RoomTypeName.Split('|');
                                            decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                            tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += rate;
                                        }
                                        else
                                        {
                                            tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += ((decimal)totalSellRate / days);
                                        }

                                    }


                                    tblRooms.InnerHtml += "</tr>";
                                }
                                //totalRoomSellRate += roomResult.Markup;
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                tblRooms.InnerHtml += "</tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                decimal extraGuestRate = 0;


                                extraGuestRate = roomResult.SellExtraGuestCharges;
                                tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                decimal tempTotalP = totalRoomSellRate + extraGuestRate;


                                tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "</table></div>";
                                #endregion
                                tblRooms.InnerHtml += "</td><td>";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                }
                                firstRoom++;
                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName.Replace("'", "\\'") + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";
                            }
                        }
                        # endregion
                        #region HIS
                        else if (resultObj.BookingSource == HotelBookingSource.HotelConnect)
                        {
                            if (resultObj.RoomDetails[y].SequenceNo != null && (resultObj.RoomDetails[y].SequenceNo).Contains((x + 1).ToString()))
                            {
                                decimal totalSellRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                totalSellRate = (Convert.ToDecimal(roomResult.SellingFare + roomResult.Markup + (roomResult.SellExtraGuestCharges * roomResult.Rates.Length) + roomResult.TotalTax));

                                discount = roomResult.Discount;
                                //if (resultObj.RoomDetails[y].DiscountType == "P")
                                //{
                                //    totalSellRate = totalSellRate - resultObj.RoomDetails[y].Discount;
                                //}
                                //else
                                //{
                                //    decimal tmpDiscount = resultObj.RoomDetails[y].Discount / request.NoOfRooms;
                                //    totalSellRate = totalSellRate - tmpDiscount;
                                //}
                                tblRooms.InnerHtml += "<tr>";
                                if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                {
                                    tblRooms.InnerHtml += "<td>";
                                    tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                    if (firstRoom == 0)
                                        tblRooms.InnerHtml += "checked=\'checked\'";
                                    else
                                        tblRooms.InnerHtml += "";

                                    tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";
                                    tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";
                                }
                                if (roomResult.Occupancy != null && roomResult.Occupancy.ContainsKey("ExtraBed") && roomResult.Occupancy["ExtraBed"] == 1)
                                {
                                    tblRooms.InnerHtml += " + &nbsp;&nbsp;<b>Extra Bed included</b>";
                                }



                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin: 0px 0px 0px 0px;' id='AmenityDiv-" + index.ToString() + y + "'>";
                                tblRooms.InnerHtml += "<span id='amenityClo' title='close' style='font-size: 24px; cursor: pointer; position: absolute;right: 10px; top: 0px' onclick=\"hideAmenities('AmenityDiv-" + index.ToString() + y + "')\"><a class='closex' href='#'>X</a></span>";
                                tblRooms.InnerHtml += "<div class='showMsgHeading'>Room Amenities</div>";
                                if (resultObj.RoomDetails[y].Amenities != null && resultObj.RoomDetails[y].Amenities.Count > 0)
                                {
                                    
                                    tblRooms.InnerHtml += "<div style='overflow:auto;height:170px'><ul>";

                                    foreach (string amenity in resultObj.RoomDetails[y].Amenities)
                                    {
                                        tblRooms.InnerHtml += "<li style='padding-left:10px; padding-top:3px'>" + amenity + "</li>";
                                    }
                                    tblRooms.InnerHtml += "</ul></div>";
                                }

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowRoomAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'>" + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a style='line-height:20px;' href='#'>Rate Breakup</a></span>";

                                #region Fare Breakup

                                tblRooms.InnerHtml += "<div class='links'>";

                                DateTime d1 = resultObj.StartDate;
                                DateTime d2 = resultObj.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;
                                                                
                                int days = request.EndDate.Subtract(request.StartDate).Days;

                                //tblRooms.InnerHtml += "<table><tr><td colspan='3'>";
                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr ><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 0px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;

                                decimal totalRoomSellRate = 0;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td >Week " + (i + 1) + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);

                                        tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / (days), decimalValue).ToString("N" + decimalValue) + "</td>";

                                        totalRoomSellRate += totalSellRate / (days);
                                    }

                                    tblRooms.InnerHtml += "</tr>";
                                }
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round((totalRoomSellRate ), decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                //tblRooms.InnerHtml += "</ul>";
                                //if (resultObj.Price.Discount > 0)
                                //{
                                //    tblRooms.InnerHtml += "<ul><li>Discount : ";
                                //    tblRooms.InnerHtml += "<b>" + (resultObj.Price.Discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //}
                                //tblRooms.InnerHtml += "<ul><li>Tax</li>";
                                //tblRooms.InnerHtml += " <li><b>" + (roomResult.TotalTax).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";

                                decimal extraGuestRate = 0;
                                if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                {
                                    extraGuestRate = roomResult.SellExtraGuestCharges * roomResult.Rates.Length;
                                }
                                else
                                {
                                    extraGuestRate = roomResult.SellExtraGuestCharges;
                                }

                                tblRooms.InnerHtml += " <b>" + Math.Round(extraGuestRate, 2).ToString("N" + decimalValue) + "</b></td></tr>";
                                if (resultObj.BookingSource == HotelBookingSource.TBOConnect)
                                {
                                    tblRooms.InnerHtml += " <ul><li>Child Charge</li>";
                                    tblRooms.InnerHtml += " <li><b>" + roomResult.ChildCharges.ToString("N" + decimalValue) + "</b></td></tr>";
                                }

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";


                                decimal tempTotalP = 0;

                                tempTotalP = (roomResult.SellingFare + roomResult.Markup + roomResult.TotalTax );

                                //tempTotalP -= resultObj.Price.Discount;

                                tblRooms.InnerHtml += " <b>" + Math.Round(tempTotalP , decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                

                                tblRooms.InnerHtml += "</table></div>";
                                #endregion

                                tblRooms.InnerHtml += "</td><td>";

                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                    tblRooms.InnerHtml += "<script type='text/javascript'>";
                                    tblRooms.InnerHtml += "document.getElementById('rCode" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                    tblRooms.InnerHtml += "document.getElementById('rCodeDefault" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                    tblRooms.InnerHtml += "document.getElementById('priceTag" + index + "-" + x + "').value = '" + totalSellRate + "';";
                                    tblRooms.InnerHtml += "</script>";
                                    totFare += totalSellRate;
                                }

                                firstRoom++;
                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + index + "')\"><a href='#' style='line-height:20px;' >Cancellation Details</a></span></a></div></td>";
                            }
                        }
                        #endregion
                        #region MIKI
                        if (resultObj.BookingSource == HotelBookingSource.Miki)
                        {
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                            {
                                double totalSellRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));

                                tblRooms.InnerHtml += "    <tr>";
                                tblRooms.InnerHtml += " <td>";
                                tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "checked=\'checked\'";
                                    totFare += (decimal)totalSellRate;
                                    
                                }
                                else
                                    tblRooms.InnerHtml += "";
                                tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";

                                {
                                    tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";
                                }

                                if (request.NoOfRooms == 1)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                if (roomResult.PromoMessage != null)
                                {
                                    //tblRooms.InnerHtml += "<div style='position: relative; top: 20px; left: 10px; display: block'>";
                                    tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += roomResult.PromoMessage;
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }

                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none; text-align: left;margin: 0px 0px 0px 0px;' id='AmenityDiv-" + index.ToString() + y + "'>";

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'> " + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span></div>";

                                #region Fare Breakup
                                tblRooms.InnerHtml += "<div class='links'>";
                                DateTime d1 = request.StartDate;
                                DateTime d2 = request.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;



                                //tblRooms.InnerHtml += "</tr><tr><td colspan='3'>";
                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 0px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                int days = d2.Subtract(d1).Days;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    int week = i + 1;
                                    tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);
                                        if (roomResult.RoomTypeName.Contains("|"))
                                        {
                                            string[] names = roomResult.RoomTypeName.Split('|');
                                            decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                            tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += rate;
                                        }
                                        else
                                        {
                                            tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += ((decimal)totalSellRate / days);
                                        }

                                    }


                                    tblRooms.InnerHtml += "</tr>";
                                }
                                //totalRoomSellRate += roomResult.Markup;
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                tblRooms.InnerHtml += "</tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                decimal extraGuestRate = 0;


                                extraGuestRate = roomResult.SellExtraGuestCharges;
                                tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                decimal tempTotalP = totalRoomSellRate + extraGuestRate;


                                tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "</table></div>";
                                #endregion

                                tblRooms.InnerHtml += "</td><td>";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                }
                                firstRoom++;

                                //string roomTypeCode = "";
                                ////if (roomResult.RoomTypeCode.Length <= 0)
                                //{
                                //    roomTypeCode = index.ToString() + x.ToString();
                                //}
                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName.Replace("'", "\\'") + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";

                            }
                        }
                        #endregion

                        //Added by brahmam 18.09.2015 ....req Rezlive Rooms Binding
                        #region RezLive
                        if (resultObj.BookingSource == HotelBookingSource.RezLive)
                        {
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                            {
                                double totalSellRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));

                                tblRooms.InnerHtml += "    <tr>";
                                tblRooms.InnerHtml += " <td>";
                                tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "checked=\'checked\'";
                                    totFare += (decimal)totalSellRate;
                                    discount = roomResult.Discount;
                                }
                                else
                                    tblRooms.InnerHtml += "";

                                if (x > 0)
                                {
                                    //tblRooms.InnerHtml += " disabled='disabled'";
                                }
                                tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";

                                {
                                    tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";
                                }

                                if (request.NoOfRooms == 1)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                tblRooms.InnerHtml += "<input  type='hidden' name='NoOfCots" + index.ToString() + x + "-" + y + "' id='NoOfCots" + index.ToString() + x + "-" + y + "' value='" + resultObj.RoomDetails[y].NumberOfCots.ToString() + "' /> ";
                                tblRooms.InnerHtml += "<input  type='hidden' name='SharingBedding" + index.ToString() + x + "-" + y + "' id='SharingBedding" + index.ToString() + x + "-" + y + "' value='" + resultObj.RoomDetails[y].SharingBedding.ToString() + "' /> ";

                                if (roomResult.PromoMessage != null)
                                {
                                    //tblRooms.InnerHtml += "<div style='position: relative; top: 20px; left: 10px; display: block'>";
                                    tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += roomResult.PromoMessage;
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }

                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin: 0px 0px 0px 0px;' id='AmenityDiv-" + index.ToString() + y + "'>";

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'> " + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span></div>";

                                #region Fare Breakup
                                tblRooms.InnerHtml += "<div class='links'>";

                                //tblRooms.InnerHtml += "</div></td></tr><tr>";

                                DateTime d1 = request.StartDate;
                                DateTime d2 = request.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;



                                //tblRooms.InnerHtml += "</tr><tr><td colspan='3'>";
                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 0px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                int days = d2.Subtract(d1).Days;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    int week = i + 1;
                                    tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);
                                        if (roomResult.RoomTypeName.Contains("|"))
                                        {
                                            string[] names = roomResult.RoomTypeName.Split('|');
                                            decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                            tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += rate;
                                        }
                                        else
                                        {
                                            tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += ((decimal)totalSellRate / days);
                                        }

                                    }


                                    tblRooms.InnerHtml += "</tr>";
                                }
                                //totalRoomSellRate += roomResult.Markup;
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                tblRooms.InnerHtml += "</tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                decimal extraGuestRate = 0;


                                extraGuestRate = roomResult.SellExtraGuestCharges;
                                tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                decimal tempTotalP = 0;
                                tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "</table></div>";
                                #endregion

                                tblRooms.InnerHtml += "</td><td>";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                }
                                firstRoom++;

                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName.Replace("'", "\\'") + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";

                            }
                        }
                        #endregion

                          //added by brahmam
                        #region TBOHotel
                        if (resultObj.BookingSource == HotelBookingSource.TBOHotel)
                        {
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                            {
                                double totalSellRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));

                                tblRooms.InnerHtml += "    <tr>";
                                tblRooms.InnerHtml += " <td>";
                                tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "checked=\'checked\'";
                                    totFare += (decimal)totalSellRate;
                                    discount = roomResult.Discount;
                                }
                                else
                                    tblRooms.InnerHtml += "";

                                tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";


                                tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";



                                if (request.NoOfRooms == 1)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";
                                if (roomResult.PromoMessage != null)
                                {
                                    tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += roomResult.PromoMessage;
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }

                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin: 0px 0px 0px 0px;' id='AmenityDiv-" + index.ToString() + y + "'>";
                                tblRooms.InnerHtml += "<span id='amenityClo' title='close' style='font-size: 24px; cursor: pointer; position: absolute;right: 10px; top: 0px' onclick=\"hideAmenities('AmenityDiv-" + index.ToString() + y + "')\"><a class='closex' href='#'>X</a></span>";
                                if (resultObj.RoomDetails[y].Amenities != null && resultObj.RoomDetails[y].Amenities.Count > 0)
                                {
                                    tblRooms.InnerHtml += "<div class='showMsgHeading'>Room Amenities</div>";
                                    tblRooms.InnerHtml += "<div style='overflow:auto;height:170px'><ul>";

                                    foreach (string amenity in resultObj.RoomDetails[y].Amenities)
                                    {
                                        tblRooms.InnerHtml += "<li style='padding-left:10px; padding-top:3px'>" + amenity + "</li>";
                                    }
                                    tblRooms.InnerHtml += "</ul></div>";
                                }

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowRoomAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'> " + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span></div>";

                                #region Fare Breakup
                                tblRooms.InnerHtml += "<div class='links'>";

                                //tblRooms.InnerHtml += "</div></td></tr><tr>";

                                DateTime d1 = request.StartDate;
                                DateTime d2 = request.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;

                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 0px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                int days = d2.Subtract(d1).Days;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    int week = i + 1;
                                    tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);
                                        if (roomResult.RoomTypeName.Contains("|"))
                                        {
                                            string[] names = roomResult.RoomTypeName.Split('|');
                                            decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                            tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += rate;
                                        }
                                        else
                                        {
                                            tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += ((decimal)totalSellRate / days);
                                        }

                                    }
                                    tblRooms.InnerHtml += "</tr>";
                                }
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                tblRooms.InnerHtml += "</tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                decimal extraGuestRate = 0;


                                extraGuestRate = roomResult.SellExtraGuestCharges;
                                tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                decimal tempTotalP = 0;
                                tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "</table></div>";
                                #endregion

                                tblRooms.InnerHtml += "</td><td>";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                }
                                firstRoom++;
                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName.Replace("'", "\\'") + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";

                            }
                        }
                        #endregion
                        //Added by brahma 02.07.2016 ..Jac rooms binding
                        # region JAC
                        if (resultObj.BookingSource == HotelBookingSource.JAC)
                        {
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                            {
                                double totalSellRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));

                                tblRooms.InnerHtml += "    <tr>";
                                tblRooms.InnerHtml += " <td>";
                                tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "checked=\'checked\'";
                                    totFare += (decimal)totalSellRate;
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "";
                                }
                                tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";
                                tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";
                                if (roomResult.IsExtraBed)
                                {
                                    tblRooms.InnerHtml += " + &nbsp;&nbsp;<b>Extra Bed available</b>";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                if (roomResult.PromoMessage != null)
                                {
                                    tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += roomResult.PromoMessage;
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }

                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none; text-align: left;' id='AmenityDiv-" + index.ToString() + y + "'>";

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'> " + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span></div>";

                                #region Fare Breakup
                                tblRooms.InnerHtml += "<div class='links'>";
                                DateTime d1 = request.StartDate;
                                DateTime d2 = request.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;

                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 0px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                int days = d2.Subtract(d1).Days;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    int week = i + 1;
                                    tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);
                                        if (roomResult.RoomTypeName.Contains("|"))
                                        {
                                            string[] names = roomResult.RoomTypeName.Split('|');
                                            decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                            tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += rate;
                                        }
                                        else
                                        {
                                            tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += ((decimal)totalSellRate / days);
                                        }

                                    }


                                    tblRooms.InnerHtml += "</tr>";
                                }
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                tblRooms.InnerHtml += "</tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                decimal extraGuestRate = 0;


                                extraGuestRate = roomResult.SellExtraGuestCharges;
                                tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                decimal tempTotalP = totalRoomSellRate + extraGuestRate;


                                tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "</table></div>";
                                #endregion
                                tblRooms.InnerHtml += "</td><td>";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                }
                                firstRoom++;
                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName.Replace("'", "\\'") + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";
                            }
                        }
                        # endregion

                        //Added by brahmam 03.10.2016
                        # region EET
                        else if (resultObj.BookingSource == HotelBookingSource.EET)
                        {
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Split(',').Length > 1)
                            {
                                for (int k = 0; k < resultObj.RoomDetails[y].SequenceNo.Split(',').Length; k++)
                                {
                                    if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo.Split(',')[k]) == (x + 1))
                                    {
                                        double totalSellRate = 0;
                                        double totalPubRate = 0;
                                        HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                        totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));

                                        //if (resultObj.Currency == "USD")
                                        //{
                                        //    totalSellRate = totalSellRate * (double)rateOfEx[resultObj.Currency];
                                        //}

                                        tblRooms.InnerHtml += "    <tr>";
                                        tblRooms.InnerHtml += " <td>";
                                        tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                        if (firstRoom == 0)
                                        {
                                            tblRooms.InnerHtml += "checked=\'checked\'";
                                            totFare += (decimal)totalSellRate;
                                            discount = roomResult.Discount;
                                        }
                                        else
                                            tblRooms.InnerHtml += "";

                                        // if (x > 0)
                                        // {
                                        //tblRooms.InnerHtml += " disabled='disabled'";
                                        // }

                                        tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";

                                        //if (roomResult.RoomTypeName.Split('|').Length > 0)
                                        //{
                                        //    tblRooms.InnerHtml += "<b>" + (roomResult.RoomTypeName.Contains("|") ? roomResult.RoomTypeName.Split('|')[x] : roomResult.RoomTypeName) + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                        //}
                                        //else
                                        {
                                            //tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + " - " + roomResult.RoomTypeCode + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                            tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? "</b> Incl: " + roomResult.mealPlanDesc : "") + "</br>";
                                        }

                                        if (roomResult.Occupancy != null && roomResult.Occupancy.ContainsKey("ExtraBed") && roomResult.Occupancy["ExtraBed"] == 1)
                                        {
                                            tblRooms.InnerHtml += " + &nbsp;&nbsp;<b>Extra Bed available</b>";
                                        }

                                        if (request.NoOfRooms == 1)
                                        {
                                            tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                        }
                                        else
                                        {
                                            tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                        }
                                        tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                        if (roomResult.PromoMessage != null)
                                        {
                                            //tblRooms.InnerHtml += "<div style='position: relative; top: 20px; left: 10px; display: block'>";
                                            tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                            tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                            tblRooms.InnerHtml += roomResult.PromoMessage;
                                            tblRooms.InnerHtml += "</label></td></tr></table>";
                                        }

                                        tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin: 0px 0px 0px 0px;' id='AmenityDiv-" + index.ToString() + y + "'>";

                                        tblRooms.InnerHtml += "</div>";
                                        tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                        tblRooms.InnerHtml += "</div></td>";
                                        tblRooms.InnerHtml += "<td align='center'><div class='style1'>" + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                        tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span>";

                                        #region Fare Breakup
                                        tblRooms.InnerHtml += "<div class='links'>";

                                        //tblRooms.InnerHtml += "</div></td></tr><tr>";

                                        DateTime d1 = request.StartDate;
                                        DateTime d2 = request.EndDate;
                                        d2 = d2.AddDays(-1);
                                        int result = 0;
                                        // Get first full week
                                        DateTime firstDayOfFirstFullWeek = d1;
                                        if (d1.DayOfWeek != DayOfWeek.Sunday)
                                        {
                                            result += 1;
                                            firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                        }
                                        // Get last full week
                                        DateTime lastDayOfLastFullWeek = d2;
                                        if (d2.DayOfWeek != DayOfWeek.Saturday)
                                        {
                                            result += 1;
                                            lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                        }
                                        System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                        // Add number of full weeks
                                        result += (diffResult.Days + 1) / 7;



                                        //tblRooms.InnerHtml += "</tr><tr><td colspan='3'>";
                                        tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                        tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 0px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                        tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                        tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                        d1 = resultObj.StartDate;
                                        d2 = resultObj.EndDate;
                                        decimal totalRoomSellRate = 0;
                                        int days = d2.Subtract(d1).Days;
                                        for (int i = 0, j = 0; i < result; i++)
                                        {
                                            int week = i + 1;
                                            tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                            tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                            int l = 0;
                                            if (d1.DayOfWeek != DayOfWeek.Sunday)
                                            {
                                                int predeser = Convert.ToInt16(d1.DayOfWeek);
                                                for (; l < predeser; l++)
                                                {
                                                    tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                                }
                                            }

                                            for (; l < 7 && d1 < d2; l++)
                                            {
                                                RoomRates rmInfo = new RoomRates();
                                                j++;
                                                if (roomResult.Rates.Length > j)
                                                {
                                                    rmInfo = roomResult.Rates[j];
                                                }
                                                else
                                                {
                                                    rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                                }
                                                d1 = d1.AddDays(1);
                                                //if (resultObj.BookingSource != HotelBookingSource.RezLive)
                                                //{
                                                //    tblRooms.InnerHtml += "  <li>" + Math.Round(rmInfo.SellingFare, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                                //    totalRoomSellRate += rmInfo.SellingFare;
                                                //}
                                                //else
                                                {

                                                    if (roomResult.RoomTypeName.Contains("|"))
                                                    {
                                                        string[] names = roomResult.RoomTypeName.Split('|');
                                                        decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                                        tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                                        totalRoomSellRate += rate;

                                                        //if (resultObj.Currency != "AED")
                                                        //{
                                                        //    totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                                        //}
                                                    }
                                                    else
                                                    {
                                                        tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                                        totalRoomSellRate += ((decimal)totalSellRate / days);

                                                        //if (resultObj.Currency != "AED")
                                                        //{
                                                        //    totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                                        //}
                                                    }
                                                }
                                            }


                                            tblRooms.InnerHtml += "</tr>";
                                        }
                                        //totalRoomSellRate += roomResult.Markup;
                                        tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                        tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                        tblRooms.InnerHtml += "</tr>";
                                        //if (roomResult.Discount > 0)
                                        //{
                                        //    tblRooms.InnerHtml += "<ul><li>Discount</li> ";
                                        //    tblRooms.InnerHtml += "<li><b>" + (roomResult.Discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                        //}
                                        //tblRooms.InnerHtml += "<ul><li>Tax</li>";
                                        //tblRooms.InnerHtml += " <li><b>" + (roomResult.TotalTax).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";

                                        tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                        decimal extraGuestRate = 0;


                                        extraGuestRate = roomResult.SellExtraGuestCharges;
                                        tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                        tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                        decimal tempTotalP = 0;
                                        if (resultObj.BookingSource == HotelBookingSource.EET)
                                        {
                                            tempTotalP = (roomResult.SellingFare + roomResult.Markup + roomResult.TotalTax);
                                        }

                                        tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                        tblRooms.InnerHtml += "</table></div>";
                                        #endregion

                                        tblRooms.InnerHtml += "</td><td>";
                                        if (firstRoom == 0)
                                        {
                                            //tblRooms.InnerHtml += " <script type='text/javascript'>";
                                            //tblRooms.InnerHtml += " document.getElementById('rCode" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                            //tblRooms.InnerHtml += " document.getElementById('rCodeDefault" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                            //if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                            //{
                                            //    tblRooms.InnerHtml += " document.getElementById('priceTag" + index + "-" + x + "').value = '" + totalSellRate + "';";
                                            //}
                                            //else
                                            //{
                                            //    tblRooms.InnerHtml += " document.getElementById('priceTag" + index + "-" + x + "').value = '" + totalPubRate + "';";
                                            //}
                                            //tblRooms.InnerHtml += "</script>";
                                            tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                            tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                            tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                        }
                                        firstRoom++;

                                        string roomTypeCode = "";
                                        //if (roomResult.RoomTypeCode.Length <= 0)
                                        {
                                            roomTypeCode = index.ToString() + x.ToString();
                                        }
                                        //else
                                        //{
                                        //    roomTypeCode = roomResult.RoomTypeCode;
                                        //}

                                        tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomTypeCode + "-" + y.ToString() + "'></div> ";
                                        tblRooms.InnerHtml += "<span class='fright width-140' onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomTypeCode + "-" + y.ToString() + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></div></td>";
                                    }
                                }
                            }
                            else if (resultObj.RoomDetails[y].SequenceNo != null && Convert.ToInt32(resultObj.RoomDetails[y].SequenceNo) == (x + 1))
                            {
                                double totalSellRate = 0;
                                double totalPubRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));

                                //if (resultObj.Currency == "USD")
                                //{
                                //    totalSellRate = totalSellRate * (double)rateOfEx[resultObj.Currency];
                                //}

                                tblRooms.InnerHtml += "    <tr>";
                                tblRooms.InnerHtml += " <td>";
                                tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "checked=\'checked\'";
                                    totFare += (decimal)totalSellRate;
                                    discount = roomResult.Discount;
                                }
                                else
                                    tblRooms.InnerHtml += "";

                                if (x > 0)
                                {
                                    //tblRooms.InnerHtml += " disabled='disabled'";
                                }
                                tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";

                                //if (roomResult.RoomTypeName.Split('|').Length > 0)
                                //{
                                //    tblRooms.InnerHtml += "<b>" + (roomResult.RoomTypeName.Contains("|") ? roomResult.RoomTypeName.Split('|')[x] : roomResult.RoomTypeName) + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                //}
                                //else
                                {
                                    //tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + " - " + roomResult.RoomTypeCode + "</b> Incl: " + (roomResult.RoomTypeCode != null && roomResult.RoomTypeCode.Length > 0 ? roomResult.RoomTypeCode : "Room Only") + "</br>";
                                    tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? "</b> Incl: " + roomResult.mealPlanDesc : "") + "</br>";
                                }

                                //if (roomResult.Occupancy != null && roomResult.Occupancy.ContainsKey("ExtraBed") && roomResult.Occupancy["ExtraBed"] == 1)
                                //{
                                //    tblRooms.InnerHtml += " + &nbsp;&nbsp;<b>Extra Bed available</b>";
                                //}

                                if (request.NoOfRooms == 1)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                if (roomResult.PromoMessage != null)
                                {
                                    //tblRooms.InnerHtml += "<div style='position: relative; top: 20px; left: 10px; display: block'>";
                                    tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += roomResult.PromoMessage;
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }

                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin:0px 0px 0px 100px;' id='AmenityDiv-" + index.ToString() + y + "'>";

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'> " + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span></div>";

                                #region Fare Breakup
                                tblRooms.InnerHtml += "<div class='links'>";

                                //tblRooms.InnerHtml += "</div></td></tr><tr>";

                                DateTime d1 = request.StartDate;
                                DateTime d2 = request.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;



                                //tblRooms.InnerHtml += "</tr><tr><td colspan='3'>";
                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 0px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                int days = d2.Subtract(d1).Days;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    int week = i + 1;
                                    tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);
                                        //if (resultObj.BookingSource != HotelBookingSource.RezLive)
                                        //{
                                        //    tblRooms.InnerHtml += "  <li>" + Math.Round(rmInfo.SellingFare, 2).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</li>";
                                        //    totalRoomSellRate += rmInfo.SellingFare;
                                        //}
                                        //else
                                        {

                                            if (roomResult.RoomTypeName.Contains("|"))
                                            {
                                                string[] names = roomResult.RoomTypeName.Split('|');
                                                decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                                tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                                totalRoomSellRate += rate;

                                                //if (resultObj.Currency != "AED")
                                                //{
                                                //    totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                                //}
                                            }
                                            else
                                            {
                                                tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                                totalRoomSellRate += ((decimal)totalSellRate / days);

                                                //if (resultObj.Currency != "AED")
                                                //{
                                                //    totalRoomSellRate = totalRoomSellRate * rateOfEx[resultObj.Currency];
                                                //}
                                            }
                                        }
                                    }


                                    tblRooms.InnerHtml += "</tr>";
                                }
                                //totalRoomSellRate += roomResult.Markup;
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                tblRooms.InnerHtml += "</tr>";
                                //if (roomResult.Discount > 0)
                                //{
                                //    tblRooms.InnerHtml += "<ul><li>Discount</li> ";
                                //    tblRooms.InnerHtml += "<li><b>" + (roomResult.Discount).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";
                                //}
                                //tblRooms.InnerHtml += "<ul><li>Tax</li>";
                                //tblRooms.InnerHtml += " <li><b>" + (roomResult.TotalTax).ToString(ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]) + "</b></li></ul>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                decimal extraGuestRate = 0;


                                extraGuestRate = roomResult.SellExtraGuestCharges;
                                tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                decimal tempTotalP = 0;
                                if (resultObj.BookingSource == HotelBookingSource.EET)
                                {
                                    tempTotalP = (roomResult.SellingFare + roomResult.Markup + roomResult.TotalTax);
                                }

                                tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "</table></div>";
                                #endregion

                                tblRooms.InnerHtml += "</td><td>";
                                if (firstRoom == 0)
                                {
                                    //tblRooms.InnerHtml += " <script type='text/javascript'>";
                                    //tblRooms.InnerHtml += " document.getElementById('rCode" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                    //tblRooms.InnerHtml += " document.getElementById('rCodeDefault" + index + "-" + x + "').value = '" + roomResult.RoomTypeCode + "';";
                                    //if (resultObj.BookingSource != HotelBookingSource.TBOConnect)
                                    //{
                                    //    tblRooms.InnerHtml += " document.getElementById('priceTag" + index + "-" + x + "').value = '" + totalSellRate + "';";
                                    //}
                                    //else
                                    //{
                                    //    tblRooms.InnerHtml += " document.getElementById('priceTag" + index + "-" + x + "').value = '" + totalPubRate + "';";
                                    //}
                                    //tblRooms.InnerHtml += "</script>";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                }
                                firstRoom++;

                                string roomTypeCode = "";
                                //if (roomResult.RoomTypeCode.Length <= 0)
                                {
                                    roomTypeCode = index.ToString() + x.ToString();
                                }
                                //else
                                //{
                                //    roomTypeCode = roomResult.RoomTypeCode;
                                //}

                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomTypeCode + "-" + y.ToString() + "'></div> ";
                                tblRooms.InnerHtml += "<span class='fright width-140' onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomTypeCode + "-" + y.ToString() + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></div></td>";

                            }
                        }
                        # endregion

                        //Added by Brahmam 07.02.2018 ....Agoda Rooms Binding
                        #region AGODA
                        if (resultObj.BookingSource == HotelBookingSource.Agoda)
                        {
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                            {
                                double totalSellRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];
                                totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges + roomResult.TotalTax));

                                tblRooms.InnerHtml += "    <tr>";
                                tblRooms.InnerHtml += " <td>";
                                tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "checked=\'checked\'";
                                    totFare += (decimal)totalSellRate;
                                    discount = roomResult.Discount;
                                }
                                else
                                    tblRooms.InnerHtml += "";

                                tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";
                                tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";
                                if (request.NoOfRooms == 1)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                tblRooms.InnerHtml += "<input  type='hidden' name='NoOfCots" + index.ToString() + x + "-" + y + "' id='NoOfCots" + index.ToString() + x + "-" + y + "' value='" + resultObj.RoomDetails[y].NumberOfCots.ToString() + "' /> ";
                                tblRooms.InnerHtml += "<input  type='hidden' name='SharingBedding" + index.ToString() + x + "-" + y + "' id='SharingBedding" + index.ToString() + x + "-" + y + "' value='" + resultObj.RoomDetails[y].SharingBedding.ToString() + "' /> ";

                                if (roomResult.PromoMessage != null)
                                {
                                    tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += roomResult.PromoMessage;
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }

                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin: 0px 0px 0px 0px;' id='AmenityDiv-" + index.ToString() + y + "'>";

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'> " + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a class='hidden-xs' style=' line-height:20px; ' href='#'>Rate Breakup</a></span></div>";

                                #region Fare Breakup
                                tblRooms.InnerHtml += "<div class='links'>";

                                DateTime d1 = request.StartDate;
                                DateTime d2 = request.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;

                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 0px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                int days = d2.Subtract(d1).Days;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    int week = i + 1;
                                    tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);
                                        if (roomResult.RoomTypeName.Contains("|"))
                                        {
                                            string[] names = roomResult.RoomTypeName.Split('|');
                                            decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                            tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += rate;
                                        }
                                        else
                                        {
                                            tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += ((decimal)totalSellRate / days);
                                        }

                                    }


                                    tblRooms.InnerHtml += "</tr>";
                                }
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                tblRooms.InnerHtml += "</tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                decimal extraGuestRate = 0;


                                extraGuestRate = roomResult.SellExtraGuestCharges;
                                tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                decimal tempTotalP = 0;
                                tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "</table></div>";
                                #endregion

                                tblRooms.InnerHtml += "</td><td>";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                }
                                firstRoom++;
                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName.Replace("'","\\'") + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";

                            }
                        }
                        #endregion

                        //Added by  Somasekhar on 30/08/2018  for Yatra Rooms Binding
                        #region Yatra
                        if (resultObj.BookingSource == HotelBookingSource.Yatra)
                        {

                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                            {
                                double totalSellRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];

                                totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges));// + roomResult.TotalTax));

                                tblRooms.InnerHtml += "    <tr>";
                                tblRooms.InnerHtml += " <td>";
                                tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";

                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "checked=\'checked\'";
                                    totFare += (decimal)totalSellRate;
                                    discount = roomResult.Discount;
                                }
                                else
                                    tblRooms.InnerHtml += "";

                                if (x > 0)
                                {
                                    //tblRooms.InnerHtml += " disabled='disabled'";
                                }
                                tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";

                                {
                                    tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "") + "</br>";
                                    //tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";
                                    //tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? "</b> Incl: " + roomResult.mealPlanDesc : "") + "</br>";
                                }

                                if (request.NoOfRooms == 1)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";

                                tblRooms.InnerHtml += "<input  type='hidden' name='NoOfCots" + index.ToString() + x + "-" + y + "' id='NoOfCots" + index.ToString() + x + "-" + y + "' value='" + resultObj.RoomDetails[y].NumberOfCots.ToString() + "' /> ";
                                tblRooms.InnerHtml += "<input  type='hidden' name='SharingBedding" + index.ToString() + x + "-" + y + "' id='SharingBedding" + index.ToString() + x + "-" + y + "' value='" + resultObj.RoomDetails[y].SharingBedding.ToString() + "' /> ";

                                if (!string.IsNullOrEmpty(roomResult.PromoMessage ))
                                {
                                    //tblRooms.InnerHtml += "<div style='position: relative; top: 20px; left: 10px; display: block'>";
                                    tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += roomResult.PromoMessage;
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }

                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin: 0px 0px 0px 100px;' id='AmenityDiv-" + index.ToString() + y + "'>";

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'> " + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a style=' line-height:20px; ' href='#'>Rate Breakup</a></span></div>";

                                #region Fare Breakup
                                tblRooms.InnerHtml += "<div class='links'>";

                                //tblRooms.InnerHtml += "</div></td></tr><tr>";

                                DateTime d1 = request.StartDate;
                                DateTime d2 = request.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;



                                //tblRooms.InnerHtml += "</tr><tr><td colspan='3'>";
                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 5px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                int days = d2.Subtract(d1).Days;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    int week = i + 1;
                                    tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);
                                        if (roomResult.RoomTypeName.Contains("|"))
                                        {
                                            string[] names = roomResult.RoomTypeName.Split('|');
                                            decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                            tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += rate;
                                        }
                                        else
                                        {
                                            tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += ((decimal)totalSellRate / days);
                                        }

                                    }


                                    tblRooms.InnerHtml += "</tr>";
                                }
                                //totalRoomSellRate += roomResult.Markup;
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                tblRooms.InnerHtml += "</tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                decimal extraGuestRate = 0;


                                extraGuestRate = roomResult.SellExtraGuestCharges;
                                tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                decimal tempTotalP = 0;
                                if (resultObj.BookingSource == HotelBookingSource.LOH)
                                {
                                    tempTotalP = (roomResult.SellingFare + roomResult.Markup + roomResult.TotalTax);
                                }

                                tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "</table></div>";
                                #endregion

                                tblRooms.InnerHtml += "</td><td>";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                }
                                firstRoom++;

                                //string roomTypeCode = "";
                                ////if (roomResult.RoomTypeCode.Length <= 0)
                                //{
                                //    roomTypeCode = index.ToString() + x.ToString();
                                //}
                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";

                            }
                        }
                        #endregion

 //Added by Harish  ....GRN Rooms Binding
                        #region GRN
                        if (resultObj.BookingSource == HotelBookingSource.GRN)
                        {
                            
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                            {
                                double totalSellRate = 0;
                                string groupcode = string.Empty;
                         
                                string rateKey = string.Empty;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];
                                totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup));
                                groupcode = roomResult.RoomTypeCode.Split('|')[5];                     
                                rateKey= roomResult.RoomTypeCode.Split('|')[4];
                                tblRooms.InnerHtml += "    <tr>";
                                tblRooms.InnerHtml += " <td>";
                                tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";
                                string[] type = resultObj.SupplierType.Split('|');
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "checked=\'checked\'";
                                    totFare += (decimal)totalSellRate;
                                    discount = roomResult.Discount;                                                                      
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "";
                                }
                                    
                                if (type[0] == "Bundeled" && x == 0)
                                {
                                    tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";
                                }
                                //else if (type[0] == "Bundeled" && x > 0)
                                //{
                                //    tblRooms.InnerHtml += " disabled='disabled'";
                                //}
                                else if (type[0] == "NonBundeled")
                                {
                                    tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";
                                }
                                else
                                {
                                    tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";
                                }                         
                                tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName +  "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";

                                if (request.NoOfRooms == 1)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rGroupCode" + index.ToString() + x + "-" + y + "' id='rGroupCode" + index.ToString() + x + "-" + y + "' value='" + groupcode + "' /> ";
                              
                                tblRooms.InnerHtml += "<input  type='hidden' name='rRateKey" + index.ToString() + x + "-" + y + "' id='rRateKey" + index.ToString() + x + "-" + y + "' value='" + rateKey + "' /> ";
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";
                                if (roomResult.PromoMessage != null)
                                {
                                    tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += roomResult.PromoMessage;
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }

                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin: 0px 0px 0px 100px;' id='AmenityDiv-" + index.ToString() + y + "'>";

                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'> " + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a style=' line-height:20px; ' href='#'>Rate Breakup</a></span></div>";

                                #region Fare Breakup
                                tblRooms.InnerHtml += "<div class='links'>";

                                DateTime d1 = request.StartDate;
                                DateTime d2 = request.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;

                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 5px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                int days = d2.Subtract(d1).Days;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    int week = i + 1;
                                    tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }

                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);
                                        if (roomResult.RoomTypeName.Contains("|"))
                                        {
                                            string[] names = roomResult.RoomTypeName.Split('|');
                                            decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                            tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += rate;
                                        }
                                        else
                                        {
                                            tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += ((decimal)totalSellRate / days);
                                        }

                                    }


                                    tblRooms.InnerHtml += "</tr>";
                                }
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                tblRooms.InnerHtml += "</tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                decimal extraGuestRate = 0;


                                extraGuestRate = roomResult.SellExtraGuestCharges;
                                tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";

                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";

                                decimal tempTotalP = 0;
                                tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "</table></div>";
                                #endregion

                                tblRooms.InnerHtml += "</td><td>";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                }
                                firstRoom++;
                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName.Replace("'", "\\'") + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";
                                tblRooms.InnerHtml += "<input type='hidden' id='hdnroomType" + index.ToString() + x + "-" + y + "' value='" + resultObj.RoomDetails[y].BedTypeCode + "' />";               
                            }
                        }
                        #endregion
                        //Added by  Somasekhar on 13/12/2018  for OYO Rooms Binding
                        #region OYO source
                        if (resultObj.BookingSource == HotelBookingSource.OYO)
                        {
                            if (resultObj.RoomDetails[y].SequenceNo != null && resultObj.RoomDetails[y].SequenceNo.Contains((x + 1).ToString()))
                            {
                                double totalSellRate = 0;
                                HotelRoomsDetails roomResult = resultObj.RoomDetails[y];
                                totalSellRate = (Convert.ToDouble(roomResult.SellingFare + roomResult.Markup + roomResult.SellExtraGuestCharges + roomResult.ChildCharges));// + roomResult.TotalTax));
                                tblRooms.InnerHtml += "    <tr>";
                                tblRooms.InnerHtml += " <td>";
                                tblRooms.InnerHtml += "<input type='radio' id = 'RoomChoice" + x + "Type" + y + "' name = 'RoomChoice" + x + "' style='border:0' ";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "checked=\'checked\'";
                                    totFare += (decimal)totalSellRate;
                                    discount = roomResult.Discount;
                                }
                                else
                                    tblRooms.InnerHtml += "";
                                if (x > 0)
                                {
                                    //tblRooms.InnerHtml += " disabled='disabled'";
                                }
                                tblRooms.InnerHtml += " onclick=\"javascript:SelectRoomType(\'" + index + "\',\'" + x + "\',\'" + y + "\',\'" + roomResult.RoomTypeCode + "\',\'" + totalSellRate + "\',\'" + false + "\');\"/>";
                                {
                                    tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "") + "</br>";
                                    //tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + "</b> Incl: " + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? roomResult.mealPlanDesc : "Room Only") + "</br>";
                                    //tblRooms.InnerHtml += "<b>" + roomResult.RoomTypeName + (roomResult.mealPlanDesc != null && roomResult.mealPlanDesc.Length > 0 ? "</b> Incl: " + roomResult.mealPlanDesc : "") + "</br>";
                                }
                                if (request.NoOfRooms == 1)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                else
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rPrice" + index.ToString() + x + "-" + y + "' id='rPrice" + index.ToString() + x + "-" + y + "' value='" + totalSellRate + "' /> ";
                                }
                                tblRooms.InnerHtml += "<input  type='hidden' name='rSource" + index + "-" + x + "' id='rSource" + index + "-" + x + "' value='" + resultObj.BookingSource.ToString() + "' /> ";
                                tblRooms.InnerHtml += "<input  type='hidden' name='NoOfCots" + index.ToString() + x + "-" + y + "' id='NoOfCots" + index.ToString() + x + "-" + y + "' value='" + resultObj.RoomDetails[y].NumberOfCots.ToString() + "' /> ";
                                tblRooms.InnerHtml += "<input  type='hidden' name='SharingBedding" + index.ToString() + x + "-" + y + "' id='SharingBedding" + index.ToString() + x + "-" + y + "' value='" + resultObj.RoomDetails[y].SharingBedding.ToString() + "' /> ";
                                if (!string.IsNullOrEmpty(roomResult.PromoMessage))
                                {
                                    //tblRooms.InnerHtml += "<div style='position: relative; top: 20px; left: 10px; display: block'>";
                                    tblRooms.InnerHtml += " <table style='border:solid 0px #ffffff' cellspacing='0' cellpadding='0'><tr><td style='padding-right: 5px;'><img src='images/special-deal-gift.jpg' /></td><td>";
                                    tblRooms.InnerHtml += "<label style='font-size: 11px; color: Red'>";
                                    tblRooms.InnerHtml += roomResult.PromoMessage;
                                    tblRooms.InnerHtml += "</label></td></tr></table>";
                                }
                                tblRooms.InnerHtml += "<div class='room_amenities' style='display: none;text-align:left;margin: 0px 0px 0px 100px;' id='AmenityDiv-" + index.ToString() + y + "'>";
                                tblRooms.InnerHtml += "</div>";
                                tblRooms.InnerHtml += "<div style='text-align: right'><span onclick=\"javascript:ShowAmenities('AmenityDiv-" + index.ToString() + y + "','" + index + "','" + resultObj.HotelCode + "','" + x + "','" + y + "')\"><a href='#'>Amenities</a></span>";
                                tblRooms.InnerHtml += "</div></td>";
                                tblRooms.InnerHtml += "<td align='center'><div class='style1'> " + resultObj.Currency + " " + Math.Round(totalSellRate, decimalValue).ToString("N" + decimalValue) + "</div>";
                                tblRooms.InnerHtml += "<span onclick=\"javascript:ShowFare('fareP" + index.ToString() + y + "','" + index + "','" + x + "','" + y + "')\"><a style=' line-height:20px; ' href='#'>Rate Breakup</a></span></div>";
                                #region Fare Breakup
                                tblRooms.InnerHtml += "<div class='links'>";
                                //tblRooms.InnerHtml += "</div></td></tr><tr>";
                                DateTime d1 = request.StartDate;
                                DateTime d2 = request.EndDate;
                                d2 = d2.AddDays(-1);
                                int result = 0;
                                // Get first full week
                                DateTime firstDayOfFirstFullWeek = d1;
                                if (d1.DayOfWeek != DayOfWeek.Sunday)
                                {
                                    result += 1;
                                    firstDayOfFirstFullWeek = d1.AddDays((7 - (int)d1.DayOfWeek));
                                }
                                // Get last full week
                                DateTime lastDayOfLastFullWeek = d2;
                                if (d2.DayOfWeek != DayOfWeek.Saturday)
                                {
                                    result += 1;
                                    lastDayOfLastFullWeek = d2.AddDays(-(int)d2.DayOfWeek - 1);
                                }
                                System.TimeSpan diffResult = lastDayOfLastFullWeek.Subtract(firstDayOfFirstFullWeek);
                                // Add number of full weeks
                                result += (diffResult.Days + 1) / 7;
                                //tblRooms.InnerHtml += "</tr><tr><td colspan='3'>";
                                tblRooms.InnerHtml += "<table style='width: 550px; background: #fff;border:0 float: left;display:none;z-index:100;margin:0px 0 0 -300px;' class='fare_breakup_popup' id='fareP" + index.ToString() + y + "' >";
                                tblRooms.InnerHtml += "<tr><th style='width: 550px;' colspan='8' class='showMsgHeading' >Rate Breakup <a style='position:absolute; top: 5px; right:10px' class='closex' href='#' onclick=\"javascript:hideFare('fareP" + index.ToString() + y + "')\"> X</a></th></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;font-size:11px;font-weight:bold;'>";
                                tblRooms.InnerHtml += "<td>&nbsp;</td><td>Sun</td><td>Mon</td><td>Tue</td><td>Wed</td><td>Thu</td><td>Fri</td><td>Sat</td></tr>";
                                d1 = resultObj.StartDate;
                                d2 = resultObj.EndDate;
                                decimal totalRoomSellRate = 0;
                                int days = d2.Subtract(d1).Days;
                                for (int i = 0, j = 0; i < result; i++)
                                {
                                    int week = i + 1;
                                    tblRooms.InnerHtml += "<tr style='font-size:11px;font-weight:bold;'>";
                                    tblRooms.InnerHtml += "<td class='week_sno'>Week " + week + "</td>";
                                    int l = 0;
                                    if (d1.DayOfWeek != DayOfWeek.Sunday)
                                    {
                                        int predeser = Convert.ToInt16(d1.DayOfWeek);
                                        for (; l < predeser; l++)
                                        {
                                            tblRooms.InnerHtml += "<td>&nbsp;</td>";
                                        }
                                    }
                                    for (; l < 7 && d1 < d2; l++)
                                    {
                                        RoomRates rmInfo = new RoomRates();
                                        j++;
                                        if (roomResult.Rates.Length > j)
                                        {
                                            rmInfo = roomResult.Rates[j];
                                        }
                                        else
                                        {
                                            rmInfo = roomResult.Rates[roomResult.Rates.Length - 1];
                                        }
                                        d1 = d1.AddDays(1);
                                        if (roomResult.RoomTypeName.Contains("|"))
                                        {
                                            string[] names = roomResult.RoomTypeName.Split('|');
                                            decimal rate = (rmInfo.SellingFare / names.Length) / days;
                                            tblRooms.InnerHtml += "<td>" + Math.Round(rate, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += rate;
                                        }
                                        else
                                        {
                                            tblRooms.InnerHtml += "<td>" + Math.Round(totalSellRate / days, decimalValue).ToString("N" + decimalValue) + "</td>";
                                            totalRoomSellRate += ((decimal)totalSellRate / days);
                                        }
                                    }
                                    tblRooms.InnerHtml += "</tr>";
                                }
                                //totalRoomSellRate += roomResult.Markup;
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total : ";
                                tblRooms.InnerHtml += "<b>" + Math.Round(totalRoomSellRate, decimalValue).ToString("N" + decimalValue) + "</b></td>";
                                tblRooms.InnerHtml += "</tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Extra Guest Charge : ";
                                decimal extraGuestRate = 0;
                                extraGuestRate = roomResult.SellExtraGuestCharges;
                                tblRooms.InnerHtml += "<b>" + (extraGuestRate).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "<tr style='width: 550px;'><td colspan='8'>Total Price : ";
                                decimal tempTotalP = 0;
                                if (resultObj.BookingSource == HotelBookingSource.LOH)
                                {
                                    tempTotalP = (roomResult.SellingFare + roomResult.Markup + roomResult.TotalTax);
                                }
                                tblRooms.InnerHtml += "<b>" + Math.Round(tempTotalP, decimalValue).ToString("N" + decimalValue) + "</b></td></tr>";
                                tblRooms.InnerHtml += "</table></div>";
                                #endregion
                                tblRooms.InnerHtml += "</td><td>";
                                if (firstRoom == 0)
                                {
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCode" + index + "-" + x + "' id='rCode" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "'/> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='rCodeDefault" + index + "-" + x + "' id='rCodeDefault" + index + "-" + x + "' value = '" + roomResult.RoomTypeCode + "' /> ";
                                    tblRooms.InnerHtml += "<input  type='hidden' name='priceTag" + index + "-" + x + "' id='priceTag" + index + "-" + x + "' value='" + totalSellRate + "' />";
                                }
                                firstRoom++;
                               
                                tblRooms.InnerHtml += "<div class='room_details_popup cancel_details_popup' style='background-color:#fff;display:none;margin: 0px 0px 0px -300px;' id='room" + roomResult.RoomTypeCode + "'></div> ";
                                tblRooms.InnerHtml += "<span onclick=\"GetRoomCancelInfo('" + resultObj.BookingSource + "','" + resultObj.HotelCode + "','" + roomResult.RoomTypeCode + "','" + resultObj.Currency + "','" + resultObj.CityCode + "','" + resultObj.HotelName + "','" + index + "')\"><a href='#' style=' line-height:20px; ' >Cancellation Details</a></span></a></div></td>";
                            }
                        }
                        #endregion
                    }
                }
                tblRooms.InnerHtml += "<input  type='hidden' id='hdnRoomSeries' value='" + roomSeries + "' />";
                tblRooms.InnerHtml += "<input  type='hidden' id='hdnSameRooms' value='" + sameRooms + "' />";
                tblRooms.InnerHtml += "<input  type='hidden' id='hdnDiffRooms' value='" + rooms + "' />";
                tblRooms.InnerHtml += "<input  type='hidden' id='hdnDiffRoomsSel'  />";
                if (resultObj.RoomDetails.Length > 0 && resultObj.RoomDetails[0].DiscountType == "P" )
                {
                    discount = Math.Ceiling(totFare) * discount;
                }
                //Added by brahmam Checking TBO Rooms Combation(open or Fixed)
                tblRooms.InnerHtml += "<input type='hidden' id='hdnroomComb' value='" + resultObj.SupplierType + "' />"; 
 //Added by Harish For GRN Rooms (number of room types in a hotel)
                string count = Convert.ToString(resultObj.RoomDetails.Length);
                tblRooms.InnerHtml += "<input type='hidden' id='hdnroomTypesCount' value='" + count + "' />";
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.High, 1, "Failed to get room details for DOTW: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        
    }
}
