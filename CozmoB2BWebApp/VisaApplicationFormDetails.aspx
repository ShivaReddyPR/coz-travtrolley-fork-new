﻿<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="VisaApplicationFormDetails" ValidateRequest="false" Codebehind="VisaApplicationFormDetails.aspx.cs" %>
    
<%@ Import Namespace="Visa" %>
<%@ Import Namespace="System.Collections" %>
<%@ Import Namespace="System.Collections.Generic" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme %>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="<%=Request.Url.Scheme %>://www.w3.org/1999/xhtml">
<head runat="server">
<title >Cozmo Travels</title>
</head>
<body style="overflow:visible;float:none" dir="ltr">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
  
    <script type="text/javascript">
   
    function PrintContent()
    {
      document.getElementById('printIcon').style.display='none';
      window.print();
      document.getElementById('printIcon').style.display='block';
    }
    
    </script>


  <div id="printIcon" style="width:835px;margin-top:10px;">
  <span style="float:right">
        <a id="btnPrint12" class="mini_menu_link" href="javascript:PrintContent()">Print &nbsp;
            <img title="Print" src="images/print_icon.jpg" style="padding-left: 2;" alt="Print" />
        </a>
        </span>
    </div>
   
    <div id="divtoprint" class="coz_content_container" style="margin-left:61px">
        <div class="row_div" style="width: 777px">
            <div class="search_box_main border_no width_100">
           
                <% int i = 1;

                   foreach (VisaPassenger passenger in application.PassengerList)
                   { %>
                <!-- Search Section start here -->
                <div class="airfare_calender width_100">
                    <div class="cal_head" style="line-height:0px;padding:12px 5px; width:99%;">
                        <b>Applicant
                            <%=i++ %>
                            Detail</b><div style="float: right">
                            </div>
                    </div>
                    <!--Div Tag For Personel Detail -->
                    <div class="login">
                        <div class="parent width_98 margin_left_10 border_no">
                            <p>
                                <b class="heading_txt">Personal Details</b></p>
                            <p>
                                <label>
                                    Name</label>
                                <span><ins>:
                                    <%=passenger.Title%>
                                </ins></span><span><ins>
                                    <%=passenger.Firstname%>
                                </ins></span><span><ins></ins></span>
                            </p>
                            <p>
                                <dfn>
                                    <label>
                                        Nationality
                                    </label>
                                    <span><ins>:
                                        <%if (passenger.Nationality != null)
                                          {%>
                                        <%=nationalityList[passenger.Nationality]%>
                                        <%}
                                          else
                                          { %>
                                        <%=passenger.NationalityOther%>
                                        <%} %>
                                    </ins></span></dfn><dfn>
                                        <label>
                                            Pre. nationality if any</label>
                                        <span><ins>:
                                            <%=passenger.PreNationality%>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label>
                                        Gender</label>
                                    <span><ins>:
                                        <%=passenger.Sex%>
                                    </ins></span></dfn><dfn>
                                        <label>
                                            Date of birth
                                        </label>
                                        <span><ins>:
                                            <%=String.Format("{0:dd-MMM-yyyy}",passenger.Dob)%>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label>
                                        Place of birth (Country)</label>
                                    <span><ins>:
                                        <%= countryList[passenger.BirthCountry]%>
                                    </ins></span></dfn><dfn>
                                        <label>
                                            Place of birth (City)</label>
                                        <span><ins>:
                                            <%if (passenger.BirthPlace != null)
                                              { %>
                                            <%= cityList[passenger.BirthPlace]%>
                                            <%}
                                              else
                                              { %>
                                            <%=passenger.BirthPlaceOther%>
                                            <%} %>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label>
                                        Marital Status</label>
                                    <span><ins>:
                                        <%=passenger.AdditionalInfo.MaritalStatus%>
                                    </ins></span></dfn><dfn>
                                        <label>
                                            Occupation</label>
                                        <span><ins>:
                                            <%=passenger.AdditionalInfo.Occupation%>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label>
                                        Religion</label>
                                    <span><ins>:
                                        <%=passenger.AdditionalInfo.Religion%>
                                    </ins></span></dfn><dfn>
                                        <label>
                                            Edu. qualifications</label>
                                        <span><ins>:
                                            <%=passenger.AdditionalInfo.Qualification%>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label>
                                        Main language</label>
                                    <span><ins>:
                                        <%=passenger.AdditionalInfo.SpokenLanguage%>
                                    </ins></span></dfn>
                            </p>
                            <p>
                                <label>
                                    Fathers Name</label>
                                <span><ins>: Mr.
                                    <%=passenger.FatherName%>
                                </ins></span>
                            </p>
                            <p>
                                <label>
                                    Mothers Name</label>
                                <span><ins>: Mrs.
                                    <%=passenger.MotherName%>
                                </ins></span>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="pddetail">
                    <!--Space area-->
                </div>
                <!--  Div Tag For Passport Detail-->
                <div class="airfare_calender width_100">
                    <div class="login">
                        <div class="parent width_98 margin_left_10 border_no">
                            <p>
                                <b class="heading_txt">Passport Details</b></p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Type</label>
                                    <span><ins>:
                                        <%=passenger.PassportType%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Passport number</label>
                                        <span><ins>:<%=passenger.PassportNo%></ins></span></dfn></p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Place of Issue (Country)</label>
                                    <span><ins>:
                                        <%=countryList[passenger.PassportIssueCountry]%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Place of Issue (City)</label>
                                        <span><ins>:
                                            <%if (passenger.PassprotIssueCity != null)
                                              { %>
                                            <%=cityList[passenger.PassprotIssueCity]%>
                                            <%} %>
                                            <%else
                                                { %>
                                            <%=passenger.PassprotIssueCityOther%>
                                            <%} %>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Date of issue
                                    </label>
                                    <span><ins>:
                                        <% =String.Format("{0:dd-MMM-yyyy}", passenger.PassportIssueDate) %>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Expiry date</label>
                                        <span><ins>:
                                            <%=String.Format("{0:dd-MMM-yyyy}",passenger.PassportExpiryDate)%>
                                        </ins></span></dfn>
                            </p>
                        </div>
                    </div>
                </div>
                
                <div class="pddetail">
                    <!--Space area-->
                </div>
                 <!--  Div Tag For Document  Uploaded-->
                <div class="airfare_calender width_100">
                    <div class="login">
                   
                        <div class="parent width_98 margin_left_10 border_no">
                      
                       
                            <p>
                                <b class="heading_txt">Uploaded Document</b></p>
                                <p>
                                 <label style="width:170px; color:#0b91c1"><b>Document Type</b></label>
                                 <label style="width:416px;color:#0b91c1"><b>Document Description</b></label>
                                <label style="color:#0b91c1"><b>Document</b></label>
                                </p>
                                <%
                                List<VisaUploadedDocument> DocumentList=VisaUploadedDocument.GetUploadedDocumentList(passenger.PaxId);
                                foreach (VisaUploadedDocument document in DocumentList)
                                {   
                                %>
                              <p>
                           <%foreach (VisaDocument visaDocument in visaDocumentList)
                             {
                                 if (visaDocument.DocumentId == document.DocumentId)
                                 {%>
                                 
                                 <label style="width:170px"><%=visaDocument.DocumentType%></label>
                                 <label style="width:416px"><%=visaDocument.DocumentDescription%></label>
                                 <span class="hand underline" onclick="javascript:ShowDocument('<% =passenger.PaxId %>','<%=document.DocumentPath %>')"><img src="<%=document.DocumentPath %>" alt="" height="23px" width="45px" /></span>
                                 
                               <% } %>
                             <%} %>
                               </p>
                            <%} %>
                              <!-- pop up div--->
                            <div class="eligibility_criteria visa_popup width-400" id="document<% =passenger.PaxId %>" style="display:none;" >
                           <div class="visa_detail_container">
                              <div class="editable_section_style parent airfare_calender" style="width: 453px; padding: 0 !important; margin-top: 0 !important;">
                               <h1 style="margin-top: 0px; margin-right: 5px">
                               Uploaded Document<a style="float: right" href="javascript:HidePopUP('document<% =passenger.PaxId %>')">X</a>
                                 </h1>
                           <div class="pop_scroll " style="width:448px;height:200px">
                     <img id="DocumentImg<%=passenger.PaxId  %>" src="" alt="" />
                          </div>
                          </div>
                       </div>
                      </div>
                        </div>
                    </div>
                </div>
                <div class="pddetail">
                    <!--Space area-->
                </div>
                <% if (i == 2)
                   {%>
                <!-- Div Tag For Address -->
                <div class="airfare_calender width_100">
                    <div class="login">
                        <div class="parent width_98 margin_left_10 border_no">
                            <p>
                                <b class="heading_txt">Address Details</b></p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Line -1</label>
                                    <span><ins>:
                                        <%=passenger.Address.Line1%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Line - 2</label>
                                        <span><ins>:
                                            <%=passenger.Address.Line2%>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Country</label>
                                    <span><ins>:
                                        <%=countryList[passenger.Address.CountryCode]%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            State</label>
                                        <span><ins>:
                                            <%=passenger.Address.StateOther%>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        City</label>
                                    <span><ins>:
                                        <%=passenger.Address.CityName%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Mobile</label>
                                        <span><ins>:
                                            <%=passenger.Address.Mobile%>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Land Line</label>
                                    <span><ins>:
                                        <%=passenger.Address.Phone%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Fax No</label>
                                        <span><ins>:
                                            <%=passenger.Address.Fax%>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Email ID</label>
                                    <span><ins>:
                                        <%=passenger.Address.Email%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            &nbsp;</label>
                                        <span></span>&nbsp;</dfn></p>
                        </div>
                    </div>
                </div>
                <div class="pddetail">
                    <!--Space area-->
                </div>
                <%if (!String.IsNullOrEmpty(passenger.AdditionalInfo.GuranterNationality))
                  { %>
                <!-- Div Tag For Guarantor  Details-->
                <div class="airfare_calender width_100">
                    <div class="login">
                        <div class="parent width_98 margin_left_10 border_no">
                            <p>
                                <b class="heading_txt">Guarantor Details</b></p>
                            <p>
                                <label class="wd">
                                    Name</label>
                                <span>
                                    <% =passenger.AdditionalInfo.GuranterName %></span>
                            </p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Nationality</label>
                                    <span><ins>:
                                        <%if (passenger.AdditionalInfo.GuranterName != null)
                                          { %>
                                        <%=nationalityList[passenger.AdditionalInfo.GuranterNationality]%>
                                        <%}
                                          else
                                          { %>
                                        <%=passenger.AdditionalInfo.GuranterNationalityOther%>
                                        <%} %>
                                    </ins></span>&nbsp;</dfn><dfn>
                                        <label class="wd">
                                            Pre. nationality if any</label>
                                        <span><ins>:
                                            <%=passenger.AdditionalInfo.GuranterPreNationality%>
                                        </ins></span>&nbsp;</dfn></p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Type</label>
                                    <span><ins>:
                                        <%=passenger.AdditionalInfo.GuranterPassportType%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Passport number</label>
                                        <span><ins>:
                                            <%=passenger.AdditionalInfo.GuranterPassport%>
                                        </ins></span>&nbsp;</dfn></p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Place of Issue (Country)</label>
                                    <span><ins>:
                                        <%=countryList[passenger.AdditionalInfo.GuranterPassportCountry]%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Place of Issue (City)</label>
                                        <span><ins>:
                                            <%if (passenger.AdditionalInfo.GuranterPassportCity != null)
                                              {%>
                                            <%=passenger.AdditionalInfo.GuranterPassportCity%>
                                            <%}
                                              else
                                              { %>
                                            <%=passenger.AdditionalInfo.GuranterPassportCity%>
                                            <%} %>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Date of issue
                                    </label>
                                    <span><ins>:
                                        <%=String.Format("{0:dd-MMM-yyyy}",passenger.AdditionalInfo.GuranterPassportDate)%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Expiry date</label>
                                        <span><ins>:
                                            <%=String.Format("{0:dd-MMM-yyyy}",passenger.AdditionalInfo.GuranterPassportExpiryDate)%>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Relation to Visitor</label>
                                    <span><ins>:
                                        <%=passenger.AdditionalInfo.RealationShipWithGuranter%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Visa Number</label>
                                        <span><ins>:
                                            <%=passenger.AdditionalInfo.GuranterVisaNo%>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Visa Expiry Date</label>
                                    <span><ins>:
                                        <%=String.Format("{0:dd-MMM-yyyy}",passenger.AdditionalInfo.GuranterVisaExpiryDate)%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Mobile</label>
                                        <span><ins>:
                                            <%=passenger.AdditionalInfo.GuranterMobile%>
                                        </ins></span>&nbsp;</dfn></p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Office
                                    </label>
                                    <span><ins>:
                                        <%=passenger.AdditionalInfo.GuranterOfficeNo%>
                                    </ins></span>&nbsp;</dfn> <dfn>
                                        <label class="wd">
                                            Email ID</label>
                                        <span><ins>:
                                            <%=passenger.AdditionalInfo.GuranterEmailId%>
                                        </ins></span></dfn>
                            </p>
                        </div>
                    </div>
                </div>
                <div class="pddetail">
                    <!--Space area-->
                </div>
                <%} %>
                <!-- End Of Guarantor Block-->
                <!-- Div Tag For Traveling Details-->
                <div class="airfare_calender width_100">
                    <div class="login">
                        <div class="parent width_98 margin_left_10 border_no">
                            <p>
                                <b class="heading_txt">Travel Details</b></p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Airline</label>
                                    <span><ins>:
                                        <%=passenger.AdditionalInfo.Airline%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Flight</label>
                                        <span><ins>:
                                            <%=passenger.AdditionalInfo.FlightNumber%>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Date of Travel/Date of Entry
                                    </label>
                                    <span><ins>:
                                        <%if (passenger.TravelDate != new DateTime(1 / 1 / 1).Date)
                                          { %>
                                        <%=String.Format("{0:dd-MMM-yyyy}", passenger.TravelDate)%>
                                        <%} %>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Latest Date you need your Visa to be processed</label>
                                        <span><ins>:
                                            <%if (passenger.VisaProcessingLastDate != new DateTime(1 / 1 / 1).Date)
                                              { %>
                                            <%=String.Format("{0:dd-MMM-yyyy}", passenger.VisaProcessingLastDate)%>
                                            <%} %>
                                        </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Date of Departure
                                    </label>
                                    <span><ins>:
                                        <%if (passenger.AdditionalInfo.Depart != new DateTime(1 / 1 / 1).Date)
                                          { %>
                                        <%=String.Format("{0:dd-MMM-yyyy}", passenger.AdditionalInfo.Depart)%>
                                        <%} %>
                                    </ins></span></dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label class="input_wd">
                                        &nbsp;</label>
                                </dfn><dfn></dfn>
                            </p>
                            <!--calender-->
                            <div style="display: none">
                                Please put your calender code here
                            </div>
                            <!--//calender-->
                        </div>
                    </div>
                </div>
                <div class="pddetail">
                    <!--Space area-->
                </div>
                <!-- Div Tag For Employer Details-->
                <div class="airfare_calender width_100" style="display: block">
                    <div class="cal_head">
                        <span><b>If applying as Principal Applicant, attach visiting card/complete the following</b></span>
                    </div>
                    <div class="login">
                        <div class="parent width_98 margin_left_10 border_no">
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Name of Business/Employer
                                    </label>
                                    <span><ins>:
                                        <%=passenger.EmployerInfo.EmployerName%>
                                    </ins></span></dfn><dfn>
                                        <label class="wd">
                                            Address of Business/Employer</label><span> <ins>:
                                                <%=passenger.EmployerInfo.Address%></ins></span> </dfn>
                            </p>
                            <p>
                                <dfn>
                                    <label class="wd">
                                        Designation</label><span> <ins>:
                                            <%=passenger.EmployerInfo.Designation%></ins></span> </dfn><dfn>
                                                <label class="wd">
                                                    Attach Visiting Card</label><span> </span></dfn>
                            </p>
                            <!--calender-->
                            <div style="display: none">
                                Please put your calender code here
                            </div>
                            <!--//calender-->
                        </div>
                    </div>
                </div>
                <div class="pddetail">
                    <!--Space area-->
                </div>
                <%} %>
                <%} %>
                <div class="pddetail">
                    <div style="float: right" id="lastDiv">
                    </div>
                </div>
                <!-- //Search Section start here -->
                <!-- Hotel Section Starts Here -->
            </div>
            <!-- Div For Fare Detail  -->
        </div>
    </div>
    <iframe id="ifrmPrint" src="#" style="width: 0px; height: 0px; position: absolute;
        z-index: -1000;"></iframe>
</body>
</html>
