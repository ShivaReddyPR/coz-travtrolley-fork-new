﻿using System;
using System.Collections;
using CT.BookingEngine;

public partial class PrintTicket :CT.Core.ParentPage// System.Web.UI.Page
{
    string sessionId;
    int resultId;
    SearchResult resultObj = new SearchResult();
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["sessionId"] != null)
        {
            sessionId = Session["sessionId"].ToString();
            resultId = Convert.ToInt32(Request["id"]);
            resultObj = Basket.FlightBookingSession[sessionId].Result[resultId - 1];
            SearchRequest request = Session["FlightRequest"] as SearchRequest;
            for (int i = 0; i < resultObj.Flights.Length; i++)
            {
                lblBaseFare.Text = resultObj.BaseFare.ToString("0.000");
                lblTaxes.Text = resultObj.Tax.ToString("0.000");
                lblTotalFare.Text = resultObj.TotalFare.ToString("0.000");

                if (i == 0)
                {
                    DictionaryEntry logoInfo = BookingUtility.GetLogo(resultObj.Flights, Request.ServerVariables["APPL_PHYSICAL_PATH"].ToString());

                    lblOnwardAirlines.Text = logoInfo.Key.ToString();
                    for (int j = 0; j < resultObj.Flights[i].Length; j++)
                    {
                        lblOnwardFlightNumber.Text = resultObj.Flights[i][j].FlightNumber;
                        switch (j)
                        {
                            case 0://non stop
                                lblDepartureCity.Text = request.Segments[0].Origin;
                                lblArrivalCity.Text = request.Segments[0].Destination;
                                lblOnwardPNR.Text = new FlightItinerary(resultObj.Flights[i][j].FlightId).PNR;
                                lblOnwardTicketNo.Text = new FlightItinerary(resultObj.Flights[i][j].FlightId).PNR;
                                lblOnwardStatus.Text = resultObj.Flights[i][j].Status;
                                lblOnwardPaxName.Text = new FlightItinerary(resultObj.Flights[i][j].FlightId).Passenger[0].FirstName + new FlightItinerary(resultObj.Flights[i][j].FlightId).Passenger[0].LastName;
                                lblOnwardArrivalCity.Text = resultObj.Flights[i][j].Destination.CityName;
                                lblOnwardDepartureCity.Text = resultObj.Flights[i][j].Origin.CityName;
                                lblOnwardDepartureTime.Text = resultObj.Flights[i][j].DepartureTime.ToString("dd MMM yyyy, hh:mm tt");
                                lblOnwardArrivalTime.Text = resultObj.Flights[i][j].ArrivalTime.ToString("dd MMM yyyy, hh:mm tt");
                                TimeSpan duration = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);
                                lblOnwardStops.Text = resultObj.Flights[i][j].Stops.ToString();
                                lblOnwardDuration.Text = duration.Hours + "hrs " + duration.Minutes + "mins";
                                lblOnwardDepartureAirport.Text = resultObj.Flights[i][j].Origin.AirportName;
                                lblOnwardArrivalAirport.Text = resultObj.Flights[i][j].Destination.AirportName;
                                lblOnwardArrivalTerminal.Text = resultObj.Flights[i][j].DepTerminal;
                                lblOnwardDepartureTerminal.Text = resultObj.Flights[i][j].ArrTerminal;
                                break;

                        }
                    }
                }
                else
                {
                    DictionaryEntry logoInfo = BookingUtility.GetLogo(resultObj.Flights, Request.ServerVariables["APPL_PHYSICAL_PATH"].ToString());
                    for (int j = 0; j < resultObj.Flights[i].Length; j++)
                    {
                        lblReturnAirline.Text = logoInfo.Key.ToString();
                        lblReturnFlightNumber.Text = resultObj.Flights[i][j].FlightNumber;

                        switch (j)
                        {
                            case 0://Non Stop
                                lblDepartureCity1.Text = request.Segments[0].Destination;
                                lblArrivalCity1.Text = request.Segments[0].Origin;
                                lblReturnTicketNo.Text = new FlightItinerary(resultObj.Flights[i][j].FlightId).PNR;
                                lblReturnPNR.Text = new FlightItinerary(resultObj.Flights[i][j].FlightId).PNR;
                                lblReturnStatus.Text = resultObj.Flights[i][j].Status;
                                lblReturnPaxName.Text = new FlightItinerary(resultObj.Flights[i][j].FlightId).Passenger[0].FirstName + new FlightItinerary(resultObj.Flights[i][j].FlightId).Passenger[0].LastName;
                                lblReturnDepartureCity.Text += resultObj.Flights[i][j].Origin.CityName;
                                lblReturnArrivalCity.Text += resultObj.Flights[i][j].Destination.CityName;
                                lblReturnDepartureTime.Text += resultObj.Flights[i][j].DepartureTime.ToString("dd MMM yyyy, hh:mm tt");
                                lblReturnArrivalTime.Text += resultObj.Flights[i][j].ArrivalTime.ToString("dd MMM yyyy, hh:mm tt");
                                lblReturnStops.Text = resultObj.Flights[i][j].Stops.ToString();
                                TimeSpan duration = resultObj.Flights[i][j].ArrivalTime.Subtract(resultObj.Flights[i][j].DepartureTime);
                                lblReturnDuration.Text = duration.Hours + "hrs " + duration.Minutes + "mins";
                                lblReturnArrivalAirport.Text = resultObj.Flights[i][j].Destination.AirportName;
                                lblReturnDepartureAirport.Text = resultObj.Flights[i][j].Origin.AirportName;
                                lblReturnArrivalTerminal.Text = resultObj.Flights[i][j].ArrTerminal;
                                lblReturnDepartureTerminal.Text = resultObj.Flights[i][j].DepTerminal;
                                break;

                        }
                    }
                }
            }
        }
        else
        {
            Response.Redirect("HotelSearch.aspx");
        }
    }
}
