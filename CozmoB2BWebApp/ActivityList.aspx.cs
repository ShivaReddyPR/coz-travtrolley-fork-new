﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.ActivityDeals;
using CT.Core;

public partial class ActivityListGUI : CT.Core.ParentPage
{
    protected int counter = 0;
    protected string activeCheck = string.Empty;
    protected string inactiveCheck = string.Empty;
    protected int activeCount = 0;
    protected string errorString = string.Empty;
    protected string whereString = string.Empty;
    protected string activeValue = string.Empty;
    protected string inactiveValue = string.Empty;

    protected AgentMaster agency = new AgentMaster();
    protected UserMaster member = new UserMaster();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
			this.Master.PageRole = true;
            if (!IsPostBack)
            {
                BindAgent();
                BindGrid(Convert.ToInt16(ddlAgent.SelectedItem.Value));
            }
        }
        catch 
        {
        }
    }
    protected void BindGrid(int agencyId)
    {
        try
        {
            DataTable ds = CT.ActivityDeals.Activity.GetList("N", agencyId);
            DataList1.DataSource = ds;
            DataList1.DataBind();
        }
        catch { throw; }
    }
    protected void DataList1_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Edit") //Editing
            {
                HiddenField hdfActivityId = (HiddenField)DataList1.Items[e.Item.ItemIndex].FindControl("IThdfActivityId");
                string activityId = hdfActivityId.Value;
                string url = string.Format("ActivityMaster.aspx?activity={0}", activityId);
                Response.Redirect(url, false);
            }
            else if (e.CommandName == "StatusUpdate") //Status Changing
            {
                HiddenField hdfActivityId = (HiddenField)DataList1.Items[e.Item.ItemIndex].FindControl("IThdfActivityId");
                int activityId =Convert.ToInt32(hdfActivityId.Value);
                HiddenField hdfStatus = (HiddenField)DataList1.Items[e.Item.ItemIndex].FindControl("IThdfStatus");
                string activityStatus = string.Empty;
                if (hdfStatus.Value == "A") //A means Active and D means DeActive
                {
                    activityStatus = "D";
                }
                else
                {
                    activityStatus = "A";
                }
                Activity.ActivityStatusUpdate(activityId,activityStatus);
                BindGrid(Convert.ToInt16(ddlAgent.SelectedItem.Value));
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ActivityList" + ex.Message, "0");
        }
    }

    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agency--", "0"));
            ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ActivityList" + ex.Message, "0");
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid(Convert.ToInt16(ddlAgent.SelectedItem.Value));
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ActivityList" + ex.Message, "0");
        }
    }
}
