﻿using CT.AccountingEngine;
using CT.BookingEngine;
using CT.Core;
using CT.Corporate;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.DataAccessLayer;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;

using System.Transactions;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
//using Newtonsoft.Json;

public partial class OffLineEntry : CT.Core.ParentPage
{
    protected FlightItinerary itinerary = new FlightItinerary();
    protected List<FlightPassenger> PassengerList = new List<FlightPassenger>();
    protected List<FlightInfo> FlightInfoList = new List<FlightInfo>();
    MetaSearchEngine mse = new MetaSearchEngine();
    public static bool isAdded = false;
    public static bool isEdited = false;
    public static bool isEditFromPopUp = false;
    public static bool isItinerarysaved = false;
    public static bool isFromClear = false;
    public string errorMessage = string.Empty;
    public int agentDecimalPoints;
    public static int userID = 0;
    public static int AgentID = 0;
    public static string locationName = string.Empty;
    protected LocationMaster agentLocation = new LocationMaster();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null)
            {
                GetAgencyDecimal();
                if (!IsPostBack)
                {
                    isEditable.Value = "1";
                    AgentID = Settings.LoginInfo.AgentId;
                    hdnAgentId.Value = Convert.ToString(AgentID);
                    //InitializeControls();
                    BindAirlines();
                    bindSectorsDropdown();
                    BindSuppliers(Convert.ToInt32(AgentID));
                    userID = (int)Settings.LoginInfo.UserID;
                    HideSearch();
                    agentLocation = new LocationMaster(Settings.LoginInfo.LocationID);//To display the GST Fields if login Agent Location Country is INdia
                    
                }


                //if (hdnSaveItinerary.Value == "SaveItinerary")
                //{
                //    SaveItinerary();
                //    hdnSaveItinerary.Value = "";
                //}
                //if (hdnSaveItinerary.Value == "AddPax")
                //{
                //    btnAdd_Click(sender, e);
                //    hdnSaveItinerary.Value = "";
                //}
                //if (Session["itinerary"] != null)
                //{
                //    itinerary = (FlightItinerary)Session["itinerary"];
                //}
                //if (itinerary != null && itinerary.Passenger != null && itinerary.Passenger.Length > 0)
                //{
                //    btnSave.Visible = true;
                //}
                //else
                //{
                //    btnSave.Visible = false;
                //}
                //if (!string.IsNullOrEmpty(hdnSelectedIndex.Value) && isAdded == true && hdnIsFromEdit.Value == "true")
                //{
                //    EditPaxDetails(Convert.ToInt32(hdnSelectedIndex.Value));
                //}
                //if (!string.IsNullOrEmpty(hdnRemovePax.Value))
                //{
                //    RemovePaxFromItinerary(Convert.ToInt32(hdnRemovePax.Value));
                //}
                //if (itinerary != null && itinerary.Passenger != null && itinerary.Passenger.Count() > 0)
                //{
                //    btnSave.Visible = true;
                //}
                //else
                //{
                //    btnSave.Visible = false;
                //    btnUpdate.Visible = false;
                //    //btnAdd.Visible = true;
                //}

            }
            else
            {
                Response.Redirect("AbandonSession.aspx", false);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in BindSources()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
            throw ex;
        }
    }
    //protected void Page_PreRender(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        //DisplayAgentBalance();
    //        //if (Session["taxBreakUp"] != null)
    //        //{
    //        //    int counter = 0;
    //        //    string taxDetails = "";
    //        //    List<KeyValuePair<string, decimal>> taxBreakUp = Session["TaxBreakup"] as List<KeyValuePair<string, decimal>>;
    //        //    foreach (KeyValuePair<string, decimal> pair in taxBreakUp)
    //        //    {
    //        //        if (string.IsNullOrEmpty(taxDetails))
    //        //            taxDetails = counter + "-" + pair.Key + "," + pair.Value;
    //        //        else
    //        //            taxDetails += "|" + counter + "-" + pair.Key + "," + pair.Value;
    //        //        counter++;
    //        //    }

    //        //    if (taxBreakUp.Count > 1)
    //        //        hndTaxCounter.Value = counter.ToString();
    //        //    hndTaxCodeValue.Value = taxDetails;

    //        //    //AddNewRow(pair.Key,pair.Value);
    //        //}
    //        //Utility.StartupScript(this.Page, "ReLoadingTaxControlsFromEdit();", "ReLoadingTaxControlsFromEdit");
    //        //Utility.StartupScript(this.Page, "ReloadSelectedTab();", "ReloadSelectedTab");   
    //        //Utility.StartupScript(this.Page, "ShowSectorTable('" + hndsegmentsCount.Value + "')", "ShowSectorTable");
    //        //Utility.StartupScript(this.Page, "disableSectors();", "disableSectors");
    //        //Utility.StartupScript(this.Page, "ReinitiateValues();", "ReinitiateValues");
    //        //Utility.StartupScript(this.Page, "CalculateVAT();", "CalculateVAT");

    //        //itinerary = (FlightItinerary)Session["itinerary"];
    //        //if (itinerary != null && itinerary.Passenger != null)
    //        //{
    //        //    Utility.StartupScript(this.Page, "DisableSectorsList();", "DisableSectorsList");
    //        //}
    //        //if (isItinerarysaved)
    //        //{
    //        //    isItinerarysaved = false;
    //        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Sucessfully Saved');window.location='OffLineEntry.aspx';", true);
    //        //}
    //        //if (!string.IsNullOrEmpty(errorMessage))
    //        //{
    //        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Failed To Save :" + errorMessage + "');window.location='OffLineEntry.aspx';", true);
    //        //    errorMessage = string.Empty;
    //        //}
    //        //if(isFromClear)
    //        //{
    //        //    Utility.StartupScript(this.Page, "SetActiveTab();", "SetActiveTab");
    //        //    isFromClear = false;
    //        //}


    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in Page_PreRender()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);

    //    }
    //}
    private void BindAgents()
    {
        try
        {
            int agentId = Convert.ToInt32(Settings.LoginInfo.AgentId);
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);

            //DataRow[] dr = dtAgents.Select("AGENT_ID NOT ='" + agentId + "'");
            //DataTable DTagents = new DataTable();
            //DTagents.Columns.Add("AGENT_ID", typeof(int));
            //DTagents.Columns.Add("AGENT_CODE", typeof(string));
            //DTagents.Columns.Add("AGENT_NAME", typeof(string));
            //DTagents.Columns.Add("agent_type", typeof(int));
            //foreach (DataRow row in dr)
            //{
            //    DataRow Row = DTagents.NewRow();
            //    Row["AGENT_ID"] = Convert.ToInt32(row.ItemArray[0].ToString());
            //    Row["AGENT_CODE"] = row.ItemArray[1].ToString();
            //    Row["AGENT_NAME"] = row.ItemArray[2].ToString();
            //    Row["agent_type"] = row.ItemArray[3].ToString();
            //    DTagents.Rows.Add(Row);
            //}

            if (dtAgents != null)
            {
                ddlAgent.DataSource = dtAgents.Select("AGENT_ID NOT ='" + agentId + "'").CopyToDataTable();
                ddlAgent.DataTextField = "AGENT_NAME";
                ddlAgent.DataValueField = "AGENT_ID";
                ddlAgent.DataBind();
                ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "0"));
                ddlAgent.SelectedIndex = 0;
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in BindAgents()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
            throw ex;
        }
    }
    private void BindSources()
    {
        try
        {
            DataTable dtAgentSources = OffLineBO.GetSourcesForOffLine(Settings.LoginInfo.AgentId, (int)ProductType.Flight);
            ddlSource.DataSource = dtAgentSources;
            ddlSource.DataTextField = "Name";
            ddlSource.DataMember = "SourceId";
            ddlSource.DataBind();
            ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
            ddlSource.Items.Insert(1, new ListItem("OffLine", "1"));
            ddlSource.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in BindSources()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
            throw ex;
        }
    }
    private void InitializeControls()
    {
        try
        {
            BindAgents();
            BindSources();

            if (Settings.LoginInfo.AgentType == AgentType.B2B || Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                rbtnSelf.Checked = false;
                rbtnSelf.Visible = false;
                rbtnAgent.Checked = true;
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "disablefield()", "disablefield");

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in InitializeControls()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
            throw ex;
        }
    }

    /// <summary>
    /// Bind the Airlines To airline dropdownlist
    /// </summary>
    private void BindAirlines()
    {
        try
        {
            DataTable dtAirline = Airline.GetAllAirlines();
            ddlairline.DataSource = dtAirline;
            ddlairline.DataValueField = "airlineCode";
            ddlairline.DataTextField = "AirlineDescription";
            ddlairline.DataBind();
            ddlairline.Items.Insert(0, new ListItem("select", "-1"));

            ddlRepAirline.DataSource = dtAirline;
            ddlRepAirline.DataValueField = "airlineCode";
            ddlRepAirline.DataTextField = "AirlineDescription";
            ddlRepAirline.DataBind();
            ddlRepAirline.Items.Insert(0, new ListItem("select", "-1"));
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in BindAirlines()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
            throw ex;
        }
    }
    private void BindAgentCode()
    {
        try
        {
            DataTable dtCustomer1 = AgentMaster.GetList(1, "AGENT-ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
            ddlCustomer1.DataSource = dtCustomer1;
            ddlCustomer1.DataMember = "AGENT_ID";
            ddlCustomer1.DataValueField = "AGENT_ID";
            ddlCustomer1.DataTextField = "AGENT_NAME";
            ddlCustomer1.DataBind();
            ddlCustomer1.Items.Insert(0, new ListItem("Select", "-1"));
            ddlCustomer1.Items.Insert(1, new ListItem("0-Cash_Customer", "1"));
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in BindAgentCode()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
            throw ex;
        }

    }

    private void BindSuppliers(int agentcode)
    {
        SqlParameter[] paramList = new SqlParameter[2];
        paramList[0] = new SqlParameter("@AgentID", agentcode);
        paramList[1] = new SqlParameter("@ProductID", ProductType.Flight);
        DataTable dtSupplier = DBGateway.FillDataTableSP("usp_GetSupplierCodeForOffLine", paramList);
        ddlSupplier.DataSource = dtSupplier;
        ddlSupplier.DataMember = "supp_code";
        ddlSupplier.DataValueField = "supp_code";
        ddlSupplier.DataTextField = "supp_Source_name";
        ddlSupplier.DataBind();
        ddlSupplier.Items.Insert(0, new ListItem("Select", "-1"));
    }
    //protected void btnRetrieve_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        int agentID = 0;
    //        int locationID = 0;
    //        int decimalPoint = 2;

    //        //int agentId = (rbtnAgent.Checked == true ? Convert.ToInt32(ddlAgent.SelectedValue) : Settings.LoginInfo.AgentId);
    //        if (rbtnAgent.Checked)
    //        {

    //            Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(ddlAgent.SelectedItem.Value);
    //            agentID = Settings.LoginInfo.OnBehalfAgentID;
    //            Settings.LoginInfo.IsOnBehalfOfAgent = true;
    //            //Settings.LoginInfo.OnBehalfAgentLocation = Convert.ToInt32(ddlConsultant.SelectedValue);

    //            AgentMaster agent = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
    //            Settings.LoginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
    //            Settings.LoginInfo.OnBehalfAgentDecimalValue = agent.DecimalValue;
    //            StaticData sd = new StaticData();
    //            sd.BaseCurrency = agent.AgentCurrency;
    //            //Settings.LoginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;               
    //            //locationID = Convert.ToInt32(ddlConsultant.SelectedValue);
    //            decimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;

    //            agentDecimalPoints = agent.DecimalValue;
    //            ddlcurrencycode.Items.Add(new ListItem(agent.AgentCurrency, agent.AgentCurrency));
    //            ddlcurrencycode.SelectedValue = agent.AgentCurrency;
    //            BindAgentCode(agentID);
    //            ddlCustomer1.Enabled = false;
    //            string agentType = string.Empty;
    //            BindLocations(agent.AgentType, agentID);
    //            ddlMode.SelectedValue = "3";
    //            ddlMode.Enabled = false;

    //        }
    //        else
    //        {
    //            agentID = Settings.LoginInfo.AgentId;
    //            Settings.LoginInfo.IsOnBehalfOfAgent = false;
    //            locationID = Convert.ToInt32(Settings.LoginInfo.LocationID);
    //            decimalPoint = Settings.LoginInfo.DecimalValue;
    //            agentDecimalPoints = Settings.LoginInfo.DecimalValue;
    //            ddlcurrencycode.Items.Add(new ListItem(Settings.LoginInfo.Currency, Settings.LoginInfo.Currency));
    //            ddlcurrencycode.SelectedValue = Settings.LoginInfo.Currency;
    //            BindAgentCode(0);
    //            ddlMode.SelectedValue = "3";
    //            ddlMode.Enabled = true;
    //            BindLocations(Convert.ToInt32(Settings.LoginInfo.AgentType), agentID);
    //            ddlConsultant.SelectedValue = Settings.LoginInfo.LocationID.ToString();
    //            ddlConsultant.Items.Add(new ListItem("select", "-1"));
    //        }












    //        // ddlConsultant.Enabled = (rbtnAgent.Checked) ? true : false;
    //        //if (rbtnAgent.Checked)
    //        //{
    //        //    AgentMaster agent = new AgentMaster(agentId);
    //        //    agentDecimalPoints = agent.DecimalValue;
    //        //    ddlcurrencycode.Items.Add(new ListItem(agent.AgentCurrency, agent.AgentCurrency));
    //        //    ddlcurrencycode.SelectedValue = agent.AgentCurrency;
    //        //    BindAgentCode(agentId);
    //        //    ddlCustomer1.Enabled = false;
    //        //    string agentType = string.Empty;
    //        //    BindLocations(agent.AgentType, agentId);
    //        //    ddlMode.SelectedValue = "3";
    //        //    ddlMode.Enabled = false;

    //        //}
    //        //else
    //        //{
    //        //    agentDecimalPoints = Settings.LoginInfo.DecimalValue;
    //        //    ddlcurrencycode.Items.Add(new ListItem(Settings.LoginInfo.Currency, Settings.LoginInfo.Currency));
    //        //    ddlcurrencycode.SelectedValue = Settings.LoginInfo.Currency;
    //        //    BindAgentCode(0);
    //        //    ddlMode.SelectedValue = "3";
    //        //    ddlMode.Enabled = true;
    //        //    BindLocations(Convert.ToInt32(Settings.LoginInfo.AgentType), agentId);
    //        //    ddlConsultant.SelectedValue = Settings.LoginInfo.LocationID.ToString();
    //        //    ddlConsultant.Items.Add(new ListItem("select", "-1"));

    //        //}

    //        if (ddlSource.SelectedItem.Text == "OffLine")
    //        {
    //            EditPNRDiv.Style.Add("display", "block");
    //            ddlType.SelectedIndex = 0;
    //            ddlPaxTitle.Items.Clear();
    //            ddlPaxTitle.Items.Add(new ListItem("Mr.", "Mr."));
    //            ddlPaxTitle.Items.Add(new ListItem("Ms.", "Ms."));
    //            ddlPaxTitle.Items.Add(new ListItem("Dr.", "Dr."));
    //            ddlPaxTitle.Items.Add(new ListItem("Master", "MSTR"));
    //            Utility.StartupScript(this.Page, "HideSearchOption()", "HideSearchOption");
    //        }
    //        //FillDummyvalues();
    //        Session["sectorCount"] = null;
    //    }
    //    catch (Exception ex)
    //    {
    //        Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in btnRetrieve_Click()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
    //    }
    //}

    /// <summary>
    /// To get the Decimal places of Agent or Self based on the selection
    /// </summary>
    private void GetAgencyDecimal()
    {
        int agentId = (rbtnAgent.Checked == true ? Convert.ToInt32(ddlAgent.SelectedValue) : Settings.LoginInfo.AgentId);

        if (rbtnAgent.Checked)
        {
            AgentMaster agent = new AgentMaster(agentId);
            agentDecimalPoints = agent.DecimalValue;
        }
        else
        {
            agentDecimalPoints = Settings.LoginInfo.DecimalValue;
        }
    }

    /// <summary>
    /// Bind the location based on the agent type
    /// </summary>
    /// <param name="agentTypeID"></param>
    /// <param name="agentId"></param>
    private void BindLocations(int agentTypeID, int agentId)
    {
        string agentType = string.Empty;
        switch (agentTypeID)
        {
            case 1:
                agentType = "BaseAgent ";
                break;
            case 2:
                agentType = "Agent";
                break;
            case 3:
                agentType = "B2B";
                break;
            case 4:
                agentType = "B2B2B";
                break;

        }

        DataTable dtlocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
        hdnloginAgentLocations.Value = JsonConvert.SerializeObject(dtlocations);//assigning login agent locations and binding if selected agent currency is not INR then binding these locations in ddlConsulatant dropdown.
        ddlConsultant.DataSource = dtlocations;
        ddlConsultant.DataValueField = "LOCATION_ID";
        ddlConsultant.DataTextField = "MAP_CODE_NAME";
        ddlConsultant.DataBind();
        ddlConsultant.Items.Insert(0, new ListItem("--Select Location--", "-1"));


    }

    /// <summary>
    /// Editing the pax details from Popup sending Index id as parm
    /// </summary>
    /// <param name="paxIndex"></param>
    private void EditPaxDetails(int paxIndex)
    {
        try
        {
            if (Session["itinerary"] != null)
            {
                EditPNRDiv.Style.Add("display", "block");

                itinerary = (FlightItinerary)Session["itinerary"];
                bindPaxType(paxIndex);
                FlightPassenger pax = itinerary.Passenger[paxIndex];
                txtPaxName.Text = itinerary.Passenger[paxIndex].FirstName + "/" + itinerary.Passenger[paxIndex].LastName;
                txtSellingFare.Text = itinerary.Passenger[paxIndex].Price.PublishedFare.ToString();
                ddlPaxTitle.SelectedValue = pax.Title;
                txtPnr.Text = itinerary.PNR;
                //ddlairline.SelectedValue = itinerary.AirlineCode;//ziya
                ddlairline.SelectedValue = itinerary.ValidatingAirlineCode;
                txtSalesDate.Text = itinerary.LastTicketDate.ToString("MM/dd/yyyy");
                txtTravelDt.Text = itinerary.TravelDate.ToString("MM/dd/yyyy");
                ddlSector1.SelectedValue = itinerary.Origin;
                ddldestination.SelectedValue = itinerary.Destination;
                //txtRepAirLine.Text = "";//itinerary.Segments[paxIndex].OperatingCarrier;
                switch (itinerary.Passenger[paxIndex].Type)
                {
                    case PassengerType.Adult:
                        ddlType.SelectedValue = "AD";
                        break;
                    case PassengerType.Child:
                        ddlType.SelectedValue = "CHD";
                        break;
                    case PassengerType.Infant:
                        ddlType.SelectedValue = "INF";
                        break;
                }
                ddlType.Enabled = false;
                List<string> Sectors = new List<string>();
                foreach (FlightInfo fi in itinerary.Segments)
                {
                    if (!Sectors.Contains(fi.Origin.AirportCode))
                    {
                        Sectors.Add(fi.Origin.AirportCode);
                    }
                    if (!Sectors.Contains(fi.Destination.AirportCode))
                    {
                        Sectors.Add(fi.Destination.AirportCode);
                    }
                }
                ddlSector1.SelectedValue = Sectors[0];
                ddlSector2.SelectedValue = Sectors[1];

                switch (Sectors.Count)
                {
                    case 3:
                        ddlSector3.SelectedValue = Sectors[2];
                        break;
                    case 4:
                        ddlSector4.SelectedValue = Sectors[3];
                        break;
                    case 5:
                        ddlSector5.SelectedValue = Sectors[4];
                        break;
                    case 6:
                        ddlSector6.SelectedValue = Sectors[5];
                        break;
                    case 7:
                        ddlSector7.SelectedValue = Sectors[6];
                        break;
                    case 8:
                        ddlSector8.SelectedValue = Sectors[7];
                        break;
                    case 9:
                        ddlSector9.SelectedValue = Sectors[8];
                        break;
                    case 10:
                        ddlSector10.SelectedValue = Sectors[9];
                        break;

                }
                hndsegmentsCount.Value = Convert.ToInt32(Sectors.Count - 1).ToString();
                txtPnr.Text = itinerary.PNR;
                List<KeyValuePair<string, decimal>> taxBreakup = new List<KeyValuePair<string, decimal>>();
                taxBreakup = itinerary.Passenger[paxIndex].TaxBreakup;
                if (pax.Price == null)
                    pax.Price = new PriceAccounts();

                txtTax.Text = pax.Price.Tax.ToString();
                txtSellingFare.Text = pax.Price.PublishedFare.ToString();

                //commission percent value stored in commType
                txtCommper.Text = pax.Price.CommissionValue.ToString();
                txtCommisionAmt.Text = pax.Price.OurCommission.ToString();
                txtCommisionOn.Text = txtSellingFare.Text;
                txtCommision.Text = txtCommisionAmt.Text;

                //txtCommper.Text = pax.Price.CommissionType.ToString();

                //mark up
                txtExcRate.Text = pax.Price.RateOfExchange.ToString();
                txtMarkUp.Text = pax.Price.Markup.ToString();
                txtAmount2.Text = pax.Price.AsvAmount.ToString();

                //Discount details
                txtDiscountAmt.Text = pax.Price.Discount.ToString();
                txtDiscountper.Text = pax.Price.DiscountValue.ToString();
                txtDiscountOn.Text = txtSellingFare.Text;
                txtDiscount.Text = txtDiscountAmt.Text;

                //transaction Details
                txtTransFee.Text = pax.Price.TransactionFee.ToString();
                txtAddlTransFee.Text = pax.Price.AdditionalTxnFee.ToString();

                //Calc Details
                TxtInputVAT.Text = pax.Price.InputVATAmount.ToString();
                txtOutPutVAT.Text = pax.Price.OutputVATAmount.ToString();

                //calculation logic
                txtTotalFare.Text = Math.Round((Convert.ToDecimal(txtSellingFare.Text) + Convert.ToDecimal(txtTax.Text)), agentDecimalPoints).ToString();
                txtProfit.Text = Math.Round((Convert.ToDecimal(txtMarkUp.Text) + Convert.ToDecimal(txtAmount2.Text) + Convert.ToDecimal(txtTransFee.Text) + Convert.ToDecimal(txtAddlTransFee.Text)), agentDecimalPoints).ToString();
                txtNetpayable.Text = Math.Round(((Convert.ToDecimal(txtSellingFare.Text) + Convert.ToDecimal(txtTax.Text)) - Convert.ToDecimal(txtCommisionAmt.Text)), agentDecimalPoints).ToString();
                txtcashAmount.Text = Math.Round(Convert.ToDecimal(txtSellingFare.Text) + Convert.ToDecimal(txtTax.Text) + Convert.ToDecimal(txtMarkUp.Text) + Convert.ToDecimal(txtAmount2.Text) - Convert.ToDecimal(txtTransFee.Text) - Convert.ToDecimal(txtAddlTransFee.Text) - Convert.ToDecimal(txtDiscountAmt.Text), agentDecimalPoints).ToString();
                txtCollected.Text = Math.Round(Convert.ToDecimal(txtSellingFare.Text) + Convert.ToDecimal(txtTax.Text) + Convert.ToDecimal(txtMarkUp.Text) + Convert.ToDecimal(txtAmount2.Text) - Convert.ToDecimal(txtDiscountAmt.Text), agentDecimalPoints).ToString();


                //txtSellingFare.Text = pax.Price.PublishedFare.ToString();
                //txtMarkUp.Text = pax.Price.Markup.ToString();
                //txtTax.Text = pax.Price.Tax.ToString();
                //txtCommision.Text = pax.Price.OurCommission.ToString();
                //txtOurPLB.Text = pax.Price.OurPLB.ToString();
                //txtAgentCom.Text = pax.Price.AgentCommission.ToString();
                //txtAgentPLB.Text = pax.Price.AgentPLB.ToString();
                //ddlcurrencycode.SelectedValue = pax.Price.CurrencyCode;
                //txtTransFee.Text = pax.Price.TransactionFee.ToString();
                //txtAddlTransFee.Text = pax.Price.AdditionalTxnFee.ToString();
                //TxtInputVAT.Text = pax.Price.InputVATAmount.ToString();
                //txtOutPutVAT.Text = pax.Price.OutputVATAmount.ToString();  


                isEditFromPopUp = true;
                Session["taxBreakUp"] = taxBreakup;

                FlightInfo seg = null;
                int segment = 0;
                if (itinerary != null)
                {
                    seg = itinerary.Segments[segment];
                }
                for (int i = 0; i < Convert.ToInt32(hndsegmentsCount.Value); i++)
                {
                    int RowNo = i + 1;
                    CheckBox chkIsRefundable = (CheckBox)tblSector.Rows[RowNo].Cells[0].FindControl("chkIsRefund" + RowNo);
                    DropDownList ddlFromSector = (DropDownList)tblSector.Rows[RowNo].Cells[1].FindControl("ddlFromSector" + RowNo);
                    DropDownList ddlToSector = (DropDownList)tblSector.Rows[RowNo].Cells[2].FindControl("ddlToSector" + RowNo);
                    TextBox txtSectorFlyingCarrier = (TextBox)tblSector.Rows[RowNo].Cells[3].FindControl("txtFlyingCarrier" + RowNo);
                    TextBox txtSectorFlightno = (TextBox)tblSector.Rows[RowNo].Cells[4].FindControl("txtFlightN0" + RowNo);
                    DateControl txtSectorDeptDate = (DateControl)tblSector.Rows[RowNo].Cells[5].FindControl("DepartureDt" + RowNo);
                    DateControl txtSectorArrvDate = (DateControl)tblSector.Rows[RowNo].Cells[6].FindControl("ArrivalDt" + RowNo);
                    TextBox txtSectorFairBase = (TextBox)tblSector.Rows[RowNo].Cells[7].FindControl("txtFareBasis" + RowNo);
                    TextBox txtSectorClass = (TextBox)tblSector.Rows[RowNo].Cells[8].FindControl("txtClass" + RowNo);
                    TextBox txtSectorDuration = (TextBox)tblSector.Rows[RowNo].Cells[9].FindControl("txtDuration" + RowNo);

                    if (RowNo == 1)
                    {
                        chkIsRefund1.Checked = (itinerary != null ? itinerary.NonRefundable : true);
                        ddlFromSector1.SelectedValue = (itinerary.Segments[i].Origin).AirportCode;
                        ddlToSector1.SelectedValue = (itinerary.Segments[i].Destination).AirportCode;
                        txtClass1.Text = itinerary.Segments[i].BookingClass.ToString();
                        txtFlyingCarrier1.Text = itinerary.Segments[i].Airline;
                        txtSectorFlightno.Text = itinerary.Segments[i].FlightNumber;
                        txtSectorDeptDate.Value = itinerary.Segments[i].DepartureTime;
                        txtSectorArrvDate.Value = itinerary.Segments[i].ArrivalTime;
                        txtDuration1.Text = (itinerary.Segments[i].Duration).TotalMinutes.ToString();
                    }

                    if (RowNo == 2)
                    {
                        chkIsRefund2.Checked = (itinerary != null ? itinerary.NonRefundable : true);
                        ddlFromSector2.SelectedValue = (itinerary.Segments[i].Origin).AirportCode;
                        ddlToSector2.SelectedValue = (itinerary.Segments[i].Destination).AirportCode;
                        txtClass2.Text = itinerary.Segments[i].BookingClass.ToString();
                        txtFlyingCarrier2.Text = itinerary.Segments[i].Airline;
                        txtSectorFlightno.Text = itinerary.Segments[i].FlightNumber;
                        txtSectorDeptDate.Value = itinerary.Segments[i].DepartureTime;
                        txtSectorArrvDate.Value = itinerary.Segments[i].ArrivalTime;
                        txtDuration2.Text = (itinerary.Segments[i].Duration).TotalMinutes.ToString();

                    }
                    if (RowNo == 3)
                    {
                        chkIsRefund3.Checked = (itinerary != null ? itinerary.NonRefundable : true);
                        ddlFromSector3.SelectedValue = (itinerary.Segments[i].Origin).AirportCode;
                        ddlToSector3.SelectedValue = (itinerary.Segments[i].Destination).AirportCode;
                        txtClass3.Text = itinerary.Segments[i].BookingClass.ToString();
                        txtFlyingCarrier3.Text = itinerary.Segments[i].Airline;
                        txtSectorFlightno.Text = itinerary.Segments[i].FlightNumber;
                        txtSectorDeptDate.Value = itinerary.Segments[i].DepartureTime;
                        txtSectorArrvDate.Value = itinerary.Segments[i].ArrivalTime;
                        txtDuration3.Text = (itinerary.Segments[i].Duration).TotalMinutes.ToString();

                    }
                    if (RowNo == 4)
                    {
                        chkIsRefund4.Checked = (itinerary != null ? itinerary.NonRefundable : true);
                        ddlFromSector4.SelectedValue = (itinerary.Segments[i].Origin).AirportCode;
                        ddlToSector4.SelectedValue = (itinerary.Segments[i].Destination).AirportCode;
                        txtClass4.Text = itinerary.Segments[i].BookingClass.ToString();
                        txtFlyingCarrier4.Text = itinerary.Segments[i].Airline;
                        txtSectorFlightno.Text = itinerary.Segments[i].FlightNumber;
                        txtSectorDeptDate.Value = itinerary.Segments[i].DepartureTime;
                        txtSectorArrvDate.Value = itinerary.Segments[i].ArrivalTime;
                        txtDuration4.Text = (itinerary.Segments[i].Duration).TotalMinutes.ToString();

                    }
                    if (RowNo == 5)
                    {
                        chkIsRefund5.Checked = (itinerary != null ? itinerary.NonRefundable : true);
                        ddlFromSector5.SelectedValue = (itinerary.Segments[i].Origin).AirportCode;
                        ddlToSector5.SelectedValue = (itinerary.Segments[i].Destination).AirportCode;
                        txtClass5.Text = itinerary.Segments[i].BookingClass.ToString();
                        txtFlyingCarrier5.Text = itinerary.Segments[i].Airline;
                        txtSectorFlightno.Text = itinerary.Segments[i].FlightNumber;
                        txtSectorDeptDate.Value = itinerary.Segments[i].DepartureTime;
                        txtSectorArrvDate.Value = itinerary.Segments[i].ArrivalTime;
                        txtDuration5.Text = (itinerary.Segments[i].Duration).TotalMinutes.ToString();

                    }
                    if (RowNo == 6)
                    {
                        chkIsRefund6.Checked = (itinerary != null ? itinerary.NonRefundable : true);
                        ddlFromSector6.SelectedValue = (itinerary.Segments[i].Origin).AirportCode;
                        ddlToSector6.SelectedValue = (itinerary.Segments[i].Destination).AirportCode;
                        txtClass6.Text = itinerary.Segments[i].BookingClass.ToString();
                        txtFlyingCarrier6.Text = itinerary.Segments[i].Airline;
                        txtSectorFlightno.Text = itinerary.Segments[i].FlightNumber;
                        txtSectorDeptDate.Value = itinerary.Segments[i].DepartureTime;
                        txtSectorArrvDate.Value = itinerary.Segments[i].ArrivalTime;
                        txtDuration6.Text = (itinerary.Segments[i].Duration).TotalMinutes.ToString();
                    }
                    if (RowNo == 7)
                    {
                        chkIsRefund7.Checked = (itinerary != null ? itinerary.NonRefundable : true);
                        ddlFromSector7.SelectedValue = (itinerary.Segments[i].Origin).AirportCode;
                        ddlToSector7.SelectedValue = (itinerary.Segments[i].Destination).AirportCode;
                        txtClass7.Text = itinerary.Segments[i].BookingClass.ToString();
                        txtFlyingCarrier7.Text = itinerary.Segments[i].Airline;
                        txtSectorFlightno.Text = itinerary.Segments[i].FlightNumber;
                        txtSectorDeptDate.Value = itinerary.Segments[i].DepartureTime;
                        txtSectorArrvDate.Value = itinerary.Segments[i].ArrivalTime;
                        txtDuration7.Text = (itinerary.Segments[i].Duration).TotalMinutes.ToString();

                    }
                    if (RowNo == 8)
                    {
                        chkIsRefund8.Checked = (itinerary != null ? itinerary.NonRefundable : true);
                        ddlFromSector8.SelectedValue = (itinerary.Segments[i].Origin).AirportCode;
                        ddlToSector8.SelectedValue = (itinerary.Segments[i].Destination).AirportCode;
                        txtClass8.Text = itinerary.Segments[i].BookingClass.ToString();
                        txtFlyingCarrier8.Text = itinerary.Segments[i].Airline;
                        txtSectorFlightno.Text = itinerary.Segments[i].FlightNumber;
                        txtSectorDeptDate.Value = itinerary.Segments[i].DepartureTime;
                        txtSectorArrvDate.Value = itinerary.Segments[i].ArrivalTime;
                        txtDuration8.Text = (itinerary.Segments[i].Duration).TotalMinutes.ToString();
                    }
                    if (RowNo == 9)
                    {
                        chkIsRefund9.Checked = (itinerary != null ? itinerary.NonRefundable : true);
                        ddlFromSector9.SelectedValue = (itinerary.Segments[i].Origin).AirportCode;
                        ddlToSector9.SelectedValue = (itinerary.Segments[i].Destination).AirportCode;
                        txtClass9.Text = itinerary.Segments[i].BookingClass.ToString();
                        txtFlyingCarrier9.Text = itinerary.Segments[i].Airline;
                        txtSectorFlightno.Text = itinerary.Segments[i].FlightNumber;
                        txtSectorDeptDate.Value = itinerary.Segments[i].DepartureTime;
                        txtSectorArrvDate.Value = itinerary.Segments[i].ArrivalTime;
                        txtDuration9.Text = (itinerary.Segments[i].Duration).TotalMinutes.ToString();
                    }
                    if (RowNo == 10)
                    {
                        chkIsRefund10.Checked = (itinerary != null ? itinerary.NonRefundable : true);
                        ddlFromSector10.SelectedValue = (itinerary.Segments[i].Origin).AirportCode;
                        ddlToSector10.SelectedValue = (itinerary.Segments[i].Destination).AirportCode;
                        txtClass10.Text = itinerary.Segments[i].BookingClass.ToString();
                        txtFlyingCarrier10.Text = itinerary.Segments[i].Airline;
                        txtSectorFlightno.Text = itinerary.Segments[i].FlightNumber;
                        txtSectorDeptDate.Value = itinerary.Segments[i].DepartureTime;
                        txtSectorArrvDate.Value = itinerary.Segments[i].ArrivalTime;
                        txtDuration10.Text = (itinerary.Segments[i].Duration).TotalMinutes.ToString();

                    }

                    //ddlFromSector1.SelectedValue = (itinerary != null ? itinerary.Segments[i].Origin.AirportCode : "");
                    ////ddldestination.SelectedValue = (itinerary != null ? itinerary.Segments[0].Destination.AirportCode : "");
                    //txtSectorFlyingCarrier.Text = (itinerary != null ? seg.Airline : "");
                    //txtSectorFlightno.Text = (itinerary != null ? seg.FlightNumber : "");
                    //if (itinerary != null)
                    //{
                    //    txtSectorDeptDate.Value = (seg.DepartureTime);
                    //}
                    //if (itinerary != null)
                    //{
                    //    txtSectorArrvDate.Value = (seg.ArrivalTime);
                    //}
                    //if (itinerary != null && itinerary.FareRules != null && itinerary.FareRules.Count > 0)
                    //{
                    //    txtSectorFairBase.Text = itinerary.FareRules[0].FareBasisCode;
                    //}
                    //btnUpdate.Visible = true;
                    //btnAdd.Visible = false;
                    btnClear.Visible = false;
                    hdnEditMode.Value = "EditMode";
                    Utility.StartupScript(this.Page, "makeEditFalse()", "makeEditFalse");

                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in EditPaxDetails()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
        }
    }

    /// <summary>
    /// Updating the pax details after editing the pax details changes
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        int paxIndex = Convert.ToInt32(hdnSelectedIndex.Value);
        if (Convert.ToDecimal(txtcashAmount.Text) <= 0)
        {
            Utility.Alert(this.Page, "Cash Amount should not be negative");
            isFromClear = true;
            return;
        }
        int agentID = (rbtnAgent.Checked == true ? Convert.ToInt32(ddlAgent.SelectedValue) : Settings.LoginInfo.AgentId);
        AgentMaster agent = new AgentMaster(agentID);
        decimal agentbalance = agent.UpdateBalance(0);
        //if (agentbalance <= 0 || agentbalance < itinerary.AgentPrice) // Not req
        //{
        //    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Insufficient Funds.');window.location='OffLineEntry.aspx';", true);
        //}

        IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-US");
        itinerary = (FlightItinerary)Session["itinerary"];
        if (txtPaxName.Text.Contains("/"))
        {
            string[] name = txtPaxName.Text.Split('/');
            itinerary.Passenger[paxIndex].FirstName = name[0];
            itinerary.Passenger[paxIndex].LastName = name[1];
        }
        itinerary.Passenger[paxIndex].Price.PublishedFare = Convert.ToDecimal(txtSellingFare.Text);
        itinerary.PNR = txtPnr.Text;
        itinerary.AirlineCode = ddlairline.SelectedValue;

        itinerary.LastTicketDate = Convert.ToDateTime(txtSalesDate.Text, dateFormat);
        itinerary.TravelDate = Convert.ToDateTime(txtTravelDt.Text, dateFormat);
        itinerary.Origin = ddlSector1.SelectedValue;
        itinerary.Destination = ddldestination.SelectedValue;

        itinerary.Segments[0].Airline = ddlairline.SelectedValue;
        itinerary.PNR = txtPnr.Text;

        FlightPassenger pax = itinerary.Passenger[paxIndex];
        pax.Title = ddlPaxTitle.SelectedValue;
        pax.Price.SupplierPrice = Convert.ToDecimal(txtTotalFare.Text) - (Convert.ToDecimal(txtMarkUp.Text) + Convert.ToDecimal(txtAmount2.Text));

        pax.Price.PublishedFare = Convert.ToDecimal(txtSellingFare.Text);
        pax.Price.NetFare = 0;

        pax.Price.Tax = Convert.ToDecimal(txtTax.Text);



        pax.Price.OurCommission = Convert.ToDecimal(txtCommisionAmt.Text);
        pax.Price.CommissionValue = Convert.ToDecimal(txtCommper.Text);
        //mark up
        pax.Price.RateOfExchange = Convert.ToDecimal(txtExcRate.Text);
        pax.Price.Markup = Convert.ToDecimal(txtMarkUp.Text);
        pax.Price.AsvAmount = Convert.ToDecimal(txtAmount2.Text);

        //Discount details
        pax.Price.Discount = Convert.ToDecimal(txtDiscountAmt.Text);
        pax.Price.DiscountType = txtDiscountper.Text;
        pax.Price.DiscountValue = Convert.ToDecimal(txtDiscountper.Text);

        //transaction Details
        pax.Price.TransactionFee = Convert.ToDecimal(txtTransFee.Text);
        pax.Price.AdditionalTxnFee = Convert.ToDecimal(txtAddlTransFee.Text);

        List<KeyValuePair<string, decimal>> taxBreakUp = new List<KeyValuePair<string, decimal>>();
        //taxBreakUp.Add(new KeyValuePair<string, decimal>(txtCode0.Text, Convert.ToDecimal(txtValue0.Text)));
        //if (Convert.ToInt32(hndTaxCounter.Value) > 0)
        //{
        //    for (int k = 1; k <= Convert.ToInt32(hndTaxCounter.Value); k++)
        //    {
        //        string TaxCode = Request["txtCode" + k];
        //        decimal TaxAmount = Convert.ToDecimal(Request["txtValue" + k]);
        //        taxBreakUp.Add(new KeyValuePair<string, decimal>(TaxCode, TaxAmount));

        //    }
        //}
        pax.TaxBreakup = taxBreakUp;
        for (int i = 0; i < Convert.ToInt32(hndsegmentsCount.Value); i++)
        {
            int RowNo = i + 1;

            //tblSectors
            CheckBox chkIsRefundable = (CheckBox)tblSector.Rows[RowNo].Cells[0].FindControl("chkIsRefund" + RowNo);
            DropDownList ddlFromSector = (DropDownList)tblSector.Rows[RowNo].Cells[1].FindControl("ddlFromSector" + RowNo);
            DropDownList ddlToSector = (DropDownList)tblSector.Rows[RowNo].Cells[2].FindControl("ddlToSector" + RowNo);
            TextBox txtSectorFlyingCarrier = (TextBox)tblSector.Rows[RowNo].Cells[3].FindControl("txtFlyingCarrier" + RowNo);
            TextBox txtSectorFlightno = (TextBox)tblSector.Rows[RowNo].Cells[4].FindControl("txtFlightN0" + RowNo);
            DateControl txtSectorDeptDate = (DateControl)tblSector.Rows[RowNo].Cells[5].FindControl("DepartureDt" + RowNo);
            DateControl txtSectorArrvDate = (DateControl)tblSector.Rows[RowNo].Cells[6].FindControl("ArrivalDt" + RowNo);
            TextBox txtSectorFairBase = (TextBox)tblSector.Rows[RowNo].Cells[7].FindControl("txtFareBasis" + RowNo);
            TextBox txtSectorClass = (TextBox)tblSector.Rows[RowNo].Cells[8].FindControl("txtClass" + RowNo);
            TextBox txtSectorDuration = (TextBox)tblSector.Rows[RowNo].Cells[9].FindControl("txtDuration" + RowNo);

            itinerary.Segments[i].Origin = new Airport(ddlFromSector.SelectedValue.ToUpper());
            itinerary.Segments[i].Destination = new Airport(ddlToSector.SelectedValue.ToUpper());
            itinerary.Segments[i].BookingClass = txtSectorClass.Text;
            itinerary.Segments[i].FlightStatus = FlightStatus.Confirmed;
            itinerary.Segments[i].Airline = ddlairline.SelectedValue;
            itinerary.Segments[i].FlightNumber = txtSectorFlightno.Text;
            itinerary.Segments[i].DepartureTime = Convert.ToDateTime(txtSectorDeptDate.Value, dateFormat);
            itinerary.Segments[i].LastModifiedBy = (int)Settings.LoginInfo.UserID;
            itinerary.Segments[i].ArrivalTime = Convert.ToDateTime(txtSectorArrvDate.Value, dateFormat);
            itinerary.Segments[i].CreatedBy = (int)Settings.LoginInfo.UserID;
            itinerary.Segments[i].LastModifiedBy = (int)Settings.LoginInfo.UserID;
            //itinerary.Segments[i].OperatingCarrier = (ddlairline.SelectedValue == ddlRepAirline.SelectedValue ? string.Empty : ddlRepAirline.SelectedValue);
            itinerary.Segments[i].OperatingCarrier = txtSectorFlyingCarrier.Text;
            itinerary.Segments[i].Duration = new TimeSpan(0, Convert.ToInt32(txtSectorDuration.Text), 0);
            if (ddlSector6.SelectedValue != "-1" && !string.IsNullOrEmpty(ddlSector6.SelectedValue))
            {
                itinerary.Segments[i].ConjunctionNo = txtCongTicketNo.Text + "/" + txtConjPNR.Text;
            }
            itinerary.Segments[i].Status = "confirmed";
        }
        ddlType.Enabled = true;
        btnUpdate.Visible = false;

        //btnAdd.Visible = true;
        btnClear.Visible = true;
        hdnEditMode.Value = "Update";
        MakeDefaultValues();
        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Pax Details Updated");
    }
    private void bindPaxType(int paxIndex)
    {
        try
        {
            if (itinerary.Passenger[paxIndex].Type.ToString() == "Adult")
            {
                ddlPaxTitle.Items.Add(new ListItem("Mr.", "Mr."));
                ddlPaxTitle.Items.Add(new ListItem("Ms.", "Ms."));
                ddlPaxTitle.Items.Add(new ListItem("Dr.", "Dr."));
                ddlPaxTitle.Items.Add(new ListItem("Master", "MSTR"));

            }
            else
            {
                ddlPaxTitle.Enabled = true;
                ddlPaxTitle.Items.Clear();
                ddlPaxTitle.Items.Add(new ListItem("Mr.", "Mr."));
                ddlPaxTitle.Items.Add(new ListItem("Ms.", "Ms."));
                ddlPaxTitle.Items.Add(new ListItem("Dr.", "Dr."));
                ddlPaxTitle.Items.Add(new ListItem("Master", "MSTR"));
                ddlPaxTitle.Items.Add(new ListItem("Child", "CHD"));
                ddlPaxTitle.Items.Add(new ListItem("Infant", "INF"));
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in bindPaxType()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
        }
    }
    protected void ddlType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlType.SelectedItem.Text == "Adult")
            {
                ddlPaxTitle.Enabled = true;
                ddlPaxTitle.Items.Clear();
                ddlPaxTitle.Items.Add(new ListItem("Mr.", "Mr."));
                ddlPaxTitle.Items.Add(new ListItem("Ms.", "Ms."));
                ddlPaxTitle.Items.Add(new ListItem("Dr.", "Dr."));
                ddlPaxTitle.Items.Add(new ListItem("Master", "MSTR"));
            }
            else
            {
                ddlPaxTitle.Enabled = true;
                ddlPaxTitle.Items.Clear();
                ddlPaxTitle.Items.Add(new ListItem("Mr.", "Mr."));
                ddlPaxTitle.Items.Add(new ListItem("Ms.", "Ms."));
                ddlPaxTitle.Items.Add(new ListItem("Dr.", "Dr."));
                ddlPaxTitle.Items.Add(new ListItem("Master", "MSTR"));
                ddlPaxTitle.Items.Add(new ListItem("Child", "CHD"));
                ddlPaxTitle.Items.Add(new ListItem("Infant", "INF"));
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in ddlType_SelectedIndexChanged()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
        }
    }

    /// <summary>
    /// To show the PoP Up with saved pax Details
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            btnSave.Enabled = false;
            SaveItinerary();
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "SaveSuccess('S');", true);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in btnSave_Click()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "SaveSuccess('E');", true);
        }
        finally
        {
            btnSave.Enabled = true;
        }
    }

    /// <summary>
    /// Adding the pax Details 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {

            if (itinerary != null && itinerary.Passenger != null && itinerary.Passenger.Length == 9)
            {
                Utility.Alert(this.Page, " Can't  Add More than 9 Passesngers Details. ");
                isFromClear = true;
                return;
            }
            if (Convert.ToDecimal(txtcashAmount.Text) <= 0)
            {
                Utility.Alert(this.Page, "Cash Amount should not be Zero/negative");
                isFromClear = true;
                return;
            }
            int agentID = (rbtnAgent.Checked == true ? Convert.ToInt32(ddlAgent.SelectedValue) : Settings.LoginInfo.AgentId);
            AgentMaster agent = new AgentMaster(agentID);
            decimal agentbalance = agent.UpdateBalance(0);
            //if (agentbalance <= 0 || agentbalance < Convert.ToDecimal(txtcashAmount.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Insufficient Funds.');window.location='OffLineEntry.aspx';", true);
            //    return;
            //}

            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-US");
            List<FareRule> fareRule = new List<FareRule>();
            itinerary.FareRules = fareRule;
            itinerary.PNR = txtPNRNum.Text;
            itinerary.AirlineCode = ddlairline.SelectedValue;

            itinerary.Origin = ddlSector1.SelectedValue;
            itinerary.Destination = ddldestination.SelectedValue;
            itinerary.FareType = "PUB";
            itinerary.FlightBookingSource = BookingSource.OffLine;
            itinerary.LastTicketDate = Convert.ToDateTime(txtSalesDate.Text, dateFormat);
            itinerary.TicketAdvisory = string.Empty;
            itinerary.LastModifiedBy = (int)Settings.LoginInfo.UserID;
            itinerary.TravelDate = Convert.ToDateTime(txtTravelDt.Text, dateFormat);
            itinerary.CreatedOn = Convert.ToDateTime(txtSalesDate.Text, dateFormat);
            itinerary.SupplierCode = (ddlSupplier.Text == "-1" ? "" : ddlSupplier.Text);
            itinerary.LocationId = (hdnlocCountryCode.Value == "IN" ? Convert.ToInt32(hndLocationId.Value) : Convert.ToInt32(Settings.LoginInfo.LocationID));
            itinerary.LocationName = (hdnlocCountryCode.Value == "IN" ? locationName : Settings.LoginInfo.LocationName);
            if (!string.IsNullOrEmpty(ddlMode.SelectedValue))
            {
                if (Convert.ToInt32(ddlMode.SelectedValue) == 1)
                {
                    itinerary.PaymentMode = ModeOfPayment.Cash;
                    lblCardType.Text = "Cash Amount";
                }
                if (Convert.ToInt32(ddlMode.SelectedValue) == 2)
                {
                    itinerary.PaymentMode = ModeOfPayment.CreditCard;
                }
                if (Convert.ToInt32(ddlMode.SelectedValue) == 3)
                {
                    itinerary.PaymentMode = ModeOfPayment.Credit;
                    lblCardType.Text = "Credit Amount";
                }
            }
            itinerary.NonRefundable = true;
            itinerary.TransactionType = "B2B";
            itinerary.CreatedBy = (int)Settings.LoginInfo.UserID;
            itinerary.BookingId = 0;//
            itinerary.AliasAirlineCode = string.Empty;
            itinerary.IsOurBooking = false;
            itinerary.UniversalRecord = string.Empty;
            itinerary.AirLocatorCode = string.Empty;
            itinerary.SupplierLocatorCode = string.Empty;
            //itinerary.LocationId = (int)Settings.LoginInfo.LocationID;

            itinerary.IsInsured = false; //f
            itinerary.IsLCC = false;//f
            itinerary.TripId = string.Empty; //null
            if (itinerary.Segments == null)
            {
                FlightInfo[] segments = new FlightInfo[Convert.ToInt32(hndsegmentsCount.Value)];
                itinerary.Segments = new FlightInfo[Convert.ToInt32(hndsegmentsCount.Value)];
                for (int i = 0; i < Convert.ToInt32(hndsegmentsCount.Value); i++)
                {
                    int RowNo = i + 1;

                    //tblSectors
                    CheckBox chkIsRefundable = (CheckBox)tblSector.Rows[RowNo].Cells[0].FindControl("chkIsRefund" + RowNo);
                    DropDownList ddlFromSector = (DropDownList)tblSector.Rows[RowNo].Cells[1].FindControl("ddlFromSector" + RowNo);
                    DropDownList ddlToSector = (DropDownList)tblSector.Rows[RowNo].Cells[2].FindControl("ddlToSector" + RowNo);
                    TextBox txtSectorFlyingCarrier = (TextBox)tblSector.Rows[RowNo].Cells[3].FindControl("txtFlyingCarrier" + RowNo);
                    TextBox txtSectorFlightno = (TextBox)tblSector.Rows[RowNo].Cells[4].FindControl("txtFlightN0" + RowNo);
                    DateControl txtSectorDeptDate = (DateControl)tblSector.Rows[RowNo].Cells[5].FindControl("DepartureDt" + RowNo);
                    DateControl txtSectorArrvDate = (DateControl)tblSector.Rows[RowNo].Cells[6].FindControl("ArrivalDt" + RowNo);
                    TextBox txtSectorFairBase = (TextBox)tblSector.Rows[RowNo].Cells[7].FindControl("txtFareBasis" + RowNo);
                    TextBox txtSectorClass = (TextBox)tblSector.Rows[RowNo].Cells[8].FindControl("txtClass" + RowNo);
                    TextBox txtSectorDuration = (TextBox)tblSector.Rows[RowNo].Cells[9].FindControl("txtDuration" + RowNo);


                    itinerary.Segments[i] = new FlightInfo();

                    itinerary.Segments[i].Origin = new Airport(ddlFromSector.SelectedValue.ToUpper());
                    itinerary.Segments[i].Destination = new Airport(ddlToSector.SelectedValue.ToUpper());
                    itinerary.Segments[i].BookingClass = txtSectorClass.Text;
                    itinerary.Segments[i].FlightStatus = FlightStatus.Confirmed;
                    itinerary.Segments[i].Airline = ddlairline.SelectedValue;
                    itinerary.Segments[i].FlightNumber = txtSectorFlightno.Text;
                    itinerary.Segments[i].DepartureTime = Convert.ToDateTime(txtSectorDeptDate.Value, dateFormat);
                    itinerary.Segments[i].ArrivalTime = Convert.ToDateTime(txtSectorArrvDate.Value, dateFormat);
                    itinerary.Segments[i].LastModifiedBy = (int)Settings.LoginInfo.UserID;
                    itinerary.Segments[i].Duration = new TimeSpan(0, Convert.ToInt32(txtSectorDuration.Text), 0);
                    itinerary.Segments[i].CreatedBy = (int)Settings.LoginInfo.UserID;
                    itinerary.Segments[i].LastModifiedBy = (int)Settings.LoginInfo.UserID;
                    //itinerary.Segments[i].OperatingCarrier = (ddlairline.SelectedValue == ddlRepAirline.SelectedValue ? string.Empty : ddlRepAirline.SelectedValue);
                    itinerary.Segments[i].OperatingCarrier = txtSectorFlyingCarrier.Text;

                    if (ddlSector6.SelectedValue != "-1" && !string.IsNullOrEmpty(ddlSector6.SelectedValue))
                    {
                        itinerary.Segments[i].ConjunctionNo = txtCongTicketNo.Text + "/" + txtConjPNR.Text;
                    }

                    //itinerary.Segments[i].Group = 1;
                    //itinerary.Segments[i].BookingClass = txtSectorClass.Text;
                    itinerary.Segments[i].Status = "confirmed";
                    //itinerary.Segments[i].
                }
            }
            if (itinerary.Passenger != null)
            {
                PassengerList.AddRange(itinerary.Passenger);
                btnSave.Visible = true;
                if (PassengerList[0].FirstName.Trim() == txtPaxName.Text.Trim())
                {
                    return;
                }
            }
            if (itinerary.Segments != null)
            {
                FlightInfoList.AddRange(itinerary.Segments);
            }
            FlightPassenger pax = new FlightPassenger();
            if (txtPaxName.Text.Contains("/"))
            {
                string[] name = txtPaxName.Text.Split('/');
                pax.FirstName = name[0];
                pax.LastName = name[1];

            }

            //pax.LastName = txtPaxName.Text;// lastName
            //pax.FlightId = Convert.ToInt32(txtSectorFlightno.);

            pax.CreatedBy = (int)Settings.LoginInfo.UserID;
            switch (ddlType.SelectedValue)
            {
                case "AD":
                    pax.Type = PassengerType.Adult;
                    break;
                case "CHD":
                    pax.Type = PassengerType.Child;
                    break;
                case "INF":
                    pax.Type = PassengerType.Infant;
                    break;
            }
            pax.Title = ddlPaxTitle.SelectedValue;
            pax.Price = new PriceAccounts();
            pax.Price.SupplierPrice = Convert.ToDecimal(txtTotalFare.Text) - (Convert.ToDecimal(txtMarkUp.Text) + Convert.ToDecimal(txtAmount2.Text));

            pax.Price.PublishedFare = Convert.ToDecimal(txtSellingFare.Text);
            pax.Price.NetFare = 0;


            pax.Price.Tax = Convert.ToDecimal(txtTax.Text);

            //commission percent value stored in commType
            //pax.Price.OtherCharges = Convert.ToDecimal(txtCommper.Text);
            //pax.Price.OtherCharges= Convert.ToDecimal(txtCommper.Text);
            pax.Price.OurCommission = Convert.ToDecimal(txtCommisionAmt.Text);
            pax.Price.CommissionType = "P";
            pax.Price.CommissionValue = Convert.ToDecimal(txtCommper.Text);



            //mark up
            pax.Price.RateOfExchange = Convert.ToDecimal(txtExcRate.Text);
            pax.Price.Markup = Convert.ToDecimal(txtMarkUp.Text);
            pax.Price.AsvAmount = Convert.ToDecimal(txtAmount2.Text);
            pax.Price.AsvElement = "TF";

            //Discount details
            pax.Price.Discount = Convert.ToDecimal(txtDiscountAmt.Text);
            pax.Price.DiscountType = "P";
            pax.Price.DiscountValue = Convert.ToDecimal(txtDiscountper.Text);

            //transaction Details
            pax.Price.TransactionFee = Convert.ToDecimal(txtTransFee.Text);
            pax.Price.AdditionalTxnFee = Convert.ToDecimal(txtAddlTransFee.Text);

            //Calc Details
            pax.Price.InputVATAmount = Convert.ToDecimal(TxtInputVAT.Text);
            pax.Price.OutputVATAmount = hdnlocCountryCode.Value == "IN" ? Convert.ToDecimal(txtGST.Text) : Convert.ToDecimal(txtOutPutVAT.Text);


            pax.Price.OurPLB = Convert.ToDecimal(txtOurPLB.Text);
            pax.Price.AgentCommission = Convert.ToDecimal(txtAgentCom.Text);
            pax.Price.AgentPLB = Convert.ToDecimal(txtAgentPLB.Text);
            pax.Price.SeviceTax = 0;
            pax.Price.Currency = hdnCurrencyCode.Value;
            pax.Price.CurrencyCode = hdnCurrencyCode.Value;

            //pax.Price.
            PassengerList.Add(pax);
            if (PassengerList.Count == 1)
            {
                pax.IsLeadPax = true;
            }
            btnSave.Visible = true;
            itinerary.Passenger = PassengerList.ToArray();
            List<KeyValuePair<string, decimal>> taxBreakUp = new List<KeyValuePair<string, decimal>>();
            //taxBreakUp.Add(new KeyValuePair<string, decimal>(txtCode0.Text, Convert.ToDecimal(txtValue0.Text)));
            //if (Convert.ToInt32(hndTaxCounter.Value) > 0)
            //{
            //    for (int k = 1; k <= Convert.ToInt32(hndTaxCounter.Value); k++)
            //    {
            //        string TaxCode = Request["txtCode" + k];
            //        decimal TaxAmount = Convert.ToDecimal(Request["txtValue" + k]);
            //        taxBreakUp.Add(new KeyValuePair<string, decimal>(TaxCode, TaxAmount));

            //    }
            //}
            pax.TaxBreakup = taxBreakUp;
            Session["taxBreakUp"] = taxBreakUp;
            hdnPaxAdded.Value = "true";
            Session["itinerary"] = itinerary;
            txtPaxName.Text = string.Empty;
            txtSellingFare.Text = "0.00";
            //txtCode0.Text = string.Empty;
            //txtRemarks.Text = string.Empty;
            isAdded = true;
            hdnIsFromEdit.Value = "false";
            MakeDefaultValues();
            ddlSector1.Enabled = false;
            ddlSector2.Enabled = false;
            ddlSector3.Enabled = false;
            ddlSector4.Enabled = false;
            ddlSector5.Enabled = false;
            ddlSector6.Enabled = false;
            ddlSector7.Enabled = false;
            ddlSector8.Enabled = false;
            ddlSector9.Enabled = false;
            ddlSector10.Enabled = false;
            CT.TicketReceipt.Common.Utility.Alert(this.Page, "Pax Details Added");

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in btnAdd_Click()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
            string str = Request.Url.AbsoluteUri.Replace("#", "");
            string script = "alert('Failed To Add Pax Details :" + ex.Message + "'); window.location='" + str + "'";
            ScriptManager.RegisterStartupScript(this, GetType(),
            "ServerControlScript", script, true);
        }
        finally
        {
            hndTaxCodeValue.Value = "";
            Session["taxBreakUp"] = null;

        }
    }
    private void MakeDefaultValues()
    {
        txtPaxName.Text = string.Empty;

        txtSellingFare.Text = "0.00";
        //txtValue0.Text = "0.00";
        txtCommision.Text = "0.00";
        txtCommisionAmt.Text = "0.00";
        txtCommper.Text = "0.00";
        txtCommisionOn.Text = "0.00";

        txtDiscount.Text = "0.00";
        txtDiscountAmt.Text = "0.00";
        txtDiscountper.Text = "0.00";
        txtDiscountOn.Text = "0.00";

        txtMarkUp.Text = "0.00";
        txtAmount2.Text = "0.00";
        txtTransFee.Text = "0.00";
        txtAddlTransFee.Text = "0.00";

        txtAgentCom.Text = "0.00";
        txtAgentPLB.Text = "0.00";

        txtOurComm.Text = "0.00";
        txtOurPLB.Text = "0.00";
        txtOutPutVAT.Text = "0.00";
        TxtInputVAT.Text = "0.00";

        txtNetVAT.Text = "0.00";
        txtTotalFare.Text = "0.00";
        txtTax.Text = "0.00";
        txtCollected.Text = "0.00";
        txtNetpayable.Text = "0.00";
        txtProfit.Text = "0.00";
        txtcashAmount.Text = "0.00";
        hndTaxCounter.Value = "0";
        hndTaxCodeValue.Value = "";
        txtTypeCode.Text = "";
        Session["taxBreakUp"] = null;
        Utility.StartupScript(this.Page, "TaxCounterTozero()", "TaxCounterTozero");
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in btnClear_Click()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
        }
    }

    /// <summary>
    /// Saving the Complete itinerary object to database from Pop up in save button click 
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    private void SaveItinerary()
    {
        try
        {
            AgentMaster agent = new AgentMaster(Convert.ToInt32(ddlCustomer1.SelectedValue) != Settings.LoginInfo.AgentId ? Convert.ToInt32(ddlCustomer1.SelectedItem.Value) : Settings.LoginInfo.AgentId);

            if (Convert.ToInt32(ddlCustomer1.SelectedValue) != Settings.LoginInfo.AgentId)
            {
                Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(ddlCustomer1.SelectedItem.Value);
                AgentID = Settings.LoginInfo.OnBehalfAgentID;
                Settings.LoginInfo.IsOnBehalfOfAgent = true;
                Settings.LoginInfo.OnBehalfAgentCurrency = Settings.LoginInfo.Currency;
                Settings.LoginInfo.OnBehalfAgentDecimalValue = Settings.LoginInfo.DecimalValue;
            }

            itinerary = Bindpaxinfo();


            string flightNumbers = string.Empty;

            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-US");
            for (int i = 0; i < itinerary.Segments.Length; i++)
            {
                if (string.IsNullOrEmpty(flightNumbers))
                    flightNumbers += itinerary.Segments[i].FlightNumber;
                else
                    flightNumbers += "-" + itinerary.Segments[i].FlightNumber;
            }

            if (!string.IsNullOrEmpty(hdnprofileId.Value) && !string.IsNullOrEmpty(hdnTravelResonId.Value))
            {
                FlightPolicy policy = new FlightPolicy();
                policy.ProfileId = Convert.ToInt32(hdnprofileId.Value.Split('~')[0]);
                policy.ProfileGrade = hdnprofileId.Value.Split('~')[1];
                policy.TravelReasonId = Convert.ToInt32(hdnTravelResonId.Value);
                policy.CreatedBy = (int)Settings.LoginInfo.UserID;
                policy.TravelDate = Convert.ToDateTime(txtTravelDt.Text, dateFormat);
                policy.Status = "A";
                policy.FlightNumbers = flightNumbers;
                policy.PolicyBreakingRules = string.Empty;
                policy.ApprovalStatus = "P";
                itinerary.FlightPolicy = policy;
            }

            if (itinerary == null)
            {
                Utility.Alert(this.Page, " Please Add At least one Passenger Details. ");
                isFromClear = true;
                return;
            }

            decimal agentbalance = agent.UpdateBalance(0);
            //if (agentbalance <= 0 || agentbalance < itinerary.AgentPrice)
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Insufficient Funds.');window.location='OffLineEntry.aspx';", true);
            //    return;
            //}            
            using (TransactionScope updateTransaction = new TransactionScope())
            {
                try
                {
                    if (int.Parse(hdnFlightId.Value) == 0)
                    {
                        BookingResponse bookingResponse = new BookingResponse();
                        BookingDetail booking = new BookingDetail();
                        List<string> pmTicketNo = new List<string>();
                        string errorMsg = string.Empty;
                        //itinerary = (FlightItinerary)Session["itinerary"];


                        int TimeLimitToComplete = 7;
                        //itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
                        Airline airline = new Airline();
                        airline.Load(itinerary.Segments[0].Airline);

                        booking.Status = (BookingStatus)int.Parse(ddlTransactionType.SelectedItem.Value);
                        booking.AgencyId = (AgentID);
                        booking.CreatedBy = (int)Settings.LoginInfo.UserID;
                        BookingHistory bh = new BookingHistory();
                        string remarks = string.Empty;
                        bh.EventCategory = EventCategory.Booking;
                        string remark = string.Empty;
                        if (booking.Status == BookingStatus.Hold)
                        {
                            remarks = "Booking is on hold" + remark;
                        }
                        else if (booking.Status == BookingStatus.Cancelled)
                        {
                            remarks = "Booked seat are released";
                        }
                        else if (booking.Status == BookingStatus.Ticketed)
                        {
                            bh.EventCategory = EventCategory.Ticketing;
                            remarks = "Ticket is created";
                        }
                        else if (booking.Status == BookingStatus.Ready)
                        {
                            remarks = "Booking is in ready state";
                        }
                        else if (booking.Status == BookingStatus.Inactive)
                        {
                            remarks = "Booking is Inactive";
                        }
                        else if (booking.Status == BookingStatus.InProgress || booking.Status == BookingStatus.OutsidePurchase)
                        {
                            remarks = "Booking is in-progress";
                        }
                        else
                        {
                            bh.EventCategory = EventCategory.Miscellaneous;
                            remarks = booking.Status.ToString();
                        }
                        if (Request["REMOTE_ADDR"].Length > 0)
                        {
                            remarks += "(IP Address :- " + Request["REMOTE_ADDR"].ToString() + ")";
                        }
                        bh.Remarks = remarks;
                        bh.CreatedBy = (int)Settings.LoginInfo.UserID;
                        CT.Core.Queue queue = new CT.Core.Queue();
                        queue.QueueTypeId = (int)QueueType.Booking;
                        queue.StatusId = (int)QueueStatus.Assigned;
                        queue.AssignedTo = (int)Settings.LoginInfo.UserID;
                        queue.AssignedBy = (int)Settings.LoginInfo.UserID;
                        queue.AssignedDate = DateTime.UtcNow;
                        queue.CompletionDate = DateTime.UtcNow.AddDays(TimeLimitToComplete);
                        queue.CreatedBy = (int)Settings.LoginInfo.UserID;

                        Comment ssrComment = new Comment();
                        ssrComment.AgencyId = booking.AgencyId;
                        ssrComment.CommentDescription = bookingResponse.SSRMessage;
                        ssrComment.CommentTypes = CommmentType.Booking;

                        ssrComment.CreatedBy = 1;
                        ssrComment.IsPublic = true;
                        int txnRetryCount = Convert.ToInt32(CT.Configuration.ConfigurationSystem.BookingEngineConfig["txnRetryCount"]);

                        booking.Save(itinerary, false);
                        //Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(OffLineEntry)Booking Saved completed." , Request["REMOTE_ADDR"]);
                        if (!airline.IsLCC)
                        {
                            bh.BookingId = booking.BookingId;
                            bh.Save();
                            //  Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(OffLineEntry)Booking history Saved completed.", Request["REMOTE_ADDR"]);
                        }
                        if (bookingResponse.SSRDenied)
                        {
                            ssrComment.ParentId = booking.BookingId;
                            ssrComment.AddComment();
                        }
                        queue.ItemId = booking.BookingId;
                        queue.Save();
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(OffLineEntry)Queue Saved completed.", Request["REMOTE_ADDR"]);
                        SaveTicket();
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(OffLineEntry)Ticket Save completed.", Request["REMOTE_ADDR"]);
                    }
                    else
                    {
                        itinerary.BookingId = int.Parse(hdnBookingId.Value);
                        itinerary.AgencyId = AgentID;
                        itinerary.save();
                        SaveTicket();
                    }
                    updateTransaction.Complete();
                    updateTransaction.Dispose();
                    if (int.Parse(isEditable.Value.ToString()) != 1)
                    {
                        SaveInvoiceAndLedger();
                    }
                }
                catch (Exception e)
                {
                    updateTransaction.Dispose();
                }
            }
            hdnFlightId.Value = Utility.ToString(itinerary.FlightId);
            isEditable.Value = "1";
            btnUpdate.Visible = false;
            Settings.LoginInfo.IsOnBehalfOfAgent = false;
            AgentID = Settings.LoginInfo.AgentId;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in Saving Itinearary Object to DB" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
            throw ex;
        }
    }
    private void SaveInvoiceAndLedger()
    {
        try
        {
            if (itinerary.IsEdit != 1)
            {
                Invoice invoice = new Invoice();
                int invoiceNumber = Invoice.isInvoiceGenerated(Ticket.GetTicketList(itinerary.FlightId)[0].TicketId, ProductType.Flight);

                if (invoiceNumber > 0)
                {
                    invoice.Load(invoiceNumber);
                }
                else
                {
                    invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.FlightId, "", (int)Settings.LoginInfo.UserID, ProductType.Flight, 1);
                    if (invoiceNumber > 0)
                    {
                        invoice.Load(invoiceNumber);
                        invoice.Status = InvoiceStatus.Paid;
                        invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                        invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                        invoice.UpdateInvoice();
                    }
                }
                if (int.Parse(ddlTransactionType.SelectedItem.Value) == (int)BookingStatus.Refunded)
                {
                    SaveRefundLedger(itinerary, "", (int)Settings.LoginInfo.UserID);
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in Saving Invoice " + ex.Message, Request.ServerVariables["REMOTE_ADDR"]);
            throw ex;
        }
    }
    private void SaveRefundLedger(FlightItinerary itinerary, string remarks, int memberId)
    {
        LedgerTransaction ledgerTxn = new LedgerTransaction();
        NarrationBuilder objNarration = new NarrationBuilder();
        int invoiceNumber = 0;
        invoiceNumber = Invoice.isInvoiceGenerated(Ticket.GetTicketList(itinerary.FlightId)[0].TicketId, ProductType.Flight);
        Invoice invoice = new Invoice();
        invoice.Load(invoiceNumber);
        objNarration.DocNo = invoice.CompleteInvoiceNumber;
        BookingDetail bookingDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(itinerary.FlightId, ProductType.Flight));
        decimal bookingAmount = 0;
        decimal cancelAmount = 0;
        for (int i=0;i< itinerary.Passenger.Length;i++)
        {
            bookingAmount = bookingAmount+ Convert.ToDecimal(itinerary.Passenger[i].Price.PublishedFare) + 
                itinerary.Passenger[i].Price.Tax + itinerary.Passenger[i].Price.Markup + 
                itinerary.Passenger[i].Price.AsvAmount - itinerary.Passenger[i].Price.Discount -
                itinerary.Passenger[i].Price.TransactionFee - itinerary.Passenger[i].Price.AdditionalTxnFee + 
                itinerary.Passenger[i].Price.OutputVATAmount + itinerary.Passenger[i].Price.InputVATAmount;
            cancelAmount = Convert.ToDecimal(cancelAmount) + Convert.ToDecimal(itinerary.Passenger[i].Price.AgentCancellationCharge) + 
                Convert.ToDecimal(itinerary.Passenger[i].Price.SupplierCancellationCharge) + Convert.ToDecimal(itinerary.Passenger[i].Price.CancellationInVat) + 
                Convert.ToDecimal(itinerary.Passenger[i].Price.CancellationOutVat);
        }
        
        ledgerTxn = new LedgerTransaction();
        objNarration.DocNo = invoice.CompleteInvoiceNumber;
        ledgerTxn.LedgerId = bookingDetail.AgencyId;
        ledgerTxn.Amount = bookingAmount - (Convert.ToDecimal(cancelAmount));
        FlightPassenger FlightPax = new FlightPassenger();
        FlightPax.Load(itinerary.Passenger[0].PaxId);
        string paxData = FlightPax.FirstName + " " + FlightPax.LastName;
        objNarration.PaxName = paxData;

        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.TransferRefund;
        ledgerTxn.Notes = "Flight booking Refunded";
        objNarration.Remarks = "Refunded for PNR No -" + itinerary.PNR;


        ledgerTxn.Narration = objNarration;
        ledgerTxn.IsLCC = itinerary.IsLCC;
        ledgerTxn.ReferenceId = itinerary.FlightId;

        ledgerTxn.Date = DateTime.UtcNow;
        ledgerTxn.CreatedBy = (int)Settings.LoginInfo.UserID;
        ledgerTxn.TransType = itinerary.TransactionType;
        ledgerTxn.PaymentMode = 0;
        ledgerTxn.Save();
        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

        ledgerTxn = new LedgerTransaction();
        NarrationBuilder objNarration1 = new NarrationBuilder();
        objNarration1.DocNo = invoice.CompleteInvoiceNumber;
        ledgerTxn.LedgerId = bookingDetail.AgencyId;
        ledgerTxn.Amount = -(Convert.ToDecimal(cancelAmount));
        FlightPax.Load(itinerary.Passenger[0].PaxId);
        objNarration1.PaxName = paxData;
        ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.TransferRefund;
        ledgerTxn.Notes = "Flight booking Cancellation";
        objNarration1.Remarks = "Cancellation charge for PNR No -" + itinerary.PNR;
        ledgerTxn.Narration = objNarration1;
        ledgerTxn.IsLCC = itinerary.IsLCC;
        ledgerTxn.ReferenceId = itinerary.FlightId;
        ledgerTxn.Date = DateTime.UtcNow;
        ledgerTxn.CreatedBy = (int)Settings.LoginInfo.UserID;
        ledgerTxn.TransType = itinerary.TransactionType;
        ledgerTxn.PaymentMode = 0;
        ledgerTxn.Save();
        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

    }
    private void SaveTicket()
    {
        try
        {
            //ticketing region
            int priceID = 0;
            List<TicketAndHandlingEml> HandlingElementList = JsonConvert.DeserializeObject<List<TicketAndHandlingEml>>(hdnPaxDetails.Value);
            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                bool used = false;
                FlightPassenger pax = itinerary.Passenger[i];
                Ticket ticket = new Ticket();

                ticket.ConjunctionNumber = "";
                ticket.CorporateCode = "";
                ticket.CreatedBy = itinerary.CreatedBy;
                ticket.CreatedOn = DateTime.Now;
                ticket.Endorsement = "";
                ticket.ETicket = true;
                ticket.FareCalculation = "";
                ticket.FareRule = "";
                ticket.FlightId = itinerary.FlightId;
                ticket.IssueDate = DateTime.Now;
                ticket.LastModifiedBy = itinerary.CreatedBy;

                ticket.PaxFirstName = itinerary.Passenger[i].FirstName;
                ticket.PaxId = itinerary.Passenger[i].PaxId;
                ticket.PaxLastName = itinerary.Passenger[i].LastName;
                ticket.PaxType = itinerary.Passenger[i].Type;
                ticket.Price = itinerary.Passenger[i].Price;
                ticket.Status = "Ticketed";
                ticket.TicketAdvisory = "";
                ticket.TicketDesignator = "";
                if (HandlingElementList[i].TicketNumber == "")
                {
                    ticket.TicketNumber = "NA";
                }
                else
                {
                    ticket.TicketNumber = HandlingElementList[i].TicketNumber;
                }
                ticket.Remarks = txtRemarks.Text;

                ticket.OriginalIssue = string.Empty;
                ticket.IssueInExchange = string.Empty;
                if (TypeCode.SelectedValue == "Deal Code" || TypeCode.SelectedValue == "Tour Code")
                {
                    ticket.TourCode = txtTypeCode.Text;
                }
                else
                    ticket.CorporateCode = txtTypeCode.Text;
                if (string.IsNullOrEmpty(ticket.TourCode))
                    ticket.TourCode = "";

                if (ddlSector6.SelectedValue != "-1" && !string.IsNullOrEmpty(ddlSector6.SelectedValue))
                {
                    if (i < itinerary.Segments.Count())
                    {
                        if (!string.IsNullOrEmpty(itinerary.Segments[i].ConjunctionNo))
                        {
                            ticket.ConjunctionNumber = itinerary.Segments[i].ConjunctionNo;
                        }
                    }

                }

                ticket.ValidatingAriline = ddlairline.SelectedValue;
                ticket.TaxBreakup = itinerary.Passenger[i].TaxBreakup;

                //Ticket.GetTicketList(itinerary.FlightId);
                if (int.Parse(hdnFlightId.Value) > 0)
                {
                    ticket.UpdateTicketByPax();
                }
                else
                {
                    ticket.Save();
                }
                if (int.Parse(ddlTransactionType.SelectedItem.Value) == (int)BookingStatus.Refunded)// PAssing cancellation chanrge in servicetable
                {
                    decimal bookingAmount = Convert.ToDecimal(itinerary.Passenger[0].Price.PublishedFare) + itinerary.Passenger[0].Price.Tax + itinerary.Passenger[0].Price.Markup + itinerary.Passenger[0].Price.AsvAmount - itinerary.Passenger[0].Price.Discount - itinerary.Passenger[0].Price.TransactionFee - itinerary.Passenger[0].Price.AdditionalTxnFee + itinerary.Passenger[0].Price.OutputVATAmount;
                    CT.BookingEngine.CancellationCharges cancellationCharge = new CT.BookingEngine.CancellationCharges();
                    cancellationCharge.AdminFee = Convert.ToDecimal(txtAgentCancellationAmt.Text) + Convert.ToDecimal(hdnCancelOPVat.Value);
                    cancellationCharge.SupplierFee = Convert.ToDecimal(txtSupplierCancelationAmt.Text) + Convert.ToDecimal(hdnCancelIPVat.Value);
                    cancellationCharge.PaymentDetailId = 0;//pd.PaymentDetailId;
                    cancellationCharge.ReferenceId = ticket.TicketId;
                    cancellationCharge.CancelPenalty = bookingAmount;
                    cancellationCharge.CreatedBy = itinerary.CreatedBy;
                    cancellationCharge.ProductType = ProductType.Flight;
                    cancellationCharge.Save();
                }

                priceID = ticket.Price.PriceId;
                txtRemarks.Text = string.Empty;

                HandlingElement HandlingEml = new HandlingElement();
                HandlingEml.HandlingId = int.Parse(HandlingElementList[i].HandlingId);
                HandlingEml.ServiceId = int.Parse(ddlServiceType.SelectedValue);
                HandlingEml.ReferenceId = priceID;
                HandlingEml.Status = 1;
                HandlingEml.CreatedBy = (int)Settings.LoginInfo.UserID;
                HandlingEml.Amount = Convert.ToDecimal(itinerary.Passenger[i].Price.Markup) + Convert.ToDecimal(itinerary.Passenger[i].Price.AsvAmount);
                HandlingEml.AdultCount = int.Parse(ddlAdultCount.SelectedValue);
                HandlingEml.ChildCount = int.Parse(ddlChildCount.SelectedValue);
                HandlingEml.InfantCount = int.Parse(ddlInfantCount.SelectedValue);
                if (int.Parse(hdnFlightId.Value) > 0)
                {
                    HandlingEml.Update();
                }
                else
                {
                    HandlingEml.Save();
                }
            }
            if (!string.IsNullOrEmpty(hdnsettlementDetails.Value) && hdnsettlementDetails.Value.ToString()!="")
            {

                if (int.Parse(hdnFlightId.Value) > 0)
                {
                    SettlementDetailes settlementitem = new SettlementDetailes();
                    settlementitem.ServiceReferenceId = itinerary.FlightId;
                    settlementitem.Deactivate();
                }

                SettlementData settlement = JsonConvert.DeserializeObject<SettlementData>(hdnsettlementDetails.Value);
                foreach (SettlementDetailes settlementitem in settlement.SettlementObject)
                {
                    settlementitem.ServiceReferenceId = itinerary.FlightId;
                    settlementitem.CreatedBy = (int)Settings.LoginInfo.UserID;
                    settlementitem.UpdatedBy = (int)Settings.LoginInfo.UserID;
                    settlementitem.Status = 1;
                    //settlementitem.ServiceReferenceId = priceID;
                    settlementitem.Save();
                }
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in Saving Tacket " + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
            throw ex;
        }
    }

    /// <summary>
    /// Remove pax Detail from Pop Up 
    /// </summary>
    /// <param name="Index"></param>
    private void RemovePaxFromItinerary(int Index)
    {
        int adultCount = 0;
        try
        {
            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                if (itinerary.Passenger[i].Type.ToString() == "Adult")
                {
                    adultCount++;

                }
                if (i != Index)
                {
                    PassengerList.Add(itinerary.Passenger[i]);
                }
            }
            if (adultCount == 1)
            {
                //itinerary.Passenger[i] = null;
                Clear();
                hdnSelectedTabName.Value = "";
            }
            itinerary.Passenger = PassengerList.ToArray();
            hdnRemovePax.Value = null;
            if (itinerary != null && itinerary.Passenger != null && itinerary.Passenger.Count() > 0)
            {
                Utility.StartupScript(this.Page, "showpopup()", "showpopup");
            }
            else
            {
                Session["itinerary"] = null;
                Session["TaxBreakup"] = null;
                txtRemarks.Text = "";
                txtTypeCode.Text = "";
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in RemovePaxFromItinerary()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
        }
    }
    private void Clear()
    {
        try
        {
            ddlairline.SelectedIndex = -1;
            txtPNRNum.Text = string.Empty;
            txtNumber.Text = string.Empty;
            ddlRepAirline.SelectedIndex = -1;
            txtSalesDate.Text = string.Empty;
            txtTravelDt.Text = string.Empty;
            ddlAgent.SelectedIndex = -1;
            ddlSector1.SelectedIndex = -1;
            ddldestination.SelectedIndex = -1;
            ddlCustomer1.SelectedIndex = -1;
            TypeCode.SelectedIndex = -1;
            ddlSector2.SelectedIndex = -1;
            ddlSector3.SelectedIndex = -1;
            ddlSector4.SelectedIndex = -1;
            ddlSector5.SelectedIndex = -1;
            txtPaxName.Text = string.Empty;
            txtSellingFare.Text = string.Empty;
            ddlConsultant.SelectedIndex = -1;
            txtRemarks.Text = string.Empty;
            ddlType.Enabled = true;
            ddlSector6.SelectedIndex = -1;
            ddlSector7.SelectedIndex = -1;
            ddlSector8.SelectedIndex = -1;
            ddlSector9.SelectedIndex = -1;
            ddlSector10.SelectedIndex = -1;
            txtCongTicketNo.Text = string.Empty;
            ddlType.SelectedIndex = -1;
            //txtValue0.Text = "0.00";
            txtCommision.Text = "0.00";
            txtCommisionAmt.Text = "0.00";
            txtCommper.Text = "0.00";
            txtCommisionOn.Text = "0.00";

            txtDiscount.Text = "0.00";
            txtDiscountAmt.Text = "0.00";
            txtDiscountper.Text = "0.00";
            txtDiscountOn.Text = "0.00";

            txtMarkUp.Text = "0.00";
            txtAmount2.Text = "0.00";
            txtTransFee.Text = "0.00";
            txtAddlTransFee.Text = "0.00";

            txtTypeCode.Text = "";

            txtAgentCom.Text = "0.00";
            txtAgentPLB.Text = "0.00";

            txtOurComm.Text = "0.00";
            txtOurPLB.Text = "0.00";
            txtOutPutVAT.Text = "0.00";
            TxtInputVAT.Text = "0.00";

            txtNetVAT.Text = "0.00";
            txtTotalFare.Text = "0.00";
            txtTax.Text = "0.00";
            txtCollected.Text = "0.00";
            txtNetpayable.Text = "0.00";
            txtProfit.Text = "0.00";
            txtSellingFare.Text = "0.00";

            txtcashAmount.Text = "0.00";
            txtClassSector.Text = string.Empty;
            //txtCode0.Text = string.Empty;
            btnUpdate.Visible = false;
            //btnAdd.Visible = true;
            txtConjPNR.Text = string.Empty;
            btnSave.Visible = false;
            hndTaxCounter.Value = "0";
            hndTaxCodeValue.Value = "";
            hdnPaxAdded.Value = "false";
            Session["itinerary"] = null;
            Session["TaxBreakup"] = null;
            Session["sectorCount"] = null;
            //Session["BookingSource"] = null;
            hdnprofileId.Value = "";
            hdnTravelResonId.Value = "";
            int SectCount = Convert.ToInt32(hndsegmentsCount.Value);
            hndsegmentsCount.Value = "0";
            Utility.StartupScript(this.Page, "ClearSectorTable('" + SectCount + "')", "ClearSectorTable");

            for (int i = 0; i < itinerary.Passenger.Length; i++)
            {
                PassengerList.Remove(itinerary.Passenger[i]);
            }
            itinerary.Passenger = PassengerList.ToArray();

            //string str = Request.Url.AbsoluteUri.Replace("#", "");
            //string script = "window.location='" + str + "'";
            //ScriptManager.RegisterStartupScript(this, GetType(),
            //"ServerControlScript", script, true);
            hndsegmentsCount.Value = "0";
            isFromClear = true;
            Utility.StartupScript(this.Page, "SetActiveTab();", "SetActiveTab");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #region Binding Sectors
    private void bindSectorsDropdown()
    {
        DataTable dt = Airport.GetAirportList();
        BindDropdownvalues(ddldestination, dt, string.Empty);

        //BindDropdownvalues(ddlSector1, dt, string.Empty);
        //BindDropdownvalues(ddlSector2, dt, string.Empty);
        //BindDropdownvalues(ddlSector3, dt, string.Empty);
        //BindDropdownvalues(ddlSector4, dt, string.Empty);
        //BindDropdownvalues(ddlSector5, dt, string.Empty);

        //Session["sectorsList"] = dt;

        //ddldestination.DataSource = dt;
        //ddldestination.DataTextField = "AirportCode";
        //ddldestination.DataValueField = "AirportCode";
        //ddldestination.DataBind();
        //ddldestination.Items.Insert(0, new ListItem("Select", "-1"));

    }
    //protected void ddlSector2_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Utility.StartupScript(this.Page, "TaxCounterTozero()", "TaxCounterTozero");
    //    if (ddlSector1.SelectedValue == "-1")
    //    {
    //        ddlSector2.SelectedValue = "-1";
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Please Select Previous Selector");
    //    }
    //    if (ddlSector2.SelectedValue == ddlSector1.SelectedValue)
    //    {
    //        ddlSector2.SelectedValue = "-1";
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Sector can't be Duplicated");
    //    }
    //    else
    //    {
    //        ddlFromSector1.DataSource = Session["sectorsList"];
    //        ddlFromSector1.DataTextField = "AirportCode";
    //        ddlFromSector1.DataValueField = "AirportCode";
    //        ddlFromSector1.DataBind();
    //        ddlFromSector1.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlFromSector1.SelectedValue = ddlSector1.SelectedValue;

    //        ddlToSector1.DataSource = Session["sectorsList"];
    //        ddlToSector1.DataTextField = "AirportCode";
    //        ddlToSector1.DataValueField = "AirportCode";
    //        ddlToSector1.DataBind();
    //        ddlToSector1.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlToSector1.SelectedValue = ddlSector2.SelectedValue;
    //        if (ddlFromSector2.SelectedIndex > 0)
    //        {
    //            ddlFromSector2.SelectedValue = ddlSector2.SelectedValue;
    //        }
    //    }

    //}
    //protected void ddlSector3_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Utility.StartupScript(this.Page, "TaxCounterTozero()", "TaxCounterTozero");
    //    if (ddlSector2.SelectedValue == "-1")
    //    {
    //        ddlSector3.SelectedValue = "-1";
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Please Select Previous Selector");
    //    }
    //    if ((ddlSector3.SelectedValue == ddlSector1.SelectedValue) || (ddlSector3.SelectedValue == ddlSector2.SelectedValue))
    //    {
    //        ddlSector3.SelectedValue = "-1";
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Sector can't be Duplicated");
    //    }
    //    else
    //    {
    //        ddlFromSector2.DataSource = Session["sectorsList"];
    //        ddlFromSector2.DataTextField = "AirportCode";
    //        ddlFromSector2.DataValueField = "AirportCode";
    //        ddlFromSector2.DataBind();
    //        ddlFromSector2.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlFromSector2.SelectedValue = ddlSector2.SelectedValue;

    //        ddlToSector2.DataSource = Session["sectorsList"];
    //        ddlToSector2.DataTextField = "AirportCode";
    //        ddlToSector2.DataValueField = "AirportCode";
    //        ddlToSector2.DataBind();
    //        ddlToSector2.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlToSector2.SelectedValue = ddlSector3.SelectedValue;
    //        if(ddlFromSector3.SelectedIndex>0)
    //        {
    //            ddlFromSector3.SelectedValue = ddlSector3.SelectedValue;
    //        }
    //    }

    //}
    //protected void ddlSector4_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Utility.StartupScript(this.Page, "TaxCounterTozero()", "TaxCounterTozero");
    //    if (ddlSector3.SelectedValue == "-1")
    //    {
    //        ddlSector4.SelectedValue = "-1";
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Please Select Previous Selector");
    //    }
    //    if ((ddlSector4.SelectedValue == ddlSector1.SelectedValue) || (ddlSector4.SelectedValue == ddlSector2.SelectedValue) || (ddlSector4.SelectedValue == ddlSector3.SelectedValue))
    //    {
    //        ddlSector4.SelectedValue = "-1";
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Sector can't be Duplicated");
    //    }
    //    else
    //    {
    //        ddlFromSector3.DataSource = Session["sectorsList"];
    //        ddlFromSector3.DataTextField = "AirportCode";
    //        ddlFromSector3.DataValueField = "AirportCode";
    //        ddlFromSector3.DataBind();
    //        ddlFromSector3.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlFromSector3.SelectedValue = ddlSector3.SelectedValue;

    //        ddlToSector3.DataSource = Session["sectorsList"];
    //        ddlToSector3.DataTextField = "AirportCode";
    //        ddlToSector3.DataValueField = "AirportCode";
    //        ddlToSector3.DataBind();
    //        ddlToSector3.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlToSector3.SelectedValue = ddlSector4.SelectedValue;
    //        if (ddlFromSector4.SelectedIndex > 0)
    //        {
    //            ddlFromSector4.SelectedValue = ddlSector4.SelectedValue;
    //        }
    //    }
    //}
    //protected void ddlSector5_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (ddlSector6.Items.Count == 0)
    //    {


    //        ddlSector6.DataSource = Session["sectorsList"];
    //        ddlSector6.DataTextField = "AirportCode";
    //        ddlSector6.DataValueField = "AirportCode";
    //        ddlSector6.DataBind();
    //        ddlSector6.Items.Insert(0, new ListItem("Select", "-1"));


    //        ddlSector7.DataSource = Session["sectorsList"];
    //        ddlSector7.DataTextField = "AirportCode";
    //        ddlSector7.DataValueField = "AirportCode";
    //        ddlSector7.DataBind();
    //        ddlSector7.Items.Insert(0, new ListItem("Select", "-1"));

    //        ddlSector8.DataSource = Session["sectorsList"];
    //        ddlSector8.DataTextField = "AirportCode";
    //        ddlSector8.DataValueField = "AirportCode";
    //        ddlSector8.DataBind();
    //        ddlSector8.Items.Insert(0, new ListItem("Select", "-1"));

    //        ddlSector9.DataSource = Session["sectorsList"];
    //        ddlSector9.DataTextField = "AirportCode";
    //        ddlSector9.DataValueField = "AirportCode";
    //        ddlSector9.DataBind();
    //        ddlSector9.Items.Insert(0, new ListItem("Select", "-1"));

    //        ddlSector10.DataSource = Session["sectorsList"];
    //        ddlSector10.DataTextField = "AirportCode";
    //        ddlSector10.DataValueField = "AirportCode";
    //        ddlSector10.DataBind();
    //        ddlSector10.Items.Insert(0, new ListItem("Select", "-1"));
    //    }

    //    Utility.StartupScript(this.Page, "TaxCounterTozero()", "TaxCounterTozero");
    //    if (ddlSector1.SelectedValue == "-1" || ddlSector2.SelectedValue == "-1" || ddlSector3.SelectedValue == "-1" || ddlSector4.SelectedValue == "-1")
    //    {
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Please Select Previous Selector");
    //    }
    //    if ((ddlSector5.SelectedValue == ddlSector1.SelectedValue) || (ddlSector5.SelectedValue == ddlSector2.SelectedValue) || (ddlSector5.SelectedValue == ddlSector3.SelectedValue) || (ddlSector5.SelectedValue == ddlSector4.SelectedValue))
    //    {
    //        ddlSector5.SelectedValue = "-1";
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Sector can't be Duplicated");
    //    }
    //    else
    //    {
    //        ddlFromSector4.DataSource = Session["sectorsList"];
    //        ddlFromSector4.DataTextField = "AirportCode";
    //        ddlFromSector4.DataValueField = "AirportCode";
    //        ddlFromSector4.DataBind();
    //        ddlFromSector4.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlFromSector4.SelectedValue = ddlSector4.SelectedValue;

    //        ddlToSector4.DataSource = Session["sectorsList"];
    //        ddlToSector4.DataTextField = "AirportCode";
    //        ddlToSector4.DataValueField = "AirportCode";
    //        ddlToSector4.DataBind();
    //        ddlToSector4.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlToSector4.SelectedValue = ddlSector5.SelectedValue;
    //        if (ddlFromSector5.SelectedIndex > 0)
    //        {
    //            ddlFromSector5.SelectedValue = ddlSector5.SelectedValue;
    //        }
    //    }

    //}
    //protected void ddlSector6_SelectedIndexChanged(object sender, EventArgs e)
    //{

    //    Utility.StartupScript(this.Page, "TaxCounterTozero()", "TaxCounterTozero");
    //    if (ddlSector1.SelectedValue == "-1" || ddlSector2.SelectedValue == "-1" || ddlSector3.SelectedValue == "-1" || ddlSector4.SelectedValue == "-1" || ddlSector5.SelectedValue == "-1")
    //    {
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Please Select Previous Selector");
    //    }
    //    if ((ddlSector6.SelectedValue == ddlSector1.SelectedValue) || (ddlSector6.SelectedValue == ddlSector2.SelectedValue) || (ddlSector6.SelectedValue == ddlSector3.SelectedValue) || (ddlSector6.SelectedValue == ddlSector4.SelectedValue) || (ddlSector6.SelectedValue == ddlSector5.SelectedValue))
    //    {
    //        ddlSector6.SelectedValue = "-1";
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Sector can't be Duplicated");
    //    }
    //    else
    //    {
    //        ddlFromSector5.DataSource = Session["sectorsList"];
    //        ddlFromSector5.DataTextField = "AirportCode";
    //        ddlFromSector5.DataValueField = "AirportCode";
    //        ddlFromSector5.DataBind();
    //        ddlFromSector5.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlFromSector5.SelectedValue = ddlSector5.SelectedValue;

    //        ddlToSector5.DataSource = Session["sectorsList"];
    //        ddlToSector5.DataTextField = "AirportCode";
    //        ddlToSector5.DataValueField = "AirportCode";
    //        ddlToSector5.DataBind();
    //        ddlToSector5.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlToSector5.SelectedValue = ddlSector6.SelectedValue;
    //        if (ddlFromSector6.SelectedIndex > 0)
    //        {
    //            ddlFromSector6.SelectedValue = ddlSector6.SelectedValue;
    //        }
    //    }
    //}
    //protected void ddlSector7_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Utility.StartupScript(this.Page, "TaxCounterTozero()", "TaxCounterTozero");
    //    if (ddlSector1.SelectedValue == "-1" || ddlSector2.SelectedValue == "-1" || ddlSector3.SelectedValue == "-1" || ddlSector4.SelectedValue == "-1" || ddlSector5.SelectedValue == "-1" || ddlSector6.SelectedValue == "-1")
    //    {
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Please Select Previous Selector");
    //    }
    //    if ((ddlSector7.SelectedValue == ddlSector1.SelectedValue) || (ddlSector7.SelectedValue == ddlSector2.SelectedValue) || (ddlSector7.SelectedValue == ddlSector3.SelectedValue) || (ddlSector7.SelectedValue == ddlSector4.SelectedValue) || (ddlSector7.SelectedValue == ddlSector5.SelectedValue) || (ddlSector7.SelectedValue == ddlSector6.SelectedValue))
    //    {
    //        ddlSector7.SelectedValue = "-1";
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Sector can't be Duplicated");
    //    }
    //    else
    //    {
    //        ddlFromSector6.DataSource = Session["sectorsList"];
    //        ddlFromSector6.DataTextField = "AirportCode";
    //        ddlFromSector6.DataValueField = "AirportCode";
    //        ddlFromSector6.DataBind();
    //        ddlFromSector6.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlFromSector6.SelectedValue = ddlSector6.SelectedValue;

    //        ddlToSector6.DataSource = Session["sectorsList"];
    //        ddlToSector6.DataTextField = "AirportCode";
    //        ddlToSector6.DataValueField = "AirportCode";
    //        ddlToSector6.DataBind();
    //        ddlToSector6.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlToSector6.SelectedValue = ddlSector7.SelectedValue;
    //        if (ddlFromSector7.SelectedIndex > 0)
    //        {
    //            ddlFromSector7.SelectedValue = ddlSector7.SelectedValue;
    //        }
    //    }
    //}
    //protected void ddlSector8_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Utility.StartupScript(this.Page, "TaxCounterTozero()", "TaxCounterTozero");
    //    if (ddlSector1.SelectedValue == "-1" || ddlSector2.SelectedValue == "-1" || ddlSector3.SelectedValue == "-1" || ddlSector4.SelectedValue == "-1" || ddlSector5.SelectedValue == "-1" || ddlSector6.SelectedValue == "-1" || ddlSector7.SelectedValue == "-1")
    //    {
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Please Select Previous Selector");
    //    }
    //    if ((ddlSector8.SelectedValue == ddlSector1.SelectedValue) || (ddlSector8.SelectedValue == ddlSector2.SelectedValue) || (ddlSector8.SelectedValue == ddlSector3.SelectedValue) || (ddlSector8.SelectedValue == ddlSector4.SelectedValue) || (ddlSector8.SelectedValue == ddlSector5.SelectedValue) || (ddlSector8.SelectedValue == ddlSector6.SelectedValue) || (ddlSector8.SelectedValue == ddlSector7.SelectedValue))
    //    {
    //        ddlSector8.SelectedValue = "-1";
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Sector can't be Duplicated");
    //    }
    //    else
    //    {
    //        ddlFromSector7.DataSource = Session["sectorsList"];
    //        ddlFromSector7.DataTextField = "AirportCode";
    //        ddlFromSector7.DataValueField = "AirportCode";
    //        ddlFromSector7.DataBind();
    //        ddlFromSector7.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlFromSector7.SelectedValue = ddlSector7.SelectedValue;

    //        ddlToSector7.DataSource = Session["sectorsList"];
    //        ddlToSector7.DataTextField = "AirportCode";
    //        ddlToSector7.DataValueField = "AirportCode";
    //        ddlToSector7.DataBind();
    //        ddlToSector7.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlToSector7.SelectedValue = ddlSector8.SelectedValue;
    //        if (ddlFromSector8.SelectedIndex > 0)
    //        {
    //            ddlFromSector8.SelectedValue = ddlSector8.SelectedValue;
    //        }
    //    }
    //}
    //protected void ddlSector9_SelectedIndexChanged(object sender, EventArgs e)
    //{

    //    Utility.StartupScript(this.Page, "TaxCounterTozero()", "TaxCounterTozero");
    //    if (ddlSector1.SelectedValue == "-1" || ddlSector2.SelectedValue == "-1" || ddlSector3.SelectedValue == "-1" || ddlSector4.SelectedValue == "-1" || ddlSector5.SelectedValue == "-1" || ddlSector6.SelectedValue == "-1" || ddlSector7.SelectedValue == "-1" || ddlSector8.SelectedValue == "-1")
    //    {
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Please Select Previous Selector");
    //    }
    //    if ((ddlSector9.SelectedValue == ddlSector1.SelectedValue) || (ddlSector9.SelectedValue == ddlSector2.SelectedValue) || (ddlSector9.SelectedValue == ddlSector3.SelectedValue) || (ddlSector9.SelectedValue == ddlSector4.SelectedValue) || (ddlSector9.SelectedValue == ddlSector5.SelectedValue) || (ddlSector9.SelectedValue == ddlSector6.SelectedValue) || (ddlSector9.SelectedValue == ddlSector7.SelectedValue) || (ddlSector9.SelectedValue == ddlSector8.SelectedValue))
    //    {
    //        ddlSector9.SelectedValue = "-1";
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Sector can't be Duplicated");
    //    }
    //    else
    //    {
    //        ddlFromSector8.DataSource = Session["sectorsList"];
    //        ddlFromSector8.DataTextField = "AirportCode";
    //        ddlFromSector8.DataValueField = "AirportCode";
    //        ddlFromSector8.DataBind();
    //        ddlFromSector8.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlFromSector8.SelectedValue = ddlSector8.SelectedValue;

    //        ddlToSector8.DataSource = Session["sectorsList"];
    //        ddlToSector8.DataTextField = "AirportCode";
    //        ddlToSector8.DataValueField = "AirportCode";
    //        ddlToSector8.DataBind();
    //        ddlToSector8.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlToSector8.SelectedValue = ddlSector9.SelectedValue;
    //        if (ddlFromSector9.SelectedIndex > 0)
    //        {
    //            ddlFromSector9.SelectedValue = ddlSector9.SelectedValue;
    //        }
    //    }

    //}
    //protected void ddlSector10_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    Utility.StartupScript(this.Page, "TaxCounterTozero()", "TaxCounterTozero");
    //    if (ddlSector1.SelectedValue == "-1" || ddlSector2.SelectedValue == "-1" || ddlSector3.SelectedValue == "-1" || ddlSector4.SelectedValue == "-1" || ddlSector5.SelectedValue == "-1" || ddlSector6.SelectedValue == "-1" || ddlSector7.SelectedValue == "-1" || ddlSector8.SelectedValue == "-1" || ddlSector9.SelectedValue == "-1")
    //    {
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Please Select Previous Selector");
    //    }
    //    if ((ddlSector10.SelectedValue == ddlSector1.SelectedValue) || (ddlSector10.SelectedValue == ddlSector2.SelectedValue) || (ddlSector10.SelectedValue == ddlSector3.SelectedValue) || (ddlSector10.SelectedValue == ddlSector4.SelectedValue) || (ddlSector10.SelectedValue == ddlSector5.SelectedValue) || (ddlSector10.SelectedValue == ddlSector6.SelectedValue) || (ddlSector10.SelectedValue == ddlSector7.SelectedValue) || (ddlSector10.SelectedValue == ddlSector8.SelectedValue) || (ddlSector10.SelectedValue == ddlSector9.SelectedValue))
    //    {
    //        ddlSector10.SelectedValue = "-1";
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Sector can't be Duplicated");
    //    }
    //    else
    //    {
    //        ddlFromSector9.DataSource = Session["sectorsList"];
    //        ddlFromSector9.DataTextField = "AirportCode";
    //        ddlFromSector9.DataValueField = "AirportCode";
    //        ddlFromSector9.DataBind();
    //        ddlFromSector9.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlFromSector9.SelectedValue = ddlSector9.SelectedValue;

    //        ddlToSector9.DataSource = Session["sectorsList"];
    //        ddlToSector9.DataTextField = "AirportCode";
    //        ddlToSector9.DataValueField = "AirportCode";
    //        ddlToSector9.DataBind();
    //        ddlToSector9.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlToSector9.SelectedValue = ddlSector10.SelectedValue;
    //        if (ddlFromSector10.SelectedIndex > 0)
    //        {
    //            ddlFromSector10.SelectedValue = ddlSector10.SelectedValue;
    //        }
    //    }
    //}
    //protected void ddldestination_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (ddlSector1.Items.Count == 0)
    //    {
    //        ddlSector1.DataSource = Session["sectorsList"];
    //        ddlSector1.DataTextField = "AirportCode";
    //        ddlSector1.DataValueField = "AirportCode";
    //        ddlSector1.DataBind();
    //        ddlSector1.Items.Insert(0, new ListItem("Select", "-1"));

    //        ddlSector2.DataSource = Session["sectorsList"];
    //        ddlSector2.DataTextField = "AirportCode";
    //        ddlSector2.DataValueField = "AirportCode";
    //        ddlSector2.DataBind();
    //        ddlSector2.Items.Insert(0, new ListItem("Select", "-1"));

    //        ddlSector3.DataSource = Session["sectorsList"];
    //        ddlSector3.DataTextField = "AirportCode";
    //        ddlSector3.DataValueField = "AirportCode";
    //        ddlSector3.DataBind();
    //        ddlSector3.Items.Insert(0, new ListItem("Select", "-1"));

    //        ddlSector4.DataSource = Session["sectorsList"];
    //        ddlSector4.DataTextField = "AirportCode";
    //        ddlSector4.DataValueField = "AirportCode";
    //        ddlSector4.DataBind();
    //        ddlSector4.Items.Insert(0, new ListItem("Select", "-1"));

    //        ddlSector5.DataSource = Session["sectorsList"];
    //        ddlSector5.DataTextField = "AirportCode";
    //        ddlSector5.DataValueField = "AirportCode";
    //        ddlSector5.DataBind();
    //        ddlSector5.Items.Insert(0, new ListItem("Select", "-1"));
    //    }
    //    Utility.StartupScript(this.Page, "TaxCounterTozero()", "TaxCounterTozero");
    //}
    //protected void ddlSector1_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (ddlSector1.SelectedValue == ddldestination.SelectedValue)
    //    {
    //        CT.TicketReceipt.Common.Utility.Alert(this.Page, "Origin & Destination Should not be Same.");
    //        ddlSector1.SelectedValue = "-1";
    //    }
    //    else
    //    {
    //        ddlFromSector1.DataSource = Session["sectorsList"];
    //        ddlFromSector1.DataTextField = "AirportCode";
    //        ddlFromSector1.DataValueField = "AirportCode";
    //        ddlFromSector1.DataBind();
    //        ddlFromSector1.Items.Insert(0, new ListItem("Select", "-1"));
    //        ddlFromSector1.SelectedValue = ddlSector1.SelectedValue;
    //    }


    //    Utility.StartupScript(this.Page, "TaxCounterTozero()", "TaxCounterTozero");
    //}

    #endregion




    protected void btnRetrieve_Click(object sender, EventArgs e)
    {
        try
        {
            //int agentID = 0;
            int locationID = 0;
            int decimalPoint = 2;

            //int agentId = (rbtnAgent.Checked == true ? Convert.ToInt32(ddlAgent.SelectedValue) : Settings.LoginInfo.AgentId);
            if (rbtnAgent.Checked)
            {

                Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                AgentID = Settings.LoginInfo.OnBehalfAgentID;
                Settings.LoginInfo.IsOnBehalfOfAgent = true;
                //Settings.LoginInfo.OnBehalfAgentLocation = Convert.ToInt32(ddlConsultant.SelectedValue);

                AgentMaster agent = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                Settings.LoginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
                Settings.LoginInfo.OnBehalfAgentDecimalValue = agent.DecimalValue;
                StaticData sd = new StaticData();
                sd.BaseCurrency = agent.AgentCurrency;
                //Settings.LoginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;               
                //locationID = Convert.ToInt32(ddlConsultant.SelectedValue);
                decimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                decimal agentbalance = agent.UpdateBalance(0);
                lblAgentBalance.Text = "(" + agent.AgentCurrency + " " + agentbalance + ")";
                lblAgentBalance.Style.Add("display", "block");
                //spnLocaations.Style.Add("display", "inline");
                agentDecimalPoints = agent.DecimalValue;
                ddlcurrencycode.Items.Add(new ListItem(agent.AgentCurrency, agent.AgentCurrency));
                ddlcurrencycode.SelectedValue = agent.AgentCurrency;
                hdnCurrencyCode.Value = agent.AgentCurrency;
                BindAgentCode();
                ddlCustomer1.Enabled = false;
                string agentType = string.Empty;
                BindLocations(agent.AgentType, AgentID);
                //ddlMode.SelectedValue = "3";
                ddlMode.Enabled = false;

            }
            else
            {
                AgentID = Settings.LoginInfo.AgentId;
                Settings.LoginInfo.IsOnBehalfOfAgent = false;
                locationID = Convert.ToInt32(Settings.LoginInfo.LocationID);
                decimalPoint = Settings.LoginInfo.DecimalValue;
                agentDecimalPoints = Settings.LoginInfo.DecimalValue;
                ddlcurrencycode.Items.Add(new ListItem(Settings.LoginInfo.Currency, Settings.LoginInfo.Currency));
                ddlcurrencycode.SelectedValue = Settings.LoginInfo.Currency;
                hdnCurrencyCode.Value = Settings.LoginInfo.Currency;
                BindAgentCode();
                //ddlMode.SelectedValue = "3";
                ddlMode.Enabled = true;
                BindLocations(Convert.ToInt32(Settings.LoginInfo.AgentType), AgentID);
                ddlConsultant.SelectedValue = Settings.LoginInfo.LocationID.ToString();
                ddlConsultant.Items.Add(new ListItem("select", "-1"));
            }
            if (ddlSource.SelectedItem.Text == "OffLine")
            {
                EditPNRDiv.Style.Add("display", "block");
                ddlType.SelectedIndex = 0;
                ddlPaxTitle.Items.Clear();
                ddlPaxTitle.Items.Add(new ListItem("Mr.", "Mr."));
                ddlPaxTitle.Items.Add(new ListItem("Ms.", "Ms."));
                ddlPaxTitle.Items.Add(new ListItem("Dr.", "Dr."));
                ddlPaxTitle.Items.Add(new ListItem("Master", "MSTR"));
                Utility.StartupScript(this.Page, "HideSearchOption()", "HideSearchOption");
            }
            //FillDummyvalues();
            Session["sectorCount"] = null;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in btnRetrieve_Click()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
        }
    }

    private void DisplayAgentBalance()
    {
        ((Label)this.Master.FindControl("lblOBAgentBalance")).Visible = false;
        ((Label)this.Master.FindControl("lblOBLocation")).Visible = false;
        ((Label)this.Master.FindControl("lblOBUserName")).Visible = false;

        if (rbtnAgent.Checked)
        {
            Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            AgentMaster agent = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
            decimal agentbalance = agent.UpdateBalance(0);
            lblAgentBalance.Text = "(" + agent.AgentCurrency + " " + agentbalance.ToString("N" + agent.DecimalValue) + ")";
            lblAgentBalance.Style.Add("display", "block");
            ScriptManager.RegisterStartupScript(this, GetType(), "alert", "disablefield();", true);
        }
    }

    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        DisplayAgentBalance();
    }

    private void BindDropdownvalues(DropDownList ddllist, DataTable dtdata, string defaultVal)
    {
        try
        {
            ddllist.DataSource = dtdata;
            ddllist.DataTextField = "AirportCode";
            ddllist.DataValueField = "AirportCode";
            ddllist.DataBind();
            if (!string.IsNullOrEmpty(defaultVal))
            {
                ddllist.SelectedValue = defaultVal;
            }
            ddldestination.Items.Insert(0, new ListItem("Select", "-1"));
        }
        catch (Exception)
        {
            throw;
        }
    }

    public class TicketAndHandlingEml
    {
        public string HandlingId { get; set; }
        public string TicketNumber { get; set; }
    }
    [WebMethod]
    public static string CheckAgentBalance(string sAgentId)
    {
        try
        {
            AgentMaster agent = new AgentMaster(string.IsNullOrEmpty(sAgentId) || sAgentId == "-1" || sAgentId == "0" ? AgentID : Convert.ToInt32(sAgentId));
            decimal agentbalance = agent.UpdateBalance(0);
            return agent.AgentCurrency + '|' + Convert.ToString(agentbalance);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private FlightItinerary Bindpaxinfo()
    {
        try
        {

            //if (Convert.ToDecimal(txtcashAmount.Text) <= 0)
            //{
            //    Utility.Alert(this.Page, "Cash Amount should not be Zero/negative");
            //    isFromClear = true;
            //    return;
            //}
            //int agentID = (rbtnAgent.Checked == true ? Convert.ToInt32(ddlAgent.SelectedValue) : Settings.LoginInfo.AgentId);
            //AgentMaster agent = new AgentMaster(agentID);
            //decimal agentbalance = agent.UpdateBalance(0);
            //if (agentbalance <= 0 || agentbalance < Convert.ToDecimal(txtcashAmount.Text))
            //{
            //    ScriptManager.RegisterStartupScript(this, this.GetType(), "alert", "alert('Insufficient Funds.');window.location='OffLineEntry.aspx';", true);
            //    return;
            //}

            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-US");
            List<FareRule> fareRule = new List<FareRule>();
            itinerary.FlightId =int.Parse(hdnFlightId.Value);
            itinerary.FareRules = fareRule;
            itinerary.PNR = txtPNRNum.Text.ToUpper();
            itinerary.AirlineCode = ddlRepAirline.SelectedValue; // exp EK
            itinerary.ValidatingAirlineCode = ddlairline.SelectedValue; // example 11
                       
            itinerary.Destination = ddldestination.SelectedValue;
            itinerary.FareType = "PUB";
            itinerary.FlightBookingSource = BookingSource.OffLine;
            itinerary.LastTicketDate = Convert.ToDateTime(txtSalesDate.Text, dateFormat);
            itinerary.TicketAdvisory = string.Empty;
            itinerary.LastModifiedBy = (int)Settings.LoginInfo.UserID;
            itinerary.TravelDate = Convert.ToDateTime(txtTravelDt.Text, dateFormat);
            itinerary.CreatedOn = Convert.ToDateTime(txtSalesDate.Text, dateFormat);
            itinerary.SupplierCode = (ddlSupplier.Text == "-1" ? "" : ddlSupplier.Text);
            itinerary.LocationId = Convert.ToInt32(hndLocationId.Value);
            itinerary.LocationName = locationName;
            if (!string.IsNullOrEmpty(ddlMode.SelectedValue))
            {
                if (Convert.ToInt32(ddlMode.SelectedValue) == 1)
                {
                    itinerary.PaymentMode = ModeOfPayment.Cash;
                    lblCardType.Text = "Cash Amount";
                }
                if (Convert.ToInt32(ddlMode.SelectedValue) == 2)
                {
                    itinerary.PaymentMode = ModeOfPayment.CreditCard;
                }
                if (Convert.ToInt32(ddlMode.SelectedValue) == 3)
                {
                    itinerary.PaymentMode = ModeOfPayment.Credit;
                    lblCardType.Text = "Credit Amount";
                }
            }
            itinerary.NonRefundable = true;
            itinerary.TransactionType = "B2B";
            itinerary.CreatedBy = (int)Settings.LoginInfo.UserID;
            itinerary.BookingId = 0;//
            itinerary.AliasAirlineCode = string.Empty;
            itinerary.IsOurBooking = false;
            itinerary.UniversalRecord = string.Empty;
            itinerary.AirLocatorCode = string.Empty;
            itinerary.SupplierLocatorCode = string.Empty;
            //itinerary.LocationId = (int)Settings.LoginInfo.LocationID;

            itinerary.IsInsured = false; //f
            itinerary.IsLCC = false;//f
            itinerary.TripId = string.Empty; //null
            itinerary.SalesExecutiveId =int.Parse(hdnsalesExecutive.Value);
            itinerary.IsEdit = int.Parse(isEditable.Value.ToString());
            itinerary.SaleType = ddlSaleType.SelectedValue.ToString();
            int iTotSegments = Convert.ToInt32(hndsegmentsCount.Value) - 1;
            string[] existSegments = new string[] { };
            string[] durationArray = new string[] { };
            durationArray = hdnDuration.Value.Split(',');
            if (segmentId.Value!="")
            {
                existSegments = segmentId.Value.Split(',');
            }
            

            string[] arrSegments = hdnsegments.Value.Split('|');
            itinerary.Origin = arrSegments[0].Substring(0, 3);           
            if (itinerary.Segments == null)
            {
               // FlightInfo[] segments = new FlightInfo[iTotSegments];   
               if(iTotSegments< existSegments.Length)
                {
                    itinerary.Segments = new FlightInfo[existSegments.Length];
                }
               else
                {
                    itinerary.Segments = new FlightInfo[iTotSegments];
                }
                
                for (int i = 0; i < iTotSegments; i++)
                {
                    int RowNo = i + 1;

                    //tblSectors
                    CheckBox chkIsRefundable = (CheckBox)tblSector.Rows[RowNo].Cells[0].FindControl("chkIsRefund" + RowNo);
                    DropDownList ddlFromSector = (DropDownList)tblSector.Rows[RowNo].Cells[1].FindControl("ddlFromSector" + RowNo);
                    DropDownList ddlToSector = (DropDownList)tblSector.Rows[RowNo].Cells[2].FindControl("ddlToSector" + RowNo);
                    TextBox txtSectorFlyingCarrier = (TextBox)tblSector.Rows[RowNo].Cells[3].FindControl("txtFlyingCarrier" + RowNo);
                    TextBox txtSectorFlightno = (TextBox)tblSector.Rows[RowNo].Cells[4].FindControl("txtFlightN0" + RowNo);
                    DateControl txtSectorDeptDate = (DateControl)tblSector.Rows[RowNo].Cells[5].FindControl("DepartureDt" + RowNo);
                    DateControl txtSectorArrvDate = (DateControl)tblSector.Rows[RowNo].Cells[6].FindControl("ArrivalDt" + RowNo);
                    TextBox txtSectorFairBase = (TextBox)tblSector.Rows[RowNo].Cells[7].FindControl("txtFareBasis" + RowNo);
                    TextBox txtSectorClass = (TextBox)tblSector.Rows[RowNo].Cells[8].FindControl("txtClass" + RowNo);
                    TextBox txtSectorDuration = (TextBox)tblSector.Rows[RowNo].Cells[9].FindControl("txtDuration" + RowNo);


                    itinerary.Segments[i] = new FlightInfo();

                    itinerary.Segments[i].Origin = new Airport(arrSegments[i].Substring(0, 3).ToUpper());
                    itinerary.Segments[i].Destination = new Airport(arrSegments[i].Substring(4, 3).ToUpper());
                    itinerary.Segments[i].BookingClass = !string.IsNullOrEmpty(txtSectorClass.Text) ? txtSectorClass.Text : "NA" ;
                    itinerary.Segments[i].CabinClass = !string.IsNullOrEmpty(txtClassSector.Text) ? txtClassSector.Text : "NA";
                    itinerary.Segments[i].FlightStatus = FlightStatus.Confirmed;
                    itinerary.Segments[i].Airline = ddlairline.SelectedValue;
                    itinerary.Segments[i].FlightNumber = txtSectorFlightno.Text;
                    itinerary.Segments[i].DepartureTime = Convert.ToDateTime(txtSectorDeptDate.Value, dateFormat);
                    itinerary.Segments[i].ArrivalTime = Convert.ToDateTime(txtSectorArrvDate.Value, dateFormat);
                    itinerary.Segments[i].LastModifiedBy = (int)Settings.LoginInfo.UserID;
                    if(durationArray.Length>1)
                    {
                        itinerary.Segments[i].Duration = new TimeSpan(0, Convert.ToInt32(durationArray[i]), 0);                        
                    }
                    else
                    {
                        itinerary.Segments[i].Duration = new TimeSpan(0, Convert.ToInt32(durationArray[0]), 0);
                    }
                    
                    itinerary.Segments[i].CreatedBy = (int)Settings.LoginInfo.UserID;
                    itinerary.Segments[i].LastModifiedBy = (int)Settings.LoginInfo.UserID;
                    //itinerary.Segments[i].OperatingCarrier = (ddlairline.SelectedValue == ddlRepAirline.SelectedValue ? string.Empty : ddlRepAirline.SelectedValue);
                    itinerary.Segments[i].OperatingCarrier = txtSectorFlyingCarrier.Text;

                    if (ddlSector6.SelectedValue != "-1" && !string.IsNullOrEmpty(ddlSector6.SelectedValue))
                    {
                        itinerary.Segments[i].ConjunctionNo = txtCongTicketNo.Text + "/" + txtConjPNR.Text;
                    }

                    //itinerary.Segments[i].Group = 1;
                    //itinerary.Segments[i].BookingClass = txtSectorClass.Text;
                    itinerary.Segments[i].Status = "confirmed";
                    //itinerary.Segments[i].
                    if(int.Parse(hdnFlightId.Value)>0)
                    {
                        if (RowNo <= existSegments.Length)
                        {
                            itinerary.Segments[i].SegmentId = int.Parse(existSegments[i]);
                        }
                        else
                        {
                            itinerary.Segments[i].SegmentId = 0;
                        }
                    }
                }
                if (iTotSegments < existSegments.Length)
                {
                    for(int i= iTotSegments;i< existSegments.Length;i++)
                    {
                        DateControl txtSectorDeptDate = (DateControl)tblSector.Rows[1].Cells[5].FindControl("DepartureDt1");
                        DateControl txtSectorArrvDate = (DateControl)tblSector.Rows[1].Cells[6].FindControl("ArrivalDt1");
                        itinerary.Segments[i] = new FlightInfo();

                        itinerary.Segments[i].Origin = new Airport("0");
                        itinerary.Segments[i].Destination = new Airport("0");
                        itinerary.Segments[i].BookingClass = "NA";
                        itinerary.Segments[i].CabinClass = "NA";
                        itinerary.Segments[i].FlightStatus = FlightStatus.Confirmed;
                        itinerary.Segments[i].Airline = ddlairline.SelectedValue;
                        itinerary.Segments[i].FlightNumber = null;
                        itinerary.Segments[i].DepartureTime = Convert.ToDateTime(txtSectorDeptDate.Value, dateFormat); 
                        itinerary.Segments[i].ArrivalTime = Convert.ToDateTime(txtSectorArrvDate.Value, dateFormat);
                        itinerary.Segments[i].LastModifiedBy = (int)Settings.LoginInfo.UserID;
                        itinerary.Segments[i].Duration = new TimeSpan(0, 0, 0);
                        itinerary.Segments[i].CreatedBy = (int)Settings.LoginInfo.UserID;
                        itinerary.Segments[i].LastModifiedBy = (int)Settings.LoginInfo.UserID;
                        //itinerary.Segments[i].OperatingCarrier = (ddlairline.SelectedValue == ddlRepAirline.SelectedValue ? string.Empty : ddlRepAirline.SelectedValue);
                        itinerary.Segments[i].OperatingCarrier = null;
                        itinerary.Segments[i].ConjunctionNo = "";
                        itinerary.Segments[i].Status = "confirmed";
                        itinerary.Segments[i].RowStatus = 0;
                        itinerary.Segments[i].SegmentId = int.Parse(existSegments[i]);
                    }
                }
            }

            //FlightPassenger pax = new FlightPassenger();
            //if (txtPaxName.Text.Contains("/"))
            //{
            //    string[] name = txtPaxName.Text.Split('/');
            //    pax.FirstName = name[0];
            //    pax.LastName = name[1];

            //}
            //pax.PaxId = int.Parse(HdnPaxId.Value);
            //pax.CellPhone = txtPhone.Text;
            //pax.Email = txtEmail.Text;

            ////pax.LastName = txtPaxName.Text;// lastName
            ////pax.FlightId = Convert.ToInt32(txtSectorFlightno.);

            //pax.CreatedBy = (int)Settings.LoginInfo.UserID;
            //switch (ddlType.SelectedValue)
            //{
            //    case "AD":
            //        pax.Type = PassengerType.Adult;
            //        break;
            //    case "CHD":
            //        pax.Type = PassengerType.Child;
            //        break;
            //    case "INF":
            //        pax.Type = PassengerType.Infant;
            //        break;
            //}
            //pax.Title = ddlPaxTitle.SelectedValue;
            //pax.Price = new PriceAccounts();
            //pax.Price.PriceId = int.Parse(hdnPriceId.Value);
            //pax.Price.SupplierPrice = Convert.ToDecimal(txtTotalFare.Text);

            //pax.Price.PublishedFare = Convert.ToDecimal(txtSellingFare.Text);
            //pax.Price.NetFare = 0;
            //pax.Price.InputVATAmount = Convert.ToDecimal(TxtInputVAT.Text);
            //pax.Price.OutputVATAmount = hdnlocCountryCode.Value == "IN" ? Convert.ToDecimal(txtGST.Text) : Convert.ToDecimal(txtOutPutVAT.Text);


            //pax.Price.Tax = Convert.ToDecimal(txtTax.Text);

            ////commission percent value stored in commType
            ////pax.Price.OtherCharges = Convert.ToDecimal(txtCommper.Text);
            ////pax.Price.OtherCharges= Convert.ToDecimal(txtCommper.Text);
            //pax.Price.OurCommission = Convert.ToDecimal(txtCommisionAmt.Text);
            //pax.Price.CommissionType = "P";
            //pax.Price.CommissionValue = Convert.ToDecimal(txtCommper.Text);



            ////mark up
            //pax.Price.RateOfExchange = Convert.ToDecimal(txtExcRate.Text);
            //pax.Price.Markup = Convert.ToDecimal(txtMarkUp.Text);
            //pax.Price.AsvAmount = Convert.ToDecimal(txtAmount2.Text);
            //pax.Price.AsvElement = "TF";

            ////Discount details
            //pax.Price.Discount = Convert.ToDecimal(txtDiscountAmt.Text);
            //pax.Price.DiscountType = "P";
            //pax.Price.DiscountValue = Convert.ToDecimal(txtDiscountper.Text);

            ////transaction Details
            //pax.Price.TransactionFee = Convert.ToDecimal(txtTransFee.Text);
            //pax.Price.AdditionalTxnFee = Convert.ToDecimal(txtAddlTransFee.Text);

            ////Calc Details
            //pax.Price.InputVATAmount = Convert.ToDecimal(TxtInputVAT.Text);
            //pax.Price.OutputVATAmount = hdnlocCountryCode.Value == "IN" ? Convert.ToDecimal(txtGST.Text) : Convert.ToDecimal(txtOutPutVAT.Text);



            //pax.Price.OurPLB = Convert.ToDecimal(txtOurPLB.Text);
            //pax.Price.AgentCommission = Convert.ToDecimal(txtAgentCom.Text);
            //pax.Price.AgentPLB = Convert.ToDecimal(txtAgentPLB.Text);
            //pax.Price.SeviceTax = 0;
            //pax.Price.Currency = hdnCurrencyCode.Value;
            //pax.Price.CurrencyCode = hdnCurrencyCode.Value;

            //if(int.Parse(ddlTransactionType.SelectedItem.Value)==(int)BookingStatus.Refunded)
            //{
            //    pax.Price.AgentCancellationCharge = Convert.ToDecimal(txtAgentCancellationAmt.Text);
            //    pax.Price.SupplierCancellationCharge = Convert.ToDecimal(txtSupplierCancelationAmt.Text);
            //    pax.Price.CancellationInVat = Convert.ToDecimal(hdnCancelIPVat.Value);
            //    pax.Price.CancellationOutVat = Convert.ToDecimal(hdnCancelOPVat.Value);
            //}

            //List<KeyValuePair<string, decimal>> taxBreakUp = new List<KeyValuePair<string, decimal>>();
            //taxBreakUp.Add(new KeyValuePair<string, decimal>(txtCode0.Text, Convert.ToDecimal(txtValue0.Text)));
            //if (Convert.ToInt32(hndTaxCounter.Value) > 0)
            //{
            //    for (int k = 1; k <= Convert.ToInt32(hndTaxCounter.Value); k++)
            //    {
            //        string TaxCode = Request["txtCode" + k];
            //        decimal TaxAmount = Convert.ToDecimal(Request["txtValue" + k]);
            //        taxBreakUp.Add(new KeyValuePair<string, decimal>(TaxCode, TaxAmount));

            //    }
            //}
            //pax.TaxBreakup = taxBreakUp;
            //pax.IsLeadPax = PassengerList.Count == 1 ? true : pax.IsLeadPax;
            ////pax.Price.
            //PassengerList.Add(pax);
            //pax.IsLeadPax = PassengerList.Count == 1 ? true : pax.IsLeadPax;
            
            PassengerList = JsonConvert.DeserializeObject<List<FlightPassenger>>(hdnPaxDetails.Value);
            

            if (!string.IsNullOrEmpty(hdnPaxFlexInfo.Value)) //adding the flex details to pax object
            {
                for (int paxInd = 0; paxInd < PassengerList.Count; paxInd++)
                {
                    PassengerList[paxInd].FlexDetailsList = JsonConvert.DeserializeObject<List<FlightFlexDetails>>(hdnPaxFlexInfo.Value);
                    PassengerList[paxInd].FlexDetailsList.ForEach(x => x.sIsOffline = "OE");
                }
            }
            for (int paxInd = 0; paxInd < PassengerList.Count; paxInd++)
            {
                PassengerList[paxInd].CreatedBy = (int)Settings.LoginInfo.UserID;
            }
            itinerary.Passenger = PassengerList.ToArray();
            return itinerary;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in btnAdd_Click()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
            throw ex;
        }
    }

    private void HideSearch()
    {
        try
        {
            BindLocations(Convert.ToInt32(Settings.LoginInfo.AgentType), AgentID);
            //ddlMode.SelectedValue = "3";
            ddlcurrencycode.Items.Add(new ListItem(Settings.LoginInfo.Currency, Settings.LoginInfo.Currency));
            ddlcurrencycode.SelectedValue = Settings.LoginInfo.Currency;
            hdnCurrencyCode.Value= Settings.LoginInfo.Currency;
            BindAgentCode();
            EditPNRDiv.Style.Add("display", "block");
            ddlType.SelectedIndex = 0;
            ddlPaxTitle.Items.Clear();
            ddlPaxTitle.Items.Add(new ListItem("Mr.", "Mr."));
            ddlPaxTitle.Items.Add(new ListItem("Ms.", "Ms."));
            ddlPaxTitle.Items.Add(new ListItem("Dr.", "Dr."));
            ddlPaxTitle.Items.Add(new ListItem("Master", "MSTR"));
            Utility.StartupScript(this.Page, "HideSearchOption()", "HideSearchOption");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in HideSearch()" + Convert.ToString(ex.GetBaseException()), Request["REMOTE_ADDR"]);
        }
    }

    [WebMethod]
    public static string LoadFlexControls(int agentid) //loading Flex controls based on agentId
    {
        string data = string.Empty;
        DataTable dtFlexFields = CorporateProfile.GetAgentFlexDetailsByProduct((int)agentid, 1);
        if (dtFlexFields != null && dtFlexFields.Rows.Count > 0)
            data = JsonConvert.SerializeObject(dtFlexFields);
        return data;
    }

    [WebMethod]
    public static string[] GetCorporateTravelReasonAndProfiles(int agentid)
    {
        string[] corpData = null;
        AgentMaster agentMaster = new AgentMaster(agentid);
        if (agentMaster.IsCorporate)
        {
            string data = string.Empty;
            data += JsonConvert.SerializeObject(TravelUtility.GetTravelReasonList("S", agentid, ListStatus.Short));
            data += "^" + JsonConvert.SerializeObject(TravelUtility.GetProfileList(Settings.LoginInfo.CorporateProfileId, agentid, ListStatus.Short));
            corpData = data.Split('^');
        }
        return corpData;
    }

    [WebMethod] //Load the location country code of selected agent if currency is INR
    public static string GetAgentLocationCountryCode(string locationId)
    {
        string locationCountryCode = string.Empty;
        try
        {
            LocationMaster location = new LocationMaster(Convert.ToInt32(locationId));
            locationCountryCode = location.CountryCode;
            locationName = location.Name;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "(OffLineEntry)Error in GetAgentLocationCountryCode()" + Convert.ToString(ex.GetBaseException()), "");
            throw ex;
        }
        return locationCountryCode;
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
    public static object LoadCreditCard()
    {
        DataTable dtAgents = new DataTable();
        try
        {
            dtAgents = CreditCardMaster.GetList(ListStatus.Short, RecordStatus.Activated); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            return JsonConvert.SerializeObject(dtAgents);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "LoadCreditCard web service" + Convert.ToString(ex.GetBaseException()), "");
            return JsonConvert.SerializeObject(dtAgents);
        }
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
    public static object LoadTransferType()
    {
        DataTable dtTransferType = new DataTable();
        DataSet dtTransfer = new DataSet();
        try
        {
            dtTransfer = UserMaster.GetMemberTypeList("bank_transfer_type"); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            dtTransferType = dtTransfer.Tables[0];
            return JsonConvert.SerializeObject(dtTransferType);
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "LoadTransferType web service" + Convert.ToString(ex.GetBaseException()), "");
            return JsonConvert.SerializeObject(dtTransferType);
        }
        
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
    public static object LoadBankData()
    {
        DataTable dtBank = new DataTable();
        try
        {
            dtBank = UserMaster.GetBankDetails("A"); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            return JsonConvert.SerializeObject(dtBank);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "LoadBankData web service" + Convert.ToString(ex.GetBaseException()), "");
            return JsonConvert.SerializeObject(dtBank);
        }
       
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
    public static object LoadSalesExecutive(int agentId)
    {
        DataTable dtSalesExecutive = new DataTable();
        try
        {
            dtSalesExecutive = UserMaster.GetSalesExecList(agentId); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            return JsonConvert.SerializeObject(dtSalesExecutive);
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "LoadSalesExecutive web service" + Convert.ToString(ex.GetBaseException()),"");
            return JsonConvert.SerializeObject(dtSalesExecutive);
        }        
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
    public static object LoadHandlingFee(int serviceId)
    {
        DataTable dtHandlingFee = new DataTable();
        try
        {
            dtHandlingFee = HandlingFeeMaster.GetHandlingDetails(serviceId); 
            return JsonConvert.SerializeObject(dtHandlingFee);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "LoadHandlingFee web service" + Convert.ToString(ex.GetBaseException()), "");
            return JsonConvert.SerializeObject(dtHandlingFee);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
    public static object LoadItinerary(int flightId)
    {
        FlightItinerary FlItinerary = new FlightItinerary(flightId);        
        try
        {
            List<Ticket> ticketList = Ticket.GetTicketList(flightId);            
            SettlementDetailes SettlementObj = new SettlementDetailes();
            List<SettlementDetailes> settleMent = new List<SettlementDetailes>();
            settleMent = SettlementObj.load(flightId);
            List<HandlingElement> handlingList = new List<HandlingElement>();
            //handling.Load(FlItinerary.Passenger[0].Price.PriceId);
            BookingDetail booking = new BookingDetail(FlItinerary.BookingId);            
            handlingList=HandlingElement.LoadByFlightId(flightId);     
            var OfflineResponseObj = new
            {
                itinerary = FlItinerary,
                settleMent= settleMent,
                handling= handlingList,
                booking = booking,
                ticket= ticketList
            };
            
            return JsonConvert.SerializeObject(OfflineResponseObj);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "LoadHandlingFee web service" + Convert.ToString(ex.GetBaseException()), "");
            var OfflineResponseObj = new
            {
                itinerary = "",
                settleMent = "",
                handling = "",
                booking = "",
                ticket =""
            };
            return JsonConvert.SerializeObject(OfflineResponseObj);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
    public static object LoadCustomerlocation(int agentId)
    {
        DataTable dtLocations = new DataTable();
        try
        {
           dtLocations = CT.TicketReceipt.BusinessLayer.LocationMaster.GetList(Convert.ToInt32(agentId), CT.TicketReceipt.BusinessLayer.ListStatus.Short, CT.TicketReceipt.BusinessLayer.RecordStatus.Activated, string.Empty); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
           return JsonConvert.SerializeObject(dtLocations);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, userID, "LoadCustomerlocation web service" + Convert.ToString(ex.GetBaseException()), "");
            return JsonConvert.SerializeObject(dtLocations);
        }
    }

    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
    public static bool CheckPNRExist(string pnrNo)
    {
        SqlConnection connection = DBGateway.GetConnection();
        SqlParameter[] paramList = new SqlParameter[3];
        paramList[0] = new SqlParameter("@pnr", pnrNo);       
        SqlDataReader dataReader = DBGateway.ExecuteReaderSP("usp_CheckPNR", paramList, connection);
        if (dataReader != null && dataReader.HasRows)
        {
            dataReader.Close();
            connection.Close();
            return true;
        }
        else
        {
            dataReader.Close();
            connection.Close();
        }        
        return false;
    }

}


