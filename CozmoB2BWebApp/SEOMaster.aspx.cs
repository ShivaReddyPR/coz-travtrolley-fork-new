﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.BookingEngine.WhiteLabel;
using CT.Core;
using CT.TicketReceipt.Web.UI.Controls;

public partial class SEOMasterGUI : CT.Core.ParentPage
{
    string SEO_SEARCH_SESSION = "_SEOSearchList";
    string SEO_SESSION = "_SEOMaster";

    #region Protected Methods
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            lblSuccessMsg.Text = string.Empty;
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
                lblSuccessMsg.Text = string.Empty;
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    /// <summary>
    /// grid filtering
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{"HTtxtPageName", "PageName" }
                                             ,{ "HTtxtURL", "URL" }
                                             ,{ "HTtxtURLRewriting", "URLRewriting" }
                                              ,{ "HTtxtAgentName", "AgentName" }
                                              ,{"HTtxtMemberName", "MemberName" }
                                         };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }
    }
    /// <summary>
    /// Paging
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }

    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            int seoId = Utility.ToInteger(gvSearch.SelectedValue);
            Edit(seoId);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    #endregion

    #region private methods
    private void InitializePageControls()
    {
        try
        {
            BindAgent();
            Clear();
        }
        catch { throw; }
    }

    /// <summary>
    /// Binding AgentList
    /// </summary>
    private void BindAgent()
    {
        try
        {
            ddlAgent.DataSource = AgentMaster.GetB2CAgentList();
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "-1"));
            ddlAgent.SelectedIndex = 0;
        }
        catch { throw; }
    }

    /// <summary>
    /// clear All details
    /// </summary>
    private void Clear()
    {
        ddlAgent.SelectedIndex = -1;
        txtPageName.Text = string.Empty;
        txtUrl.Text = string.Empty;
        txtURLRewriting.Text = string.Empty;
        txtPageTitle.Text = string.Empty;
        txtImageAltText.Text = string.Empty;
        txtMetaDescription.Text = string.Empty;
        txtMetaKeywords.Text = string.Empty;
        CurrentObject = null;
        btnSave.Text = "Save";
        btnClear.Text = "Clear";
        ddlAgent.Enabled = true;
        txtH1Tag.Text = string.Empty;
        txtSocialTag.Text = string.Empty;
        txtH1TagDesc.Text = string.Empty;
    }

    /// <summary>
    /// SEo Details Saving and updateing
    /// </summary>
    private void Save()
    {
        try
        {
            SEOMaster seoMaster;
            if (CurrentObject == null)
            {
                seoMaster = new SEOMaster();
            }
            else
            {
                seoMaster = CurrentObject;
            }
            seoMaster.PageName = txtPageName.Text.Trim();
            seoMaster.URL = txtUrl.Text.Trim();
            seoMaster.URLRewriting = txtURLRewriting.Text.Trim();
            seoMaster.PageTitle = txtPageTitle.Text.Trim();
            seoMaster.ImageAltText =txtImageAltText.Text.Trim();
            seoMaster.MetaDescription = txtMetaDescription.Text.Trim();
            seoMaster.MetaKeywords = txtMetaKeywords.Text.Trim();
            seoMaster.AgentId = Utility.ToInteger(ddlAgent.SelectedValue);
            long memberId = UserMaster.GetPrimaryUserId(seoMaster.AgentId);
            seoMaster.MemberId = Utility.ToInteger(memberId);
            seoMaster.CreatedBy =Convert.ToInt32(Settings.LoginInfo.UserID);
            seoMaster.SocialTag = txtSocialTag.Text.Trim(); ;
            seoMaster.H1Tag = txtH1Tag.Text.Trim();
            seoMaster.H1TagDesc = txtH1TagDesc.Text.Trim();
            seoMaster.Save();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage(("Transaction For "), txtPageName.Text.Trim(), (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated)) + " Successfully.";
            Clear();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }
    }

    private void bindSearch()
    {
        try
        {
            DataTable dt = SEOMaster.GetList(RecordStatus.Activated, ListStatus.Long);
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;

        }
    }

    private void Edit(int id)
    {
        try
        {
            SEOMaster seoData = new SEOMaster(id);
            CurrentObject = seoData;
            ddlAgent.SelectedValue = Utility.ToString(seoData.AgentId);
            ddlAgent.Enabled = false;
            txtPageName.Text = seoData.PageName;
            txtUrl.Text = seoData.URL;
            txtURLRewriting.Text = seoData.URLRewriting;
            txtImageAltText.Text = seoData.ImageAltText;
            txtPageTitle.Text = seoData.PageTitle;
            txtMetaDescription.Text = seoData.MetaDescription;
            txtMetaKeywords.Text = seoData.MetaKeywords;
            txtSocialTag.Text = seoData.SocialTag;
            txtH1Tag.Text = seoData.H1Tag;
            txtH1TagDesc.Text = seoData.H1TagDesc;
            btnSave.Text = "Update";
            btnClear.Text = "Cancel";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }

    }
    #endregion

    #region Session Properties
    private SEOMaster CurrentObject
    {
        get
        {
            return (SEOMaster)Session[SEO_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(SEO_SESSION);
            }
            else
            {
                Session[SEO_SESSION] = value;
            }

        }
    }
    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[SEO_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["id"] };

            Session[SEO_SEARCH_SESSION] = value;
        }
    }
    #endregion
}
