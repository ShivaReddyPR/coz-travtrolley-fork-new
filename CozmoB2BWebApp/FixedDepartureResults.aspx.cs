﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

public partial class FixedDepartureResultsGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected DataTable ThemeList;
    protected DataTable Country;
    protected DataTable RegionList;
    DataTable Activities;
    DataTable dtPriceActivity;
    protected string activityImgFolder;
    int agencyId = 0;

    PagedDataSource pagedData = new PagedDataSource();
    string isFixedDeparture = "Y";

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        if (Settings.LoginInfo != null)
        {
            activityImgFolder = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"].ToString();
            if (!IsPostBack)
            {
                CurrentPage = 0;
                BindActivities();
            }
            else
            {
                DataTable dt = (DataTable)Session["FixedDepartures"];
                if (hdfClearFilters.Value == "no")
                {
                    if (hdfTheme.Value != null && hdfTheme.Value != string.Empty)
                    {
                        string str = hdfTheme.Value;
                        SortTheme(str);
                    }
                    if (hdfCountry.Value != null && hdfCountry.Value != string.Empty)
                    {
                        string str = hdfCountry.Value;
                        SortCountry(str);
                    }
                    if (hdfRegion.Value != null && hdfRegion.Value != string.Empty)
                    {
                        string str = hdfRegion.Value;
                        SortRegion(str.Trim());
                    }

                    if (hdfTourValue.Value == "1")
                    {
                        IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                        string StartFromDate = (checkInDate.Text);
                        DateTime fromDate = Convert.ToDateTime(StartFromDate, dateFormat);
                        string StartToDate = (checkOutDate.Text);
                        DateTime toDate = Convert.ToDateTime(StartToDate, dateFormat);
                        dtPriceActivity = ActivityDetails.GetPriceActivityIds(fromDate, toDate);
                        string ActivitiesId = string.Empty;
                        if (dtPriceActivity.Rows.Count > 0)
                        {
                            foreach (DataRow dr in dtPriceActivity.Rows)
                            {
                                if (ActivitiesId == string.Empty)
                                {
                                    ActivitiesId = dr["priceActivityId"].ToString();
                                }
                                else
                                {
                                    ActivitiesId = ActivitiesId + "," + dr["priceActivityId"].ToString();
                                }
                            }
                            SortTourDate(ActivitiesId);
                        }
                        else
                        {
                            ViewState["FixedDeparture"] = null;
                        }
                        hdfTourValue.Value = "0";
                    }
                    else
                    {
                        checkInDate.Text = "DD/MM/YYYY";
                        checkOutDate.Text = "DD/MM/YYYY";
                    }
                }
                else
                {
                    BindActivities();
                    ViewState["FixedDeparture"] = dt;
                    checkInDate.Text = "DD/MM/YYYY";
                    checkOutDate.Text = "DD/MM/YYYY";
                    sortBy();
                }

            }

            InitializePageControls();
            doPaging();
        }
        else
        {
            Response.Redirect("AbandonSession.aspx");
        }

    }
    private void Clear()
    {
        hdfRegion.Value = null;
        hdfTheme.Value = null;
        hdfCountry.Value = null;
        hdfTourValue.Value = "0";
    }
    private void fill_Tree2()
    {

        TreeView1.Nodes.Clear();
        foreach (DataRow row in RegionList.Rows)
        {

            TreeNode tnParent = new TreeNode();
            tnParent.Text = row["regionName"].ToString();
            tnParent.Value = row["regionCode"].ToString();

            tnParent.ToolTip = "Click to get Child";
            tnParent.SelectAction = TreeNodeSelectAction.SelectExpand;
            tnParent.NavigateUrl = "javascript:SortRegionJS('" + tnParent.Value + "')";
            tnParent.Expand();
            tnParent.Selected = true;
            FillChild(tnParent, tnParent.Value);
            TreeView1.Nodes.Add(tnParent);

        }
    }
    public void FillChild(TreeNode parent, string ParentId)
    {
        //DataSet ds = PDataset("Select * from ChildTable where ParentId =" + ParentId);
        parent.ChildNodes.Clear();
        //foreach (DataRow row in CityList.Rows )
        {
            DataRow[] drcountry = Country.Select("region='" + ParentId + "'");
            if (drcountry != null && drcountry.Length > 0)
            {
                foreach (DataRow row in drcountry)
                {
                    TreeNode child = new TreeNode();
                    child.Text = row["countryName"].ToString();
                    child.Value = row["countryCode"].ToString();
                    if (child.ChildNodes.Count == 0)
                    {
                        child.PopulateOnDemand = true;
                    }
                    child.NavigateUrl = "javascript:SortCountryJS('" + child.Value + "')";
                    child.ToolTip = "Click to get Child";
                    child.SelectAction = TreeNodeSelectAction.SelectExpand;
                    child.CollapseAll();

                    {
                        parent.ChildNodes.Add(child);
                    }
                }
            }
        }
    }

    private void InitializePageControls()
    {
        ActivityDetails ad = new ActivityDetails();
        //CityList = ad.LoadCity("Y");
        RegionList = ad.LoadRegion(isFixedDeparture,Settings.LoginInfo.AgentId);
        Country = ad.LoadCountry(isFixedDeparture,Settings.LoginInfo.AgentId);
        ThemeList = ad.LoadTheme("F");
        //Duration = ad.LoadDuration("Y");
        fill_Tree2();

    }
    private void BindActivities()
    {
        try
        {
            //string status = "A";

            ActivityDetails.AgentCurrency = Settings.LoginInfo.Currency;
            ActivityDetails.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
            Activities = ActivityDetails.GetActivities(Settings.LoginInfo.AgentId,isFixedDeparture);
            Session["FixedDepartures"] = Activities;
            BindDataList(Activities);
            sortBy();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindDataList(DataTable dt)
    {
        try
        {

            DLActivities.DataSource = dt;
            DLActivities.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }


    protected void ddlSort_SelectedIndexChanged1(object sender, EventArgs e)
    {
        //DataTable dt;
        //DataView dv;
        try
        {
            sortBy();
            //DataTable pdt = (DataTable)ViewState["Activity"];
            //if (pdt != null && pdt.Rows.Count > 0)
            //{
            //    dt = (DataTable)ViewState["Activity"];
            //    dv = new DataView(dt);
            //}
            //else
            //{
            //    dt = (DataTable)Session["Activities"];
            //    dv = new DataView(dt);
            //}

            //if (ddlSort.SelectedValue == "price")
            //{
            //    dv.Sort = "amount";
            //    dt = dv.ToTable();
            //    // BindDataList(dt);
            //    ViewState["Activity"] = dt;
            //    doPaging();
            //}
            //else if (ddlSort.SelectedValue == "alphabetical")
            //{
            //    dv.Sort = "activityName";
            //    dt = dv.ToTable();
            //    // BindDataList(dt);
            //    ViewState["Activity"] = dt;
            //    doPaging();
            //}
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    void sortBy()
    {
        DataTable dt;
        DataView dv;
        string sortType = ddlSort.SelectedItem.Value == "alphabetical" ? "activityName" : ddlSort.SelectedItem.Value == "price" ? "amount" : "";
        try
        {
            DataTable pdt = (DataTable)ViewState["FixedDeparture"];
            if (pdt != null && pdt.Rows.Count > 0)
            {
                dt = (DataTable)ViewState["FixedDeparture"];
                dv = new DataView(dt);
            }
            else
            {
                dt = (DataTable)Session["FixedDepartures"];
                dv = new DataView(dt);
            }

            if (!string.IsNullOrEmpty(sortType))
            {
                dv.Sort = sortType;
                dt = dv.ToTable();
                // BindDataList(dt);
                ViewState["FixedDeparture"] = dt;
                doPaging();
            }
            //else if (ddlSort.SelectedValue == "alphabetical")
            //{
            //    dv.Sort = "activityName";
            //    dt = dv.ToTable();
            //    // BindDataList(dt);
            //    ViewState["Activity"] = dt;
            //    doPaging();
            //}
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void doPaging()
    {
        DataTable pdt = (DataTable)ViewState["FixedDeparture"];
        //if (pdt!=null && pdt.Rows.Count>0)
        if (pdt != null && pdt.Rows.Count > 0)
        {
            ddlSort.Enabled = true;
            DataTable dt = (DataTable)ViewState["FixedDeparture"];
            pagedData.DataSource = dt.DefaultView;
        //}
        //else
        //{
        //    DataTable dt = (DataTable)Session["Activities"];
        //    pagedData.DataSource = dt.DefaultView;
        //}
        pagedData.AllowPaging = true;
        pagedData.PageSize = 5;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        DLActivities.DataSource = pagedData;
        DLActivities.DataBind();
        Clear();
        }
        else
        {
            DLActivities.DataSource = null;
            DLActivities.DataBind();
            ddlSort.Enabled = false;
        }
        //ViewState["Activity"] = null;
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }


    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }

    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }
     
    //protected void lnkDubai_Click(object sender, EventArgs e)
    //{
    //    string str = "Dubai";
    //    SortCity(str);
    //}
    //protected void LinkButton1_Click(object sender, EventArgs e)
    //{
    //    string str = "Sharjah";
    //    SortCity(str);
    //}
    //protected void LinkButton2_Click(object sender, EventArgs e)
    //{
    //    string str = "Ummul Queen";
    //    SortCity(str);
    //}
    //protected void lnkCountry1_Click(object sender, EventArgs e)
    //{
    //    string str = "United Arab Emirates";
    //    SortCountry(str);
    //}
    //protected void LinkCountry2_Click(object sender, EventArgs e)
    //{
    //    string str = "Qatar";
    //    SortCountry(str);
    //}
    //protected void LinkButton4_Click(object sender, EventArgs e)
    //{
    //    string str = "Doha";
    //    SortCity(str);
    //}
    //protected void LinkButton5_Click(object sender, EventArgs e)
    //{
    //    string str = "Sharjah";
    //    SortCity(str);
    //}
    //protected void LinkButton6_Click(object sender, EventArgs e)
    //{
    //    string str = "Ummul Queen";
    //    SortCity(str);
    //}
    //protected void LinkCountry3_Click(object sender, EventArgs e)
    //{
    //    string str = "Saudia Arabia";
    //    SortCountry(str);
    //}
    //protected void LinkButton7_Click(object sender, EventArgs e)
    //{
    //    string str = "Riyadh";
    //    SortCity(str);
    //}
    //protected void LinkButton8_Click(object sender, EventArgs e)
    //{
    //    string str = "Jeddah";
    //    SortCity(str);
    //}
    //protected void LinkButton9_Click(object sender, EventArgs e)
    //{
    //    string str = "Najd";
    //    SortCity(str);
    //}
    //protected void LinkButton3_Click(object sender, EventArgs e)
    //{
    //    string str = LinkButton3.Text;
    //    SortTheme(str);
    //}
    //protected void LinkButton10_Click(object sender, EventArgs e)
    //{
    //    string str = LinkButton10.Text;
    //    SortTheme(str);
    //}
    //protected void LinkButton11_Click(object sender, EventArgs e)
    //{
    //    string str = LinkButton11.Text;
    //    SortTheme(str);
    //}
    //protected void LinkButton12_Click(object sender, EventArgs e)
    //{
    //    string str = LinkButton12.Text;
    //    SortTheme(str);
    //}

    #region FilterMethods
    public void SortCountry(string str)
    {
        try
        {
            DataTable table1 = (DataTable)Session["FixedDepartures"];
            DataRow[] rows = table1.Select("activityCountry like ('%" + str + "%')");
            DataTable dt = table1.Clone();
            foreach (DataRow row in rows)
            {
                dt.ImportRow(row);
            }
            ViewState["FixedDeparture"] = dt;
            CurrentPage = 0;
            doPaging();
            if (rows.Length > 0)
            {
                sortBy();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void SortRegion(string str)
    {
        try
        {
            DataTable table1 = (DataTable)Session["FixedDepartures"];
            //DataRow[] rows = table1.Select("regionName='" + str + "'");
            DataRow[] rows = table1.Select("region like ('%" + str + "%')");
            DataTable dt = table1.Clone();
            foreach (DataRow row in rows)
            {
                dt.ImportRow(row);
            }
            ViewState["FixedDeparture"] = dt;
            CurrentPage = 0;
            doPaging();
            if (rows.Length > 0)
            {
                sortBy();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
     
    public void SortTourDate(string ids)
    {
        try
        {
            DataTable table1 = (DataTable)Session["FixedDepartures"];
            DataRow[] rows = table1.Select("activityId in (" + ids + ")");
            DataTable dt = table1.Clone();
            foreach (DataRow row in rows)
            {
                dt.ImportRow(row);
            }
            ViewState["FixedDeparture"] = dt;
            CurrentPage = 0;
            doPaging();
            if (rows.Length > 0)
            {
                sortBy();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void SortTheme(string str)
    {
        try
        {
            DataTable table1 = (DataTable)ViewState["FixedDepartures"];
            if (table1 == null)
            {
                table1 = (DataTable)Session["FixedDepartures"];
            }
            DataRow[] rows = table1.Select("ThemeId like ('%" + str + "%')");
            DataTable dt = table1.Clone();
            foreach (DataRow row in rows)
            {
                dt.ImportRow(row);
            }
            ViewState["FixedDeparture"] = dt;
            CurrentPage = 0;
            doPaging();
            if (rows.Length > 0)
            {
                sortBy();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion


    protected void DLActivities_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            Label lblCategory = e.Item.FindControl("lblCategory") as Label;
            Label lblCountry = e.Item.FindControl("lblCountry") as Label;
            Label lblRegion = e.Item.FindControl("lblRegion") as Label;

            DataRowView view = e.Item.DataItem as DataRowView;
            ActivityDetails obj = new ActivityDetails();

            // For Theme
            ThemeList = obj.LoadTheme("F");
            DataRow[] filteredRows = ThemeList.Select("themeid in (" + view["themeId"].ToString() + ")");

            if (filteredRows != null && filteredRows.Length > 0)
            {
                foreach (DataRow fr in filteredRows)
                {
                    if (lblCategory.Text.Length > 0)
                    {
                        lblCategory.Text += ", " + fr["ThemeName"].ToString();
                    }
                    else
                    {
                        lblCategory.Text = fr["ThemeName"].ToString();
                    }
                }
            }

            // For Country
            Country = obj.LoadCountry(isFixedDeparture,Settings.LoginInfo.AgentId);
            string[] filCount = view["activityCountry"].ToString().Split(',');
            string activityCountry = string.Empty;
            foreach (string country in filCount)
            {
                if (activityCountry == string.Empty)
                {
                    activityCountry = "'" + country + "'";
                }
                else
                {
                    activityCountry = activityCountry + "," + "'" + country + "'";
                }
            }

            DataRow[] filteredCountryRows = Country.Select("countryCode in (" + activityCountry + ")");

            if (filteredCountryRows != null && filteredCountryRows.Length > 0)
            {
                foreach (DataRow frCountry in filteredCountryRows)
                {
                    if (lblCountry.Text.Length > 0)
                    {
                        lblCountry.Text += ", " + frCountry["countryName"].ToString();
                    }
                    else
                    {
                        lblCountry.Text = frCountry["countryName"].ToString();
                    }
                }
            }
            //for region
            RegionList = obj.LoadRegion(isFixedDeparture,Settings.LoginInfo.AgentId);
            string[] filRegion = view["Region"].ToString().Split(',');
            string activityRegion = string.Empty;
            foreach (string region in filRegion)
            {
                if (activityRegion == string.Empty)
                {
                    activityRegion = "'" + region + "'";
                }
                else
                {
                    activityRegion = activityRegion + "," + "'" + region + "'";
                }
            }

            DataRow[] filteredRegionRows = RegionList.Select("regionCode in (" + activityRegion + ")");

            if (filteredRegionRows != null && filteredRegionRows.Length > 0)
            {
                foreach (DataRow frRegion in filteredRegionRows)
                {
                    if (lblRegion.Text.Length > 0)
                    {
                        lblRegion.Text += ", " + frRegion["regionName"].ToString();
                    }
                    else
                    {
                        lblRegion.Text = frRegion["regionName"].ToString();
                    }
                }
            }

        }
    }
    protected string CTCurrencyFormat(object currency)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
    }

    # region GridBindMethods
    protected object restrictLength(object value)
    {
        string txt = value.ToString();
        if (txt.Length > 200)
        {
            txt = txt.Substring(0, 199) + "....";
        }
        return txt;

    }
    # endregion
}
