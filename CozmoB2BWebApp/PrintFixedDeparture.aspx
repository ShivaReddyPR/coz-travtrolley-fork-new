﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PrintFixedDepartureGUI" Codebehind="PrintFixedDeparture.aspx.cs" %>
<%@ Import Namespace="System.Collections.Generic" %>

<link rel="stylesheet" href="css/ajax_tab_css.css">
<link rel="stylesheet" href="css/main-style.css">

<script>
    function printpage() {
        window.print()
    }
</script>
<form id="form1">
<div>
    <input type="button" value="Print this page" onclick="printpage()"></div>
<table style="font-family: Arial, Helvetica, sans-serif; line-height: 17px" width="100%"
    border="0" cellspacing="0" cellpadding="0">
    <tr>
        <td>
            <table style="border: solid 1px #000;" width="100%" border="0" cellpadding="0" cellspacing="0"
                bgcolor="#0099ff">
                <tr>
                    <td width="50%" height="30">
                        <label style="color: #fff; font-size: 18px;">
                            <%=activity.Name%>
                            (<%=activity.Days %>
                            Nights &amp;
                            <%=activity.Days  + 1 %>
                            Days)
                        </label>
                    </td>
                    <td width="50%" align="right">
                        <label style="padding-right: 15px; color: #FFFFFF; font-size: 16px">
                            Starting From- AED
                            <%=minPrice.ToString("N0") %></label>
                        <label style="padding-right: 15px; color: #FFFFFF">
                            per person</label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td style="padding-top: 5px">
            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                <tr>
                    <td valign="top" width="30%">
                        <img width="366px" height="247px" src="<%= activity.ImagePath1%>"
                            alt="<%=activity.Name %>" title="<%=activity.Name %>" />
                    </td>
                    <td width="70%" valign="top">
                        <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                            <tr>
                                <td height="0">
                                    <label style="color: #cc0000; font-size: 16px">
                                        <strong>Overview</strong></label>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: top; font-size: 14px!important" valign="top">
                                    <label>
                                        <%=activity.Overview %>
                                    </label>
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                <tr>
                    <td height="0">
                        <label style="color: #cc0000; font-size: 16px">
                            <strong>Inclusions </strong>
                        </label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="Inclusions" class="active" >
                            
                                
                                <ul>
                                
                                <%if (activity != null)
                                          { %>
                                <%foreach (string item in activity.Inclusions)
                                              { %>
                                <li style="list-style-type: circle">
                                    <%=item%></li>
                                <%} %>
                            </ul>
                            <label style="color: #cc0000; font-size: 16px">
                                    <strong>Exclusions </strong>
                                </label>
                            <ul>
                                
                                <%foreach (string item in activity.Exclusions)
                                              { %>
                                <li style="list-style-type: circle">
                                    <%=item%></li>
                                <%}
                                          }%></ul>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                <tr>
                    <td height="0">
                        <label style="color: #cc0000; font-size: 16px">
                            <strong>Itinerary</strong></label>
                    </td>
                </tr>
                <tr>
                    <td valign="top">
                       
                            <div id="Itinerary" class="active" >
                            <asp:DataList ID="dlFDItinerary" runat="server" CellPadding="4" DataKeyField="Id"
                                                Width="900px">
                                                <ItemTemplate>
                                                        <table style=" border: solid 1px #ccc; margin-bottom:10px;" class="" width="100%">
                                                           <tr style="height: 25px;">
                                                                <td>

                                                                        <table style="margin-bottom:14px; margin-top:10px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                        <td width="14%"> <strong style=" font-size:14px">Day <%# Eval("Day")%> </strong></td>
                                                                        <td width="86%"></td>
                                                                        </tr>

                                                                        </table>
                                                   
                                                                   
                                                                    
                                                                    
                                                                    
                                                                    
                                                                </td>
                                                            </tr>
                                                            <tr style="height: 25px;">
                                                                <td>
                                                                  
                                                                  
                                                                  
                                                                  <table style="margin-bottom:16px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="14%"><strong>ItineraryName: </strong></td>
    <td width="86%"><%# Eval("ItineraryName")%></td>
  </tr>
  
</table>
                                                                  
                                                                    
                                                                    
                                                                    
                                                                    
                                                                </td>
                                                            </tr>
                                                                    <tr style="height: 25px;">
                                                                <td>
                                                                
                                 <table style="margin-bottom:10px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="14%"><strong>  Meals:</strong> </td>
    <td width="86%"><%# Eval("Meals")%></td>
  </tr>
  
</table>
                                                                
                                                                    
                                                                </td>
                                                            </tr>
                                                             <tr style="height: 25px;">
                                                                <td>
                                                                   
                                                                   
                                                                   
  <table style="margin-bottom:14px;" align="right" width="98%" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td valign="top" width="14%"> <strong>Itinerary: </strong></td>
    <td width="86%"><%# Eval("Itinerary")%></td>
  </tr>
  
</table>
                                                                   
                                                                   
                                                                   
                                                                    
                                                                </td>
                                                            </tr>
                                                        </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                               <%-- <label>
                                    <%=(activity != null ? activity.Itinerary1 : "") %></label>
                                <label>
                                    <%=(activity != null ? activity.Itinerary2 : "") %></label>
                                <%=(activity != null ? activity.Details : "") %>
                        
                        <h4>
                            Transfer included</h4>
                            <ul>
                        <li style="list-style-type: circle">
                            <%=(activity != null ? activity.TransferIncluded : "") %></li></ul>
                        <h4>
                            Meals Included</h4>
                        <ul>
                            <%if(activity != null && activity.MealsIncluded != null){ %>
                            <%string[] mealItems = activity.MealsIncluded.Split(','); %>
                            <%foreach (string meal in mealItems)
                                              { %>
                            <li style="list-style-type: circle">
                                <%=meal %></li>
                            <%} %>
                            <%} %>
                        </ul>--%>
                        </div> 
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                <tr>
                    <td height="0">
                        <label style="color: #cc0000; font-size: 16px">
                            <strong>Price</strong></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <label style="font-size: 14px">
                            <div id="RoomRates">
                                <div>
                                    <asp:DataList id="dlRoomRates" runat="server" width="100%">
                                                <ItemTemplate>
                                                    <table width="100%" border="1">
                                   
                                                        <tr>
                                                        <td height="25px" width="30%">
                                                        
                                                        <%#(Eval("PriceDate")==DBNull.Value)?string.Empty: Convert.ToDateTime(Eval("PriceDate")).ToString("dd-MMM-yyyy")%>
                                                        </td>
                                                            <td width="30%">
                                                                <%#Eval("Label") %>
                                                            </td>
                                                        
                                                           <td width="70%">
                                                               <%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>
                                                                <%#CTCurrencyFormat(Eval("Total"))%>
                                                            </td>
                                                        </tr>
                                                    </table>
                                                </ItemTemplate>
                                            </asp:DataList>
                                </div>
                            </div>
                        </label>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
    <tr>
        <td>
            <table width="100%" border="0" align="right" cellpadding="6" cellspacing="0">
                <tr>
                    <td height="0">
                        <label style="color: #cc0000; font-size: 16px">
                            <strong>Terms & Conditions</strong></label>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div id="Terms" class="active" >
                            
                            <h4>Things to bring</h4>
                            <ul>
                                <%if (activity != null && activity.ThingsToBring != null)
                                  { %>
                                <%string[] things = activity.ThingsToBring.Split(','); %>
                                <%foreach (string thing in things)
                                  { %>
                                <li style=" list-style-type:circle; margin-left:20px;">
                                    <%=thing%></li>
                                <%} %>
                                <%} %>
                            </ul>
                            
                            <h4>Cancellation Policy</h4>
                                     <ul>
                                        <%if (activity != null && activity.CancelPolicy != null)
                                          { %>
                                        <%string[] cancelP = activity.CancelPolicy.Split(new string[] { "#&#" }, StringSplitOptions.RemoveEmptyEntries); %>
                                        <%foreach (string cancel in cancelP)
                                          { %>
                                          <li style=" list-style-type:circle; margin-left:20px;"><%=cancel %></li>
                                        <%} %>
                                        <%} %>
                                        </ul>
                        </div>
                    </td>
                </tr>
            </table>
        </td>
    </tr>
    <tr>
        <td>
            &nbsp;
        </td>
    </tr>
</table>

</form>
