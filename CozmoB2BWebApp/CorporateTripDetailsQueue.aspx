﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorporateTripDetailsQueueUI"
    Title="Corporate Trip Details Queue " Codebehind="CorporateTripDetailsQueue.aspx.cs" %>

<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript">

        function Validate() {

            var valid = false;
            document.getElementById('errMess').style.display = "none";

            if (document.getElementById('ctl00_cphTransaction_dcReimFromDate_Date').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select from date .";
            }
            else if (document.getElementById('ctl00_cphTransaction_dcReimToDate_Date').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select to date.";
            }

            else {
                valid = true;
            }

            return valid;
        }

        //Validating Hotel Tab fields
        function ValidateHotelTab() {

            var valid = false;
            document.getElementById('errMess').style.display = "none";

            if (document.getElementById('ctl00_cphTransaction_dcHotelFromDate_Date').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select from date .";
            }
            else if (document.getElementById('ctl00_cphTransaction_dcHotelToDate_Date').value.length == 0) {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select to date.";
            }

            else {
                valid = true;
            }

            return valid;
        }

        
        function SetTabSelection(tabIndex) {
            document.getElementById('<%=hdnBindData.ClientID%>').value = "true";
            if (tabIndex == "1") {
                $('#hotelTab').removeClass('active');
                $('#internationalTab').addClass('active');
                $('#liHotel').removeClass('active');
                $('#liInternational').addClass('active');
                document.getElementById('<%=hdnProductType.ClientID %>').value = "1";
                document.forms[0].submit();
            }
            else {
                 $('#internationalTab').removeClass('active');
                $('#hotelTab').addClass('active');
                $('#liInternational').removeClass('active');
                $('#liHotel').addClass('active');
                 document.getElementById('<%=hdnProductType.ClientID %>').value = "2";
                 document.forms[0].submit();
            }

        }
        //Set Tab selection as active after post back 
        function SetActiveTab() {
            
            if (document.getElementById('<%=hdnProductType.ClientID %>').value == "2") {
                $('#internationalTab').removeClass('active');
                $('#hotelTab').addClass('active');
                $('#divInternationalTrip').removeClass('active');
                $('#divHotelTab').addClass('active');
                $('#liInternational').removeClass('active');
                $('#liHotel').addClass('active');
            }
            else {
                $('#hotelTab').removeClass('active');
                $('#internationalTab').addClass('active');
                $('#divHotelTab').removeClass('active');
                $('#divInternationalTrip').addClass('active');
                $('#liHotel').removeClass('active');
                $('#liInternational').addClass('active');
            }
        }

    </script>

    <script type="text/javascript">
        $(document).ready(function () {
             //Set Tab selection in initially.
            SetActiveTab();
        })
    </script>
    <br />

    <h2 style="color: #60c231;
    text-align: center;
    font-weight: bold;
    font-size: 15px;
    text-decoration: underline;
    text-transform: uppercase;">
        Trip Details Queue</h2>
   <br />
        
    <div class="col-md-12">
        <div class="col-md-12 CorpTrvl-tabbed-panel">
            <!--Start : Nav tabs -->
            <ul class="nav nav-tabs responsive" role="tablist">
                <li class="" role="presentation" id="liInternational"><a href="#InternationalTrip" aria-controls="destinations-policy" id="internationalTab"
                    role="tab" data-toggle="tab" aria-expanded="true" onclick="SetTabSelection(1);" >Flight</a></li>
                <li style="display:none;"role="presentation" class=""><a href="#LocalTrip" aria-controls="air-policy" id="localTab"
                    role="tab" data-toggle="tab" aria-expanded="false" >Local</a></li>
                <li role="presentation" class="" id="liHotel"><a href="#HotelTab" aria-controls="air-policy" id="hotelTab"
                    role="tab" data-toggle="tab" aria-expanded="false" onclick="SetTabSelection(2);">Hotel</a></li>
            </ul>
            <!-- End: Nav tabs -->
            <div id="errMess" style="display: none; color: Red; font-weight: bold; text-align: center;">
            </div>
            <div class="tab-content responsive">
                <asp:HiddenField ID="hdnProductType" runat="server" Value="1" />
                <asp:HiddenField ID="hdnBindData" runat="server" Value="false" />
                <div role="tabpanel" class="tab-pane active" id="divInternationalTrip">
                    <div style="background: #fff; padding: 10px;">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>
                                        From Date <span class="fcol_red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                        <uc1:DateControl ID="dcReimFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                        </uc1:DateControl>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>
                                        To Date <span class="fcol_red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                        <uc1:DateControl ID="dcReimToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>
                                        Select Employee</label>
                                    <asp:DropDownList CssClass="form-control" ID="ddlEmployee" runat="server">
                                        <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <%--<div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Select BookingType</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlBookingType" runat="server">
                                    <asp:ListItem Selected="True" Value="Normal" Text="Normal"></asp:ListItem>
                                    <asp:ListItem Value="Routing" Text="Routing"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>--%>
                        <div class="col-md-2">
                            <label class="center-block">
                                &nbsp;</label>
                            <asp:Button OnClientClick="return Validate();" runat="server" ID="btnSearch" OnClick="btnSearch_Click"
                                CssClass="btn but_d btn_xs_block cursor_point mar-5" Text="Search" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="CorpTrvl-tabbed-panel">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#Pending" aria-controls="Pending"
                                        role="tab" data-toggle="tab" aria-expanded="true">Pending</a></li>
                                    <li role="presentation" class=""><a href="#Approved" aria-controls="Approved" role="tab"
                                        data-toggle="tab" aria-expanded="false">Approved</a></li>
                                    <li role="presentation" class=""><a href="#Rejected" aria-controls="Rejected" role="tab"
                                        data-toggle="tab" aria-expanded="false">Rejected</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="Pending"  style="overflow-x:scroll">
                                        <div class="table table-responsive mb-0">
                                        <asp:GridView OnRowDataBound ="Item_Bound" ID="gvPending" Width="100%" runat="server" AllowPaging="true" DataKeyNames="TRIP_ID"
                                            EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom"
                                            GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvPending_PageIndexChanging">
                                            <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Details">
                                                    <ItemTemplate>
                                                        <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server"  Text='View Details' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtEMP_ID" Width="80px" HeaderText="Employee ID" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblEMP_ID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("EMP_ID") %>' Width="80px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtEMP_NAME" Width="100px" HeaderText="Employee Name" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblEMP_NAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("EMP_NAME") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTRIP_ID" Width="150px" HeaderText="Reference No" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTRIP_ID" runat="server" Text='<%# Eval("UNIQUE_TRIP_ID") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("UNIQUE_TRIP_ID") %>' Width="150px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtBOOKING_DATE" Width="100px" HeaderText="Booking Date" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblBOOKING_DATE" runat="server" Text='<%# Eval("BOOKING_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("BOOKING_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTRAVEL_DATE" Width="100px" HeaderText="Travel Date" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTRAVEL_DATE" runat="server" Text='<%# Eval("TRAVEL_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("TRAVEL_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtRETURN_DATE" Width="100px" HeaderText="Return Date" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblRETURN_DATE" runat="server" Text='<%# Eval("RETURN_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("RETURN_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtAIRLINE_CODE" Width="200px" HeaderText="Airline" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblAIRLINE_CODE" runat="server" Text='<%# Eval("AIRLINE_CODE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("AIRLINE_CODE") %>' Width="200px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtROUTE" Width="300px" HeaderText="Route" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblROUTE" runat="server" Text='<%# Eval("ROUTE") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("ROUTE") %>' Width="300px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtREASON_OF_TRAVEL" Width="100px" HeaderText="Reason Of Travel"
                                                            CssClass="form-control" OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblREASON_OF_TRAVEL" runat="server" Text='<%# Eval("REASON_OF_TRAVEL") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("REASON_OF_TRAVEL") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTOTAL_FARE" Width="100px" HeaderText="Fare" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTOTAL_FARE" runat="server" Text='<%# Eval("BOOKING_TYPE") == "Routing" ? GetRoundedAmount(Convert.ToDecimal(Eval("TOTAL_FARE")), Eval("Source").ToString()) : Eval("TOTAL_FARE") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("BOOKING_TYPE") == "Routing" ? GetRoundedAmount(Convert.ToDecimal(Eval("TOTAL_FARE")), Eval("Source").ToString()) : Eval("TOTAL_FARE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                            </Columns>
                                        </asp:GridView>

                                        </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="Approved"  style="overflow-x:scroll">
                                         <div class="table table-responsive mb-0">
                                        <asp:GridView OnRowDataBound ="Item_Bound" ID="gvApproved" Width="100%" runat="server" AllowPaging="true" DataKeyNames="TRIP_ID"
                                            EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom"
                                            GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvApproved_PageIndexChanging">
                                            <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Details">
                                                    <ItemTemplate>
                                                        <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server"  Text='View Details' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtEMP_ID" Width="80px" HeaderText="Employee ID" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblEMP_ID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("EMP_ID") %>' Width="80px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtEMP_NAME" Width="100px" HeaderText="Employee Name" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblEMP_NAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("EMP_NAME") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTRIP_ID" Width="150px" HeaderText="Reference No" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTRIP_ID" runat="server" Text='<%# Eval("UNIQUE_TRIP_ID") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("UNIQUE_TRIP_ID") %>' Width="150px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtBOOKING_DATE" Width="100px" HeaderText="Booking Date" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblBOOKING_DATE" runat="server" Text='<%# Eval("BOOKING_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("BOOKING_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTRAVEL_DATE" Width="100px" HeaderText="Travel Date" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTRAVEL_DATE" runat="server" Text='<%# Eval("TRAVEL_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("TRAVEL_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtRETURN_DATE" Width="100px" HeaderText="Return Date" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblRETURN_DATE" runat="server" Text='<%# Eval("RETURN_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("RETURN_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtAIRLINE_CODE" Width="200px" HeaderText="Airline" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblAIRLINE_CODE" runat="server" Text='<%# Eval("AIRLINE_CODE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("AIRLINE_CODE") %>' Width="200px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtROUTE" Width="300px" HeaderText="Route" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblROUTE" runat="server" Text='<%# Eval("ROUTE") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("ROUTE") %>' Width="300px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtREASON_OF_TRAVEL" Width="100px" HeaderText="Reason Of Travel"
                                                            CssClass="form-control" OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblREASON_OF_TRAVEL" runat="server" Text='<%# Eval("REASON_OF_TRAVEL") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("REASON_OF_TRAVEL") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTOTAL_FARE" Width="100px" HeaderText="Fare" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                         <asp:Label ID="ITlblTOTAL_FARE" runat="server" Text='<%# Eval("BOOKING_TYPE") == "Routing" ? GetRoundedAmount(Convert.ToDecimal(Eval("TOTAL_FARE")), Eval("Source").ToString()) : Eval("TOTAL_FARE") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("BOOKING_TYPE") == "Routing" ? GetRoundedAmount(Convert.ToDecimal(Eval("TOTAL_FARE")), Eval("Source").ToString()) : Eval("TOTAL_FARE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                            </Columns>
                                        </asp:GridView>
                                             </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="Rejected"  style="overflow-x:scroll">
                                         <div class="table table-responsive mb-0">
                                        <asp:GridView OnRowDataBound ="Item_Bound" ID="gvRejected" Width="100%" runat="server" AllowPaging="true" DataKeyNames="TRIP_ID"
                                            EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10" PagerSettings-Mode="NumericFirstLast" PagerSettings-Position="TopAndBottom"
                                            GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvRejected_PageIndexChanging">
                                            <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                            <Columns>
                                                <asp:TemplateField HeaderText="Details">
                                                    <ItemTemplate>
                                                        <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server"  Text='View Details' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtEMP_ID" Width="100px" HeaderText="Employee ID" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblEMP_ID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("EMP_ID") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtEMP_NAME" Width="100px" HeaderText="Employee Name" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblEMP_NAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("EMP_NAME") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTRIP_ID" Width="150px" HeaderText="Reference No" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTRIP_ID" runat="server" Text='<%# Eval("UNIQUE_TRIP_ID") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("UNIQUE_TRIP_ID") %>' Width="150px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtBOOKING_DATE" Width="100px" HeaderText="Booking Date" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblBOOKING_DATE" runat="server" Text='<%# Eval("BOOKING_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("BOOKING_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTRAVEL_DATE" Width="100px" HeaderText="Travel Date" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTRAVEL_DATE" runat="server" Text='<%# Eval("TRAVEL_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("TRAVEL_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtRETURN_DATE" Width="100px" HeaderText="Return Date" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblRETURN_DATE" runat="server" Text='<%# Eval("RETURN_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("RETURN_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtAIRLINE_CODE" Width="200px" HeaderText="Airline" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblAIRLINE_CODE" runat="server" Text='<%# Eval("AIRLINE_CODE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("AIRLINE_CODE") %>' Width="200px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtROUTE" Width="100px" HeaderText="Route" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblROUTE" runat="server" Text='<%# Eval("ROUTE") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("ROUTE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtREASON_OF_TRAVEL" Width="100px" HeaderText="Reason Of Travel"
                                                            CssClass="form-control" OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblREASON_OF_TRAVEL" runat="server" Text='<%# Eval("REASON_OF_TRAVEL") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("REASON_OF_TRAVEL") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTOTAL_FARE" Width="100px" HeaderText="Fare" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                         <asp:Label ID="ITlblTOTAL_FARE" runat="server" Text='<%# Eval("BOOKING_TYPE") == "Routing" ? GetRoundedAmount(Convert.ToDecimal(Eval("TOTAL_FARE")), Eval("Source").ToString()) : Eval("TOTAL_FARE")  %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("BOOKING_TYPE") == "Routing" ? GetRoundedAmount(Convert.ToDecimal(Eval("TOTAL_FARE")), Eval("Source").ToString()) : Eval("TOTAL_FARE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                            </Columns>
                                        </asp:GridView>
                                             </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
             
                <div role="tabpanel" class="tab-pane" id="LocalTrip">
                <div style="background: #fff; padding: 10px;">
                    <div class="row">
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    From Date <span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcLocalTripFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                    </uc1:DateControl>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    To Date <span class="fcol_red">*</span></label>
                                <div class="input-group">
                                    <div class="input-group-addon">
                                        <span class="glyphicon glyphicon-calendar"></span>
                                    </div>
                                    <uc1:DateControl ID="dcLocalTripToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                </div>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <div class="form-group">
                                <label>
                                    Select Employee</label>
                                <asp:DropDownList CssClass="form-control" ID="ddlLocalTripEmployees" runat="server">
                                    <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div class="col-md-2">
                            <label class="center-block">
                                &nbsp;</label>
                            <asp:Button OnClientClick="return Validate();" runat="server" ID="btnLocalTripSearch" OnClick="btnLocalTripSearch_Click"
                                CssClass="btn but_d btn_xs_block cursor_point mar-5" Text="Search" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="CorpTrvl-tabbed-panel">
                                <!-- Nav tabs -->
                                <ul class="nav nav-tabs" role="tablist">
                                    <li role="presentation" class="active"><a href="#LTPending" aria-controls="Pending"
                                        role="tab" data-toggle="tab" aria-expanded="true">Pending</a></li>
                                    <li role="presentation" class=""><a href="#LTApproved" aria-controls="Approved" role="tab"
                                        data-toggle="tab" aria-expanded="false">Approved</a></li>
                                    <li role="presentation" class=""><a href="#LTRejected" aria-controls="Rejected" role="tab"
                                        data-toggle="tab" aria-expanded="false">Rejected</a></li>
                                </ul>
                                <!-- Tab panes -->
                                <div class="tab-content">
                                    <div role="tabpanel" class="tab-pane active" id="LTPending">
                                         <div class="table table-responsive mb-0">
                                        <asp:GridView ID="gvLTPending" Width="100%" runat="server" AllowPaging="true" DataKeyNames="DETAIL_ID"
                                            EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                            GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvLTPending_PageIndexChanging">
                                            <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                        <cc1:Filter ID="HTtxtEMP_ID" Width="100px" HeaderText="EMPLOYEE ID" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblEMP_ID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("EMP_ID") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtEMP_NAME" Width="70px" HeaderText="EMPLOYEE NAME" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblEMP_NAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("EMP_NAME") %>' Width="70px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtCOUNTRY" Width="70px" HeaderText="COUNTRY" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblCOUNTRY" runat="server" Text='<%# Eval("COUNTRY") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("COUNTRY") %>' Width="70px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtCITY" Width="70px" HeaderText="CITY" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblCITY" runat="server" Text='<%# Eval("CITY") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("CITY") %>' Width="70px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTRAVEL_DATE" Width="70px" HeaderText="TRAVEL_DATE" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTRAVEL_DATE" runat="server" Text='<%# Eval("TRAVEL_DATE") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("TRAVEL_DATE") %>' Width="70px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                
                                                
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtAPPROVALSTATUS" Width="70px" HeaderText="APPROVAL STATUS" CssClass="form-control"
                                                            OnClick="FilterSearch1_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblAPPROVALSTATUS" runat="server" Text='<%# Eval("APPROVALSTATUS") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("APPROVALSTATUS") %>' Width="70px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                                                                  
                                                <asp:TemplateField HeaderText="Details">
                                                    <ItemTemplate>
                                                        <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# string.Format("~/CorporateTripSummary.aspx?detailID={0}&empId={1}&empName={2}&status=P&Category=L",
Eval("DETAIL_ID"),Eval("EMP_ID"),Eval("EMP_NAME")) %>' Text='View Details' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                         </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="LTApproved">
                                         <div class="table table-responsive mb-0">
                                        <asp:GridView ID="gvLTApproved" Width="100%" runat="server" AllowPaging="true" DataKeyNames="TRIP_ID"
                                            EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                            GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvLTPending_PageIndexChanging">
                                            <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtEMP_ID" Width="100px" HeaderText="Employee ID" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblEMP_ID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("EMP_ID") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtEMP_NAME" Width="70px" HeaderText="Employee Name" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblEMP_NAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("EMP_NAME") %>' Width="70px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTRIP_ID" Width="100px" HeaderText="Reference No" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTRIP_ID" runat="server" Text='<%# Eval("TRIP_ID") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("TRIP_ID") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtBOOKING_DATE" Width="100px" HeaderText="Booking Date" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblBOOKING_DATE" runat="server" Text='<%# Eval("BOOKING_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("BOOKING_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTRAVEL_DATE" Width="100px" HeaderText="Travel Date" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTRAVEL_DATE" runat="server" Text='<%# Eval("TRAVEL_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("TRAVEL_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtAIRLINE_CODE" Width="100px" HeaderText="Airline" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblAIRLINE_CODE" runat="server" Text='<%# Eval("AIRLINE_CODE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("AIRLINE_CODE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtROUTE" Width="100px" HeaderText="Route" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblROUTE" runat="server" Text='<%# Eval("ROUTE") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("ROUTE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtREASON_OF_TRAVEL" Width="100px" HeaderText="Reason Of Travel"
                                                            CssClass="form-control" OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblREASON_OF_TRAVEL" runat="server" Text='<%# Eval("REASON_OF_TRAVEL") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("REASON_OF_TRAVEL") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTOTAL_FARE" Width="100px" HeaderText="Fare" CssClass="form-control"
                                                            OnClick="FilterSearch2_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTOTAL_FARE" runat="server" Text='<%# Eval("TOTAL_FARE") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("TOTAL_FARE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Details">
                                                    <ItemTemplate>
                                                        <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# string.Format("~/CorporateTripSummary.aspx?flightId={0}&empId={1}&empName={2}&status=A",
Eval("TRIP_ID"),Eval("EMP_ID"),Eval("EMP_NAME")) %>' Text='View Details' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                               </div>
                                    </div>
                                    <div role="tabpanel" class="tab-pane" id="LTRejected">
                                         <div class="table table-responsive mb-0">
                                        <asp:GridView ID="gvLTRejected" Width="100%" runat="server" AllowPaging="true" DataKeyNames="TRIP_ID"
                                            EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                            GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvLTRejected_PageIndexChanging">
                                            <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                            <Columns>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtEMP_ID" Width="100px" HeaderText="Employee ID" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblEMP_ID" runat="server" Text='<%# Eval("EMP_ID") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("EMP_ID") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtEMP_NAME" Width="70px" HeaderText="Employee Name" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblEMP_NAME" runat="server" Text='<%# Eval("EMP_NAME") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("EMP_NAME") %>' Width="70px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTRIP_ID" Width="100px" HeaderText="Reference No" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTRIP_ID" runat="server" Text='<%# Eval("TRIP_ID") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("TRIP_ID") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtBOOKING_DATE" Width="100px" HeaderText="Booking Date" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblBOOKING_DATE" runat="server" Text='<%# Eval("BOOKING_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("BOOKING_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTRAVEL_DATE" Width="100px" HeaderText="Travel Date" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTRAVEL_DATE" runat="server" Text='<%# Eval("TRAVEL_DATE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("TRAVEL_DATE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtAIRLINE_CODE" Width="100px" HeaderText="Airline" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblAIRLINE_CODE" runat="server" Text='<%# Eval("AIRLINE_CODE") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("AIRLINE_CODE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtROUTE" Width="100px" HeaderText="Route" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblROUTE" runat="server" Text='<%# Eval("ROUTE") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("ROUTE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtREASON_OF_TRAVEL" Width="100px" HeaderText="Reason Of Travel"
                                                            CssClass="form-control" OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblREASON_OF_TRAVEL" runat="server" Text='<%# Eval("REASON_OF_TRAVEL") %>'
                                                            CssClass="label grdof" ToolTip='<%# Eval("REASON_OF_TRAVEL") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField>
                                                    <HeaderStyle HorizontalAlign="Left" />
                                                    <HeaderTemplate>
                                                        <cc1:Filter ID="HTtxtTOTAL_FARE" Width="100px" HeaderText="Fare" CssClass="form-control"
                                                            OnClick="FilterSearch3_Click" runat="server" />
                                                    </HeaderTemplate>
                                                    <ItemTemplate>
                                                        <asp:Label ID="ITlblTOTAL_FARE" runat="server" Text='<%# Eval("TOTAL_FARE") %>' CssClass="label grdof"
                                                            ToolTip='<%# Eval("TOTAL_FARE") %>' Width="100px"></asp:Label>
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                                <asp:TemplateField HeaderText="Details">
                                                    <ItemTemplate>
                                                        <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# string.Format("~/CorporateTripSummary.aspx?flightId={0}&empId={1}&empName={2}&status=R",
Eval("TRIP_ID"),Eval("EMP_ID"),Eval("EMP_NAME")) %>' Text='View Details' />
                                                    </ItemTemplate>
                                                </asp:TemplateField>
                                            </Columns>
                                        </asp:GridView>
                                               </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                </div>
             
                <div role="tabpanel" class="tab-pane" id="divHotelTab">
                    <div style="background: #fff; padding: 10px;">
                        <div class="row">
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>
                                        From Date <span class="fcol_red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                        <uc1:DateControl ID="dcHotelFromDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True">
                                        </uc1:DateControl>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>
                                        To Date <span class="fcol_red">*</span></label>
                                    <div class="input-group">
                                        <div class="input-group-addon">
                                            <span class="glyphicon glyphicon-calendar"></span>
                                        </div>
                                        <uc1:DateControl ID="dcHotelToDate" runat="server" DateFormat="DDMMYYYY" DateOnly="True" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <div class="form-group">
                                    <label>
                                        Select Employee</label>
                                    <asp:DropDownList CssClass="form-control" ID="ddlHtlEmployees" runat="server">
                                        <asp:ListItem Selected="True" Value="0" Text="Select"></asp:ListItem>
                                    </asp:DropDownList>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <label class="center-block">
                                    &nbsp;</label>
                                <asp:Button OnClientClick="return ValidateHotelTab();" runat="server" ID="btnHotelSearch"  OnClick="btnHotelSearch_Click"
                                    CssClass="btn but_d btn_xs_block cursor_point mar-5" Text="Search" />
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="CorpTrvl-tabbed-panel">
                                    <!-- Nav tabs -->
                                    <ul class="nav nav-tabs" role="tablist">
                                        <li role="presentation" class="active"><a href="#HTPending" aria-controls="Pending"
                                            role="tab" data-toggle="tab" aria-expanded="true">Pending</a></li>
                                        <li role="presentation" class=""><a href="#HTApproved" aria-controls="Approved" role="tab"
                                            data-toggle="tab" aria-expanded="false">Approved</a></li>
                                        <li role="presentation" class=""><a href="#HTRejected" aria-controls="Rejected" role="tab"
                                            data-toggle="tab" aria-expanded="false">Rejected</a></li>
                                    </ul>
                                    <!-- Tab panes -->
                                    <div class="tab-content">
                                        <div role="tabpanel" class="tab-pane active" id="HTPending">
                                            <div class="table table-responsive mb-0">
                                                <asp:GridView ID="gvHTPending" Width="100%" runat="server" AllowPaging="true" DataKeyNames="ROWID"
                                                    EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                                    GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvHTPending_PageIndexChanging" >
                                                    <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                                    <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                                    <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtEmpId" Width="100px" HeaderText="Employee Id" CssClass="form-control"
                                                                      OnClick="HotelFilterSearch1_Click"  runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblEmpId" runat="server" Text='<%# Eval("EmpId") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("EmpId") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtEmpName" Width="70px" HeaderText="Employee Name" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblEmpname" runat="server" Text='<%# Eval("EmpName") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("EmpName") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtRefenceNo" Width="70px" HeaderText="Reference No" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblReferenceNo" runat="server" Text='<%# Eval("Reference_No") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("Reference_No") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtBookingDate" Width="70px" HeaderText="booking Date" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblBookingDate" runat="server" Text='<%# Eval("BookingDate") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("BookingDate") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtCheckinDate" Width="70px" HeaderText="Checkin Date" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblCheckinDate" runat="server" Text='<%# Eval("CheckinDate") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("CheckinDate") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>


                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtCheckoutDate" Width="70px" HeaderText="Checkout Date" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblCheckoutDate" runat="server" Text='<%# Eval("CheckoutDate") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("CheckoutDate") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                         <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtHotelName" Width="70px" HeaderText="Hotel Name" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblHotelname" runat="server" Text='<%# Eval("HotelName") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("HotelName") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                         <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtCity" Width="70px" HeaderText="City" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblCity" runat="server" Text='<%# Eval("City") %>' CssClass="label grdof" style="text-align: left"
                                                                    ToolTip='<%# Eval("City") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                         <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtReasonOfTravel" Width="70px" HeaderText="Reason Of Travel" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblReasonOfTravel" runat="server" Text='<%# Eval("ReasonOfTravel") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("ReasonOfTravel") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                         <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtPrice" Width="100px" HeaderText="Price" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch1_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblPrice" runat="server" Text='<%# GetPriceRoundOff(Eval("Price")) %>' CssClass="label grdof"
                                                                    ToolTip='<%# GetPriceRoundOff(Eval("Price")) %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Details">
                                                            <ItemTemplate>
                                                                <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# string.Format("~/CorpHotelSummary.aspx?hotelId={0}&empId={1}&empName={2}&status=P&Category=L&ConfNo={3}",
Eval("HotelId"),Eval("EmpId"),Eval("EmpName"),Eval("confirmationNo")) %>'
                                                                    Text='View Details' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="HTApproved">
                                            <div class="table table-responsive mb-0">
                                                <asp:GridView ID="gvHTApproved" Width="100%" runat="server" AllowPaging="true" DataKeyNames="ROWID"
                                                    EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                                    GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvHTApproved_PageIndexChanging" >
                                                    <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                                    <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                                    <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtEmpId" Width="100px" HeaderText="Employee ID" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch2_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblEmpId" runat="server" Text='<%# Eval("EmpId") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("EmpId") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtEmpName" Width="70px" HeaderText="Employee Name" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch2_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblEmpName" runat="server" Text='<%# Eval("EmpName") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("EmpName") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtReferenceNo" Width="100px" HeaderText="Reference No" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch2_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblRefenecNo" runat="server" Text='<%# Eval("Reference_No") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("Reference_No") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtBookingDate" Width="100px" HeaderText="Booking Date" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch2_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblBookingDate" runat="server" Text='<%# Eval("BookingDate") %>'
                                                                    CssClass="label grdof" ToolTip='<%# Eval("BookingDate") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtChekckinDate" Width="100px" HeaderText="Checkin Date" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch2_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblCheckinDate" runat="server" Text='<%# Eval("CheckinDate") %>'
                                                                    CssClass="label grdof" ToolTip='<%# Eval("CheckinDate") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxCheckoutDate" Width="100px" HeaderText="CheckoutDate" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch2_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblCheckoutDate" runat="server" Text='<%# Eval("CheckoutDate") %>'
                                                                    CssClass="label grdof" ToolTip='<%# Eval("CheckoutDate") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxthotelName" Width="100px" HeaderText="HotelName" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch2_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlbHotelName" runat="server" Text='<%# Eval("HotelName") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("HotelName") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtCity" Width="100px" HeaderText="City"
                                                                    CssClass="form-control" OnClick="HotelFilterSearch2_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblCity" runat="server" Text='<%# Eval("City") %>' style="text-align: left"
                                                                    CssClass="label grdof" ToolTip='<%# Eval("City") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtReasonOfTravel" Width="100px" HeaderText="Reason of travel" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch2_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblReasonofTravel" runat="server" Text='<%# Eval("ReasonOfTravel") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("ReasonOfTravel") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                         <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtPrice" Width="100px" HeaderText="Price" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch2_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblPrice" runat="server" Text='<%# GetPriceRoundOff(Eval("Price")) %>'  CssClass="label grdof"
                                                                    ToolTip='<%# GetPriceRoundOff(Eval("Price")) %>'   Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>

                                                        <asp:TemplateField HeaderText="Details">
                                                            <ItemTemplate>           
                                                                <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# string.Format("~/CorpHotelSummary.aspx?hotelId={0}&empId={1}&empName={2}&status=A&Category=L&ConfNo={3}",
Eval("HotelId"),Eval("EmpId"),Eval("EmpName"),Eval("confirmationNo")) %>'
                                                                    Text='View Details' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                        <div role="tabpanel" class="tab-pane" id="HTRejected">
                                            <div class="table table-responsive mb-0">
                                                <asp:GridView ID="gvHTRejected" Width="100%" runat="server" AllowPaging="true" DataKeyNames="ROWID"
                                                    EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
                                                    GridLines="none" CssClass="b2b-corp-table table table-bordered table-condensed" CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvHTRejected_PageIndexChanging" >
                                                    <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
                                                    <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                                    <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                                    <Columns>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtEmpId" Width="100px" HeaderText="Employee ID" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch3_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblempId" runat="server" Text='<%# Eval("EmpId") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("EmpId") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtEmpName" Width="70px" HeaderText="Employee Name" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch3_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblEmpName" runat="server" Text='<%# Eval("EmpName") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("EmpName") %>' Width="70px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtRefereneNo" Width="100px" HeaderText="Reference No" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch3_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblReferenceNo" runat="server" Text='<%# Eval("Reference_No") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("Reference_No") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtBookingDate" Width="100px" HeaderText="Booking Date" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch3_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblBookingDate" runat="server" Text='<%# Eval("BookingDate") %>'
                                                                    CssClass="label grdof" ToolTip='<%# Eval("BookingDate") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtCheckinDate" Width="100px" HeaderText="Checkin Date" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch3_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblcheckinDate" runat="server" Text='<%# Eval("CheckinDate") %>'
                                                                    CssClass="label grdof" ToolTip='<%# Eval("CheckinDate") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtCheckoutDate" Width="100px" HeaderText="CheckoutDate" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch3_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblCheckoutDate" runat="server" Text='<%# Eval("CheckoutDate") %>'
                                                                    CssClass="label grdof" ToolTip='<%# Eval("CheckoutDate") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtHotelName" Width="100px" HeaderText="Hotel Name" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch3_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblHoteName" runat="server" Text='<%# Eval("HotelName") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("HotelName") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtCity" Width="100px" HeaderText="City"
                                                                    CssClass="form-control" OnClick="HotelFilterSearch3_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblCity" runat="server" Text='<%# Eval("City") %>' style="text-align: left"
                                                                    CssClass="label grdof" ToolTip='<%# Eval("City") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtReasonofTravel" Width="100px" HeaderText="Reason of Travel" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch3_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblReasonofTravel" runat="server" Text='<%# Eval("ReasonOfTravel") %>' CssClass="label grdof"
                                                                    ToolTip='<%# Eval("Price") %>' Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField>
                                                            <HeaderStyle HorizontalAlign="Left" />
                                                            <HeaderTemplate>
                                                                <cc1:Filter ID="HTtxtPrice" Width="100px" HeaderText="Price" CssClass="form-control"
                                                                    OnClick="HotelFilterSearch3_Click" runat="server" />
                                                            </HeaderTemplate>
                                                            <ItemTemplate>
                                                                <asp:Label ID="ITlblPrice" runat="server" Text='<%# GetPriceRoundOff(Eval("Price")) %>' CssClass="label grdof"
                                                                    ToolTip='<%# GetPriceRoundOff(Eval("Price")) %>'  Width="100px"></asp:Label>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:TemplateField HeaderText="Details">
                                                            <ItemTemplate>
                                                                <asp:HyperLink Target="_blank" ID="hlinkDetails" runat="server" NavigateUrl='<%# string.Format("~/CorpHotelSummary.aspx?hotelId={0}&empId={1}&empName={2}&status=R&Category=L&ConfNo={3}",
Eval("HotelId"),Eval("EmpId"),Eval("EmpName"),Eval("confirmationNo")) %>'
                                                                    Text='View Details' />
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                    </Columns>
                                                </asp:GridView>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>
    
</asp:Content>
