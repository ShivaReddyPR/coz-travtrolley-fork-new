﻿using System;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using System.Collections.Generic;
using CT.Core;
using System.Threading;
using CT.MetaSearchEngine;
using System.Net;

public partial class CorpEmailAjax : System.Web.UI.Page
{
    protected bool isBookingSuccess = false;
    protected List<Ticket> ticketList;
    protected FlightItinerary flightItinerary;
    protected BookingDetail booking;
    protected string inBaggage = "", outBaggage = "";
    protected CT.Core.Airline airline;
    protected string AirlineLogoPath = ConfigurationManager.AppSettings["AirlineLogoPath"];
    protected AgentMaster agency;
    protected decimal charges = 0;
    protected int emailCounter = 1;//Checking for Agent details display in Email body
    protected AutoResetEvent[] BookingEvents = new AutoResetEvent[2];
    protected int agencyId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["Result"] != null && Session["FlightRequest"] != null && Session["TripId"] != null && Session["CriteriaItineraries"] != null && Session["sessionId"] != null && Session["CorpBookingResponses"] != null)
            {
                SearchRequest request = null;

                request = Session["FlightRequest"] as SearchRequest;

                FlightItinerary itinerary = null;
                //HOLD remaining TWO criteria results for Corporate profile
                //TODO: Ziya
                if (Settings.LoginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId > 0 && request.AppliedPolicy)
                {
                    BookingResponse corpBookingResponse = new BookingResponse();
                    Dictionary<FlightItinerary, BookingResponse> BookingResponses = Session["CorpBookingResponses"] as Dictionary<FlightItinerary, BookingResponse>;

                    foreach (KeyValuePair<FlightItinerary, BookingResponse> pair in BookingResponses)
                    {
                        itinerary = pair.Key;
                    }

                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                        agencyId = Settings.LoginInfo.OnBehalfAgentID;
                    }
                    else
                    {
                        agency = new AgentMaster(Settings.LoginInfo.AgentId);
                        agencyId = Settings.LoginInfo.AgentId;
                    }
                    MetaSearchEngine mse = new MetaSearchEngine(Session["sessionId"].ToString());
                    mse.SettingsLoginInfo = Settings.LoginInfo;


                    if (Session["CriteriaItineraries"] != null)
                    {
                        List<FlightItinerary> CriteriaItineries = Session["CriteriaItineraries"] as List<FlightItinerary>;
                        Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();
                        //Book other optional bookings simultaneously

                        listOfThreads.Add("0", new WaitCallback(MakeCorporateBookings));
                        listOfThreads.Add("1", new WaitCallback(MakeCorporateBookings));


                        int i = 0;
                        foreach (FlightItinerary fi in CriteriaItineries)
                        {
                            flightItinerary = fi;
                            //Audit.Add(EventType.Exception, Severity.High, 0, "(CorpBooking)Additional Booking Source " + (i + 1) + " : " + flightItinerary.FlightBookingSource.ToString(), Request["REMOTE_ADDR"]);
                            if (!flightItinerary.IsLCC)
                            {
                                if (flightItinerary.FlightBookingSource == BookingSource.AirArabia)
                                {
                                    CalculateBaggageFare(flightItinerary, i);
                                }
                                flightItinerary.TripId = Session["TripId"].ToString();//Set the trip id for remaining flights
                                if (flightItinerary.FlightBookingSource != BookingSource.AirArabia)
                                {
                                    List<object> Objects = new List<object>();
                                    Objects.Add(flightItinerary);//Add Itinerary
                                    Objects.Add(mse);//Add MSE
                                    Objects.Add(i);//Add ThreadPool Index
                                    Objects.Add(BookingResponses);
                                    LoginInfo loginInfo = Settings.LoginInfo;
                                    Objects.Add(loginInfo);
                                    ThreadPool.QueueUserWorkItem(listOfThreads[i.ToString()], Objects);//Pass Object collection as Paramter to MakeCorporateBookings(...)                                    
                                    BookingEvents[i] = new AutoResetEvent(false);
                                }
                                else
                                {
                                    corpBookingResponse = mse.Book(ref flightItinerary, agencyId, (flightItinerary.IsLCC ? BookingStatus.Ticketed : BookingStatus.Hold), new UserMaster(flightItinerary.CreatedBy), true, Request["REMOTE_ADDR"]);
                                    BookingResponses.Add(flightItinerary, corpBookingResponse);
                                }
                                i++;//increment counter only when result is not LCC
                            }                            
                        }

                        try
                        {
                            if (i != 0 && BookingEvents[0] != null)
                            {
                                //If any thread takes more than 2 minutes stop the thread
                                if (!WaitHandle.WaitAll(BookingEvents, new TimeSpan(0, 1, 0), true))
                                {
                                    //TODO: audit which thread is timed out                
                                }
                            }
                        }
                        catch { }
                    }


                    isBookingSuccess = true;//to send HOLD emails

                    string EmailBody = string.Empty, subject = string.Empty;

                    //Send Corporate Emails for successful bookings
                    foreach (KeyValuePair<FlightItinerary, BookingResponse> pair in BookingResponses)
                    {
                        //If booking is success send email
                        if (pair.Value.Status == BookingResponseStatus.Successful)
                        {
                            //Combine all 3 email bodies in one
                            EmailBody += SendFlightTicketEmail(pair.Key);
                            try
                            {
                                if (pair.Key.FareRules[0].FareRuleDetail != null && itinerary.FlightBookingSource == BookingSource.UAPI)
                                {
                                    //Append the Penalties Fare Rule to the Email Body
                                    EmailBody += "<br/><hr/>Fare Rules :" + pair.Key.FareRules[0].FareRuleDetail.Substring(0, pair.Key.FareRules[0].FareRuleDetail.IndexOf("/p><p") + 3) + "<hr/>";
                                }
                            }
                            catch { }
                            //Combine PNR's
                            if (subject.Length > 0)
                            {
                                subject += "," + pair.Key.PNR;
                            }
                            else
                            {
                                subject = pair.Key.PNR;
                            }
                            emailCounter++;//Increment the counter for hiding the Agent details in Email body
                        }
                        else
                        {
                            //TODO: Failure email
                        }
                    }

                    try
                    {
                        List<string> toArray = new List<string>();
                        toArray.Add(itinerary.Passenger[0].Email);
                        if (!string.IsNullOrEmpty(Settings.LoginInfo.AgentEmail))
                        {
                            toArray.Add(Settings.LoginInfo.AgentEmail);
                        }
                        if (!string.IsNullOrEmpty(Settings.LoginInfo.Email))
                        {
                            toArray.Add(Settings.LoginInfo.Email);
                        }

                        //Send Corporate email
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Booking Confirmation - " + subject, EmailBody, new Hashtable());
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Email, Severity.High, 1, "(CorpBooking)Failed to Send Corporate Emails. Reason : " + ex.ToString(), Request["REMOTE_ADDR"]);
                    }
                    finally
                    {
                        Session["CorpBookingResponses"] = null;
                        Session["CriteriaItineraries"] = null;
                        Session["FlightItinerary"] = null;
                        Session["TripId"] = null;
                        Session.Remove("CriteriaResults");
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Book, Severity.High, 1, "(CorpBooking)Failed to book Cheapest & Shortest Flights. Reason : " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    private void CalculateBaggageFare(FlightItinerary itinerary, int index)
    {
        try
        {
            SearchResult result = (Session["CriteriaResults"] as List<SearchResult>)[index];
            string sessionId = Session["sessionId"].ToString();
            SearchRequest request = Session["FlightRequest"] as SearchRequest;
            if (result.ResultBookingSource == BookingSource.AirArabia)
            {
                //Hold booking is not allowed for G9

                string[] RPH = new string[result.Flights.Length];
                List<string> rph = new List<string>();
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    //for (int j = 0; j < result.Flights[i].Length; j++)
                    {
                        //RPH[i] = result.Flights[i][j].FareInfoKey.ToString() + "|" + result.Flights[i][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[i][j].Airline + result.Flights[i][j].FlightNumber;
                        rph.Add(itinerary.Segments[i].UapiDepartureTime.ToString() + "|" + itinerary.Segments[i].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + itinerary.Segments[i].Airline + itinerary.Segments[i].FlightNumber);
                        //rph.Add(result.Flights[i][j].UapiDepartureTime.ToString());
                    }
                }
                RPH = rph.ToArray();
                int arraySize = 0;

                //if (itinerary.Segments.Length <= 1)
                //{
                //    arraySize = (itinerary.Passenger.Length - request.InfantCount) ;
                //}
                //else
                {
                    arraySize = (itinerary.Passenger.Length - request.InfantCount) * itinerary.Segments.Length;
                }

                string[] paxBaggages = new string[arraySize];
                int count = 0;


                for (int i = 0; i < (itinerary.Passenger.Length - request.InfantCount); i++)
                {
                    if (itinerary.Passenger[i].Type != PassengerType.Infant)
                    {
                        string travelerRPH = "";
                        if (itinerary.Passenger[i].Type == PassengerType.Adult || itinerary.Passenger[i].Type == PassengerType.Senior)
                        {
                            travelerRPH = "A" + (i + 1);
                        }
                        else if (itinerary.Passenger[i].Type == PassengerType.Child)
                        {
                            travelerRPH = "C" + (i + 1);
                        }

                        string bagCode = itinerary.Passenger[i].BaggageCode;
                        for (int k = 0; k < itinerary.Segments.Length; k++)
                        {
                            //for (int j = 0; j < result.Flights[k].Length; j++)
                            {
                                paxBaggages[count] = bagCode.Split(',')[k].Trim() + "|" + travelerRPH + "|" + itinerary.Segments[k].UapiDepartureTime.ToString() + "|" + itinerary.Segments[k].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + itinerary.Segments[k].Airline + itinerary.Segments[k].FlightNumber;
                                count++;
                                //if (itinerary.Segments.Length > 1)
                                //{
                                //    if (itinerary.Segments.Length > 2)
                                //    {
                                //        paxBaggages[count] = bagCode.Split(',')[1].Trim() + "|" + travelerRPH + "|" + RPH[count];
                                //    }
                                //    else
                                //    {
                                //        paxBaggages[count] = bagCode.Split(',')[1].Trim() + "|" + travelerRPH + "|" + RPH[count];
                                //    }
                                //    count++;
                                //}

                            }
                        }
                    }
                }

                CT.BookingEngine.GDS.AirArabia aaObj = new CT.BookingEngine.GDS.AirArabia();
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    aaObj.UserName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].UserID;
                    aaObj.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].Password;
                    aaObj.Code = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].HAP;
                    aaObj.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    aaObj.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                }
                else
                {
                    aaObj.UserName = Settings.LoginInfo.AgentSourceCredentials["G9"].UserID;
                    aaObj.Password = Settings.LoginInfo.AgentSourceCredentials["G9"].Password;
                    aaObj.Code = Settings.LoginInfo.AgentSourceCredentials["G9"].HAP;
                    aaObj.AgentCurrency = Settings.LoginInfo.Currency;
                    aaObj.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                }
                CT.BookingEngine.GDS.AirArabia.SessionData data = (CT.BookingEngine.GDS.AirArabia.SessionData)Basket.BookingSession[sessionId][result.Airline];
                string fareXml = "";
                CookieContainer cookie = new CookieContainer();
                aaObj.AppUserId = Settings.LoginInfo.UserID.ToString();
                aaObj.SessionID = Session["sessionId"].ToString();
                //Modified by Lokesh on 4-April-2018
                Dictionary<string, decimal> PaxBagFares = new Dictionary<string, decimal>();
                //This is for Only G9 Air Source
                //Need to pass the state code(mandatory) and taxRegNo(Not mandatory) only if the customer is travelling from India.
                //string[] baggageFare = aaObj.GetBaggageFare(ref result, request, ref data, ref RPH, ref fareXml, sessionId, ref cookie, ref paxBaggages, itinerary.Passenger[0].GSTStateCode, itinerary.Passenger[0].GSTTaxRegNo, ref PaxBagFares);
                List<string> tmp = new List<string>();
                string[] baggageFare = aaObj.GetBaggageFare(ref result, request, ref data, ref RPH, ref fareXml, sessionId, ref cookie, ref tmp, itinerary.Passenger[0].GSTStateCode, itinerary.Passenger[0].GSTTaxRegNo, ref PaxBagFares);
                data.SelectedResultInfo = fareXml;
                // TodO ziya: update itineray according to last price quote
                // foreach (FlightPassenger pax in itinerary.Passenger)
                //  {
                //     for (int i = 0; i < result.FareBreakdown.Length; i++)
                //     {
                //         if (pax.Type == result.FareBreakdown[i].PassengerType && result.FareBreakdown[i].SupplierFare > 0)
                //        {
                //            pax.Price.SupplierPrice = (decimal)result.FareBreakdown[i].SupplierFare / result.FareBreakdown[i].PassengerCount;
                //        }
                //     }
                //  }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorpBooking)Failed to get Baggage price for G9. Reason " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    /// <summary>
    /// Makes bookings for optional results in case of Corporate profile
    /// </summary>
    void MakeCorporateBookings(object collection)
    {
        FlightItinerary itinerary = null;
        int eventIndex = 0;
        try
        {
            List<object> BookingObjects = collection as List<object>;
            itinerary = BookingObjects[0] as FlightItinerary;

            MetaSearchEngine mse = BookingObjects[1] as MetaSearchEngine;
            eventIndex = Convert.ToInt32(BookingObjects[2]);
            mse.SettingsLoginInfo = BookingObjects[4] as LoginInfo;
            BookingResponse corpBookingResponse = mse.Book(ref itinerary, agencyId, (itinerary.IsLCC ? BookingStatus.Ticketed : BookingStatus.Hold), new UserMaster(itinerary.CreatedBy), true, Request["REMOTE_ADDR"]);
            
            Dictionary<FlightItinerary, BookingResponse> BookingResponses = BookingObjects[3] as Dictionary<FlightItinerary, BookingResponse>;
            //Add the booked response along with Itinerary for sending emails
            BookingResponses.Add(itinerary, corpBookingResponse);

            
            BookingEvents[eventIndex].Set();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorpBooking)Failed to Book Corporate booking - Source :" + itinerary.FlightBookingSource + ". Reason : " + ex.ToString(), "");
            BookingEvents[eventIndex].Set();
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="FlightId"></param>
    protected void BindTicketInfo(int FlightId)
    {
        try
        {
            if (FlightId > 0)
            {
                flightItinerary = new FlightItinerary(FlightId);
                agency = new AgentMaster(flightItinerary.AgencyId);
                booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(FlightId));
                if (booking.Status == BookingStatus.Ticketed)
                {
                    ticketList = Ticket.GetTicketList(FlightId);
                }
            }
            if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai)
            {
                foreach (FlightPassenger pax in flightItinerary.Passenger)
                {
                    if (inBaggage.Length > 0)
                    {
                        inBaggage += ", " + pax.BaggageCode.Split(',')[0];
                    }
                    else
                    {
                        inBaggage = pax.BaggageCode.Split(',')[0];
                    }
                    if (pax.BaggageCode.Split(',').Length > 1)
                    {
                        if (outBaggage.Length > 0)
                        {
                            outBaggage += ", " + pax.BaggageCode.Split(',')[1];
                        }
                        else
                        {
                            outBaggage = pax.BaggageCode.Split(',')[1];
                        }
                    }
                }
            }
            airline = new Airline();
            airline.Load(flightItinerary.ValidatingAirlineCode);

            if (airline.LogoFile.Trim().Length != 0)
            {
                string[] fileExtension = airline.LogoFile.Split('.');
                AirlineLogoPath = AirlineLogoPath + flightItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="itinerary"></param>
    /// <returns></returns>
    protected string SendFlightTicketEmail(FlightItinerary itinerary)
    {
        string myPageHTML = string.Empty;
        try
        {
            BindTicketInfo(itinerary.FlightId);
            string logoPath = "";
            int agentId = 0;

            if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agentId = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.OnBehalfAgentID;
            }
            else
            {
                agentId = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId;
            }

            string serverPath = "";

            if (Request.Url.Port > 0)
            {
                serverPath = Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }
            else
            {
                serverPath = Request.Url.Scheme+"://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }

            if (agentId > 1)
            {
                logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + new AgentMaster(agentId).ImgFileName;
                imgLogo.ImageUrl = logoPath;
            }
            else
            {
                imgLogo.ImageUrl = serverPath + "/images/logo.jpg";
            }



            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            EmailDiv.RenderControl(htw);
            myPageHTML = sw.ToString();
            //myPageHTML = myPageHTML.Replace("\"", "/\"");
            myPageHTML = myPageHTML.Replace("none", "block");

            //System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            //toArray.Add(itinerary.Passenger[0].Email);

            string subject = "Ticket Confirmation - " + itinerary.PNR;

            //if (ViewState["MailSent"] == null)//If booking in not corporate then send email
            {
                //CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable());
                //ViewState["MailSent"] = true;
            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "(CorpBooking)Failed to Send Flight Corporate Booking Email PNR " + itinerary.PNR + ": reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        return myPageHTML;
    }
    
}
