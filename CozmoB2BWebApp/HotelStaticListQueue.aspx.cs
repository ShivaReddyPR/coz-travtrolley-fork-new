﻿using CT.BookingEngine;
using Newtonsoft.Json;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class HotelStaticListQueue :CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (!IsPostBack)
            {

            }

        }
        [WebMethod]
        public static ArrayList BindCountry()
        {
            ArrayList list = new ArrayList();
            DataTable dtLocations = GimmonixStaticData.GetCountryList();
            if (dtLocations != null && dtLocations.Rows.Count > 0)
            {
                foreach (DataRow dr in dtLocations.Rows)
                {
                    list.Add(new ListItem(
              dr["countryName"].ToString(),
              dr["countryCode"].ToString()
               ));
                }
            }
            return list;
        }
        [WebMethod]
        //[ScriptMethod]
        public static string Search(String Country, String HotelId, string HotelName, string CityName, int Rating,string Phone,string Email,string Website)
        {
            try
            {
                string JSONString = string.Empty;
                DataTable dt = GimmonixStaticData.GetSearchDetails(Country,HotelId,HotelName,CityName,Rating,Phone,Email,Website);
                //int count=dt.Rows.Count;
                JSONString = JsonConvert.SerializeObject(dt);
                return JSONString;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        [WebMethod]
        public static string GetUpdateStatus(string id)
        {
            try
            {
                string text = GimmonixStaticData.GetUpdateStatus(Convert.ToInt32(id));
                return text;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
    }
}