

/**********************DB Changes For Meal Implementation in SG and 6E**************/
--Added 2 new columns in BKE_PRICE and BKE_Flight_Passenger
--Modified the necessary proc for insertion into BKE_PRICE and BKE_Flight_Passenger tables.

/**************New Columns List**************/
alter table BKE_PRICE
add mealCharge money null

alter table BKE_Flight_Passenger
add mealDesc nvarchar(500) null


/********************Procedure Changes ********************/

GO
/****** Object:  StoredProcedure [dbo].[usp_AddFlightPassenger]    Script Date: 09/22/2018 21:21:00 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO

ALTER PROCEDURE [dbo].[usp_AddFlightPassenger]

(

@firstName nvarchar(50),

@lastName nvarchar(50) = NULL,

@title NVARCHAR(10) = NULL,

@flightId INT,

@cellPhone NVARCHAR(20) = NULL,

@leadPax BIT,

@dateOfBirth DATETIME,

@paxType CHAR(3),

@passportNumber NVARCHAR(50) = NULL,

@countryCode CHAR(2) = NULL,

@line1 NVARCHAR(100) = NULL,

@line2 NVARCHAR(100) = NULL,

@email NVARCHAR(100) = NULL,

@mealCode CHAR(4) = NULL,

@seatCode CHAR(1) = NULL,

@priceId int,

@ffAirline CHAR(2) = NULL,

@ffNumber NVARCHAR(10) = NULL,

@gender INT=NULL,

@CREATEdBy INT,
@city VARCHAR(50) = NULL,
@nationality CHAR(2)=NULL,
@baggageCode varchar(50)=NULL,
@paxId INT OUTPUT,

@destinationPhone NVARCHAR(20) = NULL,
@gststatecode NVARCHAR(5) = null, -- For G9 Source if the customer is travelling from India and provides any GST info
@gsttaxregno nvarchar(50) = null, -- For G9 Source if the customer is travelling from India and provides any GST info
@stateCode nvarchar(2)=null,
@mealDesc nvarchar(500) = null -- For SG and 6E to save Veg Meal and Non Veg Meal per segment wise.

)



AS

INSERT INTO BKE_FLIGHT_PASSENGER

(flightId, paxType, firstName, lastName, title, dateOfBirth, passportNumber, countryCode, line1, line2, cellPhone, email, leadPax, mealCode, 

seatCode, FFAirlineCode, FFNumber, priceId,gender, city, nationality, CREATEdOn, CREATEdBy, lastModifiedOn, lastModifiedBy,baggageCode,destinationPhone,
GSTStateCode, -- For G9 Source if the customer is travelling from India and provides any GST info
GSTTaxRegNo, -- For G9 Source if the customer is travelling from India and provides any GST info
stateCode,mealDesc
)

VALUES (@flightId,@paxType,@firstName,@lastName,@title,@dateOfBirth,@passportNumber,@countryCode,@line1,@line2,@cellPhone,@email,@leadPax,@mealCode,@seatCode,@ffAirline,@ffNumber,@priceId,@gender,

@city, @nationality, GetUTCDate(),@CREATEdBy, GetUTCDate(),@CREATEdBy,@baggageCode,@destinationPhone,
@gststatecode, -- For G9 Source if the customer is travelling from India and provides any GST info
@gsttaxregno, -- For G9 Source if the customer is travelling from India and provides any GST info
@stateCode
,@mealDesc
)



SET @paxId = SCOPE_IDENTITY();



RETURN




/***** Object: StoredProcedure [dbo].[usp_AddPrice] Script Date: 22-09-2018 15:18:27 *****/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER PROCEDURE [dbo].[usp_AddPrice]
(
@priceId int OUTPUT,
@publishedFare money,
@netFare money,
@markup money=NULL,
@tax money,
@otherCharges money =NULL,
@ourCommission money =NULL,
@ourPLB money =NULL,
@agentCommission money = NULL,
@agentPLB money = NULL,
@tdsCommission money = NULL,
@tdsPLB money = NULL,
@serviceTax money = NULL,
@whiteLabelDiscount money=0,
@transactionFee money,
@currency varchar(5),
@priceType int = 1,
@rateOfExchange money = 1,
@currencyCode varchar(10) = 'AED',
@additionalTxnFee money,
@wlCharge money = 0,
@discount money=0,
@commissionType varchar(2)='RB',
@reverseHandlingCharge money = 0,
@yqTax money=0,
@isServiceTaxOnBaseFarePlusYQ bit=0,
@tdsRate money=0,
@baggageCharge money=0,
@asvAmount money = 0,
@asvType nvarchar(1)= null,
@asvElement nvarchar(5) = null,
@markupValue money=0, 
@markupType varchar(5)=null,
@discountValue money=0,
@discountType varchar(5)=null,
@sourceCurrency varchar(3)=null,
@sourceAmount money=0,
@B2CMarkup money=0,
@B2CMarkupType nvarchar(5)=null,
@B2CMarkupValue money=0,
@IncentiveEarned	money=0,
@TdsOnIncentive	money=0,
@SServiceFee	money=0,
@InputVATAmount money=0,
@OutputVATAmount money=0,
@HandlingFeeType nvarchar(1)=null,
@HandlingFeeValue money=0,
@HandlingFeeAmount money=0,
@mealCharge money=0  -- For SG and 6E Passenger Wise Meal charge is assigned to this variable

)

AS


INSERT INTO BKE_PRICE
(publishedFare,netFare,markup,tax,otherCharges,ourCommission,ourPLB,agentCommission,agentPLB,
tdsCommission,tdsPLB,serviceTax,whiteLabelDiscount,transactionFee,currency, priceType,rateOfExchange,
currencyCode,additionalTxnFee, wlCharge,discount,commissionType,reverseHandlingCharge,yqTax,
isServiceTaxOnBaseFarePlusYQ,tdsRate,baggageCharge,asvamount,asvtype,asvElement,markupValue, markupType ,
discountValue ,discountType, SourceCurrency, SourceAmount, B2CMarkup, B2CMarkupType, B2CMarkupValue,
IncentiveEarned ,TdsOnIncentive ,SServiceFee, INVAT_AMOUNT, OUTVAT_AMOUNT,HandlingFeeType, HandlingFeeValue, HandlingFeeAmount,
mealCharge

)
VALUES 
(@publishedFare,@netFare,@markup,@tax,@otherCharges,@ourCommission,@ourPLB,@agentCommission,@agentPLB,
@tdsCommission,@tdsPLB,@serviceTax,@whiteLabelDiscount,@transactionFee,@currency, @priceType,
@rateOfExchange,@currencyCode,@additionalTxnFee, @wlCharge,@discount,@commissionType,@reverseHandlingCharge,
@yqTax,@isServiceTaxOnBaseFarePlusYQ,@tdsRate,@baggageCharge,@asvAmount,@asvType,@asvElement, @markupValue, 
@markupType ,@discountValue ,@discountType, @sourceCurrency, @sourceAmount, @B2CMarkup, @B2CMarkupType, @B2CMarkupValue,
@IncentiveEarned,@TdsOnIncentive,@SServiceFee, @InputVATAmount, @OutputVATAmount,@HandlingFeeType, @HandlingFeeValue, @HandlingFeeAmount
,@mealCharge
)


set @priceId = @@Identity