﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CT.Core;
using CT.BookingEngine;
using CT.Configuration;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.MetaSearchEngine;

public partial class SightseeingBookingQueue : CT.Core.ParentPage
{
    
    protected SightseeingItinerary[] sightseeingItinerary = new SightseeingItinerary[0];
    protected BookingDetail[] bookingDetail = new BookingDetail[0];
    protected UserMaster loggedMember;
    protected string message = string.Empty;
    //protected int recordsPerPage = 5; // By Default it is 5
    protected int recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["BookingQueueRecordsPerPage"]);
    protected int noOfPages;
    protected int pageNo;
    protected int flag = 0;
    protected string show = string.Empty;
    // protected string pageType = string.Empty; // for sorting with paging
    protected string Cancelled = string.Empty; // For filtering with paging 
    protected string Confirmed = string.Empty; // For filtering with paging 
    protected string lastCanDate = string.Empty;
    protected int agentFilter;// For restricted filtering
    protected string SightseeingFilter = string.Empty; // For restricted filtering
    protected string paxFilter = string.Empty;// For restricted filtering
    protected string pnrFilter = string.Empty;// For restricted filtering
    protected string reqType = "both"; // For filtering with paging
    protected List<int> bookingModes = new List<int>();
    private List<int> shownStatuses = new List<int>();
    protected List<int> sourcesList = new List<int>();
    protected List<int> selectedSources = new List<int>();
    protected bool filtered = false;
    protected int locationId;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            hdfParam.Value = "1";
            Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
            if (Settings.LoginInfo != null)
            {

                Page.Title = "Sightseeing Booking Queue";
                //AuthorizationCheck();

                if (!IsPostBack)
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                    hdfParam.Value = "0";
                    if (Request["FromDate"] != null)
                    {
                        CheckIn.Text = Request["FromDate"];
                    }
                    else
                    {
                        CheckIn.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    }

                    CheckOut.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    sourcesList = SightseeingItinerary.LoadSourceId();
                    selectedSources = sourcesList;
                    InitializeControls();
                    Array Sources = Enum.GetValues(typeof(SightseeingBookingSource));
                    foreach (SightseeingBookingSource source in Sources)
                    {
                        if (source == SightseeingBookingSource.GTA || source==SightseeingBookingSource.CZA)    //Modified by chandan 
                        {
                            ListItem item = new ListItem(Enum.GetName(typeof(SightseeingBookingSource), source), ((int)source).ToString());
                            ddlSource.Items.Add(item);
                        }
                    }
                    Array Statuses = Enum.GetValues(typeof(SightseeingBookingStatus));
                    foreach (SightseeingBookingStatus status in Statuses)
                    {
                        ListItem item = new ListItem(Enum.GetName(typeof(SightseeingBookingStatus), status), ((int)status).ToString());
                        if (item.Text == "Confirmed")
                        {
                            ddlBookingStatus.Items.Insert(2, new ListItem("Vouchered", "1"));
                        }
                        else
                        {
                            ddlBookingStatus.Items.Add(item);
                        }
                    }

                    ddlLocations.SelectedIndex = -1;
                    ddlLocations.Items.FindByText(Settings.LoginInfo.LocationName).Selected = true;
                    pageNo = 1;
                    LoadBookings();
                }


                loggedMember = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
                if (Request["PageNoString"] != null)
                {
                    pageNo = Convert.ToInt16(Request["PageNoString"]);
                }
                else
                {
                    pageNo = 1;
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SightseeingBooking, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.Message, "0");
        }

    }

    private void InitializeControls()
    {
        BindAgent();
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
        {
            ddlLocations.Enabled = true;
        }
        else
        {
            ddlSource.Visible = false;
            lblSource.Visible = false;
        }
        ddlLocations.DataSource = LocationMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
        ddlLocations.DataTextField = "location_name";
        ddlLocations.DataValueField = "location_id";
        ddlLocations.DataBind();
        int b2bAgentId;
        int b2b2bAgentId;
        if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
        {
            ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
        }
        else if (Settings.LoginInfo.AgentType == AgentType.Agent)
        {
            ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            ddlAgents.Enabled = false;
        }
        else if (Settings.LoginInfo.AgentType == AgentType.B2B)
        {
            ddlAgents.Enabled = false;
            b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
            ddlAgents.SelectedValue = Convert.ToString(b2bAgentId);
            ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            ddlB2BAgent.Enabled = false;
        }
        else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
        {
            ddlAgents.Enabled = false;
            ddlB2BAgent.Enabled = false;
            b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
            b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
            ddlAgents.SelectedValue = Convert.ToString(b2bAgentId);
            ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
            ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            ddlB2B2BAgent.Enabled = false;
        }
        BindB2BAgent(Convert.ToInt32(ddlAgents.SelectedItem.Value));
        BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
        {
            ddlB2B2BAgent.Enabled = false;
        }
    }


    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            ddlAgents.DataSource = dtAgents;
            ddlAgents.DataTextField = "Agent_Name";
            ddlAgents.DataValueField = "agent_id";
            ddlAgents.DataBind();
            ddlAgents.Items.Insert(0, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string type = string.Empty;
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            if (agentId == 0)
            {
                if (Convert.ToInt32(ddlAgents.SelectedItem.Value) > 1)
                {
                    type = "AGENT";// AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                }
                else
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                }
            }
            else
            {
                if (agentId == -1)
                {
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    if (agentId == 0)
                    {
                        type = "BASE";// BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations
                    }
                }
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    type = "BASEB2B";// BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations
                }
            }

            BindLocation(agentId, type);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void ddlB2B2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "B2B2B";// B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                }
            }
            else if (agentId == -1)
            {
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                }
                //if (agentId == -1)
                //{
                //    type = "BASE";
                //}
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = string.Empty;
                    }
                }
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    }
                }
            }
            BindLocation(agentId, type);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Loads all the bookings
    /// </summary>
    /// 

    private void LoadBookings()
    {
        try
        {
            SightseeingItinerary[] tempItinerary = new SightseeingItinerary[0];
            string orderByString = string.Empty;
            string whereString = string.Empty;
            whereString = GetWhereString();
            orderByString = GetOrderbyString();
            
            int queueCount = 0;
            ArrayList listOfItemId = new ArrayList();
            queueCount = CT.Core.Queue.GetTotalFilteredRecordsCount(whereString, orderByString, "Sightseeing");
            string url = "SightseeingBookingQueue.aspx";
            if ((queueCount % recordsPerPage) > 0)
            {
                noOfPages = (queueCount / recordsPerPage) + 1;
            }
            else
            {
                noOfPages = (queueCount / recordsPerPage);
            }


            if (noOfPages > 1)
            {
                if (PageNoString.Value != null)
                {
                    pageNo = Convert.ToInt16(PageNoString.Value);
                }
                else
                {
                    pageNo = 1;
                }
            }
            else
            {
                pageNo = 1;
            }
            if (queueCount > 0)
            {
                show = MetaSearchEngine.PagingJavascript(noOfPages, url, pageNo);
            }

            DataTable dtQueue = CT.Core.Queue.GetTotalFilteredRecords(pageNo, recordsPerPage, queueCount, whereString, orderByString, "Sightseeing");


            dlBookings.DataSource = dtQueue;
            dlBookings.DataBind();
        }
        catch (Exception exp)
        {
            CT.Core.Audit.Add(EventType.Exception, Severity.Normal, 0, "Exception from Booking Queue:" + exp.ToString(), "");
        }
    }

    /// <summary>
    /// Method Gets Where String from the filters(Html Controls)
    /// </summary>
    /// <returns></returns>
    private string GetWhereString()
    {
        bool isOrderByLCD = false;
        string source = string.Empty;
        string voucherd = string.Empty;
        shownStatuses.Clear();
        if (Request["filter"] != null && Request["filter"] == "true")
        {
            filtered = true;
        }
        if (filtered == false)
        {
            //voucherStatus = "Both";
            voucherd = "";
            source = "";
            reqType = "both";
            Cancelled = "True";
            Confirmed = "True";
            shownStatuses.Clear();
            shownStatuses.Add((int)SightseeingBookingStatus.Cancelled);
            shownStatuses.Add((int)SightseeingBookingStatus.Confirmed);
            shownStatuses.Add((int)SightseeingBookingStatus.Pending);

        }
        if (ddlBookingStatus.SelectedValue == "-1")
        {
            shownStatuses.Add((int)SightseeingBookingStatus.Cancelled);
            shownStatuses.Add((int)SightseeingBookingStatus.Confirmed);
            shownStatuses.Add((int)SightseeingBookingStatus.Error);
            shownStatuses.Add((int)SightseeingBookingStatus.Failed);
            shownStatuses.Add((int)SightseeingBookingStatus.Pending);
        }
        else
        {
            shownStatuses.Add(Convert.ToInt32(ddlBookingStatus.SelectedValue));
        }

        if (ddlSource.SelectedItem.Text == "All")
        {
            foreach (ListItem item in ddlSource.Items)
            {
                if (item.Value != "-1")
                {
                    selectedSources.Add(Convert.ToInt32(item.Value));
                }
            }
        }
        else
        {
            selectedSources.Add(Convert.ToInt32(ddlSource.SelectedValue));
        }



        isOrderByLCD = true;
        if (txtSightseeingName.Text != string.Empty)
        {
            SightseeingFilter = txtSightseeingName.Text.Trim();
        }
        if (txtPaxName.Text != string.Empty)
        {
            paxFilter = txtPaxName.Text.Trim();
        }
        if (txtConfirmNo.Text != string.Empty)
        {
            pnrFilter = txtConfirmNo.Text;
        }
        string agentType = string.Empty;
        if (!IsPostBack)
        {
            agentFilter = Utility.ToInteger(Settings.LoginInfo.AgentId);
        }
        else
        {
            agentFilter = Utility.ToInteger(ddlAgents.SelectedItem.Value);
            if (agentFilter == 0)
            {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty;// null Means binding in list all BOOKINGS
                }
            }
            //if (ddlAgents.SelectedIndex > 0)
            //{


            if (agentFilter > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgents.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agentFilter = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agentFilter = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agentFilter > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agentFilter = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgents.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agentFilter = Convert.ToInt32(ddlAgents.SelectedItem.Value);
                    }
                }
            }
            //}
        }

        DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
        IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
        if (CheckIn.Text != string.Empty)
        {
            try
            {
                startDate = Convert.ToDateTime(CheckIn.Text, provider);
            }
            catch { }
        }
        else
        {
            startDate = DateTime.Now;
        }

        if (CheckOut.Text != string.Empty)
        {
            try
            {
                endDate = Convert.ToDateTime(Convert.ToDateTime(CheckOut.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
            }
            catch { }
        }
        else
        {
            endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
        }

        if (!IsPostBack)
        {
            startDate = Convert.ToDateTime(CheckIn.Text, provider);
            endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
        }
        if (ddlLocations.SelectedIndex > 0)
        {
            locationId = Convert.ToInt32(ddlLocations.SelectedValue);
        }
        #region B2C purpose
        string transType = string.Empty;
        if (Settings.LoginInfo.TransType == "B2B")
        {
            ddlTransType.Visible = false;
            lblTransType.Visible = false;
            transType = "B2B";
        }
        else if (Settings.LoginInfo.TransType == "B2C")
        {
            ddlTransType.Visible = false;
            lblTransType.Visible = false;
            transType = "B2C";
        }
        else
        {
            ddlTransType.Visible = true;
            lblTransType.Visible = true;
            if (ddlTransType.SelectedItem.Value == "-1")
            {
                transType = null;
            }
            else
            {
                transType = ddlTransType.SelectedItem.Value;
            }
        }
        #endregion

        //string whereString = string.Empty;
        string whereString = CT.Core.Queue.GetWhereStringForSightseeingBookingQueue(shownStatuses, SightseeingFilter, paxFilter, pnrFilter, agentFilter, bookingModes, source, string.Empty, isOrderByLCD, selectedSources, locationId, startDate, endDate, agentType, transType);

        return whereString;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <returns></returns>
    private string GetOrderbyString()
    {
        string orderString = string.Empty;
        if (Request["lcd"] != null && Request["lcd"] != string.Empty)
        {
            lastCanDate = Request["lcd"];
            orderString = " order by lastCancellationDate asc ";
        }
        return orderString;
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        LoadBookings();
    }

    protected void dlBookings_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            //Check if the bound item is DataItem only
            if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
            {
                //Get the bound item
                DataRowView sightseeingItinerary = e.Item.DataItem as DataRowView;
                Label lblPrice = e.Item.FindControl("lblPrice") as Label;
                Label lblGuest = e.Item.FindControl("lblGuests") as Label;
                Label lblPaxName = e.Item.FindControl("lblPaxName") as Label;
                Label lblSupplier = e.Item.FindControl("lblSupplier") as Label;
                //Label lblRequestStatus = e.Item.FindControl("lblRequestStatus") as Label;
                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                Label lblBooking = e.Item.FindControl("lblBooking") as Label;
                Button btnBooking = e.Item.FindControl("btnBooking") as Button;
                HtmlTable tblBooking = e.Item.FindControl("tblBooking") as HtmlTable;
                Label lblAgentName = e.Item.FindControl("lblAgentName") as Label;
                Label lblBookedBy = e.Item.FindControl("lblBookedBy") as Label;
                Label lblLocation = e.Item.FindControl("lblLocation") as Label;


                Label lblAgencyName = e.Item.FindControl("lblAgencyName") as Label;
                Label lblAgencyBalance = e.Item.FindControl("lblAgencyBalance") as Label;
                Label lblAgencyPhone1 = e.Item.FindControl("lblAgencyPhone1") as Label;
                Label lblAgencyPhone2 = e.Item.FindControl("lblAgencyPhone2") as Label;
                Label lblAgencyEmail = e.Item.FindControl("lblAgencyEmail") as Label;

                Button btnInvoice = e.Item.FindControl("btnInvoice") as Button;
                Button btnOpen = e.Item.FindControl("btnOpen") as Button;
                LinkButton lnkPayment = e.Item.FindControl("lnkPayment") as LinkButton;
                Button btnupdate = e.Item.FindControl("btnupdate") as Button;
                //LinkButton lnkUpdate = e.Item.FindControl("LnkUpdate") as LinkButton;
                TextBox txtUpdate = e.Item.FindControl("txtUpdate") as TextBox;

                if (sightseeingItinerary != null)
                {
                    int bkgId = 0;
                    btnBooking.OnClientClick = "return CancelAmendBooking('" + e.Item.ItemIndex + "');";
                    SightseeingItinerary itinerary = new SightseeingItinerary();
                    BookingDetail bkgDetail = null;
                    AgentMaster agent = null;
                    try
                    {
                        //---------------------------Retrieve Agent-------------------------------------------
                        bkgId = BookingDetail.GetBookingIdByProductId(Convert.ToInt32(sightseeingItinerary["sightseeingId"]), ProductType.SightSeeing);
                        bkgDetail = new BookingDetail(bkgId);
                        Product[] products = BookingDetail.GetProductsLine(bkgId);

                        itinerary.Load(products[0].ProductId);
                        btnInvoice.OnClientClick = "return ViewInvoice('" + itinerary.SightseeingId + "','" + bkgDetail.AgencyId + "','" + itinerary.ConfirmationNo + "');";
                        btnOpen.OnClientClick = "return ViewVoucher('" + itinerary.ConfirmationNo + "');";
                       

                        agent = new AgentMaster(bkgDetail.AgencyId);
                        lblAgentName.Text = agent.Name;
                        lblAgencyBalance.Text = agent.CurrentBalance.ToString("0.000");
                        lblAgencyEmail.Text = agent.Email1;
                        lblAgencyName.Text = agent.Name;
                        lblAgencyPhone1.Text = agent.Phone1;
                        lblAgencyPhone2.Text = agent.Phone2;
                        LocationMaster location = new LocationMaster(itinerary.LocationId);
                        lblLocation.Text = location.Name;


                        //------------------------------------------------------------------------------------
                    }
                    catch { }
                    try
                    {
                        UserMaster bookedBy = new UserMaster(Convert.ToInt64(sightseeingItinerary["CreatedBy"]));
                        lblBookedBy.Text = bookedBy.FirstName + " " + bookedBy.LastName;
                    }
                    catch { }
                    // int adults = 0, childs = 0;
                    decimal totalPrice = 0;

                    totalPrice = Convert.ToDecimal(itinerary.Price.NetFare + itinerary.Price.Markup + itinerary.Price.B2CMarkup); //Added B2c Markup only by chandann on 13062016
                    totalPrice += Math.Ceiling(itinerary.Price.OutputVATAmount);
                    //Bind the Sightseeing Booking price
                    lblPrice.Text = (itinerary.Price.Currency != null ? Util.GetCurrencySymbol(itinerary.Price.Currency) : "AED ") + " " + Math.Ceiling(totalPrice).ToString("N" + agent.DecimalValue); //Use ceiling instead of Round, Changed on 05082016
                    lblPaxName.Text = itinerary.PaxNames == null ? "" : itinerary.PaxNames[0];
                    lblGuest.Text = itinerary.AdultCount + itinerary.ChildCount + "- Adults (" + itinerary.AdultCount + ") Childs (" + itinerary.ChildCount + ")";

                    lblSupplier.Text = (itinerary.Source.ToString());
                    DateTime checkInDate = itinerary.TourDate;

                    if (Settings.LoginInfo.MemberType == MemberType.SUPER || Convert.ToInt32(Math.Ceiling(checkInDate.Subtract(DateTime.Now).TotalDays)) > 1)
                    {

                        tblBooking.Visible = true;
                        btnBooking.Visible = true;
                    }
              
                    if (itinerary.VoucherStatus && itinerary.BookingStatus == SightseeingBookingStatus.Confirmed)
                    {
                        lblStatus.Text = "Vouchered";

                    }
                    else
                    {
                        lblStatus.Text = itinerary.BookingStatus.ToString();

                        lblStatus.Style["color"] = "Red";

                        btnInvoice.Visible = false;
                    }

                    if (bkgDetail != null && bkgDetail.Status == BookingStatus.InProgress)
                    {
                        lblBooking.Text = "Request For Cancellation";
                        btnBooking.Visible = false;
                        tblBooking.Visible = false;
                    }
                    if (itinerary.TransType == "B2C")
                    {
                        lnkPayment.Visible = true;
                        lnkPayment.OnClientClick = "return ViewPaymentInfo('" + e.Item.ItemIndex + "','" + bkgId + "');";
                    }
                    if (txtUpdate.Text.Length > 0)
                    {
                        txtUpdate.Enabled = false;
                        btnupdate.Visible = false;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SightseeingBooking, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), ex.ToString(), "0");
        }
    }

    //need to check for sightSeeing
    protected void dlBookings_ItemCommand(object sender, DataListCommandEventArgs e)
    {
        if (e.CommandName == "Booking")
        {
            DropDownList cmbBooking = e.Item.FindControl("ddlBooking") as DropDownList;
            if (cmbBooking.SelectedIndex > 0)
            {
                
                TextBox txtRemarks = e.Item.FindControl("txtRemarks") as TextBox;

                string bkg = cmbBooking.SelectedValue;

                string confirmationNo = e.CommandArgument.ToString();


                if (bkg == "Cancel Booking")
                {
                    BookingDetail bookingDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(SightseeingItinerary.GetSightseeingId(confirmationNo), ProductType.SightSeeing));
                    int bookingId = bookingDetail.BookingId;
                    string data = txtRemarks.Text;
                    int loggedMemberId = Convert.ToInt32(Settings.LoginInfo.UserID);
                    int requestTypeId = 6; 
                    AgentMaster agency = null;
                    try
                    {

                        ServiceRequest serviceRequest = new ServiceRequest();
                        BookingDetail booking = new BookingDetail();
                        booking = new BookingDetail(bookingId);
                        SightseeingItinerary itineary = new SightseeingItinerary();
                        Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
                        for (int i = 0; i < products.Length; i++)
                        {
                            if (products[i].ProductTypeId == (int)ProductType.SightSeeing)
                            {
                                itineary.Load(products[i].ProductId);
                                break;
                            }
                        }

                        agency = new AgentMaster(booking.AgencyId);
                        serviceRequest.BookingId = bookingId;
                        serviceRequest.ReferenceId = itineary.SightseeingId;
                        serviceRequest.ProductType = ProductType.SightSeeing;

                        serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                        serviceRequest.Data = data;
                        //serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;

                        serviceRequest.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                        serviceRequest.IsDomestic = itineary.IsDomestic;
                        
                       // Audit.Add(EventType.SightseeingBooking, Severity.High, 1, "Total Pax:" +(itineary.ChildCount+ itineary.AdultCount).ToString(), "");
                       
                        serviceRequest.PaxName = itineary.PaxNames[0];
                        serviceRequest.Pnr = itineary.ConfirmationNo;
                        serviceRequest.Source = (int)itineary.Source;
                        serviceRequest.StartDate = itineary.TourDate;
                        serviceRequest.SupplierName = itineary.ItemName;
                        serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                        serviceRequest.ReferenceNumber = itineary.BookingReference;
                        serviceRequest.PriceId = itineary.Price.PriceId;
                        serviceRequest.AgencyId = itineary.AgentId; //booking.AgencyId; //Every request must fall in Admin Queue, so assign Admin ID
                        serviceRequest.ItemTypeId = InvoiceItemTypeId.SightseeingBooking;
                        serviceRequest.DocName = "";
                        serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
                        serviceRequest.LastModifiedOn = DateTime.Now;
                        serviceRequest.CreatedOn = DateTime.Now;
                        serviceRequest.IsDomestic = true;
                        serviceRequest.AgencyTypeId = Agencytype.Cash;
                        serviceRequest.RequestSourceId = RequestSource.BookingAPI;
                        serviceRequest.Save();
                        // Setting booking details status 
                        if (requestTypeId == 6)
                        {
                            booking.SetBookingStatus(BookingStatus.InProgress, loggedMemberId);
                        }
                        else
                        {
                            booking.SetBookingStatus(BookingStatus.AmendmentInProgress, loggedMemberId);
                        }

                        BookingHistory bh = new BookingHistory();
                        bh.BookingId = bookingId;
                        bh.Remarks = "Request sent to " + serviceRequest.RequestType.ToString() + " of the sightseeing booking.";

                        if (requestTypeId == 6)
                        {
                            bh.EventCategory = EventCategory.SightSeeingCancel;
                        }
                        //else
                        //{
                        //    bh.EventCategory = EventCategory.SightSeeingAmendment;
                        //}
                        bh.CreatedBy = (int)Settings.LoginInfo.UserID;
                        bh.Save();
                        //Sending email.
                        Hashtable table = new Hashtable();
                        table.Add("agentName", agency.Name);
                        table.Add("itemName", itineary.ItemName);
                        table.Add("confirmationNo", itineary.ConfirmationNo);

                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        UserMaster bookedBy = new UserMaster(itineary.CreatedBy);
                        AgentMaster bookedAgency = new AgentMaster(itineary.AgentId);
                        toArray.Add(bookedBy.Email);
                        toArray.Add(bookedAgency.Email1);
                        string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["SIGHTSEEING_CANCEL_MAIL"]).Split(';');
                        foreach (string cnMail in cancelMails)
                        {
                            toArray.Add(cnMail);
                        }
                        //toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]); for time bing

                        try
                        {
                            string message = ConfigurationManager.AppSettings["SIGHTSEEING_CANCEL_REQUEST"];
                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Sightseeing)Request for cancellation. Confirmation No:(" + itineary.ConfirmationNo + ")", message, table);
                        }
                        catch { }

                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.ChangeRequest, Severity.High, 1, "Exception in the Sightseeing Change Request. Message:" + ex.Message, "");

                    }
                }

            }
            Label lblRequestStatus = e.Item.FindControl("lblRequestStatus") as Label;
            Label lblBooking = e.Item.FindControl("lblBooking") as Label;

            BookingDetail bkgDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(SightseeingItinerary.GetSightseeingId(e.CommandArgument.ToString()), ProductType.SightSeeing));
            if (bkgDetail.Status == BookingStatus.InProgress)
            {
                lblBooking.Text = "Request For Cancellation";
            }

            LoadBookings();
        }

        if(e.CommandName== "Update")
        {
            TextBox txtUpdate = e.Item.FindControl("txtUpdate") as TextBox;
            Button btnupdate= e.Item.FindControl("btnupdate") as Button;
            string reference = txtUpdate.Text;
            string confirmationNo = e.CommandArgument.ToString();
            //SightseeingItinerary itineary = new SightseeingItinerary();
            //itineary.BookingReference = reference;
           int number= SightseeingItinerary.UpdateReferenceNumber(confirmationNo,reference);
            txtUpdate.Enabled = false;
            btnupdate.Visible = false;
        }
    }

    protected void ddlAgents_SelectedIndexChanged(object sender, EventArgs e)
    {
        int agentId = Convert.ToInt32(ddlAgents.SelectedItem.Value);
        if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
        BindB2BAgent(agentId);
        BindB2B2BAgent(agentId);
        if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
        {
            ddlB2B2BAgent.Enabled = false;
        }
        string type = string.Empty;
        if (agentId == 0)
        {
            type = "BASE";
        }
        BindLocation(Utility.ToInteger(ddlAgents.SelectedItem.Value), type);
    }
    void BindLocation(int agentId, string type)
    {
        DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);

        ddlLocations.Items.Clear();
        ddlLocations.DataSource = dtLocations;
        ddlLocations.DataTextField = "location_name";
        ddlLocations.DataValueField = "location_id";
        ddlLocations.DataBind();

        ListItem item = new ListItem("All", "-1");
        ddlLocations.Items.Insert(0, item);
        hdfParam.Value = "0";
    }
}


