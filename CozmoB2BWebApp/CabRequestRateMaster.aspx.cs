﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;

namespace CozmoB2BWebApp
{
    public partial class RateMaster : CT.Core.ParentPage
    {
        private string RateMsterSearch= "_RateMasterList";
        private string RateMaster_Session = "_RateMaster";
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Settings.LoginInfo!=null && !IsPostBack)
            {
                GetCabDetails();
            }
        }
        private CabRequestRateMaster CurrentObject
        {
            get
            {
                return (CabRequestRateMaster)Session[RateMaster_Session];
            }
            set
            {
                if (value == null)
                {
                    Session.Remove(RateMaster_Session);
                }
                else
                {
                    Session[RateMaster_Session] = value;
                }

            }
        }

        private DataTable SearchList
        {
            get
            {
                return (DataTable)Session[RateMsterSearch];
            }
            set
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["Id"] };
                Session[RateMsterSearch] = value;
            }
        }
        public void GetCabDetails()
        {
            try
            {
                DataRow[] dr;
                CabRequestHeader cabRequest = new CabRequestHeader();
                DataTable dt = cabRequest.GetCabPreferences();
                dr = dt.Select("LST_CODE= " + "'CabType'");
                if (dr != null && dr.Length > 0)
                {
                    ddlCabPreference.DataSource = dr.CopyToDataTable();
                    ddlCabPreference.DataTextField = "LST_DESCP";
                    ddlCabPreference.DataValueField = "LST_VALUE";
                    ddlCabPreference.DataBind();
                    ddlCabPreference.Items.Insert(0, new ListItem("--Select--", "-1"));

                }
                dr = dt.Select("LST_CODE= " + "'SerType'");
                if (dr != null && dr.Length > 0)
                {
                    ddlRentalType.DataSource = dr.CopyToDataTable();
                    ddlRentalType.DataTextField = "LST_DESCP";
                    ddlRentalType.DataValueField = "LST_VALUE";
                    ddlRentalType.DataBind();
                    ddlRentalType.Items.Insert(0, new ListItem("--Select--", "-1"));
                }
                dr = dt.Select("LST_CODE= " + "'CabModel'");
                if (dr != null && dr.Length > 0)
                {
                    ddlCabModel.DataSource = dr.CopyToDataTable();
                    ddlCabModel.DataTextField = "LST_DESCP";
                    ddlCabModel.DataValueField = "LST_VALUE";
                    ddlCabModel.DataBind();
                    ddlCabModel.Items.Insert(0, new ListItem("--Select--", "-1"));
                }
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                CabRequestRateMaster cabRequestRateMaster;
                if (CurrentObject==null)
                {
                     cabRequestRateMaster = new CabRequestRateMaster();
                     cabRequestRateMaster.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                }
                else
                {
                    cabRequestRateMaster = CurrentObject;
                    cabRequestRateMaster.Id = Convert.ToInt32(hdnId.Value);
                    cabRequestRateMaster.ModifiedBy= Convert.ToInt32(Settings.LoginInfo.UserID);
                }
                cabRequestRateMaster.RentalType = ddlRentalType.SelectedValue;
                cabRequestRateMaster.CabPreference = ddlCabPreference.SelectedValue;
                cabRequestRateMaster.CabModel = ddlCabModel.SelectedValue;
                cabRequestRateMaster.Amount = Convert.ToDecimal(txtAmount.Text);
                cabRequestRateMaster.AgentId = Settings.LoginInfo.AgentId;
                cabRequestRateMaster.status = chkStatus.Checked ? true : false;
                cabRequestRateMaster.Save();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("Rate Details","", (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
                if(CurrentObject!=null)
                {
                    btnClear.Text = "Clear";
                    btnSave.Text = "Save";
                }
                Clear();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.ShowSearch("Search");
                CabRequestRateMaster cabRequestRateMaster = new CabRequestRateMaster();
                DataTable dt = new DataTable();
                dt = cabRequestRateMaster.GetRateDetails();
                SearchList = dt;
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvSearch, dt);
            }
            catch(Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        protected void FilterSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string[,] textboxesNColumns ={{ "HTtxtRentalType", "RentalType" }
                                             ,{"HTtxtCabPref", "CabPreference" }
                                             ,{"HTtxtCabModel", "CabModel" }
                                         ,{"HTtxtStatus", "Status" }
                                         };
                CommonGrid g = new CommonGrid();
                g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                
                long id = Utility.ToLong(gvSearch.SelectedValue);
                CabRequestRateMaster cabRequestRateMaster = new CabRequestRateMaster(id);
                CurrentObject = cabRequestRateMaster;
                ddlCabPreference.SelectedValue = Utility.ToString(cabRequestRateMaster.CabPreference);
                ddlCabModel.SelectedValue = Utility.ToString(cabRequestRateMaster.CabModel);
                ddlRentalType.SelectedValue = Utility.ToString(cabRequestRateMaster.RentalType);
                txtAmount.Text = cabRequestRateMaster.Amount.ToString("N" + Settings.LoginInfo.DecimalValue);
                chkStatus.Checked = cabRequestRateMaster.status;
                btnSave.Text = "Update";
                btnClear.Text = "Cancel";
                lblSuccessMsg.Visible = false;
                hdnId.Value = Convert.ToString(id);
                this.Master.HideSearch();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            Clear();
            lblSuccessMsg.Visible = false;
            btnClear.Text = "Clear";
            btnSave.Text = "Save";
        }

        private void Clear()
        {
            CabRequestRateMaster cabRequestRateMaster = new CabRequestRateMaster(-1);
            CurrentObject = null;
            ddlRentalType.SelectedIndex = -1;
            ddlCabPreference.SelectedIndex = -1;
            ddlCabModel.SelectedIndex = -1;
            txtAmount.Text = "0.00";
            hdnId.Value = "0";
            chkStatus.Checked = false;
        }

        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
    }
}