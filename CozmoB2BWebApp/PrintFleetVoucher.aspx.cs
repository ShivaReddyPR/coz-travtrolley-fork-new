﻿using System;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using System.IO;

public partial class PrintFleetVoucher : CT.Core.ParentPage
{
    protected BookingResponse bookingResponse;
    protected FleetItinerary itinerary;
    protected BookingDetail booking;
    protected AgentMaster agent;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Request.QueryString["ConfNo"] != null)
            {
                if (Settings.LoginInfo == null)
                {
                    Response.Redirect("AbandonSession.aspx");
                }
                itinerary = new FleetItinerary();

                string confirmationNo = Request.QueryString["ConfNo"].ToString();
                //Loading booking info based on confirmation no
                booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(FleetItinerary.GetFleetId(confirmationNo), ProductType.Car));
                try
                {
                    Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
                    agent = new AgentMaster(booking.AgencyId);
                    for (int i = 0; i < products.Length; i++)
                    {
                        if (products[i].ProductTypeId == (int)ProductType.Car)
                        {
                            //Loading itinerary object
                            itinerary.Load(products[i].ProductId);
                            break;
                        }
                    }
                    imgHeaderLogo.ImageUrl = Request.Url.Scheme+"://ctb2b.cozmotravel.com/Uploads/AgentLogos/sayara-logo.png";

                    BindData();
                }
                catch (Exception exp)
                {
                    Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Exception Loading itinerary. Message: " + exp.Message, Request["REMOTE_ADDR"]);
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get Fleet Voucher. Error: " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }

    //Bind all controls
    private void BindData()
    {
        if (itinerary != null)
        {
            lblFleetName.Text = itinerary.FleetName;
            lblBookingDate.Text = itinerary.CreatedOn.ToString("dd MMM yyyy HH:mm");
            lblBookingRef.Text = itinerary.BookingRefNo;
            lblTitle.Text = itinerary.PassengerInfo[0].Title;
            lblFirstName.Text = itinerary.PassengerInfo[0].FirstName;
            lblLastName.Text = itinerary.PassengerInfo[0].LastName;
            lblEmail.Text = itinerary.PassengerInfo[0].Email;
            lblMobileNo.Text = itinerary.PassengerInfo[0].MobileNo;
            lblFromDate.Text = itinerary.FromDate.ToString("dd MMM yyyy HH:mm");
            lblToDate.Text = itinerary.ToDate.ToString("dd MMM yyyy HH:mm");
            lblPickupLocation.Text = itinerary.FromLocation;
            if (!string.IsNullOrEmpty(itinerary.PassengerInfo[0].PickupAddress))
            {
                lblPickupAddress.Visible = true;
                lblPickupAddressValue.Text = itinerary.PassengerInfo[0].PickupAddress;
            }
            if (!string.IsNullOrEmpty(itinerary.PassengerInfo[0].Landmark))
            {
                lblLandmark.Visible = true;
                lblLandmarkValue.Text = itinerary.PassengerInfo[0].Landmark;
            }
            if (!string.IsNullOrEmpty(itinerary.PassengerInfo[0].MobileNo2))
            {
                lblMobileNo2.Visible = true;
                lblMobileNo2Value.Text = itinerary.PassengerInfo[0].MobileNo2;
            }
            if (itinerary.ReturnStatus == "Y")
            {
                lblDropupLocation.Visible = true;
                lblDropupLocationValue.Visible = true;
                lblDropupLocationValue.Text = itinerary.ToLocation;
            }
        }
    }
    //Sending mail
    protected void btnEmailVoucher_Click(object sender, EventArgs e)
    {
        try
        {
            #region Sending Email
            Hashtable table = new Hashtable();

            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();

            toArray.Add(txtEmailId.Text.Trim());
            btnEmail.Visible = false;
            btnPrint.Visible = false;

            string message = "";

            StringWriter sWriter = new StringWriter();
            HtmlTextWriter htWriter = new HtmlTextWriter(sWriter);
            printableArea.RenderControl(htWriter);


            message = sWriter.ToString();

            message = message.Replace("class=\"themecol1\"", "style=\"background: #1c498a; height: 24px; line-height: 22px; font-size: 12px;color: #fff; padding-left: 10px;\"");
            try
            {
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], txtEmailId.Text.Trim(), toArray, "Fleet Voucher", message, table);
            }
            catch (System.Net.Mail.SmtpException ex)
            {
                throw ex;
            }
            finally
            {
                btnEmail.Visible = true;
                btnPrint.Visible = true;
            }

            #endregion

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.Normal, 0, "Failed to send Fleet Voucher to " + txtEmailId.Text + " Reason " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }
}
