﻿using System;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.Web.UI.Controls;
using CT.TicketReceipt.Common;

public partial class CorporateGlobalVisaDetailsQueueUI : CT.Core.ParentPage
{
    protected int userCorpProfileId;
    protected int userId;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                if (!Page.IsPostBack)
                {
                    userId = (int)Settings.LoginInfo.UserID;
                    IntialiseControls();
                }
            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateGlobalVisaDetailsQueueUI) Page Load Event Error: " + ex.Message, "0");
        }
    }
    private void IntialiseControls()
    {
        try
        {
            dcReimFromDate.Value = DateTime.Now;
            dcReimToDate.Value = DateTime.Now;

            int user_corp_profile_id = CorporateProfile.GetCorpProfileId((int)Settings.LoginInfo.UserID);
            if (user_corp_profile_id > 0)
            {
                this.userCorpProfileId = user_corp_profile_id;
            }

            bindEmployeesListByGrade();
            bindSearch(DateTime.Now.AddMonths(-1), DateTime.Now, userId);


        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void bindSearch(DateTime fromDate, DateTime toDate, int userId)
    {
        try
        {
            DataTable dt = CorporateGlobalVisaDetails.GetGVDetailsList(fromDate, toDate, userId);
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvVisaSales, dt);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void gvVisaSales_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvVisaSales.PageIndex = e.NewPageIndex;
            gvVisaSales.EditIndex = -1;
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }


    protected string IDDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (ddlEmployee.Enabled && ddlEmployee.SelectedItem.Value != "0")
            {
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), Convert.ToInt32(ddlEmployee.SelectedItem.Value));
            }
            else
            {
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), (int)Settings.LoginInfo.UserID);

            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateTripDetailsQueue) btnSearchApprovals Error: " + ex.ToString(), "0");
        }
    }
    private void bindEmployeesListByGrade()
    {
        DataTable dt = CorporateProfile.GetCorpEmployeesListByGrade((int)Settings.LoginInfo.CorporateProfileId);
        if (dt != null && dt.Rows.Count > 0)
        {

            ddlEmployee.DataSource = dt;
            ddlEmployee.DataTextField = "Name";
            ddlEmployee.DataValueField = "UserId";
            ddlEmployee.DataBind();
            //Check the Corporate User is of Grade 'M' -- Manager then enable dropdown

            bool exists = dt.Select().ToList().Exists(row => Convert.ToString(row["ProfileId"]) == Convert.ToString(Settings.LoginInfo.CorporateProfileId) && row["Grade"].ToString().ToUpper() == "M");
            if (exists)
            {
                ddlEmployee.Enabled = true;
                ddlEmployee.Items.Insert(0, new ListItem("--Select Employee--", "0"));
            }
            else
            {

                ddlEmployee.SelectedItem.Value = Convert.ToString(Settings.LoginInfo.UserID);
                ddlEmployee.Enabled = false;
            }
        }
    }

    protected void gvVisaSales_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Label lblStatus = (Label)e.Row.FindControl("ITlblVisaStatus");
                if (Convert.ToString(drv.Row["FVS_APPROVE_STATUS"]) == "N")
                {
                    lblStatus.Text = "Not Approved";
                }
                else if (Convert.ToString(drv.Row["FVS_APPROVE_STATUS"]) == "Y")
                {
                    lblStatus.Text = "In Process";
                }
                else if (Convert.ToString(drv.Row["FVS_APPROVE_STATUS"]) == "A")
                {
                    lblStatus.Text = "Approved";
                }
                else if (Convert.ToString(drv.Row["FVS_APPROVE_STATUS"]) == "R")
                {
                    lblStatus.Text = "Rejected";
                }
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateGlobalVisaDetailsQueueUI : gvHeroV_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }


}
