﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Corporate;

public partial class CorporateExpenseSummaryUI : CT.Core.ParentPage
{
    protected string currency;
    protected long corpProfileUserId;
    protected decimal amountSpend = 0;
    protected int userCorpProfileId;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                this.currency = Settings.LoginInfo.Currency;
                
                if (!Page.IsPostBack)
                {
                    IntialiseControls();
                    GetAmountSpend(corpProfileUserId);
                }
               // this.corpProfileUserId = Settings.LoginInfo.CorporateProfileId;
            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateExpenseSummary page Load Error: " + ex.Message, "0");
        }
    }

    private void GetAmountSpend(long corpProfileUserId)
    {
        try
        {
            this.amountSpend = CorporateProfileExpenseDetails.GetAmountSpend((int)corpProfileUserId);
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    private void IntialiseControls()
    {
        try
        {
            dcExpFromDate.Value = DateTime.Now;
            dcExpToDate.Value = DateTime.Now;

            //int user_corp_profile_id = CorporateProfile.GetCorpProfileId((int)Settings.LoginInfo.UserID);
            //if (user_corp_profile_id > 0)
            //{
            //    this.userCorpProfileId = user_corp_profile_id;
            //}
            //if (user_corp_profile_id < 0)
            //{
            //    user_corp_profile_id = 0;
            //}
            bindEmployeesList();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void bindEmployeesList()
    {
        ddlEmployee.DataSource = CorporateProfile.GetCorpProfilesList((int)Settings.LoginInfo.AgentId);
        ddlEmployee.DataTextField = "Name";
        ddlEmployee.DataValueField = "ProfileId";
        ddlEmployee.DataBind();
        ddlEmployee.Items.Insert(0, new ListItem("--Select Employee--", "0"));
        ddlEmployee.Enabled = false;
        if (Settings.LoginInfo.MemberType == MemberType.TRAVELCORDINATOR || Settings.LoginInfo.MemberType == MemberType.SUPER || Settings.LoginInfo.MemberType == MemberType.ADMIN)
        {
            ddlEmployee.Enabled = true;
            ddlEmployee.SelectedIndex = 0;
        }
        else
        {
            try
            {
                //ddlEmployee.SelectedItem.Value = Convert.ToString(Settings.LoginInfo.CorporateProfileId);
                ddlEmployee.SelectedIndex = ddlEmployee.Items.IndexOf(ddlEmployee.Items.FindByValue(Convert.ToString(Settings.LoginInfo.CorporateProfileId)));

            }
            catch { }
        }

        //if (this.userCorpProfileId > 0)
        //{
        //    CorporateProfile cp = new CorporateProfile(this.userCorpProfileId, (int)Settings.LoginInfo.AgentId);
        //    if (cp != null && !string.IsNullOrEmpty(cp.ProfileType) && cp.ProfileType == "EMP")
        //    {
        //        ddlEmployee.SelectedValue = Convert.ToString(userCorpProfileId);
        //        ddlEmployee.Enabled = false;
        //    }
        //    else
        //    {
        //        ddlEmployee.Enabled = true;
        //    }
        //}
    }

    protected void btnCreateExpense_Click(object sender, EventArgs e)
    {
        try
        {
            DataTable dt = CorporateProfile.GetCorpProfileDetailsByUserId((int)Settings.LoginInfo.UserID);
            if (dt != null && dt.Rows.Count > 0)
            {
                DataRow dr = dt.Rows[0];
                if(!string.IsNullOrEmpty(Convert.ToString(dr["Name"])) && !string.IsNullOrEmpty(Convert.ToString(dr["CostCentre"])))
                {
                    Response.Redirect("CorporateCreateExpenseReport.aspx?travelMode=N&en=" + dr["Name"] + "&cc=" + dr["CostCentre"]);
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateExpenseSummaryPage) btnCreateExpense_Click Error: " + ex.Message, "0");
        }
    }

}
