﻿using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class bizzlogin : System.Web.UI.Page
    {
        protected void Page_PreInit(object sender, EventArgs e)
        {
            try
            {
                //Response.Redirect("http://ctb2bstage.cozmotravel.com/globalvisa/LoginAT.aspx?b2bagentId=1&userId=1&userLoginName=gvuser1&product=VISAENQUIRY&password=fc0iUkg331qk3V8HY6MWvQ%3d%3d");

                string hostName = BookingUtility.ExtractSiteName(Request["HTTP_HOST"]);
                
                string[] themeDetails = AgentMaster.GetThemByDoamin(hostName);
                if (!string.IsNullOrEmpty(themeDetails[1]))
                    Session["themeName"] = themeDetails[1];
                else Session["themeName"] = "Default";
                

                if (!string.IsNullOrEmpty(themeDetails[0]))
                {
                    string logoPath = ConfigurationManager.AppSettings["AgentImage"] + themeDetails[0];

                }

                string themeName = (string)Session["themeName"];
                if (themeName != null)
                {
                    this.Page.Theme = themeName;
                }
                else
                {
                    this.Page.Theme = "Default";
                }
                //if (!string.IsNullOrEmpty(themeDetails[2]) )// For Copyright
                //{
                //    // CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, hostName+"1 zi", "");
                //    lblCopyRight.Text = themeDetails[2];
                    

                //}
            }

            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Email, CT.Core.Severity.Normal, 0, "Dom Error:" + ex.ToString(), "");
                lblError.Text = ex.Message;
            }
        }
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                lblForgotPwd.Text = string.Empty;
                if (!IsPostBack) InitializeControls();
                lblError.Text = string.Empty;
                if (Request.QueryString["errMessage"] != null)
                {
                    lblForgotPwd.Text = Request.QueryString["errMessage"];
                }
            }

            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

        private void InitializeControls()
        {
            try
            {
                if (Settings.LoginInfo != null)
                {
                    Session.Abandon();
                   
                }
           
            }
            catch { throw; }

        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {

                bool isAuthenticated = UserMaster.IsAuthenticatedUser(txtLoginName.Text.Trim(), txtPassword.Text, Utility.ToInteger(ddlCompany.SelectedValue));
                if (isAuthenticated)
                {
                  
                    if (!string.IsNullOrEmpty(Settings.LoginInfo.AgentTheme))
                        Session["themeName"] = Settings.LoginInfo.AgentTheme;
                    else Session["themeName"] = "Default";

                 

                    string[] agentLeadProduct = Settings.LoginInfo.AgentProduct.Split(',');//, StringSplitOptions.RemoveEmptyEntries);
                    if (agentLeadProduct.Length > 0)
                    {

                        Array.Sort(agentLeadProduct);

                        if (!string.IsNullOrEmpty(agentLeadProduct[0]))
                        {
                            switch (agentLeadProduct[0])
                            {
                                case "1":
                                    Response.Redirect("HotelSearch.aspx?source=Flight", false);
                                    break;
                                case "2":
                                    if (Settings.LoginInfo.Currency == "INR")
                                        Response.Redirect("HotelSearch.aspx?source=Hotel", false);
                                    else Response.Redirect("findHotels.aspx", false);
                                    break;
                                case "3":
                                    Response.Redirect("PackageMaster.aspx", false);
                                    break;
                                case "4":
                                    Response.Redirect("ActivityMaster.aspx", false);
                                    break;
                                case "5":
                                    Response.Redirect("Insurance.aspx", false);
                                    break;
                                default:
                                    Response.Redirect("Index.aspx", false);// Default landing page for All roles
                                    break;

                            }
                        }
                        else Response.Redirect("Index.aspx", false);// Default landing page for All roles
                    }
                    else Response.Redirect("Index.aspx", false);// Default landing page for All roles
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                lblError.Visible = true;
               

            }

        }
        protected void btnGetPassword_Click(object sender, EventArgs e)
        {
            try
            {
                string pagePath = Request.Url.Scheme + "://" + Request.ServerVariables["HTTP_HOST"] + Page.Request.Path;
                string[] pageName = pagePath.Split('/');
                pagePath = string.Empty;
                for (int i = 0; i < pageName.Length - 1; i++)
                {
                    pagePath = pagePath + pageName[i] + "/";
                }
                string guid = UserMaster.RequestPasswordChange(txtEmailId.Text.Trim());
                pagePath = pagePath + "ResetPassword.aspx?requestId=" + guid;

               
                #region Sending Email
               

                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                
                toArray.Add(txtEmailId.Text.Trim());
               
                string link = "<a href='" + pagePath + "'>" + pagePath + "</a>";
                string message = ConfigurationManager.AppSettings["changePassowrdMessage"].Replace("%link%", link);
                try
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Change your password", message, null);
                    lblForgotPwd.Text = "An email has been sent to you with a link to reset your password!";
                }
                catch (System.Net.Mail.SmtpException)
                {
                    //CT.Core1.Audit.Add(CoreLogic.EventType.Email, CoreLogic.Severity.Normal, 0, "Smtp is unable to send the message", "");
                }
              
                #endregion

            }
            catch (Exception ex)
            {
                lblForgotPwd.Text = ex.Message.ToString();
            }
        }
    }
}
