﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;

public partial class StockNotAvailableListGUI : System.Web.UI.Page
{
    PagedDataSource pagedData = new PagedDataSource();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    InitializePageControls();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void InitializePageControls()
    {
        try
        {
            Clear();
            txtFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtTo.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            BindAgent();
            ddlAgent.SelectedIndex = -1;
            if (Settings.LoginInfo.AgentId > 0)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            if (Settings.LoginInfo.AgentId > 1)
            {
                ddlAgent.Enabled = false;
            }
            BindSearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agency--", "0"));
            ddlAgent.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void Clear()
    {
        ddlAgent.SelectedIndex = 0;
    }

    protected void btnSearch_OnClick(object sender, EventArgs e)
    {
        try
        {
            BindSearch();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindSearch()
    {
        try
        {

            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            string StartFromDate = (txtFrom.Text);
            DateTime fromDate = Convert.ToDateTime(StartFromDate, dateFormat);
            string StartToDate = (txtTo.Text);
            DateTime toDate = Convert.ToDateTime(StartToDate, dateFormat);
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            string productId = string.Empty;
            //DataTable dtEnquiry = Enquiry.GetStockNotAvailableList(fromDate, toDate, agentId, Settings.LoginInfo.UserID, Settings.LoginInfo.MemberType.ToString());
            DataTable dtEnquiry = new DataTable();
            BindEnquiry(dtEnquiry);
        }
        catch
        {
            throw;
        }
    }

    private void BindEnquiry(DataTable dtEnquiry)
    {
        try
        {
            if (dtEnquiry.Rows.Count > 0)
            {
                dlEnquiry.Visible = true;
                lblMessage.Visible = false;
                dlEnquiry.DataSource = dtEnquiry;
                ViewState["Enquiry"] = dtEnquiry;
                dlEnquiry.DataBind();
                doPaging();
            }
            else
            {
                dlEnquiry.Visible = false;
                btnPrev.Visible = false;
                btnFirst.Visible = false;
                btnLast.Visible = false;
                btnNext.Visible = false;
                lblMessage.Visible = true;
                lblMessage.Text = "No Results Found.";
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }

    void doPaging()
    {
        DataTable pdt = (DataTable)ViewState["Enquiry"];
        if (pdt != null && pdt.Rows.Count > 0)
        {
            DataTable dt = (DataTable)ViewState["Enquiry"];
            pagedData.DataSource = dt.DefaultView;
        }
        pagedData.AllowPaging = true;
        pagedData.PageSize = 10;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        dlEnquiry.DataSource = pagedData;
        dlEnquiry.DataBind();
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }


    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }

    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }

}
