﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

public partial class ActivityPassengerListGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected Activity activity = null;
    protected string activityImgFolder;
    protected string dateControlId;
    protected void Page_Load(object sender, EventArgs e)
    {
        activityImgFolder = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"].ToString();
        if (!IsPostBack)
        {
            if (Session["Activity"] != null)
            {
                activity = Session["Activity"] as Activity;

                lblName.Text = activity.Name;
                //imgActivity.ImageUrl = activity.ImagePath1;
                imgActivity.ImageUrl = activityImgFolder + activity.ImagePath1;
                lblLocation.Text = activity.City + ", " + activity.Country;
                lblDuration.Text = activity.DurationHours + " hours";
                lblBookingDate.Text = Convert.ToDateTime(activity.TransactionHeader.Rows[0]["Booking"]).ToString("dd MMM yyyy");

                LoadPassengers(activity);

                DataTable dtFlexFields = activity.FlexMaster;

                //dlFlexList.DataSource = dtFlexFields;
                //dlFlexList.DataBind();
                CreateFlexFields(activity);
            }
            else
            {
                Response.Redirect("ActivityResults.aspx", false);
            }
        }
        else
        {
            activity = Session["Activity"] as Activity;
            CreateFlexFields(activity);
        }
    }

    void LoadPassengers(Activity activity)
    {
        try
        {
            DataTable dtTransHeader = activity.TransactionHeader;
            int adults = 0, childs = 0, infants = 0;

            DataTable dtPassengers = new DataTable();
            if (dtPassengers.Columns.Count <= 0)
            {
                dtPassengers.Columns.Add("PaxType", typeof(string));
                dtPassengers.Columns.Add("ActivityId", typeof(int));
            }

            DataTable dtPaxPrice = Session["PaxPrice"] as DataTable;

            DataTable dtPax = dtPaxPrice.Clone();

            foreach (DataRow row in activity.TransactionPrice.Rows)
            {
                int pax = Convert.ToInt32(row["LabelQty"]);
                DataRow dr1 = dtPax.NewRow();
                dr1["Adults"] = pax;
                dr1["Label"] = row["Label"].ToString();
                dr1["ActivityId"] = activity.Id;
                dr1["Price"] = Convert.ToDecimal(row["Amount"]);
                dr1["Amount"] = pax * Convert.ToDecimal(row["Amount"]);
                dtPax.Rows.Add(dr1);
            }

            dlPaxPrice.DataSource = dtPax;
            dlPaxPrice.DataBind();

            decimal Total = 0;
            foreach (DataRow drow in dtPax.Rows)
            {
                Total += Convert.ToDecimal(drow["Amount"]);
            }
            lblSumTotal.Text = Total.ToString();

            if (dtTransHeader != null && dtTransHeader.Rows.Count > 0)
            {
                adults = Convert.ToInt32(dtTransHeader.Rows[0]["Adult"]);
                childs = Convert.ToInt32(dtTransHeader.Rows[0]["Child"]);
                infants = Convert.ToInt32(dtTransHeader.Rows[0]["Infant"]);
            }
            decimal total = 0;
            foreach (DataRow row in dtPax.Rows)
            {
                total += Convert.ToDecimal(row["Amount"]);
            }
            activity.TransactionHeader.Rows[0]["TotalPrice"] = total;
            DataRow dr = dtTransHeader.Rows[0];
            for (int i = 1; i <= (adults); i++)
            {
                if (i == 1)
                {
                    DataRow row = dtPassengers.NewRow();

                    if (dr["Adult"] != DBNull.Value && Convert.ToInt32(dr["Adult"]) > 0)
                    {
                        row["PaxType"] = "Adult " + i;
                    }

                    row["ActivityId"] = dr["ActivityId"];
                    dtPassengers.Rows.Add(row);
                }
            }

            dlPassengers.DataSource = dtPassengers;
            dlPassengers.DataBind();


        }
        catch (Exception ex)
        {
           // Audit.Add(EventType.PakageQueries, Severity.High, 1, ex.Message, "0");
            throw ex;
        }
    }


    protected void dlPassengers_DataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            if (e.Item.ItemIndex == 0)
            {
                //SortedList nationalities = new SortedList();  // Visa.VisaNationality.GetNationSortredList();
                SortedList nationalities = CT.Core.Country.GetCountryList();
                DropDownList ddlNationality = e.Item.FindControl("ddlNationality") as DropDownList;
                RequiredFieldValidator rf1 = e.Item.FindControl("RequiredFieldValidator1") as RequiredFieldValidator;
                RequiredFieldValidator rf2 = e.Item.FindControl("RequiredFieldValidator2") as RequiredFieldValidator;
                RequiredFieldValidator rf3 = e.Item.FindControl("RequiredFieldValidator3") as RequiredFieldValidator;
                RequiredFieldValidator rf4 = e.Item.FindControl("RequiredFieldValidator4") as RequiredFieldValidator;
                RequiredFieldValidator rf5 = e.Item.FindControl("RequiredFieldValidator5") as RequiredFieldValidator;
                RequiredFieldValidator rf6 = e.Item.FindControl("RequiredFieldValidator6") as RequiredFieldValidator;

                if (e.Item.ItemIndex > 0)
                {
                    rf1.Visible = false;
                    rf2.Visible = false;
                    rf3.Visible = false;
                    rf4.Visible = false;
                    rf5.Visible = false;
                    rf6.Visible = false;
                }

                ddlNationality.DataSource = nationalities;
                ddlNationality.DataTextField = "key";
                ddlNationality.DataValueField = "value";
                ddlNationality.DataBind();
            }
            else
            {
                e.Item.Visible = false;
            }
        }
    }


    void CreateFlexFields(Activity activity)
    {
        foreach (DataRow row in activity.FlexMaster.Rows)
        {
            HtmlTableRow tableRow = new HtmlTableRow();
            HtmlTableCell tableCell = new HtmlTableCell();
            HiddenField hdnFlexId = new HiddenField();
            hdnFlexId.ID = "hdnFlexId" + row["flexId"].ToString();
            hdnFlexId.Value = row["flexId"].ToString();
            tableCell.Controls.Add(hdnFlexId);
            Label lblFlex1 = new Label();
            lblFlex1.ID = "lblFlex" + row["flexId"].ToString();
            lblFlex1.Text = row["flexLabel"].ToString();

            HtmlTableCell tableCell1 = new HtmlTableCell();

            RequiredFieldValidator rfvFlex1 = new RequiredFieldValidator();
            rfvFlex1.ID = "rfvFlex" + row["flexId"].ToString();
            rfvFlex1.ValidationGroup = "flex";
            TextBox txtFlex1 = new TextBox();
            txtFlex1.ID = "txtFlex" + row["flexId"].ToString();
            txtFlex1.ValidationGroup = "flex";
            txtFlex1.CausesValidation = true;
            TextBox dcFlex1 = new TextBox();
            dcFlex1.ID = "dcFlexField" + row["flexId"].ToString();

            dcFlex1.ValidationGroup = "flex";
            dcFlex1.CausesValidation = true;
            DropDownList ddlFlex1 = new DropDownList();
            ddlFlex1.ID = "ddlFlex" + row["flexId"].ToString();
            ddlFlex1.ValidationGroup = "flex";
            ddlFlex1.CausesValidation = true;
            HyperLink hlCal1 = new HyperLink();
            hlCal1.ID = "hlCal" + row["flexId"].ToString();
            HtmlImage img = new HtmlImage();
            img.Src = "images/call-cozmo.png";
            hlCal1.Controls.Add(img);

            LoadFlexControl(row, rfvFlex1, txtFlex1, lblFlex1, dcFlex1, ddlFlex1, hlCal1);

            tableCell.Controls.Add(lblFlex1);


            tableCell1.Controls.Add(txtFlex1);
            tableCell1.Controls.Add(dcFlex1);
            tableCell1.Controls.Add(ddlFlex1);
            tableCell1.Controls.Add(hlCal1);
            tableCell1.Controls.Add(rfvFlex1);

            tableCell1.Style.Add("align", "left");

            tableRow.Cells.Add(tableCell);
            tableRow.Cells.Add(tableCell1);

            tblFlexFields.Rows.Add(tableRow);
        }

    }

    protected void dlFlexList_DataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView view = e.Item.DataItem as DataRowView;

            Label lblFlex1 = e.Item.FindControl("lblFlexField1") as Label;
            RequiredFieldValidator rfvFlex1 = e.Item.FindControl("rfvFlex1") as RequiredFieldValidator;
            TextBox txtFlex1 = e.Item.FindControl("txtFlexField1") as TextBox;
            TextBox dcFlex1 = e.Item.FindControl("dcFlexField1") as TextBox;
            DropDownList ddlFlex1 = e.Item.FindControl("ddlFlexField1") as DropDownList;
            HyperLink hlCal1 = e.Item.FindControl("hlCal1") as HyperLink;

            int flexOrder = Convert.ToInt32(view["flexOrder"]);
            
        }
    }

    void LoadFlexControl(DataRow view, RequiredFieldValidator rfvFlex1, TextBox txtFlex1, Label lblFlex1, TextBox dcFlex1, DropDownList ddlFlex1, HyperLink hlCal1)
    {
        try
        {
            switch (view["flexControl"].ToString())
            {
                case "T":
                    if (view["flexStatus"].ToString() == "A")
                    {
                        if (view["flexMandatoryStatus"].ToString() == "Y")
                        {
                            rfvFlex1.ControlToValidate = txtFlex1.ID;
                            rfvFlex1.ErrorMessage = "Enter " + view["flexLabel"].ToString();
                            rfvFlex1.SetFocusOnError = true;
                            rfvFlex1.InitialValue = "";
                            lblFlex1.Text += "<span class='red-color'>*</span>";
                        }
                        else
                        {
                            rfvFlex1.Visible = false;
                        }

                        if (view["flexDataType"].ToString() == "N")
                        {
                            txtFlex1.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'1');");
                        }
                        
                        lblFlex1.Visible = true;
                        txtFlex1.Visible = true;
                        dcFlex1.Visible = false;
                        ddlFlex1.Visible = false;
                        hlCal1.Visible = false;
                    }
                    else
                    {
                        lblFlex1.Visible = false;
                        //lblFlex2.Visible = false;
                    }
                    break;
                case "D":
                    if (view["flexStatus"].ToString() == "A")
                    {
                        dateControlId = "ctl00_cphTransaction_" + dcFlex1.ID;
                        if (view["flexMandatoryStatus"].ToString() == "Y")
                        {
                            rfvFlex1.ControlToValidate = dcFlex1.ID;
                            //rfvFlex1.Visible = false;
                            rfvFlex1.ErrorMessage = "Enter " + view["flexLabel"].ToString();
                            rfvFlex1.SetFocusOnError = true;
                            rfvFlex1.InitialValue = "";
                            lblFlex1.Text += "<span class='red-color'>*</span>";
                        }
                        else
                        {
                            rfvFlex1.Visible = false;
                        }
                        lblFlex1.Visible = true;
                        txtFlex1.Visible = false;
                        dcFlex1.Visible = true;
                        hlCal1.Visible = true;
                        ddlFlex1.Visible = false;
                        hlCal1.NavigateUrl = "javascript:void(null)";
                        hlCal1.Attributes.Add("onclick", "showCalendar1()");
                    }
                    else
                    {
                        lblFlex1.Visible = false;
                        //lblFlex2.Visible = false;
                    }
                    break;
                case "L":
                    if (view["flexStatus"].ToString() == "A")
                    {
                        if (view["flexMandatoryStatus"].ToString() == "Y")
                        {
                            rfvFlex1.ControlToValidate = ddlFlex1.ID;
                            rfvFlex1.ErrorMessage = "Select " + view["flexLabel"].ToString();
                            rfvFlex1.SetFocusOnError = true;
                            rfvFlex1.InitialValue = "-1";
                            lblFlex1.Text += "<span class='red-color'>*</span>";
                        }
                        else
                        {
                            rfvFlex1.Visible = false;
                        }
                        lblFlex1.Visible = true;
                        txtFlex1.Visible = false;
                        dcFlex1.Visible = false;
                        ddlFlex1.Visible = true;
                        hlCal1.Visible = false;

                        SqlParameter[] paramList = new SqlParameter[0];
                        DataSet ds = DBGateway.ExecuteQuery(view["flexSqlQuery"].ToString(), paramList, CommandType.Text);
                        ddlFlex1.DataSource = ds;
                        ddlFlex1.DataTextField = ds.Tables[0].Columns[1].ColumnName;
                        ddlFlex1.DataValueField = ds.Tables[0].Columns[0].ColumnName;
                        ddlFlex1.DataBind();
                    }
                    else
                    {
                        lblFlex1.Visible = false;
                        //lblFlex2.Visible = false;
                    }
                    break;
            }
        }
        catch (Exception ex)
        {
           // Audit.Add(EventType.PakageQueries, Severity.High, 1, ex.Message, "0");
            throw ex;
        }
    }

    protected void imgSubmit_Click(object sender, EventArgs e)
    {
        //Fill the details entered into the tables and store it in session
        try
        {
           // WhiteLabelCore.Customer customer = null;
            if (Session["Activity"] != null)
            {
                activity = Session["Activity"] as Activity;
            }
            else
            {
                Response.Redirect("ActivityResults.aspx");
            }
            activity.TransactionHeader.Rows[0]["Status"] = "A";
            activity.TransactionHeader.Rows[0]["AgencyId"] = Settings.LoginInfo.AgentId;
            activity.TransactionHeader.Rows[0]["LocationId"] = Settings.LoginInfo.LocationID;
            if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId != null)
            {
               // customer = Session["customer"] as WhiteLabelCore.Customer;
                activity.TransactionHeader.Rows[0]["CreatedBy"] = Settings.LoginInfo.UserID;
                activity.TransactionHeader.Rows[0]["LastModifiedBy"] = Settings.LoginInfo.UserID;
            }
            else
            {
                Response.Redirect("ActivityResults.aspx", false);
            }


            //Fill the transaction detail
            foreach (DataListItem Item in dlPassengers.Items)
            {
                if (Item.ItemType == ListItemType.AlternatingItem || Item.ItemType == ListItemType.Item)
                {
                    TextBox txtFirstName = Item.FindControl("txtFirstName") as TextBox;
                    TextBox txtEmail = Item.FindControl("txtEmail") as TextBox;
                    TextBox txtLastName = Item.FindControl("txtLastName") as TextBox;
                    //TextBox txtCountryCode = Item.FindControl("txtFirstName") as TextBox;
                    TextBox txtCountryCode = Item.FindControl("txtCountryCode") as TextBox;
                    TextBox txtMobileNo = Item.FindControl("txtMobileNo") as TextBox;
                    DropDownList ddlNationality = Item.FindControl("ddlNationality") as DropDownList;

                    if (activity.TransactionDetail == null)
                    {
                        activity.TransactionDetail = ActivityDetails.GetActivityTransactionDetail(1);
                    }
                    if (activity.TransactionDetail.Rows.Count <= 0)
                    {
                        DataRow row = activity.TransactionDetail.NewRow();

                    row["FirstName"] = txtFirstName.Text;
                    row["LastName"] = txtLastName.Text;
                    row["Email"] = txtEmail.Text;
                    row["PhoneCountryCode"] = txtCountryCode.Text;
                    row["Phone"] = txtMobileNo.Text;
                    row["Nationality"] = ddlNationality.SelectedItem.Text;
                    row["PaxSerial"] = Item.ItemIndex + 1;
                    row["CreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                    row["CreatedDate"] = DateTime.Now;

                        activity.TransactionDetail.Rows.Add(row);
                    }
                    else
                    {
                        DataRow row = activity.TransactionDetail.Rows[0];

                        row["FirstName"] = txtFirstName.Text;
                        row["LastName"] = txtLastName.Text;
                        row["Email"] = txtEmail.Text;
                        row["PhoneCountryCode"] = txtCountryCode.Text;
                        row["Phone"] = txtMobileNo.Text;
                        row["Nationality"] = ddlNationality.SelectedItem.Text;
                        row["PaxSerial"] = Item.ItemIndex + 1;
                        row["CreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                        row["CreatedDate"] = DateTime.Now;
                    }
                }
            }


            //Fill the Flex Details
            //foreach (DataListItem Item in dlFlexList.Items)
            for (int i = 0; i < tblFlexFields.Rows.Count; i++)
            {
                HtmlTableRow Item = tblFlexFields.Rows[i];
                //if (Item.ItemType == ListItemType.Item || Item.ItemType == ListItemType.AlternatingItem)
                {
                    DataRow view = activity.FlexMaster.Rows[i];
                    Label lblFlex1 = Item.FindControl("lblFlex" + view[0]) as Label;
                    RequiredFieldValidator rfvFlex1 = Item.FindControl("rfvFlex"+ view[0]) as RequiredFieldValidator;
                    TextBox txtFlex1 = Item.FindControl("txtFlex" + view[0]) as TextBox;
                    TextBox dcFlex1 = Item.FindControl("dcFlexField" + view[0]) as TextBox;
                    DropDownList ddlFlex1 = Item.FindControl("ddlFlex" + view[0]) as DropDownList;
                    HiddenField hdnFlexId = Item.FindControl("hdnFlexId" + view[0]) as HiddenField;

                    if (activity.FlexDetails == null)
                    {
                        SqlParameter[] paramList = new SqlParameter[1];

                    }
                    if (activity.FlexDetails.Rows.Count <= tblFlexFields.Rows.Count - 1)
                    {
                        DataRow row = activity.FlexDetails.NewRow();
                        IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

                    row["ActivityId"] = activity.Id;
                    row["flexCreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                    row["flexCreatedOn"] = DateTime.Now;
                    row["flexId"] = Convert.ToInt64(hdnFlexId.Value);

                    switch (view["flexControl"].ToString())
                    {
                        case "T":
                            row["flexLabel"] = lblFlex1.Text;
                            row["flexData"] = txtFlex1.Text;
                            break;
                        case "D":
                            row["flexLabel"] = lblFlex1.Text;
                            row["flexData"] = Convert.ToDateTime(dcFlex1.Text, dateFormat);
                            break;
                        case "L":
                            row["flexLabel"] = lblFlex1.Text;
                            row["flexData"] = ddlFlex1.SelectedValue;
                            break;
                    }

                        activity.FlexDetails.Rows.Add(row);
                    }
                    else
                    {
                        DataRow row = activity.FlexDetails.Rows[i];
                        IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                        row["ActivityId"] = activity.Id;
                        row["flexCreatedBy"] = CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID;
                        row["flexCreatedOn"] = DateTime.Now;
                        row["flexId"] = Convert.ToInt64(hdnFlexId.Value);

                        switch (view["flexControl"].ToString())
                        {
                            case "T":
                                row["flexLabel"] = lblFlex1.Text;
                                row["flexData"] = txtFlex1.Text;
                                break;
                            case "D":
                                row["flexLabel"] = lblFlex1.Text;
                                row["flexData"] = Convert.ToDateTime(dcFlex1.Text, dateFormat);
                                break;
                            case "L":
                                row["flexLabel"] = lblFlex1.Text;
                                row["flexData"] = ddlFlex1.SelectedValue;
                                break;
                        }
                    }
                }
            }

            Session["Activity"] = activity;

            Response.Redirect("ActivityConfirmation.aspx", false);
        }
        catch (Exception ex)
        {
           // Audit.Add(EventType.PakageQueries, Severity.High, 1, ex.Message, "0");\
            throw ex;
        }

    }
}
