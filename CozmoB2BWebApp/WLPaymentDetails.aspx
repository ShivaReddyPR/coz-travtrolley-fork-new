﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="WLPaymentDetails" Title="WLPaymentDetails" Codebehind="WLPaymentDetails.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.BookingEngine.WhiteLabel" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
 <style type="text/css">
.remarks-popup {
	z-index:1000; 
	position:absolute; 
	width:auto;
	font-size:12px;
	padding:0 0 0 11px;
   background:#fff url(images/left_pointer.gif) no-repeat 0 0;
}
.remarks_popup_content {
	float:left;
	width:180px;
	font-size:12px;
  background:#fff;
  border:ridge 2px #aaa;
}
strong.inactive,
strong.active{
	color:#016DB1;
	text-decoration:underline;
	font-weight:normal;
	cursor:pointer;
	font-size:11px;
}
strong.active{
	text-decoration:none;
}
 </style>
<div>
<div>
<form id="Searchform" action="WLPaymentDetails.aspx" method="post">
<input type="hidden" id="page" name="page" value="1" />
<input   type="hidden" id="postbackPayment" name="postbackPayment" value="true" />



<div> 



<table border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td>Search</td>
     <td width="10"> </td>
    <td>
    
    
    
    <select style="width:200px" class="form-control" id="Searchthrough">
     <option  value="1" selected="selected" >Select Filter</option>
     <option  value="2" >Change Request ID</option>
     <option  value="3" >Payment ID</option>
     <option  value="4" >Customer Email ID</option>     
</select>


    


    
    
    </td>
  <td width="10"> </td>
    <td> 
    
    <input  class="form-control" type="text" id="Text1" name="searchBox" onkeydown="testForEnter(event)" value="<%=searchBox%>" />
<input type="hidden" id="searchBy" name="searchBy" value="<%=searchBy%>" />



    </td>
      <td width="10"> </td>
    <td>
    
    <input type="button" Class="btn but_b" style=" width:100px"  id="search" value="Submit" onclick="submitsearch()" />
    
    
    
    </td>
  </tr>
</table>



</div>





 
 </form>
 </div>
 <div>
    <div class="martop_14">
  
        <span><b>CR : </b> Change Request</span>
 
        <span><b>DR : </b> Direct Payment</span>
        </div>
        
        

   
   
    <div>
        
        <label class=" pull-right"> <%= show %></label>
    </div>
    
    
    <table  class="datagrid"  border="1" style="width:100%; position: relative; overflow: hidden;" >
    <tr>
    <th><b>Track ID</b></th>
    <th><b>Payment Id</b></th>
    <th><b>Customer Email id</b></th>
    <th><b>Request ID</b></th>
    <th><b>Amount</b></th>
    <th><b>Payment Source</b></th>
    <th><b>Payment Status</b></th>
    <th><b>Remarks</b></th>    
    </tr>
    <%if (payments.Length > 0)
    {%>
    <%for (int i = 0; i < payments.Length; i++)
      { %>
     <tr>
     <td><% =payments[i].TrackId%></td>
     <td><% =payments[i].PaymentId%></td>
     <%Customer cust = new Customer();
       cust.LoadByCustomerId(payments[i].CustomerId); %>
     <td><%=cust.UserName %></td> 
     <td>
         <%if (payments[i].PaymentRequestType == PaymentRequestType.ChangeRequest)
         {%>
            <b>[CR]</b>
         <%}
         else if (payments[i].PaymentRequestType == PaymentRequestType.TripRequest)
         {%>
            <b>[TR]</b>
         <%}
           else if (payments[i].PaymentRequestType == PaymentRequestType.DirectPayment)
         {%>
            <b>[DR]</b>
         <%}%>
         <%=payments[i].RequestId%>
     </td> 
     <td><% =payments[i].Amount.ToString("0.00")%></td>
     <td><% =payments[i].PaymentSource%></td> 
     <td><% =payments[i].PaymentStatus%></td> 
		 <%payments[i].Remarks = payments[i].Remarks.Replace("\"", "&quot;");
           payments[i].Remarks = payments[i].Remarks.Replace("&", "&amp;");
           payments[i].Remarks = payments[i].Remarks.Replace("<", "&lt;");
           payments[i].Remarks = payments[i].Remarks.Replace(">", "&gt;"); %>
     <td><strong class="inactive" onmouseover="this.className='active'" onmouseout="this.className='inactive'" onclick="javascript:findPos( this,' <%=payments[i].Remarks%>')">View Remarks</strong></td>     
    </tr>
    <%}
    }%>
    
    </table>    
    
    
    
 </div>
 </div>
 <div class="remarks-popup" id="RemarksPopup" style="display:none;">
  <div class="remarks_popup_content" style=" left: 766px; top: 261px; display: inline;">
      <img style="float:right;" alt="close" src="images/close1.gif" onclick="javascript:DoHideRemarks()" />
      <span class="fleft" id="ShowRemarks"></span>
  </div>
</div>
 <script type="text/javascript">
 
   for(var i=0;i<document.getElementById("Searchthrough").options.length;i=i+1)
   {    
        if(document.getElementById("Searchthrough").options[i].value==document.getElementById("searchBy").value)
           {
            document.getElementById("Searchthrough").selectedIndex=i;
           }
   }
  
   </script>
   
   <script type="text/javascript">
    function submitsearch()
    {
       document.getElementById("searchBy").value=document.getElementById("Searchthrough").options[document.getElementById("Searchthrough").selectedIndex].value;
       document.forms[0].submit();
    }
    function testForEnter(event) 
    {    
	    if (event.keyCode == 13) 
	    {	          
	    document.getElementById("searchBy").value=document.getElementById("Searchthrough").options[document.getElementById("Searchthrough").selectedIndex].value;
        }
    }
    function findPos(obj,remarks) 
    {
        var curleft = curtop = 0;
        if (obj.offsetParent) 
        {
             curleft = obj.offsetLeft;
             curtop = obj.offsetTop;
             document.getElementById('RemarksPopup').style.display = 'block';
            
             flag=1;
             while (obj = obj.offsetParent) {
              curleft += obj.offsetLeft;
              curtop += obj.offsetTop;
             }
             document.getElementById('RemarksPopup').style.left = (curleft-195)+ 'px';
             document.getElementById('RemarksPopup').style.top=curtop+3+'px';
             document.getElementById('ShowRemarks').innerHTML = remarks;
         }
    }
    function ShowPage(pageNo)
    {
        document.getElementById('page').value = pageNo;
        document.forms[0].submit();
    }
    function DoHideRemarks()
    {
        document.getElementById('RemarksPopup').style.display = 'none';
    }
   </script>
</asp:Content>

