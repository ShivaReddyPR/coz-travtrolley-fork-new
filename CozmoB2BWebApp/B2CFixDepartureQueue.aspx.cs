﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;

public partial class B2CFixDepartureQueueGUI : CT.Core.ParentPage
{
    protected DataTable ActivityList;
    PagedDataSource pagedData = new PagedDataSource();
    bool isShowAll = false;
    protected int AgentId;
    protected AgentMaster agency;
    #region Pageload

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                isShowAll = true;
                InitializeControls();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "pageLoad", "0");
        }
    }
    private void InitializeControls()
    {
        BindAgent();
        Clear();
    }
    #endregion
    #region BindEvents
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgency.DataSource = dtAgents;
            ddlAgency.DataTextField = "Agent_Name";
            ddlAgency.DataValueField = "agent_id";
            ddlAgency.DataBind();
            ddlAgency.Items.Insert(0, new ListItem("--Select Agency--", "0"));

            ddlAgency.SelectedIndex = -1;
            if (Settings.LoginInfo.AgentId > 0)
            {
                ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }

            if (Settings.LoginInfo.AgentId > 1)
            {
                ddlAgency.Enabled = false;
            }
            else
            {
                ddlAgency.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindActivityQueue()
    {
        try
        {
            string isFisxedDeparture = Request.QueryString["isFD"];
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            string StartFromDate = string.Empty;
            //if (!isShowAll)
                StartFromDate = (txtFrom.Text);
            //else
            //    StartFromDate = DateTime.MinValue.ToString();
            DateTime fromDate = Convert.ToDateTime(StartFromDate, dateFormat);
            string StartToDate = (txtTo.Text);
            DateTime toDate = Convert.ToDateTime(StartToDate, dateFormat);
            toDate = Convert.ToDateTime(toDate.ToString("MM/dd/yyyy 23:59"));
            string tripId = txtTripId.Text;
            string paxName = txtPaxName.Text;
            decimal price = Convert.ToDecimal(txtPrice.Text);
            int agencyId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
            ActivityList = Activity.GetB2CFixedDepartureBookingQueue(fromDate, toDate, tripId, paxName, price, agencyId, isFisxedDeparture);
            Session["ActivityQueue"] = ActivityList;
            dlActivityQueue.DataSource = ActivityList;
            dlActivityQueue.DataBind();
            CurrentPage = 0;
            doPaging();
            AgentId = Convert.ToInt32(Session["agencyId"]);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    #region Button Click Events
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindActivityQueue();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
        }
    }
    #endregion

    #region Private Methods
    private void Clear()
    {
        try
        {
            txtFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtTo.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtTripId.Text = string.Empty;
            txtPaxName.Text = string.Empty;
            txtPrice.Text = "0.00";
            isShowAll = true;
            Session["ActivityQueue"] = null;
            BindActivityQueue();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    #endregion

    #region Paging
    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }

    void doPaging()
    {
        DataTable dt = (DataTable)Session["ActivityQueue"];
        pagedData.DataSource = dt.DefaultView;
        pagedData.AllowPaging = true;
        pagedData.PageSize = 5;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        dlActivityQueue.DataSource = pagedData;
        dlActivityQueue.DataBind();
    }
    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }
    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }
    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }

    protected string CTCurrencyFormat(object currency, object AgentId)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            agency = new AgentMaster(Convert.ToInt32(AgentId));
            return agency.AgentCurrency + " " + Convert.ToDecimal(0).ToString("N" + agency.DecimalValue);
        }
        else
        {
            agency = new AgentMaster(Convert.ToInt32(AgentId));
            return agency.AgentCurrency + " " + Convert.ToDecimal(currency).ToString("N" + agency.DecimalValue);
        }
    }
    #endregion
}
