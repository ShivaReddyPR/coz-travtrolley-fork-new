using System;
using System.Data;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;

public partial class PrintAgentPaymentReceipt :CT.Core.ParentPage// System.Web.UI.Page
{
    private string PAYMENT_SESSION = "_paymentDetails";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                LoginInfo loginInfo = Settings.LoginInfo;
                if (loginInfo == null)
                {
                    Response.Redirect("SessionExpired.aspx");
                    // Response.Redirect(string.Format("ErrorPage.aspx?Err={0}", GetGlobalResourceObject("ErrorMessages", "INVALID_USER")));
                }
                //ShowData(14);//todo
                if (!string.IsNullOrEmpty(Request.QueryString["apid"]))
                {
                    long apId = Utility.ToLong(Request.QueryString["apid"]);
                    ShowData(apId);
                }

            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void ShowData(long payId)
    {
        try
        {
            AgentPaymentDetails agent = new AgentPaymentDetails(payId);
            //lblCompanyValue.Text = poPrint.LocationName;
            if (agent.TranxType == "TRA")
            {
                lblReceived.Text = lblCopyReceived.Text = "Transfer From : ";
                lblReceiptHeader.Text = lblCopyReceiptHeader.Text = "Credit Transfer Receipt";
                if (agent.TranxProvision == "ADD")
                {
                    if (agent.Amount > 0) lblReceivedValue.Text = lblCopyReceivedValue.Text = agent.AgentName + " To " + agent.TransRefName;
                    else lblReceivedValue.Text = lblCopyReceivedValue.Text = agent.TransRefName + " To " + agent.AgentName;
                }
                else
                {
                    if (agent.Amount < 0) lblReceivedValue.Text = lblCopyReceivedValue.Text = agent.TransRefName + " To " + agent.AgentName;
                    else lblReceivedValue.Text = lblCopyReceivedValue.Text = agent.AgentName + " To " + agent.TransRefName;
                }

            }
            else
            {
                lblReceiptHeader.Text = lblCopyReceiptHeader.Text = "Payment Receipt";
                lblReceivedValue.Text = lblCopyReceivedValue.Text = agent.AgentName;
            }

            //lblReceivedValue.Text = lblCopyReceivedValue.Text= agent.AgentName;
           // lblAddressValue.Text = tpoPrint.LocationAddress;
            lblPrintTimeValue.Text =lblCopyPrintTimeValue.Text = IDDateTimeFormat(DateTime.Now);
            lblReceiptNoValue.Text = lblCopyReceiptNoValue.Text=agent.ReceiptNo;
            lblDateValue.Text = lblCopyDateValue.Text =IDDateFormat(agent.ReceiptDate);
            lblCurrencyValue.Text = lblCopyCurrencyValue.Text=agent.Currency;
            lblAmountFigureValue.Text = lblCopyAmountFigureValue.Text=Formatter.ToCurrency(agent.Amount);
           // string amountInWord=NumberToEnglish.changeCurrencyToWords(Utility.ToDouble(agent.Amount));
            string amountInWord = NumberToEnglish.changeCurrencyToWords(Utility.ToDouble(agent.Amount) < 0 ? Utility.ToDouble(agent.Amount) * -1 : Utility.ToDouble(agent.Amount));
            lblAmountWordValue.Text = lblCopyAmountWordValue.Text = amountInWord.ToUpper();
            lblRemarksValue.Text = lblCopyRemarksValue.Text =agent.Remarks;

            lblModeValue.Text = lblCopyModeValue.Text =agent.PayMode;
            if (agent.PayMode == "CASH")
            {
                trBank.Visible =trCopyBank.Visible= false;
            }
            else
                lblBankValue.Text = lblCopyBankValue.Text =agent.BankName;

                lblCreatedValue.Text = lblCopyCreatedValue.Text =agent.CreatedByName;
            
        }
        catch { throw; }
    }
   
    private DataTable PaymentDetails
    {
        get
        {
            return (DataTable)Session[PAYMENT_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ap_id"] };
            Session[PAYMENT_SESSION] = value;
        }
    }

    #region Date Format

    protected string IDDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:MM");
        }
    }
    #endregion    

   
}
