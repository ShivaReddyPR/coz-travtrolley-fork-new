﻿using System;

using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.Data;
using CT.BookingEngine;

public partial class ViewInvoiceforPackage :  System.Web.UI.Page
{
    protected DataTable dtInvoiceDetails;
    protected UserMaster loggedMember = new UserMaster();
    protected Invoice invoice = new Invoice();
    

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            AuthorizationCheck();
            loggedMember = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
            if (!IsPostBack)
            {
                if (Request.QueryString["tranxId"] != null && Request.QueryString.Count > 0)
                {
                    LoadPackageInvoiceDetailsByTranxId(Convert.ToInt32(Request.QueryString["tranxId"]));                
                    int invoiceNumber = 0;
                    try
                    {
                        
                     invoiceNumber = Invoice.isInvoiceGenerated(Convert.ToInt32(Request.QueryString["tranxId"]), ProductType.Packages);
                        
                        if (invoiceNumber > 0)
                        {
                            invoice = new Invoice();
                            invoice.Load(invoiceNumber);
                        }
                        else
                        {
                            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(Convert.ToInt32(Request.QueryString["tranxId"]), string.Empty, (int)loggedMember.ID, ProductType.Packages,1);
                            invoice.Load(invoiceNumber);
                        }
                    }
                    catch (Exception exp)
                    {
                      
                        Audit.Add(EventType.Exception, Severity.High, 1, "Exception while generating Invoice.. Message:" + exp.Message, "");
                        return;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from (ViewBookingforPackage Page):" + ex.ToString(), "");
        }
    }


    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }

    private void LoadPackageInvoiceDetailsByTranxId(int tranxId)
    {
        try
        {
            dtInvoiceDetails = Queue.GetPackageDetailsForInvoiceByTranxId(tranxId);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
