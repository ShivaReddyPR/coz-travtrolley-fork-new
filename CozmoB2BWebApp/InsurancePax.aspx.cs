﻿using System;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.BookingEngine.Insurance;
using System.Collections.Generic;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

public partial class InsurancePax :CT.Core.ParentPage// System.Web.UI.Page
{
    protected InsuranceResponse selectedPlans;
    protected InsuranceRequest InsRequest;
    protected int paxCount = 0;
    protected int adultCount = 0;
    protected bool check = false;
    protected decimal totalAmount = 0;
    protected bool isSeniorCitizen = false;
    protected string currencyCode = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["InsRequest"] == null || Session["SelectedPlans"] == null) //checking sessions
            {
                Response.Redirect("Insurance.aspx", false);
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "session expaired in insurance pax page load" , Request["REMOTE_ADDR"]);
            }
            else
            {
                //Loading Selected Plans
                selectedPlans = Session["SelectedPlans"] as InsuranceResponse;
                InsRequest = Session["InsRequest"] as InsuranceRequest;
                paxCount = InsRequest.Adults + InsRequest.Childs + InsRequest.Infants;
                adultCount = InsRequest.Adults;
                isSeniorCitizen = InsRequest.IsSeniorCitizen;
                BindPassengers();
                currencyCode =  (!string.IsNullOrEmpty(InsRequest.PseudoCode)) ? InsRequest.CurrencyCode : Settings.LoginInfo.Currency;
                if (currencyCode == "INR") //If currency is INR then displaying Nominee details 
                   BindNominee();
                check = true;
                if (Request.QueryString["err"] != null)
                {
                    lblError.Text = Request.QueryString["err"];
                }
                if (!IsPostBack && Session["InsPassengers"] != null)
                {
                    LoadControlState();
                }
                ScriptManager.RegisterStartupScript(this, typeof(Page), "fillCity", "fillCityOnSubmit()", true);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
    //binding All passengers
    void BindPassengers()
    {
        InsuranceRequest InsRequest = Session["InsRequest"] as InsuranceRequest;
        
        int i = 0;
        int pax = 0;
        string paxType = string.Empty;
        for (; i < InsRequest.Adults; i++)
        {
            HtmlTableRow hRow = new HtmlTableRow();
            HtmlTableCell hCell = new HtmlTableCell();
            if (i == 0)
            {
                paxType= isSeniorCitizen ? " Senior Citizen " : " Adult ";
                hCell.InnerHtml = "<b>" + paxType + (i + 1).ToString() + "</b>" + " (Lead Guest)";
                hCell.Attributes.Add("class", "subgray-header");
            }
            else
            {
                paxType = isSeniorCitizen ? " Senior Citizen " : " Adult ";
                hCell.InnerHtml += "<b>" + paxType + (i + 1).ToString() + "</b>";
                hCell.Attributes.Add("class", "subgray-header");
            }
            hCell.ColSpan = 8;

            hRow.Cells.Add(hCell);

            HtmlTableRow detailRow = new HtmlTableRow();
            HtmlTableCell prefix = new HtmlTableCell();

            HtmlGenericControl divlblTitle = new HtmlGenericControl("Div");
            divlblTitle.Attributes.Add("class", "col-md-3");
            divlblTitle.InnerHtml = "<div class='col-md-3'>Title&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divDdlTitle = new HtmlGenericControl("Div"); // For Adult Div
            divDdlTitle.Attributes.Add("class", "col-md-3"); // For Adult Div class attribute

            DropDownList ddlTitle = new DropDownList();
            ddlTitle.ID = "Prefix" + 1 + i.ToString();
            ddlTitle.CssClass = "form-control";
            ddlTitle.CausesValidation = true;
            ddlTitle.ValidationGroup = "pax";
            ListItem item1 = new ListItem("Select Title", "-1");
            ListItem item2 = new ListItem("Mr", "Mr");
            ListItem item3 = new ListItem("Mrs", "Mrs");
            ListItem item4 = new ListItem("Ms", "Ms");
            ListItem item5 = new ListItem("Dr", "Dr");
            ddlTitle.Items.Add(item1);
            ddlTitle.Items.Add(item2);
            ddlTitle.Items.Add(item3);
            ddlTitle.Items.Add(item4);
            ddlTitle.Items.Add(item5);
            ddlTitle.Attributes.Add("onchange", "setGender(1," + i.ToString() + ")");
            //prefix.Controls.Add(lblPrefix);
            divDdlTitle.Controls.Add(ddlTitle);

            RequiredFieldValidator prefixvc = new RequiredFieldValidator();
            prefixvc.ControlToValidate = ddlTitle.ID;
            prefixvc.ErrorMessage = "Select Title";
            prefixvc.ValidationGroup = "pax";
            prefixvc.InitialValue = "-1";
            prefixvc.SetFocusOnError = true;
            divDdlTitle.Controls.Add(prefixvc);

             prefix.Controls.Add(divlblTitle);
             prefix.Controls.Add(divDdlTitle);

             //Adult First Name
             HtmlGenericControl divlblfName = new HtmlGenericControl("Div"); // For Adult First Name
             divlblfName.Attributes.Add("class", "col-md-3");
             divlblfName.InnerHtml = "<div  class='col-md-3' >First&nbsp;Name&nbsp;<span style='color:red'>*</span>:</Div>";

             HtmlGenericControl divTxtfName = new HtmlGenericControl("Div"); // For Adult First Name
             divTxtfName.Attributes.Add("class", "col-md-3");

            TextBox txtFirstName = new TextBox();
            txtFirstName.ID = "txtAFirstName" + 1 + i.ToString();
            txtFirstName.CssClass = "form-control";
            txtFirstName.CausesValidation = true;
            txtFirstName.ValidationGroup = "pax";
            txtFirstName.MaxLength = 50;
            divTxtfName.Controls.Add(txtFirstName);

            RequiredFieldValidator fnamevc = new RequiredFieldValidator();
            fnamevc.ControlToValidate = txtFirstName.ID;
            fnamevc.ErrorMessage = "Enter First Name";
            fnamevc.InitialValue = "";
            fnamevc.SetFocusOnError = true;
            fnamevc.ValidationGroup = "pax";
            divTxtfName.Controls.Add(fnamevc);

            prefix.Controls.Add(divlblfName);
            prefix.Controls.Add(divTxtfName);

            //Adult Last Name
            HtmlGenericControl divlbllName = new HtmlGenericControl("Div"); // For Adult Last Name
            divlbllName.Attributes.Add("class", "col-md-3");
            divlbllName.InnerHtml = "<div  class='col-md-3' >Last&nbsp;Name&nbsp;<span style='color:red;'>*</span>:</Div>";

            HtmlGenericControl divtxtlName = new HtmlGenericControl("Div"); // For Adult Last Name
            divtxtlName.Attributes.Add("class", "col-md-3");

            TextBox txtLastName = new TextBox();
            txtLastName.ID = "txtALastName" + 1 + i.ToString();
            txtLastName.CssClass = "form-control";
            txtLastName.CausesValidation = true;
            txtLastName.ValidationGroup = "pax";
            txtLastName.MaxLength = 50;
            divtxtlName.Controls.Add(txtLastName);

            RequiredFieldValidator lnamevc = new RequiredFieldValidator();
            lnamevc.ControlToValidate = txtLastName.ID;
            lnamevc.ErrorMessage = "Enter&nbsp;Last&nbsp;Name";
            lnamevc.InitialValue = "";
            lnamevc.SetFocusOnError = true;
            lnamevc.ValidationGroup = "pax";
            divtxtlName.Controls.Add(lnamevc);

            prefix.Controls.Add(divlbllName);
            prefix.Controls.Add(divtxtlName);

            HtmlGenericControl divlblGender = new HtmlGenericControl("Div"); // For Adult Gender
            divlblGender.Attributes.Add("class", "col-md-3");
            divlblGender.InnerHtml = "<div  class='col-md-3' >Gender&nbsp;<span style='color:red;'>*</span>:</Div>";
 
            HtmlGenericControl divddlGender = new HtmlGenericControl("Div"); // For Adult Gender
            divddlGender.Attributes.Add("class", "col-md-3");

            DropDownList ddlGender = new DropDownList();
            ddlGender.ID = "ddlGender" + 1 + i.ToString();
            ddlGender.CssClass = "form-control";
            ddlGender.CausesValidation = true;
            ddlGender.ValidationGroup = "pax";
            divddlGender.Controls.Add(ddlGender);

            RequiredFieldValidator gendervc = new RequiredFieldValidator();
            gendervc.ControlToValidate = ddlGender.ID;
            gendervc.ErrorMessage = "Select Gender";
            gendervc.InitialValue = "-1";
            gendervc.SetFocusOnError = true;
            gendervc.ValidationGroup = "pax";
            BindGender(ddlGender);
            divddlGender.Controls.Add(gendervc);

            prefix.Controls.Add(divlblGender);
            prefix.Controls.Add(divddlGender);

            HtmlGenericControl divlblNation = new HtmlGenericControl("Div"); // For Adult Nationality
            divlblNation.Attributes.Add("class", "col-md-3");
            divlblNation.InnerHtml = "<div  class='col-md-3' >Nationality&nbsp;<span style='color:red;'>*</span>:</Div>";

            HtmlGenericControl divddlNationality = new HtmlGenericControl("Div"); // For Adult Nationality
            divddlNationality.Attributes.Add("class", "col-md-3");

            DropDownList ddlNationality = new DropDownList();
            ddlNationality.ID = "ddlNationality" + 1 + i.ToString();
            ddlNationality.CssClass = "form-control";
            ddlNationality.CausesValidation = true;
            ddlNationality.ValidationGroup = "pax";
            BindNationality(ddlNationality);
            divddlNationality.Controls.Add(ddlNationality);

            RequiredFieldValidator nationvc = new RequiredFieldValidator();
            nationvc.ControlToValidate = ddlNationality.ID;
            nationvc.ErrorMessage = "Select Nationality";
            nationvc.InitialValue = "-1";
            nationvc.SetFocusOnError = true;
            nationvc.ValidationGroup = "pax";
            divddlNationality.Controls.Add(nationvc);

            prefix.Controls.Add(divlblNation);
            prefix.Controls.Add(divddlNationality);

            HtmlGenericControl divlblPhone = new HtmlGenericControl("Div"); // For Adult Mobile
            divlblPhone.Attributes.Add("class", "col-md-3");
            divlblPhone.InnerHtml = "<div  class='col-md-3' >Phone&nbsp;<span style='color:red;'>*</span>:</Div>";

            HtmlGenericControl divtxtPhone = new HtmlGenericControl("Div"); // For Adult Phone
            divtxtPhone.Attributes.Add("class", "col-md-3");

            TextBox txtPhone = new TextBox();
            txtPhone.ID = "txtAPhone" + 1 + i.ToString();
            txtPhone.CssClass = "form-control";
            txtPhone.CausesValidation = true;
            txtPhone.ValidationGroup = "pax";
            txtPhone.MaxLength = 15;
            divtxtPhone.Controls.Add(txtPhone);


            RequiredFieldValidator mobilevc = new RequiredFieldValidator();
            mobilevc.ControlToValidate = txtPhone.ID;
            mobilevc.ErrorMessage = "Enter Phone No";
            mobilevc.InitialValue = "";
            mobilevc.SetFocusOnError = true;
            mobilevc.ValidationGroup = "pax";
            divtxtPhone.Controls.Add(mobilevc);

            prefix.Controls.Add(divlblPhone);
            prefix.Controls.Add(divtxtPhone);

           

            HtmlGenericControl divlblCountry = new HtmlGenericControl("Div"); // For Adult Country
            divlblCountry.Attributes.Add("class", "col-md-3");
            divlblCountry.InnerHtml = "<div  class='col-md-3' >Country&nbsp;of&nbsp;residence&nbsp;<span style='color:red;'>*</span>:</Div>";

            HtmlGenericControl divddlCountry = new HtmlGenericControl("Div"); // For Adult Country
            divddlCountry.Attributes.Add("class", "col-md-3");

            DropDownList ddlCountry = new DropDownList();
            ddlCountry.ID = "ddlCountry" + 1 + i.ToString();
            ddlCountry.CssClass = "form-control";
            ddlCountry.Attributes.Add("onchange", "getCities(1," + i.ToString() + ")");
            BindCountry(ddlCountry);
            divddlCountry.Controls.Add(ddlCountry);

            RequiredFieldValidator countryvc = new RequiredFieldValidator();
            countryvc.ControlToValidate = ddlCountry.ID;
            countryvc.ErrorMessage = "Select Country";
            countryvc.InitialValue = "-1";
            countryvc.SetFocusOnError = true;
            countryvc.ValidationGroup = "pax";
            divddlCountry.Controls.Add(countryvc);

            prefix.Controls.Add(divlblCountry);
            prefix.Controls.Add(divddlCountry);

            HtmlGenericControl divlblCity = new HtmlGenericControl("Div"); // For Adult City
            divlblCity.Attributes.Add("class", "col-md-3");
            divlblCity.InnerHtml = "<div  class='col-md-3'>City&nbsp;of&nbsp;residence&nbsp;<span style='color:red;'>*</span>:</Div>";

            HtmlGenericControl divddlCity = new HtmlGenericControl("Div"); // For Adult City
            divddlCity.Attributes.Add("class", "col-md-3");

            DropDownList ddlCity = new DropDownList();
            ddlCity.ID = "ddlCity" + 1 + i.ToString();
            ddlCity.CssClass = "form-control";
            ddlCity.Attributes.Add("onchange", "setCity(1," + i.ToString() + ")");

            HiddenField hdnCity = new HiddenField();
            hdnCity.ID = "hdnCity1" + i.ToString();
            //city.Controls.Add(lblCity);            
            divddlCity.Controls.Add(ddlCity);
            divddlCity.Controls.Add(hdnCity);


            RequiredFieldValidator cityvc = new RequiredFieldValidator();
            cityvc.ControlToValidate = ddlCity.ID;
            cityvc.ErrorMessage = "Select City";
            cityvc.InitialValue = "-1";
            cityvc.SetFocusOnError = true;
            cityvc.ValidationGroup = "pax";
            divddlCity.Controls.Add(cityvc);

            prefix.Controls.Add(divlblCity);
            prefix.Controls.Add(divddlCity);

            HtmlGenericControl divlblIDType = new HtmlGenericControl("Div");
            divlblIDType.Attributes.Add("class", "col-md-3");
            divlblIDType.InnerHtml = "<div  class='col-md-3' >Identity&nbsp;Type&nbsp;<span style='color:red;'>*</span>:</Div>";
 
            HtmlGenericControl divddlIDType = new HtmlGenericControl("Div");
            divddlIDType.Attributes.Add("class", "col-md-3");

            DropDownList ddlIDType = new DropDownList();
            ddlIDType.ID = "ddlIDType" + 1 + i.ToString();
            ddlIDType.CssClass = "form-control";
            ListItem item11 = new ListItem("Passport", "Passport");
            ListItem item21 = new ListItem("Identity Card", "Identity Card");
            ddlIDType.Items.Add(item11);
            ddlIDType.Items.Add(item21);
            divddlIDType.Controls.Add(ddlIDType);

            prefix.Controls.Add(divlblIDType);
            prefix.Controls.Add(divddlIDType);

            HtmlGenericControl divlblIDNo = new HtmlGenericControl("Div");
            divlblIDNo.Attributes.Add("class", "col-md-3");
            divlblIDNo.InnerHtml = "<div  class='col-md-3' >Identity&nbsp;No&nbsp;<span style='color:red;'>*</span>:</Div>";

            HtmlGenericControl divtxtIDNo = new HtmlGenericControl("Div");
            divtxtIDNo.Attributes.Add("class", "col-md-3");

            TextBox txtIDNo = new TextBox();
            txtIDNo.ID = "txtIDNo" + 1 + i.ToString();
            txtIDNo.CssClass = "form-control";
            txtIDNo.CausesValidation = true;
            txtIDNo.ValidationGroup = "pax";
            txtIDNo.MaxLength = 50;
            divtxtIDNo.Controls.Add(txtIDNo);

            RequiredFieldValidator idnoevc = new RequiredFieldValidator();
            idnoevc.ControlToValidate = txtIDNo.ID;
            idnoevc.ErrorMessage = "Enter Identity No";
            idnoevc.InitialValue = "";
            idnoevc.SetFocusOnError = true;
            idnoevc.ValidationGroup = "pax";
            divtxtIDNo.Controls.Add(idnoevc);

            prefix.Controls.Add(divlblIDNo);
            prefix.Controls.Add(divtxtIDNo);

            HtmlGenericControl divlblEmail = new HtmlGenericControl("Div");
            divlblEmail.Attributes.Add("class", "col-md-3");
            divlblEmail.InnerHtml = "<div  class='col-md-3' >Email&nbsp;<span style='color:red;'>*</span>:</Div>";

            HtmlGenericControl divtxtEmail = new HtmlGenericControl("Div"); // For Adult Country
            divtxtEmail.Attributes.Add("class", "col-md-3");

            TextBox txtEmail = new TextBox();
            txtEmail.ID = "txtEmail" + 1 + i.ToString();
            txtEmail.CssClass = "form-control";
            txtEmail.CausesValidation = true;
            txtEmail.ValidationGroup = "pax";
            txtEmail.MaxLength = 100;
            txtEmail.Text = "Enter multiple emails Separate by semicolon";
            txtEmail.Attributes.Add("onblur", "if( this.value=='' ) {this.value='Enter multiple emails Separate by semicolon'; }");
            txtEmail.Attributes.Add("onfocus", "if( this.value=='Enter multiple emails Separate by semicolon' ) {this.value=''; }");
            divtxtEmail.Controls.Add(txtEmail);

            RequiredFieldValidator emailvc = new RequiredFieldValidator();
            emailvc.ControlToValidate = txtEmail.ID;
            emailvc.ErrorMessage = "Enter Email";
            emailvc.InitialValue = "";
            emailvc.SetFocusOnError = true;
            emailvc.ValidationGroup = "pax";
            divtxtEmail.Controls.Add(emailvc);

            RegularExpressionValidator emailrv = new RegularExpressionValidator();
            emailrv.ControlToValidate = txtEmail.ID;
            emailrv.ErrorMessage = "Invalid Email address";
            emailrv.SetFocusOnError = true;
            emailrv.ValidationGroup = "pax";
            emailrv.ValidationExpression = @"^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([;])*)*$";
            divtxtEmail.Controls.Add(emailrv);

            prefix.Controls.Add(divlblEmail);
            prefix.Controls.Add(divtxtEmail);

            HtmlGenericControl divlblDOB = new HtmlGenericControl("Div");
            divlblDOB.Attributes.Add("class", "col-md-3");
            divlblDOB.InnerHtml = "<div  class='col-md-3' >DOB&nbsp;<span style='color:red;'>*</span>::</Div>";

            HtmlGenericControl divDOB = new HtmlGenericControl("Div");
            divDOB.Attributes.Add("class", "col-md-3");

            DropDownList ddlDay = new DropDownList();
            ddlDay.ID = "ddlDay" + 1 + i.ToString();
            ddlDay.Width = new Unit(30, UnitType.Percentage);
            ddlDay.CssClass = "form-control pull-left ";
            ddlDay.Attributes.Add("onchange", "ValidatePaxDOB(1)");
            BindDates(ddlDay);

            DropDownList ddlMonth = new DropDownList();
            ddlMonth.ID = "ddlMonth" + 1 + i.ToString();
            ddlMonth.Width = new Unit(30, UnitType.Percentage);
            ddlMonth.CssClass = "form-control pull-left ";
            ddlMonth.Attributes.Add("onchange", "ValidatePaxDOB(1)");
            BindMonths(ddlMonth);

            DropDownList ddlYear = new DropDownList();
            ddlYear.ID = "ddlYear" + 1 + i.ToString();
            ddlYear.Width = new Unit(40, UnitType.Percentage);
            ddlYear.CssClass = "form-control pull-left ";
            ddlYear.Attributes.Add("onchange", "ValidatePaxDOB(1)");
            BindYears(ddlYear);

            Label lblError = new Label();
            lblError.ID = "lblError" + 1 + i.ToString();

            divDOB.Controls.Add(ddlDay);
            divDOB.Controls.Add(ddlMonth);
            divDOB.Controls.Add(ddlYear);
            divDOB.Controls.Add(lblError);

            RequiredFieldValidator dayvc = new RequiredFieldValidator();
            dayvc.ControlToValidate = ddlDay.ID;
            dayvc.ErrorMessage = "Select Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            dayvc.InitialValue = "-1";
            dayvc.SetFocusOnError = true;
            dayvc.ValidationGroup = "pax";
            divDOB.Controls.Add(dayvc);
            

            RequiredFieldValidator monthvc = new RequiredFieldValidator();
            monthvc.ControlToValidate = ddlMonth.ID;
            monthvc.ErrorMessage = "Select Month&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            monthvc.InitialValue = "-1";
            monthvc.SetFocusOnError = true;
            monthvc.ValidationGroup = "pax";
            divDOB.Controls.Add(monthvc);

            RequiredFieldValidator yearvc = new RequiredFieldValidator();
            yearvc.ControlToValidate = ddlYear.ID;
            yearvc.ErrorMessage = "Select Year&nbsp;&nbsp;";
            yearvc.InitialValue = "-1";
            yearvc.SetFocusOnError = true;
            yearvc.ValidationGroup = "pax";
            divDOB.Controls.Add(yearvc);            

            //RangeValidator rvYear = new RangeValidator();
            //rvYear.ControlToValidate = ddlYear.ID;
            //rvYear.SetFocusOnError = true;
            //rvYear.ErrorMessage = "Passenger Age must be in between 30-Days and 75-Years";
            //rvYear.Type = ValidationDataType.Integer;
            //rvYear.ValidationGroup = "pax";
            //rvYear.MinimumValue = DateTime.Now.AddYears(-75).Year.ToString();
            //rvYear.MaximumValue = DateTime.Now.AddYears(0).Year.ToString();

            ////If only single pax is booking restrict him/her age between 2 and 75
            //if (InsRequest.Adults == 1 && InsRequest.Childs == 0 && InsRequest.Infants == 0)
            //{
            //    divDOB.Controls.Add(rvYear);
            //}

            prefix.Controls.Add(divlblDOB);
            prefix.Controls.Add(divDOB);

          

            HtmlGenericControl divPaxType = new HtmlGenericControl("Div");

            //HtmlTableCell paxType = new HtmlTableCell();
            HiddenField hdfPaxType = new HiddenField();
            hdfPaxType.ID = "hdfPaxType" + pax + i.ToString();
            hdfPaxType.Value = "A";
            divPaxType.InnerHtml += "</br> </br>";
            divPaxType.Controls.Add(hdfPaxType);

            prefix.Controls.Add(divPaxType);

            if (i == 0)
            {
                HtmlGenericControl divlblRemarks = new HtmlGenericControl("Div");
                divlblRemarks.Attributes.Add("class", "col-md-3");
                divlblRemarks.InnerHtml = "<div  class='col-md-3' >Remarks:</Div>";

                HtmlGenericControl divtxtRemarks = new HtmlGenericControl("Div");
                divtxtRemarks.Attributes.Add("class", "col-md-3");

                TextBox txtRemarks = new TextBox();
                txtRemarks.ID = "txtRemarks" + 1 + i.ToString();
                txtRemarks.CssClass = "form-control";
                txtRemarks.CausesValidation = true;
                txtRemarks.ValidationGroup = "pax";
                txtRemarks.MaxLength = 75;
                txtRemarks.TextMode = TextBoxMode.MultiLine;
                //remarks.Controls.Add(lblRemarks);
                divtxtRemarks.Controls.Add(txtRemarks);
                prefix.Controls.Add(divlblRemarks);
                prefix.Controls.Add(divtxtRemarks);
            }


            detailRow.Cells.Add(prefix);
            
            tblPassengers.Rows.Add(hRow);
            tblPassengers.Rows.Add(detailRow);
            pax++;
        }

        i = 0;

        for (; i < InsRequest.Childs; i++)
        {
            HtmlTableRow hRow = new HtmlTableRow();
            HtmlTableCell hCell = new HtmlTableCell();
            hCell.InnerHtml += "<b>" + " Child " + (i + 1).ToString() + "</b>";

            hCell.ColSpan = 8;
            hCell.Attributes.Add("class", "subgray-header");
            hRow.Cells.Add(hCell);
            tblPassengers.Rows.Add(hRow);

            HtmlTableRow detailRow = new HtmlTableRow();
            HtmlTableCell prefix = new HtmlTableCell();


            HtmlGenericControl divlblPrefix = new HtmlGenericControl("Div");
            divlblPrefix.Attributes.Add("class", "col-md-3");
            divlblPrefix.InnerHtml = "<div class='col-md-3'>Title&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divddlTitle = new HtmlGenericControl("Div");
            divddlTitle.Attributes.Add("class", "col-md-3");

            DropDownList ddlTitle = new DropDownList();
            ddlTitle.ID = "Prefix" + 2 + i.ToString();
            ddlTitle.CssClass = "form-control";
            ddlTitle.CausesValidation = true;
            ddlTitle.ValidationGroup = "pax";
            ListItem item1 = new ListItem("Select Title", "-1");
            ListItem item2 = new ListItem("Mr", "Mr");
            ListItem item3 = new ListItem("Mrs", "Mrs");
            ListItem item4 = new ListItem("Ms", "Ms");
            ListItem item5 = new ListItem("Dr", "Dr");
            ddlTitle.Items.Add(item1);
            ddlTitle.Items.Add(item2);
            ddlTitle.Items.Add(item3);
            ddlTitle.Items.Add(item4);
            ddlTitle.Items.Add(item5);
            ddlTitle.Attributes.Add("onchange", "setGender(2," + i.ToString() + ")");
            divddlTitle.Controls.Add(ddlTitle);

            RequiredFieldValidator prefixvc = new RequiredFieldValidator();
            prefixvc.ControlToValidate = ddlTitle.ID;
            prefixvc.ErrorMessage = "Select Title";
            prefixvc.ValidationGroup = "pax";
            prefixvc.InitialValue = "-1";
            prefixvc.SetFocusOnError = true;
            divddlTitle.Controls.Add(prefixvc);

            prefix.Controls.Add(divlblPrefix);
            prefix.Controls.Add(divddlTitle);

            HtmlGenericControl divlblFirstName = new HtmlGenericControl("Div");
            divlblFirstName.Attributes.Add("class", "col-md-3");
            divlblFirstName.InnerHtml = "<div class='col-md-3'>First&nbsp;Name&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divtxtFirstName = new HtmlGenericControl("Div");
            divtxtFirstName.Attributes.Add("class", "col-md-3");

            TextBox txtFirstName = new TextBox();
            txtFirstName.ID = "txtFirstName" + 2 + i.ToString();
            txtFirstName.CssClass = "form-control";
            divtxtFirstName.Controls.Add(txtFirstName);

            RequiredFieldValidator fnamevc = new RequiredFieldValidator();
            fnamevc.ControlToValidate = txtFirstName.ID;
            fnamevc.ErrorMessage = "Enter First Name";
            fnamevc.ValidationGroup = "pax";
            fnamevc.InitialValue = "";
            fnamevc.SetFocusOnError = true;
            divtxtFirstName.Controls.Add(fnamevc);

            prefix.Controls.Add(divlblFirstName);
            prefix.Controls.Add(divtxtFirstName);

            HtmlGenericControl divlblLastName = new HtmlGenericControl("Div");
            divlblLastName.Attributes.Add("class", "col-md-3");
            divlblLastName.InnerHtml = "<div class='col-md-3'>Last&nbsp;Name&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divtxtLastName = new HtmlGenericControl("Div");
            divtxtLastName.Attributes.Add("class", "col-md-3");

            TextBox txtLastName = new TextBox();
            txtLastName.ID = "txtLastName" + 2 + i.ToString();
            txtLastName.CssClass = "form-control";
            divtxtLastName.Controls.Add(txtLastName);

            RequiredFieldValidator lnamevc = new RequiredFieldValidator();
            lnamevc.ControlToValidate = txtLastName.ID;
            lnamevc.ErrorMessage = "Enter Last Name";
            lnamevc.ValidationGroup = "pax";
            lnamevc.InitialValue = "";
            lnamevc.SetFocusOnError = true;
            divtxtLastName.Controls.Add(lnamevc);

            prefix.Controls.Add(divlblLastName);
            prefix.Controls.Add(divtxtLastName);

            HtmlGenericControl divhdfPaxType = new HtmlGenericControl("Div");

            HiddenField hdfPaxType = new HiddenField();
            hdfPaxType.ID = "hdfPaxType" + pax + i.ToString();
            hdfPaxType.Value = "C";
            divhdfPaxType.InnerHtml += "</br> </br>";
            divhdfPaxType.Controls.Add(hdfPaxType);

            prefix.Controls.Add(divhdfPaxType);

            HtmlGenericControl divlblGender = new HtmlGenericControl("Div");
            divlblGender.Attributes.Add("class", "col-md-3");
            divlblGender.InnerHtml = "<div class='col-md-3'>Gender&nbsp;&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divddlGender = new HtmlGenericControl();
            divddlGender.Attributes.Add("class", "col-md-3");


            DropDownList ddlGender = new DropDownList();
            ddlGender.ID = "ddlGender" + 2 + i.ToString();
            ddlGender.CssClass = "form-control";
            ddlGender.CausesValidation = true;
            ddlGender.ValidationGroup = "pax";
            divddlGender.Controls.Add(ddlGender);

            RequiredFieldValidator gendervc = new RequiredFieldValidator();
            gendervc.ControlToValidate = ddlGender.ID;
            gendervc.ErrorMessage = "Select Gender";
            gendervc.InitialValue = "-1";
            gendervc.SetFocusOnError = true;
            gendervc.ValidationGroup = "pax";
            BindGender(ddlGender);
            divddlGender.Controls.Add(gendervc);

            prefix.Controls.Add(divlblGender);
            prefix.Controls.Add(divddlGender);

           

            HtmlGenericControl divlblIDType = new HtmlGenericControl("Div");
            divlblIDType.Attributes.Add("class", "col-md-3");
            divlblIDType.InnerHtml = "<div class='col-md-3'>Identity&nbsp;Type&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divddlIDType = new HtmlGenericControl("Div");
            divddlIDType.Attributes.Add("class", "col-md-3");

            DropDownList ddlIDType = new DropDownList();
            ddlIDType.ID = "ddlIDType" + 2 + i.ToString();
            ddlIDType.CssClass = "form-control";
            ListItem item11 = new ListItem("Passport", "Passport");
            ListItem item21 = new ListItem("Identity Card", "Identity Card");
            ddlIDType.Items.Add(item11);
            ddlIDType.Items.Add(item21);
            divddlIDType.Controls.Add(ddlIDType);

            prefix.Controls.Add(divlblIDType);
            prefix.Controls.Add(divddlIDType);

            HtmlGenericControl divlblIDNo = new HtmlGenericControl("Div");
            divlblIDNo.Attributes.Add("class", "col-md-3");
            divlblIDNo.InnerHtml = "<div class='col-md-3'>Identity&nbsp;No&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divtxtIDNo = new HtmlGenericControl("Div");
            divtxtIDNo.Attributes.Add("class", "col-md-3");

            TextBox txtIDNo = new TextBox();
            txtIDNo.ID = "txtIDNo" + 2 + i.ToString();
            txtIDNo.CssClass = "form-control";
            txtIDNo.CausesValidation = true;
            txtIDNo.ValidationGroup = "pax";
            divtxtIDNo.Controls.Add(txtIDNo);

            RequiredFieldValidator idnoevc = new RequiredFieldValidator();
            idnoevc.ControlToValidate = txtIDNo.ID;
            idnoevc.ErrorMessage = "Enter Identity No";
            idnoevc.InitialValue = "";
            idnoevc.SetFocusOnError = true;
            idnoevc.ValidationGroup = "pax";
            divtxtIDNo.Controls.Add(idnoevc);

            prefix.Controls.Add(divlblIDNo);
            prefix.Controls.Add(divtxtIDNo);

            HtmlGenericControl divlblNation = new HtmlGenericControl("Div");
            divlblNation.Attributes.Add("class", "col-md-3");
            divlblNation.InnerHtml = "<div class='col-md-3'>Nationality&nbsp;&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divddlNationality = new HtmlGenericControl("Div");
            divddlNationality.Attributes.Add("class", "col-md-3");

            DropDownList ddlNationality = new DropDownList();
            ddlNationality.ID = "ddlNationality" + 2 + i.ToString();
            ddlNationality.CssClass = "form-control";
            ddlNationality.CausesValidation = true;
            ddlNationality.ValidationGroup = "pax";
            BindNationality(ddlNationality);
            divddlNationality.Controls.Add(ddlNationality);

            RequiredFieldValidator nationvc = new RequiredFieldValidator();
            nationvc.ControlToValidate = ddlNationality.ID;
            nationvc.ErrorMessage = "Select Nationality";
            nationvc.InitialValue = "-1";
            nationvc.SetFocusOnError = true;
            nationvc.ValidationGroup = "pax";
            divddlNationality.Controls.Add(nationvc);

            prefix.Controls.Add(divlblNation);
            prefix.Controls.Add(divddlNationality);

            HtmlGenericControl divlblDOB = new HtmlGenericControl("Div");
            divlblDOB.Attributes.Add("class", "col-md-3");
            divlblDOB.InnerHtml = "<div class='col-md-3'>DOB<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divDOB = new HtmlGenericControl("Div");
            divDOB.Attributes.Add("class", "col-md-3");

            DropDownList ddlDay = new DropDownList();
            ddlDay.ID = "ddlDay" + 2 + i.ToString();
            ddlDay.Width = new Unit(30, UnitType.Percentage);
            ddlDay.CssClass = "form-control pull-left ";
            ddlDay.Attributes.Add("onchange", "ValidatePaxDOB(2)");
            BindDates(ddlDay);

            DropDownList ddlMonth = new DropDownList();
            ddlMonth.ID = "ddlMonth" + 2 + i.ToString();
            ddlMonth.Width = new Unit(30, UnitType.Percentage);
            ddlMonth.CssClass = "form-control pull-left ";
            ddlMonth.Attributes.Add("onchange", "ValidatePaxDOB(2)");
            BindMonths(ddlMonth);

            DropDownList ddlYear = new DropDownList();
            ddlYear.ID = "ddlYear" + 2 + i.ToString();
            ddlYear.Width = new Unit(40, UnitType.Percentage);
            ddlYear.CssClass = "form-control pull-left ";
            ddlYear.Attributes.Add("onchange", "ValidatePaxDOB(2)");
            BindYears(ddlYear);

            Label lblError = new Label();
            lblError.ID = "lblError" + 2 + i.ToString();

            divDOB.Controls.Add(ddlDay);
            divDOB.Controls.Add(ddlMonth);
            divDOB.Controls.Add(ddlYear);
            divDOB.Controls.Add(lblError);

            RequiredFieldValidator dayvc = new RequiredFieldValidator();
            dayvc.ControlToValidate = ddlDay.ID;
            dayvc.ErrorMessage = "Select Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            dayvc.InitialValue = "-1";
            dayvc.SetFocusOnError = true;
            dayvc.ValidationGroup = "pax";
            divDOB.Controls.Add(dayvc);
            

            RequiredFieldValidator monthvc = new RequiredFieldValidator();
            monthvc.ControlToValidate = ddlMonth.ID;
            monthvc.ErrorMessage = "Select Month&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            monthvc.InitialValue = "-1";
            monthvc.SetFocusOnError = true;
            monthvc.ValidationGroup = "pax";
            divDOB.Controls.Add(monthvc);

            RequiredFieldValidator yearvc = new RequiredFieldValidator();
            yearvc.ControlToValidate = ddlYear.ID;
            yearvc.ErrorMessage = "Select Year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            yearvc.InitialValue = "-1";
            yearvc.SetFocusOnError = true;
            yearvc.ValidationGroup = "pax";
            divDOB.Controls.Add(yearvc);


            prefix.Controls.Add(divlblDOB);
            prefix.Controls.Add(divDOB);

            detailRow.Cells.Add(prefix);

            tblPassengers.Rows.Add(detailRow);
            pax++;
        }

        i = 0;

        for (; i < InsRequest.Infants; i++)
        {
            HtmlTableRow hRow = new HtmlTableRow();
            HtmlTableCell hCell = new HtmlTableCell();
            hCell.InnerHtml += "<b>" + " Infant " + (i + 1).ToString() + "</b>";

            hCell.ColSpan = 8;
            hCell.Attributes.Add("class", "subgray-header");
            hRow.Cells.Add(hCell);
            tblPassengers.Rows.Add(hRow);

            HtmlTableRow detailRow = new HtmlTableRow();
            HtmlTableCell prefix = new HtmlTableCell();

            HtmlGenericControl divlblPrefix = new HtmlGenericControl("Div");
            divlblPrefix.Attributes.Add("class", "col-md-3");
            divlblPrefix.InnerHtml = "<div class='col-md-3'>Title&nbsp;<span style='color:red'>*</span>:</div>";


            HtmlGenericControl divddlTitle = new HtmlGenericControl("Div");
            divddlTitle.Attributes.Add("class", "col-md-3");

            DropDownList ddlTitle = new DropDownList();
            ddlTitle.ID = "Prefix" + 3 + i.ToString();
            ddlTitle.CssClass = "form-control";
            ddlTitle.CausesValidation = true;
            ddlTitle.ValidationGroup = "pax";
            ListItem item1 = new ListItem("Select Title", "-1");
            ListItem item2 = new ListItem("Mr", "Mr");
            ListItem item3 = new ListItem("Mrs", "Mrs");
            ListItem item4 = new ListItem("Ms", "Ms");
            ListItem item5 = new ListItem("Dr", "Dr");
            ddlTitle.Items.Add(item1);
            ddlTitle.Items.Add(item2);
            ddlTitle.Items.Add(item3);
            ddlTitle.Items.Add(item4);
            ddlTitle.Items.Add(item5);
            ddlTitle.Attributes.Add("onchange", "setGender(3," + i.ToString() + ")");
            divddlTitle.Controls.Add(ddlTitle);

            RequiredFieldValidator prefixvc = new RequiredFieldValidator();
            prefixvc.ControlToValidate = ddlTitle.ID;
            prefixvc.ErrorMessage = "Select Title";
            prefixvc.ValidationGroup = "pax";
            prefixvc.InitialValue = "-1";
            prefixvc.SetFocusOnError = true;
            divddlTitle.Controls.Add(prefixvc);

            prefix.Controls.Add(divlblPrefix);
            prefix.Controls.Add(divddlTitle);


            HtmlGenericControl divlblFirstName = new HtmlGenericControl("Div");
            divlblFirstName.Attributes.Add("class", "col-md-3");
            divlblFirstName.InnerHtml = "<div class='col-md-3'>First&nbsp;Name&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divtxtFirstName = new HtmlGenericControl("Div");
            divtxtFirstName.Attributes.Add("class", "col-md-3");

            TextBox txtFirstName = new TextBox();
            txtFirstName.ID = "txtIFirstName" + 3 + i.ToString();
            txtFirstName.CssClass = "form-control";
            divtxtFirstName.Controls.Add(txtFirstName);

            RequiredFieldValidator fnamevc = new RequiredFieldValidator();
            fnamevc.ControlToValidate = txtFirstName.ID;
            fnamevc.ErrorMessage = "Enter First Name";
            fnamevc.ValidationGroup = "pax";
            fnamevc.InitialValue = "";
            fnamevc.SetFocusOnError = true;
            divtxtFirstName.Controls.Add(fnamevc);


            prefix.Controls.Add(divlblFirstName);
            prefix.Controls.Add(divtxtFirstName);

            HtmlGenericControl divlblLastName = new HtmlGenericControl("Div");
            divlblLastName.Attributes.Add("class", "col-md-3");
            divlblLastName.InnerHtml = "<div class='col-md-3'>Last&nbsp;Name&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divtxtLastName = new HtmlGenericControl("Div");
            divtxtLastName.Attributes.Add("class", "col-md-3");

            TextBox txtLastName = new TextBox();
            txtLastName.ID = "txtILastName" + 3 + i.ToString();
            txtLastName.CssClass = "form-control";
            divtxtLastName.Controls.Add(txtLastName);

            RequiredFieldValidator lnamevc = new RequiredFieldValidator();
            lnamevc.ControlToValidate = txtLastName.ID;
            lnamevc.ErrorMessage = "Enter Last Name";
            lnamevc.ValidationGroup = "pax";
            lnamevc.InitialValue = "";
            lnamevc.SetFocusOnError = true;
            divtxtLastName.Controls.Add(lnamevc);

            prefix.Controls.Add(divlblLastName);
            prefix.Controls.Add(divtxtLastName);

            HtmlGenericControl divhdfPaxType = new HtmlGenericControl("Div");

            HiddenField hdfPaxType = new HiddenField();
            hdfPaxType.ID = "hdfPaxType" + pax + i.ToString();
            hdfPaxType.Value = "I";
            divhdfPaxType.InnerHtml += "</br> </br>";
            divhdfPaxType.Controls.Add(hdfPaxType);

            prefix.Controls.Add(divhdfPaxType);

            HtmlGenericControl divlblGender = new HtmlGenericControl("Div");
            divlblGender.Attributes.Add("class", "col-md-3");
            divlblGender.InnerHtml = "<div class='col-md-3'>Gender&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divddlGender = new HtmlGenericControl("Div");
            divddlGender.Attributes.Add("class", "col-md-3");

            DropDownList ddlGender = new DropDownList();
            ddlGender.ID = "ddlGender" + 3 + i.ToString();
            ddlGender.CssClass="form-control";
            ddlGender.CausesValidation = true;
            ddlGender.ValidationGroup = "pax";
            divddlGender.Controls.Add(ddlGender);

            RequiredFieldValidator gendervc = new RequiredFieldValidator();
            gendervc.ControlToValidate = ddlGender.ID;
            gendervc.ErrorMessage = "Select Gender";
            gendervc.InitialValue = "-1";
            gendervc.SetFocusOnError = true;
            gendervc.ValidationGroup = "pax";
            BindGender(ddlGender);
            divddlGender.Controls.Add(gendervc);

            prefix.Controls.Add(divlblGender);
            prefix.Controls.Add(divddlGender);

            

            HtmlGenericControl divlblIDType = new HtmlGenericControl("Div");
            divlblIDType.Attributes.Add("class", "col-md-3");
            divlblIDType.InnerHtml = "<div class='col-md-3'>Identity&nbsp;Type&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divddlIDType = new HtmlGenericControl("Div");
            divddlIDType.Attributes.Add("class", "col-md-3");

            DropDownList ddlIDType = new DropDownList();
            ddlIDType.ID = "ddlIDType" + 3 + i.ToString();
            ddlIDType.CssClass = "form-control";
            ListItem item11 = new ListItem("Passport", "Passport");
            ListItem item21 = new ListItem("Identity Card", "Identity Card");
            ddlIDType.Items.Add(item11);
            ddlIDType.Items.Add(item21);
            divddlIDType.Controls.Add(ddlIDType);

            prefix.Controls.Add(divlblIDType);
            prefix.Controls.Add(divddlIDType);

            HtmlGenericControl divlblIDNo = new HtmlGenericControl("Div");
            divlblIDNo.Attributes.Add("class", "col-md-3");
            divlblIDNo.InnerHtml = "<div class='col-md-3'>Identity&nbsp;No&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divtxtIDNo = new HtmlGenericControl("Div");
            divtxtIDNo.Attributes.Add("class", "col-md-3");

            TextBox txtIDNo = new TextBox();
            txtIDNo.ID = "txtIDNo" + 3 + i.ToString();
            txtIDNo.CssClass = "form-control";
            txtIDNo.CausesValidation = true;
            txtIDNo.ValidationGroup = "pax";
            divtxtIDNo.Controls.Add(txtIDNo);

            RequiredFieldValidator idnoevc = new RequiredFieldValidator();
            idnoevc.ControlToValidate = txtIDNo.ID;
            idnoevc.ErrorMessage = "Enter Identity No";
            idnoevc.InitialValue = "";
            idnoevc.SetFocusOnError = true;
            idnoevc.ValidationGroup = "pax";
            divtxtIDNo.Controls.Add(idnoevc);

            prefix.Controls.Add(divlblIDNo);
            prefix.Controls.Add(divtxtIDNo);

            HtmlGenericControl divlblNation = new HtmlGenericControl("Div");
            divlblNation.Attributes.Add("class", "col-md-3");
            divlblNation.InnerHtml = "<div class='col-md-3'>Nationality&nbsp;<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divddlNationality = new HtmlGenericControl("Div");
            divddlNationality.Attributes.Add("class", "col-md-3");

            DropDownList ddlNationality = new DropDownList();
            ddlNationality.ID = "ddlNationality" + 3 + i.ToString();
            ddlNationality.CssClass = "form-control";
            ddlNationality.CausesValidation = true;
            ddlNationality.ValidationGroup = "pax";
            BindNationality(ddlNationality);
            divddlNationality.Controls.Add(ddlNationality);

            RequiredFieldValidator nationvc = new RequiredFieldValidator();
            nationvc.ControlToValidate = ddlNationality.ID;
            nationvc.ErrorMessage = "Select Nationality";
            nationvc.InitialValue = "-1";
            nationvc.SetFocusOnError = true;
            nationvc.ValidationGroup = "pax";
            divddlNationality.Controls.Add(nationvc);

            prefix.Controls.Add(divlblNation);
            prefix.Controls.Add(divddlNationality);

            HtmlGenericControl divlblDOB = new HtmlGenericControl("Div");
            divlblDOB.Attributes.Add("class", "col-md-3");
            divlblDOB.InnerHtml = "<div class='col-md-3'>DOB<span style='color:red'>*</span>:</div>";

            HtmlGenericControl divDOB = new HtmlGenericControl("Div");
            divDOB.Attributes.Add("class", "col-md-3");

            DropDownList ddlDay = new DropDownList();
            ddlDay.ID = "ddlDay" + 3 + i.ToString();
            ddlDay.Width = new Unit(30, UnitType.Percentage);
            ddlDay.CssClass = "form-control pull-left ";
            ddlDay.Attributes.Add("onchange", "ValidatePaxDOB(3)");
            BindDates(ddlDay);

            DropDownList ddlMonth = new DropDownList();
            ddlMonth.ID = "ddlMonth" + 3 + i.ToString();
            ddlMonth.Width = new Unit(30, UnitType.Percentage);
            ddlMonth.CssClass = "form-control pull-left ";
            ddlMonth.Attributes.Add("onchange", "ValidatePaxDOB(3)");
            BindMonths(ddlMonth);

            DropDownList ddlYear = new DropDownList();
            ddlYear.ID = "ddlYear" + 3 + i.ToString();
            ddlYear.Width = new Unit(40, UnitType.Percentage);
            ddlYear.CssClass = "form-control pull-left ";
            ddlYear.Attributes.Add("onchange", "ValidatePaxDOB(3)");
            BindYears(ddlYear);

            Label lblError = new Label();
            lblError.ID = "lblError" + 3 + i.ToString();
                       

            divDOB.Controls.Add(ddlDay);
            divDOB.Controls.Add(ddlMonth);
            divDOB.Controls.Add(ddlYear);
            divDOB.Controls.Add(lblError);

            RequiredFieldValidator dayvc = new RequiredFieldValidator();
            dayvc.ControlToValidate = ddlDay.ID;
            dayvc.ErrorMessage = "Select Date&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            dayvc.InitialValue = "-1";
            dayvc.SetFocusOnError = true;
            dayvc.ValidationGroup = "pax";
            divDOB.Controls.Add(dayvc);
            

            RequiredFieldValidator monthvc = new RequiredFieldValidator();
            monthvc.ControlToValidate = ddlMonth.ID;
            monthvc.ErrorMessage = "Select Month&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            monthvc.InitialValue = "-1";
            monthvc.SetFocusOnError = true;
            monthvc.ValidationGroup = "pax";
            divDOB.Controls.Add(monthvc);

            RequiredFieldValidator yearvc = new RequiredFieldValidator();
            yearvc.ControlToValidate = ddlYear.ID;
            yearvc.ErrorMessage = "Select Year&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
            yearvc.InitialValue = "-1";
            yearvc.SetFocusOnError = true;
            yearvc.ValidationGroup = "pax";
            divDOB.Controls.Add(yearvc);


            prefix.Controls.Add(divlblDOB);
            prefix.Controls.Add(divDOB);

            detailRow.Cells.Add(prefix);

            tblPassengers.Rows.Add(detailRow);
            pax++;
        }
    }

    //To bind the Nominee fields
    void BindNominee()
    {
        HtmlTableRow headerRow = new HtmlTableRow();
        HtmlTableCell headerCell = new HtmlTableCell();
        headerCell.InnerHtml = "<b>Nominee </b>";
        headerCell.Attributes.Add("class", "subgray-header");
        headerCell.ColSpan = 1;
        headerRow.Cells.Add(headerCell);

        HtmlTableRow detailrow = new HtmlTableRow();
        HtmlTableCell detailcell = new HtmlTableCell();

        HtmlGenericControl divlblNomieeFirstName = new HtmlGenericControl("Div");
        divlblNomieeFirstName.Attributes.Add("class", "col-md-3");
        divlblNomieeFirstName.InnerHtml = "<div class='col-md-3'>First&nbsp;Name<span style='color:red'>*</span>:</div>";

        HtmlGenericControl divNomineeFirstName = new HtmlGenericControl("Div"); 
        divNomineeFirstName.Attributes.Add("class", "col-md-3");

        TextBox txtNomineeFirstName = new TextBox();
        txtNomineeFirstName.ID = "txtNomineeFirstName";
        txtNomineeFirstName.CssClass = "form-control";
        txtNomineeFirstName.CausesValidation = true;
        txtNomineeFirstName.ValidationGroup = "pax";
        txtNomineeFirstName.MaxLength = 50;
        divNomineeFirstName.Controls.Add(txtNomineeFirstName);

        RequiredFieldValidator nomineeFirstNamevc = new RequiredFieldValidator();
        nomineeFirstNamevc.ControlToValidate = txtNomineeFirstName.ID;
        nomineeFirstNamevc.ErrorMessage = "Enter&nbsp;First&nbsp;Name";
        nomineeFirstNamevc.InitialValue = "";
        nomineeFirstNamevc.SetFocusOnError = true;
        nomineeFirstNamevc.ValidationGroup = "pax";
        divNomineeFirstName.Controls.Add(nomineeFirstNamevc);

        detailcell.Controls.Add(divlblNomieeFirstName);
        detailcell.Controls.Add(divNomineeFirstName);
        
        //Last Name
        HtmlGenericControl divlblNomieeLastName = new HtmlGenericControl("Div");
        divlblNomieeLastName.Attributes.Add("class", "col-md-3");
        divlblNomieeLastName.InnerHtml = "<div class='col-md-3'>Last&nbsp;Name<span style='color:red'>*</span>:</div>";

        HtmlGenericControl divNomineeLastName = new HtmlGenericControl("Div");
        divNomineeLastName.Attributes.Add("class", "col-md-3");

        TextBox txtNomineeLastName = new TextBox();
        txtNomineeLastName.ID = "txtNomineeLastName";
        txtNomineeLastName.CssClass = "form-control";
        txtNomineeLastName.CausesValidation = true;
        txtNomineeLastName.ValidationGroup = "pax";
        txtNomineeLastName.MaxLength = 50;
        divNomineeLastName.Controls.Add(txtNomineeLastName);

        RequiredFieldValidator nomineeLastNamevc = new RequiredFieldValidator();
        nomineeLastNamevc.ControlToValidate = txtNomineeLastName.ID;
        nomineeLastNamevc.ErrorMessage = "Enter&nbsp;last&nbsp;Name";
        nomineeLastNamevc.InitialValue = "";
        nomineeLastNamevc.SetFocusOnError = true;
        nomineeLastNamevc.ValidationGroup = "pax";
        divNomineeLastName.Controls.Add(nomineeLastNamevc);

        detailcell.Controls.Add(divlblNomieeLastName);
        detailcell.Controls.Add(divNomineeLastName);

        //Mobile Number
        HtmlGenericControl divlblNomieeMobilenNo = new HtmlGenericControl("Div");
        divlblNomieeMobilenNo.Attributes.Add("class", "col-md-3");
        divlblNomieeMobilenNo.InnerHtml = "<div class='col-md-3'>Mobile&nbsp;Number<span style='color:red'>*</span>:</div>";

        HtmlGenericControl divNomineeMobileNo = new HtmlGenericControl("Div");
        divNomineeMobileNo.Attributes.Add("class", "col-md-3");

        TextBox txtNomineeMobileNo = new TextBox();
        txtNomineeMobileNo.ID = "txtNomineeMobileNo";
        txtNomineeMobileNo.CssClass = "form-control";
        txtNomineeMobileNo.CausesValidation = true;
        txtNomineeMobileNo.ValidationGroup = "pax";
        txtNomineeMobileNo.MaxLength = 15;
        divNomineeMobileNo.Controls.Add(txtNomineeMobileNo);

        RequiredFieldValidator nomineeMobileNovc = new RequiredFieldValidator();
        nomineeMobileNovc.ControlToValidate = txtNomineeMobileNo.ID;
        nomineeMobileNovc.ErrorMessage = "Enter Mobile Number";
        nomineeMobileNovc.InitialValue = "";
        nomineeMobileNovc.SetFocusOnError = true;
        nomineeMobileNovc.ValidationGroup = "pax";
        divNomineeMobileNo.Controls.Add(nomineeMobileNovc);

        detailcell.Controls.Add(divlblNomieeMobilenNo);
        detailcell.Controls.Add(divNomineeMobileNo);

        //Email ID

        HtmlGenericControl divlblNomineeEmail = new HtmlGenericControl("Div");
        divlblNomineeEmail.Attributes.Add("class", "col-md-3");
        divlblNomineeEmail.InnerHtml = "<div  class='col-md-3' >Email&nbsp;<span style='color:red;'>*</span>:</Div>";

        HtmlGenericControl divNomineeEmail = new HtmlGenericControl("Div"); // For Adult Country
        divNomineeEmail.Attributes.Add("class", "col-md-3");

        TextBox txtNomineeEmail = new TextBox();
        txtNomineeEmail.ID = "txtNomineeEmail";
        txtNomineeEmail.CssClass = "form-control";
        txtNomineeEmail.CausesValidation = true;
        txtNomineeEmail.ValidationGroup = "pax";
        txtNomineeEmail.MaxLength = 100;
        txtNomineeEmail.Text = "Enter multiple emails Separate by semicolon";
        txtNomineeEmail.Attributes.Add("onblur", "if( this.value=='' ) {this.value='Enter multiple emails Separate by semicolon'; }");
        txtNomineeEmail.Attributes.Add("onfocus", "if( this.value=='Enter multiple emails Separate by semicolon' ) {this.value=''; }");
        divNomineeEmail.Controls.Add(txtNomineeEmail);

        RequiredFieldValidator emailvc = new RequiredFieldValidator();
        emailvc.ControlToValidate = txtNomineeEmail.ID;
        emailvc.ErrorMessage = "Enter Email";
        emailvc.InitialValue = "";
        emailvc.SetFocusOnError = true;
        emailvc.ValidationGroup = "pax";
        divNomineeEmail.Controls.Add(emailvc);

        RegularExpressionValidator emailrv = new RegularExpressionValidator();
        emailrv.ControlToValidate = txtNomineeEmail.ID;
        emailrv.ErrorMessage = "Invalid Email address";
        emailrv.SetFocusOnError = true;
        emailrv.ValidationGroup = "pax";
        emailrv.ValidationExpression = @"^((\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*)*([;])*)*$";
        divNomineeEmail.Controls.Add(emailrv);

        detailcell.Controls.Add(divlblNomineeEmail);
        detailcell.Controls.Add(divNomineeEmail);


        //Country

        HtmlGenericControl divlblNomineeCountry = new HtmlGenericControl("Div"); 
        divlblNomineeCountry.Attributes.Add("class", "col-md-3");
        divlblNomineeCountry.InnerHtml = "<div  class='col-md-3' >Country&nbsp;of&nbsp;residence&nbsp;<span style='color:red;'>*</span>:</Div>";

        HtmlGenericControl divddlCountry = new HtmlGenericControl("Div"); 
        divddlCountry.Attributes.Add("class", "col-md-3");

        DropDownList ddlNomineeCountry = new DropDownList();
        ddlNomineeCountry.ID = "ddlNomineeCountry";
        ddlNomineeCountry.CssClass = "form-control";
        ddlNomineeCountry.Attributes.Add("onchange", "getCities(1,0)");
        BindCountry(ddlNomineeCountry);
        divddlCountry.Controls.Add(ddlNomineeCountry);

        RequiredFieldValidator countryvc = new RequiredFieldValidator();
        countryvc.ControlToValidate = ddlNomineeCountry.ID;
        countryvc.ErrorMessage = "Select Country";
        countryvc.InitialValue = "-1";
        countryvc.SetFocusOnError = true;
        countryvc.ValidationGroup = "pax";
        divddlCountry.Controls.Add(countryvc);

        detailcell.Controls.Add(divlblNomineeCountry);
        detailcell.Controls.Add(divddlCountry);

        //Remarks
        HtmlGenericControl divlblNomineeRemarks = new HtmlGenericControl("Div");
        divlblNomineeRemarks.Attributes.Add("class", "col-md-3");
        divlblNomineeRemarks.InnerHtml = "<div  class='col-md-3' >Remarks:</Div>";

        HtmlGenericControl divNomineeRemarks = new HtmlGenericControl("Div");
        divNomineeRemarks.Attributes.Add("class", "col-md-3");

        TextBox txtNomineeRemarks = new TextBox();
        txtNomineeRemarks.ID = "txtNomineeRemarks";
        txtNomineeRemarks.CssClass = "form-control";
        txtNomineeRemarks.CausesValidation = true;
        txtNomineeRemarks.ValidationGroup = "pax";
        txtNomineeRemarks.MaxLength = 75;
        txtNomineeRemarks.TextMode = TextBoxMode.MultiLine;
        //remarks.Controls.Add(lblRemarks);
        divNomineeRemarks.Controls.Add(txtNomineeRemarks);
        detailcell.Controls.Add(divlblNomineeRemarks);
        detailcell.Controls.Add(divNomineeRemarks);

        detailrow.Cells.Add(detailcell);

        tblPassengers.Rows.Add(headerRow);
        tblPassengers.Rows.Add(detailrow);

    }
    void BindGender(DropDownList ddlGender)
    {
        ListItem item1 = new ListItem("Select Gender", "-1");
        ListItem item2 = new ListItem("Male", "Male");
        ListItem item3 = new ListItem("Female", "Female");

        ddlGender.Items.Add(item1);
        ddlGender.Items.Add(item2);
        ddlGender.Items.Add(item3);
    }

    void BindNationality(DropDownList ddlNationality)
    {
        ListItem item = new ListItem("Select Nationality", "-1");
        
        ddlNationality.DataSource = CT.Core.Country.GetNationalityList();
        ddlNationality.DataTextField = "key";
        ddlNationality.DataValueField = "value";
        ddlNationality.DataBind();        
        ddlNationality.Items.Insert(0, item);
    }

    void BindCountry(DropDownList ddlCountry)
    {
        ddlCountry.DataSource = CT.Core.Country.GetCountryList();
        ddlCountry.DataTextField = "key";
        ddlCountry.DataValueField = "value";
        ddlCountry.DataBind();
        ddlCountry.Items.Insert(0, new ListItem("Select Country", "-1"));        
    }       

    void BindDates(DropDownList ddlDay)
    {
        ddlDay.Items.Add(new ListItem("Day", "-1"));
        for (int i = 1; i <= 31; i++)
        {
            ddlDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }

    void BindMonths(DropDownList ddlMonth)
    {
        ddlMonth.Items.Add(new ListItem("Month", "-1"));
        string[] months = new string[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        for (int i = 1; i <= months.Length; i++ )
        {
            ddlMonth.Items.Add(new ListItem(months[i-1], i.ToString()));
        }
    }

    void BindYears(DropDownList ddlYear)
    {
        ddlYear.Items.Add(new ListItem("Year", "-1"));
        if (InsRequest.IsSeniorCitizen) //based on the current year we are binding the years the age between 76 to 85 years
        {
            //if currency is INR then senior citizen age should be 61- 80 yrs other wise 76 - 85 yrs.
            int fromYear = currencyCode == "INR" ? DateTime.Now.Year - 80 : DateTime.Now.Year - 85;
            int toYear = currencyCode == "INR" ? DateTime.Now.Year - 61 : DateTime.Now.Year - 75;
          
            for (int i = fromYear; i <= toYear; i++)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }
        else
        {
            for (int i = 1930; i <= DateTime.Now.Year; i++)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }
        }
    }

    protected void btnContinue_Click(object sender, EventArgs e)
    {
        try
        {
            //Genarating Booking Request
            InsuranceHeader header = new InsuranceHeader();
            header.Adults = InsRequest.Adults;
            header.Childs = InsRequest.Childs;
            header.Infants = InsRequest.Infants;
            header.IsSeniorCitizen = InsRequest.IsSeniorCitizen;
            header.CountryCode = InsRequest.CountryCode;
            header.CultureCode = InsRequest.CultureCode;
            header.DepartureStationCode = InsRequest.DepartureStationCode;
            header.DepartureAirlineCode = InsRequest.DepartureAirlineCode;
            header.DepartureCountryCode = InsRequest.DepartureCountryCode;
            header.DepartureFlightNo = InsRequest.DepartureFlightNo;
            header.ReturnAirlineCode = InsRequest.ReturnAirlineCode;
            header.ReturnDateTime = InsRequest.ReturnDateTime;
            header.ReturnFlightNo = InsRequest.ReturnFlightNo;
            header.DepartureDateTime = InsRequest.DepartureDateTime;
            header.ArrivalStationCode = InsRequest.ArrivalStationCode;
            header.ReturnAirlineCode = InsRequest.ReturnAirlineCode;
            header.ArrivalCountryCode = InsRequest.ArrivalCountryCode;
            header.ReturnFlightNo = InsRequest.ReturnFlightNo;
            header.AgentId = Settings.LoginInfo.AgentId;
            header.CreatedBy = (int)Settings.LoginInfo.UserID;
            header.LocationId = (int)Settings.LoginInfo.LocationID;
            header.Currency = Settings.LoginInfo.Currency;
            header.Product = "S";
            header.TransType = "B2B";
            //plan.CurrencyCode = header.CurrencyCode;
            header.PNR = txtPNR.Text.Trim();
            header.PseudoCode = (!string.IsNullOrEmpty(InsRequest.PseudoCode) ? InsRequest.PseudoCode : string.Empty);
            if (currencyCode == "INR") //Saving the nominee details if currency is INR
            {
                header.NomineeFirstName = ((TextBox)tblPassengers.FindControl("txtNomineeFirstName")).Text;
                header.NomineeLastName = ((TextBox)tblPassengers.FindControl("txtNomineeLastName")).Text;
                header.NomineeMobileNo = ((TextBox)tblPassengers.FindControl("txtNomineeMobileNo")).Text;
                header.NomineeEmailAddress = ((TextBox)tblPassengers.FindControl("txtNomineeEmail")).Text;
                header.NomineeCountry = ((DropDownList)tblPassengers.FindControl("ddlNomineeCountry")).SelectedValue;
                header.NomineeRemarks = ((TextBox)tblPassengers.FindControl("txtNomineeRemarks")).Text;
            }

            List<InsurancePassenger> passengers = new List<InsurancePassenger>();
            
            for (int i = 0; i < InsRequest.Adults; i++)
            {

                DropDownList ddlTitle = tblPassengers.FindControl("Prefix" + 1 + i.ToString()) as DropDownList;
                TextBox txtFirstName = tblPassengers.FindControl("txtAFirstName" + 1 + i.ToString()) as TextBox;
                TextBox txtLastName = tblPassengers.FindControl("txtALastName" + 1 + i.ToString()) as TextBox;
                DropDownList ddlGender = tblPassengers.FindControl("ddlGender" + 1 + i.ToString()) as DropDownList;
                DropDownList ddlNationality = tblPassengers.FindControl("ddlNationality" + 1 + i.ToString()) as DropDownList;
                TextBox txtPhone = tblPassengers.FindControl("txtAPhone" + 1 + i.ToString()) as TextBox;
                DropDownList ddlCountry = tblPassengers.FindControl("ddlCountry" + 1 + i.ToString()) as DropDownList;
                //DropDownList ddlCity = tblPassengers.FindControl("ddlCity" + 1 + i.ToString()) as DropDownList;
                HiddenField hdnCity = tblPassengers.FindControl("hdnCity" + 1 + i.ToString()) as HiddenField;
                TextBox txtEmail = tblPassengers.FindControl("txtEmail" + 1 + i.ToString()) as TextBox;
                DropDownList ddlIDType = tblPassengers.FindControl("ddlIDType" + 1 + i.ToString()) as DropDownList;
                TextBox txtIdNumber = tblPassengers.FindControl("txtIDNo" + 1 + i.ToString()) as TextBox;
                DropDownList ddlDate = tblPassengers.FindControl("ddlDay" + 1 + i.ToString()) as DropDownList;
                DropDownList ddlMonth = tblPassengers.FindControl("ddlMonth" + 1 + i.ToString()) as DropDownList;
                DropDownList ddlYear = tblPassengers.FindControl("ddlYear" + 1 + i.ToString()) as DropDownList;
                TextBox txtRemarks = tblPassengers.FindControl("txtRemarks" + 1 + i.ToString()) as TextBox;
                InsurancePassenger pax = new InsurancePassenger();
                pax.PaxType = "Adult";
                pax.FirstName = txtFirstName.Text;
                pax.LastName = txtLastName.Text;
                if (ddlGender.SelectedValue == "Male")
                {
                    pax.Gender = ZeusInsB2B.Zeus.GenderType.Male;
                }
                else
                {
                    pax.Gender = ZeusInsB2B.Zeus.GenderType.Female;
                }
                pax.Nationality = ddlNationality.SelectedValue;
                pax.PhoneNumber = txtPhone.Text;
                pax.Country = ddlCountry.SelectedItem.Text + "-" + ddlCountry.SelectedValue;
                pax.City = hdnCity.Value;//ddlCity.SelectedItem.Text;
                if (ddlIDType.SelectedItem.Text == "Passport")
                {
                    pax.DocumentType = ZeusInsB2B.Zeus.IdentificationType.Passport;
                }
                else
                {
                    pax.DocumentType = ZeusInsB2B.Zeus.IdentificationType.IdentificationCard;
                }
                pax.DocumentNo = txtIdNumber.Text;


                //pax.DOB = Convert.ToDateTime(ddlDate.SelectedValue + "/" + ddlMonth.SelectedValue + "/" + ddlYear.SelectedValue);
                DateTime tempDob = new DateTime(Convert.ToInt32(ddlYear.SelectedItem.Value), Convert.ToInt32(ddlMonth.SelectedItem.Value), Convert.ToInt32(ddlDate.SelectedItem.Value));
                pax.DOB = tempDob;
                pax.TempDOB = ddlDate.SelectedValue + "/" + ddlMonth.SelectedItem.Text + "/" + ddlYear.SelectedValue;
                pax.Title = ddlTitle.SelectedItem.Value;
                //Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "after date", Request["REMOTE_ADDR"]);
                pax.Email = txtEmail.Text;
                pax.IsInfant = false;
                pax.State = "";
                pax.PolicyNo = "";
                pax.PolicyUrlLink = "";
                pax.IsQualified = true;
                pax.PNR = txtPNR.Text.Trim();
                if (i == 0)
                {
                    header.ContactPersonName = pax.FirstName + pax.LastName;
                    header.EmailAddress = pax.Email;
                    pax.Remarks = txtRemarks.Text;
                }
                passengers.Add(pax);

            }

            for (int j = 0; j < InsRequest.Childs; j++)
            {
                DropDownList ddlTitle = tblPassengers.FindControl("Prefix" + 2 + j.ToString()) as DropDownList;
                TextBox txtFirstName = tblPassengers.FindControl("txtFirstName" + 2 + j.ToString()) as TextBox;
                TextBox txtLastName = tblPassengers.FindControl("txtLastName" + 2 + j.ToString()) as TextBox;
                DropDownList ddlGender = tblPassengers.FindControl("ddlGender" + 2 + j.ToString()) as DropDownList;
                DropDownList ddlNationality = tblPassengers.FindControl("ddlNationality" + 2 + j.ToString()) as DropDownList;
                DropDownList ddlIDType = tblPassengers.FindControl("ddlIDType" + 2 + j.ToString()) as DropDownList;
                TextBox txtIdNumber = tblPassengers.FindControl("txtIDNo" + 2 + j.ToString()) as TextBox;
                DropDownList ddlDate = tblPassengers.FindControl("ddlDay" + 2 + j.ToString()) as DropDownList;
                DropDownList ddlMonth = tblPassengers.FindControl("ddlMonth" + 2 + j.ToString()) as DropDownList;
                DropDownList ddlYear = tblPassengers.FindControl("ddlYear" + 2 + j.ToString()) as DropDownList;

                InsurancePassenger pax = new InsurancePassenger();
                pax.PaxType = "Child";
                pax.FirstName = txtFirstName.Text;
                pax.LastName = txtLastName.Text;
                if (ddlGender.SelectedValue == "Male")
                {
                    pax.Gender = ZeusInsB2B.Zeus.GenderType.Male;
                }
                else
                {
                    pax.Gender = ZeusInsB2B.Zeus.GenderType.Female;
                }
                pax.Nationality = ddlNationality.SelectedValue;
                pax.PhoneNumber = passengers[0].PhoneNumber;
                pax.Country = passengers[0].Country;
                pax.City = passengers[0].City;
                if (ddlIDType.SelectedItem.Text == "Passport")
                {
                    pax.DocumentType = ZeusInsB2B.Zeus.IdentificationType.Passport;
                }
                else
                {
                    pax.DocumentType = ZeusInsB2B.Zeus.IdentificationType.IdentificationCard;
                }
                pax.DocumentNo = txtIdNumber.Text;
                DateTime tempDob = new DateTime(Convert.ToInt32(ddlYear.SelectedItem.Value), Convert.ToInt32(ddlMonth.SelectedItem.Value), Convert.ToInt32(ddlDate.SelectedItem.Value));
                pax.DOB = tempDob;

                pax.TempDOB = ddlDate.SelectedValue + "/" + ddlMonth.SelectedItem.Text + "/" + ddlYear.SelectedValue; //sai
                pax.Title = ddlTitle.SelectedItem.Value;

                pax.Email = passengers[0].Email;
                pax.IsInfant = false;
                pax.State = "";
                pax.PolicyNo = "";
                pax.PolicyUrlLink = "";
                pax.IsQualified = true;

                passengers.Add(pax);

            }

            for (int k = 0; k < InsRequest.Infants; k++)
            {
                DropDownList ddlTitle = tblPassengers.FindControl("Prefix" + 3 + k.ToString()) as DropDownList;
                TextBox txtFirstName = tblPassengers.FindControl("txtIFirstName" + 3 + k.ToString()) as TextBox;
                TextBox txtLastName = tblPassengers.FindControl("txtILastName" + 3 + k.ToString()) as TextBox;
                DropDownList ddlGender = tblPassengers.FindControl("ddlGender" + 3 + k.ToString()) as DropDownList;
                DropDownList ddlNationality = tblPassengers.FindControl("ddlNationality" + 3 + k.ToString()) as DropDownList;
                DropDownList ddlIDType = tblPassengers.FindControl("ddlIDType" + 3 + k.ToString()) as DropDownList;
                TextBox txtIdNumber = tblPassengers.FindControl("txtIDNo" + 3 + k.ToString()) as TextBox;
                DropDownList ddlDate = tblPassengers.FindControl("ddlDay" + 3 + k.ToString()) as DropDownList;
                DropDownList ddlMonth = tblPassengers.FindControl("ddlMonth" + 3 + k.ToString()) as DropDownList;
                DropDownList ddlYear = tblPassengers.FindControl("ddlYear" + 3 + k.ToString()) as DropDownList;

                InsurancePassenger pax = new InsurancePassenger();
                pax.PaxType = "Infant";
                //pax.FirstName = ddlTitle.SelectedValue + "." + txtFirstName.Text;
                pax.FirstName = txtFirstName.Text;
                pax.LastName = txtLastName.Text;
                if (ddlGender.SelectedValue == "Male")
                {
                    pax.Gender = ZeusInsB2B.Zeus.GenderType.Male;
                }
                else
                {
                    pax.Gender = ZeusInsB2B.Zeus.GenderType.Female;
                }
                pax.Nationality = ddlNationality.SelectedValue;
                pax.PhoneNumber = passengers[0].PhoneNumber;
                pax.Country = passengers[0].Country;
                pax.City = passengers[0].City;
                if (ddlIDType.SelectedItem.Text == "Passport")
                {
                    pax.DocumentType = ZeusInsB2B.Zeus.IdentificationType.Passport;
                }
                else
                {
                    pax.DocumentType = ZeusInsB2B.Zeus.IdentificationType.IdentificationCard;
                }
                pax.DocumentNo = txtIdNumber.Text;
                DateTime tempDob = new DateTime(Convert.ToInt32(ddlYear.SelectedItem.Value), Convert.ToInt32(ddlMonth.SelectedItem.Value), Convert.ToInt32(ddlDate.SelectedItem.Value));
                pax.DOB = tempDob;

                pax.TempDOB = ddlDate.SelectedValue + "/" + ddlMonth.SelectedItem.Text + "/" + ddlYear.SelectedValue; //sai
                pax.Title = ddlTitle.SelectedItem.Value;

                pax.Email = passengers[0].Email;
                pax.IsInfant = true;
                pax.State = "";
                pax.PolicyNo = "";
                pax.PolicyUrlLink = "";
                pax.IsQualified = true;

                passengers.Add(pax);
            }
            
            header.InsPassenger = passengers;
             
            Session["Header"] = header;

            if (header.InsPassenger.Count > 0)
            {
                bool isAllowed = false;
                string paxName = string.Empty;
                foreach (InsurancePassenger  pax in header.InsPassenger)
                {
                    int age = (int)DateTime.Now.Subtract(pax.DOB).TotalDays;
                    //if (age < 31 || age >= 27375)
                    //if ((age >= 30 && age <= 27394 && !isSeniorCitizen) || (isSeniorCitizen && age > 27394) || (age >= 30 && age <= 21914 && !isSeniorCitizen) || (isSeniorCitizen && age > 21914))
                    if ((age >= 30 && age <= 27650 && !isSeniorCitizen) || (isSeniorCitizen && age > 27394) || (age >= 30 && age <= 21914 && !isSeniorCitizen) || (isSeniorCitizen && age > 21914))
                    {
                        isAllowed = true;//one passenge is eligible
                    }
                    paxName += pax.FirstName + " " + pax.LastName + ",";
                    
                }
                if (isAllowed) 
                {
                    lblError.Text = "";
                    //plan.UnQualifiedPassengers = null;
                    Response.Redirect("InsuranceConfirmation.aspx",false);
                    Session["InsPassengers"] = passengers;
                }
                else
                {
                    paxName = paxName.TrimEnd(',');
                    //Show the message "Atleast one passenger required for insurance booking".
                    lblError.Text =  isSeniorCitizen ? "Please enter D.O.B of the passenger's above 75-Years for " + paxName : "Please enter D.O.B of the passenger's between 30-days and 75-Years for " + paxName;
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to add Insurance Pax : " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    private void LoadControlState()
    {
        List<InsurancePassenger> passengers = (List<InsurancePassenger>)Session["InsPassengers"];
        int paxcount = 0;
        ScriptManager.RegisterStartupScript(this, typeof(Page), "fillCity", "fillCityOnSubmit()", true);
        for (int i = 0; i < InsRequest.Adults; i++)
        {
            
            DropDownList ddlTitle = tblPassengers.FindControl("Prefix" + 1 + i.ToString()) as DropDownList;
            TextBox txtFirstName = tblPassengers.FindControl("txtAFirstName" + 1 + i.ToString()) as TextBox;
            TextBox txtLastName = tblPassengers.FindControl("txtALastName" + 1 + i.ToString()) as TextBox;
            DropDownList ddlGender = tblPassengers.FindControl("ddlGender" + 1 + i.ToString()) as DropDownList;
            DropDownList ddlNationality = tblPassengers.FindControl("ddlNationality" + 1 + i.ToString()) as DropDownList;
            TextBox txtPhone = tblPassengers.FindControl("txtAPhone" + 1 + i.ToString()) as TextBox;
            DropDownList ddlCountry = tblPassengers.FindControl("ddlCountry" + 1 + i.ToString()) as DropDownList;
            DropDownList ddlCity = tblPassengers.FindControl("ddlCity" + 1 + i.ToString()) as DropDownList;
            HiddenField hdnCity = tblPassengers.FindControl("hdnCity" + 1 + i.ToString()) as HiddenField;
            TextBox txtEmail = tblPassengers.FindControl("txtEmail" + 1 + i.ToString()) as TextBox;
            DropDownList ddlIDType = tblPassengers.FindControl("ddlIDType" + 1 + i.ToString()) as DropDownList;
            TextBox txtIdNumber = tblPassengers.FindControl("txtIDNo" + 1 + i.ToString()) as TextBox;
            DropDownList ddlDate = tblPassengers.FindControl("ddlDay" + 1 + i.ToString()) as DropDownList;
            DropDownList ddlMonth = tblPassengers.FindControl("ddlMonth" + 1 + i.ToString()) as DropDownList;
            DropDownList ddlYear = tblPassengers.FindControl("ddlYear" + 1 + i.ToString()) as DropDownList;
            TextBox txtRemarks = tblPassengers.FindControl("txtRemarks" + 1 + i.ToString()) as TextBox;
            InsurancePassenger pax = passengers[i];
            if (!string.IsNullOrEmpty(pax.PNR))
            {
                txtPNR.Text = pax.PNR.Trim();
            }
            ddlTitle.Items.FindByText(pax.Title).Selected = true;
            txtFirstName.Text = pax.FirstName;
            txtLastName.Text = pax.LastName;
            if (pax.Gender == ZeusInsB2B.Zeus.GenderType.Male)
            {
                ddlGender.SelectedValue = "Male";
            }
            else
            {
                ddlGender.SelectedValue = "Female";
            }
            ddlNationality.SelectedValue = pax.Nationality;
            txtPhone.Text = pax.PhoneNumber;
            string[] country = pax.Country.Split('-');
            ddlCountry.SelectedValue = country[1];
            
            //ddlCity.Items.FindByText(hdfCode.Value).Selected = true;
            hdnCity.Value = pax.City;
            if (pax.DocumentType == ZeusInsB2B.Zeus.IdentificationType.Passport)
            {
                ddlIDType.SelectedItem.Text = "Passport";
            }
            else
            {
                ddlIDType.SelectedItem.Text = "Identity Card";
            }
            txtIdNumber.Text = pax.DocumentNo;

            string[] dob = pax.TempDOB.ToString().Split('/');
            ddlDate.Items.FindByText(dob[0]).Selected = true;
            ddlMonth.Items.FindByText(dob[1]).Selected = true;
            ddlYear.Items.FindByText(dob[2]).Selected = true;
            txtEmail.Text = pax.Email;
            if (i == 0)
            {
                txtRemarks.Text = pax.Remarks;
            }
            paxcount++;
        }
        //Child Details
        for (int j = 0; j < InsRequest.Childs; j++)
        {
            
            DropDownList ddlTitle = tblPassengers.FindControl("Prefix" + 2 + j.ToString()) as DropDownList;
            TextBox txtFirstName = tblPassengers.FindControl("txtFirstName" + 2 + j.ToString()) as TextBox;
            TextBox txtLastName = tblPassengers.FindControl("txtLastName" + 2 + j.ToString()) as TextBox;
            DropDownList ddlGender = tblPassengers.FindControl("ddlGender" + 2 + j.ToString()) as DropDownList;
            DropDownList ddlNationality = tblPassengers.FindControl("ddlNationality" + 2 + j.ToString()) as DropDownList;
            DropDownList ddlIDType = tblPassengers.FindControl("ddlIDType" + 2 + j.ToString()) as DropDownList;
            TextBox txtIdNumber = tblPassengers.FindControl("txtIDNo" + 2 + j.ToString()) as TextBox;
            DropDownList ddlDate = tblPassengers.FindControl("ddlDay" + 2 + j.ToString()) as DropDownList;
            DropDownList ddlMonth = tblPassengers.FindControl("ddlMonth" + 2 + j.ToString()) as DropDownList;
            DropDownList ddlYear = tblPassengers.FindControl("ddlYear" + 2 + j.ToString()) as DropDownList;

            InsurancePassenger pax = passengers[paxcount + j];
            ddlTitle.Items.FindByText(pax.Title).Selected = true;
            txtFirstName.Text = pax.FirstName;
            txtLastName.Text = pax.LastName;
            if (pax.Gender == ZeusInsB2B.Zeus.GenderType.Male)
            {
                ddlGender.SelectedValue = "Male";
            }
            else
            {
                ddlGender.SelectedValue = "Female";
            }
            ddlNationality.SelectedValue = pax.Nationality;
            passengers[0].PhoneNumber = pax.PhoneNumber;
            
            if (pax.DocumentType == ZeusInsB2B.Zeus.IdentificationType.Passport)
            {
                ddlIDType.SelectedItem.Text = "Passport";
            }
            else
            {
                ddlIDType.SelectedItem.Text = "Identity Card";
            }
            txtIdNumber.Text = pax.DocumentNo;
            string[] dob = pax.TempDOB.ToString().Split('/');
            ddlDate.Items.FindByText(dob[0]).Selected = true;
            ddlMonth.Items.FindByText(dob[1]).Selected = true;
            ddlYear.Items.FindByText(dob[2]).Selected = true;
        }
        paxcount += InsRequest.Childs;
        //Infant Details
        for (int k = 0; k < InsRequest.Infants; k++)
        {
            DropDownList ddlTitle = tblPassengers.FindControl("Prefix" + 3 + k.ToString()) as DropDownList;
            TextBox txtFirstName = tblPassengers.FindControl("txtIFirstName" + 3 + k.ToString()) as TextBox;
            TextBox txtLastName = tblPassengers.FindControl("txtILastName" + 3 + k.ToString()) as TextBox;
            DropDownList ddlGender = tblPassengers.FindControl("ddlGender" + 3 + k.ToString()) as DropDownList;
            DropDownList ddlNationality = tblPassengers.FindControl("ddlNationality" + 3 + k.ToString()) as DropDownList;
            DropDownList ddlIDType = tblPassengers.FindControl("ddlIDType" + 3 + k.ToString()) as DropDownList;
            TextBox txtIdNumber = tblPassengers.FindControl("txtIDNo" + 3 + k.ToString()) as TextBox;
            DropDownList ddlDate = tblPassengers.FindControl("ddlDay" + 3 + k.ToString()) as DropDownList;
            DropDownList ddlMonth = tblPassengers.FindControl("ddlMonth" + 3 + k.ToString()) as DropDownList;
            DropDownList ddlYear = tblPassengers.FindControl("ddlYear" + 3 + k.ToString()) as DropDownList;

            InsurancePassenger pax = passengers[paxcount + k];
            ddlTitle.Items.FindByText(pax.Title).Selected = true;
            txtFirstName.Text = pax.FirstName;
            txtLastName.Text = pax.LastName;
            if (pax.Gender == ZeusInsB2B.Zeus.GenderType.Male)
            {
                ddlGender.SelectedValue = "Male";
            }
            else
            {
                ddlGender.SelectedValue = "Female";
            }
            ddlNationality.SelectedValue = pax.Nationality;

            if (pax.DocumentType == ZeusInsB2B.Zeus.IdentificationType.Passport)
            {
                ddlIDType.SelectedItem.Text = "Passport";
            }
            else
            {
                ddlIDType.SelectedItem.Text = "Identity Card";
            }
            txtIdNumber.Text = pax.DocumentNo;
            string[] dob = pax.TempDOB.ToString().Split('/');
            ddlDate.Items.FindByText(dob[0]).Selected = true;
            ddlMonth.Items.FindByText(dob[1]).Selected = true;
            ddlYear.Items.FindByText(dob[2]).Selected = true;
        }
        paxcount += InsRequest.Infants;
    }
}
