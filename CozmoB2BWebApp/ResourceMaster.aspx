﻿<%@ Page Title="Resource Master" MasterPageFile="~/TransactionBE.master" EnableEventValidation="false" Language="C#" AutoEventWireup="true" Inherits="ResourceMasterUI" Codebehind="ResourceMaster.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript">
        function deleteAlert() {
            alert('Changed Successfully');
        }
        function saveNotification() {
            alert('Saved Successfully');
        }
        function updateNotification() {
            alert('Updated Successfully');

        }

        function Validate() {
            var valid = false;
            if (document.getElementById('<%=ddlCultureCode.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please Select Culture Code !";
            }
            else if (document.getElementById('<%=ddlResourceClass.ClientID %>').value == "-1") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please Select Resource Class !";
            }
            else {
                valid = true;
            }
            return valid;
        }

    </script>

    <div class="body_container" style="border: 1px solid black">

        <div class="Col-md-3" style="text-align: center">
            <h3>Resource Master 
            </h3>
        </div>

        <div id="errMess" style="display: none; color: Red; font-weight: bold; text-align: center;">
        </div>


        <div role="tabpanel" class="tab-pane" id="ResourceManager">
            <div class="row">

                <table style="width: 50%; padding: 15px;">
                    <tr>
                        <td style="padding: 5px">Select Culture Code:<span class="fcol_red">*</span>
                        </td>
                        <td style="padding: 5px">

                            <asp:DropDownList CssClass="form-control" runat="server" ID="ddlCultureCode">
                                <asp:ListItem Text="Select" Value="0"></asp:ListItem>
                                <asp:ListItem Text="ENGLISH" Value="EN"></asp:ListItem>
                                <asp:ListItem Text="ARABIC" Value="AR"></asp:ListItem>
                            </asp:DropDownList>
                        </td>

                    </tr>

                    <tr>
                        <td style="padding: 5px">Select Resource Class:<span class="fcol_red">*</span>
                        </td>
                        <td style="padding: 5px">
                            <asp:DropDownList CssClass="form-control" runat="server" ID="ddlResourceClass">
                                <asp:ListItem Text="Select" Value="-1"></asp:ListItem>
                                <asp:ListItem Text="common" Value="common"></asp:ListItem>
                                <asp:ListItem Text="Flight" Value="Flight"></asp:ListItem>
                                <asp:ListItem Text="Packages" Value="Packages"></asp:ListItem>
                                <asp:ListItem Text="Activity" Value="Activity"></asp:ListItem>
                                <asp:ListItem Text="Insurance" Value="Insurance"></asp:ListItem>
                                <asp:ListItem Text="SightSeeing" Value="SightSeeing"></asp:ListItem>
                                <asp:ListItem Text="Train" Value="Train"></asp:ListItem>
                                <asp:ListItem Text="SMSPack" Value="SMSPack"></asp:ListItem>
                                <asp:ListItem Text="MobileRecharge" Value="MobileRecharge"></asp:ListItem>
                                <asp:ListItem Text="FixedDeparture" Value="FixedDeparture"></asp:ListItem>
                                <asp:ListItem Text="Visa" Value="Visa"></asp:ListItem>
                                <asp:ListItem Text="Car" Value="Car"></asp:ListItem>
                                <asp:ListItem Text="ItineraryAddService" Value="ItineraryAddService"></asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2" style="text-align: right; padding: 5px;">
                            <asp:Button OnClick="btnLoad_Click" OnClientClick="return Validate();" CssClass="btn btn-primary rbpackage-btn" ID="btnLoad" runat="server"
                                Text="Load" />
                            <asp:Button OnClick="btnClear_Click" CssClass="btn btn-primary rbpackage-btn" ID="btnClear" runat="server"
                                Text="Clear" />

                        </td>
                    </tr>
                </table>

                <div class="col-md-12">
                    <asp:UpdatePanel ID="pnlRM" runat="server">
                        <ContentTemplate>
                            <asp:GridView CssClass="table b2b-corp-table" ID="gridViewRM" ShowFooter="true" DataKeyNames="resource_id"
                                runat="server" AutoGenerateColumns="false" HeaderStyle-Font-Bold="true" OnRowCancelingEdit="gridViewRM_RowCancelingEdit"
                                OnRowDeleting="gridViewRM_RowDeleting" OnRowEditing="gridViewRM_RowEditing" OnRowUpdating="gridViewRM_RowUpdating"
                                OnRowCommand="gridViewRM_RowCommand" OnRowDataBound="gridViewRM_RowDataBound">
                                <Columns>
                                    <asp:TemplateField HeaderText="RESOURCE KEY">
                                        <ItemTemplate>
                                            <asp:HiddenField runat="server" ID="hdnStatus" Value='<%# Eval("status")%>' />
                                            <asp:Label ID="lblResKey" runat="server" Text='<%# Eval("resource_key")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:TextBox MaxLength="200" TextMode="MultiLine" CssClass="form-control" ID="txtEditResKey" runat="server"
                                                Text='<%# Eval("resource_key")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditResKey" runat="server" ControlToValidate="txtEditResKey"
                                                Text="Required" ValidationGroup="ValidationEditRM" />

                                   
                                        </EditItemTemplate>

                                        <FooterTemplate>
                                            <asp:TextBox MaxLength="200" TextMode="MultiLine" CssClass="form-control" ID="txtResKey" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvResKey" runat="server" ControlToValidate="txtResKey"
                                                Text="Required" ValidationGroup="ValidationRM" />
                                             
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="RESOURCE DESCRIPTION">
                                        <ItemTemplate>
                                            <asp:Label ID="lblResDescription" runat="server" Text='<%# Eval("resource_description")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:TextBox MaxLength="50" TextMode="MultiLine" CssClass="form-control" ID="txtEditResDesc" runat="server"
                                                Text='<%# Eval("resource_description")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditResDesc" runat="server" ControlToValidate="txtEditResDesc"
                                                Text="Required" ValidationGroup="ValidationEditRM" />
                                        </EditItemTemplate>

                                        <FooterTemplate>
                                            <asp:TextBox MaxLength="50" TextMode="MultiLine" CssClass="form-control" ID="txtResDesc" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvResDesc" runat="server" ControlToValidate="txtResDesc"
                                                Text="Required" ValidationGroup="ValidationRM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="RESOURCE VALUE">
                                        <ItemTemplate>
                                            <asp:Label ID="lblResValue" runat="server" Text='<%# Eval("resource_value")%>'></asp:Label>
                                        </ItemTemplate>
                                        <EditItemTemplate>

                                            <asp:TextBox MaxLength="500" TextMode="MultiLine" CssClass="form-control" ID="txtEditResValue" runat="server"
                                                Text='<%# Eval("resource_value")%>'></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvEditResValue" runat="server" ControlToValidate="txtEditResValue"
                                                Text="Required" ValidationGroup="ValidationEditRM" />
                                        </EditItemTemplate>

                                        <FooterTemplate>
                                            <asp:TextBox MaxLength="500" TextMode="MultiLine" CssClass="form-control" ID="txtResValue" runat="server"></asp:TextBox>
                                            <asp:RequiredFieldValidator ID="rfvResValue" runat="server" ControlToValidate="txtResValue"
                                                Text="Required" ValidationGroup="ValidationRM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>

                                    <asp:TemplateField HeaderText="STATUS">
                                        <ItemTemplate>
                                            <asp:Label runat="server" ID="lblStatus"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField HeaderText="ACTIONS">
                                        <EditItemTemplate>
                                            <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="btnUpdateRM" runat="server" CommandName="Update" Text="Update"
                                                ValidationGroup="ValidationEditRM" />
                                            <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="btnCancelRM" runat="server" CommandName="Cancel" Text="Cancel" />
                                        </EditItemTemplate>
                                        <ItemTemplate>
                                            <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="btnEditRM" runat="server" CommandName="Edit" Text="Edit" />
                                            <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="btnDeleteRM" Text="Delete" runat="server" CommandName="Delete" />
                                        </ItemTemplate>
                                        <FooterTemplate>
                                            <asp:Button CssClass="btn btn-primary rbpackage-btn" ID="btnAddRM" runat="server"
                                                CommandName="AddNew" Text="Add" ValidationGroup="ValidationRM" />
                                        </FooterTemplate>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>
                        </ContentTemplate>
                        <Triggers>
                            <asp:AsyncPostBackTrigger ControlID="gridViewRM" />
                        </Triggers>
                    </asp:UpdatePanel>
                </div>
            </div>
        </div>

    </div>

</asp:Content>
