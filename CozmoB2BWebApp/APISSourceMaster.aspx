﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true"
    ValidateRequest="false" EnableEventValidation="false"
    Inherits="APISSourceMasterGUI" Title="APIS Source Master" Codebehind="APISSourceMaster.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <script type="text/javascript">

        //This function is used to select all the airlines      
        function selectAllAirlinesList() {
            var chkFields = document.getElementById("<%=chkAirLines.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");
            if (document.getElementById('selectAllAirlines').checked == true) {

                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = true;
                }
            }
            else {
                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = false;
                }
            }
            displaySelectedAirlines(); //function which displays the selected airlines from the list.
        }

        //This function is used to display the selected airlines under selected airlines section.
        function displaySelectedAirlines() {

            document.getElementById('selectedAirlinesList').style.display = "none";
            document.getElementById('selectedAirlinesList').innerHTML = "";

            var selectedAirlineSource = document.getElementById('<%=ddlAirSources.ClientID %>').value;
            switch (selectedAirlineSource) {
                case 'UA':
                case 'TA':
                case 'PK':
                case '1A'://Added by lokesh on 20-June-2018 for Amadeus Purpose

                    var selairlinesCount = 0;
                    var chkAirlines = document.getElementById("<%=chkAirLines.ClientID%>")
                    var checkboxAirlines = chkAirlines.getElementsByTagName("input");

                    var selectedAirlinesList = "";

                    for (var i = 0; i < checkboxAirlines.length; i++) {
                        if (checkboxAirlines[i].checked) {
                            if (selectedAirlinesList == "") {

                                selectedAirlinesList = checkboxAirlines[i].nextSibling.innerHTML;
                            }
                            else {
                                selectedAirlinesList += "," + " " + checkboxAirlines[i].nextSibling.innerHTML;
                            }
                            selairlinesCount++;
                        }
                    }

                    if (selairlinesCount > 0) {
                        document.getElementById('selectedAirlinesList').style.display = "block";
                        document.getElementById('selectedAirlinesList').innerHTML = selectedAirlinesList;

                    }
                    else {
                        document.getElementById('selectedAirlinesList').style.display = "none";
                        document.getElementById('selectedAirlinesList').innerHTML = "";
                    }
                    break;
                case 'G9':
                    document.getElementById('selectedAirlinesList').style.display = "block";
                    document.getElementById('selectedAirlinesList').innerHTML = "Air Arabia";
                    break;
                case 'FZ':
                    document.getElementById('selectedAirlinesList').style.display = "block";
                    document.getElementById('selectedAirlinesList').innerHTML = "Fly Dubai";
                    break;
                case 'SG':
                    document.getElementById('selectedAirlinesList').style.display = "block";
                    document.getElementById('selectedAirlinesList').innerHTML = "Spice Jet";
                    break;
                case '6E':
                    document.getElementById('selectedAirlinesList').style.display = "block";
                    document.getElementById('selectedAirlinesList').innerHTML = "Indigo";
                    break;
                case 'IX':
                    document.getElementById('selectedAirlinesList').style.display = "block";
                    document.getElementById('selectedAirlinesList').innerHTML = "Air India Express";
                    break;
                case 'G8':
                    document.getElementById('selectedAirlinesList').style.display = "block";
                    document.getElementById('selectedAirlinesList').innerHTML = "Go Air";
                    break;
                case 'SGCORP':
                    document.getElementById('selectedAirlinesList').style.display = "block";
                    document.getElementById('selectedAirlinesList').innerHTML = "Spice Jet Corp";
                    break;
                case '6ECORP':
                    document.getElementById('selectedAirlinesList').style.display = "block";
                    document.getElementById('selectedAirlinesList').innerHTML = "Indigo Corp";
                    break;
                case 'G8CORP':
                    document.getElementById('selectedAirlinesList').style.display = "block";
                    document.getElementById('selectedAirlinesList').innerHTML = "Go Air Corp";
                    break;
            }
        }

        //Function which selects all the countries from the list.
        function selectAllCountriesList() {
            var chkFields = document.getElementById("<%=chkCountries.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");
            if (document.getElementById('selectAllcountries').checked == true) {

                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = true;
                }
            }
            else {
                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = false;
                }
            }
            displaySelectedCountries(); //function which displays the selected countries.
        }


        //Function which displays the selected countries under selected countries section.
        function displaySelectedCountries() {
            document.getElementById('selectedCountriesList').style.display = "none";
            document.getElementById('selectedCountriesList').innerHTML = "";
            var selcountriesCount = 0;
            var chkCountries = document.getElementById("<%=chkCountries.ClientID%>")
            var checkboxCountries = chkCountries.getElementsByTagName("input");

            var selectedCountriesList = "";

            for (var i = 0; i < checkboxCountries.length; i++) {
                if (checkboxCountries[i].checked) {
                    if (selectedCountriesList == "") {

                        selectedCountriesList = checkboxCountries[i].nextSibling.innerHTML;
                    }
                    else {
                        selectedCountriesList += "," + " " + checkboxCountries[i].nextSibling.innerHTML;
                    }
                    selcountriesCount++;
                }
            }

            if (selcountriesCount > 0) {
                document.getElementById('selectedCountriesList').style.display = "block";
                document.getElementById('selectedCountriesList').innerHTML = selectedCountriesList;

            }
            else {
                document.getElementById('selectedCountriesList').style.display = "none";
                document.getElementById('selectedCountriesList').innerHTML = "";
            }

        }



        //Function which displays the selected source i.e selected from the dropdown.

        function displaySelectedSource() {
            document.getElementById('sourceName').style.display = "block";
            document.getElementById('sourceName').innerHTML = "";
            var selectedAirlineSource = document.getElementById('<%=ddlAirSources.ClientID %>').value;
            switch (selectedAirlineSource) {
                case 'UA':
                    document.getElementById('sourceName').innerHTML = "Selected Source : UAPI";
                    break;
                case 'G8':
                    document.getElementById('sourceName').innerHTML = "Selected Source : Go Air";
                    break;
                case 'G9':
                    document.getElementById('sourceName').innerHTML = "Selected Source : Air Arabia";
                    break;
                case 'FZ':
                    document.getElementById('sourceName').innerHTML = "Selected Source : Fly Dubai";
                    break;
                case 'IX':
                    document.getElementById('sourceName').innerHTML = "Selected Source : Air India Express";
                    break;
                case 'SG':
                    document.getElementById('sourceName').innerHTML = "Selected Source : Spice Jet";
                    break;
                case '6E':
                    document.getElementById('sourceName').innerHTML = "Selected Source : Indigo";
                    break;
                case 'TA':
                    document.getElementById('sourceName').innerHTML = "Selected Source : TBOAir";
                    break;
                case 'PK':
                    document.getElementById('sourceName').innerHTML = "Selected Source : PKFares";
                    break;
               case '1A'://Added by lokesh on 20-June-2018 for Amadeus Purpose
                    document.getElementById('sourceName').innerHTML = "Selected Source : Amadeus";
                    break;
                 case 'SGCORP':
                    document.getElementById('sourceName').innerHTML = "Selected Source : Spice Jet Corp";
                    break;
                case '6ECORP':
                    document.getElementById('sourceName').innerHTML = "Selected Source : Indigo Corp";
                    break;
                case 'G8CORP':
                    document.getElementById('sourceName').innerHTML = "Selected Source : Go Air Corp";
                    break;
                default:
                    document.getElementById('sourceName').style.display = "none";
                    document.getElementById('sourceName').innerHTML = "";
                    break;
            }
        }

        //During updation we will not allow the user to select any counrty.
        //Previous selection will be displayed but in disable mode only.
        function disableCountriesList() {

            var chkFields = document.getElementById("<%=chkCountries.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");
            for (var i = 0; i < checkboxFields.length; i++) {
                checkboxFields[i].disabled = true;
            }
        }
        //During updation we will not allow the user to select any airline.
        //Previous selection will be displayed but in disable mode only.
        function disableAirlinesList() {
            var chkFields = document.getElementById("<%=chkAirLines.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");
            for (var i = 0; i < checkboxFields.length; i++) {
                checkboxFields[i].disabled = true;
            }
        }

        //Common function which allows the user to select all airlines,all countries and all page fields
        function checkSelectAllForfieldsList() {

            var allSelected = true;
            var chkFields = document.getElementById("<%=chkFields.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");

            for (var i = 0; i < checkboxFields.length; i++) {
                if (checkboxFields[i].checked == false) {
                    allSelected = false;
                }
            }

            if (allSelected) {
                document.getElementById('selectAll').checked = true;
            }
            else {
                document.getElementById('selectAll').checked = false;
            }


        }

        //This function is used to display the previously selected items to the user.
        function showSourceList() {


            document.getElementById('fieldsList').style.display = "block";
            checkSelectAllForfieldsList();
            document.getElementById('masterSourcesList').style.display = "block";
            document.getElementById('tdBtnSave').style.display = "block";
            document.getElementById('tdStatus').style.display = "block";
            displaySelectedSource();
            document.getElementById('<%=ddlAirSources.ClientID %>').disabled = true;
            document.getElementById('errMess').style.display = "none";
            if (document.getElementById('<%=rbtnCountry.ClientID %>').checked == true) {

                document.getElementById('countriesList').style.display = "block";
                document.getElementById('airLinesList').style.display = "none";

                document.getElementById('tdAirline').style.display = "none";
                document.getElementById('tdCommon').style.display = "none";
                disableCountriesList();
                document.getElementById('selectAllcountries').style.display = "none";
                displaySelectedCountries();
                document.getElementById('<%=rbtnCountry.ClientID %>').disabled = true;
            }
            else if (document.getElementById('<%=rbtnAirline.ClientID %>').checked == true) {
                document.getElementById('airLinesList').style.display = "block";
                document.getElementById('countriesList').style.display = "none";

                document.getElementById('tdCountry').style.display = "none";
                document.getElementById('tdCommon').style.display = "none";
                disableAirlinesList();
                document.getElementById('selectAllAirlines').style.display = "none";
                displaySelectedAirlines();
                document.getElementById('<%=rbtnAirline.ClientID %>').disabled = true;
                var selectedAirlineSource = document.getElementById('<%=ddlAirSources.ClientID %>').value;
                switch (selectedAirlineSource) {
                    case 'UA':
                        document.getElementById('airLinesList').style.display = "block";
                        document.getElementById('airLinesLccList').style.display = "none";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('divJ9').style.display = "none";
                        break;
                    case 'G8':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "block";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                         document.getElementById('divJ9').style.display = "none";
                        break;
                    case 'G9':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "block";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                          document.getElementById('divJ9').style.display = "none";
                        break;
                    case 'FZ':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "block";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                         document.getElementById('divJ9').style.display = "none";
                        break;
                    case 'IX':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "block";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                         document.getElementById('divJ9').style.display = "none";
                        break;
                    case 'SG':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "block";
                        document.getElementById('div6E').style.display = "none";
                         document.getElementById('divJ9').style.display = "none";
                        break;
                    case '6E':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "block";
                         document.getElementById('divJ9').style.display = "none";
                        break;
                     case 'J9':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                         document.getElementById('divJ9').style.display = "block";
                        break;
                    case 'TA':
                    case 'PK':
                    case '1A': //Added by lokesh on 20-June-2018 for Amadeus Purpose
                        document.getElementById('airLinesList').style.display = "block";
                        document.getElementById('airLinesLccList').style.display = "none";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                         document.getElementById('divJ9').style.display = "none";
                        break;
                    default:
                        document.getElementById('airLinesList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                         document.getElementById('divJ9').style.display = "none";
                        break;
                }


            }

            else if (document.getElementById('<%=rbtnCommon.ClientID %>').checked == true) {
                document.getElementById('airLinesList').style.display = "none";
                document.getElementById('countriesList').style.display = "none";

                document.getElementById('tdCountry').style.display = "none";
                document.getElementById('tdAirline').style.display = "none";
                document.getElementById('tdStatus').style.display = "none";
                document.getElementById('<%=rbtnCommon.ClientID %>').disabled = true;

            }
            else {

                document.getElementById('airLinesList').style.display = "none";
                document.getElementById('countriesList').style.display = "none";
                document.getElementById('airLinesLccList').style.display = "none";

            }
            updateCommonFields();

        }

        //This function is used to select all the page fields on the page.
        function selectAllPageFieldsList() {
            var chkFields = document.getElementById("<%=chkFields.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");
            if (document.getElementById('selectAll').checked == true) {

                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = true;
                }
            }
            else {
                for (var i = 0; i < checkboxFields.length; i++) {
                    checkboxFields[i].checked = false;
                }
            }
            updateCommonFields(); //Function which selects the 5 common page fields like title,first name ,last name,phone ,email.
        }


        //Validates the user input based on source,country,airline or common(includes both airline and country)
        function Validate() {
            if (document.getElementById('<%=hdfMode.ClientID %>').value == "1") {
                document.getElementById('<%=rbtnCommon.ClientID %>').disabled = false;
                document.getElementById('<%=rbtnAirline.ClientID %>').disabled = false;
                document.getElementById('<%=rbtnCountry.ClientID %>').disabled = false;
            }

            document.getElementById('errMess').style.display = "none";
            var valid = false;
            if (document.getElementById('<%=ddlAirSources.ClientID %>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select any Air source from the list .";

            }
            var restrictFieldsBasedOn = document.querySelectorAll('input[type="radio"]:checked');
            //alert("restrictFieldsBasedOn:" + restrictFieldsBasedOn.length);

            var selcountriesCount = 0;
            var chkCountries = document.getElementById("<%=chkCountries.ClientID%>")
            var checkboxCountries = chkCountries.getElementsByTagName("input");
            for (var i = 0; i < checkboxCountries.length; i++) {
                if (checkboxCountries[i].checked) {
                    selcountriesCount++;
                }
            }

            //alert(selcountriesCount);

            var selAirlinesCount = 0;
            var chkAirlines = document.getElementById("<%=chkAirLines.ClientID%>");
            var checkboxAirlines = chkAirlines.getElementsByTagName("input");
            for (var i = 0; i < checkboxAirlines.length; i++) {
                if (checkboxAirlines[i].checked) {
                    selAirlinesCount++;
                }
            }
            //alert("selAirlinesCount:" + selAirlinesCount);



            var selPageFieldsCount = 0;
            var chkFields = document.getElementById("<%=chkFields.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");
            for (var i = 0; i < checkboxFields.length; i++) {
                if (checkboxFields[i].checked) {
                    selPageFieldsCount++;
                }
            }
            //alert("selPageFieldsCount" + selPageFieldsCount);

            if (restrictFieldsBasedOn.length > 0) {


                if (document.getElementById('<%=rbtnCountry.ClientID %>').checked == true) {

                    //alert("Inside the country block");
                    if (selcountriesCount == 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errMess').innerHTML = "Please select any country from the list .";
                    }
                    else if (selPageFieldsCount == 0) {
                        document.getElementById('errMess').style.display = "block";
                        document.getElementById('errMess').innerHTML = "Please select mandatory fields from the list.";

                    }
                    else {
                        valid = true;
                    }





                }
                else if (document.getElementById('<%=rbtnAirline.ClientID %>').checked == true) {
                    //alert("Inside the airline block");
                    if (document.getElementById('<%=ddlAirSources.ClientID %>').value == 'UA' || document.getElementById('<%=ddlAirSources.ClientID %>').value == 'TA' ||document.getElementById('<%=ddlAirSources.ClientID %>').value == 'PK' || document.getElementById('<%=ddlAirSources.ClientID %>').value == '1A' ) {

                        if (selAirlinesCount == 0) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select any airline from the list .";
                        }
                        else if (selPageFieldsCount == 0) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select mandatory fields from the list.";

                        }
                        else {
                            valid = true;
                        }
                    }


                    else if (document.getElementById('<%=ddlAirSources.ClientID %>').value == 'G9') {


                        if (document.getElementById('<%=chkG9.ClientID %>').checked == false) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select Air Arabia airline  .";
                        }
                        else if (selPageFieldsCount == 0) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select mandatory fields from the list.";

                        }
                        else {
                            valid = true;
                        }

                    }

                    else if (document.getElementById('<%=ddlAirSources.ClientID %>').value == 'FZ') {

                        if (document.getElementById('<%=chkFZ.ClientID %>').checked == false) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select Fly Dubai airline  .";
                        }
                        else if (selPageFieldsCount == 0) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select mandatory fields from the list.";

                        }
                        else {
                            valid = true;
                        }

                    }

                    else if (document.getElementById('<%=ddlAirSources.ClientID %>').value == 'SG') {

                        if (document.getElementById('<%=chkSG.ClientID %>').checked == false) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select Spice Jet airline  .";
                        }
                        else if (selPageFieldsCount == 0) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select mandatory fields from the list.";

                        }
                        else {
                            valid = true;
                        }

                    }

                    else if (document.getElementById('<%=ddlAirSources.ClientID %>').value == '6E') {

                        if (document.getElementById('<%=chk6E.ClientID %>').checked == false) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select indigo airline  .";
                        }
                        else if (selPageFieldsCount == 0) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select mandatory fields from the list.";

                        }
                        else {
                            valid = true;
                        }

                    }

                    else if (document.getElementById('<%=ddlAirSources.ClientID %>').value == 'IX') {

                        if (document.getElementById('<%=chkIX.ClientID %>').checked == false) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select Air India Express airline  .";
                        }
                        else if (selPageFieldsCount == 0) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select mandatory fields from the list.";

                        }
                        else {
                            valid = true;
                        }

                    }

                    else if (document.getElementById('<%=ddlAirSources.ClientID %>').value == 'G8' || document.getElementById('<%=ddlAirSources.ClientID %>').value == 'G8CORP') {

                        if (document.getElementById('<%=chkG8.ClientID %>').checked == false) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select Go Air airline  .";
                        }
                        else if (selPageFieldsCount == 0) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select mandatory fields from the list.";

                        }
                        else {
                            valid = true;
                        }

                    }
                      else if (document.getElementById('<%=ddlAirSources.ClientID %>').value == 'J9') {

                        if (document.getElementById('<%=chkG8.ClientID %>').checked == false) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select Jazeera Airways airline  .";
                        }
                        else if (selPageFieldsCount == 0) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errMess').innerHTML = "Please select mandatory fields from the list.";

                        }
                        else {
                            valid = true;
                        }

                    }




                }
                else if (document.getElementById('<%=rbtnCommon.ClientID %>').checked == true && selPageFieldsCount == 0) {
                    //alert("Inside the common fields count");
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errMess').innerHTML = "Please select  mandatory fields from the list .";
                }
                else {
                    valid = true;
                }
            }
            else {

                    document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Country/Airline/Common .";
                valid = false;
            }
            if (document.getElementById('<%=ddlTransType.ClientID %>').value == "0") {
                    document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select TransType.";
                 valid = false;

            }
            if (document.getElementById('<%=ddlFlightType.ClientID%>').value == "0") {
                document.getElementById('errMess').style.display = "block";
                document.getElementById('errMess').innerHTML = "Please select Flight Type.";
                 valid = false;
            }
            

            return valid;

        }


        //This function removes the previuos selection .
        function UncheckSelectedSources() {
            var chkCountries = document.getElementById("<%=chkCountries.ClientID%>")
            var checkboxCountries = chkCountries.getElementsByTagName("input");
            for (var i = 0; i < checkboxCountries.length; i++) {
                checkboxCountries[i].checked = false;
            }

            var chkAirlines = document.getElementById("<%=chkAirLines.ClientID%>")
            var checkboxAirlines = chkAirlines.getElementsByTagName("input");
            for (var i = 0; i < checkboxAirlines.length; i++) {
                checkboxAirlines[i].checked = false;
            }

            var chkFields = document.getElementById("<%=chkFields.ClientID%>")
            var checkboxFields = chkFields.getElementsByTagName("input");
            for (var i = 0; i < checkboxFields.length; i++) {
                checkboxFields[i].checked = false;
            }

            document.getElementById('selectAllcountries').checked = false;
            document.getElementById('selectAllAirlines').checked = false;
            document.getElementById('selectAll').checked = false;


            document.getElementById('selectedCountriesList').style.display = "none";
            document.getElementById('selectedCountriesList').innerHTML = "";


            document.getElementById('selectedAirlinesList').style.display = "none";
            document.getElementById('selectedAirlinesList').innerHTML = "";

        }
        //display the sources list on the page.
        function DisplaySourceList() {
            RefreshPageMandateFields();
            document.getElementById('fieldsList').style.display = "block";
            document.getElementById('errMess').style.display = "none";

            if (document.getElementById('<%=rbtnCountry.ClientID %>').checked == true) {
                document.getElementById('countriesList').style.display = "block";
                document.getElementById('airLinesList').style.display = "none";
                document.getElementById('airLinesLccList').style.display = "none";
                UncheckSelectedSources();
            }
            else if (document.getElementById('<%=rbtnAirline.ClientID %>').checked == true) {


                document.getElementById('countriesList').style.display = "none";
                UncheckSelectedSources();
                var selectedAirlineSource = document.getElementById('<%=ddlAirSources.ClientID %>').value;
              
                switch (selectedAirlineSource) {
                    case 'UA':
                        document.getElementById('airLinesList').style.display = "block";
                        document.getElementById('airLinesLccList').style.display = "none";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('div6ECorp').style.display = "none";
                        document.getElementById('divSGCorp').style.display = "none";
                        document.getElementById('divJ9').style.display = "none";
                        document.getElementById('divG8Corp').style.display = "none";
                        break;
                    case 'G8':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "block";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('div6ECorp').style.display = "none";
                        document.getElementById('divSGCorp').style.display = "none";
                        document.getElementById('divJ9').style.display = "none";
                        document.getElementById('divG8Corp').style.display = "none";
                        break;
                    case 'G9':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "block";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('div6ECorp').style.display = "none";
                        document.getElementById('divSGCorp').style.display = "none";
                        document.getElementById('divJ9').style.display = "none";
                        document.getElementById('divG8Corp').style.display = "none";
                        break;
                    case 'FZ':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "block";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('div6ECorp').style.display = "none";
                        document.getElementById('divSGCorp').style.display = "none";
                        document.getElementById('divJ9').style.display = "none";
                        document.getElementById('divG8Corp').style.display = "none";
                        break;
                    case 'IX':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "block";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('div6ECorp').style.display = "none";
                        document.getElementById('divSGCorp').style.display = "none";
                        document.getElementById('divJ9').style.display = "none";
                        document.getElementById('divG8Corp').style.display = "none";
                        break;
                    case 'SG':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "block";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('div6ECorp').style.display = "none";
                        document.getElementById('divSGCorp').style.display = "none";
                        document.getElementById('divJ9').style.display = "none";
                        document.getElementById('divG8Corp').style.display = "none";
                      
                        break;
                     case 'SGCORP':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('div6ECorp').style.display = "none";
                        document.getElementById('divSGCorp').style.display = "block";
                        document.getElementById('divJ9').style.display = "none";
                        document.getElementById('divG8Corp').style.display = "none";
                        break;
                    case '6E':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "block";
                        document.getElementById('div6ECorp').style.display = "none";
                        document.getElementById('divSGCorp').style.display = "none";
                        document.getElementById('divJ9').style.display = "none";
                        document.getElementById('divG8Corp').style.display = "none";
                      
                        break;
                    case '6ECORP':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('div6ECorp').style.display = "block";
                        document.getElementById('divSGCorp').style.display = "none";
                        document.getElementById('divJ9').style.display = "none";
                        document.getElementById('divG8Corp').style.display = "none";
                        break;
                    case 'J9':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('div6ECorp').style.display = "none";
                        document.getElementById('divSGCorp').style.display = "none";
                        document.getElementById('divJ9').style.display = "block";
                        document.getElementById('divG8Corp').style.display = "none";
                        break;
                    case 'G8CORP':
                        document.getElementById('airLinesList').style.display = "none";
                        document.getElementById('airLinesLccList').style.display = "block";
                        document.getElementById('divG8Corp').style.display = "block";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('div6ECorp').style.display = "none";
                        document.getElementById('divSGCorp').style.display = "none";
                        document.getElementById('divJ9').style.display = "none";
                        
                        break;
                    case 'TA':
                    case 'PK':
                    case '1A':
                        document.getElementById('airLinesList').style.display = "block";
                        document.getElementById('airLinesLccList').style.display = "none";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('div6ECorp').style.display = "none";
                        document.getElementById('divSGCorp').style.display = "none";
                        document.getElementById('divJ9').style.display = "none";
                        document.getElementById('divG8Corp').style.display = "none";
                        break;
                    default:
                        document.getElementById('airLinesList').style.display = "block";
                        document.getElementById('airLinesLccList').style.display = "none";
                        document.getElementById('divG8').style.display = "none";
                        document.getElementById('divG9').style.display = "none";
                        document.getElementById('divFZ').style.display = "none";
                        document.getElementById('divIX').style.display = "none";
                        document.getElementById('divSG').style.display = "none";
                        document.getElementById('div6E').style.display = "none";
                        document.getElementById('div6ECorp').style.display = "none";
                        document.getElementById('divSGCorp').style.display = "none";
                        document.getElementById('divJ9').style.display = "none";
                        document.getElementById('divG8Corp').style.display = "none";
                        break;
                }
            }

            else if (document.getElementById('<%=rbtnCommon.ClientID %>').checked == true) {
                document.getElementById('airLinesList').style.display = "none";
                document.getElementById('countriesList').style.display = "none";
                document.getElementById('airLinesLccList').style.display = "none";
                UncheckSelectedSources();

            }
            else {
                document.getElementById('airLinesList').style.display = "none";
                document.getElementById('countriesList').style.display = "none";
                document.getElementById('airLinesLccList').style.display = "none";
                UncheckSelectedSources();
            }
            selectCommonFields();

        }

        //Function which displays the airline ,country and page fields based on the user selection.
        function LoadMasterSourcesList() {

            document.getElementById('errMess').style.display = "none";
            document.getElementById('<%=rbtnCountry.ClientID %>').checked = false;
            document.getElementById('<%=rbtnAirline.ClientID %>').checked = false;
            document.getElementById('<%=rbtnCommon.ClientID %>').checked = false;
            document.getElementById('countriesList').style.display = "none";
            document.getElementById('airLinesLccList').style.display = "none";
            document.getElementById('airLinesList').style.display = "none";
            document.getElementById('fieldsList').style.display = "none";
            UncheckSelectedSources();
            document.getElementById('<%=lblSuccess.ClientID %>').innerHTML = "";


            if (document.getElementById('<%=ddlAirSources.ClientID %>').value == "0") {
                document.getElementById('masterSourcesList').style.display = "none";
                document.getElementById('airLinesList').style.display = "none";
                document.getElementById('countriesList').style.display = "none";
                document.getElementById('fieldsList').style.display = "none";
                document.getElementById('errMess').style.display = "block";
                //document.getElementById('errMess').innerHTML = "Please select any Air source from the list .";
                document.getElementById('tdBtnSave').style.display = "none";
                document.getElementById('sourceName').style.display = "none";
                document.getElementById('sourceName').innerHTML = "";

            }
            else {
                document.getElementById('masterSourcesList').style.display = "block";
                document.getElementById('tdBtnSave').style.display = "block";
                displaySelectedSource();
            }

         //  if (document.getElementById('<%=ddlAirSources.ClientID %>').value == "UA" || document.getElementById('<%=ddlAirSources.ClientID %>').value == "TA") {
          //  document.getElementById('tdCommon').style.display = "block";
          //  }
          //  else {
          //  document.getElementById('tdCommon').style.display = "none";
          //  } 

        }

        //Clears the user input .
        function clearControls() {
            document.getElementById('<%=ddlAirSources.ClientID %>').value = "0";
            document.getElementById('masterSourcesList').style.display = "none";
            document.getElementById('countriesList').style.display = "none";
            document.getElementById('airLinesLccList').style.display = "none";
            document.getElementById('airLinesList').style.display = "none";
            document.getElementById('fieldsList').style.display = "none";
            document.getElementById('errMess').style.display = "none";
            document.getElementById('<%=rbtnCountry.ClientID %>').checked = false;
            document.getElementById('<%=rbtnAirline.ClientID %>').checked = false;
            document.getElementById('<%=rbtnCommon.ClientID %>').checked = false;
            //document.getElementById('lblSuccess').innerHTML = "";
            document.getElementById('tdBtnSave').style.display = "none";
            document.getElementById('sourceName').style.display = "none";
            document.getElementById('sourceName').innerHTML = "";
            document.getElementById('searchDiv').style.display = "none";
            location.reload();
        }

        //Select the common fields irrespective of counrty or airline or common (includes both airline and country)
        function selectCommonFields() {
            var selectedSource = document.getElementById('<%=ddlAirSources.ClientID %>').value;
            if (document.getElementById('<%=hdfMode.ClientID %>').value == '0') {
                var chkFields = document.getElementById("<%=chkFields.ClientID%>");
                var checkboxFields = chkFields.getElementsByTagName("input");
                for (var i = 0; i < checkboxFields.length; i++) {
                    //5 Common Mandatory Fields to All sources
                    var fieldName = checkboxFields[i].nextSibling.innerHTML;
                    if (fieldName == 'Title' || fieldName == 'FirstName' || fieldName == 'LastName' || fieldName == 'Phone' || fieldName == 'Email') {
                        checkboxFields[i].checked = true;
                        checkboxFields[i].disabled = true;

                    }
                    else {
                        checkboxFields[i].checked = false;
                        checkboxFields[i].disabled = false;

                    }
                }
            }
        }

        //This function is used to refresh page mandate fields.
        function RefreshPageMandateFields() {
            var chkFields = document.getElementById("<%=chkFields.ClientID%>");
            var checkboxFields = chkFields.getElementsByTagName("input");
            for (var i = 0; i < checkboxFields.length; i++) {
                checkboxFields[i].checked = false;
                checkboxFields[i].disabled = false;

            }
        }

        //This function is used to select the common fields while retrieving.
        function updateCommonFields() {
            var selectedSource = document.getElementById('<%=ddlAirSources.ClientID %>').value;
            var chkFields = document.getElementById("<%=chkFields.ClientID%>")
            var checkboxFields = chkFields.getElementsByTagName("input");
            for (var i = 0; i < checkboxFields.length; i++) {
                //5 Common Mandatory Fields to All sources
                var fieldName = checkboxFields[i].nextSibling.innerHTML;
                if (fieldName == 'Title' || fieldName == 'FirstName' || fieldName == 'LastName' || fieldName == 'Phone' || fieldName == 'Email') {
                    checkboxFields[i].checked = true;
                    checkboxFields[i].disabled = true;

                }
                
            }
        }
        
    </script>

    <style type="text/css">
        #ctl00_cphTransaction_chkFields
        {
            width: 100%;
        }
        #divbtns table td
        {
            padding: 5px;
        }
    </style>
    <div class="col-md-12" style="height: 800px;">
        <div class="col-md-12" style="text-align: center">
            <h3>
                API's Source Mandatory Page</h3>
        </div>
        <asp:HiddenField runat="server" ID="hdfEMId" Value="0"></asp:HiddenField>
        <asp:HiddenField runat="server" ID="hdfMode" Value="0"></asp:HiddenField>
        <div class="col-md-12" id="errMess" style="color: Red; font-weight: bold; text-align: center">
        </div>
        <!-- Start: Active sources -->
        <div class="col-md-12">
            <h4>
                Air Sources</h4>
            <asp:DropDownList ID="ddlAirSources" CssClass="form-control" Style="width: 200px;"
                runat="server" onchange="LoadMasterSourcesList();">
                <asp:ListItem Value="0">-- Select Air Source --</asp:ListItem>
            </asp:DropDownList>


            Flight Type: 
            <asp:DropDownList ID="ddlFlightType" CssClass="form-control" Style="width: 200px;"
                runat="server">
                <asp:ListItem Value="0">-- Select FlightType --</asp:ListItem>
                <asp:ListItem Value="DOMESTIC">DOMESTIC</asp:ListItem>
                <asp:ListItem Value="INTERNATIONAL">INTERNATIONAL</asp:ListItem>
                <asp:ListItem Value="ALL">ALL</asp:ListItem>
            </asp:DropDownList>
            <br />
            Select TransType
            <asp:DropDownList ID="ddlTransType" CssClass="form-control" Style="width: 200px;"
                runat="server">
                <asp:ListItem Value="0">-- Select TransType --</asp:ListItem>
                <asp:ListItem Value="B2B">B2B</asp:ListItem>
                <asp:ListItem Value="B2C">B2C</asp:ListItem>
                <asp:ListItem Value="MOBILE">MOBILE</asp:ListItem>
            </asp:DropDownList>
            <br />
           
            <span id="sourceName" style="display: none; font-weight: bolder; font-size: 18px;"></span>
        </div>
        <br />
        <!-- End: Active sources -->
        <!-- Start:  sources -->
        <div class="col-md-12" id="masterSourcesList" style="display: none;">
            <table>
                <tr>
                    <td colspan="2">
                        <h4>
                            Restrict fields based on :</h4>
                    </td>
                </tr>
                <tr>
                    <td id="tdCountry">
                        <asp:RadioButton ID="rbtnCountry" runat="server" GroupName="source" Font-Bold="True"
                            onclick="DisplaySourceList();" Text="Country" />
                    </td>
                    <td id="tdAirline">
                        <asp:RadioButton ID="rbtnAirline" runat="server" GroupName="source" Font-Bold="True"
                            onclick="DisplaySourceList();" Text="Airline" />
                    </td>
                    <td id="tdCommon">
                        <asp:RadioButton ID="rbtnCommon" runat="server" GroupName="source" Font-Bold="True"
                            onclick="DisplaySourceList();" Text="Common(Includes both Country and Airline)" />
                    </td>
                    <td id="tdStatus" style="display: none;">
                        <span style="font-weight: bolder; font-size: 18px;">Status:</span>
                        <asp:CheckBox runat="server" ID="chkStatus" />
                    </td>
                </tr>
            </table>
        </div>
        <!-- End:  sources -->
        <div class="col-md-12">
            <!--Start:countries List-->
            <div class="col-md-4" id="countriesList" style="display: none;">
                <span style="font-weight: bolder; font-size: 18px;">Countries:</span>
                <input type="checkbox" id="selectAllcountries" onchange="selectAllCountriesList();" />
                <span style="font-weight: bolder; font-size: 13px;">Select All</span>
                <div class="col-md-12" style="height: 200px; overflow-y: scroll;">
                    <asp:CheckBoxList ID="chkCountries" runat="server" RepeatDirection="Vertical" RepeatColumns="2"
                        onchange="displaySelectedCountries();">
                    </asp:CheckBoxList>
                </div>
                <br />
                <span style="font-weight: bolder; font-size: 18px;">Selected Countries:</span>
                <div class="col-md-12" id="selectedCountriesList" style="display: none; height: 200px;
                    overflow-y: scroll;">
                </div>
            </div>
            <!--End:countries List-->
            <!--Start:Airlines List-->
            <div class="col-md-4" id="airLinesLccList" style="display: none;">
                <span style="font-weight: bolder; font-size: 18px;">Airlines:</span>
                <div id="divG9" class="col-md-12" style="display: none;">
                    <asp:CheckBox Enabled="false" Checked="true" runat="server" ID="chkG9" Text="Air Arabia" />
                </div>
                <div id="divFZ" class="col-md-12" style="display: none;">
                    <asp:CheckBox Enabled="false" Checked="true" runat="server" ID="chkFZ" Text="Fly Dubai" />
                </div>
                <div id="divSG" class="col-md-12" style="display: none;">
                    <asp:CheckBox Enabled="false" Checked="true" runat="server" ID="chkSG" Text="SpiceJet" />
                </div>
                <div id="div6E" class="col-md-12" style="display: none;">
                    <asp:CheckBox Enabled="false" Checked="true" runat="server" ID="chk6E" Text="IndiGo" />
                </div>
                <div id="divIX" class="col-md-12" style="display: none;">
                    <asp:CheckBox Enabled="false" Checked="true" runat="server" ID="chkIX" Text="Air India Express" />
                </div>
                <div id="divG8" class="col-md-12" style="display: none;">
                    <asp:CheckBox Enabled="false" Checked="true" runat="server" ID="chkG8" Text="GoAir" />
                </div>
                <div id="divSGCorp" class="col-md-12" style="display: none;">
                    <asp:CheckBox Enabled="false" Checked="true" runat="server" ID="chkSGCorp" Text="SpiceJet Corp" />
                </div>
                <div id="div6ECorp" class="col-md-12" style="display: none;">
                    <asp:CheckBox Enabled="false" Checked="true" runat="server" ID="chk6ECorp" Text="IndiGo Corp" />
                </div>
                <div id="divJ9" class="col-md-12" style="display: none;">
                    <asp:CheckBox Enabled="false" Checked="true" runat="server" ID="chkJ9" Text="Jazeera Airways" />
                </div>
                <div id="divG8Corp" class="col-md-12" style="display: none;">
                    <asp:CheckBox Enabled="false" Checked="true" runat="server" ID="chkG8Corp" Text="GoAir Corp" />
                </div>


            </div>
            <div class="col-md-4" id="airLinesList" style="display: none;">
                <span style="font-weight: bolder; font-size: 18px;">Airlines:</span>
                <input type="checkbox" id="selectAllAirlines" onchange="selectAllAirlinesList();" />
                <div class="col-md-12" style="height: 200px; overflow-y: scroll;">
                    <asp:CheckBoxList ID="chkAirLines" runat="server" RepeatColumns="2" RepeatDirection="Vertical"
                        onchange="displaySelectedAirlines();">
                    </asp:CheckBoxList>
                </div>
                <br />
                <span style="font-weight: bolder; font-size: 18px;">Selected Airlines:</span>
                <div class="col-md-12" id="selectedAirlinesList" style="display: none; height: 200px;
                    overflow-y: scroll;">
                </div>
            </div>
            <!--End:Airlines List-->
            <!--Start:Page Fields List-->
            <div class="col-md-4" id="fieldsList" style="display: none;">
                <span style="font-weight: bolder; font-size: 18px;">Page Fields:</span>
                <input type="checkbox" id="selectAll" onchange="selectAllPageFieldsList();" />
                <asp:CheckBoxList ID="chkFields" RepeatColumns="2" runat="server" RepeatDirection="Vertical">
                </asp:CheckBoxList>
            </div>
            <!--End:Page Fields List-->
        </div>
        <div class="col-md-12" id="searchDiv">
        </div>
        <div class="col-md-12">
            <div class="col-md-4">
            </div>
            <div class="col-md-4">
            </div>
            <div class="col-md-4" id="divbtns">
                <table>
                    <tr>
                        <td>
                            <asp:Button ID="btnSearch" CssClass="btn but_b" OnClick="btnSearch_Click" runat="server"
                                Text="Search" />
                        </td>
                        <td id="tdBtnSave" style="display: none;">
                            <asp:Button CssClass="btn but_b" ID="btnSave" OnClientClick="return Validate();"
                                OnClick="btnSave_Click" runat="server" Text="Save" />
                        </td>
                        <td>
                            <asp:Button CssClass="btn but_b" ID="btnCancel" OnClientClick="clearControls();"
                                OnClick="btnCancel_Click" runat="server" Text="Cancel" />
                        </td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="col-md-12">
            <asp:Label runat="server" ID="lblSuccess" CssClass="alert-success"></asp:Label>
        </div>
        <div class="col-md-12">
            <asp:Label runat="server" ID="lblError" CssClass="alert-danger"></asp:Label>
        </div>

    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
    <div class="col-md-12">
        <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="MandateId"
            EmptyDataText="No Data Available!" AutoGenerateColumns="false" PageSize="10"
            GridLines="none" CssClass="grdTable" OnSelectedIndexChanged="gvSearch_SelectedIndexChanged"
            CellPadding="4" CellSpacing="0" OnPageIndexChanging="gvSearch_PageIndexChanging">
            <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
            <Columns>
                <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"
                    ControlStyle-CssClass="label" ShowSelectButton="True" />
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtSource" Width="70px" HeaderText="Source" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblSource" runat="server" Text='<%# Eval("Source") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Source") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtCountry" Width="150px" CssClass="inputEnabled" HeaderText="Country"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblCountry" runat="server" Text='<%# Eval("Country") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Country") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtAirline" Width="70px" HeaderText="Airline" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblAirline" runat="server" Text='<%# Eval("Airline") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Airline") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtStatus" Width="70px" HeaderText="Status" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("Status") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("Status") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtFlightType" Width="70px" HeaderText="FlightType" CssClass="inputEnabled"
                            OnClick="FilterSearch_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("FlightType") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("FlightType") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <asp:Label runat="server" Width="70px" Font-Bold="true" Font-Size="Medium" Text="TransType"></asp:Label>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblTransType" runat="server" Text='<%# Eval("TransType") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("TransType") %>' Width="70px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

            </Columns>
        </asp:GridView>
    </div>
</asp:Content>
