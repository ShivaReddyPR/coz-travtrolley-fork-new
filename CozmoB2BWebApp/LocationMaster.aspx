<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="LocationMasterUI" Title="Location Master" Codebehind="LocationMaster.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="InfoDynamic" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<%--<div class="grdScrlTrans" style="margin-top:-2px;height:520px;border:solid0px;text-align:center" >--%>



<div class="body_container">

<div class="paramcon" title="header">
                            
                            
                         <div class="col-md-12 padding-0 marbot_10">                               

       
    <div class="col-md-2"> <asp:Label ID="lblAgent" Text="Agent:" runat="server"></asp:Label></div>
    
    
    <div class="col-md-2"> <asp:DropDownList ID="ddlAgent" runat="server" Enabled="true" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged"
                                                AutoPostBack="true" CssClass="inpuTddlEnabled form-control">
                                            </asp:DropDownList></div>
                                            
                                            
                                            
    <div class="col-md-2"><asp:Label ID="Label1" Text="Sub Agent:" runat="server"></asp:Label> </div>
    
    
     <div class="col-md-2"><asp:DropDownList ID="ddlSubAgent" AutoPostBack="true" runat="server" Enabled="true" CssClass="inpuTddlEnabled form-control" OnSelectedIndexChanged="ddlSubAgent_SelectedIndexChanged">
                                            </asp:DropDownList> </div>
                                            
                                            
                                            
    <div class="col-md-2"> <asp:Label ID="lblCode" Text="Code:" runat="server" CssClass="label"></asp:Label></div>
     <div class="col-md-2"><asp:TextBox ID="txtCode" runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
    
    
            <div class="col-md-12 padding-0 marbot_10">                               

       
    <div class="col-md-2"><asp:Label ID="lblName" Text="Name:" runat="server"></asp:Label> </div>
    
    <div class="col-md-2"> <asp:TextBox ID="txtName" runat="server" Enabled="true" CssClass="inputEnabled form-control"></asp:TextBox></div>
    
    
    
    <div class="col-md-2"> <asp:Label ID="lblCurrency" Text="Currency:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"> <asp:DropDownList ID="ddlCurrency" runat="server" Enabled="false" CssClass="inputddlEnabled form-control">
                                            </asp:DropDownList></div>
                                            



    <div class="clearfix"></div>
    </div>

  
    <div class="col-md-12 padding-0 marbot_10">
          <div class="col-md-2"><asp:Label ID="lblCountry" Text="Country:" runat="server"></asp:Label> </div>
    
    <div class="col-md-2"><asp:DropDownList ID="ddlcountrylist" runat="server" Enabled="true" AutoPostBack="true" OnSelectedIndexChanged="ddlcountrylist_SelectedIndexChanged" CssClass="inputddlEnabled form-control">
                                            </asp:DropDownList> </div>
    
    
    
    <div class="col-md-2"> <asp:Label ID="lblstate" Text="State:" runat="server"></asp:Label></div>
    
    
     <div class="col-md-2"> <asp:DropDownList ID="ddlstatelist" runat="server" CssClass="inputddlEnabled form-control">
                                            </asp:DropDownList></div>
                                            
<div class="col-md-2">
    <asp:Label ID="lblparentcode" Text="Parent Code:" runat="server"></asp:Label>
</div>
        <div class="col-md-2">
            <asp:TextBox ID="txtParentcode" runat="server" CssClass="inputEnabled form-control"></asp:TextBox>
        </div>
        

    <div class="clearfix"></div>
    </div>
    <div class="col-md-12 padding-0 marbot_10">

        <div class="col-md-2">
    <asp:Label ID="lblgstnumber" Text="GST Number:" runat="server"></asp:Label>
</div>
        <div class="col-md-2">
            <asp:TextBox ID="txtgstnumber" runat="server" CssClass="inputEnabled form-control"></asp:TextBox>
        </div>
		 <div class="col-md-2">
            <asp:Label ID="lblMapCode" Text="Map Code" runat="server"></asp:Label>
        </div>
        <div class="col-md-2">
           <asp:TextBox ID="txtMapCode" runat="server" CssClass="inputEnabled form-control"></asp:TextBox>
        </div>
        <div class="clearfix"></div>
    </div>


       
        <div class="col-md-12 padding-0 marbot_10">   
        
        <div class="col-md-2"> <asp:Label ID="lblAddress" Text="Address:" runat="server"></asp:Label></div>
        
        
          <div class="col-md-10"> <asp:TextBox ID="txtAddress" runat="server" Height="50px" TextMode="MultiLine"
                                                Enabled="true" CssClass="inputEnabled"></asp:TextBox></div>
                                                
                                                
        
        
    <div class="clearfix"></div>
    </div>
    
    
       <div class="col-md-12 padding-0 marbot_10">   
       
       
        <div class="col-md-2"><asp:Label ID="lblTerms" Text="Terms:" runat="server"></asp:Label> </div>
        
        
         <div class="col-md-10"><asp:TextBox ID="txtTerms" runat="server" Width="100%" TextMode="MultiLine" Enabled="true" Height="80px" CssClass="inputEnabled"
                                                ></asp:TextBox> </div>
       
        <div class="clearfix"></div>
    </div>
    
    
    
    <div class="col-md-12 padding-0 marbot_10">   
    
    
       <label class=" f_R mar-5">
                                                <asp:Button ID="btnSave" Text="Save" runat="server" OnClientClick="return Save();"
                                                    CssClass="btn but_b" OnClick="btnSave_Click"></asp:Button></label>
                                            <label class=" f_R mar-5">
                                                <asp:Button ID="btnClear" Text="Clear" runat="server" CssClass="btn but_b" OnClick="btnClear_Click">
                                                </asp:Button></label>
                                            <label class=" f_R mar-5">
                                                <asp:Button ID="btnSearch" Text="Search" runat="server" CssClass="btn but_b" OnClick="btnSearch_Click">
                                                </asp:Button></label>
    
    </div>
    
                                
                            </div>


    <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess"></asp:Label>
    <div class="clearfix"></div>
    
     </div>

    <script type="text/javascript">

        function Save() {
            if (getElement('ddlAgent').selectedIndex <= 0) addMessage('Please select Agent from the list!', '');
//            if (getElement('ddlSubAgent').options.length > 1 && getElement('ddlSubAgent').selectedIndex <= 0) addMessage('Please select Sub Agent from the list!', ''); 
            if (getElement('txtCode').value == '') addMessage('Code cannot be blank!', '');
            if (getElement('txtName').value == '') addMessage('Name cannot be blank!', '');
            if (getElement('ddlCurrency').selectedIndex <= 0) addMessage('Please select Currency from the list!', '');
            if (getElement('ddlcountrylist').selectedIndex <= 0) addMessage('Please select Country from the list!', '');
            if (getElement('ddlcountrylist').value == "IN") {
                if (getElement('ddlstatelist').selectedIndex <= "0")
                    addMessage('Please Select State', '');
                if (getElement('txtParentcode').value == "")
                    addMessage('Parent code cannnot be blank!', '');
                if (getElement('txtgstnumber').value == "")
                    addMessage('GST Number cannot be blank!', '');
			}
			if (getElement('txtMapCode').value == '') addMessage('Map Code cannot be blank!', '');

            if (getMessage() != '') {
                alert(getMessage()); clearMessage(); return false;
            }
            
        }
        
    </script>

</asp:Content>
<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
<asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="LOCATION_ID" 
    EmptyDataText="No Location List!" AutoGenerateColumns="false" PageSize="15" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging" >
    
     <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />"  ControlStyle-CssClass="label" ShowSelectButton="True" />
    
     <asp:TemplateField>
    <HeaderStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc1:Filter   ID="HTtxtCode" Width="70px" HeaderText="Code" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblCode" runat="server" Text='<%# Eval("LOCATION_CODE") %>' CssClass="label grdof" ToolTip='<%# Eval("LOCATION_CODE") %>' Width="70px"></asp:Label>
    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>--%>
    </ItemTemplate>    
    
    </asp:TemplateField>      
      
      <asp:TemplateField>
      <HeaderStyle HorizontalAlign="left" />
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtName"  Width="100px" CssClass="inputEnabled" HeaderText="Name" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblName" runat="server" Text='<%# Eval("LOCATION_NAME") %>' CssClass="label grdof"  ToolTip='<%# Eval("LOCATION_NAME") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="left" />
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtAddress"  Width="150px" CssClass="inputEnabled" HeaderText="Address" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
  <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblAddress" runat="server" Text='<%# Eval("LOCATION_ADDRESS") %>' CssClass="label grdof"  ToolTip='<%# Eval("LOCATION_ADDRESS") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>        
    
    <asp:TemplateField>
    <HeaderStyle HorizontalAlign="left" />
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtStatus"  Width="150px" CssClass="inputEnabled" HeaderText="Status" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("LOCATION_STATUS") %>' CssClass="label grdof"  ToolTip='<%# Eval("LOCATION_STATUS") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
		 <asp:TemplateField>
    <HeaderStyle HorizontalAlign="left" />
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtMapCode"  Width="150px" CssClass="inputEnabled" HeaderText="MapCode" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate>
    <asp:Label ID="ITlblMapCode" runat="server" Text='<%# Eval("map_code") %>' CssClass="label grdof"  ToolTip='<%# Eval("map_code") %>' Width="150px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
   
   
    </Columns>           
    </asp:GridView>

</asp:Content>





