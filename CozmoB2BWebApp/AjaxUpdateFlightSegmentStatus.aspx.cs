using System;
using CT.BookingEngine;
using CT.Core;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;

public partial class AjaxUpdateFlightSegmentStatus : System.Web.UI.Page
{
    protected bool statusUpdated = false;
    protected FlightItinerary itinerarySrc = new FlightItinerary();
    protected FlightItinerary itineraryDb = new FlightItinerary();
    protected int memberId;
    protected UserMaster loggedMember;
    protected bool error = false;

    protected void Page_Load(object sender, EventArgs e)
    {
        memberId = Convert.ToInt32(Settings.LoginInfo.UserID);
        int bookingId = Convert.ToInt32(Request["bookingId"]);
        string remarks = string.Empty;
        loggedMember = new UserMaster(memberId);

        // updating status
        string pnr = Request["pnr"].Trim();

        // retrieving data from our Data Base
        int flightId = FlightItinerary.GetFlightId(pnr);
        itineraryDb = new FlightItinerary(flightId);

        // retriving data from Source      
        try
        {
            itinerarySrc = MetaSearchEngine.RetrieveInfo(pnr, itineraryDb.FlightBookingSource);
            //itinerarySrc = Worldspan.RetrieveInfo(pnr);
        }
        catch (BookingEngineException excep)
        {
            Audit.Add(EventType.UpdateSSR, Severity.High, memberId, "Update Status - Retrieve failed: " + Util.GetExceptionInformation(excep, string.Empty), Request.ServerVariables["REMOTE_ADDR"]);
            error = true;
            Response.Write("FALSE");
            Response.Write("@" + excep.Message);
        }

        try
        {
            if (error == false)
            {
                if (itinerarySrc.Segments != null && itinerarySrc.Segments.Length != 0)
                {
                    //// adding last modified by
                    for (int i = 0; i < itinerarySrc.Segments.Length; i++)
                    {
                        if (itinerarySrc.Segments[i] != null)
                        {
                            itinerarySrc.Segments[i].LastModifiedBy = memberId;
                        }
                    }
                    statusUpdated = itineraryDb.UpdateStatus(itinerarySrc);
                    if (statusUpdated == true)
                    {
                        Response.Write("TRUE");
                        for (int j = 0; j < itinerarySrc.Segments.Length; j++)
                        {
                            Response.Write("@" + itinerarySrc.Segments[j].FlightKey + "=" + itinerarySrc.Segments[j].Status);
                        }
                        remarks = "Status updated successfully";
                    }
                    else
                    {
                        Response.Write("FALSE");
                        Response.Write("@" + "Failed to update.");
                        remarks = "Failed to update status";
                    }
                }
                else
                {
                    Response.Write("FALSE");
                    Response.Write("@" + "This booking has been released now.");
                    remarks = "This booking has been released now";
                }
                //BookingHistory bh = BookingHistory();
                BookingHistory bh = new BookingHistory();
                bh.BookingId = bookingId;
                bh.EventCategory = EventCategory.Booking;
                bh.Remarks = "Status updated successfully";
                bh.CreatedBy = memberId;
                bh.Save();
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.UpdateSSR, Severity.High, memberId, "Update Status  failed: " + Util.GetExceptionInformation(ex, string.Empty), Request.ServerVariables["REMOTE_ADDR"]);
        }
    }
}
