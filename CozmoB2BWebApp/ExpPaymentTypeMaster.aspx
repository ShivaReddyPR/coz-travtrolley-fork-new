﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="ExpPaymentTypeMaster.aspx.cs" Inherits="CozmoB2BWebApp.ExpPaymentTypeMaster" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">

    <link href="css/toastr.css" rel="stylesheet" type="text/css" />
    <script src="scripts/paginathing.js" type="text/javascript"></script>
    <script src="scripts/Common/EntityGrid.js" type="text/javascript"></script>
    <script src="css/toastr.min.js"></script>

    <script>

        /* Global variables */
        var apiHost = ''; var cntrllerPath = 'api/commonMasters'; var cntrlrPath = 'api/expenseMasters';
        var apiAgentInfo = {}; var expRepId = 0; var selectedRows = [];
        var costCenterData = {}; var typesData = {}; var screenData = {};
        var flag = '';
        var PT_Id = 0;
        var agentdata = {}; var selectedProfile = 0;

        /* Page Load */
        $(document).ready(function () {
            /* Check query string to see if existing report needs to open */
            expRepId = '<%=Request.QueryString["ExpRepId"] == null ? "0" : Request.QueryString["ExpRepId"]%>';

            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();

            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {
                alert('Session expired, please login once again.');
                return;
            }

            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();

            /* To get screen dropdown list data */
            GetScreenData();

            /* To get screenNames dropdown list data */
            GetSreenList();
        });

        /* To get agent and login info */
        function GetAgentInfo() {
            try {
                var loginInfo = '<%=Settings.LoginInfo == null%>';
                if (loginInfo == true)
                    return apiAgentInfo;

                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {
            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';
            if (IsEmpty(apiHost))
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";
            return apiHost;
        }

        /* To get agent */
        function GetScreenData() {
            var reqData = { AgentInfo: apiAgentInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getPaymentTypeScreenLoadData';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);
        }

        /* To bind card master agent and cost center */
        function BindScreenData(screenData) {
            agentdata = {};
            if (!IsEmpty(screenData.dtAgents)) {

                agentdata = screenData.dtAgents;
                var options = agentdata.length != 1 ? GetddlOption('', '--Select Agent--') : '';
                $.each(agentdata, function (key, col) {

                    options += GetddlOption(col.agenT_ID, col.agenT_NAME);
                });
                $('#ddlAgentId').empty();
                $('#ddlAgentId').append(options);
                $("#ddlAgentId").select2("val", '');
                if (agentdata.length == 1) {

                    $('#ddlAgentId').attr('disabled', 'disabled');
                    $("#ddlAgentId").select2("val", agentdata[0].agenT_ID);
                }
            }
            if (!IsEmpty(screenData.dtCostCenter))
                BindCostCenterData(screenData.dtCostCenter);
            if (!IsEmpty(screenData.dtTypes))
                BindExpenseTypesData(screenData.dtTypes);
        }

        /* To bind cost center data to costcenter control */
        function BindCostCenterData(ccData) {
            costCenterData = {};
            if (IsEmpty(ccData)) {
                $('#ddlCostCenter').empty();
                $("#ddlCostCenter").select2("val", '');
                return;
            }
            costCenterData = ccData;
            var options = costCenterData.length != 1 ? GetddlOption('', '--Select Cost Center--') : '';
            $.each(costCenterData, function (key, col) {

                options += GetddlOption(col.setupId, col.name);
            });
            $('#ddlCostCenter').empty();
            $('#ddlCostCenter').append(options);
            $("#ddlCostCenter").select2("val", '');
            if (costCenterData.length == 1) {
                $("#ddlCostCenter").select2("val", costCenterData[0].setupId);
            }
        }

        /* To bind screenNameList with checkoxes */
        function BindScreenListData(sData) {
            screenData = {};
            screenData = sData;
            var screen = "";
            $.each(screenData, function (key, col) {
                screen += "<div><input class='screen mr-3' type='checkbox' value='" + col.func_name + "' /><label>" + col.func_name + "</label></div>";

            });
            $("#PTS_Screen").append(screen);
        }

        /* To bind expense type control */
        function BindExpenseTypesData(tData) {
            typesData = {};
            typesData = tData;
            var type_text = "";
            $("#PTE_ET_Id").html('');
            if (typesData != '') {
                $("#Msg").html('');
                $.each(typesData, function (key, col) {
                    type_text += "<div><input class='pte mr-3' type='checkbox' value='" + col.eT_ID + "' /><label>" + col.eT_Desc + "</label></div>";
                });
                $("#PTE_ET_Id").append(type_text);
            }
            else {
                $("#Msg").html('No data available.');
            }
        }

        /* To set agent name and cost center on agent change */
        function AgentChange(agentId) {
            selectedProfile = agentId;
            if (selectedProfile > 0) {

                var reqData = { AgentInfo: apiAgentInfo, AgentId: parseInt(selectedProfile) };
                var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getCostCenterData';
                WebApiReq(apiUrl, 'POST', reqData, '', BindCostCenterData, null, null);
            }
            GetTypes(agentId);
            ClearInfo();
        }

        /* To set agent and expense types on agent change */
        function GetTypes(agentId) {
            selectedProfile = agentId;
            if (selectedProfile > 0) {

                var reqData = { AgentInfo: apiAgentInfo, AgentId: parseInt(selectedProfile) };
                var apiUrl = apiHost.trimRight('/') + '/' + cntrlrPath.trimRight('/') + '/getTypesData';
                WebApiReq(apiUrl, 'POST', reqData, '', BindExpenseTypesData, null, null);
            }
            ClearInfo();
        }

        /* To get screenNames List */
        function GetSreenList() {
            var cfield = { UserId: apiAgentInfo.LoginUserId };
            var reqData = { AgentInfo: apiAgentInfo, cField: cfield };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getScreenList';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenListData, null, null);
            ClearInfo();
        }

        /* To prepare and get the types entity list for selected rows */
        function GetSelectedRows() {
            selectedRows = GetSetSelectedRows();
            if (IsEmpty(selectedRows) || selectedRows.length == 0)
                return [];
            var exptSelected = [];
            $.each(selectedRows, function (key, col) {

                var ptInfo = ptInfoDetails.find(item => item.pT_ID == col);
                ptInfo.pT_Status = false;
                ptInfo.pT_ModifiedBy = apiAgentInfo.LoginUserId;
                exptSelected = exptSelected.concat(ptInfo);
            });
            return exptSelected;
        }

        /* To prepare and set types info entity */
        function RefreshGrid(resp) {
            $.each(selectedRows, function (key, col) {

                var ptid = parseInt(col);
                ptInfoDetails = ptInfoDetails.filter(item => (item.pT_ID) !== ptid);
            });
            if (ptInfoDetails.length == 0) {
                BindPaymentTypesInfo(ptInfoDetails);
                return;
            }
            selectedRows = [];
            GetSetSelectedRows('set', selectedRows);
            GetSetDataEntity('set', ptInfoDetails);
            LoadData(GetSetCurrentPageNo('get', ''));
        }

        /* To load payment types info */
        function LoadPaymentTypesInfo() {
            var pT_AgentId = $("#ddlAgentId").val();
            if (pT_AgentId == "" || pT_AgentId == "--Select Agent--") {
                toastr.error('Please select agent');
                $('#ddlAgentId').parent().addClass('form-text-error');
                return false;
            }
            var typesInfo = { PT_AgentId: pT_AgentId }
            var reqData = {AgentInfo: apiAgentInfo, PaymentTypesInfo: typesInfo };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/getPaymentTypeInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', BindPaymentTypesInfo, null, null);
        }

        /* To bind payment types info to grid */
        function BindPaymentTypesInfo(ptInfo) {
            if (ptInfo.length == 0) {

                ShowError('no data available for selected agent.');
                ClearInfo();
                return;
            }
            ptInfoDetails = ptInfo;
            var gridProperties = GetGridPropsEntity();
            gridProperties.headerColumns = ('Code|Desc|GLAccount|GLType').split('|');
            gridProperties.displayColumns = ('pT_Code|pT_Desc|pT_GLAccount|pT_GLType').split('|');
            gridProperties.pKColumnNames = ('pT_ID').split('|');
            gridProperties.dataEntity = ptInfo;
            gridProperties.divGridId = 'PaymentTypeList';
            gridProperties.headerPaging = true;
            gridProperties.gridSelectedRows = selectedRows;
            gridProperties.selectType = 'M';
            gridProperties.recordsperpage = 5;

            EnablePagingGrid(gridProperties);
            $('#PTypeList').show();
        }

        /* To save payment types info */
        function Save() {
            var pT_AgentId = $("#ddlAgentId").val();
            var pT_CostCenter = $("#ddlCostCenter").val();
            var pT_Code = $("#PT_Code").val();
            var pT_Desc = $("#PT_Desc").val();
            var pT_GLAccount = $("#PT_GLAccount").val();
            var pT_GLType = $("#PT_GLType").val();
            var screenName = $('.screen:input[type="checkbox"]:checked').map(function () {
                return this.value;
            }).get().join(",");
            var expType = $('.pte:input[type="checkbox"]:checked').map(function () {
                return this.value;
            }).get().join(",");

            if ((pT_AgentId == "" || pT_AgentId == "--Select Agent--") || (pT_Code == "" || pT_Code == "Enetr Code") || (pT_Desc == "" || pT_Desc == "Enter Desc") || (pT_CostCenter == "" || pT_CostCenter == "--Select CostCenter--" || pT_CostCenter == null)) {
                if (pT_AgentId == "" || pT_AgentId == "--Select Agent--") {
                    toastr.error('Please Select Agent');
                    $('#ddlAgentId').parent().addClass('form-text-error');
                }
                if (pT_CostCenter == "" || pT_CostCenter == "--Select CostCenter--" || pT_CostCenter == null) {
                    toastr.error('Please Select CostCenter');
                    $('#ddlCostCenter').parent().addClass('form-text-error');
                }
                if (pT_Code == "" || pT_Code == "Enter Code") {
                    toastr.error('Please Enter Code');
                    $("#PT_Code").addClass('form-text-error');
                }
                if (pT_Desc == "" || pT_Desc == "Enter Desc") {
                    toastr.error('Please Enter Desc');
                    $("#PT_Desc").addClass('form-text-error');
                }
                return false;
            }

            var paymentTypesInfo = { PT_ID: PT_Id, PT_AgentId: pT_AgentId, PT_Code: pT_Code, PT_Desc: pT_Desc, PT_CostCenter: pT_CostCenter, PTS_Screen: screenName, PTE_ET_Id: expType, PT_GLAccount: pT_GLAccount, PT_GLType: pT_GLType, PT_Status: true, PT_CreatedBy: apiAgentInfo.LoginUserId, PT_ModifiedBy: apiAgentInfo.LoginUserId }
            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expRepId, PaymentTypesInfo: paymentTypesInfo, flag: "INSERT" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/savePaymentTypeInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', StatusConfirm, null, null);
        }

        /* To bind status of paymenttypes info */
        function StatusConfirm() {
            if (PT_Id > 0) {
                alert('record updated successfully!');
            }
            else if (PT_Id == 0) {
                alert('record added successfully!');
            }
            ClearInfo();
            LoadPaymentTypesInfo();
        }

        /* To update selected types info from the grid and update the status in data base */
        function EditPaymentTypesInfo() {
            $("#PTE_ET_Id input").prop('checked', false);
            $("#PTS_Screen input").prop('checked', false);
            var delTInfo = GetSelectedRows();
            if (IsEmpty(delTInfo) || delTInfo.length == 0) {
                ShowError('Please check any one of the row in below table');
                return;
            }
            if (IsEmpty(delTInfo) || delTInfo.length > 1) {
                ShowError('Please check one of the row in below table');
                return;
            }
            var ptDetails = ptInfoDetails.find(item => item.pT_ID == selectedRows[0]);
            PT_Id = ptDetails.pT_ID;
            $("#ddlAgentId").select2('val', ptDetails.pT_AgentId);
            $("#PT_Code").val(ptDetails.pT_Code);
            $("#PT_Desc").val(ptDetails.pT_Desc);
            $("#ddlCostCenter").select2('val', ptDetails.pT_CostCenter);
            $("#PT_GLAccount").val(ptDetails.pT_GLAccount);
            $("#PT_GLType").val(ptDetails.pT_GLType);
            var scr = ptDetails.ptS_Screen;
            scr = scr.split(',');
            for (var i = 0; i < scr.length; i++) {
                $("#PTS_Screen").find('input[value="' + scr[i] + '"]').prop("checked", true);
            }
            var exp = ptDetails.ptE_ET_Id;
            exp = exp.split(',');
            for (var j = 0; j < exp.length; j++) {
                $("#PTE_ET_Id").find('input[value="' + exp[j] + '"]').prop("checked", true);
            }
        }

        /* To delete selected rows from the grid and update the status in data base */
        function Delete() {
            var delTInfo = GetSelectedRows();
            if (IsEmpty(delTInfo) || delTInfo.length == 0) {

                ShowError('Please select record to delete');
                return;
            }
            var reqData = { AgentInfo: apiAgentInfo, ExpRepId: expRepId, delTInfo, flag: "DELETE" };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/savePaymentTypeInfo';
            WebApiReq(apiUrl, 'POST', reqData, '', RefreshGrid, null, null);
        }

        /* To clear types details and hide the div */
        function ClearInfo() {
            selectedRows = [];
            selectedProfile = 0;
            flag = ''
            PT_Id = 0;
            $("#PT_Code").val('');
            $("#PT_GLAccount").val('');
            $("#PT_GLType").val('');
            $("#PT_Desc").val('');
            $("#ddlCostCenter").select2('val', '');
            $("#PTE_ET_Id input").prop('checked', false);
            $("#PTS_Screen input").prop('checked', false);

            $("#ddlAgentId").parent().removeClass('form-text-error');
            $("#PT_Code").removeClass('form-text-error');
            $("#PT_Desc").removeClass('form-text-error');
            $("#ddlCostCenter").parent().removeClass('form-text-error');
            RemoveGrid();
            $('#PTypeList').hide();
        }

        /* textbox allows only alphanumeric and @-_$,.:; */
        function check(e) {
            var keynum;
            var keychar;

            // For Internet Explorer
            if (window.event) {
                keynum = e.keyCode;
            }
            // For Netscape/Firefox/Opera
            else if (e.which) {
                keynum = e.which;
            }
            keychar = String.fromCharCode(keynum);
            //List of special characters you want to restrict
            if (keychar == "'" || keychar == "`" || keychar == "!" || keychar == "#" || keychar == "%" || keychar == "^" || keychar == "*" || keychar == "(" || keychar == ")" || keychar == "+" || keychar == "=" || keychar == "/" || keychar == "~" || keychar == "<" || keychar == ">" || keychar == "|" || keychar == "?" || keychar == "{" || keychar == "}" || keychar == "[" || keychar == "]" || keychar == "¬" || keychar == "£" || keychar == '"' || keychar == "\\") {
                return false;
            } else {
                return true;
            }
        }

        /* textbox allows only alphanumeric */
        function IsAlphaNumeric(e) {
            var keyCode = e.keyCode == 0 ? e.charCode : e.keyCode;
            var ret = ((keyCode >= 48 && keyCode <= 57) || (keyCode >= 65 && keyCode <= 90) || (keyCode >= 97 && keyCode <= 122) || (keyCode == 32));
            return ret;
        }

        /* textbox allows only numeric */
        function isNumberKey(evt) {
            var charCode = (evt.which) ? evt.which : evt.keyCode;
            if (charCode != 46 && charCode > 31
                && (charCode < 48 || charCode > 57))
                return false;
            return true;
        }

        /* Load Screen Names List */
        function LoadMapToScreen() {
            $('#MapSList').modal('show');
        }

        /* load expense type list */
        function LoadMapToExpenseType() {
            $('#MapEList').modal('show');
        }
    </script>

    <div class="body_container p-0 corp-exp-module">
        <div class="c-exp-title-wrapper">
            <div class="row custom-gutter">
                <div class="col-12 col-sm-6">
                    <div class="d-flex flex-wrap header-blocks">
                        <h3 class="m-0 title py-3 px-4 float-md-left">Payment Type Master</h3>
                    </div>
                </div>
                <div class="col-12 col-sm-6 d-flex align-items-end justify-content-end mt-2 mt-xl-0 p-3">
                    <button class="btn btn-info mr-2" id="showTypesList" type="button" data-toggle="collapse" data-target="#PTypeList" aria-expanded="false" aria-controls="TypesList" onclick="LoadPaymentTypesInfo()">VIEW ALL <i class="icon icon-search "></i></button>
                    <button type="button" class="btn btn-gray mr-2" onclick="ClearInfo();">CLEAR <i class="icon icon-close "></i></button>
                    <button type="button" class="btn btn-primary" onclick="Save();">SUBMIT <i class="icon icon-play_arrow "></i></button>
                </div>
            </div>
        </div>
        <div class="clear"></div>
        <div class="c-exp-main-wrapper">
            <div class="c-exp-left-block" id="createExp-left-block">
                <div class="exp-content-block">
                    <div class="form-row">
                        <input type="hidden" id="PT_ID" name="PT_ID" />
                        <div class="form-group col-md-3">
                            <label>Agent</label><span style="color: red;">*</span>
                            <select name="Agent" id="ddlAgentId" class="form-control" onchange="AgentChange(this.value)"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Cost Center</label><span style="color: red;">*</span>
                            <select name="CostCenter" id="ddlCostCenter" class="form-control"></select>
                        </div>
                        <div class="form-group col-md-3">
                            <label>Code</label><span style="color: red;">*</span>
                            <input type="text" id="PT_Code" class="form-control" maxlength="10" placeholder="Enter Code" onkeypress="return IsAlphaNumeric(event);" />
                        </div>
                        <div class="form-group col-md-3">
                            <label>Desc</label><span style="color: red;">*</span>
                            <input type="text" class="form-control" id="PT_Desc" maxlength="50" placeholder="Enter Desc" onkeypress="return check(event)" />
                        </div>
                        <div class="form-group col-md-3">
                            <label>GLAccount</label>
                            <input type="text" class="form-control" id="PT_GLAccount" maxlength="20" placeholder="Enter GLAccount" onkeypress="return IsAlphaNumeric(event);" />
                        </div>
                        <div class="form-group col-md-3">
                            <label>GLType</label>
                            <input type="text" class="form-control" id="PT_GLType" maxlength="20" placeholder="Enter GLType" onkeypress="return IsAlphaNumeric(event);" />
                        </div>
                        <div class="form-group col-md-6">
                            <a class="btn btn-info mt-5 mr-3" id="MapToScreen" data-toggle="collapse" data-target="#MSList" aria-expanded="false" aria-controls="MSList" onclick="LoadMapToScreen()">MapTo Screen </a>
                     
                            <a class="btn btn-info mt-5" id="MapToExpenseType" data-toggle="collapse" data-target="#MEList" aria-expanded="false" aria-controls="MEList" onclick="LoadMapToExpenseType()">MapTo ExpenseType </a>
                        </div>                    
                    </div>
                </div>

                <div class="exp-content-block collapse" id="PTypeList" style="display: none;">
                    <h5 class="mb-3 float-left">All PaymentTypes</h5>
                    <div class="button-controls text-right">
                        <button class="btn btn-edit" type="button" onclick="EditPaymentTypesInfo();">EDIT  <i class="icon icon-edit"></i></button>
                        <button class="btn btn-danger" type="button" onclick="Delete();">DELETE  <i class="icon icon-delete"></i></button>
                    </div>
                    <div class="clear"></div>
                    <div class="table-responsive" id="PaymentTypeList"></div>
                </div>

                <div class="modal fade in farerule-modal-style" data-backdrop="static" id="MapSList" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel" style="display: none;">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" id="btnModelC">&times;</button>
                                <h4 class="modal-title">ScreenName Details</h4>
                            </div>
                            <div class="modal-body">
                                <div class="px-3">
                                    <div id="PTS_Screen"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="modal fade in farerule-modal-style" data-backdrop="static" id="MapEList" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
                    <div class="modal-dialog modal-md" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" data-dismiss="modal" id="btnModelM">&times;</button>
                                <h4 class="modal-title">ExpenseType Details</h4>
                            </div>
                            <div class="modal-body">
                                <div class="px-3">
                                    <div id="PTE_ET_Id"></div>
                                    <label id="Msg"></label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
