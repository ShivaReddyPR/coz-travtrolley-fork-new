﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="AgentSupportGUI" Title="Agent Support" Codebehind="AgentSupport.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <table width="100%">
        <tr>
            <td align="center">
                <div  style="width: 400px; height: 100px; margin-top: 50px; border: solid 1px #ccc; border-radius: 16px;">
                    
                    <table align="left">
                    <tr height="30px">
                    <td align="left" colspan="2">
                    <span style="color: Black; font-size:16px; font-family:Verdana;">Help Desk No's</span>
                    </td>
                    </tr>
                        <tr height="30px">
                        <td width="40px"></td>
                            <td>
                            <span style="color: Blue; font-size:13px; font-family:Verdana;">
                                9723472384792734(Operations)</span>
                            </td>
                            </tr>
                            <tr height="30px">
                            <td width="40px"></td>
                            <td>
                                <span style="color: Blue; font-size:13px; font-family:Verdana;">
                                9723472384792734(Operations)</span>
                            </td>
                        </tr>
                    </table>
                </div>
            </td>
        </tr>
        <tr>
        <td align="center">
            <table cellspacing="20px">
                <tr>
                    <td width="70%">
                        <div align="center" style="width: 100%; height: 250px; margin-top: 20px; border: solid 1px #ccc;
                            border-radius: 16px;">
                            <table width="100%" cellspacing="8px">
                                <tr height="30px">
                                    <td align="left" colspan="2">
                                        <span style="color: Black; font-size: 16px; font-family: Verdana;">Email IDs</span>
                                    </td>
                                </tr>
                                <tr height="30px">
                                    <td width="50%">
                                        <span style="color: Maroon; font-size:13px; font-family:Verdana;">Cancellation & Refunds</span>
                                    </td>
                                    <td>:</td>
                                    <td width="50%">
                                        <span style="color: Blue; font-size:13px; font-family:Verdana;">vijay@cozmotravel.com</span>
                                    </td>
                                </tr>
                                <tr height="30px">
                                    <td width="50%">
                                        <span style="color: Maroon; font-size:13px; font-family:Verdana;">For Reissues & Revalidations</span>
                                    </td>
                                    <td>:</td>
                                    <td width="50%">
                                        <span style="color: Blue; font-size:13px; font-family:Verdana;">vinay@cozmotravel.com</span>
                                    </td>
                                </tr>
                                <tr height="30px">
                                    <td width="50%">
                                        <span style="color: Maroon; font-size:13px; font-family:Verdana;">For Group Queries</span>
                                    </td>
                                    <td>:</td>
                                    <td width="50%">
                                        <span style="color: Blue; font-size:13px; font-family:Verdana;">ziyad@cozmotravel.com</span>
                                    </td>
                                </tr>
                              <tr height="30px">
                                    <td width="50%">
                                        <span style="color: Maroon; font-size:13px; font-family:Verdana;">For Other Queries</span>
                                    </td>
                                    <td>:</td>
                                    <td width="50%">
                                        <span style="color: Blue; font-size:13px; font-family:Verdana;">shiva@cozmotravel.com</span>
                                    </td>
                                </tr>
                               <tr height="30px">
                                    <td width="50%">
                                        <span style="color: Maroon; font-size:13px; font-family:Verdana;">For International Refund, Reissue & Other Queries</span>
                                    </td>
                                    <td>:</td>
                                    <td width="50%">
                                        <span style="color: Blue; font-size:13px; font-family:Verdana;">info@cozmotravel.com</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                    <td width="">
                        <div align="center" style="width: 400px; height: 250px; margin-top: 20px; border: solid 1px #ccc;
                            border-radius: 16px;">
                            <table width="100%" cellspacing="8px">
                                <tr height="30px">
                                    <td align="left" colspan="2">
                                        <span style="color: Black; font-size: 16px; font-family: Verdana;">Bank Details</span>
                                    </td>
                                </tr>
                                <tr height="30px">
                                    <td width="50%">
                                        <span style="color: Maroon; font-size:13px; font-family:Verdana;">Details1</span>
                                    </td>
                                    <td>:</td>
                                    <td width="50%">
                                        <span style="color: Black; font-size:13px; font-family:Verdana;">vijay@cozmotravel.com</span>
                                    </td>
                                </tr>
                                <tr height="30px">
                                    <td width="50%">
                                        <span style="color: Maroon; font-size:13px; font-family:Verdana;">Details2</span>
                                    </td>
                                    <td>:</td>
                                    <td width="50%">
                                        <span style="color: Black; font-size:13px; font-family:Verdana;">vinay@cozmotravel.com</span>
                                    </td>
                                </tr>
                                <tr height="30px">
                                    <td width="50%">
                                        <span style="color: Maroon; font-size:13px; font-family:Verdana;">Details3</span>
                                    </td>
                                    <td>:</td>
                                    <td width="50%">
                                        <span style="color: Black; font-size:13px; font-family:Verdana;">ziyad@cozmotravel.com</span>
                                    </td>
                                </tr>
                              <tr height="30px">
                                    <td width="50%">
                                        <span style="color: Maroon; font-size:13px; font-family:Verdana;">Details4</span>
                                    </td>
                                    <td>:</td>
                                    <td width="50%">
                                        <span style="color: Black; font-size:13px; font-family:Verdana;">shiva@cozmotravel.com</span>
                                    </td>
                                </tr>
                               <tr height="30px">
                                    <td width="50%">
                                        <span style="color: Maroon; font-size:13px; font-family:Verdana;">Details5</span>
                                    </td>
                                    <td>:</td>
                                    <td width="50%">
                                        <span style="color: Black; font-size:13px; font-family:Verdana;">info@cozmotravel.com</span>
                                    </td>
                                </tr>
                            </table>
                        </div>
                    </td>
                </tr>
            </table>
            </td>
        </tr>
    </table>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
