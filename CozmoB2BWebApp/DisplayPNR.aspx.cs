﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Web.Services;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;

public partial class DisplayPNRGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected FlightItinerary itinerary = new FlightItinerary();
    protected Ticket[] ticket;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        if (Settings.LoginInfo != null)
        {
            if (Request["Error"] != null && Request["Error"].Length > 0)
            {
                lblError.Text = Request["Error"].ToString();
            }
            if (!IsPostBack)
            {
                InitializeControls();
            }
        }
        else
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
    private void InitializeControls()
    {
        try
        {
            BindAgents();
            if (Settings.LoginInfo.AgentType == AgentType.B2B || Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                //rbtnSelf.Checked = false;
                rbtnSelf.Visible= false;
                rbtnAgent.Visible = false;
                //rbtnAgent.Checked = true;
                rbtnSelf.Checked = true;
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Visible=ddlAgent.Enabled = false;
            }

            if (Settings.LoginInfo.IsCorporate == "Y")
            {
                DataSet ds = new DataSet();
                ds.Tables.Add(TravelUtility.GetTravelReasonList("S", Settings.LoginInfo.AgentId, ListStatus.Short));
                ds.Tables.Add(TravelUtility.GetProfileList(Settings.LoginInfo.CorporateProfileId, Settings.LoginInfo.AgentId, ListStatus.Short));
                hdnCorpInfo.Value = JsonConvert.SerializeObject(ds);
            }

            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "disablefield()", "disablefield");
            if(Session["itinerary"]!=null)
                Session.Remove("itinerary");

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindAgents()
    {
        try
        {
            int agentId = Convert.ToInt32(Settings.LoginInfo.AgentId);
            //AgentMaster1 agent = new AgentMaster1(agentId);
            //DataTable dtAgents = null;
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            //if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
            //{
            //    dtAgents = AgentMaster.GetList(1, "ALL", agentId, ListStatus.Short, RecordStatus.Activated);
            //}
            //else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            //{
            //    dtAgents = AgentMaster.GetList(1, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);
            //}
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "0"));
            ddlAgent.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnRetrieve_Click(object sender, EventArgs e)
    {
        try
        {
            int agentId = 0;
            if (rbtnSelf.Visible && rbtnSelf.Checked)
            {
                agentId = Settings.LoginInfo.AgentId;
                Settings.LoginInfo.IsOnBehalfOfAgent = false;
            }
            else if (rbtnAgent.Checked)
            {
                Settings.LoginInfo.OnBehalfAgentID = Convert.ToInt32(ddlAgent.SelectedItem.Value);

                Settings.LoginInfo.IsOnBehalfOfAgent = true;
                AgentMaster agent = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                Settings.LoginInfo.OnBehalfAgentCurrency = agent.AgentCurrency;
                Settings.LoginInfo.OnBehalfAgentDecimalValue = agent.DecimalValue;
                CT.Core.StaticData sd = new CT.Core.StaticData();
                sd.BaseCurrency = agent.AgentCurrency;
                Settings.LoginInfo.OnBehalfAgentExchangeRates = sd.CurrencyROE;
                Dictionary<string, SourceDetails> AgentCredentials = AgentMaster.GetAirlineCredentials(Settings.LoginInfo.OnBehalfAgentID);
                
                Settings.LoginInfo.OnBehalfAgentSourceCredentials = AgentCredentials;

                //agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);

                //Added by lokesh
                //If onbehalf assign the agent location id.
                Settings.LoginInfo.OnBehalfAgentLocation = Convert.ToInt32(hdnAgentLocation.Value);
            }

            string sPrfid = !string.IsNullOrEmpty(hdnCorpInfo.Value) ? hdnCorpInfo.Value.Split('|')[0].Split('~')[0] : string.Empty;
            string sReason = !string.IsNullOrEmpty(hdnCorpInfo.Value) ? hdnCorpInfo.Value.Split('|')[1].Split('~')[0] : string.Empty;

            Response.Redirect("DisplayPNRInformation.aspx?pnrNo=" + txtPnr.Text.Trim().ToUpper() + "&source=" + ddlSource.SelectedItem.Value + "&prfid=" + sPrfid + "&trid=" + sReason, false);
            //int flightId = FlightItinerary.GetFlightId(txtPnr.Text.Trim());
            //FlightItinerary urItinerary = new FlightItinerary(flightId);
            ////FlightItinerary urItinerary = new FlightItinerary(FlightItinerary.GetFlightId(txtPnr.Text.Trim()));
            //string uRecord = urItinerary.UniversalRecord;
            //if (!string.IsNullOrEmpty(uRecord))
            //{
            //    itinerary = UAPI.RetrieveItinerary(uRecord, out ticket);
            //    Session["itinerary"] = itinerary;
            //    Response.Redirect("DisplayPNRInformation.aspx", false);
            //}
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// To check agent type and get the corporate info 
    /// </summary>
    /// <param name="sProfileId"></param>
    /// <returns></returns>
    [WebMethod(EnableSession = true)]
    public static string GetCorpData(string sAgentId)
    {
        string JSONString = string.Empty;
        try
        {
            AgentMaster clsAM = new AgentMaster(long.Parse(sAgentId));

            if (clsAM.IsCorporate)
            {
                DataSet ds = new DataSet();
                ds.Tables.Add(TravelUtility.GetTravelReasonList("S", long.Parse(sAgentId), ListStatus.Short));
                ds.Tables.Add(TravelUtility.GetProfileList(Settings.LoginInfo.CorporateProfileId, long.Parse(sAgentId), ListStatus.Short));
                JSONString = JsonConvert.SerializeObject(ds);
            }
            return JSONString;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SpiceJetBooking, Severity.High, 1, "(GetCorpData)Failed to get corporate profiles and travel reasons info. Reason : " + ex.ToString(), "");
            return JSONString;
        }
    }
}
