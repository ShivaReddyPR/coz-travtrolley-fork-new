﻿using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class ApiRoomDetails : CT.Core.ParentPage//System.Web.UI.Page
    {
        protected HotelRequest request = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {     
                if (Session["req"] == null || Settings.LoginInfo == null)
                {
                    Response.Redirect("AbandonSession.aspx", true);
                }
                else
                {
                    request = Session["req"] as HotelRequest;
                    hdnObjRequest.Value = JsonConvert.SerializeObject(request);
                    hdnUserId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.UserID);
                    if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.AgentId);
                        hdnBehalfLocation.Value = JsonConvert.SerializeObject(0);
                    }
                    else
                    {
                        hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentID);
                        hdnBehalfLocation.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentLocation);
                    }
                    //hdnIndex.Value = JsonConvert.SerializeObject(Request.QueryString["index"]);
                    //hdnTotFare.Value = JsonConvert.SerializeObject(Request.QueryString["totFare"]);
                    //hdnHotelCode.Value = JsonConvert.SerializeObject(Request.QueryString["hotelCode"]);
                    //hdnSessionId.Value = JsonConvert.SerializeObject(Request.QueryString["sessionId"]);
                    //hdnSourceId.Value = JsonConvert.SerializeObject(Request.QueryString["sourceId"]);
                    hdnTokenId.Value = JsonConvert.SerializeObject(Session["ApiTokenId"]);
                    if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
                    {
                        hdnIsAgent.Value = "1";
                    }
                    else {
                        hdnIsAgent.Value = "0";
                    }
                    if(Settings.LoginInfo.IsCorporate=="Y")
                    {
                        hdnisCorporate.Value = JsonConvert.SerializeObject(Settings.LoginInfo.IsCorporate);
                    }
                    else
                    {
                        hdnisCorporate.Value = JsonConvert.SerializeObject(Settings.LoginInfo.IsCorporate);
                    }
                }
            }
            catch (Exception ex)
            {
                if (Settings.LoginInfo != null)
                {
                    Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Room Detail Results. Error: " + ex.Message, Request["REMOTE_ADDR"]);
                }
                else
                {
                    Response.Redirect("AbandonSession.aspx");
                }
            }

        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string HotelResult(string data)
        {
            try
            {
                data = data.Replace("\\","");
                HotelSearchResult Result = JsonConvert.DeserializeObject<HotelSearchResult>(data);
                HttpContext.Current.Session["ApiHotelResult"] = Result;
                return "Success";
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Room Detail (HotelResult) . Error: " + ex.Message,string.Empty);
                return null;
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetRegion(string CountryName)
        {
            try
            {
           string countryCode= Country.GetCountryCodeFromCountryName(CountryName);
            Country country= Country.GetCountry(countryCode);
                return country.Regioncode;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Room Detail (HotelResult) . Error: " + ex.Message, string.Empty);
                return null;
            }
        }
        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public static string GetGroupTaxes(string taxLists)
        {
            try
            {
               List<RoomTaxBreakup> Result = JsonConvert.DeserializeObject<List<RoomTaxBreakup>>(taxLists);
                var distinctList=Result.Distinct();
                List<RoomTaxBreakup> finalResult = new List<RoomTaxBreakup>();
                foreach (var Breakups in distinctList)
                {
                    RoomTaxBreakup rbreakup = new RoomTaxBreakup();
                    rbreakup = Breakups;
                    List<RoomTaxBreakup> commonList = new List<RoomTaxBreakup>();
                    commonList=Result.Where(x => x.TaxTitle == Breakups.TaxTitle && x.UnitType == Breakups.UnitType && x.IsIncluded==Breakups.IsIncluded).ToList();
                    decimal sum = 0;
                    foreach(var taxes in commonList)
                    {
                        sum = sum + taxes.Value;
                    }
                    rbreakup.Value = sum;
                    if(!finalResult.Exists(x=>x.TaxTitle==rbreakup.TaxTitle && x.UnitType==rbreakup.UnitType && x.IsIncluded == Breakups.IsIncluded))
                    finalResult.Add(rbreakup);
                }

                return JsonConvert.SerializeObject(finalResult);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Room Detail (HotelResult) . Error: " + ex.Message, string.Empty);
                return null;
            }
        }
    }
}
