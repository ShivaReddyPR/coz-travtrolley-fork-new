﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="ETicket" Codebehind="ETicketOLD.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="CT.BookingEngine" %>
<%@ Import Namespace="CT.Core" %>
<%@ Import Namespace="System.Collections.Generic" %>
<%@ Import Namespace="CT.Configuration" %>
<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="CT.TicketReceipt.Common" %>
<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <style>
        h1, h2, h3 {
            margin: 0px;
            padding: 0px;
        }

        body {
            margin: 20px 0% 20px 0%;
            font-family: Verdana, Geneva, sans-serif;
            font-size: 13px;
        }

        hr {
            margin: 0px;
            border: 0;
            height: 1px;
            background: #333;
            background-image: linear-gradient(to right, #ccc, #333, #ccc);
        }
    </style>
    <title>E Ticket</title>
    <%--<script src="ash.js" type="text/javascript"></script>--%>

    <link href="css/main-style.css" rel="stylesheet" type="text/css" />

    <script language="JavaScript" type="text/javascript" src="Scripts/Utils.js"></script>

    <%--<script language="JavaScript" src="prototype.js" type="text/javascript"></script>--%>

    <script language="JavaScript" type="text/javascript">
        function ClosePop() {
            window.close();
        }
    </script>

    <script language="JavaScript" type="text/javascript">
<!--


        //Added by lokesh on 22-06-2018
        //When the user clicks on the print button do not display the 
        //email, print and show fare labels in the print functionality.
        function prePrint() {
            document.getElementById('ancFare').style.display = "none";
            if (document.getElementById('ancFareRouting') != null) {
                document.getElementById('ancFareRouting').style.display = "none";
            }
            document.getElementById('tblPrintEmailActions').style.display = "none";
            window.print();
            setTimeout('showFareButtons()', 1000);
            return false;
        }
        function showFareButtons() {
            document.getElementById('ancFare').style.display = "block";
            if (document.getElementById('ancFareRouting') != null) {
                document.getElementById('ancFareRouting').style.display = "block";
            }
            document.getElementById('tblPrintEmailActions').style.display = "block";
        }

    function showAllButtons() {
        document.getElementById('Print').style.display = "block";

        if (document.getElementById('SendMailButton')) {
            document.getElementById('SendMailButton').style.display = "block";
        }
    }

    function ShowEmailDivD() {
        document.getElementById('LowerEmailSpan').style.display = 'block';
        document.getElementById('addressBox').focus();
    }
    function HideEmailDiv() {
        document.getElementById('LowerEmailSpan').style.display = 'none';

    }
    var Ajax;
    if (window.XMLHttpRequest) {
        Ajax = new window.XMLHttpRequest();
    }
    else {
        Ajax = new ActiveXObject("Microsoft.XMLHTTP");
    }
    function SendMail() {
        var addressList = document.getElementById('addressBox').value;
        if (Trim(addressList) == '') {
            document.getElementById('emailSent').style.display = 'block';
            document.getElementById('messageText').innerHTML = 'Please fill the address';
            return;
        }
        if (!ValidEmail.test(Trim(addressList))) {
            document.getElementById('emailSent').style.display = 'block';
            document.getElementById('messageText').innerHTML = 'Please fill correct email address';
            return;
        }
        var paramList = 'ticketId=' + ticketId;
        paramList += "&addressList=" + addressList;
        paramList += "&isEticket=true";
        var url = "EmailItineraryPage.aspx";

        Ajax.onreadystatechange = DisplayMessage;
        Ajax.open('POST', url);
        Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
        Ajax.send(paramList);
    }
    function DisplayMessage() {
        if (Ajax.readyState == 4) {
            if (Ajax.status == 200) {
                document.getElementById('emailSent').style.display = 'block';
                document.getElementById('messageText').innerHTML = 'Email Sent Successfully';
                HideEmailDiv();
            }
        }
    }

    function ShowPopUp(id) {
            //document.getElementById('<%#txtEmailId.ClientID %>').value = "";
            document.getElementById('txtEmailId').value = "";
            document.getElementById('err').style.display = 'none';
            document.getElementById('emailBlock').style.display = "block";
            //            var positions = getRelativePositions(document.getElementById(id));
            //            document.getElementById('emailBlock').style.left = (530) + 'px';
            //            document.getElementById('emailBlock').style.top = (400) + 'px';
            return false;
        }
        function HidePopUp() {
            document.getElementById('emailBlock').style.display = "none";
        }
        function Validate() {
            var isValid = true;
            var reg = /^\w+([-+.']\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/
            document.getElementById('err').style.display = 'none';
            if (document.getElementById('<%#txtEmailId.ClientID %>').value.length <= 0) {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Email address";
                isValid = false;
            }
            else if (reg.test(document.getElementById('<%#txtEmailId.ClientID %>').value)) {
                document.getElementById('err').style.display = 'none';
            }
            else {
                document.getElementById('err').style.display = 'block';
                document.getElementById('err').innerHTML = "Enter Correct Email";
                isValid = false;
            }
            if (isValid == true) {
                return true;
            }
            else {
                return false;
            }
        }

        function ShowHide(div) {
            debugger;
            if (document.getElementById('hdfFare').value == '1') {
                document.getElementById('ancFare').innerHTML = 'Show Fare'
                document.getElementById(div).style.display = 'none';
                document.getElementById('hdfFare').value = '0';
            }
            else {
                document.getElementById('ancFare').innerHTML = 'Hide Fare'
                document.getElementById('ancFare').value = 'Hide Fare'
                document.getElementById(div).style.display = 'block';
                document.getElementById('hdfFare').value = '1';
            }
        }
// -->
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <center>

            <!-- mail block -->
            <div id="emailBlock" class="showmsg" style="position: absolute; display: none; left: 400px; top: 200px; width: 300px;">
                <div class="showMsgHeading">Enter Your Email Address</div>

                <a style="position: absolute; right: 5px; top: 3px;" onclick="return HidePopUp();" href="#" class="closex">X</a>
                <div class="padding-5">
                    <div style="background: #fff">
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">

                            <tr>
                                <td height="40" align="center">
                                    <b style="display: none; color: Red" id="err"></b>
                                    <asp:TextBox Style="border: solid 1px #ccc; width: 90%; padding: 2px;" ID="txtEmailId" runat="server" CausesValidation="True"></asp:TextBox>
                                </td>
                            </tr>
                            <tr>

                                <td height="40" align="center">
                                    <asp:Button CssClass="button-normal" ID="btnEmailVoucher" runat="server" Text="Send Email" OnClientClick="return Validate()" OnClick="btnEmailVoucher_Click" />
                                </td>
                            </tr>
                        </table>
                    </div>
                </div>
            </div>

            <asp:MultiView ID="MultiViewETicket" runat="server">
                <asp:View ID="TicketView" runat="server">
                    <asp:HiddenField ID="hdfFare" runat="server" Value="1" />
                    <asp:HiddenField ID="hdfTicketType" runat="server" />
                    <div id="PrintDiv" runat="server" style="width: 100%">
                        <div style="padding: 10PX">

                            <div style="display: none;" id="emailSent">
                                <div style="float: left; width: 100%; margin: auto; text-align: center;">
                                    <span class="email-message-child" id="messageText">Email sent successfully</span>
                                </div>
                            </div>
                            <br />
                            <% int bc = 0;
                                List<SegmentPTCDetail> ptcDetails = new List<SegmentPTCDetail>();
                                if (Request["bkg"] != null)
                                {
                                    ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(flightItinerary.FlightId);
                                }
                                else
                                {
                                    ptcDetails = ticketList[0].PtcDetail;
                                }
                                for (int count = 0; count < flightItinerary.Segments.Length; count++)
                                {
                                    int paxIndex = 0;
                                    if (Request["paxId"] != null)
                                    {
                                        paxIndex = Convert.ToInt32(Request["paxId"]);
                                    }
                                    List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
                                    if (Request["paxId"] == null)
                                    {
                                        ptcDetail = ptcDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.SegmentId == flightItinerary.Segments[count].SegmentId; });
                                    }
                                    else
                                    {
                                        ptcDetail = ptcDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.SegmentId == flightItinerary.Segments[count].SegmentId && ptc.PaxType == paxType; });
                                    }%>
                            <table width="600px" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td><%if (count == 0)
                                                { %>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <h2>e-Ticket</h2>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--<tr style="display:none;">
                                                                                        <td>Trip ID : <%=booking.BookingId%></td>
                                                                                    </tr>--%>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <!--Always show booking date only --> 
                    <%if (flightItinerary != null && flightItinerary.CreatedOn != DateTime.MinValue) %>
                    <%{ %>
                                              Booking Date:  <%=flightItinerary.CreatedOn.ToString("ddd") + "," + flightItinerary.CreatedOn.ToString("dd MMM yyyy")%>
                    <%} %>
                                                                                            
                                                                                            </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td align="right" valign="top">
                                                                            <%-- <img ID="imgLogo" runat="server" src="./Eticket_files/main-logo.svg" alt="#" height="70" />--%>
                                                                            <asp:Image ID="imgLogo" style="border-width:0px;width: 159px;height: 51px" runat="server" AlternateText="AgentLogo" ImageUrl="<%=Request.Url.Scheme%>://ctb2b.cozmotravel.com/images/Logo.jpg" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <hr />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td rowspan="3" valign="top">
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <h2>Reservation Details </h2>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><strong>PNR No.  <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR)%></strong></td>
                                                                                    </tr>
                                                                                    <%if(!string.IsNullOrEmpty(flightItinerary.RoutingTripId)){ %>
                                                                                    <tr>
                                                                                        <td><strong>Routing Trip Id: <%=flightItinerary.RoutingTripId %></strong></td>
                                                                                    </tr>
                                                                                    <%} %>
                                                                                     <%var parentid =CT.TicketReceipt.BusinessLayer.AgentMaster.GetParentId(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId);
                                                                                         if (parentid == 2125 || CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId==2125)

                                                                                         {
                                                                                      CT.TicketReceipt.BusinessLayer.LocationMaster location = new CT.TicketReceipt.BusinessLayer.LocationMaster(flightItinerary.LocationId);       %>
                                                                                    <tr>
                                                                                        <td><strong>GSTIN: <%=location.GstNumber %></strong></td>
                                                                                    </tr>
                                                                                    <%} %>
                                                                                </tbody>
                                                                            </table>
                                                                            <h2>&nbsp;</h2>
                                                                        </td>
                                                                        <td align="right">
                                                                            <table id="tblPrintEmailActions" width="200" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table border="0" cellspacing="0" cellpadding="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <img id="imgEmail" runat="server" src="https://ctb2b.cozmotravel.com/images/email_icon.png" width="15" height="12" alt="#" /></td>
                                                                                                        <td>
                                                                                                            <asp:LinkButton ID="btnEmail" runat="server" Text="Email" OnClientClick="return ShowPopUp(this.id);" /></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td>
                                                                                            <table border="0" cellpadding="0" cellspacing="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td>
                                                                                                            <img id="imgPrint" runat="server" src="https://ctb2b.cozmotravel.com/images/print_icon.jpg" width="20" height="20" alt="#" /></td>
                                                                                                        <td>
                                                                                                            <asp:LinkButton ID="btnPrint" runat="server" Text="Print" OnClientClick="return prePrint();" /></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right"><strong>Agent Name: <%=agency.Name %></strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right"><strong>Phone: <%=agency.Phone1 %></strong></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <% }%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><%if (count == 0)
                                                { %>
                                            <table width="100%" border="1" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="middle"><strong>Passenger Name</strong></td>
                                                        <td align="center" valign="middle"><strong>E-Ticket Number</strong></td>
                                                        <td align="center" valign="middle"><strong>Baggage</strong></td>
                                                        <%if (flightItinerary != null && flightItinerary.FlightBookingSource == BookingSource.UAPI || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp)  %>
                                                        <%{ %>
                                                        <td align="center" valign="middle"><strong>Meal</strong></td>
                                                        <td align="center" valign="middle"><strong>Seats</strong></td>
                                                        <%} %>
                                                        <td align="center" valign="middle"><strong>Airline Ref.</strong></td>
                                                    </tr>
                                                    <%if (hdfTicketType.Value == "Individual")
                                                        { %>
                                                    <tr>
                                                        <td align="center" valign="top"><%=paxName %></td>
                                                        <td align="center" valign="top">
                                                            <%if (Request["bkg"] == null)
                                                                { %>
                                                            <%=(ticketList[0].TicketNumber.Split('|').Length > 1 ? ticketList[0].TicketNumber.Split('|')[flightItinerary.Segments[count].Group] : ticketList[0].TicketNumber) %>
                                                            <%}
                                                                else
                                                                { %>
                                                            <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[count] : flightItinerary.PNR)%>
                                                            <%} %></td>
                                                        <td align="center" valign="top">
                                                            <%if (flightItinerary.FlightBookingSource == BookingSource.Amadeus || flightItinerary.FlightBookingSource == BookingSource.UAPI || (flightItinerary.FlightBookingSource == BookingSource.TBOAir && !(flightItinerary.IsLCC)))
    {%>
                             <%=GetBaggageForGDS(flightItinerary.FlightId,flightItinerary.Passenger[paxIndex].Type)%>
    <%}   

 else if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.PKFares || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir  || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp ||  flightItinerary.FlightBookingSource == BookingSource.Jazeera )
    {
        if (flightItinerary.Passenger[paxIndex].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(flightItinerary.Passenger[paxIndex].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[paxIndex].BaggageCode)%>
                                                     <%}
    }
    else if (flightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl && flightItinerary.Passenger[paxIndex].Type != PassengerType.Infant)
    {%>
                                                       
                                                            <%=(string.IsNullOrEmpty(flightItinerary.Passenger[paxIndex].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[paxIndex].BaggageCode)%>
                                                        
                                                    <%}
    //Added by lokesh on 22-06-2018
    //For Amadeus Air Source
    //Display baggage information based on the pax type.
    else if (flightItinerary.FlightBookingSource == BookingSource.Amadeus && flightItinerary.Passenger[paxIndex].Type != PassengerType.Infant)
    {%>
              <%=(string.IsNullOrEmpty(flightItinerary.Passenger[paxIndex].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[paxIndex].BaggageCode)%>
    <%}

    else if (flightItinerary.FlightBookingSource == BookingSource.TBOAir && (flightItinerary.IsLCC))
    {
        if (flightItinerary.Passenger[paxIndex].Type != PassengerType.Infant)
        {
            string strBaggage = string.Empty;
            
            if (!string.IsNullOrEmpty(flightItinerary.Passenger[paxIndex].BaggageCode))
            {
                strBaggage = flightItinerary.Passenger[paxIndex].BaggageCode;
            }
            else
            {
                strBaggage = "Airline Norms";
            }
                                                           %>
                                                      <%=strBaggage%> 
                                                    <%}
    } %>
                                                            


                                                        </td>
                                                        <%if (flightItinerary.FlightBookingSource == BookingSource.UAPI || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{
                                                                string MealDesc = string.Empty;
                                                                %>
                                                        <td align="center" valign="top">
                                                             <%if (flightItinerary.Passenger[paxIndex].Type != PassengerType.Infant && !string.IsNullOrEmpty(flightItinerary.Passenger[paxIndex].MealDesc)) %>
                                                            <%{
                                                                    MealDesc = flightItinerary.Passenger[paxIndex].MealDesc;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Meals"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <td align="center" valign="top">
                                                             <%if (flightItinerary.Passenger[paxIndex].Type != PassengerType.Infant && !string.IsNullOrEmpty(flightItinerary.Passenger[paxIndex].Seat.Code)) %>
                                                            <%{
                                                                    MealDesc = flightItinerary.Passenger[paxIndex].Seat.Code;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "NoSeat"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <%} %>

                                                        <td align="center" valign="top"><%=(flightItinerary.Segments[count].AirlinePNR == null || flightItinerary.Segments[count].AirlinePNR == "" ? (flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR) : flightItinerary.Segments[count].AirlinePNR) %></td>
                                                    </tr>
                                                    <%}
                                                        else if (hdfTicketType.Value == "Multiple")
                                                        {%>
                                                    <%for (int j = 0; j < flightItinerary.Passenger.Length; j++)
                                                        {
                                                    %>
                                                    <tr>
                                                        <td align="center" valign="top"><%=flightItinerary.Passenger[j].Title + " " + flightItinerary.Passenger[j].FirstName + " " + flightItinerary.Passenger[j].LastName%></td>
                                                        <td align="center" valign="top">
                                                            <%if (Request["bkg"] == null)
                                                                { %>
                                                            <%=(ticketList[j].TicketNumber.Split('|').Length > 1 ? ticketList[j].TicketNumber.Split('|')[flightItinerary.Segments[count].Group] : ticketList[j].TicketNumber) %>
                                                            <%}
                                                                else
                                                                { %>
                                                            <%=(flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[count] : flightItinerary.PNR)%>
                                                            <%} %></td>
                                                        <td align="center" valign="top">
                                                            
                                                            
   <%if (flightItinerary.FlightBookingSource == BookingSource.Amadeus || flightItinerary.FlightBookingSource == BookingSource.UAPI || (flightItinerary.FlightBookingSource == BookingSource.TBOAir && !(flightItinerary.IsLCC)))
    {%>
                             <%=GetBaggageForGDS(flightItinerary.FlightId,flightItinerary.Passenger[j].Type)%>
    <%}                                                            
 else if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.PKFares || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp || flightItinerary.FlightBookingSource == BookingSource.Jazeera)
    {
        if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[j].BaggageCode)%>
                                                     <%}
    }
    else if (flightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl && flightItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
                                                       
                                                            <%=(string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[j].BaggageCode)%>
                                                        
                                                    <%}
    //Added by lokesh on 22-06-2018
    //For Amadeus Air Source
    //Display baggage information based on the pax type.
    else if (flightItinerary.FlightBookingSource == BookingSource.Amadeus && flightItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
              <%=(string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : flightItinerary.Passenger[j].BaggageCode)%>
    <%}

    else if (flightItinerary.FlightBookingSource == BookingSource.TBOAir && (flightItinerary.IsLCC))
    {
        if (flightItinerary.Passenger[j].Type != PassengerType.Infant)
        {
            string strBaggage = string.Empty;
            
            if (!string.IsNullOrEmpty(flightItinerary.Passenger[j].BaggageCode))
            {
                strBaggage = flightItinerary.Passenger[j].BaggageCode;
            }
            else
            {
                strBaggage = "Airline Norms";
            }
                                                           %>
                                                      <%=strBaggage%> 
                                                    <%}
    } %> </td>


                                                        <%if (flightItinerary.FlightBookingSource == BookingSource.UAPI || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{
                                                                string MealDesc = string.Empty;
                                                                %>
                                                        <td>
                                                             <%if (flightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(flightItinerary.Passenger[j].MealDesc)) %>
                                                            <%{
                                                                    MealDesc = flightItinerary.Passenger[j].MealDesc;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Meals"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <td>
                                                             <%if (flightItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(flightItinerary.Passenger[j].Seat.Code)) %>
                                                            <%{
                                                                    MealDesc = flightItinerary.Passenger[j].Seat.Code;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "NoSeat"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <%} %>
                                                        <td align="center" valign="top"><%=(flightItinerary.Segments[count].AirlinePNR == null || flightItinerary.Segments[count].AirlinePNR == "" ? (flightItinerary.PNR.Split('|').Length > 1 ? flightItinerary.PNR.Split('|')[flightItinerary.Segments[count].Group] : flightItinerary.PNR) : flightItinerary.Segments[count].AirlinePNR) %></td>
                                                    </tr>
                                                    <%}
                                                        }%>
                                                </tbody>
                                            </table>
                                            <% }%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" border="1" style="border-collapse: collapse;" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="border-right: solid 1px #ccc;" valign="top">
                                                                            <table style="margin: auto" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="padding-top: 5px" align="center">
                                                                                            <img src="<%=Request.Url.Scheme%>://ctb2b.cozmotravel.com/images/AirlineLogo/<%=airline.AirlineCode%>.gif" alt="#" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center"><%airline.Load(flightItinerary.Segments[count].Airline); %> <%=airline.AirlineName%></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center"><strong><%=flightItinerary.Segments[count].Airline + "-" + flightItinerary.Segments[count].FlightNumber%><br />
                                                                                            <!--Displaying Code Share Info (Operating Airline name) for the Segment (shiva 21 Sep 2016) -->
                                                                                            <%try
                                                                                                {
                                                                                                    if ((!flightItinerary.IsLCC && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0)||(flightItinerary.FlightBookingSource == BookingSource.Indigo && flightItinerary.Segments[count].OperatingCarrier != null && flightItinerary.Segments[count].OperatingCarrier.Length > 0 && flightItinerary.Segments[count].OperatingCarrier != "6E"))////Modified for Indigo CodeShare
                                                                                                    {
                                                                                                        string opCarrier = flightItinerary.Segments[count].OperatingCarrier;
                                                                                                        Airline opAirline = new Airline();
                                                                                                        if (opCarrier.Split('|').Length > 1)
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Split('|')[0]);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Substring(0, 2));
                                                                                                        } %>
                                             (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                                }
                                                catch { } %></strong></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td align="center" valign="top">
                                                                            <table style="margin: auto" width="94%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td><i>Departure </i></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><strong><%=flightItinerary.Segments[count].Origin.CityName + "( " + flightItinerary.Segments[count].Origin.CityCode+")"%></strong></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><%=flightItinerary.Segments[count].DepartureTime.ToString("ddd")+","+flightItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy")+","+flightItinerary.Segments[count].DepartureTime.ToString("HH:mm tt")%></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><%=flightItinerary.Segments[count].Origin.AirportName %> </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Terminal <%=flightItinerary.Segments[count].DepTerminal %></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                        <td>
                                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td><i>Arrival </i></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><strong><%=flightItinerary.Segments[count].Destination.CityName + "( " + flightItinerary.Segments[count].Destination.CityCode+")"%></strong></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><%=flightItinerary.Segments[count].ArrivalTime.ToString("ddd")+","+flightItinerary.Segments[count].ArrivalTime.ToString("dd MMM yyyy")+","+flightItinerary.Segments[count].ArrivalTime.ToString("HH:mm tt")%></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><%=flightItinerary.Segments[count].Destination.AirportName %>  </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Terminal <%=flightItinerary.Segments[count].ArrTerminal %></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td style="border-left: solid 1px #ccc;" valign="top">
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="center">
                  <%if (flightItinerary.Segments[count].Stops == 0) %>
                    <%{ %>
                    <strong> Non-Stop </strong>
                    <%} %>
                    <%else if (flightItinerary.Segments[count].Stops == 1) %>
                    <%{ %>
                    <strong> Single Stop </strong>
                    <%} %>
                    <%else if (flightItinerary.Segments[count].Stops == 2) %>
                    <%{ %>
                    <strong> Two Stops </strong>
                    <%} %>
                    <%else if (flightItinerary.Segments[count].Stops> 2) %>
                    <%{ %>
                    <strong> Two+ Stops </strong>
                    <%} %></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <%=flightItinerary.Segments[count].Duration.Hours +"hr "+flightItinerary.Segments[count].Duration.Minutes+"m"%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">Class:  <%=flightItinerary.Segments[count].CabinClass%>
                                                                                            <%if (flightItinerary.FlightBookingSource == BookingSource.FlyDubai || flightItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp || flightItinerary.FlightBookingSource == BookingSource.Jazeera)
                                                                                                {%>
                                                                                            <br />
                                                                                            <label style='padding-left: 20px'>
                                                                                                <strong>Fare Type :<br />
                                                                                                </strong><%=flightItinerary.Segments[count].SegmentFareType%></label>
                                                                                            <%} %></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="color: #66ca1d" align="center"><%=booking.Status.ToString()%></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <%}%>
                                </tbody>
                            </table>
                            <table cellpadding="0" cellspacing="0" class="label">
                                <tr>
                                    <td style="width: 600px" align="right">
                                        <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancFare" runat="server"
                                            onclick="return ShowHide('divFare');">Hide Fare</a>
                                    </td>
                                </tr>
                            </table>                           
                            <div title="Param" id="divFare" runat="server" style="font-size: 12px;">
                                <table style="font-size: 12px;" width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0, outputVAT = 0,Meal =0,SeatPrice =0, k3Tax = 0;
                                                if (Request["bkg"] == null) // For Ticketing
                                                {
                                                    int paxId = -1;

                                                    List<TaxBreakup> allPaxTaxBreakups = new List<TaxBreakup>();
                                                    ticketList.ForEach(t =>  t.TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); } ));

                                                    switch(flightItinerary.FlightBookingSource)
                                                    {
                                                        case BookingSource.GoAir:
                                                        case BookingSource.GoAirCorp:
                                                            k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("ST") || t.TaxCode.Contains("CT") || t.TaxCode.Contains("IT")? t.TaxValue : 0);
                                                            break;
                                                        case BookingSource.UAPI:
                                                            k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                                            break;
                                                        case BookingSource.SpiceJet:
                                                        case BookingSource.SpiceJetCorp:
                                                        case BookingSource.Indigo:
                                                        case BookingSource.IndigoCorp:
                                                            k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("GST") ? t.TaxValue : 0);
                                                            break;
                                                        default:
                                                            k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                                            break;
                                                    }

                                                    k3Tax = Convert.ToDecimal(k3Tax.ToString("N" + agency.DecimalValue));
                                                    if (Request["paxId"] != null)
                                                    {
                                                        paxId = Convert.ToInt32(Request["paxId"]);
                                                    }
                                                    if(paxId >-1 && flightItinerary.Passenger[paxId].Type == PassengerType.Infant && (flightItinerary.FlightBookingSource!=BookingSource.TBOAir && flightItinerary.FlightBookingSource!=BookingSource.UAPI ))
                                                    {
                                                        k3Tax = 0;
                                                    }
                                                    //if(paxId>=0)
                                                    //{
                                                    //    k3Tax = k3Tax / flightItinerary.Passenger.Length;
                                                    //}
                                                    for (int k = 0; k < ticketList.Count; k++)
                                                    {
                                                        AirFare += ticketList[k].Price.PublishedFare + ticketList[k].Price.HandlingFeeAmount;
                                                        Taxes += ticketList[k].Price.Tax + ticketList[k].Price.Markup;
                                                        if(string.IsNullOrEmpty(flightItinerary.RoutingTripId))
                                                        {
                                                            k3Tax = 0;
                                                        }
                                                        if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                                        {
                                                            Taxes += ticketList[k].Price.OtherCharges + ticketList[k].Price.SServiceFee + ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.TransactionFee;
                                                        }
                                                        if (flightItinerary.FlightBookingSource == BookingSource.OffLine)
                                                        {
                                                            Taxes -= (ticketList[k].Price.AdditionalTxnFee + ticketList[k].Price.TransactionFee);
                                                        }
                                                        Baggage += ticketList[k].Price.BaggageCharge;
                                                        Meal += ticketList[k].Price.MealCharge;
                                                        SeatPrice += ticketList[k].Price.SeatPrice;
                                                        MarkUp += ticketList[k].Price.Markup;
                                                        Discount += ticketList[k].Price.Discount;
                                                        AsvAmount += ticketList[k].Price.AsvAmount;
                                                        outputVAT += ticketList[k].Price.OutputVATAmount;
                                                    }
                                                    if (ticketList[0].Price.AsvElement == "BF")
                                                    {
                                                        AirFare += AsvAmount;
                                                    }
                                                    else if (ticketList[0].Price.AsvElement == "TF")
                                                    {
                                                        Taxes += AsvAmount;
                                                    }
                                                }
                                                else // For Hold Itinerary View
                                                {
                                                    int paxId = 0;

                                                    if (Request["paxId"] != null)
                                                    {
                                                        paxId = Convert.ToInt32(Request["paxId"]);
                                                    }

                                                    for (int i = 0; i < flightItinerary.Passenger.Length; i++)
                                                    {
                                                        if (Request["paxId"] != null && i == paxId) // Induvidual
                                                        {
                                                            FlightPassenger pax = flightItinerary.Passenger[i];
                                                            AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;
                                                            Taxes += pax.Price.Tax + pax.Price.Markup;
                                                            if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                                            {
                                                                Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                                                            }
                                                            if (flightItinerary.FlightBookingSource == BookingSource.OffLine)
                                                            {
                                                                Taxes += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                                                            }
                                                            Baggage += pax.Price.BaggageCharge;
                                                            Meal += pax.Price.MealCharge;
                                                            SeatPrice += pax.Price.SeatPrice;
                                                            MarkUp += pax.Price.Markup;
                                                            Discount += pax.Price.Discount;
                                                            AsvAmount += pax.Price.AsvAmount;
                                                            outputVAT += pax.Price.OutputVATAmount;
                                                            break;
                                                        }
                                                        if (Request["paxId"] == null)// For all
                                                        {
                                                            FlightPassenger pax = flightItinerary.Passenger[i];
                                                            AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;

                                                            if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                                                            {
                                                                Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                                                            }
                                                            if (flightItinerary.FlightBookingSource == BookingSource.OffLine)
                                                            {
                                                                Taxes += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                                                            }
                                                            else
                                                                Taxes += pax.Price.Tax + pax.Price.Markup;

                                                            Baggage += pax.Price.BaggageCharge;
                                                            Meal += pax.Price.MealCharge;
                                                            SeatPrice += pax.Price.SeatPrice;
                                                            MarkUp += pax.Price.Markup;
                                                            Discount += pax.Price.Discount;
                                                            AsvAmount += pax.Price.AsvAmount;
                                                            outputVAT += pax.Price.OutputVATAmount;//Load GST or VAT for Hold bookings also in order to match the total
                                                        }
                                                    }

                                                    if (flightItinerary.Passenger[0].Price.AsvElement == "BF")
                                                    {
                                                        AirFare += AsvAmount;
                                                    }
                                                    else if (flightItinerary.Passenger[0].Price.AsvElement == "TF")
                                                    {
                                                        Taxes += AsvAmount;
                                                    }
                                                }
                                            %>
                                            <% //if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.BaseAgent)
                                               // { %>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="41%"></td>
                                                                    <td>
                                                                        <table width="50%" class="">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Air Fare</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=AirFare.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Taxes & Fees</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=(Taxes-k3Tax).ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <%if (!string.IsNullOrEmpty(flightItinerary.RoutingTripId)) { %>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>K3 Tax</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=k3Tax.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>
                                                                                <%if (flightItinerary.IsLCC)
                                                                                    { %>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Baggage Fare</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=Baggage.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>

                                                                                <%if (flightItinerary != null && (flightItinerary.FlightBookingSource == BookingSource.UAPI || flightItinerary.FlightBookingSource == BookingSource.Indigo || flightItinerary.FlightBookingSource == BookingSource.SpiceJet || flightItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || flightItinerary.FlightBookingSource == BookingSource.IndigoCorp || flightItinerary.FlightBookingSource == BookingSource.GoAir || flightItinerary.FlightBookingSource == BookingSource.GoAirCorp)) %>
                                                                                <%{ %>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Meal Price</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=Meal.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Seat Price</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=SeatPrice.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>

                                                                                <%if (Discount > 0)
                                                                                    { %>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Discount</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                       -<%=Discount.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <%CT.TicketReceipt.BusinessLayer.LocationMaster location = new CT.TicketReceipt.BusinessLayer.LocationMaster(flightItinerary.LocationId);
                                                                                             if (location.CountryCode == "IN")
                                                                                            {%>
                                                                                        <b>GST</b>
                                                                                        <%}    else
    { %>
                                                                                        <b>VAT</b>
                                                                                        <%} %>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=outputVAT.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3"> <hr /></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Total Air Fare</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%if (flightItinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                                                                                            { %>
                                                                                        <b><%=Math.Ceiling((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%}
                                                                                         else if (flightItinerary.FlightBookingSource == BookingSource.OffLine) 
                                                                                            { %>
                                                                                        <b><%=((AirFare + Taxes + outputVAT ) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%}

                                                                                            else
                                                                                            { %>
                                                                                        <b><%=((AirFare + Taxes + Baggage + Meal + SeatPrice + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%} %>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <%//}
                                              %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.Agent)
                                { %>
                            <script type="text/javascript">
                                 ShowHide("divFare");
                            </script> 
                            <%} %>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td><strong>Important Information </strong></td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td height="10">
                                            <hr />
                                        </td>
                                    </tr>

                                    <tr style="display: none;">
                                        <td>
                                            <ul>
                                                <li>Use your Trip ID for all communication with Cleartrip about this booking </li>
                                                <li>Check-in counters for International flights close 75 minutes before departure</li>
                                                <li>Your carry-on baggage shouldn't weigh more than 7kgs</li>
                                                <li>Carry photo identification, you will need it as proof of identity while checking-in</li>
                                                <li>Kindly ensure that you have the relevant visa, immigration clearance and travel with a passport, with a validity of at least 6 months.</li>
                                                <li>For hassle free refund processing, cancel/amend your tickets with Cleartrip Customer Care instead of doing so directly with Airline.</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td>
                                            <img src="<%=Request.Url.Scheme%>://ctb2b.cozmotravel.com/images/goapic.jpg" width="728" height="90" alt="#" /></td>
                                    </tr>
                                </tbody>
                            </table>

                                    <!-- Added by lokesh on 7-June-2018-->
        <!-- Need to display the below note in email for PK Fares -->
         <%if (flightItinerary != null && flightItinerary.FlightBookingSource == BookingSource.PKFares) %>
         <%{ %>
         <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                <span style="color:red">Note: This is just an itinerary. Ticket copy should not be handed over to customer unless ticket number is shown on the ticket.</span>
                    
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <%} %>



                        </div>
                   <%if(routingItinerary != null){ %>
                        <div style="padding: 10PX">

                            <div style="display: none;" id="emailSent">
                                <div style="float: left; width: 100%; margin: auto; text-align: center;">
                                    <span class="email-message-child" id="messageText">Email sent successfully</span>
                                </div>
                            </div>
                            <br />
                            <% bc = 0;
                                
                                if (Request["bkg"] != null)
                                {
                                    ptcDetails = SegmentPTCDetail.GetSegmentPTCDetail(routingItinerary.FlightId);
                                }
                                else if(routingTicketList.Count > 0)// Segment Wise Booking
                                {
                                    ptcDetails = routingTicketList[0].PtcDetail;
                                }
                                for (int count = 0; count < routingItinerary.Segments.Length; count++)
                                {
                                    int paxIndex = 0;
                                    if (Request["paxId"] != null)
                                    {
                                        paxIndex = Convert.ToInt32(Request["paxId"]);
                                    }
                                    List<SegmentPTCDetail> ptcDetail = new List<SegmentPTCDetail>();
                                    if (Request["paxId"] == null)
                                    {
                                        ptcDetail = ptcDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.SegmentId == routingItinerary.Segments[count].SegmentId; });
                                    }
                                    else
                                    {
                                        ptcDetail = ptcDetails.FindAll(delegate (SegmentPTCDetail ptc) { return ptc.SegmentId == routingItinerary.Segments[count].SegmentId && ptc.PaxType == paxType; });
                                    }%>
                            <table width="600px" border="0" cellspacing="0" cellpadding="0">
                                <tbody>
                                    <tr>
                                        <td><%if (count == 0)
                                                { %>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td valign="top">
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <h2>e-Ticket</h2>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <%--<tr style="display:none;">
                                                                                        <td>Trip ID : <%=booking.BookingId%></td>
                                                                                    </tr>--%>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <!--Always show booking date only --> 
                    <%if (routingItinerary != null && routingItinerary.CreatedOn != DateTime.MinValue) %>
                    <%{ %>
                                              Booking Date:  <%=routingItinerary.CreatedOn.ToString("ddd") + "," + routingItinerary.CreatedOn.ToString("dd MMM yyyy")%>
                    <%} %>
                                                                                            
                                                                                            </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td align="right" valign="top">
                                                                            <%-- <img ID="imgLogo" runat="server" src="./Eticket_files/main-logo.svg" alt="#" height="70" />--%>
                                                                            <asp:Image ID="Image1" style="border-width:0px;width: 159px;height: 51px" runat="server" AlternateText="AgentLogo" ImageUrl="<%=Request.Url.Scheme%>://ctb2b.cozmotravel.com/images/Logo.jpg" />
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <hr />
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>&nbsp;</td>
                                                    </tr>
                                                    <tr>
                                                        <td align="right">
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td rowspan="3" valign="top">
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <h2>Reservation Details </h2>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td>&nbsp;</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td><strong>PNR No.  <%=(routingItinerary.PNR.Split('|').Length > 1 ? routingItinerary.PNR.Split('|')[routingItinerary.Segments[count].Group] : routingItinerary.PNR)%></strong></td>
                                                                                    </tr>
                                                                                    <%if(!string.IsNullOrEmpty(routingItinerary.RoutingTripId)){ %>
                                                                                    <tr>
                                                                                        <td><strong>Routing Trip Id: <%=routingItinerary.RoutingTripId %></strong></td>
                                                                                    </tr>
                                                                                    <%} %>
                                                                                </tbody>
                                                                            </table>
                                                                            <h2>&nbsp;</h2>
                                                                        </td>
                                                                        <td align="right">
                                                                            
                                                                        </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right"><strong>Agent Name: <%=agency.Name %></strong></td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td align="right"><strong>Phone: <%=agency.Phone1 %></strong></td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <% }%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td><%if (count == 0)
                                                { %>
                                            <table width="100%" border="1" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td align="center" valign="middle"><strong>Passenger Name</strong></td>
                                                        <td align="center" valign="middle"><strong>E-Ticket Number</strong></td>
                                                        <td align="center" valign="middle"><strong>Baggage</strong></td>
                                                        <%if (routingItinerary != null && routingItinerary.FlightBookingSource == BookingSource.UAPI || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp)  %>
                                                        <%{ %>
                                                        <td align="center" valign="middle"><strong>Meal</strong></td>
                                                        <td align="center" valign="middle"><strong>Seats</strong></td>
                                                        <%} %>
                                                        <td align="center" valign="middle"><strong>Airline Ref.</strong></td>
                                                    </tr>
                                                    <%if (hdfTicketType.Value == "Individual")
                                                        { %>
                                                    <tr>
                                                        <td align="center" valign="top"><%=paxName %></td>
                                                        <td align="center" valign="top">
                                                            <%if (Request["bkg"] == null && routingTicketList.Count > 0)
                                                                { %>
                                                            <%=(routingTicketList[paxIndex].TicketNumber.Split('|').Length > 1 ? routingTicketList[paxIndex].TicketNumber.Split('|')[routingItinerary.Segments[count].Group] : routingTicketList[paxIndex].TicketNumber) %>
                                                            <%}
                                                                else
                                                                { %>
                                                            <%=(routingItinerary.PNR.Split('|').Length > 1 ? routingItinerary.PNR.Split('|')[count] : routingItinerary.PNR)%>
                                                            <%} %></td>
                                                        <td align="center" valign="top">
                                                            <%if (routingItinerary.FlightBookingSource == BookingSource.Amadeus || routingItinerary.FlightBookingSource == BookingSource.UAPI || (routingItinerary.FlightBookingSource == BookingSource.TBOAir && !(routingItinerary.IsLCC)))
    {%>
                             <%=GetBaggageForGDS(routingItinerary.FlightId,routingItinerary.Passenger[paxIndex].Type)%>
    <%}   

 else if (routingItinerary.FlightBookingSource == BookingSource.AirArabia || routingItinerary.FlightBookingSource == BookingSource.FlyDubai || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.PKFares || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp || routingItinerary.FlightBookingSource == BookingSource.Jazeera)
    {
        if (routingItinerary.Passenger[paxIndex].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(routingItinerary.Passenger[paxIndex].BaggageCode) ? "Airline Norms" : routingItinerary.Passenger[paxIndex].BaggageCode)%>
                                                     <%}
    }
    else if (routingItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl && routingItinerary.Passenger[paxIndex].Type != PassengerType.Infant)
    {%>
                                                       
                                                            <%=(string.IsNullOrEmpty(routingItinerary.Passenger[paxIndex].BaggageCode) ? "Airline Norms" : routingItinerary.Passenger[paxIndex].BaggageCode)%>
                                                        
                                                    <%}
    //Added by lokesh on 22-06-2018
    //For Amadeus Air Source
    //Display baggage information based on the pax type.
    else if (routingItinerary.FlightBookingSource == BookingSource.Amadeus && routingItinerary.Passenger[paxIndex].Type != PassengerType.Infant)
    {%>
              <%=(string.IsNullOrEmpty(routingItinerary.Passenger[paxIndex].BaggageCode) ? "Airline Norms" : routingItinerary.Passenger[paxIndex].BaggageCode)%>
    <%}

    else if (routingItinerary.FlightBookingSource == BookingSource.TBOAir && (routingItinerary.IsLCC))
    {
        if (routingItinerary.Passenger[paxIndex].Type != PassengerType.Infant)
        {
            string strBaggage = string.Empty;
            
            if (!string.IsNullOrEmpty(routingItinerary.Passenger[paxIndex].BaggageCode))
            {
                strBaggage = routingItinerary.Passenger[paxIndex].BaggageCode;
            }
            else
            {
                strBaggage = "Airline Norms";
            }
                                                           %>
                                                      <%=strBaggage%> 
                                                    <%}
    } %>
                                                            


                                                        </td>
                                                        <%if (routingItinerary.FlightBookingSource == BookingSource.UAPI || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{
                                                                string MealDesc = string.Empty;
                                                                %>
                                                        <td>
                                                             <%if (routingItinerary.Passenger[paxIndex].Type != PassengerType.Infant && !string.IsNullOrEmpty(routingItinerary.Passenger[paxIndex].MealDesc)) %>
                                                            <%{
                                                                    MealDesc = routingItinerary.Passenger[paxIndex].MealDesc;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Meals"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <td>
                                                             <%if (routingItinerary.Passenger[paxIndex].Type != PassengerType.Infant && !string.IsNullOrEmpty(routingItinerary.Passenger[paxIndex].Seat.Code)) %>
                                                            <%{
                                                                    MealDesc = routingItinerary.Passenger[paxIndex].Seat.Code;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "NoSeat"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <%} %>

                                                        <td align="center" valign="top"><%=(routingItinerary.Segments[count].AirlinePNR == null || routingItinerary.Segments[count].AirlinePNR == "" ? (routingItinerary.PNR.Split('|').Length > 1 ? routingItinerary.PNR.Split('|')[routingItinerary.Segments[count].Group] : routingItinerary.PNR) : routingItinerary.Segments[count].AirlinePNR) %></td>
                                                    </tr>
                                                    <%}
                                                        else if (hdfTicketType.Value == "Multiple")
                                                        {%>
                                                    <%for (int j = 0; j < routingItinerary.Passenger.Length; j++)
                                                        {
                                                    %>
                                                    <tr>
                                                        <td align="center" valign="top"><%=routingItinerary.Passenger[j].Title + " " + routingItinerary.Passenger[j].FirstName + " " + routingItinerary.Passenger[j].LastName%></td>
                                                        <td align="center" valign="top">
                                                            <%if (Request["bkg"] == null && routingTicketList.Count > 0 && routingTicketList.Count > j)
                                                                { %>
                                                            <%=(routingTicketList[j].TicketNumber.Split('|').Length > 1 ? routingTicketList[j].TicketNumber.Split('|')[routingItinerary.Segments[count].Group] : routingTicketList[j].TicketNumber) %>
                                                            <%}
                                                                else
                                                                { %>
                                                            <%=(routingItinerary.PNR.Split('|').Length > 1 ? routingItinerary.PNR.Split('|')[count] : routingItinerary.PNR)%>
                                                            <%} %></td>
                                                        <td align="center" valign="top">
                                                            
                                                            
   <%if (routingItinerary.FlightBookingSource == BookingSource.Amadeus || routingItinerary.FlightBookingSource == BookingSource.UAPI || (routingItinerary.FlightBookingSource == BookingSource.TBOAir && !(routingItinerary.IsLCC)))
    {%>
                             <%=GetBaggageForGDS(routingItinerary.FlightId,routingItinerary.Passenger[j].Type)%>
    <%}                                                            
 else if (routingItinerary.FlightBookingSource == BookingSource.AirArabia || routingItinerary.FlightBookingSource == BookingSource.FlyDubai || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.PKFares || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp || routingItinerary.FlightBookingSource == BookingSource.Jazeera)
    {
        if (routingItinerary.Passenger[j].Type != PassengerType.Infant)
        { %>
                                                            <%=(string.IsNullOrEmpty(routingItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : routingItinerary.Passenger[j].BaggageCode)%>
                                                     <%}
    }
    else if (routingItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl && routingItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
                                                       
                                                            <%=(string.IsNullOrEmpty(routingItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : routingItinerary.Passenger[j].BaggageCode)%>
                                                        
                                                    <%}
    //Added by lokesh on 22-06-2018
    //For Amadeus Air Source
    //Display baggage information based on the pax type.
    else if (routingItinerary.FlightBookingSource == BookingSource.Amadeus && routingItinerary.Passenger[j].Type != PassengerType.Infant)
    {%>
              <%=(string.IsNullOrEmpty(routingItinerary.Passenger[j].BaggageCode) ? "Airline Norms" : routingItinerary.Passenger[j].BaggageCode)%>
    <%}

    else if (routingItinerary.FlightBookingSource == BookingSource.TBOAir && (routingItinerary.IsLCC))
    {
        if (routingItinerary.Passenger[j].Type != PassengerType.Infant)
        {
            string strBaggage = string.Empty;
            
            if (!string.IsNullOrEmpty(routingItinerary.Passenger[j].BaggageCode))
            {
                strBaggage = routingItinerary.Passenger[j].BaggageCode;
            }
            else
            {
                strBaggage = "Airline Norms";
            }
                                                           %>
                                                      <%=strBaggage%> 
                                                    <%}
    } %> </td>


                                                        <%if (routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp) %>
                                                        <%{
                                                                string MealDesc = string.Empty;
                                                                %>
                                                        <td>
                                                             <%if (routingItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(routingItinerary.Passenger[j].MealDesc)) %>
                                                            <%{
                                                                    MealDesc = routingItinerary.Passenger[j].MealDesc;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "No Meals"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <td>
                                                             <%if (routingItinerary.Passenger[j].Type != PassengerType.Infant && !string.IsNullOrEmpty(routingItinerary.Passenger[j].Seat.Code)) %>
                                                            <%{
                                                                    MealDesc = routingItinerary.Passenger[j].Seat.Code;
                                                                    %>
                                                                    <%=MealDesc%>
                                                            <%} %>
                                                            <%else %>
                                                            <%{
                                                                    MealDesc = "NoSeat"; %>

                                                                 <%=MealDesc%>
                                                              
                                                            <%} %>
                                                        </td>
                                                        <%} %>
                                                        <td align="center" valign="top"><%=(routingItinerary.Segments[count].AirlinePNR == null || routingItinerary.Segments[count].AirlinePNR == "" ? (routingItinerary.PNR.Split('|').Length > 1 ? routingItinerary.PNR.Split('|')[routingItinerary.Segments[count].Group] : routingItinerary.PNR) : routingItinerary.Segments[count].AirlinePNR) %></td>
                                                    </tr>
                                                    <%}
                                                        }%>
                                                </tbody>
                                            </table>
                                            <% }%>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td valign="top">
                                            <table width="100%" border="1" style="border-collapse: collapse;" cellspacing="0" cellpadding="0">
                                                <tbody>
                                                    <tr>
                                                        <td>
                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                <tbody>
                                                                    <tr>
                                                                        <td style="border-right: solid 1px #ccc;" valign="top">
                                                                            <table style="margin: auto" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td style="padding-top: 5px" align="center">
                                                                                            <img src="<%=Request.Url.Scheme%>://ctb2b.cozmotravel.com/images/AirlineLogo/<%=routingAirline.AirlineCode%>.gif" alt="#" />
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center"><%routingAirline.Load(routingItinerary.Segments[count].Airline); %> <%=routingAirline.AirlineName%></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center"><strong><%=routingItinerary.Segments[count].Airline + "-" + routingItinerary.Segments[count].FlightNumber%><br />
                                                                                            <!--Displaying Code Share Info (Operating Airline name) for the Segment (shiva 21 Sep 2016) -->
                                                                                            <%try
                                                                                                {
                                                                                                    if ((!routingItinerary.IsLCC && routingItinerary.Segments[count].OperatingCarrier != null && routingItinerary.Segments[count].OperatingCarrier.Length > 0)||(routingItinerary.FlightBookingSource == BookingSource.Indigo && routingItinerary.Segments[count].OperatingCarrier != null && routingItinerary.Segments[count].OperatingCarrier.Length > 0 && routingItinerary.Segments[count].OperatingCarrier != "6E"))////Modified for Indigo CodeShare
                                                                                                    {
                                                                                                        string opCarrier = routingItinerary.Segments[count].OperatingCarrier;
                                                                                                        Airline opAirline = new Airline();
                                                                                                        if (opCarrier.Split('|').Length > 1)
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Split('|')[0]);
                                                                                                        }
                                                                                                        else
                                                                                                        {
                                                                                                            opAirline.Load(opCarrier.Substring(0, 2));
                                                                                                        } %>
                                             (Operated by <%=(opCarrier.Contains("|") ? "(" + opCarrier.Replace("|", " ").ToUpper() + ")" : "")%> <%=opAirline.AirlineName.ToUpper()%>)
                                            <%}
                                                }
                                                catch { } %></strong></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td align="center" valign="top">
                                                                            <table style="margin: auto" width="94%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td>
                                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td><i>Departure </i></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><strong><%=routingItinerary.Segments[count].Origin.CityName + "( " + routingItinerary.Segments[count].Origin.CityCode+")"%></strong></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><%=routingItinerary.Segments[count].DepartureTime.ToString("ddd")+","+routingItinerary.Segments[count].DepartureTime.ToString("dd MMM yyyy")+","+routingItinerary.Segments[count].DepartureTime.ToString("HH:mm tt")%></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><%=routingItinerary.Segments[count].Origin.AirportName %> </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Terminal <%=routingItinerary.Segments[count].DepTerminal %></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                        <td width="20"></td>
                                                                                        <td>
                                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                                <tbody>
                                                                                                    <tr>
                                                                                                        <td><i>Arrival </i></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><strong><%=routingItinerary.Segments[count].Destination.CityName + "( " + routingItinerary.Segments[count].Destination.CityCode+")"%></strong></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><%=routingItinerary.Segments[count].ArrivalTime.ToString("ddd")+","+routingItinerary.Segments[count].ArrivalTime.ToString("dd MMM yyyy")+","+routingItinerary.Segments[count].ArrivalTime.ToString("HH:mm tt")%></td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td><%=routingItinerary.Segments[count].Destination.AirportName %>  </td>
                                                                                                    </tr>
                                                                                                    <tr>
                                                                                                        <td>Terminal <%=routingItinerary.Segments[count].ArrTerminal %></td>
                                                                                                    </tr>
                                                                                                </tbody>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                        <td style="border-left: solid 1px #ccc;" valign="top">
                                                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                                <tbody>
                                                                                    <tr>
                                                                                        <td align="center">
                  <%if (routingItinerary.Segments[count].Stops == 0) %>
                    <%{ %>
                    <strong> Non-Stop </strong>
                    <%} %>
                    <%else if (routingItinerary.Segments[count].Stops == 1) %>
                    <%{ %>
                    <strong> Single Stop </strong>
                    <%} %>
                    <%else if (routingItinerary.Segments[count].Stops == 2) %>
                    <%{ %>
                    <strong> Two Stops </strong>
                    <%} %>
                    <%else if (routingItinerary.Segments[count].Stops> 2) %>
                    <%{ %>
                    <strong> Two+ Stops </strong>
                    <%} %></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">
                                                                                            <%=routingItinerary.Segments[count].Duration.Hours +"hr "+routingItinerary.Segments[count].Duration.Minutes+"m"%>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="center">Class:  <%=routingItinerary.Segments[count].CabinClass%>
                                                                                            <%if (routingItinerary.FlightBookingSource == BookingSource.FlyDubai || routingItinerary.FlightBookingSource == BookingSource.AirIndiaExpressIntl || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp || routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp || routingItinerary.FlightBookingSource == BookingSource.Jazeera)
                                                                                                {%>
                                                                                            <br />
                                                                                            <label style='padding-left: 20px'>
                                                                                                <strong>Fare Type :<br />
                                                                                                </strong><%=routingItinerary.Segments[count].SegmentFareType%></label>
                                                                                            <%} %></td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td style="color: #66ca1d" align="center"><%=routingBooking.Status.ToString()%></td>
                                                                                    </tr>
                                                                                </tbody>
                                                                            </table>
                                                                        </td>
                                                                    </tr>
                                                                </tbody>
                                                            </table>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </td>
                                    </tr>
                                    <%}%>
                                </tbody>
                            </table>
                            <table cellpadding="0" cellspacing="0" class="label">
                                <tr>
                                    <td style="width: 600px" align="right">
                                        <a style="cursor: Hand; font-weight: bold; font-size: 8pt; color: Black;" id="ancFareRouting" runat="server"
                                            onclick="return ShowHide('divFareRouting');">Hide Fare</a>
                                    </td>
                                </tr>
                            </table>                           
                            <div title="Param" id="divFareRouting" runat="server" style="font-size: 12px;">
                                <table style="font-size: 12px;" width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <%decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0, outputVAT = 0, Meal = 0, SeatPrice = 0, k3Tax = 0;
                                                if (Request["bkg"] == null) // For Ticketing
                                                {
                                                    int paxId = -1;
                                                    if (routingTicketList.Count > 0 && Request["paxId"] == null)
                                                    {
                                                        List<TaxBreakup> allPaxTaxBreakups = new List<TaxBreakup>();
                                                        routingTicketList.ForEach(t =>  t.TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); } ));

                                                        switch(routingItinerary.FlightBookingSource)
                                                        {
                                                            case BookingSource.GoAir:
                                                            case BookingSource.GoAirCorp:
                                                                k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("ST") || t.TaxCode.Contains("CT") || t.TaxCode.Contains("IT") ? t.TaxValue : 0);
                                                                break;
                                                            case BookingSource.UAPI:
                                                                k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                                                break;
                                                            case BookingSource.SpiceJet:
                                                            case BookingSource.SpiceJetCorp:
                                                            case BookingSource.Indigo:
                                                            case BookingSource.IndigoCorp:
                                                                k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("GST") ? t.TaxValue : 0);
                                                                break;
                                                            default:
                                                                k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                                                break;
                                                        }
                                                        k3Tax = Convert.ToDecimal(k3Tax.ToString("N" + agency.DecimalValue));
                                                        if(string.IsNullOrEmpty(routingItinerary.RoutingTripId))
                                                        {
                                                            k3Tax = 0;
                                                        }
                                                        if (Request["paxId"] != null)
                                                        {
                                                            paxId = Convert.ToInt32(Request["paxId"]);
                                                        }
                                                        if(paxId >-1 && routingItinerary.Passenger[paxId].Type == PassengerType.Infant && (routingItinerary.FlightBookingSource!=BookingSource.TBOAir && routingItinerary.FlightBookingSource!=BookingSource.UAPI ))
                                                        {
                                                            k3Tax = 0;
                                                        }
                                                        //if(paxId>=0)
                                                        //{
                                                        //    k3Tax = k3Tax / routingItinerary.Passenger.Length;
                                                        //}
                                                        for (int k = 0; k < routingTicketList.Count; k++)
                                                        {
                                                            AirFare += routingTicketList[k].Price.PublishedFare + routingTicketList[k].Price.HandlingFeeAmount;
                                                            Taxes += routingTicketList[k].Price.Tax + routingTicketList[k].Price.Markup;
                                                            if (routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                                                            {
                                                                Taxes += routingTicketList[k].Price.OtherCharges + routingTicketList[k].Price.SServiceFee + routingTicketList[k].Price.AdditionalTxnFee + routingTicketList[k].Price.TransactionFee;
                                                            }
                                                            if (routingItinerary.FlightBookingSource == BookingSource.OffLine)
                                                            {
                                                                Taxes -= (routingTicketList[k].Price.AdditionalTxnFee + routingTicketList[k].Price.TransactionFee);
                                                            }
                                                            Baggage += routingTicketList[k].Price.BaggageCharge;
                                                            Meal += routingTicketList[k].Price.MealCharge;
                                                            SeatPrice += routingTicketList[k].Price.SeatPrice;
                                                            MarkUp += routingTicketList[k].Price.Markup;
                                                            Discount += routingTicketList[k].Price.Discount;
                                                            AsvAmount += routingTicketList[k].Price.AsvAmount;
                                                            outputVAT += routingTicketList[k].Price.OutputVATAmount;
                                                        }

                                                        if (routingTicketList[0].Price.AsvElement == "BF")
                                                        {
                                                            AirFare += AsvAmount;
                                                        }
                                                        else if (routingTicketList[0].Price.AsvElement == "TF")
                                                        {
                                                            Taxes += AsvAmount;
                                                        }
                                                    }
                                                    else
                                                    {


                                                        if (Request["paxId"] != null)
                                                        {
                                                            paxId = Convert.ToInt32(Request["paxId"]);
                                                        }
                                                        if(routingTicketList.Count>0)
                                                        {

                                                            List<TaxBreakup> allPaxTaxBreakups = new List<TaxBreakup>();
                                                            if (paxId == -1)
                                                                routingTicketList.ForEach(t => t.TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); }));
                                                            else
                                                                routingTicketList[paxId].TaxBreakup.ForEach(tb => { TaxBreakup tax = new TaxBreakup(); tax.TaxCode = tb.Key; tax.TaxValue = tb.Value; allPaxTaxBreakups.Add(tax); } );

                                                            switch(routingItinerary.FlightBookingSource)
                                                            {
                                                                case BookingSource.GoAir:
                                                                case BookingSource.GoAirCorp:
                                                                    k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("ST") || t.TaxCode.Contains("CT") || t.TaxCode.Contains("IT") ? t.TaxValue : 0);
                                                                    break;
                                                                case BookingSource.UAPI:
                                                                    k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                                                    break;
                                                                case BookingSource.SpiceJet:
                                                                case BookingSource.SpiceJetCorp:
                                                                case BookingSource.Indigo:
                                                                case BookingSource.IndigoCorp:
                                                                    k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("GST") ? t.TaxValue : 0);
                                                                    break;
                                                                default:
                                                                    k3Tax += allPaxTaxBreakups.Sum(t => t.TaxValue = t.TaxCode.Contains("K3") ? t.TaxValue : 0);
                                                                    break;
                                                            }
                                                            k3Tax = Convert.ToDecimal(k3Tax.ToString("N" + agency.DecimalValue));
                                                            if(paxId >-1 && routingItinerary.Passenger[paxId].Type == PassengerType.Infant && (routingItinerary.FlightBookingSource!=BookingSource.TBOAir && routingItinerary.FlightBookingSource!=BookingSource.UAPI))
                                                            {
                                                                k3Tax = 0;
                                                            }
                                                        }

                                                        if(string.IsNullOrEmpty(routingItinerary.RoutingTripId))
                                                        {
                                                            k3Tax = 0;
                                                        }
                                                        for (int i = 0; i < routingItinerary.Passenger.Length; i++)
                                                        {
                                                            if (Request["paxId"] != null && i == paxId) // Induvidual
                                                            {
                                                                FlightPassenger pax = routingItinerary.Passenger[i];
                                                                AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;
                                                                Taxes += pax.Price.Tax + pax.Price.Markup;
                                                                if (routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                                                                {
                                                                    Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                                                                }
                                                                if (routingItinerary.FlightBookingSource == BookingSource.OffLine)
                                                                {
                                                                    Taxes += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                                                                }
                                                                Baggage += pax.Price.BaggageCharge;
                                                                Meal += pax.Price.MealCharge;
                                                                SeatPrice += pax.Price.SeatPrice;
                                                                MarkUp += pax.Price.Markup;
                                                                Discount += pax.Price.Discount;
                                                                AsvAmount += pax.Price.AsvAmount;
                                                                outputVAT += pax.Price.OutputVATAmount;
                                                                break;
                                                            }
                                                            if (Request["paxId"] == null)// For all
                                                            {
                                                                FlightPassenger pax = routingItinerary.Passenger[i];
                                                                AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;

                                                                if (routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                                                                {
                                                                    Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                                                                }
                                                                if (routingItinerary.FlightBookingSource == BookingSource.OffLine)
                                                                {
                                                                    Taxes += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                                                                }
                                                                else
                                                                    Taxes += pax.Price.Tax + pax.Price.Markup;

                                                                Baggage += pax.Price.BaggageCharge;
                                                                Meal += pax.Price.MealCharge;
                                                                SeatPrice += pax.Price.SeatPrice;
                                                                MarkUp += pax.Price.Markup;
                                                                Discount += pax.Price.Discount;
                                                                AsvAmount += pax.Price.AsvAmount;
                                                                outputVAT += pax.Price.OutputVATAmount;
                                                            }
                                                        }

                                                        if (routingItinerary.Passenger[0].Price.AsvElement == "BF")
                                                        {
                                                            AirFare += AsvAmount;
                                                        }
                                                        else if (routingItinerary.Passenger[0].Price.AsvElement == "TF")
                                                        {
                                                            Taxes += AsvAmount;
                                                        }
                                                    }
                                                }
                                                else // For Hold Itinerary View
                                                {
                                                    int paxId = 0;

                                                    if (Request["paxId"] != null)
                                                    {
                                                        paxId = Convert.ToInt32(Request["paxId"]);
                                                    }

                                                    for (int i = 0; i < routingItinerary.Passenger.Length; i++)
                                                    {
                                                        if (Request["paxId"] != null && i == paxId) // Induvidual
                                                        {
                                                            FlightPassenger pax = routingItinerary.Passenger[i];
                                                            AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;
                                                            Taxes += pax.Price.Tax + pax.Price.Markup;
                                                            if (routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                                                            {
                                                                Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                                                            }
                                                            if (routingItinerary.FlightBookingSource == BookingSource.OffLine)
                                                            {
                                                                Taxes += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                                                            }
                                                            Baggage += pax.Price.BaggageCharge;
                                                            Meal += pax.Price.MealCharge;
                                                            SeatPrice += pax.Price.SeatPrice;
                                                            MarkUp += pax.Price.Markup;
                                                            Discount += pax.Price.Discount;
                                                            AsvAmount += pax.Price.AsvAmount;
                                                            outputVAT += pax.Price.OutputVATAmount;
                                                            break;
                                                        }
                                                        if (Request["paxId"] == null)// For all
                                                        {
                                                            FlightPassenger pax = routingItinerary.Passenger[i];
                                                            AirFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;

                                                            if (routingItinerary.FlightBookingSource == BookingSource.TBOAir)
                                                            {
                                                                Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                                                            }
                                                            if (routingItinerary.FlightBookingSource == BookingSource.OffLine)
                                                            {
                                                                Taxes += (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                                                            }
                                                            else
                                                                Taxes += pax.Price.Tax + pax.Price.Markup;

                                                            Baggage += pax.Price.BaggageCharge;
                                                            Meal += pax.Price.MealCharge;
                                                            SeatPrice += pax.Price.SeatPrice;
                                                            MarkUp += pax.Price.Markup;
                                                            Discount += pax.Price.Discount;
                                                            AsvAmount += pax.Price.AsvAmount;
                                                            outputVAT += pax.Price.OutputVATAmount;
                                                        }
                                                    }

                                                    if (routingItinerary.Passenger[0].Price.AsvElement == "BF")
                                                    {
                                                        AirFare += AsvAmount;
                                                    }
                                                    else if (routingItinerary.Passenger[0].Price.AsvElement == "TF")
                                                    {
                                                        Taxes += AsvAmount;
                                                    }
                                                }
                                            %>
                                            <% //if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.BaseAgent)
                                               // { %>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td>
                                                        <table width="100%" cellspacing="0" cellpadding="0" border="0">
                                                            <tbody>
                                                                <tr>
                                                                    <td width="41%"></td>
                                                                    <td>
                                                                        <table width="50%" class="">
                                                                            <tbody>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Air Fare</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=AirFare.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Taxes & Fees</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=(Taxes-k3Tax).ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <%if (!string.IsNullOrEmpty(routingItinerary.RoutingTripId))
                                                                                    { %>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>K3 Tax</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=(k3Tax).ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>
                                                                                <%if (routingItinerary.IsLCC)
                                                                                    { %>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Baggage Fare</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=Baggage.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>

                                                                                <%if (routingItinerary != null && (routingItinerary.FlightBookingSource == BookingSource.UAPI || routingItinerary.FlightBookingSource == BookingSource.Indigo || routingItinerary.FlightBookingSource == BookingSource.SpiceJet || routingItinerary.FlightBookingSource == BookingSource.SpiceJetCorp || routingItinerary.FlightBookingSource == BookingSource.IndigoCorp|| routingItinerary.FlightBookingSource == BookingSource.GoAir || routingItinerary.FlightBookingSource == BookingSource.GoAirCorp)) %>
                                                                                <%{ %>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Meal Price</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=Meal.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Seat Price</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=SeatPrice.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>

                                                                                <%if (Discount > 0)
                                                                                    { %>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Discount</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                       -<%=Discount.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <%} %>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <%CT.TicketReceipt.BusinessLayer.LocationMaster location = new CT.TicketReceipt.BusinessLayer.LocationMaster(routingItinerary.LocationId);
                                                                                             if (location.CountryCode == "IN")
                                                                                            {%>
                                                                                        <b>GST</b>
                                                                                        <%}    else
    { %>
                                                                                        <b>VAT</b>
                                                                                        <%} %>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%=outputVAT.ToString("N" + agency.DecimalValue) %> <%=agency.AgentCurrency%>
                                                                                    </td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td colspan="3"> <hr /></td>
                                                                                </tr>
                                                                                <tr>
                                                                                    <td style='text-align: left!important;'>
                                                                                        <b>Total Air Fare</b>
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>:
                                                                                    </td>
                                                                                    <td style='text-align: right!important;'>
                                                                                        <%if (routingItinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                                                                                            { %>
                                                                                        <b><%=Math.Ceiling((AirFare + Taxes + Baggage + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%}
                                                                                         else if (routingItinerary.FlightBookingSource == BookingSource.OffLine) 
                                                                                            { %>
                                                                                        <b><%=((AirFare + Taxes + outputVAT ) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%}

                                                                                            else
                                                                                            { %>
                                                                                        <b><%=((AirFare + Taxes + Baggage + Meal + SeatPrice + outputVAT) - Discount).ToString("N" + agency.DecimalValue)%> <%=agency.AgentCurrency%></b>
                                                                                        <%} %>
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                            <%//}
                                              %>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </div>
                            <%if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentType == CT.TicketReceipt.BusinessLayer.AgentType.Agent)
                                { %>
                            <script type="text/javascript">
                                 ShowHide("divFare");
                            </script> 
                            <%} %>
                            <table>
                                <tbody>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;</td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td><strong>Important Information </strong></td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td height="10">
                                            <hr />
                                        </td>
                                    </tr>

                                    <tr style="display: none;">
                                        <td>
                                            <ul>
                                                <li>Use your Trip ID for all communication with Cleartrip about this booking </li>
                                                <li>Check-in counters for International flights close 75 minutes before departure</li>
                                                <li>Your carry-on baggage shouldn't weigh more than 7kgs</li>
                                                <li>Carry photo identification, you will need it as proof of identity while checking-in</li>
                                                <li>Kindly ensure that you have the relevant visa, immigration clearance and travel with a passport, with a validity of at least 6 months.</li>
                                                <li>For hassle free refund processing, cancel/amend your tickets with Cleartrip Customer Care instead of doing so directly with Airline.</li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td>
                                            <img src="<%=Request.Url.Scheme%>://ctb2b.cozmotravel.com/images/goapic.jpg" width="728" height="90" alt="#" /></td>
                                    </tr>
                                </tbody>
                            </table>

                                    <!-- Added by lokesh on 7-June-2018-->
        <!-- Need to display the below note in email for PK Fares -->
         <%if (routingItinerary != null && routingItinerary.FlightBookingSource == BookingSource.PKFares) %>
         <%{ %>
         <table width='100%' border='0' cellspacing='0' cellpadding='0'>
            <tr>
                <td height='60' valign='top'>
                <span style="color:red">Note: This is just an itinerary. Ticket copy should not be handed over to customer unless ticket number is shown on the ticket.</span>
                    
                </td>
                <td>
                </td>
                <td>
                </td>
            </tr>
        </table>
        <%} %>



                        </div>
                        <%} %>

                        <%if (flightItinerary.FlightBookingSource == BookingSource.FlightInventory) %>
                        <% { %>
                            
                        <%  GetConfigData("FI_Email_Alert"); %> 
                        <div style='color:red'>Note: <asp:Label ID="lblConfigData" runat="server"  ></asp:Label>
                        </div>
      
                        <%} %> 
                    </div>                      
                    
                </asp:View>
                <asp:View ID="ErrorView" runat="server">
                    <div class="width-720 margin-left-30" style="text-align: center">
                        <br />
                        <br />
                        <br />
                        <span>
                            <asp:Label ID="ErrorMessage" runat="server"></asp:Label>
                        </span>
                    </div>
                </asp:View>
            </asp:MultiView>
            
            
       </center>
    </form>
</body>
</html>
