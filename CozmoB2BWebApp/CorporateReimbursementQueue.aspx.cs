﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.Web.UI.Controls;
using CT.TicketReceipt.Common;
using System.Collections.Generic;
public partial class CorporateReimbursementQueueUI : CT.Core.ParentPage
{
    private string REIM_SEARCH_SESSION = "_ReimSearchList";
    //protected List<Corp_Profile_ExpenseRequest_List_Email> _expenseRequests = new List<Corp_Profile_ExpenseRequest_List_Email>();
    protected CorporateProfileExpenseDetails detail;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                if (!Page.IsPostBack)
                {
                    IntialiseControls();

                }
            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateReimbursementQueue) Page Load Event Error: " + ex.Message, "0");
        }
    }
    private void bindExpenseTypes()
    {
        try
        {
            ddlExpType.DataSource = PolicyExpenseDetails.GetAllExpenseTypes('A', "ET");
            ddlExpType.DataValueField = "SetupId";
            ddlExpType.DataTextField = "Name";
            ddlExpType.AppendDataBoundItems = true;
            ddlExpType.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    private void IntialiseControls()
    {
        try
        {
            dcReimFromDate.Value = DateTime.Now;
            dcReimToDate.Value = DateTime.Now;

            bindEmployeesList();
            bindExpenseTypes();
            bindSearch(DateTime.Now.AddMonths(-1), DateTime.Now, 0, string.Empty, string.Empty);
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), 0, string.Empty, string.Empty);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void bindEmployeesList()
    {
        ddlEmployee.DataSource = CorporateProfile.GetCorpProfilesList((int)Settings.LoginInfo.AgentId);
        ddlEmployee.DataTextField = "Name";
        ddlEmployee.DataValueField = "ProfileId";
        ddlEmployee.DataBind();
        ddlEmployee.Items.Insert(0, new ListItem("--Select Employee--", "0"));
    }
    protected void gvSearch_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            var ddl = e.Row.FindControl("ddlCurrency") as DropDownList;
            if (ddl != null)
            {
                ListItem item = new ListItem();
                item.Text = Settings.LoginInfo.Currency;
                item.Value = Settings.LoginInfo.Currency;
                ddl.Items.Add(item);

            }
        }
    }

    private void isTrackingEmpty()
    {
        try
        {
            bool _selected = false;
            foreach (GridViewRow gvRow in gvSearch.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");


                if (chkSelect.Checked)
                {
                    _selected = true;
                    return;
                }

            }
            string strMsg = "Please Select atleast one Item ! ";
            if (!_selected)
                Utility.Alert(this.Page, strMsg);
        }
        catch { throw; }
    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateReimbursementQueue)gvSearch_PageIndexChanging method .Error:" + ex.ToString(), "0");

        }

    }

    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={

                                         {"HTtxtDATE", "DATE" }
                                        ,{"HTtxtEMPLOYEEID", "EMP_ID" } 
                                        ,{"HTtxtEMPLOYEENAME", "EMP_NAME" }
                                         ,{"HTtxtEXPREF",  "EXP_REF"}
                                         ,{"HTtxtEXPCAT",  "EXP_CATEGORY"}
                                         ,{"HTtxtEXPTYPE", "EXP_TYPE"}
                                          ,{"HTtxtAMOUNT", "AMOUNT"}

                                          ,{"HTtxtTRAMOUNT", "TotalReimbursementAmount"}
                                          ,{"HTtxtBALANCE", "Balance"}

                                        ,
                                         };
        CommonGrid g = new CommonGrid();
        g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);

    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

            int employeeProfileId = 0;
            if (ddlEmployee.SelectedIndex > 0)
            {
                employeeProfileId = Convert.ToInt32(ddlEmployee.SelectedValue);
            }
            string expCategory = string.Empty;
            if (ddlExpCategory.SelectedIndex > 0)
            {
                expCategory = ddlExpCategory.SelectedValue;
            }
            string expenseType = string.Empty;
            if (ddlExpType.SelectedIndex > 0)
            {
                expenseType = ddlExpType.SelectedValue;
            }

            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), employeeProfileId, expCategory, expenseType);



        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateExpenseApprovals btnSearchApprovals Error: " + ex.Message, "0");
        }
    }

    private void bindSearch(DateTime fromDate, DateTime toDate, int profileId, string expCategory, string expType)
    {
        try
        {
            DataTable dt = CorporateProfileExpenseDetails.GetCorpReimbursementList(profileId, fromDate, toDate, expCategory, expType);
            if (dt != null && dt.Rows.Count > 0)
            {
                SearchList = dt;
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvSearch, dt);
                //Label lblMasterError = (Label)this.Master.FindControl("lblError");
                //lblMasterError.Visible = false;
                gvSearch.Visible = true;
            }
            else
            {
                //Label lblMasterError = (Label)this.Master.FindControl("lblError");
                //lblMasterError.Visible = true;
                //lblMasterError.Text = "No Records Found!";
                gvSearch.Visible = false;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private DataTable SearchList
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["EXP_DETAIL_ID"] };
            Session[REIM_SEARCH_SESSION] = value;
        }
    }

    protected void ITchkSelect_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
            CheckBox chkSelectAll = (CheckBox)gvHdrRow.FindControl("HTchkSelectAll");
            foreach (GridViewRow gvRow in gvSearch.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                chkSelect.Checked = chkSelectAll.Checked;
            }
        }
        catch (Exception ex)
        {

            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnPay_Click(object sender, EventArgs e)
    {
        try
        {

            isTrackingEmpty();
            foreach (GridViewRow gvRow in gvSearch.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                DropDownList ddl = (DropDownList)gvRow.FindControl("ddlCurrency");
                TextBox txt = (TextBox)gvRow.FindControl("txtReimAmount");
                HiddenField hdfExpId = (HiddenField)gvRow.FindControl("IThdfEXPDETAIL_ID");

                HiddenField hdfClaimAmount = (HiddenField)gvRow.FindControl("hdfClaimAmount");
                if (chkSelect.Checked && txt.Text.Length > 0)
                {
                    Corp_Profile_Expense_Reimbursement_Detail detail = new Corp_Profile_Expense_Reimbursement_Detail();
                    detail.ClaimAmount = Convert.ToDecimal(hdfClaimAmount.Value);
                    detail.ExpDetailId = Convert.ToInt32(hdfExpId.Value);
                    detail.Currency = ddl.SelectedValue;
                    detail.ReimbursementAmount = Convert.ToDecimal(txt.Text);
                    detail.Status = "A";
                    detail.CreatedBy = (int)Settings.LoginInfo.UserID;
                    detail.Save();

                    Label lblMasterError = (Label)this.Master.FindControl("lblError");
                    lblMasterError.Visible = true;
                    lblMasterError.Text = "Details updated Successfully !";
                    //As this is reimbursement 
                    //So email will be triggered to only to the particular with reimbursement details.
                    try
                    {
                        SendEmail("E", Convert.ToInt32(hdfExpId.Value));
                    }
                    catch { }


                }
            }
            IntialiseControls();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateReimbursementQueue)btnPay_Click Event .Error:" + ex.ToString(), "0");

        }
    }

    protected string SendEmail(string toEmpOrApp, int expDetailId)
    {
        detail = new CorporateProfileExpenseDetails(expDetailId);
        string myPageHTML = string.Empty;
        try
        {
            List<string> toArray = new System.Collections.Generic.List<string>();
            if (!string.IsNullOrEmpty(detail.EmpEmail))
            {
                toArray.Add(detail.EmpEmail);
            }
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            EmailDivEmployee.RenderControl(htw);
            myPageHTML = sw.ToString();
            myPageHTML = myPageHTML.Replace("none", "block");
            string subject = "Reimbursement Notification";
            if (toArray != null && toArray.Count > 0)
            {
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateCreateExpenseReport.aspx)Failed to Send Email For Employee and Approvers: Reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        return myPageHTML;
    }



}
