﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

public partial class ImportantMessages :CT.Core.ParentPage// System.Web.UI.Page
{
    private string IMPMESSAGE_SESSION = "_impMessageSession";
    private string IMPMESSAGE_SEARCH_SESSION ="_impMessageSearchSession";
    protected void Page_Load(object sender, EventArgs e)
    {

        this.Master.PageRole = true;
        try
        {
            if(!IsPostBack)
            {
                 InitializeControls();
            }
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
    private void InitializeControls()
    {
        try
        {
            BingAgents();
        }
        catch
        {
            throw;
        }
    }
    private IMPMessages CurrentObject
    {
        get
        {
            return (IMPMessages)Session[IMPMESSAGE_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(IMPMESSAGE_SESSION);
            }
            else
            {
                Session[IMPMESSAGE_SESSION] = value;
            }
        }
    }
    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[IMPMESSAGE_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["msg_id"] };
            Session[IMPMESSAGE_SEARCH_SESSION] = value;
        }
    }
    private void BingAgents()
    {
        try
        {
            ddlAgent.DataSource = AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataValueField = "AGENT_ID";
            ddlAgent.DataTextField = "AGENT_NAME";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "0"));
            ddlAgent.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    private void Save()
    {

        try
        {
            IMPMessages objMessage;
            if (CurrentObject == null)
            {
                objMessage = new IMPMessages();
            }
            else
            {
                objMessage = CurrentObject;
            }
            objMessage.AgencyId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            objMessage.Subject = Convert.ToString(txtSubject.Text);
            objMessage.Message = Convert.ToString(txtMessage.Text);
            if (rbtnReadStatus.SelectedItem.Text == "Yes")
            {
                objMessage.ReadStatus = rbtnReadStatus.SelectedItem.Value;
            }
            else
            {
                objMessage.ReadStatus = rbtnReadStatus.SelectedItem.Value;
            }
            objMessage.Status = Settings.ACTIVE;
            objMessage.CreatedBy = Settings.LoginInfo.UserID;
            objMessage.Save();
            lblSuccessMsg.Visible = true;
            if (CurrentObject == null)
            {
                lblSuccessMsg.Text = "Message Saved successfully";
            }
            else
            {
                lblSuccessMsg.Text = "Message Updated Successfully";
            }
            Clear();
        }

        catch
        { throw; }
    }
    private void Clear()
    {
        try
        {
            txtSubject.Text = string.Empty;
            txtMessage.Text = string.Empty;
            ddlAgent.SelectedIndex = -1;
            rbtnReadStatus.SelectedIndex = 0;
            btnSave.Text = "Save";
            CurrentObject = null;
        }
        catch { throw; }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            Clear();
            bindSearch();
        }
        catch { throw; }
    }

    private void bindSearch()
    {
        try
        {
            DataTable dt = IMPMessages.GetList(ListStatus.Long, RecordStatus.All);
            SearchList = dt;
            gvSearch.DataSource = SearchList;
            gvSearch.DataBind();
        }
        catch { throw; }
    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            gvSearch.DataSource = SearchList;
            gvSearch.DataBind();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            long msgId = Convert.ToInt64(gvSearch.SelectedValue);
            Edit(msgId);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }

    }
    private void Edit(long id)
    {
        try
        {
            IMPMessages objMessage = new IMPMessages(id);
            CurrentObject = objMessage;
            ddlAgent.SelectedValue = Convert.ToString(objMessage.AgencyId);
            txtSubject.Text = Convert.ToString(objMessage.Subject);
            txtMessage.Text = Convert.ToString(objMessage.Message);
            rbtnReadStatus.SelectedValue = Convert.ToString(CurrentObject.ReadStatus);
            btnSave.Text = "Update";
        }
        catch
        {
            throw;
        }
    }
}
