using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using CT.Core;

public partial class StaticPackagesGUI:CT.Core.ParentPage// System.Web.UI.Page
{
    #region Variables
    protected string imageServerPath;
    protected string Destination = "";
    protected string ThemeId;
    protected decimal startPrice;
    protected decimal endPrice;
    protected string tourType;
    HolidayPackage packageList = new HolidayPackage();
    PagedDataSource pagedData = new PagedDataSource();
    #endregion

    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            imageServerPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["PackageImagesFolder"]);
            
            if (!IsPostBack)
            {
                if (Session["packageList"] != null)
                {
                    packageList = Session["packageList"] as HolidayPackage;
                    tourType = packageList.Classification;
                    if (packageList.StartRate != 0)
                    {
                        startPrice = packageList.StartRate;
                    }
                    if (packageList.EndRate != 0)
                    {
                        endPrice = packageList.EndRate;
                    }
                    if (packageList.City != "Others")
                    {
                        Destination = packageList.City;
                    }
                    else
                    {
                        Destination = packageList.PackageSearchCity;
                    }
                    ThemeId = packageList.ThemeName;

                    HolidayPackage.AgentCurrency = Settings.LoginInfo.Currency;
                    HolidayPackage.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                    //DataTable dtHolidayPackages = HolidayPackage.GetHolidayPackage(Destination, ThemeId, Settings.LoginInfo.AgentId, tourType, startPrice, endPrice);
                    DataTable dtHolidayPackages = HolidayPackage.GetHolidayPackagesWithCurrency(Destination, ThemeId, Settings.LoginInfo.AgentId, tourType, startPrice, endPrice);
                    BindHolidayPackage(dtHolidayPackages);

                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "staticPAckage page " + ex.Message, "0"); 
        }
    }

    private void BindHolidayPackage(DataTable dtHolidayPackages)
    {
        if (dtHolidayPackages.Rows.Count > 0)
        {
            dlPackages.DataSource = dtHolidayPackages;
            ViewState["Package"] = dtHolidayPackages;
            dlPackages.DataBind();
            doPaging();
        }
        else
        {
            dlPackages.Visible = false;
            btnPrev.Visible = false;
            btnFirst.Visible = false;
            btnLast.Visible = false;
            btnNext.Visible = false;
            lblMessage.Visible = true;
            lblMessage.Text = "No Results Found.";
        }
    }

    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }
    void doPaging()
    {
        DataTable pdt = (DataTable)ViewState["Package"];
        if (pdt != null && pdt.Rows.Count > 0)
        {
            DataTable dt = (DataTable)ViewState["Package"];
            pagedData.DataSource = dt.DefaultView;
        }
        pagedData.AllowPaging = true;
        pagedData.PageSize = 20;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        dlPackages.DataSource = pagedData;
        dlPackages.DataBind();
    }
    protected void dlPackages_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        //dlPackages.Visible = true;
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            DataRowView dr = e.Item.DataItem as DataRowView;
            Label lblDays = (Label)e.Item.FindControl("lblDays");
            lblDays.Text = (Convert.ToInt32(dr["Nights"]) + 1).ToString();

            #region Description
            Label lblDesc = e.Item.FindControl("lblDescription") as Label;

            string _return = string.Empty;
            if (dr["Description"] != null)
            {
                string[] Words = dr["Description"].ToString().Split(' ');

                if (Words.Length <= 23)
                {
                    _return = dr["Description"].ToString();
                }
                else
                {
                    for (int i = 0; i < 23; i++)
                    {
                        _return += Words.GetValue(i).ToString() + " ";
                    }
                    _return += "...";
                }
            }
            lblDesc.Text = _return;
            #endregion

            Image img = e.Item.FindControl("imglogo") as Image;
            img.ImageUrl = imageServerPath + dr["ImagePath"].ToString();
        }
    }

   protected void dlPackages_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "View")
            {
                string PackageId = dlPackages.DataKeys[e.Item.ItemIndex].ToString();
                Response.Redirect("PackageDetails.aspx?packageId=" + PackageId, false);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "PackageDetails page " + ex.Message, "0"); 
        }
   }

    

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }


    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }

    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }
 
}
