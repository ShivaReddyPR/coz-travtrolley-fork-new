﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using CT.Roster;

public partial class ROSVehicleMasterGUI : CT.Core.ParentPage
{
    string ROSVEH_SEARCH_SESSION = "_rosVehMasterSearchList";
    string ROSVEH_SESSION = "_rosVehMaster";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            if (!IsPostBack)
            {
               
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
               
            }
           
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    #region Session Properties
    private ROSVehicleMaster CurrentObject
    {
        get
        {
            return (ROSVehicleMaster)Session[ROSVEH_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(ROSVEH_SESSION);
            }
            else
            {
                Session[ROSVEH_SESSION] = value;
            }

        }
    }
    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[ROSVEH_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ve_id"] };

            Session[ROSVEH_SEARCH_SESSION] = value;
        }
    }
    #endregion
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void Save()
    {
        try
        {
            ROSVehicleMaster rosVehMaster;
            if (CurrentObject == null)
            {
                rosVehMaster = new ROSVehicleMaster();
            }
            else
            {
                rosVehMaster = CurrentObject;
            }

            rosVehMaster.VehCode = txtVehicleCode.Text;
            rosVehMaster.VehName = txtVehicleName.Text;
            rosVehMaster.VehNumber = txtVehicleNumber.Text;
            rosVehMaster.VehCreatedBy = Settings.LoginInfo.AgentId;


            rosVehMaster.Save();

            lblMessage.Visible = true;
            lblMessage.Text = Formatter.ToMessage(("Transaction For "), txtVehicleCode.Text, (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated)) + " Successfully.";
            Clear();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            lblMessage.Visible = false;
            Clear();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void Clear()
    {

        try
        {
            txtVehicleCode.Text = string.Empty;
            txtVehicleName.Text = string.Empty;
            txtVehicleNumber.Text= string.Empty;
            
            CurrentObject = null;
            btnSave.Text = "Save";
            btnClear.Text = "Clear";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }

    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            lblMessage.Visible = false;
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void bindSearch()
    {
        try
        {
            DataTable dt = ROSVehicleMaster.GetList(RecordStatus.Activated, ListStatus.Long);
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;

        }
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }

    }

    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{"HTtxtVehCode", "ve_code" }
                                             ,{ "HTtxtVehName", "ve_name" }
                                             ,{ "HTtxtVehNumber", "ve_number" }
                                              
                                         };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }
    }

    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            int vehId = Utility.ToInteger(gvSearch.SelectedValue);
            Edit(vehId);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void Edit(int vehId)
    {
        try
        {
            lblMessage.Visible = false;

            ROSVehicleMaster getVehMasterData = new ROSVehicleMaster(vehId);
            CurrentObject = getVehMasterData;

            txtVehicleCode.Text = getVehMasterData.VehCode;
            txtVehicleName.Text = getVehMasterData.VehName;
            txtVehicleNumber.Text = getVehMasterData.VehNumber;

           
            btnSave.Text = "Update";
            btnClear.Text = "Cancel";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }

    }

}
