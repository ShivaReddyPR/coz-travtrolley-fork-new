﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

namespace CozmoB2BWebApp
{
    public partial class HotelInvoiceDocuments : CT.Core.ParentPage
    {
        protected string id;

        protected void Page_Load(object sender, EventArgs e)
        {

            id = GenericStatic.GetSetPageParams("HotelInvoiceDocuments", "", "get");

            if (Settings.LoginInfo != null)
            {
                //if (Request.QueryString["id"] != null)
                if (id != "")
                {
                    BindGSTDocuments();
                }

            }
            else
            {
                pnlDragandDrop.Visible = false;
                lblMessage.Text = "Session Expired!.";
            }
        }

        protected void btnUploadFiles_Click(object sender, EventArgs e)
        {
            if (Settings.LoginInfo != null)
            {
                List<HttpPostedFile> files = new List<HttpPostedFile>();
                //if (Session["GSTFiles"] != null && Request.QueryString["id"] != null)
                if (Session["GSTFiles"] != null && id != "")
                {
                    try
                    {
                        files = Session["GSTFiles"] as List<HttpPostedFile>;
                        string gstFolderPath = (ConfigurationManager.AppSettings["GSTFolderPath"] != null ? Server.MapPath(ConfigurationManager.AppSettings["GSTFolderPath"]) : Server.MapPath(@"\upload\GSTDocs\"));
                        //gstFolderPath = Path.Combine(gstFolderPath, "HL_" + Request.QueryString["id"]);
                        gstFolderPath = Path.Combine(gstFolderPath, "HL_" + id);
                        if (!Directory.Exists(gstFolderPath))
                        {
                            Directory.CreateDirectory(gstFolderPath);
                        }

                        foreach (HttpPostedFile file in files)
                        {
                            string filePath = Path.Combine(gstFolderPath, file.FileName);
                            file.SaveAs(filePath);
                        }
                        Session["GSTFiles"] = null;
                        BindGSTDocuments();

                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, 1, "(GSTDocs)Failed to Upload GST Docs. Reason: " + ex.ToString(), Request["REMOTE_ADDR"]);
                    }
                    finally
                    {
                        Session["GSTFiles"] = null;
                        BindGSTDocuments();
                    }
                }
            }
            else
            {
                pnlDragandDrop.Visible = false;
                lblMessage.Text = "Session Expired!.";
            }
        }

        void BindGSTDocuments()
        {
            string gstFolderPath = (ConfigurationManager.AppSettings["GSTFolderPath"] != null ? Server.MapPath(ConfigurationManager.AppSettings["GSTFolderPath"]) : Server.MapPath(@"\upload\GSTDocs\"));
            //gstFolderPath = Path.Combine(gstFolderPath, "HL_" + Request.QueryString["id"]);
            gstFolderPath = Path.Combine(gstFolderPath, "HL_" + id);

            if (Directory.Exists(gstFolderPath))
            {
                DirectoryInfo directoryInfo = new DirectoryInfo(gstFolderPath);
                FileInfo[] fileList = directoryInfo.GetFiles().OrderBy(p => p.CreationTime).ToArray();
                pnlUploadedFiles.Controls.Clear();

                HtmlGenericControl lblHeading = new HtmlGenericControl("div");
                lblHeading.Attributes.Add("class", "ns-h3");
                HtmlGenericControl span = new HtmlGenericControl("span");
                span.InnerText = "View Files";
                lblHeading.Controls.Add(span);
                pnlUploadedFiles.Controls.Add(lblHeading);

                if (fileList.Length > 0)
                {
                    for (int i = 0; i < fileList.Length; i++)
                    {
                        FileInfo file = fileList[i];
                        LinkButton link = new LinkButton();
                        link.ID = "hlFile" + i;
                        link.Click += Link_Click;
                        link.CommandArgument = "DownloadeDoc.aspx?path=" + file.FullName + "&type=" + file.Extension + "&docName=" + file.Name;
                        link.Text = (i + 1) + "." + file.Name;
                        LiteralControl ctrl = new LiteralControl();
                        ctrl.Text = "<br/>";
                        pnlUploadedFiles.Controls.Add(link);
                        pnlUploadedFiles.Controls.Add(ctrl);
                    }
                }
                else
                {
                    pnlUploadedFiles.Visible = false;
                }
            }
            else
            {
                pnlUploadedFiles.Visible = false;
            }
        }

        private void Link_Click(object sender, EventArgs e)
        {
            LinkButton link = (sender as LinkButton);

            if (link != null)
            {
                Response.Redirect(link.CommandArgument);
            }
        }
    }
}