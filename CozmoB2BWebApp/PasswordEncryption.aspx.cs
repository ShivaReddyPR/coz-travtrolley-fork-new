﻿using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class PasswordEncryption : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            
        }

        [WebMethod]
        public static string GetAllUsers()
        {
            string usersJSON = string.Empty;
            try
            {
                DataTable dtUsers = UserMaster.GetAllUsers(UserStatus.All);
                List<UserInfo> users = new List<UserInfo>();

                foreach (DataRow dr in dtUsers.Rows)
                {
                    if (dr["user_password"] != DBNull.Value)
                    {
                        UserInfo user = new UserInfo();
                        user.user_id = Convert.ToInt32(dr["user_id"]);
                        user.user_password = dr["user_password"].ToString();
                        users.Add(user);
                    }
                }

                usersJSON = JsonConvert.SerializeObject(users);
            }
            catch(Exception ex)
            {
                usersJSON = ex.Message;
            }

            return usersJSON;
        }

        [WebMethod]
        public static string UpdatePasswords(string userId, string password)
        {
            int recordsCount = 0;
            string records = string.Empty;
            try
            {
                if (!string.IsNullOrEmpty(password))
                {
                    int UserId = Convert.ToInt32(userId);
                    recordsCount += UserMaster.EncryptPasswords(UserId, password);
                    records = recordsCount.ToString();
                }
            }
            catch (Exception ex)
            {
                records = ex.Message;
            }
            return records;
        }
    }


    public class UserInfo
    {
        public int user_id;
        public string user_password;
    }
}