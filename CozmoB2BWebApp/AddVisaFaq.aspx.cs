﻿using System;
using CT.Core;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using Visa;


public partial class AddVisaFaq : CT.Core.ParentPage
{
    protected bool isAdmin = false;
    public int UpdateFaq = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
        {
            isAdmin = true;
        }
        if (!IsPostBack)
        {
            BindAgent();
            if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
            {
                int faqId;
                bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out faqId);
                if (!isnumerice)
                {
                    Response.Write("<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page");
                    Response.End();
                }
                try
                {
                    Visafaq faq = new Visafaq(faqId);

                    if (faq != null)
                    {
                        UpdateFaq = 1;
                        txtQuestion.Text = faq.FaqQuestion;
                        txtAnswer.Text = faq.FaqAnswer;
                        ddlAgent.SelectedIndex = ddlAgent.Items.IndexOf(ddlAgent.Items.FindByValue(Convert.ToString(faq.AgentId)));
                        ddlAgent.Enabled = false;
                        btnSave.Text = "Update";
                        Page.Title = "Update Faq";
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaFaq.aspx,Err:" + ex.Message,"");
                }
            }
            else 
            {
                if (!isAdmin)
                {
                    ddlAgent.SelectedValue = Settings.LoginInfo.AgentId.ToString();
                    ddlAgent.Enabled = false;
                }
            }
        }
    }


    private void BindAgent()
    {
        try
        {
            int agentId = 0;
            if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            string agentType = (Settings.LoginInfo.AgentType.ToString() != null ? Settings.LoginInfo.AgentType.ToString() : "BASEAGENT");
            ddlAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataValueField = "agent_Id";
            ddlAgent.DataTextField = "agent_Name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("Select Agent", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
   protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Visafaq objVisaFaq = new Visafaq();
            objVisaFaq.FaqQuestion = txtQuestion.Text.Trim();
            objVisaFaq.FaqAnswer = txtAnswer.Text.Trim();
            objVisaFaq.AgentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            if (btnSave.Text == "Update")
            {
                if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
                {
                    objVisaFaq.FaqId = Convert.ToInt32(Request.QueryString[0]);
                    objVisaFaq.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                }
            }
            else
            {
                objVisaFaq.IsActive = true;
                objVisaFaq.CreatedBy = (int)Settings.LoginInfo.UserID;
            }
            objVisaFaq.Save();
            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaFaq,Err:" + ex.Message, "");
        }
        Response.Redirect("VisaFaqList.aspx?AgencyId=" + ddlAgent.SelectedItem.Value);
    }


    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }
}
