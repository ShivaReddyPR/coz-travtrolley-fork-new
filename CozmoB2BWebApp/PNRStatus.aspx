﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="PNRStatusGUI" MasterPageFile="~/TransactionBE.master" Title="PNR Status" Codebehind="PNRStatus.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server" >
    <asp:HiddenField ID="hdfStatus" runat="server" Value="" />
    <div>
    <div class="col-md-12">
        <div class="col-md-2">
            <asp:Label ID="lblText" runat="server" Text="Enter PNR:*">
            </asp:Label>
        </div>
        <div class="col-md-2">
            <asp:TextBox ID="txtPNR" runat="server" CssClass="inputEnabled form-control"></asp:TextBox>
        </div>
        <div class="col-md-2">
            <asp:Button ID="btnSearch" CssClass="button" Text="Search" runat="server" Width="100px" OnClick="btnSearch_Click" OnClientClick="return Validate();"/>
        </div>
       
    </div>
    <div class="col-md-12"  id="dvResult" runat="server"  >
        <div class="col-md-2">
            <asp:Label ID="lblMessage" runat="server" Text="Select Status:*">
            </asp:Label>
        </div>
        <div class="col-md-2">
            <asp:DropDownList ID="ddlStatus" runat="server" AppendDataBoundItems="true" onchange="SetStatus();" CssClass="inputDdlEnabled">
           <asp:ListItem Text="--Select--" Value="0"></asp:ListItem>                              
        </asp:DropDownList>                      
            </div>
            <div class="col-md-2">
                <asp:Button ID="btnChangeStatus" CssClass="button" runat="server" Text="Change Status" OnClick="btnChangeStatus_Click" OnClientClick="return Check();"   />               
            </div>
        
       
       
        </div>
       
        <div >
             <asp:Label ID="lblStatus"  runat="server" Text="" style="color:Red"></asp:Label>
        </div>
    </div>
     <script type="text/javascript">
         
         function Validate()
         {
             
             if (document.getElementById('<%=txtPNR.ClientID%>').value == '') { addMessage('Please Enter PNR', ''); }
             if (getMessage() != '') {

                 alert(getMessage()); clearMessage();
                 return false;
             }
             else return true;
         }
         function Check()
         {
             
             if (document.getElementById('<%=ddlStatus.ClientID%>').selectedIndex <= 0) { addMessage('Please Choose Status!', ''); }
             if (getMessage() != '') {

                 alert(getMessage()); clearMessage();
                 return false;
             }
         }
         function SetStatus()
         {

            
             document.getElementById('<%=hdfStatus.ClientID%>').value = $('#<%=ddlStatus.ClientID%>').select2("data").element[0].index;;
         }
        
     </script>
</asp:Content>
