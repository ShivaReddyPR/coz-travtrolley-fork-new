﻿using CT.BookingEngine.Insurance;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using ReligareInsurance;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class ReligareProposerDetails : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (IsPostBack)
                {
                    bindCities();
                    if (!string.IsNullOrEmpty(txtpropdob.Text))
                        txtdob.Text = txtpropdob.Text;
                }
                IntialisePageControls();
                // Quotation Details 
                if (Session["quotation"] != null)
                {
                    ReligareQuotation quotation = (ReligareQuotation)Session["quotation"];
                    lblStartDate.Text = Convert.ToDateTime(quotation.StartDate).ToString("dd/MMM/yyyy");
                    lblEndDate.Text = Convert.ToDateTime(quotation.EndDate).ToString("dd/MMM/yyyy");
                    lblTravellingTo.Text = quotation.ProductName;
                    hdnproductType.Value = quotation.ProductType.ToString();
                    hdnNoofPax.Value = quotation.TravellerAges.Count.ToString();
                    lblPED.Text = quotation.Ped == "0" ? "YES" : "NO";                 
                    if (quotation.AgentId != Settings.LoginInfo.AgentId)
                    {
                        AgentMaster agentMaster = new AgentMaster(quotation.AgentId);
                        lblagentBal.Text = "Cr.Balance :" + CTCurrencyFormat(agentMaster.CurrentBalance, quotation.AgentId) + " Booking on behalf of " + agentMaster.Name + " " + agentMaster.City;
                    }                   
                    lblagentBal.Visible = quotation.AgentId != Settings.LoginInfo.AgentId ? true:false;
                    lblPremium.Text = CTCurrencyFormat(quotation.Premium+quotation.Markup+quotation.HandlingFee, quotation.AgentId);
                    // Checking the  Trip start from India only.
                    chknationality.Checked = quotation.ProductId == "40001001" ? true : false;                                  
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details Failed to Quotation Details." + ex.ToString(), Request["REMOTE_ADDR"]);
            }
            try
            {
                if (!(IsPostBack))
                {
                    BindData();
                    if (Session["religareHeader"] != null)
                    {
                        ReligareHeader religareHeader = (ReligareHeader)Session["religareHeader"];
                        List<ReligarePassengers> obj_liPassengers = religareHeader.ReligarePassengers;
                        List<ReligarePaxQuestions> obj_liRPQuestions = religareHeader.ReligarePaxQuestions;
                        hdnstatus.Value = "postback";
                        hdnproductType.Value = religareHeader.Produ_type;
                        //Proposer Details
                        ReligarePassengers religarePassengers = obj_liPassengers[0];
                        txtAddressLine1.Text = religarePassengers.Address1;
                        txtAddressLine2.Text = religarePassengers.Address2;
                        txtEmail.Text = religarePassengers.Email;
                        txtMobileNo.Text = religarePassengers.Mobileno;
                        txtPincode.Text = religarePassengers.PinCode;
                        txtcity.Text = religarePassengers.City;
                        ListItem item = ddlNominee.Items.FindByText(religarePassengers.NomineeRelation); 
                        if (item != null)
                            ddlNominee.SelectedValue = ddlNominee.Items.FindByText(religarePassengers.NomineeRelation).Value;
                         
                        bindCities();
                        txtState.Text = religarePassengers.City;
                        ddlCity.SelectedValue = religarePassengers.City;
                        txtState.Text = religarePassengers.State;
                        txtNomineeName.Text = religarePassengers.Nominee_name;
                        ddlPurposeVisit.SelectedValue = religarePassengers.PurposeVisit;
                          txtPassportNo.Text = religarePassengers.PassportNo;
                        txtdob.Text = Convert.ToDateTime(religarePassengers.Dob).ToString("dd/MMM/yyyy");
                        ddlresidenceProof.SelectedValue = religarePassengers.ResidenceProof;
                        txtProof.Text = religarePassengers.ProofDetails;
                        txtFirstName.Text = religarePassengers.FirstName;
                        txtLastName.Text = religarePassengers.LastName;
                        ddlNationality.SelectedValue = religarePassengers.Nationality;
                        if (religarePassengers.Title == "Mr")
                            ddlTitle.SelectedValue = "Mr";
                        else if (religarePassengers.Title == "Ms")
                            ddlTitle.SelectedValue = "Ms";
                         
                        bool status = true;
                        status = ddlNationality.SelectedItem.Value.Trim().ToUpper() == "IN" ? false : true;
                        ddlresidenceProof.Visible = status;
                        txtProof.Visible = status;
                        lblresidenceProof.Visible = status;
                        lblProof.Visible = status;
                        //Insurance Details
                        int counter = 0;
                        foreach (TableRow tr in tblInsuredDetails.Rows)
                        {
                            foreach (TableCell tc in tr.Cells)
                            {
                                foreach (Control control in tc.Controls)
                                {
                                    if (control is DropDownList)
                                    {
                                        DropDownList dropDownList = (DropDownList)control;
                                        if (dropDownList.ID.StartsWith("ddlInsRelation"))
                                        {
                                            religarePassengers = obj_liPassengers[counter + 1];
                                            dropDownList.SelectedValue = religarePassengers.Relation;
                                            counter++;
                                        }
                                        else if (dropDownList.ID.StartsWith("ddlInsNationality"))
                                            dropDownList.SelectedValue = religarePassengers.Nationality;
                                        else if (dropDownList.ID.StartsWith("ddlInsTitle"))
                                            dropDownList.SelectedValue = religarePassengers.Title;
                                        else if (dropDownList.ID.StartsWith("ddlInsResidence"))
                                        {
                                             if (religarePassengers.Nationality == "IN")
                                                dropDownList.Visible = false;
                                            else
                                                dropDownList.SelectedValue = religarePassengers.ResidenceProof;
                                        }
                                    }
                                    else if (control is TextBox)
                                    {
                                        TextBox textBox = (TextBox)control;
                                        if (textBox.ID.StartsWith("txtInsPassportNo"))
                                            textBox.Text = religarePassengers.PassportNo;
                                        else if (textBox.ID.StartsWith("txtInsFirstName"))
                                            textBox.Text = religarePassengers.FirstName;
                                        else if (textBox.ID.StartsWith("txtInsLastName"))
                                            textBox.Text = religarePassengers.LastName;
                                        else if (textBox.ID.StartsWith("txtInsDOB"))
                                            textBox.Text = Convert.ToDateTime(religarePassengers.Dob).ToString("dd/MMM/yyyy");
                                        else if (textBox.ID.StartsWith("txtInsureDOB"))
                                            textBox.Text = Convert.ToDateTime(religarePassengers.Dob).ToString("dd/MMM/yyyy");
                                        else if (textBox.ID.StartsWith("txtInsProof"))
                                        {
                                            textBox.Visible = religarePassengers.Nationality == "IN" ? false : true;
                                            if (textBox.Visible)
                                                textBox.Text = religarePassengers.ProofDetails;                                           
                                        }
                                    }
                                    else if (control is Label)
                                    {
                                        Label lbl = (Label)control;
                                        if (lbl.ID != null && (lbl.ID.StartsWith("lblproofDetails") || lbl.ID.StartsWith("lblResidence")))
                                        {
                                            lbl.Visible = religarePassengers.Nationality == "IN" ? false : true;                                            
                                        }
                                    }
                                }
                            }
                        }
                         status = false;
                        string qcode = "";
                        //Questionary Details
                        foreach (TableRow tr in tblQuestionire.Rows)
                        {
                            foreach (TableCell tc in tr.Cells)
                            {
                                foreach (Control control in tc.Controls)
                                {
                                    if (control is RadioButton)
                                    {
                                        RadioButton rb = (RadioButton)control;
                                        rb.Checked = true;
                                        if (rb.Text.ToUpper() == "NO" && lblPED.Text.ToUpper() == "YES")
                                        {
                                            List<ReligarePaxQuestions> li = GetPaxQuestions(rb.ID.Remove(rb.ID.Length - 2).Split('_')[1], "YESNO");
                                            foreach (ReligarePaxQuestions paxQuestionary in li)
                                            {
                                                if (paxQuestionary.Question_status == true)
                                                {
                                                    rb.Checked = false;
                                                    status = true;
                                                }
                                            }
                                        }
                                    }
                                    else if (control is CheckBox)
                                    {
                                        CheckBox cb = (CheckBox)control;
                                        List<ReligarePaxQuestions> li = GetPaxQuestions(cb.ID.Split('_')[1], cb.ID.Split('_')[2]);
                                        foreach (ReligarePaxQuestions paxQuestionary in li)
                                        {
                                            cb.Checked = paxQuestionary.Question_status;
                                            qcode = paxQuestionary.Question_code;
                                        }
                                        if (status)
                                            cb.Enabled = true;
                                    }
                                    else if (control is Label)
                                    {
                                        Label lbl = (Label)control;
                                        if (lbl.Text == "")
                                            qcode = lbl.ID.Split('_')[2];
                                    }
                                    else if (control is TextBox)
                                    {
                                        TextBox tb = (TextBox)control;
                                        List<ReligarePaxQuestions> li = null;
                                        li = GetPaxQuestions(qcode, tb.ID.Split('_')[2]);                                       
                                        foreach (ReligarePaxQuestions paxQuestionary in li)
                                        {
                                            tb.Text = paxQuestionary.Question_other_remarks;
                                        }
                                        tb.Enabled = status && !(string.IsNullOrEmpty(tb.Text)) ? true : false;                                        
                                    }
                                }
                            }
                        }
                        if (Convert.ToInt32(religareHeader.Produ_type) == (int)ReligareProducts.StudentExplore)
                        {
                            //Education Details
                            foreach (TableRow tr in tblEducationDetails.Rows)
                            {
                                foreach (TableCell cell in tr.Cells)
                                {
                                    foreach (Control control in cell.Controls)
                                    {
                                        if (control is TextBox)
                                        {
                                            TextBox txt = (TextBox)control;
                                            if (txt.ID == "txtEIName")
                                                txt.Text = religareHeader.UniversityName;
                                            else if (txt.ID == "txtECourse")
                                                txt.Text = religareHeader.CourseDetails;
                                            else if (txt.ID == "txtEIAddress")
                                                txt.Text = religareHeader.UniversityAddress;
                                            else if (txt.ID == "txtEICountry")
                                                txt.Text = religareHeader.UniversityAddress;
                                            else if (txt.ID == "txtESponser")
                                                txt.Text = religareHeader.SponsorName;
                                            else if (txt.ID == "txtESDob")
                                                txt.Text = Convert.ToDateTime(religareHeader.SponsorDOB).ToString("dd/MMM/yyyy");
                                        }
                                        else if (control is DropDownList)
                                        {
                                            DropDownList dropDown = (DropDownList)control;
                                            dropDown.SelectedValue = religareHeader.RelationshipToStudent;
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details Failed to  updating Passenger Details." + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Initialize the dynamic controls
        /// </summary>
        private void IntialisePageControls()
        {
            try
            {
                AddInsuredControls();
                AddQuestionire();
                AddEducationDetails();
                AddOptionalCovers();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// Get the  filtering the questions list of questions using paxid and type
        /// </summary>
        /// <param name="id"></param>
        /// <param name="paxtype"></param>
        /// <returns></returns>
        private List<ReligarePaxQuestions> GetPaxQuestions(string id, string paxtype)
        {
            try
            {
                ReligareHeader religareHeader = (ReligareHeader)Session["religareHeader"];
                List<ReligarePaxQuestions> liquestions;
                if (paxtype == "YESNO")
                {
                    liquestions = (from pax in religareHeader.ReligarePaxQuestions
                                   where pax.Question_code == id
                                   select pax).ToList();
                }
                else
                {
                    liquestions = (from pax in religareHeader.ReligarePaxQuestions
                                   where pax.Question_code == id && pax.Pax_id.ToString() == paxtype
                                   select pax).ToList();
                }
                return liquestions;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details failed to Get the Questionary Details:" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
            return null;
        }
        /// <summary>
        /// Binding the proposer and insured Nationalities.
        /// </summary>
        private void BindData()
        {
            try
            {
                DataTable dtRelation = ReligareQuotation.Get_Relations("N");
                ddlNominee.DataSource = dtRelation;
                ddlNominee.DataValueField = "code";
                ddlNominee.DataTextField = "RELATION";
                ddlNominee.DataBind();
                ddlNominee.Items.Insert(0, new ListItem("-- Select --", "-1"));
                ddlNominee.Items.Remove(new ListItem("SELF- PRIMARY MEMBER", "SELF"));
                string prodtype = Session["productId"].ToString();
                if (Convert.ToInt32(prodtype) == (int)ReligareProducts.StudentExplore)
                    dtRelation = ReligareQuotation.Get_Relations("A");
                else
                    dtRelation = ReligareQuotation.Get_Relations("E");


                DataTable dtNationalities = ReligareQuotation.GetVisaNationalities();
                ddlNationality.DataSource = dtNationalities;
                ddlNationality.DataValueField = "countryCode";// "nationality_id";
                ddlNationality.DataTextField = "nationality";// "nationality_name";
                ddlNationality.DataBind();
                ddlNationality.Items.Insert(0, new ListItem("-- Select --", "-1"));
                //For Education Purpose                
                foreach (TableRow tr in tblInsuredDetails.Rows)
                {
                    foreach (TableCell tc in tr.Cells)
                    {
                        foreach (Control control in tc.Controls)
                        {
                            if (control is DropDownList)
                            {
                                DropDownList dropDownList = (DropDownList)control;
                                DataTable dt = new DataTable();
                                if (dropDownList.ID.StartsWith("ddlInsRelation"))
                                {
                                    dropDownList.DataSource = dtRelation;
                                    dropDownList.DataValueField = "CODE";
                                    dropDownList.DataTextField = "RELATION";
                                }
                                else if (dropDownList.ID.StartsWith("ddlInsNationality"))
                                {
                                    dropDownList.DataSource = dtNationalities;
                                    dropDownList.DataValueField = "countryCode";
                                    dropDownList.DataTextField = "nationality";
                                }
                                dropDownList.DataBind();
                                dropDownList.Items.Insert(0, new ListItem("-- Select --", "-1"));
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details Failed to Binding the nationality details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Adding the Insured controls from Passengers.
        /// </summary>
        private void AddInsuredControls()
        {
            try
            {
                short tabindex = ddlPurposeVisit.TabIndex;
                if (ddlNationality.SelectedItem.Value.Trim().ToUpper() != "IN")  
                    tabindex = txtProof.TabIndex;
                tabindex++;
                ReligareQuotation quotation = (ReligareQuotation)Session["quotation"];
                int travellerCount = quotation.Travellers;
                string[] lblcontrols = "Relation,Nationality,Passport No,Title,First Name,Last Name,DOB,Residence Proof,Proof Details".Split(',');
                string[] controls = "ddlInsRelation,ddlInsNationality,txtInsPassportNo,ddlInsTitle,txtInsFirstName,txtInsLastName,txtInsDOB,ddlInsResidence,txtInsProof".Split(',');
                for (int i = 0; i < travellerCount; i++)
                {
                    TableRow tr = new TableRow();
                    TableRow trv = new TableRow();
                    TableCell tableCell = new TableCell();
                    tr.CssClass = "trPassenger";
                    Label lbl = new Label();
                    lbl.CssClass = "ns-h3";
                    lbl.Text = "Passenger " + Convert.ToInt32(i + 1) + " ";
                    tableCell.Controls.Add(lbl);
                    tableCell.Font.Bold = true;
                    tr.Cells.Add(tableCell);
                    tblInsuredDetails.Rows.Add(tr);
                    tr = new TableRow();
                    for (int k = 0; k < 4; k++)
                    {
                        tableCell = new TableCell();
                        tableCell.Text = lblcontrols[k];
                        tr.Cells.Add(tableCell);
                    }
                    tblInsuredDetails.Rows.Add(tr);
                    tr = new TableRow();
                    for (int j = 0; j < lblcontrols.Length; j++)
                    {
                        tableCell = new TableCell();
                        if (j < 2 || j == 3 || j == 7)
                        {
                            DropDownList ddl = new DropDownList();
                            ddl.ID = controls[j] + i;
                            ddl.TabIndex = tabindex;
                            tabindex++;
                            ddl.CssClass = "form-control";
                            ddl.Items.Insert(0, new ListItem("-- Select --", "-1"));
                            if (controls[j] == "ddlInsTitle")
                            {
                                ddl.Items.Insert(1, new ListItem("Mr", "Mr"));
                                ddl.Items.Insert(2, new ListItem("Ms", "Ms"));
                                ddl.Items.Remove(new ListItem("-- Select --", "-1"));
                            }
                            else if (controls[j] == "ddlInsResidence")
                            {
                                ddl.Items.Insert(1, new ListItem("Govt issued ID", "1"));
                                ddl.Items.Insert(2, new ListItem("OCI card", "2"));
                                ddl.Items.Insert(3, new ListItem("Company ID", "3"));
                                ddl.Items.Remove(new ListItem("-- Select --", "-1"));
                            }
                            else if (controls[j] == "ddlInsNationality")
                            {
                                ddl.AutoPostBack = true;
                                ddl.SelectedIndexChanged += new EventHandler(this.ddlNationality_SelectedIndexChanged);
                            }
                            else if (controls[j] == "ddlInsRelation")
                            {
                                ddl.AutoPostBack = true;
                                ddl.SelectedIndexChanged += new EventHandler(this.ddlInsRelation_SelectedIndexChanged);
                            }
                            tableCell.Controls.Add(ddl);
                            tr.Cells.Add(tableCell);
                        }
                        else
                        {
                            if (j == 8)
                            {
                                tr = new TableRow();
                                tableCell = new TableCell();
                                lbl = new Label();
                                lbl.Text = lblcontrols[j];
                                lbl.ID = "lblproofDetails" + i;
                                tableCell.Controls.Add(lbl);
                                tr.Cells.Add(tableCell);
                                tblInsuredDetails.Rows.Add(tr);
                                tr = new TableRow();
                                tableCell = new TableCell();
                            }
                            TextBox txt = new TextBox();
                            txt.TabIndex = tabindex;
                            tabindex++;
                            txt.CssClass = "form-control";
                            txt.ID = controls[j] + i;
                            if (lblcontrols[j] == "Passport No")
                                txt.MaxLength = 8;
                            else if (controls[j] == "txtInsDOB")
                            {
                                TextBox txtdob = new TextBox();
                                txtdob.ID = "txtInsureDOB" + i;
                                txtdob.Attributes.CssStyle.Add("display", "none");
                                tableCell.Controls.Add(txtdob);

                                tableCell.ID = "tdDOB" + i;
                                txt.ReadOnly = true;
                                txt.Attributes.CssStyle.Add("background-color", "#fff") ;
                                txt.Attributes.Add("onChange", "insuredDob('"+txt.ID+"')");
                            }
                            else if (controls[j] == "txtInsFirstName" || controls[j] == "txtInsLastName")
                                txt.Attributes.Add("onKeyPress", "return validChar(this.event)");
                            tableCell.Controls.Add(txt);
                            tr.Cells.Add(tableCell);
                            if (j == 8)
                            {
                                trv = new TableRow();
                                for (int k = j; k < lblcontrols.Length; k++)
                                {
                                    trv.CssClass = "Validations";
                                    tableCell = new TableCell();
                                    tableCell.Text = "<span id='error" + controls[k] + i + "' style='color: red;'></span>";
                                    trv.Cells.Add(tableCell);
                                }
                            }
                        }
                        if (j == 3 || j == 7)
                        {
                            tblInsuredDetails.Rows.Add(tr);
                            trv = new TableRow();
                            for (int k = j - 3; k <= j; k++)
                            {
                                trv.CssClass = "Validations";
                                tableCell = new TableCell();
                                tableCell.Text = "<span id='error" + controls[k] + i + "' style='color: red;'></span>";
                                trv.Cells.Add(tableCell);
                            }
                            tblInsuredDetails.Rows.Add(trv);
                            tr = new TableRow();
                            for (int k = j + 1; k < 8; k++)
                            {
                                tableCell = new TableCell();
                                tableCell.Text = lblcontrols[k];
                                if (k == 7)
                                {
                                    lbl = new Label();
                                    lbl.Text = lblcontrols[k];
                                    lbl.ID = "lblResidence" + i;
                                    tableCell.Controls.Add(lbl);
                                }
                                tr.Cells.Add(tableCell);
                            }
                            tblInsuredDetails.Rows.Add(tr);
                            tr = new TableRow();
                        }
                    }
                    tblInsuredDetails.Rows.Add(tr);
                    tblInsuredDetails.Rows.Add(trv);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details Failed to adding the Insured Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Adding the questionary to passengers.
        /// </summary>
        private void AddQuestionire()
        {
            try
            {
                ReligareQuotation quotation = (ReligareQuotation)Session["quotation"];
                string productId = quotation.ProductId;
                string typeId = quotation.ProductType.ToString();
                DataTable dt_Question = ReligareQuotation.GetQuestions(ref productId, ref typeId);
                if (dt_Question != null && dt_Question.Rows.Count > 0)
                {
                    TableRow tr;
                    TableCell tableCell;
                    TextBox textBox;
                    RadioButton radioButton;
                    CheckBox checkBox;
                    DropDownList ddl;
                    Label label;
                    string[] status = "Yes,No".Split(',');
                    int travellerCount = quotation.Travellers;
                    int rowcount = 0;
                    foreach (DataRow objdr in dt_Question.Rows)
                    {
                        if (objdr["QUESTION_CODE"].ToString().ToUpper() == "pedYesNo".ToUpper() || objdr["QUESTION_CODE"].ToString().ToUpper().StartsWith("T00"))
                        {
                            tr = new TableRow();
                            tr.CssClass = "tr" + objdr["QUESTION_CODE"].ToString() + "Main";
                            tableCell = new TableCell();
                            label = new Label();
                            label.ID = objdr["QUESTION_SET_CODE"].ToString();
                            label.Text = objdr["QUESION_DESCRIPTION"].ToString();
                            tableCell.Controls.Add(label);
                            tr.Cells.Add(tableCell);
                            foreach (string str in status)
                            {
                                radioButton = new RadioButton();
                                radioButton.Text = str;
                                radioButton.CssClass = "radioButton";
                                radioButton.ID = "rb_" + objdr["QUESTION_CODE"].ToString() + str;
                                radioButton.GroupName = objdr["QUESTION_CODE"].ToString(); 
                                radioButton.Checked = true;
                                radioButton.Attributes.Add("onchange", "enableQuestionary('" + radioButton.ID + "')"); 
                                tableCell = new TableCell();
                                tableCell.Controls.Add(radioButton);
                                tr.Cells.Add(tableCell);
                            }
                            tblQuestionire.Rows.Add(tr);
                            tr = new TableRow();
                            tableCell = new TableCell();
                            tableCell.Text = "Select your choice";
                            tr.Cells.Add(tableCell);
                            tr.CssClass = objdr["QUESTION_CODE"].ToString() == "pedYesNo" ? "trQuestions" : "tr" + objdr["QUESTION_CODE"].ToString();
                            for (int i = 0; i < travellerCount; i++)
                            {
                                tableCell = new TableCell();
                                ddl = new DropDownList();
                                ddl.ID = "ddl_" + objdr["QUESTION_CODE"].ToString() + "_" + i;
                                ddl.CssClass = "form-control";
                                ddl.Items.Insert(0, new ListItem("Select", "-1"));
                                ddl.Items.Insert(1, new ListItem("Yes", "1"));
                                ddl.Items.Insert(2, new ListItem("No", "2")); 
                                ddl.Attributes.Add("onchange", "enableQuestionary('" + ddl.ID + "')"); 
                                tableCell.Controls.Add(ddl);
                                tr.Cells.Add(tableCell);
                            }
                            tblQuestionire.Rows.Add(tr);
                            if (objdr["QUESTION_CODE"].ToString().ToUpper().StartsWith("T00"))
                            {
                                tr = new TableRow();
                                tr.CssClass = "tr" + objdr["QUESTION_CODE"].ToString();
                                tableCell = new TableCell();
                                label = new Label();
                                label.ID = "lbl_" + objdr["QUESTION_SET_CODE"].ToString();
                                label.Text = "Description";
                                tableCell.Controls.Add(label);
                                DataRow dr = dt_Question.Rows[rowcount + 1];
                                label = new Label();
                                label.ID = "lbl_" + dr["QUESTION_SET_CODE"].ToString() + "_" + dr["QUESTION_CODE"].ToString();
                                tableCell.Controls.Add(label);
                                tr.Cells.Add(tableCell);

                                for (int i = 0; i < travellerCount; i++)
                                {
                                    textBox = new TextBox();
                                    textBox.CssClass = "form-control";
                                    textBox.ID = "txt_" + objdr["QUESTION_CODE"].ToString() + "_" + i;
                                    textBox.Enabled = false;
                                    tableCell = new TableCell();
                                    tableCell.Controls.Add(textBox);
                                    tr.Cells.Add(tableCell);
                                }
                                tblQuestionire.Rows.Add(tr);
                            }
                        }
                        else if (!(objdr["QUESTION_SET_CODE"].ToString().StartsWith("TRVLDET")))
                        {
                            tr = new TableRow();
                            tr.CssClass = "trQuestions";
                            tableCell = new TableCell();
                            label = new Label();
                            if (objdr["QUESTION_CODE"].ToString().ToLower() != "otherdiseasesdescription")
                                label.ID = objdr["QUESTION_SET_CODE"].ToString();
                            label.Text = objdr["QUESION_DESCRIPTION"].ToString();
                            tableCell.Controls.Add(label);
                            tr.Cells.Add(tableCell);
                            if (objdr["QUESTION_CODE"].ToString() != "otherDiseasesDescription")
                            {
                                for (int i = 0; i < travellerCount; i++)
                                {
                                    tableCell = new TableCell();
                                    //tableCell.CssClass = "col-md-1";
                                    checkBox = new CheckBox();
                                    checkBox.ID = "cb_" + objdr["QUESTION_CODE"].ToString() + "_" + i;
                                    checkBox.Enabled = false;
                                    checkBox.CssClass = "cb" + i;
                                    if (objdr["QUESTION_SET_CODE"].ToString() == "PEDotherDetailsTravel") 
                                        checkBox.Attributes.Add("onchange", "enableQuestionary('" + checkBox.ID + "')");  
                                    tableCell.Controls.Add(checkBox);
                                    tr.Cells.Add(tableCell);
                                }
                                tblQuestionire.Rows.Add(tr);
                            }
                            if (objdr["QUESTION_CODE"].ToString().ToLower() == "otherdiseasesdescription")
                            {
                                tr = new TableRow();
                                tr.CssClass = "trQuestions";
                                tableCell = new TableCell();
                                if (objdr["QUESTION_CODE"].ToString() == "otherDiseasesDescription")
                                {
                                    tableCell.Text = objdr["QUESION_DESCRIPTION"].ToString();
                                    tableCell.ID = objdr["QUESTION_CODE"].ToString();
                                }
                                else
                                    tableCell.Text = "Description";
                                tr.Cells.Add(tableCell);
                                DataRow dr = dt_Question.Rows[rowcount];
                                for (int i = 0; i < travellerCount; i++)
                                {
                                    textBox = new TextBox();
                                    textBox.CssClass = "form-control";
                                    textBox.ID = "txt_" + dr["QUESTION_CODE"].ToString() + "_" + i;
                                    textBox.Enabled = false;
                                    tableCell = new TableCell();
                                    tableCell.Controls.Add(textBox);
                                    tr.Cells.Add(tableCell);
                                }
                                tblQuestionire.Rows.Add(tr);
                            }
                        }
                        rowcount++;
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details Failed to Adding The questionary" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Enable/Disable the proof details and residence 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlNationality_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList dropDownList = (DropDownList)sender;
                if (dropDownList.ID.StartsWith("ddlNationality"))
                {
                    // For Indian NAtional,residency proof and proof detauls are not mandatory
                    bool status = true;
                    status = dropDownList.SelectedItem.Value.Trim().ToUpper() == "IN" ? false : true;
                    ddlresidenceProof.Visible = status;
                    txtProof.Visible = status;
                    lblresidenceProof.Visible = status;
                    lblProof.Visible = status;                   
                    txtPassportNo.Focus();
                }
                else if (dropDownList.ID.StartsWith("ddlInsNationality"))
                {
                    string id = dropDownList.ID.Replace("ddlInsNationality", "");
                    string nationCode = "";
                    foreach (TableRow tr in tblInsuredDetails.Rows)
                    {
                        foreach (TableCell tc in tr.Cells)
                        {
                            tc.Visible = true;
                            foreach (Control control in tc.Controls)
                            {
                                if (control is DropDownList)
                                {
                                    DropDownList ddl = (DropDownList)control;
                                    if (ddl.ID.StartsWith("ddlInsNationality"))
                                        nationCode = ddl.SelectedValue;
                                    if (ddl.ID.StartsWith("ddlInsResidence" + id))
                                    {
                                        ddl.Visible = nationCode == "IN" ? false : true;
                                        if (nationCode == "IN") 
                                            ddl.ClearSelection(); 
                                    }
                                }
                                else if (control is TextBox)
                                {
                                    TextBox textBox = (TextBox)control;
                                    if (textBox.ID.StartsWith("txtInsProof" + id))
                                    {
                                        textBox.Visible = nationCode == "IN" ? false : true;
                                        textBox.Text = nationCode == "IN" ? "" : textBox.Text;                                        
                                    }
                                }
                                else if (control is Label)
                                {
                                    Label lbl = (Label)control;
                                    if (lbl.ID != null)
                                    {
                                        string d = lbl.ID[lbl.ID.Length - 1].ToString();
                                        if ((lbl.ID.StartsWith("lblproofDetails") || lbl.ID.StartsWith("lblResidence")) && id == d)
                                        {
                                            lbl.Visible = dropDownList.SelectedItem.Value .Trim().ToUpper() == "IN" ? false : true;
                                             
                                        }
                                    }
                                }
                            }
                            if (nationCode == "IN" && (tc.Text == "Residence Proof" || tc.Text == "Proof Details"))
                            {
                                tc.Visible = tc.Visible ? false : true;
                            }

                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details Failed to Visible/hide the residence Details " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Binding the self details to insured person by selecting relation the self.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void ddlInsRelation_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                DropDownList ddl = (DropDownList)sender;
                string id = ddl.ID.Replace("ddlInsRelation", "");
                foreach (TableRow tr in tblInsuredDetails.Rows)
                {
                    foreach (TableCell tc in tr.Cells)
                    {
                        foreach (Control control in tc.Controls)
                        {
                            if (control is DropDownList)
                            {
                                DropDownList dropDownList = (DropDownList)control;
                                if (dropDownList.ID.EndsWith(id))
                                {
                                    if (ddl.SelectedValue == "SELF")
                                    {
                                        if (dropDownList.ID.StartsWith("ddlInsNationality" + id))
                                            dropDownList.SelectedValue = ddlNationality.SelectedValue;
                                        else if (dropDownList.ID.StartsWith("ddlInsTitle" + id))
                                            dropDownList.SelectedValue = ddlTitle.SelectedValue;
                                        else if (dropDownList.ID.StartsWith("ddlInsResidence" + id))
                                        {
                                            dropDownList.Visible = ddlNationality.SelectedItem.Value.Trim().ToUpper() == "IN" ? false : true;
                                            dropDownList.SelectedValue = ddlNationality.SelectedItem.Value.Trim().ToUpper() != "IN" ? ddlresidenceProof.SelectedValue : "-1";
                                            //if (ddlNationality.SelectedItem.Text == "Indian")
                                            //    dropDownList.Visible = false;
                                            //else
                                            //{
                                            //    dropDownList.SelectedValue = ddlresidenceProof.SelectedValue;
                                            //    dropDownList.Visible = true;
                                            //}
                                        }
                                        if (!(dropDownList.ID.StartsWith("ddlInsRelation" + id)))
                                            dropDownList.Enabled = false;
                                    }
                                    else
                                    {
                                        dropDownList.Enabled = true;
                                        if (!(dropDownList.ID.StartsWith("ddlInsRelation" + id)))
                                            dropDownList.ClearSelection();
                                        //Binding the title based on Relation.
                                        string[] mr = "SONM,FATH,BOTH".Split(',');
                                        string[] ms = "SIST,SPSE,UDTR,MOTH".Split(',');
                                        if (mr.Contains(ddl.SelectedValue) && dropDownList.ID.StartsWith("ddlInsTitle"))
                                        {
                                            dropDownList.SelectedValue = "Mr";
                                            dropDownList.Enabled = false;
                                        }
                                        else if (ms.Contains(ddl.SelectedValue) && dropDownList.ID.StartsWith("ddlInsTitle"))
                                        {
                                            dropDownList.SelectedValue = "Ms";
                                            dropDownList.Enabled = false;
                                        }
                                        else
                                            dropDownList.Enabled = true;
                                    }
                                }
                            }
                            else if (control is TextBox)
                            {
                                TextBox textBox = (TextBox)control;
                                if (textBox.ID.EndsWith(id))
                                {
                                    if (ddl.SelectedValue == "SELF")
                                    {
                                        if (textBox.ID.StartsWith("txtInsPassportNo" + id))
                                            textBox.Text = txtPassportNo.Text;
                                        else if (textBox.ID.StartsWith("txtInsFirstName" + id))
                                            textBox.Text = txtFirstName.Text;
                                        else if (textBox.ID.StartsWith("txtInsLastName" + id))
                                            textBox.Text = txtLastName.Text;
                                        else if (textBox.ID.StartsWith("txtInsDOB")) 
                                            textBox.Text = txtdob.Text;
                                        else if (textBox.ID.StartsWith("txtInsureDOB"))
                                            textBox.Text = txtdob.Text;
                                        else if (textBox.ID.StartsWith("txtInsProof" + id))
                                        {
                                            textBox.Visible = ddlNationality.SelectedItem.Value.Trim().ToUpper() == "IN" ? false : true;
                                            if (textBox.Visible)
                                                textBox.Text = txtProof.Text;
                                        }
                                        textBox.ReadOnly = true;
                                        textBox.Focus();
                                    }
                                    else
                                    {
                                        if (!textBox.ID.StartsWith("txtInsDOB"))
                                            textBox.ReadOnly = false;
                                        textBox.Text = "";
                                    }

                                }
                            }
                            else if (control is Label)
                            {
                                Label lbl = (Label)control;
                                if (lbl.ID != null && ddl.SelectedValue == "SELF")
                                {
                                    string d = lbl.ID[lbl.ID.Length - 1].ToString();
                                    // hiding the Residence and Proof Details when Nationalily is indian (Insured Person)
                                    if ((lbl.ID.StartsWith("lblproofDetails") || lbl.ID.StartsWith("lblResidence")) && id == d)
                                        lbl.Visible = ddlNationality.SelectedItem.Value.Trim().ToUpper() == "IN" ? false : true;
                                }
                            }
                        }
                    }
                }
                ddl.Focus();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details failed to binding self proposer Details:" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Navigate the religare insurance page.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnEdit_Click(object sender, EventArgs e)
        {
            if (Session["quotation"] != null)
            {
                Response.Redirect("ReligareInsurance.aspx", false);
            }
        }
        /// <summary>
        /// Binding the Cities using pincode.
        /// </summary>
        private void bindCities()
        {
            try
            {
                if (!string.IsNullOrEmpty(txtPincode.Text))
                {
                    DataTable dt = ReligareHeader.GetCityCodes(txtPincode.Text);
                    ddlCity.DataSource = dt;
                    ddlCity.DataTextField = "citycd";
                    ddlCity.DataValueField = "citycd";
                    ddlCity.DataBind();
                    ddlCity.Items.Insert(0, new ListItem("-- Select --", "-1"));
                    if (!string.IsNullOrEmpty(txtcity.Text))
                        ddlCity.SelectedValue = txtcity.Text;
                    txtState.Text = dt.Rows[0]["statecd"].ToString();
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details failed to binding the Cities:" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Adding Education Details For Student Explorer Product.
        /// </summary>
        private void AddEducationDetails()
        {
            try
            {
                ReligareQuotation quotation = (ReligareQuotation)Session["quotation"];
                string productId = quotation.ProductId;
                string typeId = quotation.ProductType.ToString();
                DataTable dt_Question = ReligareQuotation.GetEducationQuestions(ref productId, ref typeId);
                if (dt_Question != null && dt_Question.Rows.Count > 0)
                {
                    string[] lblcontrols = "Institute Name,Course Name,Institute Address,Country,Sponser Name,Sponser DOB,Sponser Relation".Split(',');
                    string[] controls = "txtEIName,txtECourse,txtEIAddress,txtEICountry,txtESponser,txtESDob,ddlERelation".Split(',');
                    int count = 0;
                    TableRow row = new TableRow();
                    TableRow rowV;
                    TableRow rowc;
                    TableCell cell = new TableCell();
                    foreach (DataRow dr in dt_Question.Rows)
                    {
                        if (count == 3)
                        {
                            rowc = new TableRow();
                            rowV = new TableRow();
                            for (int i = 0; i <= count; i++)
                            {
                                cell = new TableCell();
                                Label lbl = new Label();
                                DataRow dataRow = dt_Question.Rows[i + 1];
                                if (i == 3)
                                    dataRow = dt_Question.Rows[i];
                                lbl.Text = lblcontrols[i];
                                cell.Controls.Add(lbl);
                                row.Cells.Add(cell);

                                cell = new TableCell();
                                if (i != 3)
                                    cell.ID = dataRow["QUESTION_SET_CODE"].ToString() + "-" + dataRow["QUESTION_CODE"].ToString();
                                TextBox txt = new TextBox();
                                txt.ID = controls[i];
                                txt.CssClass = "form-control";                                
                                cell.Controls.Add(txt);
                                rowc.Cells.Add(cell);

                                rowV.CssClass = "Validations";
                                cell = new TableCell();
                                cell.Text = "<span id='error" + controls[i] + "' style='color: red;'></span>";
                                rowV.Cells.Add(cell);
                            }
                            tblEducationDetails.Rows.Add(row);
                            tblEducationDetails.Rows.Add(rowc);
                            tblEducationDetails.Rows.Add(rowV);
                        }
                        else if (count == dt_Question.Rows.Count - 1)
                        {
                            rowc = new TableRow();
                            row = new TableRow();
                            rowV = new TableRow();
                            for (int i = 4; i <= dt_Question.Rows.Count; i++)
                            {
                                cell = new TableCell();
                                Label lbl = new Label();
                                DataRow dataRow;

                                lbl.Text = lblcontrols[i];
                                cell.Controls.Add(lbl);
                                row.Cells.Add(cell);

                                cell = new TableCell();
                                if (controls[i].StartsWith("ddl"))
                                {
                                    DropDownList ddl = new DropDownList();
                                    ddl.ID = controls[i];
                                    ddl.CssClass = "form-control";
                                    ddl.Items.Insert(0, new ListItem("-- Select --", "-1"));
                                    if (controls[i] == "ddlERelation")
                                    {   //For Education Purpose
                                        cell.ID = "SPDsponsorDetails-relationwithInsured";
                                        DataTable dtRelation = ReligareQuotation.Get_Relations("ALL");
                                        ddl.DataSource = dtRelation;
                                        ddl.DataValueField = "CODE";
                                        ddl.DataTextField = "RELATION";
                                        ddl.DataBind();
                                        ddl.Items.Insert(0, new ListItem("-- Select --", "-1"));
                                        ddl.Items.Remove(new ListItem("SELF- PRIMARY MEMBER", "SELF"));
                                    }
                                    cell.Controls.Add(ddl);
                                }
                                else
                                {
                                    TextBox txt = new TextBox();
                                    txt.ID = controls[i];
                                    dataRow = dt_Question.Rows[i];
                                    cell.ID = dataRow["QUESTION_SET_CODE"].ToString() + "-" + dataRow["QUESTION_CODE"].ToString();
                                    txt.CssClass = "form-control";
                                    if (txt.ID == "txtESponser")
                                        txt.Attributes.Add("onKeyPress", "return validChar(this.event)");
                                    cell.Controls.Add(txt);
                                }
                                rowc.Cells.Add(cell);

                                rowV.CssClass = "Validations";
                                cell = new TableCell();
                                cell.Text = "<span id='error" + controls[i] + "' style='color: red;'></span>";
                                rowV.Cells.Add(cell);
                            }
                            tblEducationDetails.Rows.Add(row);
                            tblEducationDetails.Rows.Add(rowc);
                            tblEducationDetails.Rows.Add(rowV);
                        }

                        count++;
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details failed to binding Education Details:" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Binding the Optional Cover fot Student Explorer product
        /// </summary>
        private void AddOptionalCovers()
        {
            try
            {
                ReligareQuotation quotation = (ReligareQuotation)Session["quotation"];
                string productId = quotation.ProductId;
                string typeId = quotation.ProductType.ToString();
                if (Convert.ToInt32(typeId) == (int)ReligareProducts.StudentExplore)
                {
                    DataTable dt_Question = ReligareQuotation.GetOptionalCoverQuestions(ref productId, ref typeId);
                    if (dt_Question != null && dt_Question.Rows.Count > 0)
                    {
                        TableRow row;
                        TableCell cell;
                        string[] status = "YES,NO".Split(',');
                        foreach (DataRow dr in dt_Question.Rows)
                        {
                            row = new TableRow();
                            cell = new TableCell();
                            Label lbl = new Label();
                            lbl.Text = dr["QUESION_DESCRIPTION"].ToString();
                            if (dr["QUESTION_SET_CODE"].ToString().Trim() == "STUEXPLPLUSWWMATERNITY")
                                lbl.ToolTip = "Cover medical expenses incurred in respect of the insured person for hospitalization for the delivery of child and vaccination of new born";
                            else if (dr["QUESTION_SET_CODE"].ToString().Trim() == "STUEXPLPLUSWWHIVAIDS")
                                lbl.ToolTip = "Cover medical expenses incurred for the treatment of AIDS";
                            else if (dr["QUESTION_SET_CODE"].ToString().Trim() == "VISIONCARE")
                                lbl.ToolTip = "Cover cost incurred for vision check up and spectacles damage for the insured person";
                            else if (dr["QUESTION_SET_CODE"].ToString().Trim() == "ADVTSPORTINURY")
                                lbl.ToolTip = "Cover medical expenses incurred for the insured person due to any sporting hazardous activity injury in case of hospitalization";
                            else if (dr["QUESTION_SET_CODE"].ToString().Trim() == "COMPLETEPED")
                                lbl.ToolTip = "Cover medical expenses for Pre Existing disease in life threatening medical condition upto 100% ";
                            else if (dr["QUESTION_SET_CODE"].ToString().Trim() == "TRAVALOPTIONALCOVER")
                                lbl.ToolTip = "Cover medical expenses incurred due to any self inflicted injury in case of hospitalization ";

                            cell.Controls.Add(lbl);
                            row.Cells.Add(cell);
                            for (int i = 0; i < status.Length; i++)
                            {
                                cell = new TableCell();
                                RadioButton rb = new RadioButton();
                                rb.ID = "rb_" + dr["QUESTION_SET_CODE"].ToString() + "_" + status[i];
                                rb.GroupName = dr["QUESTION_SET_CODE"].ToString();
                                rb.Checked = true;
                                rb.Text = status[i];
                                cell.Controls.Add(rb);
                                row.Cells.Add(cell);
                            }
                            tblOptionalCovers.Rows.Add(row);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details failed to binding Optional Cover Details:" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        /// <summary>
        /// Calling the ajax method to get the cities list using pincode.
        /// </summary>
        /// <param name="pincode"></param>
        /// <returns></returns>
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static List<string> GetCityCodes(string pincode)
        {
            List<string> citycodes = new List<string>();
            DataTable dt = ReligareHeader.GetCityCodes(pincode);
            if (dt != null && dt.Rows.Count > 0)
            {
                ReligareProposerDetails religareSample = new ReligareProposerDetails();
                citycodes.Add(dt.Rows[0]["statecd"].ToString());
                foreach (DataRow dr in dt.Rows)
                    citycodes.Add(dr["citycd"].ToString());
            }
            return citycodes;
        }
        private ReligarePassengers commanDetails(ref ReligarePassengers passengers)
        {
            try
            {
                passengers.Address1 = txtAddressLine1.Text;
                passengers.Address2 = txtAddressLine2.Text;
                passengers.Email = txtEmail.Text;
                passengers.Mobileno = txtMobileNo.Text;
                passengers.PinCode = txtPincode.Text;
                passengers.City = ddlCity.SelectedValue;
                passengers.State = txtState.Text;
                passengers.Nominee_name = txtNomineeName.Text;
                passengers.NomineeRelation = ddlNominee.SelectedItem.Text;
                passengers.PurposeVisit = ddlPurposeVisit.SelectedValue;
                return passengers;
            }
            catch { }
            return passengers;
        }

        /// <summary>
        /// Saving the religare header details
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnContinue_Click(object sender, EventArgs e)
        {
            try
            {
                Session["religareHeader"] = null;
                ReligareQuotation quotation = (ReligareQuotation)Session["quotation"];
                ReligareHeader religareHeader = new ReligareHeader();
                List<ReligarePassengers> obj_liPassengers = new List<ReligarePassengers>();
                ReligarePassengers religarePassengers = new ReligarePassengers();
                int paxcount = quotation.Travellers;
                // Religare Header Details.
                // Markup----- Handling Fee----------Discount Details
                religareHeader.MarkupValue = quotation.MarkupValue;
                religareHeader.Markup = quotation.Markup;
                religareHeader.MarkupType = string.IsNullOrEmpty(quotation.MarkupType) ? "P" : quotation.MarkupType;// quotation.MarkupType;
                religareHeader.HandlingFeeValue = quotation.HandlingFeeValue;
                religareHeader.HandlingFeeType = string.IsNullOrEmpty(quotation.HandlingFeeType) ? "P" : quotation.HandlingFeeType;// quotation.HandlingFeeType;
                religareHeader.HandlingFee = quotation.HandlingFee;
                religareHeader.Discount = quotation.Discount;
                religareHeader.DiscountType = string.IsNullOrEmpty(quotation.DiscountType)?"P": quotation.DiscountType;
                religareHeader.DiscountValue = quotation.DiscountValue;

                religareHeader.Produ_type = quotation.ProductType.ToString();
                religareHeader.Produ_id = quotation.ProductId;
                religareHeader.ProductName = quotation.ProductName;
                religareHeader.Startdate = quotation.StartDate;
                religareHeader.EndDate = quotation.EndDate;
                religareHeader.Days = quotation.Term;
                TimeSpan days = (Convert.ToDateTime(religareHeader.EndDate) - Convert.ToDateTime(religareHeader.Startdate));
                religareHeader.Business_type = ddlPurposeVisit.SelectedValue;
                religareHeader.NoofPax = paxcount;
                religareHeader.SumInsured_optioncode = quotation.SumInsuredId;
                religareHeader.AddFee = 0;                
                religareHeader.Premium_Amount = Convert.ToDecimal(quotation.Premium);
                //religareHeader.Discount = Convert.ToDecimal(ReligareQuotation.GetDiscountDetails(ref paxcount));
                //religareHeader.DiscountType = "P";
                religareHeader.Currency = Settings.LoginInfo.Currency;
                religareHeader.SourceCurrency = "INR";
                religareHeader.ROE = Settings.LoginInfo.AgentExchangeRates[Settings.LoginInfo.Currency];
                religareHeader.B2CMarkup = 0;
                religareHeader.B2CMarkupType = string.Empty;
                religareHeader.B2CMarkupValue = 0; 
                religareHeader.IsAccounted = false;
                religareHeader.Trans_type = "B2B";
                religareHeader.Agency_ref = string.Empty;
                religareHeader.Agent_id = quotation.AgentId;// Settings.LoginInfo.AgentId;
                religareHeader.Location_id = quotation.LocationId;
                religareHeader.Status = (int)InsuranceBookingStatus.Pending;
                religareHeader.Created_by = (int)Settings.LoginInfo.UserID;
                religareHeader.Remarks = "";
                religareHeader.VisitPurpose = ddlPurposeVisit.SelectedValue;
                religareHeader.Cover_type = "Individual";
                religareHeader.SumInsured = quotation.RangeId;
                religareHeader.Voucher_status = "P";
                religareHeader.TripType = quotation.TripType.ToUpper();
                religareHeader.Trip_from_india = chknationality.Checked ? true : false;
                religareHeader.IsPED = quotation.Ped == "0" ? true : false;
                if (quotation.TripType == "MULTI" )
                    religareHeader.MaxTripPeriod = string.IsNullOrEmpty(quotation.MaxTripPeriod) ? "0" : quotation.MaxTripPeriod;
                // Religare Passenger Common Details
                religarePassengers = new ReligarePassengers();
                religarePassengers = commanDetails(ref religarePassengers);
                religarePassengers.Nationality = ddlNationality.SelectedValue;                 
                religarePassengers.FirstName = txtFirstName.Text.Trim().ToUpper();
                religarePassengers.ResidenceProof = ddlresidenceProof.SelectedValue;
                religarePassengers.ProofDetails = txtProof.Text;
                religarePassengers.LastName = txtLastName.Text.Trim().ToUpper();
                religarePassengers.PassportNo = txtPassportNo.Text;
                religarePassengers.Title = ddlTitle.SelectedValue;
                religarePassengers.Dob = Convert.ToDateTime(txtdob.Text);
                religarePassengers.Gender = ddlTitle.SelectedValue == "Mr" ? "Male" : "Female";
                religarePassengers.Pax_id = 9;
                int counter = 0;
                // Religare Passenger Details 
                foreach (TableRow tr in tblInsuredDetails.Rows)
                {
                    if (!(tr.CssClass.StartsWith("Validations")))
                    {
                        foreach (TableCell tc in tr.Cells)
                        {
                            foreach (Control control in tc.Controls)
                            {
                                if (control is DropDownList)
                                {
                                    DropDownList dropDownList = (DropDownList)control;
                                    if (dropDownList.ID.StartsWith("ddlInsRelation"))
                                    {
                                        religarePassengers.TravellingAgeId = Convert.ToInt32(quotation.TravellerAges[counter].travellerAge);
                                        obj_liPassengers.Add(religarePassengers);
                                        religarePassengers = new ReligarePassengers();
                                        // Religare Passenger Common Details
                                        religarePassengers = commanDetails(ref religarePassengers);                                       
                                        religarePassengers.Pax_id = counter;
                                        religarePassengers.Relation = dropDownList.SelectedValue;                                       
                                        counter++;
                                    }
                                    else if (dropDownList.ID.StartsWith("ddlInsNationality"))
                                        religarePassengers.Nationality = dropDownList.SelectedValue;
                                    else if (dropDownList.ID.StartsWith("ddlInsTitle"))
                                    {
                                        religarePassengers.Title = dropDownList.SelectedValue;
                                        religarePassengers.Gender = dropDownList.SelectedValue == "Mr" ? "Male" : "Female"; 
                                    }
                                    else if (dropDownList.ID.StartsWith("ddlInsResidence"))
                                    {
                                        if (!(dropDownList.SelectedValue == "-1"))
                                            religarePassengers.ResidenceProof = dropDownList.SelectedValue;
                                    }
                                }
                                else if (control is TextBox)
                                {
                                    TextBox textBox = (TextBox)control;
                                    if (textBox.ID.StartsWith("txtInsPassportNo"))
                                        religarePassengers.PassportNo = textBox.Text;
                                    else if (textBox.ID.StartsWith("txtInsFirstName"))
                                        religarePassengers.FirstName = textBox.Text.Trim().ToUpper();
                                    else if (textBox.ID.StartsWith("txtInsLastName"))
                                        religarePassengers.LastName = textBox.Text.Trim().ToUpper();
                                    else if (textBox.ID.StartsWith("txtInsureDOB"))
                                        religarePassengers.Dob = Convert.ToDateTime(textBox.Text);
                                    else if (textBox.ID.StartsWith("txtInsProof"))
                                        religarePassengers.ProofDetails = textBox.Text;
                                }
                            }
                        }
                    }
                }
                religarePassengers.TravellingAgeId = Convert.ToInt32(quotation.TravellerAges[counter - 1].travellerAge);
                obj_liPassengers.Add(religarePassengers);
                int count = 0;
                List<ReligarePaxQuestions> obj_liRPQuestions = new List<ReligarePaxQuestions>();
                ReligarePaxQuestions religarePaxQuestions;
                string setcode = "";
                foreach (TableRow tr in tblQuestionire.Rows)
                {
                    religarePaxQuestions = new ReligarePaxQuestions();
                    foreach (TableCell tc in tr.Cells)
                    {
                        foreach (Control control in tc.Controls)
                        {
                            if (control is Label)
                            {
                                Label label = (Label)control;
                                religarePaxQuestions.Question_description = label.Text;
                                religarePaxQuestions.Question_set_code = label.ID;
                                setcode = label.ID;
                            }
                            if (control is RadioButton)
                            {
                                RadioButton rb = (RadioButton)control;
                                if (rb.Text.ToUpper() == "NO")
                                {
                                    religarePaxQuestions.Question_status = rb.Checked ? false : true;
                                    religarePaxQuestions.Question_other_remarks = "";
                                    religarePaxQuestions.Question_code = rb.GroupName;
                                    religarePaxQuestions.Question_id = (count).ToString();
                                    religarePaxQuestions.Question_category = "yesno";
                                    obj_liRPQuestions.Add(religarePaxQuestions);
                                    count = count + 1;
                                }
                            }
                            else if (control is CheckBox)
                            {
                                CheckBox cb = (CheckBox)control;
                                religarePaxQuestions.Question_code = cb.ID.Split('_')[1];
                                religarePaxQuestions.Pax_id = Convert.ToInt32(cb.ID.Split('_')[2]);
                                religarePaxQuestions.Question_id = (count).ToString();
                                religarePaxQuestions.Question_other_remarks = "CB";
                                religarePaxQuestions.Question_status = cb.Checked ? true : false;
                                obj_liRPQuestions.Add(religarePaxQuestions);
                                if (religarePaxQuestions.Pax_id < religareHeader.NoofPax)
                                {
                                    religarePaxQuestions = new ReligarePaxQuestions();
                                    religarePaxQuestions.Question_set_code = obj_liRPQuestions[obj_liRPQuestions.Count - 1].Question_set_code;
                                    religarePaxQuestions.Question_other_remarks = obj_liRPQuestions[obj_liRPQuestions.Count - 1].Question_other_remarks;
                                }
                                count = count + 1;
                            }
                            else if (control is TextBox)
                            {
                                string trid = tr.CssClass.Replace("tr", "txt_");
                                TextBox textBox = (TextBox)control;
                                string ss = textBox.ID.Remove(textBox.ID.Length - 2, 2);
                                int paxid = Convert.ToInt32(textBox.ID.Split('_')[2]);
                                if (textBox.ID.StartsWith("txt_otherDiseasesDescription"))
                                {
                                    string str = textBox.ID.Split('_')[1].ToString();
                                    var ids = from objli in obj_liRPQuestions
                                              where objli.Pax_id == paxid && objli.Question_set_code == "PEDotherDetailsTravel"
                                              select objli.Question_id;
                                    foreach (string id in ids)
                                        obj_liRPQuestions[Convert.ToInt32(id)].Question_other_remarks = textBox.Text;
                                }
                                else if (textBox.ID.Remove(textBox.ID.Length - 2, 2).Contains(trid))
                                {
                                    religarePaxQuestions = new ReligarePaxQuestions();
                                    religarePaxQuestions.Question_code = setcode.Split('_')[2];
                                    religarePaxQuestions.Question_set_code = setcode.Split('_')[1];
                                    religarePaxQuestions.Question_other_remarks = textBox.Text;
                                    religarePaxQuestions.Question_status = string.IsNullOrEmpty(textBox.Text) ? false : true;
                                    obj_liRPQuestions[count - 1].Question_status = string.IsNullOrEmpty(textBox.Text) ? false : true;
                                    religarePaxQuestions.Pax_id = Convert.ToInt32(textBox.ID.Split('_')[2]);
                                    religarePaxQuestions.Question_id = count.ToString();
                                    obj_liRPQuestions.Add(religarePaxQuestions);
                                    count++;
                                }
                            }
                        }
                    }
                }
                if (Convert.ToInt32(religareHeader.Produ_type) == (int)ReligareProducts.StudentExplore)
                {
                    foreach (TableRow tr in tblEducationDetails.Rows)
                    {
                        foreach (TableCell cell in tr.Cells)
                        {
                            foreach (Control control in cell.Controls)
                            {
                                religarePaxQuestions = new ReligarePaxQuestions();
                                if (control is TextBox)
                                {
                                    TextBox txt = (TextBox)control;
                                    if (!string.IsNullOrEmpty(cell.ID))
                                    {
                                        if (txt.ID == "txtEIName")
                                            religareHeader.UniversityName = txt.Text;
                                        else if (txt.ID == "txtECourse")
                                            religareHeader.CourseDetails = txt.Text;
                                        else if (txt.ID == "txtEIAddress")
                                            religareHeader.UniversityAddress = txt.Text;
                                        else if (txt.ID == "txtEICountry")
                                            religareHeader.UniversityAddress += txt.Text;
                                        else if (txt.ID == "txtESponser")
                                            religareHeader.SponsorName = txt.Text;
                                        else if (txt.ID == "txtESDob" && !(string.IsNullOrEmpty(txt.Text)))
                                            religareHeader.SponsorDOB = Convert.ToDateTime(txt.Text);

                                        religarePaxQuestions.Question_set_code = cell.ID.Split('-')[0];
                                        religarePaxQuestions.Question_code = cell.ID.Split('-')[1];
                                        religarePaxQuestions.Question_status = true; religarePaxQuestions.Question_other_remarks = religarePaxQuestions.Question_code != "dateofBirth" ? txt.Text : Convert.ToDateTime(txt.Text).ToString("dd/MM/yyyy");
                                        religarePaxQuestions.Question_category = "student";
                                        obj_liRPQuestions.Add(religarePaxQuestions);
                                    }
                                }
                                else if (control is DropDownList)
                                {
                                    DropDownList dropDown = (DropDownList)control;
                                    if (!string.IsNullOrEmpty(cell.ID))
                                    {
                                        religareHeader.RelationshipToStudent = dropDown.SelectedValue;
                                        religarePaxQuestions.Question_set_code = cell.ID.Split('-')[0];
                                        religarePaxQuestions.Question_code = cell.ID.Split('-')[1];
                                        religarePaxQuestions.Question_status = true;
                                        religarePaxQuestions.Question_other_remarks = dropDown.SelectedValue;
                                        religarePaxQuestions.Question_category = "student";
                                        obj_liRPQuestions.Add(religarePaxQuestions);
                                    }
                                }
                            }
                        }
                    }
                    religareHeader.Addons = "";
                    foreach (TableRow tr in tblOptionalCovers.Rows)
                    {
                        foreach (TableCell cell in tr.Cells)
                        {
                            foreach (Control control in cell.Controls)
                            {
                                religarePaxQuestions = new ReligarePaxQuestions();
                                if (control is RadioButton)
                                {
                                    RadioButton rb = (RadioButton)control;
                                    if (rb.Text == "YES" && rb.Checked)
                                        religareHeader.Addons += rb.ID.Split('_')[1] + ",";
                                }
                            }
                        }
                    }
                }
                religareHeader.ReligarePassengers = obj_liPassengers;
                religareHeader.ReligarePaxQuestions = obj_liRPQuestions;
                Session["religareHeader"] = religareHeader;
                Response.Redirect("ReligarePaymentConfirmation.aspx", false);
            }
            catch (Exception ex)
            {
                System.Threading.Thread.ResetAbort();
                Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details Failed to saving/Updating the header Details" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        //taking the currency value 
        protected string CTCurrencyFormat(object currency, object AgentId)
        {
            AgentMaster agency;
            if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
            {
                agency = new AgentMaster(Convert.ToInt32(AgentId));
                return agency.AgentCurrency + " " + Convert.ToDecimal(0).ToString("N" + agency.DecimalValue);
            }
            else
            {
                agency = new AgentMaster(Convert.ToInt32(AgentId));
                return agency.AgentCurrency + " " + Convert.ToDecimal(currency).ToString("N" + agency.DecimalValue);
            }
        }
        #region test code not necessary
        /// <summary>
        /// Enable/Disable the other questions details.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void cb_CheckedChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        CheckBox cb = (CheckBox)sender;
        //        string id = cb.ID.Split('_')[2];
        //        bool status = false;
        //        foreach (TableRow tr in tblQuestionire.Rows)
        //        {
        //            foreach (TableCell tc in tr.Cells)
        //            {
        //                foreach (Control control in tc.Controls)
        //                {
        //                    if (control is RadioButton)
        //                    {
        //                        RadioButton rb = (RadioButton)control;
        //                        if (rb.ID.EndsWith("Yes") && rb.Checked)
        //                            status = true;
        //                    }
        //                    else if (control is TextBox)
        //                    {
        //                        TextBox tb = (TextBox)control;
        //                        if (status == true && tb.ID.Split('_')[2] == id && tr.Cells[0].ID == "otherDiseasesDescription")
        //                        {
        //                            if (!(cb.Checked))
        //                            {
        //                                tb.Text = "";
        //                                tb.Enabled = false;
        //                            }
        //                            else
        //                                tb.Enabled = true;
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        System.Threading.Thread.ResetAbort();
        //        Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details Failed to Enable/Disable the other quetion details textbox control" + ex.ToString(), Request["REMOTE_ADDR"]);
        //    }
        //}
        ///// <summary>
        ///// enable/disable the controls based on selection radio button.
        ///// </summary>
        ///// <param name="sender"></param>
        ///// <param name="e"></param>
        //protected void rb_CheckedChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        RadioButton radioButton = (RadioButton)sender;
        //        bool status = false;
        //        foreach (TableRow tr in tblQuestionire.Rows)
        //        {
        //            foreach (TableCell tc in tr.Cells)
        //            {
        //                foreach (Control control in tc.Controls)
        //                {
        //                    if (control is RadioButton)
        //                    {
        //                        RadioButton rb = (RadioButton)control;
        //                        if (rb.ID.EndsWith("Yes") && rb.Checked)
        //                            status = true;
        //                    }
        //                    else if (control is CheckBox)
        //                    {
        //                        CheckBox cb = (CheckBox)control;
        //                        if (status == false)
        //                        {
        //                            cb.Checked = false;
        //                            cb.Enabled = false;
        //                        }
        //                    }
        //                    else if (control is DropDownList)
        //                    {
        //                        DropDownList ddl = (DropDownList)control;
        //                        if (status == false)
        //                        {
        //                            string cid = radioButton.ID.Replace("rb_", "ddl_").Replace("No", "");
        //                            if (ddl.ID.StartsWith(cid))
        //                                ddl.ClearSelection();
        //                        }
        //                    }
        //                    else if (control is TextBox)
        //                    {
        //                        TextBox tb = (TextBox)control;
        //                        if (status == false)
        //                        {
        //                            tb.Enabled = false;
        //                            tb.Text = "";
        //                        }

        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details Failed to changing the question status" + ex.ToString(), Request["REMOTE_ADDR"]);
        //    }
        //}
        /// <summary>
        /// enable /disable the controls selection of choice.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        //protected void ddldisease_SelectedIndexChanged(object sender, EventArgs e)
        //{
        //    try
        //    {
        //        DropDownList ddl = (DropDownList)sender;
        //        string id = ddl.ID.Split('_')[2];
        //        bool status = false;
        //        foreach (TableRow tr in tblQuestionire.Rows)
        //        {
        //            foreach (TableCell tc in tr.Cells)
        //            {
        //                foreach (Control control in tc.Controls)
        //                {
        //                    if (control is RadioButton)
        //                    {
        //                        RadioButton rb = (RadioButton)control;
        //                        if (rb.ID.EndsWith("Yes") && rb.Checked)
        //                            status = true;
        //                    }
        //                    else if (control is CheckBox)
        //                    {
        //                        if (ddl.ID.StartsWith("ddl_pedYesNo"))
        //                        {
        //                            CheckBox cb = (CheckBox)control;
        //                            if (status == true)
        //                            {
        //                                if (cb.ID.StartsWith("cb_") && cb.ID.EndsWith(id))
        //                                {
        //                                    if (ddl.SelectedValue == "1")
        //                                        cb.Enabled = true;
        //                                    else
        //                                        cb.Enabled = false;
        //                                }
        //                            }
        //                            else
        //                            {
        //                                if (cb.ID.StartsWith("cb_") && cb.ID.EndsWith(id))
        //                                {
        //                                    cb.Enabled = false;
        //                                    cb.Checked = false;
        //                                }
        //                            }
        //                        }
        //                    }
        //                    else if (control is TextBox)
        //                    {
        //                        TextBox textBox = (TextBox)control;
        //                        string cid = ddl.ID.Replace("ddl", "txt");
        //                        if (status == true)
        //                        {
        //                            if (textBox.ID == cid)
        //                            {
        //                                if (ddl.SelectedValue == "1")
        //                                    textBox.Enabled = true;
        //                                else
        //                                {
        //                                    textBox.Enabled = false;
        //                                    textBox.Text = "";
        //                                }
        //                            }
        //                        }
        //                    }
        //                }
        //            }
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        Audit.Add(EventType.Exception, Severity.High, 0, "Proposer Details Failed to Enable/Disable the textbox controls " + ex.ToString(), Request["REMOTE_ADDR"]);
        //    }
        //}
        #endregion


    }
}
