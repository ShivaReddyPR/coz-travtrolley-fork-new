﻿using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class ibytalogin : System.Web.UI.Page
    {
      
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {

                lblForgotPwd.Text = string.Empty;
                if (!IsPostBack) InitializeControls();
                lblError.Text = string.Empty;
                if (Request.QueryString["errMessage"] != null)
                {
                    lblForgotPwd.Text = Request.QueryString["errMessage"];
                }
            }

            catch (Exception ex)
            {
                lblError.Text = ex.Message;
            }
        }

        private void InitializeControls()
        {
            try
            {
                if (Settings.LoginInfo != null)
                {
                    Session.Abandon();
                   
                }
           
            }
            catch { throw; }

        }
        protected void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {

                bool isAuthenticated = UserMaster.IsAuthenticatedUser(txtLoginName.Text.Trim(), txtPassword.Text, Utility.ToInteger(ddlCompany.SelectedValue));
                if (isAuthenticated)
                {
                  
                    if (!string.IsNullOrEmpty(Settings.LoginInfo.AgentTheme))
                        Session["themeName"] = Settings.LoginInfo.AgentTheme;
                    else Session["themeName"] = "Default";

                 

                    string[] agentLeadProduct = Settings.LoginInfo.AgentProduct.Split(',');//, StringSplitOptions.RemoveEmptyEntries);
                  
                    if (agentLeadProduct.Length > 0)
                    {

                        Array.Sort(agentLeadProduct);

                        if (!string.IsNullOrEmpty(agentLeadProduct[0]))
                        {
                            switch (agentLeadProduct[0])
                            {
                                case "1":
                                    if (Settings.roleFunctionList.Exists(r => r.ToLower().Contains("hotelsearch?source=flight")))
                                        Response.Redirect("HotelSearch?source=Flight", false);
                                    else
                                        Response.Redirect("Index", false);
                                    //Response.Redirect("UnauthorizedAccess", false);

                                    break;
                                case "2":
                                    if (Settings.LoginInfo.Currency == "INR" && Settings.roleFunctionList.Exists(r => r.ToLower().Contains("hotelsearch?source=hotel")))
                                        Response.Redirect("HotelSearch?source=Hotel", false);
                                    else if (Settings.roleFunctionList.Exists(r => r.ToLower().Contains("findhotels")))
                                        Response.Redirect("findHotels", false);
                                    else
                                        Response.Redirect("Index", false);
                                    break;
                                case "3":
                                    if (Settings.roleFunctionList.Exists(r => r.ToLower().Contains("packageMaster")))
                                        Response.Redirect("PackageMaster", false);
                                    else
                                        Response.Redirect("Index", false);
                                    break;
                                case "4":
                                    if (Settings.roleFunctionList.Exists(r => r.ToLower().Contains("activitymaster")))
                                        Response.Redirect("ActivityMaster", false);
                                    else
                                        Response.Redirect("Index", false);
                                    break;
                                case "5":
                                    if (Settings.roleFunctionList.Exists(r => r.ToLower().Contains("insurance")))
                                        Response.Redirect("Insurance", false);
                                    else
                                        Response.Redirect("Index", false);
                                    break;
                                default:
                                    Response.Redirect("Index", false);// Default landing page for All roles
                                    break;

                            }
                        }
                        else Response.Redirect("Index", false);// Default landing page for All roles
                    }
                    else Response.Redirect("Index", false);// Default landing page for All roles
                }
            }
            catch (Exception ex)
            {
                lblError.Text = ex.Message;
                lblError.Visible = true;
               

            }

        }
        protected void btnGetPassword_Click(object sender, EventArgs e)
        {
            try
            {
                string pagePath = Request.Url.Scheme + "://" + Request.ServerVariables["HTTP_HOST"] + Page.Request.Path;
                string[] pageName = pagePath.Split('/');
                pagePath = string.Empty;
                for (int i = 0; i < pageName.Length - 1; i++)
                {
                    pagePath = pagePath + pageName[i] + "/";
                }
                string guid = UserMaster.RequestPasswordChange(txtEmailId.Text.Trim());
                pagePath = pagePath + "ResetPassword?requestId=" + guid;

               
                #region Sending Email
               

                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                
                toArray.Add(txtEmailId.Text.Trim());
               
                string link = "<a href='" + pagePath + "'>" + pagePath + "</a>";
                string message = ConfigurationManager.AppSettings["changePassowrdMessage"].Replace("%link%", link);
                try
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Change your password", message, null);
                    lblForgotPwd.Text = "An email has been sent to you with a link to reset your password!";
                }
                catch (System.Net.Mail.SmtpException)
                {
                    //CT.Core1.Audit.Add(CoreLogic.EventType.Email, CoreLogic.Severity.Normal, 0, "Smtp is unable to send the message", "");
                }
              
                #endregion

            }
            catch (Exception ex)
            {
                lblForgotPwd.Text = ex.Message.ToString();
            }
        }
    }
}
