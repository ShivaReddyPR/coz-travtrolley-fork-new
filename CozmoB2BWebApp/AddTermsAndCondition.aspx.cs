﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Visa;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;



public partial class AddTermsAndCondition : CT.Core.ParentPage
{
    protected bool isAdmin = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        AuthorizationCheck();
        if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
        {
            isAdmin = true;
        }
        if (!IsPostBack)
        {
            Page.Title = "Add Terms And Condition";
            BindAgent();
            ddlCountry.DataSource = Country.GetCountryList(); //VisaCountry.GetActiveVisaCountryList();
            ddlCountry.DataTextField = "key";
            ddlCountry.DataValueField = "value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, "Select");
            if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
            {
                int termId;
                bool isnumerice = int.TryParse(Request.QueryString[0].Trim(), out termId);
                if (!isnumerice)
                {
                    string url = "<h1> Bad request url</h1><br/><a href=\"" + Request.Url.Host + "\">Click Here</a> to go to home page"; 
                    Response.Write(url.ToString());
                    Response.End();

                }
                try
                {
                    VisaTermsAndCondition visaTerms = new VisaTermsAndCondition(termId);
                    if (visaTerms != null)
                    {
                        editor.Text= visaTerms.Description;
                        ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(visaTerms.CountryCode));
                        ddlAgent.SelectedIndex = ddlAgent.Items.IndexOf(ddlAgent.Items.FindByValue(Convert.ToString(visaTerms.AgentId)));
                        ddlAgent.Enabled = false;
                        btnSave.Text = "Update";
                        Page.Title = "Update Terms And Condition";
                        lblStatus.Text = "Update Visa Terms And Condition";
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaTermsAndCondition,Err:" + ex.Message, "");
                }

            }
            else
            {
                if (!isAdmin)
                {
                    ddlAgent.SelectedValue = Settings.LoginInfo.AgentId.ToString();
                    ddlAgent.Enabled = false;
                }
            }
        }

    }
    private void BindAgent()
    {
        try
        {
            int agentId = 0;
            if (Settings.LoginInfo.AgentId > 1) agentId = Settings.LoginInfo.AgentId;
            string agentType = (Settings.LoginInfo.AgentType.ToString() != null ? Settings.LoginInfo.AgentType.ToString() : "BASEAGENT");
            ddlAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataValueField = "agent_Id";
            ddlAgent.DataTextField = "agent_Name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("Select Agent", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            VisaTermsAndCondition visaTerms = new VisaTermsAndCondition();
            visaTerms.AgentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            visaTerms.CountryCode = ddlCountry.SelectedValue;
            visaTerms.Description = editor.Text;
            if (btnSave.Text == "Update")
            {
                if (Request.QueryString.Count > 0 && Request.QueryString[0] != "")
                {

                    visaTerms.TermsId = Convert.ToInt32(Request.QueryString[0]);
                    visaTerms.LastModifiedBy = (int)Settings.LoginInfo.UserID;

                }


            }
            else
            {

                visaTerms.IsActive = true;
                visaTerms.CreatedBy = (int)Settings.LoginInfo.UserID;

            }
            visaTerms.Save();
           
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:AddVisaTermsAndCondition,Err:" + ex.Message, "");
        }
        Response.Redirect("VisaTermsAndConditionList.aspx?Country=" + ddlCountry.SelectedValue+ "&AgencyId=" + ddlAgent.SelectedItem.Value);

    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }


}
