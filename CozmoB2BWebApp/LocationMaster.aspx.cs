using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;

public partial class LocationMasterUI :CT.Core.ParentPage// System.Web.UI.Page
{
    private string LOCATION_MAP_SESSION = "_TranxLocationMap";
    private string LOCATION_SEARCH_SESSION = "_LocationSearchList";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;

        try
        {
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {

                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
                lblSuccessMsg.Text = string.Empty;

            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);

        }
    }


    private void InitializePageControls()
    {
        try
        {
            Clear();
            BindAgent();
            BindSubAgent();
            BindCurrency();
            BindCountries();

        }
        catch { throw; }
    }
 
    private LocationMaster CurrentObject
    {
        get
        {
            return (LocationMaster)Session[LOCATION_MAP_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(LOCATION_MAP_SESSION);
            }
            else
            {
                Session[LOCATION_MAP_SESSION] = value;
            }

        }

    }

    private void Clear()
    {
        try
        {
            txtAddress.Text = string.Empty;
            txtCode.Text = string.Empty;
            txtName.Text = string.Empty;
            txtTerms.Text = string.Empty;
            //txtMapLocation.Text = string.Empty;
            lblSuccessMsg.Text = string.Empty;
            lblSuccessMsg.Visible = false;
            ddlCurrency.SelectedIndex = 1;
            btnSave.Text = "Save";
            ddlcountrylist.SelectedIndex = 0;
            ddlstatelist.Items.Clear();
            ddlstatelist.Items.Insert(0, new ListItem("--Select State--", "0"));
            txtParentcode.Text = string.Empty;
            txtgstnumber.Text = string.Empty;
			txtMapCode.Text = string.Empty;
            CurrentObject = null;

            if (Settings.LoginInfo.AgentId > 1)
            {
                ListItem item = ddlCurrency.Items.FindByText(Settings.LoginInfo.Currency);
                if (item != null)
                {
                    ddlCurrency.ClearSelection();
                    item.Selected = true;
                }
            }

            if (Settings.LoginInfo.MemberType == MemberType.SUPER || Settings.LoginInfo.MemberType == MemberType.ADMIN)
            {
                ddlAgent.SelectedIndex = 0;
                ddlAgent.Enabled = true;
            }
            else
            {
                ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
            ddlSubAgent.Items.Clear();
        }
        catch
        {
            throw;
        }
    }

    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[LOCATION_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["LOCATION_ID"] };
            Session[LOCATION_SEARCH_SESSION] = value;
        }
    }

    private void Save()
    {
        try
        {
            LocationMaster location;
            if (CurrentObject == null)
            {
                location = new LocationMaster();
            }
            else
            {
                location = CurrentObject;
            }

            location.Code = txtCode.Text.Trim();
            location.Name = txtName.Text.Trim();
            string locationName = txtName.Text.Trim();
            location.AgentId = (ddlSubAgent.SelectedIndex > 0 ? Utility.ToInteger(ddlSubAgent.SelectedValue) : Utility.ToInteger(ddlAgent.SelectedItem.Value));
            location.Address = txtAddress.Text.Trim();
            location.Terms = txtTerms.Text.Trim();
            location.Currency = ddlCurrency.SelectedItem.Value;
            location.Status = Settings.ACTIVE;
            location.CreatedBy = Settings.LoginInfo.UserID;
            location.CountryCode = Utility.ToString(ddlcountrylist.SelectedValue);
            location.StateId = Convert.ToInt32(ddlstatelist.SelectedValue);
            location.ParentCode = txtParentcode.Text.Trim();
            location.GstNumber = txtgstnumber.Text.Trim();
            
			location.LocationMapCode = txtMapCode.Text.Trim();
            location.Save();
            Clear();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage("Locations ", locationName, (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));


        }
        catch
        {
            throw;
        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

   protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

  
  

    private void BindAgent()
    {
        try
        {
            int agentId = Settings.LoginInfo.AgentId;
            if (agentId <= 1) agentId = 0;
            ddlAgent.DataSource = AgentMaster.GetList(1, Settings.LoginInfo.AgentType.ToString(), agentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--select Agent--", "-1"));
            if (Settings.LoginInfo.MemberType == MemberType.SUPER || Settings.LoginInfo.MemberType == MemberType.ADMIN)
            {
                ddlAgent.SelectedIndex = 0;
                ddlAgent.Enabled = true;
            }
            else
            {
                ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
        }
        catch { throw; }
    }

    private void BindSubAgent()
    {
        try
        {
            int agentId = Convert.ToInt32(ddlAgent.SelectedValue);
            AgentMaster agent = new AgentMaster(agentId);
            DataTable dt = null;
            if (agent.AgentType == (int)AgentType.BaseAgent)
            {
                dt = AgentMaster.GetList(1, "B2B-ALL", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            else if (agent.AgentType == (int)AgentType.Agent)
            {
                dt = AgentMaster.GetList(1, "B2B-ALL", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            else if (agent.AgentType == (int)AgentType.B2B)
            {
                dt = AgentMaster.GetList(1, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            ddlSubAgent.DataSource = dt;
            ddlSubAgent.DataValueField = "agent_id";
            ddlSubAgent.DataTextField = "agent_name";
            ddlSubAgent.DataBind();
            ddlSubAgent.Items.Insert(0, new ListItem("--select Sub Agent--", "-1"));
            if (Settings.LoginInfo.AgentType != AgentType.B2B2B)
            {
                ddlSubAgent.SelectedIndex = 0;
                ddlSubAgent.Enabled = true;
            }
            else
            {
                ddlSubAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
                ddlSubAgent.Enabled = false;
            }
        }
        catch { throw; }
    }
    private void BindCurrency()
    {
        try
        {
            ddlCurrency.DataSource = CurrencyMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            ddlCurrency.DataValueField = "CURRENCY_CODE";
            ddlCurrency.DataTextField = "CURRENCY_CODE";
            ddlCurrency.DataBind();
            ddlCurrency.Items.Insert(0, new ListItem("--Select Currency--", "0"));
            ddlCurrency.SelectedIndex = 1;
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
   
    private void BindCountries()
    {
        try
        {
            ddlcountrylist.DataSource = CT.Core.Country.GetCountryList();
            ddlcountrylist.DataTextField = "KEY";
            ddlcountrylist.DataValueField = "VALUE";
            ddlcountrylist.DataBind();
            ddlcountrylist.Items.Insert(0, new ListItem("--Select Country--", "0"));
            ddlcountrylist.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
   
 

   # region Content Search
   private void bindSearch()
   {
       try
       {
           LoginInfo loginfo = Settings.LoginInfo;
           int agentId = loginfo.AgentId;
           if(Utility.ToInteger(ddlAgent.SelectedItem.Value)>1)
               agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
           else if(loginfo.AgentType==AgentType.BaseAgent || loginfo.AgentType==AgentType.Agent)
               agentId = -1;
           
           

           DataTable dt = LocationMaster.GetList(agentId, ListStatus.Long, RecordStatus.All, loginfo.AgentType.ToString());
           SearchList = dt;
           CommonGrid g = new CommonGrid();
           g.BindGrid(gvSearch, dt);
       }
       catch { throw; }
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
   protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
   {
        try
        {
            Clear();
            long vsId = Utility.ToLong(gvSearch.SelectedValue);
            Edit(vsId);
            this.Master.HideSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
   protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

           string[,] textboxesNColumns ={{ "HTtxtCode", "LOCATION_CODE" },{"HTtxtName", "LOCATION_NAME" }, 
            { "HTtxtStatus", "LOCATION_STATUS" },{ "HTtxtAddress", "LOCATION_ADDRESS" },
			{ "HTtxtMapCode", "map_code"} };
           CommonGrid g = new CommonGrid();
           g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
       }
       catch (Exception ex)
       {
           Label lblMasterError = (Label)this.Master.FindControl("lblError");
           lblMasterError.Visible = true;
           lblMasterError.Text = ex.Message;
           Utility.WriteLog(ex, this.Title);
       }
   }

    private void Edit(long id)
    {
        try
        {
            LocationMaster location = new LocationMaster(id);
            CurrentObject = location;
            txtCode.Text = Utility.ToString(location.Code);
            if (location.ParentAgentId > 1)
            {
                ddlAgent.SelectedValue = Utility.ToString(location.ParentAgentId);
                BindSubAgent();
                try
                {
                    ddlSubAgent.SelectedValue = location.AgentId.ToString();
                }
                catch { }
            }
            else
            {
                ddlAgent.SelectedValue = location.AgentId.ToString();
            }
           
            txtName.Text = location.Name;
            txtAddress.Text = location.Address;
            txtTerms.Text = location.Terms;
            ddlCurrency.SelectedValue = location.Currency;
            if (!string.IsNullOrEmpty(location.CountryCode))
            {
                ddlcountrylist.SelectedValue = Utility.ToString(location.CountryCode);
                BindStates();
                ddlstatelist.SelectedValue = Utility.ToString(location.StateId);
                txtParentcode.Text = location.ParentCode;
                txtgstnumber.Text = location.GstNumber;
            }
            
			txtMapCode.Text = location.LocationMapCode;
            btnSave.Text = "Update";
            btnClear.Text = "Clear";
            
        }
        catch
        {
            throw;
        }
    }

    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindSubAgent();
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
            AgentMaster agent = new AgentMaster(Utility.ToLong(agentId));
            ListItem item = ddlCurrency.Items.FindByText(agent.AgentCurrency);
            if (item != null)
            {
                ddlCurrency.ClearSelection();
                item.Selected = true;
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }


    
    protected void ddlSubAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlSubAgent.SelectedIndex > 0)
            {
                int agentId = Utility.ToInteger(ddlSubAgent.SelectedItem.Value);
                if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
                AgentMaster agent = new AgentMaster(Utility.ToLong(agentId));
                ListItem item = ddlCurrency.Items.FindByText(agent.AgentCurrency);
                if (item != null)
                {
                    ddlCurrency.ClearSelection();
                    item.Selected = true;
                }
            }
            else
            {
                int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
                if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
                AgentMaster agent = new AgentMaster(Utility.ToLong(agentId));
                ListItem item = ddlCurrency.Items.FindByText(agent.AgentCurrency);
                if (item != null)
                {
                    ddlCurrency.ClearSelection();
                    item.Selected = true;
                }
            }
        }
        catch(Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    #endregion

    protected void ddlcountrylist_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindStates();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void BindStates()
    {
        try
        {
            ddlstatelist.DataSource = LocationMaster.GetTourRegionList(ListStatus.Short, RecordStatus.Activated, "S", ddlcountrylist.SelectedValue);
            ddlstatelist.DataValueField = "rgn_id";
            ddlstatelist.DataTextField = "rgn_name";
            ddlstatelist.DataBind();
            ddlstatelist.Items.Insert(0, new ListItem("--Select State--", "0"));
            ddlstatelist.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

   
}


