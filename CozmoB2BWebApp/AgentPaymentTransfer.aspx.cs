using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;

public partial class AgentPaymentTransferUI :CT.Core.ParentPage// System.Web.UI.Page
{
    private string AGENT_PAYMENTTRA_SESSION = "_AgentPayemntTraDetails";
    // private string LOCATION_MAPPING_SESSION = "_LocationMappingList";

    private string AGENT_PAYMENTTRA_SEARCH_SESSION = "_AgentSearchPaymentTraDetails";
    //private string VISA_SETTLEMENT_DETAILS_SESSION = "_SettlementDetailsList";

        
    //private enum PageMode
    //{
    //    Add = 1,
    //    Update = 2
    //}
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            // Session["RCPT_TICKET_ID"] = "aaaa";
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
                Clear();
                lblSuccessMsg.Text = string.Empty;
            }
            //StartupScript(this.Page, " showHidMode(" + ddlSettlementMode.SelectedItem.Value + ")", " showHidMode");
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void InitializePageControls()
    {
        try
        {
            
            //Clear();
            BindAgent();
            //BindPaymentMode();
            BindSubAgent(Utility.ToInteger(ddlAgent.SelectedItem.Value));
            BindAgentCurrency("");
            BindSubAgentCurrency("");
            //dcDocDate.Value = DateTime.Now;
            //dcChequeDate.Value = DateTime.Now;
            // string uploadPath = "test";
            //string rootFolder = @"Uploads\\AgentSlip\\";
            // string rootFolder = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["DEPOSITE_SLIP"]);
            //uploadPath = rootFolder + "\\" + uploadPath + "\\";

            // docPaySlip.SavePath = rootFolder;
        }
        catch { throw; }
    }

    private void BindAgent()
    {
        try
        {
            DataTable dt = AgentMaster.GetList(1, Settings.LoginInfo.AgentType.ToString(), Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            DataView dv = dt.DefaultView;
            //dv.RowFilter = "AGENT_TYPE IN ('B2B')";
            ddlAgent.DataSource = dv.ToTable();
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "-1"));
            ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);

            //if (Settings.LoginInfo.MemberType == MemberType.SUPER)
            //{
            //    ddlAgent.SelectedIndex = 0;
            //    ddlAgent.Enabled = true;
            //}
            //else
            //{
            //    ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
            //    ddlAgent.Enabled = false;
            //}

        }
        catch { throw; }
    }

    private void BindSubAgent(int agentId)
    {
        try
        {
            AgentMaster agent = new AgentMaster(agentId);
            DataTable dt = null;
            if (agent.AgentType == (int)AgentType.BaseAgent)
            {
                dt = AgentMaster.GetList(1, "B2B-ALL", agentId, ListStatus.Short, RecordStatus.Activated);// Load B2B and B2B2B 
            }
            else  if (agent.AgentType == (int)AgentType.Agent)
            {
                dt = AgentMaster.GetList(1, "B2B-ALL", agentId, ListStatus.Short, RecordStatus.Activated);// Load B2B and B2B2B 
            }
            else if (agent.AgentType == (int)AgentType.B2B)
            {
                dt = AgentMaster.GetList(1, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);
            }

            ddlSubAgent.DataSource = dt;
            ddlSubAgent.DataValueField = "agent_id";
            ddlSubAgent.DataTextField = "agent_name";
            ddlSubAgent.DataBind();
            ddlSubAgent.Items.Insert(0, new ListItem("--Select SubAgent--", "-1"));


            //ddlSubAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);

            //if (Settings.LoginInfo.MemberType == MemberType.SUPER)
            //{
            //    ddlAgent.SelectedIndex = 0;
            //    ddlAgent.Enabled = true;
            //}
            //else
            //{
            //    ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
            //    ddlAgent.Enabled = false;
            //}

        }
        catch { throw; }
    }

    private void BindAgentCurrency(string currency)
    {
        try
        {
            ddlAgentCurrency.DataSource = CurrencyMaster.GetList(currency, ListStatus.Short, RecordStatus.Activated);
            ddlAgentCurrency.DataValueField = "CURRENCY_ID";
            ddlAgentCurrency.DataTextField = "CURRENCY_CODE";
            ddlAgentCurrency.DataBind();
            ddlAgentCurrency.Items.Insert(0, new ListItem("--Select Currency--", "0"));
            ddlAgentCurrency.SelectedIndex = 1;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }

    private void BindSubAgentCurrency(string currency)
    {
        try
        {   
            ddlSubAgentCurrency.DataSource = CurrencyMaster.GetList(currency, ListStatus.Short, RecordStatus.Activated);            
            ddlSubAgentCurrency.DataValueField = "CURRENCY_ID";
            ddlSubAgentCurrency.DataTextField = "CURRENCY_CODE";
            ddlSubAgentCurrency.DataBind();
            //ddlSubAgentCurrency.Items.Insert(0, new ListItem("--Select Currency--", "0"));
            ddlSubAgentCurrency.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }

    private AgentPaymentDetails CurrentObject
    {
        get
        {
            return (AgentPaymentDetails)Session[AGENT_PAYMENTTRA_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(AGENT_PAYMENTTRA_SESSION);
            }
            else
            {
                Session[AGENT_PAYMENTTRA_SESSION] = value;
            }
        }
    }
    private void Clear()
    {
        try
        {
            AgentPaymentDetails tempObject = new AgentPaymentDetails(-1);
            dcDocDate.Value = DateTime.Now;
            //ddlAgent.SelectedIndex = 0;           
            txtAmount.Text = Formatter.ToCurrency("0");

            ListItem item = ddlAgentCurrency.Items.FindByText(Settings.LoginInfo.Currency);
            if (item != null)
            {
                ddlAgentCurrency.ClearSelection();
                item.Selected = true;
                string[] exchRate = ddlAgentCurrency.SelectedItem.Value.Split('~');
                // Utility.StartupScript(this.Page, "setExchRate(" + exchRate[1] + ");", "setExchRate;");
                txtAgentExchrate.Text = exchRate[1];
                if (Settings.LoginInfo.AgentId > 1)
                    ddlAgentCurrency.Enabled = false;
            }
            if (Settings.LoginInfo.AgentId > 1)
            {
                ddlAgent_SelectedIndexChanged(null, null);
                Settings.LoginInfo.AgentBalance = Utility.ToDecimal(Formatter.ToCurrency(txtAgentBalance.Text));
            }
            else
            {
                ddlAgent.SelectedIndex = 0;
                txtAgentBalance.Text = string.Empty;
                ddlAgent_SelectedIndexChanged(null, null);
            }
            
            ddlSubAgent.SelectedIndex = 0;
            txtRemarks.Text = string.Empty;
            txtReceiptNo.Text = string.Empty;
            txtSubAgentBalance.Text = string.Empty;
            lblSuccessMsg.Text = string.Empty;
            lblSuccessMsg.Visible = false;
            btnSave.Text = "Save";

            CurrentObject = null;

            MemberType mType = Settings.LoginInfo.MemberType;
            if (mType == MemberType.SUPER || (Settings.LoginInfo.AgentId == 1 && (mType == MemberType.ADMIN || mType == MemberType.BACKOFFICE || mType == MemberType.CASHIER)))
            {

                ddlAgent.Enabled = true;
            }
            else
            {
                ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
        }
        catch
        {
            throw;
        }
    }

    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[AGENT_PAYMENTTRA_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["AP_ID"] };
            Session[AGENT_PAYMENTTRA_SEARCH_SESSION] = value;
        }
    }
    private void Edit(long id)
    {
        try
        {
            AgentPaymentDetails payment = new AgentPaymentDetails(id);
            CurrentObject = payment;
            txtReceiptNo.Text = payment.ReceiptNo;
            dcDocDate.Value = Utility.ToDate(payment.ReceiptDate);


            txtPayMode.Text = payment.PayMode;
            txtAmount.Text = Utility.ToString(payment.Amount);

            txtRemarks.Text = payment.Remarks;

            btnSave.Text = "Update";
            btnClear.Text = "Cancel";

        }
        catch
        {
            throw;
        }
    }
    private void Save()
    {
        try
        {

            decimal agentbalance = Utility.ToDecimal(txtAgentBalance.Text);
            decimal subAgentbalance = Utility.ToDecimal(txtSubAgentBalance.Text);
            decimal tranxAmount = Utility.ToDecimal(txtAmount.Text);

            if (rdblProvision.SelectedItem.Value == "ADD")
            {
                if (agentbalance < tranxAmount) throw new Exception("Amount should be less than Agent Current Balance!");
            }
            else if (rdblProvision.SelectedItem.Value == "DEDUCT")
            {
                if (subAgentbalance < tranxAmount) throw new Exception("Amount should be less than SubAgent Current Balance!");
            }

            AgentPaymentDetails payment;
            if (CurrentObject == null)
            {
                payment = new AgentPaymentDetails();
            }
            else
            {
                payment = CurrentObject;
            }

            
            payment.ReceiptNo = txtReceiptNo.Text.Trim();
            payment.ReceiptDate = dcDocDate.Value;
            string agentName = ddlAgent.SelectedItem.Text;
            payment.AgentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            payment.Amount = Utility.ToDecimal(txtAmount.Text.Trim());
            payment.TranxType = "TRA";
            payment.PayMode = txtPayMode.Text;
            payment.TranxProvision = rdblProvision.SelectedItem.Value;
            payment.Remarks = txtRemarks.Text.Trim();
            payment.Currency = ddlAgentCurrency.SelectedItem.Text;
            payment.ExchRate = Utility.ToDecimal(txtAgentExchrate.Text);
            payment.SubAgentId = Utility.ToInteger(ddlSubAgent.SelectedItem.Value);
            payment.SubAgentCurrency = ddlSubAgentCurrency.SelectedValue.Split('~')[1];
            payment.SubAgentExchangeRate = Utility.ToDecimal(txtSubAgentExchRate.Text);
            if (rdblProvision.SelectedIndex == 0)
            {
                payment.SubAgentAmount = payment.Amount * payment.SubAgentExchangeRate;
            }
            else
            {
                payment.Amount = payment.Amount / payment.SubAgentExchangeRate;
                payment.SubAgentAmount = Utility.ToDecimal(txtAmount.Text.Trim());
            }
            payment.Status = Settings.ACTIVE;
            payment.CreatedBy = Settings.LoginInfo.UserID;

            string mode = CurrentObject == null ? "Add" : "Update";
            payment.SavePaymentTransfer();
            txtReceiptNo.Text = payment.ReceiptNo;
            string rcptNo = txtReceiptNo.Text.Trim();
            Clear();
            lblSuccessMsg.Visible = true;

            string successMsg = string.Format(" Receipt No {0} is {1} !", rcptNo, (mode == "Add" ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            lblSuccessMsg.Text = successMsg;
            //Utility.StartupScript(this.Page, "ShowMessageDialog('Payment Receipt No','" + successMsg + "','Information')", "btnSave_Click");
            long apId = payment.ID;
            string script = "window.open('PrintAgentPaymentReceipt.aspx?apId=" + Utility.ToString(apId) + "','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            Utility.StartupScript(this.Page, script, "PrintAgentPaymentReceipt");
        }
        catch
        {
            throw;
        }

    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
           // Utility.StartupScript(this.Page, "ShowMessageDialog('btnSave_Click','" + ex.Message + "','Information')", "btnSave_Click");
            //Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            //ticketDelete();
            Clear();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            bindSearch();

        }
        catch (Exception ex)
        {

        }
    }


    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (ddlAgent.SelectedIndex > 0)
            {
                int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
                //if (agentId < 0)
                //{
                //    BindSubAgent(Settings.LoginInfo.AgentId);
                //}
                //else
                BindSubAgent(agentId);
                AgentMaster agent = new AgentMaster(Utility.ToLong(agentId));
                BindAgentCurrency(agent.AgentCurrency);
                txtAgentBalance.Text = Formatter.ToCurrency(agent.CurrentBalance);
                ListItem item = ddlAgentCurrency.Items.FindByText(agent.AgentCurrency);
                lblAmount.Text = "Amount (in " + agent.AgentCurrency + "):";
                if (item != null)
                {
                    ddlAgentCurrency.ClearSelection();
                    item.Selected = true;
                    string[] exchRate = ddlAgentCurrency.SelectedItem.Value.Split('~');

                    txtAgentExchrate.Text = exchRate[1];
                }
                txtSubAgentBalance.Text = "0.00";
                ddlSubAgentCurrency.SelectedIndex = 0;
            }
        }
        catch{ }
    }

    protected void ddlSubAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        try
        {
            AgentMaster agent = new AgentMaster(Convert.ToInt32(ddlAgent.SelectedValue));
            int agentId = Utility.ToInteger(ddlSubAgent.SelectedItem.Value);
            AgentMaster subAgent = new AgentMaster(Utility.ToLong(agentId));
            BindSubAgentCurrency(agent.AgentCurrency);
            txtSubAgentBalance.Text = Formatter.ToCurrency(subAgent.CurrentBalance);
            ListItem item = ddlSubAgentCurrency.Items.FindByText(subAgent.AgentCurrency);
            if (item != null)
            {
                ddlSubAgentCurrency.ClearSelection();
                item.Selected = true;
                string[] exchRate = ddlSubAgentCurrency.SelectedItem.Value.Split('~');

                txtSubAgentExchRate.Text = exchRate[1];
            }


        }
        catch (Exception ex)
        { }
    }

    # region Content Search
    private void bindSearch()
    {
        try
        {
            LoginInfo loginfo = Settings.LoginInfo;
            int agentId = loginfo.AgentId;
            if (loginfo.AgentId <= 1) agentId = 0;
            DataTable dt = AgentPaymentDetails.GetList(agentId, loginfo.AgentType.ToString(), ListStatus.Long, RecordStatus.Activated, "N", "TRA");
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch { throw; }
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            long apId = Utility.ToLong(gvSearch.SelectedValue);
            AgentPaymentDetails transfer = new AgentPaymentDetails(apId);
            transfer.Status = Settings.DELETED;
            transfer.SubAgentId = transfer.TransReftId;
            transfer.SavePaymentTransfer();
            string rcptNo = transfer.ReceiptNo;
            this.Master.HideSearch();
            lblSuccessMsg.Visible = true;
            // lblErrorMsg.Text = string.Format("User '{0}' is saved successfully !", user.LoginName);
            string successMsg = string.Format(" Receipt No {0} is {1} !", rcptNo, CT.TicketReceipt.Common.Action.Canceled);
            lblSuccessMsg.Text = successMsg;
            Utility.StartupScript(this.Page, "ShowMessageDialog('Payment Receipt No','" + successMsg + "','Information')", "btnCancel_Click");

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{ "HTtxtName", "agent_name" },{"HTtxtPayMode", "ap_payment_mode_name" }, 
            { "HTtxtReceiptNo", "ap_reciept_no" },{"HTtxtDocDate","ap_receipt_date"},{ "HTtxtAmount", "ap_amount" },{ "HTtxtTo", "ref_name" },{ "HTtxtProvision", "ap_tranx_provision" }};
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }


    # endregion

    #region Date Format
    protected string IDDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
    #endregion

}


