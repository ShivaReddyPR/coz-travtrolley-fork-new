using System;
using System.Data;
using System.Collections.Generic;
using System.Web.UI.WebControls;
using CT.HolidayDeals;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

public partial class HotelDealSettingGUI : CT.Core.ParentPage
{
   
    protected DataTable packagesList;
    protected int counter = 0;
    protected string activeCheck = string.Empty;
    protected string inactiveCheck = string.Empty;
    protected int activeCount = 0;
    protected string errorString = string.Empty;
    protected string whereString = string.Empty;
    protected string activeValue = string.Empty;
    protected string inactiveValue = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            bool active = false;
            bool inactive = false;
            DataTable dt = new DataTable();
            if (Request["pageSubmit"] != null)
            {
                if (Request["active"] == "on")
                {
                    activeCheck = "checked=\"checked\"";
                    activeValue = "checked=\"checked\"";
                    active = true;
                }
                if (Request["inactive"] == "on")
                {
                    inactiveCheck = "checked=\"checked\"";
                    inactiveValue = "checked=\"checked\"";
                    inactive = true;
                }
            }
            if (Request["pageSubmit"] == null)
            {
                activeCheck = "checked=\"checked\"";
                inactiveCheck = "checked=\"checked\"";
                activeValue = "checked=\"checked\"";
                inactiveValue = "checked=\"checked\"";

                whereString = Package.GenerateWhereString(true, true);
                   
                int agentId;
                if (Request["agencyId"] != null)
                {
                    agentId = Convert.ToInt32(Request["agencyId"]);
                }
                else
                {
                    agentId = Settings.LoginInfo.AgentId;
                }
                BindAgent(agentId);
                BindTheme();

                packagesList = Package.GetPackageListByAgentId(Convert.ToInt32(ddlAgent.SelectedItem.Value), Convert.ToInt32(ddlTheme.SelectedItem.Value), whereString,ddlCultureCode.SelectedItem.Value);
                counter = packagesList.Rows.Count;                
            }
                //For Search
            else if (Request["pageSubmit"] == "search")
            {
                whereString = Package.GenerateWhereString(active, inactive);
                packagesList = Package.GetPackageListByAgentId(Convert.ToInt32(ddlAgent.SelectedItem.Value), Convert.ToInt32(ddlTheme.SelectedItem.Value), whereString, ddlCultureCode.SelectedItem.Value);
                    
                counter = packagesList.Rows.Count;
                int agencyId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                //if (agencyId > 0) agency = new AgentMaster(agencyId);
            }
            else if (Request["pageSubmit"] == "save" || Request["pageSubmit"] == "inactivate")
            {
                activeValue = Request["activeValue"];
                inactiveValue = Request["inactiveValue"];
                activeCheck = activeValue;
                inactiveCheck = inactiveValue;
                whereString = Request["whereString"];
                Dictionary<int, int> pagingList = new Dictionary<int, int>();
               
                for (int i = 0; i <= Convert.ToInt32(Request["counter"]); i++)
                {
                    if (!string.IsNullOrEmpty(Convert.ToString(Request["Package-"+i])) && Request["paging-" + i] !=null && !string.IsNullOrEmpty(Request["paging-" + i].ToString()))
                    {
                        pagingList.Add(Convert.ToInt32(Request["Package-" + i]), Convert.ToInt32(Request["paging-" + i]));
                    }
                }
                //For Inactivate
                 if (Request["pageSubmit"] == "inactivate")
                 {
                     string toActivate = "D";
                     int packageId = Convert.ToInt32(Request["submitPackageId"]);

                     using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                     {

                        //Audit.Add(CT.Core.EventType.Email, Severity.High, 0, "2", "");
                        //packagesList = Package.UpdatePackageSettings(whereString, pagingList, Convert.ToInt16(ddlAgent.SelectedItem.Value));
                        Package.UpdatePackageSettings(pagingList, Settings.LoginInfo.AgentId);
                        int rowsAffected = Package.UpdatePackageStatus(packageId, toActivate);
                         if (rowsAffected > 0)
                         {
                             updateTransaction.Complete();
                             errorString = "Package was successfully Inactivated.";
                         }
                         else
                         {
                             errorString = "Unable to Inactivate Package. Please try again.";
                         }
                     }
                 }
                 //For Saving
                 else
                 {
                    //dt = Package.UpdatePackageSettings(whereString, pagingList, Convert.ToInt16(ddlAgent.SelectedItem.Value));
                    Package.UpdatePackageSettings(pagingList, Settings.LoginInfo.AgentId);
                    errorString = "Package deal settings successfully saved.";
                 }
                packagesList = Package.GetPackageListByAgentId(Convert.ToInt32(ddlAgent.SelectedItem.Value), Convert.ToInt32(ddlTheme.SelectedItem.Value), whereString, ddlCultureCode.SelectedItem.Value);
                counter = packagesList.Rows.Count;             
            }
                //For Activate
            else if (Request["pageSubmit"] == "activate")
            {
                activeValue = Request["activeValue"];
                inactiveValue = Request["inactiveValue"];
                activeCheck = activeValue;
                inactiveCheck = inactiveValue;
                whereString = Request["whereString"];
                string toActivate = "A";
                int packageId = Convert.ToInt32(Request["submitPackageId"]);
                int rowsAffected = Package.UpdatePackageStatus(packageId, toActivate);
                packagesList = Package.GetPackageListByAgentId(Convert.ToInt32(ddlAgent.SelectedItem.Value), Convert.ToInt32(ddlTheme.SelectedItem.Value), whereString, ddlCultureCode.SelectedItem.Value);
                counter = packagesList.Rows.Count;
                if (rowsAffected > 0)
                {
                    errorString = "Package was successfully activated.";

                }
                else
                {
                    errorString = "Unable to activate Package. Please try again.";
                }
            }
                //For Delete
            else if (Request["pageSubmit"] == "delete")
            {
                activeValue = Request["activeValue"];
                inactiveValue = Request["inactiveValue"];
                activeCheck = activeValue;
                inactiveCheck = inactiveValue;
                whereString = Request["whereString"];
                int packageId = Convert.ToInt32(Request["submitPackageId"]);
                
                int rows = Package.DeletePackageStatus(packageId);
                
                if (rows> 0)
                {
                    packagesList = Package.GetPackageListByAgentId(Convert.ToInt32(ddlAgent.SelectedItem.Value), Convert.ToInt32(ddlTheme.SelectedItem.Value), whereString, ddlCultureCode.SelectedItem.Value);
                    counter = packagesList.Rows.Count;      
                    errorString = "Package  successfully deleted.";
                }
                else
                {
                    errorString = "Unable to delete Package. Please try again.";
                }
                
            }
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.HotelCMS, Severity.High, (int)Settings.LoginInfo.UserID, "HotelDeal Settings Pageload " + ex.ToString() + " " + DateTime.Now, "");
        }
    }
    //For Binding Agents in ddlAgent
    private void BindAgent(int agentId)
    {
        try
        {
            DataTable dtAgents;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT-ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            }
            else
            {
                dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            }
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";

            ddlAgent.DataBind();
            //ddlAgent.Items.Insert(0, new ListItem("-- Select Agent --", "-1"));
            ddlAgent.SelectedValue = Convert.ToString(agentId);
            if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgent.Enabled = false;
            }
            else
            {
                ddlAgent.Enabled = true;
                
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindTheme()
    {
        try
        {
            ddlTheme.DataSource = Package.GetTheme("A");
            ddlTheme.DataValueField = "themeId";
            ddlTheme.DataTextField = "themeName";
            ddlTheme.DataBind();
            ddlTheme.Items.Insert(0, new ListItem("Select Theme", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

}



