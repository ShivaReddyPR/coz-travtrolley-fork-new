﻿using System;
using System.Configuration;
using System.Data;
using System.IO;
using CT.Core;
using Visa;



public partial class DownloadVisaDocument : System.Web.UI.Page
{
    protected string B2CSitePhysicalPath;
    protected string filePath;
    protected string fileExtension;
    protected string tempFolderPath;
    

    protected void Page_Load(object sender, EventArgs e)
    {
        
        try
        {
            B2CSitePhysicalPath = ConfigurationManager.AppSettings["B2CSitePhysicalPath"];
            DataTable visaDT = VisaQueue.GetUploadedDocuement(Convert.ToInt32(Request["paxId"]), Convert.ToInt32(Request["documentId"]));
           
                if (visaDT.Rows.Count > 0)
                {
                    filePath = Path.Combine(B2CSitePhysicalPath, visaDT.Rows[0]["documentPath"].ToString());
                    //Audit.Add(EventType.Visa, Severity.High, 0, "File Path:"+ filePath, "");
                    fileExtension = filePath.Substring(filePath.LastIndexOf('.'));
                    Response.ContentType = "image/jpeg";
                    Response.AppendHeader("Content-Disposition", "attachment; filename=" + visaDT.Rows[0]["documentType"].ToString().Replace(" ", "") + "PaxId" + Request["paxId"] + "DocId" + Request["documentId"] + fileExtension);
                    Response.TransmitFile(filePath);
                    
                }
                else
                {
                    Audit.Add(EventType.Visa, Severity.High, 0, "page:DownloadVisaDocument.aspx,paxId=" + Request.QueryString["paxId"] + "err=" + filePath, "");
                }

           
        }
        catch (Exception ex)
        {
           
              Audit.Add(EventType.HolidayPackageQueue, Severity.High,0, "page:DownloadVisaDocument.aspx,err=" + ex.Message +";stack="+ex.StackTrace, "");

        }
        finally
        {
            Response.End();
            
        }

    }
}
