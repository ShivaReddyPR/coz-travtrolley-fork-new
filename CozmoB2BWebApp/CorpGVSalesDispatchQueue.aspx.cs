﻿using CT.TicketReceipt.Common;
using CT.Core;
using CT.GlobalVisa;
using CT.GlobasVisa;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using Ionic.Zip;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Threading;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class CorpGVSalesDispatchQueue : CT.Core.ParentPage
{
    private string GV_DTL_SESSION_NAME = "_GvVisaCollection";
    private string GV_DOC_SESSION = "_gvVisaDoc";
    protected void Page_Load(object sender, EventArgs e)
    {
        ((ScriptManager)Master.FindControl("smTransaction")).RegisterPostBackControl(lnlDownloadAll);
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null)
            {
                lblSuccessMsg.Text = string.Empty;
                hdfParam.Value = "1";
                Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
                if (!IsPostBack)
                {
                    ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                    InitializePageControls();
                }
                //if(hdnDispatchStatus.Value == "G") {
                //    disableControls();
                //}
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    public void disableControls()
    {
        if (gvVisaSales.Rows.Count > 0)
        {
            foreach (GridViewRow gvRow in gvVisaSales.Rows)
            {
                TextBox txtVisaNo = (TextBox)gvRow.FindControl("ITtxtVisaNo");
                
                DateControl dcVisaIssueDate = (DateControl)gvRow.FindControl("ITdcVisaIssueDate");
               
                DateControl dcVisaExpDate = (DateControl)gvRow.FindControl("ITdcVisaExpDate");
               
                DateControl dcVisaCollDate = (DateControl)gvRow.FindControl("ITdcVisaCollDate");
               
            }
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        try
        {
            foreach (GridViewRow gvRow in gvVisaSales.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                TextBox txtVisaNo = (TextBox)gvRow.FindControl("ITtxtVisaNo");
                TextBox txtRemarks = (TextBox)gvRow.FindControl("ITtxtRemarks");
                DateControl dcVisaIssueDate = (DateControl)gvRow.FindControl("ITdcVisaIssueDate");
                DateControl dcVisaExpDate = (DateControl)gvRow.FindControl("ITdcVisaExpDate");
                DateControl dcVisaCollDate = (DateControl)gvRow.FindControl("ITdcVisaCollDate");
                DropDownList ddlApprove = (DropDownList)gvRow.FindControl("ITddlApprove");
                //Button lnkAddCharges = (Button)gvRow.FindControl("ITlnkAddCharges");
                TextBox txtTrackingNo = (TextBox)gvRow.FindControl("ITtxtTrackingNo");
                Button lnkDocuments = (Button)gvRow.FindControl("ITlnkDocuments");
                DropDownList ddlAssignTo = (DropDownList)gvRow.FindControl("ITddlAssignTo");
                txtRemarks.Enabled = chkSelect.Checked;
                txtVisaNo.Enabled = chkSelect.Checked;
                ddlAssignTo.Enabled = chkSelect.Checked;

                dcVisaIssueDate.Enabled = ddlApprove.Enabled = chkSelect.Checked;
                dcVisaExpDate.Enabled = ddlApprove.Enabled = chkSelect.Checked;
                dcVisaCollDate.Enabled = ddlApprove.Enabled = chkSelect.Checked;
                ddlApprove.CssClass = chkSelect.Checked ? "form-control" : "form-control";
                //lnkAddCharges.Enabled = chkSelect.Checked;
                txtTrackingNo.Enabled = chkSelect.Checked;
                txtTrackingNo.CssClass = chkSelect.Checked ? "inputEnabled" : "inputDisabled";
                lnkDocuments.Enabled = chkSelect.Checked;
                //lnkDocuments.CssClass = chkSelect.Checked ? "inputEnabled" : "inputDisabled";

                if (hdnDispatchStatus.Value == "G")
                {
                    txtVisaNo.Enabled = false;
                    dcVisaIssueDate.Enabled = false;
                    dcVisaExpDate.Enabled = false;
                    dcVisaCollDate.Enabled = false;
                }
            }
            /// commented by brahmam brahmam 12.07.2017
            /////  empty means All
            /////  T means Acknowledge
            /////  D Means Docment Received
            ////   E means Docment match
            ////   B means Document Verified
            ////   C means Received 
            ////   V means  Ready for sub
            ////   G means  collection
            ///    R means Rejected
            ////   A means  Depatched
            if (hdnDispatchStatus.Value == string.Empty || hdnDispatchStatus.Value == "T")
            {
                gvVisaSales.Columns[0].Visible = false;
                gvVisaSales.Columns[5].Visible = false;
                gvVisaSales.Columns[6].Visible = false;
                gvVisaSales.Columns[7].Visible = false;
                gvVisaSales.Columns[8].Visible = false;
                gvVisaSales.Columns[9].Visible = false;
                gvVisaSales.Columns[10].Visible = false;
                //gvVisaSales.Columns[11].Visible = false;
                gvVisaSales.Columns[11].Visible = false;
                gvVisaSales.Columns[12].Visible = false;
                gvVisaSales.Columns[13].Visible = false;
                gvVisaSales.Columns[14].Visible = true;

            }
            else if (hdnDispatchStatus.Value == "D" || hdnDispatchStatus.Value == "E")
            {
                gvVisaSales.Columns[0].Visible = true;
                gvVisaSales.Columns[5].Visible = false;
                gvVisaSales.Columns[6].Visible = false;
                gvVisaSales.Columns[7].Visible = false;
                gvVisaSales.Columns[8].Visible = false;
                gvVisaSales.Columns[9].Visible = false;
                gvVisaSales.Columns[10].Visible = true;
                //gvVisaSales.Columns[11].Visible = false;
                gvVisaSales.Columns[11].Visible = false;
                gvVisaSales.Columns[12].Visible = true;
                gvVisaSales.Columns[13].Visible = false;
                gvVisaSales.Columns[14].Visible = true;
            }
            else if (hdnDispatchStatus.Value == "B")
            {
                gvVisaSales.Columns[0].Visible = true;
                gvVisaSales.Columns[5].Visible = false;
                gvVisaSales.Columns[6].Visible = false;
                gvVisaSales.Columns[7].Visible = false;
                gvVisaSales.Columns[8].Visible = false;
                gvVisaSales.Columns[9].Visible = false;
                gvVisaSales.Columns[10].Visible = true;
                //gvVisaSales.Columns[11].Visible = false;
                gvVisaSales.Columns[11].Visible = false;
                gvVisaSales.Columns[12].Visible = true;
                gvVisaSales.Columns[13].Visible = false;
                gvVisaSales.Columns[14].Visible = true;
            }
            else if (hdnDispatchStatus.Value == "C" || hdnDispatchStatus.Value == "V" || hdnDispatchStatus.Value == "F")
            {
                gvVisaSales.Columns[0].Visible = true;
                gvVisaSales.Columns[5].Visible = false;
                gvVisaSales.Columns[6].Visible = false;
                gvVisaSales.Columns[7].Visible = false;
                gvVisaSales.Columns[8].Visible = false;
                gvVisaSales.Columns[9].Visible = false;
                gvVisaSales.Columns[10].Visible = true;
                //gvVisaSales.Columns[11].Visible = false;
                gvVisaSales.Columns[11].Visible = false;
                gvVisaSales.Columns[12].Visible = false;
                if (hdnDispatchStatus.Value == "V" || hdnDispatchStatus.Value == "F")
                {
                    gvVisaSales.Columns[13].Visible = false;
                    gvVisaSales.Columns[14].Visible = true;
                }
                else
                {
                    gvVisaSales.Columns[13].Visible = true;
                    gvVisaSales.Columns[14].Visible = false;
                }
            }
            else if (hdnDispatchStatus.Value == "G" || hdnDispatchStatus.Value == "R" || (hdnDispatchStatus.Value == "A" && hdnDispatchValue.Value != "A"))
            {
                gvVisaSales.Columns[0].Visible = true;
                gvVisaSales.Columns[5].Visible = true;
                gvVisaSales.Columns[6].Visible = true;
                gvVisaSales.Columns[7].Visible = true;
                gvVisaSales.Columns[8].Visible = true;
                gvVisaSales.Columns[9].Visible = true;
                gvVisaSales.Columns[10].Visible = true;
                //gvVisaSales.Columns[11].Visible = false;
                gvVisaSales.Columns[11].Visible = false;
                gvVisaSales.Columns[12].Visible = false;
                gvVisaSales.Columns[13].Visible = false;
                gvVisaSales.Columns[14].Visible = true;
            }
            else if (hdnDispatchStatus.Value == "A" && hdnDispatchValue.Value == "A")
            {
                gvVisaSales.Columns[0].Visible = true;
                gvVisaSales.Columns[5].Visible = false;
                gvVisaSales.Columns[6].Visible = false;
                gvVisaSales.Columns[7].Visible = false;
                gvVisaSales.Columns[8].Visible = false;
                gvVisaSales.Columns[9].Visible = false;
                gvVisaSales.Columns[10].Visible = true;
                //gvVisaSales.Columns[11].Visible = true;
                gvVisaSales.Columns[11].Visible = true;
                gvVisaSales.Columns[12].Visible = false;
                gvVisaSales.Columns[13].Visible = false;
                gvVisaSales.Columns[14].Visible = true;
            }
        }
        catch { throw; }
    }
    private void ChangeButtonColors(Button button)
    {
        try
        {
            btnSearch.ForeColor = System.Drawing.Color.White;
            btnReceived.ForeColor = System.Drawing.Color.White;
            btnDocMisMatch.ForeColor = System.Drawing.Color.White;
            btnDocReceived.ForeColor = System.Drawing.Color.White;
            btnDocVerified.ForeColor = System.Drawing.Color.White;
            btnReadyForSub.ForeColor = System.Drawing.Color.White;
            btnSubmitted.ForeColor = System.Drawing.Color.White;
            btnCollection.ForeColor = System.Drawing.Color.White;
            btnDispatched.ForeColor = System.Drawing.Color.White;
            btnAcknowledge.ForeColor = System.Drawing.Color.White;
            button.ForeColor = System.Drawing.Color.Red;
        }
        catch { throw; }
    }
    private void InitializePageControls()
    {
        try
        {
            dcFromDate.Value = Utility.ToDate(DateTime.Now.Date);
            dcToDate.Value = Utility.ToDate(DateTime.Now.Date.ToString("MMM-dd-yyyy 23:59"));
            BindAgent();
            if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
            {
                ddlLocation.Enabled = true;
            }
            else
            {
                ddlLocation.Enabled = false;
            }
            ddlLocation.DataSource = LocationMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
            ddlLocation.DataTextField = "location_name";
            ddlLocation.DataValueField = "location_id";
            ddlLocation.DataBind();
            ddlLocation.SelectedIndex = -1;
            ddlLocation.SelectedValue = Convert.ToString(Settings.LoginInfo.LocationID);
            BindUser(Settings.LoginInfo.AgentId);
            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgent.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgent.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            BindB2BAgent(Convert.ToInt32(ddlAgent.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
            BindCountry();
            BindVisaType(ddlCountry.SelectedValue);
            BindNationality();
            hdnDispatchStatus.Value = string.Empty;
            ChangeButtonColors(btnSearch);
            BindGrid();

        }
        catch
        { throw; }

    }
    private void BindCountry()
    {
        try
        {
            ddlCountry.DataSource = Country.GetCountryList();
            ddlCountry.DataTextField = "Key";
            ddlCountry.DataValueField = "Value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Select Country", "-1"));
        }
        catch (Exception ex) { throw ex; }
    }
    void BindVisaType(string countryCode)
    {
        try
        {
            ddlVisaType.DataSource = VisaTypeMaster.GetCountryByVisaType(countryCode);
            ddlVisaType.DataTextField = "visa_type_name";
            ddlVisaType.DataValueField = "visa_type_id";
            ddlVisaType.DataBind();
            ddlVisaType.Items.Insert(0, new ListItem("Select VisaType", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    void BindNationality()
    {
        try
        {
            ddlNationality.DataSource = Country.GetNationalityList();
            ddlNationality.DataTextField = "Key";
            ddlNationality.DataValueField = "Value";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("Select Nationality", "-1"));
        }
        catch (Exception ex)
        {

        }
    }
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);// AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    void BindLocation(int agentId, string type)
    {
        try
        {
            DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);

            ddlLocation.Items.Clear();
            ddlLocation.DataSource = dtLocations;
            ddlLocation.DataTextField = "location_name";
            ddlLocation.DataValueField = "location_id";
            ddlLocation.DataBind();

            ListItem item = new ListItem("All", "-1");
            ddlLocation.Items.Insert(0, item);
            hdfParam.Value = "0";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    void BindUser(int agentId)
    {
        try
        {
            DataTable dtUsers = UserMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated,MemberType.GVOPERATIONS);
            ddlConsultant.Items.Clear();
            ddlConsultant.DataSource = dtUsers;
            ddlConsultant.DataTextField = "USER_FULL_NAME";
            ddlConsultant.DataValueField = "USER_ID";
            ddlConsultant.DataBind();
            ListItem item = new ListItem("All", "-1");
            ddlConsultant.Items.Insert(0, item);
            hdfParam.Value = "0";
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    void BindGrid()
    {
        try
        {
            DateTime fromDate = Utility.ToDate(dcFromDate.Value);
            DateTime toDate = Utility.ToDate(dcToDate.Value);
            int agent = Utility.ToInteger(ddlAgent.SelectedValue);
            string countryCode = string.Empty;
            string nationalityCode = string.Empty;
            if (ddlCountry.SelectedIndex > 0)
            {
                countryCode = Utility.ToString(ddlCountry.SelectedItem.Value);
            }
            int visaType = Utility.ToInteger(ddlVisaType.SelectedItem.Value);
            if (ddlNationality.SelectedIndex > 0)
            {
                nationalityCode = Utility.ToString(ddlNationality.SelectedItem.Value);
            }
            int consaltant = Utility.ToInteger(ddlConsultant.SelectedValue);
            int locationId = Utility.ToInteger(ddlLocation.SelectedItem.Value);
            string agentType = string.Empty;
            if (agent == 0)
            {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty;// null Means binding in list all BOOKINGS
                }
            }
            if (agent > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agent > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agent = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                }
            }
            DataTable dt = GVVisaSale.GetDispatchQueueList(fromDate, toDate, countryCode,nationalityCode,visaType, hdnDispatchStatus.Value, consaltant,locationId, agent, agentType,Settings.LoginInfo.MemberType.ToString(),Settings.LoginInfo.UserID);
            dt.Columns["checked_status"].ReadOnly = false;
            VisaSalesDtlList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvVisaSales, VisaSalesDtlList);
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(EventType.Exception, Severity.High, 0, "Exception from Hotel Report:" + ex.ToString(), "");
        }
    }
    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
            BindB2BAgent(agentId);
            BindB2B2BAgent(agentId);
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "BASE";
            }
            BindLocation(Utility.ToInteger(ddlAgent.SelectedItem.Value), type);
            BindUser(Utility.ToInteger(ddlAgent.SelectedItem.Value));
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string type = string.Empty;
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            if (agentId == 0)
            {
                if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
                {
                    type = "AGENT";// AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
                else
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
            }
            else
            {
                if (agentId == -1)
                {
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    if (agentId == 0)
                    {
                        type = "BASE";// BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations
                    }
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    type = "BASEB2B";// BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations
                }
            }
            BindLocation(agentId, type);
            BindUser(agentId);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Visible = false;
            ddlStatus.Visible = false;
            hdnDispatchStatus.Value = string.Empty;
            hdnDispatchValue.Value = string.Empty;
            ChangeButtonColors(btnSearch);
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void ddlB2B2BAgent_SelectedIndexChanged1(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "B2B2B";// B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                }
            }
            else if (agentId == -1)
            {
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = string.Empty;
                    }
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                }
            }
            BindLocation(agentId, type);
            BindUser(agentId);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindVisaType(ddlCountry.SelectedItem.Value);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "CorpGVRequest TravelingTo event exception reson: " + ex.ToString(), Request["REMOTE_ADDR"]);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void ITchkSelect_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            //if (hdnDispatchStatus.Value == "C" || hdnDispatchStatus.Value == "V" || hdnDispatchStatus.Value == "F")
            {

                GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
                CheckBox chkSelectAll = (CheckBox)gvHdrRow.FindControl("HTchkSelectAll");
                foreach (GridViewRow gvRow in gvVisaSales.Rows)
                {
                    CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                    TextBox txtRemarks = (TextBox)gvRow.FindControl("ITtxtRemarks");
                    TextBox txtTrackingNo = (TextBox)gvRow.FindControl("ITtxtTrackingNo");
                    if (chkSelect.Enabled) chkSelect.Checked = chkSelectAll.Checked;
                    txtRemarks.Text = string.Empty;
                    txtTrackingNo.Text = txtTrackingNo.Text = string.Empty;
                }
            }
        }
        catch { throw; }
    }
    private DataTable VisaSalesDtlList
    {
        get
        {
            return (DataTable)Session[GV_DTL_SESSION_NAME];
        }
        set
        {
            if (value == null)
                Session.Remove(GV_DTL_SESSION_NAME);
            else
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["FPAX_ID"] };
                Session[GV_DTL_SESSION_NAME] = value;
            }
        }
    }
    private DataTable VisaDOCDtlList
    {
        get
        {
            return (DataTable)Session[GV_DOC_SESSION];
        }
        set
        {
            if (value == null)
                Session.Remove(GV_DOC_SESSION);
            else
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["doc_id"] };
                Session[GV_DOC_SESSION] = value;
            }
        }
    }
    protected void ITlblDocNo_Click(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvRow = (GridViewRow)(((LinkButton)sender).NamingContainer);
            LinkButton ITlblDocNo = (LinkButton)gvRow.FindControl("ITlblDocNo");
            Label ITlblCountry = (Label)gvRow.FindControl("ITlblCountry");
            Label ITlblNationality = (Label)gvRow.FindControl("ITlblNationality");
            HiddenField IThdnPSPType = (HiddenField)gvRow.FindControl("IThdnPSPType");
            HiddenField IThdnVisaType = (HiddenField)gvRow.FindControl("IThdnVisaType");
            HiddenField IThdnResidenceName = (HiddenField)gvRow.FindControl("IThdnResidenceName");
            Label ITlblVisitorName = (Label)gvRow.FindControl("ITlblVisitorName");
            Label ITlblPassport = (Label)gvRow.FindControl("ITlblPassport");


            lblDocCountryValue.Text = ITlblCountry.Text;
            lblDocNationalityValue.Text = ITlblNationality.Text;
            lblDocPassportTypeValue.Text = IThdnPSPType.Value;
            lblDocVisaValue.Text = IThdnVisaType.Value;
            lblDocResidenceNameValue.Text = IThdnResidenceName.Value;
            lblDocVisitorNameValue.Text = ITlblVisitorName.Text;
            lblDocPasportNoValue.Text = ITlblPassport.Text;
            Utility.StartupScript(this.Page, "setDocumentNoPosition('" + ITlblDocNo.ClientID + "')", "setDocumentNoPosition");
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnDClose_Click(object sender, EventArgs e)
    {
        try
        {
            Utility.StartupScript(this.Page, "document.getElementById('divDocNo').style.display='none'", "divDocNo");
            lblDocCountryValue.Text = string.Empty;
            lblDocNationalityValue.Text = string.Empty;
            lblDocPassportTypeValue.Text = string.Empty;
            lblDocVisaValue.Text = string.Empty;
            lblDocResidenceNameValue.Text = string.Empty;
            lblDocVisitorNameValue.Text = string.Empty;
            lblDocPasportNoValue.Text = string.Empty;

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void ITlnkPrint_Click(object sender, EventArgs e)
    {
        try
        {

            GridViewRow gvRow = (GridViewRow)(((LinkButton)sender).NamingContainer);
            HiddenField hdfVSId = (HiddenField)gvRow.FindControl("IThdfVSId");
            if (hdfVSId != null)
            {
                long vsId = Utility.ToLong(hdfVSId.Value);
                string script = "window.open('PrintGVSalesReceipt.aspx?vsId=" + Utility.ToString(vsId) + "','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
                Utility.StartupScript(this.Page, script, "PrintOBVisaSalesReceipt");

            }

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    
    protected void gvVisaSales_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                btnUpdate.Visible = true;
                DataRowView view = e.Row.DataItem as DataRowView;
                string status = Utility.ToString(view["FPAX_DISPATCH_STATUS"]);
                CheckBox chkSelect = (CheckBox)e.Row.FindControl("ITchkSelect");
                DropDownList ddlApprove = (DropDownList)e.Row.FindControl("ITddlApprove");
                chkSelect.Enabled = true;
                if (hdnDispatchStatus.Value == "G" || (hdnDispatchStatus.Value == "A" && hdnDispatchValue.Value != "A") || hdnDispatchStatus.Value == "R")
                {
                    if (status == "A")
                    {
                        chkSelect.Enabled = false;
                    }
                }
                if (hdnDispatchStatus.Value == "B")
                {
                    if (status == "V")
                    {
                        chkSelect.Enabled = false;
                    }
                }
                DropDownList ddlAssign = (DropDownList)e.Row.FindControl("ITddlAssignTo");
                BindAssignto(ddlAssign);
                if (status != null)
                {
                    ddlApprove.SelectedValue = status;
                }
                if ((hdnDispatchStatus.Value == "G"))// && status == "S"
                {
                    DateControl dcVisaIssueDate = (DateControl)e.Row.FindControl("ITdcVisaIssueDate");
                    DateControl dcVisaExpDate = (DateControl)e.Row.FindControl("ITdcVisaExpDate");
                    DateControl dcVisaCollDate = (DateControl)e.Row.FindControl("ITdcVisaCollDate");

                    if (dcVisaIssueDate.Value == DateTime.MinValue) dcVisaIssueDate.Clear();
                    if (dcVisaExpDate.Value == DateTime.MinValue) dcVisaExpDate.Clear();
                    if (dcVisaCollDate.Value == DateTime.MinValue) dcVisaCollDate.Clear();
                    Utility.StartupScript(this.Page, "enableGridControls('" + chkSelect.ClientID + "')", "enableGridControls");
                }
                else if (hdnDispatchStatus.Value == "D" || hdnDispatchStatus.Value == "E" || hdnDispatchStatus.Value == "T" || hdnDispatchStatus.Value == string.Empty)
                {
                    btnUpdate.Visible = false;
                }

            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    void BindAssignto(DropDownList ddlAssign)
    {
        try
        {
            DataTable dtUsers = UserMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated,MemberType.GVOPERATIONS);
            ddlAssign.DataSource = dtUsers;
            ddlAssign.DataTextField = "USER_FULL_NAME";
            ddlAssign.DataValueField = "USER_ID";
            ddlAssign.DataBind();
            ddlAssign.Items.Insert(0, new ListItem("Select AssignTo", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void gvVisaSales_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvVisaSales.PageIndex = e.NewPageIndex;
            gvVisaSales.EditIndex = -1;
            selectedItem();
            BindGrid();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void gvVisaSales_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvSeletedRow = gvVisaSales.SelectedRow;
            HiddenField IThdfVSId = (HiddenField)gvSeletedRow.FindControl("IThdfVSId");
            HiddenField IThdfPaxId = (HiddenField)gvSeletedRow.FindControl("IThdfPaxId");
            if (hdfHistory.Value == "0")
            {
                if (hdnDispatchStatus.Value == "A" && hdnDispatchValue.Value == "A") //Verified
                {
                    //clearChargesDetails();
                    //// pnlAddCharges.Visible = true;
                    //hdfPaxId.Value = Utility.ToString(gvVisaSales.DataKeys[gvVisaSales.SelectedIndex].Values[0]);
                    //GridViewRow gvSeletedRow = gvVisaSales.SelectedRow;
                    //Button lnkAddCharges = (Button)gvSeletedRow.FindControl("ITlnkAddCharges");
                    //HiddenField hdfChargesId = (HiddenField)gvSeletedRow.FindControl("IThdfChargesId");
                    //if (Utility.ToInteger(hdfChargesId.Value) > 0)
                    //{
                    //    ddlChargesType.SelectedValue = hdfChargesId.Value;
                    //}
                    //Utility.StartupScript(this.Page, "setChargesPosition('" + lnkAddCharges.ClientID + "')", "setChargesPosition");
                }
                else if (hdnDispatchStatus.Value == "D") //received
                {

                    Button ITlnkDocuments = (Button)gvSeletedRow.FindControl("ITlnkDocuments");
                    HiddenField IThdfProfileId = (HiddenField)gvSeletedRow.FindControl("IThdfProfileId");
                    LinkButton ITlblDocNo = (LinkButton)(gvSeletedRow.FindControl("ITlblDocNo"));
                    Label ITlblVisitorName = (Label)gvSeletedRow.FindControl("ITlblVisitorName");

                    hdfPaxId.Value = IThdfPaxId.Value;
                    hdnVisaId.Value = IThdfVSId.Value;
                    hdnProfileId.Value = IThdfProfileId.Value;
                    lblDocNoValue.Text = ITlblDocNo.Text;
                    lblDPaxNameValue.Text = ITlblVisitorName.Text;
                    chkDocumentVerified.Visible = true;
                    VisaDOCDtlList = GVVisaSale.GetDoucmentList(Utility.ToLong(IThdfProfileId.Value), Utility.ToLong(IThdfVSId.Value), Utility.ToLong(IThdfPaxId.Value));
                    BindDocuments(VisaDOCDtlList);
                    Utility.StartupScript(this.Page, "setDocumentPosition('" + ITlnkDocuments.ClientID + "',divDocuments)", "setDocumentPosition");
                }
                else if (hdnDispatchStatus.Value == "E" || hdnDispatchStatus.Value == "B") //E means mismatch and B meansverified
                {

                    Button ITlnkDocuments = (Button)gvSeletedRow.FindControl("ITlnkDocuments");
                    LinkButton ITlblDocNo = (LinkButton)(gvSeletedRow.FindControl("ITlblDocNo"));
                    Label ITlblVisitorName = (Label)gvSeletedRow.FindControl("ITlblVisitorName");
                    HiddenField IThdfProfileId = (HiddenField)gvSeletedRow.FindControl("IThdfProfileId");
                    hdfPaxId.Value = IThdfPaxId.Value;
                    hdnVisaId.Value = IThdfVSId.Value;
                    lblDocNoValue.Text = ITlblDocNo.Text;
                    hdnProfileId.Value = IThdfProfileId.Value;
                    lblDPaxNameValue.Text = ITlblVisitorName.Text;
                    VisaDOCDtlList = GVVisaSale.GetDoucmentList(Utility.ToLong(IThdfProfileId.Value), Utility.ToLong(IThdfVSId.Value), Utility.ToLong(IThdfPaxId.Value));
                    BindDocuments(VisaDOCDtlList);
                    Utility.StartupScript(this.Page, "setDocumentPosition('" + ITlnkDocuments.ClientID + "',divDocuments)", "setDocumentPosition");
                }
                if (hdnDispatchStatus.Value == "E" || hdnDispatchStatus.Value == "B" || hdnDispatchStatus.Value == "D")
                {
                    if (VisaDOCDtlList != null)
                    {
                        string status = GVVisaSale.GetVerifiedDocStatus(Utility.ToLong(IThdfVSId.Value), Utility.ToLong(IThdfPaxId.Value), "DocumentVerified");
                        txtRemarks.Style.Add("display", "none");
                        if (string.IsNullOrEmpty(status))
                        {
                            chkDocumentVerified.Enabled = true;
                            chkDocumentVerified.Checked = true;
                        }
                        else if (Convert.ToBoolean(status))
                        {
                            chkDocumentVerified.Checked = true;
                            chkDocumentVerified.Enabled = false;
                        }
                        else if (!Convert.ToBoolean(status))
                        {
                            chkDocumentVerified.Checked = false;
                            chkDocumentVerified.Enabled = true;
                            txtRemarks.Style.Add("display", "block");
                        }
                    }
                    if (hdnDispatchStatus.Value == "B")
                    {
                        txtRemarks.Style.Add("display", "none");
                    }
                }
            }
            if (hdfHistory.Value == "1")
            {
                LinkButton ITlnkHistory = (LinkButton)gvSeletedRow.FindControl("ITlnkHistory");
                //Getting History
                DataTable dtDispatchQueue = GVVisaSale.GetDispatchHistory(Utility.ToLong(IThdfVSId.Value), Utility.ToLong(IThdfPaxId.Value));
                BindHistory(dtDispatchQueue);
                Utility.StartupScript(this.Page, "setDocumentPosition('" + ITlnkHistory.ClientID + "',divHistory)", "setDocumentPosition");
            }

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void BindDocuments(DataTable VisaDOCDtlList)
    {
        try
        {
            CommonGrid grid = new CommonGrid();
            grid.BindGrid(gvDocuments, VisaDOCDtlList);
        }
        catch { throw; }
    }
    private void BindHistory(DataTable dtHistory)
    {
        try
        {
            CommonGrid grid = new CommonGrid();
            grid.BindGrid(gvHistory, dtHistory);
        }
        catch { throw; }
    }
    #region Date Format
    protected string IDDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
    #endregion

    protected void btnDocReceived_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Visible = false;
            ddlStatus.Visible = false;
            hdnDispatchStatus.Value = "D";
            hdnDispatchValue.Value = string.Empty;
            ChangeButtonColors(btnDocReceived);
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnDocVerified_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Visible = false;
            ddlStatus.Visible = false;
            hdnDispatchStatus.Value = "B";
            hdnDispatchValue.Value = string.Empty;
            ChangeButtonColors(btnDocVerified);
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnDocMisMatch_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Visible = false;
            ddlStatus.Visible = false;
            hdnDispatchStatus.Value = "E";
            hdnDispatchValue.Value = string.Empty;
            ChangeButtonColors(btnDocMisMatch);
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnReadyForSub_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Visible = false;
            ddlStatus.Visible = false;
            hdnDispatchStatus.Value = "V";
            hdnDispatchValue.Value = string.Empty;
            ChangeButtonColors(btnReadyForSub);
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnSubmitted_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Visible = false;
            ddlStatus.Visible = false;
            hdnDispatchStatus.Value = "F";
            hdnDispatchValue.Value = string.Empty;
            ChangeButtonColors(btnSubmitted);
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnCollection_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Visible = true;
            ddlStatus.Visible = true;
            ddlStatus.SelectedIndex = 0;
            hdnDispatchStatus.Value = "G";
            hdnDispatchValue.Value = string.Empty;
            ChangeButtonColors(btnCollection);
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnDispatched_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Visible = false;
            ddlStatus.Visible = false;
            hdnDispatchStatus.Value = "A";
            hdnDispatchValue.Value = "A";
            ChangeButtonColors(btnDispatched);
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnAcknowledge_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Visible = false;
            ddlStatus.Visible = false;
            hdnDispatchStatus.Value = "T";
            hdnDispatchValue.Value = string.Empty;
            ChangeButtonColors(btnAcknowledge);
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnReceived_Click(object sender, EventArgs e)
    {
        try
        {
            lblStatus.Visible = false;
            ddlStatus.Visible = false;
            hdnDispatchStatus.Value = "C";
            hdnDispatchValue.Value = string.Empty;
            ChangeButtonColors(btnReceived);
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            selectedItem();
            isTrackingEmpty();
            List<DataRow> UpdatedRows = new List<DataRow>();
            GlobalVisaDispatchStatus dispatchStatus = GlobalVisaDispatchStatus.All;

            VisaSalesDtlList.Columns.Add("Dispatch_Status", typeof(System.String));

            foreach (DataRow dr in VisaSalesDtlList.Rows)
            {
                if (dr != null)
                {

                    bool selected = Utility.ToString(dr["checked_status"]) == "true" ? true : false;
                    if (selected)
                    {
                        UpdatedRows.Add(dr);

                        long paxId = Utility.ToLong(dr["FPAX_ID"]);
                        string dispatchRemarks = Utility.ToString(dr["FPAX_DISPATCH_REMARKS"]);
                        string approveStatus = Utility.ToString(dr["fpax_status"]);
                        string visaNo = Utility.ToString(dr["fpax_visa_no"]);
                        DateTime visaIssueDate = Utility.ToDate(dr["fpax_visa_issue_date"]);
                        DateTime visaExpDate = Utility.ToDate(dr["fpax_visa_Exp_date"]);
                        DateTime visaCollDate = Utility.ToDate(dr["fpax_visa_coll_date"]);
                        long vsId = 0;
                        int chargesId = 0;
                        decimal chargesAmnt = 0;
                        string chargesRemarks = string.Empty;
                        string trackingNo = string.Empty;
                        string chargesStatus = string.Empty;
                        long assignTo = 0;
                        if (hdnDispatchStatus.Value == "C")  //Means Received
                        {
                            dispatchStatus = GlobalVisaDispatchStatus.DocumentReceived;
                            vsId = Utility.ToLong(dr["FVS_ID"]);
                            assignTo = Utility.ToLong(dr["fvs_Assign_id"]);
                        }
                        else if (hdnDispatchStatus.Value == "B")  //means verified
                        {
                            dispatchStatus = GlobalVisaDispatchStatus.DocumentVerified;

                        }
                        else if (hdnDispatchStatus.Value == "V") //Ready 
                        {
                            dispatchStatus = GlobalVisaDispatchStatus.ReadyForSubmit;
                        }
                        else if (hdnDispatchStatus.Value == "F") //subamited
                        {
                            dispatchStatus = GlobalVisaDispatchStatus.Submitted;
                        }
                        else if (hdnDispatchStatus.Value == "G" || hdnDispatchStatus.Value == "R") //collection and Received
                        {
                            dispatchStatus = (approveStatus == "A") ? GlobalVisaDispatchStatus.Approved : GlobalVisaDispatchStatus.Rejected;
                        }
                        else if (hdnDispatchStatus.Value == "A" || hdnDispatchValue.Value == "A") //Received
                        {
                            vsId = Utility.ToLong(dr["FVS_ID"]);
                            chargesId = Utility.ToInteger(dr["FPAX_ADD_CHARGES_ID"]);
                            chargesAmnt = Utility.ToDecimal(dr["FPAX_ADD_CHARGES_AMNT"]);
                            chargesRemarks = Utility.ToString(dr["FPAX_ADD_CHARGES_REMARKS"]);
                            trackingNo = Utility.ToString(dr["fpax_tracking_no"]);
                            chargesStatus = Utility.ToString(dr["fvs_add_charges_status"]);
                            dispatchStatus = GlobalVisaDispatchStatus.Dispatched;
                        }
                        if (selected) GVVisaSale.SaveDipatchDetails(vsId, paxId, dispatchStatus, string.Empty, dispatchRemarks, trackingNo,
                               visaNo, visaIssueDate, visaExpDate, visaCollDate, Settings.LoginInfo.UserID, assignTo);//TODO

                        dr.BeginEdit();
                        dr["Dispatch_Status"] = dispatchStatus;
                        dr.EndEdit();
                        dr.AcceptChanges();
                    }

                }
            }

            EmailDispatchDetails(UpdatedRows, dispatchStatus.ToString());
            BindGrid();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = "Application status changed to # : " + dispatchStatus;
            Utility.Alert(this.Page, lblSuccessMsg.Text);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.StartupScript(this.Page, "alert('" + ex.Message + "');", "Err");
        }
        btnUpdate.Style.Add("display", "block");
    }
    private void selectedItem()
    {
        try
        {
            foreach (GridViewRow gvRow in gvVisaSales.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                HiddenField hdfPaxId = (HiddenField)gvRow.FindControl("IThdfPaxId");
                TextBox txtRemarks = (TextBox)gvRow.FindControl("ITtxtRemarks");
                DropDownList ddlApprove = (DropDownList)gvRow.FindControl("ITddlApprove");
                TextBox txtVisaNo = (TextBox)gvRow.FindControl("ITtxtVisaNo");
                DateControl dcVisaIssueDate = (DateControl)gvRow.FindControl("ITdcVisaIssueDate");
                DateControl dcVisaExpDate = (DateControl)gvRow.FindControl("ITdcVisaExpDate");
                DateControl dcVisaCollDate = (DateControl)gvRow.FindControl("ITdcVisaCollDate");
                HiddenField vsId = (HiddenField)gvRow.FindControl("IThdfVSId");
                TextBox txtTrackingNo = (TextBox)gvRow.FindControl("ITtxtTrackingNo");
                DropDownList ddlAssignTo = (DropDownList)gvRow.FindControl("ITddlAssignTo");
                foreach (DataRow dr in VisaSalesDtlList.Rows)
                {
                    if (dr != null)
                    {
                        dr.BeginEdit();
                        if (Utility.ToString(dr["FPAX_ID"]) == hdfPaxId.Value)
                        {
                            dr["checked_status"] = chkSelect.Checked ? "true" : "false";
                            dr["FPAX_DISPATCH_REMARKS"] = txtRemarks.Text.Trim();
                            if (hdnDispatchStatus.Value == "G" || hdnDispatchStatus.Value == "R") //Collection
                            {
                                dr["fpax_status"] = ddlApprove.SelectedValue;
                                dr["fpax_visa_no"] = txtVisaNo.Text.Trim();
                                dr["fpax_visa_issue_date"] = dcVisaIssueDate.Value;
                                dr["fpax_visa_exp_date"] = dcVisaExpDate.Value;
                                dr["fpax_visa_coll_date"] = dcVisaCollDate.Value;
                            }
                            else if (hdnDispatchStatus.Value == "A" && hdnDispatchValue.Value == "A") //Verified
                            {
                                dr["fpax_tracking_no"] = txtTrackingNo.Text.Trim();
                            }
                            else if (hdnDispatchStatus.Value == "C")
                            {
                                dr["fvs_Assign_id"] = Utility.ToInteger(ddlAssignTo.SelectedItem.Value);
                            }
                        }
                        dr.EndEdit();
                    }
                }
            }

        }
        catch { throw; }
    }
    /// <summary>
    /// Dispatch Queue Grid Checking
    /// </summary>
    private void isTrackingEmpty()
    {
        try
        {
            bool _selected = false;
            foreach (DataRow dr in VisaSalesDtlList.Rows)
            {
                if (dr != null)
                {
                    if (Utility.ToString(dr["checked_status"]) == "true")
                    {
                        _selected = true;
                        ///here G means Collection and R means Rejected
                        if (hdnDispatchStatus.Value == "G" || hdnDispatchStatus.Value == "R")
                        {
                            if (Utility.ToString(dr["fpax_status"]) != "R")
                            {
                                if (Utility.ToString(dr["fpax_visa_no"]) == string.Empty) throw new Exception("Visa # cannot be Blank !");
                                else if (Utility.ToString(dr["fpax_visa_issue_date"]) == string.Empty || Utility.ToDate(dr["fpax_visa_issue_date"]) == DateTime.MinValue) throw new Exception("Visa Issue Date cannot be Blank !");
                                else if (Utility.ToString(dr["fpax_visa_exp_date"]) == string.Empty || Utility.ToDate(dr["fpax_visa_exp_date"]) == DateTime.MinValue) throw new Exception("Visa Expiry Date cannot be Blank !");
                                else if (Utility.ToString(dr["fpax_visa_coll_date"]) == string.Empty || Utility.ToDate(dr["fpax_visa_coll_date"]) == DateTime.MinValue) throw new Exception("Visa Collection Date cannot be Blank !");
                            }
                        }
                        else if (hdnDispatchStatus.Value == "A" || hdnDispatchValue.Value == "A")  //A means Received
                        {
                            if (Utility.ToString(dr["fpax_tracking_no"]) == string.Empty) throw new Exception("Tracking # cannot be Blank !");
                        }
                        //return;
                    }
                }
            }
            if (!_selected) throw new Exception("Please Select @least one Item ! ");
        }
        catch
        {

            throw;
        }
    }

    protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            string status = "G";
            if (ddlStatus.SelectedIndex > 0)
            {
                status = ddlStatus.SelectedItem.Value;
            }
            hdnDispatchStatus.Value = status;
            hdnDispatchValue.Value = string.Empty;
            ChangeButtonColors(btnCollection);
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void gvDocuments_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView view = e.Row.DataItem as DataRowView;
                string status = Utility.ToString(view["docStatus"]);
                CheckBox chkSelect = (CheckBox)e.Row.FindControl("ITchkSelect");
                Label ITlblDocStatus = (Label)e.Row.FindControl("ITlblDocStatus");
                HyperLink ITHylnkDocument = (HyperLink)e.Row.FindControl("ITHylnkDocument");
                ITHylnkDocument.Attributes.Add("onclick", "DownloadDocument('"+ view["DocFilePath"].ToString().Replace("\\","/") +"','"+ view["DocFileName"].ToString() + "')");
                //view["DocFilePath"].ToString() + "/"+view["DocFileName"].ToString();
                gvDocuments.Columns[0].Visible = true;
                chkSelect.Enabled = true;
                chkDocumentVerified.Visible = true;
                if (hdnDispatchStatus.Value == "D")
                {
                    if (string.IsNullOrEmpty(status))
                    {
                        chkSelect.Enabled = true;
                        chkSelect.Checked = true;
                        ITlblDocStatus.Text = "Not Received";
                    }
                    else if (Convert.ToBoolean(status))
                    {
                        chkSelect.Enabled = false;
                        chkSelect.Checked = true;
                        ITlblDocStatus.Text = "Received";
                    }
                    else
                    {
                        ITlblDocStatus.Text = "Mismatch";
                    }
                }
                if (hdnDispatchStatus.Value == "E")
                {
                    if (string.IsNullOrEmpty(status))
                    {
                        chkSelect.Enabled = true;
                        chkSelect.Checked = true;
                        ITlblDocStatus.Text = "Not Received";
                    }
                    else if (Convert.ToBoolean(status))
                    {
                        chkSelect.Enabled = false;
                        chkSelect.Checked = true;
                        ITlblDocStatus.Text = "Received";
                    }
                    else
                    {
                        ITlblDocStatus.Text = "Mismatch";
                    }
                }
                btnDocUpdate.Visible = true;
                if (hdnDispatchStatus.Value == "B")
                {
                    gvDocuments.Columns[0].Visible = false;
                    btnDocUpdate.Visible = false;
                    chkDocumentVerified.Visible = false;
                    if (string.IsNullOrEmpty(status))
                    {
                        ITlblDocStatus.Text = "Not Received";
                    }
                    else if (Convert.ToBoolean(status))
                    {
                        ITlblDocStatus.Text = "Received";
                    }
                    else
                    {
                        ITlblDocStatus.Text = "Mismatch";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void selectedDocItem()
    {
        try
        {
            foreach (GridViewRow gvRow in gvDocuments.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                HiddenField hdfDocId = (HiddenField)gvRow.FindControl("IThdfDocKey");
                foreach (DataRow dr in VisaDOCDtlList.Rows)
                {
                    if (dr != null)
                    {
                        dr.BeginEdit();
                        if (Utility.ToString(dr["docId"]) == hdfDocId.Value)
                        {
                            dr["checked_status"] = chkSelect.Checked ? "true" : "false";
                        }
                        dr.EndEdit();
                    }
                }
            }
        }
        catch { throw; }
    }
    /// <summary>
    /// Document Grid Checking
    /// </summary>
    private void isDocTrackingEmpty()
    {
        try
        {

            bool _selected = false;
            foreach (DataRow dr in VisaDOCDtlList.Rows)
            {
                if (dr != null)
                {
                    if (Utility.ToString(dr["checked_status"]) == "true")
                    {
                        _selected = true;
                    }
                }
            }
            if (!_selected) throw new Exception("Please Select @least one Item ! ");
        }
        catch { throw; }
    }
    protected void btnDocUpdate_Click(object sender, EventArgs e)
    {
        try
        {
            selectedDocItem();
            if (hdnDispatchStatus.Value == "E") isDocTrackingEmpty();
            foreach (DataRow dr in VisaDOCDtlList.Rows)
            {
                bool selected = Utility.ToString(dr["checked_status"]) == "true" ? true : false;
                long docId = Utility.ToLong(dr["docId"]);
                long profileId = Utility.ToLong(dr["ProfileId"]);
                string docStatus = Utility.ToString(dr["docStatus"]);
                if (selected)
                {
                    GVVisaSale.saveDocDetails(Utility.ToLong(hdnVisaId.Value), Utility.ToLong(hdfPaxId.Value), string.Empty, true, profileId, docId);
                }
                else
                {
                    GVVisaSale.saveDocDetails(Utility.ToLong(hdnVisaId.Value), Utility.ToLong(hdfPaxId.Value), string.Empty, false, profileId, docId);
                }
            }
            if (hdnDispatchStatus.Value == "D")
            {
                bool status = true;
                string remarks = string.Empty;
                if (!chkDocumentVerified.Checked)
                {
                    status = false;
                    remarks = txtRemarks.Text.Trim();
                }
                GVVisaSale.saveDocDetails(Utility.ToLong(hdnVisaId.Value), Utility.ToLong(hdfPaxId.Value), "DocumentVerified", status, Utility.ToLong(hdnProfileId.Value), -1);
                GlobalVisaDispatchStatus dispatchStatus = GlobalVisaDispatchStatus.DocumentReceived;
                if (!status)
                {
                    GVVisaSale.SaveDipatchDetails(Utility.ToLong(hdnVisaId.Value), Utility.ToLong(hdfPaxId.Value), dispatchStatus, string.Empty, remarks, string.Empty,
                   string.Empty, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, Settings.LoginInfo.UserID, -1);//TODO
                }
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = "Application status changed to # : " + "DocumentReceived";
            }
            else if (hdnDispatchStatus.Value == "E")
            {
                bool status = true;
                string remarks = string.Empty;
                if (!chkDocumentVerified.Checked)
                {
                    status = false;
                    remarks = txtRemarks.Text.Trim();
                }
                GVVisaSale.saveDocDetails(Utility.ToLong(hdnVisaId.Value), Utility.ToLong(hdfPaxId.Value), "DocumentVerified", status, Utility.ToLong(hdnProfileId.Value), -1);
                GlobalVisaDispatchStatus dispatchStatus = GlobalVisaDispatchStatus.DocumentMismatch;
                GVVisaSale.SaveDipatchDetails(Utility.ToLong(hdnVisaId.Value), Utility.ToLong(hdfPaxId.Value), dispatchStatus, string.Empty, remarks, string.Empty,
               string.Empty, DateTime.MinValue, DateTime.MinValue, DateTime.MinValue, Settings.LoginInfo.UserID, -1);//TODO
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = "Application status changed to # : " + dispatchStatus;

                List<DataRow> lstSelectedRows = new List<DataRow>();
                if (VisaSalesDtlList != null && VisaSalesDtlList.Rows != null && VisaSalesDtlList.Rows.Count > 0)
                {
                    DataRow[] salesList = VisaSalesDtlList.Select("fvs_id=" + hdnVisaId.Value);
                    if(salesList != null && salesList.Length>0)
                    {
                        foreach (DataRow dr in salesList)
                        {
                            if (!string.IsNullOrEmpty(remarks))
                            {
                                dr.BeginEdit();
                                dr["FPAX_DISPATCH_REMARKS"] = remarks;
                                dr.EndEdit();
                            }
                            lstSelectedRows.Add(dr);
                        }
                    }
                    EmailDispatchDetails(lstSelectedRows, dispatchStatus.ToString());
                }
                txtRemarks.Text = string.Empty;
            }
            BindGrid();
            Utility.Alert(this.Page, lblSuccessMsg.Text);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void btnDocClose_Click(object sender, EventArgs e)
    {
        try
        {
            Utility.StartupScript(this.Page, "document.getElementById('divDocuments').style.display='none'", "divDocuments");
            clearDocDetails();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    private void clearDocDetails()
    {
        try
        {
            lblDocNoValue.Text = string.Empty;
            lblDPaxNameValue.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            txtRemarks.Style.Add("display", "none");
            VisaDOCDtlList.Rows.Clear();
            BindDocuments(VisaDOCDtlList);
        }
        catch { throw; }
    }
    /// <summary>
    /// Downloading all documents at time implemented by brahmam 21.09.2016
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void lnlDownloadAll_Click(object sender, EventArgs e)
    {
        try
        {
            using (ZipFile zip = new ZipFile())
            {
                zip.AlternateEncodingUsage = ZipOption.AsNecessary;
                foreach (DataRow dr in VisaDOCDtlList.Rows)
                {
                    if (dr["DocFilePath"] != DBNull.Value && dr["DocFileName"] != DBNull.Value)
                    {
                        string path = dr["DocFilePath"].ToString().Replace("\\", "/") + "/" + dr["DocFileName"].ToString();
                        zip.AddFile(path).FileName = dr["DocFileName"].ToString(); //renameing File name
                    }
                }
                Response.Clear();
                Response.BufferOutput = false;
                string zipName = String.Format("{0}.zip", lblDocNoValue.Text); //creating zip name
                Response.ContentType = "application/zip";
                Response.AddHeader("content-disposition", "attachment; filename=" + zipName);
                zip.Save(Response.OutputStream);
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
        catch (Exception ex)
        {

        }
        finally
        {
            Response.End();
        }
    }

    void EmailDispatchDetails(List<DataRow> Rows, string dispatchStatus)
    {
        try
        {
            Dictionary<string, List<DataRow>> UniqueVisas = new Dictionary<string, List<DataRow>>();
            foreach (DataRow dr in Rows)
            {
                string visaRefNo = dr["fvs_doc_no"].ToString();
                List<DataRow> SameVisaPax = Rows.FindAll(delegate (DataRow row) { return visaRefNo == row["fvs_doc_no"].ToString(); });

                if (!UniqueVisas.ContainsKey(visaRefNo))
                {
                    UniqueVisas.Add(visaRefNo, SameVisaPax);
                }
            }

            StreamReader sr = new StreamReader(Server.MapPath("~/") + "/global-visa.html");
            string emailMessage = sr.ReadToEnd();
            sr.Close();

            string PaxDetails = "", AgentName = "", Agent = "", AgentLogo = "", country = "", paxName = "";

            //Agent = ddlAgent.SelectedItem.Text + "-" + ddlLocation.SelectedItem.Text;
            //AgentName = ddlAgent.SelectedItem.Text;
            //AgentMaster agent = new AgentMaster(Convert.ToInt64(ddlAgent.SelectedValue));
            //AgentLogo = ConfigurationManager.AppSettings["logopath"] + agent.ImgFileName;//Assign Site url here
            AgentLogo = Request.Url.Scheme+"://travtrolley.com/images/logo.jpg";




            string subject = "";
            string bccEmailList = ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"];
            List<string> toList = new List<string>();

            string submissionDate = string.Empty;
            string collDate = string.Empty;
            //string leadPaxEmail = string.Empty;

            foreach (KeyValuePair<string, List<DataRow>> pair in UniqueVisas)
            {
                //leadPaxEmail = string.Empty;
                PaxDetails = string.Empty;
                if (pair.Value.Count > 1)
                {
                    Hashtable Params = new Hashtable();
                    Params.Add("AgentLogo", AgentLogo);
                    paxName = string.Empty;
                    foreach (DataRow dr in pair.Value)
                    {
                        toList.Add(Convert.ToString(dr["fpax_email"]));

                        AgentName = Agent = dr["fvs_agent_name"].ToString();
                        subject = dr["fvs_doc_no"].ToString();
                        country = dr["countryName"].ToString();
                        if (string.IsNullOrEmpty(paxName))
                        {
                            paxName = dr["FPAX_NAME"].ToString();
                        }
                        PaxDetails += "<tr style='padding: 0; text - align:left; vertical - align:top'>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 15px 6px 20px;text-align:left;vertical-align:top;word-wrap:break-word'>" + dr["fvs_doc_no"].ToString() + "</td>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + dr["FPAX_NAME"].ToString() + "</td>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + dr["fvs_doc_no"].ToString() + "</td>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + dr["countryName"].ToString() + "</td>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + dr["fvs_visa_type_name"].ToString() + "</td>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + Convert.ToString(dr["Dispatch_Status"]) + "</td>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + dr["FPAX_DISPATCH_REMARKS"].ToString() + "</td>";
                        //PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + submissionDate + "</td>";
                        //PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + collDate + "</td>";
                        PaxDetails += "</tr>";
                    }
                    if (Params.ContainsKey("PaxDetails"))
                    {
                        Params["PaxDetails"] = PaxDetails;
                    }
                    else
                    {
                        Params.Add("PaxDetails", PaxDetails);
                    }

                    if (Params.ContainsKey("Agent"))
                    {
                        Params["Agent"] = Agent;
                    }
                    else
                    {
                        Params.Add("Agent", Agent);
                    }
                    subject += " - " + AgentName + " Visa Request Status Updates - " + country + " - " + paxName;

                    Hashtable mailDet = new Hashtable();
                    mailDet.Add("from", ConfigurationManager.AppSettings["fromEmail"]);
                    mailDet.Add("reply", toList);
                    mailDet.Add("toArray", bccEmailList);
                    mailDet.Add("subject", subject);
                    mailDet.Add("message", emailMessage);
                    mailDet.Add("params", Params);

                    try
                    {
                        Thread emailThread = new Thread(new ParameterizedThreadStart(sendMail));
                        emailThread.Start(mailDet);
                        emailThread.Join(1000);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Email, Severity.High, 0, "Thread Exception while sending mail in Visa Dispatch Status:" + ex.Message, "");
                    }

                }
                else
                {
                    Hashtable Params = new Hashtable();
                    Params.Add("AgentLogo", AgentLogo);
                    paxName = string.Empty;
                    foreach (DataRow dr in pair.Value)
                    {
                        toList.Add(Convert.ToString(dr["fpax_email"]));
                        
                        AgentName = Agent = dr["fvs_agent_name"].ToString();
                        subject = dr["fvs_doc_no"].ToString();
                        country = dr["countryName"].ToString();
                        if (string.IsNullOrEmpty(paxName))
                        {
                            paxName = dr["FPAX_NAME"].ToString();
                        }
                        PaxDetails = "<tr style='padding: 0; text - align:left; vertical - align:top'>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 15px 6px 20px;text-align:left;vertical-align:top;word-wrap:break-word'>" + dr["fvs_doc_no"].ToString() + "</td>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + dr["FPAX_NAME"].ToString() + "</td>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + dr["fvs_doc_no"].ToString() + "</td>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + dr["countryName"].ToString() + "</td>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + dr["fvs_visa_type_name"].ToString() + "</td>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + Convert.ToString(dr["Dispatch_Status"]) + "</td>";
                        PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + dr["FPAX_DISPATCH_REMARKS"].ToString() + "</td>";
                        //PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + submissionDate + "</td>";
                        //PaxDetails += "<td style='-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-bottom:1px solid #ddd;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:12px;font-weight:400;hyphens:auto;line-height:1.3;margin:0;padding:8px 3px;text-align:left;vertical-align:top;word-wrap:break-word'>" + collDate + "</td>";
                        PaxDetails += "</tr>";
                    }
                    if (Params.ContainsKey("PaxDetails"))
                    {
                        Params["PaxDetails"] = PaxDetails;
                    }
                    else
                    {
                        Params.Add("PaxDetails", PaxDetails);
                    }
                    if (Params.ContainsKey("Agent"))
                    {
                        Params["Agent"] = Agent;
                    }
                    else
                    {
                        Params.Add("Agent", Agent);
                    }
                    subject += " - " + AgentName + " Visa Request Status Updates - " + country + " - " + paxName;

                    Hashtable mailDet = new Hashtable();
                    mailDet.Add("from", ConfigurationManager.AppSettings["fromEmail"]);
                    mailDet.Add("reply", toList);
                    mailDet.Add("toArray", bccEmailList);
                    mailDet.Add("subject", subject);
                    mailDet.Add("message", emailMessage);
                    mailDet.Add("params", Params);

                    try
                    {
                        Thread emailThread = new Thread(new ParameterizedThreadStart(sendMail));
                        emailThread.Start(mailDet);
                        emailThread.Join(1000);
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Email, Severity.High, 0, "Thread Exception while sending mail in Visa Dispatch Status:" + ex.Message, "");
                    }
                }
            }
        }
        catch (Exception ex) {
            Audit.Add(EventType.Email, Severity.High, 0, "Exception While Sending the email Multiple Emails: "+ex.Message, "");
        }

    }

    private static void sendMail(object mailData)
    {
        try
        {
            Hashtable Mail = (Hashtable)mailData;
            Email.Send(Mail["from"].ToString(), Mail["from"].ToString(), Mail["reply"] as List<string>,  Mail["subject"].ToString(), Mail["message"].ToString(), Mail["params"] as Hashtable, Mail["toArray"].ToString());
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, 0, "Exception while sending mail in Visa Dispatch Status:" + ex.Message, "");
        }
    }
}
