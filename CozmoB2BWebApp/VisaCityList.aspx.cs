﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using Visa;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;

public partial class VisaCityList : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        Page.Title = "City List";
        AuthorizationCheck();
        if (!IsPostBack)
        {
            ddlCountry.DataSource = VisaCountry.GetActiveVisaCountryList();
            ddlCountry.DataTextField = "countryName";
            ddlCountry.DataValueField = "countryCode";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Select Country", "-1"));
            if (Request.QueryString.Count > 0 && Request.QueryString[0].Trim() != "")
            {
                ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(Request.QueryString[0].Trim()));
            }
            else
            {
                ddlCountry.SelectedIndex = 1;
            }
            BindData();
        }

    }

    private void BindData()
    {

            
        List<VisaCity> allCity = VisaCity.GetCityList(ddlCountry.SelectedValue);
        if (allCity == null || allCity.Count == 0)
        {
            lbl_msg.Text = "There are no record.";
            lbl_msg.Visible = true;

        }
        else
        {
            lbl_msg.Visible = false;
        }
        GridView1.DataSource = allCity;
        GridView1.DataBind();

    }
    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lb = ((LinkButton)e.Row.Cells[5].Controls[1]);
            lb.CommandArgument = GridView1.DataKeys[e.Row.RowIndex].Value.ToString();
            if (e.Row.Cells[3].Text == "True")
            {
                lb.Text = "Deactivate";
                e.Row.Cells[3].Text = "Active";
            }
            else
            {
                lb.Text = "Activate";
                e.Row.Cells[3].Text = "Inactive";
            }
        }
    }


    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "ChangeStatus")
        {
            bool isActive;
            string status = ((LinkButton)e.CommandSource).Text;
            int rowNumber = Convert.ToInt32(e.CommandArgument);
            // int i = Convert.ToInt32(GridView1.DataKeys[rowNumber].Value);
            if (status == "Activate")
            {
                isActive = true;
            }
            else
            {
                isActive = false;
            }
            VisaCity.ChangeStatus(rowNumber, isActive);
            BindData();

        }
    }

    protected void GridView1_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GridView1.PageIndex = e.NewPageIndex;
        BindData();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        BindData();
    }

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
        //if (Session["roleId"] == null)
        //{
        //    String values = "?errMessage=Login Required to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
        //else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.CozmoVisa))
        //{
        //    String values = "?errMessage=You are not authorised to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
    }

}
