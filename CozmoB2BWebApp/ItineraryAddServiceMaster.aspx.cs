﻿#region NameSpaceRegion
using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.Web.UI.Controls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
#endregion

public partial class ItineraryAddServiceMasterGUI : CT.Core.ParentPage
{
    private string SEARCH_SESSION = "_SearchList";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    IntialiseControls();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void IntialiseControls()
    {
        try
        {
            BindAgentsList();
            BindNationalitiesList();
            Clear();
        }
        catch
        {
            throw;
        }
    }

    private void BindAgentsList()
    {
        try
        {
            DataTable dtAgents = CT.TicketReceipt.BusinessLayer.AgentMaster.GetList(1, "ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            DataView view = dtAgents.DefaultView;
            view.Sort = "agent_Name ASC";
            dtAgents = view.ToTable();
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "agent_Name";
            ddlAgent.DataValueField = "agent_Id";
            ddlAgent.AppendDataBoundItems = true;
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "0"));
        }
        catch
        {
            throw;
        }
    }
    private void BindNationalitiesList()
    {
        try
        {
            chkNationalities.DataSource = Country.GetNationalityList();
            chkNationalities.DataTextField = "Key";
            chkNationalities.DataValueField = "Value";         
            chkNationalities.DataBind();
        }
        catch
        {
            throw;
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            bindSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={
                                         {"HTtxtPromoCode", "service_code"},
                                         {"HTtxtPromoName", "service_name"}, 
                                         };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void bindSearch()
    {
        try
        {
            DataTable dt = ItineraryAddServiceMaster.GetAdditionalServiceMasterDetails(Convert.ToString(Settings.LoginInfo.AgentType),Settings.LoginInfo.AgentId);
            if (dt != null && dt.Rows.Count > 0)
            {
                SearchList = dt;
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvSearch, dt);
            }
        }
        catch { throw; }
    }
    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["service_id"] };
            Session[SEARCH_SESSION] = value;
        }
    }

    private void Edit(int id)
    {
        try
        {
            
            foreach (ListItem chkitem in chkNationalities.Items)
            {
                chkitem.Selected = false;
            }
            ItineraryAddServiceMaster master = new ItineraryAddServiceMaster(id);
            hdfEMId.Value = Convert.ToString(id);
            txtServiceCode.Text = master.Service_code;
            txtServiceName.Text = master.Service_name;
            txtServiceAmount.Text = Convert.ToString(master.Service_amount);
            ddlProduct.SelectedValue = Convert.ToString(master.Service_product);
            ddlAgent.SelectedValue = Convert.ToString(master.Service_agent_id);
            txtServiceType.Text = master.Service_type;

            string[] selNationality = master.Service_nationality.Split(',');
            if (selNationality.Length > 0)
            {
                if(selNationality.Length < chkNationalities.Items.Count)
                {
                    chkAll.Checked = false;
                }
                foreach (string value in selNationality)
                {
                    chkNationalities.Items.FindByValue(value).Selected = true;
                }
            }
            if (master.Service_status == "A")
            {
                chbkActive.Checked = true;
            }
            else
            {
                chbkActive.Checked = false;
            }
            divPromoStatus.Visible = true;
            btnClear.Text = "Cancel";
            btnSave.Text = "Update";
            hdfMode.Value = "1";
        }
        catch
        {
            throw;
        }
    }
    private void Clear()
    {
        try
        {
            hdfMode.Value = "0";
            hdfEMId.Value = "0";
            btnClear.Text = "Clear";
            btnSave.Text = "Save";
            txtServiceCode.Text = string.Empty;
            txtServiceName.Text = string.Empty;
            txtServiceAmount.Text = string.Empty;
            ddlProduct.SelectedIndex = 0;
            ddlAgent.SelectedIndex = 0;
            txtServiceType.Text = string.Empty;
            chkNationalities.ClearSelection();
            divPromoStatus.Visible = false;
            foreach (ListItem chkitem in chkNationalities.Items)
            {
                chkitem.Selected = true;
            }
        }
        catch
        {
            throw;
        }

    }
    #region GridEvents
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //Clear();
            Int32 PId = Utility.ToInteger(gvSearch.SelectedValue);
            Edit(PId);
            this.Master.HideSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void Save()
    {
        try
        {

            ItineraryAddServiceMaster master;
            if (hdfMode.Value == "1")//Edit and Update Mode
            {
                master = new ItineraryAddServiceMaster(Convert.ToInt32(hdfEMId.Value));
            }
            else //Insert Mode
            {
                master = new ItineraryAddServiceMaster();
            }
            string nationalities = string.Empty;
            foreach (ListItem item in chkNationalities.Items)
            {
                if (item.Selected)
                {
                    if (nationalities.Length > 0)
                    {
                        nationalities = nationalities + "," + item.Value;
                    }
                    else
                    {
                        nationalities = item.Value;
                    }
                }
            }
            master.Service_code = txtServiceCode.Text.Trim();
            master.Service_name = txtServiceName.Text.Trim();
            master.Service_amount = Convert.ToDecimal(txtServiceAmount.Text.Trim());
            master.Service_product = Convert.ToInt32(ddlProduct.SelectedItem.Value);
            master.Service_agent_id = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            master.Service_type = txtServiceType.Text.Trim();
            master.Service_nationality = nationalities;
            if (hdfMode.Value == "1")
            {
                if (chbkActive.Checked)
                {
                    master.Service_status = "A";
                }
                else
                {
                    master.Service_status = "D";
                }

            }
            else
            {
                master.Service_status = "A";
            }

            master.Created_by = (int)Settings.LoginInfo.UserID;
            int serviceId = master.Save();

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            if (serviceId > 0)
            {
                if (hdfMode.Value == "1")
                {
                    lblMasterError.Text = "Updated Successfully";
                }
                else
                {
                    lblMasterError.Text = "Saved Successfully";
                }
            }
            else
            {
                lblMasterError.Text = "Duplicate Record !";
            }
            Clear();
        }
        catch
        {
            throw;
        }
    }
    #endregion


}
