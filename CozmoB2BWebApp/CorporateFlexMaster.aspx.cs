﻿using System;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using System.Data;
using CT.TicketReceipt.Web.UI.Controls;
using CT.Corporate;
using System.Collections;
using System.Xml;
using Newtonsoft.Json;

public partial class CorporateFlexMasterUI1 : CT.Core.ParentPage
{
    private string FLEX_MASTER_SESSION = "_FlexMasterList";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    private void InitializePageControls()
    {
        try
        {
            BindAgent();
            AgentMaster agentMaster = new AgentMaster(Convert.ToInt32(ddlAgent.SelectedValue));
            hdnCorporate.Value = agentMaster != null && agentMaster.IsCorporate ? "Y" : "N";
            BindGrid(Settings.LoginInfo.AgentId,1);
            if (Settings.LoginInfo.AgentType == AgentType.Agent || Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgent.Enabled = true;
            }
            BindTravelReasons();
        }
        catch { throw; }
    }
    private void BindAgent()
    {
        try
        {
            ddlAgent.DataSource = AgentMaster.GetList(1, "ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            //ddlAgent.Items.Insert(0, new ListItem("-- All --", "0"));
            ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
        }
        catch { throw; }
    }
    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            AgentMaster agentMaster = new AgentMaster(Convert.ToInt32(ddlAgent.SelectedValue));
            hdnCorporate.Value = agentMaster != null && agentMaster.IsCorporate ? "Y" : "N";
            BindGrid(Utility.ToInteger(ddlAgent.SelectedItem.Value), Convert.ToInt32(ddlProduct.SelectedItem.Value));
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void ClearControls(bool isTrue)
    {
        try
        {
            gvFlexDetails.Visible = isTrue;
            btnClear.Visible = isTrue;
            btnSave.Visible = isTrue;
          
        }
        catch { throw; }
    }

    private DataTable DTFlexMasterList
    {
        get
        {
            return (DataTable)Session[FLEX_MASTER_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["flexId"] };
            Session[FLEX_MASTER_SESSION] = value;
        }
    }
    private void BindGrid()
    {
        try
        {
            CommonGrid grid = new CommonGrid();
            var Check = DTFlexMasterList.AsEnumerable().Where((row) => row.RowState != DataRowState.Deleted);
            if (Check.GetEnumerator().MoveNext())
            {
                DataTable DDt = Check.CopyToDataTable();
                grid.BindGrid(gvFlexDetails, DDt);
                hdnDTFlexMasterList.Value = JsonConvert.SerializeObject(DDt);
            }
            else
            {
                DataTable dt = DTFlexMasterList.Copy();
                dt.Clear();
                grid.BindGrid(gvFlexDetails, dt);
                hdnDTFlexMasterList.Value = JsonConvert.SerializeObject(dt);
            }
            // Hiding the Travel Reasos when the selected agent is non corporate.            
            if (!string.IsNullOrEmpty(hdnCorporate.Value) && hdnCorporate.Value != "Y")
            {
                for (int i = 0; i < gvFlexDetails.Columns.Count; i++)
                {
                    if (gvFlexDetails.Columns[i].HeaderText.ToUpper() == "TRAVEL REASON")
                    {
                        gvFlexDetails.HeaderRow.Cells[i].Visible = false;
                        gvFlexDetails.FooterRow.Cells[i].Visible = false;
                        foreach (GridViewRow row in gvFlexDetails.Rows)
                        {
                            row.Cells[i].Visible = false;
                        } 
                    }
                }
            }
        }
        catch { throw; }
    }
    //Adding new Row
    protected void gvFlexDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Add")
            {
                GridViewRow gvRow = gvFlexDetails.FooterRow;
                DataRow dr = DTFlexMasterList.NewRow();
                DataTable dtDeleted = DTFlexMasterList.GetChanges(DataRowState.Deleted);
                long deletedSerial = 0;
                if (dtDeleted != null)
                {
                    dtDeleted.RejectChanges();
                    deletedSerial = Convert.ToInt32(dtDeleted.Compute("MAX(flexId)", ""));
                }
                long serial = 0;
                if (DTFlexMasterList.Compute("MAX(flexId)", "") != DBNull.Value)
                {
                    serial = Convert.ToInt32(DTFlexMasterList.Compute("MAX(flexId)", ""));
                }
                serial = (serial >= deletedSerial) ? serial + 1 : deletedSerial + 1;
                dr["flexId"] = serial;
                SetFlexDetails(dr, gvRow, "FT");
                DTFlexMasterList.Rows.Add(dr);
                BindGrid();
                BindValid();
                BindTravelReasons();
                hdfFlexDetails.Value = "0";
            }

        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }

    }
    private void SetFlexDetails(DataRow dr, GridViewRow gvRow, string mode)
    {
        try
        {
            dr["flexControl"] = ((DropDownList)gvRow.FindControl(mode + "ddlControl")).SelectedItem.Value;
            dr["flexControlDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlControl")).SelectedItem.Text;
            dr["flexplacehold"] = ((DropDownList)gvRow.FindControl(mode + "ddlplaceHolder")).SelectedItem.Value;
            dr["flexplaceholdDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlplaceHolder")).SelectedItem.Text;
            if (((CheckBox)gvRow.FindControl(mode + "chkDisable")) != null && ((CheckBox)gvRow.FindControl(mode + "chkDisable")).Checked)
            {
                dr["flexMandatoryStatus"] = "N";
                dr["flexMandatoryStatusDESC"] = "No";
            }
            else
            {
                dr["flexMandatoryStatus"] = ((DropDownList)gvRow.FindControl(mode + "ddlMandatory")).SelectedItem.Value;
                dr["flexMandatoryStatusDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlMandatory")).SelectedItem.Text;
            }
            if (((DropDownList)gvRow.FindControl(mode + "ddlMandatory")).SelectedItem.Value == "N")
            {
                string label = ((TextBox)gvRow.FindControl(mode + "txtLabel")).Text;
                DataRow[] filteredRow = DTFlexMasterList.Select("flexLabel ='" + label + "'");
                if(filteredRow.Length!=0 && filteredRow!=null )
                {
                    int id = Convert.ToInt32(filteredRow[0]["flexId"]);
                    filteredRow = DTFlexMasterList.Select("flexValId =" + id);
                    if (filteredRow.Length != 0 && filteredRow != null)
                    {
                        filteredRow[0]["flexMandatoryStatus"] = "N";
                        filteredRow[0]["flexMandatoryStatusDESC"] = "No";
                    }
                    
                }
                
            }
            if (((DropDownList)gvRow.FindControl(mode + "ddlValid")).Items.Count !=0)
            {
                dr["flexValId"] =((DropDownList)gvRow.FindControl(mode + "ddlValid")).SelectedItem.Value;
                dr["flexValIdDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlValid")).SelectedItem.Text;
                if(Convert.ToInt32(((DropDownList)gvRow.FindControl(mode + "ddlValid")).SelectedItem.Value) != 0 && ((DropDownList)gvRow.FindControl(mode + "ddlMandatory")).SelectedItem.Value== "Y")
                {
                    DataRow[] filteredRow = DTFlexMasterList.Select("flexid=" + dr["flexValId"]);
                    if (Convert.ToBoolean(filteredRow[0]["flexDisable"]) != true && Convert.ToString(filteredRow[0]["flexMandatoryStatus"]) != "N")
                    {
                        if(!((CheckBox)gvRow.FindControl(mode + "chkDisable")).Checked)
                        {
                            filteredRow[0]["flexMandatoryStatus"] = "Y";
                            filteredRow[0]["flexMandatoryStatusDESC"] = "Yes";
                        }
                        else
                        {
                            dr["flexMandatoryStatus"] = "N";
                            dr["flexMandatoryStatusDESC"] = "No";
                        }
                    }
                    else
                    {
                        dr["flexMandatoryStatus"] = "N";
                        dr["flexMandatoryStatusDESC"] = "No";
                    }
                }
               
            }
            
            dr["flexLabel"] = ((TextBox)gvRow.FindControl(mode + "txtLabel")).Text;
            dr["flexGDSprefix"] = ((TextBox)gvRow.FindControl(mode + "txtGDSprefix")).Text;
            dr["flexSqlQuery"] = ((TextBox)gvRow.FindControl(mode + "txtSqlQuery")).Text;
            dr["flexDataType"] = ((DropDownList)gvRow.FindControl(mode + "ddlDatatype")).SelectedItem.Value;
            dr["flexDataTypeDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlDatatype")).SelectedItem.Text;            
            dr["flexOrder"] = Convert.ToInt32(((TextBox)gvRow.FindControl(mode + "txtOrder")).Text);
            dr["flexDisable"] = ((CheckBox)gvRow.FindControl(mode + "chkDisable")).Checked;
            dr["flexCreatedBy"] = Settings.LoginInfo.UserID;
            dr["flexCreatedBy"] = string.IsNullOrEmpty(dr["flexCreatedBy"].ToString()) ? 0 : Settings.LoginInfo.UserID;
            dr["flexStatus"] = ((DropDownList)gvRow.FindControl(mode + "ddlFlexStatus")).SelectedItem.Value;
            dr["flexStatusDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlFlexStatus")).SelectedItem.Text;
            if (!string.IsNullOrEmpty(hdnCorporate.Value) && hdnCorporate.Value == "Y")
            {
                dr["flexTravelReason"] = ((DropDownList)gvRow.FindControl(mode + "ddlTravelReason")).SelectedItem.Value;
                dr["flexTravelReasonDESC"] = ((DropDownList)gvRow.FindControl(mode + "ddlTravelReason")).SelectedItem.Text;
            }
            else
            {
                dr["flexTravelReason"] = 0;
                dr["flexTravelReasonDESC"] = "";
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void gvFlexDetails_RowEditing(object sender, GridViewEditEventArgs e)
    {
        try
        {
            gvFlexDetails.EditIndex = e.NewEditIndex;
            BindGrid();
            long serial = Convert.ToInt32(gvFlexDetails.DataKeys[e.NewEditIndex].Value);
            GridViewRow gvRow = gvFlexDetails.Rows[e.NewEditIndex];
            DropDownList ddl = (DropDownList)gvRow.FindControl("EITddlValid");
           
            if (DTFlexMasterList.Rows.Count > 1)
            {
                string label = ((TextBox)gvRow.FindControl("EITtxtLabel")).Text;
                DataRow[] filteredRow = DTFlexMasterList.Select("flexLabel <>'"+ label + "' AND flexControl<>'D'");
                DataTable DTFlexMasterEdit = new DataTable();
                if (filteredRow.Length > 0)
                {
                    DTFlexMasterEdit = filteredRow.CopyToDataTable();
                    ddl.DataSource = DTFlexMasterEdit;
                    ddl.DataTextField = "flexLabel";
                    ddl.DataValueField = "flexId";
                    ddl.DataBind();
                    ddl.Items.Insert(0, new ListItem("Select", "0"));
                }
                DataTable dtTravelReasons = TravelUtility.GetTravelReasonList("S", Convert.ToInt32(ddlAgent.SelectedValue), ListStatus.Short);
                if (dtTravelReasons != null && dtTravelReasons.Rows.Count > 0)
                {
                    DropDownList ddlTravelReason = gvRow.FindControl("EITddlTravelReason") as DropDownList;
                    ddlTravelReason.DataSource = dtTravelReasons;
                    ddlTravelReason.DataTextField = "Description";
                    ddlTravelReason.DataValueField = "ReasonId";
                    ddlTravelReason.DataBind();
                    ddlTravelReason.Items.Insert(0, new ListItem("--Select Reason--", "0"));
                }
            }
       
            ((DropDownList)gvRow.FindControl("EITddlControl")).Text = ((HiddenField)gvRow.FindControl("EIThdfControl")).Value;
            ((DropDownList)gvRow.FindControl("EITddlDataType")).Text = ((HiddenField)gvRow.FindControl("EIThdfDataType")).Value;
            ((DropDownList)gvRow.FindControl("EITddlMandatory")).Text = ((HiddenField)gvRow.FindControl("EIThdfStatus")).Value;
            ((DropDownList)gvRow.FindControl("EITddlplaceHolder")).Text = ((HiddenField)gvRow.FindControl("EIThdfplaceHolder")).Value;
            ((DropDownList)gvRow.FindControl("EITddlValid")).Text = ((HiddenField)gvRow.FindControl("EIThdfValid")).Value;
            ((DropDownList)gvRow.FindControl("EITddlTravelReason")).SelectedValue = ((HiddenField)gvRow.FindControl("EIThdTravelReason")).Value;
            ((DropDownList)gvRow.FindControl("EITddlFlexStatus")).SelectedValue = ((HiddenField)gvRow.FindControl("EIThdFlexStatus")).Value;
            gvFlexDetails.FooterRow.Visible = false;
            hdfFlexDetails.Value = "2";
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvFlexDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvFlexDetails.Rows[e.RowIndex];
            long serial = Convert.ToInt32(gvFlexDetails.DataKeys[e.RowIndex].Value);
            DTFlexMasterList.Rows.Find(serial).Delete();
           
            BindGrid();
            BindValid();
            BindTravelReasons();
            gvFlexDetails.EditIndex = -1;
            hdfFlexDetails.Value = "0";
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvFlexDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            GridViewRow gvRow = gvFlexDetails.Rows[e.RowIndex];
            long serial = Convert.ToInt32(gvFlexDetails.DataKeys[e.RowIndex].Value);
            DataRow dr = DTFlexMasterList.Rows.Find(serial);
            dr.BeginEdit();
            SetFlexDetails(dr, gvRow, "EIT");
            dr.EndEdit();
            gvFlexDetails.EditIndex = -1;
            BindGrid();
            BindValid();
            BindTravelReasons();
            hdfFlexDetails.Value = "0";
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvFlexDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {
            gvFlexDetails.EditIndex = -1;
            BindGrid();
            BindValid();
            BindTravelReasons();
            hdfFlexDetails.Value = "0";
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void gvFlexDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvFlexDetails.PageIndex = e.NewPageIndex;
            gvFlexDetails.EditIndex = -1;
            BindGrid();
            BindValid();
            BindTravelReasons();
            hdfFlexDetails.Value = "0";
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (DTFlexMasterList.Rows.Count <= 0) { throw new Exception("Flex details cannot be blank !"); }
            FlexMaster flexMaster = new FlexMaster();
            flexMaster.AgentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            flexMaster.ProductId = Convert.ToInt32(ddlProduct.SelectedValue);
            flexMaster.DtFlexMaster = DTFlexMasterList;             
            flexMaster.Save();
            BindGrid(flexMaster.AgentId, flexMaster.ProductId);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = "Flex Details Saving Successfull";
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }

    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo.MemberType == MemberType.SUPER)
            {
                gvFlexDetails.Controls.Clear();
                DTFlexMasterList.Rows.Clear();
                ClearControls(false);
                hdfFlexDetails.Value = "0";
                ddlAgent.SelectedIndex = -1;
            }
            else
            {
                hdfFlexDetails.Value = "0";
                BindGrid(Utility.ToInteger(ddlAgent.SelectedItem.Value),Convert.ToInt32(ddlProduct.SelectedItem.Value));
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    private void BindGrid(int agentId,int ProductId)
    {
        try
        {
            if (agentId > 0)
            {
                FlexMaster tempFlex = new FlexMaster(agentId, ProductId);
                DTFlexMasterList = tempFlex.DtFlexMaster;
                gvFlexDetails.EditIndex = -1;
                BindGrid();
                BindValid();
                BindTravelReasons();
                ClearControls(true);
            }
            else
            {
                gvFlexDetails.Controls.Clear();
                DTFlexMasterList.Rows.Clear();
                ddlAgent.SelectedIndex = -1;
                hdfFlexDetails.Value = "0";
                ClearControls(false);

            }
        }
        catch { throw; }
    }

    private void BindValid()
    {
        GridViewRow gvRow = gvFlexDetails.FooterRow;
        DropDownList ddl = (DropDownList)gvRow.FindControl("FTddlValid");
        if (DTFlexMasterList.Rows.Count > 0)
        {
           DataRow[] filteredRow = DTFlexMasterList.Select("flexControl<>'D'");
            DataTable filteredData = new DataTable();
            if (filteredRow.Length > 0)
            {
                filteredData = filteredRow.CopyToDataTable(); 
            }
            if (filteredData.Rows.Count > 0)
            {
                ddl.DataSource = filteredData;
                ddl.DataTextField = "flexLabel";
                ddl.DataValueField = "flexId";
                ddl.DataBind();
                ddl.Items.Insert(0, new ListItem("Select", "0"));
            }
           
        }
    }
    private void BindTravelReasons()
    {
        DataTable dtTravelReasons = TravelUtility.GetTravelReasonList("S",Convert.ToInt32(ddlAgent.SelectedValue), ListStatus.Short);
        DropDownList  ddl = gvFlexDetails.FooterRow.FindControl("FTddlTravelReason") as DropDownList;
        ddl.DataSource = dtTravelReasons;
        ddl.DataTextField = "Description";
        ddl.DataValueField = "ReasonId";
        ddl.DataBind();
        ddl.Items.Insert(0, new ListItem("--Select Reason--", "0"));

    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        BindGrid(Utility.ToInteger(ddlAgent.SelectedItem.Value), Convert.ToInt32(ddlProduct.SelectedItem.Value));
    }
}
