﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using CT.Roster;


public partial class ROSLocationMasterGUI : CT.Core.ParentPage
{
    string ROSLOC_SEARCH_SESSION = "_rosLocMasterSearchList";
    string ROSLOC_SESSION = "_rosLocMaster";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;

            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    #region Session Properties
    private ROSLocationMaster CurrentObject
    {
        get
        {
            return (ROSLocationMaster)Session[ROSLOC_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(ROSLOC_SESSION);
            }
            else
            {
                Session[ROSLOC_SESSION] = value;
            }

        }
    }
    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[ROSLOC_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["loc_id"] };

            Session[ROSLOC_SEARCH_SESSION] = value;
        }
    }


    #endregion
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void Save()
    {
        try
        {
            ROSLocationMaster rosLocMaster;
            if (CurrentObject == null)
            {
                rosLocMaster = new ROSLocationMaster();
            }
            else
            {
                rosLocMaster = CurrentObject;
            }

            rosLocMaster.LocName = txtLocName.Text;
            rosLocMaster.LocType = ddlLocType.SelectedValue;
            rosLocMaster.LocDetail = txtLocDetail.Text;
            rosLocMaster.CreatedBy = Settings.LoginInfo.AgentId;
            rosLocMaster.Save();

            lblMessage.Visible = true;
            lblMessage.Text = Formatter.ToMessage(("Transaction For "), txtLocName.Text, (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated)) + " Successfully.";
            Clear();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw;
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            lblMessage.Visible = false;
            Clear();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    private void Clear()
    {
        try
        {
            txtLocName.Text = string.Empty;
            ddlLocType.SelectedIndex = 0;
            txtLocDetail.Text = string.Empty;
            CurrentObject = null;
            btnSave.Text = "Save";
            btnClear.Text = "Clear";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            lblMessage.Visible = false;
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void bindSearch()
    {
        try
        {
            DataTable dt = ROSLocationMaster.GetROSLocMasterList(RecordStatus.Activated,ListStatus.Long);
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;

        }
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }

    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            int locId = Utility.ToInteger(gvSearch.SelectedValue);
            Edit(locId);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{"HTtxtLocName", "loc_name" }
                                             ,{ "HTtxtLocType", "loc_type" }
                                             ,{ "HTtxtLocDetail", "loc_detail" }
                                         };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }
    }

    private void Edit(int LocId)
    {
        try
        {
            lblMessage.Visible = false;

            ROSLocationMaster getLocMasterData = new ROSLocationMaster(LocId);
            CurrentObject = getLocMasterData;
            txtLocName.Text = getLocMasterData.LocName;
            ddlLocType.SelectedValue = getLocMasterData.LocType;
            txtLocDetail.Text = getLocMasterData.LocDetail;            

            btnSave.Text = "Update";
            btnClear.Text = "Cancel";
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, ex.Message, "0");
            throw ex;
        }

    }

}
