using System;
using System.Data;
using System.Collections;
using System.Web.UI;
using CT.ActivityDeals;
using System.Collections.Generic;
using CozmoB2BWebApp;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;

public partial class CityAjax : System.Web.UI.Page
{
   
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request["requestFrom"] == "cityList")
        {
            string cityString = "";
            string countryCode = Request["ccode"];
            SortedList cities = CT.Core.RegCity.GetListByCountry(countryCode);

            
            foreach (DictionaryEntry city in cities)
            {
               if (cityString.Length > 0)
               {
                    cityString += "/" + city.Key.ToString() + "," + city.Value.ToString();
               }
              else
               {
                   cityString = city.Key.ToString() + "," + city.Value.ToString();
               }

                
            }
            
            Response.Write(cityString);
        }
        else if (Request["requestSource"] == "HotelSearchDomestic")
        {
            string key = Request["searchKey"];
            key = key.ToLower();

            string tempKey = string.Empty;
            string presentIndex = string.Empty;
            foreach (char c in key)
            {
                if (c >= 'a' && c <= 'z')
                {
                    tempKey += c;
                }
            }
            key = tempKey.ToLower();
            string value = string.Empty;
            foreach (City aPort in ((Trie<City>)Page.Application["tree4"]).PrefixFind(key))
            {
                if (!(presentIndex.Contains(aPort.Index + ",")))
                {
                    presentIndex += aPort.Index + "," + aPort.CityName + ",";
                    value = value + aPort.CityCode + "," + aPort.CityName + "," + aPort.StateProvince + "," + aPort.CountryName + "/";
                }
            }
            string str = string.Empty;
            if (value.Length != 0)
            {
                str = value.Remove(value.Length - 1);
            }
            Response.Write(str);
        }
        else if (Request["requestSource"] == "PreferredAirline")
        {
            string key = Request["searchKey"];
            key = key.ToLower();
            key = (key.Contains(" ") ? key.Split(' ')[0] : key);
            
            string tempKey = string.Empty;
            string presentIndex = string.Empty;
            foreach (char c in key)
            {
                //if (c >= 'a' && c <= 'z')//Allow alpha-numeric eg: G9 or 9W etc
                if(Char.IsLetterOrDigit(c))//do not allow special chars
                {
                    tempKey += c;
                }
            }
            key = tempKey.ToLower();
            string value = string.Empty;

            //Search with Airline Code
            if (key.Length <= 2)
            {
                foreach (AirlineInfo aLine in ((Trie<AirlineInfo>)Page.Application["tree5"]))
                {
                    if (aLine.AirlineCode.Length == key.Length && aLine.AirlineCode.ToLower() == key)
                    {
                        if (!presentIndex.Contains(aLine.AirlineCode))
                        {
                            presentIndex += aLine.AirlineCode + "," + aLine.AirlineName + ",";
                            value = value + aLine.AirlineCode + "," + aLine.AirlineName + "/";
                        }
                    }
                }
            }
            else if(key.Length > 2)//Search Airline name
            {
                foreach (AirlineInfo aLine in ((Trie<AirlineInfo>)Page.Application["tree5"]).PrefixFind(key))
                {
                    presentIndex += aLine.AirlineCode + "," + aLine.AirlineName + ",";
                    value = value + aLine.AirlineCode + "," + aLine.AirlineName + "/";
                }
            }
            string str = string.Empty;
            if (value.Length != 0)
            {
                str = value.Remove(value.Length - 1);
            }
            Response.Write(str);
        }
        else if (Request["requestSource"] == "getCabinTypeList")
        {
            string response = "";
            try
            {
                System.Collections.Generic.Dictionary<string, string> dictionary = new System.Collections.Generic.Dictionary<string, string>();
                dictionary.Add("1", "Business");
                dictionary.Add("2", "Economy");
                dictionary.Add("3", "First");
                dictionary.Add("4", "PremiumEconomy");
                if (dictionary.ContainsKey(Request["removeCabinType"]))
                {
                    dictionary.Remove(Request["removeCabinType"]);
                }
                foreach (System.Collections.Generic.KeyValuePair<string, string> item in dictionary)
                {
                    if (response.Length > 0)
                    {
                        response += "," + item.Value + "|" + item.Key.ToString();
                    }
                    else
                    {
                        response = Request["id"] + "#" + item.Value + "|" + item.Key.ToString();
                    }
                }
                Response.Write(response);
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

        }



        else if (Request["requestSource"] == "getAirPortList")
        {
            string response = "";
            try
            {
                System.Collections.Generic.List<CT.BookingEngine.Airport> CityList = CT.BookingEngine.Airport.GetAirportsListForCountry(Request["Country"]);
                foreach (CT.BookingEngine.Airport item in CityList)
                {
                    if (response.Length > 0)
                    {
                        response += "," + item.CityName + "|" + item.CityCode.ToString();
                    }
                    else
                    {
                        response = Request["id"] + "#" + item.CityName + "|" + item.CityCode.ToString();
                    }
                }
                Response.Write(response);
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

        }


        else if (Request["requestSource"] == "getCountryIdByCities")
        {
            string response = "";
            try
            {
                DataTable CityList =CT.ActivityDeals.Activity.GetCity(Request["Country"]);

                foreach (DataRow item in CityList.Rows)
                {
                    if (response.Length > 0)
                    {
                        response += "," + item["CityName"].ToString() + "|" + item["CityCode"].ToString();
                    }
                    else
                    {
                        response = Request["id"] + "#" + item["CityName"].ToString() + "|" + item["CityCode"].ToString();
                    }
                }

                Response.Write(response);
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
        }
        else if (Request["requestSource"] == "packageGetCountryIdByCities")
        {
            string response = "";
            try
            {
                DataTable CityList = CT.ActivityDeals.Activity.GetCity(Request["Country"]);

                foreach (DataRow item in CityList.Rows)
                {
                    if (response.Length > 0)
                    {
                        response += "," + item["CityName"].ToString() + "|" + item["CityCode"].ToString();
                    }
                    else
                    {
                        response = Request["id"] + "#" + Request["CountryId"] + "#" +Request["selId"] +"#" + item["CityName"].ToString() + "|" + item["CityCode"].ToString();
                    }
                }

                Response.Write(response);
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
        }


        //Added by Lokesh on 28Mar2018 To get the GST State Id's
        //This is only for G9 source .
        //Only For Lead Pax.
        //If the customer is travelling from India only.
        else if (Request["requestSource"] == "getStates")
        {
            string response = "";
            try
            {
                List<CT.Core.State> statesList = CT.Core.State.GetStates(Convert.ToString(Request["countryCode"]));
                foreach (CT.Core.State item in statesList)
                {
                    if (response.Length > 0)
                    {
                        response += "," + item.StateName + "|" + item.StateCode.ToString();
                    }
                    else
                    {
                        response = Request["id"] + "#" + item.StateName + "|" + item.StateCode.ToString();
                    }
                }
                Response.Write(response);
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }

        }

        //Added By lokesh
        //Get Agent Locations Based on AgentId
        //Used in HotelSearch.aspx page.
        else if (Request["requestSource"] == "getAgentsLocationsByAgentId")
        {
            string response = "";
            try
            {
                //Retrieve On behalf agent details to know whether routing is enabled or not
                CT.TicketReceipt.BusinessLayer.AgentMaster agent = new CT.TicketReceipt.BusinessLayer.AgentMaster(Convert.ToInt32(Request["AgentId"]));
                
                DataTable dtLocations = CT.TicketReceipt.BusinessLayer.LocationMaster.GetList(Convert.ToInt32(Request["AgentId"]), CT.TicketReceipt.BusinessLayer.ListStatus.Short, CT.TicketReceipt.BusinessLayer.RecordStatus.Activated, string.Empty);
                if (dtLocations != null && dtLocations.Rows.Count > 0)
                {
                    foreach (DataRow item in dtLocations.Rows)
                    {
                        if (response.Length > 0)
                        {
                            response += "," + item["location_name"].ToString() + "|" + item["location_id"].ToString();
                        }
                        else
                        {
                            //add as the first param
                            response = agent.IsRoutingEnabled.ToString().ToLower() + "#" + item["location_name"].ToString() + "|" + item["location_id"].ToString();
                        }
                    }
                }
                Response.Write(response);
            }
            catch (Exception ex)
            {
                response = ex.Message;
            }
        }
        //Added by somasekhar on 28/09/2018
        // Get Agent County Code Based on AgentId Location Id
        //Used in HotelSearch.aspx page. To set Default Nationality and Residence India for IN county code
        else if (Request["requestSource"] == "getAgentsCountryByLocId")
        {
            CT.Core.Audit.Add(EventType.Search, Severity.High, 0, "getAgentsCountryByLocId enter", "");

            string response = "";
            try
            {
                CT.Core.Audit.Add(EventType.Search, Severity.High, 0, "getAgentsCountryByLocId enter 1", "");
                int loc = Convert.ToInt32(Request["LocationId"]);
                CT.Core.Audit.Add(EventType.Search, Severity.High, 0, "getAgentsCountryByLocId loc id  "+Convert.ToString(loc), "");
                string countryCode = CT.TicketReceipt.BusinessLayer.LocationMaster.GetCountryCodeByLocationId(loc);
                CT.Core.Audit.Add(EventType.Search, Severity.High, 0, "getAgentsCountryByLocId enter 2", "");
                CT.Core.Audit.Add(EventType.Search, Severity.High, 0, "getAgentsCountryByLocId countryCode:"+countryCode, "");
                if (!string.IsNullOrEmpty(countryCode))
                {
                    response = countryCode;
                }
                CT.Core.Audit.Add(EventType.Search, Severity.High, 0, "getAgentsCountryByLocId response:" + response, "");
                Response.Write(response);
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(EventType.Search, Severity.High, 0, "getAgentsCountryByLocId exeption "+ex.ToString(), "");
                response = ex.Message;
            }
        }
        else if(Request["requestSource"] == "getRoutingSources")
        {
            string response = string.Empty;

            try
            {
                DataTable dtFlightSources = AgentMaster.GetAgentSources(Convert.ToInt32(Request["agentid"]), 1);
                foreach (DataRow row in dtFlightSources.Rows)
                {
                    if (Convert.ToBoolean(row["IsRouting"]))
                    {
                        if (string.IsNullOrEmpty(response))
                        {
                            response = row["Name"].ToString() + "," + row["Id"].ToString();
                        }
                        else
                        {
                            response += "-" + row["Name"].ToString() + "," + row["Id"].ToString();
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to Bind Routing Sources. Reason : " + ex.ToString(), Request["REMOTE_ADDR"]);
            }

            Response.Write(response);
        }

        else if (Request["requestSource"] == "HotelSearchDomesticForGimmonix")
        {
            string key = Request["searchKey"];
            key = key.ToLower();

            string tempKey = string.Empty;
            string presentIndex = string.Empty;
            foreach (char c in key)
            {
                if (c >= 'a' && c <= 'z')
                {
                    tempKey += c;
                }
            }
            key = tempKey.ToLower();
            string value = string.Empty;
            foreach (City aPort in ((Trie<City>)Page.Application["GimmonixCityList"]).PrefixFind(key))
            {
                //if (!(presentIndex.Contains(aPort.Index + ",")))
                if (!(presentIndex.Contains(aPort.Index + ",")) && !value.Contains(aPort.CityName + "," + aPort.CountryName))
                {
                    presentIndex += aPort.Index + "," + aPort.CityName + ",";
                    value = value + aPort.CityName + "," + aPort.CountryName + "/";
                }
            }
            string str = string.Empty;
            if (value.Length != 0)
            {
                str = value.Remove(value.Length - 1);
            }
            Response.Write(str);
        }

        else
        {
            string key = Request["searchKey"];
            key = key.ToLower();
            bool isDomestic = Convert.ToBoolean(Request["isDomestic"]);
            string tempKey = string.Empty;
            string presentIndex = string.Empty;
            foreach (char c in key)
            {
                if (c >= 'a' && c <= 'z')
                {
                    tempKey += c;
                }
            }
            key = tempKey.ToLower();
            string value = string.Empty;
            //int i =0;
            if (isDomestic)
            {
                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(key))
                {
                    if (!presentIndex.Contains(aPort.Index + ","))
                    {
                        presentIndex += aPort.Index + "," + aPort.CityName + ",";
                        value = value + aPort.CityName + " (" + aPort.AirportCode + ")" + "/";// +"," + aPort.CountryName + "-" + aPort.AirportName + " (" + aPort.AirportCode + ")" + "/";
                    }
                }
            }
            else
            {

                foreach (airport aPort in ((Trie<airport>)Page.Application["tree3"]).PrefixFind(key))// For Airport Code
                {
                    if (!presentIndex.Contains(aPort.Index + ","))
                    {
                        presentIndex += aPort.Index + "," + aPort.CityName + ",";
                        //value = value + aPort.CityName + " (" + aPort.AirportCode + ")" + "/"; // + "," + aPort.CountryName + "-" + aPort.AirportName + " (" + aPort.AirportCode + ")" + "/";
                        value = value + aPort.CityName + " (" + aPort.AirportCode + ")," + aPort.CountryName + "/"; // + "," + aPort.CountryName + "-" + aPort.AirportName + " (" + aPort.AirportCode + ")" + "/"; // For insuracne also to add Country --addded by ziyad
                    }
                }
            }   //Console.WriteLine(aPort.Index);
            //value=value.Remove(value.Length - 2,2);
            //value.Remove(value.Length - 2);
            string str = string.Empty;
            if (value.Length != 0)
            {
                str = value.Remove(value.Length - 1);
            }
            Response.Write(str);
        }
    }
}
