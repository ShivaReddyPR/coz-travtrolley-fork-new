﻿using CT.AccountingEngine;
using CT.BookingEngine;
using CT.BookingEngine.Insurance;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class BaggageInsuranceChangeRequestQueueGUI : CT.Core.ParentPage
    {
        PagedDataSource pagedData = new PagedDataSource();

        protected string pagingEnable = "style='display:block'";
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    InitializeControls();
                    GetBaggageInsuranceChangeQueue();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        private void InitializeControls()
        {
            try
            {
                BindAgency();
                int b2bAgentId;
                int b2b2bAgentId;
                if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
                {
                    ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                }
                else if (Settings.LoginInfo.AgentType == AgentType.Agent)
                {
                    ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    ddlAgency.Enabled = false;
                }
                else if (Settings.LoginInfo.AgentType == AgentType.B2B)
                {
                    ddlAgency.Enabled = false;
                    b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                    ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);

                }
                else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
                {
                    ddlAgency.Enabled = false;

                    b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                    b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                    ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);

                }

                txtCreatedDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");

            }

            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindAgency()
        {
            try
            {
                DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);// AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
                ddlAgency.DataSource = dtAgents;
                ddlAgency.DataTextField = "Agent_Name";
                ddlAgency.DataValueField = "agent_id";
                ddlAgency.DataBind();
                ddlAgency.Items.Insert(0, new ListItem("--All--", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        public int CurrentPage
        {
            get
            {
                if (ViewState["_currentPage"] == null)
                {
                    return 0;
                }
                else
                {
                    return (int)ViewState["_currentPage"];
                }
            }

            set
            {
                ViewState["_currentPage"] = value;
            }
        }

        private void Clear()
        {
            txtCreatedDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtPNRno.Text = string.Empty;
            txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            Session["BIchangeReqInsQueue"] = null;
            GetBaggageInsuranceChangeQueue();
        }
        void doPaging()
        {
            DataTable dt = (DataTable)Session["BIchangeReqInsQueue"];
            pagedData.DataSource = dt.DefaultView;
            pagedData.AllowPaging = true;
            pagedData.PageSize = 5;
            Session["count"] = pagedData.PageCount - 1;
            pagedData.CurrentPageIndex = CurrentPage;
            btnPrev.Visible = (!pagedData.IsFirstPage);
            btnFirst.Visible = (!pagedData.IsFirstPage);
            btnNext.Visible = (!pagedData.IsLastPage);
            btnLast.Visible = (!pagedData.IsLastPage);
            lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
            DataView dView = (DataView)pagedData.DataSource;
            DataTable dTable;
            dTable = (DataTable)dView.Table;
            dlBaggageInsChangeReqQueue.DataSource = pagedData;
            dlBaggageInsChangeReqQueue.DataBind();
            if (dt.Rows.Count <= 0)
            {
                pagingEnable = "style='display:none'";
            }
        }
        protected void btnPrev_Click(object sender, EventArgs e)
        {
            CurrentPage--;
            doPaging();
        }
        protected void btnNext_Click(object sender, EventArgs e)
        {
            CurrentPage++;
            doPaging();
        }
        protected void btnFirst_Click(object sender, EventArgs e)
        {
            CurrentPage = 0;
            doPaging();
        }
        protected void btnLast_Click(object sender, EventArgs e)
        {
            CurrentPage = (int)Session["count"];
            doPaging();
        }

        protected void dlBaggageInsChangeReqQueue_ItemDataBound(object sender, DataListItemEventArgs e)
        {
            try
            {
                if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
                {

                    Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                    HtmlTable tblRefund = e.Item.FindControl("tblRefund") as HtmlTable;
                    int state = Convert.ToInt32(((DataRowView)e.Item.DataItem).Row["BI_BookingStatus_ID"].ToString());
                    Button btnRefund = e.Item.FindControl("btnRefund") as Button;
                    Label lblPrice = e.Item.FindControl("lblPrice") as Label;
                    Label lblAgentCurrency = e.Item.FindControl("lblAgentCurrency") as Label;
                    HiddenField hdnPlanId = e.Item.FindControl("hdnPlanId") as HiddenField;
                    HiddenField hdnRequestId = e.Item.FindControl("hdnRequestId") as HiddenField;
                    decimal totalAmt = 0;// markup = 0, b2cMarkup = 0, discount = 0, gst = 0, inputVAT = 0, outputVAT = 0;
                    DataRowView view = e.Item.DataItem as DataRowView;
                    int agentId = Convert.ToInt32(view["BI_AGENT_ID"]);
                    totalAmt = Convert.ToDecimal(view["BI_TOTAL_PRICE"]);
                    //markup = Convert.ToDecimal(view["PaxMarkUp"]);
                    //gst=  Convert.ToDecimal(view["PAX_GST"]);
                    //inputVAT= Convert.ToDecimal(view["PAX_INPUTVAT"]);
                    //outputVAT=Convert.ToDecimal(view["PAX_OUTPUTVAT"]);
                    //b2cMarkup = 0; //Convert.ToDecimal(view["B2CMarkup"]);
                    //discount = Convert.ToDecimal(view["PaxDiscount"]);
                    hdnPlanId.Value = Convert.ToString(view["BI_PLAN_ID"]);
                    //DataSet ds = header.LoadBaggageHeader(IPHId);

                    hdnRequestId.Value = Convert.ToString(view["requestId"]);

                    AgentMaster agent = new AgentMaster(agentId);

                    //lblPrice.Text = agent.AgentCurrency + " " + (totalAmt +  markup - discount + b2cMarkup+gst+inputVAT+ outputVAT).ToString("N" + agent.DecimalValue);//add markup and b2c markup in total price, by chandan on 16062016

                    lblPrice.Text = agent.AgentCurrency + " " + totalAmt.ToString("N" + agent.DecimalValue);


                    btnRefund.OnClientClick = "return ValidateFee('" + e.Item.ItemIndex + "');";
                    lblStatus.Text = BaggageInsuranceBookingStatus.CancelRequest.ToString();
                    if (state != (int)BaggageInsuranceBookingStatus.Cancelled)
                    {
                        tblRefund.Visible = true;
                        lblStatus.Text = "REQUESTED FOR CANCEL";
                        lblStatus.ForeColor = System.Drawing.Color.Green;
                    }
                    else
                    {
                        tblRefund.Visible = false;
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                    }
                    lblAgentCurrency.Text = agent.AgentCurrency;
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + ":baggage ins_changeREq_Q:dlBaggageInsChangeReqQueue_ItemDataBound ", "0");
            }
        }

        protected void dlBaggageInsChangeReqQueue_ItemCommand(object source, DataListCommandEventArgs e)
        {
            if (e.CommandName == "Refund")
            {
                if (e.CommandArgument.ToString().Length > 0)
                {


                    int IPHId = Convert.ToInt32(e.CommandArgument);
                    BaggageInsuranceHeader header = new BaggageInsuranceHeader();
                    DataSet ds = header.LoadBaggageHeader(IPHId);
                    TextBox txtAdminFee = e.Item.FindControl("txtAdminFee") as TextBox;
                    HiddenField hdnPlanId = e.Item.FindControl("hdnPlanId") as HiddenField;
                    HiddenField hdnrequestId = e.Item.FindControl("hdnRequestId") as HiddenField;
                    int planId = Convert.ToInt32(hdnPlanId.Value);
                    //decimal serviceFee= Convert.ToDecimal(ds.Tables[0].Rows[0]["ServiceFee"].ToString());
                    int noOfPax= Convert.ToInt32(ds.Tables[0].Rows[0]["BI_NOOF_PAX"].ToString());
                    decimal amount = Convert.ToDecimal(ds.Tables[0].Rows[0]["BI_TOTAL_PRICE"].ToString());
                    decimal markup = Convert.ToDecimal(ds.Tables[1].Rows[0]["PAX_MarkUp"].ToString());
                    decimal discount = Convert.ToDecimal(ds.Tables[1].Rows[0]["PAX_Discount"].ToString());
                    decimal gstamount = Convert.ToDecimal(ds.Tables[1].Rows[0]["PAX_GST"].ToString());
                    decimal inputVAT = Convert.ToDecimal(ds.Tables[1].Rows[0]["PAX_INPUTVAT"].ToString());
                    decimal outputVAT = Convert.ToDecimal(ds.Tables[1].Rows[0]["PAX_OUTPUTVAT"].ToString());
                    markup = markup * noOfPax;
                    gstamount = gstamount * noOfPax;
                    decimal totalbookingAmt = 0;
                    try
                    {
                        if (header != null)
                        {
                            if (header.Bid > 0)
                            {
                                decimal bookingAmt = amount;//+ markup - discount + gstamount+ inputVAT+ outputVAT;

                                Dictionary<string, string> cancellationData = new Dictionary<string, string>();
                                cancellationData.Add("Status", "Cancelled");
                                AgentMaster agent = new AgentMaster(header.AgentID);

                                if (cancellationData["Status"] == "Cancelled" || cancellationData["Status"] == "CANCELLED")
                                {

                                    //TODO Ziyad  check the cancellation charges from the response and deduct from booking amount.
                                    decimal adminFee = Convert.ToDecimal(txtAdminFee.Text);
                                    totalbookingAmt = bookingAmt;
                                    if (adminFee <= bookingAmt)
                                    {
                                        totalbookingAmt -= (adminFee + markup + gstamount);
                                        if(totalbookingAmt < 0)
                                        {
                                            Utility.Alert(this.Page,"Entered amount is greater than booking amount.");
                                            GetBaggageInsuranceChangeQueue();
                                            return;
                                        }
                                    }
                                    if (totalbookingAmt > 0)
                                    {
                                        decimal updatebookingamount = bookingAmt - (markup + gstamount);
                                        agent.UpdateBalance(-updatebookingamount);
                                    }
                                    CancellationCharges cancellationCharge = new CancellationCharges();
                                    cancellationCharge.AdminFee = Convert.ToDecimal(txtAdminFee.Text);
                                    cancellationCharge.SupplierFee = 0;
                                    cancellationCharge.PaymentDetailId = 0;//pd.PaymentDetailId;
                                    cancellationCharge.ReferenceId = header.Bid;
                                    cancellationCharge.ProductType = ProductType.BaggageInsurance;
                                    cancellationCharge.CancelPenalty = bookingAmt;
                                    cancellationCharge.CreatedBy = (int)Settings.LoginInfo.UserID;
                                    cancellationCharge.Save();

                                    //Ledger transaction
                                    NarrationBuilder objNarration = new NarrationBuilder();
                                    LedgerTransaction ledgerTxn = new LedgerTransaction();
                                    ledgerTxn.LedgerId = (int)header.AgentID;
                                    int invoiceNumber = CT.BookingEngine.Invoice.isInvoiceGenerated(header.Bid, CT.BookingEngine.ProductType.BaggageInsurance);
                                    ledgerTxn.Amount = -adminFee;
                                    objNarration.PaxName = string.Format("{0} {1}", header.FirstName, header.LastName);
                                    objNarration.Remarks = "Baggage Insurance Cancellation Charges";
                                    objNarration.PolicyNo = header.Policy;

                                    ledgerTxn.Narration = objNarration;
                                    ledgerTxn.ReferenceId = (int)header.Bid;
                                    ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.InsuranceCancellationCharge;
                                    ledgerTxn.Notes = "";
                                    ledgerTxn.Date = DateTime.UtcNow;
                                    ledgerTxn.CreatedBy = (int)Settings.LoginInfo.UserID;
                                    ledgerTxn.TransType = "B2B";
                                    ledgerTxn.Save();
                                    LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                                    if (adminFee <= bookingAmt)
                                    {
                                        bookingAmt -= (adminFee+ markup + gstamount);
                                       
                                    }
                                    
                                    ledgerTxn = new LedgerTransaction();
                                    ledgerTxn.LedgerId = header.AgentID;
                                    ledgerTxn.Amount = bookingAmt;
                                    objNarration.PaxName = string.Format("{0} {1}", header.FirstName, header.LastName);
                                    objNarration.Remarks = "Baggage Insurance Refund";
                                    objNarration.PolicyNo = header.Policy;

                                    ledgerTxn.Narration = objNarration;
                                    ledgerTxn.ReferenceId = header.Bid;
                                    ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.InsuranceRefund;
                                    ledgerTxn.Notes = "";
                                    ledgerTxn.Date = DateTime.UtcNow;
                                    ledgerTxn.CreatedBy = (int)Settings.LoginInfo.UserID;
                                    ledgerTxn.TransType = "B2B";//header.TransType;
                                    ledgerTxn.Save();
                                    LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);


                                    if (header.TransType != "B2C")
                                    {
                                        agent.CreatedBy = Settings.LoginInfo.UserID;
                                        agent.UpdateBalance(adminFee);
                                       

                                        //if (header.AgentID == Settings.LoginInfo.AgentId)
                                        //{
                                        //    Settings.LoginInfo.AgentBalance += bookingAmt;
                                        //}
                                    }
                                    //updateing service request
                                    ServiceRequest sr = new ServiceRequest();
                                    sr.UpdateServiceRequestAssignment(Convert.ToInt32(hdnrequestId.Value), (int)ServiceRequestStatus.Completed, (int)Settings.LoginInfo.UserID, (int)ServiceRequestStatus.Completed, null);
                                    BaggageInsuranceHeader.Update(header.Bid, (int)InsuranceBookingStatus.Cancelled, Convert.ToInt32(Settings.LoginInfo.UserID));

                                    //Sending email.
                                    Hashtable table = new Hashtable(3);
                                    table.Add("agentName", agent.Name);
                                    table.Add("policyName", header.PlanTitle);
                                    table.Add("policyNo", header.Policy);

                                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                                    UserMaster bookedUser = new UserMaster(header.CreatedBy);
                                    AgentMaster bookedAgency = new AgentMaster(header.AgentID);
                                    if (!string.IsNullOrEmpty(bookedUser.Email))
                                    {
                                        toArray.Add(bookedUser.Email);
                                    }
                                    if (!string.IsNullOrEmpty(bookedAgency.Email1))
                                    {
                                        toArray.Add(bookedAgency.Email1);
                                    }
                                    toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
                                    toArray.Add(ConfigurationManager.AppSettings["BAGGAGE_INS_CANCEL_MAIL"]);

                                    string message = ConfigurationManager.AppSettings["INSURANCE_REFUND"];
                                    try
                                    {
                                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Baggage Insurance) Response for cancellation. Policy No:(" + header.Policy + ")", message, table);
                                    }
                                    catch (Exception ex)
                                    {

                                    }
                                }



                            }

                            GetBaggageInsuranceChangeQueue();
                        }


                    }
                    catch (Exception ex)
                    {
                        CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + ":ins_changeREq_Q:dlInsQueue_ItemCommand() ", "0");
                    }
                }
            }
        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                GetBaggageInsuranceChangeQueue();
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "Baggage Insurance change Request btnSearch_Click()", "0");
            }
        }

        private void GetBaggageInsuranceChangeQueue()
        {
            try
            {
                int agencyId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                DateTime fromDate, toDate;
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                if (txtCreatedDate.Text.Trim().Length > 0)
                {
                    fromDate = Convert.ToDateTime(txtCreatedDate.Text.Trim(), dateFormat);
                }
                else
                {
                    fromDate = Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"));
                }
                if (txtToDate.Text.Trim().Length > 0)
                {
                    toDate = Convert.ToDateTime(txtToDate.Text.Trim(), dateFormat);
                }
                else
                {
                    toDate = Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"));
                }

                BaggageInsuranceQueue obj = new BaggageInsuranceQueue();
                DataTable dtQueueList = obj.GetBaggageInsuranceChangeReqQueueList(agencyId, fromDate, toDate, txtPNRno.Text, txtPolicyNo.Text, Convert.ToInt32(Settings.LoginInfo.UserID));
                Session["BIchangeReqInsQueue"] = dtQueueList;
                dlBaggageInsChangeReqQueue.DataSource = dtQueueList;
                dlBaggageInsChangeReqQueue.DataBind();
                CurrentPage = 0;
                doPaging();
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "Baggage Insurance GetBaggageInsuranceChangeQueue()", "0");
            }

        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "Baggage Insurance change Request Queue calling clear event", "0");
            }
        }
    }
}
