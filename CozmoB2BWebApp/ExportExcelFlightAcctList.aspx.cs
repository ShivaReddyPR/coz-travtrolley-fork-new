﻿using System;
using System.Data;
using System.Web.UI;
using System.IO;

public partial class ExportExcelFlightAcctListGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            dgFlightAcctReportList.DataSource = Session["FlightAcctReport"] as DataTable;
            dgFlightAcctReportList.DataBind();
            string attachment = "attachment; filename=FlightAcctReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            dgFlightAcctReportList.AllowPaging = false;
            dgFlightAcctReportList.DataSource = Session["FlightAcctReport"] as DataTable;
            dgFlightAcctReportList.DataBind();
            Form.RenderControl(htw);
            Response.Write(sw.ToString());
            Response.End();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}
