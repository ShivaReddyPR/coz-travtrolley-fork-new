﻿using System;
using System.Collections.Generic;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;

public partial class SightSeeingAjax : System.Web.UI.Page
{
    protected bool ErrorFlag = false;
   
    protected SightseeingStaticData staticInfo = new SightseeingStaticData();
    protected SightseeingSearchResult[] filteredResultList;
    protected SightseeingSearchResult selectedResult;
    protected SightSeeingReguest request = new SightSeeingReguest();
    protected Dictionary<string, string> chargeResponse=new Dictionary<string,string>();
    protected string errorMessage = string.Empty;
    string itemcode="";
    protected void Page_Load(object sender, EventArgs e)
    {

        string userIPAddress = "10.200.44.29";
        //int index = Convert.ToInt32(Request["index"]);
        if (Request["itemCode"] !=null)
        {
        itemcode=Request["itemCode"].ToString();
        }
        if (Session["searchResult"] != null)
        {
            filteredResultList = (SightseeingSearchResult[])Session["searchResult"];
            for(int i=0; i<filteredResultList.Length; i++)
            {
                if(filteredResultList[i].ItemCode == itemcode.Trim())
                {
                    selectedResult = filteredResultList[i];
                    break;
                }
            }
            if (Request["Action"].ToString() == "getSightSeeingDetail")
            {
                string cityCode = Request["cityCode"];
                string source = Request["source"];
                string itemCode = Request["itemCode"];
                string itemName = Request["itemName"];

                itemName = itemName.Replace("#", "'");
                try
                {
                    request = (SightSeeingReguest)Session["ssReq"];
                    if (source.ToUpper() == "CZA")
                    {
                        try
                        { 
                            CZActivity.Activity activity = new CZActivity.Activity();
                            activity.AgentCurrency = Settings.LoginInfo.Currency;
                            activity.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            activity.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                            activity.GetSightseeingItemInformation(itemCode, userIPAddress,  ref staticInfo);
                            //staticInfo = activity.GetCancellationPolicy(request, ref selectedResult, 0, itemCode, userIPAddress, ref staticInfo);
                            //request = (WSSightSeeingRequest)Session["ssReq"];
                            //cancelPolicyResponse = sightService.GetCancellationPolicy(request, ref selectedResult, 0, itemCode, userIPAddress, ref staticInfo);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Search, Severity.High, 0, "SightSeeing cancelpolicy method Error message :" + ex.ToString() + ex.StackTrace, "");
                            errorMessage = "Unable to Load Cancellation details, data not available.";
                            ErrorFlag = true;
                            return;
                        }
                    }
                    if (source.ToUpper() == "GTA")
                    {
                        CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();
                        gtaApi.AgentCurrency = Settings.LoginInfo.Currency;
                        gtaApi.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        gtaApi.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                        staticInfo = gtaApi.GetSightseeingItemInformation(cityCode, itemName, itemCode);
                        try
                        {                            
                            chargeResponse = gtaApi.GetSightseeingChargeCondition(request, selectedResult, 0);
                            
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Search, Severity.High, 0, "SightSeeing cancelpolicy method Error message :" + ex.ToString() + ex.StackTrace, "");
                            errorMessage = "Unable to Load Cancellation details, data not available.";
                            ErrorFlag = true;
                            return;
                        }
                    }
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.Search, Severity.High, 0, "SightSeeing Static data method Error message :" + ex.ToString() + ex.StackTrace, "");
                    errorMessage = "Unable to Load StaticData details, data not available.";
                    ErrorFlag = true;
                    return;
                }
            }
            else if (Request["Action"].ToString() == "getCancellationDetail")
            {
                 string source = Request["source"];
                 int choseTour= Convert.ToInt32(Request["choseTour"]);
                if (Session["ssReq"] != null)
                {
                    request = (SightSeeingReguest)Session["ssReq"];
                    if (source.ToUpper() == "GTA")
                    {
                        try
                        {


                            CT.BookingEngine.GDS.GTA gta = new CT.BookingEngine.GDS.GTA();
                            gta.AgentCurrency = Settings.LoginInfo.Currency;
                            gta.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            gta.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                            //Dictionary<string, string> chargeResponse = new Dictionary<string, string>();
                            chargeResponse = gta.GetSightseeingChargeCondition(request, selectedResult, choseTour);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Search, Severity.High, 0, "SightSeeing cancelpolicy method Error message :" + ex.ToString() + ex.StackTrace, "");
                            errorMessage = "Unable to Load Cancellation details, data not available.";
                            ErrorFlag = true;
                            return;
                        }
                    }
                    if (source.ToUpper() == "CZA")
                    {
                        try
                        {
                            CZActivity.Activity activity = new CZActivity.Activity();
                            activity.AgentCurrency = Settings.LoginInfo.Currency;
                            activity.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            activity.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                            activity.GetSightseeingItemInformation(selectedResult.ItemCode, userIPAddress, ref staticInfo);
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Search, Severity.High, 0, "SightSeeing cancelpolicy method Error message :" + ex.ToString() + ex.StackTrace, "");
                            errorMessage = "Unable to Load Cancellation details, data not available.";
                            ErrorFlag = true;
                            return;
                        }
                    }
                }
                else
                {
                    errorMessage = "Your Session is expired !! Please" + "<a href=\"Sightseeing.aspx\"" + "\">" + " Retry" + "</a>.";
                    ErrorFlag = true;
                    return;
                }
            }
        }
        else
        {
            errorMessage = "Your Session is expired !! Please" + "<a href=\"Sightseeing.aspx\"" + "\">" + " Retry" + "</a>.";
            ErrorFlag = true;
            return;
        }
    }
}
