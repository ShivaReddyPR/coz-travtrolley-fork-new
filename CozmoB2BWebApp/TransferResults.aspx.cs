﻿using CT.BookingEngine;
using CT.CMS;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class TransferResults : CT.Core.ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.AgentId);
                hdnBehalfLocation.Value = JsonConvert.SerializeObject(0);
                //LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.LocationID);
                //request.LoginCountryCode = locationMaster.CountryCode != null ? locationMaster.CountryCode : "";
            }
            else
            {
                hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentID);
                hdnBehalfLocation.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentLocation);
                //LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                //request.LoginCountryCode = locationMaster.CountryCode != null ? locationMaster.CountryCode : "";
            }
            hdnUserId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.UserID);
            hdnAgentType.Value = JsonConvert.SerializeObject(Settings.LoginInfo.AgentType);

            //if (!string.IsNullOrEmpty(request.Corptravelreason))
            //{
            //    DataSet ds = new DataSet();
            //    ds.Tables.Add(TravelUtility.GetTravelReasonList("S", Settings.LoginInfo.AgentId, ListStatus.Short));
            //    ds.Tables.Add(TravelUtility.GetProfileList(Settings.LoginInfo.CorporateProfileId, Settings.LoginInfo.AgentId, ListStatus.Short));
            //    hdnCorpInfo.Value = JsonConvert.SerializeObject(ds);
            //}

        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object TransferSources()
        {
            DataTable Sources=new DataTable();
            try
            {
                Sources = AgentMaster.GetAgentSources(Settings.LoginInfo.AgentId, 9, false);
                return JsonConvert.SerializeObject(Sources);
            }
            catch(Exception e)
            {
                Audit.Add(EventType.HotelSearch, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get TransferSources. Error: " +e.Message,"");
                return JsonConvert.SerializeObject(Sources);
            }
            
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static AgentMaster OnBehalfAgentDetails(int agentId)
        {
            AgentMaster clsAM = new AgentMaster(agentId);
            return clsAM;
        }
    }
    
}
