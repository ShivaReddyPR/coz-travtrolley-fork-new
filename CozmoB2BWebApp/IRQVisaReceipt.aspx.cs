﻿using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Text;
using System.Web.UI.WebControls;

public partial class IRQVisaReceipt : CT.Core.ParentPage
{
    protected string currency = string.Empty;
    Dictionary<string, string> lstVisaPrice = new Dictionary<string, string>();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            lblErrorMessage.Text = string.Empty;
          //  lblErrorMessage.Style.Add("display","none");
            if (!IsPostBack)
            {
                BindVisaTypes();
                
            }
            else {
                //    //1.Redirected to AjaxValidation.aspx
                //    //2.From Ajax Validation redirected here and we will verify the query strings and then documents saving process starts here
                //    if (Request.QueryString.Count > 0 && Request.QueryString["Key"] == "Submit") //This is the condition which is used to save the uploaded documennts 
                //    {
                //Save();
                //    }
               
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "");
        }
    }

    private void BindVisaTypes()
    {
        DataTable visaMaster = IRQApplicantDetails.GetVisaTypes();
        if (visaMaster != null && visaMaster.Rows != null && visaMaster.Rows.Count > 0)
        {
            currency = visaMaster.Rows[0]["Currency"].ToString();
            ddlVisaType.DataSource = visaMaster;
            ddlVisaType.DataValueField = "VisaTypeIDPrice";
            ddlVisaType.DataTextField = "VisaType";
            ddlVisaType.DataBind();
            ddlVisaType.Items.Insert(0, new ListItem("Select", "-1"));
            
        }
    }



    protected void btnSave_Click(object sender, EventArgs e)
    {
        Save();
    }
    protected void Save()
    {
        try
        {
            int totalPaxCount = (Convert.ToInt32(hdnAdultCount.Value) + Convert.ToInt32(hdnChildCount.Value) + Convert.ToInt32(hdnInfantCount.Value));
            int totalPaxCounter = (Convert.ToInt32(hdnAdultCounter.Value) + Convert.ToInt32(hdnChildCounter.Value) + Convert.ToInt32(hdnInfantCounter.Value));

            if (totalPaxCount == totalPaxCounter)
            {
                IRQHeader headerDet = new IRQHeader();
                headerDet.AdultCount = Convert.ToInt32(hdnAdultCounter.Value);
                headerDet.AgentId = Convert.ToInt32(Settings.LoginInfo.AgentId);
                headerDet.ChildCount = Convert.ToInt32(hdnChildCounter.Value);
                headerDet.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                headerDet.InfantCount = Convert.ToInt32(hdnInfantCounter.Value);
                headerDet.LocationId = Convert.ToInt32(Settings.LoginInfo.LocationID);
               headerDet.Price = Convert.ToDecimal(Request["lblPrice"].ToString().Split(' ')[1]);
              //if(!string.IsNullOrEmpty(hdnVisaPrice.Value) && hdnVisaPrice.Value.Length >0)
              //  {
              //      headerDet.Price = Convert.ToDecimal(hdnVisaPrice.Value.Split(' ')[1]);
              //  }

                string[] priceArray = ddlVisaType.SelectedValue.Split('~');
                headerDet.VisaTypeId = Convert.ToInt32(priceArray[0]);
                List<IRQApplicantDetails> lstApplicantDet = new List<IRQApplicantDetails>();
                List<string> uploadFileNames = new List<string>();
                List<string> filenames = new List<string>();
                
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                for (int i = 1; i <= Convert.ToInt32(hdnAdultCounter.Value); i++)
                {
                    filenames = uploadImageFiles(i, "Adt");
                    uploadFileNames.AddRange(filenames);
                    IRQApplicantDetails applicantDet = new IRQApplicantDetails();
                    applicantDet.ApplicantName = Convert.ToString(Request["txtAdtFullName_" + i]);
                    applicantDet.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                    applicantDet.DateOfBirth = Convert.ToDateTime(Request["ddlAdtdobDay_" + i] + "/" + Request["ddlAdtdobMonth_" + i] + "/" + Request["ddlAdtdobYear_" + i], dateFormat);
                    // applicantDet.DateOfBirth = Convert.ToDateTime(Request["txtAdtDOB_" + i], dateFormat);
                    if (filenames.Count > 0)
                    {
                        applicantDet.Document1Path = Convert.ToString(filenames[0]);
                    }
                    else
                    {
                        throw new Exception("File Uploading Failed");
                    }
                    if (filenames.Count > 1)
                    {
                        applicantDet.Document2Path = Convert.ToString(filenames[1]);
                    }
                    else
                    {
                        throw new Exception("File Uploading Failed");
                    }
                    if (filenames.Count > 2)
                    {
                        applicantDet.Document3Path = Convert.ToString(filenames[2]);
                    }
                    else
                    {
                        throw new Exception("File Uploading Failed");
                    }
                    if (filenames.Count > 3)
                    {
                        applicantDet.Document4Path = Convert.ToString(filenames[3]);
                    }
                    else
                    {
                        applicantDet.Document4Path = Convert.ToString("N/A");
                    }
                    if (filenames.Count > 4)
                    {
                        applicantDet.Document5Path = Convert.ToString(filenames[4]);
                    }
                    else
                    {
                        applicantDet.Document5Path = Convert.ToString("N/A");
                    }
                    applicantDet.Nationality = Convert.ToString(Request["ddlAdtNationality_" + i]);
                    //if (!string.IsNullOrEmpty(hdnNationality.Value) && hdnNationality.Value.Length > 0)
                    //{
                    //    applicantDet.Nationality = hdnNationality.Value.Split('~')[i-1];
                    //}
                    //applicantDet.PassportExpiry = Convert.ToDateTime(Request["txtAdtPassExpiry_" + i], dateFormat);
                    applicantDet.PassportExpiry = Convert.ToDateTime(Request["ddlAdtPassExpiryDay_" + i] + "/" + Request["ddlAdtPassExpiryMonth_" + i] + "/" + Request["ddlAdtPassExpiryYear_" + i], dateFormat);
                    applicantDet.PassportNo = Convert.ToString(Request["txtAdtPassport_" + i]);
                    applicantDet.PaxType = "Adult";
                    applicantDet.VisaStatus = "I";
                    lstApplicantDet.Add(applicantDet);
                }
                for (int i = 1; i <= Convert.ToInt32(hdnChildCounter.Value); i++)
                {
                    filenames = uploadImageFiles(i, "Chd");
                    uploadFileNames.AddRange(filenames);
                    IRQApplicantDetails applicantDet = new IRQApplicantDetails();
                    applicantDet.ApplicantName = Convert.ToString(Request["txtChdFullName_" + i]);
                    applicantDet.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                    // applicantDet.DateOfBirth = Convert.ToDateTime(Request["txtChdDOB_" + i], dateFormat);
                    applicantDet.DateOfBirth = Convert.ToDateTime(Request["ddlChddobDay_" + i] + "/" + Request["ddlChddobMonth_" + i] + "/" + Request["ddlChddobYear_" + i], dateFormat);

                    if (filenames.Count > 0)
                    {
                        applicantDet.Document1Path = Convert.ToString(filenames[0]);
                    }
                    else
                    {
                        throw new Exception("File Uploading Failed");
                    }
                    if (filenames.Count > 1)
                    {
                        applicantDet.Document2Path = Convert.ToString(filenames[1]);
                    }
                    else
                    {
                        throw new Exception("File Uploading Failed");
                    }
                    if (filenames.Count > 2)
                    {
                        applicantDet.Document3Path = Convert.ToString(filenames[2]);
                    }
                    else
                    {
                        throw new Exception("File Uploading Failed");
                    }
                    if (filenames.Count > 3)
                    {
                        applicantDet.Document4Path = Convert.ToString(filenames[3]);
                    }
                    else
                    {
                        applicantDet.Document4Path = Convert.ToString("N/A");
                    }
                    if (filenames.Count > 4)
                    {
                        applicantDet.Document5Path = Convert.ToString(filenames[4]);
                    }
                    else
                    {
                        applicantDet.Document5Path = Convert.ToString("N/A");
                    }
                    //if (!string.IsNullOrEmpty(hdnChdNationality.Value) && hdnChdNationality.Value.Length > 0)
                    //{
                    //    applicantDet.Nationality = hdnChdNationality.Value.Split('~')[i - 1];
                    //}
                    applicantDet.Nationality = Convert.ToString(Request["ddlChdNationality_" + i]);
                    //applicantDet.PassportExpiry = Convert.ToDateTime(Request["txtChdPassExpiry_" + i], dateFormat);
                    applicantDet.PassportExpiry = Convert.ToDateTime(Request["ddlChdPassExpiryDay_" + i] + "/" + Request["ddlChdPassExpiryMonth_" + i] + "/" + Request["ddlChdPassExpiryYear_" + i], dateFormat);
                    applicantDet.PassportNo = Convert.ToString(Request["txtChdPassport_" + i]);
                    applicantDet.PaxType = "Child";
                    applicantDet.VisaStatus = "I";
                    
                    lstApplicantDet.Add(applicantDet);
                }
                for (int i = 1; i <= Convert.ToInt32(hdnInfantCounter.Value); i++)
                {
                    filenames = uploadImageFiles(i, "Inf");
                    uploadFileNames.AddRange(filenames);
                    IRQApplicantDetails applicantDet = new IRQApplicantDetails();
                    applicantDet.ApplicantName = Convert.ToString(Request["txtInfFullName_" + i]);
                    applicantDet.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                    //applicantDet.DateOfBirth = Convert.ToDateTime(Request["txtInfDOB_" + i], dateFormat);
                    applicantDet.DateOfBirth = Convert.ToDateTime(Request["ddlInfdobDay_" + i] + "/" + Request["ddlInfdobMonth_" + i] + "/" + Request["ddlInfdobYear_" + i], dateFormat);
                    if (filenames.Count > 0)
                    {
                        applicantDet.Document1Path = Convert.ToString(filenames[0]);
                    }
                    else
                    {
                        throw new Exception("File Uploading Failed");
                    }
                    if (filenames.Count > 1)
                    {
                        applicantDet.Document2Path = Convert.ToString(filenames[1]);
                    }
                    else
                    {
                        throw new Exception("File Uploading Failed");
                    }
                    if (filenames.Count > 2)
                    {
                        applicantDet.Document3Path = Convert.ToString(filenames[2]);
                    }
                    else
                    {
                        throw new Exception("File Uploading Failed");
                    }
                    if (filenames.Count > 3)
                    {
                        applicantDet.Document4Path = Convert.ToString(filenames[3]);
                    }
                    else
                    {
                        applicantDet.Document4Path = Convert.ToString("N/A");
                    }
                    if (filenames.Count > 4)
                    {
                        applicantDet.Document5Path = Convert.ToString(filenames[4]);
                    }
                    else
                    {
                        applicantDet.Document5Path = Convert.ToString("N/A");
                    }
                    applicantDet.Nationality = Convert.ToString(Request["ddlInfNationality_" + i]);
                    //if (!string.IsNullOrEmpty(hdnInfNationality.Value) && hdnInfNationality.Value.Length > 0)
                    //{
                    //    applicantDet.Nationality = hdnInfNationality.Value.Split('~')[i - 1];
                    //}

                     applicantDet.PassportExpiry = Convert.ToDateTime(Request["txtInfPassExpiry_" + i], dateFormat);
                    applicantDet.PassportExpiry = Convert.ToDateTime(Request["ddlInfPassExpiryDay_" + i] + "/" + Request["ddlInfPassExpiryMonth_" + i] + "/" + Request["ddlInfPassExpiryYear_" + i], dateFormat);
                    applicantDet.PassportNo = Convert.ToString(Request["txtInfPassport_" + i]);
                    applicantDet.PaxType = "Infant";
                    applicantDet.VisaStatus = "I";
                    
                    lstApplicantDet.Add(applicantDet);
                }

                if (lstApplicantDet != null && lstApplicantDet.Count > 0 && headerDet != null && headerDet.VisaTypeId > 0)
                {
                    IRQApplicantDetails.Save(lstApplicantDet, headerDet);

                    //

                    try
                    {
                        int counter = 0;
                        string pax = string.Empty;
                        //int paxInt = 1;

                        for (int i = 0; i < lstApplicantDet.Count; i++)
                        {
                            IRQApplicantDetails appDetails = lstApplicantDet[i];
                            //Taking the paxType and counter from saving image name
                            string appDoc = appDetails.Document1Path.Substring(0, 4);
                            
                            List<string> paxFileNames = uploadFileNames.FindAll(delegate (string f) { return f.Contains(appDoc); });
                            for (int j = 0; j < paxFileNames.Count; j++)
                            {
                                RenameImage(paxFileNames[j], headerDet.AgentId, headerDet.VisaId, appDetails.PassportNo, Convert.ToInt32(paxFileNames[j].Split('_')[1]));
                            }
                        }

                        //for (int i = 0; i < uploadFileNames.Count; i++)
                        //{
                        //    string filename = uploadFileNames[i];
                        //    //if (i == 0)
                        //    //{
                        //    //    pax = Convert.ToString(filename.Substring(0, 3));
                        //    //    paxInt = Convert.ToInt32(filename.Substring(3, 1));
                        //    //}
                        //    //else 
                        //    if ((pax == Convert.ToString(filename.Substring(0, 3)) && paxInt != Convert.ToInt32(filename.Substring(3, 1))) || (pax != Convert.ToString(filename.Substring(0, 3)) && paxInt == Convert.ToInt32(filename.Substring(3, 1))) || (pax != Convert.ToString(filename.Substring(0, 3)) && paxInt != Convert.ToInt32(filename.Substring(3, 1))))
                        //    {
                        //        counter++;
                        //    }
                        //    pax = Convert.ToString(filename.Substring(0, 3));
                        //    paxInt = Convert.ToInt32(filename.Substring(3, 1));
                        //    RenameImage(filename, headerDet.AgentId, headerDet.VisaId, lstApplicantDet[counter].PassportNo, Convert.ToInt32(filename.Split('_')[1]));

                        //}
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(IRQ Visa Receipt)Failed to rename uploaded documents. Reason : " + ex.ToString(), Request["REMOTE_ADDR"]);
                    }
                    lblErrorMessage.Visible = true;
                    lblErrorMessage.Text = "All Applicant Details Saved Successfully";

                    try
                    {
                        //Sending the Email
                        string fromEmail = ConfigurationManager.AppSettings["fromEmail"];
                        string replyTo = ConfigurationManager.AppSettings["replyTo"];
                        List<string> toArray = new List<string>();
                        string[] visaMails = Convert.ToString(ConfigurationManager.AppSettings["IRQ_Email"]).Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                        foreach (string cnMail in visaMails)
                        {
                            toArray.Add(cnMail);
                        }

                        string subject = "Visa Applicant Details";
                        string message = string.Empty;

                        emailHeaderDet.Visible = true;
                        emailApplicantDet.Visible = true;
                        string headerDetails = emailHeaderDet.InnerText;
                        string applicantDetails = emailApplicantDet.InnerText;
                        StringBuilder emailContent = new StringBuilder();

                        emailHeaderDet.Visible = false;
                        emailApplicantDet.Visible = false;

                        System.Collections.Hashtable hashTable = new System.Collections.Hashtable();

                        if (!string.IsNullOrEmpty(headerDetails) && !string.IsNullOrEmpty(applicantDetails))
                        {
                            hashTable.Add("VisaType", ddlVisaType.SelectedItem.Text);
                            hashTable.Add("AdultCount", headerDet.AdultCount.ToString());
                            hashTable.Add("ChildCount", headerDet.ChildCount.ToString());
                            hashTable.Add("InfantCount", headerDet.InfantCount.ToString());

                            decimal totalCost = Convert.ToDecimal(headerDet.Price);
                            hashTable.Add("VisaPrice", totalCost.ToString() + " " + currency);
                            message = headerDetails;

                            for (int t = 0; t < totalPaxCounter; t++)
                            {
                                emailContent.Append("<strong>PaxType :</strong> " + lstApplicantDet[t].PaxType + ",<br />");
                                emailContent.Append("<strong> ApplicantName :</strong> " + lstApplicantDet[t].ApplicantName + ", <br />");
                                emailContent.Append("<strong> DateOfBirth :</strong> " + lstApplicantDet[t].DateOfBirth.ToString("dd/MM/yyyy") + ", <br />");
                                emailContent.Append("<strong> PassportNumber :</strong> " + lstApplicantDet[t].PassportNo + ", <br />");
                                emailContent.Append("<strong> Passport ExpiryDate:</strong>" + lstApplicantDet[t].PassportExpiry.ToString("dd/MM/yyyy") + ", <br />");
                                emailContent.Append("<strong> Nationality :</strong>" + lstApplicantDet[t].Nationality.ToString() + ", <br /><br />");
                            }
                            message += emailContent;
                            CT.Core.Email.Send(fromEmail, replyTo, toArray, subject, message, hashTable);
                        }
                        ddlVisaType.SelectedIndex = 0;
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "(IRQ Visa Receipt)Failed to send Confirmation Email. Reason : " + ex.ToString(), Request["REMOTE_ADDR"]);
                    }
                }
            }
            else
            {
                lblErrorMessage.Visible = true;
                lblErrorMessage.Text = "Please fill all the Applicant Details";
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            lblErrorMessage.Visible = true;
            lblErrorMessage.Text = ex.Message;
        }
    }

    protected List<string> uploadImageFiles(int id,string paxType)
    {
        List<string> lstUploadImages = new List<string>();
        try
        {  
            string folderName = System.Configuration.ConfigurationManager.AppSettings["IRQVisaImageSavePath"];           

            string savingFilename = paxType + id + "PassportFront_1_" + Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss:fff")).Replace(":", "").Replace(" ", "_").Replace("-", "") + ".jpg";
            folderName = Server.MapPath(folderName + Settings.LoginInfo.AgentId + "\\");
            if (!Directory.Exists(folderName))
            {
                Directory.CreateDirectory(folderName);
            }

            byte[] bytes = Convert.FromBase64String(Request["hdnPP" + paxType + "Front_" + id]);
            FileStream fs = new FileStream(folderName + savingFilename, FileMode.CreateNew);
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();            
            lstUploadImages.Add(savingFilename);

            savingFilename = paxType + id + "PassportBack_2_" + Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss:fff")).Replace(":", "").Replace(" ", "_").Replace("-", "") + ".jpg";
            bytes = Convert.FromBase64String(Request["hdnPP" + paxType + "Back_" + id]);
            fs = new FileStream(folderName + savingFilename, FileMode.CreateNew);
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();
            
            lstUploadImages.Add(savingFilename);

            savingFilename = paxType + id + "Photo_3_" + Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss:fff")).Replace(":", "").Replace(" ", "_").Replace("-", "") + ".jpg";
            bytes = Convert.FromBase64String(Request["hdnPP" + paxType + "Photo_" + id]);
            fs = new FileStream(folderName + savingFilename, FileMode.CreateNew);
            fs.Write(bytes, 0, bytes.Length);
            fs.Close();
            
            lstUploadImages.Add(savingFilename);

            if (!string.IsNullOrEmpty(Request["hdnPP" + paxType + "Birth" + id]))
            {
                savingFilename = paxType + id + "BirthCert_4_" + Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss:fff")).Replace(":", "").Replace(" ", "_").Replace("-", "") + ".jpg";
                bytes = Convert.FromBase64String(Request["hdnPP" + paxType + "Birth_" + id]);
                fs = new FileStream(folderName + savingFilename, FileMode.CreateNew);
                fs.Write(bytes, 0, bytes.Length);
                fs.Close();

                lstUploadImages.Add(savingFilename);
            }
            
            
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "(IRQ Visa Receipt)Exception while uploading images. Reason : " + ex.ToString(), Request["REMOTE_ADDR"]);
            throw new Exception(ex.Message);
        }
        return lstUploadImages;
    }

    //protected void RenameImage(string packageId, string imagePath, string slNo)
    private void RenameImage(string oldImageName, int agentId, int visaId, string passportNo, int imageId)
    {
        try
        {
            string folderName = System.Configuration.ConfigurationManager.AppSettings["IRQVisaImageSavePath"];
            folderName = Server.MapPath(folderName + Settings.LoginInfo.AgentId + "\\");

            if (!string.IsNullOrEmpty(folderName) && !string.IsNullOrEmpty(oldImageName))
            {
                string oldFile = Path.GetFileName(folderName + oldImageName);
                string fileExtension = Path.GetExtension(oldImageName);
                string dFilePath = string.Empty;
                string newfile = agentId + "_" + visaId + "_" + passportNo + "_" + imageId + fileExtension;
                if (newfile != oldFile)
                {
                    if (System.IO.File.Exists(folderName + newfile)) System.IO.File.Delete(folderName + newfile);
                    System.IO.File.Copy(folderName + oldFile, folderName + newfile);
                    System.IO.File.Delete(folderName + oldFile);
                }
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(IRQ Visa Receipt)Failed to rename uploaded document. Reason : " + ex.ToString(), Request["REMOTE_ADDR"]);
        }

    }


  
}
