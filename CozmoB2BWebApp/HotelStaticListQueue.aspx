﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" CodeBehind="HotelStaticListQueue.aspx.cs" Inherits="CozmoB2BWebApp.HotelStaticListQueue" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
 <link href="build/css/jquery-ui.css" rel="stylesheet" type="text/css" />
 <script src="scripts/paginathing.js" type="text/javascript"></script>
  <script src="scripts/select2.min.js"></script>
  <%--<script type="text/javascript" src="Scripts/jsBE/Search.js"></script>--%>
   <script src="scripts/jquery-ui.js"></script>
   <script src="scripts/jsBE/Search.js"></script>
<%--<link href="scripts/jquery-ui.css" rel="stylesheet" />--%>

 <script type="text/javascript">
     var Recordsperpage =20; Pagenodisplay = 100, TotalRecords = 0;
        var SearchList;
        $(document).ready(function () {
            BindCountry();
        });

          function AjaxCall(Ajaxurl, Inputdata) {

    var obj = '';

    $.ajax({
        type: "POST",
        url: Ajaxurl,
        contentType: "application/json; charset=utf-8",
        data: Inputdata,// "{'sOrigin':'" + origin + "','sDestination':'" + destination + "','sPaxnames':'" + PaxNames.toString() + "'}",
        dataType: "json",
        async: false,
        success: function (data) {
            obj = (data == null || data.d == null || data.d == 'undefined' || data.d == '') ? '' : data.d;
        },
        error: function (error) {
            console.log(JSON.stringify(error));
        }
    });

    return obj;
        }

       function BindCountry() {
        var options = "";
        options += "<option value='-1'>Select Country</option>";

        var data = AjaxCall('HotelStaticListQueue.aspx/BindCountry','');

        $.each(data, function (item) {
            options += "<option value='" + this['Value'] + "'>" + this['Text'] + "</option>";
        });

        $('#ddlCountry').empty();
        $('#ddlCountry').append(options);
        $('#ddlCountry').select2('val', '-1');
     }

     /* To show and hide filter options on the screen */
    function ShowHide(divParam) {

        var display = document.getElementById(divParam).style.display;
        document.getElementById(divParam).style.display = display == 'block' ? 'none' : 'block';
        document.getElementById('ancParam').innerHTML = display == 'block' ? 'Show Param' : 'Hide Param';
        return false;
     }

        function SearchData() {
            var isValid = true;
         var code=$('#ddlCountry').val();
        var rating=$('#ddlRating').val();
            if (code == -1) {
                document.getElementById('Country').style.display = 'block';
                document.getElementById('Country').innerHTML = "Please Select Country";
                return false;
            }
            else {
                document.getElementById('Country').style.display = 'none';
            }
            var CityName = $('#txtCItyName').val();
            if (CityName == "") {
                document.getElementById('City').style.display = 'block';
                document.getElementById('City').innerHTML = "Please Enter City Name";
                return false;
            }
            else {
                document.getElementById('City').style.display = 'none';
            }
            var obj = {};
              $("#ctl00_upProgress").show();
            obj.Country = $('#ddlCountry').val();
            obj.HotelId = $('#txtHotelId').val();
            obj.HotelName = $('#txtHotelName').val();
            obj.CityName = $('#txtCItyName').val();
            obj.Rating = $('#ddlRating').val();
            obj.Phone = $('#txtPhone').val();
            obj.Email = $('#txtEmail').val();
            obj.Website = $('#txtWebsite').val();

        //obj.productId = $('#ddlProduct').val();

        SearchList = JSON.parse(AjaxCall('HotelStaticListQueue.aspx/Search', JSON.stringify(obj)));

        TotalRecords = $('#ddlProduct').val() == "" ? SearchList.Table.length : SearchList.length;                    
        $('.panel-footer').remove();

        if (TotalRecords > 0) {

            $('#NoRecords').hide();
            $('#list-group').paginathing({
                limitPagination: (TotalRecords / Recordsperpage) > Pagenodisplay ? Pagenodisplay : Math.ceil(TotalRecords / Recordsperpage),
                containerClass: 'panel-footer',
                pageNumbers: true,
                totalRecords: TotalRecords
            });                      
            LoadData(1);
            ShowHide('divParam');
              $("#ctl00_upProgress").hide();
        }
        else {
              $("#ctl00_upProgress").hide();
            $('#list-group').children().remove();
            $('#NoRecords').show();
        }

            $("#ctl00_upProgress").hide();
            if (isValid == true) {
            return true;
        }
            else {
                  $("#ctl00_upProgress").hide();
            return false;
        }
     }

      function LoadData(page) {
        debugger;
        $('#list-group').children().remove();            
        var showFrom = ((Math.ceil(page) - 1) * Recordsperpage);
        var showTo = Math.ceil(showFrom) + (Math.ceil(Recordsperpage) - 1);
          showTo = showTo > TotalRecords ? (Math.ceil(TotalRecords) - 1) : showTo;
          BindStaticData(showFrom, showTo);
     }
     function BindStaticData(showFrom, showTo) {
        var orgqueueTemplate = $('#divStaticData').html();

        for (var i = Math.ceil(showFrom); i <= Math.ceil(showTo); i++) {

            $('#list-group').append('<li id="List' + i + '">' + orgqueueTemplate + '</li>');
            AssignData('HSDCountry', i, SearchList[i].countryName);
            AssignData('HSDHId', i, SearchList[i].TravolutionaryID);
            AssignData('HSDHName', i, SearchList[i].DisplayName);
            AssignData('HSDCityName', i, SearchList[i].CityName);
            AssignData('HSDRating', i, SearchList[i].StarRating);
            AssignData('HSDPhone', i, SearchList[i].Phone);
            AssignData('HSDEmail', i, SearchList[i].Email);
            AssignData('HSDWebSite', i, SearchList[i].WebSite);
            
            $('#Edit').attr('id', 'Edit-' + i);
            $('#Edit-' + i).append('<a id="Open-' + i + '" class="btn but_b pull-right" href="HotelStaticDataMaster.aspx?ref=' + SearchList[i].HotelstaticID + '">Edit</a>');
            $('#Delete').attr('id', 'Delete-' + i);
            $('#Delete-' + i).append('<a id="Open-' + i + '" class="btn but_b pull-right" href="javascript:UpdateStatus(' + SearchList[i].HotelstaticID + ')">Delete</a>');
        }
     }
         function UpdateStatus(Hid) {
             var txt;
        var r = confirm("Which you want to delete the record");
        if (r == true) {
        var Ajaxurl = 'HotelStaticListQueue.aspx/GetUpdateStatus';
             var Inputdata = "{'id':'" + Hid + "'}"

             $.ajax({
                type: "POST",
                url: Ajaxurl,
                contentType: "application/json; charset=utf-8",
                data: Inputdata,
                dataType: "json",
                 async: false,
                 success: function (data) {   
                    var message = data.d;
                    if (message.length > 0) {
                        alert("Sucessfully Deleted.");
                    }
                    else {
                        //$('#CodeText').html('');
                    }
                 },
                 
                error: (error) => {
                    console.log(JSON.stringify(error));
                }
        });
        }
        else {
        txt = "You pressed Cancel!";
        } 
     }
     /* To show grid selected page as active and load the page data */
    function showselpage(event, pageno) {
        event.parentNode.className= 'page active';
        LoadData(pageno);
        BindHeaderPagination();
    }

    /* To bind pagination tab on top of the grid */
    function BindHeaderPagination() {

        $('#HeaderPagination').children().remove();
        $('#HeaderPagination').append($('.panel-footer')[0] != null ? $('.panel-footer')[0].innerHTML : '');
        var nodes = $('#HeaderPagination')[0].childNodes[1].childNodes;
        $.each(nodes, function (key, node) {
            if (node.childNodes[0].attributes.length > 0)
                node.childNodes[0].attributes[0].nodeValue = 'Hdrpagingclick(this,' + key + ')';
        });
    }

    function Hdrpagingclick(event, id) {
        var linodes = $('.panel-footer')[0].childNodes[0].childNodes;
        var clickfn = '';
        $.each(linodes, function (nodid, linode) {
            linode.className= 'page';
            if (nodid == id)
                clickfn = linode.childNodes[0];
        });
        clickfn.onclick();
    }
     /* Common function to set dynaic id's and data to grid variables */
    function AssignData(Mainid, childid, data) {
        $('#' + Mainid).attr('id', Mainid + '-' + childid);
        $('#' + Mainid + '-' + childid).text(data);
    }
    </script>
    <a style="cursor: default; font-weight: bold; font-size: 8pt; color: Black;" id="ancParam" onclick="return ShowHide('divParam');">Hide Param</a>
     <%--Filter optoins HTML --%>
<div title="Param" id="divParam" style="display:block">
     <div class="paramcon">
         <div class="col-md-12 padding-0 marbot_10">
            <div class="col-md-2"> Country: <span class="red_span">*</span> </div> 
            <div class="col-md-2"> <select class="form-control" id="ddlCountry"></select><span class="red_span" style="display: none" id="Country"></span></div>
            <div class="col-md-2"> Hotel Id: </div>
            <div class="col-md-2"> <table> <tr> <td> <input type="text" id="txtHotelId" class="inputEnabled form-control" /> </td> </tr> </table> </div>
             <div class="col-md-2">Hotel Name:</div>
             <div class="col-md-2"><table><tr><td> <input type="tel" id="txtHotelName" class="inputEnabled form-control" /></td></tr></table></div>
         </div>
         <div class="col-md-12 padding-0 marbot_10">
             <div class="col-md-2">City Name:</div>
             <div class="col-md-2"><table><tr><td><input type="text" id="txtCItyName" class="inputEnabled form-control" /><span class="red_span" style="display: none" id="City"></span></td></tr></table></div>
             <div class="col-md-2">Rating:<span class="red_span">*</span></div>
             <div class="col-md-2"><select class="form-control" id="ddlRating">
             <option value="0">Select Rating</option>
             <option value="1">1</option>
             <option value="2">2</option>
             <option value="3">3</option>
             <option value="4">4</option>
             <option value="5">5</option>
             </select><span class="red_span" style="display: none" id="Rating"></span></div>
             <div class="col-md-2">Phone Number:</div>
             <div class="col-md-2"><table><tr><td><input type="text" id="txtPhone" class="inputEnabled form-control" /></td></tr></table></div>
         </div>
         <div class="col-md-12 padding-0 marbot_10">
             <div class="col-md-2">Email:</div>
             <div class="col-md-2"><table><tr><td><input type="text" id="txtEmail" class="inputEnabled form-control" /></td></tr></table></div>
             <div class="col-md-2">WebSite:</div>
             <div class="col-md-2"><table><tr><td><input type="text" id="txtWebsite" class="inputEnabled form-control" /></td></tr></table></div>
             <div class="col-md-2" style="display:none"> Product : </div>
            <div class="col-md-2" style="display:none"> <select class="form-control" id="ddlProduct"> <option value="1">Flight</option> <option value="2">Hotel</option> </select> </div>
             <div class="clearfix"></div>
         </div>
          <div class="col-md-12 padding-0 marright_10"> <input type="button" id="btnSearch" class="btn but_b pull-right" value="Search" onclick="SearchData(); return false;" /></div>
         </div>
    </div>
<div> <label id="NoRecords"><b>No Records Found!</b></label> </div>
<div id="HeaderPagination" style="float:right" class="pagmargin"> </div>
<div class="row"> <div class="col-md-12"> <ul id="list-group"> </ul> </div> </div>
<%--Sim Results View HTML--%>
<div id="divStaticData" style="display: none;">    
    <div class="tbl queue-design-hotel">                                
        <div class="row">
            <div class="col-md-12 col-sm-11 col-10"> 
                <div class="row">
                    <div class="col-md-3"> <label> <small class="text-muted">Country</small> <label class="font-weight-bold" id="HSDCountry"></label> </label> </div>
                    <div class="col-md-3"> Hotel Id: <label class="font-weight-bold" id="HSDHId"></label> </div> 
                     <div class="col-md-3">Hotel Name: <label class="font-weight-bold" id="HSDHName"></label>  </div>
                    <div class="col-md-3"> City Name : <label class="font-weight-bold" id="HSDCityName"></label>  </div>
                </div>
                <div class="row mt-2"> 
                    <div class="col-md-3"> Rating : <label class="font-weight-bold" id="HSDRating"></label> </div> 
                    <div class="col-md-3"> Phone:<label class="font-weight-bold" id="HSDPhone"></label>  </div>
                    <div class="col-md-3">Email:  <label class="font-weight-bold" id="HSDEmail"></label>  </div> 
                    <div class="col-md-3"> WebSite: <label class="font-weight-bold" id="HSDWebSite"></label> </div>
                </div>
                 </div>
            <div class="col-md-12 col-sm-11 col-10 ">
            <div class="row pull-right">
      <div class="col-md-3"> <div class="col-md-3"> <div class="float-lg-right" id="Edit">  </div> </div></div>
     <div class="col-md-3"> <div class="col-md-3"> <div class="float-lg-right" id="Delete">  </div> </div> </div>
   <%-- <div class="row mt-4"> <div class="col-md-12"><div id="CodeText" class="float-lg-right" style="overflow: auto; padding-top: 20px;text-align: center;display:none"></div></div> </div>--%>
    </div>
                </div>
        </div>
        
    </div>

</div>
     <style> 

    /*firoz 04 july 2019*/
    .queue-design img  { height:40px;  }
    .queue-design label { margin:0px;  }
    .queue-design .departure,.queue-design .arrival,.queue-design .booking-da  { font-size:16px;   }
    .queue-design .departure,.queue-design .arrival,.queue-design .destination  { width:21%;     }
    .queue-design .aeroplane-icon{ filter: invert(48%) sepia(79%) saturate(0%) hue-rotate(86deg) brightness(118%) contrast(119%);  }
    .queue-design-hotel .bed-icon{ filter: invert(48%) sepia(79%) saturate(0%) hue-rotate(86deg) brightness(118%) contrast(119%);  }
    .queue-design .text-small  { font-size:85%; }
    @media only screen and (max-width: 600px) { .queue-design .departure,.queue-design .arrival,.queue-design .destination  { float:left;    } }

    .pagmargin {

        padding: 10px 15px;
        background-color: #f5f5f5;
        border-top: 1px solid #ddd;
        border-bottom-right-radius: 3px;
        border-bottom-left-radius: 3px;
    }
 
</style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="server">
</asp:Content>
