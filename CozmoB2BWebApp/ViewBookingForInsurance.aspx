﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="ViewBookingForInsuranceGUI" Title="Insurance Details" Codebehind="ViewBookingForInsurance.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script language="javascript">


        function ShowVoucher() {


            var url = 'PrintInsuranceVoucher?InsId=<%=insId %>';
        //        var wnd = window.open(url, 'Voucher', 'width:600lpx;height:600px;');
        var wnd = window.open(url, 'Voucher', 'width:600,height:600,toolbar = yes, location = no, directories = no, status = yes, menubar = no, scrollbars = yes, resizable = yes, left = 50, top = 50');
        
    
        }

</script>

<%--<form id="form1" runat="server">--%> 
<div class="body_container" id="Div1"  runat="server">

<div class="ns-h3"> Insurance Header 


<label class=" pull-right marright_10"> <a class="fcol_fff" href="#" onclick="ShowVoucher()">Show Voucher</a> </label>
    
  <label class=" pull-right marright_10 hidden-xs">   <a href="InsuranceQueue.aspx" class="fcol_fff" >  Insurance Queue</a> </label>
  
  <div class="clearfix"></div>
  </div>   
<div class=" bor_gray bg_white padding-5 marbot_10">
   
   
   

   
   
     <%if (InsuranceHdr != null)
      { %>

<div class="col-md-12 padding-0 marbot_10"> 



<div class="col-md-2 col-xs-6"><label>Departure City: </label> </div>
<div class="col-md-2 col-xs-6"> <b><%= DepartureCity%> </b></div>

<div class="col-md-2 col-xs-12"><label>ArrivalCity: </label> </div>
<div class="col-md-2 col-xs-12"> <b><%= ArrivalCity%> </b></div>

<div class="col-md-2 col-xs-12"> <label>Booking Status: </label></div>

<div class="col-md-2 col-xs-12"> <b><%= InsuranceHdr.Status.ToString()%> </b></div>


<div class="clearfix"> </div>
</div>


<div class="col-md-12 padding-0 marbot_10"> 

<div class="col-md-2"> <label>Departure Date: </label></div>

<div class="col-md-2"> <b><%= InsuranceHdr.DepartureDateTime%> </b></div>
<div class="col-md-2"><label>Return Date: </label> </div>
<div class="col-md-2"><b><%= InsuranceHdr.ReturnDateTime%> </b> </div>

<div class="col-md-2"><label>Trip Id: </label> </div>
<div class="col-md-2"> <b><%= InsuranceHdr.AgentRefNo%> </b></div>


<div class="clearfix"> </div>
</div>



<div class="col-md-12 padding-0 marbot_10"> 


<div class="col-md-2"> <label>PNR: </label></div>
<div class="col-md-2"> <b><%= InsuranceHdr.PNR%> </b></div>

<div class="col-md-2"><label>Total Amount: </label> </div>
<div class="col-md-2"><b><%= InsuranceHdr.Currency%>&nbsp;&nbsp; <%= (Convert.ToDecimal(InsuranceHdr.TotalAmount) ).ToString("N" + agent.DecimalValue)%> </b> </div>
<div class="col-md-2"> <label>Booking Date: </label></div>

<div class="col-md-2"> <b><%= InsuranceHdr.CretedOn.ToString("dd-MMM-yyyy hh:mm:ss")%> </b></div>

<div class="clearfix"> </div>
</div>
   
   
   
<div class="col-md-12 padding-0 marbot_10"> 

<div class="col-md-2"><label>Adults: </label> </div>

<div class="col-md-2"><b><%= InsuranceHdr.Adults%> </b> </div>
<div class="col-md-2"><label>Childs: </label> </div>
<div class="col-md-2"> <b><%= InsuranceHdr.Childs%> </b></div>

<div class="col-md-2"> <label>Infants: </label></div>
<div class="col-md-2"><b><%= InsuranceHdr.Infants%> </b> </div>


<div class="clearfix"> </div>
</div>


 <div class="col-md-12 padding-0"> 

<%--<div class="col-md-2"><label>Premium Charge type: </label> </div>

<div class="col-md-2"> <b><%= InsuranceHdr.Rows[0]["PremiumChargeType"]%> </b></div>
--%><div class="col-md-2"> <label>Product: </label></div>
<div class="col-md-2"> 
<%if (InsuranceHdr.Product == "S")
  { %>
<b>Standalone </b>
<%}
           else
  { %>
 <b>Flight </b>


  <%} %>
 
</div>
<div class="clearfix"></div>
<%if (InsuranceHdr.SourceCurrency == "INR")
{%>
<div class="ns-h3"> Nominee Details
</div>
<div class="col-md-12 padding-0 marbot_10"> 
<div class="col-md-2"><label>FirstName: </label> </div>
<div class="col-md-2"><b><%= InsuranceHdr.NomineeFirstName%> </b> </div>

<div class="col-md-2"><label>LastName: </label> </div>
<div class="col-md-2"><b><%= InsuranceHdr.NomineeLastName%> </b> </div>

<div class="col-md-2"><label>Mobile No : </label> </div>
<div class="col-md-2"><b><%= InsuranceHdr.NomineeMobileNo%> </b> </div>
<div class="clearfix"> </div>
</div>
<div class="col-md-12 padding-0 marbot_10" title="Nominee Details"> 
<div class="col-md-2"><label>Email: </label> </div>
<div class="col-md-2"><b><%= InsuranceHdr.NomineeEmailAddress%> </b> </div>

<div class="col-md-2"><label>Country : </label> </div>
<div class="col-md-2"><b><%= InsuranceHdr.NomineeCountry%> </b> </div>

<div class="col-md-2"><label>Remarks : </label> </div>
<div class="col-md-2"><b><%= InsuranceHdr.NomineeRemarks%> </b> </div>
<div class="clearfix"> </div>
</div>
<%} %>
<div class="clearfix"> </div>
</div> 
  
  
  <%} %> 
   
    <div class="clearfix"></div>
    </div>
    
    
    <div class="table-responsive">
     <div class="col-md-12 padding-0 marbot_10">
      <% if (InsuranceHdr != null && InsuranceHdr.InsPassenger != null && InsuranceHdr.InsPassenger.Count > 0)
                     {
                         for (int i = 0; i < InsuranceHdr.InsPassenger.Count; i++)
                         {%>
              <div id="accordion" class="panel-group">
  <div class="panel panel-default">
   
   
    <div class="panel-heading">
      <h4 class="panel-title">
        <a href="#collapseOne<%=i %>" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle btn-block collapsed" aria-expanded="false">
          <span class="glyphicon pull-right font_bold glyphicon-plus"></span>
       
       <%=InsuranceHdr.InsPassenger[i].FirstName + " " + InsuranceHdr.InsPassenger[i].LastName%>
          
        </a>
      </h4>
    </div>
   
   
    <div class="panel-collapse collapse" id="collapseOne<%=i %>" aria-expanded="false" style="height: 0px;">
      <div> 
          <div class="col-md-3 col-xs-6"><b>Pax Type:</b><%=InsuranceHdr.InsPassenger[i].PaxType%></div>
          <div class="col-md-3 col-xs-6"><b>Pax Gender:</b><%=InsuranceHdr.InsPassenger[i].Gender.ToString()%></div>
          <div class="col-md-3 col-xs-6"><b>Pax DOB:</b><%=InsuranceHdr.InsPassenger[i].DOB.ToString("dd-MMM-yyyy")%></div>
          <div class="col-md-3 col-xs-6"><b>Pax Email:</b><%=InsuranceHdr.InsPassenger[i].Email%></div>
          <div class="col-md-3 col-xs-6"><b>Pax City:</b><%=InsuranceHdr.InsPassenger[i].City%></div>
          <div class="col-md-3 col-xs-6"><b>Pax Country:</b><%=InsuranceHdr.InsPassenger[i].Country%></div>
          <div class="col-md-3 col-xs-6"><b>Pax Phone:</b><%=InsuranceHdr.InsPassenger[i].PhoneNumber%></div>
          <div class="col-md-3 col-xs-6"><b>Pax Doc Type:</b><%=InsuranceHdr.InsPassenger[i].DocumentType%></div>
          <div class="col-md-3 col-xs-6"><b>Pax Doc No:</b><%=InsuranceHdr.InsPassenger[i].DocumentNo%></div>
      
      </div>
      
      
       <table class="table table-bordered">
       <tr>
      
       <td>Plan Title</td>
       <td>Plan Code</td>
       <td>Policy No</td>
        <td>Policy Link</td>
        
       </tr>
      
       <%if (InsuranceHdr.InsPlans != null && InsuranceHdr.InsPlans.Count > 0)
         {
             for (int k = 0; k < InsuranceHdr.InsPlans.Count; k++)
             {%>
            
       <tr>
       <td><%=InsuranceHdr.InsPlans[k].PlanTitle%></td>
       <td><%=InsuranceHdr.InsPlans[k].InsPlanCode%></td>
       <% if (!string.IsNullOrEmpty(InsuranceHdr.InsPlans[k].PolicyNo))
           {   string [] policyNo = InsuranceHdr.InsPassenger[i].PolicyNo.Split('|');
               for (int m=0; m<policyNo.Length; m++)
               {
                   if (policyNo[m].Contains(InsuranceHdr.InsPlans[k].PolicyNo))
                   {%>
                   <td><%=policyNo[m]%></td>
                  <% }
               }
         } %>
           <% if (!string.IsNullOrEmpty(InsuranceHdr.InsPlans[k].PolicyNo) && !string.IsNullOrEmpty(InsuranceHdr.InsPassenger[i].PolicyUrlLink))
            {
                string[] policyUrl = InsuranceHdr.InsPassenger[i].PolicyUrlLink.Split('|');
                for (int n = 0; n < policyUrl.Length; n++)
                {
                    if (policyUrl[n].Contains(InsuranceHdr.InsPlans[k].PolicyNo))
                    {%>
                 <td><a target='_blank' href='<%=policyUrl[n]%>'>View Certificate</a></td>
                    <%}
                }
           } %>
       </tr> 
       <%}
         } %>
         </table>
    </div>
  </div>


   </div>
            <%}
                     }%>
                     
                <div id="divPlan" runat="server">
                </div>
            </div>
    </div>
    
          </div>
          
                                 <script>


                                                $('.collapse').on('shown.bs.collapse', function() {
                                                    $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
                                                }).on('hidden.bs.collapse', function() {
                                                    $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
                                                });

</script> 
         
<%-- </form>--%>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

