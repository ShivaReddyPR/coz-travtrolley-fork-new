﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="TransferAcctReportGUI" Title="Transfer Account Report" Codebehind="TransferAcctReport.aspx.cs" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls" TagPrefix="cc1" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<asp:HiddenField runat="server" id="hdfParam" value="1"></asp:HiddenField>
      <table cellpadding="0" cellspacing="0" class="label">
            <tr>
                <td style="width:100px" align="right"><a style="cursor:Hand;font-weight:bold;font-size:8pt;color:Black" id="ancParam" onclick="return ShowHide('divParam');">Hide Parameter</a> </td>
                <td width="800px" align="right">
        </td>
          </tr>
         </table>
          <div class="grdScrlTrans" style="margin-top:-1px;">
    <div title="Param" id="divParam">
        <asp:Panel runat="server" ID="pnlParam" Visible="true" >
        <table cellpadding="0" cellspacing="0" class="label">
               <tr>
                <td style="width:125px"></td>
                <td style="width:150px"></td>
                <td style="width:75px"></td>
                <td style="width:150px"></td>
                <td style="width:80px"></td>
                <td style="width:120px"></td>
                <td style="width:75px"></td>
                <td style="width:120px"></td>
                <td style="width:120px"></td>
            </tr> 
            <tr>
                <td colspan="3"></td>
            </tr> 
                <tr>
                    <td align="right"><asp:Label ID="lblFromDate" Text="From Date:" runat="server" CssClass="label" ></asp:Label></td>
                    <td ><uc1:DateControl Id="dcFromDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></td>
                    <td align="right"><asp:Label ID="lblToDate" Text="To Date:" runat="server" CssClass="label" ></asp:Label></td>
                    <td ><uc1:DateControl Id="dcToDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="false" DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl></td>
                   <td align="right"><asp:Label ID="lblAgent" runat="server" Visible="true"  Text="Agent:"></asp:Label></td>        
                    <td><asp:DropDownList ID="ddlAgent"  CssClass="inputDdlEnabled" Visible="true"  runat="server" Width="120px" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
              
               </tr>  
               <tr>
               <td align="right"><asp:Label ID="lblB2BAgent" runat="server" Visible="true"  Text="B2BAgent:" CssClass="label"></asp:Label></td>        
                    <td><asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="inputDdlEnabled" Width="120px" OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList></td>
                     <td align="right"><asp:Label ID="lblB2B2BAgent" runat="server" Visible="true"  Text="B2B2BAgent:" CssClass="label"></asp:Label></td>        
                    <td><asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled" Width="120px"></asp:DropDownList></td>
                <td align="right" class="label">
                       <asp:Label ID="lblSource" runat="server" Text="Source:"></asp:Label>
                   </td>
                   <td>
                       <asp:DropDownList ID="ddlSource" runat="server" AppendDataBoundItems="true" Width="120">
                           <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                       </asp:DropDownList>
                   </td>
               </tr>
               <tr>
                  <%-- <td align="right" class="label"><asp:Label ID="lblStatus" runat="server" Text="Status"></asp:Label></td>
                   <td><asp:DropDownList ID="ddlStatus" runat="server" Width="120px">
                    <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                   </asp:DropDownList></td>--%>
                   
                   <td align="right" class="label" ><asp:Label ID="lblAcctStatus" runat="server" Text="Accounted Status"></asp:Label></td>
                   <td>
                       <asp:DropDownList ID="ddlAcctStatus" runat="server" Width="120px">
                           <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                           <asp:ListItem  Value="1" Text="YES"></asp:ListItem>
                           <asp:ListItem  Value="0" Text="NO"></asp:ListItem>
                       </asp:DropDownList>
                   </td>
                   <td><asp:Label ID="lblTransType" runat="server" Text="TransType:" Visible="false"></asp:Label></td>
                    <td><asp:DropDownList ID="ddlTransType" runat="server" Width="120px" Visible="false">
                    <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                           <asp:ListItem  Value="B2B" Text="B2B"></asp:ListItem>
                           <asp:ListItem  Value="B2C" Text="B2C"></asp:ListItem>
                    </asp:DropDownList>
                    </td>
                    <td align="right" class="label">&nbsp;</td>
                   <td>&nbsp;</td>
                   <td>
                       <asp:Button runat="server" ID="btnSearch" Text="Search" Width="50px" OnClientClick="return ValidateParam();"
                           CssClass="button" OnClick="btnSearch_Click" />
                   </td>
               </tr>      
        </table>
        </asp:Panel>
    </div>
    <table width="100%"  id="tabSearch" border="0" cellpadding="0" cellspacing="0">
    <tr>
    <td>
        <asp:GridView ID="gvTransferAcctReport" Width="100%" runat="server" AllowPaging="true"
            DataKeyNames="transferId" EmptyDataText="No Transfer List!" AutoGenerateColumns="false"
            PageSize="25" GridLines="none" CssClass="grdTable" CellPadding="4" CellSpacing="0"
            OnPageIndexChanging="gvTransferAcctReport_PageIndexChanging" >
            <HeaderStyle CssClass="gvHeader" HorizontalAlign="Left"></HeaderStyle>
            <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
            <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
            <Columns>
               <asp:TemplateField>
                <ItemTemplate>
                    <asp:CheckBox ID="ITchkSelect" runat="server" Width="20px" CssClass="label" Checked='<%# Eval("isAccounted").ToString()!= "N"%>'
                        Enabled='<%# Eval("isAccounted").ToString()!="U"%>'></asp:CheckBox>
                    <asp:HiddenField ID="IThdfTransferId" runat="server" Value='<%# Bind("transferId") %>'></asp:HiddenField>
                </ItemTemplate>
            </asp:TemplateField>
            
                <asp:TemplateField>
                    <ItemStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' CssClass="label"
                            ToolTip='<%# Container.DataItemIndex+1 %>' Width="20px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtAgentCode" HeaderText="Agent Code" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentCode" runat="server" Text='<%# Eval("agent_code") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("agent_code") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtAgentName" HeaderText="Agent Name" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentName" runat="server" Text='<%# Eval("agent_name") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("agent_name") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
               
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td>
                                    <uc1:DateControl ID="TFtxtBookingDate" runat="server" DateOnly="true" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="STbtnBookingDate" runat="server" ImageUrl="~/Images/wg_filter.GIF"
                                        ImageAlign="AbsMiddle" OnClick="Filter_Click" />
                                </td>
                            </tr>
                        </table>
                        <label class="filterHeaderText">
                            Booking.&nbsp;Date</label>
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblBookingDate" Width="120px" runat="server" Text='<%# CTDateTimeFormat(Eval("createdOn")) %>'
                            CssClass="label grdof" ToolTip='<%# Eval("createdOn") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <%-- <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="HTtxtHotelRef" HeaderText="Hotel Ref" CssClass="inputEnabled" Width="120px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblHotelRef" runat="server" Text='<%#Eval("bookingID")%>'
                            CssClass="label grdof" ToolTip='<%# Eval("bookingID")%>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>--%>
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtSupplier" HeaderText="Supplier" CssClass="inputEnabled" Width="60px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblSupplier" runat="server" Text='<%# Eval("transferSource") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("transferSource") %>' Width="60px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtSupConfirm" HeaderText="Sup Confirm" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblSupCondirm" runat="server" Text='<%# Eval("confirmationNo") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("confirmationNo") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtSupReference" HeaderText="Sup Reference" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblSupReference" runat="server" Text='<%# Eval("bookingRef") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("bookingRef") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtPassengerName" HeaderText="Passenger Name" CssClass="inputEnabled" Width="120px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblPassengerName" runat="server" Text='<%# Eval("PassengerName") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("PassengerName") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtPaxCount" HeaderText="No Of Pax" CssClass="inputEnabled" Width="60px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblPaxCount" runat="server" Text='<%# Eval("TotalPax") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("TotalPax") %>' Width="60px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                  <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <table>
                            <tr>
                                <td>
                                    <uc1:DateControl ID="TFtxtDepDate" runat="server" DateOnly="true" />
                                </td>
                                <td>
                                    <asp:ImageButton ID="STbtnDepDate" runat="server" ImageUrl="~/Images/wg_filter.GIF"
                                        ImageAlign="AbsMiddle" OnClick="Filter_Click" />
                                </td>
                            </tr>
                        </table>
                        <label class="filterHeaderText">
                            Transfer&nbsp;Date</label>
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblCheckIn" Width="120px" runat="server" Text='<%# CTDateTimeFormat(Eval("transferDate")) %>'
                            CssClass="label grdof" ToolTip='<%# Eval("transferDate") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtTransfer" HeaderText="Transfer Name" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle/>
                    <ItemTemplate>
                        <asp:Label ID="ITlblTransfer" runat="server" Text='<%# Eval("itemName") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("itemName") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtPickupType" HeaderText="Pickup Type" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle/>
                    <ItemTemplate>
                        <asp:Label ID="ITlblPickupType" runat="server" Text='<%# Eval("PickupType") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("PickupType") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                 <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtDropoffType" HeaderText="Dropoff Type" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle/>
                    <ItemTemplate>
                        <asp:Label ID="ITlblDropoffType" runat="server" Text='<%# Eval("DropoffType") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("DropoffType") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtTransferTime" HeaderText="Duration" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle/>
                    <ItemTemplate>
                        <asp:Label ID="ITlblPickupTime" runat="server" Text='<%# Eval("TransferTime") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("TransferTime") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtLang" HeaderText="Language" CssClass="inputEnabled" Width="100px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle/>
                    <ItemTemplate>
                        <asp:Label ID="ITlblLang" runat="server" Text='<%# Eval("Language") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("Language") %>' Width="120px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtVehicleName" HeaderText="Vehicle Name" CssClass="inputEnabled" Width="120px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblVehicleName" runat="server" Text='<%# Eval("VehicleName") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("VehicleName") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                 <asp:TemplateField>
                    <HeaderStyle HorizontalAlign="left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtVehicleCount" HeaderText="No Of Vehicle" CssClass="inputEnabled" Width="120px"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblVehicleCount" runat="server" Text='<%# Eval("NoOfVehicle") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("NoOfVehicle") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtCity" HeaderText="City" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblCity" runat="server" Text='<%# Eval("cityName") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("cityName") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtPayableAmount" HeaderText="Payable Amount" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblPayableAmount" runat="server" Text='<%# CTCurrencyFormat(Eval("Payableamount"),Eval("agent_decimal") ) %>'  CssClass="label grdof"
                            ToolTip='<%# CTCurrencyFormat(Eval("Payableamount"),Eval("agent_decimal") ) %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtInvoiceAmount" HeaderText="Invoice Amount" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblInvoiceAmount" runat="server" Text='<%#CTCurrencyFormat(Eval("TotalAmount"),Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# CTCurrencyFormat(Eval("TotalAmount"),Eval("agent_decimal")) %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtProfit" HeaderText="Profit" Width="80px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblProfit" runat="server" Text='<%# CTCurrencyFormat(Eval("profit"),Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# CTCurrencyFormat(Eval("profit"),Eval("agent_decimal")) %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                   
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtCurrrency" HeaderText="Currency" Width="80px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblCurrency" runat="server" Text='<%# Eval("agent_currency") %>' CssClass="label grdof"
                            ToolTip='<%# Eval("agent_currency") %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle  HorizontalAlign="Left" />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtAgentSF" HeaderText="AgentSF" Width="80px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle />
                    <ItemTemplate>
                        <asp:Label ID="ITlblAgentSF" runat="server" Text='<%# CTCurrencyFormat(Eval("AgentSF"),Eval("agent_decimal")) %>' CssClass="label grdof"
                            ToolTip='<%# CTCurrencyFormat(Eval("AgentSF"),Eval("agent_decimal")) %>' Width="80px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtInvoiceNo" HeaderText="Invoice No" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblInvoiceNo" runat="server" Text='<%# Eval("InvoiceNo") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("InvoiceNo") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtUserName" HeaderText="User Name" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblUserName" runat="server" Text='<%# Eval("user_full_name") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("user_full_name") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtStatus" HeaderText="Status" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblStatus" runat="server" Text='<%# Eval("status") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("status") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtLocation" HeaderText="Location" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblLocation" runat="server" Text='<%# Eval("LocationName") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("LocationName") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                
                <asp:TemplateField>
                    <HeaderStyle />
                    <HeaderTemplate>
                        <cc1:Filter ID="TFtxtAcctStatus" HeaderText="AccountedStatus" Width="100px" CssClass="inputEnabled"
                            OnClick="Filter_Click" runat="server" />
                    </HeaderTemplate>
                    <ItemStyle HorizontalAlign="left" />
                    <ItemTemplate>
                        <asp:Label ID="ITlblAcctStatus" runat="server" Text='<%# Eval("AccountedStatus") %>'
                            CssClass="label grdof" ToolTip='<%# Eval("AccountedStatus") %>' Width="100px"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </td></tr>
    </table>
              <table>
                  <tr>
                      <td width="120px">
                          <asp:Button OnClick="btnUpdateStatus_Click" runat="server" ID="btnUpdateStatus" Text="Update Status"
                              CssClass="button" />
                      </td>
                      <td width="20px">
                      </td>
                      <td width="80px">
                          <asp:Button OnClick="btnExport_Click" runat="server" ID="btnExport" Text="Export To Excel"
                              CssClass="button" />
                      </td>
                      <td>
                      </td>
                  </tr>
              </table>
  </div>
  <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
<asp:Label style="COLOR: #dd1f10" id="lblError" runat="server"></asp:Label>
<div>
    <asp:DataGrid ID="dgTransferAcctReportList" runat="server" AutoGenerateColumns="false"  OnItemDataBound="dgTransferAcctReportList_ItemDataBound">
    <Columns>
    <asp:BoundColumn HeaderText="Agent Code" DataField="agent_code" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Agent Name" DataField="agent_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Booking Date" DataField="createdOn" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Supplier" DataField="transferSource" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="Sup Confirm" DataField="confirmationNo" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Sup Reference" DataField="bookingRef" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Passenger Name" DataField="PassengerName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="No Of Pax" DataField="TotalPax" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Transfer Date" DataField="transferDate" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Pickup Type" DataField="PickupType" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Dropoff Type" DataField="DropoffType" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
     <asp:BoundColumn HeaderText="Duration" DataField="transferTime" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Language" DataField="Language" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Transfer" DataField="itemName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Vehicle Name" DataField="VehicleName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
     <asp:BoundColumn HeaderText="No. Of Vehicle" DataField="NoOfVehicle" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="City" DataField="cityName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Payable Amount" DataField="Payableamount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Invoice Amount" DataField="TotalAmount" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="profit" DataField="profit" HeaderStyle-Font-Bold="true"></asp:BoundColumn> 
    <asp:BoundColumn HeaderText="Currency" DataField="agent_currency" HeaderStyle-Font-Bold="true"></asp:BoundColumn>    
    <asp:BoundColumn HeaderText="AgentSF" DataField="AgentSF" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Invoice No" DataField="InvoiceNo" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Status" DataField="status" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="User Name" DataField="user_full_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="AcctStatus" DataField="AccountedStatus" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    <asp:BoundColumn HeaderText="Location" DataField="LocationName" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
    </Columns>
    </asp:DataGrid>
    </div>
  <script  type="text/javascript">

function ShowHide(div)
{
    if(getElement('hdfParam').value=='1')
        {
            document.getElementById('ancParam').innerHTML='Show Param'
            document.getElementById(div).style.display='none';
            getElement('hdfParam').value='0';
        }
        else
        {
            document.getElementById('ancParam').innerHTML='Hide Param'
           document.getElementById('ancParam').value='Hide Param'
            document.getElementById(div).style.display='block';
            getElement('hdfParam').value='1';
        }
}
function ValidateParam()
{
     clearMessage();
    var fromDate=GetDateTimeObject('ctl00_cphTransaction_dcFromDate');
    var fromTime=getElement('dcFromDate_Time');
    var toDate=GetDateTimeObject('ctl00_cphTransaction_dcToDate');
    var toTime=getElement('dcToDate_Time');
    if(fromDate==null) addMessage('Please select From Date !','');
    //alert(fromTime);
    if(fromTime.value=='') addMessage('Please select From Time!','');
    if(toDate==null) addMessage('Please select To Date !','');
    if(toTime.value=='') addMessage('Please select To Time!','');
    if((fromDate!=null && toDate!=null) && fromDate>toDate) addMessage('From Date should not be later than To Date!','');
    if(getMessage()!=''){ 
    alert(getMessage()); 
    clearMessage(); return false; }
}

 
 function getElement(id)
 {
    return document.getElementById('ctl00_cphTransaction_'+id);
 }

</script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

