﻿using System;
using CT.BookingEngine;
using System.Collections.Generic;
using CT.Core;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.MetaSearchEngine;
public partial class SightseeingBookingConfirm : CT.Core.ParentPage//System.Web.UI.Page
{
    protected SightseeingItinerary itinerary = new SightseeingItinerary();
    protected SightseeingStaticData staticInfo = new SightseeingStaticData();
    protected decimal amountToCompare = 0;
    //protected AgentMaster agency;
   
    protected decimal rateOfExchange = 1;
    protected decimal markup = 0, total = 0;
    protected SightSeeingReguest request;
    protected BookingResponse bookRes = new BookingResponse();
    protected Dictionary<string, string> CancellationData = new Dictionary<string, string>();
    protected string warningMsg = "";
    protected string cancelData = "", amendmentData = "";
    protected MetaSearchEngine mse;
    protected int tourId = 0;
    protected AgentMaster agency;
    int agencyId = -1;
    protected LocationMaster location = new LocationMaster();
    protected decimal outPutVat = 0m;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agencyId = Settings.LoginInfo.OnBehalfAgentID;
                agency = new AgentMaster(agencyId);
               
                location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
            }
            else
            {
                agencyId = Settings.LoginInfo.AgentId;
                agency = new AgentMaster(Settings.LoginInfo.AgentId);
                location = new LocationMaster(Settings.LoginInfo.LocationID);
            }
            if (Settings.LoginInfo != null)
            {
                if (Session["SItinerary"] != null)
                {
                    itinerary = Session["SItinerary"] as SightseeingItinerary;
                    if (Session["Markup"] != null)
                    {
                        markup = Convert.ToDecimal(Session["Markup"]); //PageLevelMarkup
                    }
                    if (Request.QueryString["TourId"] != null)
                    {
                        tourId = Convert.ToInt16(Request.QueryString["TourId"]);
                    }
                    rateOfExchange = (Settings.LoginInfo.ExchangeRate > 0 ? Settings.LoginInfo.ExchangeRate : 1);


                    
                    if (itinerary.Price.AccPriceType == PriceType.NetFare)
                    {
                        total = Math.Round((itinerary.Price.NetFare + itinerary.Price.Markup), Settings.LoginInfo.DecimalValue);
                    }
                    //Use ceiling instead of Round, Changed on 06082016
                    markup += Math.Ceiling(total);
                    //markup=Math.Round(markup,Settings.LoginInfo.DecimalValue);
                    decimal totMarkup = 0, totalSGAmount = 0; ;
                    if (!IsPostBack)
                    {
                        mse = new MetaSearchEngine(Session["cSessionId"].ToString());
                        mse.SettingsLoginInfo = Settings.LoginInfo;
                        //mse.DecimalPoint = agency.DecimalValue;

                        //Here IN Means India Users IF India User logined we need to calucate GST Oather wise Need to Calucate VAT 
                        if (location.CountryCode == "IN")
                        {
                            List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                            decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, (itinerary.Price.Markup), Settings.LoginInfo.LocationID);
                            itinerary.Price.GSTDetailList = new List<GSTTaxDetail>();
                            itinerary.Price.GSTDetailList = gstTaxList;
                            outPutVat = Math.Round(gstAmount, Settings.LoginInfo.DecimalValue);
                            itinerary.Price.OutputVATAmount = outPutVat;
                        }
                        else
                        {
                            //Out put Vat Calucation
                            //PriceTaxDetails taxDetails = PriceTaxDetails.GetVATCharges(Settings.LoginInfo.LocationCountryCode, Settings.LoginInfo.LocationCountryCode, Settings.LoginInfo.LocationCountryCode, (int)ProductType.SightSeeing, Module.SightSeeing.ToString(), "International"); //itinerary.Price.TaxDetails;
                            PriceTaxDetails taxDetails = PriceTaxDetails.GetVATCharges(location.CountryCode, location.CountryCode, location.CountryCode, (int)ProductType.SightSeeing, Module.SightSeeing.ToString(), "International"); //itinerary.Price.TaxDetails;
                            if (Settings.LoginInfo.AgentType == AgentType.Agent)
                            {
                                totalSGAmount = Math.Ceiling(markup); //here markup means total with Pagelevel markup
                                totMarkup = Math.Round((itinerary.Price.Markup + itinerary.Price.AsvAmount), Settings.LoginInfo.DecimalValue);
                            }
                            else
                            {
                                totalSGAmount = Math.Round(Math.Ceiling(total), agency.DecimalValue);
                                totMarkup = Math.Round(itinerary.Price.Markup, agency.DecimalValue);
                            }
                            if (taxDetails != null && taxDetails.OutputVAT != null)
                            {
                                outPutVat = taxDetails.OutputVAT.CalculateVatAmount(totalSGAmount, totMarkup, agency.DecimalValue);
                                itinerary.Price.OutputVATAmount = outPutVat;
                            }
                        }
                        if (itinerary.Source == SightseeingBookingSource.GTA)
                        {
                            //staticInfo = gta.GetSightseeingItemInformation(itinerary.CityCode, itinerary.ItemName, itinerary.ItemCode);
                            cancelData = itinerary.CancellationPolicy.Split('@')[0];
                            amendmentData = itinerary.CancellationPolicy.Split('@')[1];

                        }
                    }
                    // hdnWarning.Value = warningMsg;

                }
                else
                {
                    Response.Redirect("Sightseeing.aspx");
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderPageLoad");
            PaymentMultiView.ActiveViewIndex = 1;
            lblError.Text = ex.Message;

        }

    }
    protected void imgMakePayment_Click(object sender, EventArgs e)
    {
        try
        {
            //get only pax name
            List<string> paxDetail = new List<string>();

            foreach (string getPax in itinerary.PaxNames)
            {

                paxDetail.Add(getPax.Split('|')[0]);
            }
            itinerary.PaxNames = null;
            itinerary.PaxNames = paxDetail;
            itinerary = Session["SItinerary"] as SightseeingItinerary;
            //getting here check balance
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                agency.UpdateBalance(0);
            }
            else
            {
                agency = new AgentMaster(Settings.LoginInfo.AgentId);
                agency.UpdateBalance(0);
            }

            //assign update balance to current agent id
            Settings.LoginInfo.AgentBalance = agency.CurrentBalance;

            amountToCompare = agency.CurrentBalance;

            decimal sightseeingCost = 0;

            sightseeingCost = itinerary.Price.GetAgentPrice();
            sightseeingCost = Math.Ceiling(sightseeingCost) + Math.Ceiling(itinerary.Price.OutputVATAmount);

            if (sightseeingCost > amountToCompare)
            {
                lblMessage.Text = "Dear Agent you do not have sufficient balance to book Sightseeing.";
                return;
            }
            else
            {
                request = Session["ssReq"] as SightSeeingReguest;
                itinerary.ProductType = ProductType.SightSeeing;
                itinerary.Nationality = request.Nationality;
                Product prod = (Product)itinerary;
                //Product prod = new Product();
                prod.ProductId = itinerary.ProductId;
                prod.ProductType = itinerary.ProductType;
                prod.ProductTypeId = itinerary.ProductTypeId;

                if (itinerary.ProductType != ProductType.SightSeeing)
                {
                    itinerary.ProductType = ProductType.SightSeeing;
                }

                itinerary.CancelId = "";

                itinerary.CreatedOn = DateTime.Now;
                itinerary.CreatedBy = (int)Settings.LoginInfo.UserID;
                //Update markup
                if (Session["Markup"] != null)
                {
                    //itinerary.Price.Markup += Convert.ToDecimal(Session["Markup"]);
                }
               
                if (itinerary.Source == SightseeingBookingSource.GTA)
                {
                    try
                    {
                        SightseeingSearchResult[] SearchResults = new SightseeingSearchResult[0];

                        List<string> catCode = new List<string>();

                        mse = new MetaSearchEngine(Session["cSessionId"].ToString());
                        mse.SettingsLoginInfo = Settings.LoginInfo;
                        request.ItemCode = itinerary.ItemCode;
                        request.ItemName = itinerary.ItemName.Trim();
                        SearchResults = mse.GetSightseeingResults(request, agencyId);
                        decimal oldPrice = 0;
                        decimal newPrice = 0;
                        oldPrice += itinerary.Price.NetFare;
                        if (SearchResults[0].TourOperationList.Length > tourId)
                        {
                            if (itinerary.Price.NetFare == SearchResults[0].TourOperationList[tourId].PriceInfo.NetFare)
                            {
                                newPrice += SearchResults[0].TourOperationList[tourId].PriceInfo.NetFare;

                            }
                        }
                        if (oldPrice != newPrice)
                        {
                            lblMessage.Text = "Price has been revised for this Sightseeing. Please search again to book this Sightseeing.";
                            Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderPageLoad");
                            PaymentMultiView.ActiveViewIndex = 1;
                            lblError.Text = lblMessage.Text;
                            return;
                        }
                    }
                    catch
                    {
                        warningMsg = "Price has been revised for this Sightseeing. Please search again to book this Sightseeing.";
                    }
                }


                    if (warningMsg.Length <= 0)
                {
                    // actual booking starts
                    mse = new MetaSearchEngine(Session["cSessionId"].ToString());
                    bookRes = mse.Book(ref prod, agencyId, BookingStatus.Ready, Settings.LoginInfo.UserID);
                    

                    if (bookRes.Status != BookingResponseStatus.Successful)
                    {
                        //Added on 12032016
                        string bookingMsg = "Price has been revised for this Sightseeing. Please search again to book this Sightseeing.";
                        PaymentMultiView.ActiveViewIndex = 1;
                        if (bookRes.Error != null && bookRes.Error.Length > 0)
                        {

                            bookingMsg = bookRes.Error;

                        }

                        //Show failed message
                        lblError.Text = bookingMsg;

                        Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloader");
                        PaymentMultiView.ActiveViewIndex = 1;
                    }

                    else
                    {
                        Session["SItinerary"] = itinerary;
                        Session["BookingResponse"] = bookRes;

                        //SendHotelVoucherEmail();
                        //Load or save the invoice
                        int invoiceNumber = Invoice.isInvoiceGenerated(itinerary.SightseeingId, ProductType.SightSeeing);
                        Invoice invoice = new Invoice();
                        if (invoiceNumber > 0)
                        {
                            invoice.Load(invoiceNumber);
                        }
                        else
                        {
                            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.SightseeingId, string.Empty, (int)Settings.LoginInfo.UserID, ProductType.SightSeeing, rateOfExchange);
                            invoice.Load(invoiceNumber);
                            invoice.Status = InvoiceStatus.Paid;
                            invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                            invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                            invoice.UpdateInvoice();

                            //SendHotelInvoiceEmail(invoiceNumber);
                        }
                        try
                        {
                            //Reduce Agent Balance
                            decimal total = 0;

                            if (itinerary.Price.AccPriceType == PriceType.NetFare)
                            {
                                total = (itinerary.Price.NetFare + itinerary.Price.Markup);
                            }



                            //No need to deduct from agent balance as it is addl markup only. We are not adding markup to total
                            //so no need to deduct addl markup from total now. commented by shiva on 28 Aug 2014 4:30 PM
                            //total = Math.Ceiling(total) - Convert.ToDecimal(Session["Markup"]);

                            //Use ceiling instead of Round, Changed on 06082016
                            total = Math.Ceiling(total) + Math.Ceiling(itinerary.Price.OutputVATAmount);
                            if (bookRes.Status == BookingResponseStatus.Successful)
                            {
                                //Update agent balance, reduce the Sightseeing booking amount from the balance.
                                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                {
                                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                                    agency.CreatedBy = Settings.LoginInfo.UserID;
                                    agency.UpdateBalance(-total);
                                }
                                else
                                {
                                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                                    agency.CreatedBy = Settings.LoginInfo.UserID;
                                    agency.UpdateBalance(-total);

                                }

                                //after transaction update current agent balance
                                agency.CurrentBalance -= total;

                            }

                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.SightseeingBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to update Agent Balance. Error: " + ex.Message, Request["REMOTE_ADDR"]);
                        }

                        Response.Redirect("SightseeingPaymentVoucher.aspx?ConfNo=" + bookRes.ConfirmationNo, false);

                    }
                }
                else//In Case of Warning message search again.
                {
                    if (warningMsg.Length > 0)
                    {
                        //Response.Redirect("Sightseeing.aspx");
                        PaymentMultiView.ActiveViewIndex = 1; //changed on 14/03/2016
                        lblError.Text = warningMsg;
                    }

                }

            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.SightseeingBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed Sightseeing Booking. Error: " + ex.Message, Request["REMOTE_ADDR"]);
            Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderErr");
            PaymentMultiView.ActiveViewIndex = 1;

            if (string.IsNullOrEmpty(ex.Message))
            {
                lblError.Text = ex.Message;
            }
            else
            {
                lblError.Text = "There was a problem booking your Sightseeing Item. Please try after some time.";
            }
        }
    }
   

}
