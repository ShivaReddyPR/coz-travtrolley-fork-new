﻿<%@ WebHandler Language="C#" Class="hn_GSTFileUploader" %>

using System;
using System.Web;
using System.IO;
using System.Drawing;
using System.Web.SessionState;
using System.Collections.Generic;
using CT.Core;

public class hn_GSTFileUploader : IHttpHandler, IRequiresSessionState
{
    //int numFiles = 1;
    public void ProcessRequest(HttpContext context)
    {
        try
        {

            //uploaded files saving session
            List<HttpPostedFile> files = new List<HttpPostedFile>();
            if (context.Session["GSTFiles"] != null)
            {
                files = context.Session["GSTFiles"] as List<HttpPostedFile>;
            }
            foreach (string s in context.Request.Files)
            {
                HttpPostedFile file = context.Request.Files[s];
                files.Add(file);
            }
            context.Session["GSTFiles"] = files;
        }
        catch(Exception ex) {
            Audit.Add(EventType.Exception, Severity.High, 1, "(Invoice Docs)Failed to upload. Reason : "+ex.ToString(), "0");
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }

    

}
