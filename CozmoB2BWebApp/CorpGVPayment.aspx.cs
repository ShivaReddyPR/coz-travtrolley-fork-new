﻿using CT.AccountingEngine;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.GlobalVisa;
using CT.TicketReceipt.BusinessLayer;
using NetworkInternationalPay;
using System;

public partial class CorpGVPaymentGUI :CT.Core.ParentPage
{
    protected string errorMessage;
    protected void Page_Load(object sender, EventArgs e)
    {
        string paymentId = string.Empty;
        string trackId = string.Empty;
        string orderId = string.Empty;
        #region Network International
        if (!string.IsNullOrEmpty(Request["responseParameter"]))
        {
            NetworkInternationalPaymentService ser = new NetworkInternationalPaymentService();
            string resMessage = ser.DecryptTransactionResponse(Request["responseParameter"]);
            if (!string.IsNullOrEmpty(Request["responseParameter"]))
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "NI Payment GateWay Encrypted response data : " + Request["responseParameter"], Request["REMOTE_ADDR"]);
                //ErrorCode : 00000 -- No Error in the payment processing
                if (resMessage.Split('|')[8] == "00000" && resMessage.Split('|')[9] == "No Error.")
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "NI Payment GateWay Decrypted response data : " + resMessage, Request["REMOTE_ADDR"]);
                    //Loading pending paymentdetails
                    orderId = resMessage.Split('|')[0];
                    paymentId = resMessage.Split('|')[1];
                    long visaId = Convert.ToInt64(resMessage.Split('|')[19]);
                    
                    try
                    {
                        GVVisaSale.UpdatePaymentStatus(visaId, orderId, paymentId, "S", "Payment Sucess");
                        if (Session["GVSession"] != null)
                        {
                            GlobalVisaSession visaSession = Session["GVSession"] as GlobalVisaSession;
                            GVVisaSale objVisaSale = visaSession.VisaSale as GVVisaSale;
                            LedgerTransaction ledgerTxn = new LedgerTransaction();
                            NarrationBuilder objNarration = new NarrationBuilder();
                            ledgerTxn.LedgerId = Settings.LoginInfo.AgentId;
                            ledgerTxn.Amount = -objVisaSale.TotVisaFee;
                            objNarration.HotelConfirmationNo = objVisaSale.DocNumber;
                            objNarration.TravelDate = objVisaSale.TTTravelDate.ToShortDateString();
                            ledgerTxn.ReferenceType = ReferenceType.GlobalVisaBooked;
                            objNarration.Remarks = "Global Visa Booking Charges Debit Entry";
                            ledgerTxn.Narration = objNarration;
                            ledgerTxn.IsLCC = true;
                            ledgerTxn.ReferenceId = Utility.ToInteger(objVisaSale.TransactionId);

                            ledgerTxn.Notes = "";
                            ledgerTxn.Date = DateTime.UtcNow;
                            ledgerTxn.CreatedBy = (int)Settings.LoginInfo.UserID;
                            ledgerTxn.TransType = "B2B";
                            ledgerTxn.Save();
                            
                            ledgerTxn = new LedgerTransaction();
                            objNarration = new NarrationBuilder();
                            ledgerTxn.LedgerId = Settings.LoginInfo.AgentId;
                            ledgerTxn.Amount = objVisaSale.TotVisaFee;
                            objNarration.HotelConfirmationNo = objVisaSale.DocNumber;
                            objNarration.TravelDate = objVisaSale.TTTravelDate.ToShortDateString();
                            ledgerTxn.ReferenceType = ReferenceType.GlobalVisaBooked;
                            objNarration.Remarks = "Global Visa Booking Charges Credit Entry";
                            ledgerTxn.Narration = objNarration;
                            ledgerTxn.IsLCC = true;
                            ledgerTxn.ReferenceId = Utility.ToInteger(objVisaSale.TransactionId);

                            ledgerTxn.Notes = "";
                            ledgerTxn.Date = DateTime.UtcNow;
                            ledgerTxn.CreatedBy = (int)Settings.LoginInfo.UserID;
                            ledgerTxn.TransType = "B2B";
                            ledgerTxn.Save();
                            Session["GVSession"] = null;
                        }
                        Response.Redirect("CorpGVAcknowledgement.aspx?gvId=" + visaId, false);
                    }
                    catch (Exception ex)
                    {
                        throw ex;
                    }
                }
                else
                {
                    string errorMessage = string.Empty;
                    if (!string.IsNullOrEmpty(resMessage.Split('|')[8]) && !string.IsNullOrEmpty(resMessage.Split('|')[9]))
                    {
                        errorMessage = "Description:" + resMessage.Split('|')[9];
                        Audit.Add(EventType.Exception, Severity.High, 1, "NI Payment GateWay error : " + errorMessage, Request["REMOTE_ADDR"]);
                    }
                    else
                    {
                        errorMessage = "Payment declined due to invalid details or due to technical error.";
                        Audit.Add(EventType.Exception, Severity.High, 1, "NI Payment GateWay error : " + errorMessage, Request["REMOTE_ADDR"]);
                    }
                    Response.Redirect("ErrorPage.aspx?error_message=" + errorMessage, false);
                }

            }
            else
            {
                try
                {
                    errorMessage = "Payment declined due to invalid details or due to technical error.";
                }
                catch { }
            }
        }
        else
        {
            string errorMessage = "Payment declined due to invalid details or due to technical error.";
            Audit.Add(EventType.Exception, Severity.High, 1, "NI Payment GateWay error : " + errorMessage, Request["REMOTE_ADDR"]);
        }
        #endregion
    }
}