﻿using System;
using System.Configuration;
using System.Web.UI;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using System.IO;
using System.Linq;

public partial class PrintHotelVoucherUI : CT.Core.ParentPage// System.Web.UI.Page
{
    protected BookingResponse bookingResponse;
    protected HotelItinerary itinerary;
    protected PriceType priceType;
    protected decimal rateOfExchange = 1;
    protected int nights;
    protected AgentMaster agent;
    HotelPassenger hPax = null;
    HotelRoom hRoom;
    PriceAccounts priceInfo = new PriceAccounts();
    bool isMultiRoom = false;
    protected HotelDetails hotelDetails = new HotelDetails();
    protected BookingDetail booking;
    protected AgentMaster parentAgent;
    protected bool showVoucherTerms = false;
    protected string dynamicAgentLabel = "Cozmo";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }

            var pageParams = GenericStatic.GetSetPageParams("PrintHotelVoucher", "", "get").Split('|');
                //if (Request.QueryString["ConfNo"] != null)
                if (pageParams[0] != null && pageParams[0] != "")
                {
                itinerary = new HotelItinerary();
                hPax = new HotelPassenger();
                hRoom = new HotelRoom();
                //string confirmationNo = Request["ConfNo"].ToString();
                //int hotelId = Convert.ToInt32(Request["HotelId"]);
                string confirmationNo = Convert.ToString(pageParams[0]);
                int hotelId = Convert.ToInt32(pageParams[1]);
                if (confirmationNo.Contains("|"))
                {
                    confirmationNo = confirmationNo.Split(new string[] { "|" }, StringSplitOptions.RemoveEmptyEntries)[0];
                }
                string logoPath = "";
                AgentAppConfig clsAppCnf = new AgentAppConfig();
                clsAppCnf.AgentID = Settings.LoginInfo.AgentId;
                var liAppConfig = clsAppCnf.GetConfigData();
                if (liAppConfig != null && liAppConfig.Where(x => x.AppKey.ToUpper() == "HOTELSHOWVOUCHERTERMS").Count() > 0 && liAppConfig.Where(x => x.AppKey.ToUpper() == "HOTELSHOWVOUCHERTERMS").Select(y => y.AppValue).FirstOrDefault().ToUpper() == "TRUE")// Restricting Voucher terms & conditions
                {
                    showVoucherTerms = true;
                }
                //if (Settings.LoginInfo.AgentId > 1)
                //{
                //    //imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + "images/logo.jpg";

                //    logoPath = ConfigurationManager.AppSettings["AgentImage"] + Settings.LoginInfo.AgentLogoPath;
                //    imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + logoPath;
                //    //logoPath = Server.MapPath(new AgentMaster(Settings.LoginInfo.AgentId).ImgFileName);
                //    //System.Drawing.Image img = System.Drawing.Image.FromFile(logoPath, true);
                //    //logoPath = new AgentMaster(Settings.LoginInfo.AgentId).ImgFileName;
                //    //imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + logoPath;

                //    //if (img.Width > 159)
                //    //{
                //    //    imgLogo.Width = new Unit(159, UnitType.Pixel);
                //    //}
                //    //if (img.Height > 51)
                //    //{
                //    //    imgLogo.Height = new Unit(51, UnitType.Pixel);
                //    //}
                //}
                //else
                //{
                //    imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + "images/logo.jpg";
                //} 

                //if (Source.Length > 0 )
                {
                    booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(!string.IsNullOrEmpty(confirmationNo)? HotelItinerary.GetHotelId(confirmationNo):hotelId, ProductType.Hotel));
                    try
                    {
                        Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
                        agent = new AgentMaster(booking.AgencyId);
                        string serverPath = "";
                        if (Request.Url.Port > 0)
                        {
                            serverPath = Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                        }
                        else
                        {
                            serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                        }
                        //to show agent logo
                        if (agent.ID > 1)
                        {

                            logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + agent.ImgFileName;
                            imgLogo.ImageUrl = logoPath;


                            logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + agent.ImgFileName;
                            //imgLogo2.ImageUrl = logoPath; // firoz

                        }
                        else
                        {
                            imgLogo.ImageUrl = serverPath + "images/logo.jpg";


                            //imgLogo2.ImageUrl = serverPath + "images/logo.jpg";//firoz
                        }

                        for (int i = 0; i < products.Length; i++)
                        {
                            if (products[i].ProductTypeId == (int)ProductType.Hotel)
                            {
                                itinerary.Load(products[i].ProductId);
                                if (!(itinerary.HotelPolicyDetails.Contains("^")))
                                {
                                    itinerary.HotelPolicyDetails = itinerary.HotelPolicyDetails + "^";
                                }
                                // Getting Passenger details
                                hPax.Load(products[i].ProductId);
                                itinerary.HotelPassenger = hPax;
                                //Getting parent agent information when user is corporate
                                if (!string.IsNullOrEmpty(itinerary.HotelPassenger.CorpProfileId) && agent.AgentParantId > 0)
                                {
                                    parentAgent = new AgentMaster(agent.AgentParantId);
                                }
                                //Getting HotelRoom Details                

                                itinerary.Roomtype = hRoom.Load(products[i].ProductId);
                                hRoom = itinerary.Roomtype[0];
                                priceInfo.Load(hRoom.PriceId);
                                hRoom.Price = priceInfo;
                                Session["hItinerary"] = itinerary;
                                System.TimeSpan DiffResult = itinerary.EndDate.Subtract(itinerary.StartDate);
                                nights = DiffResult.Days;
                                if (itinerary.Source == HotelBookingSource.DOTW)
                                {
                                    CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
                                    if (string.IsNullOrEmpty(itinerary.HotelPolicyDetails))
                                    {
                                        // hotelDetails = mse.GetHotelDetails(itinerary.CityCode, "Hotel", itinerary.HotelCode, itinerary.Source);

                                        hotelDetails = mse.GetHotelDetails(itinerary.CityCode, itinerary.HotelCode, itinerary.StartDate, itinerary.EndDate, itinerary.PassengerNationality, itinerary.PassengerCountryOfResidence, itinerary.Source);
                                    }
                                }
                                else if (itinerary.Source == HotelBookingSource.RezLive)
                                {
                                    RezLive.XmlHub rezAPI = new RezLive.XmlHub();
                                    hotelDetails = rezAPI.GetHotelDetails(itinerary.HotelCode);
                                }
                                else if (itinerary.Source == HotelBookingSource.LOH)
                                {
                                    LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine();
                                    hotelDetails = jxe.GetHotelDetails(itinerary.HotelCode, itinerary.CityCode);
                                }
                                if (itinerary.Source != HotelBookingSource.Desiya && itinerary.Source != HotelBookingSource.IAN && itinerary.NoOfRooms > 1)
                                {
                                    isMultiRoom = true;
                                }
                                break;
                            }
                        }




                    }
                    catch (Exception exp)
                    {
                        //errorMessage = "Invalid Booking";
                        Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Exception Loading itinerary. Message: " + exp.Message, "");
                    }

                }
            }
            if (Session["hItinerary"] != null)
            {
                itinerary = Session["hItinerary"] as HotelItinerary;
            }
            if (Session["BookingResponse"] != null)
            {
                bookingResponse = (BookingResponse)Session["BookingResponse"];
            }
            //else if (Request.QueryString["ConfNo"] == null)
            else if (pageParams[0] == null && pageParams[0] == "")
            {
                Response.Redirect("AbandonSession.aspx");
            }



        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetHotelBooking, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get Voucher. Error: " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }

    //private void GeneratePDF(string path, string fileName, bool download, string text)
    //{

    //    var document = new Document();

    //    try
    //    {

    //        if (download)
    //        {
    //            PdfWriter.GetInstance(document, Response.OutputStream);
    //        }
    //        else
    //        {
    //            PdfWriter.GetInstance(document, new FileStream(path + fileName, FileMode.Create));
    //        }


    //        // generates the grid first

    //        StringBuilder strB = new StringBuilder();

    //        document.Open();

    //        if (text.Length.Equals(0)) // export the text
    //        {
    //            StringWriter sWriter = new StringWriter();
    //            HtmlTextWriter htWriter = new HtmlTextWriter(sWriter);
    //            PrintDiv.RenderControl(htWriter);
    //            strB.Append(sWriter.ToString());
    //        }
    //        else // export the grid
    //        {
    //            strB.Append(text);
    //        }

    //        string html = strB.ToString();

    //        html = html.Replace("/images/logo.jpg", Server.MapPath("images/logo.jpg"));

    //        // now read the Grid html one by one and add into the document object
    //        StyleSheet style = new StyleSheet();

    //        using (TextReader sReader = new StringReader(html))
    //        {
    //            ArrayList list = HTMLWorker.ParseToList(sReader, new StyleSheet());

    //            foreach (IElement elm in list)
    //            {
    //                document.Add(elm);
    //            }
    //        }
    //    }

    //    catch (Exception ee)
    //    {
    //        Audit.Add(EventType.GetHotelBooking, Severity.High, 1, ee.Message, Request["REMOTE_ADDR"]);
    //    }

    //    finally
    //    {
    //        document.Close();


    //        Response.ContentType = "application/pdf";
    //        Response.AddHeader("content-disposition", "attachment; filename=" + fileName);
    //        Response.TransmitFile(path + fileName);
    //        Response.End();
    //    }

    //}
    //public override void VerifyRenderingInServerForm(Control control)
    //{

    //}

    protected void btnEmailVoucher_Click(object sender, EventArgs e)
    {
        try
        {



            #region Sending Email
            //if (Convert.ToBoolean(ConfigurationManager.AppSettings["isSendEmail"]))
            {

                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();

                toArray.Add(txtEmailId.Text.Trim());
                btnEmail.Visible = false;
                btnPrint.Visible = false;

                string message = "";

                StringWriter sWriter = new StringWriter();
                HtmlTextWriter htWriter = new HtmlTextWriter(sWriter);
                PrintDiv.RenderControl(htWriter);
                message = sWriter.ToString();
                // replacing cozmo to ibyta
                if (System.Web.HttpContext.Current.Request.Url.Host.ToLower().Contains("ibyta.com"))
                {
                    message = message.Replace("Cozmo", "ibyta");
                }
                    try
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "Hotel Voucher", message, null);
                }

                catch (System.Net.Mail.SmtpException ex)
                {
                    throw ex;
                }
                finally
                {
                    btnEmail.Visible = true;
                    btnPrint.Visible = true;
                }
            }
            #endregion

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.Normal, 0, "Failed to send Hotel Voucher to " + txtEmailId.Text + " Reason " + ex.Message, Request["REMOTE_ADDR"]);
        }
    }
}


