﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine;
using CT.Core;
using CT.AccountingEngine;
using CT.MetaSearchEngine;
using System.Text.RegularExpressions;
using CT.Configuration;
using CT.Corporate;

public partial class GuestDetails : CT.Core.ParentPage// System.Web.UI.Page
{
    public HotelItinerary itinerary = new HotelItinerary();
    protected SortedList countryList = new SortedList();
    protected string errorMessage = string.Empty;
    //protected Agency agency;
    protected AgentMaster agency;
    //protected Member loggedMember = new Member();
    protected UserMaster loggedMember;
    protected decimal rateofExchange = 1;
    protected Dictionary<string, decimal> rateOfExList = new Dictionary<string, decimal>();
    protected string symbol = "" + ConfigurationSystem.LocaleConfig["CurrencySign"] + "";
    protected string mailMessage = string.Empty;
    protected string bookingMessage = string.Empty;
    protected bool isMultiRoom = false;
    protected PriceType priceType;
    HotelSource hSource = new HotelSource();
    protected decimal totalPrice = 0, markup = 0, discount = 0;
    string sessionId = "";
    int index = 0;
    protected HotelRequest request = new HotelRequest();
    int agencyId = 0;
    protected DataTable dtDept = new DataTable();
    protected Dictionary<string, string> cancellationInfo = new Dictionary<string, string>();
    protected string warningMsg = "";
    protected Dictionary<int, HotelItinerary> UserBookings = new Dictionary<int, HotelItinerary>();
    protected string cancelData = "", remarks = "";

    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            if (Session["hItinerary"] == null || Session["cSessionId"] == null || Session["req"] == null || Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agencyId = Settings.LoginInfo.OnBehalfAgentID;
                rateOfExList = Settings.LoginInfo.OnBehalfAgentExchangeRates;
            }
            else
            {
                agencyId = Settings.LoginInfo.AgentId;
                rateOfExList = Settings.LoginInfo.AgentExchangeRates;
            }
            agency = new AgentMaster(agencyId);
            if (!IsPostBack)
            {
                try
                {
                    //DOTWApi dotw = new DOTWApi(Session.SessionID);
                    ddlCountry.DataSource = CT.Core.Country.GetCountryList();
                    ddlCountry.DataTextField = "key";
                    ddlCountry.DataValueField = "value";
                    ddlCountry.DataBind();
                    ddlCountry.Items.Insert(0, new ListItem("Select Country"));
                    if (Session["req"] != null)//  ziya to rmove
                    {
                        request = (HotelRequest)Session["req"];
                        DOTWCountry dotw = new DOTWCountry();
                        Dictionary<string, string> Countries = dotw.GetAllCountries();
                        txtNationality.Text = Countries[request.PassengerNationality];
                    }
                    //ddlNationality.DataSource = CT.Core.Country.GetNationalityList();
                    // ddlNationality.DataTextField = "key";
                    //ddlNationality.DataValueField = "value";
                    //ddlNationality.DataBind();
                    //ddlNationality.Items.Insert(0, new ListItem("Select Nationality"));
                    //ddlNationality.SelectedValue = "IN";//ziya to rmove

                    if (agency.AddnlPaxDetails == "Y")
                    {
                        dtDept = HotelPassenger.GetBkeDeptList();// sainadh to do based on agent status(ziyad)
                        ddlDepartment.DataSource = dtDept;
                        ddlDepartment.DataTextField = "dept_name";
                        ddlDepartment.DataValueField = "dept_id";
                        ddlDepartment.DataBind();
                        ddlDepartment.Items.Insert(0, new ListItem("Select Department"));
                    }

                    if (!string.IsNullOrEmpty(txtNationality.Text) && txtNationality.Text.ToUpper() == "INDIA")
                    {
                        BindStates();
                    }
                    
                }
                //catch { }
                catch { throw; }
                try
                {
                    if (Session["hItinerary"] == null)
                    {
                        LoadHotelItinerary();
                    }
                    else
                    {
                        itinerary = Session["hItinerary"] as HotelItinerary;
                    }



                    //CALL MSE METHODS TO GET CANCELLATION DETAILS & HOTEL DETAILS
                    MetaSearchEngine mse = new MetaSearchEngine(sessionId);
                    try
                    {
                        #region Hotel Agreement Details

                        LoadCancellationDetails(ref itinerary);

                        if (itinerary.Source != HotelBookingSource.HotelConnect)
                        {
                            foreach (HotelRoom room in itinerary.Roomtype)
                            {
                                if (room.NonRefundable || room.PaymentMode == "CC" || room.RatePlanCode == "")
                                {
                                    warningMsg = "This Hotel cannot be booked. Please search again to book another Hotel.";
                                }

                            }
                        }
                        #endregion

                        #region Hotel Details
                        // LoadHotelDetails(ref itinerary);

                        #endregion
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.GetHotelDetails, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get HotelItinerary. Error: " + ex.Message, "0");
                    }
                    // for hotel check-in check-out information - End
                    Session["hItinerary"] = itinerary;

                    //lblHotelName.Text = itinerary.HotelName;
                    //lblAdult.Text = itinerary.Roomtype[0].AdultCount.ToString();
                    //lblChild.Text = itinerary.Roomtype[0].ChildCount.ToString();

                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GetHotelDetails, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get HotelItinerary. Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
                }
            }

            // added by Hari 
            if (agency.RequiredFlexFields)
            {
                DataTable dtFlexFields = CorporateProfile.GetAgentFlexDetailsByProduct((int)agency.ID, 2);
                hdnFlexCount.Value = dtFlexFields.Rows.Count.ToString();
                int i = 0;
                foreach (DataRow row in dtFlexFields.Rows)
                {
                    HtmlGenericControl divlblFlex1 = new HtmlGenericControl("Div");
                    divlblFlex1.Attributes.Add("class", "col-md-3");

                    HiddenField hdnFlexId = new HiddenField();
                    hdnFlexId.ID = "hdnFlexId" + i;
                    hdnFlexId.Value = row["flexId"].ToString();
                    divlblFlex1.Controls.Add(hdnFlexId);

                    HiddenField hdnFlexControl = new HiddenField();
                    hdnFlexControl.ID = "hdnFlexControl" + i;
                    hdnFlexControl.Value = Convert.ToString(row["flexControl"]);
                    divlblFlex1.Controls.Add(hdnFlexControl);

                    HiddenField hdnFlexMandatory = new HiddenField();
                    hdnFlexMandatory.ID = "hdnFlexMandatory" + i;
                    hdnFlexMandatory.Value = Convert.ToString(row["flexMandatoryStatus"]);
                    divlblFlex1.Controls.Add(hdnFlexMandatory);

                    HiddenField hdnFlexLabel = new HiddenField();
                    hdnFlexLabel.ID = "hdnFlexLabel" + i;
                    hdnFlexLabel.Value = Convert.ToString(row["flexLabel"]);
                    divlblFlex1.Controls.Add(hdnFlexLabel);

                    Label lblFlex1 = new Label();
                    lblFlex1.ID = "lblFlex" + i;
                    if (Convert.ToString(row["flexMandatoryStatus"]) == "N")
                    {
                        lblFlex1.Text = "<strong>" + Convert.ToString(row["flexLabel"]) + ":</strong>";
                    }
                    else
                    {
                        lblFlex1.Text = "<strong>" + Convert.ToString(row["flexLabel"]) + ":<span class='red_span'>*</span></strong>";
                    }

                    divlblFlex1.Controls.Add(lblFlex1);

                    HtmlGenericControl divFlexControl = new HtmlGenericControl("Div");
                    divFlexControl.Attributes.Add("class", "col-md-3");

                    Label lblError = new Label();
                    lblError.ID = "lblError" + i;
                    lblError.Font.Bold = true;
                    lblError.CssClass = "red_span";

                    HtmlTableCell tableCell1 = new HtmlTableCell();
                    RequiredFieldValidator fieldValidator;
                    switch (row["flexControl"].ToString())
                    {
                        case "T": //TextBox
                            TextBox txtFlex = new TextBox();
                            txtFlex.ID = "txtFlex" + i;
                            txtFlex.CssClass = "form-control";
                            txtFlex.CausesValidation = true;
                            if (row["flexDataType"].ToString() == "N") //N means Numeric
                            {
                                txtFlex.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'1');");
                            }
                            if (row["Detail_FlexData"] != DBNull.Value)
                            {
                                txtFlex.Text = Convert.ToString(row["Detail_FlexData"]);
                            }                           
                            fieldValidator = new RequiredFieldValidator();
                            fieldValidator.ID = "validator_" + i;
                            fieldValidator.ControlToValidate = "txtFlex" + i;                           
                            fieldValidator.Display = ValidatorDisplay.Dynamic;
                            fieldValidator.ValidationGroup = "pax";
                            fieldValidator.SetFocusOnError = true;
                            fieldValidator.ErrorMessage = "Please Enter " + Convert.ToString(row["flexLabel"]); 
                            fieldValidator.Enabled = true;
                       
                            divFlexControl.Controls.Add(txtFlex);
                            divFlexControl.Controls.Add(lblError);
                            if (Convert.ToString(row["flexMandatoryStatus"]) == "Y")
                                divFlexControl.Controls.Add(fieldValidator);
                            break;
                        case "D"://Date

                            DropDownList ddlDay = new DropDownList();
                            ddlDay.ID = "ddlDay" + i;
                            ddlDay.Width = new Unit(30, UnitType.Percentage);
                            ddlDay.CssClass = "form-control pull-left ";
                            BindDates(ddlDay);

                            Label lbldayError = new Label();
                            lbldayError.ID = "lbldayError" + i;
                            lbldayError.Font.Bold = true;
                            lbldayError.CssClass = "red_span";

                            fieldValidator = new RequiredFieldValidator();
                            fieldValidator.ID = "dayvalidator_" + i;
                            fieldValidator.ControlToValidate = "ddlDay" + i;
                            fieldValidator.Display = ValidatorDisplay.Dynamic;
                            fieldValidator.ValidationGroup = "pax";
                            fieldValidator.SetFocusOnError = true;
                            fieldValidator.ErrorMessage = "Select Day";
                            fieldValidator.InitialValue = "-1";
                            fieldValidator.Enabled = true;

                            divFlexControl.Controls.Add(ddlDay);
                            divFlexControl.Controls.Add(lbldayError);
                            if (Convert.ToString(row["flexMandatoryStatus"]) == "Y")
                                divFlexControl.Controls.Add(fieldValidator);

                            DropDownList ddlMonth = new DropDownList();
                            ddlMonth.ID = "ddlMonth" + i;
                            ddlMonth.Width = new Unit(30, UnitType.Percentage);
                            ddlMonth.CssClass = "form-control pull-left ";
                            BindMonths(ddlMonth);

                            Label lblMonthError = new Label();
                            lblMonthError.ID = "lblMonthError" + i;
                            lblMonthError.Font.Bold = true;
                            lblMonthError.CssClass = "red_span";

                            fieldValidator = new RequiredFieldValidator();
                            fieldValidator.ID = "Monthvalidator_" + i;
                            fieldValidator.ControlToValidate = "ddlMonth" + i;
                            fieldValidator.Display = ValidatorDisplay.Dynamic;
                            fieldValidator.ValidationGroup = "pax";
                            fieldValidator.SetFocusOnError = true;
                            fieldValidator.InitialValue = "-1";
                            fieldValidator.ErrorMessage = "Select Month";
                            fieldValidator.Enabled = true;

                            divFlexControl.Controls.Add(ddlMonth);
                            if (Convert.ToString(row["flexMandatoryStatus"]) == "Y")
                                divFlexControl.Controls.Add(fieldValidator);
                            divFlexControl.Controls.Add(lblMonthError);

                            DropDownList ddlYear = new DropDownList();
                            ddlYear.ID = "ddlYear" + i;
                            ddlYear.Width = new Unit(40, UnitType.Percentage);
                            ddlYear.CssClass = "form-control pull-left ";
                            BindYears(ddlYear);

                            Label lblyearError = new Label();
                            lblyearError.ID = "lblyearError" + i;
                            lblyearError.Font.Bold = true;
                            lblyearError.CssClass = "red_span";

                            fieldValidator = new RequiredFieldValidator();
                            fieldValidator.ID = "Yearvalidator_" + i;
                            fieldValidator.ControlToValidate = "ddlYear" + i;
                            fieldValidator.Display = ValidatorDisplay.Dynamic;
                            fieldValidator.ValidationGroup = "pax";
                            fieldValidator.SetFocusOnError = true;
                            fieldValidator.InitialValue = "-1";
                            fieldValidator.ErrorMessage = "Select Year";
                            fieldValidator.Enabled = true;

                            DateTime date;
                            if (row["Detail_FlexData"] != DBNull.Value)
                            {
                                try
                                {
                                    date = Convert.ToDateTime(row["Detail_FlexData"]);
                                    ddlDay.SelectedValue = date.Day.ToString();
                                    ddlMonth.SelectedValue = date.Month.ToString();
                                    ddlYear.SelectedValue = date.Year.ToString();
                                }
                                catch { }
                            }

                            
                            
                            divFlexControl.Controls.Add(ddlYear);
                            if (Convert.ToString(row["flexMandatoryStatus"]) == "Y")
                                divFlexControl.Controls.Add(fieldValidator);
                            divFlexControl.Controls.Add(lblError);
                            break;
                        case "L"://DropDown
                            DropDownList ddlFlex = new DropDownList();
                            ddlFlex.ID = "ddlFlex" + i;
                            ddlFlex.CssClass = "form-control";
                            DataTable dt = CorporateProfile.FillDropDown(Convert.ToString(row["flexSqlQuery"]));

                            if (dt != null && dt.Rows.Count > 0)
                            {
                                ddlFlex.DataSource = dt;
                                ddlFlex.DataTextField = dt.Columns[1].ColumnName;
                                ddlFlex.DataValueField = dt.Columns[0].ColumnName;
                                ddlFlex.DataBind();
                            }
                            ddlFlex.Items.Insert(0, new ListItem("-- Select--", "-1"));
                            if (row["Detail_FlexData"] != DBNull.Value)
                            {
                                try
                                {
                                    ddlFlex.SelectedValue = Convert.ToString(row["Detail_FlexData"]);
                                }
                                catch { }
                            }

                            fieldValidator = new RequiredFieldValidator();
                            fieldValidator.ID = "validator_" + i;
                            fieldValidator.ControlToValidate = "ddlFlex" + i;
                            fieldValidator.Display = ValidatorDisplay.Dynamic;
                            fieldValidator.ValidationGroup = "pax";
                            fieldValidator.SetFocusOnError = true;
                            fieldValidator.InitialValue = "-1";
                            fieldValidator.ErrorMessage = "Please Select " + Convert.ToString(row["flexLabel"]);
                            fieldValidator.Enabled = true;

                            divFlexControl.Controls.Add(ddlFlex);
                            divFlexControl.Controls.Add(lblError);
                            if (Convert.ToString(row["flexMandatoryStatus"]) == "Y")
                                divFlexControl.Controls.Add(fieldValidator);
                            break;
                    }
                    //HtmlGenericControl divClear = new HtmlGenericControl("Div");
                    //divClear.Attributes.Add("class", "clearfix");
                    tblFlexFields.Controls.Add(divlblFlex1); //Flex Label Binding
                    tblFlexFields.Controls.Add(divFlexControl);//Flex Control Binding
                    //tblFlexFields.Controls.Add(divClear);
                    i++;
                }
            }
            imgContinue.Click += new EventHandler(imgContinue_Click);
            sessionId = Request["sessionId"];
            if (Session["hCode"] != null && Session["hCode"].ToString() != "")
            {
                index = Convert.ToInt16(Session["hCode"]);
            }

            int memberId = Convert.ToInt32(Session["memberId"]);
            if (agencyId == 0)
            {
                errorMessage = "Your Session is expired !! Please" + "<a href=\"HotelSearch.aspx\"" + "\">" + " Retry" + "</a>.";
                return;
            }

            HotelSearchResult[] hotelInfo = new HotelSearchResult[0];
            if (Session["cSessionId"] == null || Session["req"] == null || Session["hItinerary"] == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
            else
            {
                //Audit.Add(EventType.GetHotelDetails, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Get HotelItinerary Start", "0");
                try
                {

                    //if (itinerary.Source == HotelBookingSource.RezLive)
                    //{
                    //    lblRoomName.Text = itinerary.Roomtype[0].RoomName.Split('|')[0] + " - " + itinerary.Roomtype[0].RoomTypeCode;
                    //}
                    //else
                    //{
                    //    lblRoomName.Text = itinerary.Roomtype[0].RoomName;
                    //}
                    itinerary = Session["hItinerary"] as HotelItinerary;

                    // Calculation Price
                    decimal total = 0;
                    foreach (HotelRoom room in itinerary.Roomtype)
                    {
                        if (priceType == PriceType.PublishedFare)
                        {
                            total += ((room.Price.PublishedFare) * itinerary.NoOfRooms) * rateofExchange;
                        }
                        else
                        {
                            if (itinerary.Source == HotelBookingSource.DOTW)
                            {
                                total += Math.Round((room.Price.NetFare + room.Price.Markup), agency.DecimalValue) * rateofExchange;
                            }
                            else
                            {
                                total += Math.Round((room.Price.NetFare + room.Price.Markup), agency.DecimalValue) * rateofExchange;
                            }
                            discount = room.Price.Discount;
                        }
                    }

                    //total -= discount;
                    //total = Math.Ceiling(total);
                    lblCost.Text = Math.Round(total, agency.DecimalValue).ToString("N" + agency.DecimalValue.ToString());
                    //Add Markup Price entered by the user
                    if (txtMarkup.Text.Trim().Length > 0)
                    {
                        decimal _markup = 0, percent = 0, fix = 0, mtotal = Math.Ceiling(total - discount);
                        string asvType = "";
                        //If P is selected, calculate the percent amount from the total and add it.
                        //If F is selected, directly add the price to the total
                        if (rbtnPercent.Checked)
                        {
                            try
                            {
                                percent = Convert.ToDecimal(txtMarkup.Text);
                            }
                            catch { }
                            asvType = "P";
                            _markup = ((mtotal) * (percent / 100));
                            mtotal = total - discount + _markup;
                        }
                        else if (rbtnFixed.Checked)
                        {
                            try
                            {
                                fix = Convert.ToDecimal(txtMarkup.Text);
                            }
                            catch { }
                            asvType = "F";
                            _markup = fix;
                            mtotal = _markup + total;
                        }
                        //itinerary.Roomtype[0].Price.Markup = markup;
                        this.markup = _markup;
                        Session["Markup"] = _markup;
                        mtotal -= discount;
                        lblTotal.Text = Math.Ceiling(mtotal).ToString("N" + agency.DecimalValue.ToString());

                        foreach (HotelRoom room in itinerary.Roomtype)
                        {
                            if (room.Price.AccPriceType == PriceType.NetFare)
                            {
                                room.Price.AsvAmount = markup;
                                room.Price.AsvType = asvType;
                                room.Price.AsvElement = txtMarkup.Text;
                                break;//save addl markup for first room only
                            }
                        }
                    }
                    else
                    {
                        total -= discount;
                        lblTotal.Text = Math.Ceiling(total).ToString("N" + agency.DecimalValue.ToString());
                    }
                    lblPrice.Text = lblTotal.Text;
                    //if (itinerary.Source == HotelBookingSource.RezLive)
                    //{
                    //    lblRoomName.Text = itinerary.Roomtype[0].RoomName.Split('|')[0] + " - " + itinerary.Roomtype[0].RoomTypeCode;
                    //}
                    //else
                    //{
                    //    lblRoomName.Text = itinerary.Roomtype[0].RoomName;
                    //}

                    //Load additional Passenger Grid to fill details
                    List<string> pass = new List<string>();
                    List<string> childs = new List<string>();

                    for (int k = 0; k < itinerary.Roomtype.Length; k++)
                    {
                        HotelRoom room = itinerary.Roomtype[k];
                        for (int i = 1; i < room.AdultCount; i++)
                        {
                            pass.Add("Adult");
                        }

                        for (int j = 0; j < room.ChildCount; j++)
                        {
                            HotelPassenger obj = new HotelPassenger();
                            childs.Add("Child");
                        }
                    }

                    BindOtherPassengers();

                    //Audit.Add(EventType.GetHotelDetails, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Get HotelItinerary End", "0");
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.GetHotelDetails, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Get HotelItinerary. Error: " + ex.Message, "0");
                }
            }

            if (action.Value == "SendEmail")
            {
                itinerary = (HotelItinerary)Session["hItinerary"];
                request = (HotelRequest)Session["req"];
                if (itinerary != null)
                {
                    //agency.Load(agencyId);
                    string fromEmail = ConfigurationManager.AppSettings["fromEmail"].ToString();
                    string toEmail = ConfigurationManager.AppSettings["hotelToEmail"].ToString();
                    // TODO: send stack trace also in the mail
                    string messageText = "Dear  CozmoOnline,\n\n " + "Please make a Hotel Booking for the following Details.\n\n" + "City Name:" + itinerary.CityRef + "\nCheck In :" + itinerary.StartDate.ToString("dd MMM yy") + "\nCheck Out :" + itinerary.EndDate.ToString("dd MMM yy") + "\n No.of Rooms:" + itinerary.Roomtype.Length + "\n \nPassenger Information: ";
                    for (int k = 0; k < request.RoomGuest.Length; k++)
                    {
                        messageText += "\nRoom " + (k + 1) + " ( " + itinerary.Roomtype[k].RoomName + " ) :";
                        messageText += " Adults :" + request.RoomGuest[k].noOfAdults;
                        messageText += "  Children:" + request.RoomGuest[k].noOfChild;
                    }
                    messageText += "\nHotel Name: " + itinerary.HotelName;
                    messageText += "\nHotel Address:" + itinerary.HotelAddress1 + itinerary.HotelAddress2;
                    messageText += "\n Hotel Rating: " + itinerary.Rating.ToString();

                    messageText += "\n\n\nThanking you,\n";
                    messageText += agency.Name;
                    messageText += "\nPhone No: " + agency.Phone1;
                    try
                    {
                        Email.Send(fromEmail, toEmail, "Hotel Booking Request", messageText);
                        errorMessage = "Email sent sucessfully. Goto" + "<a href=\"HotelSearch.aspx\"" + "\">" + "Search Page" + "</a>.";
                    }
                    catch (System.Net.Mail.SmtpException excep)
                    {
                        errorMessage = "Unable to send Email";
                        Audit.Add(EventType.DesiyaAvailSearch, Severity.High, 0, "SMTP Exception returned from Hotel Results Page. Error Message:" + excep.Message + " | " + DateTime.Now, "");
                    }
                    loggedMember = new UserMaster(Convert.ToInt32(Session["memberId"]));

                    string IPAddr = Request.ServerVariables["REMOTE_ADDR"];
                    CT.Core.Audit.Add(EventType.DesiyaAvailSearch, Severity.High, Convert.ToInt32(loggedMember.ID), "Hotel Request Details : " + " | " + messageText + DateTime.Now + "|Agency Details:" + agency.Name + "|" + agency.Phone2, IPAddr);
                }
                else
                {
                    errorMessage = "Unable to send your mail";
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetHotelDetails, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to Add Guest Details. Error: " + ex.Message, "0");

        }
    }

    void BindOtherPassengers()
    {

        for (int j = 0; j < itinerary.Roomtype.Length; j++)
        {
            int i = 0;
            if (j == 0)
            {
                i = 1;
            }
            else
            {
                i = 0;
            }

            int pax = 0;

            if (itinerary.Roomtype[j].AdultCount > 0)
            {
                pax = itinerary.Roomtype[j].AdultCount;
            }
            else
            {
                pax = itinerary.Roomtype[j].ChildCount;
                i = 0;
            }
            //New Design purpose chmaged by brahmam 27.06.2016
            for (; i < itinerary.Roomtype[j].AdultCount; i++)
            {
                HtmlTableRow hRow = new HtmlTableRow();
                HtmlTableCell hCell = new HtmlTableCell();
                if (i == 0)
                {
                    if (itinerary.Roomtype[j].AdultCount > 0)
                    {
                        if (itinerary.Source == HotelBookingSource.RezLive)
                        {
                            hCell.InnerHtml = "<b class=''>" + itinerary.Roomtype[j].RoomName.Split('|')[0] + " - " + itinerary.Roomtype[j].RoomTypeCode + ":" + " Adult " + (i + 1).ToString() + "</b>" + " (Lead Guest)";
                        }
                        else
                        {
                            hCell.InnerHtml = "<b class=''>" + itinerary.Roomtype[j].RoomName + ":" + " Adult " + (i + 1).ToString() + "</b>" + " (Lead Guest)";
                        }
                    }
                    else
                    {
                        if (itinerary.Source == HotelBookingSource.RezLive)
                        {
                            hCell.InnerText = itinerary.Roomtype[j].RoomName.Split('|')[0] + " - " + itinerary.Roomtype[j].RoomTypeCode + ": " + " Child " + (i + 1).ToString();
                        }
                        else
                        {
                            hCell.InnerText = itinerary.Roomtype[j].RoomName + ": " + " Child " + (i + 1).ToString();
                        }
                    }
                    hCell.Attributes.Add("class", "subgray-header marbot_10 center-block pt-2");
                }
                else
                {

                    if (itinerary.Roomtype[j].AdultCount > 0)
                    {
                        hCell.InnerHtml += "<b class=''>" + " Adult " + (i + 1).ToString() + "</b>";
                    }
                    else
                    {
                        hCell.InnerText = " Child " + (i + 1).ToString();
                    }
                    // hCell.InnerHtml += "<hr  />";
                }
                hCell.ColSpan = 8;

                hRow.Cells.Add(hCell);

                HtmlTableRow detailRow = new HtmlTableRow();

                HtmlTableCell prefix = new HtmlTableCell();
                prefix.Attributes.Add("class", "row");
                HtmlGenericControl divlblTitle = new HtmlGenericControl("Div");
                divlblTitle.Attributes.Add("class", "col-md-3");
                divlblTitle.InnerHtml = "<div class='label'>Title:</div>";

                HtmlGenericControl divDdlTitle = new HtmlGenericControl("Div"); // For Adult Div
                divDdlTitle.Attributes.Add("class", "col-md-3"); // For Adult Div class attribute

                DropDownList ddlTitle = new DropDownList();
                ddlTitle.Items.Add(new ListItem("Select", "Select"));
                ddlTitle.Items.Add(new ListItem("Mr.", "Mr."));
                ddlTitle.Items.Add(new ListItem("Ms.", "Ms."));
                // ddlTitle.Items.Add(new ListItem("Dr.", "Dr.")); //Commented By Chandan on 09012016
                ddlTitle.SelectedIndex = 0;
                ddlTitle.ID = "ddlATitle" + j + i.ToString();
                ddlTitle.CssClass = "form-control";
                ddlTitle.CausesValidation = true;

                ddlTitle.ValidationGroup = "pax";
                divDdlTitle.Controls.Add(ddlTitle);

                RequiredFieldValidator prefixvc = new RequiredFieldValidator();
                prefixvc.ControlToValidate = ddlTitle.ID;
                prefixvc.ErrorMessage = "Select Title";
                prefixvc.Display = ValidatorDisplay.Dynamic;
                prefixvc.ValidationGroup = "pax";
                prefixvc.InitialValue = "Select";
                prefixvc.SetFocusOnError = true;
                divDdlTitle.Controls.Add(prefixvc);

                prefix.Controls.Add(divlblTitle);
                prefix.Controls.Add(divDdlTitle);

                //Adult First Name
                HtmlGenericControl divlblfName = new HtmlGenericControl("Div"); // For Adult First Name
                divlblfName.Attributes.Add("class", "col-md-3");
                divlblfName.InnerHtml = "<div  class='label' >First Name:</Div>";

                HtmlGenericControl divTxtfName = new HtmlGenericControl("Div"); // For Adult First Name
                divTxtfName.Attributes.Add("class", "col-md-3");

                TextBox txtFirstName = new TextBox();
                txtFirstName.ID = "txtAFirstName" + j + i.ToString();
                txtFirstName.CssClass = "form-control";
                txtFirstName.CausesValidation = true;
                txtFirstName.MaxLength = 20;
                txtFirstName.ValidationGroup = "pax";
                txtFirstName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                txtFirstName.Attributes.Add("ondrop", "return false;");
                //txtFirstName.Attributes.Add("onpaste", "return false;");
                divTxtfName.Controls.Add(txtFirstName);


                RequiredFieldValidator fnamevc = new RequiredFieldValidator();
                fnamevc.ControlToValidate = txtFirstName.ID;
                fnamevc.ErrorMessage = "Enter First Name";
                fnamevc.Display = ValidatorDisplay.Dynamic;
                fnamevc.InitialValue = "";
                fnamevc.SetFocusOnError = true;
                fnamevc.ValidationGroup = "pax";
                divTxtfName.Controls.Add(fnamevc);

                CustomValidator fnameValidator = new CustomValidator();
                fnameValidator.ControlToValidate = txtFirstName.ID;
                fnameValidator.ClientValidationFunction = "validateName";
                fnameValidator.Display = ValidatorDisplay.Dynamic;
                fnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                fnameValidator.SetFocusOnError = true;
                fnameValidator.ValidationGroup = "pax";
                divTxtfName.Controls.Add(fnameValidator);

                prefix.Controls.Add(divlblfName);
                prefix.Controls.Add(divTxtfName);

                //Adult Last Name
                HtmlGenericControl divlbllName = new HtmlGenericControl("Div"); // For Adult Last Name
                divlbllName.Attributes.Add("class", "col-md-3");
                divlbllName.InnerHtml = "<div  class='label' >Last Name:</Div>";

                HtmlGenericControl divtxtlName = new HtmlGenericControl("Div"); // For Adult Last Name
                divtxtlName.Attributes.Add("class", "col-md-3");

                TextBox txtLastName = new TextBox();
                txtLastName.ID = "txtALastName" + j + i.ToString();
                txtLastName.CssClass = "form-control";
                txtLastName.CausesValidation = true;
                txtLastName.MaxLength = 20;
                txtLastName.ValidationGroup = "pax";
                txtLastName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                txtLastName.Attributes.Add("ondrop", "return false;");
                //txtLastName.Attributes.Add("onpaste", "return false;");

                divtxtlName.Controls.Add(txtLastName);


                RequiredFieldValidator lnamevc = new RequiredFieldValidator();
                lnamevc.ControlToValidate = txtLastName.ID;
                lnamevc.ErrorMessage = "Enter Last Name";
                lnamevc.Display = ValidatorDisplay.Dynamic;
                lnamevc.InitialValue = "";
                lnamevc.SetFocusOnError = true;
                lnamevc.ValidationGroup = "pax";
                divtxtlName.Controls.Add(lnamevc);

                CustomValidator lnameValidator = new CustomValidator();
                lnameValidator.ControlToValidate = txtLastName.ID;
                lnameValidator.ClientValidationFunction = "validateName";
                lnameValidator.Display = ValidatorDisplay.Dynamic;
                lnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                lnameValidator.SetFocusOnError = true;
                lnameValidator.ValidationGroup = "pax";
                divtxtlName.Controls.Add(lnameValidator);

                prefix.Controls.Add(divlbllName);
                prefix.Controls.Add(divtxtlName);

                HiddenField hdfPaxType = new HiddenField();
                hdfPaxType.ID = "hdfAPaxType" + j + i.ToString();
                hdfPaxType.Value = "A";

                prefix.Controls.Add(hdfPaxType);
                detailRow.Cells.Add(prefix);
                tblAdults.Rows.Add(hRow);
                tblAdults.Rows.Add(detailRow);



                HtmlTableRow addnlrow = new HtmlTableRow();
                addnlrow.ID = "addnlrow" + j + i.ToString();

                HtmlTableCell addnlCell = new HtmlTableCell();

                //Department
                HtmlGenericControl divlbldepartment = new HtmlGenericControl("Div");
                divlbldepartment.Attributes.Add("class", "col-md-3");
                divlbldepartment.InnerHtml = "<div class='label'>Department:*</div>";

                HtmlGenericControl divddlDepartment = new HtmlGenericControl("Div"); // For Department Div
                divddlDepartment.Attributes.Add("class", "col-md-3"); // For Department Div class attribute

                DropDownList ddlDepartment = new DropDownList();
                //ddlDepartment.Width = new Unit(100, UnitType.Pixel);
                ddlDepartment.CssClass = "form-control";

                ddlDepartment.ID = "ddlDepartment" + j + i.ToString();
                ddlDepartment.CausesValidation = true;
                ddlDepartment.ValidationGroup = "pax";
                ddlDepartment.DataSource = dtDept;
                ddlDepartment.DataTextField = "dept_name";
                ddlDepartment.DataValueField = "dept_id";
                ddlDepartment.DataBind();
                ddlDepartment.Items.Insert(0, new ListItem("Select Department", "-1"));
                ddlDepartment.Attributes.Add("onchange", "AssignValue(this.id,'A');");
                divddlDepartment.Controls.Add(ddlDepartment);

                RequiredFieldValidator rfvDept = new RequiredFieldValidator();
                rfvDept.ControlToValidate = ddlDepartment.ID;
                rfvDept.ErrorMessage = "Enter Department Name";
                rfvDept.Display = ValidatorDisplay.Dynamic;
                rfvDept.ValidationGroup = "pax";
                rfvDept.InitialValue = "-1";
                rfvDept.SetFocusOnError = true;
                divddlDepartment.Controls.Add(rfvDept);



                addnlCell.Controls.Add(divlbldepartment);
                addnlCell.Controls.Add(divddlDepartment);


                HiddenField hdfDept = new HiddenField();
                hdfDept.ID = "hdfADept" + j + i.ToString();
                addnlCell.Controls.Add(hdfDept);

                //Employee

                HtmlGenericControl divlblemployee = new HtmlGenericControl("Div");
                divlblemployee.Attributes.Add("class", "col-md-3");
                divlblemployee.InnerHtml = "<div class='label'>Employee:*</div>";

                HtmlGenericControl divtxtemployee = new HtmlGenericControl("Div"); // For Employee Div
                divtxtemployee.Attributes.Add("class", "col-md-3"); // For Employee Div class attribute

                TextBox txtEmployee = new TextBox();
                txtEmployee.CssClass = "form-control";
                txtEmployee.ID = "txtEmployee" + j + i.ToString();
                txtEmployee.CausesValidation = true;
                txtEmployee.MaxLength = 50;
                txtEmployee.ValidationGroup = "pax";
                txtEmployee.Attributes.Add("ondrop", "return false;");
                //txtEmployee.Attributes.Add("onpaste", "return false;");
                divtxtemployee.Controls.Add(txtEmployee);

                RequiredFieldValidator rfvEmp = new RequiredFieldValidator();
                rfvEmp.ControlToValidate = txtEmployee.ID;
                rfvEmp.ErrorMessage = "Enter Employee Name";
                rfvEmp.Display = ValidatorDisplay.Dynamic;
                rfvEmp.ValidationGroup = "pax";
                rfvEmp.InitialValue = "";
                rfvEmp.SetFocusOnError = true;
                divtxtemployee.Controls.Add(rfvEmp);

                addnlCell.Controls.Add(divlblemployee);
                addnlCell.Controls.Add(divtxtemployee);

                //Employee ID
                HtmlGenericControl divlblemployeeID = new HtmlGenericControl("Div");
                divlblemployeeID.Attributes.Add("class", "col-md-3");
                divlblemployeeID.InnerHtml = "<div class='label'>Employee ID:*</div>";

                HtmlGenericControl divtxtemployeeID = new HtmlGenericControl("Div"); // For employeeID Div
                divtxtemployeeID.Attributes.Add("class", "col-md-3"); // For employeeID Div class attribute

                TextBox txtEmployeeID = new TextBox();
                txtEmployeeID.ID = "txtEmployeeID" + j + i.ToString();

                txtEmployeeID.CssClass = "form-control";

                txtEmployeeID.CausesValidation = true;
                txtEmployeeID.MaxLength = 15;
                txtEmployeeID.ValidationGroup = "pax";
                txtEmployeeID.Attributes.Add("ondrop", "return false;");
                //txtEmployeeID.Attributes.Add("onpaste", "return false;");
                divtxtemployeeID.Controls.Add(txtEmployeeID);

                RequiredFieldValidator rfvEmpID = new RequiredFieldValidator();
                rfvEmpID.ControlToValidate = txtEmployeeID.ID;
                rfvEmpID.ErrorMessage = "Enter Employee ID";
                rfvEmpID.Display = ValidatorDisplay.Dynamic;
                rfvEmpID.ValidationGroup = "pax";
                rfvEmpID.InitialValue = "";
                rfvEmpID.SetFocusOnError = true;
                divtxtemployeeID.Controls.Add(rfvEmpID);


                addnlCell.Controls.Add(divlblemployeeID);
                addnlCell.Controls.Add(divtxtemployeeID);

                addnlrow.Cells.Add(addnlCell);
                tblAdults.Rows.Add(addnlrow);


                HtmlTableRow addnlrow1 = new HtmlTableRow();
                addnlrow.ID = "addnlrow1" + j + i.ToString();

                HtmlTableCell addnlCell1 = new HtmlTableCell();
                //Division
                HtmlGenericControl divlblDivision = new HtmlGenericControl("Div");
                divlblDivision.Attributes.Add("class", "col-md-3");
                divlblDivision.InnerHtml = "<div class='label'>Division:*</div>";

                HtmlGenericControl divtxtDivision = new HtmlGenericControl("Div"); // For Division Div
                divtxtDivision.Attributes.Add("class", "col-md-3"); // For Division Div class attribute

                TextBox txtDivision = new TextBox();
                txtDivision.CssClass = "form-control";
                txtDivision.ID = "txtDivision" + j + i.ToString();
                txtDivision.CausesValidation = true;
                txtDivision.MaxLength = 50;
                txtDivision.ValidationGroup = "pax";
                txtDivision.Attributes.Add("ondrop", "return false;");
                //txtDivision.Attributes.Add("onpaste", "return false;");
                divtxtDivision.Controls.Add(txtDivision);

                RequiredFieldValidator rfvDivision = new RequiredFieldValidator();
                rfvDivision.ControlToValidate = txtDivision.ID;
                rfvDivision.ErrorMessage = "Enter Division Name";
                rfvDivision.Display = ValidatorDisplay.Dynamic;
                rfvDivision.ValidationGroup = "pax";
                rfvDivision.InitialValue = "";
                rfvDivision.SetFocusOnError = true;
                divtxtDivision.Controls.Add(rfvDivision);

                addnlCell1.Controls.Add(divlblDivision);
                addnlCell1.Controls.Add(divtxtDivision);


                //Purpose of Travel
                HtmlGenericControl divlblPurpose = new HtmlGenericControl("Div");
                divlblPurpose.Attributes.Add("class", "col-md-3");
                divlblPurpose.InnerHtml = "<div class='label'>Purpose of Travel:*</div>";

                HtmlGenericControl divtxtPurpose = new HtmlGenericControl("Div"); // For Purpose of Travel Div
                divtxtPurpose.Attributes.Add("class", "col-md-3"); // For Purpose of Travel Div class attribute


                TextBox txtPurpose = new TextBox();
                txtPurpose.CssClass = "form-control";

                txtPurpose.ID = "txtPurpose" + j + i.ToString();
                txtPurpose.CausesValidation = true;
                txtPurpose.MaxLength = 250;
                txtPurpose.TextMode = TextBoxMode.MultiLine;
                txtPurpose.ValidationGroup = "pax";
                txtPurpose.Attributes.Add("onkeypress", "return (this.value.length < 250);");
                txtPurpose.Attributes.Add("ondrop", "return false;");
                //txtPurpose.Attributes.Add("onpaste", "return false;");
                divtxtPurpose.Controls.Add(txtPurpose);


                RequiredFieldValidator rfvPurpose = new RequiredFieldValidator();
                rfvPurpose.ControlToValidate = txtPurpose.ID;
                rfvPurpose.ErrorMessage = "Enter Purpose of travel";
                rfvPurpose.Display = ValidatorDisplay.Dynamic;
                rfvPurpose.ValidationGroup = "pax";
                rfvPurpose.InitialValue = "";
                rfvPurpose.SetFocusOnError = true;
                divtxtPurpose.Controls.Add(rfvPurpose);

                addnlCell1.Controls.Add(divlblPurpose);
                addnlCell1.Controls.Add(divtxtPurpose);

                addnlrow1.Cells.Add(addnlCell1);
                tblAdults.Rows.Add(addnlrow1);

                if (agency.AddnlPaxDetails == "N")
                {
                    addnlrow.Visible = false;
                    addnlrow1.Visible = false;
                }
                else
                {
                    addnlrow.Visible = true;
                    addnlrow1.Visible = true;
                }
            }

            i = 0;

            //New design purpose changed by by brahmam 27.06.2016
            for (; i < itinerary.Roomtype[j].ChildCount; i++)
            {
                HtmlTableRow hRow = new HtmlTableRow();
                HtmlTableCell hCell = new HtmlTableCell();
                hCell.InnerHtml += "<b class=''>" + " Child " + (i + 1).ToString() + "</b>";
                hCell.ColSpan = 8;
                hCell.Attributes.Add("class", "b_bot_1");
                hRow.Cells.Add(hCell);
                tblAdults.Rows.Add(hRow);

                HtmlTableRow detailRow = new HtmlTableRow();
                HtmlTableCell prefix = new HtmlTableCell();

                HtmlGenericControl divlblTitle = new HtmlGenericControl("Div");// For Child Div
                divlblTitle.Attributes.Add("class", "col-md-3");
                divlblTitle.InnerHtml = "<div class='label'>Title:</div>";

                HtmlGenericControl divDdlTitle = new HtmlGenericControl("Div"); // For Child Div
                divDdlTitle.Attributes.Add("class", "col-md-3"); // For Adult Div class attribute
                DropDownList ddlTitle = new DropDownList();
                ddlTitle.Items.Add(new ListItem("Select", "Select"));
                if(itinerary.Source!= HotelBookingSource.GRN)
                {
                    ddlTitle.Items.Add(new ListItem("Master", "Mstr"));
                }              
                ddlTitle.Items.Add(new ListItem("Mr.", "Mr."));
                ddlTitle.Items.Add(new ListItem("Ms.", "Ms."));

                ddlTitle.CssClass = "form-control";
                ddlTitle.SelectedIndex = 0;
                ddlTitle.ID = "ddlTitle" + j + i.ToString();
                ddlTitle.CausesValidation = true;
                ddlTitle.ValidationGroup = "pax";
                divDdlTitle.Controls.Add(ddlTitle);

                RequiredFieldValidator prefixvc = new RequiredFieldValidator();
                prefixvc.ControlToValidate = ddlTitle.ID;
                prefixvc.ErrorMessage = "Select Title";
                prefixvc.Display = ValidatorDisplay.Dynamic;
                prefixvc.ValidationGroup = "pax";
                prefixvc.InitialValue = "Select";
                prefixvc.SetFocusOnError = true;
                divDdlTitle.Controls.Add(prefixvc);

                prefix.Controls.Add(divlblTitle);
                prefix.Controls.Add(divDdlTitle);

                //Child First Name   modified by brahmam 27.06.2016
                HtmlGenericControl divlblfName = new HtmlGenericControl("Div"); // For Child First Name
                divlblfName.Attributes.Add("class", "col-md-3");
                divlblfName.InnerHtml = "<div  class='label' >First Name:</Div>";

                HtmlGenericControl divTxtfName = new HtmlGenericControl("Div"); // For Child First Name
                divTxtfName.Attributes.Add("class", "col-md-3");

                TextBox txtFirstName = new TextBox();
                txtFirstName.ID = "txtFirstName" + j + i.ToString();


                txtFirstName.CssClass = "form-control";
                txtFirstName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                txtFirstName.Attributes.Add("ondrop", "return false;");
                //txtFirstName.Attributes.Add("onpaste", "return false;");
                txtFirstName.MaxLength = 20;
                divTxtfName.Controls.Add(txtFirstName);

                RequiredFieldValidator fnamevc = new RequiredFieldValidator();
                fnamevc.ControlToValidate = txtFirstName.ID;
                fnamevc.ErrorMessage = "Enter First Name";
                fnamevc.InitialValue = "";
                fnamevc.Display = ValidatorDisplay.Dynamic;
                fnamevc.SetFocusOnError = true;
                fnamevc.ValidationGroup = "pax";
                divTxtfName.Controls.Add(fnamevc);

                CustomValidator fnameValidator = new CustomValidator();
                fnameValidator.ControlToValidate = txtFirstName.ID;
                fnameValidator.ClientValidationFunction = "validateName";
                fnameValidator.Display = ValidatorDisplay.Dynamic;
                fnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                fnameValidator.SetFocusOnError = true;
                fnameValidator.ValidationGroup = "pax";
                divTxtfName.Controls.Add(fnameValidator);

                prefix.Controls.Add(divlblfName);
                prefix.Controls.Add(divTxtfName);

                //Last Name
                HtmlGenericControl divlbllName = new HtmlGenericControl("Div"); // For Child Last Name
                divlbllName.Attributes.Add("class", "col-md-3");
                divlbllName.InnerHtml = "<div  class='label' >Last Name:</Div>";

                HtmlGenericControl divtxtlName = new HtmlGenericControl("Div"); // For Child Last Name
                divtxtlName.Attributes.Add("class", "col-md-3");

                TextBox txtLastName = new TextBox();


                txtLastName.ID = "txtLastName" + j + i.ToString();

                txtLastName.CssClass = "form-control";


                txtLastName.Attributes.Add("onkeypress", "return IsAlphaNumeric(event);");
                txtLastName.Attributes.Add("ondrop", "return false;");
                //txtLastName.Attributes.Add("onpaste", "return false;");
                txtLastName.MaxLength = 20;
                divtxtlName.Controls.Add(txtLastName);

                RequiredFieldValidator lnamevc = new RequiredFieldValidator();
                lnamevc.ControlToValidate = txtLastName.ID;
                lnamevc.ErrorMessage = "Enter Last Name";
                lnamevc.Display = ValidatorDisplay.Dynamic;
                lnamevc.InitialValue = "";
                lnamevc.SetFocusOnError = true;
                lnamevc.ValidationGroup = "pax";
                divtxtlName.Controls.Add(lnamevc);

                CustomValidator lnameValidator = new CustomValidator();
                lnameValidator.ControlToValidate = txtLastName.ID;
                lnameValidator.ClientValidationFunction = "validateName";
                lnameValidator.Display = ValidatorDisplay.Dynamic;
                lnameValidator.ErrorMessage = "Minimum 2 alphabets required for First Name";
                lnameValidator.SetFocusOnError = true;
                lnameValidator.ValidationGroup = "pax";
                divtxtlName.Controls.Add(lnameValidator);

                prefix.Controls.Add(divlbllName);
                prefix.Controls.Add(divtxtlName);

                HiddenField hdfPaxType = new HiddenField();
                hdfPaxType.ID = "hdfPaxType" + j + i.ToString();
                hdfPaxType.Value = "C";
                prefix.Controls.Add(hdfPaxType);

                detailRow.Cells.Add(prefix);
                tblAdults.Rows.Add(detailRow);
            }
        }


    }

    /// <summary>
    /// 
    /// </summary>
    void LoadHotelItinerary()
    {
        HotelSearchResult[] hotelInfo = new HotelSearchResult[0];
        if (Session["cSessionId"] == null || Session["req"] == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
        else
        {
            sessionId = Session["cSessionId"].ToString();
            Dictionary<string, string> filterCriteria = (Dictionary<string, string>)Session["hFilterCriteria"];
            HotelSearchResult hData = new HotelSearchResult();
            int noOfPages = 1;
            hotelInfo = hData.GetFilteredResult(sessionId, filterCriteria, ref noOfPages);
            if (hotelInfo == null || hotelInfo.Length == 0)
            {
                errorMessage = "Your Session is expired !! Please" + "<a href=\"HotelSearch.aspx\"" + "\">" + " Retry" + "</a>.";
                return;
            }
            request = (HotelRequest)Session["req"];
            //in case of IAN There is no way to select different room types for multi room, its like Desiya
            isMultiRoom = (hotelInfo[index].BookingSource == HotelBookingSource.IAN) ? false : request.IsMultiRoom;
            Session["isMultiRoom"] = isMultiRoom;
            if (hotelInfo[index].BookingSource == HotelBookingSource.IAN)
            {
                hotelInfo[index].RoomDetails = (HotelRoomsDetails[])Session["RoomAvailability"];
            }

            itinerary.EndDate = hotelInfo[index].EndDate;
            itinerary.StartDate = hotelInfo[index].StartDate;
            itinerary.HotelCode = hotelInfo[index].HotelCode;
            itinerary.HotelAddress1 = hotelInfo[index].HotelAddress;
            itinerary.HotelAddress2 = hotelInfo[index].HotelContactNo;
            itinerary.HotelName = hotelInfo[index].HotelName;
            itinerary.NoOfRooms = request.NoOfRooms;
            itinerary.Rating = hotelInfo[index].Rating;
            itinerary.CityRef = request.CityName;
            itinerary.Map = hotelInfo[index].HotelMap;
            itinerary.CityCode = hotelInfo[index].CityCode;
            itinerary.HotelPolicyDetails = string.Empty;
            //update request city code for the selected booking source
            request.CityCode = hotelInfo[index].CityCode;
            itinerary.Source = hotelInfo[index].BookingSource;
            itinerary.IsDomestic = false;
            //if (hotelInfo[index].Currency == "" + ConfigurationSystem.LocaleConfig["CurrencyCode"] + "")
            //{
            //    itinerary.IsDomestic = true;
            //}
            //else
            //{
            //    itinerary.IsDomestic = false;
            //}
            //Get Hotel source commission type                
            hSource.Load(hotelInfo[index].BookingSource.ToString());

            //since there are two enums of similar type where one is in BookingEngine(PriceAccounts) & other in CoreLogic(Enumerator).
            if (hSource.FareTypeId == CT.Core.FareType.Net)
            {
                priceType = PriceType.NetFare;
            }
            else
            {
                priceType = PriceType.PublishedFare;
            }


            //setting room info into itinerary
            HotelRoom[] roomInfo = new HotelRoom[itinerary.NoOfRooms];
            if (isMultiRoom && itinerary.Source != HotelBookingSource.Desiya)
            {
                #region block for multiple room booking

                roomInfo = LoadMultiRoomDetails(ref hotelInfo);
                #endregion

            }
            else
            {
                #region block for single room booking

                roomInfo = LoadRoomDetails(ref hotelInfo);
                #endregion
            }

            itinerary.Roomtype = roomInfo;

            //Add day wise subagent commission for B2B2B pricing
            //to do
            if (Convert.ToBoolean(Session["isB2B2BAgent"]) && hSource.CommissionTypeId == CT.Core.CommissionType.Percentage)
            {
                roomInfo = MetaSearchEngine.GetSubAgentHotelPrice(roomInfo, request);
            }

            if (itinerary.Source == HotelBookingSource.IAN || itinerary.Source == HotelBookingSource.Desiya)
            {
                rateofExchange = 1;
            }
            else if (roomInfo != null && roomInfo.Length > 0 && rateOfExList != null && rateOfExList.ContainsKey(roomInfo[0].Price.Currency))
            {
                rateofExchange = rateOfExList[roomInfo[0].Price.Currency];
            }
            else
            {
                rateofExchange = 1;
            }


            //CALL MSE METHODS TO GET CANCELLATION DETAILS & HOTEL DETAILS

            try
            {
                LoadCancellationDetails(ref itinerary);

                //LoadHotelDetails(ref itinerary);
            }
            catch (Exception ex)
            {
                errorMessage = ex.Message;
            }
            // for hotel check-in check-out information - End
            Session["hItinerary"] = itinerary;
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="hotelInfo"></param>
    /// <returns></returns>
    protected HotelRoom[] LoadMultiRoomDetails(ref HotelSearchResult[] hotelInfo)
    {
        HotelRoom[] roomInfo = new HotelRoom[itinerary.NoOfRooms];
        int z = 0;//index for each hotel room
        try
        {
            string[] rCodes = null;//Request["rCode"].Split(',');

            if (Request.QueryString["rCode"] != null)
            {
                if (itinerary.Source != HotelBookingSource.HotelBeds)  //Modified by brahmam 26.09.2014
                {
                    rCodes = Request.QueryString["rCode"].Split(',');
                    Session["rCode"] = Request.QueryString["rCode"];
                }
                else
                {
                    rCodes = Request.QueryString["rCode"].Replace(" ", "+").Split(',');
                    Session["rCode"] = Request.QueryString["rCode"].Replace(" ", "+");
                }
            }
            else
            {
                rCodes = Session["rCode"].ToString().Split(',');
            }

            for (int iR = 0; iR < request.NoOfRooms; iR++)
            {
                for (int j = 0; j < hotelInfo[index].RoomDetails.Length; j++)
                {
                    string rCode = "";

                    if (request.NoOfRooms > rCodes.Length)
                    {
                        rCode = rCodes[0];
                    }
                    else
                    {
                        rCode = rCodes[iR];
                    }

                    if (hotelInfo[index].BookingSource != HotelBookingSource.RezLive)
                    {
                        if (hotelInfo[index].RoomDetails[j].RoomTypeCode.Equals(rCode))
                        {
                            HotelRoom roomdata = new HotelRoom();
                            if (itinerary.Source == HotelBookingSource.GTA)
                            {
                                string[] roomList = hotelInfo[index].RoomDetails[j].RoomTypeCode.Split('|');
                                roomdata.RoomTypeCode = roomList[0];
                                roomdata.NoOfCots = Convert.ToInt16(roomList[1]);
                                roomdata.ExtraBed = Convert.ToBoolean(Convert.ToInt16(roomList[2]));
                            }
                            else if (itinerary.Source == HotelBookingSource.TBOConnect)
                            {
                                string[] roomList = hotelInfo[index].RoomDetails[j].RoomTypeCode.Split('|');
                                roomdata.RoomId = Convert.ToInt32(roomList[0]);
                                roomdata.RoomTypeCode = roomList[1];
                            }
                            else
                            {
                                roomdata.RoomTypeCode = hotelInfo[index].RoomDetails[j].RoomTypeCode;
                            }
                            roomdata.RoomName = hotelInfo[index].RoomDetails[j].RoomTypeName;
                            roomdata.RatePlanCode = hotelInfo[index].RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = hotelInfo[index].RoomDetails[j].Amenities;
                            roomdata.ExtraGuestCharge = hotelInfo[index].RoomDetails[j].SellExtraGuestCharges;
                            roomdata.ChildCharge = hotelInfo[index].RoomDetails[j].ChildCharges;
                            HotelRoomFareBreakDown[] fareInfo;
                            if (itinerary.Source != HotelBookingSource.RezLive)
                            {
                                fareInfo = new HotelRoomFareBreakDown[hotelInfo[index].RoomDetails[j].Rates.Length];
                                for (int k = 0; k < hotelInfo[index].RoomDetails[j].Rates.Length; k++)
                                {
                                    HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                    fare.Date = hotelInfo[index].RoomDetails[j].Rates[k].Days;
                                    if (itinerary.Source != HotelBookingSource.RezLive)
                                    {
                                        fare.RoomPrice = hotelInfo[index].RoomDetails[j].Rates[k].SellingFare;
                                    }

                                    fareInfo[k] = fare;
                                }
                                roomdata.RoomFareBreakDown = fareInfo;
                            }
                            else
                            {
                                fareInfo = new HotelRoomFareBreakDown[hotelInfo[index].RoomDetails[j].Rates.Length];
                                for (int k = 0; k < hotelInfo[index].RoomDetails[j].Rates.Length; k++)
                                {
                                    if (k == 0)
                                    {
                                        HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                        fare.Date = hotelInfo[index].RoomDetails[j].Rates[k].Days;

                                        fare.RoomPrice = hotelInfo[index].RoomDetails[j].Rates[k].Totalfare;
                                        if (hotelInfo[index].Currency != "AED")
                                        {
                                            fare.RoomPrice = fare.RoomPrice * rateOfExList[hotelInfo[index].Currency];
                                        }
                                        fareInfo[k] = fare;
                                    }
                                }
                                roomdata.RoomFareBreakDown = fareInfo;


                            }
                            //room guest for each room

                            {
                                roomdata.AdultCount = request.RoomGuest[iR].noOfAdults;
                                roomdata.ChildCount = request.RoomGuest[iR].noOfChild;
                                roomdata.ChildAge = request.RoomGuest[iR].childAge;
                            }
                            roomdata.NoOfUnits = "1";//for each room there will be only one unit
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;
                            if (itinerary.Source == HotelBookingSource.DOTW)
                            {
                                roomdata.Price = AccountingEngine.GetPrice(hotelInfo[index], hotelInfo[index].RoomDetails[j].RoomTypeCode, agencyId, 0, 1, nights, priceType);
                            }
                            else
                            {
                                roomdata.Price = new PriceAccounts();
                                decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                                for (int m = j; m < hotelInfo[index].RoomDetails.Length; m++)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[hotelInfo[index].RoomDetails[m].Rates.Length];
                                    for (int k = 0; k < hotelInfo[index].RoomDetails[m].Rates.Length; k++)
                                    {
                                        if (itinerary.Source == HotelBookingSource.HotelConnect)
                                        {
                                            grossFare += hotelInfo[index].RoomDetails[m].Rates[k].SellingFare;
                                        }
                                        else if (itinerary.Source == HotelBookingSource.RezLive)
                                        {
                                            grossFare += hotelInfo[index].RoomDetails[m].Rates[k].Totalfare;
                                        }
                                    }
                                    totalExtraGuestCharge = hotelInfo[index].RoomDetails[m].SellExtraGuestCharges;
                                    totalChildCharge = hotelInfo[index].RoomDetails[m].ChildCharges;
                                    totalTax = hotelInfo[index].RoomDetails[m].TotalTax;
                                    break;

                                }
                                if (hotelInfo[index].Currency != "AED")
                                {
                                    roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * rateOfExList[hotelInfo[index].Currency];
                                }
                                else
                                {
                                    roomdata.Price.NetFare = grossFare + totalExtraGuestCharge + totalChildCharge;
                                }
                                roomdata.Price.Tax = totalTax;
                                //roomdata.Price.SeviceTax = AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                                decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * hotelInfo[index].Price.AgentCommission / 100;
                                roomdata.Price.PublishedFare += agentComm;
                                roomdata.Price.AgentCommission = agentComm;
                            }
                            roomdata.Price.AccPriceType = priceType;
                            roomdata.Price.Discount = hotelInfo[index].RoomDetails[j].Discount;
                            roomdata.Price.CurrencyCode = hotelInfo[index].Currency;
                            if (rateOfExList.ContainsKey(hotelInfo[index].Currency))
                            {
                                roomdata.Price.RateOfExchange = rateOfExList[hotelInfo[index].Currency];
                            }
                            else
                            {
                                roomdata.Price.RateOfExchange = 1;
                            }
                            roomdata.Price.Markup = roomdata.Price.Markup;
                            roomdata.Price.Currency = hotelInfo[index].Currency;
                            roomInfo[z++] = roomdata;
                            break;
                        }
                    }
                    else
                    {
                        j = Convert.ToInt32(rCode);
                        decimal price = 0;
                        //CT.Core.StaticData staticInfo = new CT.Core.StaticData();
                        //Dictionary<string, decimal> rateOfExchange = staticInfo.CurrencyROE;
                        //if (hotelInfo[index].Currency == "USD")
                        //{
                        //    price = hotelInfo[index].RoomDetails[j].SellingFare * (decimal)rateOfExchange[hotelInfo[index].Currency];
                        //}
                        //else
                        {
                            price = hotelInfo[index].RoomDetails[j].SellingFare;
                        }
                        if (hotelInfo[index].RoomDetails[j].RoomTypeName.Contains("|"))
                        {
                            string[] names = hotelInfo[index].RoomDetails[j].RoomTypeName.Split('|');
                            price = hotelInfo[index].RoomDetails[j].TotalPrice / names.Length;
                        }
                        price = Math.Round(price, 2);
                        rCode = Math.Round(Convert.ToDecimal(rCode), 2).ToString();
                        //For RezLive update based on room index
                        if (hotelInfo[index].RoomDetails[j].RoomTypeName != null)
                        {
                            HotelRoom roomdata = new HotelRoom();

                            {
                                roomdata.RoomTypeCode = hotelInfo[index].RoomDetails[j].RoomTypeCode;
                            }
                            roomdata.RoomName = hotelInfo[index].RoomDetails[j].RoomTypeName;
                            roomdata.RatePlanCode = hotelInfo[index].RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = hotelInfo[index].RoomDetails[j].Amenities;
                            roomdata.ExtraGuestCharge = hotelInfo[index].RoomDetails[j].SellExtraGuestCharges;
                            roomdata.ChildCharge = hotelInfo[index].RoomDetails[j].ChildCharges;
                            HotelRoomFareBreakDown[] fareInfo;
                            if (itinerary.Source != HotelBookingSource.RezLive)
                            {
                                fareInfo = new HotelRoomFareBreakDown[hotelInfo[index].RoomDetails[j].Rates.Length];
                                for (int k = 0; k < hotelInfo[index].RoomDetails[j].Rates.Length; k++)
                                {
                                    HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                    fare.Date = hotelInfo[index].RoomDetails[j].Rates[k].Days;
                                    if (itinerary.Source != HotelBookingSource.RezLive)
                                    {
                                        fare.RoomPrice = hotelInfo[index].RoomDetails[j].Rates[k].SellingFare;
                                    }

                                    fareInfo[k] = fare;
                                }
                                roomdata.RoomFareBreakDown = fareInfo;
                            }
                            else
                            {
                                fareInfo = new HotelRoomFareBreakDown[1];
                                for (int k = 0; k < 1; k++)
                                {
                                    if (k == 0)
                                    {
                                        HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                        fare.Date = hotelInfo[index].RoomDetails[j].Rates[k].Days;

                                        fare.RoomPrice = hotelInfo[index].RoomDetails[j].Rates[k].Totalfare;

                                        fareInfo[k] = fare;
                                    }
                                }
                                roomdata.RoomFareBreakDown = fareInfo;


                            }
                            //room guest for each room

                            {
                                roomdata.AdultCount = request.RoomGuest[iR].noOfAdults;
                                roomdata.ChildCount = request.RoomGuest[iR].noOfChild;
                                roomdata.ChildAge = request.RoomGuest[iR].childAge;
                            }
                            roomdata.NoOfUnits = "1";//for each room there will be only one unit
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;
                            //No need to call Get Price as it is already set for HotelResult
                            {
                                roomdata.Price = new PriceAccounts();
                                decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                                for (int m = j; m < hotelInfo[index].RoomDetails.Length; m++)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[1];
                                    for (int k = 0; k < 1; k++)
                                    {
                                        if (itinerary.Source == HotelBookingSource.HotelConnect)
                                        {
                                            grossFare += hotelInfo[index].RoomDetails[m].Rates[k].SellingFare;
                                        }
                                        else if (itinerary.Source == HotelBookingSource.RezLive)
                                        {
                                            grossFare += hotelInfo[index].RoomDetails[m].Rates[k].Totalfare;
                                        }
                                    }
                                    totalExtraGuestCharge = hotelInfo[index].RoomDetails[m].SellExtraGuestCharges;
                                    totalChildCharge = hotelInfo[index].RoomDetails[m].ChildCharges;
                                    totalTax = hotelInfo[index].RoomDetails[m].TotalTax;
                                    break;

                                }
                                //if (hotelInfo[index].Currency != "AED")
                                //{
                                //    roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * rateOfExList[hotelInfo[index].Currency];
                                //}
                                //else
                                {
                                    roomdata.Price.NetFare = grossFare + totalExtraGuestCharge + totalChildCharge;
                                }
                                roomdata.Price.Tax = totalTax;
                                //roomdata.Price.SeviceTax = AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                                decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * hotelInfo[index].Price.AgentCommission / 100;
                                roomdata.Price.PublishedFare += agentComm;
                                roomdata.Price.AgentCommission = agentComm;
                            }
                            roomdata.Price.AccPriceType = priceType;
                            roomdata.Price.Discount = hotelInfo[index].RoomDetails[j].Discount;
                            roomdata.Price.CurrencyCode = hotelInfo[index].Currency;
                            if (rateOfExList.ContainsKey(hotelInfo[index].Currency))
                            {
                                roomdata.Price.RateOfExchange = rateOfExList[hotelInfo[index].Currency];
                            }
                            else
                            {
                                roomdata.Price.RateOfExchange = 1;
                            }
                            roomdata.Price.Markup = roomdata.Price.Markup;
                            roomdata.Price.Currency = hotelInfo[index].Currency;
                            roomInfo[z++] = roomdata;
                            break;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get MultiRoom Details. Error: " + ex.Message, "0");
        }
        return roomInfo;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="hotelInfo"></param>
    /// <returns></returns>
    protected HotelRoom[] LoadRoomDetails(ref HotelSearchResult[] hotelInfo)
    {
        HotelRoom[] roomInfo = new HotelRoom[itinerary.NoOfRooms];
        string roomCode = string.Empty;


        try
        {

            if (itinerary.Source == HotelBookingSource.IAN)
            {
                roomCode = Request["rCode" + index].ToString();
                roomCode = roomCode.Replace("\r\n", string.Empty).Trim();
                itinerary.PropertyType = hotelInfo[index].PropertyType;
                itinerary.SupplierType = hotelInfo[index].SupplierType;
            }
            else
            {
                roomCode = Session["rCode"].ToString();
            }
            bool rpValidate = true;
            for (int j = 0; j < hotelInfo[index].RoomDetails.Length; j++)
            {
                rpValidate = true;
                if (hotelInfo[index].BookingSource == HotelBookingSource.Desiya && !hotelInfo[index].RoomDetails[j].RatePlanCode.Equals(Session["rpCode"].ToString()))
                {
                    rpValidate = false;
                }
                if (hotelInfo[index].BookingSource == HotelBookingSource.RezLive)
                {
                    int rc = Convert.ToInt32(roomCode);
                    if (hotelInfo[index].RoomDetails[rc].RoomTypeName != null && rpValidate)
                    {

                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            HotelRoom roomdata = new HotelRoom();

                            {
                                roomdata.RoomTypeCode = hotelInfo[index].RoomDetails[rc].RoomTypeCode;
                            }
                            roomdata.RoomName = hotelInfo[index].RoomDetails[rc].RoomTypeName;
                            roomdata.RatePlanCode = hotelInfo[index].RoomDetails[rc].RatePlanCode;
                            roomdata.Ameneties = hotelInfo[index].RoomDetails[rc].Amenities;
                            roomdata.ExtraGuestCharge = hotelInfo[index].RoomDetails[rc].SellExtraGuestCharges / request.NoOfRooms;
                            roomdata.ChildCharge = hotelInfo[index].RoomDetails[rc].ChildCharges / request.NoOfRooms;
                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[hotelInfo[index].RoomDetails[rc].Rates.Length];
                            for (int k = 0; k < hotelInfo[index].RoomDetails[rc].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = hotelInfo[index].RoomDetails[rc].Rates[k].Days;
                                if (request.NoOfRooms == hotelInfo[index].RoomDetails.Length)
                                {
                                    fare.RoomPrice = hotelInfo[index].RoomDetails[rc].Rates[k].SellingFare * request.NoOfRooms;
                                    if (hotelInfo[index].Currency != "AED")
                                    {
                                        fare.RoomPrice = fare.RoomPrice * rateOfExList[hotelInfo[index].Currency];
                                    }
                                }
                                else
                                {
                                    fare.RoomPrice = hotelInfo[index].RoomDetails[rc].Rates[k].SellingFare / request.NoOfRooms;
                                    if (hotelInfo[index].Currency != "AED")
                                    {
                                        fare.RoomPrice = fare.RoomPrice * rateOfExList[hotelInfo[index].Currency];
                                    }
                                }

                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            roomdata.AdultCount = request.RoomGuest[i].noOfAdults;
                            roomdata.ChildAge = request.RoomGuest[i].childAge;
                            roomdata.ChildCount = request.RoomGuest[i].noOfChild;
                            roomdata.NoOfUnits = "1";//Number of units 1...Hardcoded temporarily..Need to change later.
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;

                            //No need to call Get Price as it is already set for HotelResult
                            roomdata.Price = new PriceAccounts();
                            decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                            for (int m = 0; m < hotelInfo[index].RoomDetails.Length; m++)
                            {
                                if (m == rc)
                                {
                                    fareInfo = new HotelRoomFareBreakDown[hotelInfo[index].RoomDetails[m].Rates.Length];
                                    for (int k = 0; k < hotelInfo[index].RoomDetails[m].Rates.Length; k++)
                                    {
                                        grossFare = hotelInfo[index].RoomDetails[m].Rates[k].SellingFare;
                                    }
                                    totalExtraGuestCharge = hotelInfo[index].RoomDetails[m].SellExtraGuestCharges;
                                    totalChildCharge = hotelInfo[index].RoomDetails[m].ChildCharges;
                                    totalTax = hotelInfo[index].RoomDetails[m].TotalTax;
                                    break;
                                }
                            }
                            if (hotelInfo[index].Currency != "AED")
                            {
                                roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * request.NoOfRooms * rateOfExList[hotelInfo[index].Currency];
                            }
                            else
                            {
                                roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * request.NoOfRooms;
                            }
                            roomdata.Price.Tax = totalTax * request.NoOfRooms;
                            if (itinerary.Source == HotelBookingSource.TBOConnect)
                            {
                                decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * hotelInfo[index].Price.AgentCommission / 100;
                                roomdata.Price.PublishedFare += agentComm;
                                roomdata.Price.AgentCommission = agentComm;
                                roomdata.Price.SeviceTax = AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                            }

                            //incase of fixed type commission it is giving mark up for all room at once in accounting engine to do change.
                            if (hSource.CommissionTypeId == CT.Core.CommissionType.Fixed)
                            {
                                roomdata.Price.Markup = roomdata.Price.Markup * request.NoOfRooms;
                            }
                            roomdata.Price.AccPriceType = priceType;
                            if (itinerary.Source == HotelBookingSource.Desiya)
                            {
                                roomdata.Price.RateOfExchange = 1;
                                roomdata.Price.CurrencyCode = "" + ConfigurationSystem.LocaleConfig["CurrencyCode"] + "";
                            }
                            else
                            {
                                roomdata.Price.CurrencyCode = hotelInfo[index].Currency;
                                if (rateOfExList.ContainsKey(hotelInfo[index].Currency))
                                {
                                    roomdata.Price.RateOfExchange = rateOfExList[hotelInfo[index].Currency];
                                    roomdata.Price.RateOfExchange = (roomdata.Price.RateOfExchange <= 0 ? 1 : roomdata.Price.RateOfExchange);
                                }
                                else
                                {
                                    roomdata.Price.RateOfExchange = 1;
                                }
                            }
                            roomdata.Price.Currency = hotelInfo[index].Currency;
                            roomdata.Price.Discount = hotelInfo[index].RoomDetails[rc].Discount;
                            roomInfo[i] = roomdata;
                        }

                        break;
                    }
                }
                else
                {
                    if (hotelInfo[index].RoomDetails[j].RoomTypeCode.Replace("\r\n", string.Empty).Trim().Equals(roomCode) && rpValidate)
                    {
                        for (int i = 0; i < request.NoOfRooms; i++)
                        {
                            HotelRoom roomdata = new HotelRoom();
                            if (itinerary.Source == HotelBookingSource.GTA)
                            {
                                string[] roomList = hotelInfo[index].RoomDetails[j].RoomTypeCode.Split('|');
                                roomdata.RoomTypeCode = roomList[0];
                                roomdata.NoOfCots = Convert.ToInt16(roomList[1]);
                                roomdata.ExtraBed = Convert.ToBoolean(Convert.ToInt16(roomList[2]));
                            }
                            else if (itinerary.Source == HotelBookingSource.TBOConnect)
                            {
                                string[] roomList = hotelInfo[index].RoomDetails[j].RoomTypeCode.Split('|');
                                roomdata.RoomId = Convert.ToInt32(roomList[0]);
                                roomdata.RoomTypeCode = roomList[1];
                            }
                            else
                            {
                                roomdata.RoomTypeCode = hotelInfo[index].RoomDetails[j].RoomTypeCode;
                            }
                            roomdata.RoomName = hotelInfo[index].RoomDetails[j].RoomTypeName;
                            roomdata.RatePlanCode = hotelInfo[index].RoomDetails[j].RatePlanCode;
                            roomdata.Ameneties = hotelInfo[index].RoomDetails[j].Amenities;
                            roomdata.ExtraGuestCharge = hotelInfo[index].RoomDetails[j].SellExtraGuestCharges / request.NoOfRooms;
                            roomdata.ChildCharge = hotelInfo[index].RoomDetails[j].ChildCharges / request.NoOfRooms;
                            HotelRoomFareBreakDown[] fareInfo = new HotelRoomFareBreakDown[hotelInfo[index].RoomDetails[j].Rates.Length];
                            for (int k = 0; k < hotelInfo[index].RoomDetails[j].Rates.Length; k++)
                            {
                                HotelRoomFareBreakDown fare = new HotelRoomFareBreakDown();
                                fare.Date = hotelInfo[index].RoomDetails[j].Rates[k].Days;
                                if (request.NoOfRooms == hotelInfo[index].RoomDetails.Length)
                                {
                                    if (itinerary.Source == HotelBookingSource.IAN)
                                    {
                                        fare.RoomPrice = hotelInfo[index].RoomDetails[j].Rates[k].Totalfare;
                                    }
                                    else
                                    {
                                        fare.RoomPrice = hotelInfo[index].RoomDetails[j].Rates[k].SellingFare * request.NoOfRooms;
                                    }
                                }
                                else
                                {
                                    if (itinerary.Source == HotelBookingSource.IAN)
                                    {
                                        fare.RoomPrice = hotelInfo[index].RoomDetails[j].Rates[k].Totalfare / request.NoOfRooms;
                                    }
                                    else
                                    {
                                        if (itinerary.Source != HotelBookingSource.RezLive)
                                        {
                                            fare.RoomPrice = hotelInfo[index].RoomDetails[j].Rates[k].SellingFare / request.NoOfRooms;
                                        }
                                        else
                                        {
                                            fare.RoomPrice = hotelInfo[index].RoomDetails[j].Rates[k].Totalfare / request.NoOfRooms;
                                            if (hotelInfo[index].Currency != "AED")
                                            {
                                                fare.RoomPrice = fare.RoomPrice * rateOfExList[hotelInfo[index].Currency];
                                            }
                                        }
                                    }
                                }

                                fareInfo[k] = fare;
                            }
                            roomdata.RoomFareBreakDown = fareInfo;
                            roomdata.AdultCount = request.RoomGuest[i].noOfAdults;
                            roomdata.ChildAge = request.RoomGuest[i].childAge;
                            roomdata.ChildCount = request.RoomGuest[i].noOfChild;
                            roomdata.NoOfUnits = "1";//Number of units 1...Hardcoded temporarily..Need to change later.
                            System.TimeSpan DiffResult = request.EndDate.Subtract(request.StartDate);
                            int nights = DiffResult.Days;

                            //No need to call Get Price as it is already set for HotelResult
                            {
                                roomdata.Price = new PriceAccounts();
                                decimal grossFare = 0, totalTax = 0, totalExtraGuestCharge = 0, totalChildCharge = 0;
                                for (int m = 0; m < hotelInfo[index].RoomDetails.Length; m++)
                                {
                                    if (hotelInfo[index].RoomDetails[m].RoomTypeCode.Replace("\r\n", string.Empty).Trim().Equals(roomCode))
                                    {
                                        fareInfo = new HotelRoomFareBreakDown[hotelInfo[index].RoomDetails[m].Rates.Length];
                                        for (int k = 0; k < hotelInfo[index].RoomDetails[m].Rates.Length; k++)
                                        {
                                            if (itinerary.Source != HotelBookingSource.RezLive)
                                            {
                                                grossFare += hotelInfo[index].RoomDetails[m].Rates[k].SellingFare;
                                            }
                                            else
                                            {
                                                grossFare += (hotelInfo[index].RoomDetails[m].Rates[k].Totalfare / hotelInfo[index].RoomDetails[m].Rates.Length);
                                            }

                                        }
                                        totalExtraGuestCharge = hotelInfo[index].RoomDetails[m].SellExtraGuestCharges;
                                        totalChildCharge = hotelInfo[index].RoomDetails[m].ChildCharges;
                                        totalTax = hotelInfo[index].RoomDetails[m].TotalTax;
                                        break;
                                    }
                                }
                                if (hotelInfo[index].Currency != "AED")
                                {
                                    roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * request.NoOfRooms * rateOfExList[hotelInfo[index].Currency];
                                }
                                else
                                {
                                    roomdata.Price.NetFare = (grossFare + totalExtraGuestCharge + totalChildCharge) * request.NoOfRooms;
                                }
                                roomdata.Price.Tax = totalTax * request.NoOfRooms;
                                if (itinerary.Source == HotelBookingSource.TBOConnect)
                                {
                                    decimal agentComm = (roomdata.Price.PublishedFare - totalExtraGuestCharge - totalChildCharge + roomdata.Price.Tax) * hotelInfo[index].Price.AgentCommission / 100;
                                    roomdata.Price.PublishedFare += agentComm;
                                    roomdata.Price.AgentCommission = agentComm;
                                    roomdata.Price.SeviceTax = AccountingEngine.CalculateProductServiceTax(grossFare + totalExtraGuestCharge + totalChildCharge + roomdata.Price.Tax, true, ProductType.Hotel);
                                }
                            }
                            //incase of fixed type commission it is giving mark up for all room at once in accounting engine to do change.
                            if (hSource.CommissionTypeId == CT.Core.CommissionType.Fixed)
                            {
                                roomdata.Price.Markup = roomdata.Price.Markup * request.NoOfRooms;
                            }
                            roomdata.Price.AccPriceType = priceType;
                            if (itinerary.Source == HotelBookingSource.Desiya)
                            {
                                roomdata.Price.RateOfExchange = 1;
                                roomdata.Price.CurrencyCode = "" + ConfigurationSystem.LocaleConfig["CurrencyCode"] + "";
                            }
                            else
                            {
                                roomdata.Price.CurrencyCode = hotelInfo[index].Currency;
                                if (rateOfExList.ContainsKey(hotelInfo[index].Currency))
                                {
                                    roomdata.Price.RateOfExchange = rateOfExList[hotelInfo[index].Currency];
                                    roomdata.Price.RateOfExchange = (roomdata.Price.RateOfExchange <= 0 ? 1 : roomdata.Price.RateOfExchange);
                                }
                                else
                                {
                                    roomdata.Price.RateOfExchange = 1;
                                }
                            }
                            roomdata.Price.Currency = hotelInfo[index].Currency;
                            roomdata.Price.Discount = hotelInfo[index].RoomDetails[j].Discount;
                            roomInfo[i] = roomdata;
                        }

                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetHotelDetails, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get Room Details. Error: " + ex.Message, "0");
        }
        return roomInfo;
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="itinerary"></param>
    void LoadCancellationDetails(ref HotelItinerary itinerary)
    {
        List<HotelPenality> penaltyInfo = itinerary.PenalityInfo;
        CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(Session["cSessionId"].ToString());
        CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(Session["cSessionId"].ToString());
        mse.SettingsLoginInfo = Settings.LoginInfo;
        //DOTWApi dotw = new DOTWApi(Session["cSessionId"].ToString());

        if (penaltyInfo == null)
        {
            penaltyInfo = new List<HotelPenality>();
        }
        CZInventory.SearchEngine se = new CZInventory.SearchEngine();


        if (itinerary.Source == HotelBookingSource.DOTW)
        {
            cancellationInfo = mse.GetCancellationDetails(itinerary, ref penaltyInfo, true, HotelBookingSource.DOTW);
        }
        else if (itinerary.Source == HotelBookingSource.HotelConnect)
        {
            HotelSearchResult result = Session["SelectedHotel"] as HotelSearchResult;
            List<string> Rooms = new List<string>();

            foreach (HotelRoom room in itinerary.Roomtype)
            {
                Rooms.Add(room.RoomTypeCode);
            }

            cancellationInfo = se.GetCancellationPolicy(ref result, request, Rooms);
        }
        else if (itinerary.Source == HotelBookingSource.RezLive) //modified by brahmam   20/09/2015
        {
            //request = Session["req"] as HotelRequest;
            //request.CityCode = itinerary.CityCode;
            //request.CityName = request.CityName.Replace(" City", "");
            //decimal price = 0;
            //foreach (HotelRoom room in itinerary.Roomtype)
            //{
            //    price += room.Price.SupplierPrice;
            //}

            //RezLive.XmlHub rezAPI = new RezLive.XmlHub(Session["cSessionId"].ToString());
            //if (Settings.LoginInfo.IsOnBehalfOfAgent)
            //{
            //    rezAPI.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
            //    rezAPI.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
            //    rezAPI.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
            //}
            //else
            //{
            //    rezAPI.AgentCurrency = Settings.LoginInfo.Currency;
            //    rezAPI.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
            //    rezAPI.DecimalPoint = Settings.LoginInfo.DecimalValue;
            //}
            //cancellationInfo = rezAPI.GetCancellationInfo(itinerary, request,price);
            cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
            cancellationInfo.Add("HotelPolicy", itinerary.HotelPolicyDetails);
        }
        else if (itinerary.Source == HotelBookingSource.LOH)
        {
            LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine(Session["cSessionId"].ToString());
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                jxe.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                jxe.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                jxe.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
            }
            else
            {
                jxe.AgentCurrency = Settings.LoginInfo.Currency;
                jxe.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                jxe.DecimalPoint = Settings.LoginInfo.DecimalValue;
            }
            decimal supplierPrice = 0;

            foreach (HotelRoom room in itinerary.Roomtype)
            {
                supplierPrice += room.Price.SupplierPrice;
            }
            //cancelation policy is coming based on the nationality
            cancellationInfo = jxe.GetCancellationDetails(itinerary.HotelCode, itinerary.Roomtype[0].RatePlanCode, itinerary.StartDate, itinerary.EndDate, itinerary.SequenceNumber, supplierPrice, itinerary.PassengerNationality);
        }
        else if (itinerary.Source == HotelBookingSource.HotelBeds)    //Added by brahmam 26.09.2014
        {
            CT.BookingEngine.GDS.HotelBeds hBeds = new CT.BookingEngine.GDS.HotelBeds();
            hBeds.sessionId = Session["cSessionId"].ToString();
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                hBeds.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                hBeds.AgentExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                hBeds.AgentDecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
            }
            else
            {
                hBeds.AgentCurrency = Settings.LoginInfo.Currency;
                hBeds.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                hBeds.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
            }
            cancellationInfo = hBeds.GetHotelChargeCondition(itinerary, ref penaltyInfo, true);
        }
        else if (itinerary.Source == HotelBookingSource.GTA || itinerary.Source == HotelBookingSource.Agoda)
        {
            //CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();
            //if (Settings.LoginInfo.IsOnBehalfOfAgent)
            //{
            //    gtaApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
            //    gtaApi.AgentExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
            //    gtaApi.AgentDecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
            //}
            //else
            //{
            //    gtaApi.AgentCurrency = Settings.LoginInfo.Currency;
            //    gtaApi.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
            //    gtaApi.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
            //}
            //cancellationInfo = gtaApi.GetHotelChargeCondition(itinerary, ref penaltyInfo, true);
            cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
        }
        else if (itinerary.Source == HotelBookingSource.TBOHotel)
        {
            cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
        }
        else if (itinerary.Source == HotelBookingSource.WST)
        {
            if (string.IsNullOrEmpty(itinerary.HotelCancelPolicy))
            {
                cancellationInfo = mse.GetCancellationDetails(itinerary, ref penaltyInfo, true, HotelBookingSource.WST);
            }
            else
            {
                cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
            }
        }
        else if (itinerary.Source == HotelBookingSource.Miki)
        {
            cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy); //@@@@ to be check for all rooms
        }
        else if (itinerary.Source == HotelBookingSource.JAC) // Loading cancellationpolicy Added by Brahmam 20.09.2016
        {
            if (string.IsNullOrEmpty(itinerary.HotelCancelPolicy))
            {
                cancellationInfo = mse.GetCancellationDetails(itinerary, ref penaltyInfo, true, HotelBookingSource.JAC);
            }
            else
            {
                cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
            }
        }
        else if (itinerary.Source == HotelBookingSource.EET)
        {
            CT.BookingEngine.GDS.EET eetApi = new CT.BookingEngine.GDS.EET(Session["cSessionId"].ToString());
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                eetApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                eetApi.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                eetApi.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
            }
            else
            {
                eetApi.AgentCurrency = Settings.LoginInfo.Currency;
                eetApi.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                eetApi.DecimalPoint = Settings.LoginInfo.DecimalValue;
            }
            decimal supplierPrice = 0;

            foreach (HotelRoom room in itinerary.Roomtype)
            {
                supplierPrice += room.Price.SupplierPrice;
            }
            //cancelation policy is coming based on the nationality
            cancellationInfo = eetApi.GetCancellationDetails(itinerary.HotelCode, itinerary.Roomtype[0].RatePlanCode, itinerary.StartDate, itinerary.EndDate, itinerary.SequenceNumber, supplierPrice, itinerary.PassengerNationality);
        }
        // Added by somasekhar on 03/09/2018 
        else if (itinerary.Source == HotelBookingSource.Yatra)
        {
            cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
            ////for showing Customer Detail entry fields in GuestDatails.aspx page
            //cancellationInfo.Add("RequireCityCode", "");
            //cancellationInfo.Add("RequireCountryCode", "");
            //cancellationInfo.Add("RequireFullAddress", "");
            //cancellationInfo.Add("RequireTelephoneHolder", "");
            //cancellationInfo.Add("RequirePostalCode", "");
            //cancellationInfo.Add("RequireCountryAccessCode", "");
            //cancellationInfo.Add("RequireState", "");
            //cancellationInfo.Add("RequireAreaCode", "");
            // GST Details
            cancellationInfo.Add("RequireGSTNumber", "");
            cancellationInfo.Add("RequireGSTCompanyName", "");
            cancellationInfo.Add("RequireGSTCompanyEmailId", "");
            cancellationInfo.Add("RequireGSTCompanyAddress", "");
            cancellationInfo.Add("RequireGSTCity", "");
            cancellationInfo.Add("RequireGSTPinCode", "");
            cancellationInfo.Add("RequireGSTState", "");
            cancellationInfo.Add("RequireGSTPhoneISD", "");
            cancellationInfo.Add("RequireGSTPhoneNumber", "");
            cancellationInfo.Add("RequireGSTCustomerName", "");
            cancellationInfo.Add("RequireGSTCustomerAddress", "");
            cancellationInfo.Add("RequireGSTCustomerState", "");

        }
        #region GRN Cancellation Policy
        else if (itinerary.Source == HotelBookingSource.GRN)
        {
            cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
            cancellationInfo.Add("RequireTelephoneHolder","");
        }
        #endregion     
        //Added by Somasekhar on 14/12/2018 -- OYO Source
        #region OYO Cancellation Policy
        else
        if (itinerary.Source == HotelBookingSource.OYO)
        {
            DateTime CancelDate = itinerary.StartDate;
            cancellationInfo.Add("CancelPolicy", itinerary.HotelCancelPolicy);
            cancellationInfo.Add("RequireTelephoneHolder", "");
            // GST INVOICE Details
            cancellationInfo.Add("RequireGSTCompanyName", "");
            cancellationInfo.Add("RequireGSTNumber", "");
            cancellationInfo.Add("RequireGSTCompanyEmailId", "");
            cancellationInfo.Add("RequireGSTCompanyAddress", ""); 
            cancellationInfo.Add("RequireGSTPhoneNumber", "");
            //for finding Date (dd-MMM-yy format -- 13-Dec-2018 / 13-DEC-2018 / 6-Dec-2018 / 6-DEC-2018   )
            Regex rgx = new Regex(@"\d{2}-[A-Za-z]{3}-\d{4}");
            Match mat = rgx.Match(itinerary.HotelCancelPolicy);
            if (string.IsNullOrEmpty(mat.ToString()))
            {
                rgx = new Regex(@"\d{1}-[A-Za-z]{3}-\d{4}");
                mat = rgx.Match(itinerary.HotelCancelPolicy);
            }
            CancelDate =!string.IsNullOrEmpty(mat.ToString())? Convert.ToDateTime(mat.ToString()) :itinerary.StartDate;
            cancellationInfo.Add("lastCancellationDate", CancelDate.ToString("yyyy-MMM-dd"));
        }
        #endregion
        try
        {


            foreach (KeyValuePair<string, string> pair in cancellationInfo)
            {
                switch (pair.Key)
                {
                    case "lastCancellationDate":
                        if (pair.Value.Length > 0 && itinerary.Source != HotelBookingSource.DOTW)
                        {
                            try
                            {
                                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                                itinerary.LastCancellationDate = Convert.ToDateTime(pair.Value, dateFormat);
                            }
                            catch { }
                        }
                        break;
                    case "CancelPolicy":
                        cancelData = pair.Value;
                        itinerary.HotelCancelPolicy = cancelData;
                        break;
                    case "HotelPolicy":
                        remarks = pair.Value;
                        break;
                    case "QUOTE_CHANGED":
                        warningMsg = "Price/Quotation has been revised for this Hotel. Please search again to book this Hotel.";
                        break;
                    case "PRICE_CHANGED":
                        warningMsg = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                        break;
                    case "NOT_AVAILABILITY":
                        warningMsg = "This particular room(s) is not available for this Hotel. Please search again to book this Hotel.";
                        break;
                    case "NOT_POSSIBLE":
                        warningMsg = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                        break;
                }
            }
            if (itinerary.Source != HotelBookingSource.LOH && itinerary.Source != HotelBookingSource.EET)
            {
                foreach (HotelRoom room in itinerary.Roomtype)
                {
                    if (room.PreviousFare > 0 && room.PreviousFare < room.Price.NetFare)
                    {
                        warningMsg += " Price has been revised for this Hotel. Please search again to book this Hotel.";
                        break;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.GetBooking, CT.Core.Severity.High, Convert.ToInt32(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID), "Failed to get Cancellation details(GuestDetails). Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        //CT.MetaSearchEngine1.MetaSearchEngine mse = new  CT.MetaSearchEngine1.MetaSearchEngine(Session["cSessionId"].ToString());
        //itinerary.HotelCancelPolicy = string.Empty;
        //List<HotelPenality> penalityList = new List<HotelPenality>();
        //Dictionary<string, string> polList = new Dictionary<string, string>();
        //if (itinerary.Source == HotelBookingSource.DOTW)
        //{
        //    polList = mse.GetCancellationDetails(itinerary, ref penalityList, true, itinerary.Source);
        //}
        //else if (itinerary.Source == HotelBookingSource.RezLive)
        //{
        //    //polList.Add("CancelPolicy", itinerary.HotelCancelPolicy);      

        //}
        itinerary.VoucherStatus = true;//always set to true, otherwise View Invoice button on booking queue will not be displayed- Shiva
        if (itinerary.IsDomestic || itinerary.Source == HotelBookingSource.IAN)
        {
            //itinerary.VoucherStatus = true;
            if (itinerary.Source == HotelBookingSource.TBOConnect)
            {
                itinerary.PenalityInfo = penaltyInfo;
            }
        }
        else
        {
            //itinerary.VoucherStatus = false;
            itinerary.PenalityInfo = penaltyInfo;
        }
        if (cancellationInfo.ContainsKey("HotelPolicy"))
        {
            itinerary.HotelPolicyDetails = cancellationInfo["HotelPolicy"];
        }
        if (cancellationInfo.ContainsKey("CancelPolicy"))
        {
            itinerary.HotelCancelPolicy = cancellationInfo["CancelPolicy"];
        }
        if (itinerary.Source == HotelBookingSource.TBOConnect)
        {
            DateTime lastCancellationDate = Convert.ToDateTime(cancellationInfo["lastCancellationDate"]);
            itinerary.LastCancellationDate = lastCancellationDate;
        }
        else if (itinerary.Source == HotelBookingSource.DOTW)
        {
            if (cancellationInfo.ContainsKey("lastCancellationDate"))
            {
                itinerary.LastCancellationDate = mse.GetLastCancellationDateforHotel(Convert.ToInt32(cancellationInfo["lastCancellationDate"]), itinerary.StartDate, itinerary.Source);
                //if ((!Role.IsAllowedTask((int)Session["roleId"], (int)Task.HotelBookingUnderCancellation)) && itinerary.LastCancellationDate <= DateTime.Now && !itinerary.IsDomestic && itinerary.Source != HotelBookingSource.IAN)
                //{
                //    bookingMessage = "We are unable to book the hotel as The check-in date for the booking is within the cancellation period.";
                //    mailMessage = "If you still want to book this hotel Go to: <a href=\"HotelSearch.aspx\">Search Page</a> or  <a href=\"javascript:sendMail()\">Send E-mail.</a>";
                //    Session["hItinerary"] = itinerary;
                //}
            }
            else
            {
                //incase of non refundable room booking, last cancellation date is null in case of IAN/Tourico/HotelBeds
                itinerary.LastCancellationDate = DateTime.Now;
            }
        }

        if (cancellationInfo.ContainsKey("isNonRefundable") && cancellationInfo["isNonRefundable"] == "true")
        {
            itinerary.LastCancellationDate = DateTime.Now;//since its a non-refundable booking, can not be cancelled
            //if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.HotelBookingUnderCancellation) && !itinerary.IsDomestic && itinerary.Source != HotelBookingSource.IAN)
            {
                bookingMessage = "We are unable to book the hotel as you have selected non refundable room.";
                mailMessage = "If you still want to book this hotel <a href=\"javascript:sendMail()\">Click here to E-mail.</a> <br/>or<br/> <a href=\"javascript:history.go(-1)\">Go Back</a> to Choose another hotel.";
                Session["hItinerary"] = itinerary;
            }
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="itinerary"></param>
    void LoadHotelDetails(ref HotelItinerary itinerary)
    {
        MetaSearchEngine mse = new MetaSearchEngine(Session["cSessionId"].ToString());
        HotelDetails hotelDetail = new HotelDetails();
        if (itinerary.Source != HotelBookingSource.HotelConnect && itinerary.Source != HotelBookingSource.TBOHotel)
        {
            // we can avoid this method --ziya
            hotelDetail = mse.GetHotelDetails(itinerary.CityCode, itinerary.HotelCode, itinerary.StartDate, itinerary.EndDate, request.PassengerNationality, request.PassengerCountryOfResidence, itinerary.Source);
        }
        else if (itinerary.Source == HotelBookingSource.TBOHotel)
        {
            //Loading static data   added by brahmam
            try
            {
                TBOHotel.HotelV10 tboHotel = new TBOHotel.HotelV10();
                hotelDetail = tboHotel.GetHotelDetails(itinerary.CityCode, itinerary.HotelCode, Convert.ToInt32(itinerary.SequenceNumber), itinerary.PropertyType);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TBOHoteDetailsSerch, Severity.High, 0, "Exception returned from TBOHotel.GetHotelDetails Error Message:" + ex.Message + DateTime.Now, "");
                throw new BookingEngineException("Error: " + ex.Message);
            }
        }
        if (itinerary.Source == HotelBookingSource.Desiya || itinerary.Source == HotelBookingSource.IAN)
        {
            if (!string.IsNullOrEmpty(hotelDetail.HotelPolicy))
            {
                itinerary.HotelPolicyDetails += hotelDetail.HotelPolicy + "| ";
            }
            else
            {
                itinerary.HotelPolicyDetails = string.Empty;
            }
        }

        if (hotelDetail.Email != null && hotelDetail.Email.Length > 0 && hotelDetail.Email != "NA" && hotelDetail.Email != "NA-NA")
        {
            itinerary.HotelAddress2 += "\n E-mail :" + hotelDetail.Email;
        }
        if (hotelDetail.PhoneNumber != null && hotelDetail.PhoneNumber.Length > 0 && hotelDetail.PhoneNumber != "NA" && hotelDetail.PhoneNumber != "NA-NA")
        {
            itinerary.HotelAddress2 += "\n Phone No: " + hotelDetail.PhoneNumber;
        }
        if (hotelDetail.FaxNumber != null && hotelDetail.FaxNumber.Length > 0 && hotelDetail.FaxNumber != "NA" && hotelDetail.FaxNumber != "NA-NA")
        {
            itinerary.HotelAddress2 += "\n Fax : " + hotelDetail.FaxNumber;
        }
    }

    protected void imgContinue_Click(object sender, EventArgs e)
    {
        if (action.Value != null)
        {
            if (action.Value == "bookHotel")
            {
                if (Session["hItinerary"] == null)
                {
                    errorMessage = "Your Session is expired !! Please" + "<a href=\"HotelSearch.aspx\"" + "\">" + " Retry" + "</a>.";
                    return;
                }
                else
                {
                    itinerary = (HotelItinerary)Session["hItinerary"];
                    string reviewBookingPageLink = "HTLBookingConfirm.aspx";
                    if (itinerary.Source == HotelBookingSource.IAN)
                    {
                        reviewBookingPageLink = Request.Url.Scheme + "://";
                        //if (Convert.ToBoolean(ConfigurationManager.AppSettings["SSL"]))
                        //{
                        //    reviewBookingPageLink = "https://";
                        //}
                        //else
                        //{
                        //    reviewBookingPageLink = "http://";
                        //}
                        reviewBookingPageLink += Request.ServerVariables["HTTP_HOST"] + Regex.Match(Request.ServerVariables["URL"], @"^(/([\w]+?/)*?)\w+?[.]aspx$").Groups[1];
                        reviewBookingPageLink += "HTLBookingConfirm.aspx";
                    }
                    //if (txtFirstName.Text.Length > || txtLastName.Text.Length < 2)
                    //{
                    isMultiRoom = Convert.ToBoolean(Session["isMultiRoom"]);
                    HotelPassenger leadpassInfo = new HotelPassenger();
                    HotelPassengerGST leadpassGSTInfo = new HotelPassengerGST();
                    leadpassInfo.PaxType = (HotelPaxType)Convert.ToInt32(Request["paxType"]);
                    leadpassInfo.Title = ddlTitle.SelectedValue;
                    leadpassInfo.Firstname = txtFirstName.Text.Trim();
                    leadpassInfo.Lastname = txtLastName.Text.Trim();
                    if ((itinerary.Source == HotelBookingSource.LOH || itinerary.Source == HotelBookingSource.EET || itinerary.Source == HotelBookingSource.Yatra) && txtAddress.Visible)
                    {
                        leadpassInfo.Addressline1 = txtAddress.Text.Trim();
                    }
                    else
                    {
                        leadpassInfo.Addressline1 = "";//txtAddress.Text.Trim();
                    }
                    leadpassInfo.Addressline2 = "";
                    leadpassInfo.Email = txtEmail.Text.Trim();
                    //added by somasekhar on 12/09/2018 
                    if (itinerary.Source == HotelBookingSource.Yatra && txtAreaCode.Visible)
                    {
                        leadpassInfo.Areacode = txtAreaCode.Text;
                    }
                    else {
                        leadpassInfo.Areacode = "";
                    }

                    if ((itinerary.Source == HotelBookingSource.LOH || itinerary.Source == HotelBookingSource.EET || itinerary.Source == HotelBookingSource.Yatra) && ddlCountry.Visible)
                    {
                        leadpassInfo.Countrycode = ddlCountry.SelectedValue;
                    }
                    else
                    {
                        leadpassInfo.Countrycode = "";
                    }
                    leadpassInfo.NationalityCode = Country.GetCountryCodeFromCountryName(txtNationality.Text.Trim());
                    if ((itinerary.Source == HotelBookingSource.LOH || itinerary.Source == HotelBookingSource.EET || itinerary.Source == HotelBookingSource.Yatra || itinerary.Source == HotelBookingSource.GRN || itinerary.Source == HotelBookingSource.OYO) && txtMobile.Visible)
                    {
                        leadpassInfo.Phoneno = txtMobile.Text.Trim();
                    }
                    else
                    {
                        leadpassInfo.Phoneno = "";
                    }
                    if ((itinerary.Source == HotelBookingSource.LOH || itinerary.Source == HotelBookingSource.EET || itinerary.Source == HotelBookingSource.Yatra) && txtPostalCode.Visible)
                    {
                        leadpassInfo.Zipcode = txtPostalCode.Text;
                    }
                    else
                    {
                        leadpassInfo.Zipcode = "";
                    }
                    itinerary.SpecialRequest = string.Empty;
                    itinerary.FlightInfo = "";
                    if ((itinerary.Source == HotelBookingSource.LOH || itinerary.Source == HotelBookingSource.EET || itinerary.Source == HotelBookingSource.Yatra) && txtCity.Visible)
                    {
                        leadpassInfo.City = txtCity.Text.Trim();
                    }
                    else
                    {
                        leadpassInfo.City = "";
                    }
                    leadpassInfo.PaxType = HotelPaxType.Adult;
                    if ((itinerary.Source == HotelBookingSource.LOH || itinerary.Source == HotelBookingSource.EET || itinerary.Source == HotelBookingSource.Yatra) && ddlCountry.Visible)
                    {
                        leadpassInfo.Country = ddlCountry.SelectedItem.Text.Trim();
                    }
                    else
                    {
                        leadpassInfo.Country = "";
                    }
                    leadpassInfo.Nationality = txtNationality.Text.Trim();
                    leadpassInfo.LeadPassenger = true;
                    if (agency.AddnlPaxDetails == "Y")
                    {
                        leadpassInfo.Department = ddlDepartment.SelectedItem.Text.Trim();
                        leadpassInfo.Employee = txtEmployee.Text.Trim();
                        leadpassInfo.Division = txtDivision.Text.Trim();
                        leadpassInfo.Purpose = txtPurpose.Text.Trim();
                        leadpassInfo.EmployeeID = txtEmpID.Text.Trim();
                    }
                    //if (Request["zipcode"] != "")
                    //{
                    //    leadpassInfo.Zipcode = Request["zipcode"];
                    //}
                    //else
                    //{
                    //    leadpassInfo.Zipcode = "00000";
                    //}
                   // leadpassInfo.State = "";// txtState.Text;
                                           
                    // Added by somasekhar on 12/09/2018 -- Passanger and GST Details 
                    if (itinerary.Source == HotelBookingSource.Yatra && txtState.Visible)
                    {
                        leadpassInfo.State  = txtState.Text;
                    }
                    else
                    {
                        leadpassInfo.State = ""; 
                    }
                    if (itinerary.Source == HotelBookingSource.Yatra && txtCountryAccessCode.Visible)
                    {
                        leadpassInfo.Countrycode = txtCountryAccessCode.Text;
                    }
                    else
                    {
                        leadpassInfo.Countrycode = "";
                    }
                    leadpassInfo.IsGST = false;
                    // =====================  GST Details -- Added by somasekhar for Yatra and OYO  only  ============================
                    if (itinerary.Source == HotelBookingSource.Yatra || itinerary.Source == HotelBookingSource.OYO)
                    {
                        if (txtGSTNumber.Visible && !string.IsNullOrEmpty(txtGSTNumber.Text))
                    {
                        leadpassGSTInfo.GSTNumber = txtGSTNumber.Text.ToUpper().Trim();
                    }
                    else
                    {
                        leadpassGSTInfo.GSTNumber = "";
                    }
                    if (txtGSTCompanyName.Visible && !string.IsNullOrEmpty(txtGSTCompanyName.Text))
                    {
                        leadpassGSTInfo.GSTCompanyName = txtGSTCompanyName.Text.Trim();
                    }
                    else
                    {
                        leadpassGSTInfo.GSTCompanyName = "";
                    }
                  
                    if (txtGSTCompanyEmailId.Visible && !string.IsNullOrEmpty(txtGSTCompanyEmailId.Text))
                    {
                        leadpassGSTInfo.GSTCompanyEmailId = txtGSTCompanyEmailId.Text.Trim();
                    }
                    else
                    {
                        leadpassGSTInfo.GSTCompanyEmailId = "";
                    }
                    if (txtGSTCompanyAddress.Visible && !string.IsNullOrEmpty(txtGSTCompanyAddress.Text))
                    {
                        leadpassGSTInfo.GSTCompanyAddress = txtGSTCompanyAddress.Text.Trim();
                    }
                    else
                    {
                        leadpassGSTInfo.GSTCompanyAddress = "";
                    }
                    if (txtGSTCity.Visible && !string.IsNullOrEmpty(txtGSTCity.Text))
                    {
                        leadpassGSTInfo.GSTCity = txtGSTCity.Text.Trim();
                    }
                    else
                    {
                        leadpassGSTInfo.GSTCity = "";
                    }
                    if (txtGSTPinCode.Visible && !string.IsNullOrEmpty(txtGSTPinCode.Text))
                    {
                        leadpassGSTInfo.GSTPinCode = txtGSTPinCode.Text.Trim();
                    }
                    else
                    {
                        leadpassGSTInfo.GSTPinCode = "";
                    }
                    //if (txtGSTState.Visible  && !string.IsNullOrEmpty(txtGSTState.Text))
                    if (ddlstatelist.Visible && ddlstatelist.SelectedIndex > 0)//
                    {
                            leadpassGSTInfo.GSTState = ddlstatelist.SelectedValue.ToString();
                    }
                    else
                    {
                        leadpassGSTInfo.GSTState = "";
                    }
                    if (txtGSTPhoneISD.Visible && !string.IsNullOrEmpty(txtGSTPhoneISD.Text))
                    {
                        leadpassGSTInfo.GSTPhoneISD = txtGSTPhoneISD.Text.Trim();
                    }
                    else
                    {
                        leadpassGSTInfo.GSTPhoneISD = "";
                    }
                    if (txtGSTPhoneNumber.Visible && !string.IsNullOrEmpty(txtGSTPhoneNumber.Text))
                    {
                        leadpassGSTInfo.GSTPhoneNumber = txtGSTPhoneNumber.Text.Trim();
                    }
                    else
                    {
                        leadpassGSTInfo.GSTPhoneNumber = "";
                    }
                    if (txtGSTCustomerName.Visible && !string.IsNullOrEmpty(txtGSTCustomerName.Text))
                    {
                        leadpassGSTInfo.GSTCustomerName = txtGSTCustomerName.Text.Trim();
                    }
                    else
                    {
                        leadpassGSTInfo.GSTCustomerName = "";
                    }
                    if (txtGSTCustomerAddress.Visible && !string.IsNullOrEmpty(txtGSTCustomerAddress.Text))
                    {
                        leadpassGSTInfo.GSTCustomerAddress = txtGSTCustomerAddress.Text.Trim();
                    }
                    else
                    {
                        leadpassGSTInfo.GSTCustomerAddress = "";
                    }
                    if (itinerary.Source == HotelBookingSource.Yatra && txtGSTCustomerState.Visible && !string.IsNullOrEmpty(txtGSTCustomerState.Text))
                    {
                        leadpassGSTInfo.GSTCustomerState  = txtGSTCustomerState.Text.Trim();
                    }
                    else
                    {
                        leadpassGSTInfo.GSTCustomerState = "";
                    }
                    leadpassInfo.HotelPassengerGST = leadpassGSTInfo;

                    if (!string.IsNullOrEmpty(txtGSTNumber.Text) || !string.IsNullOrEmpty(txtGSTCompanyName.Text) || !string.IsNullOrEmpty(txtGSTCompanyEmailId.Text) ||
                        !string.IsNullOrEmpty(txtGSTCompanyAddress.Text) || !string.IsNullOrEmpty(txtGSTCity.Text) || !string.IsNullOrEmpty(txtGSTPinCode.Text) ||
                        ddlstatelist.SelectedIndex > 0 || !string.IsNullOrEmpty(txtGSTPhoneISD.Text) || !string.IsNullOrEmpty(txtGSTPhoneNumber.Text) ||
                        !string.IsNullOrEmpty(txtGSTCustomerName.Text) || !string.IsNullOrEmpty(txtGSTCustomerAddress.Text) || !string.IsNullOrEmpty(txtGSTCustomerState.Text)
                        )
                    {
                        leadpassInfo.IsGST = true;
                    }
                    }
                    ///=======================================================
                    //Added by Hari
                    if (tblFlexFields != null && Convert.ToInt32(hdnFlexCount.Value) > 0)
                    {
                        leadpassInfo.FlexDetailsList = new List<FlightFlexDetails>();

                        for (int i = 0; i < Convert.ToInt32(hdnFlexCount.Value); i++)
                        {
                            FlightFlexDetails flexDetails = new FlightFlexDetails();
                            HiddenField hdnFlexControl = tblFlexFields.FindControl("hdnFlexControl" + i) as HiddenField;
                            HiddenField hdnFlexId = tblFlexFields.FindControl("hdnFlexId" + i) as HiddenField;
                            HiddenField hdnFlexLabel = tblFlexFields.FindControl("hdnFlexLabel" + i) as HiddenField;
                            flexDetails.FlexId = Convert.ToInt32(hdnFlexId.Value);
                            flexDetails.FlexLabel = hdnFlexLabel.Value;
                            switch (hdnFlexControl.Value)
                            {
                                case "T":
                                    TextBox txtFlex = tblFlexFields.FindControl("txtFlex" + i) as TextBox;
                                    flexDetails.FlexData = txtFlex.Text;
                                    break;
                                case "D":
                                    DropDownList ddlPaxDay = tblFlexFields.FindControl("ddlDay" + i) as DropDownList;
                                    DropDownList ddlPaxMonth = tblFlexFields.FindControl("ddlMonth" + i) as DropDownList;
                                    DropDownList ddlPaxYear = tblFlexFields.FindControl("ddlYear" + i) as DropDownList;
                                    flexDetails.FlexData = Convert.ToString(Convert.ToDateTime(ddlPaxDay.SelectedValue + "/" + ddlPaxMonth.SelectedValue + "/" + ddlPaxYear.SelectedValue));
                                    break;
                                case "L":
                                    DropDownList ddlFlex = tblFlexFields.FindControl("ddlFlex" + i) as DropDownList;
                                    flexDetails.FlexData = ddlFlex.SelectedItem.Value;
                                    break;
                            }
                            flexDetails.CreatedBy = (int)Settings.LoginInfo.UserID;
                            flexDetails.ProductID = 2;
                            leadpassInfo.FlexDetailsList.Add(flexDetails);
                        }
                    }


                    int j, k;
                    for (int i = 0; i < itinerary.Roomtype.Length; i++)
                    {
                        List<HotelPassenger> hotelPaxList = new List<HotelPassenger>();
                        //for (int iR = 1; iR <= Convert.ToInt16(itinerary.Roomtype[i].NoOfUnits); iR++)
                        {
                            k = 0;
                            if (i == 0)
                            {
                                j = 1;
                                hotelPaxList.Add(leadpassInfo);
                            }
                            else
                            {
                                j = 0;
                            }

                            for (; j < (itinerary.Roomtype[i].AdultCount + itinerary.Roomtype[i].ChildCount); j++)
                            {

                                if (tblAdults.Rows.Count > 0)
                                {
                                    HiddenField hdfPaxType = new HiddenField();
                                    HiddenField hdfAPaxType = tblAdults.Rows[j].FindControl("hdfAPaxType" + i + j) as HiddenField;
                                    if (hdfAPaxType == null)
                                    {
                                        if (k <= 0)
                                        {
                                            k = 0;
                                        }
                                        hdfPaxType = tblAdults.Rows[j].FindControl("hdfPaxType" + i + k) as HiddenField;
                                    }

                                    TextBox txtPrefix = new TextBox();
                                    TextBox txtFName = new TextBox();
                                    TextBox txtLName = new TextBox();
                                    //DropDownList ddlDept = new DropDownList();
                                    TextBox txtEmp = new TextBox();
                                    TextBox txtdivision = new TextBox();
                                    TextBox txtpurpose = new TextBox();
                                    TextBox txtEmpId = new TextBox();
                                    DropDownList ddlPrefix = new DropDownList();
                                    HiddenField hdfDepartment = new HiddenField();
                                    //TextBox txt
                                    if (hdfAPaxType != null && hdfAPaxType.Value == "A")
                                    {
                                        HotelPassenger adultpassInfo = new HotelPassenger();
                                        //txtPrefix = tblAdults.Rows[j].FindControl("txtAPrefix" + i + j) as TextBox;
                                        ddlPrefix = tblAdults.Rows[j].FindControl("ddlATitle" + i + j) as DropDownList;
                                        txtFName = tblAdults.Rows[j].FindControl("txtAFirstName" + i + j) as TextBox;
                                        txtLName = tblAdults.Rows[j].FindControl("txtALastName" + i + j) as TextBox;

                                        //ddlDept = tblAdults.Rows[j + 1].FindControl("ddlDepartment" + i + j) as DropDownList;
                                        txtEmp = tblAdults.Rows[j + 1].FindControl("txtEmployee" + i + j) as TextBox;
                                        txtEmpId = tblAdults.Rows[j + 1].FindControl("txtEmployeeID" + i + j) as TextBox;
                                        txtdivision = tblAdults.Rows[j + 2].FindControl("txtDivision" + i + j) as TextBox;
                                        txtpurpose = tblAdults.Rows[j + 2].FindControl("txtPurpose" + i + j) as TextBox;
                                        hdfDepartment = tblAdults.Rows[j + 1].FindControl("hdfADept" + i + j) as HiddenField;

                                        adultpassInfo.PaxType = HotelPaxType.Adult;
                                        adultpassInfo.Title = ddlPrefix.SelectedValue;//txtPrefix.Text;
                                        adultpassInfo.Firstname = txtFName.Text;
                                        adultpassInfo.Lastname = txtLName.Text;
                                        adultpassInfo.Phoneno = leadpassInfo.Phoneno;
                                        adultpassInfo.Email = leadpassInfo.Email;
                                        adultpassInfo.Addressline1 = leadpassInfo.Addressline1;
                                        adultpassInfo.City = leadpassInfo.City;
                                        adultpassInfo.Country = leadpassInfo.Country;
                                        adultpassInfo.Nationality = leadpassInfo.Nationality;
                                        adultpassInfo.NationalityCode = leadpassInfo.NationalityCode;
                                        adultpassInfo.State = leadpassInfo.State;
                                        adultpassInfo.LeadPassenger = false;
                                        if (agency.AddnlPaxDetails == "Y")
                                        {
                                            adultpassInfo.Department = hdfDepartment.Value;
                                            adultpassInfo.Employee = txtEmp.Text;
                                            adultpassInfo.Division = txtdivision.Text;
                                            adultpassInfo.Purpose = txtpurpose.Text;
                                            adultpassInfo.EmployeeID = txtEmpId.Text;
                                        }

                                        if (j == 0)
                                        {
                                            //for each room first adult will be the lead pax
                                            adultpassInfo.LeadPassenger = true;
                                        }
                                        else
                                        {
                                            adultpassInfo.LeadPassenger = false;
                                        }
                                        hotelPaxList.Add(adultpassInfo);
                                        // j++;
                                    }
                                    if (hdfPaxType != null && hdfPaxType.Value == "C")
                                    {
                                        HotelPassenger childpassInfo = new HotelPassenger();
                                        //txtPrefix = tblAdults.Rows[j].FindControl("txtPrefix" + i + k.ToString()) as TextBox;
                                        ddlPrefix = tblAdults.Rows[j].FindControl("ddlTitle" + i + k) as DropDownList;
                                        txtFName = tblAdults.Rows[j].FindControl("txtFirstName" + i + k.ToString()) as TextBox;
                                        txtLName = tblAdults.Rows[j].FindControl("txtLastName" + i + k.ToString()) as TextBox;
                                        //ddlDept = tblAdults.Rows[j + 1].FindControl("ddlDepartment" + i + (k + 2)) as DropDownList;
                                        //txtEmp = tblAdults.Rows[j + 1].FindControl("txtCEmployee" + i + (k + 2)) as TextBox;
                                        //txtEmpId = tblAdults.Rows[j + 1].FindControl("txtCEmployeeID" + i + (k + 2)) as TextBox;
                                        //txtdivision = tblAdults.Rows[j + 2].FindControl("txtCDivision" + i + (k + 2)) as TextBox;
                                        //txtpurpose = tblAdults.Rows[j + 2].FindControl("txtCPurpose" + i + (k + 2)) as TextBox;
                                        //hdfDepartment = tblAdults.Rows[j + 1].FindControl("hdfDept" + i + (k + 2)) as HiddenField;

                                        childpassInfo.Title = ddlPrefix.SelectedValue;//txtPrefix.Text;

                                        childpassInfo.Firstname = txtFName.Text;
                                        childpassInfo.Lastname = txtLName.Text;
                                        childpassInfo.LeadPassenger = false;
                                        childpassInfo.PaxType = HotelPaxType.Child;
                                        childpassInfo.Age = itinerary.Roomtype[i].ChildAge[k];
                                        childpassInfo.Phoneno = leadpassInfo.Phoneno;
                                        childpassInfo.Email = leadpassInfo.Email;
                                        childpassInfo.Addressline1 = leadpassInfo.Addressline1;
                                        childpassInfo.City = leadpassInfo.City;
                                        childpassInfo.Country = leadpassInfo.Country;
                                        childpassInfo.Nationality = leadpassInfo.Nationality;
                                        childpassInfo.NationalityCode = leadpassInfo.NationalityCode;
                                        childpassInfo.State = leadpassInfo.State;
                                        //if (agency.AddnlPaxDetails == "Y")
                                        //{
                                        //    childpassInfo.Department = hdfDepartment.Value;
                                        //    childpassInfo.Employee = txtEmp.Text;
                                        //    childpassInfo.Division = txtdivision.Text;
                                        //    childpassInfo.Purpose = txtpurpose.Text;
                                        //    childpassInfo.EmployeeID = txtEmpId.Text;
                                        //}

                                        hotelPaxList.Add(childpassInfo);
                                        k++;
                                        //j++;
                                    }
                                    if (itinerary.Roomtype[i].ChildCount <= 0)
                                    {
                                        itinerary.Roomtype[i].ChildAge = new List<int>();
                                    }
                                }


                                //for (int k = 0; k < itinerary.Roomtype[i].ChildCount; k++)
                                //{
                                //    if (tblChilds.Rows.Count > 0)
                                //    {
                                //HotelPassenger childpassInfo = new HotelPassenger();



                                //childpassInfo.Title = txtPrefix.Text;
                                //childpassInfo.Firstname = txtFName.Text;
                                //childpassInfo.Lastname = txtLName.Text;
                                //childpassInfo.LeadPassenger = false;
                                //childpassInfo.PaxType = HotelPaxType.Child;
                                //childpassInfo.Age = itinerary.Roomtype[i].ChildAge[k];
                                //childpassInfo.Phoneno = leadpassInfo.Phoneno;
                                //childpassInfo.Email = leadpassInfo.Email;
                                //childpassInfo.Addressline1 = leadpassInfo.Addressline1;
                                //childpassInfo.City = leadpassInfo.City;
                                //childpassInfo.Country = leadpassInfo.Country;
                                //childpassInfo.Nationality = leadpassInfo.Nationality;
                                //childpassInfo.State = leadpassInfo.State;
                                //hotelPaxList.Add(childpassInfo);
                                //    }
                                //}
                            }
                        }

                        itinerary.Roomtype[i].PassenegerInfo = hotelPaxList;
                        //itinerary.Roomtype[i].Price.AsvType = string.Empty;
                        //itinerary.Roomtype[i].Price.AsvElement = string.Empty;
                    }

                    itinerary.HotelPassenger = leadpassInfo;
                    itinerary.VatDescription = txtPaymentNotes.Text.Trim(); // Payment notes is inserting in Hotel Itinerary table

                    Session["hItinerary"] = itinerary;
                    UserBookings.Add((int)Settings.LoginInfo.UserID, itinerary);
                    Session["UserBookings"] = UserBookings;

                    if (warningMsg.Length > 0)
                    {
                        Response.Redirect("HotelSearch.aspx?source=Hotel");
                    }
                    else
                    {
                        Response.Redirect(reviewBookingPageLink);
                    }
                }
            }
        }
    }



    protected void dlAdults_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        Label lblGuest = e.Item.FindControl("lblGuestInfo") as Label;

        lblGuest.Text = "Adult-" + (e.Item.ItemIndex + 1);
    }
    protected void dlChilds_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        Label lblGuest = e.Item.FindControl("lblGuestInfo") as Label;

        lblGuest.Text = "Child-" + (e.Item.ItemIndex + 1);
    }
    protected void ImageButton1_Click(object sender, ImageClickEventArgs e)
    {

    }

    void BindDates(DropDownList ddlDay)
    {
        ddlDay.Items.Add(new ListItem("Day", "-1"));
        for (int i = 1; i <= 31; i++)
        {
            ddlDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
        ddlDay.SelectedValue = "-1";
    }
    void BindMonths(DropDownList ddlMonth)
    {
        ddlMonth.Items.Add(new ListItem("Month", "-1"));
        string[] months = new string[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        for (int i = 1; i <= months.Length; i++)
        {
            ddlMonth.Items.Add(new ListItem(months[i - 1], i.ToString()));
        }
    }
    void BindYears(DropDownList ddlYear)
    {
        ddlYear.Items.Add(new ListItem("Year", "-1"));
        for (int i = 1930; i <= DateTime.Now.Year; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }

    protected void BindStates()
    {
        try
        {
            ddlstatelist.DataSource = LocationMaster.GetTourRegionList(ListStatus.Short, RecordStatus.Activated, "S", "IN");
            ddlstatelist.DataValueField = "rgn_code";
            ddlstatelist.DataTextField = "rgn_name";
            ddlstatelist.DataBind();
            ddlstatelist.Items.Insert(0, new ListItem("--Select State--", "0"));
            ddlstatelist.SelectedIndex = 0;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
}

