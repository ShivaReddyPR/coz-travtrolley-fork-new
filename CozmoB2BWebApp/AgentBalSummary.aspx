﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="AgentBalSummary" Title="Agent Balance Summary" Codebehind="AgentBalSummary.aspx.cs" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="uc1" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript">
    function Validation() {
        var fromDate = GetDateObject('ctl00_cphTransaction_dcFromDate');
        if (fromDate == null) addMessage('Please select From Date !', '');
        if (getMessage() != '') {
            alert(getMessage()); clearMessage(); return false;
        }
    }
</script>
<asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
<table>
    <tr>
            <td>
                 <div class="col-md-2"> <asp:Label Text="Agent:" ID="lblAgent" runat="server" style="text-align:right;"></asp:Label>
                 </div>
                            
            </td>
            <td>
                  <asp:DropDownList ID="ddlAgent" runat="server" Width="150px">
                                    
                                </asp:DropDownList>
                            
            </td>
            <td>
                 <div class="col-md-2">  <asp:Label Text="Date:" ID="lblDate" runat="server"></asp:Label>
                 </div>
            </td>
            <td>                
                  <div class="col-md-2">
                  <uc1:DateControl ID="dcFromDate" runat="server" BaseYearLimit="0"  DateFormat="DDMMMYYYY" DateOnly="true"  DropDownYears="10" Enabled="true" HorizontalAlignment="Left" OutOfMonthDisable="false" OutOfMonthHide="false" TimeDelimiter="Colon" VerticalAlignment="Down" WeekNumberBaseDay="Tuesday" WeekNumberDisplay="true" WeekStartDate="Saturday" ></uc1:DateControl>
                  </div>
            </td>
            <td>                 
                 <div class="col-md-2"> <asp:Button ID="btnExport" OnClientClick="return Validation();" CssClass="button form-control" runat="server" Text="Export"  Width="100px"  OnClick="btnExport_Click"/> 
                 </div>           
            </td>
                       
                 
                 
                   <%-- </div>--%>
            
    </tr>
</table>
<div>
                      <asp:DataGrid ID="dgAgencyBalReportList" runat="server" AutoGenerateColumns="false">
                      <Columns> 
                                 <asp:BoundColumn HeaderText="Agent Code" DataField="agent_code" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                 <asp:BoundColumn HeaderText="Agent Name" DataField="agent_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                 <asp:BoundColumn HeaderText="Agent Balance" DataField="agent_current_balance" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                 <asp:BoundColumn HeaderText="Agent Type" DataField="agent_type" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                 <asp:BoundColumn HeaderText="Agent Parent Name" DataField="parent_name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                      </Columns>
                      </asp:DataGrid>
                 </div>
</asp:Content>
 


