﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using Visa;
using System.IO;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;


public partial class Email_Visa : System.Web.UI.Page
{
    protected VisaApplication application;
    protected string visaTypeName = string.Empty;
    protected int Adult = 0;
    protected int Child = 0;
    protected int Infant = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        int visaId;

        if (Request["Type"] == "Acknowledgement")
        {
            bool isnumerice = int.TryParse(Request["visaId"], out visaId);
            if (!isnumerice)
            {
                Response.End();
            }
            try
            {
                application = new VisaApplication(visaId);
                VisaType visaType = new VisaType(application.VisaTypeId);
                visaTypeName = visaType.VisaTypeName;
                foreach (VisaPassenger Passenger in application.PassengerList)
                {
                    int yearPass = (Passenger.CreatedOn.Year - Passenger.Dob.Year);
                    if (Passenger.CreatedOn.Month < Passenger.Dob.Month || (Passenger.CreatedOn.Month == Passenger.Dob.Month && Passenger.CreatedOn.Day < Passenger.Dob.Day))
                    {
                        yearPass--;
                    }
                    int year = yearPass;
                    if (year >= 12)
                    {
                        Adult++;
                    }
                    else if (year < 12 && year >= 2)
                    {
                        Child++;
                    }
                    else
                    {
                        Infant++;
                    }


                }
                Hashtable globalHashTable = new Hashtable();
                globalHashTable.Add("Header", "Visa Acknowledgment Receipt");
                globalHashTable.Add("Date of Issue", application.CreatedOn.Day.ToString() + '-' + String.Format("{0:MMM}", application.CreatedOn) + '-' + application.CreatedOn.Year);
                globalHashTable.Add("Trip ID", "VISA" + application.CreatedOn.ToString("yy") + String.Format("{0:MM}", application.CreatedOn) + application.VisaId);
                globalHashTable.Add("Adult", Adult);
                globalHashTable.Add("Child", Child);
                globalHashTable.Add("Infant", Infant);
                globalHashTable.Add("PaymentId", "");
                string message = "<p style='margin: 5px 0px; border-top: 1px solid rgb(195, 195, 195); padding-top: 10px; font-family: Arial,Sans-Serif; font-size: 13px; line-height: 18px;'> Thank you for choosing cozmotravel.Please find your TRIP ID which will be your future";
                message += "reference for any details or queries on your application, we will verify if the";
                message += " documents & details provided by you are in accordance with the UAE Immigration Law";
                message += " & Policies and update your application status on the MY TRIP Button which you find";
                message += "at the top right side of the homepage within one hour of submitting your application.";
                message += "To get the MY TRIP Button on the home page you need to sign in with your user name";
                message += " and password. User name would be your email ID, password is sent to your email.</p>";
                message += " <p style='margin: 5px 0px;  padding-top: 5px; font-family: Arial,Sans-Serif; font-size: 13px; line-height: 18px;'>If your application meets UAE Immigration Law and Policies your TRIP ID status would";
                message += "be shown as Eligible and payment can be made online by your credit card using the";
                message += "make payment link.</p>";

                globalHashTable.Add("Message", message);

                string paxDetails = string.Empty;
                int depositFeeFor = 0;
                int payDepositFee = 0;
                List<string> familyName = new List<string>();
                for (int i = 0; i < application.PassengerList.Count; i++)
                {

                    paxDetails += "<tr><td>" + application.PassengerList[i].Title + ' ' + application.PassengerList[i].Firstname + ' ' + application.PassengerList[i].LastName + "</td>";
                    paxDetails += "<td>" + visaTypeName + "</td>";
                    paxDetails += "<td>" + Math.Round((application.PassengerList[0].Price.NetFare), 2) + "</td>";
                    if (!familyName.Contains(application.PassengerList[i].LastName.ToLower()) || i == 0)
                    {
                        depositFeeFor++;
                        familyName.Add(application.PassengerList[i].LastName.ToLower());
                        payDepositFee = 1;
                        paxDetails += "<td>" + Math.Round(application.PassengerList[0].Price.OtherCharges, 2) + "</td>";
                    }
                    else
                    {
                        payDepositFee = 0;
                        paxDetails += "<td>0.00</td>";
                    }
                    paxDetails += "<td>" + Math.Round(application.PassengerList[0].Price.NetFare + (application.PassengerList[0].Price.OtherCharges * payDepositFee), 2) + "</td>";
                    paxDetails += "</tr>";
                }

                globalHashTable.Add("paxDetails", paxDetails);
                globalHashTable.Add("Total Amount", Math.Round((application.PassengerList[0].Price.NetFare * application.PassengerList.Count) + (application.PassengerList[0].Price.OtherCharges * depositFeeFor), 2));
                //Html code in text file for E-ticket
                string filePath = ConfigurationManager.AppSettings["EmailVisaReceipt"];// "d:\\visa-reciept.text";
                StreamReader sr = new StreamReader(filePath);
                //string contains the code of loop
                string loopString = string.Empty;
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    loopString += line;

                }
                List<string> toArray = new List<string>();
                string addressList = Request["addressList"];
                string[] addressArray = addressList.Split(',');
                for (int k = 0; k < addressArray.Length; k++)
                {
                    toArray.Add(addressArray[k]);
                }
                string from = new UserMaster((int)Settings.LoginInfo.UserID).Email;
                Email.Send(from, from, toArray, "Visa Acknowledgement for VisaId : VISA" + application.CreatedOn.ToString("yy") + String.Format("{0:MM}", application.CreatedOn) + application.VisaId, loopString, globalHashTable);
                Response.Write("Email Sent");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:Email_visa.aspx,Err:" + ex.Message, "");
            }

        }
        else if (Request["Type"] == "PaymentSlip")
        {
            bool isnumerice = int.TryParse(Request["visaId"], out visaId);
            if (!isnumerice)
            {
                Response.End();
            }
            try
            {
                application = new VisaApplication(visaId);
                VisaPaymentInfo PaymentInfo = new VisaPaymentInfo(visaId);
                VisaType visaType = new VisaType(application.VisaTypeId);
                visaTypeName = visaType.VisaTypeName;
                foreach (VisaPassenger Passenger in application.PassengerList)
                {
                    if (Passenger.Status != VisaPassengerStatus.NotEligible)
                    {

                        int yearPass = (Passenger.CreatedOn.Year - Passenger.Dob.Year);
                        if (Passenger.CreatedOn.Month < Passenger.Dob.Month || (Passenger.CreatedOn.Month == Passenger.Dob.Month && Passenger.CreatedOn.Day < Passenger.Dob.Day))
                        {
                            yearPass--;
                        }
                        int year = yearPass;
                        if (year >= 12)
                        {
                            Adult++;
                        }
                        else if (year < 12 && year >= 2)
                        {
                            Child++;
                        }
                        else
                        {
                            Infant++;
                        }
                    }

                }
                Hashtable globalHashTable = new Hashtable();
                globalHashTable.Add("Header", "Visa Payment Slip");
                globalHashTable.Add("Date of Issue", application.CreatedOn.Day.ToString() + '-' + String.Format("{0:MMM}", application.CreatedOn) + '-' + application.CreatedOn.Year);
                globalHashTable.Add("Trip ID", "VISA" + application.CreatedOn.ToString("yy") + String.Format("{0:MM}", application.CreatedOn) + application.VisaId);
                string PaymentId = string.Empty;
                PaymentId += " <div style='width:55%; float:left;'><span style='font-family:Arial, Sans-Serif; font-size:14px; color:#000; font-weight:bold; float:left; width:25%;'>Payment Id</span>";
                PaymentId += "<span style='font-family:Arial, Sans-Serif; font-size:14px; color:#000; float:left; width:30%;'>:" + PaymentInfo.PaymentId + "</span></div>";
                globalHashTable.Add("PaymentId", PaymentId);
                globalHashTable.Add("Adult", Adult);
                globalHashTable.Add("Child", Child);
                globalHashTable.Add("Infant", Infant);
                string message = "<p style='margin: 5px 0px; border-top: 1px solid rgb(195, 195, 195); padding-top: 10px; font-family: Arial,Sans-Serif; font-size: 13px; line-height: 18px;'> Thank you for choosing cozmotravel.Please find your Payment ID & Trip ID which will be your future";
                message += "reference for any details or queries on your application,";
                message += "Your application is under process. You can check your application status in MY TRIPS-My Visa Details. ";
                message += "You can download your Visa copy from MY TRIPS once your visa is approved from the respective UAE immigration.  </p>";

                globalHashTable.Add("Message", message);

                string paxDetails = string.Empty;
                int depositFeeFor = 0;
                int payDepositFee = 0;
                List<string> familyName = new List<string>();
                for (int i = 0; i < application.PassengerList.Count; i++)
                {
                    if (application.PassengerList[i].Status != VisaPassengerStatus.NotEligible)
                    {
                        paxDetails += "<tr><td>" + application.PassengerList[i].Title + ' ' + application.PassengerList[i].Firstname + ' ' + application.PassengerList[i].LastName + "</td>";
                        paxDetails += "<td>" + visaTypeName + "</td>";
                        paxDetails += "<td>" + Math.Round((application.PassengerList[0].Price.NetFare), 2) + "</td>";
                        if (!familyName.Contains(application.PassengerList[i].LastName.ToLower()) || i == 0)
                        {
                            depositFeeFor++;
                            familyName.Add(application.PassengerList[i].LastName.ToLower());
                            payDepositFee = 1;
                            paxDetails += "<td>" + Math.Round(application.PassengerList[0].Price.OtherCharges, 2) + "</td>";
                        }
                        else
                        {
                            payDepositFee = 0;
                            paxDetails += "<td>0.00</td>";
                        }
                        paxDetails += "<td>" + Math.Round(application.PassengerList[0].Price.NetFare + (application.PassengerList[0].Price.OtherCharges * payDepositFee), 2) + "</td>";
                        paxDetails += "</tr>";
                    }
                }

                globalHashTable.Add("paxDetails", paxDetails);
                globalHashTable.Add("Total Amount", Math.Round((application.PassengerList[0].Price.NetFare * application.PassengerList.Count) + (application.PassengerList[0].Price.OtherCharges * depositFeeFor), 2));
                //Html code in text file for E-ticket
                string filePath = ConfigurationManager.AppSettings["EmailVisaReceipt"];
                StreamReader sr = new StreamReader(filePath);
                //string contains the code of loop
                string loopString = string.Empty;
                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    loopString += line;

                }
                List<string> toArray = new List<string>();
                string addressList = Request["addressList"];
                string[] addressArray = addressList.Split(',');
                for (int k = 0; k < addressArray.Length; k++)
                {
                    toArray.Add(addressArray[k]);
                }
                string from = new UserMaster((int)Settings.LoginInfo.UserID).Email;
                Email.Send(from, from, toArray, "Visa Payment Receipt for VisaId :VISA" + application.CreatedOn.ToString("yy") + String.Format("{0:MM}", application.CreatedOn) + application.VisaId, loopString, globalHashTable);
                Response.Write("Email Sent");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "Page:Email_visa.aspx,Err:" + ex.Message, "");
            }
        }
    }
}
