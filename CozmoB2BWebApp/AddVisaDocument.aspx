﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true"
    Inherits="AddVisaDocument" Title="Cozmo Travels" ValidateRequest="false" Codebehind="AddVisaDocument.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <link rel="stylesheet" type="text/css" href="css/style.css" />
    
    
     <div> 
    <div class="col-md-6"> <h4> <asp:Label runat="server" ID="lblStatus" 
                                Text="Add Visa Document" ></asp:Label></h4> </div>
    
    <div class="col-md-6">
   
   
<asp:HyperLink ID="HyperLink1" class="fcol_blue pull-right" runat="server" NavigateUrl="~/VisaDocumentList.aspx">Go to Visa Document List</asp:HyperLink>
 </div>     
    
    <div class="clearfix"> </div> 
    </div>
    
    
        
    <div class="paramcon"> 
    
    
           <div class="margin-bottom-10"> 
    <div class="col-md-2"> <label>
                                    Agent<sup>*</sup>
                                </label></div>
    
    <div class="col-md-2"> <p>
                                
                                <span>
                                    <asp:DropDownList ID="ddlAgent" CssClass="form-control" runat="server"  >
                                    </asp:DropDownList></span><asp:RequiredFieldValidator ID="RequiredFieldValidator10" runat="server" ErrorMessage="Please select a Agent "
                                     ControlToValidate="ddlAgent"  InitialValue="Select Agent"></asp:RequiredFieldValidator>
                            </p></div>    
    <div class="col-md-2"><label>
                                    Document Type<sup style="color:Red">*</sup></label> </div>
    
    <div class="col-md-2"> <p>
                                
                                <span>
                                    <asp:TextBox CssClass="form-control" ID="txtDocumentType" runat="server"></asp:TextBox>&nbsp;</span>
                                    
                                    
                                    
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Please fill document type "
                                    ControlToValidate="txtDocumentType" Display="Dynamic"></asp:RequiredFieldValidator>
                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" 
                                    ControlToValidate="txtDocumentType" Display="Dynamic" 
                                    ErrorMessage="Special character is not allowed" 
                                    ValidationExpression="[0-9a-zA-Z' ']+$"></asp:RegularExpressionValidator>
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator7" runat="server" ErrorMessage="Please enter maximum 1000 character"
                                      ValidationExpression="[\S\s]{0,1000}" ControlToValidate="txtDocumentType" Display="Dynamic"></asp:RegularExpressionValidator>
                                    </p></div>   
                                    
                                        <div class="col-md-2"><label>
                                    Country<sup style="color:Red">*</sup></label> </div>  
                                        
                                        
                                            <div class="col-md-2"><p>
                                
                                <span>
                                    <asp:DropDownList ID="ddlCountry" runat="server" CssClass="form-control">
                                    </asp:DropDownList>
                                </span>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ErrorMessage="Please select country "
                                    ControlToValidate="ddlCountry" InitialValue="Select"></asp:RequiredFieldValidator>
                            </p> </div>   

    
    <div class="clearfix"> </div> 
    </div>



       <div class="margin-bottom-10"> 
    <div class="col-md-2"><label>
                                    File Type<sup style="color:Red">*</sup></label> </div>
    
    <div class="col-md-2"> <p>
                                
                                <span>
                                    <asp:TextBox ID="txtFileType" CssClass="form-control" runat="server"  ToolTip="Enter File Extensions separated  with Comma(,)"></asp:TextBox>
                                     <br /><em  title="Enter File Extensions separated  with Comma(,)" style="color:#7F7C7C"> Ex : jpeg,gif,png</em>
                                </span>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"  Display="Dynamic" ErrorMessage="Please enter file type "
                                    ControlToValidate="txtFileType" InitialValue=""></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator6" runat="server" ErrorMessage="Please enter maximum 500 character"
                                      ValidationExpression="[\S\s]{0,500}" ControlToValidate="txtFileType" Display="Dynamic"></asp:RegularExpressionValidator>
                            </p></div>   
 
    <div class="col-md-2"><label>
                                   File Size<sup style="color:Red">*</sup></label> </div>
    
    <div class="col-md-2"> <p>
                                
                                <span>
                                   <asp:TextBox  CssClass="form-control" ID="txtFileSize" runat="server" MaxLength="6" ></asp:TextBox>
                                   <br /><em style="color:#7F7C7C"> File size in kb</em>
                                </span>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server" Display="Dynamic" ErrorMessage="Please enter image File size"
                                    ControlToValidate="txtFileSize" InitialValue=""></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator5" runat="server" Display="Dynamic" ControlToValidate="txtFileSize"
                                        ErrorMessage="Please fill numeric value " ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                            
                            </p></div>    

    <div class="col-md-2"> <label>
                                   Height<sup style="color:Red">*</sup></label></div>
    
    <div class="col-md-2"><p>
                                
                                <span>
                                    <asp:TextBox CssClass="form-control" ID="txtheight" runat="server" MaxLength="6" ></asp:TextBox>
                                      <br /><em style="color:#7F7C7C"> Image height in pixels</em>
                                </span>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" Display="Dynamic" ErrorMessage="Please enter image height "
                                    ControlToValidate="txtheight" InitialValue=""></asp:RequiredFieldValidator>
                                     <asp:RegularExpressionValidator ID="RegularExpressionValidator4" runat="server" Display="Dynamic" ControlToValidate="txtheight"
                                        ErrorMessage="Please fill numeric value " ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                            
                            </p> </div>    
    
    <div class="clearfix"> </div> 
    </div>
    
    
    
    
    
           <div class="margin-bottom-10"> 
    <div class="col-md-2"><label>
                                   Width<sup style="color:Red">*</sup></label> </div>
    
    <div class="col-md-2"><p>
                                
                                <span>
                                    <asp:TextBox CssClass="form-control" ID="txtWidth" runat="server" MaxLength="6" ></asp:TextBox>
                                    <br /><em style="color:#7F7C7C"> Image width in pixels</em>
                                </span>
                                 <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" Display="Dynamic" ErrorMessage="Please enter image width"
                                    ControlToValidate="txtWidth" InitialValue=""></asp:RequiredFieldValidator>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator3" runat="server" Display="Dynamic" ControlToValidate="txtWidth"
                                        ErrorMessage="Please fill numeric value " ValidationExpression="^[0-9]*"></asp:RegularExpressionValidator>
                            </p> </div>   
 
 
    
    <div class="clearfix"> </div> 
    </div>
    
    
    
          <div class="margin-bottom-10"> 
    <div class="col-md-2"><label>
                                    Description<sup style="color:Red">*</sup></label> </div>
    
    <div class="col-md-6"> 
                                
                               
                                    <asp:TextBox  ID="txtDescription" runat="server" TextMode="MultiLine" Height="180px" Width="100%"></asp:TextBox>
                                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Please fill Description "
                                    ControlToValidate="txtDescription" Display="Dynamic"></asp:RequiredFieldValidator>
                             
        
                                    
                              </div>   

    <div class="clearfix"> </div> 
    </div>
    
    
         <div class="margin-bottom-10"> 
    <div class="col-md-2"> </div>
    
    <div class="col-md-2"> <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn but_b"
                                        OnClick="btnSave_Click" /></div>   
 
    
    <div class="clearfix"> </div> 
    </div>
    
    
    </div>
    
   
</asp:Content>
