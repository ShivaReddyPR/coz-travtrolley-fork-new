﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using CT.BookingEngine;
using System.Collections.Generic;
using CT.Core;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.AccountingEngine;
using System.IO;
using CT.MetaSearchEngine;
using CZInventory;
using CCA.Util;
using System.Collections.Specialized;
using System.Linq;

public partial class HTLBookingConfirmUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected HotelItinerary itinerary = new HotelItinerary();
    protected PriceType priceType;
    protected decimal amountToCompare = 0;
    protected AgentMaster agency;
    protected string errorMessage;
    protected decimal rateOfExchange = 1;
    protected decimal markup = 0, total = 0, discount = 0;
    protected HotelRequest request;
    protected BookingResponse bookRes = new BookingResponse();
    protected Dictionary<string, string> CancellationData = new Dictionary<string, string>();
    protected string warningMsg = "";
    protected string cancelData = "", remarks = "";
    protected Dictionary<int, HotelItinerary> UserBookings = new Dictionary<int, HotelItinerary>();
    protected int decimalPoint = 2;
    protected decimal charges = 0;
    protected decimal outPutVat = 0m;
    protected LocationMaster location = new LocationMaster();
    
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            string paymentId = string.Empty;
            string trackId = string.Empty;
            string orderId = string.Empty;
            bool isPaymentDone = false;
            if (Request["ErrorPG"] == null || Request["ErrorPG"] != "True")  //Payment Time Error Checking
            {
                if (Request.Form["encResp"] != null) //Payment success checking
                {
                    string workingKey = ConfigurationManager.AppSettings["CCA_Encrypt_Key"];
                    CCACrypto ccaCrypto = new CCACrypto();
                    string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], workingKey);
                    NameValueCollection Params = new NameValueCollection();
                    string[] segments = encResponse.Split('&');
                    foreach (string seg in segments)
                    {
                        string[] parts = seg.Split('=');
                        if (parts.Length > 0)
                        {
                            string Key = parts[0].Trim();
                            string Value = parts[1].Trim();
                            Params.Add(Key, Value);
                        }
                    }
                    Audit.Add(EventType.CCAvenue, Severity.Normal, 1, "HTL CCAvenue URL response " + encResponse, "0");
                    if (Params["order_status"] == "Success")
                    {
                        Audit.Add(EventType.CCAvenue, Severity.Normal, 1, "Alert: Payment Susscess. Payment Id " + Params["tracking_id"].ToString(), "0");

                        orderId = Params["order_id"];
                        paymentId = Params["tracking_id"];
                        isPaymentDone = true;
                    }
                    else
                    {
                        itinerary = Session["hItinerary"] as HotelItinerary;
                        Audit.Add(EventType.CCAvenue, Severity.Normal, 1, "Alert: Payment Faluire. Payment Id " + Params["tracking_id"].ToString(), "0");
                        string errorMessage = string.Empty;
                        if (!string.IsNullOrEmpty(Params["failure_message"]))
                        {
                            errorMessage = Params["failure_message"];
                        }
                        else if (!string.IsNullOrEmpty(Params["status_message"]))
                        {
                            errorMessage = Params["status_message"];
                        }
                        else
                        {
                            errorMessage = "Payment declined due to invalid details or due to technical error.";
                        }
                        lblError.Text = errorMessage;
                        PaymentMultiView.ActiveViewIndex = 1;
                    }
                    //PAyment is sucess Booking Flow is started  Added by brahmam
                    if (isPaymentDone)
                    {
                        HotelBooking(CT.TicketReceipt.BusinessLayer.PaymentMode.Card, paymentId);
                    }
                }
                else
                {
                    if (Settings.LoginInfo != null)
                    {
                        if (Session["hItinerary"] != null)
                        {
                            decimal asvAmount = 0;
                            itinerary = Session["hItinerary"] as HotelItinerary;
                            if (Session["Markup"] != null)  //PageLevelMarkup
                            {
                                markup = Convert.ToDecimal(Session["Markup"]);
                                asvAmount = Convert.ToDecimal(Session["Markup"]);
                            }
                            rateOfExchange = (Settings.LoginInfo.ExchangeRate > 0 ? Settings.LoginInfo.ExchangeRate : 1);

                            HotelSource hSource = new HotelSource();
                            hSource.Load(itinerary.Source.ToString());
                            //try
                            //{

                            if (Settings.LoginInfo.IsOnBehalfOfAgent)
                            {
                                decimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                                agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                                location = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                            }
                            else
                            {
                                decimalPoint = Settings.LoginInfo.DecimalValue;
                                agency = new AgentMaster(Settings.LoginInfo.AgentId);
                            }


                            decimal totMarkup = 0, totalHtAmount = 0; ;
                            foreach (HotelRoom room in itinerary.Roomtype)
                            {
                                if (hSource.FareTypeId == FareType.Published)
                                {
                                    total += ((room.Price.PublishedFare)) * room.Price.RateOfExchange;
                                }
                                else
                                {
                                    total += Math.Round((room.Price.NetFare + room.Price.Markup), agency.DecimalValue);
                                    totMarkup += room.Price.Markup;
                                }
                            }
                            markup += Math.Ceiling(total);



                            if (!IsPostBack)
                            {                                
                                //Here IN Means India Users IF India User loggedIn we need to calculate GST otherwise need to calculate VAT 
                                if ((Settings.LoginInfo.IsOnBehalfOfAgent && location.CountryCode == "IN")) 
                                {
                                    foreach (HotelRoom room in itinerary.Roomtype) //Room Wsie Gst Calculations
                                    {
                                        List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                                        decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, (room.Price.Markup), Settings.LoginInfo.OnBehalfAgentLocation);
                                        room.Price.GSTDetailList = new List<GSTTaxDetail>();
                                        room.Price.GSTDetailList = gstTaxList;
                                        outPutVat += Math.Round(gstAmount, Settings.LoginInfo.OnBehalfAgentDecimalValue);
                                        room.Price.OutputVATAmount = Math.Round(gstAmount, Settings.LoginInfo.OnBehalfAgentDecimalValue);
                                    }
                                }
                                else if(!Settings.LoginInfo.IsOnBehalfOfAgent && Settings.LoginInfo.LocationCountryCode=="IN")
                                {
                                    foreach (HotelRoom room in itinerary.Roomtype) //Room Wsie Gst Calculations
                                    {
                                        List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                                        decimal gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, (room.Price.Markup), Settings.LoginInfo.LocationID);
                                        room.Price.GSTDetailList = new List<GSTTaxDetail>();
                                        room.Price.GSTDetailList = gstTaxList;
                                        outPutVat += Math.Round(gstAmount, Settings.LoginInfo.DecimalValue);
                                        room.Price.OutputVATAmount = Math.Round(gstAmount, Settings.LoginInfo.DecimalValue);
                                    }
                                }
                                else
                                {
                                    //Out put Vat Calucation
                                    PriceTaxDetails taxDetails = itinerary.Roomtype[0].Price.TaxDetails;
                                    if (Settings.LoginInfo.AgentType == AgentType.Agent)
                                    {
                                        totalHtAmount = Math.Ceiling(markup); //here markup means total with Pagelevel markup
                                        totMarkup = Math.Round((totMarkup + asvAmount), Settings.LoginInfo.DecimalValue);
                                    }
                                    else
                                    {
                                        totalHtAmount = Math.Round(Math.Ceiling(total), Settings.LoginInfo.DecimalValue);
                                        totMarkup = Math.Round(totMarkup, Settings.LoginInfo.DecimalValue);
                                    }
                                    if (taxDetails != null && taxDetails.OutputVAT != null)
                                    {
                                        outPutVat = taxDetails.OutputVAT.CalculateVatAmount(totalHtAmount, totMarkup, Settings.LoginInfo.DecimalValue);
                                        foreach (HotelRoom room in itinerary.Roomtype)
                                        {
                                            if (room.Price.AccPriceType == PriceType.NetFare)
                                            {
                                                room.Price.OutputVATAmount = Math.Round(outPutVat / itinerary.Roomtype.Length, Settings.LoginInfo.DecimalValue);
                                            }
                                        }
                                    }
                                }

                                //added by brahmam payment time should be dropdown enabled
                                if (agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Card)
                                {
                                    ddlPayment.Enabled = true;
                                    //Loading Cradcharges
                                    try
                                    {
                                        DataTable dtChrges = AgentCreditCardCharges.GetList();
                                        DataView dv = dtChrges.DefaultView;
                                        dv.RowFilter = "Productid IN('" + 2 + "') AND Agentid IN('" + agency.ID + "') AND PGSource IN('" + (int)CT.AccountingEngine.PaymentGatewaySource.CCAvenue + "')";
                                        dtChrges = dv.ToTable();
                                        if (dtChrges != null && dtChrges.Rows.Count > 0)
                                        {
                                            charges = Convert.ToDecimal(dtChrges.Rows[0]["PGCharge"]);
                                        }
                                    }
                                    catch { }
                                }
                                else if (agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Credit_Limit)
                                    ddlPayment.Enabled = false;

                                List<HotelPenality> penaltyInfo = new List<HotelPenality>();

                                SearchEngine se = new SearchEngine();

                                MetaSearchEngine mse = new MetaSearchEngine(Session["cSessionId"].ToString());
                                mse.SettingsLoginInfo = Settings.LoginInfo;
                                //mse.DecimalPoint = agency.DecimalValue;
                                CT.BookingEngine.GDS.DOTWApi dotw = new CT.BookingEngine.GDS.DOTWApi(Session["cSessionId"].ToString());
                                if (bookRes.ConfirmationNo == null || bookRes.ConfirmationNo.Length == 0)
                                {
                                    if (itinerary.Source == HotelBookingSource.DOTW)
                                    {
                                        CancellationData = mse.GetCancellationDetails(itinerary, ref penaltyInfo, true, HotelBookingSource.DOTW);
                                        if (itinerary.HotelCancelPolicy.Length == 0)
                                        {
                                            itinerary.HotelCancelPolicy = CancellationData["CancelPolicy"];
                                            if (itinerary.LastCancellationDate == DateTime.MinValue)
                                            {
                                                itinerary.LastCancellationDate = itinerary.StartDate;
                                            }
                                        }
                                    }
                                    else if (itinerary.Source == HotelBookingSource.HotelConnect)
                                    {
                                        if (Session["SelectedHotel"] != null)
                                        {
                                            HotelSearchResult result = Session["SelectedHotel"] as HotelSearchResult;
                                            List<string> Rooms = new List<string>();

                                            foreach (HotelRoom room in itinerary.Roomtype)
                                            {
                                                Rooms.Add(room.RoomTypeCode);
                                            }

                                            CancellationData = se.GetCancellationPolicy(ref result, request, Rooms);
                                        }
                                    }
                                    //else if (itinerary.Source == HotelBookingSource.RezLive)//modified by brahmam   21/09/2015  And here  checking price validation and cancellation policy details
                                    //{
                                    //    if (string.IsNullOrEmpty(itinerary.SpecialRequest)) //Page refresh time again no need send booking request
                                    //    {
                                    //        try
                                    //        {
                                    //            request = Session["req"] as HotelRequest;
                                    //            //Dictionary<string, string> preBookinginfo = new Dictionary<string, string>();
                                    //            RezLive.XmlHub rezAPI = new RezLive.XmlHub(Session["cSessionId"].ToString());
                                    //            if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                    //            {
                                    //                rezAPI.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                                    //                rezAPI.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                                    //                rezAPI.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                                    //            }
                                    //            else
                                    //            {
                                    //                rezAPI.AgentCurrency = Settings.LoginInfo.Currency;
                                    //                rezAPI.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                                    //                rezAPI.DecimalPoint = Settings.LoginInfo.DecimalValue;
                                    //            }
                                    //            decimal price = 0;
                                    //            foreach (HotelRoom room in itinerary.Roomtype)
                                    //            {
                                    //                price += room.Price.SupplierPrice;
                                    //            }
                                    //            CancellationData = rezAPI.GetPreBooking(itinerary, price);

                                    //            itinerary.HotelCancelPolicy = CancellationData["CancelPolicy"];
                                    //            itinerary.LastCancellationDate = Convert.ToDateTime(CancellationData["lastCancellationDate"]);
                                    //            itinerary.HotelPolicyDetails = CancellationData["HotelPolicy"];
                                    //            itinerary.SpecialRequest = "True"; //Page refresh time again no need send booking request
                                    //            for (int i = 0; i < itinerary.Roomtype.Length; i++)
                                    //            {
                                    //                itinerary.Roomtype[i].RatePlanCode = CancellationData["BookingKey"];
                                    //            }

                                    //            bool status = Utility.ToBoolean(CancellationData["Status"]);

                                    //            if (!status)
                                    //            {
                                    //                string error = Utility.ToString(CancellationData["Error"]);
                                    //                if (error == "PriceChanged")
                                    //                {
                                    //                    lblMessage.Text = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                                    //                }
                                    //                else
                                    //                {
                                    //                    lblMessage.Text = error;
                                    //                }
                                    //                Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderPageLoad");
                                    //                PaymentMultiView.ActiveViewIndex = 1;
                                    //                lblError.Text = lblMessage.Text;
                                    //                return;
                                    //            }
                                    //        }
                                    //        catch
                                    //        {

                                    //            lblMessage.Text = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                                    //            Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderPageLoad");
                                    //            PaymentMultiView.ActiveViewIndex = 1;
                                    //            lblError.Text = lblMessage.Text;
                                    //            return;
                                    //        }
                                    //    }
                                    //    else
                                    //    {
                                    //        CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                                    //        CancellationData.Add("HotelPolicy", itinerary.HotelPolicyDetails);
                                    //    }

                                    //    //decimal price = 0;
                                    //    //foreach (HotelRoom room in itinerary.Roomtype)
                                    //    //{
                                    //    //    price += room.Price.SupplierPrice;
                                    //    //}
                                    //    //request = Session["req"] as HotelRequest;

                                    //    //RezLive.XmlHub rezAPI = new RezLive.XmlHub(Session["cSessionId"].ToString());
                                    //    //if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                    //    //{
                                    //    //    rezAPI.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                                    //    //    rezAPI.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                                    //    //    rezAPI.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                                    //    //}
                                    //    //else
                                    //    //{
                                    //    //    rezAPI.AgentCurrency = Settings.LoginInfo.Currency;
                                    //    //    rezAPI.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                                    //    //    rezAPI.DecimalPoint = Settings.LoginInfo.DecimalValue;
                                    //    //}
                                    //    //CancellationData = rezAPI.GetCancellationInfo(itinerary, request, price);
                                    //    //if (CancellationData.Count > 0)
                                    //    //{
                                    //    //    itinerary.HotelCancelPolicy = CancellationData["CancelPolicy"];
                                    //    //    itinerary.LastCancellationDate = Convert.ToDateTime(CancellationData["lastCancellationDate"]);
                                    //    //}
                                    //}
                                    else if (itinerary.Source == HotelBookingSource.LOH)
                                    {
                                        LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine(Session["cSessionId"].ToString());
                                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                        {
                                            jxe.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                                            jxe.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                                            jxe.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                                        }
                                        else
                                        {
                                            jxe.AgentCurrency = Settings.LoginInfo.Currency;
                                            jxe.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                                            jxe.DecimalPoint = Settings.LoginInfo.DecimalValue;
                                        }
                                        decimal supplierPrice = 0;

                                        foreach (HotelRoom room in itinerary.Roomtype)
                                        {
                                            supplierPrice += room.Price.SupplierPrice;
                                        }
                                        //cancelation policy is coming based on the nationality
                                        CancellationData = jxe.GetCancellationDetails(itinerary.HotelCode, itinerary.Roomtype[0].RatePlanCode, itinerary.StartDate, itinerary.EndDate, itinerary.SequenceNumber, supplierPrice, itinerary.PassengerNationality);
                                        itinerary.HotelCancelPolicy = CancellationData["CancelPolicy"];
                                        itinerary.LastCancellationDate = Convert.ToDateTime(CancellationData["lastCancellationDate"]);

                                        if (CancellationData.ContainsKey("RatePlanCode"))
                                        {
                                            foreach (HotelRoom room in itinerary.Roomtype)
                                            {
                                                room.RatePlanCode = CancellationData["RatePlanCode"];
                                            }
                                        }
                                    }
                                    else if (itinerary.Source == HotelBookingSource.HotelBeds)  //Added by brahmam 26.09.2014
                                    {
                                        CT.BookingEngine.GDS.HotelBeds hBeds = new CT.BookingEngine.GDS.HotelBeds();
                                        hBeds.sessionId = Session["cSessionId"].ToString();
                                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                        {
                                            hBeds.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                                            hBeds.AgentExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                                            hBeds.AgentDecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                                        }
                                        else
                                        {
                                            hBeds.AgentCurrency = Settings.LoginInfo.Currency;
                                            hBeds.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                                            hBeds.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                                        }
                                        CancellationData = hBeds.GetHotelChargeCondition(itinerary, ref penaltyInfo, true);
                                        if (CancellationData.Count > 0)
                                        {
                                            itinerary.LastCancellationDate = Convert.ToDateTime(CancellationData["lastCancellationDate"]);
                                        }
                                    }
                                    else if (itinerary.Source == HotelBookingSource.GTA || itinerary.Source == HotelBookingSource.Agoda)  //Added by brahmam 26.09.2014
                                    {
                                        //CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();
                                        //if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                        //{
                                        //    gtaApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                                        //    gtaApi.AgentExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                                        //    gtaApi.AgentDecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                                        //}
                                        //else
                                        //{
                                        //    gtaApi.AgentCurrency = Settings.LoginInfo.Currency;
                                        //    gtaApi.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                                        //    gtaApi.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                                        //}
                                        //CancellationData = gtaApi.GetHotelChargeCondition(itinerary, ref penaltyInfo, true);
                                        //if (CancellationData.Count > 0)
                                        //{
                                        //    itinerary.LastCancellationDate = Convert.ToDateTime(CancellationData["lastCancellationDate"]);
                                        //}
                                        CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                                    }
                                    else if (itinerary.Source == HotelBookingSource.Yatra)  //Added by somasekhar on 04/09/2018
                                    {                                        
                                        CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                                    }
                                    else if (itinerary.Source == HotelBookingSource.OYO)  //Added by somasekhar for OYO to show cancellation policy
                                    {
                                        CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                                        for (int i = 0; i < itinerary.Roomtype.Length; i++)
                                        {
                                            if (!string.IsNullOrEmpty(itinerary.HotelPolicyDetails))
                                            {
                                                itinerary.HotelPolicyDetails = itinerary.HotelPolicyDetails + "|" + itinerary.Roomtype[i].EssentialInformation;
                                            }
                                            else
                                            {
                                                itinerary.HotelPolicyDetails = itinerary.Roomtype[i].EssentialInformation;
                                            }
                                        }
                                        CancellationData.Add("HotelPolicy", itinerary.HotelPolicyDetails);
                                    }
                                    //For Miki
                                    #region PriceCheckingForMikiSubProd
                                    else if (itinerary.Source == HotelBookingSource.Miki)
                                    {
                                        if (string.IsNullOrEmpty(itinerary.SpecialRequest))
                                        {
                                            try
                                            {

                                                request = Session["req"] as HotelRequest;
                                                HotelSearchResult[] hotelSearchResult = new HotelSearchResult[0];
                                                CT.BookingEngine.GDS.MikiApi mikiApi = new CT.BookingEngine.GDS.MikiApi(Session["cSessionId"].ToString(),Settings.LoginInfo.LocationCountryCode);
                                                //request.HotelCode = itinerary.HotelCode;
                                                //request.RoomTypeCode = itinerary.Roomtype[0].RoomTypeCode.Split('|')[5].ToString();
                                                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                                {
                                                    mikiApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                                                    mikiApi.AgentExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                                                    mikiApi.AgentDecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                                                }
                                                else
                                                {
                                                    mikiApi.AgentCurrency = Settings.LoginInfo.Currency;
                                                    mikiApi.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                                                    mikiApi.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                                                }
                                                try
                                                {
                                                    Dictionary<string, string> activeSources = AgentMaster.GetActiveSourcesByProduct((int)ProductType.Hotel);

                                                    if (activeSources != null && activeSources.Count > 0)
                                                    {
                                                        mikiApi.SourceCountryCode = activeSources[HotelBookingSource.Miki.ToString()];
                                                    }
                                                    else
                                                    {
                                                        mikiApi.SourceCountryCode = "AE";// TODO Ziyad
                                                    }
                                                }
                                                catch
                                                {
                                                    mikiApi.SourceCountryCode = "AE";
                                                }
                                                //send paxdomicile country code for item search
                                                DOTWCountry dotwApi = new DOTWCountry();
                                                Dictionary<string, string> Countries = dotwApi.GetAllCountries();
                                                request.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[request.PassengerNationality]);


                                                //Getting updated Item Info For selected Hotel
                                                hotelSearchResult = mikiApi.GetHotelAvailability(request.CopyByValue(), 0, string.Empty, itinerary.HotelCode);

                                                itinerary.LastCancellationDate = itinerary.StartDate;
                                                itinerary.PropertyType = hotelSearchResult[0].PropertyType; // PropertyType use for get Hotel Alert Message
                                                itinerary.SpecialRequest = "True";

                                                decimal oldPrice = 0;
                                                decimal newPrice = 0;
                                                string updatedCancelPolicy = string.Empty;
                                                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                                                {
                                                    oldPrice += itinerary.Roomtype[i].Price.NetFare;
                                                    for (int k = 0; k < hotelSearchResult.Length; k++)
                                                    {

                                                        for (int j = 0; j < hotelSearchResult[k].RoomDetails.Length; j++)
                                                        {
                                                            if (itinerary.Roomtype[i].RoomTypeCode == hotelSearchResult[k].RoomDetails[j].RoomTypeCode)
                                                            {
                                                                newPrice += hotelSearchResult[k].RoomDetails[j].SellingFare;
                                                                updatedCancelPolicy += hotelSearchResult[k].RoomDetails[j].CancellationPolicy + "|"; //getting updated cancel policy
                                                                //getting latest rateplancode after Re-Request For Hotel (Price checking Purpose)
                                                                itinerary.Roomtype[i].RatePlanCode = hotelSearchResult[k].RoomDetails[j].RatePlanCode;
                                                                break;
                                                            }
                                                        }
                                                    }
                                                }
                                                if (oldPrice != newPrice)
                                                {
                                                    lblMessage.Text = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                                                    Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderPageLoad");
                                                    PaymentMultiView.ActiveViewIndex = 1;
                                                    lblError.Text = lblMessage.Text;
                                                    return;
                                                }
                                                itinerary.HotelCancelPolicy = updatedCancelPolicy;
                                                CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                                                CancellationData.Add("HotelPolicy", itinerary.PropertyType);// PropertyType use for get Hotel Alert Message
                                            }
                                            catch
                                            {
                                                warningMsg = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                                                // Need to throw Error here
                                                throw new Exception(warningMsg);
                                            }

                                        }
                                        else
                                        {
                                            CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                                            CancellationData.Add("HotelPolicy", itinerary.HotelPolicyDetails);

                                        }
                                    }

                                    #endregion
                                    //Loading cancelation policy added by brahmam 27.06.2016
                                    else if (itinerary.Source == HotelBookingSource.WST)
                                    {
                                        if (string.IsNullOrEmpty(itinerary.HotelCancelPolicy))
                                        {
                                            CT.BookingEngine.GDS.WST wstApi = new CT.BookingEngine.GDS.WST();
                                            if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                            {
                                                wstApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                                                wstApi.AgentExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                                                wstApi.AgentDecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                                            }
                                            else
                                            {
                                                wstApi.AgentCurrency = Settings.LoginInfo.Currency;
                                                wstApi.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                                                wstApi.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                                            }
                                            CancellationData = wstApi.GetHotelChargeCondition(itinerary, ref penaltyInfo, true);
                                        }
                                        else
                                        {
                                            CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                                            CancellationData.Add("HotelPolicy", itinerary.HotelPolicyDetails);
                                        }
                                    }
                                    #region Price Validation
                                    else if (itinerary.Source == HotelBookingSource.TBOHotel)//Added by brahmam   26.11.2015  And here  checking price validation and cancellation policy details
                                    {
                                        if (string.IsNullOrEmpty(itinerary.SpecialRequest)) //Page refresh time again no need send booking request
                                        {
                                            try
                                            {
                                                HotelItinerary hitinerary = Session["hItinerary"] as HotelItinerary;
                                                TBOHotel.HotelV10 tboHotel = new TBOHotel.HotelV10();
                                                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                                {
                                                    tboHotel.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                                                    tboHotel.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                                                    tboHotel.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                                                }
                                                else
                                                {
                                                    tboHotel.AgentCurrency = Settings.LoginInfo.Currency;
                                                    tboHotel.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                                                    tboHotel.DecimalPoint = Settings.LoginInfo.DecimalValue;
                                                }
                                                DOTWCountry dotwApi = new DOTWCountry();
                                                Dictionary<string, string> Countries = dotwApi.GetAllCountries();
                                                hitinerary.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[itinerary.PassengerNationality]);
                                                CancellationData = tboHotel.BlockRooms(hitinerary);
                                                bool status = Utility.ToBoolean(CancellationData["Status"]);
                                                if (!status)
                                                {
                                                    string error = Utility.ToString(CancellationData["Error"]);
                                                    if (error == "PriceChanged")
                                                    {
                                                        lblMessage.Text = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                                                    }
                                                    else
                                                    {
                                                        lblMessage.Text = error;
                                                    }
                                                    Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderPageLoad");
                                                    PaymentMultiView.ActiveViewIndex = 1;
                                                    lblError.Text = lblMessage.Text;
                                                    return;
                                                }
                                                else
                                                {
                                                    itinerary.HotelCancelPolicy = CancellationData["CancelPolicy"];
                                                    itinerary.LastCancellationDate = Convert.ToDateTime(CancellationData["lastCancellationDate"]);
                                                    itinerary.HotelPolicyDetails = CancellationData["HotelPolicy"];
                                                    itinerary.PropertyType = CancellationData["traceId"];
                                                    itinerary.SpecialRequest = "True"; //Page refresh time again no need send booking request
                                                }
                                            }
                                            catch
                                            {

                                                lblMessage.Text = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                                                Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderPageLoad");
                                                PaymentMultiView.ActiveViewIndex = 1;
                                                lblError.Text = lblMessage.Text;
                                                return;
                                            }
                                        }
                                        else
                                        {
                                            CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                                            CancellationData.Add("HotelPolicy", itinerary.HotelPolicyDetails);
                                        }
                                    }
                                    else if (itinerary.Source == HotelBookingSource.JAC)
                                    {
                                        CT.BookingEngine.GDS.JAC jacApi = new CT.BookingEngine.GDS.JAC();
                                        jacApi.SessionId = Session["cSessionId"].ToString();
                                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                        {
                                            jacApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                                            jacApi.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                                            jacApi.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                                        }
                                        else
                                        {
                                            jacApi.AgentCurrency = Settings.LoginInfo.Currency;
                                            jacApi.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                                            jacApi.DecimalPoint = Settings.LoginInfo.DecimalValue;
                                        }
                                        CancellationData = jacApi.GetHotelChargeCondition(itinerary);
                                        if (CancellationData.Count > 0)
                                        {
                                            itinerary.SequenceNumber = CancellationData["PreBookingToken"]; //PreBookingToken in Booking time need to send
                                        }
                                    }
                                    else if (itinerary.Source == HotelBookingSource.EET)
                                    {
                                        CT.BookingEngine.GDS.EET eetApi = new CT.BookingEngine.GDS.EET(Session["cSessionId"].ToString());
                                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                                        {
                                            eetApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                                            eetApi.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                                            eetApi.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                                        }
                                        else
                                        {
                                            eetApi.AgentCurrency = Settings.LoginInfo.Currency;
                                            eetApi.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                                            eetApi.DecimalPoint = Settings.LoginInfo.DecimalValue;
                                        }
                                        decimal supplierPrice = 0;

                                        foreach (HotelRoom room in itinerary.Roomtype)
                                        {
                                            supplierPrice += room.Price.SupplierPrice;
                                        }
                                        //cancelation policy is coming based on the nationality
                                        CancellationData = eetApi.GetCancellationDetails(itinerary.HotelCode, itinerary.Roomtype[0].RatePlanCode, itinerary.StartDate, itinerary.EndDate, itinerary.SequenceNumber, supplierPrice, itinerary.PassengerNationality);
                                        itinerary.HotelCancelPolicy = CancellationData["CancelPolicy"];
                                        itinerary.LastCancellationDate = Convert.ToDateTime(CancellationData["lastCancellationDate"]);

                                        if (CancellationData.ContainsKey("RatePlanCode"))
                                        {
                                            foreach (HotelRoom room in itinerary.Roomtype)
                                            {
                                                room.RatePlanCode = CancellationData["RatePlanCode"];
                                            }
                                        }
                                    }
                                    #region GRN Cancellation Policy
                                    else if (itinerary.Source == HotelBookingSource.GRN)
                                    {
                                        CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                                        for (int i = 0; i < itinerary.Roomtype.Length; i++)
                                        {
                                            if (!string.IsNullOrEmpty(itinerary.HotelPolicyDetails))
                                            {
                                                itinerary.HotelPolicyDetails = itinerary.HotelPolicyDetails + "|" + itinerary.Roomtype[i].EssentialInformation;
                                            }
                                            else
                                            {
                                                itinerary.HotelPolicyDetails = itinerary.Roomtype[i].EssentialInformation;
                                            }
                                        }
                                        CancellationData.Add("HotelPolicy", itinerary.HotelPolicyDetails);
                                    }
                                    #endregion
                                    #endregion

                                }
                                else
                                {
                                    CancellationData.Add("CancelPolicy", itinerary.HotelCancelPolicy);
                                }
                                foreach (KeyValuePair<string, string> pair in CancellationData)
                                {
                                    switch (pair.Key)
                                    {
                                        case "lastCancellationDate":
                                            try
                                            {
                                                if (itinerary.Source == HotelBookingSource.TBOHotel || itinerary.Source == HotelBookingSource.WST || itinerary.Source == HotelBookingSource.JAC)
                                                {
                                                    itinerary.LastCancellationDate = Convert.ToDateTime(pair.Value);
                                                }
                                                else if (itinerary.Source != HotelBookingSource.RezLive)
                                                {
                                                    int days = Convert.ToInt32(pair.Value);
                                                }
                                            }
                                            catch { }
                                            break;
                                        case "CancelPolicy":
                                            cancelData = pair.Value;
                                            itinerary.HotelCancelPolicy = pair.Value;
                                            break;
                                        case "HotelPolicy":
                                            remarks = pair.Value;
                                            itinerary.HotelPolicyDetails = pair.Value;
                                            break;
                                        case "QUOTE_CHANGED":
                                            warningMsg = "Price/Quotation has been revised for this Hotel. Please search again to book this Hotel.";
                                            break;
                                        case "PRICE_CHANGED":
                                            warningMsg = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                                            break;
                                        case "NOT_AVAILABILITY":
                                            warningMsg = "This particular room(s) is not available for this Hotel. Please search again to book this Hotel.";
                                            break;
                                        case "NOT_POSSIBLE":
                                            warningMsg = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                                            break;
                                    }
                                }

                                if (itinerary.Source != HotelBookingSource.LOH && itinerary.Source != HotelBookingSource.HotelConnect && itinerary.Source != HotelBookingSource.EET)
                                {
                                    foreach (HotelRoom room in itinerary.Roomtype)
                                    {
                                        if (room.PreviousFare > 0 && room.PreviousFare < room.Price.NetFare)
                                        {
                                            warningMsg += "Price has been revised for this Hotel. Please search again to book this Hotel.";
                                            break;
                                        }

                                        if (room.NonRefundable || room.PaymentMode == "CC" || room.RatePlanCode == "")
                                        {
                                            warningMsg += "This Hotel cannot be booked. Please search again to book another Hotel.";
                                        }

                                    }
                                }
                                hdnWarning.Value = warningMsg;
                            }
                        }
                        else
                        {
                            Response.Redirect("HotelSearch.aspx?source=Hotel");
                        }
                    }
                    else
                    {
                        Response.Redirect("AbandonSession.aspx");
                    }
                }
            }
            else
            {
                itinerary = Session["hItinerary"] as HotelItinerary;
                //card time  getting any issue showing below message
                lblError.Text = "Unable to communicate to paymentgetway";
                PaymentMultiView.ActiveViewIndex = 1;
            }
        }
        catch (Exception ex)
        {
            Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderPageLoad");
            PaymentMultiView.ActiveViewIndex = 1;
            lblError.Text = ex.Message;

        }

    }

    protected void imgMakePayment_Click(object sender, EventArgs e)
    {

        try
        {
            itinerary = Session["hItinerary"] as HotelItinerary;
            request = Session["req"] as HotelRequest;
            if (itinerary.Source == HotelBookingSource.GTA)
            {
                try
                {
                    HotelSearchResult[] hotelSearchResult = new HotelSearchResult[0];
                    CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();
                    gtaApi.SessionId = Session["cSessionId"].ToString();
                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        gtaApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                        gtaApi.AgentExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        gtaApi.AgentDecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                    }
                    else
                    {
                        gtaApi.AgentCurrency = Settings.LoginInfo.Currency;
                        gtaApi.AgentExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        gtaApi.AgentDecimalPoint = Settings.LoginInfo.DecimalValue;
                    }
                    try
                    {
                        Dictionary<string, string> activeSources = AgentMaster.GetActiveSourcesByProduct((int)ProductType.Hotel);

                        if (activeSources != null && activeSources.Count > 0)
                        {
                            gtaApi.SourceCountryCode = activeSources[HotelBookingSource.GTA.ToString()];
                        }
                        else
                        {
                            gtaApi.SourceCountryCode = "AE";// TODO Ziyad
                        }
                    }
                    catch
                    {
                        gtaApi.SourceCountryCode = "AE";
                    }
                    DOTWCountry dotw = new DOTWCountry();
                    Dictionary<string, string> Countries = dotw.GetAllCountries();
                    request.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[request.PassengerNationality]);
                    hotelSearchResult = gtaApi.GetHotelAvailability(request.CopyByValue(), 0, null, itinerary.HotelCode, itinerary.Roomtype[0].RatePlanCode);
                    decimal oldPrice = 0;
                    decimal newPrice = 0;
                    if (hotelSearchResult != null && hotelSearchResult.Length > 0)
                    {
                        for (int i = 0; i < itinerary.Roomtype.Length; i++)
                        {
                            oldPrice += itinerary.Roomtype[i].Price.NetFare;
                            for (int j = 0; j < hotelSearchResult[0].RoomDetails.Length; j++)
                            {
                                if (itinerary.Roomtype[i].RoomTypeCode == hotelSearchResult[0].RoomDetails[j].RoomTypeCode)
                                {
                                    newPrice += hotelSearchResult[0].RoomDetails[j].SellingFare;
                                    break;
                                }
                            }
                        }
                    }
                    else
                    {
                        Audit.Add(EventType.GetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "GTA RePricing Results count Getting zero", Request["REMOTE_ADDR"]);
                        lblMessage.Text = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                        Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderPageLoad");
                        PaymentMultiView.ActiveViewIndex = 1;
                        lblError.Text = lblMessage.Text;
                        return;
                    }
                    if (oldPrice != newPrice)
                    {
                        lblMessage.Text = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                        Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderPageLoad");
                        PaymentMultiView.ActiveViewIndex = 1;
                        lblError.Text = lblMessage.Text;
                        return;
                    }
                }
                catch
                {
                    warningMsg = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                }
            }
            #region GRN Price Recheck Before Booking
            if (itinerary.Source == HotelBookingSource.GRN)
            {
                try
                {
                    HotelSearchResult resultObj = null;
                    HotelSearchResult[] hotelSearchResult = new HotelSearchResult[0];
                    CT.BookingEngine.GDS.GRN grnApi = new CT.BookingEngine.GDS.GRN();
                    grnApi.SessionId = Session["cSessionId"].ToString();
                    grnApi.GrnUserId = Settings.LoginInfo.UserID;
                    int agencyId = 0;
                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        agencyId = Settings.LoginInfo.OnBehalfAgentID;
                        grnApi.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                        grnApi.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                        grnApi.DecimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                    }
                    else
                    {
                        agencyId = Settings.LoginInfo.AgentId;
                        grnApi.AgentCurrency = Settings.LoginInfo.Currency;
                        grnApi.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                        grnApi.DecimalPoint = Settings.LoginInfo.DecimalValue;
                    }
                    try
                    {
                        Dictionary<string, string> activeSources = AgentMaster.GetActiveSourcesByProduct((int)ProductType.Hotel);

                        if (activeSources != null && activeSources.Count > 0)
                        {
                            grnApi.SourceCountryCode = activeSources[HotelBookingSource.GRN.ToString()];
                        }
                        else
                        {
                            grnApi.SourceCountryCode = "AE";
                        }
                    }
                    catch
                    {
                        grnApi.SourceCountryCode = "AE";
                    }
                    DataTable dtMarkup = CT.BookingEngine.UpdateMarkup.Load(agencyId, HotelBookingSource.GTA.ToString(), (int)ProductType.Hotel);
                    DOTWCountry dotw = new DOTWCountry();
                    Dictionary<string, string> Countries = dotw.GetAllCountries();
                    request.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[request.PassengerNationality]);

                    decimal oldPrice = 0;
                    decimal newPrice = 0;
                    HotelSearchResult[] hotelInfo = Session["SearchResults"] as HotelSearchResult[];
                    resultObj = hotelInfo.First<HotelSearchResult>(delegate (HotelSearchResult h) { return h.HotelCode == itinerary.HotelCode; });
                    string rateTye = resultObj.SupplierType.Split('|')[0];
                    //HotelSearchResult resultGRN = new HotelSearchResult();
                    //  resultGRN = resultObj.CopyByValue();
                    string exceptionResult = string.Empty;
                    HotelRoomsDetails[] roomDetails = new HotelRoomsDetails[resultObj.RoomDetails.Length];
                    for (int i = 0; i < resultObj.RoomDetails.Length; i++)
                    {
                        roomDetails[i].RoomTypeCode = resultObj.RoomDetails[i].RoomTypeCode;
                    }
                    if (itinerary.Roomtype[0].BedTypeCode == "Bundeled")
                    {

                        for (int i = 0; i < itinerary.Roomtype.Length; i++)
                        {
                            oldPrice += itinerary.Roomtype[i].Price.NetFare;
                        }
                        try
                        {
                            grnApi.GetRoomPriceValidate(ref resultObj, request.CopyByValue(), 0, null, itinerary.Roomtype[0].RoomTypeCode.Split('|')[1], itinerary.Roomtype[0].RoomTypeCode.Split('|')[4], itinerary.Roomtype[0].RoomTypeCode.Split('|')[5]);
                        }
                        catch 
                        {
                            exceptionResult = "searchid is invalid or expired";
                        }

                        if(string.IsNullOrEmpty(exceptionResult))
                        {
                            resultObj.RoomDetails = resultObj.RoomDetails.OrderBy(o => o.RoomTypeCode.Split('|')[0]).ToArray();
                            if (itinerary.Roomtype.Length == resultObj.RoomDetails.Length)
                            {
                                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                                {
                                    for (int j = 0; j < resultObj.RoomDetails.Length; j++)
                                    {
                                        if (resultObj.RoomDetails[j].RoomTypeCode.Split('|')[6] == itinerary.Roomtype[i].RoomTypeCode.Split('|')[6] && itinerary.Roomtype[i].RoomTypeCode.Split('|')[2] == resultObj.RoomDetails[j].RoomTypeCode.Split('|')[2] && itinerary.Roomtype[i].MealPlanDesc == resultObj.RoomDetails[j].mealPlanDesc)
                                        {
                                            newPrice += resultObj.RoomDetails[j].TotalPrice;
                                            itinerary.Roomtype[i].RoomTypeCode = resultObj.RoomDetails[j].RoomTypeCode;
                                            itinerary.PropertyType = resultObj.PropertyType;
                                            break;

                                        }
                                    }
                                }
                            }

                            else
                            {
                                for (int i = 0; i < itinerary.Roomtype.Length; i++)
                                {
                                    for (int j = 0; j < resultObj.RoomDetails.Length; j++)
                                    {
                                        if (itinerary.Roomtype[i].RoomTypeCode.Split('|')[7] == resultObj.RoomDetails[j].RoomTypeCode.Split('|')[7] && itinerary.Roomtype[i].RoomTypeCode.Split('|')[8] == resultObj.RoomDetails[j].RoomTypeCode.Split('|')[8])
                                        {
                                            newPrice += resultObj.RoomDetails[j].TotalPrice;
                                            if (itinerary.Roomtype[i].RoomTypeCode.Split('|')[2] == resultObj.RoomDetails[j].RoomTypeCode.Split('|')[2] && itinerary.Roomtype[i].MealPlanDesc == resultObj.RoomDetails[j].mealPlanDesc)
                                            {
                                                itinerary.Roomtype[i].RoomTypeCode = resultObj.RoomDetails[j].RoomTypeCode;
                                                itinerary.PropertyType = resultObj.PropertyType;
                                                break;
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    else
                    {
                        for (int i = 0; i < itinerary.Roomtype.Length; i++)
                        {
                            oldPrice += itinerary.Roomtype[i].Price.NetFare;
                            try
                            {
                                grnApi.GetRoomPriceValidate(ref resultObj, request.CopyByValue(), 0, null, itinerary.Roomtype[i].RoomTypeCode.Split('|')[1], itinerary.Roomtype[i].RoomTypeCode.Split('|')[4], itinerary.Roomtype[i].RoomTypeCode.Split('|')[5]);
                            }
                            catch 
                            {
                                exceptionResult = "searchid is invalid or expired";
                            }
                            newPrice += resultObj.RoomDetails[0].TotalPrice;
                            if (itinerary.Roomtype[i].RoomTypeCode.Split('|')[2] == resultObj.RoomDetails[0].RoomTypeCode.Split('|')[2] && itinerary.Roomtype[i].MealPlanDesc == resultObj.RoomDetails[0].mealPlanDesc)
                            {
                                itinerary.PropertyType = resultObj.PropertyType;
                                itinerary.Roomtype[i].RoomTypeCode = resultObj.RoomDetails[0].RoomTypeCode;
                            }
                        }
                    }

                    if (oldPrice != newPrice || !string.IsNullOrEmpty(exceptionResult))
                    {
                        if(!string.IsNullOrEmpty(exceptionResult))
                        {
                            lblMessage.Text = exceptionResult;
                        }
                        else
                        {
                            lblMessage.Text = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                        }
                        Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderPageLoad");
                        PaymentMultiView.ActiveViewIndex = 1;
                        lblError.Text = lblMessage.Text;
                        return;
                    }
                }
                catch
                {                  
                        warningMsg = "Price has been revised for this Hotel. Please search again to book this Hotel.";
                }               
            }
            #endregion

            if (ddlPayment.SelectedItem.Value == "Credit") //paymentType Credit time directly saving Database
            {
                HotelBooking(CT.TicketReceipt.BusinessLayer.PaymentMode.Credit,"0"); //credit time paymentid passing always 0 added by brahmam
            }
            else //card time navigating PaymentProcessingForHotel page
            {
               
                //navigating paymentgetway
                Response.Redirect("PaymentProcessingForHotel.aspx?paymentGateway=CCAvenue");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed Hotel Booking. Error: " + ex.Message, Request["REMOTE_ADDR"]);
            Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderErr");
            PaymentMultiView.ActiveViewIndex = 1;

            if (ex.Message.Contains("Block") || ex.Message.Contains("RezLive"))
            {
                lblError.Text = "There was a problem booking your room. Please try after some time.";
            }
            else
            {
                lblError.Text = ex.Message;
            }
            //Session.Abandon();
        }
    }

    private void HotelBooking(CT.TicketReceipt.BusinessLayer.PaymentMode  mode,string paymentid)
    {
        try
        {
            itinerary = Session["hItinerary"] as HotelItinerary;
            int agencyId = 0;
            if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                agencyId = Settings.LoginInfo.AgentId;
            }
            else
            {
                agencyId = Settings.LoginInfo.OnBehalfAgentID; //Selected Agency
            }

            agency = new AgentMaster(agencyId);

            agency.UpdateBalance(0);
            if (agencyId == Settings.LoginInfo.AgentId)
            {
                Settings.LoginInfo.AgentBalance = agency.CurrentBalance;
            }
            amountToCompare = agency.CurrentBalance;

            decimal hotelRate = 0;
            decimal outputVat = 0; //Here Vat Also We are doing Celling
            for (int k = 0; k < itinerary.Roomtype.Length; k++)
            {
                hotelRate += itinerary.Roomtype[k].Price.GetAgentPrice();
                outputVat += itinerary.Roomtype[k].Price.OutputVATAmount;
            }
            hotelRate = Math.Ceiling(hotelRate) + Math.Ceiling(outputVat);
            //Credit should be checking Agentbalance
            if (hotelRate > amountToCompare && CT.TicketReceipt.BusinessLayer.PaymentMode.Credit == mode)
            {
                lblMessage.Text = "Dear Agent you do not have sufficient balance to book a room for this hotel.";
                return;
            }
            else
            {
              
                itinerary.ProductType = ProductType.Hotel;
                itinerary.ConfirmationNo = "";
                itinerary.BookUserIP = Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"];
                Product prod = itinerary;
                //Product prod = new Product();
                prod.ProductId = itinerary.ProductId;
                prod.ProductType = itinerary.ProductType;
                prod.ProductTypeId = itinerary.ProductTypeId;
                if (itinerary.ProductType != ProductType.Hotel)
                {
                    itinerary.ProductType = ProductType.Hotel;
                }
                itinerary.HotelCategory = "";
                itinerary.VatDescription = "";
                itinerary.AgencyReference = "";
                itinerary.CancelId = "";
                //itinerary.HotelAddress2 = "";
                itinerary.CreatedOn = DateTime.Now;
                itinerary.CreatedBy = (int)Settings.LoginInfo.UserID;
                if (agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Credit_Limit)
                {
                    itinerary.PaymentMode = ModeOfPayment.CreditLimit;
                }
                else
                {
                    if (mode == CT.TicketReceipt.BusinessLayer.PaymentMode.Card) //mode checking And saving 
                    {
                        itinerary.PaymentMode = ModeOfPayment.CreditCard;
                    }
                    else 
                    {
                        itinerary.PaymentMode = ModeOfPayment.Credit;
                    }
                }
                
                //RezLive and miki sources  price checking time assigned SpecialRequest is True that value need change empty  added by brahmam 
                if (itinerary.Source == HotelBookingSource.RezLive || itinerary.Source == HotelBookingSource.Miki || itinerary.Source == HotelBookingSource.TBOHotel)
                {
                    itinerary.SpecialRequest = string.Empty;
                }
                request = Session["req"] as HotelRequest;
                //if (itinerary.PassengerCountryOfResidence == string.Empty)
                {
                    if (request != null)
                    {
                        itinerary.PassengerCountryOfResidence = request.PassengerCountryOfResidence;
                        //Credit and Source GTA  time no need to take Passenger Nationality
                        if (itinerary.Source != HotelBookingSource.GTA)
                        {
                            itinerary.PassengerNationality = request.PassengerNationality;
                        }
                    }
                }
                MetaSearchEngine mse = new MetaSearchEngine(Session["cSessionId"].ToString());
                bool SamePax = false;
                if (Session["UserBookings"] != null && Session["BookingMade"] != null)
                {
                    UserBookings = Session["UserBookings"] as Dictionary<int, HotelItinerary>;
                    HotelItinerary hotel = UserBookings[(int)Settings.LoginInfo.UserID];
                    //Checking for duplicate booking
                    foreach (HotelRoom room in itinerary.Roomtype)
                    {
                        foreach (HotelPassenger pax in room.PassenegerInfo)
                        {
                            foreach (HotelRoom hroom in hotel.Roomtype)
                            {
                                foreach (HotelPassenger rPax in hroom.PassenegerInfo)
                                {
                                    if (pax.Firstname == rPax.Firstname && pax.Lastname == rPax.Lastname && itinerary.Source == hotel.Source && itinerary.StartDate == hotel.StartDate && itinerary.EndDate == hotel.EndDate)
                                    {
                                        SamePax = true;
                                    }
                                }
                            }
                        }
                    }
                }
                if (warningMsg.Length <= 0 && !SamePax)
                {
                    Session["BookingMade"] = true;
                    if (itinerary.Source == HotelBookingSource.RezLive) //modified by brahmam   21/09/2015
                    {
                        DOTWCountry dotwApi = new DOTWCountry();
                        Dictionary<string, string> Countries = dotwApi.GetAllCountries();
                        itinerary.PassengerNationality = Country.GetCountryCodeFromCountryName(Countries[itinerary.PassengerNationality]);
                    }
                    else if (itinerary.Source == HotelBookingSource.LOH || itinerary.Source == HotelBookingSource.EET)
                    {
                        ///////////////////////////////////////////////////////////////////////////////////////////
                        //  Sequence Number returned from Search Result must be passed in order to Book a Hotel  //
                        //  for LotsOfHotel API. Without it booking will happen and gives an Error.              //
                        ///////////////////////////////////////////////////////////////////////////////////////////
                        if (string.IsNullOrEmpty(itinerary.SequenceNumber))
                        {
                            Response.Redirect("HotelSearch.aspx?source=Hotel");
                        }

                    }
                    else if (itinerary.Source == HotelBookingSource.GTA || itinerary.Source == HotelBookingSource.Agoda)
                    {
                        itinerary.SpecialRequest = itinerary.Roomtype[0].EssentialInformation;
                    }

                    mse.SettingsLoginInfo = Settings.LoginInfo;
                    bookRes = mse.Book(ref prod, agencyId, BookingStatus.Ready, Settings.LoginInfo.UserID);

                    if (bookRes.Status != BookingResponseStatus.Successful)
                    {
                        //string bookingMsg = "";
                        //Load hotel booking messages
                        //try
                        //{
                        //    string filePath = ConfigurationManager.AppSettings["configFiles"] + "HotelBookingMsg.txt";
                        //    System.IO.StreamReader sr = new StreamReader(filePath);
                        //    bookingMsg = sr.ReadToEnd();
                        //}
                        //catch (Exception ex)
                        //{
                        //    Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to read Hotel Booking Messages: " + ex.ToString(), Request["REMOTE_ADDR"]);
                        //}
                        //string[] msgs = bookingMsg.Split('|');
                        //PaymentMultiView.ActiveViewIndex = 1;
                        //if (bookRes.Error != null && bookRes.Error.Length > 0)
                        //{
                        //    foreach (string msg in msgs)
                        //    {
                        //        if (bookRes.Error.Contains("Block") && msg.Split('=')[0] == "Block")
                        //        {
                        //            lblError.Text = msg.Split('=')[1];
                        //            break;
                        //        }
                        //        else if (bookRes.Error.Contains("Failed") && msg.Split('=')[0] == "Failed")
                        //        {
                        //            lblError.Text = msg.Split('=')[1];
                        //            break;
                        //        }
                        //        else if (bookRes.Error.Contains("Error") && msg.Split('=')[0] == "Error")
                        //        {
                        //            lblError.Text = msg.Split('=')[1];
                        //            break;
                        //        }
                        //        else
                        //        {
                        //            lblError.Text = bookRes.Error;
                        //        }
                        //    }

                        //}
                        //else
                        //{
                        //    //Show failed message
                        //    lblError.Text = msgs[1].Split('=')[1];
                        //}
                        Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloader");
                        //PaymentMultiView.ActiveViewIndex = 1;
                        Response.Redirect("ErrorPage.aspx?Err=" + bookRes.Error, false);
                    }
                    else
                    {
                        Session["hItinerary"] = itinerary;
                        Session["BookingResponse"] = bookRes;

                        //SendHotelVoucherEmail();

                        UserBookings.Remove((int)Settings.LoginInfo.UserID);

                        //if (itinerary.Source != HotelBookingSource.HotelConnect)
                        //{
                        //    //itinerary = prod as HotelItinerary;
                        //    //Load or save the invoice
                        //    int invoiceNumber = Invoice.isInvoiceGenerated(itinerary.Roomtype[0].RoomId, ProductType.Hotel);
                        //    Invoice invoice = new Invoice();
                        //    if (invoiceNumber > 0)
                        //    {
                        //        invoice.Load(invoiceNumber);
                        //    }
                        //    else
                        //    {
                        //        invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.HotelId, string.Empty, (int)Settings.LoginInfo.UserID, ProductType.Hotel, rateOfExchange);
                        //        invoice.Load(invoiceNumber);
                        //        invoice.Status = InvoiceStatus.Paid;
                        //        invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                        //        invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                        //        invoice.UpdateInvoice();

                        //        //SendHotelInvoiceEmail(invoiceNumber);// commented invoice mail by ziya
                        //    }
                        //    try
                        //    {

                        //        //Reduce Agent Balance
                        //        decimal total = 0, discount = 0, outVatAmount = 0;
                        //        foreach (HotelRoom room in itinerary.Roomtype)
                        //        {
                        //            ///room.Price.pri
                        //            if (room.Price.AccPriceType == PriceType.NetFare)
                        //            {
                        //                total += (room.Price.NetFare + room.Price.Markup);
                        //                outVatAmount += room.Price.OutputVATAmount;
                        //            }
                        //            else
                        //            {
                        //                total += (room.Price.PublishedFare);
                        //            }
                                   // total += Settings.LoginInfo.IsOnBehalfOfAgent ? room.Price.AsvAmount : 0;
                        //            discount = room.Price.Discount;
                        //        }
                        //        //No need to deduct from agent balance as it is addl markup only. We are not adding markup to total
                        //        //so no need to deduct addl markup from total now. commented by shiva on 28 Aug 2014 4:30 PM
                        //        //total = Math.Ceiling(total) - Convert.ToDecimal(Session["Markup"]);
                        //        total -= discount;
                        //        total = Math.Ceiling(total) + Math.Ceiling(outVatAmount); //VAT And Total Amount Both are Celling

                        //        if (bookRes.Status == BookingResponseStatus.Successful && CT.TicketReceipt.BusinessLayer.PaymentMode.Credit == mode) //Credit mode checking and deducting the agent balance
                        //        {
                        //            //Update agent balance, reduce the hotel booking amount from the balance.
                        //            agency = new AgentMaster(agencyId);
                        //            agency.CreatedBy = Settings.LoginInfo.UserID;
                        //            agency.UpdateBalance(-total);

                        //            if (agencyId == Settings.LoginInfo.AgentId)
                        //            {
                        //                Settings.LoginInfo.AgentBalance -= total;
                        //            }
                        //        }
                        //        else if(CT.TicketReceipt.BusinessLayer.PaymentMode.Card==mode)  //card mode checking and updateing Card info
                        //        {
                        //            CreditCardPaymentInformation payment = new CreditCardPaymentInformation();
                        //            payment.PaymentStatus = 1; //here 1 means Suceess
                        //            payment.ReferenceId = bookRes.BookingId;
                        //            payment.PaymentId = paymentid;
                        //            payment.PaymentInformationId = (int)Session["PaymentInformationId"];
                        //            payment.Remarks = "Successful";
                        //            payment.UpdatePaymentDetails();
                        //            Session["PaymentInformationId"] = null;
                        //        }

                        //    }
                        //    catch (Exception ex)
                        //    {
                        //        Audit.Add(EventType.GetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to update Agent Balance. Error: " + ex.Message, Request["REMOTE_ADDR"]);
                        //    }
                        //}
                        Response.Redirect("HotelPaymentVoucher.aspx?ConfNo=" + bookRes.ConfirmationNo, false);
                    }
                }
                else//In Case of Warning message search again.
                {
                    if (warningMsg.Length > 0)
                    {
                        Response.Redirect("HotelSearch.aspx?source=Hotel");
                    }
                    else if (SamePax)
                    {
                        lblError.Text = "Duplicate bookings are not allowed for the same Hotel room passengers";
                        PaymentMultiView.ActiveViewIndex = 1;
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GetBooking, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed Hotel Booking. Error("+ itinerary.Source.ToString() + ": " + ex.ToString(), Request["REMOTE_ADDR"]);
            Utility.StartupScript(this.Page, "document.getElementById('PreLoader').style.display = 'none'", "offPreloaderErr");
            PaymentMultiView.ActiveViewIndex = 1;

            if (ex.Message.Contains("Block") || ex.Message.Contains("RezLive"))
            {
                lblError.Text = "There was a problem booking your room. Please try after some time.";
            }
            else
            {
                lblError.Text = ex.Message;
            }
        }
    }

    void SendHotelVoucherEmail()
    {
        //try
        //{
        //    HotelItinerary itineary = Session["hItinerary"] as HotelItinerary;
        //    Hashtable globalHashTable = new Hashtable();
        //    int agencyId = 0;
        //    if (itinerary.Source != HotelBookingSource.HotelConnect)
        //    {
        //        BookingDetail booking = new BookingDetail(BookingDetail.GetBookingIdByProductId(Convert.ToInt32(itineary.HotelId), ProductType.Hotel));

        //        agencyId = booking.AgencyId;
        //    }
        //    else
        //    {
        //        agencyId = Settings.LoginInfo.AgentId;
        //    }
        //    AgentMaster agency = new AgentMaster(agencyId);
        //    long bookedBy = Settings.LoginInfo.UserID;
        //    string cityName = "";
        //    if (agency.City.Length > 0)
        //    {
        //        cityName = agency.City;
        //    }
        //    else
        //    {
        //        cityName = "";
        //    }
        //    //logo = Agency.GetAgencyLogo(agency.AgencyId);
        //    string phoneString = string.Empty;
        //    if (agency.Phone1 != null && agency.Phone1.Length > 0)
        //    {
        //        phoneString = agency.Phone1;
        //    }

        //    //This is a global hash table which we send in Email.Send which replaces 
        //    //hashvariables with values(code which is not in loop)

        //    //globalHashTable.Add("logo", logo);
        //    globalHashTable.Add("agencyName", agency.Name);
        //    globalHashTable.Add("AaddressLine1", agency.Address);
        //    globalHashTable.Add("AaddressLine2", "");
        //    string agentSpan = " <span style=\"width:100%;float:left;margin:0px;font-size:12px;font-weight:normal;padding:0px;color:#000;\">";

        //    if (agency.City != null && agency.City.Length != 0)
        //    {
        //        globalHashTable.Add("AcityName", agentSpan + agency.City + "</span>");
        //    }
        //    else
        //    {
        //        globalHashTable.Add("AcityName", "");
        //    }
        //    if (agency.Code != null && agency.Code.Length != 0)
        //    {
        //        globalHashTable.Add("AaddressPin", agentSpan + agency.Code + "</span>");
        //    }
        //    else
        //    {
        //        globalHashTable.Add("AaddressPin", "");
        //    }
        //    if (agency.Phone1 != null && agency.Phone1.Length != 0)
        //    {
        //        globalHashTable.Add("AaddressPhone", agentSpan + "Phone:" + agency.Phone1 + "</span>");
        //    }
        //    else
        //    {
        //        globalHashTable.Add("AaddressPhone", "");
        //    }
        //    if (agency.Fax != null && agency.Fax.Length != 0)
        //    {
        //        globalHashTable.Add("agencyFax", agentSpan + "Fax:" + agency.Fax + "</span>");
        //    }
        //    else
        //    {
        //        globalHashTable.Add("agencyFax", "");
        //    }
        //    if (agency.Email1 != null && agency.Email1.Length != 0)
        //    {
        //        globalHashTable.Add("agencyEmail", agentSpan + "Email: " + agency.Email1 + "</span>");
        //    }
        //    else
        //    {
        //        globalHashTable.Add("agencyEmail", "");
        //    }

        //    globalHashTable.Add("HotelName", itineary.HotelName);
        //    //string rating=string.Empty;
        //    //rating+="<em>";
        //    //for (int j = 0; j <Convert.ToInt16(itineary.Rating); j++)
        //    //{
        //    // rating+=" <b><img src=\"Images/star.gif\" alt=\"star\" /></b>";
        //    // }
        //    //rating += "</em>";
        //    //globalHashTable.Add("Rating", rating);
        //    globalHashTable.Add("AddressLine1", itineary.HotelAddress1);
        //    string adLine2 = string.Empty;
        //    if (itineary.HotelAddress2 != null && itineary.HotelAddress2.Length > 0)
        //    {
        //        adLine2 += "<span style=\"width:100%;float:left;margin:0px;font-size:12px;color:#000;font-weight:normal;padding:0px;\">";
        //        adLine2 += itineary.HotelAddress2;
        //        adLine2 += "</span>";
        //    }
        //    if (itineary.Source == HotelBookingSource.IAN)
        //    {
        //        adLine2 += "<span style=\"width:100%;float:left;margin:0px;font-size:12px;color:#000;font-weight:normal;padding:0px;\">";
        //        adLine2 += " Phone: 001-800-568-1972";
        //        adLine2 += "</span>";
        //    }

        //    globalHashTable.Add("AddressLine2", adLine2);
        //    globalHashTable.Add("HotelCity", itineary.CityRef);
        //    string Rating = string.Empty;
        //    if (Convert.ToInt16(itineary.Rating) > 0)
        //    {
        //        Rating = "( " + Convert.ToInt16(itineary.Rating) + " Star )";
        //    }
        //    globalHashTable.Add("Rating", Rating);
        //    if (itineary.IsDomestic)
        //    {
        //        globalHashTable.Add("PassengerName", itineary.HotelPassenger.Title.ToUpper() + " " + itineary.HotelPassenger.Firstname.ToUpper() + " " + itineary.HotelPassenger.Lastname.ToUpper());
        //    }
        //    else
        //    {
        //        string paxString = string.Empty;
        //        for (int i = 0; i < itineary.NoOfRooms; i++)
        //        {
        //            foreach (HotelPassenger hpaxInfo in itineary.Roomtype[i].PassenegerInfo)
        //            {
        //                string name = hpaxInfo.Firstname + hpaxInfo.Lastname;
        //                if (name.Length > 0)
        //                {
        //                    paxString += "    Room " + (i + 1) + " :<b>" + hpaxInfo.Firstname + "  " + hpaxInfo.Lastname + " - " + hpaxInfo.PaxType.ToString();
        //                    if (hpaxInfo.PaxType == HotelPaxType.Child)
        //                    {
        //                        paxString += "(" + hpaxInfo.Age + " Yrs)";
        //                    }
        //                    paxString += "</b> <br />";
        //                }
        //            }
        //        }
        //        globalHashTable.Add("PassengerName", paxString);
        //    }
        //    if (itineary.Source == HotelBookingSource.HotelBeds)
        //    {
        //        globalHashTable.Add("AgencyRefNo", "Agency Ref:" + itineary.AgencyReference);
        //    }
        //    else
        //    {
        //        globalHashTable.Add("AgencyRefNo", "");
        //    }
        //    globalHashTable.Add("ConfirmationNo", itineary.ConfirmationNo);
        //    globalHashTable.Add("CheckIn", itineary.StartDate.ToString("dd MMM yyyy"));
        //    globalHashTable.Add("CheckOut", itineary.EndDate.ToString("dd MMM yyyy"));
        //    globalHashTable.Add("RoomName", itineary.Roomtype[0].RoomName);
        //    globalHashTable.Add("RoomStatus", "Status : " + itineary.Status.ToString());
        //    string amenitys = string.Empty;
        //    //if (itineary.Roomtype[0].Ameneties.Count > 0)
        //    //{
        //    //    amenitys += "Incl:";
        //    //    foreach (string amenity in itineary.Roomtype[0].Ameneties)
        //    //    {
        //    //        amenitys += amenity + " ";
        //    //    }
        //    //}
        //    globalHashTable.Add("Ameneties", amenitys);
        //    globalHashTable.Add("NoofRooms", itineary.NoOfRooms);
        //    int adults = 0, children = 0;
        //    for (int k = 0; k < itineary.NoOfRooms; k++)
        //    {
        //        adults += itineary.Roomtype[k].AdultCount;
        //        children += itineary.Roomtype[k].ChildCount;
        //    }
        //    globalHashTable.Add("paxCount", (adults + children).ToString());
        //    string paxInfo = string.Empty;
        //    for (int i = 1; i <= itineary.NoOfRooms; i++)
        //    {
        //        paxInfo += "   <p>";
        //        paxInfo += " <span style=\"width:100px;float:left;margin:0px;padding:0px;font-size:12px;color:#000;\">Room" + i + ": </span> ";
        //        paxInfo += " <span style=\"width:170px;float:left;margin:0px;padding:0px;font-size:12px;font-weight:bold;color:#000;\"> ";
        //        paxInfo += itineary.Roomtype[i - 1].AdultCount + " <i style=\"font-style:normal; font-weight:normal;\">Adult(s)  </i>";
        //        if (itineary.Roomtype[i - 1].ChildCount > 0)
        //        {
        //            paxInfo += itineary.Roomtype[i - 1].ChildCount + " <i style=\"font-style:normal; font-weight:normal;\">Children</i>";
        //        }
        //        paxInfo += "   </span></p>     ";
        //    }
        //    globalHashTable.Add("paxInfo", paxInfo);
        //    System.TimeSpan diffResult = itineary.EndDate.Subtract(itineary.StartDate);
        //    globalHashTable.Add("NoofDays", diffResult.Days.ToString());
        //    string ruleString = string.Empty;
        //    if (itineary.HotelPolicyDetails != null && itineary.HotelPolicyDetails.Length > 0)
        //    {
        //        string[] tempStr = itineary.HotelPolicyDetails.Split('|');
        //        ruleString += "<p style=\"width:100%;float:left;margin:0px;padding:15px 0 0 0;font-size:14px;font-weight:bold;color:#000;\">Hotel Rules and Regulations</p><ul style=\"width:540px;float:left;margin:0px;padding:7px 5px 0 30px;display:inline;\">";
        //        for (int i = 0; i < tempStr.Length - 1; i++)
        //        {
        //            ruleString += "<li style=\"margin:0px;padding:0;list-style:disc;list-style-position:outside;font-size:12px;color:#000;\">";
        //            ruleString += tempStr[i];
        //            ruleString += "</li>";
        //        }
        //        ruleString += "   </ul>";
        //    }
        //    globalHashTable.Add("HotelRules", ruleString);
        //    ruleString = string.Empty;
        //    if (itineary.HotelCancelPolicy != null && itineary.HotelCancelPolicy.Length > 0 && (itineary.Source == HotelBookingSource.Desiya || itineary.Source == HotelBookingSource.TBOConnect))
        //    {
        //        string[] tempStr = itineary.HotelCancelPolicy.Split('|');
        //        ruleString += "<p style=\"width:100%;float:left;margin:0px;padding:15px 0 0 0;font-size:14px;font-weight:bold;color:#000;\">Cancellation and Changes</p><ul style=\"width:540px;float:left;margin:0px;padding:7px 5px 0 30px;display:inline;\">";

        //        for (int i = 0; i < tempStr.Length - 1; i++)
        //        {
        //            ruleString += " <li style=\"margin:0px;padding:0;list-style:disc;list-style-position:outside;font-size:12px;color:#000;\">";
        //            ruleString += tempStr[i];
        //            ruleString += "</li>";
        //        }
        //        ruleString += "</ul>";
        //    }
        //    string aotString = string.Empty;
        //    if (itineary.Source == HotelBookingSource.GTA)
        //    {
        //        GTACity cityInfo = new GTACity();
        //        cityInfo.Load(itineary.CityCode);
        //        Dictionary<string, string> aotList = new Dictionary<string, string>();
        //        aotList = cityInfo.LoadGTAAOTNumber(cityInfo.CountryCode, cityInfo.CountryName);
        //        string country = cityInfo.CountryName;
        //        string city = string.Empty;
        //        string intlCall = string.Empty;
        //        string nationalCall = string.Empty;
        //        string localCall = string.Empty;
        //        string officeHours = string.Empty;
        //        string emergencyNo = string.Empty;
        //        string language = string.Empty;
        //        if (aotList.ContainsKey("City"))
        //        {
        //            city = aotList["City"];
        //        }
        //        if (aotList.ContainsKey("Language"))
        //        {
        //            language = aotList["Language"];
        //        }
        //        if (aotList.ContainsKey("OfficeHours"))
        //        {
        //            officeHours = aotList["OfficeHours"];
        //        }
        //        if (aotList.ContainsKey("InternationalNo"))
        //        {
        //            intlCall = aotList["InternationalNo"];
        //        }
        //        if (aotList.ContainsKey("LocalNo"))
        //        {
        //            localCall = aotList["LocalNo"];
        //        }
        //        if (aotList.ContainsKey("NationalNo"))
        //        {
        //            nationalCall = aotList["NationalNo"];
        //        }
        //        if (aotList.ContainsKey("EmergencyNo"))
        //        {
        //            emergencyNo = aotList["EmergencyNo"];
        //        }

        //        aotString += "<div style=\"width:100%; float:left; padding:10px 0 0;\"> <table border=\"1\">";
        //        aotString += "                <tr>";
        //        aotString += "                     <td>Country</td>";
        //        aotString += "                    <td>Contact</td>";
        //        aotString += "                   <td>Intl Call</td>";
        //        aotString += "         <td>Domestic Call</td>";
        //        aotString += "                    <td>Local Call</td>  ";
        //        aotString += "                     <td>Office Hours</td>";
        //        aotString += "                      <td>Emergency</td>";
        //        aotString += "                       <td>Language</td>";
        //        aotString += "              </tr>";
        //        aotString += "                <tr>";
        //        aotString += "                     <td>" + country + "</td>";
        //        aotString += "                     <td>" + city + "</td>";
        //        aotString += "                    <td>" + intlCall + "</td>";
        //        aotString += "                   <td>" + nationalCall + "</td>";
        //        aotString += "                   <td>" + localCall + " </td>";
        //        aotString += "                   <td>" + officeHours + "</td>";
        //        aotString += "                   <td>" + emergencyNo + "</td>";
        //        aotString += "                  <td>" + language + "</td>";
        //        aotString += "            </tr>";
        //        aotString += "   </table></div>";

        //    }
        //    globalHashTable.Add("HotelAOTContact", aotString);
        //    globalHashTable.Add("HotelCancelPolicy", ruleString);
        //    ruleString = string.Empty;
        //    if (itineary.Source == HotelBookingSource.GTA)
        //    {
        //        ruleString += "<p>Hotel Map</p><p style=\"width:100%;text-align:center;\">";
        //        //ruleString += "<a href=\""+itineary.Map +"\" target=\"_blank\">Click here</a></p>";
        //        if (itineary.Map != null && itineary.Map.Length > 0)
        //        {
        //            ruleString += "To view the map click on this Link" + itineary.Map + "</p>";
        //        }
        //        else
        //        {
        //            ruleString += "Map Not Available</p>";
        //        }
        //    }
        //    globalHashTable.Add("HotelMap", ruleString);
        //    globalHashTable.Add("PublishedFare", itineary.Roomtype[0].Price.PublishedFare.ToString(Technology.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]));
        //    globalHashTable.Add("Tax", itineary.Roomtype[0].Price.Tax.ToString(Technology.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]));
        //    globalHashTable.Add("TotalFare", Convert.ToDouble(itineary.Roomtype[0].Price.PublishedFare + itineary.Roomtype[0].Price.Tax).ToString(Technology.Configuration.ConfigurationSystem.LocaleConfig["ToStringDecimalPattern"]));
        //    string specialReq = string.Empty;
        //    if (itineary.SpecialRequest.Length > 0)
        //    {
        //        specialReq = "<p style=\"width:100%;float:left;margin:0px;padding:15px 0 0 0;font-size:14px;font-weight:bold;color:#000;\">Special Request <label style=\"margin:0px;padding:0 0 0 10px;font-size:11px;font-weight:normal;color:#555;\">(We will forward your requests to the property, but we can't guarantee that your requests will be honoured)</label></p><span style=\"width:560px;float:left;margin:0px;padding:7px 0 0 10px;font-size:12px;color:#000;\">" + itineary.SpecialRequest + "</span>";
        //        globalHashTable.Add("SpecialRequest", specialReq);
        //    }
        //    else
        //    {
        //        globalHashTable.Add("SpecialRequest", specialReq);
        //    }
        //    string flightInfo = string.Empty;
        //    if (itineary.FlightInfo.Length > 0)
        //    {
        //        flightInfo = "<p style=\"width:100%;float:left;margin:0px;padding:15px 0 0 0;font-size:14px;font-weight:bold;color:#000;\">Flight Details <label style=\"margin:0px;padding:0 0 0 10px;font-size:11px;font-weight:normal;color:#555;\"></label></p><span style=\"width:560px;float:left;margin:0px;padding:7px 0 0 10px;font-size:12px;color:#000;\">" + itineary.FlightInfo + "</span>";
        //        globalHashTable.Add("FlightInfo", flightInfo);
        //    }
        //    else
        //    {
        //        globalHashTable.Add("FlightInfo", flightInfo);
        //    }
        //    if (itineary.Source == HotelBookingSource.HotelBeds)
        //    {
        //        globalHashTable.Add("vatInfo", itineary.VatDescription + ", acting as agent for the hotel operating company, details of which can be provided upon request.");
        //    }
        //    else
        //    {
        //        globalHashTable.Add("vatInfo", "");
        //    }
        //    string filePath = ConfigurationManager.AppSettings["HotelVoucherPage"].ToString();
        //    StreamReader sr = new StreamReader(filePath);
        //    string loopString = string.Empty;

        //    bool loopStarts = false;
        //    //string contains the code before loop 
        //    string startString = string.Empty;
        //    //string contains the code after loop
        //    string endString = string.Empty;
        //    bool loopEnds = false;
        //    #region seperate out the loop code and the other code
        //    while (!sr.EndOfStream)
        //    {
        //        string line = sr.ReadLine();
        //        if (line.IndexOf("%loopEnds%") >= 0 || loopEnds)
        //        {
        //            loopEnds = true;
        //            loopStarts = false;
        //            if (line.IndexOf("%loopEnds%") >= 0)
        //            {
        //                line = sr.ReadLine();
        //            }
        //            endString += line;
        //        }
        //        if (line.IndexOf("%loopStarts%") >= 0 || loopStarts)
        //        {
        //            if (line.IndexOf("%loopStarts%") >= 0)
        //            {
        //                line = sr.ReadLine();
        //            }
        //            loopString += line;
        //            loopStarts = true;
        //        }
        //        else
        //        {
        //            if (!loopEnds)
        //            {
        //                startString += line;
        //            }
        //        }
        //    }
        //    #endregion

        //    string fullString = startString;
        //    List<string> toArray = new List<string>();
        //    string addressList = itineary.Roomtype[0].PassenegerInfo[0].Email;
        //    string[] addressArray = addressList.Split(',');
        //    for (int k = 0; k < addressArray.Length; k++)
        //    {
        //        toArray.Add(addressArray[k]);
        //    }
        //    string from = new CT.TicketReceipt.BusinessLayer.UserMaster(Settings.LoginInfo.UserID).Email;
        //    try
        //    {
        //        CT.Core.Email.Send(from, from, toArray, "Hotel Voucher", fullString, globalHashTable);
        //    }
        //    catch (Exception ex)
        //    {
        //        Audit.Add(EventType.Email, Severity.Normal, 0, "Smtp is unable to send the message: " + ex.Message, Request["REMOTE_ADDR"]);
        //    }
        //}
        //catch (Exception ex)
        //{
        //    Audit.Add(EventType.Email, Severity.High, 0, "error : " + ex.StackTrace.ToString(), "");
        //}
    }

    void SendHotelInvoiceEmail(int invoiceNumber)
    {
        #region Loading all the objects required for Invoice
        try
        {
            HotelItinerary itineary = Session["hItinerary"] as HotelItinerary;
            Invoice invoice = new Invoice();
            invoice.Load(invoiceNumber);
            agency = new AgentMaster(invoice.AgencyId);
            invoice.Load(invoiceNumber);
            
            Hashtable globalHahTable = new Hashtable();
            //globalHahTable.Add("agencyName", agency.Name);
            //globalHahTable.Add("agencyAddressLine1", agency.Address);            
            //globalHahTable.Add("agencyCity", agency.City);
            globalHahTable.Add("completeInvoiceNumber", invoice.CompleteInvoiceNumber);
            globalHahTable.Add("createdOn", invoice.CreatedOn.ToString("dd MMMM yyyy"));
            globalHahTable.Add("invoiceRemarks", invoice.Remarks);
            globalHahTable.Add("City", itineary.CityRef);
            globalHahTable.Add("StartDate", itinerary.StartDate.ToString("dd MMM yy"));
            globalHahTable.Add("EndDate", itinerary.EndDate.ToString("dd MMM yy"));
            string showMessage = string.Empty;
            //if (itinerary.IsDomestic)
            //{
            //    showMessage += " <div style=\"float:left; margin-left:10px; width:60px;text-align: center;\">Tax</div><div style=\"float:left; margin-left:10px; width:50px;text-align: center;\" >O/C</div>";
            //}
            //else
            //{
            //    showMessage += " <div style=\"float:left; margin-left:10px; width:50px;text-align: center;\"> Currency</div>";
            //}

            globalHahTable.Add("showMessage", showMessage);
            globalHahTable.Add("AgentName", agency.Name);
            globalHahTable.Add("AgentAddress", agency.Address);
            //globalHahTable.Add("ServiceTaxRegNumber", ConfigurationManager.AppSettings["ServiceTaxRegNo"].ToString());
            string filePath = ConfigurationManager.AppSettings["EmailHotelInvoicePage"].ToString();
            StreamReader sr = new StreamReader(filePath);
            //string contains the code of loop
            string loopString = string.Empty;
            bool loopStarts = false;
            //string contains the code before loop 
            string startString = string.Empty;
            //string contains the code after loop
            string endString = string.Empty;
            bool loopEnds = false;
            #region seperate out the loop code and the other code
            while (!sr.EndOfStream)
            {
                string line = sr.ReadLine();
                if (line.IndexOf("%loopEnds%") >= 0 || loopEnds)
                {
                    loopEnds = true;
                    loopStarts = false;
                    if (line.IndexOf("%loopEnds%") >= 0)
                    {
                        line = sr.ReadLine();
                    }
                    endString += line;
                }
                if (line.IndexOf("%loopStarts%") >= 0 || loopStarts)
                {
                    if (line.IndexOf("%loopStarts%") >= 0)
                    {
                        line = sr.ReadLine();
                    }
                    loopString += line;
                    loopStarts = true;
                }
                else
                {
                    if (!loopEnds)
                    {
                        startString += line;
                    }
                }
            }
            #endregion
            string midString = string.Empty;
            HotelRoom roomInfo = new HotelRoom();
            roomInfo.Load(itineary.HotelId);
            HotelRoom[] hRoomList = roomInfo.Load(itineary.HotelId);
            HotelPassenger passInfo = new HotelPassenger();
            passInfo.Load(itineary.HotelId);
            System.TimeSpan diffResult = itinerary.EndDate.Subtract(itinerary.StartDate);
            foreach (HotelRoom hotelRm in hRoomList)
            {
                string bookingClass = string.Empty;
                if (hotelRm.Price.PublishedFare != 0 && hotelRm.Price.PublishedFare - hotelRm.Price.OurCommission != 0)
                {
                    float plbInPercent = Convert.ToSingle((hotelRm.Price.AgentPLB * 100) / (hotelRm.Price.PublishedFare - hotelRm.Price.OurCommission));
                }
                Hashtable tempTable = new Hashtable();
                tempTable.Add("HotelName", itineary.HotelName);
                tempTable.Add("RoomName", hotelRm.RoomName);
                tempTable.Add("Name", passInfo.Firstname + " " + passInfo.Lastname);
                tempTable.Add("NoOfUnits", hotelRm.NoOfUnits);
                tempTable.Add("Days", diffResult.Days.ToString());
                if (hotelRm.Price.AccPriceType == PriceType.NetFare)
                {
                    tempTable.Add("Price", (hotelRm.Price.NetFare + hotelRm.Price.Markup - hotelRm.Price.Discount).ToString("N2"));
                }
                else
                {
                    tempTable.Add("Price", hotelRm.Price.PublishedFare.ToString("N2"));
                }
                //if (itineary.IsDomestic)
                //{
                //    tempTable.Add("Tax", hotelRm.Price.Tax.ToString("N2"));
                //    tempTable.Add("O/C", hotelRm.Price.OtherCharges.ToString("N2"));
                //}
                //else
                //{
                //    //tempTable.Add("Tax", hotelRm.Price.RateOfExchange.ToString("N2"));
                //    tempTable.Add("O/C", Technology.Configuration.ConfigurationSystem.LocaleConfig["CurrencySign"]);
                //}
                midString += Email.ReplaceHashVariable(loopString, tempTable);
            }
            string fullString = startString + midString + endString;
            string from = new CT.TicketReceipt.BusinessLayer.UserMaster(Settings.LoginInfo.UserID).Email;
            List<string> toArray = new List<string>();
            string addressList = agency.Email1;
            string[] addressArray = addressList.Split(',');
            for (int k = 0; k < addressArray.Length; k++)
            {
                toArray.Add(addressArray[k]);
            }
            decimal totalPrice = 0;
            decimal handlingCharge = 0;
            decimal serviceTax = 0;
            decimal tdsCommission = 0;
            decimal tdsPLB = 0;
            decimal plb = 0;
            decimal transactionFee = 0;
            PriceAccounts price = new PriceAccounts();
            price = hRoomList[0].Price;
            decimal rateofExchange = price.RateOfExchange;

            string symbol = Util.GetCurrencySymbol(price.Currency);
            if (hRoomList[0].Price.AccPriceType == PriceType.PublishedFare)
            {
                for (int k = 0; k < hRoomList.Length; k++)
                {
                    totalPrice = totalPrice + (hRoomList[k].Price.PublishedFare + hRoomList[k].Price.Tax + hRoomList[k].Price.OtherCharges - hRoomList[k].Price.Discount);
                    handlingCharge = handlingCharge + (hRoomList[k].Price.AgentCommission);
                    serviceTax = serviceTax + (hRoomList[k].Price.SeviceTax);
                    tdsCommission = tdsCommission + hRoomList[k].Price.TdsCommission;
                    plb = plb + hRoomList[k].Price.AgentPLB;
                    tdsPLB = tdsPLB + hRoomList[k].Price.TDSPLB;
                    transactionFee = transactionFee + hRoomList[k].Price.TransactionFee;

                }
            }
            else if (hRoomList[0].Price.AccPriceType == PriceType.NetFare)
            {
                for (int k = 0; k < hRoomList.Length; k++)
                {
                    totalPrice = totalPrice + (hRoomList[k].Price.NetFare + hRoomList[k].Price.Markup + hRoomList[k].Price.Tax + hRoomList[k].Price.OtherCharges - hRoomList[k].Price.Discount);
                    serviceTax = serviceTax + (hRoomList[k].Price.SeviceTax);
                    tdsCommission = tdsCommission + hRoomList[k].Price.TdsCommission;
                    plb = plb + hRoomList[k].Price.AgentPLB;
                    tdsPLB = tdsPLB + hRoomList[k].Price.TDSPLB;
                    transactionFee = transactionFee + hRoomList[k].Price.TransactionFee;
                }
            }
            string remarksMessge = string.Empty;
            if (itineary.IsDomestic)
            {
                remarksMessge += " <div style=\"float:left; width:300px; margin-top:10px; margin-right:20px;>";
                remarksMessge += "<div style=\float:left; width:100px; margin-top:5px;\"><b>   Remarks:</b></div>";
                remarksMessge += "<div style=\"clear:both\"><ul>";
                string[] tempStr = itinerary.HotelPolicyDetails.Split('|');
                for (int j = 0; j < tempStr.Length - 1; j++)
                {
                    remarksMessge += "<li style=\"list-style:disc inside;\">  " + tempStr[j] + " </li>";
                }
                remarksMessge += "   </ul>       </div>           </div>";

            }
            globalHahTable.Add("Remarks", remarksMessge);
            string priceMessage = string.Empty;
            if (itinerary.IsDomestic)
            {
                priceMessage += "   <div style=\"float: right; width: 300px; margin-top: 10px;\">";
                priceMessage += "<div style=\"font-weight: bold;\">";
                priceMessage += "  <div style=\"float: left; width: 60px; padding-left:50px;\">Gross:</div>";
                priceMessage += " <div style=\"width: 140px; float: left; text-align: right;\">" + Math.Round(totalPrice,2) + "</div></div>";
                priceMessage += " <div style=\"float: left; width: 100%; margin-top: 5px\">";
                //priceMessage += "    <div style=\"float: left; width: 50px; font-style: italic;\">Less</div>";
                //priceMessage += "   <div style=\"float: left; width: 140px;\">Handling Charges</div>";
                //if (Settings.LoginInfo == null && invoice.AgencyId == 1)
                //{
                //    priceMessage += "   <div style=\"width: 60px; float: left; text-align: right;\">" + handlingCharge.ToString("N2") + "</div>";
                //}
                //else
                //{
                //    priceMessage += "   <div style=\"width: 60px; float: left; text-align: right;\">" + (handlingCharge + plb).ToString("N2") + "</div>";
                //}
                //priceMessage += " </div>";
                //priceMessage += " <div style=\"float: left; width: 100%; margin-top: 5\">";
                //priceMessage += "      <div style=\"float: left; width: 50px; font-style: italic;\">Add</div>";
                //priceMessage += "     <div style=\"float: left; width: 140px;\">Service Tax</div>";
                //priceMessage += "    <div style=\"width: 60px; float: left; text-align: right;\">" + serviceTax.ToString("N2") + "</div>";
                //priceMessage += "   </div>";
                //priceMessage += "  <div style=\"float: left; width: 100%; margin-top: 5\">";
                //priceMessage += "      <div style=\"float: left; width: 50px; font-style: italic;\">Add</div>";
                //priceMessage += " <div style=\"float: left; width: 140px;\">Tra Fee</div>";
                //priceMessage += "     <div style=\"width: 60px; float: left; text-align: right;\">" + transactionFee.ToString("N2") + "</div>";
                //priceMessage += "  </div>";
                //priceMessage += "  <div style=\"float: left; width: 100%; margin-top: 5px; border-bottom: dashed 1px #c0c0c0;\">";
                //priceMessage += "     <div style=\"float: left; width: 50px; font-style: italic;\">Add</div>";
                //priceMessage += " <div style=\"float: left; width: 140px;\">TDS Deducted</div>";
                //priceMessage += "  <div style=\"width: 60px; text-align: right; float: left;\" id=\"TDSHandling\">" + (tdsCommission + tdsPLB).ToString("N2") + "</div>";
                //priceMessage += " </div>";
                priceMessage += " </div>";
            }

            globalHahTable.Add("PrintMessage", priceMessage);
            globalHahTable.Add("NetAmount", symbol + " " + Convert.ToDouble(totalPrice - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee).ToString("0.00"));
            globalHahTable.Add("NetReceivable", symbol + " " + Convert.ToDouble(totalPrice - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee).ToString("0.00"));
            string inrMess = string.Empty;
            decimal totalAmount = Math.Round((totalPrice - handlingCharge - plb + serviceTax + tdsCommission + tdsPLB + transactionFee) * rateofExchange, 2);
            decimal service_Tax = AccountingEngine.CalculateProductServiceTax(totalAmount, false, ProductType.Hotel);
            decimal totalPayble = totalAmount + service_Tax;
            if (!itinerary.IsDomestic && rateofExchange != 1)
            {
                inrMess += " <div  style=\"float:left; width:280px; margin-top:5px;\">";
                inrMess += " <div style=\"float:left; width:140px; padding-left:50px;\">Equivalent INR</div>";
                inrMess += " <div style=\"float:left; width:60px; text-align:right;\" id=\"Div1\">" + Math.Round(totalAmount,2) + "</div>";
                inrMess += " </div>";
                //inrMess += "<div  style=\"float:left; width:280px; margin-top:5px;\">";
                //inrMess += " <div style=\"float:left; width:140px; padding-left:50px;\">Service Tax</div>";
                //inrMess += "<div style=\"float:left; width:60px; text-align:right;\" id='Div2' > Rs." + service_Tax + "</div>";
                //inrMess += " </div>";
                inrMess += "<div  style=\"float:left; width:280px; margin-top:5px;\">";
                inrMess += "<div style=\"float:left; width:140px; padding-left:50px;\">Total Amount</div>";
                inrMess += "<div class='width-60 fleft text-right' id='Div3' >Rs." + Math.Round(totalPayble,2) + "</div>";
                inrMess += "<div style=\"float:left; width:140px; padding-left:50px; italic\">( If Paid in INR )</div>";
                inrMess += "</div> ";
            }
            globalHahTable.Add("INRMessage", inrMess);
            globalHahTable.Add("CNO", itineary.ConfirmationNo);
            CT.TicketReceipt.BusinessLayer.UserMaster member = new CT.TicketReceipt.BusinessLayer.UserMaster(invoice.CreatedBy);
            globalHahTable.Add("ticketedBy", member.FirstName + " " + member.LastName);
            try
            {
                CT.Core.Email.Send(from, from, toArray, "Invoice" , fullString, globalHahTable);
                //Response.Write("Email Sent Successfully");
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Email, Severity.Normal, 0, "Smtp is unable to send the message: " + ex.Message, Request["REMOTE_ADDR"]);
                Response.Write("Email Not Sent Successfully");
            }

        }

        catch (Exception exp)
        {
            Audit.Add(EventType.Email, Severity.Normal, 0, "Error : " + exp.StackTrace.ToString(), Request["REMOTE_ADDR"]);
        }
        #endregion
    }
    
}
