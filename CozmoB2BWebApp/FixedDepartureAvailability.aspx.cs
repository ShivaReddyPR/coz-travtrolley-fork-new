﻿using System;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.BookingEngine;

public partial class FixedDepartureAvailabilityGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected ActivityDetails Details = new ActivityDetails();
    protected Activity activity = new Activity();
    protected string activityImgFolder;
    protected string departureDates = "";
    protected string departureMonths = "";
    protected string departureYears = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        //if (!IsPostBack)
        activityImgFolder = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"].ToString();
        {

            LoadData();
        }
    }
    public void LoadData()
    {
        try
        {
            if (Session["FixedDeparture"] != null)
            {
                activity = Session["FixedDeparture"] as Activity;
                lblTitle.Text = activity.Name;
                //imgActivity.ImageUrl = activity.ImagePath1;
                imgActivity.ImageUrl = activityImgFolder + activity.ImagePath2;
                lblAir.Text = activity.Itinerary1;
                lblHotel.Text = activity.Itinerary2;
                BindPriceDetails(activity);
                //dlRoomRates.DataSource = activity.PriceDetails;
                //dlRoomRates.DataBind();
                dlFDItinerary.DataSource = activity.FixedItineraryDetails;
                dlFDItinerary.DataBind();
                int i = 1;
                foreach (DataRow dr in activity.FixedinitPriceDetails.Rows)
                {
                    if (dr["PriceDate"] != null && dr["PriceDate"].ToString() != "")
                    {
                        DateTime dt = DateTime.Parse(dr["PriceDate"].ToString());
                        if (ddlDepDate.Items.FindByText(Convert.ToString(dt.ToString("dd-MMM-yyyy"))) == null)
                        {
                            //string todaydate = DateTime.Now.ToString("dd/MM/yyyy");
                            DateTime dtSuppliedDate = DateTime.Parse(dt.ToString("dd-MMM-yyyy"));
                            if (dtSuppliedDate.Subtract(DateTime.Today).Days >= 0)
                            {
                                ListItem item = new ListItem(dt.ToString("dd-MMM-yyyy"), i.ToString());
                                ddlDepDate.Items.Add(item);
                            }
                        }
                        i++;
                    }
                }
                ddlDepDate.Items.Insert(0, new ListItem("dd-MM-YYYY", "-1"));

                //DataTable dtFliter = activity.FixedinitPriceDetails;
                //DataView dv = new DataView(dtFliter);
                //dv.Sort = "PriceDate";
                //dtFliter = dv.ToTable();
                //hdfFDFromdate.Value = dtFliter.Rows[0]["PriceDate"].ToString();
                //hdfFDTodate.Value = dtFliter.Rows[dtFliter.Rows.Count - 1]["PriceDate"].ToString();

                foreach (DataRow dr in activity.PriceDetails.Rows)
                {
                    if (dr["PriceDate"] != null && dr["PriceDate"].ToString() != "")
                    {
                        DateTime dt = DateTime.Parse(dr["PriceDate"].ToString());
                        if (departureDates.Length > 0)
                        {
                            departureDates += ",[" + dt.Year + "-" + (dt.Month - 1) + "-" + dt.Day + "]";
                        }
                        else
                        {
                            departureDates = "[" + dt.Year + "-" + (dt.Month - 1) + "-" + dt.Day + "]";
                        }
                        if (!departureMonths.Contains(dt.Year + "-" + (dt.Month - 1).ToString()))
                        {
                            if (departureMonths.Length > 0)
                            {
                                departureMonths += "," + dt.Year + "-" + (dt.Month - 1).ToString();
                            }
                            else
                            {
                                departureMonths = dt.Year + "-" + (dt.Month - 1).ToString();
                            }
                        }

                        if (!departureYears.Contains(dt.Year.ToString()))
                        {
                            if (departureYears.Length > 0)
                            {
                                departureYears += "," + dt.Year;
                            }
                            else
                            {
                                departureYears = dt.Year.ToString();
                            }
                        }
                    }
                }
            }
            else
            {
                Response.Redirect("FixedDepartureResults.aspx", false);
            }
        }
        catch (Exception ex)
        {
            throw ex;
            // Audit.Add(EventType.PakageQueries, Severity.High, 1, ex.Message, "0");
        }
    }
    void BindPriceDetails(Activity activity)
    {

        DataTable dtPriceDetails = activity.PriceDetails;

        DataTable Dates = dtPriceDetails.DefaultView.ToTable(true, new string[] { "PriceDate" });

        if (Dates != null)
        {
            foreach (DataRow row in Dates.Rows)
            {
                HtmlTableRow tr = new HtmlTableRow();
                HtmlTableCell Cell = new HtmlTableCell();
                Cell.InnerHtml = "<h3 style='background:#ccc; color:#000'>" + Convert.ToDateTime(row["PriceDate"]).ToString("dd-MMM-yyyy") + "</h3>";
                Cell.Align = "Center";

                Cell.Style.Add("font-size", "16px");

                tr.Cells.Add(Cell);
                tblPriceDates.Rows.Add(tr);
                //DataRow[] PaxTypes = dtPriceDetails.Select("PriceDate='" + row["PriceDate"].ToString() + "'");
                DataRow[] PaxTypes = dtPriceDetails.Select("PriceDate >= '" + Convert.ToDateTime(row["PriceDate"]).ToString("MM/dd/yyyy 00:00:00") + "' AND PriceDate <='" + Convert.ToDateTime(row["PriceDate"]).ToString("MM/dd/yyyy 23:59:59") + "'");
                if (PaxTypes != null)
                {
                    HtmlTableRow hRow = new HtmlTableRow();
                    HtmlTableCell hCell = new HtmlTableCell();
                    HtmlTable hTable = new HtmlTable();
                    hTable.Border = 0;
                    hTable.Width = "100%";

                    foreach (DataRow dr in PaxTypes)
                    {
                        HtmlTableRow tr1 = new HtmlTableRow();
                        HtmlTableCell Cell1 = new HtmlTableCell();
                        Cell1.Style.Add("font-size", "13px");
                        Cell1.InnerHtml = dr["Label"].ToString();
                        Cell1.Width = "50%";
                        Cell1.Align = "center";
                        tr1.Cells.Add(Cell1);
                        HtmlTableCell Cell2 = new HtmlTableCell();
                        Cell2.Style.Add("font-size", "13px");
                        Cell2.InnerHtml = dr["Amount"].ToString();
                        Cell2.Align = "left";
                        tr1.Cells.Add(Cell2);
                        hTable.Rows.Add(tr1);
                    }
                    Cell.Controls.Add(hTable);
                    hRow.Cells.Add(hCell);
                    tblPriceDates.Rows.Add(hRow);
                }
                HtmlTableRow fRow = new HtmlTableRow();
                HtmlTableCell fCell = new HtmlTableCell();
                fCell.InnerHtml = "&nbsp;";
                fRow.Cells.Add(fCell);
                tblPriceDates.Rows.Add(fRow);
            }
        }

    }

    protected void imgCheck_Click(object sender, EventArgs e)
    {
        if (Session["FixedDeparture"] != null)
        {
            activity = Session["FixedDeparture"] as Activity;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            DateTime excursionDate = Convert.ToDateTime(ddlDepDate.SelectedItem.Text, dateFormat);
            DataRow[] filteredDateRows = activity.FixedinitPriceDetails.Select("PriceDate >= '" + excursionDate.ToString("MM/dd/yyyy 00:00:00") + "' AND PriceDate <='" + excursionDate.ToString("MM/dd/yyyy 23:59:59") + "'");

            DataTable dt = activity.PriceDetails.Clone();
            foreach (DataRow row in filteredDateRows)
            {
                dt.ImportRow(row);
            }
            activity.PriceDetails = dt;

            /************************************************************************************************
             * ------------------------------------Checks for Booking---------------------------------------*
             * 1. Check whether booking date is after Excursion start date and before closing date.         *         
             * 2. Booking cut off days check against the Booking Date / Excursion Date. If available then   *
             *    proceed otherwise prompt user to choose another                                           *
             * 3. Check whether booking date falls between available date ranges.                           *
             * 4. Check whether booking date falls between unavailable date ranges.                         *
             * 5. Check whether stock is available to proceed booking.                                      *
             * ---------------------------------------------------------------------------------------------*
             * **********************************************************************************************/
            //Check 1
            //DateTime StartFrom = Convert.ToDateTime(activity.StartFrom, dateFormat);
            //DateTime EndTo = Convert.ToDateTime(activity.EndTo, dateFormat);


            if (filteredDateRows.Length > 0)
            {

                if ((activity.StockInHand - activity.StockUsed) > 0)
                {
                    DataRow row = null;
                    if (activity.TransactionHeader.Rows.Count > 0)
                    {
                        row = activity.TransactionHeader.Rows[0];
                    }
                    else
                    {
                        row = activity.TransactionHeader.NewRow();
                    }

                    row["ActivityId"] = activity.Id;
                    row["ActivityName"] = activity.Name;
                    row["TransactionDate"] = DateTime.Now;
                    //row["Adult"] = ddlAdult.SelectedValue;
                    //row["Child"] = ddlChild.SelectedValue;
                    //row["Infant"] = ddlInfant.SelectedValue;
                    row["Booking"] = Convert.ToDateTime(ddlDepDate.SelectedItem.Text, dateFormat);
                    row["TransactionType"] = "B2B";
                    row["isFixedDeparture"] = "Y";// Fixed Departure

                    if (activity.TransactionHeader.Rows.Count <= 0)
                    {
                        activity.TransactionHeader.Rows.Add(row);
                    }
                    Session["FixedDeparture"] = activity;

                    //Proceed Activity booking
                    Response.Redirect("FixedDepartureCategoriesList.aspx", false);
                }
                else
                {
                    lblError.Visible = true;
                    Image1.Visible = true;
                }
            }
            else
            {
                lblError.Visible = true;
                Image1.Visible = true;
            }
        }
        else
        {
            Response.Redirect("FixedDepartureResults.aspx", false);
        }
    }
    protected string CTCurrencyFormat(object currency)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue);
        }
    }
}
