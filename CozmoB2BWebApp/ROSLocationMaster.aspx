﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="ROSLocationMasterGUI" Title="ROS Location Master" Codebehind="ROSLocationMaster.aspx.cs" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
    <%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
     
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script>
    function Validation() {

        if (getElement('txtLocName').value == '') addMessage('Location Name cannot be blank!', '');
        if (getElement('ddlLocType').selectedIndex <= 0) addMessage('Select Location Type!', '');
        
        
        if (getMessage() != '') {

            alert(getMessage()); clearMessage(); return false;
        }
    }
</script>




  <div class=" body_container paramcon">
       
       
       
             <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"><label class="pull-right fl_xs"> Location Name:* </label></div>
    <div class="col-md-2"><asp:TextBox ID="txtLocName" CssClass="inputEnabled form-control" runat="server"></asp:TextBox> </div>

    <div class="col-md-2"><label class="pull-right fl_xs">Location Type:*</label> </div>
    <div class="col-md-2"> <asp:DropDownList ID="ddlLocType" CssClass="inputDdlEnabled form-control" runat="server">
                            <asp:ListItem Value="-1" Text="Select Location Type"></asp:ListItem>
                            <asp:ListItem Value="H" Text="Home"></asp:ListItem>
                            <asp:ListItem Value="A" Text="Airport"></asp:ListItem>
                        </asp:DropDownList></div>

    <div class="clearfix"></div>
    </div>
    
    
          <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <label class="pull-right fl_xs">Location Detail: </label></div>
    <div class="col-md-6"><asp:TextBox ID="txtLocDetail" CssClass="inputEnabled" TextMode="MultiLine" runat="server"
                            Height="55px" Width="100%"></asp:TextBox> </div>


    <div class="clearfix"></div>
    </div>
    
    
    
    
  <div class="col-md-12 padding-0 marbot_10">  
  
  
      <div class="col-md-8">  
      
              <asp:Label ID="lblMessage" runat="server" Visible="false" CssClass="lblSuccess"></asp:Label>
     <asp:Button ID="btnSave" CssClass="btn but_b pull-right marleft_10" Text="Save" runat="server" OnClientClick="return Validation();"
                        OnClick="btnSave_Click" Style="display: inline;" />&nbsp;&nbsp;
                   
                    <asp:Button ID="btnClear" CssClass="btn but_b pull-right marleft_10" Text="Clear" runat="server" OnClick="btnClear_Click"
                        Style="display: inline;" />&nbsp;&nbsp;
                    
                    <asp:Button ID="btnSearch" CssClass="btn but_b pull-right" Text="Search" runat="server" OnClick="btnSearch_Click"
                        Style="display: inline;" />
    </div>
    
    </div>
    
       <div class="clearfix"> </div>
        
          </div>
          
          

 
</asp:Content>

<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" Runat="Server">


<asp:GridView ID="gvSearch" Width="100%" runat="server"  AllowPaging="true" DataKeyNames="loc_id" 
    EmptyDataText="No ROS Location List Available!" AutoGenerateColumns="false" PageSize="15" GridLines="none"  CssClass="grdTable"
    OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
    OnPageIndexChanging="gvSearch_PageIndexChanging" >
    
    <HeaderStyle CssClass="gvHeader" HorizontalAlign="left">
     </HeaderStyle>
     <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
     <AlternatingRowStyle CssClass="gvDtlAlternateRow" />    
    <Columns> 
    <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />" ControlStyle-Width="25px"  ControlStyle-CssClass="label" ShowSelectButton="True" />
    
      
     <asp:TemplateField>
     <HeaderStyle HorizontalAlign="left"/>
    <ItemStyle HorizontalAlign="left" />
    <HeaderTemplate>
    <cc1:Filter   ID="HTtxtLocName" Width="120px" HeaderText="Location Name" CssClass="inputEnabled" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemTemplate >
    <asp:Label ID="ITlblLocName" runat="server" Text='<%# Eval("loc_name") %>' CssClass="label grdof" ToolTip='<%# Eval("loc_name") %>' Width="120px"></asp:Label>
 
    </ItemTemplate>    
    
    </asp:TemplateField>  
    
   
    
       <asp:TemplateField>
        <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtLocType"  Width="120px" CssClass="inputEnabled" HeaderText="Location Type" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblLocType" runat="server" Text='<%# Eval("loc_type") %>' CssClass="label grdof"  ToolTip='<%# Eval("loc_type") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>
    
    

 <asp:TemplateField>
  <HeaderStyle HorizontalAlign="left"/>
    <HeaderTemplate >
    <cc1:Filter ID="HTtxtLocDetail"  Width="120px" CssClass="inputEnabled" HeaderText="Location Detail" OnClick="FilterSearch_Click" runat="server" />                 
    </HeaderTemplate>
    <ItemStyle HorizontalAlign="left" />
    <ItemTemplate>
    <asp:Label ID="ITlblLocDetail" runat="server" Text='<%# Eval("loc_detail") %>' CssClass="label grdof"  ToolTip='<%# Eval("loc_detail") %>' Width="120px"></asp:Label>                
    </ItemTemplate>    
    </asp:TemplateField>

     </Columns>    
     <HeaderStyle CssClass="gvHeader"></HeaderStyle>
        <RowStyle CssClass="gvRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvAlternateRow" />  
    </asp:GridView>
 

</asp:Content>

