using System;
using System.Data;
using System.Configuration;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;

public partial class AgentPaymentDetailsUI :CT.Core.ParentPage// System.Web.UI.Page
{
    private string AGENT_PAYMENT_SESSION = "_AgentPayemntDetails";

   // private string LOCATION_MAPPING_SESSION = "_LocationMappingList";

    private string AGENT_PAYMENT_SEARCH_SESSION = "_AgentSearchPaymentDetails";
    //private string VISA_SETTLEMENT_DETAILS_SESSION = "_SettlementDetailsList";


    //private enum PageMode
    //{
    //    Add = 1,
    //    Update = 2
    //}

    
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            this.Master.PageRole = true;
            if (Request.QueryString["ref"] != null)
            {
                if (Settings.LoginInfo == null)
                {
                    CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "window.parent.location.href='Login.aspx';", "Login");                                       
                }
            }
            
            // Session["RCPT_TICKET_ID"] = "aaaa";
            lblSuccessMsg.Text = string.Empty;            
            if (!IsPostBack)
            {              

                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
                Clear();
                Utility.StartupScript(this.Page, "showDropdown('Clear');", "showDropdown");
                Utility.StartupScript(this.Page, "clear();", "clear");

                lblSuccessMsg.Text = string.Empty;

            }
            
            //StartupScript(this.Page, " showHidMode(" + ddlSettlementMode.SelectedItem.Value + ")", " showHidMode");
        }
        catch (Exception ex)
    {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);

        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        Utility.StartupScript(this.Page, "showDropdown('Edit');", "showDropdown");
    }
    private void InitializePageControls()
    {
        try
        {
            
             //Clear();
             BindAgent();
             BindPaymentMode();
             BindCurrency();
             //dcDocDate.Value = DateTime.Now;
             //dcChequeDate.Value = DateTime.Now;
            // string uploadPath = "test";
             //string rootFolder = @"Uploads\\AgentSlip\\";
             string rootFolder = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["DEPOSITE_SLIP"]);
             //uploadPath = rootFolder + "\\" + uploadPath + "\\";

             docPaySlip.SavePath = rootFolder;
             

        }
        catch { throw; }
    }

    private void BindAgent()
    {
        try
        {
            //ddlAgent.DataSource = AgentMaster.GetList(1, "B2B-ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            //Commented By Lokesh on 26-10-2018.
            //ddlAgent.DataSource = AgentMaster.GetList(1, "ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);// as per vinay advise - mod by ziyad
            //ddlAgent.DataValueField = "agent_id";
            //ddlAgent.DataTextField = "agent_name";
            //ddlAgent.DataBind();           
            //ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "-1"));

            //Added by Lokesh on 26-10-2018.--Vinay Suggested.
            //if (Settings.LoginInfo != null)
            //{
            //    int agentId = Settings.LoginInfo.AgentId;
            //    if (agentId <= 1) agentId = 0;
            //    ddlAgent.DataSource = AgentMaster.GetList(1, Settings.LoginInfo.AgentType.ToString(), agentId, ListStatus.Short, RecordStatus.Activated);
            //    ddlAgent.DataValueField = "agent_id";
            //    ddlAgent.DataTextField = "agent_name";
            //    ddlAgent.DataBind();
            //    ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "-1"));
            //}

            string agentType = string.Empty;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                agentType = Utility.ToString(Settings.LoginInfo.AgentType);
            }
            ddlAgent.DataSource = AgentMaster.GetList(1, agentType, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "-1"));



            // ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
            //if (Settings.LoginInfo.MemberType == MemberType.SUPER)
            //{
            //    ddlAgent.SelectedIndex = 0;
            //    ddlAgent.Enabled = true;
            //}
            //else
            //{
            //    ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
            //    ddlAgent.Enabled = false;
            //}

        }
        catch { throw; }
    }

    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //Utility.StartupScript(this.Page, "showDropdown('Edit');", "showDropdown");
            AgentMaster agent = new AgentMaster(Utility.ToLong(ddlAgent.SelectedValue));
            ListItem item = ddlCurrency.Items.FindByText(agent.AgentCurrency);
            if (item != null)
            {
                ddlCurrency.ClearSelection();
                item.Selected = true;
                ddlCurrency.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            //Utility.StartupScript(this.Page, "ShowMessageDialog('btnSave_Click','"+ex.Message+"','Information')", "btnSave_Click"); 
            Utility.Alert(this.Page, ex.Message);
        }
    }
    private void BindCurrency()
    {
        try
        {
            ddlCurrency.DataSource = CurrencyMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            ddlCurrency.DataValueField = "CURRENCY_ID";
            ddlCurrency.DataTextField = "CURRENCY_CODE";
            ddlCurrency.DataBind();
            ddlCurrency.Items.Insert(0, new ListItem("--Select Currency--", "0"));
            ddlCurrency.SelectedIndex = 1;
            ddlCurrency.Enabled = false;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }
    private void BindPaymentMode()
    {
        try
        {            
            //DataView dv = dtMode.DefaultView;
            //dv.RowFilter = "FIELD_VALUE NOT IN ('4','5','6')";
            ddlPayMode.DataSource = UserMaster.GetMemberTypeList("agent_pay_mode").Tables[0];
            ddlPayMode.DataValueField = "FIELD_VALUE";
            ddlPayMode.DataTextField = "FIELD_TEXT";
            ddlPayMode.DataBind();
            ddlPayMode.Items.Insert(0, new ListItem("--Select Mode--", "-1"));
            ddlPayMode.SelectedIndex = 1;
        }
        catch { throw; }
    }
//    private void enablePax(bool enabled)
//    {
//        try
//        {
//            ddlAdults.Enabled = enabled;
//            ddlChildren.Enabled = enabled;
//            ddlInfants.Enabled = enabled;
//            chkAllPax.Enabled = enabled;
//            ddlVisaFeeCode.Enabled = enabled;
//        }
//        catch { throw; }
//    }
//    # region Properties
    //private DataTable LocationMapDetails
    //{
    //    get
    //        {
    //        return (DataTable)Session[LOCATION_MAPPING_SESSION];
    //    }
    //    set
    //    {
    //        value.PrimaryKey = new DataColumn[] { value.Columns["map_id"] };
    //        Session[LOCATION_MAPPING_SESSION] = value;
    //        }
    //}
//    //private DataTable VisaSettlementList
//    //{
//    //    get
//    //    {
//    //        return (DataTable)Session[VISA_SETTLEMENT_DETAILS_SESSION];
//    //    }
//    //    set
//    //    {
//    //        value.PrimaryKey = new DataColumn[] { value.Columns["settlement_id"] };
//    //        Session[VISA_SETTLEMENT_DETAILS_SESSION] = value;
//    //    }
//    //}
   

    //}
//    # endregion
//    # region private Methods

    private AgentPaymentDetails  CurrentObject
    {
        get
        {
            return (AgentPaymentDetails)Session[AGENT_PAYMENT_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(AGENT_PAYMENT_SESSION);
            }
            else
            {
                Session[AGENT_PAYMENT_SESSION] = value;
            }

        }
    }
    private void Clear()
        {
        try
        {
            //gvLocationDetails.EditIndex = -1;
            //LocationMaster tempLocation = new LocationMaster();
            //LocationMapDetails = tempLocation.DtMappingDetails;
           // txtReceiptNo.Text = string.Empty;
            docPaySlip.Clear();
            AgentPaymentDetails tempObject = new AgentPaymentDetails(-1);
            docPaySlip.FileName = tempObject.ID<=0?"1":Utility.ToString(tempObject.ID);
            dcDocDate.Value = DateTime.Now;
            //ddlAgent.SelectedIndex = 0;
            ddlPayMode.SelectedIndex = 0;
            txtAmount.Text   = Formatter.ToCurrency("0");

            ListItem item = ddlCurrency.Items.FindByText(Settings.LoginInfo.Currency);
            if (item != null)
            {
                ddlCurrency.ClearSelection();
                item.Selected = true;
                if (Settings.LoginInfo.AgentId > 1)
                ddlCurrency.Enabled = false;
            }
            txtChequeNo.Text = string.Empty;
            dcChequeDate.Value = DateTime.Now;
            txtBankName.Text = string.Empty;
            txtBankBranch.Text = string.Empty;
            txtBankAccount.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            txtReceiptNo.Text = string.Empty;
            //ddlAgent.SelectedIndex = 0;
            lblSuccessMsg.Text = string.Empty;
            lblSuccessMsg.Visible = false;
            btnSave.Text = "Save";
            txtAmount.Enabled = true;
            //hdfmapingDetailsMode.Value = "0";
            //BindGrid();
            CurrentObject = null;
            btnSave.Style.Add("display", "block");
            //ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
            MemberType mType = Settings.LoginInfo.MemberType;
            //if (mType == MemberType.SUPER || (Settings.LoginInfo.AgentId == 1 && (mType == MemberType.ADMIN || mType == MemberType.BACKOFFICE || mType == MemberType.CASHIER)))
            if (mType == MemberType.SUPER || ((Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent) && (mType == MemberType.ADMIN || mType == MemberType.BACKOFFICE || mType == MemberType.CASHIER)))
             {
                //ddlAgent.SelectedIndex = 0;
                ddlAgent.Enabled = true;
            }
            else
            {       
                ddlAgent.Enabled = false;
            }


        }
        catch
        {
            throw;
        }
    }

    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[AGENT_PAYMENT_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["AP_ID"] };
            Session[AGENT_PAYMENT_SEARCH_SESSION] = value;
        }
    }
    private void Edit(long id)
    {
        try
        {
            AgentPaymentDetails   payment = new AgentPaymentDetails(id);
            CurrentObject = payment ;
            txtReceiptNo.Text = payment.ReceiptNo;
            dcDocDate.Value = Utility.ToDate(payment.ReceiptDate);
            ddlAgent.SelectedValue  = Utility.ToString(payment.AgentId) ;
            ListItem item = ddlCurrency.Items.FindByText(payment.Currency);
            if (item != null)
            {
                ddlCurrency.ClearSelection();
                item.Selected = true;
                if (Settings.LoginInfo.AgentId > 1)
                    ddlCurrency.Enabled = false;
            }
            if (payment.TranxType == "PAY")
            {
                ListItem item1 = ddlCreditItems.Items.FindByValue(payment.TranxProvision);
                if (item1 != null)
                {
                    ddlCreditItems.ClearSelection();
                    item1.Selected = true;
                }
                rbtnCredit.Checked = true;
                rbtnDebit.Checked = false;
                txtAmount.Text = Utility.ToString(payment.Amount * -1);
            }
            else if (payment.TranxType == "DEB")
            {
                ListItem item1 = ddlDebitItems.Items.FindByValue(payment.TranxProvision);
                if (item1 != null)
                {
                    ddlDebitItems.ClearSelection();
                    item1.Selected = true;
                }
                rbtnDebit.Checked = true;
                rbtnCredit.Checked = false;
                txtAmount.Text = Utility.ToString(payment.Amount);
            }
            ddlPayMode.SelectedValue = payment.PayMode;
            
            txtChequeNo.Text  = payment.ChequeNo;
            dcChequeDate.Value=Utility.ToDate(payment.ChequeDate);
            txtBankName.Text = payment.BankName;
            txtBankBranch.Text = payment.BankBranch;
            txtBankAccount.Text = payment.BankAccount;
            txtRemarks.Text = payment.Remarks;
            docPaySlip.FileName =Utility.ToString(id);
            docPaySlip.FullPath= payment.DepositSliPath;
            docPaySlip.ContentType = payment.DepositSlipType;
            btnSave.Text = "Update";
            btnClear.Text = "Cancel";
            txtAmount.Enabled = false;
        }
        catch
        {
            throw;
        }
    }
    private void Save()
   {
        try
        {

           // if (LocationMapDetails.Rows.Count > 0 && string.IsNullOrEmpty(txtMapLocation.Text)) { throw new Exception("Map Location cannot be blank !"); }
          //  if (string.IsNullOrEmpty(docPaySlip.FullPath)) throw new Exception("Payment Slip is not uploaded");

            AgentPaymentDetails payment;
            if (CurrentObject == null)
            {
                payment = new AgentPaymentDetails();
            }
            else
            {
                payment = CurrentObject;
            }


            payment.ReceiptNo  = txtReceiptNo.Text.Trim();
            payment.ReceiptDate = dcDocDate.Value;
            string agentName = ddlAgent.SelectedItem.Text;
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            payment.AgentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            payment.PayMode = ddlPayMode.SelectedItem.Value;
            if (rbtnCredit.Checked == true)
            {
                payment.Amount = Utility.ToDecimal(txtAmount.Text.Trim()) * -1;
                payment.TranxType = "PAY";
                payment.TranxProvision = ddlCreditItems.SelectedItem.Value;
            }
            else
            {
                payment.Amount = Utility.ToDecimal(txtAmount.Text.Trim());
                payment.TranxType = "DEB";
                payment.TranxProvision = ddlDebitItems.SelectedItem.Value;
            }
            payment.ChequeNo  = txtChequeNo.Text.Trim();
            payment.ChequeDate = dcChequeDate.Value;
            payment.BankName  = txtBankName.Text.Trim();
            payment.BankBranch = txtBankBranch.Text.Trim();
            payment.BankAccount = txtBankAccount.Text.Trim();
            payment.DepositSliPath = docPaySlip.FullPath;
            payment.DepositSlipType= docPaySlip.ContentType;
            payment.Remarks  = txtRemarks.Text.Trim();
            payment.Currency = ddlCurrency.SelectedItem.Text;
            
            payment.LocationId = Utility.ToInteger(Settings.LoginInfo.LocationID);

            string[] exchRate = ddlCurrency.SelectedItem.Value.Split('~');
            payment.ExchRate = Utility.ToDecimal(exchRate[1]);
            payment.Status = Settings.ACTIVE;
            payment.CreatedBy= Settings.LoginInfo.UserID;
            string mode = CurrentObject == null ? "Add" : "Update";
            payment.Save();
            txtReceiptNo.Text = payment.ReceiptNo;
            string rcptNo = txtReceiptNo.Text.Trim();

            if (rbtnCredit.Checked == true && ddlCreditItems.SelectedItem.Value == "ADD")//Mail should be sent only Topup 
            {
                string topupType;
                if (rbtnCredit.Checked == true)
                    topupType = ddlCreditItems.SelectedItem.Text;
                else
                    topupType = ddlDebitItems.SelectedItem.Text;

                SendMail(rcptNo, agentId, dcDocDate.Value, Utility.ToDecimal(txtAmount.Text.Trim()), topupType, ddlPayMode.SelectedItem.Text);
            }

            Clear();
            Utility.StartupScript(this.Page, "showDropdown('Clear');", "showDropdown");
            Utility.StartupScript(this.Page, "clear();", "clear");
            lblSuccessMsg.Visible = true;
            // lblErrorMsg.Text = string.Format("User '{0}' is saved successfully !", user.LoginName);
            string successMsg = string.Format(" Receipt No {0} is {1} !", rcptNo, (mode == "Add" ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            lblSuccessMsg.Text = successMsg;
            //Utility.StartupScript(this.Page, "ShowMessageDialog('Payment Receipt No','" + successMsg + "','Information')", "btnSave_Click");
            Utility.Alert(this.Page, successMsg );
            long apId = payment.ID;
            string script = "window.open('PrintAgentPaymentReceipt.aspx?apId=" + Utility.ToString(apId) + "','','width=800,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            Utility.StartupScript(this.Page, script, "PrintAgentPaymentReceipt");
            
        }
        catch
        {
            throw;
        }

    }

   protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
            
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            //Utility.StartupScript(this.Page, "ShowMessageDialog('btnSave_Click','"+ex.Message+"','Information')", "btnSave_Click"); 
            Utility.Alert(this.Page, ex.Message);
        }
    }
   protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            //ticketDelete();
            Clear();
            Utility.StartupScript(this.Page, "clear();", "clear");
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            Utility.StartupScript(this.Page, "showDropdown('Clear');", "showDropdown");
            bindSearch();
            //Clear();
        }
        catch (Exception ex)
        {
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //lblMasterError.Text = ex.Message;
            //Utility.WriteLog(ex, this.Title);
        }
    }

    
    # region Content Search
    private void bindSearch()
    {
        try
        {
            LoginInfo loginfo = Settings.LoginInfo;
            int agentId = loginfo.AgentId;
            if (loginfo.AgentId <= 1) agentId = 0;
            DataTable dt = AgentPaymentDetails.GetList(agentId, string.Empty, ListStatus.Long, RecordStatus.Activated , "N","PAY");
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch { throw; }
    }
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            Utility.StartupScript(this.Page, "showDropdown('Edit');", "showDropdown");
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            Utility.StartupScript(this.Page, "showDropdown('Edit');", "showDropdown");
            long vsId = Utility.ToLong(gvSearch.SelectedValue);
            Edit(vsId);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{ "HTtxtName", "agent_name" },{"HTtxtPayMode", "ap_payment_mode_name" }, 
            { "HTtxtReceiptNo", "ap_reciept_no" },{"HTtxtDocDate","ap_receipt_date"},{ "HTtxtAmount", "ap_amount" },{ "HTtxtChequeNo", "ap_cheque_no" },{ "HTtxtBankName", "ap_bank_name" },
            { "HTtxtBankBranch", "ap_bank_branch" },{ "HTtxtBankAc", "ap_bank_account" }};
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }


    # endregion

    #region Date Format
    protected string IDDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
    #endregion

    # region mail
    private void SendMail(string receiptNo, int agentId, DateTime receiptDate, decimal receiptAmount, string topupType, string topupMode)
    {
        try
        {

            System.Text.StringBuilder message = new System.Text.StringBuilder();
            AgentMaster agent = new AgentMaster(agentId);
            //string emailBodyContent = agent.EmailContent1;
            //string emailTimeContent = agent.EmailContent2;
            string toMail = agent.Email1;
            string agentName = agent.Name;
            string alternateEmail = agent.Email2;
            string eMailCaption = receiptNo + " - Agent Payment Under Process";
            string emailCCId = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["AGENT_TOPUP_MAIL"]);
            string emailBCCId = Utility.ToString(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
            message.AppendFormat("Dear {0},\r\n", agentName);
            //emailBccId = Utility.ToString(ConfigurationManager.AppSettings["MAIL_BCC_VISA_APPROVAL"]);
            
            message.AppendFormat("\\n\\n\t This is an AUTOMATED MESSAGE. Please do not reply. \\n\\n");
            message.AppendFormat("\t Action :	Top-up request is under process \\n");
            message.AppendFormat("\t Request Number : {0}\\n", receiptNo);
            message.AppendFormat("\t Agency : {0}\r\\n", agentName);
            message.AppendFormat("\t Submitted On : {0}\\n", receiptDate);
            message.AppendFormat("\t Submitted By : {0}\\n", Settings.LoginInfo.FullName);
            message.AppendFormat("\t Payment Amount : {0}\\n", receiptAmount);
            message.AppendFormat("\t Top-up Mode : {0}\\n", topupMode);
            message.AppendFormat("\t Top-up Type : {0}\\n", topupType);
            message.AppendFormat("\t The request you have submitted to the Top-up team is now under process.\\n");
            message.AppendFormat("\t Please note that your account will be topped-up only after the funds have been credited in the designated cozmotravel bank account.\\n\\n");

            message.AppendFormat(" Regards, \\n  Finance Top-up Team ");
            message.AppendFormat("\\n Cozmo Travel");

            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            if (!string.IsNullOrEmpty(toMail))
                toArray.Add(toMail);
            if (!string.IsNullOrEmpty(alternateEmail))
                toArray.Add(alternateEmail);
            if (!string.IsNullOrEmpty(emailCCId))
                toArray.Add(emailCCId);
           // CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, eMailCaption, message.ToString(), new Hashtable(), emailBCCId); will enable later once confirm from Vijay
            ///VSORAInterface.SendMailQueue(0, "S", ConfigurationManager.AppSettings["MAIL_FROM_ID"].ToString(), eMailCaption, toMail, emailCCId, emailBCCId, string.Empty, string.Empty, message.ToString(), string.Empty, string.Empty);

        }
        catch { }
    }
    # endregion
    // private void setRowId()
   // {
   //     try
   //     {
   //         DropDownList ddlSettleMode = null;
   //         DropDownList ddlVisaElement = null;
   //         if (gvLocationDetails.EditIndex > -1)
   //         {
   //             hdfDetailRowId.Value = gvLocationDetails.Rows[gvLocationDetails.EditIndex].ClientID;
   //             ddlSettleMode = (DropDownList)gvLocationDetails.Rows[gvLocationDetails.EditIndex].FindControl("EITddlSettleMode");
   //             ddlVisaElement = (DropDownList)gvLocationDetails.Rows[gvLocationDetails.EditIndex].FindControl("EITddlVisaElement");
   //             BindSettleMode(ddlSettleMode);
   //             BindMapElement(ddlVisaElement);
               
   //         }
   //         else
   //         {
   //             hdfDetailRowId.Value = gvLocationDetails.FooterRow.ClientID;
   //             ddlSettleMode = (DropDownList)gvLocationDetails.FooterRow.FindControl("FTddlSettleMode");
   //             ddlVisaElement = (DropDownList)gvLocationDetails.FooterRow.FindControl("FTddlVisaElement");
   //             BindSettleMode(ddlSettleMode);
   //             BindMapElement(ddlVisaElement);
   //         }

   //     }
   //     catch { throw; }
   // }
   // protected void gvLocationDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
   // {
   //     try
   //     {
   //         gvLocationDetails.PageIndex = e.NewPageIndex;
   //         gvLocationDetails.EditIndex = -1;
   //         //FilterSearchGrid();
   //         BindGrid();
   //         hdfmapingDetailsMode.Value = "0";
   //     }
   //     catch (Exception ex)
   //     {
   //         Utility.WriteLog(ex, this.Title);
   //         Label lblMasterError = (Label)this.Master.FindControl("lblError");
   //         lblMasterError.Visible = true;
   //         lblMasterError.Text = ex.Message;
   //     }
   // }
   // protected void gvLocationDetails_RowEditing(object sender, GridViewEditEventArgs e)
   // {
        
   //     try
   //     {
   //         gvLocationDetails.EditIndex = e.NewEditIndex;
   //         BindGrid();
           
   //         long serial = Utility.ToLong(gvLocationDetails.DataKeys[e.NewEditIndex].Value);
            
   //         GridViewRow gvRow = gvLocationDetails.Rows[e.NewEditIndex];
   //         ((DropDownList)gvRow.FindControl("EITddlVisaElement")).Text = ((HiddenField)gvRow.FindControl("EIThdfVisaElement")).Value;
   //         ((DropDownList)gvRow.FindControl("EITddlSettleMode")).Text = ((HiddenField)gvRow.FindControl("EIThdfSettleMode")).Value;            

   //         gvLocationDetails.FooterRow.Visible = false;
   //         hdfmapingDetailsMode.Value = "2";
   //     }
   //     catch (Exception ex)
   //     {
   //         Utility.WriteLog(ex, this.Title);
   //         Label lblMasterError = (Label)this.Master.FindControl("lblError");
   //         lblMasterError.Visible = true;
   //         lblMasterError.Text = ex.Message;
   //     }
   // }
   // private void BindSettleMode(DropDownList ddlSettlemode)
   // {
   //     try
   //     {
   //         ddlSettlemode.DataSource = UserMaster.GetMemberTypeList("settlement_mode");
   //         ddlSettlemode.DataValueField = "FIELD_VALUE";
   //         ddlSettlemode.DataTextField = "FIELD_TEXT";
   //         ddlSettlemode.DataBind();
   //         ddlSettlemode.Items.Insert(0, new ListItem("--Select Mode--", "-1"));
   //         ddlSettlemode.SelectedIndex = 0;
   //     }
   //     catch { throw; };
   // }
   // private void BindMapElement(DropDownList ddlMapElement)
   // {
   //     try
   //     {
   //         ddlMapElement.DataSource = UserMaster.GetMemberTypeList("map_element");
   //         ddlMapElement.DataValueField = "FIELD_VALUE";
   //         ddlMapElement.DataTextField = "FIELD_TEXT";
   //         ddlMapElement.DataBind();
   //         ddlMapElement.Items.Insert(0, new ListItem("--Select Mode--", "-1"));
   //         ddlMapElement.SelectedIndex = 1;
   //     }
   //     catch { throw; };
   // }
   // private void SetLocationMapDetails(DataRow dr, GridViewRow gvRow, string mode)
   // {
   //     try
   //     {
   //         dr["map_location_code"] = "";//TODo
   //         dr["map_doc_type"] = ((TextBox)gvRow.FindControl(mode +  "txtdocType")).Text;
   //         dr["map_module"] = ((TextBox)gvRow.FindControl(mode + "txtModule")).Text;            
   //         DropDownList ddlSettleMode = (DropDownList)gvRow.FindControl(mode + "ddlSettleMode");
   //         dr["visa_settle_mode"] = ddlSettleMode.SelectedItem.Value;
   //         dr["visa_settle_mode_name"] = ddlSettleMode.SelectedItem.Value == "-1" ? null : ddlSettleMode.SelectedItem.Text;
   //         dr["acct_code"] = ((TextBox)gvRow.FindControl(mode + "txtAccountCode")).Text;
   //         dr["acct_group"] = ((TextBox)gvRow.FindControl(mode + "txtAccountGrp")).Text;
   //         //dr["pax_visitor_passport_no"] = ((TextBox)gvRow.FindControl(mode + "txtPaxPassport")).Text;
   //         DropDownList ddlVisaElement = (DropDownList)gvRow.FindControl(mode + "ddlVisaElement");
   //         dr["visa_element"] = ddlVisaElement.SelectedValue;
   //         dr["visa_element_name"] = ddlVisaElement.SelectedItem.Text;
   //         dr["gl_acct_code"] = ((TextBox)gvRow.FindControl(mode + "txtGLCode")).Text;
   //         dr["map_status"] = Settings.ACTIVE;
   //        // dr["map_created_by"] = Settings.LoginInfo.UserID;
   //         //lblSuccessMsg.Text = string.Format("Location  '{0}'  is saved
   //     }
   //     catch { throw; }
   // }
   // protected void gvLocationDetails_RowCommand(object sender, GridViewCommandEventArgs e)
   // {
        
   //     try
   //     {
   //         if (e.CommandName == "Add")
   //         {
   //             GridViewRow gvRow = gvLocationDetails.FooterRow;
   //             DataRow dr = LocationMapDetails.NewRow();

   //             string docType = ((TextBox)gvRow.FindControl("FTtxtdocType")).Text;
   //             string mapModule = ((TextBox)gvRow.FindControl("FTtxtModule")).Text;            
   //             DropDownList ddlSettleMode = (DropDownList)gvRow.FindControl("FTddlSettleMode");
   //             string settleMode= ddlSettleMode.SelectedValue;
   //             DropDownList ddlVisaElement = (DropDownList)gvRow.FindControl("FTddlVisaElement");
   //             string visaElement = ddlVisaElement.SelectedValue;
   //             if (LocationMapDetails.Select(string.Format("map_doc_type='{0}' AND map_module='{1}' AND visa_settle_mode='{2}' AND visa_element='{3}'",docType,mapModule,settleMode,visaElement)).Length>0)
   //             {
   //                 throw new Exception("Combination of doc Type, Module, settle mode and visa element is already exist !");
   //             }

   //             DataTable dtDeleted = LocationMapDetails.GetChanges(DataRowState.Deleted);
   //             long deletedSerial = 0;
   //             if (dtDeleted != null)
   //             {
   //                 dtDeleted.RejectChanges();
   //                 deletedSerial = Utility.ToLong(dtDeleted.Compute("MAX(map_id)", ""));
   //             }
   //             long serial = Utility.ToLong(LocationMapDetails.Compute("MAX(map_id)", ""));
   //             serial = (serial >= deletedSerial) ? serial + 1 : deletedSerial + 1;
   //             //if (serial >= 0) serial = -1;


   //             dr["map_id"] = serial;
   //             SetLocationMapDetails(dr, gvRow, "FT");
   //             LocationMapDetails.Rows.Add(dr);
   //             BindGrid();
   //             // lnkSettlement_Click(null, null);

   //             //FilterSearchGrid();
   //         }
   //     }
   //     catch (Exception ex)
   //     {
   //         Utility.WriteLog(ex, this.Title);
   //         Label lblMasterError = (Label)this.Master.FindControl("lblError");
   //         lblMasterError.Visible = true;
   //         lblMasterError.Text = ex.Message;
   //         Utility.Alert(this.Page, ex.Message);
   //     }
   // }
   // protected void gvLocationDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
   // {
   //     try
   //     {
   //         GridViewRow gvRow = gvLocationDetails.Rows[e.RowIndex];
            
   //         string docType = ((TextBox)gvRow.FindControl("EITtxtdocType")).Text;
   //         string mapModule = ((TextBox)gvRow.FindControl("EITtxtModule")).Text;
   //         DropDownList ddlSettleMode = (DropDownList)gvRow.FindControl("EITddlSettleMode");
   //         string settleMode = ddlSettleMode.SelectedValue;
   //         DropDownList ddlVisaElement = (DropDownList)gvRow.FindControl("EITddlVisaElement");
   //         string visaElement = ddlVisaElement.SelectedValue;
   //         long serial = Utility.ToLong(gvLocationDetails.DataKeys[e.RowIndex].Value);
   //         if (LocationMapDetails.Select(string.Format("map_doc_type='{0}' AND map_module='{1}' AND visa_settle_mode='{2}' AND visa_element='{3}' AND MAP_ID<>{4}", docType, mapModule, settleMode, visaElement,serial )).Length > 0)
   //         {
   //             throw new Exception("Combination of doc Type, Module, settle mode and visa element is already exist !");
   //         }
           
   //         DataRow dr = LocationMapDetails.Rows.Find(serial);
   //         dr.BeginEdit();
   //         SetLocationMapDetails(dr, gvRow, "EIT");
   //         dr.EndEdit();

   //         //lblSuccessMsg.Text = string.Format("Location  '{0}'  is updated successfully!", location.Code);          
   //         //lblSuccessMsg.Text = Formatter.ToMessage("Location Code", location.Code, TranxAction.Updated);
   //         gvLocationDetails.EditIndex = -1;
   //         BindGrid();
   //         hdfmapingDetailsMode.Value = "0";
   //         //FilterSearchGrid();
   //         }
   //     catch (Exception ex)
        
   //     {
   //         Label lblMasterError = (Label)this.Master.FindControl("lblError");
   //         lblMasterError.Visible = true;
   //         lblMasterError.Text = ex.Message;
   //         Utility.WriteLog(ex, this.Title);
   //         Utility.Alert(this.Page, ex.Message);
   //     }
   // }
   // protected void gvLocationDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
   // {

   //     try
   //     {
   //         gvLocationDetails.EditIndex = -1;
   //         BindGrid();            
   //         hdfmapingDetailsMode.Value = "0";
   //         // FilterSearchGrid();
   //     }
   //     catch (Exception ex)
   //     {
   //         Utility.WriteLog(ex, this.Title);
   //         Label lblMasterError = (Label)this.Master.FindControl("lblError");
   //         lblMasterError.Visible = true;
   //         lblMasterError.Text = ex.Message;
   //     }
   // }
    
   // protected void gvLocationDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
   // {
   //     try
   //     {
            
   //         GridViewRow gvRow = gvLocationDetails.Rows[e.RowIndex];
   //         long serial = Utility.ToLong(gvLocationDetails.DataKeys[e.RowIndex].Value);
   //         LocationMapDetails.Rows.Find(serial).Delete();
   //         BindGrid();
   //         gvLocationDetails.EditIndex = -1;
   //         hdfmapingDetailsMode.Value = "0";
         
         
   //     }
   //     catch (Exception ex)
   //     {
   //         Utility.WriteLog(ex, this.Title);
   //         Label lblMasterError = (Label)this.Master.FindControl("lblError");
   //         lblMasterError.Visible = true;
   //         lblMasterError.Text = ex.Message;
   //     }
   //}

   
//    protected void Filter_Click(object sender, EventArgs e)
//    {

//        try
//        {
//            gvLocationDetails.EditIndex = -1;
//            FilterSearchGrid();
//        }
//        catch (Exception ex)
//        {
//            Utility.WriteLog(ex, this.Title);
//            Label lblMasterError = (Label)this.Master.FindControl("lblError");
//            lblMasterError.Visible = true;
//            lblMasterError.Text = ex.Message;
//        }
//    }
//    private void FilterSearchGrid()
//    {
//        try
//        {
//            string[,] filterValues = { { "HTtxtPaxName", "PAX_VISITOR_NAME" }, { "HTtxtPaxPassport", "PAX_VISITOR_PASSPORT_NO" }, { "HTtxtPaxNationality", "PAX_VISITOR_NATIONALITY_NAME" }, { "HTtxtVisaEntryNo", "PAX_VISA_ENTRY_NO" },
//                { " HTtxtPaxMobNo", "PAX_VISITOR_MOBILE" } ,{ " HTtxtPaxEmail", "PAX_VISITOR_EMAIL" } };
//            CommonGrid grid = new CommonGrid();
//            grid.FilterGridView(gvLocationDetails, LocationMapDetails, filterValues);
//            setRowId();
//        }
//        catch
//        {
//            throw;
//        }
//    }
//    #endregion  


}


