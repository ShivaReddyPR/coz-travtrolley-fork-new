﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using CT.BookingEngine;
using System.IO;
using CT.Core;

public partial class SightseeingAcctReportGUI :CT.Core.ParentPage //System.Web.UI.Page
{
    private string SIGHTSEEINGACCTREPORT = "_SightseeingAcctReport";
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.btnExport);
                //this.Master.PageRole = true;
                lblSuccessMsg.Text = string.Empty;
                hdfParam.Value = "1";
                Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
                if (!IsPostBack)
                {
                    ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                    InitializePageControls();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void InitializePageControls()
    {
        try
        {
            dcFromDate.Value = Utility.ToDate(DateTime.Now.Date);
            dcToDate.Value = Utility.ToDate(DateTime.Now.Date.ToString("MMM-dd-yyyy 23:59"));
            Array Sources = Enum.GetValues(typeof(SightseeingBookingSource));
            foreach (SightseeingBookingSource source in Sources)
            {
                if (source == SightseeingBookingSource.GTA)
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(SightseeingBookingSource), source), ((int)source).ToString());
                    ddlSource.Items.Add(item);
                }
            }
            //Array Statuses = Enum.GetValues(typeof(SightseeingBookingStatus));
            //foreach (SightseeingBookingStatus status in Statuses)
            //{
            //    if (status == SightseeingBookingStatus.Confirmed || status == SightseeingBookingStatus.Cancelled)
            //    {
            //        ListItem item = new ListItem(Enum.GetName(typeof(SightseeingBookingStatus), status), ((int)status).ToString());
            //        ddlStatus.Items.Add(item);
            //    }

            //}
            BindAgent();
            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgent.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgent.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            BindB2BAgent(Convert.ToInt32(ddlAgent.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
            BindGrid();

        }
        catch
        { throw; }

    }
    #endregion

    private void BindAgent()
    {
        try
        {
            ddlAgent.DataSource = AgentMaster.GetList(1, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("-- All --", "0"));
            ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
        }
        catch { throw; }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
        }
        catch { throw; }
    }

    # region Session
    private DataTable SightseeingAcctReport
    {
        get
        {
            return (DataTable)Session[SIGHTSEEINGACCTREPORT];
        }
        set
        {
            if (value == null)
                Session.Remove(SIGHTSEEINGACCTREPORT);
            else
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["sightseeingId"] };
                Session[SIGHTSEEINGACCTREPORT] = value;
            }
        }
    }


    # endregion

    private void BindGrid()
    {
        try
        {
            DateTime fromDate = Utility.ToDate(dcFromDate.Value);
            DateTime toDate = Utility.ToDate(dcToDate.Value);
            int agent = Utility.ToInteger(ddlAgent.SelectedValue);
            int source = Utility.ToInteger(ddlSource.SelectedValue);
            //int status = Utility.ToInteger(ddlStatus.SelectedItem.Value);
            int status =-1;
            int acctStatus = Utility.ToInteger(ddlAcctStatus.SelectedItem.Value);
            string agentType = string.Empty;
            if (agent == 0)
            {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty;// null Means binding in list all BOOKINGS
                }
            }
            if (agent > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agent > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agent = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                }
            }

            #region B2C purpose
            string transType = string.Empty;
            if (Settings.LoginInfo.TransType == "B2B")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2B";
            }
            else if (Settings.LoginInfo.TransType == "B2C")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2C";
            }
            else
            {
                ddlTransType.Visible = true;
                lblTransType.Visible = true;
                if (ddlTransType.SelectedItem.Value == "-1")
                {
                    transType = null;
                }
                else
                {
                    transType = ddlTransType.SelectedItem.Value;
                }
            }
            #endregion

            SightseeingAcctReport = ProductReport.SightseeingReportGetList(fromDate, toDate, agent, source, status, acctStatus, agentType, transType);
            Session["SightseeingAcctReport"] = SightseeingAcctReport;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSightseeingAcctReport, SightseeingAcctReport);
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(EventType.Exception, Severity.High, 0, "Exception from Sightseeing Report:" + ex.ToString(), "");
        }
    }

    protected void Filter_Click(object sender, EventArgs e)
    {
        try
        {
            string[,] textboxesNColumns ={ { "SStxtAgentCode", "agent_code" }, 
                                         { "SStxtAgentName", "agent_name" }, 
                                         { "SStxtBookingDate", "createdOn" },
                                         {"SStxtSupplier","sightseeingSource"},
                                            {"SStxtSupConfirm","confirmationNo"} ,
                                            {"SStxtSupReference","bookingRef"},
                                            {"SStxtDepDate","tourDate"},
                                            {"SStxtSightseeing","itemName"},
                                            {"SStxtPickupPoint","PickupPoint"},
                                            {"SStxtPickupTime","PickupTime"},
                                            {"SStxtLang","tourLanguage"},
                                            {"SStxtSpecialName","SpecialName"},
                                            {"SStxtCity","cityName"},
                                            {"SStxtPayableAmount","Payableamount"},
                                            {"SStxtInvoiceAmount","TotalAmount"},
                                            {"SStxtProfit","profit"},
                                            {"SStxtAgentSF","AgentSF"},
                                            {"SStxtInvoiceNo","InvoiceNo"},
                                            {"SStxtStatus","status"},
                                            {"SStxtAcctStatus","AccountedStatus"},
                                            {"SStxtPassengerName","PassengerName"},
                                            {"SStxtAdults","AdultCount"},
                                            {"SStxtChilds","ChildCount"},
                                            {"SStxtUserName","user_full_name"},
                                            {"SStxtLocation","LocationName"}};

            CommonGrid g = new CommonGrid();
            SightseeingAcctReport = (DataTable)Session["SightseeingAcctReport"];
            g.FilterGridView(gvSightseeingAcctReport, SightseeingAcctReport.Copy(), textboxesNColumns);
           
            SightseeingAcctReport = ((DataTable)gvSightseeingAcctReport.DataSource).Copy();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void gvSightseeingAcctReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            selectedItem();
            gvSightseeingAcctReport.PageIndex = e.NewPageIndex;
            gvSightseeingAcctReport.EditIndex = -1;
            Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnUpdateStatus_Click(object sender, EventArgs e)
    {
        try
        {

            selectedItem();
            isTrackingEmpty();

            //int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            foreach (DataRow dr in SightseeingAcctReport.Rows)
            {
                if (dr != null)
                {
                    bool selected = Utility.ToString(dr["isAccounted"]) == "Y" ? true : false;
                    long sightseeingId = Utility.ToLong(dr["sightseeingId"]);
                    bool isUpdated = Utility.ToString(dr["isAccounted"]) == "U" ? true : false;
                    if (selected && !isUpdated)
                    {
                        ProductReport.UpdateSightseeingAcctStatus(sightseeingId, (int)Settings.LoginInfo.UserID);
                    }
                }

            }
            BindGrid();
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = "Updated Successfully";
            //hdfSelectCount.Value = "0";


        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.StartupScript(this.Page, "alert('" + ex.Message + "');", "Err");
        }

    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
           
            string attachment = "attachment; filename=SightseeingAcctReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);

            dgSightseeingAcctReportList.AllowPaging = false;
            dgSightseeingAcctReportList.DataSource = SightseeingAcctReport;
            dgSightseeingAcctReportList.DataBind();
            dgSightseeingAcctReportList.RenderControl(htw);
            Response.Write(sw.ToString());
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);


        }
        finally
        {
            Response.End();
        }

    }

    protected void dgSightseeingAcctReportList_ItemDataBound(Object sender, DataGridItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Header || e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {
            if (Settings.LoginInfo.AgentType == AgentType.B2B || Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                e.Item.Cells[3].Visible = false;
                e.Item.Cells[16].Visible = false;
                e.Item.Cells[18].Visible = false;
                e.Item.Cells[23].Visible = false;
               
            }

        }
    }


    protected void gvSightseeingAcctReport_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if (Settings.LoginInfo.AgentType == AgentType.B2B || Settings.LoginInfo.AgentType == AgentType.B2B2B)
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                Label lblSupplier = (Label)e.Row.FindControl("ITlblSupplier");
                Label lblProfit = (Label)e.Row.FindControl("ITlblProfit");
                Label lblAcctStatus = (Label)e.Row.FindControl("ITlblAcctStatus");
                Label lblPayableAmount = (Label)e.Row.FindControl("ITlblPayableAmount");
                lblSupplier.Visible = false;
                lblProfit.Visible = false;
                
                lblAcctStatus.Visible = false;
                lblPayableAmount.Visible = false;

            }
            else
            {
                if (e.Row.RowType == DataControlRowType.Header)
                {
                    CT.TicketReceipt.Web.UI.Controls.Filter SStxtSupplier = (CT.TicketReceipt.Web.UI.Controls.Filter)e.Row.FindControl("SStxtSupplier");
                    CT.TicketReceipt.Web.UI.Controls.Filter SStxtProfit = (CT.TicketReceipt.Web.UI.Controls.Filter)e.Row.FindControl("SStxtProfit");
                   
                    CT.TicketReceipt.Web.UI.Controls.Filter SStxtAcctStatus = (CT.TicketReceipt.Web.UI.Controls.Filter)e.Row.FindControl("SStxtAcctStatus");
                    CT.TicketReceipt.Web.UI.Controls.Filter SStxtPayableAmount = (CT.TicketReceipt.Web.UI.Controls.Filter)e.Row.FindControl("SStxtPayableAmount");
                    SStxtSupplier.Visible = false;
                    SStxtProfit.Visible = false;
                  
                    SStxtAcctStatus.Visible = false;
                    SStxtPayableAmount.Visible = false;

                }
            }
        }
    }

    private void selectedItem()
    {
        try
        {
            foreach (System.Data.DataColumn col in SightseeingAcctReport.Columns) col.ReadOnly = false;
            foreach (GridViewRow gvRow in gvSightseeingAcctReport.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                HiddenField hdfSightseeingId = (HiddenField)gvRow.FindControl("IThdfSightseeingId");

                foreach (DataRow dr in SightseeingAcctReport.Rows)
                {
                    if (dr != null)
                    {
                        dr.BeginEdit();
                        if (Utility.ToString(dr["sightseeingId"]) == hdfSightseeingId.Value)
                        {
                            if (Utility.ToString(dr["isAccounted"]) != "U") dr["isAccounted"] = chkSelect.Checked ? "Y" : "N";
                        }
                        dr.EndEdit();
                    }
                }
            }
        }
        catch { throw; }
    }
    private void isTrackingEmpty()
    {
        try
        {
            bool _selected = false;
            foreach (DataRow dr in SightseeingAcctReport.Rows)
            {
                if (dr != null)
                {
                    bool isUpdated = Utility.ToString(dr["isAccounted"]) == "U" ? true : false; ;
                    if (!isUpdated && Utility.ToString(dr["isAccounted"]) == "Y")
                    {
                        _selected = true;
                        return;
                    }
                }
            }
            string strMsg = "Please Select atleast one Item ! ";
            if (!_selected)
                Utility.Alert(this.Page, strMsg);
        }
        catch { throw; }
    }

    //Binding B2B Agents
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Binding B2B2B Agents
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
            BindB2BAgent(agentId);
            BindB2B2BAgent(agentId);
            hdfParam.Value = "0";
        }
        catch (Exception ex)
        { }
    }

    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            hdfParam.Value = "0";

        }
        catch (Exception ex)
        { }
    }


    #region Date Format
    protected string CTDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string CTDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }

    protected string CTCurrencyFormat(object currency, object decimalPoint)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + decimalPoint);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + decimalPoint);
        }
    }
    #endregion
}
