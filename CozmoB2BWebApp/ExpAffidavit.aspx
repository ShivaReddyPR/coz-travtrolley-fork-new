﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="ExpAffidavit.aspx.cs" Inherits="CozmoB2BWebApp.ExpAffidavit" %>

<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <style>
        @media print{.receipt-wrapper{color:#000!important}}
        @media print{.receipt-visitor-info th{color:#000!important}}
        @media print{.receipt-wrapper{font-family:Arial,Helvetica,sans-serif;width:100%}.receipt-visitor-info th{color:#fff;text-shadow:0 0 0 #cfffcc}}
        @media print and (-webkit-min-device-pixel-ratio:0){.receipt-visitor-info th{color:#fff;-webkit-print-color-adjust:exact}}
    </style>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/Common/Common.js" ></script>
    <script type="text/javascript">

        var mt = ''; var apiHost = mt; var bearer = mt; var cntrllerPath = 'api/expTransactions';         
        var apiAgentInfo = {}; var expRepDetails = {}; var expenseReport = {}; var selExpDtlId = 0;

        $(document).ready(function() {

            <%--var selExpDtlId = '<%=Request.QueryString["ExpDtlId"] == null ? "0" : Request.QueryString["ExpDtlId"]%>';--%>
            var pageParams = JSON.parse(AjaxCall('ExpCreate.aspx/GetSetPageParams', "{'sessionKey':'ExpAffidavit', 'sessionData':'', 'action':'get'}"));
            var selExpDtlId = !IsEmpty(pageParams) && !IsEmpty(pageParams.ExpDtlId) ? pageParams.ExpDtlId : 0;
            
            /* Check the expense detail ref no and revert if not valid */
            if (selExpDtlId == 0) {

                ShowError('Invalid expense reference no.');
                return;
            }
            
            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();
            
            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo)) {

                ShowError('Session expired, please login once again.');
                return;
            }
            
            /* Prepare expense api host url and assign to global variable */
            GetExpenseApiHost();
            
            /* Load screen data */
            GetScreenData(selExpDtlId);      
        });

        /* To get expense info and corp profiles */
        function GetScreenData(affExpDtlId) {
            
            var reqData = { AgentInfo: apiAgentInfo, ExpDtlId: affExpDtlId };
            var apiUrl = apiHost.trimRight('/') + '/' + cntrllerPath.trimRight('/') + '/generateAffidavit';
            WebApiReq(apiUrl, 'POST', reqData, '', BindScreenData, null, null);                        
        }

        /* To bind expense info */
        function BindScreenData(screenData) {

            $('#lblExpType').append(screenData[0].eT_Desc);
            $('#spnTranDate').append(screenData[0].eD_TransDate);
            $('#spnVendor').append(screenData[0].eD_Vendor);
            $('#spnCurrency').append(screenData[0].eD_Currency);
            $('#spnAmount').append(screenData[0].eD_TotalAmount);            
            $('#spnCity').append(screenData[0].ecT_Name);

            var time = new Date().toLocaleTimeString();
            var hrsmin = time.slice(0, 5);
            var ampm = time.split(' ')[1];

            var date = new Date().toDateString();
            var dtarray = date.split(' ');

            var offset = new Date().getTimezoneOffset()
            $('#tdName').append('<strong>' + screenData[0].profileName + '</strong><br>' + dtarray[2] + '-' + dtarray[1] + '-' + dtarray[3] + '<br>' + hrsmin + ' ' + ampm + ' ' + offset);

        }

        /* To get agent and login info */
        function GetAgentInfo() {

            try {

                var loginInfo = '<%=Settings.LoginInfo == null%>';

                if (loginInfo == true)
                    return apiAgentInfo;

                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

        /* To get expense api host url */
        function GetExpenseApiHost() {

            apiHost = '<%=System.Configuration.ConfigurationManager.AppSettings["WebApiExpenseUrl"]%>';

            if (IsEmpty(apiHost))                                 
                apiHost = ('<%=Request.IsSecureConnection%>' == 'True' ? '<%=Request.Url.Scheme%>' : 'https') + "://" + '<%=Request.Url.Host%>' + "/ExpenseWebApi";

            return apiHost;
        }

        /* Print affidavit. */
        function printReport() {

            document.getElementById('btnPrint').style.display = "none";
            window.print();
            document.getElementById('btnPrint').style.display = "block";
        }

    </script>
</head>
<body>
    <form id="form1" runat="server">
        <span class="preheader"></span>
        <table class="body">
            <tr>
                <td class="center" align="center" valign="top">
                    <center data-parsed>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="receipt-wrapper float-center" align="center" style="-webkit-print-color-adjust:exact;border:1px solid #cacbcb;color:#373737;font-family:Arial,Helvetica,sans-serif;font-size:12px;line-height:normal;margin:20px auto 0 auto;padding:10px;position:relative;width:650px">
                            <tbody>
                                <tr>
                                    <td><!--Header-->
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" class="receipt-header">
                                            <tbody>
                                                <tr>
                                                    <td><img src="<%=imagePath%>" alt width="130px"></td>
                                                    <td class="receipt" style="font-weight:700;line-height:20px;text-align:left">
                                                        <div style="font-size:16px">Missing Receipt Affidavit - <label id="lblExpType"></label></div>
                                                    </td>
                                                    <td><a style="float:right" class="btn btn-primary mr-2" id="btnPrint" onclick="return printReport()">Print Report</a><td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="50%" border="0" cellspacing="0" cellpadding="0" class="receipt-general-info" style="padding-bottom:10px;padding-top:10px">
                                            <tbody>
                                                <tr>
                                                    <td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;border-top:0;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%">
                                                        <span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Date of Expense</span> 
                                                        <span id="spnTranDate" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%">
                                                            
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%">
                                                        <span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Vendor</span> 
                                                        <span id="spnVendor" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%">
                                                            
                                                        </span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%">
                                                        <span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Currency</span> 
                                                        <span id="spnCurrency" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%">
                                                        <span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">Amount</span> 
                                                        <span id="spnAmount" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%"></span>
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="background-color:#f5f5f5;border:1px solid #fff;border-bottom:1px solid #e6e6e6;border-right:0;box-shadow:inset 0 0 0 1000px #f5f5f5;padding:6px 10px 8px 10px;width:50%">
                                                        <span class="text-left" style="color:#6b6b6b;display:inline-block;float:left;width:40%">City</span> 
                                                        <span id="spnCity" class="bold-text" style="display:inline-block;float:left;font-weight:700;vertical-align:top;width:60%"></span>
                                                    </td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" style="margin-bottom:0;margin-top:20px" cellspacing="0" cellpadding="0" class="receipt-visitor-info">
                                            <tbody>
                                                <tr>
                                                    <td style="border-top:1px solid #cecece;padding:8px 10px">
                                                        Acceptance: I certify that I lost my receipt and this is a valid business expense.
                                                    </td>
                                                </tr>
                                                <tr>
                                                    <td style="border-top:1px solid #cecece;padding:8px 10px"></td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0" style="margin-top:20px" class="">
                                            <tbody><tr class="" style="text-align:right"><td id="tdName"></td></tr></tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </center>
                </td>
            </tr>
        </table>
        <!-- prevent Gmail on iOS font size manipulation -->
        <div style="display:none;white-space:nowrap;font:15px courier;line-height:0">&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
    </form>
</body>
</html>
