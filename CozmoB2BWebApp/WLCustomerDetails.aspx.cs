﻿using System;
using System.Collections.Generic;
using System.Web.UI;
using CT.BookingEngine.WhiteLabel;

public partial class WLCustomerDetails : CT.Core.ParentPage
{
    protected List<Customer> customerlist = new List<Customer>();
    protected string SearchBox;
    protected string Searchby;
    protected int pageno, noofpages;
    protected string errormessage = "";
    protected int setno, noofsets;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        Page.Title = "WLCustomerDetails";
        if (Request["pageNo"] == null)
        {
            pageno = 1;
            setno = 1;
        }
        else
        {
            pageno = Convert.ToInt32((Request["pageNo"]).Trim());
            setno = Convert.ToInt32((Request["setno"]).Trim());
        }
        if (Request["SearchBox"] == null)
            SearchBox = "";

        if ((Request["SearchBox"]) != "" && Request["SearchBox"] != null)
        {
            SearchBox = (Request["SearchBox"]).Trim();
            Searchby = (Request["Searchby"]).Trim();
            noofpages = Customer.GetPageCount(Customer.GeneratewhereString(Searchby, SearchBox, CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId));
            if (noofpages % 5 == 0)
                noofsets = noofpages / 5;
            else
                noofsets = (noofpages / 5) + 1;
            if (Request["pageNo"] != null)
                customerlist = Customer.Load(Convert.ToInt32(Request["pageNo"]), Customer.GeneratewhereString(Searchby, SearchBox, CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId));
            else
                customerlist = Customer.Load(1, Customer.GeneratewhereString(Searchby, SearchBox, CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId));

            if (customerlist.Count == 0)
                errormessage = "There are no results to show";

        }
        else
        {
            noofpages = Customer.GetPageCount(Customer.GeneratewhereString(Searchby, SearchBox, CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId));
            if (noofpages % 5 == 0)
                noofsets = noofpages / 5;
            else
                noofsets = (noofpages / 5) + 1;
            if (Request["pageNo"] != null)
                customerlist = Customer.Load(Convert.ToInt32(Request["pageNo"]), Customer.GeneratewhereString(Searchby, SearchBox, CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId));
            else
                customerlist = Customer.Load(1, Customer.GeneratewhereString(Searchby, SearchBox, CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId));


        }
        
    }
}
