﻿using System;
using CT.BookingEngine;
using CT.Core;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;

public partial class PrintPackageUI :CT.Core.ParentPage// System.Web.UI.Page
{
    #region variables
    protected string siteName = string.Empty;
    //protected WSUserPreference preference;
    protected HolidayPackage deal;
    protected List<HolidayPackage> leftDeals;
    protected string imageServerPath;
    protected decimal minPrice = 0;
    protected string[] inclusions;
    protected int inclusionsLimit = 0;
    protected List<string> itinaryList = new List<string>();
    protected string[] splitter = { "#&#" };
    protected string[] dayItinerary;
    protected string[] price;
    protected string[] notes;
    protected string[] terms;
    //protected HolidaySettings holidaySettings = new HolidaySettings();
    protected string redirectToPage = "packageLogin.aspx?Key=";
    #endregion

    /// <summary>
    /// Subroutine to handle page load event.
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        imageServerPath = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["PackageImagesFolder"]);
        if (Session["customer"] != null)
        {
            redirectToPage = "Package_PaxDetail.aspx?Key=";
        }

        int dealId = 0;
        //if (Request["packageId"] == null || Request["packageId"] == "")
        //{
        //    Response.Redirect("Default.aspx");
        //}
        //else
        //{
            try
            {
                //modified on 24/05/2016
                HolidayPackage.AgentCurrency = Settings.LoginInfo.Currency;
                HolidayPackage.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                dealId = Convert.ToInt32(Request["packageId"]);
                deal = HolidayPackage.LoadHotelDeal(dealId,Settings.LoginInfo.AgentId);
                if (deal.DealId <= 0)
                {
                    Response.Redirect("Default.aspx");
                }
                minPrice = deal.HotelSeasonList[0].TwinSharingPrice;
                //minPrice = deal.HotelSeasonList[0].SinglePrice;
                for (int i = 1; i < deal.HotelSeasonList.Count; i++)
                {
                    if (minPrice > deal.HotelSeasonList[i].TwinSharingPrice)
                    //if (minPrice > deal.HotelSeasonList[i].SinglePrice)
                    {
                        minPrice = deal.HotelSeasonList[i].TwinSharingPrice;
                        //minPrice = deal.HotelSeasonList[i].SinglePrice;
                    }
                }
                inclusions = deal.Inclusions.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                if (inclusions.Length % 2 != 0)
                {
                    inclusionsLimit = (inclusions.Length / 2) + 1;
                }
                else
                {
                    inclusionsLimit = inclusions.Length / 2;
                }
                if (deal.Itinerary1 != null && deal.Itinerary1 != string.Empty && deal.Itinerary1.Trim().Length > 0)
                {
                    itinaryList.Add(deal.Itinerary1);
                    if (deal.Itinerary2 != null && deal.Itinerary2 != string.Empty && deal.Itinerary2.Trim().Length > 0)
                    {
                        itinaryList.Add(deal.Itinerary2);
                    }
                    if (deal.Itinerary3 != null && deal.Itinerary3 != string.Empty && deal.Itinerary3.Trim().Length > 0)
                    {
                        itinaryList.Add(deal.Itinerary3);
                    }
                    if (deal.Itinerary4 != null && deal.Itinerary4 != string.Empty && deal.Itinerary4.Trim().Length > 0)
                    {
                        itinaryList.Add(deal.Itinerary4);
                    }
                    if (deal.Itinerary5 != null && deal.Itinerary5 != string.Empty && deal.Itinerary5.Trim().Length > 0)
                    {
                        itinaryList.Add(deal.Itinerary5);
                    }
                    if (deal.Itinerary6 != null && deal.Itinerary6 != string.Empty && deal.Itinerary6.Trim().Length > 0)
                    {
                        itinaryList.Add(deal.Itinerary6);
                    }
                    if (deal.Itinerary7 != null && deal.Itinerary7 != string.Empty && deal.Itinerary7.Trim().Length > 0)
                    {
                        itinaryList.Add(deal.Itinerary7);
                    }
                    if (deal.Itinerary8 != null && deal.Itinerary8 != string.Empty && deal.Itinerary8.Trim().Length > 0)
                    {
                        itinaryList.Add(deal.Itinerary8);
                    }
                    if (deal.Itinerary9 != null && deal.Itinerary9 != string.Empty && deal.Itinerary9.Trim().Length > 0)
                    {
                        itinaryList.Add(deal.Itinerary9);
                    }
                    if (deal.Itinerary10 != null && deal.Itinerary10 != string.Empty && deal.Itinerary10.Trim().Length > 0)
                    {
                        itinaryList.Add(deal.Itinerary10);
                    }
                    if (deal.Itinerary11 != null && deal.Itinerary11 != string.Empty && deal.Itinerary11.Trim().Length > 0)
                    {
                        itinaryList.Add(deal.Itinerary11);
                    }
                    if (deal.Itinerary12 != null && deal.Itinerary12 != string.Empty && deal.Itinerary10.Trim().Length > 0)
                    {
                        itinaryList.Add(deal.Itinerary12);
                    }
                    if (deal.Itinerary13 != null && deal.Itinerary13 != string.Empty && deal.Itinerary11.Trim().Length > 0)
                    {
                        itinaryList.Add(deal.Itinerary13);
                    }
                }
                price = deal.PriceExclude.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                notes = deal.Notes.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                terms = deal.TermsAndConditions.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Print Package page " + ex.Message, "0"); 
            }
        //}
    }
}
