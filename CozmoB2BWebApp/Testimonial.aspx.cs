﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.CMS;
using CT.TicketReceipt.BusinessLayer;
using System.IO;
using CT.TicketReceipt.Web.UI.Controls;

public partial class TestimonialGUI : CT.Core.ParentPage
{
    private string TESTIMONIAL_SESSION = "_TestimonialMaster";
    private string SEARCH_SESSION = "_SearchList";
    /// <summary>
    /// Page load event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);

        }
    }
    
    private void InitializePageControls()
    {
        string rootFolder = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["TestimonialImage"]);
        ImageTestimonial.SavePath = rootFolder;
        string TestimonialImg = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss:fff")).Replace(":", "");
        TestimonialImg = TestimonialImg.Replace("-", "");
        ImageTestimonial.FileName = TestimonialImg;
        string testimonialImgPath = Server.MapPath("~/" + rootFolder + "/") + TestimonialImg;
        hdfTestimonialImg.Value = testimonialImgPath;
        Clear();
    }
    /// <summary>
    /// Clear function
    /// </summary>
    private void Clear()
    {
        try
        {
            txtName.Text = string.Empty;
            txtTitle.Text = string.Empty;
            txtDescription.Text = string.Empty;
            ddlStatus.SelectedIndex = 0;
            hdfCount.Value = "0";
            lnkView.Text = "";
            imgPreview.Style.Add("display", "none");
            ImageTestimonial.Clear();
            CurrentObject = null;
        }
        catch { }
    }
    private Testimonial CurrentObject
    {
        get
        {
            return (Testimonial)Session[TESTIMONIAL_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(TESTIMONIAL_SESSION);
            }
            else
            {
                Session[TESTIMONIAL_SESSION] = value;
            }

        }
    }
    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ID"] };
            Session[SEARCH_SESSION] = value;
        }
    }

    /// <summary>
    /// Save function
    /// </summary>
    private void Save()
    {
        try
        {
            Testimonial objTestimonial;
            if (CurrentObject == null)
            {
                objTestimonial = new Testimonial();
            }
            else
            {
                objTestimonial = CurrentObject;
            }
            objTestimonial.Name = txtName.Text.Trim();
            objTestimonial.Product = 1; //Hard Coded right Now
            objTestimonial.Title = txtTitle.Text.Trim();
            objTestimonial.Description = txtDescription.Text.Trim();
            objTestimonial.Status = ddlStatus.SelectedItem.Value;
            objTestimonial.CreatedBy = Settings.LoginInfo.UserID;
            //Getting Site Full path
            string rootFolder = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["TestimonialImage"]);
            objTestimonial.ImagePath = Request.Url.ToString().Substring(0, Request.Url.ToString().LastIndexOf('/')) + "/";
            objTestimonial.ImagePath = objTestimonial.ImagePath + rootFolder;
            if (!string.IsNullOrEmpty(ImageTestimonial.FileExtension))
            {
                objTestimonial.ImageFileName = ImageTestimonial.FileName + ImageTestimonial.FileExtension;
            }
            objTestimonial.Save();
            string testimonialId = Utility.ToString(objTestimonial.RetTestimonialId);
            if (!string.IsNullOrEmpty(ImageTestimonial.FileExtension))
            {
                string imgFullPath = hdfTestimonialImg.Value + ImageTestimonial.FileExtension;
                RenameImage(testimonialId, imgFullPath);
            }
            lblSuccessMsg.Visible = true;
            if (CurrentObject == null)
            {
                lblSuccessMsg.Text = "Saved Successfully";
            }
            else
            {
                lblSuccessMsg.Text = "Updated Successfully";
            }
            Clear();
        }
        catch { }
    }

    /// <summary>
    /// Renaming Image File name
    /// </summary>
    /// <param name="activityId"></param>
    /// <param name="imagePath"></param>
    protected void RenameImage(string activityId, string imagePath)
    {
        try
        {
            string source = imagePath.Substring(0, imagePath.LastIndexOf("\\") + 1);
            string oldFile = Path.GetFileName(imagePath);
            string fileExtension = Path.GetExtension(imagePath);
            string dFilePath = string.Empty;
            string newfile = activityId + "" + fileExtension;
            if (System.IO.File.Exists(source + newfile)) System.IO.File.Delete(source + newfile);
            System.IO.File.Move(source + oldFile, source + newfile);
            if (System.IO.File.Exists(source + oldFile)) System.IO.File.Delete(source + oldFile);
        }
        catch { }
    }
    /// <summary>
    /// Save button Event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    /// <summary>
    /// Clear Event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    /// <summary>
    /// Search Click event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    /// <summary>
    /// Grid Binding
    /// </summary>
    private void bindSearch()
    {
        try
        {
            DataTable dt = Testimonial.GetList();
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch { throw; }
    }
    //Grid Filtering
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{ "HTtxtName", "NAME" },{"HTtxtTitle", "TITLE" }, 
            { "HTtxtStatus", "StatusName" }};
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    /// <summary>
    /// Grid paging event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    /// <summary>
    /// Grid Editing event
    /// </summary>
    /// <param name="sender"></param>
    /// <param name="e"></param>
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            long id = Utility.ToLong(gvSearch.SelectedValue);
            Edit(id);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    /// <summary>
    /// Edit Method
    /// </summary>
    /// <param name="id"></param>
    private void Edit(long id)
    {
        try
        {
            Testimonial objTestimonial = new Testimonial(id);
            CurrentObject = objTestimonial;
            //Getting Default Root folder
            string rootFolder = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["TestimonialImage"]);
            ImageTestimonial.SavePath = rootFolder;
            txtName.Text = objTestimonial.Name;
            txtTitle.Text = objTestimonial.Title;
            txtDescription.Text = objTestimonial.Description;
            ddlStatus.SelectedValue = objTestimonial.Status;
            if (!string.IsNullOrEmpty(objTestimonial.ImageFileName))
            {
                ImageTestimonial.DefaultImageUrl = "~/images/common/Preview.png";
                lnkView.Visible = true;
                lnkView.Text = "View Image";
                hdfImgPath.Value = rootFolder + "" + objTestimonial.ImageFileName;
                ImageTestimonial.FileExtension = null;
            }
            btnSave.Text = "Update";
            btnClear.Text = "Cancel";
        }
        catch { }
    }
}
