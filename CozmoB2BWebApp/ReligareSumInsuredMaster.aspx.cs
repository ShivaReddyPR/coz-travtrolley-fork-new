﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using System.IO;
using System.Collections.Generic;
using ReligareInsurance;
using CT.Core;

namespace CozmoB2BWebApp
{
    public partial class ReligareSumInsuredMaster : CT.Core.ParentPage
    {
        
         

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
               
                IntialiseControls();//loads the Drop Dwon binding methods

            }

        }

        #region Binding CT_T_Religare_ProductType table
         void BindProductTypeId()
        {
            try
            {
                ddlProductTypeId.DataSource = ReligareProductDetails.GetList();
                ddlProductTypeId.DataValueField = "PRODUCTID";
                ddlProductTypeId.DataTextField = "PRODUCTNAME";
                ddlProductTypeId.DataBind();
                ddlProductTypeId.Items.Insert(0, new ListItem("Select Product Type", "-1"));

            }
            catch (Exception ex) { throw ex; }
        }
        #endregion

        #region Binding CT_T_Religare_ProductDetails table
         void BindProductDetailsId(int product_type_id)
        {
            try
            {
                ddlProductId.DataSource = SumInsured.GetList(product_type_id);
                ddlProductId.DataValueField = "PRODUCT_ID";
                ddlProductId.DataTextField = "PRODUCT_NAME";
                ddlProductId.DataBind();
                ddlProductId.Items.Insert(0, new ListItem("Select Product Type Name", "-1"));
               

            }
            catch(Exception ex) { throw ex; }
        }
        #endregion
        #region loads the Drop Dwon binding methods
        void IntialiseControls()
        {
            BindProductTypeId();
            BindProductDetailsId(Utility.ToInteger(ddlProductTypeId.SelectedItem.Value));
        }
        #endregion
        #region Methods

         void Save()
        {
            try
            {
                SumInsured insured=new SumInsured();
                if (Utility.ToInteger(hdfMeid.Value)>0)
                {
                    insured.ID = Utility.ToLong(hdfMeid.Value);
                }
                else
                {
                    insured.ID = -1;
                }
               
                insured.PRODUCT_TYPE_ID = Utility.ToInteger(ddlProductTypeId.SelectedItem.Value);
                insured.PRODUCT_ID = Utility.ToInteger(ddlProductId.SelectedItem.Value);
                insured.RANGE_ID = txtRangId.Text;
                insured.RANGE_DESCRIPTION = txtRangeDescription.Text;
                insured.COVER_TYPE = txtCoverType.Text;
                insured.CREATED_BY = Settings.LoginInfo.UserID;
                insured.Save();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("Details Successfully", "", hdfMeid.Value == "0" ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);
                btnSave.Text = "Save";
                Clear();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

         void bindSearch()
        {

            try
            {

                DataTable dt = SumInsured.GetAdditionalServiceMasterDetails();
                if (dt.Rows.Count > 0)
                {
                    gvSearch.DataSource = dt;
                    gvSearch.DataBind();
                }
            }

            catch
            {
                throw;
            }

        }

         void Clear()
        {
            txtRangId.Text = string.Empty;
            txtRangeDescription.Text = string.Empty;
            txtCoverType.Text = string.Empty;
            ddlProductId.SelectedIndex = -1;
            ddlProductTypeId.SelectedIndex = -1;
        }

         void Edit(long id)
        {
            try
            {
                SumInsured insured = new SumInsured(id);
                hdfMeid.Value = Utility.ToString(insured.ID);
                ddlProductTypeId.SelectedValue = Convert.ToString(insured.PRODUCT_TYPE_ID);
                BindProductDetailsId(insured.PRODUCT_TYPE_ID);
                ddlProductId.SelectedValue = Utility.ToString(insured.PRODUCT_ID);
                txtRangId.Text = insured.RANGE_ID;
                txtRangeDescription.Text = insured.RANGE_DESCRIPTION;
                txtCoverType.Text = insured.COVER_TYPE;
                btnSave.Text = "Update";
                btnClear.Text = "Cancel";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Dropdown clicking events
        protected void ddlProductTypeId_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindProductDetailsId(Utility.ToInteger(ddlProductTypeId.SelectedItem.Value));
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        #endregion
        #region Button clicking events
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.ShowSearch("Search");
                bindSearch();
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        #endregion
        #region Gridview events
        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                long suminsuredid = Utility.ToInteger(gvSearch.SelectedValue);
                Edit(suminsuredid);
                this.Master.HideSearch();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }

        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                gvSearch.EditIndex = -1;
                bindSearch();
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        #endregion

    }
}
