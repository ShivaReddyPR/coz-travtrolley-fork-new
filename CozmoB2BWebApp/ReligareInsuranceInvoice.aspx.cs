﻿using CT.BookingEngine;
using CT.BookingEngine.Insurance;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using ReligareInsurance;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class ReligareInsuranceInvoice :ParentPage   
    {
        ReligareHeader header = null;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Settings.LoginInfo != null)
                {
                    int headerId = Convert.ToInt32(Request.QueryString["insHdrId"]);// 480;// Convert.ToInt32(Session["HeaderId"].ToString());                                      
                    ReligareHeader insheader = new ReligareHeader();
                    insheader.Load(headerId);                    
                    if (headerId > 0)
                    {
                        header = new ReligareHeader();                         
                        DataSet ds = header.GetHeaderDetails(headerId);
                        int paxCount = Convert.ToInt32(ds.Tables[0].Rows[0]["NoofPax"]);
                        AgentMaster agent = new AgentMaster(insheader.Agent_id);
                         //Client Details
                        lblagencyName.Text = agent.Name;
                        lblAgencyCode.Text = agent.Code;
                        lblAgencyAddress.Text = agent.Address;
                        lblAgencyMobile.Text = agent.Phone1;
                        //Invoice Details
                        DataTable dtinvoice = header.GetInvoiceDetails(headerId);
                        if (dtinvoice != null && dtinvoice.Rows.Count > 0)
                        {
                            lblInvoiceDate.Text = Convert.ToDateTime(Convert.ToString(dtinvoice.Rows[0]["createdOn"])).ToString("dd/MMM/yyyy"); // DateTime.Now.ToString("dd/MMM/yyyy");
                            lblInvoicedBy.Text = ds.Tables[0].Rows[0]["ProductName"].ToString();
                            lblInvoiceNo.Text = "RG"+Convert.ToString(dtinvoice.Rows[0]["invoiceNumber"]);
                        }
                        // Booking Details
                        lblBookingDate.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["created_on"]).ToString("dd/MMM/yyyy");
                        lblTravelDate.Text = Convert.ToDateTime(ds.Tables[0].Rows[0]["Startdate"]).ToString("dd/MMM/yyyy");
                        lblPlanName.Text = ds.Tables[0].Rows[0]["ProductName"].ToString();
                        lblPolicyNo.Text = ds.Tables[0].Rows[0]["policy_no"].ToString();
                        //Rate Details
                        //decimal basefare = paxCount * Convert.ToDecimal(ds.Tables[0].Rows[0]["o_Premium"].ToString());
                        decimal discount = Convert.ToDecimal(ds.Tables[0].Rows[0]["Discount"].ToString());
                        //decimal markuppercentage = Convert.ToDecimal(ds.Tables[0].Rows[0]["MarkUp"].ToString());
                        decimal gst = 0;
                        //if (discountPercentage >0)
                        //    basefare -= (basefare* discountPercentage)/ 100;

                        //if (markuppercentage > 0)
                        //{
                        //    basefare -= Convert.ToDecimal(ds.Tables[0].Rows[0]["MarkupValue"]);
                        //    gst = ((Convert.ToDecimal(ds.Tables[0].Rows[0]["MarkupValue"]) * 18) / 100);
                        //}
                        tdcgst.Visible = false;
                        gst = Convert.ToDecimal(ds.Tables[0].Rows[0]["GstTotal"]);
                        List<GSTTaxDetail> gstTaxDetails = GSTTaxDetail.LoadGSTDetailsByProductId(headerId, (int)ProductType.Insurance);
                        if (gstTaxDetails != null)
                        {
                            foreach(GSTTaxDetail gstdetail in gstTaxDetails)
                            {
                                 if (gstdetail.TaxCode == "IGST" || gstdetail.TaxCode == "SGST")
                                {
                                    lblSgst.Text = gstdetail.TaxCode + " ("+ Convert.ToDecimal(gstdetail .TaxValue).ToString("N" + agent.DecimalValue )+ " %)";
                                    lblsgstValue.Text = Convert.ToDecimal(gstdetail.TaxAmount).ToString("N" + agent.DecimalValue);
                                 }
                                if (gstdetail.TaxCode == "CGST")
                                {
                                    tdcgst.Visible = true;
                                    lblCgst.Text = gstdetail.TaxCode + " (" + Convert.ToDecimal(gstdetail.TaxValue).ToString("N" + agent.DecimalValue) + " %)";
                                    lblcgstValue.Text = Convert.ToDecimal(gstdetail.TaxAmount).ToString("N" + agent.DecimalValue);
                                }
                            }
                        }
                        
                        decimal totalamount = gst+ Convert.ToDecimal(ds.Tables[0].Rows[0]["premium_Amount"]) + Convert.ToDecimal(ds.Tables[0].Rows[0]["Markup"]) + Convert.ToDecimal(ds.Tables[0].Rows[0]["HandlingFee"]);
                        lblMarkup.Text = Convert.ToDecimal(ds.Tables[0].Rows[0]["Markup"]).ToString("N" + agent.DecimalValue);
                        lblDiscount.Text = "-"+Convert.ToDecimal(ds.Tables[0].Rows[0]["Discount"]).ToString("N" + agent.DecimalValue);
                        lblTotalAmount.Text = Convert.ToDecimal(totalamount).ToString("N" + agent.DecimalValue);
                        decimal HandlingFee = Convert.ToDecimal(ds.Tables[0].Rows[0]["HandlingFee"]);
                         lblPremium.Text =( Convert.ToDecimal(ds.Tables[0].Rows[0]["premium_Amount"])+HandlingFee).ToString("N" + agent.DecimalValue);
                        //Passenger Details
                        lblCreatedBy.Text = ds.Tables[0].Rows[0]["USER_FULLNAME"].ToString(); 
                        lblLocation.Text = ds.Tables[0].Rows[0]["location_name"].ToString();
                        lblInvoicedBy.Text = agent.Name; 
                        BindPassenger(ref insheader, ref ds);
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in ReligareInsurance Voucher : " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        public void BindPassenger(ref ReligareHeader header,ref DataSet ds)
        {
            try
            {
                TableRow tr;
                TableCell cell;
                Label lbl;
                int counter = 0;
                string proposer = string.Empty;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (dr["Relation"].ToString().ToUpper() == "PROPOSER")
                    {
                        proposer = dr["firstName"].ToString() + " " + dr["lastName"].ToString();
                        break;
                    }
                }
                foreach (ReligarePassengers passenger in header.ReligarePassengers)
                {
                    tr = new TableRow();
                    cell = new TableCell();
                    lbl = new Label();
                    lbl.Text = passenger.FirstName + " " + passenger.LastName;
                    cell.Controls.Add(lbl);
                    tr.Cells.Add(cell);
                    cell = new TableCell();
                    if (proposer == lbl.Text && counter==0)
                    {
                        lbl = new Label();
                        lbl.Text = "Proposer";
                        counter = 1;
                    }
                    else
                    {
                        lbl = new Label();
                        lbl.Text = "Passenger";
                    }
                    cell.Controls.Add(lbl);
                    tr.Cells.Add(cell);
                    tblpaxDetails.Rows.Add(tr);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in ReligareInsurance Voucher Adding Passenger Details: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        public void BindPassengers(ref DataSet ds)
        {
            try
            {                 
               TableRow tr;
                TableCell cell;
                Label lbl;
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    tr = new TableRow();
                    cell = new TableCell();
                    lbl = new Label();
                    lbl.Text = dr["firstName"].ToString() + " " + dr["lastName"].ToString();
                    cell.Controls.Add(lbl);
                    tr.Cells.Add(cell);
                    cell = new TableCell();
                    lbl = new Label();
                    if (dr["Relation"].ToString().ToUpper() == "PROPOSER")
                        lbl.Text = "Proposer";
                    else
                        lbl.Text = "Passenger";
                     cell.Controls.Add(lbl);
                    tr.Cells.Add(cell);
                    tblpaxDetails.Rows.Add(tr);

                }
            }
            catch(Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in ReligareInsurance Voucher Adding Passenger Details: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
    }
}
