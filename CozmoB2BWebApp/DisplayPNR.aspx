﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="DisplayPNRGUI" Title="Display PNR" Codebehind="DisplayPNR.aspx.cs" EnableEventValidation="false" ValidateRequest="false" Trace="false" TraceMode="SortByTime" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <script type="text/javascript">

        var IsCorporate = false;
        $(document).ready(function () {

            if (document.getElementById('<%=hdnCorpInfo.ClientID %>').value != '')
                BindCorpInfo(0, document.getElementById('<%=hdnCorpInfo.ClientID %>').value);
        }); 

        function BindCorpInfo(iAgentId, data) {

            try {
                                
                if (iAgentId > 0)
                    data = AjaxCall('DisplayPNR.aspx/GetCorpData', "{'sAgentId':'" + iAgentId + "'}");

                if (IsEmpty(data)) { document.getElementById('divReasons').style.display = document.getElementById('divProfiles').style.display = 'none'; return;}

                data = JSON.parse(data);

                var options = '<option selected="selected" value="">Select Reason for Travel</option>';
                for (var i = 0; i < data.Table1.length; i++) {

                    options += '<option value="' + data.Table1[i]['ReasonId'] + '">' + data.Table1[i]['Description'] + '</option>';
                }
                document.getElementById('ddlTravelReason').innerHTML = options;

                options = '<option selected="selected" value="">Select Traveller</option>';
                for (var i = 0; i < data.Table2.length; i++) {

                    options += '<option value="' + data.Table2[i]['Profile'] + '">' + data.Table2[i]['ProfileName'] + '</option>';
                }
                document.getElementById('ddlProfiles').innerHTML = options;

                Setddlval('ddlTravelReason', '', '');
                Setddlval('ddlProfiles', (data.Table2.length == 1 ? data.Table2[0]['Profile'] : ''), '');            
                document.getElementById('divReasons').style.display = document.getElementById('divProfiles').style.display = 'block';
                IsCorporate = true;
            }
            catch(exception)
            {
                var exp = exception;
            }
        }

        function disablefield() {

            if (document.getElementById('<%=rbtnAgent.ClientID %>') != null && document.getElementById('<%=rbtnAgent.ClientID %>').checked == true) {
                document.getElementById('wrapper').style.display = "block";
            }
            else {
                document.getElementById('wrapper').style.display = "none";
            }
        }
        function Validate() {
            var msg = "";
            
            if (document.getElementById('<%=rbtnAgent.ClientID %>') != null) {
                
                if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == false && document.getElementById('<%=rbtnSelf.ClientID %>').checked == false) 
                    msg += "Please select Agency Type \n";

                if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true) {
                    if (document.getElementById('<%=ddlAgent.ClientID %>').selectedIndex <= 0) {
                        msg += "Please select an agent from the list \n";
                    }
                }

                if (document.getElementById('<%=rbtnAgent.ClientID %>').checked == true) {
                    if (document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').selectedIndex <= 0) {
                        msg += "Please select an agent location from the list \n";
                    }
                }
            }

            if (document.getElementById('<%=txtPnr.ClientID %>').value == '') {
                msg += "Please Enter PNR number \n";
            }

            var ddlReason = document.getElementById('ddlTravelReason');
            if (IsCorporate && ddlReason != null && IsEmpty(ddlReason.value))
                msg += "Please select travel reason \n";

            var ddlProfiles = document.getElementById('ddlProfiles');
            if (IsCorporate && ddlProfiles != null && IsEmpty(ddlProfiles.value))
                msg += "Please select traveller";

            if (msg.length != 0) {
                alert(msg);
                return false;
            }
            else {
                    //Added by lokesh.
                    //Based on this location agent id
                    //Corresponding Agent Markup and handling fee calculations are done.
                    if (document.getElementById('<%=rbtnAgent.ClientID %>') != null && document.getElementById('<%=rbtnAgent.ClientID %>').checked == true
                        && document.getElementById('<%=ddlAgent.ClientID %>').selectedIndex > 0
                        && document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').selectedIndex > 0)
                    {
                       document.getElementById('<%=hdnAgentLocation.ClientID %>').value = document.getElementById('<%=ddlAirAgentsLocations.ClientID %>').value;

                    }

                if (IsCorporate)
                    document.getElementById('<%=hdnCorpInfo.ClientID %>').value = ddlProfiles.value + '|' + ddlReason.value;

                return true;
            }
        }
var Ajax;
var ddlAirAgentsLocations;
//Load Locations based onn the agency id.
      function LoadAgentLocations(id, type) {
             
        if (document.getElementById('<%=ddlAgent.ClientID %>').selectedIndex > 0) { 
            ddlAirAgentsLocations = 'ctl00_cphTransaction_ddlAirAgentsLocations';
            var sel = document.getElementById('<%=ddlAgent.ClientID %>').value;
            BindCorpInfo(sel, '');
            var paramList = 'requestSource=getAgentsLocationsByAgentId' + '&AgentId=' + sel + '&id=' + location;;
            var url = "CityAjax";
            if (window.XMLHttpRequest) {
                Ajax = new XMLHttpRequest();
            }
            else {
                Ajax = new ActiveXObject("Microsoft.XMLHTTP");
            }
            Ajax.onreadystatechange = BindLocationsList;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }
        else {
            $("#ctl00_cphTransaction_ddlAirAgentsLocations").empty();
            $("#ctl00_cphTransaction_ddlAirAgentsLocations").select2("val", "-1");
        }
    }
function BindLocationsList() {
        var ddl = document.getElementById(ddlAirAgentsLocations);
        if (Ajax.readyState == 4 && Ajax.status == 200 && Ajax.responseText.length > 0) {
        
                    if (ddl != null) {
                        ddl.options.length = 0;
                        var el = document.createElement("option");
                        el.textContent = "Select Location";
                        el.value = "-1";
                        ddl.add(el, 0);
                        
                        var values = Ajax.responseText.split('#')[1].split(',');
                        for (var i = 0; i < values.length; i++) {
                            var opt = values[i];
                            if (opt.length > 0 && opt.indexOf('|') > 0) {
                                var el = document.createElement("option");
                                el.textContent = opt.split('|')[0];
                                el.value = opt.split('|')[1];
                                ddl.appendChild(el);
                            }
                        }
                    }
                    
                }
else {//Clear previous agent locations if no locations found for the current agent
                    if (ddl != null) {
                        ddl.options.length = 0;
                        var el = document.createElement("option");
                        el.textContent = "Select Location";
                        el.value = "-1";
                        ddl.add(el, 0);
                        $("#ctl00_cphTransaction_ddlAirAgentsLocations").select2("val", "-1");
                    }                   
                }
                
    }

    </script>

    <asp:HiddenField ID="hdnAgentLocation" runat="server" />
    
 <div class="banner-container" style="background-image:url(build/img/main-bg.jpg)">
      <div class="row no-gutter">
          <div class="col-12 col-lg-6">
            <div id="tabbed-menu" class="search-container">
                  <ul class="tabbed-menu nav">  
                    <li>
                        <a  id="lnkFlights"  href="HotelSearch.aspx?source=Flight" class="current">                   
                          Flight
                        </a> 
                    </li>                           
                    <li>
                        <a id="lnkHotels"  href="HotelSearch.aspx?source=Hotel">
                         Hotel
                        </a>
                    </li>      
                    <li>
                        <a  id="lnkACtivity" onclick="Javascript:location='ActivityResults.aspx'" href="#classics2" >
                         Activity
                        </a>
                    </li>    
                    <li><a id="lnkPackages" onclick="Javascript:location='packages.aspx'"  href="#featured2">
                        Packages
                        </a>
                    </li>
                     <li>
                         <a onclick="Javascript:location='Insurance.aspx'" id="lnkInsurance" href="Insurance.aspx" >
                            Insurance
                         </a>
                   </li>                                                              
                 </ul>
           

                    <asp:Label ID="lblError" runat="server" style="color:Red"></asp:Label>

                 <div class="search-matrix-wrap p-4 search-page-form auto-height flights"  data-tab-content="source=Flight" id="core2">         
                       <ul class="tabbed-menu-outline text-uppercase pb-5 d-block">
                           <li><a href="HotelSearch.aspx?source=Flight">FLIGHT TICKETS</a></li>
                           <li><a href="Offlinebooking.aspx">OFFLINE BOOKING</a></li>
                           <li class="active"><a href="DisplayPNR.aspx">IMPORT PNR</a></li>
                       </ul>
                       <div class="custom-radio-table row my-2">
                             <div class="col-md-3 col-xs-4">
                                   <asp:RadioButton ID="rbtnSelf" runat="server" Checked GroupName="book" onchange="disablefield();"
                        Font-Bold="True" Text="Self" />
                             </div>
                             <div class="col-md-3 col-xs-4">
                                  <asp:RadioButton ID="rbtnAgent" runat="server" GroupName="book" onchange="disablefield();"
                        Font-Bold="True" Text="Agency" />
                             </div>                           

                              <div class="col-md-4 col-xs-4" id="wrapper" style="display:none">
                                 <asp:DropDownList CssClass="form-control" ID="ddlAgent" runat="server" onchange="LoadAgentLocations(this.id,'F')">
                                 </asp:DropDownList>
                                   <!--Added by lokesh on 20 feb2019-->
                                   <!--Load Agent Locations-->
                                  <br /><br />
                                  Select Location:
                                  <asp:DropDownList ID="ddlAirAgentsLocations" CssClass="form-control" runat="server">
                                  </asp:DropDownList>
                              </div>

                       </div>
                       <div class="row custom-gutter">
                         <div class="col-12 col-md-6"> 
                             <label> Enter PNR no:</label>
                             <div class="form-control-holder">
                                 <div class="icon-holder">
                                     <span class="icon-aircraft-take-off" aria-label="Origin Airport"></span>
                                 </div>
                                 <asp:TextBox CssClass="form-control" ID="txtPnr" runat="server"></asp:TextBox>
                             </div>   
                         </div>
                         <div class="col-12 col-md-6">  
                              <label>Select Source:</label>
                             <div class="form-control-holder">                                
                                 <div class="icon-holder">
                                     <span class="icon-global-settings" aria-label="Origin Airport"></span>
                                 </div>
                                  <asp:DropDownList CssClass="form-control" ID="ddlSource" runat="server">
                                    <asp:ListItem Value="UAPI" Text="Universal API"></asp:ListItem>
                                    <%--<asp:ListItem Value="1G" Text="Galileo"></asp:ListItem>--%>
                                    <asp:ListItem Value="G9" Text="Air Arabia"></asp:ListItem>
                                 </asp:DropDownList>
                             </div>   
                         </div>                         
                        <div id="divReasons" class="col-12 col-md-6" style="display:none">
                            <label>Travel Reason:</label>
                            <div class="form-control-holder">                                
                                <div class="icon-holder">
                                    <span class="icon-user" aria-label="Traveller"></span>
                                </div>  
                                <select ID="ddlTravelReason" class="form-control"></select>
                            </div>     
                        </div>
                            <div id="divProfiles" class="col-12 col-md-6" style="display:none">
                            <label>Traveller:</label>
                            <div class="form-control-holder">                                
                                <div class="icon-holder">
                                    <span class="icon-user" aria-label="Traveller"></span>
                                </div>                                      
                                <select ID="ddlProfiles" class="form-control"></select>
                            </div>     
                        </div>                         
                         <div class="col-12">
                             <asp:Button runat="server" CssClass="btn but_b float-right" ID="btnRetrieve" Text="Retrieve Information" OnClick="btnRetrieve_Click" OnClientClick="return Validate();" /> 
                         </div>
                     </div>


                 </div>
            </div>
         </div>
      </div>
 </div>
<input type="hidden" id="hdnCorpInfo" runat="server" />  
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>
