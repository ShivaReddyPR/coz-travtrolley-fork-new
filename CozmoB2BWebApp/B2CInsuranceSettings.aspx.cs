﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.Collections.Generic;

public partial class B2CInsuranceSettings : CT.Core.ParentPage //System.Web.UI.Page
{
    DataTable dtProducts;
    DataTable dtMarkupList;
    
    string layout = "INSURANCE SETTINGS";
    string eMailId = "EMAILIDFORINSURANCE";
    string waitingText = "WAITINGTEXTFORINSURANCE";
    string googleScriptType = "GOOGLESCRIPTTYPE";
    string googleScript = "GOOGLESCRIPT";
    //string waitingFile = "WAITINGLOGOFORINSURANCE";
    
    string enbdPG = "ENBD-PG";
    string enbdCreditCardCharges = "ENBD-CREDITCARD-CHARGES";
    string ccAvenuePG = "CCAVENUE-PG";
    string ccAvenueCreditCardCharges = "CCAVENUE-CREDITCARD-CHARGES";

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx", true);
        }
        else
        {
            if (!IsPostBack)
            {
                ClearMarkupControls();
                ClearSettingsControls();
                InitializePageControls();
            }
            else
            {
                BindControls();
            }
            lblSuccessMsg.Text = string.Empty;
            errMess.Style.Add("display", "none");
            errorMessage.InnerHtml = string.Empty;
        }
    }

    private void InitializePageControls()
    {
        try
        {
            BindAgent();
            BindProduct();
            BindSettings();
            BindFAQs();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    void BindSettings()
    {
        if (Request["memberId"] != null && Request["agentId"] != null)
        {
            int memberId = Convert.ToInt32(Request["memberId"]);
            UserMaster member = new UserMaster(memberId);



            Dictionary<string, string> preferences = UserPreference.GetPreferenceList(memberId, ItemType.Insurance);

            if (preferences.ContainsKey(eMailId))
            {
                txtEmail.Text = preferences[eMailId];
            }
            if (preferences.ContainsKey(googleScriptType))
            {
                string gScriptType = preferences[googleScriptType];

                switch (gScriptType)
                {
                    case "Google Analytics":
                        rbtnGoogleAnalytics.Checked = true;
                        break;
                    case "Google PPC":
                        rbtnGooglePPC.Checked = true;
                        break;
                }
            }

            if (preferences.ContainsKey(googleScript))
            {
                txtGoogleScript.Text = preferences[googleScript];
            }
            if (preferences.ContainsKey(waitingText))
            {
                txtWaitingText.Text = preferences[waitingText];
            }
            //if (preferences.ContainsKey(discount))
            //{
            //    txtDiscount.Text = preferences[discount];
            //}

            if (preferences.ContainsKey(enbdPG))
            {
                chkENBD.Checked = Convert.ToBoolean(preferences[enbdPG]);
            }

            if (preferences.ContainsKey(enbdCreditCardCharges))
            {
                txtENBDCharges.Text = preferences[enbdCreditCardCharges];
            }

            if(preferences.ContainsKey(ccAvenuePG))
            {
                chkCCA.Checked = Convert.ToBoolean(preferences[ccAvenuePG]);
            }

            if(preferences.ContainsKey(ccAvenueCreditCardCharges))
            {
                txtCCACharges.Text = preferences[ccAvenueCreditCardCharges];
            }

        }
    }

    void BindFAQs()
    {
         if (Request["memberId"] != null && Request["agentId"] != null)
        {
            int memberId = Convert.ToInt32(Request["memberId"]);
            int agentId = Convert.ToInt32(Request["agentId"]);
            List<WhiteLabelFaqs> faqs = WhiteLabelFaqs.LoadAllFAQs(agentId, ItemType.Insurance);

            gvFAQ.DataSource = faqs;
            gvFAQ.DataBind();
        }
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        Button btnNext = sender as Button;

        switch (btnNext.ID)
        {
            case "btnInsuranceViewNext":
                mvSettings.ActiveViewIndex = 1;
                break;
            case "btnMarkupNext":
                mvSettings.ActiveViewIndex = 2;
                break;
            case "btnFAQNext":
                mvSettings.ActiveViewIndex = 3;
                break;
        }
    }

    protected void btnPrev_Click(object sender, EventArgs e)
    {
        Button btnPrev = sender as Button;

        switch (btnPrev.ID)
        {
            case "btnMarkupPrev":
                mvSettings.ActiveViewIndex = 0;
                break;
            case "btnFAQPrev":
                mvSettings.ActiveViewIndex = 1;
                break;
            case "btnFraudPrev":
                mvSettings.ActiveViewIndex = 2;
                break;
        }
    }

    #region InsuranceSettings Control Events

    protected void btnSaveSettings_Click(object sender, EventArgs e)
    {
        if (Request["memberId"] != null && Request["agentId"] != null)
        {
            int memberId = Convert.ToInt32(Request["memberId"]);
            int agentId = Convert.ToInt32(Request["agentId"]);
            try
            {
                UserPreference pref = new UserPreference();
                pref.Save(memberId, eMailId, txtEmail.Text, layout, ItemType.Insurance);
                pref.Save(memberId, waitingText, txtWaitingText.Text, layout, ItemType.Insurance);

                pref.Save(memberId, googleScriptType, (rbtnGoogleAnalytics.Checked ? rbtnGoogleAnalytics.Text : rbtnGooglePPC.Text), layout, ItemType.Insurance);
                pref.Save(memberId, googleScript, txtGoogleScript.Text, layout, ItemType.Insurance);

                

                lblSuccessMsg.Text = "Settings saved successfully";
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to save B2C Insurance Settings " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
    }

    protected void btnClearSettings_Click(object sender, EventArgs e)
    {
        ClearSettingsControls();
    }

    void ClearSettingsControls()
    {
        txtEmail.Text = "";
        txtWaitingText.Text = "";
        txtGoogleScript.Text = "";
    } 

    #endregion

    #region Markup and PaymentGateway Events

    protected void btnSaveMarkup_Click(object sender, EventArgs e)
    {
        if (Request["memberId"] != null && Request["agentId"] != null)
        {
            int memberId = Convert.ToInt32(Request["memberId"]);
            int agentId = Convert.ToInt32(Request["agentId"]);
            try
            {
                UserPreference pref = new UserPreference();
                pref.Save(memberId, enbdPG, chkENBD.Checked.ToString(), layout, ItemType.Insurance);
                pref.Save(memberId, enbdCreditCardCharges, txtENBDCharges.Text, layout, ItemType.Insurance);
                
                pref.Save(memberId, ccAvenuePG, chkCCA.Checked.ToString(), layout, ItemType.Insurance);
                pref.Save(memberId, ccAvenueCreditCardCharges, txtCCACharges.Text, layout, ItemType.Insurance);

                pref.Save(memberId, string.Empty, string.Empty, layout, ItemType.Insurance);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to save B2C PG Settings " + ex.ToString(), Request["REMOTE_ADDR"]);
            }

            try
            {
                dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty);
                for (int i = 0; i < chkProduct.Items.Count; i++)
                {
                    if (chkProduct.Items[i].Selected)
                    {
                        int agent = 0;
                        string find = string.Empty;
                        string source = string.Empty;
                        int productId = Convert.ToInt32(chkProduct.Items[i].Value);
                        if (i < 2 || i == 4)
                        {
                            DropDownList ddlSource = tblMarkup.Rows[0].Cells[i].FindControl("ddlSource_" + i) as DropDownList;
                            source = ddlSource.SelectedItem.Text;
                        }
                        if (ddlAgent.SelectedItem.Value != "0")
                        {
                            agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                        }
                        TextBox txtAgentMarkup = tblMarkup.Rows[0].Cells[i].FindControl("txtB2CMarkup_" + i) as TextBox;
                        //TextBox txtOurComm = tblMarkup.Rows[0].Cells[i].FindControl("txtOurComm_" + i) as TextBox;
                        TextBox txtMarkup = tblMarkup.Rows[0].Cells[i].FindControl("txtMarkUp_" + i) as TextBox;
                        DropDownList ddlMarkUp = tblMarkup.Rows[0].Cells[i].FindControl("ddlMarkupType_" + i) as DropDownList;
                        TextBox txtDiscount = tblMarkup.Rows[0].Cells[i].FindControl("txtDiscount_" + i) as TextBox;
                        DropDownList ddlDiscount = tblMarkup.Rows[0].Cells[i].FindControl("ddlDiscountType_" + i) as DropDownList;

                        //DataTable dtMarkUp = UpdateMarkup.GetMarkupRules(productId, source,agent);
                        if (agent == 0)
                        {
                            find = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId IS NULL";
                        }
                        else
                        {
                            find = "ProductId='" + productId + "' AND SourceId = '" + source + "' AND AgentId='" + agent + "' AND UserID='" + memberId + "' AND TransType='B2C'";
                        }

                        DataRow[] foundRows = dtMarkupList.Select(find);
                        CT.BookingEngine.UpdateMarkup objUpdateMarkup = new CT.BookingEngine.UpdateMarkup();
                        if (foundRows.Length > 0)
                        {
                            objUpdateMarkup.Id = Utility.ToInteger(foundRows[0]["MRId"]);
                            objUpdateMarkup.MrDId = Utility.ToInteger(foundRows[0]["MRDId"]);
                        }
                        objUpdateMarkup.ProductId = productId;
                        objUpdateMarkup.SourceId = source;
                        objUpdateMarkup.UserId = memberId;
                        objUpdateMarkup.AgentId = agent;
                        objUpdateMarkup.B2CMarkupType = "B2C";
                        objUpdateMarkup.AgentMarkup = Utility.ToDecimal(txtAgentMarkup.Text);
                        //objUpdateMarkup.OurCommission = Utility.ToDecimal(txtOurComm.Text);
                        objUpdateMarkup.Markup = Utility.ToDecimal(txtMarkup.Text);
                        objUpdateMarkup.MarkupType = ddlMarkUp.SelectedItem.Value;
                        objUpdateMarkup.Discount = Utility.ToDecimal(txtDiscount.Text);
                        objUpdateMarkup.DiscountType = ddlDiscount.SelectedItem.Value;

                        objUpdateMarkup.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
                        objUpdateMarkup.Save();
                        chkProduct.Items[i].Selected = false;
                    }
                }
                lblSuccessMsg.Text = "Updated successfully";
                
                tblMarkup.Rows.Clear();


            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to save B2C Markup " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
    }

    protected void btnClearMarkup_Click(object sender, EventArgs e)
    {
        ClearMarkupControls();
    }

    void ClearMarkupControls()
    {
        txtENBDCharges.Text = "0";
        txtCCACharges.Text = "0";
        chkCCA.Checked = false;
        //ddlAgent.SelectedIndex = 0;
        //txtDiscount.Text = "0";
    } 

    #endregion

    #region FAQ Events
    protected void btnSaveFAQ_Click(object sender, EventArgs e)
    {
        try
        {
            if (Request["memberId"] != null && Request["agentId"] != null)
            {
                int agentId = Convert.ToInt32(Request["agentId"]);
                WhiteLabelFaqs faq = new WhiteLabelFaqs();
                faq.AgencyId = agentId;
                faq.Answer = txtAnswer.Text;
                faq.CreatedBy = (int)Settings.LoginInfo.UserID;
                faq.CreatedOn = DateTime.Now;
                faq.ItemType = ItemType.Insurance;
                faq.Question = txtQuestion.Text;
                if (gvFAQ.SelectedIndex >= 0)
                {
                    faq.QuestionId = Convert.ToInt32(gvFAQ.DataKeys[gvFAQ.SelectedIndex].Value);
                }
                else
                {
                    faq.QuestionId = -1;
                }

                faq.Save();
                gvFAQ.SelectedIndex = -1;
                BindFAQs();
                lblSuccessMsg.Text = "Saved Successfully";

                txtQuestion.Text = "";
                txtAnswer.Text = "";
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to save Flight FAQ " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    protected void btnClearFAQ_Click(object sender, EventArgs e)
    {
        txtQuestion.Text = "";
        txtAnswer.Text = "";
    }

    protected void gvFAQ_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
    {
        gvFAQ.SelectedIndex = e.NewSelectedIndex;
        txtQuestion.Text = gvFAQ.SelectedRow.Cells[1].Text;
        txtAnswer.Text = gvFAQ.SelectedRow.Cells[2].Text;
    }
    #endregion

    #region Update Markup Control Events and Methods

    private void BindAgent()
    {
        try
        {
            int agentId = Convert.ToInt32(Settings.LoginInfo.AgentId);
            if (Request["agentId"] != null)
            {
                agentId = Convert.ToInt32(Request["agentId"]);
                ddlAgent.Enabled = false;
            }
            //AgentMaster1 agent = new AgentMaster1(agentId);
            DataTable dtAgents = null;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                dtAgents = AgentMaster.GetList(1, "ALL", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                dtAgents = AgentMaster.GetList(1, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);
            }
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Agency--", "0"));
            if (Request["agentId"] != null)
            {
                ddlAgent.SelectedValue = Request["agentId"];
            }
            else
            {
                ddlAgent.SelectedIndex = 0;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindProduct()
    {
        try
        {
            if (Request["agentId"] != null)
            {
                int agentId = Convert.ToInt32(Request["agentId"]);
                Session["dtProducts"] = null;

                //dtProducts = CT.BookingEngine.UpdateMarkup.ProductGetList(ListStatus.Short, RecordStatus.Activated, agentId);
                dtProducts = UpdateMarkup.ProductGetList(ListStatus.Short, RecordStatus.Activated, agentId);
               
                DataView dv = dtProducts.DefaultView;
                dv.RowFilter = "productTypeId IN('" + 5 + "')";
                chkProduct.DataSource = dv.ToTable();
                chkProduct.DataTextField = "productType";
                chkProduct.DataValueField = "productTypeId";
                chkProduct.DataBind();
                BindControls();
                Session["dtProducts"] = dtProducts;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindControls()
    {
        if (Session["dtProducts"] != null)
        {
            dtProducts = (DataTable)Session["dtProducts"];
        }
        HtmlTableRow hr = new HtmlTableRow();
        for (int i = 0; i < dtProducts.Rows.Count; i++)
        {
            //13.10.2014 Added by brahmam
            Label lblAgentMarkup = new Label();
            lblAgentMarkup.ID = "lblAgentMarkup_" + i;
            lblAgentMarkup.Text = "Agent Markup";
            TextBox txtAgentMarkup = new TextBox();
            txtAgentMarkup.ID = "txtB2CMarkup_" + i;
            //Label lblOurComm = new Label();
            //lblOurComm.ID = "lblOurComm_" + i;
            //lblOurComm.Text = "Our Commission";
            //TextBox txtOurComm = new TextBox();
            //txtOurComm.ID = "txtOurComm_" + i;
            //txtOurComm.Text = "0.00";

            Label lblMarkup = new Label();
            lblMarkup.ID = "lblMarkUp_" + i;
            lblMarkup.Text = "Markup";
            TextBox txtMark = new TextBox();
            txtMark.ID = "txtMarkUp_" + i;
            txtMark.Enabled = false;
            Label lblmarkUpType = new Label();
            lblmarkUpType.ID = "lblmarkUpType_" + i;
            lblmarkUpType.Text = "MarkupType";
            DropDownList ddlMarkupType = new DropDownList();
            ddlMarkupType.ID = "ddlMarkupType_" + i;
            ddlMarkupType.Items.Insert(0, new ListItem("Fixed", "F"));
            ddlMarkupType.Items.Insert(0, new ListItem("Percentage", "P"));
            HtmlTableCell tc = new HtmlTableCell();
            TextBox txtDiscount = new TextBox();
            txtDiscount.ID = "txtDiscount_" + i;
            Label lblDiscountType = new Label();
            lblDiscountType.ID = "lblDiscountType_" + i;
            lblDiscountType.Text = "Disc Type";
            DropDownList ddlDiscountType = new DropDownList();
            ddlDiscountType.ID = "ddlDiscountType_" + i;
            ddlDiscountType.Items.Insert(0, new ListItem("Fixed", "F"));
            ddlDiscountType.Items.Insert(0, new ListItem("Percentage", "P"));
            Label lblDiscount = new Label();
            lblDiscount.ID = "lblDiscount_" + i;
            lblDiscount.Text = "Discount";

            // Modified by  brahmam 05 sep 2014
           
                txtDiscount.Visible = true;
                lblDiscountType.Visible = true;
                ddlDiscountType.Visible = true;
                lblDiscount.Visible = true;
          

            if (i == 0)
            {
                Label lblSource = new Label();
                lblSource.ID = "lblSource_" + i;
                lblSource.Text = "Sources";
                tc.Controls.Add(lblSource);
                DropDownList ddlSource = new DropDownList();
                ddlSource.ID = "ddlSource_" + i;
                ddlSource.DataSource = AgentMaster.GetActiveSources(5);
                ddlSource.DataValueField = "Id";
                ddlSource.DataTextField = "Name";
                ddlSource.DataBind();
                ddlSource.Items.Insert(0, new ListItem("--Select Source--", "0"));
                ddlSource.AutoPostBack = true;
                ddlSource.SelectedIndexChanged += new EventHandler(ddlSource_OnSelectedIndexChanged);
                tc.Controls.Add(ddlSource);
            }
          
            tc.Controls.Add(lblAgentMarkup);
            tc.Controls.Add(txtAgentMarkup);
            //tc.Controls.Add(lblOurComm);
            //tc.Controls.Add(txtOurComm);
            tc.Controls.Add(lblMarkup);
            tc.Controls.Add(txtMark);
            tc.Controls.Add(lblmarkUpType);
            tc.Controls.Add(ddlMarkupType);
            tc.Controls.Add(lblDiscount);
            tc.Controls.Add(txtDiscount);
            tc.Controls.Add(lblDiscountType);
            tc.Controls.Add(ddlDiscountType);

            tc.Width = "75px";
            tc.VAlign = "top";
            foreach (Control c in tc.Controls)
            {
                if (c is TextBox)
                {
                    TextBox t1 = (TextBox)c;
                    t1.Text = "0.00";
                    t1.Style.Add("display", "none");
                    t1.Width = new Unit(80, UnitType.Pixel);

                    t1.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'1');");
                    t1.Attributes.Add("onchange", "SetValue();");
                    t1.Attributes.Add("onfocus", "Check(this.id);");
                    t1.Attributes.Add("onBlur", "Set(this.id);");
                }
                else if (c is Label)
                {
                    Label l1 = (Label)c;
                    l1.Style.Add("display", "none");
                    l1.Width = new Unit(80, UnitType.Pixel);
                }
                else if (c is DropDownList)
                {
                    DropDownList d1 = (DropDownList)c;
                    d1.Style.Add("display", "none");
                    d1.Width = new Unit(80, UnitType.Pixel);
                }
            }
            hr.Cells.Add(tc);
        }
        tblMarkup.Rows.Add(hr);
    }

    protected void ddlSource_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            if (Request["memberId"] != null && Request["agentId"] != null)
            {
                int memberId = Convert.ToInt32(Request["memberId"]);
                int agentId = Convert.ToInt32(Request["agentId"]);

                DropDownList ddlSource = sender as DropDownList;
                int agent = 0;
                int rowIndex = 0;
                string find = string.Empty;
                rowIndex = Convert.ToInt32(ddlSource.ID.Split('_')[1]);
                if (ddlAgent.SelectedItem.Value != "0")
                {
                    agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
                string source = ddlSource.SelectedItem.Text;
                TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtB2CMarkup_" + rowIndex.ToString());
                //TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + rowIndex.ToString());
                TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + rowIndex.ToString());
                DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + rowIndex.ToString());
                TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + rowIndex.ToString());
                DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + rowIndex.ToString());

                //DataTable dtMarkUp = UpdateMarkup.GetMarkupRulesBySouceId(source, agent);
                dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty);

                find = "SourceId='" + source + "' AND AgentId = " + agent + " AND TransType='B2C' AND UserId=" + memberId;
                
                DataRow[] foundRows = dtMarkupList.Select(find);
                if (foundRows.Length > 0)
                {
                    txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
                    //txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
                    txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
                    ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
                    txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
                    ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
                }
                else
                {
                    txtAgentMarkup.Text = "0.00";
                    //txtOurComm.Text = "0.00";
                    txtMarkup.Text = "0.00";
                    txtDiscount.Text = "0.00";
                }
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void ddlAgent_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            foreach (ListItem item in chkProduct.Items)
            {
                item.Selected = false;
            }
            tblMarkup.Rows.Clear();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void chkProduct_OnSelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            if (ddlAgent.SelectedIndex != 0)
            {
                LoadControls();
            }
            else
            {
                errMess.Style.Add("display", "block");
                errorMessage.InnerHtml = "Please select Agent!";
            }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void LoadControls()
    {
        if (Request["memberId"] != null && Request["agentId"] != null)
        {
            int memberId = Convert.ToInt32(Request["memberId"]);
            int agentId = Convert.ToInt32(Request["agentId"]);
            
            dtMarkupList = UpdateMarkup.GetMarkupList(-1, string.Empty, -1, string.Empty);
            for (int i = 0; i < chkProduct.Items.Count; i++)
            {

                Label lblAgentMarkup = (Label)tblMarkup.FindControl("lblAgentMarkup_" + i);
                TextBox txtAgentMarkup = (TextBox)tblMarkup.FindControl("txtB2CMarkup_" + i);
                //Label lblOurComm = (Label)tblMarkup.FindControl("lblOurComm_" + i);
                //TextBox txtOurComm = (TextBox)tblMarkup.FindControl("txtOurComm_" + i);
                Label lblMarkup = (Label)tblMarkup.FindControl("lblMarkUp_" + i);
                TextBox txtMarkup = (TextBox)tblMarkup.FindControl("txtMarkUp_" + i);
                Label lblMarkupType = (Label)tblMarkup.FindControl("lblmarkUpType_" + i);
                DropDownList ddlMarkupType = (DropDownList)tblMarkup.FindControl("ddlMarkupType_" + i);
                Label lblDiscount = (Label)tblMarkup.FindControl("lblDiscount_" + i);
                TextBox txtDiscount = (TextBox)tblMarkup.FindControl("txtDiscount_" + i);
                Label lblDiscountType = (Label)tblMarkup.FindControl("lblDiscountType_" + i);
                DropDownList ddlDiscountType = (DropDownList)tblMarkup.FindControl("ddlDiscountType_" + i);
                Label lblSource = (Label)tblMarkup.FindControl("lblSource_" + i);
                DropDownList ddlSource = (DropDownList)tblMarkup.FindControl("ddlSource_" + i);
               // int productId = Convert.ToInt32(chkProduct.Items[i].Value);
                if (chkProduct.Items[i].Selected)
                {
                    lblAgentMarkup.Style.Add("display", "block");
                    txtAgentMarkup.Style.Add("display", "block");
                    //lblOurComm.Style.Add("display", "block");
                    //txtOurComm.Style.Add("display", "block");
                    lblMarkup.Style.Add("display", "block");
                    txtMarkup.Style.Add("display", "block");
                    lblMarkupType.Style.Add("display", "block");
                    ddlMarkupType.Style.Add("display", "block");
                    lblDiscount.Style.Add("display", "block");
                    txtDiscount.Style.Add("display", "block");
                    lblDiscountType.Style.Add("display", "block");
                    ddlDiscountType.Style.Add("display", "block");
                    if (i < 2 || i == 4)
                    {
                        lblSource.Style.Add("display", "block");
                        ddlSource.Style.Add("display", "block");
                    }
                    if (i > 1 && i != 4)
                    {
                        int agent = 0;
                        string find = string.Empty;
                        int productId = Convert.ToInt32(chkProduct.Items[i].Value);
                        if (ddlAgent.SelectedItem.Value != "0")
                        {
                            agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                        }


                        find = "ProductId=" + productId + " AND AgentId = " + agent + " AND TransType='B2C' AND UserId=" + memberId;
                        
                        DataRow[] foundRows = dtMarkupList.Select(find);
                        //DataTable dtMarkupDetails = UpdateMarkup.GetMarkupDetails(productId, agent);
                        if (foundRows.Length > 0)
                        {
                            txtAgentMarkup.Text = Utility.ToString(foundRows[0]["AgentMarkup"]);
                            //txtOurComm.Text = Utility.ToString(foundRows[0]["OurCommission"]);
                            txtMarkup.Text = Utility.ToString(foundRows[0]["Markup"]);
                            ddlMarkupType.SelectedValue = Utility.ToString(foundRows[0]["MarkupType"]);
                            txtDiscount.Text = Utility.ToString(foundRows[0]["Discount"]);
                            ddlDiscountType.SelectedValue = Utility.ToString(foundRows[0]["DiscountType"]);
                        }
                        else
                        {
                            txtAgentMarkup.Text = "0.00";
                            //txtOurComm.Text = "0.00";
                            txtMarkup.Text = "0.00";
                            txtDiscount.Text = "0.00";
                        }
                    }
                }
                else
                {
                    lblAgentMarkup.Style.Add("display", "none");
                    txtAgentMarkup.Style.Add("display", "none");
                    //lblOurComm.Style.Add("display", "none");
                    //txtOurComm.Style.Add("display", "none");
                    lblMarkup.Style.Add("display", "none");
                    txtMarkup.Style.Add("display", "none");
                    lblMarkupType.Style.Add("display", "none");
                    ddlMarkupType.Style.Add("display", "none");
                    lblDiscount.Style.Add("display", "none");
                    txtDiscount.Style.Add("display", "none");
                    lblDiscountType.Style.Add("display", "none");
                    ddlDiscountType.Style.Add("display", "none");
                    if (i < 2 || i == 4)
                    {
                        lblSource.Style.Add("display", "none");
                        ddlSource.Style.Add("display", "none");
                    }
                }
            }
        }
    }

    #endregion
}
