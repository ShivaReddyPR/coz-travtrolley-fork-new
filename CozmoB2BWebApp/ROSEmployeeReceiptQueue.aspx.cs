using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using CT.TicketReceipt.BusinessLayer;
using CT.Roster;

public partial class ROSEmployeeReceiptQueueUI : CT.Core.ParentPage
{
    public DataTable dtExport;
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            lblSuccessMsg.Text = string.Empty;
            hdfParam.Value = "1";
            StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnExport );
            // lblError.Visible = false;
            if (!IsPostBack)
            {
                //Label lblGrandText = (Label)this.Master.FindControl("lblGrandText");
                //Label lblGrandTotal = (Label)this.Master.FindControl("lblGrandTotal");
                //lblGrandText.Visible = true;
                //lblGrandTotal.Visible = true;
                //((UpdatePanel)this.Master.FindControl("upnlGrandTotal")).Update();
               
                //((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
               // BindGrid();
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
       
   }
    private void InitializePageControls()
    {
        try
        {
           
           
            dcFromDate.Value = Utility.ToDate(DateTime.Now.Date);
            dcToDate.Value = Utility.ToDate(DateTime.Now.Date.ToString("MMM-dd-yyyy 23:59"));
           
            LoginInfo logininfo = Settings.LoginInfo;
            BindCompany();
            BindStaffList();
            BindGrid();          
            

        }
        catch
        { throw; }

    }
    #endregion


  

        
    # region Session
    private DataTable EmpReceiptList
    {
        get
        {
            return (DataTable)Session["_EmpReceiptList"];
        }
        set
        {
            if (value == null)
                Session.Remove("_EmpReceiptList");
            else
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["emp_id"] };
                Session["_EmpReceiptList"] = value;
            }
        }
    }


    
    private ReceiptMaster ReceiptMasterObject
    {
        get
        {
            return (ReceiptMaster)Session["_ReceiptListObject"];
        }
        set
        {
            if (value == null) Session.Remove("_ReceiptListObject");
            else Session["_ReceiptListObject"] = value;
        }
    }

   # endregion
    #region Bind Methods

    private void BindCompany()//Company
    {
        try
        {
            ddlCompany.DataSource = ReceiptMaster.GetCompanyList(ListStatus.Short, RecordStatus.Activated,"R");
            ddlCompany.DataValueField = "COMPANY_ID";
            ddlCompany.DataTextField = "COMPANY_NAME";
            ddlCompany.DataBind();
            ddlCompany.Items.Insert(0, new ListItem("-- Company --", "0"));
            ddlCompany.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }

    private void BindStaffList()
    {
        try
        {
            ddlStaff.DataSource = ROSEmployeeMaster.GetROSEmpMasterList(RecordStatus.Activated, ListStatus.Short, "Crew");
            ddlStaff.DataValueField = "emp_Id";
            ddlStaff.DataTextField = "emp_Name";
            ddlStaff.DataBind();
            ddlStaff.Items.Insert(0, new ListItem("--Select Employee--", "0"));
            ddlStaff.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }
    private void BindGrid()
    {          
        long empId=Utility.ToLong(ddlStaff.SelectedItem.Value );

        EmpReceiptList = ReceiptMaster.GetEmployeeReceiptList(empId, Utility.ToInteger(ddlCompany.SelectedItem.Value),
            ddlSvcStatus.SelectedItem.Value == "-1" ? string.Empty : ddlSvcStatus.SelectedItem.Value, ddlPaidStatus.SelectedItem.Value == "-1" ? string.Empty : ddlPaidStatus.SelectedItem.Value,
            dcFromDate.Value, dcToDate.Value);
        CommonGrid g = new CommonGrid();
        g.BindGrid(gvEmployee, EmpReceiptList);
        hdfSelectCount.Value = "0";
        
    }
            
    #endregion
    #region gvReceipt Events

    protected void gvEmployee_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvEmployee.PageIndex = e.NewPageIndex;
            gvEmployee.EditIndex = -1;
           // Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    //protected void filterControls()
    //{
    //    try
    //    {
    //        Label lblGrandText = (Label)this.Master.FindControl("lblGrandText");
    //        Label lblGrandTotal = (Label)this.Master.FindControl("lblGrandTotal");

    //        string[,] textboxesNColumns ={ { "HTtxtReceiptNo", "receipt_number" }, { "HTtxtReceiptDate", "receipt_date" }, { "HTtxtPaxName", "receipt_pax_name" },
    //        { "HTtxtAdults", "receipt_adults" },{ "HTtxtChildren", "receipt_children" },{ "HTtxtInfants", "receipt_Infants" },{ "HTtxtFare", "receipt_fare" },
    //        { "HTtxtTotalFare", "receipt_total_fare" },{ "HTtxtUser", "receipt_created_name" },{ "HTtxtLocation", "receipt_location_name" },{ "HTtxtAgent", "receipt_agent_name" },
    //        { "HTtxtRemarks", "receipt_remarks" },{ "HTtxtStatus", "receipt_status_name" },{ "HTtxtStatus", "receipt_status_name" },{"HTtxtBaseToCollect","receipt_base_to_collect"},{"HTtxtLocalToCollect","receipt_local_to_collect"},
    //        {"HTtxtMode","RECEIPT_SETTLEMENT_MODE_DESCRIPTION"},{"HTtxtCash","cash_base_amount"},{"HTtxtCredit","credit_base_amount"},{"HTtxtCard","card_base_amount"},{"HTtxtEmployee","employee_base_amount"},{"HTtxtOthers","others_base_amount"},
    //        {"HTtxtAgent","receipt_agent_name"},{"HTtxtCompanyName","RECEIPT_COMPANY_NAME"},{"HTtxtStaffId","receipt_staff_id"},{"HTtxtVehicle","VEHICLE_NAME"},{"HTtxtNotes","receipt_notes"}};
    //        CommonGrid g = new CommonGrid();
    //        g.FilterGridView(gvReceipt, ReceiptList.Copy(), textboxesNColumns);
    //        string grandTotal = Utility.ToString(((DataTable)(gvReceipt.DataSource)).Copy().Compute("SUM(RECEIPT_LOCAL_TO_COLLECT)", ""));
    //        lblGrandTotal.Text = string.Format(" {0} AED", (string.IsNullOrEmpty(grandTotal)) ? Formatter.ToCurrency(0) : grandTotal);
    //        lblGrandTotal.Text = string.Format(" {0} AED", (string.IsNullOrEmpty(grandTotal)) ? Formatter.ToCurrency(0) : Formatter.ToCurrency(grandTotal));

    //        DataTable dt = (DataTable)gvReceipt.DataSource;
    //        dtExport = dt;
    //        decimal totalFare = Utility.ToDecimal(dt.Compute("SUM(RECEIPT_TOTAL_FARE)", ""));

    //        lblGrandText.Visible = true;
    //        lblGrandTotal.Visible = true;
    //        ((UpdatePanel)this.Master.FindControl("upnlGrandTotal")).Update();

    //    }
    //    catch { throw; }
    //}
    //protected void Filter_Click(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        filterControls();
    //    }
    //    catch (Exception ex)
    //    {
    //        Label lblMasterError = (Label)this.Master.FindControl("lblError");
    //        lblMasterError.Visible = true;
    //        lblMasterError.Text = ex.Message;
    //        Utility.WriteLog(ex, this.Title);
    //    }
    //}

    #endregion
    private void isTrackingEmpty()
    {
        try
        {
            bool _selected = false;
            foreach (DataRow dr in EmpReceiptList.Rows)
            {
                if (dr != null)
                {
                    if (Utility.ToString(dr["receipt_accounted_status"]) == Utility.ToString((char)VisaAccountedStatus.YES))
                    {
                        _selected = true;
                        return;
                    }
                }
            }
            if (!_selected) throw new Exception("Please Select @least one Item ! ");
        }
        catch { throw; }
    }   
    private void selectedItem()
    {
        try
        {
            foreach (GridViewRow gvRow in gvEmployee.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                HiddenField hdfReceiptId = (HiddenField)gvRow.FindControl("IThdfReceiptId");
                foreach (DataRow dr in EmpReceiptList.Rows)
                {
                    if (dr != null)
                    {
                        dr.BeginEdit();
                        if (Utility.ToString(dr["receipt_id"]) == hdfReceiptId.Value)
                        {
                            dr["receipt_accounted_status"] = chkSelect.Checked ? Utility.ToString((char)VisaAccountedStatus.YES) : Utility.ToString((char)VisaAccountedStatus.NO);
                        }
                        dr.EndEdit();
                    }
                }
            }

        }
        catch { throw; }
    }
   
    
    # region Report
    protected void ITlnkPrint_Click(object sender, EventArgs e)
    {
        try
        {

            GridViewRow gvRow = (GridViewRow)(((LinkButton)sender).NamingContainer);
            HiddenField hdfReceiptID = (HiddenField)gvRow.FindControl("IThdfLstReceiptId");
            CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
            if (hdfReceiptID != null)
            {
                long reportId=Utility.ToLong(hdfReceiptID.Value);
                string script = "window.open('printReport.aspx?receiptId=" + Utility.ToString(reportId)+ "','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=yes,scrollbars=yes,resizable=yes,left=50,top=50');";
                StartupScript(this.Page, script, "Key");
            }
            Utility.StartupScript(this.Page, "SelectRow('" + chkSelect.ClientID + "','Y')", "SelectRow1");
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void ITbtnGenerateInvoice_Click(object sender, EventArgs e)
    {
        try
        {
            //string apiTranxStatus = "S";

            //string apiTranxStatus_remarks = apiTranxStatus == "S" ? "Success" : "Faliure";

            GridViewRow gvRow = (GridViewRow)(((Button)sender).NamingContainer);
            HiddenField IThdfEmpId = (HiddenField)gvRow.FindControl("IThdfEmpId");
            HiddenField IThdfCompany = (HiddenField)gvRow.FindControl("IThdfCompany");            
            Label lblReceiptNo = (Label)gvRow.FindControl("ITlblDocNo");
            Label ITlblEmpName = (Label)gvRow.FindControl("ITlblEmpName");
            Label ITlblTariffAmount = (Label)gvRow.FindControl("ITlblTariffAmount");
            DropDownList ITddlMonth = (DropDownList)gvRow.FindControl("ITddlMonth");
            

            ReceiptMaster receipt =  new ReceiptMaster();

            receipt.DocDate = DateTime.Now;
            string docType = receipt.DocType;          
 
            receipt.PaxName = ITlblEmpName.Text.Trim();
            receipt.DocType = docType;
            receipt.PaxMobileNo = "";//;txtPaxMobNo.Text.Trim();
            receipt.Adults = 1;
            receipt.Children = 0;
            receipt.Infants = 0;
            receipt.Fare = Utility.ToDecimal(ITlblTariffAmount.Text.Trim());
            receipt.TotalFare = Utility.ToDecimal(ITlblTariffAmount.Text.Trim());
            receipt.Remarks = ""; // Utility.ToString(txtRemarks.Text.Trim());

            receipt.LocationId = Settings.LoginInfo.LocationID;
            //receipt.AgentId = Settings.LoginInfo.AgentId;
            receipt.Status = Settings.ACTIVE;
            receipt.CreatedBy = Settings.LoginInfo.UserID;

            PaymentMode mode = PaymentMode.Credit;
            receipt.SettlementMode = Utility.ToString(Convert.ToInt32(mode));
            receipt.CurrencyCode = Settings.LoginInfo.Currency; // ddlCurrency.SelectedItem.Text; ;
            receipt.ExchangeRate = 1;//Utility.ToDecimal(txtExchRate.Text.Trim());
            receipt.CreditMode.Set("CREDIT", Utility.ToDecimal(ITlblTariffAmount.Text.Trim()), Utility.ToDecimal(ITlblTariffAmount.Text.Trim()));


            receipt.LocalToCollect = Utility.ToDecimal(ITlblTariffAmount.Text.Trim());
            receipt.BaseToCollect =  Utility.ToDecimal(ITlblTariffAmount.Text);
            //receipt.HandlingFee = handlingFeetype * Utility.ToDecimal(txtHandlingFee.Text.Trim());
            receipt.TotalFare = Utility.ToDecimal(ITlblTariffAmount.Text.Trim());
            receipt.ModeRemarks = ""; // txtModeRemarks.Text.Trim();
            receipt.CompanyId = Utility.ToInteger(IThdfCompany.Value);
            receipt.StaffIdName = ITlblEmpName.Text.Trim();
            receipt.EmployeeId = Utility.ToInteger(IThdfEmpId.Value);
            receipt.VehicleId = 0;//Utility.ToInteger(ddlVehicle.SelectedItem.Value);
            //TODO
            int month = Utility.ToInteger(ITddlMonth.SelectedItem.Value);
            DateTime fromDate = new DateTime(DateTime.Now.Year , month, 01);
            DateTime toDate = fromDate.AddMonths(1).AddDays(-1);
            receipt.FromDate = fromDate;
            receipt.ToDate = toDate;
            receipt.Notes = ""; ;// txtNotes.Text.Trim();
            receipt.EmpTariffAmount = Utility.ToDecimal(ITlblTariffAmount.Text.Trim());
            receipt.PendingCreditRcpts = "";
            receipt.Save();
            long reportId = receipt.TransactionId;
            BindGrid();
            hdfSelectCount.Value = "0";
            string script = "window.open('printReport.aspx?receiptId=" + Utility.ToString(reportId) + "','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            Utility.StartupScript(this.Page, script, "printReport");
            //lblSuccessMsg.Text = Formatter.ToMessage("Receipt No", receipt.DocNumber, Mode == PageMode.Add ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);

        }
    }


    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            //string script = "window.open('ExportExcel.aspx?','','width=0,height=0,toolbar=yes,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            //StartupScript(this.Page, script, "Excel");
            
            //DataTable dt = ReceiptList.Copy(); ;
            //if(dtExport !=null)
            DataTable dt = ((DataTable)gvEmployee.DataSource).Copy(); ;           
            
            string attachment = "attachment; filename=ReceiptList.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dgReceiptList.AllowPaging = false;
            dgReceiptList.DataSource = dt;
            dgReceiptList.DataBind();
            dgReceiptList.RenderControl(htw);
            Response.Write(sw.ToString());
        }
        catch(Exception ex)
        {
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
        finally
        {
            Response.End();
        }
    }

    private  void StartupScript(Page page, string script, string key)
    {

        script = string.Format("{0};", script);
        if (page != null && !string.IsNullOrEmpty(script) && !page.ClientScript.IsStartupScriptRegistered(key))
        {
            if (ScriptManager.GetCurrent(page) != null && ScriptManager.GetCurrent(page).IsInAsyncPostBack)
                System.Web.UI.ScriptManager.RegisterClientScriptBlock(page, page.GetType(), key, script, true);
            else
                page.ClientScript.RegisterStartupScript(page.GetType(), key, script, true);

        }
    }
    
    # endregion
    #region Date Format
    protected string IDDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
    #endregion
    #region  advance filter
   

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
            //Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }    
    # endregion
    # region GridBindMethods
    protected object StatusVisible(object value)
    {
       if(Utility.ToInteger(value)>0)
            return (object)true;
       else
           return (object)false;
    }
    # endregion
}
