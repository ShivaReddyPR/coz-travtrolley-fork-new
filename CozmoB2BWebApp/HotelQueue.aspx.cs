﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.Core;
using CT.BookingEngine;
using CT.Configuration;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using Newtonsoft.Json;
using System.Data;
using System.Web.Services;
using System.Data.SqlClient;
using CT.TicketReceipt.DataAccessLayer;
using System.Web.Script.Serialization;
using System.Configuration;
using CT.MetaSearchEngine;
using System.Web.UI.HtmlControls;
using CT.Corporate;
using CT.AccountingEngine;
using System.Net;

namespace CozmoB2BWebApp
{
    public partial class HotelQueueUI : CT.Core.ParentPage//System.Web.UI.Page
    {
        protected DateTime FromDate;
        protected DateTime ToDate;
        protected int Status;
        protected String source;
        protected int AgentFilter;
        protected string agentType;
        protected String TransType;
        protected static String url;
        protected int LocationId;
        protected String Hotel;
        protected String paxName;
        protected String ConfirmationNo;
        protected ServiceRequest[] serviceRequest = new ServiceRequest[0];
        protected int recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["BookingQueueRecordsPerPage"]);

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.Master.PageRole = true;
                if (!IsPostBack)
                {
                    Settings.LoginInfo.IsOnBehalfOfAgent = false;
                    if (Request["FromDate"] != null)
                    {
                        txtFromDate.Text = Request["FromDate"];
                    }
                    else
                    {
                        txtFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                        //txtFromDate.Text = "01/01/2018";
                    }

                    txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    hdfLAgentType.Value = Settings.LoginInfo.AgentType.ToString();
                    hdfIsCorporate.Value = Settings.LoginInfo.IsCorporate;
                    InitializeControls();
                    ddlLocations.SelectedIndex = -1;
                    try
                    {
                        ddlLocations.Items.FindByText(Settings.LoginInfo.LocationName).Selected = true;
                    }
                    catch { }
                }
                url = Request.Url.Scheme + "://" + Request.Url.Host;
            }
            catch(Exception ex )
            {
                Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "HotelQueueNEw page_load Error : " + ex.ToString(), "");
            }
        }
        private void InitializeControls()
        {
            try
            {
                BindAgent();
                //DropdownDs = GetDropDownValues(Settings.LoginInfo.AgentId);
                Array Sources = Enum.GetValues(typeof(HotelBookingSource));
                foreach (HotelBookingSource source in Sources)
                {
                    if (source == HotelBookingSource.UAH || source == HotelBookingSource.DOTW || source == HotelBookingSource.RezLive || source == HotelBookingSource.LOH || source == HotelBookingSource.HotelBeds || source == HotelBookingSource.GTA || source == HotelBookingSource.Miki || source == HotelBookingSource.HotelConnect || source == HotelBookingSource.TBOHotel || source == HotelBookingSource.WST || source == HotelBookingSource.JAC || source == HotelBookingSource.EET || source == HotelBookingSource.Agoda || source == HotelBookingSource.Yatra || source == HotelBookingSource.GRN || source == HotelBookingSource.OYO || source == HotelBookingSource.GIMMONIX || source == HotelBookingSource.Offline||source==HotelBookingSource.Illusions || source == HotelBookingSource.HotelExtranet)    //Modified by brahmam 26.09.2014
                    {
                        ListItem item = new ListItem(Enum.GetName(typeof(HotelBookingSource), source), ((int)source).ToString());
                        ddlsource.Items.Add(item);
                    }
                }
                Array Statuses = Enum.GetValues(typeof(HotelBookingStatus));
                foreach (HotelBookingStatus status in Statuses)
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(HotelBookingStatus), status), ((int)status).ToString());
                    if (item.Text == "Confirmed")
                    {
                        ddlBookingStatus.Items.Insert(2, new ListItem("Vouchered", "1"));
                    }
                    else
                    {
                        ddlBookingStatus.Items.Add(item);
                    }
                }

                if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
                {
                    ddlLocations.Enabled = true;
                    ddlAgents.Enabled = true;
                    ddlB2BAgent.Enabled = true;
                    ddlB2B2BAgent.Enabled = true;
                }
                else if (Settings.LoginInfo.MemberType == MemberType.SUPERVISOR)
                {
                    //ddlLocations.Visible = false;
                    ddlLocations.Enabled = true;
                    ddlAgents.Enabled = false;
                    ddlB2BAgent.Enabled = false;
                    ddlB2B2BAgent.Enabled = false;
                    //lblSource.Visible = false;
                }
                else
                {
                    ddlLocations.Enabled = false;
                    ddlAgents.Enabled = false;
                    ddlB2BAgent.Enabled = false;
                    ddlB2B2BAgent.Enabled = false;
                }
                ddlLocations.DataSource = LocationMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
                ddlLocations.DataTextField = "location_name";
                ddlLocations.DataValueField = "location_id";
                ddlLocations.DataBind();
                ddlLocations.Items.Insert(0, new ListItem("--All--", "-1"));
                int b2bAgentId;
                int b2b2bAgentId;
                if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
                {
                    ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                }
                else if (Settings.LoginInfo.AgentType == AgentType.Agent)
                {
                    ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    ddlAgents.Enabled = false;
                }
                else if (Settings.LoginInfo.AgentType == AgentType.B2B)
                {
                    ddlAgents.Enabled = false;
                    b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                    ddlAgents.SelectedValue = Convert.ToString(b2bAgentId);
                    ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    ddlB2BAgent.Enabled = false;
                    ddlsource.Visible = false;
                    lblSource.Visible = false;
                }
                else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
                {
                    ddlAgents.Enabled = false;
                    ddlB2BAgent.Enabled = false;
                    b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                    b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                    ddlAgents.SelectedValue = Convert.ToString(b2bAgentId);
                    ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                    ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                    ddlB2B2BAgent.Enabled = false;
                    ddlsource.Visible = false;
                    lblSource.Visible = false;
                }
                BindB2BAgent(Convert.ToInt32(ddlAgents.SelectedItem.Value));
                BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
                {
                    ddlB2B2BAgent.Enabled = false;
                }

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        private void BindAgent()
        {
            try
            {
                DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
                ddlAgents.DataSource = dtAgents;
                ddlAgents.DataTextField = "Agent_Name";
                ddlAgents.DataValueField = "agent_id";
                ddlAgents.DataBind();
                ddlAgents.Items.Insert(0, new ListItem("--All--", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindB2BAgent(int agentId)
        {
            try
            {
                DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
                ddlB2BAgent.DataSource = dtAgents;
                ddlB2BAgent.DataTextField = "Agent_Name";
                ddlB2BAgent.DataValueField = "agent_id";
                ddlB2BAgent.DataBind();
                ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
                ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        private void BindB2B2BAgent(int agentId)
        {
            try
            {
                DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
                ddlB2B2BAgent.DataSource = dtAgents;
                ddlB2B2BAgent.DataTextField = "Agent_Name";
                ddlB2B2BAgent.DataValueField = "agent_id";
                ddlB2B2BAgent.DataBind();
                ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
                ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        [WebMethod]
        public static ArrayList BindLocation(int agentId, string type)
        {
            ArrayList list = new ArrayList();
            DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);
            if (dtLocations != null && dtLocations.Rows.Count > 0)
            {
                foreach (DataRow dr in dtLocations.Rows)
                {
                    list.Add(new ListItem(
              dr["location_name"].ToString(),
              dr["location_id"].ToString()
               ));
                }
            }
            return list;
        }

        [WebMethod]
        public static ArrayList BindAllAgentTypes(int agentId, string AgentType)
        {
            ArrayList list = new ArrayList();
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, AgentType, agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            if (dtAgents != null && dtAgents.Rows.Count > 0)
            {
                foreach (DataRow dr in dtAgents.Rows)
                {
                    list.Add(new ListItem(
              dr["Agent_Name"].ToString(),
              dr["agent_id"].ToString()
               ));
                }
            }
            return list;
        }

        [WebMethod]
        public static string CorpStatusUpdate(int hotelid, string ApprovalStatus, string ApprovalRemarks)
        {
            FlightPolicy flightPolicy = new FlightPolicy();
            //var loggedMemberId = (int)Settings.LoginInfo.UserID;
            var Message = "";
            try
            {
                flightPolicy.Flightid = hotelid;
                flightPolicy.ApprovalStatus = ApprovalStatus;
                flightPolicy.ApprovedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                flightPolicy.SelectedTrip = true;
                flightPolicy.ProductId = 2;
                flightPolicy.ApproverComment = ApprovalStatus == "R" ? ApprovalRemarks : string.Empty;
                flightPolicy.UpdateStatusManually();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Failed to Update Booking Approval Status. Error : " + ex.ToString(), "");
            }

            return Message;
        }

        [WebMethod]
        public static string SubmitRefundRequest(string ConfirmationNo, string TotalPrice, string Remarks, int Adminfee, int Supplierfee)
        {
            var loggedMemberId = (int)Settings.LoginInfo.UserID;
            var Message = "";
          
            if (Adminfee >= 0 || Supplierfee >= 0)
            {
                decimal bookingAmount = Math.Ceiling(Convert.ToDecimal(TotalPrice));
                //decimal bookingAmount = = (itinerary.Roomtype[0].Price.Currency != null ? Util.GetCurrencySymbol(itinerary.Roomtype[0].Price.Currency) : "AED ") + " " + Math.Round(totalPrice, agent.DecimalValue).ToString("N" + agent.DecimalValue);
                decimal feeAmount = Math.Ceiling(Convert.ToDecimal(Adminfee) + Convert.ToDecimal(Supplierfee));
                // for Checking Admin Fee and Supplier Fee should not be Greater Then Booking Amount
                if (feeAmount < 0 || bookingAmount < feeAmount)
                {
                    Message = "Admin Fee and Supplier Fee should not be Greater Then Booking Amount";
                }
                else
                {
                    BookingDetail bookingDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(HotelItinerary.GetHotelId(ConfirmationNo), ProductType.Hotel));
                    HotelItinerary itinerary = new HotelItinerary();
                    HotelRoom room = new HotelRoom();
                    HotelPassenger passInfo = new HotelPassenger();
                    AgentMaster agent = new AgentMaster(bookingDetail.AgencyId);
                    Product[] products = BookingDetail.GetProductsLine(bookingDetail.BookingId);
                    decimal bookingAmt = 0;
                    for (int i = 0; i < products.Length; i++)
                    {
                        if (products[i].ProductTypeId == (int)ProductType.Hotel)
                        {
                            itinerary.Load(products[i].ProductId);
                            passInfo.Load(products[i].ProductId);
                            itinerary.HotelPassenger = passInfo;
                            itinerary.Roomtype = room.Load(products[i].ProductId);
                            break;
                        }
                    }

                    decimal discount = 0;
                    foreach (HotelRoom hroom in itinerary.Roomtype)
                    {
                        if (hroom.Price.NetFare > 0)
                        {
                            //bookingAmt += (hroom.Price.NetFare + hroom.Price.Markup + hroom.Price.B2CMarkup);//Added only B2c Markup by chandan 
                            bookingAmt += (hroom.Price.NetFare + hroom.Price.B2CMarkup);//Added only B2c Markup by chandan 
                        }
                        //else
                        //{
                        //    bookingAmt += (hroom.Price.PublishedFare + hroom.Price.Tax);
                        //}
                        discount += hroom.Price.Discount;
                    }

                    //bookingAmt -= discount;
                    bookingAmt = Math.Ceiling(bookingAmt);
                    // Hashtable ht = new Hashtable();
                    Dictionary<string, Dictionary<string, string>> CancelDetails = new Dictionary<string, Dictionary<string, string>>();
                    Dictionary<string, string> cancellationData = new Dictionary<string, string>();
                    if (itinerary.Source == HotelBookingSource.DOTW)
                    {
                        MetaSearchEngine mse = new MetaSearchEngine();
                        cancellationData = mse.CancelHotel(itinerary, null);
                    }
                    else if (itinerary.Source == HotelBookingSource.HotelBeds)  // Added by brahmam 26.09.2014
                    {
                        CT.BookingEngine.GDS.HotelBeds hBeds = new CT.BookingEngine.GDS.HotelBeds();
                        cancellationData = hBeds.CancelBooking(itinerary);
                    }
                    else if (itinerary.Source == HotelBookingSource.RezLive)
                    {
                        RezLive.XmlHub rezAPI = new RezLive.XmlHub();
                        cancellationData = rezAPI.CancelHotelBooking(itinerary.ConfirmationNo, itinerary.BookingRefNo);
                    }
                    else if (itinerary.Source == HotelBookingSource.LOH)
                    {
                        LotsOfHotels.JuniperXMLEngine jxe = new LotsOfHotels.JuniperXMLEngine();
                        cancellationData = jxe.CancelHotel(itinerary.ConfirmationNo);
                    }
                    else if (itinerary.Source == HotelBookingSource.GTA)
                    {
                        CT.BookingEngine.GDS.GTA gtaApi = new CT.BookingEngine.GDS.GTA();
                        cancellationData = gtaApi.CancelHotelBooking(itinerary);
                    }
                    else if (itinerary.Source == HotelBookingSource.TBOHotel)
                    {
                        string remarks = Remarks;
                        TBOHotel.HotelV10 tboHotel = new TBOHotel.HotelV10();
                        cancellationData = tboHotel.CancelHotelBooking(itinerary.BookingRefNo, remarks);
                    }
                    else if (itinerary.Source == HotelBookingSource.Miki)
                    {
                        CT.BookingEngine.GDS.MikiApi mikiApi = new CT.BookingEngine.GDS.MikiApi(Settings.LoginInfo.LocationCountryCode);
                        string[] confirmationCodes = itinerary.ConfirmationNo.Split('|');
                        foreach (string confirmationCode in confirmationCodes)
                        {
                            cancellationData = mikiApi.CancelBooking(confirmationCode);
                            CancelDetails.Add(confirmationCode, cancellationData);
                            // Dictionary<string, string>[] d = new Dictionary<string, string>[2];

                        }

                    }
                    else if (itinerary.Source == HotelBookingSource.HotelConnect)
                    {
                        CZInventory.SearchEngine his = new CZInventory.SearchEngine();
                        cancellationData = his.CancelBooking(itinerary.ConfirmationNo, true);
                    }
                    //WST Cancellation added by brahmam 27.06.2016
                    else if (itinerary.Source == HotelBookingSource.WST)
                    {
                        string remarks = Remarks;
                        CT.BookingEngine.GDS.WST wstApi = new CT.BookingEngine.GDS.WST();
                        cancellationData = wstApi.CancelBooking(itinerary.ConfirmationNo, remarks);
                    }
                    //JAC Source Added by brahmam 20.09.2016
                    else if (itinerary.Source == HotelBookingSource.JAC)
                    {
                        CT.BookingEngine.GDS.JAC jacApi = new CT.BookingEngine.GDS.JAC();
                        cancellationData = jacApi.CancelHotelBooking(itinerary.ConfirmationNo);
                    }
                    //EET Source Added by brahmam 10.03.2016
                    else if (itinerary.Source == HotelBookingSource.EET)
                    {
                        CT.BookingEngine.GDS.EET eetApi = new CT.BookingEngine.GDS.EET();
                        cancellationData = eetApi.CancelHotel(itinerary.ConfirmationNo);
                    }
                    //Agoda Source Added by brahmam 08.02.2018
                    else if (itinerary.Source == HotelBookingSource.Agoda)
                    {
                        CT.BookingEngine.GDS.Agoda agodaApi = new CT.BookingEngine.GDS.Agoda();
                        cancellationData = agodaApi.CancelHotelBooking(itinerary.ConfirmationNo);
                    }
                    //Yatra Source Added by somasekhar on 14/09/2018
                    else if (itinerary.Source == HotelBookingSource.Yatra)
                    {
                        CT.BookingEngine.GDS.Yatra yatraApi = new CT.BookingEngine.GDS.Yatra();
                        yatraApi.AppUserId = loggedMemberId;
                        cancellationData = yatraApi.CancelHotelBooking(itinerary);
                    }
                    //GRN Source Added by Harish 09.19.2018
                    #region GRN
                    else if (itinerary.Source == HotelBookingSource.GRN)
                    {
                        CT.BookingEngine.GDS.GRN grnApi = new CT.BookingEngine.GDS.GRN();
                        grnApi.GrnUserId = Settings.LoginInfo.UserID;
                        cancellationData = grnApi.CancelHotelBooking(itinerary.BookingRefNo);
                    }
                    #endregion
                    //OYO Source Added by Somasekhar on 20/12/2018
                    else if (itinerary.Source == HotelBookingSource.OYO)
                    {
                        CT.BookingEngine.GDS.OYO oyoApi = new CT.BookingEngine.GDS.OYO();
                        oyoApi.AppUserId = loggedMemberId;
                        cancellationData = oyoApi.CancelHotelBooking(itinerary);
                    }
                    //Get Cancel Info for Gimmonix  source 
                    else if (itinerary.Source == HotelBookingSource.GIMMONIX || itinerary.Source == HotelBookingSource.UAH)
                    {
                        try
                        {
                            string apiUrl = string.Empty;
                            apiUrl = ConfigurationManager.AppSettings["WebApiHotelUrl"].ToString();
                            if (string.IsNullOrEmpty(apiUrl))
                            {
                                apiUrl = url+ "/HotelWebApi";
                                // if (Request.IsSecureConnection == true)
                                // {
                                //     apiUrl = "https://" + Request.Url.Host + "/HotelWebApi";
                                // }
                                // else
                                // {
                                //     apiUrl = "http://" + Request.Url.Host + "/HotelWebApi";
                                //  }
                            }
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:apiURL:" + apiUrl, "");
                            ////string apiUrl = ConfigurationManager.AppSettings["WebApiHotelUrl"];
                            //string apiUrl = string.Empty;
                            //if (Request.IsSecureConnection == true)
                            //{
                            //    apiUrl = "https://" + Request.Url.Host + "/HotelWebApi";
                            //}
                            //else
                            //{
                            //    apiUrl = "http://" + Request.Url.Host+ "/HotelWebApi";
                            //}

                            object input = new
                            {
                                agentId = itinerary.AgencyId,
                                userId = itinerary.CreatedBy,
                                source = itinerary.Source == HotelBookingSource.GIMMONIX ? "GIMMONIX" : "UAH",
                                bookingRefNo = itinerary.Source == HotelBookingSource.GIMMONIX ? itinerary.BookingRefNo : itinerary.ConfirmationNo + "|" + itinerary.BookingRefNo  //"DHB190430162048432|3900673|3871174" //
                            };
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:apiURL 200 :" + apiUrl, "");
                            string inputJson = (new JavaScriptSerializer()).Serialize(input);
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:apiURL 300 :" + apiUrl, "");
                            WebClient client = new WebClient();
                            client.Headers["Content-type"] = "application/json";
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:input 400 :" + inputJson, "");
                            string CancelInfo = client.UploadString(apiUrl + "/api/HotelCancellation/GetCancelInfo", inputJson);
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:apiURL 500 :" + apiUrl, "");
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:inputJson:" + inputJson, "");
                            cancellationData = (new JavaScriptSerializer()).Deserialize<Dictionary<string, string>>(CancelInfo);
                        }
                        catch (Exception exClient)
                        {
                            Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "ziyad:exClient" + exClient.ToString(), "");
                        }
                        //cancellationData = HttpContext.Current.Session["ApiCancelInfo"] as Dictionary<string, string>;
                    }
                    //Added conditon For get cancellation details of Miki & Non-Miki product
                    if (itinerary.Source == HotelBookingSource.Miki)
                    {
                        string currency = "";
                        decimal CancelAmount = 0;
                        //string cancelId = "";
                        foreach (KeyValuePair<string, Dictionary<string, string>> cancelDetail in CancelDetails)
                        {
                            cancellationData = cancelDetail.Value;


                            if (cancellationData["Status"] == "Cancelled" || cancellationData["Status"] == "CANCELLED") //Added by chandan
                            {
                                if (itinerary.CancelId != null && itinerary.CancelId.Length > 0)
                                {
                                    itinerary.CancelId += "|" + cancellationData["ID"];
                                }
                                else
                                {
                                    itinerary.CancelId = cancellationData["ID"]; ;
                                }
                                if (cancellationData.ContainsKey("Amount") && Convert.ToDecimal(cancellationData["Amount"]) > 0)
                                {
                                    CancelAmount += Math.Ceiling(Convert.ToDecimal(cancellationData["Amount"]));

                                }
                                currency = cancellationData["Currency"];

                            }

                            else
                            {
                                Message = "Failed Cancellation from " + itinerary.Source.ToString() + " Confirmation No: " + itinerary.ConfirmationNo;
                                Audit.Add(EventType.MikiCancel, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), Message, null);

                            }

                        }
                        //cancellationData.Add("Amount",Convert.ToString(CancelAmount));
                        // cancellationData.Add("currency", currency);
                        //cancellationData.Clear();

                        //Initialize again with update price and ID For Miki+Non Miki Item
                        cancellationData["Amount"] = Convert.ToString(CancelAmount);
                        cancellationData["Currency"] = Convert.ToString(currency);
                        cancellationData["ID"] = itinerary.CancelId;
                    }

                    if (cancellationData["Status"] == "Cancelled" || cancellationData["Status"] == "CANCELLED") //Added by brahmam
                    {

                        itinerary.Status = HotelBookingStatus.Cancelled;
                        if (itinerary.Source == HotelBookingSource.DOTW)
                        {
                            itinerary.CancelId = cancellationData["ID"];
                        }
                        else if (itinerary.Source == HotelBookingSource.HotelBeds) //Added by brahmam 26.09.2014
                        {
                            itinerary.CancelId = itinerary.ConfirmationNo;
                        }
                        else if (itinerary.Source == HotelBookingSource.RezLive)
                        {
                            //Temp code for testing
                            itinerary.CancelId = itinerary.ConfirmationNo;
                        }
                        else if (itinerary.Source == HotelBookingSource.GTA)
                        {
                            itinerary.CancelId = itinerary.ConfirmationNo;
                        }
                        else if (itinerary.Source == HotelBookingSource.WST)
                        {
                            itinerary.CancelId = itinerary.ConfirmationNo;
                        }
                        else if (itinerary.Source == HotelBookingSource.HotelConnect)
                        {
                            itinerary.CancelId = itinerary.ConfirmationNo;
                        }
                        else if (itinerary.Source == HotelBookingSource.TBOHotel)
                        {
                            itinerary.CancelId = cancellationData["ID"];
                        }
                        // Condition  ---  itinerary.Source == HotelBookingSource.Yatra  
                        // Condition added by somasekhar on 14/09/02018 for Yatra
                        // Condition  ---  itinerary.Source == HotelBookingSource.OYO  
                        // Condition added by somasekhar on 20/1202018 for OYO
                        else if (itinerary.Source == HotelBookingSource.JAC || itinerary.Source == HotelBookingSource.Agoda || itinerary.Source == HotelBookingSource.Yatra || itinerary.Source == HotelBookingSource.OYO)
                        {
                            itinerary.CancelId = cancellationData["ID"];
                        }
                        else if (itinerary.Source == HotelBookingSource.GRN)
                        {
                            itinerary.CancelId = cancellationData["ID"];
                        }
                        else if (itinerary.Source == HotelBookingSource.GIMMONIX)
                        {
                            itinerary.CancelId = cancellationData["ID"];
                        }
                        itinerary.Update();

                        BookingHistory bookingHistory = new BookingHistory();
                bookingHistory.BookingId = bookingDetail.BookingId;
                bookingHistory.EventCategory = EventCategory.HotelCancel;
                bookingHistory.Remarks = "Hotel cancel of the hotel booking";
                bookingHistory.CreatedBy = itinerary.CreatedBy;
                bookingHistory.Save();

                        //ServiceRequest serviceRequest = new ServiceRequest();
                        //serviceRequest = Session["ServiceRequests"] as ServiceRequest[];

                        //                        loggedMemberId = (int)Settings.LoginInfo.UserID;
                        ServiceRequest sr = new ServiceRequest();

                        //TODO: Create a PaymentDetails for cancellation details and update the cancellation charges: Shiva

                        CT.BookingEngine.CancellationCharges cancellationCharge = new CT.BookingEngine.CancellationCharges();
                        cancellationCharge.AdminFee = Convert.ToDecimal(Adminfee);
                        cancellationCharge.SupplierFee = Convert.ToDecimal(Supplierfee);
                        cancellationCharge.PaymentDetailId = 0;//pd.PaymentDetailId;
                        cancellationCharge.ReferenceId = itinerary.Roomtype[0].RoomId;
                        decimal exchangeRate = 0;
                        if (cancellationData["Currency"] != agent.AgentCurrency)
                        {
                            StaticData staticInfo = new StaticData();
                            staticInfo.BaseCurrency = agent.AgentCurrency;

                            Dictionary<string, decimal> rateOfExList = staticInfo.CurrencyROE;
                            if (itinerary.Source == HotelBookingSource.GRN && cancellationData.ContainsKey("BookingId") && cancellationData.ContainsKey("ID") && (cancellationData["BookingId"] == cancellationData["ID"]))
                            {
                                cancellationData["Currency"] = agent.AgentCurrency;
                                string cancelAmount = cancellationData["Amount"];
                                if (cancelAmount.Contains("%"))
                                {
                                    cancelAmount = cancelAmount.Replace('%', ' ');
                                    cancellationData["Amount"] = cancelAmount;
                                    cancellationData["Amount"] = Convert.ToString(bookingAmt * Convert.ToDecimal(cancellationData["Amount"]) / 100);
                                }
                            }
                            exchangeRate = rateOfExList[cancellationData["Currency"]];
                            cancellationCharge.CancelPenalty = (Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate);
                            //Added by somasekhar on 17/09/2018  -- For Calculate Penality Charge 
                            if (itinerary.Source == HotelBookingSource.Yatra)
                            {
                                //Convert.ToDecimal(cancellationData["Amount"]) this is refund amount by supplier(yatra in INR)
                                //Yatra supplier directly giving refund amt, then here we are calculating Cancellation charge Amt 
                                cancellationCharge.CancelPenalty = bookingAmt - Math.Ceiling((Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate));
                            }
                        }
                        else
                        {
                            cancellationCharge.CancelPenalty = Convert.ToDecimal(cancellationData["Amount"]);
                            //Added by somasekhar on 17/09/2018  -- For Calculate Penality Charge 
                            if (itinerary.Source == HotelBookingSource.Yatra)
                            {
                                //Convert.ToDecimal(cancellationData["Amount"]) this is refund amount by supplier(yatra in INR)
                                //Yatra supplier directly giving refund amt, then here we are calculating Cancellation charge Amt 
                                cancellationCharge.CancelPenalty = bookingAmt - Math.Ceiling(Convert.ToDecimal(cancellationData["Amount"]));
                            }
                        }
                        cancellationCharge.CreatedBy = loggedMemberId;
                        cancellationCharge.ProductType = ProductType.Hotel;
                        cancellationCharge.Save();
                        //(itinerary.Source == HotelBookingSource.Yatra && Convert.ToDecimal(cancellationData["Amount"]) >= 0)
                        //above condition added for yatra. If non refundable cancellation they are giving refound amount "0"
                        if (cancellationData.ContainsKey("Amount") && Convert.ToDecimal(cancellationData["Amount"]) > 0 || (itinerary.Source == HotelBookingSource.Yatra && Convert.ToDecimal(cancellationData["Amount"]) >= 0))
                        {
                            try
                            {
                                //if (itinerary.Source == HotelBookingSource.DOTW)
                                //{
                                //    bookingAmt -= Convert.ToDecimal(cancellationData["Amount"]);
                                //}
                                //else if (itinerary.Source != HotelBookingSource.DOTW)
                                //{
                                //decimal exchangeRate = 0;
                                if (cancellationData["Currency"] != agent.AgentCurrency)
                                {
                                    StaticData staticInfo = new StaticData();
                                    staticInfo.BaseCurrency = agent.AgentCurrency;
                                    Dictionary<string, decimal> rateOfExList = staticInfo.CurrencyROE;
                                    exchangeRate = rateOfExList[cancellationData["Currency"]];
                                }
                                if (exchangeRate <= 0)
                                {
                                    bookingAmt -= Math.Ceiling(Convert.ToDecimal(cancellationData["Amount"]));
                                    //Added by somasekhar on 17/09/2018  -- For Calculate Penality Charge 
                                    if (itinerary.Source == HotelBookingSource.Yatra)
                                    {
                                        //Convert.ToDecimal(cancellationData["Amount"]) this is refund amount by supplier(yatra in INR)
                                        // If non refundable cancellation they are giving refound amount "0"
                                        //the bellow we get cancellation refund amount=0, then in ledger we updeted refund amount 0;
                                        bookingAmt -= (bookingAmt - (Math.Ceiling(Convert.ToDecimal(cancellationData["Amount"]))));
                                    }
                                }
                                else if (itinerary.Source != HotelBookingSource.HotelConnect)
                                {
                                    bookingAmt -= Math.Ceiling((Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate));
                                    //Added by somasekhar on 17/09/2018  -- For Calculate Penality Charge 
                                    if (itinerary.Source == HotelBookingSource.Yatra)
                                    {
                                        //Convert.ToDecimal(cancellationData["Amount"]) this is refund amount by supplier(yatra in INR)
                                        // If non refundable cancellation they are giving refound amount "0"
                                        //the bellow we get cancellation refund amount=0, then in ledger we updeted refund amount 0;
                                        bookingAmt -= (bookingAmt - (Math.Ceiling((Convert.ToDecimal(cancellationData["Amount"]) * exchangeRate))));
                                    }
                                }
                                //}
                            }
                            catch { }
                        }
                        bookingAmt = Math.Ceiling(bookingAmt);
                        // Admin & Supplier Fee save in Leadger
                        int invoiceNumber = 0;
                        decimal adminChar = Math.Ceiling(Convert.ToDecimal(Supplierfee) + Convert.ToDecimal(Adminfee));
                        //decimal adminChar = Convert.ToDecimal(txtSupplierFee.Text) + Convert.ToDecimal(txtAdminFee.Text);
                        decimal adminFee = 0;
                        if (adminChar < bookingAmt)
                        {
                            adminFee = adminChar;
                        }
                        LedgerTransaction ledgerTxn = new LedgerTransaction();
                        NarrationBuilder objNarration = new NarrationBuilder();
                        invoiceNumber = Invoice.isInvoiceGenerated(itinerary.Roomtype[0].RoomId, ProductType.Hotel);
                        //ledgerTxn = new LedgerTransaction();
                        ledgerTxn.LedgerId = bookingDetail.AgencyId;
                        ledgerTxn.Amount = -adminFee;
                        objNarration.HotelConfirmationNo = itinerary.ConfirmationNo.Replace('|', '@'); ///modified by brahmam MIKI purpose
                        objNarration.TravelDate = itinerary.StartDate.ToShortDateString();
                        if (itinerary.PaymentMode == ModeOfPayment.CreditCard) //Checking card or credit
                        {
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CardHotelCancellationCharge;
                            objNarration.Remarks = "Card Hotel Cancellation Charges";
                        }
                        else
                        {
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.HotelCancellationCharge;
                            objNarration.Remarks = "Hotel Cancellation Charges";
                        }
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.IsLCC = true;
                        ledgerTxn.ReferenceId = itinerary.Roomtype[0].RoomId;

                        ledgerTxn.Notes = "";
                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = loggedMemberId;
                        ledgerTxn.TransType = itinerary.TransType;
                        ledgerTxn.PaymentMode = (int)itinerary.PaymentMode;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);

                        //save Refund amount
                        ledgerTxn = new LedgerTransaction();
                        ledgerTxn.LedgerId = bookingDetail.AgencyId;
                        ledgerTxn.Amount = bookingAmt;
                        objNarration.PaxName = string.Format("{0} {1}", itinerary.HotelPassenger.Firstname, itinerary.HotelPassenger.Lastname);
                        if (itinerary.PaymentMode == ModeOfPayment.CreditCard) //Checking card or credit  Added by brahmam
                        {
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.CardHotelRefund;
                            ledgerTxn.Notes = "Card Hotel Voucher Refunded";
                            objNarration.Remarks = "Card Refunded for Voucher No -" + itinerary.ConfirmationNo.Replace('|', '@');///modified by brahmam MIKI purpose
                        }
                        else
                        {
                            ledgerTxn.ReferenceType = CT.AccountingEngine.ReferenceType.HotelRefund;
                            ledgerTxn.Notes = "Hotel Voucher Refunded";
                            objNarration.Remarks = "Refunded for Voucher No -" + itinerary.ConfirmationNo.Replace('|', '@');///modified by brahmam MIKI purpose
                        }
                        ledgerTxn.Narration = objNarration;
                        ledgerTxn.IsLCC = true;
                        ledgerTxn.ReferenceId = itinerary.Roomtype[0].RoomId;

                        ledgerTxn.Date = DateTime.UtcNow;
                        ledgerTxn.CreatedBy = loggedMemberId;
                        ledgerTxn.TransType = itinerary.TransType;
                        ledgerTxn.PaymentMode = (int)itinerary.PaymentMode;
                        ledgerTxn.Save();
                        LedgerTransaction.AddInvoiceTxn(invoiceNumber, ledgerTxn.TxnId);


                        if (adminFee <= bookingAmt)
                        {
                            bookingAmt -= adminFee;
                        }
                        //agent = new AgentMaster(bookingDetail.AgencyId);
                        if (itinerary.PaymentMode != ModeOfPayment.CreditCard)
                        {
                            if (itinerary.TransType != "B2C")
                            {
                                //Settings.LoginInfo.AgentBalance += bookingAmt;
                                agent.CreatedBy = Settings.LoginInfo.UserID;
                                agent.UpdateBalance(bookingAmt);

                                if (bookingDetail.AgencyId == Settings.LoginInfo.AgentId)
                                {
                                    Settings.LoginInfo.AgentBalance += bookingAmt;
                                }
                            }
                        }


                        //Update Queue Status
                        CT.Core.Queue.SetStatus(QueueType.Request, bookingDetail.BookingId, QueueStatus.Completed, loggedMemberId, 0, "Completed");
                        sr.UpdateServiceRequestAssignment(bookingDetail.BookingId, (int)ServiceRequestStatus.Completed, loggedMemberId, (int)ServiceRequestStatus.Completed, null);

                        //Sending Email.
                        Hashtable table = new Hashtable();
                        table.Add("agentName", agent.Name);
                        table.Add("hotelName", itinerary.HotelName);
                        table.Add("confirmationNo", itinerary.ConfirmationNo);

                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        UserMaster bookedBy = new UserMaster(itinerary.CreatedBy);
                        AgentMaster bookedAgency = new AgentMaster(itinerary.AgencyId);
                        toArray.Add(bookedBy.Email);
                        toArray.Add(bookedAgency.Email1);
                        //toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
                        string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["HOTEL_CANCEL_MAIL"]).Split(';');
                        foreach (string cnMail in cancelMails)
                        {
                            toArray.Add(cnMail);
                        }
                        //string message = "Your request for cancelling hotel <b>" + itineary.HotelName + " </b> has been processed. Confirmation No:(" + itineary.ConfirmationNo + ")";
                        string message = ConfigurationManager.AppSettings["HOTEL_REFUND"];
                        try
                        {
                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Hotel)Response for cancellation. Confirmation No:(" + itinerary.ConfirmationNo + ")", message, table);
                        }
                        catch { }

                        //TODO Ziyad
                        //Generate an Invoice for the cancellation amount.
                    }
                    else
                    {
                        if (itinerary.Source == HotelBookingSource.TBOHotel)
                        {
                            Message = "Your request is inprocess,kindly contact supplier.";
                        }
                        else if (itinerary.Source == HotelBookingSource.GIMMONIX && cancellationData["Status"] == "ERC")
                        {
                            Message = "Please contact the supplier to verify the reservation status and/or cancel it directly on the supplier system.";
                        }
                        else
                        {
                            Message = "Failed Cancellation from " + itinerary.Source.ToString();
                        }
                    }
                    //return "Success";
                    //Response.Redirect("ChangeRequestQueue.aspx",false);
                }
            }
            return Message;
        }

        [WebMethod]
        //public static string Search(string FromDate, string ToDate, int Status, string PaxName, string Hotel, int LocationId, string ConfirmationNo, int source, int AgentFilter, string agentType, string transType, string Agentref, string CheckinDate, string CheckoutDate)
        public static string Search(string FromDate, string ToDate, int Status, string PaxName, string Hotel, int LocationId, string ConfirmationNo, int source, int AgentFilter, string agentType, string transType, string Agentref, string CheckDate)
        {
            try
            {
                //DateTime startDate = DateTime.MinValue;
                DateTime startDate = DateTime.MinValue;
                DateTime endDate = DateTime.MaxValue;
                DateTime CheckinDate = DateTime.MinValue;
                DateTime CheckoutDate = DateTime.MaxValue;
                IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
                //if(Agentref == "") { 
                if ((Agentref == "") && (CheckDate == "False" ))
                {
                    startDate = (FromDate != string.Empty) ? Convert.ToDateTime(FromDate, provider) : DateTime.Now;
                endDate = (ToDate != string.Empty) ? Convert.ToDateTime(Convert.ToDateTime(ToDate, provider).ToString("dd/MM/yyyy 23:59"), provider) : DateTime.Now;
                    CheckinDate = Convert.ToDateTime("01/01/1753 00:00:00", provider);
                    CheckoutDate = Convert.ToDateTime(Convert.ToDateTime(CheckoutDate, provider).ToString("dd/MM/yyyy 23:59"), provider);
                }
                else if ((Agentref == "") && (CheckDate == "True"))
                {
                    CheckinDate = (FromDate != string.Empty) ? Convert.ToDateTime(FromDate, provider) : DateTime.Now;
                    CheckoutDate = (ToDate != string.Empty) ? Convert.ToDateTime(Convert.ToDateTime(ToDate, provider).ToString("dd/MM/yyyy 23:59"), provider) : DateTime.Now;
                    startDate = Convert.ToDateTime("01/01/1753 00:00:00", provider);
                    endDate = Convert.ToDateTime(Convert.ToDateTime(endDate, provider).ToString("dd/MM/yyyy 23:59"), provider);
                }
                else
                {
                    CheckinDate = Convert.ToDateTime("01/01/1753 00:00:00", provider);
                    CheckoutDate = Convert.ToDateTime(Convert.ToDateTime(CheckoutDate, provider).ToString("dd/MM/yyyy 23:59"), provider);
                    startDate = Convert.ToDateTime("01/01/1753 00:00:00", provider);
                    endDate = Convert.ToDateTime(Convert.ToDateTime(endDate, provider).ToString("dd/MM/yyyy 23:59"), provider);

                }

               
                DataTable dt = CT.Core.Queue.GetHotelQueueList(startDate, endDate, Status, source, transType, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.UserID, AgentFilter, LocationId, ConfirmationNo, PaxName, Hotel, agentType, Agentref, CheckinDate, CheckoutDate);
                
                JavaScriptSerializer oSerializer = new JavaScriptSerializer();

                string JSONString = string.Empty;
                JSONString = JsonConvert.SerializeObject(dt);
                return JSONString;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Hotelqueue.Search. Error : " + ex.ToString(), "");
                throw ex;
            }
        }

        [WebMethod]
        public static string UpdateServiceStatus(string confirmationNr,string remarks)
        {
            string status = "";
            BookingDetail bookingDetail = new BookingDetail(BookingDetail.GetBookingIdByProductId(HotelItinerary.GetHotelId(confirmationNr), ProductType.Hotel));
            int bookingId = bookingDetail.BookingId;
            int loggedMemberId = Convert.ToInt32(Settings.LoginInfo.UserID);
            int requestTypeId = 4;
            AgentMaster agency = null;
            try
            {

                ServiceRequest serviceRequest = new ServiceRequest();
                BookingDetail booking = new BookingDetail();
                booking = new BookingDetail(bookingId);
                HotelItinerary itineary = new HotelItinerary();
                HotelRoom room = new HotelRoom();
                Product[] products = BookingDetail.GetProductsLine(booking.BookingId);
                for (int i = 0; i < products.Length; i++)
                {
                    if (products[i].ProductTypeId == (int)ProductType.Hotel)
                    {
                        itineary.Load(products[i].ProductId);
                        itineary.Roomtype = room.Load(products[i].ProductId);
                        break;
                    }
                }

                agency = new AgentMaster(booking.AgencyId);
                serviceRequest.BookingId = bookingId;
                serviceRequest.ReferenceId = itineary.Roomtype[0].RoomId;
                serviceRequest.ProductType = ProductType.Hotel;

                serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                serviceRequest.Data = remarks;
                serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;

                serviceRequest.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                //serviceRequest.AgencyId = Settings.LoginInfo.AgentId;
                serviceRequest.IsDomestic = itineary.IsDomestic;
                //Audit.Add(EventType.HotelBook, Severity.High, 1, "1", "");
                //Audit.Add(EventType.HotelBook, Severity.High, 1, "pax name:" + itineary.Roomtype[0].AdultCount.ToString(), "");
                //Audit.Add(EventType.HotelBook, Severity.High, 1, "2", "");
                serviceRequest.PaxName = itineary.Roomtype[0].PassenegerInfo[0].Firstname + itineary.Roomtype[0].PassenegerInfo[0].Lastname;
                serviceRequest.Pnr = itineary.ConfirmationNo;
                serviceRequest.Source = (int)itineary.Source;
                serviceRequest.StartDate = itineary.StartDate;
                serviceRequest.SupplierName = itineary.HotelName;
                serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                serviceRequest.ReferenceNumber = itineary.BookingRefNo;
                serviceRequest.PriceId = itineary.Roomtype[0].PriceId;
                //serviceRequest.AgencyId = (int)agency.ID;
                serviceRequest.AgencyId = booking.AgencyId;//modified by Anji based on Harish suggestions //Settings.LoginInfo.AgentId; //Modified by brahmam       //Every request must fall in Admin Queue, so assign Admin ID
                serviceRequest.ItemTypeId = InvoiceItemTypeId.HotelBooking;
                serviceRequest.DocName = "";
                serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
                serviceRequest.LastModifiedOn = DateTime.Now;
                serviceRequest.CreatedOn = DateTime.Now;
                //Added by shiva
                serviceRequest.IsDomestic = true;
                serviceRequest.AgencyTypeId = Agencytype.Cash;
                serviceRequest.RequestSourceId = RequestSource.BookingAPI;
                serviceRequest.Save();
                // Setting booking details status 
                if (requestTypeId == 4)
                {
                    booking.SetBookingStatus(BookingStatus.InProgress, loggedMemberId);
                }
                else
                {
                    booking.SetBookingStatus(BookingStatus.AmendmentInProgress, loggedMemberId);
                }

                BookingHistory bh = new BookingHistory();
                bh.BookingId = bookingId;
                bh.Remarks = "Request sent to " + serviceRequest.RequestType.ToString() + " of the hotel booking.";

                if (requestTypeId == 4)
                {
                    bh.EventCategory = EventCategory.HotelCancel;
                }
                else
                {
                    bh.EventCategory = EventCategory.HotelAmendment;
                }
                bh.CreatedBy = (int)Settings.LoginInfo.UserID;
                bh.Save();
                //Sending email.
                Hashtable table = new Hashtable();
                table.Add("agentName", agency.Name);
                table.Add("hotelName", itineary.HotelName);
                table.Add("confirmationNo", itineary.ConfirmationNo);

                System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                UserMaster bookedBy = new UserMaster(itineary.CreatedBy);
                AgentMaster bookedAgency = new AgentMaster(itineary.AgencyId);
                //toArray.Add(bookedBy.Email);
                //toArray.Add(bookedAgency.Email1);
                string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["HOTEL_CANCEL_MAIL"]).Split(',');
                foreach (string cnMail in cancelMails)
                {
                    toArray.Add(cnMail);
                }
                //toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]); for time bing

                //string message = "Your request for cancelling hotel <b>" + itineary.HotelName + " </b> is under process. Confirmation No:(" + itineary.ConfirmationNo + ")";
                string message = ConfigurationManager.AppSettings["HOTEL_CANCEL_REQUEST"];
                try
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Hotel)Request for cancellation. Confirmation No:(" + itineary.ConfirmationNo + ")", message, table);
                }
                catch { }

                #region Send Mail For HotelInventory
                if (itineary.Source == HotelBookingSource.HotelConnect)
                {
                    try
                    {
                        toArray.Clear();
                        table.Clear();
                        string from = string.Empty, subject = string.Empty, eMailsTo = string.Empty;
                        message = string.Empty;
                        eMailsTo = CZInventory.InvBooking.GetSupplierEmails(itineary.ConfirmationNo);


                        //getting supplier Email Of HotelInventory
                        if (!string.IsNullOrEmpty(eMailsTo))
                        {
                            toArray.AddRange(eMailsTo.Split(','));
                        }

                        from = ConfigurationManager.AppSettings["fromEmail"];
                        subject = ConfigurationManager.AppSettings["HIS_Supp_Cancel_Subject"];
                        message = ConfigurationManager.AppSettings["HIS_Supp_Cancel_Message"];

                        table.Add("hotelName", "<b>" + itineary.HotelName + "</b>");
                        table.Add("confNo", "<b>" + itineary.ConfirmationNo + "</b>");

                        Email.Send(from, from, toArray, subject, message, table);


                    }
                    catch (Exception ex)
                    {

                        Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Mail Sending Failed To Supplier. Error : " + ex.ToString(), "");
                    }

                    try
                    {
                        CZInventory.InvBooking.AddBookingStatusMail(itineary.HotelId, itineary.ConfirmationNo, BookingStatus.CancellationInProgress, string.Empty);
                    }
                    catch (Exception ex)
                    {

                        Audit.Add(EventType.Exception, Severity.High, Settings.LoginInfo.AgentId, "Failed to Update Booking Status. Error : " + ex.ToString(), "");
                    }
                }

                #endregion
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelBook, Severity.High, 1, "Exception in the Hotel Change Request. Message:" + ex.Message, "");

            }

            return status;
        }
        }
}
