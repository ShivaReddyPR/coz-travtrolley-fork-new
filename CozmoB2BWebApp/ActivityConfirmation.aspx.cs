﻿using System;
using System.Web.UI.WebControls;
using CT.Core;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;

public partial class ActivityConfirmationGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected Activity activity = null;
    protected string activityImgFolder;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            activityImgFolder = System.Configuration.ConfigurationManager.AppSettings["ActivityImagesFolder"].ToString();
            if (Session["Activity"] != null)
            {
                activity = Session["Activity"] as Activity;
                lblAgentBalance.Text = Settings.LoginInfo.Currency + " " + Settings.LoginInfo.AgentBalance.ToString("N" + CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue);
                lblAmountPaid.Text = Settings.LoginInfo.Currency + " " + Convert.ToDecimal(activity.TransactionHeader.Rows[0]["TotalPrice"]).ToString("N" + CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue);
                lblTitle.Text = activity.Name;
                lblLocation.Text = activity.City + ", " + activity.Country;
                lblDuration.Text = activity.DurationHours.ToString();
                lblOverview.Text = activity.Overview;
                lblTotal.Text = Convert.ToDecimal(activity.TransactionHeader.Rows[0]["TotalPrice"]).ToString("N" + CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue);
                imgActivity.ImageUrl = activityImgFolder + activity.ImagePath2;
                dlPaxList.DataSource = activity.TransactionDetail;
                dlPaxList.DataBind();


            }
            else
            {
                Response.Redirect("ActivityResults.aspx", false);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
        }
    }
    protected void btnConfirm_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["Activity"] != null)
            {
                decimal total = Convert.ToDecimal(activity.TransactionHeader.Rows[0]["TotalPrice"]);
                
                AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
                decimal currentAgentBalance = agent.UpdateBalance(0);
                if (currentAgentBalance > Convert.ToDecimal(total))
                {
                    activity.SaveActivityTransaction();
                    //Invoice 
                    try
                    {
                        Invoice invoice = new Invoice();
                        int invoiceNumber = CT.BookingEngine.Invoice.isInvoiceGenerated(Convert.ToInt32(activity.TransactionHeader.Rows[0]["ATHId"]), CT.BookingEngine.ProductType.Activity);

                        if (invoiceNumber > 0)
                        {
                            invoice.Load(invoiceNumber);
                        }
                        else
                        {
                            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(Convert.ToInt32(activity.TransactionHeader.Rows[0]["ATHId"]), "", (int)Settings.LoginInfo.UserID, CT.BookingEngine.ProductType.Activity, 1);
                            if (invoiceNumber > 0)
                            {
                                invoice.Load(invoiceNumber);
                                invoice.Status = CT.BookingEngine.InvoiceStatus.Paid;
                                invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                                invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                                invoice.UpdateInvoice();
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, Request.ServerVariables["REMOTE_ADDR"]);
                    }

                    currentAgentBalance = agent.UpdateBalance(0);
                    Settings.LoginInfo.AgentBalance = currentAgentBalance;
                    Label lblAgentBalance = (Label)Master.FindControl("lblAgentBalance");
                    lblAgentBalance.Text = CT.TicketReceipt.Common.Formatter.ToCurrency(currentAgentBalance);
                    //Session.Remove("Activity");// removing from voucher page for email purpose
                    Response.Redirect("ActivityVoucher.aspx?ID=" + Convert.ToInt64(activity.TransactionHeader.Rows[0]["ATHId"]), false);
                }
                else
                {
                    lblBalanceEtrror.Text = "Insufficient Credit Balance ! please contact Admin";
                }
            }
            else
            {
                Response.Redirect("ActivityResults.aspx", false);
            }
                
            
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

}
