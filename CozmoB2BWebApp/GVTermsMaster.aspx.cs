using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using CT.GlobalVisa;
using CT.GlobasVisa;
using CT.Core;

public partial class TermsMasterUI : CT.Core.ParentPage
{
  
    private string GV_TERMS_SESSION = "_GVtermsSession";

    private string GV_TERMS_SEARCH_SESSION = "_GVtermsSearchList";

    #region Page Events

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
            }
            
            lblErrorMsg.Text = string.Empty;
            lblSuccessMsg.Text = string.Empty;
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        try
        {
            Utility.StartupScript(this.Page, "startScript();", "startScript");
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa,Severity.High, 0, ex.ToString(),"0");
            throw;
        }
    }
    //private void StartupScript(Page page, string script, string key)
    //{

    //    script = string.Format("{0};", script);
    //    if (page != null && !string.IsNullOrEmpty(script) && !page.ClientScript.IsStartupScriptRegistered(key))
    //    {
    //        if (ScriptManager.GetCurrent(page) != null && ScriptManager.GetCurrent(page).IsInAsyncPostBack)
    //            System.Web.UI.ScriptManager.RegisterClientScriptBlock(page, page.GetType(), key, script, true);
    //        else
    //            page.ClientScript.RegisterStartupScript(page.GetType(), key, script, true);

    //    }
    //}

    private void InitializePageControls()
    {
        try
        {  
            BindCountryList();
            BindNationalityList();
            BindVisaType(ddlCountry.SelectedValue);
            BindResidence();
        }
        catch
        {
            throw;
        }
    }
    #endregion

    #region gvLocation Events
    private void BindGrid()
    {
        
        try
        {
            CommonGrid grid = new CommonGrid();
           // grid.BindGrid(gvLocation, LocationMaster.GetList(ListStatus.Short, RecordStatus.Activated));
           // setRowId();
        }
        catch
        {
            throw;
        }
    }

   
    private void BindCountryList()
    {
        try
        {
            ddlCountry.DataSource = Country.GetCountryList();
            ddlCountry.DataValueField = "Value";
            ddlCountry.DataTextField = "Key";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "-1"));

        }
        catch { throw; }
    }

    private void BindResidence()//Field Master
    {
        try
        {
            ddlResidence.DataSource = Country.GetCountryList();
            ddlResidence.DataTextField = "Key";
            ddlResidence.DataValueField = "Value";
            ddlResidence.DataBind();
            ddlResidence.Items.Insert(0, new ListItem("--Select Residence--", "-1"));
            ddlResidence.Items.Insert(1,new ListItem("All","0"));
        }
        catch { throw; }
    }
    private void BindNationalityList()
    {
        try
        {
            ddlNationality.DataSource = Country.GetNationalityList();
            ddlNationality.DataTextField = "Key";
            ddlNationality.DataValueField = "Value";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("--Select Nationality--", "-1"));
            ddlNationality.Items.Insert(1, new ListItem("All", "0"));
        }
        catch { throw; }
    }

    private void BindVisaType(string countryCode)
    {
        try
        {
            ddlVisaType.DataSource = VisaTypeMaster.GetListBYCountryCode(ListStatus.Short, RecordStatus.Activated, countryCode);
            ddlVisaType.DataTextField = "VISA_TYPE_NAME";
            ddlVisaType.DataValueField = "VISA_TYPE_ID";
            ddlVisaType.DataBind();
            ddlVisaType.Items.Insert(0, new ListItem("--Select Visa Type--", "-1"));
            ddlVisaType.Items.Insert(1, new ListItem("All", "0"));
        }
        catch { throw; }
    }
    
  
    private void bindSearch()
    {
        try
        {
            LoginInfo loginfo = Settings.LoginInfo;
            DataTable dt = GVTermsMaster.GetList(ListStatus.Long, RecordStatus.All);
            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch { throw; }
    }

    

      protected void btnSave_Click(object sender, EventArgs e)
      {
          try
          {
              Save();
          }
          catch (Exception ex)
          {
              Utility.WriteLog(ex, this.Title);
              Label lblMasterError = (Label)this.Master.FindControl("lblError");
              lblMasterError.Visible = true;
              lblMasterError.Text = ex.Message;
              Utility.Alert(this.Page, ex.Message);
          }
      }
      protected void btnCancel_Click(object sender, EventArgs e)
      {
          try
          {             
              Clear();                           
          }
          catch (Exception ex)
          {
              Utility.WriteLog(ex, this.Title);
              Label lblMasterError = (Label)this.Master.FindControl("lblError");
              lblMasterError.Visible = true;
              lblMasterError.Text = ex.Message;
              Utility.Alert(this.Page, ex.Message);
          }
      }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
         
            this.Master.ShowSearch("Search");
            Clear();
            bindSearch();                     
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.Alert(this.Page, ex.Message);
        }
    }
   
    private void Clear()
    {
        try
        {
            GVTermsMaster terms = new GVTermsMaster();
            txtHeader.Text = string.Empty;
            txtDescription.Text = string.Empty;
            ddlCountry.SelectedIndex = 0;
            ddlNationality.SelectedIndex = 0;            
            ddlResidence.SelectedIndex = 0;            
            
            ddlVisaType.Items.Clear();
            ddlVisaType.Items.Insert(0, new ListItem("--Select Visa Type--", "-1"));
            ddlVisaType.Items.Insert(1, new ListItem("All", "0"));
            ddlVisaType.SelectedIndex = 0;

            btnSave.Text = "Save";
            CurrentObject = null;

        }
        catch { throw; }      
    
    }

    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[GV_TERMS_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["TERMS_ID"] };
            Session[GV_TERMS_SEARCH_SESSION] = value;
        }
    }

    private GVTermsMaster  CurrentObject
    {
        get
        {
            return (GVTermsMaster)Session[GV_TERMS_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(GV_TERMS_SESSION);
            }
            else
            {
                Session[GV_TERMS_SESSION] = value;
            }

        }

    }
    private void Save()
    {

        try
        {
            GVTermsMaster terms;
            if (CurrentObject == null)
            {
                terms = new GVTermsMaster();
            }
            else
            {
                terms  = CurrentObject;
            }

            if (!string.IsNullOrEmpty(txtHeader.Text.Trim()))
            {
                terms.Header = txtHeader.Text.Trim();
            }
            else {
                throw new Exception("Header should not be empty");
            }
            if (!string.IsNullOrEmpty(txtDescription.Text))
            {
                terms.Description = txtDescription.Text.ToString();
            }
            else
            {
                throw new Exception("Description should not be empty");
            }
            if (!string.IsNullOrEmpty(ddlCountry.SelectedItem.Value.ToString()) && ddlCountry.SelectedItem.Value.ToString() != "-1")
            {
                terms.CountryID = Utility.ToString(ddlCountry.SelectedItem.Value);
            }
            else
            {
                throw new Exception("Please select the Country");
            }
            if (!string.IsNullOrEmpty(ddlNationality.SelectedItem.Value.ToString()) && ddlNationality.SelectedValue.ToString() != "-1")
            {
                terms.NationalityId = Utility.ToString(ddlNationality.SelectedItem.Value);
            }
            else
            {
                throw new Exception("Please select the Nationality");
            }
            if (!string.IsNullOrEmpty(ddlVisaType.SelectedItem.Value.ToString()) && ddlVisaType.SelectedItem.Value.ToString() != "-1")
            {
                terms.VisaTypeId = Utility.ToLong(ddlVisaType.SelectedItem.Value);
            }
            else
            {
                throw new Exception("Please select the Visa Type");
            }
            if (!string.IsNullOrEmpty(ddlResidence.SelectedItem.Value.ToString()) && ddlResidence.SelectedItem.Value.ToString() != "-1")
            {
                terms.ResidenceId = Utility.ToString(ddlResidence.SelectedItem.Value);
            }
            else
            {
                throw new Exception("Please select the Residence");
            }         
            
            terms.Status = Settings.ACTIVE;
            terms.CreatedBy = Settings.LoginInfo.UserID;
            terms.Save();                
            lblSuccessMsg.Visible = true;
            lblSuccessMsg.Text = Formatter.ToMessage("terms details are", "", (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            Clear();
       }

       catch
       { throw; }
    }

    private void Edit(long id)
    {
        try
        {
            GVTermsMaster  terms = new GVTermsMaster(id);
            CurrentObject = terms;
            txtHeader.Text=Utility.ToString(terms.Header);
            txtDescription.Text=Utility.ToString(terms.Description);
            ddlCountry.SelectedValue=Utility.ToString(terms.CountryID); 
            ddlNationality.SelectedValue =Utility.ToString(terms.NationalityId);
            BindVisaType(terms.CountryID);
            ddlVisaType.SelectedValue = Utility.ToString(terms.VisaTypeId);
            ddlResidence.SelectedValue = Utility.ToString(terms.ResidenceId);                       
            btnSave.Text = "Update";
            // BindGrid();
        }
        catch
        {
            throw;
        }
    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }

    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            long termsId = Utility.ToLong(gvSearch.SelectedValue);
            Edit(termsId);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }

    }

  
    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{ "HTtxtHeader", "terms_header" },{"HTtxtDescription", "terms_description" }, 
            { "HTtxtCountry", "terms_country_name" },{ "HTtxtNationality", "terms_nationality_name" },{"HTxtVisaType","terms_visa_type_name"},
            {"HTtxtResidence","terms_residence_name"}};
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    #endregion

    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            //hdnDescription.Value = txtDescription.Text;
            BindVisaType(ddlCountry.SelectedValue);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa,Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }
}
