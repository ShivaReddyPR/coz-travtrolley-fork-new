﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeFile="CorporateExpenseDashboard.aspx.cs" Inherits="CorporateExpenseDashboard" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
    <style>
.card-header {
    display: flex;
    align-items: center;
    border-bottom-width: 1px;
    padding-top: 0;
    padding-bottom: 0;
    padding-right: .625rem;
    height: 3.5rem;
}
.card-header.card-header-tab .card-header-title {
    display: flex;
    align-items: center;
    white-space: nowrap;
}
.btn-actions-pane-right {
    margin-left: auto;
    white-space: nowrap;
}
.widget-chart.text-left {
        flex-direction: row;
    align-items:flex-start;
        box-shadow: 0 2px 2px 0 rgba(0,0,0,.14), 0 3px 1px -2px rgba(0,0,0,.12), 0 1px 5px 0 rgba(0,0,0,.2);
    padding: 15px;

        min-height: 100%;
}
.widget-chart.text-left .widget-chart-content {
    display: flex;
    flex-direction: column;
    align-content: center;
    flex: 1;
    position: relative;
}
.widget-chart .widget-subheading:first-child {
    margin-top: 0;
}
.widget-chart .widget-subheading {
margin: -0.5rem 0 0;
    display: block;
    text-transform: uppercase;
    font-weight: bold;
    color: #6d6b6c;
    border-bottom: 1px solid #f1f1f1;
}
.widget-chart .widget-numbers {
    font-weight: bold;
    font-size: 2.5rem;
    display: block;
    line-height: 1;
    margin: 1rem auto;
}
    </style>
    <div class="body_container">
        <br />
        <div class="">
        
    <div class="mb-3 card">
    <div class="card-header-tab card-header">
        <div class="card-header-title font-size-lg text-capitalize font-weight-normal">
             My Tasks
        </div>       
    </div>
    <div class="no-gutters row">
        <div class="col-sm-6 col-md-4 col-xl-4 p-3">
            <div class="card no-shadow rm-border bg-transparent widget-chart text-left">         
                <div class="widget-chart-content">
                    <div class="widget-subheading bg-warning text-white py-2 px-3 ">Required Approvals</div>
                    <div class="widget-numbers text-dark">00</div>
                    <div class="widget-description opacity-8 text-success text-center">
                        
                       Great! You currently have no approvals.
                    </div>
                </div>
            </div>
            <div class="divider m-0 d-md-none d-sm-block"></div>
        </div>
        <div class="col-sm-6 col-md-4 col-xl-4 p-3">
            <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
                
                <div class="widget-chart-content">
                    <div class="widget-subheading bg-success text-white py-2 px-3 ">Available Expenses</div>
                    <div class="widget-numbers text-dark
                        "><span>02</span></div>
                    <div class="widget-description text-dark">
                        <div class="card border-0" >
                          <ul class="list-group list-group-flush mb-0">
                            <li class="list-group-item d-flex">
                                <span class="small text-gray-light pr-2 pt-1">01-Mar-2020</span><a href="javascript:void(0);" class="d-block">
                                Mar 2020 Expenses</a><span class="small text-gray-light pl-2 pt-1 ml-auto">AED 400</span></li>
                            <li class="list-group-item d-flex">  <span class="small pr-2 pt-1">01-Apr-2020</span><a href="javascript:void(0);" class="d-block">
                              
                                Transport- AbuDhabi Exp</a><span class="small text-gray-light pl-2 pt-1 ml-auto">AED 120</span></li>
                          </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
            <div class="divider m-0 d-md-none d-sm-block"></div>
        </div>
        <div class="col-sm-12 col-md-4 col-xl-4 p-3">
            <div class="card no-shadow rm-border bg-transparent widget-chart text-left">
            
                <div class="widget-chart-content">
                    <div class="widget-subheading bg-info text-white py-2 px-3">Open Reports</div>
                    <div class="widget-numbers text-dark"><span>03</span></div>
                    <div class="widget-description text-dark">
                        <div class="card border-0" >
                          <ul class="list-group list-group-flush mb-0">
                            <li class="list-group-item d-flex">
                                <span class="small text-gray-light pr-2  pt-1">14-Apr-2020</span><a href="javascript:void(0);" class="d-block">
                                Client Entertainment</a><span class="small text-gray-light pl-2  pt-1 ml-auto">AED 2500</span></li>
                            <li class="list-group-item d-flex">
                                <span class="small text-gray-light pr-2  pt-1">16-Apr-2020</span><a href="javascript:void(0);" class="d-block">
                                Transport - AbuDhabi Exp -</a><span class="small text-gray-light pl-2  pt-1 ml-auto">AED 240</span></li>
                              <li class="list-group-item d-flex">
                                <span class="small text-gray-light pr-2  pt-1">22-Apr-2020</span><a href="javascript:void(0);" class="d-block">
                                Laundry Exp</a><span class="small text-gray-light pl-2  pt-1 ml-auto">AED 110</span></li>
                          </ul>
                        </div>
                       
                    </div>
                </div>
            </div>
        </div>
        <div class="clear"></div>
    </div>

</div>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

