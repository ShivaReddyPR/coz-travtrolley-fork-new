﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorpGVViewProfileGUI" Codebehind="CorpGVViewProfile.aspx.cs" %>


<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<div class="body_container"> 

<div class="bggray bor_gray paddingtop_10 marbot_20"> 


<%--traveler details table--%>



<div> 
<%if(visaSession !=null) { %>


<div class="col-md-3">  <label>Visa Country:</label> 

<strong>
<%=visaSession.TravelToCountryName %>
 </strong>  </div>

<div class="col-md-3"> 

 <label>Visa Type: </label> 
 
 <strong> <%=visaSession.VisaTypeName %></strong>
   </div>
<div class="col-md-3"> 

<label>Visa Date: </label>

  <strong><%=visaSession.VisaDate.ToString("dd MMM yyyy") %> </strong>   </div>
<div class="col-md-3"> 

<label>No of Applicants:</label>

 <strong> Adult:<%= visaSession.Adult%>Child:<%=visaSession.Child %> Infant:<%=visaSession.Infant %> </strong> 
 
 
 </div>

     
    <%} %>
    <div class="clearfix"> </div>
    </div>





<div class="clearfix"> </div>
</div>


<div> 

    <%if (cropProfileDetails != null)
        { %>
<div class="col-md-6 pad_left0"> 
<h4> Profile </h4>
<div class="bg_white bor_gray paddingtop_10 paddingbot_10 marbot_20"> 

<div class="col-md-12"> 

<img  class="profile_picture" src="<%=rootFolder+"/"+cropProfileDetails.ImagePath %>" /> 
    
<strong><%=cropProfileDetails.Title %> <%=cropProfileDetails.SurName %> <%=cropProfileDetails.Name %></strong><br />
    
<span class="text-muted"><%=cropProfileDetails.DesignationName %></span><br />

Date of joining:  <%=cropProfileDetails.DateOfJoining.ToString("dd MMM yyyy") %><br />
    <%if(!string.IsNullOrEmpty(cropProfileDetails.Email)) { %>
<b> Email:</b> <%=cropProfileDetails.Email %><br />
    <%} %>
     <%if (!string.IsNullOrEmpty(cropProfileDetails.Telephone))
         { %>
<b> Phone:</b> <%=cropProfileDetails.Telephone %> <br />
    <%} %>
<div class="clearfix"> </div>
</div>

<div class="col-md-12"> <a href="#"> <i class="fa fa-pencil-square-o"></i> Edit Profile</a>  </div>
    
<div class="col-md-12"> DOB: <%=cropProfileDetails.DateOfBirth.ToString("dd MMM yyyy") %>  </div>

<div class="col-md-12"> <div class="bggray pad_left10 martop_10"> <b> PASSPORT DETAILS </b> </div></div>
<%if (!string.IsNullOrEmpty(cropProfileDetails.PassportNo))
    { %>
<div class="pad_left10">


<div class="col-md-4"> Passport No: <%=cropProfileDetails.PassportNo %></div>

<div class="col-md-4" >Date of Iss. <%=cropProfileDetails.DateOfIssue.ToString("dd-MM-yyyy") %> </div>

<div class="col-md-4"> Date of Exp. <%=cropProfileDetails.DateOfExpiry.ToString("dd-MM-yyyy") %> </div>


</div>
    <%} %>
<div class="col-md-12"> <div class="bggray pad_left10 martop_10"> <b>  HOLDING VISA</b> </div></div>




<div class="row"> 
<div class="col-md-12"> 

 
    <%if (cropProfileDetails.ProfileVisaDetailsList != null && cropProfileDetails.ProfileVisaDetailsList.Count > 0)
    {
        foreach (CT.Corporate.CorpProfileVisaDetails visaDetails in cropProfileDetails.ProfileVisaDetailsList)
        {
            if (!string.IsNullOrEmpty(visaDetails.Type) && visaDetails.Type == "V")
            {%>
<div class="col-md-12"> 
<div class="border_bot1"> 

<div class="col-md-4"> <%=visaDetails.VisaCountry %> Visa: <%=visaDetails.VisaNumber %> 
    
</div>
<div class="col-md-4">Date of Iss. <%=visaDetails.VisaIssueDate.ToString("dd-MM-yyyy") %> <%if (visaDetails.VisaIssueDate < DateTime.Now)
    { %>
   <span class="custom_alert"> <img src="images/alert.gif">  </span>
    <%} %> </div>
<div class="col-md-4">Date of Exp.  <%=visaDetails.VisaExpDate.ToString("dd-MM-yyyy") %> <%if (visaDetails.VisaExpDate < DateTime.Now)
    { %>
   <span class="custom_alert"> <img src="images/alert.gif">  </span>
    <%} %> </div>
<div class="clearfix"> </div>
</div>
</div>
    <%}
            }
        } %>

</div>
</div>




<div class="col-md-12"> <div class="bggray pad_left10 martop_10"> <b>  ADDRESS</b> </div></div>


<div class="pad_left10"> <div class="col-md-12">  Home Address: <%=cropProfileDetails.Address1 %> <%=cropProfileDetails.Address2 %></div> </div>









<div class="clearfix"> </div>

</div>
</div>
    


<div class="col-md-6 client_docs"> 

<h4> Documents  </h4>

<div class="row"> 
    
<div class="col-md-12"> 
<%if (cropProfileDetails.ProfileDocsList != null && cropProfileDetails.ProfileDocsList.Count > 0)
    {
        foreach (CT.Corporate.CorpProfileDocuments documents in cropProfileDetails.ProfileDocsList)
        {
            string filepath = rootFolder + "" + documents.ProfileId + "//" + documents.DocFileName;
        %>
<a title="<%=documents.DocTypeName %>"  class="image-popup-no-margins" href="<%=filepath%>"> 

<img class="client_docs" src="<%=filepath%>" /> 


</a>


    <%}
        } %>
</div>

</div>




<%--upload document--%>


<h4>  Upload Documents   </h4>
 <div> 



 <!-- Fine Uploader New/Modern CSS file
    ====================================================================== -->
    <link href="fileuploader/fine-uploader-new.css" rel="stylesheet">

    <!-- Fine Uploader JS file
    ====================================================================== -->
    <script src="fileuploader/fine-uploader.js"></script>

    <!-- Fine Uploader Thumbnails template w/ customization
    ====================================================================== -->
    <script type="text/template" id="qq-template-manual-trigger">
        <div class="qq-uploader-selector qq-uploader" qq-drop-area-text="Drop files here">
            <div class="qq-total-progress-bar-container-selector qq-total-progress-bar-container">
                <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-total-progress-bar-selector qq-progress-bar qq-total-progress-bar"></div>
            </div>
            <div class="qq-upload-drop-area-selector qq-upload-drop-area" qq-hide-dropzone>
                <span class="qq-upload-drop-area-text-selector"></span>
            </div>
            <div class="buttons">
                <div class="qq-upload-button-selector qq-upload-button">
                    <div>Select files</div>
                </div>
                <button type="button" id="trigger-upload" class="btn btn-primary">
                    <i class="icon-upload icon-white"></i> Upload
                </button>
            </div>
            <span class="qq-drop-processing-selector qq-drop-processing">
                <span>Processing dropped files...</span>
                <span class="qq-drop-processing-spinner-selector qq-drop-processing-spinner"></span>
            </span>
            <ul class="qq-upload-list-selector qq-upload-list" aria-live="polite" aria-relevant="additions removals">
                <li>
                    <div class="qq-progress-bar-container-selector">
                        <div role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" class="qq-progress-bar-selector qq-progress-bar"></div>
                    </div>
                    <span class="qq-upload-spinner-selector qq-upload-spinner"></span>
                    <img class="qq-thumbnail-selector" qq-max-size="100" qq-server-scale>
                    <span class="qq-upload-file-selector qq-upload-file"></span>
                    <span class="qq-edit-filename-icon-selector qq-edit-filename-icon" aria-label="Edit filename"></span>
                    <input class="qq-edit-filename-selector qq-edit-filename" tabindex="0" type="text">
                    <span class="qq-upload-size-selector qq-upload-size"></span>
                    <button type="button" class="qq-btn qq-upload-cancel-selector qq-upload-cancel">Cancel</button>
                    <button type="button" class="qq-btn qq-upload-retry-selector qq-upload-retry">Retry</button>
                    <button type="button" class="qq-btn qq-upload-delete-selector qq-upload-delete">Delete</button>
                    <span role="status" class="qq-upload-status-text-selector qq-upload-status-text"></span>
                </li>
            </ul>

            <dialog class="qq-alert-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Close</button>
                </div>
            </dialog>

            <dialog class="qq-confirm-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">No</button>
                    <button type="button" class="qq-ok-button-selector">Yes</button>
                </div>
            </dialog>

            <dialog class="qq-prompt-dialog-selector">
                <div class="qq-dialog-message-selector"></div>
                <input type="text">
                <div class="qq-dialog-buttons">
                    <button type="button" class="qq-cancel-button-selector">Cancel</button>
                    <button type="button" class="qq-ok-button-selector">Ok</button>
                </div>
            </dialog>
        </div>
    </script>

    <style>
        #trigger-upload {
            color: white;
            background-color: #00ABC7;
            font-size: 14px;
            padding: 7px 20px;
            background-image: none;
        }

        #fine-uploader-manual-trigger .qq-upload-button {
            margin-right: 15px;
        }

        #fine-uploader-manual-trigger .buttons {
            width: 36%;
        }

        #fine-uploader-manual-trigger .qq-uploader .qq-total-progress-bar-container {
            width: 60%;
        }
        
        
      .custom-checkbox-table input[type="checkbox"] + label::before { border: solid 1px #ccc;  } 
      
   


    </style>


   

                                <div>
                      

                       <div id="fine-uploader-manual-trigger"></div>


                                </div>

  <script>
      $(document).ready(function () {
          var manualUploader = new qq.FineUploader({
              element: document.getElementById('fine-uploader-manual-trigger'),
              template: 'qq-template-manual-trigger',
              request: {
                  endpoint: 'UploadHandler.ashx',
                  forceMultipart: true
              },
              chunking: {
                  enabled: true
              },
              resume: {
                  enabled: true
              },
              retry: {
                  enableAuto: true
              },
              callbacks: {
                  onSubmit: function (id, fileName) {
                      document.getElementById('triggerUpload').style.visibility = 'visible';
                  }
              },
              autoUpload: false,
              debug: true
          });


          qq(document.getElementById("trigger-upload")).attach("click", function () {
              manualUploader.uploadStoredFiles();
          });
      });
    </script>

</div>



<div>
<br /> 


 <asp:Button ID="btnProceed" runat="server" PostBackUrl="CorpGVEmployeeDetail.aspx" Text="Proceed" class="btn but_b pull-right"/> </div>






</div>
    <%} %>
<div class="clearfix"> </div>

</div>




</div>

<%--DROPZONES STARTS--%>
     






<%--MAGNIC POPUP STARTS--%>

<script>


    $(document).ready(function () {

        $('.image-popup-vertical-fit').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            mainClass: 'mfp-img-mobile',
            image: {
                verticalFit: true
            }

        });

        $('.image-popup-fit-width').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            image: {
                verticalFit: false
            }
        });

        $('.image-popup-no-margins').magnificPopup({
            type: 'image',
            closeOnContentClick: true,
            closeBtnInside: false,
            fixedContentPos: true,
            mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
            image: {
                verticalFit: true
            },
            zoom: {
                enabled: true,
                duration: 300 // don't foget to change the duration also in CSS
            }
        });

    });
</script>
<script src="Scripts/jquery.magnific-popup.js"></script>
<link rel="stylesheet" href="css/magnific-popup.css">

<%--AWESOME FONTS--%>
<link rel="stylesheet" href="css/fontawesome/css/font-awesome.min.css">

</asp:Content>




<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

