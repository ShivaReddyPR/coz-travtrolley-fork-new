﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using System.IO;
using CT.Corporate;
using DevExpress.Web;
using System.Drawing;
using DevExpress.Web.Internal;
using System.Web.Services;
using System.Web.Script.Services;
using Newtonsoft.Json;
using System.Linq;

public partial class CorporateProfileUI : CT.Core.ParentPage
{
    private string CORP_PROFILE_SESSION = "_CorporateProfileSession";
    private string CORP_PROFILE_SEARCH_SESSION = "_CorporateProfileSearchList";
   

    protected void Page_Load(object sender, EventArgs e)
    {

        this.Master.PageRole = true;

        try
        {              
          if( !string.IsNullOrEmpty(Request.QueryString["show"]) && Request.QueryString["show"]=="query")
            {
                divEmpQuerySearch.Visible = true;
                btnSave.Visible = false;
                btnClear.Visible = false;
                btnSearch.Visible = false;
            }
            else {
                divEmpQuerySearch.Visible = false;
                btnSave.Visible = true;
                btnClear.Visible = true;
                btnSearch.Visible = true;

            }
            if (!IsPostBack)
            {

                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();

                //Clear();
                //lblSuccessMsg.Text = string.Empty;

            }
            //StartupScript(this.Page, " showHidMode(" + ddlSettlementMode.SelectedItem.Value + ")", " showHidMode");
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void Page_PreRender(object sender, EventArgs e)
    {
        string[] splitter = { "&&" };
        //FF
        if (!string.IsNullOrEmpty(hdfFFDisplayText.Value) && !string.IsNullOrEmpty(hdfFFDisplayValue.Value))
        {
            string[] displayText = hdfFFDisplayText.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            string[] displayValue = hdfFFDisplayValue.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < displayText.Length; i++)
            {
                if (lstFF.Items.FindByValue(displayValue[i]) == null)
                    lstFF.Items.Add(new ListItem(displayText[i], displayValue[i]));


            }

        }
        //HM
        if (!string.IsNullOrEmpty(hdfHMDisplayText.Value) && !string.IsNullOrEmpty(hdfHMDisplayValue.Value))
        {
            string[] displayText = hdfHMDisplayText.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            string[] displayValue = hdfHMDisplayValue.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < displayText.Length; i++)
            {
                if (lstHM.Items.FindByValue(displayValue[i]) == null)
                    lstHM.Items.Add(new ListItem(displayText[i], displayValue[i]));
            }

        }

        //CC
        if (!string.IsNullOrEmpty(hdfCCDisplayText.Value) && !string.IsNullOrEmpty(hdfCCDisplayValue.Value))
        {
            string[] displayText = hdfCCDisplayText.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
            string[] displayValue = hdfCCDisplayValue.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

            for (int i = 0; i < displayText.Length; i++)
            {
                if (lstCC.Items.FindByValue(displayValue[i]) == null)
                    lstCC.Items.Add(new ListItem(displayText[i], displayValue[i]));
            }

        }

        //Utility.StartupScript(this.Page, "SelectAllRoles();", "SelectAllRoles");

    }
    private void InitializePageControls()
    {
        try
        {

            // Clear();
            BindApproversList();
            //string rootFolder = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["ProfileImage"]);
            //profileImage.SavePath = rootFolder;
            //string profileImg = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss")).Replace(":", "");
            //profileImg = profileImg.Replace("-", "");
            //profileImage.FileName = profileImg;
            //string AgentImgPath = Server.MapPath("~/" + rootFolder + "/") + profileImg;
            //hdfProfileImage.Value = AgentImgPath;

            BindAgent();
            BindLocation();
            BindSetupValues(ddlDesignation, "DG");
            BindSetupValues(ddlDivision, "DV");
            BindSetupValues(ddlCostCentre, "CS");
            BindAirline();
            BindNationality();
            BindMemberType();
            BindProfileType();
            BindGrade();
            BindApproversList();
            BindCountry(ddlcountryOfIssue);
            BindCountry(ddlPlaceOfBirth);
            BindCountry(ddlVisaIssueCntry);
            BindCountry(ddlcontactcountry);
            BindCountry(ddlvisaplaceofissue);
            BindCountry(ddlResidency);
            BindCorpCardDetails();
            BindCostCenters();
            BindDelegateSupervisors();
            Clear();           
            // BindCurrency();
            //BindRegion();
            //BindExecutive();
        }
        catch (Exception ex) {
            throw ex; }
    }

    private void Clear()
    {
        try
        {
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            if (agentId == 0) agentId = 1;
            CorporateProfile corpProfile = new CorporateProfile(-1, agentId);

            DataTable dtFlex = corpProfile.DtProfileFlex;

            //CreateFlexFields(dtFlex);
            BindFlexGrid(dtFlex);
            DataTable dtPolicies = corpProfile.DtProfilePolicy;
            BindPolicies(dtPolicies);

            //DataTable dtRoleDtls = corpProfile.DtUserRoles;
            //BindUserRoles(dtRoleDtls);
 txtbatch.Text = string.Empty;
            //if (Utility.ToInteger(Settings.LoginInfo.AgentId) > 1)
            //    ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
            ddlProfileType.SelectedIndex = 0;
            ddlTitle.SelectedIndex = 0;
            txtSurname.Text = string.Empty;
            txtname.Text = string.Empty;
            ddlDesignation.SelectedIndex = 0;
            //         corpProfile.ImagePath = Utility.ToString("");
            //corpProfile.ImageType = Utility.ToString("");
            txtEmpId.Text = string.Empty;
            ddlDivision.SelectedIndex = 0;
            ddlCostCentre.SelectedIndex = 0;
            ddlGrade.SelectedIndex = 0;
            txtTelPhone.Text = string.Empty;
            txtMobileNo.Text = string.Empty;
            txtFax.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtAddress1.Text = string.Empty;
            txtAddress2.Text = string.Empty;
            ddlNationality.SelectedIndex = 0;
            txtPassportNo.Text = string.Empty;
            txtDOI.Text = string.Empty;
            txtDOE.Text = string.Empty;
            ddlcountryOfIssue.SelectedIndex = 0;
            txtDOB.Text = string.Empty; ;
            ddlPlaceOfBirth.SelectedIndex = 0;
            //rdbMale.Checked = true;
            ddlGender.SelectedIndex = -1;
            txtSeatPref.Text = string.Empty;
            txtMealRequest.Text = string.Empty;
            txtRemarks.Text = string.Empty;
            ddlRoomType.SelectedIndex = 0;
            txtOtherRemarks.Text = string.Empty;
            txtMobileCoutryCode.Text = string.Empty;
            txtPhoneCountryCode.Text = string.Empty;
            txtPassword.Enabled = txtConfirmPassword.Enabled = txtLoginName.Enabled = true;
            txtPassword.Text = txtConfirmPassword.Text = txtLoginName.Text = string.Empty;
            if (Settings.LoginInfo.AgentType != AgentType.BaseAgent)
            {
                //ddlLocation.SelectedValue = Utility.ToString(Settings.LoginInfo.LocationID);
                ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
            }
            if (Utility.ToInteger(Settings.LoginInfo.AgentId) > 1)
                ddlLocation.SelectedValue = Utility.ToString(Settings.LoginInfo.LocationID);

            //txtPassword.Text = txtConfirmPassword.Text = string.Empty;
            //ddlMemberType.SelectedIndex = 0;

            ddlAirLine.SelectedIndex = 0;
            txtAirCardNo.Text = string.Empty;
            ddlHotel.SelectedIndex = 0;
            txtHotelCardNo.Text = string.Empty;
            txtCCcardNo.Text = string.Empty;
            txtCardDOE.Text = string.Empty;
            lstFF.Items.Clear();
            lstHM.Items.Clear();
            lstCC.Items.Clear();
            hdfFFDeleted.Value = string.Empty;
            hdfHMDeleted.Value = string.Empty;
            hdfCCDeleted.Value = string.Empty;

            hdfFFDisplayText.Value = string.Empty;
            hdfHMDisplayText.Value = string.Empty;
            hdfCCDisplayText.Value = string.Empty;

            hdfFFDisplayValue.Value = string.Empty;
            hdfHMDisplayValue.Value = string.Empty;
            hdfCCDisplayValue.Value = string.Empty;

            hdfRTDetailId.Value = "0";

            hdfDetailsIndexFF.Value = string.Empty;
            hdfDetailsIndexHM.Value = string.Empty;
            hdfDetailsIndexCC.Value = string.Empty;

            hdfFFId.Value = "0";
            hdfHMId.Value = "0";
            hdfCCId.Value = "0";
            hdfCount.Value = "0";
            hdfMode.Value = "0";
            CurrentObject = null;
            // imgPreview.Style.Add("display", "none");
            profileImage.Clear();

            //Approvals Tab -- hidden fields
           // hdnApprovers.Value = string.Empty;
            hdnApproversList.Value = string.Empty;
            //hdnSavedTA.Value = string.Empty;
            //hdnSavedEA.Value = string.Empty;
            //hdnDelETA.Value = string.Empty;
            //hdnVisaApprovers.Value = string.Empty;
            //hdnSavedVA.Value = string.Empty;
            hdnVisaDetails.Value = string.Empty;
            hdnDelVD.Value = string.Empty;
            hdnProfileId.Value = string.Empty;
            dcJoiningDate.Value = DateTime.Now;
            txtMiddleName.Text = string.Empty;
            //dcTerminationdate.Value = DateTime.Now;
            ddlResidency.SelectedIndex = 0;
            ddlDeligateSupervisor.SelectedValue = "0";
            txtExecAssistance.Text = string.Empty;
            ddlvisaplaceofissue.SelectedIndex = 0;
            txtGSTNumber.Text = string.Empty;
            txtGSTName.Text = string.Empty;
            txtGSTEmail.Text = string.Empty;
            txtGSTAddress.Text = string.Empty;
            txtGSTPhone.Text = string.Empty;
            ddlDomestic.SelectedIndex = 0;
            ddlinternational.SelectedIndex = 0;
            txtGDSProfilePNR.Text = string.Empty;
            txtGDS.Text = string.Empty;
            txtGDScorporateSSR.Text= string.Empty;
            txtGDSDescription.Text= string.Empty;
            txtGDSextraCommand.Text = string.Empty;
            txtGDSOSI.Text = string.Empty;
            txtGDSOwnerPCC.Text= string.Empty;
            txtGDSQueueNo.Text = string.Empty;
            txtGDSRemraks.Text = string.Empty;
            txtContactccemail.Text = string.Empty;
            txtcontactCity.Text = string.Empty; ;
            txtContactPhone.Text = string.Empty;
            txtcontactpincode.Text = string.Empty;
            txtcontactState.Text = string.Empty;
            txtcontactstreet.Text = string.Empty;
            ddlcontactcountry.SelectedIndex = 0;
            rbncontactEmailNotification.Checked = false;
            ddlApproverType.SelectedValue = "N";
            ddlExpenseCard.SelectedValue = "0";
            ddlTravellerCard.SelectedValue = "0";
            chkAffidavit.Checked = false;
            chkIsTravellerCard.Checked = false;
            chkIsExpenseCard.Checked = false;
            if (Session["profileImagePath"] != null)
                Session.Remove("profileImagePath");

        }
        catch { throw; }

    }
    //Clearing controls gridview search controls.
    //Added by Anji on 04/11/2019.
    private void CleargridSearch()
    {
        try
        {
            txtgSurname.Text = string.Empty;
            txtGname.Text = string.Empty;
            txtGEmail.Text = string.Empty;
            txtGEmpid.Text = string.Empty;

        }
        catch { throw; }
    }
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgent.DataSource = dtAgents;
            ddlAgent.DataTextField = "Agent_Name";
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("--Select Client--", "0"));
            ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
            {
                ddlAgent.Enabled = true;
            }
            else
            {
                ddlAgent.Enabled = false;
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Corporate Profile page " + ex.Message, "0");
        }
    }

    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            //if (agentId == 0) agentId = 1;
            BindLocation();
            BindGrade();
            BindSetupValues(ddlDesignation, "DG");
            BindSetupValues(ddlDivision, "DV");
            BindSetupValues(ddlCostCentre, "CS");
            Clear();
            Utility.StartupScript(this.Page, "appendAllApproversList();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            //Utility.StartupScript(this.Page, "ShowMessageDialog('btnSave_Click','"+ex.Message+"','Information')", "btnSave_Click"); 
            Utility.Alert(this.Page, ex.Message);
        }
    }
    private void BindLocation()
    {
        try
        {
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            DataTable dt = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
            ddlLocation.DataSource = dt;
            ddlLocation.DataTextField = "Location_name";
            ddlLocation.DataValueField = "location_id";
            ddlLocation.DataBind();
            ddlLocation.Items.Insert(0, new ListItem("--Select Location --", "0"));
            //if (agentId <= 1)
            ddlLocation.SelectedValue = Convert.ToString(Settings.LoginInfo.LocationID);

        }
        catch { throw; }
    }
    private void BindSetupValues(DropDownList ddl, string type) //Designations,Divisions,CostCenter
    {
        try
        {
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            DataTable dt = CorporateProfileSetup.GetList(agentId, type, ListStatus.Short);
            ddl.DataSource = dt;
            ddl.DataTextField = "Name";
            ddl.DataValueField = "SetupId";
            ddl.DataBind();
            string textDefault = string.Empty;
            if (type == "DG") textDefault = "--Select Designation --";
            else if (type == "DV") textDefault = "--Select Division --";
            else if (type == "CS") textDefault = "--Select Cost Centre --";

            ddl.Items.Insert(0, new ListItem(textDefault, "0"));

        }
        catch { throw; }
    }
    private void BindCostCenters()
    {
        try
        {
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            DataTable dt = CorporateProfileSetup.GetList(agentId, "CS", ListStatus.Short);
            foreach (DataRow dr in dt.Rows)
            {
                chkCostCenters.Items.Add(new ListItem() { Value = dr["SetupId"].ToString(), Text = dr["Name"].ToString() });
            }
        }
        catch { throw; }
    }
    private void BindDelegateSupervisors()
    {
        try
        {
            DataTable dt = CorporateProfile.GetCorpProfilesList(Settings.LoginInfo.AgentId);
            //DataView dv = dt.DefaultView;
            //dv.RowFilter = "ProfileId not in (" + Settings.LoginInfo.CorporateProfileId+")";
            ddlDeligateSupervisor.DataSource = dt;
            ddlDeligateSupervisor.DataTextField = "Name";
            ddlDeligateSupervisor.DataValueField = "ProfileId";
            ddlDeligateSupervisor.DataBind();
            ddlDeligateSupervisor.Items.Insert(0, new ListItem("--Select Supervisor --", "0"));
        }
        catch { throw; }
    }
    private void BindAirline()
    {
        try
        {
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            DataTable dt = Airline.GetAllAirlines();
            ddlAirLine.DataSource = dt;
            ddlAirLine.DataTextField = "AIRLINENAME";
            ddlAirLine.DataValueField = "AIRLINECODE";
            ddlAirLine.DataBind();
            ddlAirLine.Items.Insert(0, new ListItem("--Select Airline --", "0"));

        }
        catch { throw; }
    }
    private void BindNationality()
    {
        try
        {
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            ddlNationality.DataSource = CT.Core.Country.GetNationalityList();
            ddlNationality.DataTextField = "Key";
            ddlNationality.DataValueField = "Value";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("--Select Nationality --", "0"));

        }
        catch { throw; }
    }
    private void BindMemberType()
    {
        try
        {
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            DataSet ds = UserMaster.GetMemberTypeList("user_type");
            //ddlMemberType.DataSource = ds;
            //ddlMemberType.DataTextField = "FIELD_TEXT";
            //ddlMemberType.DataValueField = "FIELD_VALUE";
            //ddlMemberType.DataBind();
            //ddlMemberType.Items.Insert(0, new ListItem("--Select MemberType --", "0"));

        }
        catch { throw; }
    }

    private void BindProfileType()
    {
        try
        {
            //int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            DataSet ds = UserMaster.GetMemberTypeList("PROFILE_TYPE");
            ddlProfileType.DataSource = ds;
            ddlProfileType.DataTextField = "FIELD_TEXT";
            ddlProfileType.DataValueField = "FIELD_VALUE";
            ddlProfileType.DataBind();
            ddlProfileType.Items.Insert(0, new ListItem("--Select Profile Type --", "0"));

        }
        catch { }
    }

    private void BindGrade()
    {
        try
        {
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            DataTable dt = CorporateProfileSetup.GetList(agentId, "GD", ListStatus.Short);
            ddlGrade.DataSource = dt;
            ddlGrade.DataTextField = "Name";
            ddlGrade.DataValueField = "Code";
            ddlGrade.DataBind();
            ddlGrade.Items.Insert(0, new ListItem("--Select Profile Grade --", "0"));

        }
        catch { throw; }
    }

    //Lokesh -1June2017 Regarding CorporateProfile.aspx -- Approvals Tab
    /// <summary>
    /// This method will return all the approvers from the table Corp_Profile with Grade 'M'
    /// </summary>
    private void BindApproversList()
    {

        try
        {
            //M-Manager Type Profile
            DataTable dtApprovers = CorporateProfile.GetProfilesListByGrade("A", "M",Settings.LoginInfo.AgentId);
            hdnApprovers.Value =  JsonConvert.SerializeObject(dtApprovers);           
        }
        catch { throw; }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
            Utility.StartupScript(this.Page, "UploadFiles();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            //Utility.StartupScript(this.Page, "ShowMessageDialog('btnSave_Click','"+ex.Message+"','Information')", "btnSave_Click"); 
            Utility.Alert(this.Page, ex.Message);
        }
    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
            Utility.StartupScript(this.Page, "ClearAll();", "SCRIPT");

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            //Utility.StartupScript(this.Page, "ShowMessageDialog('btnSave_Click','"+ex.Message+"','Information')", "btnSave_Click"); 
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void Save()
    {
        try
        {
            CorporateProfile corpProfile;
            if (CurrentObject == null)
            {
                corpProfile = new CorporateProfile();

            }
            else
            {
                corpProfile = CurrentObject;
            }

            corpProfile.AgentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            corpProfile.ProfileType = Utility.ToString(ddlProfileType.SelectedItem.Value);
            corpProfile.Title = Utility.ToString(ddlTitle.SelectedItem.Value);//TODO
            corpProfile.SurName = Utility.ToString(txtSurname.Text.Trim());
            corpProfile.Name = Utility.ToString(txtname.Text.Trim());
            corpProfile.Designation = Utility.ToString(ddlDesignation.SelectedItem.Value);
            
            string strProfileImagePath = Session["profileImagePath"] as string;
            if (!string.IsNullOrEmpty(strProfileImagePath))
            {
                string[] fileExtension = strProfileImagePath.Split('.');
              
                corpProfile.ImagePath = strProfileImagePath.Substring(strProfileImagePath.Substring(0, strProfileImagePath.LastIndexOf("\\") + 1).Length, strProfileImagePath.Length - (strProfileImagePath.Substring(0, strProfileImagePath.LastIndexOf("\\") + 1).Length));
                
                corpProfile.ImageType = "."+fileExtension[1];
            }
            //corpProfile.ImagePath=Utility.ToString("");		
            //corpProfile.ImageType=Utility.ToString("");		
            corpProfile.EmployeeId = Utility.ToString(txtEmpId.Text.Trim());
            corpProfile.Division = Utility.ToInteger(ddlDivision.SelectedItem.Value);
            corpProfile.CostCentre = Utility.ToInteger(ddlCostCentre.SelectedItem.Value);
            corpProfile.Grade = Utility.ToString(ddlGrade.SelectedItem.Value);
            corpProfile.Telephone = txtPhoneCountryCode.Text.Trim() + (string.IsNullOrEmpty(txtTelPhone.Text.Trim()) ? string.Empty : "-" + txtTelPhone.Text.Trim());
            corpProfile.Mobilephone = txtMobileCoutryCode.Text.Trim() + (string.IsNullOrEmpty(txtMobileNo.Text.Trim()) ? string.Empty : "-" + txtMobileNo.Text.Trim());
            corpProfile.Fax = Utility.ToString(txtFax.Text.Trim());
            corpProfile.Email = Utility.ToString(txtEmail.Text.Trim());
            corpProfile.Address1 = Utility.ToString(txtAddress1.Text.Trim());
            corpProfile.Address2 = Utility.ToString(txtAddress2.Text.Trim());
            corpProfile.NationalityCode = Utility.ToString(ddlNationality.SelectedItem.Value);
            corpProfile.PassportNo = Utility.ToString(txtPassportNo.Text.Trim());

            IFormatProvider dateFormat1 = new System.Globalization.CultureInfo("en-GB");
            corpProfile.DateOfIssue = Convert.ToDateTime(txtDOI.Text, dateFormat1);
            corpProfile.DateOfExpiry = Convert.ToDateTime(txtDOE.Text, dateFormat1);
            corpProfile.DateOfBirth = Convert.ToDateTime(txtDOB.Text, dateFormat1);

            //corpProfile.DateOfIssue = Utility.ToDate(txtDOI.Text);
            //corpProfile.DateOfExpiry = Utility.ToDate(txtDOE.Text);
            corpProfile.PassportCOI = Utility.ToString(ddlcountryOfIssue.SelectedItem.Value);
            corpProfile.PlaceOfIssue= Utility.ToString(ddlcountryOfIssue.SelectedItem.Value);
            //corpProfile.DateOfBirth = Utility.ToDate(txtDOB.Text);
            corpProfile.PlaceOfBirth = Utility.ToString(ddlPlaceOfBirth.SelectedItem.Value);
            corpProfile.Gender = Utility.ToString(ddlGender.SelectedItem.Value); //rdbMale.Checked ? "M" : "F";           
            corpProfile.SeatPreference = Utility.ToString(txtSeatPref.Text.Trim());
            corpProfile.MealRequest = Utility.ToString(txtMealRequest.Text.Trim());
            corpProfile.HotelRemarks = Utility.ToString(txtRemarks.Text.Trim());
            corpProfile.OtherRemarks = Utility.ToString(txtOtherRemarks.Text.Trim());
            corpProfile.Status = Settings.ACTIVE;
            corpProfile.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
            corpProfile.LoginName = txtLoginName.Text.Trim();
            if (ddlLocation.SelectedIndex > 0)
                corpProfile.LocationId = Utility.ToInteger(ddlLocation.SelectedItem.Value);
            corpProfile.Password = txtPassword.Text.Trim();
            
            //string type = ddlMemberType.SelectedValue;
            //if (type == MemberType.ADMIN.ToString())
            //{
            //    corpProfile.MemberType = MemberType.ADMIN;
            //}
            //else if (type == MemberType.CASHIER.ToString())
            //{
            //    corpProfile.MemberType = MemberType.CASHIER;
            //}
            //else if (type == MemberType.OPERATIONS.ToString())
            //{
            //    corpProfile.MemberType = MemberType.OPERATIONS;
            //}
            //else if (type == MemberType.BACKOFFICE.ToString())
            //{
            //    corpProfile.MemberType = MemberType.BACKOFFICE;
            //}
            //else if (type == MemberType.SUPERVISOR.ToString())
            //{
            //    corpProfile.MemberType = MemberType.SUPERVISOR;
            //}
            //else if (type == MemberType.SUPER.ToString())
            //{
            //    corpProfile.MemberType = MemberType.SUPER;
            //}

            if (ddlProfileType.SelectedIndex > 0)
                corpProfile.MemberType = ddlProfileType.SelectedValue == MemberType.SUPERVISOR.ToString() ? MemberType.SUPERVISOR : MemberType.TRAVELCORDINATOR;
            //Added by Somasekhar on 29/03/2018
            corpProfile.MartialStatus = Utility.ToString(ddlMartialStatus.SelectedItem.Value);
            //phani 
            corpProfile.MiddleName = Utility.ToString(txtMiddleName.Text.Trim());
            //corpProfile.DateOfTermination= Convert.ToDateTime(dcTerminationdate.Value, dateFormat1);
            corpProfile.GstNumber = Utility.ToString(txtGSTNumber.Text.Trim());
            corpProfile.GstName = Utility.ToString(txtGSTName.Text.Trim());
            corpProfile.GstAddress= Utility.ToString(txtGSTAddress.Text.Trim());
            corpProfile.GstEmail = Utility.ToString(txtGSTEmail.Text.Trim());
            corpProfile.GstPhone= Utility.ToString(txtGSTPhone.Text.Trim());
            corpProfile.Residency = Utility.ToString(ddlResidency.SelectedItem.Value);
            corpProfile.ExecAssistance = Utility.ToString(txtExecAssistance.Text.Trim());
            corpProfile.DeligateSupervisor =Convert.ToInt64(ddlDeligateSupervisor.SelectedValue);
            corpProfile.GdsProfilePNR = Utility.ToString(txtGDSProfilePNR.Text.Trim());
            corpProfile.DomesticEligibility = Utility.ToBoolean(ddlDomestic.SelectedValue);
            corpProfile.IntlEligibility = Utility.ToBoolean(ddlinternational.SelectedValue);
            corpProfile.Batch = Utility.ToString(txtbatch.Text.Trim());
            corpProfile.ApproverType = Utility.ToString(ddlApproverType.SelectedValue);
            corpProfile.Affidivit = Utility.ToBoolean(chkAffidavit.Checked);
            corpProfile.IsCardAllowedTravel = Utility.ToBoolean(chkIsTravellerCard.Checked);
            corpProfile.IsCardAllowedExpense = Utility.ToBoolean(chkIsExpenseCard.Checked);
            corpProfile.TravelCardID = Utility.ToInteger(chkIsTravellerCard.Checked?Convert.ToInt32(ddlTravellerCard.SelectedValue):0);
            corpProfile.ExpenseCardID = Utility.ToInteger(chkIsExpenseCard.Checked ? Convert.ToInt32(ddlExpenseCard.SelectedValue) : 0);

            //Bookers Cost Centers
            List<CorpCostCenter> corpCostCenters =corpProfile.CorpCostCenters;           
            corpCostCenters = corpCostCenters == null ? new List<CorpCostCenter>(): corpCostCenters;
            if (chkCostCenters.Items != null && chkCostCenters.Items.Count > 0)
            {
                foreach (ListItem li in chkCostCenters.Items)
                {
                    CorpCostCenter corpCostCenter = null;
                    bool isExistsCostCenter = corpCostCenters.Exists(x => x.costcenterid == Convert.ToInt32(li.Value));
                    if (li.Selected || isExistsCostCenter)
                    {
                        corpCostCenter = corpCostCenters.Find(x => x.costcenterid == Convert.ToInt32(li.Value));                   
                        if (corpCostCenter == null)
                        {
                            corpCostCenter = new CorpCostCenter();
                        }
                        corpCostCenter.costcenterid = Convert.ToInt32(li.Value);
                        corpCostCenter.profileid = corpProfile.ProfileId;
                        corpCostCenter.productid = (int)CT.BookingEngine.ProductType.Flight;
                        corpCostCenter.createdby = (int)Settings.LoginInfo.UserID;
                        corpCostCenter.createdon = DateTime.Now;
                        corpCostCenter.status = li.Selected ? true : false;
                        if (!isExistsCostCenter)
                            corpCostCenters.Add(corpCostCenter);
                    }                   
                }
            }
            corpProfile.CorpCostCenters = corpCostCenters;

            DataSet ds = new DataSet();
            DataTable ProDetails = corpProfile.DtProfileDetails;
            DataColumn[] keyColumns = new DataColumn[1];
            keyColumns[0] = ProDetails.Columns["DetailId"];
            ProDetails.PrimaryKey = keyColumns;

            DataTable profileFlex = corpProfile.DtProfileFlex;

            string[] splitter = { "&&" };
            string[] spliterValue = { "||" };
            string[] spliterText = { "--" };
            //int recStatus = 0;


            if (!string.IsNullOrEmpty(hdfFFDisplayText.Value) && !string.IsNullOrEmpty(hdfFFDisplayValue.Value))
            {
                string[] displayText = hdfFFDisplayText.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                string[] displayValue = hdfFFDisplayValue.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < displayText.Length; i++)
                {
                    string[] detailsVal = displayValue[i].Split(spliterValue, StringSplitOptions.RemoveEmptyEntries);
                    string[] detailsText = displayText[i].Split(spliterText, StringSplitOptions.RemoveEmptyEntries);
                    //if (Utility.ToLong(detailsVal[0]) > 0) recStatus = 2; //UPDATE
                    //else recStatus = 1;//INSERT
                    if (Utility.ToLong(detailsVal[0]) > 0)
                    {
                        long serial = Utility.ToLong(detailsVal[0]);

                        DataRow dr = ProDetails.Rows.Find(serial);
                        dr.BeginEdit();
                        dr["DisplayValue"] = detailsVal[1];
                        dr["DisplayText"] = detailsText[0];
                        dr["Value"] = detailsVal[2];
                        dr.EndEdit();
                    }
                    else
                    {
                        long serial = Utility.ToLong(ProDetails.Compute("MAX(DetailId)", ""));
                        serial = serial + 1;
                        DataRow row = ProDetails.Rows.Add(serial, -1, "FF", detailsVal[1], detailsText[0], detailsVal[2], null, "A", Settings.LoginInfo.AgentId);
                    }
                }

            }

            if (!string.IsNullOrEmpty(hdfHMDisplayText.Value) && !string.IsNullOrEmpty(hdfHMDisplayValue.Value))
            {
                string[] displayText = hdfHMDisplayText.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                string[] displayValue = hdfHMDisplayValue.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < displayText.Length; i++)
                {
                    string[] detailsVal = displayValue[i].Split(spliterValue, StringSplitOptions.RemoveEmptyEntries);
                    string[] detailsText = displayText[i].Split(spliterText, StringSplitOptions.RemoveEmptyEntries);
                    //if (Utility.ToLong(detailsVal[0]) > 0) recStatus = 2; //UPDATE
                    //else recStatus = 1; //INSERT

                    if (Utility.ToLong(detailsVal[0]) > 0)
                    {
                        long serial = Utility.ToLong(detailsVal[0]);
                        DataRow dr = ProDetails.Rows.Find(serial);
                        dr.BeginEdit();
                        dr["DisplayValue"] = detailsVal[1];
                        dr["DisplayText"] = detailsText[0];
                        dr["Value"] = detailsVal[2];
                        dr.EndEdit();
                    }
                    else
                    {
                        long serial = Utility.ToLong(ProDetails.Compute("MAX(DetailId)", ""));
                        serial = serial + 1;
                        DataRow row = ProDetails.Rows.Add(serial, -1, "HM", detailsVal[1], detailsText[0], detailsVal[2], null, "A", Settings.LoginInfo.AgentId);
                    }
                }

            }

            if (!string.IsNullOrEmpty(hdfCCDisplayText.Value) && !string.IsNullOrEmpty(hdfCCDisplayValue.Value))
            {
                string[] displayText = hdfCCDisplayText.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                string[] displayValue = hdfCCDisplayValue.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);

                for (int i = 0; i < displayText.Length; i++)
                {
                    string[] detailsVal = displayValue[i].Split(spliterValue, StringSplitOptions.RemoveEmptyEntries);
                    string[] detailsText = displayText[i].Split(spliterText, StringSplitOptions.RemoveEmptyEntries);
                    //if (Utility.ToLong(detailsVal[0]) > 0) recStatus = 2; //UPDATE
                    //else recStatus = 1; //INSERT
                    if (Utility.ToLong(detailsVal[0]) > 0)
                    {
                        long serial = Utility.ToLong(detailsVal[0]);
                        DataRow dr = ProDetails.Rows.Find(serial);
                        dr.BeginEdit();
                        dr["DisplayValue"] = detailsVal[1];
                        dr["DisplayText"] = GenericStatic.EncryptData(detailsText[0]);
                        dr["Value"] = detailsVal[2];
                        //Added Anji by 10-oct-2019 reason CardType 
                        dr["DisplayCardType"] = detailsVal[3];
                        dr.EndEdit();
                    }
                    else
                    {
                        long serial = Utility.ToLong(ProDetails.Compute("MAX(DetailId)", ""));
                        serial = serial + 1;
                        DataRow row = ProDetails.Rows.Add(serial, -1, "CC", detailsVal[1], GenericStatic.EncryptData(detailsText[0]), detailsVal[2], detailsVal[3], "A", Settings.LoginInfo.AgentId);
                    }
                  
                }

            }
            //RoomType
            if (Convert.ToInt32(hdfRTDetailId.Value) > 0)
            {
                long serial = Convert.ToInt32(hdfRTDetailId.Value);
                DataRow dr = ProDetails.Rows.Find(serial);
                dr.BeginEdit();
                dr["DisplayValue"] = ddlRoomType.SelectedItem.Value;
                dr["DisplayText"] = ddlRoomType.SelectedItem.Text;
                dr["Value"] = txtRemarks.Text.Trim();
                dr.EndEdit();
            }
            else
            {
                long serial = Utility.ToLong(ProDetails.Compute("MAX(DetailId)", ""));
                serial = serial + 1;
                ProDetails.Rows.Add(serial, -1, "RT", ddlRoomType.SelectedItem.Value, ddlRoomType.SelectedItem.Text, txtRemarks.Text.Trim(),null, "A", Settings.LoginInfo.AgentId);
            }
            
            corpProfile.DtProfileDetails = ProDetails;
            //Flex Details saving

            if (gvFlexDetails.Rows.Count > 0)
            {
                Dictionary<int, string> DependencyValuelist = new Dictionary<int, string>();
                if (hdfdependentddlvalues.Value != null)
                {
                    string[] dependencyvalues;
                    if (hdfdependentddlvalues.Value.Contains(","))
                    {

                        dependencyvalues = hdfdependentddlvalues.Value.Split(',');
                        for (int i = 0; i < dependencyvalues.Length; i++)
                        {
                            var Value = dependencyvalues[i].Split('-');
                            DependencyValuelist.Add(Convert.ToInt32(Value[0]), Value[1]);
                        }

                    }
                    else if(!string.IsNullOrEmpty(hdfdependentddlvalues.Value))
                    {
                        dependencyvalues = hdfdependentddlvalues.Value.Split('-');
                        DependencyValuelist.Add(Convert.ToInt32(dependencyvalues[0]), dependencyvalues[1]);

                    }

                }
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");

                profileFlex = corpProfile.DtProfileFlex;

                foreach (GridViewRow gvRow in gvFlexDetails.Rows)
                {
                    HiddenField IThdfFlexId = (HiddenField)gvRow.FindControl("IThdfFlexId");
                    HiddenField IThdfFlexControl = (HiddenField)gvRow.FindControl("IThdfFlexControl");
                    HiddenField IThdfFlexMandatoryYN = (HiddenField)gvRow.FindControl("IThdfFlexMandatoryYN");
                    HiddenField IThdfFlexQry = (HiddenField)gvRow.FindControl("IThdfFlexQry");
                    HiddenField IThdfDetailID = (HiddenField)gvRow.FindControl("IThdfDetailID");
                    HiddenField IThdfFlexDataType = (HiddenField)gvRow.FindControl("IThdfFlexDataType");
                    HiddenField IThdfFlexLabel = (HiddenField)gvRow.FindControl("IThdfFlexLabel");

                    //Label ITlblLabel = (Label)gvRow.FindControl("ITlblLabel");

                    TextBox ITtxtFlexData = (TextBox)gvRow.FindControl("ITtxtFlexData");
                    DropDownList ITddlFlexData = (DropDownList)gvRow.FindControl("ITddlFlexData");
                    HiddenField IThdfFlexGDSprefix = (HiddenField)gvRow.FindControl("IThdfFlexGDSprefix");

                    DropDownList ITddlDay = (DropDownList)gvRow.FindControl("ITddlDay");
                    DropDownList ITddlMonth = (DropDownList)gvRow.FindControl("ITddlMonth");
                    DropDownList ITddlYear = (DropDownList)gvRow.FindControl("ITddlYear");

                    HiddenField IThdfFlexData = (HiddenField)gvRow.FindControl("IThdfFlexData");

                    long flexId = Utility.ToLong(IThdfFlexId.Value);
                    int agentId = 0;
                    int orderNo = 0;
                    string label = Utility.ToString(IThdfFlexLabel.Value);
                    string control = Utility.ToString(IThdfFlexControl.Value);
                    string query = Utility.ToString(IThdfFlexQry.Value);
                    string dataType = Utility.ToString(IThdfFlexDataType.Value);
                    string mandatoryYN = Utility.ToString(IThdfFlexMandatoryYN.Value);
                    string flexStatus = "A";
                    long flexDetailId = Utility.ToLong(IThdfDetailID.Value);
                    long ProfileId = 0;
                    string detail_FlexLabel = Utility.ToString(IThdfFlexLabel.Value);
                    string detail_FlexData = string.Empty;
                    string flexGDSprefix = IThdfFlexGDSprefix.Value;


                    agentId = Settings.LoginInfo.AgentId;
                    //label = detail_FlexLabel = hdnFlexLabel.Value;
                    switch (control)
                    {
                        case "T":
                            detail_FlexData = ITtxtFlexData.Text.Trim();
                            break;
                        case "D":
                            if (ITddlDay.SelectedIndex > 0 && ITddlMonth.SelectedIndex > 0 && ITddlYear.SelectedIndex > 0)
                                detail_FlexData = Convert.ToString(Convert.ToDateTime(ITddlDay.SelectedItem.Value + "/" + ITddlMonth.SelectedItem.Value + "/" + ITddlYear.SelectedItem.Value, dateFormat));
                            else
                                detail_FlexData = "";
                            break;
                        case "L":
                            if (ITddlFlexData.Items.Count != 0)
                                detail_FlexData = ITddlFlexData.SelectedItem.Value;
                            else
                            {
                                detail_FlexData = DependencyValuelist.ContainsKey(Convert.ToInt16(IThdfFlexId.Value)) ? 
                                    DependencyValuelist[Convert.ToInt16(IThdfFlexId.Value)] : string.Empty;//  DependencyValuelist.ContainsKey(i);
                            }
                          
                            break;
                    }
                    //flexDetails.CreatedBy = (int)Settings.LoginInfo.UserID;
                    //DataRow row = profileFlex.Rows.Add(flexId, agentId, orderNo, label, control, query, dataType,
                    //mandatoryYN, flexStatus, flexDetailId, ProfileId, detail_FlexLabel, detail_FlexData);
                    DataRow row = profileFlex.NewRow();
                    row["flexId"] = flexId;
                    row["flexAgentId"] = agentId;
                    row["flexOrder"] = orderNo;
                    row["flexLabel"] = label;
                    row["flexControl"] = control;
                    row["flexSqlQuery"] = query;
                    row["flexDataType"] = dataType;
                    row["flexMandatoryStatus"] = mandatoryYN;
                    row["DetailId"] = flexDetailId;
                    row["ProfileId"] = ProfileId;
                    row["Detail_FlexLabel"] = detail_FlexLabel;
                    row["Detail_FlexData"] = detail_FlexData;
                    row["flexGDSprefix"] = flexGDSprefix;
                    row["flexGDSprefix"] = "0";
                    profileFlex.Rows.Add(row);


                }
            }
            corpProfile.DtProfileFlex = profileFlex;
            corpProfile.Status = chkIsActive.Checked ? Settings.ACTIVE : Settings.DELETED;

            //toget deleted items(details)
            string deletedIds = string.Empty;
            if (!string.IsNullOrEmpty(hdfFFDeleted.Value))
            {
                string[] deletedFF = hdfFFDeleted.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < deletedFF.Length; i++)
                {
                    string[] detailsFFVal = deletedFF[i].Split(spliterValue, StringSplitOptions.RemoveEmptyEntries);
                    if (Utility.ToLong(detailsFFVal[0]) > 0)
                        deletedIds += Utility.ToString(detailsFFVal[0]) + ",";
                }
            }

            if (!string.IsNullOrEmpty(hdfHMDeleted.Value))
            {
                string[] deletedHM = hdfHMDeleted.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < deletedHM.Length; i++)
                {
                    string[] detailsHMVal = deletedHM[i].Split(spliterValue, StringSplitOptions.RemoveEmptyEntries);
                    if (Utility.ToLong(detailsHMVal[0]) > 0)
                        deletedIds += Utility.ToString(detailsHMVal[0]) + ",";
                }
            }
            if (!string.IsNullOrEmpty(hdfCCDeleted.Value))
            {
                string[] deletedCC = hdfCCDeleted.Value.Split(splitter, StringSplitOptions.RemoveEmptyEntries);
                for (int i = 0; i < deletedCC.Length; i++)
                {
                    string[] detailsCCVal = deletedCC[i].Split(spliterValue, StringSplitOptions.RemoveEmptyEntries);
                    if (Utility.ToLong(detailsCCVal[0]) > 0)
                        deletedIds += Utility.ToString(detailsCCVal[0]) + ",";
                }
            }

            corpProfile.DeletedDetails = deletedIds;
            string policyIDs = string.Empty;
            string policyStatus = string.Empty;
            for (int j = 0; j < chkPolicies.Items.Count; j++)
            {

                policyIDs += chkPolicies.Items[j].Value + ",";
                policyStatus += chkPolicies.Items[j].Selected ? "A" + "," : "D" + ",";

            }
            corpProfile.PolicyIds = policyIDs;
            corpProfile.PolicyStatus = policyStatus;


            //DataTable dtRoleUpdate = corpProfile.DtUserRoles.Clone();

            //foreach (GridViewRow gvRow in gvUserRoleDetails.Rows)
            //{

            //    HiddenField hdfRoleId = (HiddenField)gvRow.FindControl("hdfRoleId");
            //    HiddenField IThdfURDId = (HiddenField)gvRow.FindControl("IThdfURDId");
            //    HiddenField IThdfUrdRoleId = (HiddenField)gvRow.FindControl("IThdfUrdRoleId");
            //    HiddenField hdfExistStatus = (HiddenField)gvRow.FindControl("hdfExistStatus");
            //    Label ITlblRolName = (Label)gvRow.FindControl("ITlblRolName");
            //    CheckBox ITchkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");


            //    if (ITchkSelect.Checked && Utility.ToInteger(IThdfURDId.Value) > 0)
            //    {
            //        if (hdfExistStatus.Value == "D")
            //        {
            //            DataRow dr1 = dtRoleUpdate.Rows.Add(0, Utility.ToLong(hdfRoleId.Value), ITlblRolName.Text.Trim(), Utility.ToInteger(IThdfURDId.Value), Utility.ToInteger(IThdfUrdRoleId.Value), "A");
            //        }
            //    }
            //    else if (ITchkSelect.Checked && Utility.ToInteger(IThdfURDId.Value) <= 0)
            //    {

            //        DataRow dr1 = dtRoleUpdate.Rows.Add(0, Utility.ToLong(hdfRoleId.Value), ITlblRolName.Text.Trim(), Utility.ToInteger(IThdfURDId.Value), Utility.ToInteger(IThdfUrdRoleId.Value), "A");
            //    }
            //    else if (!ITchkSelect.Checked && Utility.ToInteger(IThdfURDId.Value) > 0)
            //    {
            //        DataRow dr1 = dtRoleUpdate.Rows.Add(0, Utility.ToLong(hdfRoleId.Value), ITlblRolName.Text.Trim(), Utility.ToInteger(IThdfURDId.Value), Utility.ToInteger(IThdfUrdRoleId.Value), "D");
            //    }
            //}
            //corpProfile.DtUserRoles = dtRoleUpdate;


            //Lokesh-2June,2017 : Add or Update ticket /expense approvers for particular profile
            //E-Expense Approver
            //T-Ticket Approver

            GetApproversList(corpProfile);
            //Added by lokesh on 27-Nov-2017.
            corpProfile.DateOfJoining = Convert.ToDateTime(dcJoiningDate.Value, dateFormat1);
            GetVisaDetailsList(corpProfile);
            GetContactDetailsList(corpProfile);//phani
            GetGDSSettingsList(corpProfile);
            corpProfile.Save();
            string profileId = Utility.ToString(corpProfile.ProfileIdRet);
            Session["CorpProfileId"] = profileId;
            // if (!string.IsNullOrEmpty(profileImage.FileExtension))
            if (!string.IsNullOrEmpty(strProfileImagePath))
            {
                //string[] Image1 = hdfAgentImg.Value.Split('.');
                //hdfAgentImg.Value = hdfAgentImg.Value;
              //  string imgFullPath = hdfProfileImage.Value+ profileImage.FileExtension;
                RenameImage(profileId, strProfileImagePath);
            }
            string name = txtSurname.Text.Trim() + " " + txtname.Text.Trim();
            string modeVal = hdfMode.Value;
            Clear();
            string message = string.Empty;
            if (modeVal == "0")
                message = "Corporate Profile for " + name + " is created !";
            else if (modeVal == "1")
                message = "Corporate Profile for " + name + " is updated !";

            Utility.Alert(this.Page, message);
            //lblSuccessMsg.Visible = true;
            // lblSuccessMsg.Text = Formatter.ToMessage("Locations ", locationName, (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            // lblErrorMsg.Text = string.Format("User '{0}' is saved successfully !", user.LoginName);
        }
        catch
        {
            throw;
        }

    }

    private CorporateProfile CurrentObject
    {
        get
        {
            return (CorporateProfile)Session[CORP_PROFILE_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(CORP_PROFILE_SESSION);
            }
            else
            {
                Session[CORP_PROFILE_SESSION] = value;
            }

        }

    }

    /// <summary>
    /// binding Flex Details
    /// </summary>
    /// 

    void CreateFlexFields(DataTable dtFlex)
    {
        try
        {
            if (dtFlex != null)
            {
                hdnFlexCount.Value = dtFlex.Rows.Count.ToString();
                int i = 0;
                foreach (DataRow row in dtFlex.Rows)
                {
                    //HtmlGenericControl divlblFlex1 = new HtmlGenericControl("Div");
                    HtmlTableRow tr = new HtmlTableRow();
                    HtmlTableCell tc = new HtmlTableCell();
                    //divlblFlex1.Attributes.Add("class", "col-md-3");

                    HiddenField hdnFlexId = new HiddenField();
                    hdnFlexId.ID = "hdnFlexId" + i;
                    hdnFlexId.Value = row["flexId"].ToString();
                    //divlblFlex1.Controls.Add(hdnFlexId);
                    tc.Controls.Add(hdnFlexId);

                    HiddenField hdnFlexControl = new HiddenField();
                    hdnFlexControl.ID = "hdnFlexControl" + i;
                    hdnFlexControl.Value = Convert.ToString(row["flexControl"]);
                    // divlblFlex1.Controls.Add(hdnFlexControl);
                    tc.Controls.Add(hdnFlexControl);

                    HiddenField hdnFlexMandatory = new HiddenField();

                    hdnFlexMandatory.ID = "hdnFlexMandatory" + i;
                    hdnFlexMandatory.Value = Convert.ToString(row["flexMandatoryStatus"]);
                    //divlblFlex1.Controls.Add(hdnFlexMandatory);
                    tc.Controls.Add(hdnFlexMandatory);

                    HiddenField hdnFlexLabel = new HiddenField();
                    hdnFlexLabel.ID = "hdnFlexLabel" + i;
                    hdnFlexLabel.Value = Convert.ToString(row["flexLabel"]);
                    //divlblFlex1.Controls.Add(hdnFlexLabel);
                    tc.Controls.Add(hdnFlexLabel);

                    Label lblFlex1 = new Label();
                    lblFlex1.ID = "lblFlex" + i;
                    if (Convert.ToString(row["flexMandatoryStatus"]) == "N")
                    {
                        //lblFlex1.Text = "<strong>" + Convert.ToString(row["flexLabel"]) + ":</strong>";
                        lblFlex1.Text = Convert.ToString(row["flexLabel"]);
                    }
                    else
                    {
                        lblFlex1.Text = Convert.ToString(row["flexLabel"]) + ":<span class='red_span'>*</span>";
                    }

                    //divlblFlex1.Controls.Add(lblFlex1);
                    tc.Controls.Add(lblFlex1);

                    HtmlGenericControl divFlexControl = new HtmlGenericControl("Div");
                    divFlexControl.Attributes.Add("class", "col-md-3");

                    Label lblError = new Label();
                    lblError.ID = "lblError" + i;
                    lblError.Font.Bold = true;
                    lblError.CssClass = "red_span";

                    HtmlTableCell tableCell1 = new HtmlTableCell();
                    switch (row["flexControl"].ToString())
                    {
                        case "T": //TextBox
                            TextBox txtFlex = new TextBox();
                            txtFlex.ID = "txtFlex" + i;
                            txtFlex.CssClass = "form-control";
                            if (row["flexDataType"].ToString() == "N") //N means Numeric
                            {
                                txtFlex.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'1');");
                            }
                            if (row["Detail_FlexData"] != DBNull.Value)
                            {
                                txtFlex.Text = Convert.ToString(row["Detail_FlexData"]);
                            }
                            //divFlexControl.Controls.Add(txtFlex);
                            //divFlexControl.Controls.Add(lblError);
                            tc.Controls.Add(txtFlex);
                            tc.Controls.Add(lblError);
                            break;
                        case "D"://Date

                            DropDownList ddlDay = new DropDownList();
                            ddlDay.ID = "ddlDay" + i;
                            ddlDay.Width = new Unit(30, UnitType.Percentage);
                            ddlDay.CssClass = "form-control pull-left ";
                            BindDates(ddlDay);

                            DropDownList ddlMonth = new DropDownList();
                            ddlMonth.ID = "ddlMonth" + i;
                            ddlMonth.Width = new Unit(30, UnitType.Percentage);
                            ddlMonth.CssClass = "form-control pull-left ";
                            BindMonths(ddlMonth);

                            DropDownList ddlYear = new DropDownList();
                            ddlYear.ID = "ddlYear" + i;
                            ddlYear.Width = new Unit(40, UnitType.Percentage);
                            ddlYear.CssClass = "form-control pull-left ";
                            BindYears(ddlYear);

                            DateTime date;
                            if (row["Detail_FlexData"] != DBNull.Value)
                            {
                                try
                                {
                                    date = Convert.ToDateTime(row["Detail_FlexData"]);
                                    ddlDay.SelectedValue = date.Day.ToString();
                                    ddlMonth.SelectedValue = date.Month.ToString();
                                    ddlYear.SelectedValue = date.Year.ToString();
                                }
                                catch { }
                            }

                            divFlexControl.Controls.Add(ddlDay);
                            divFlexControl.Controls.Add(ddlMonth);
                            divFlexControl.Controls.Add(ddlYear);
                            divFlexControl.Controls.Add(lblError);
                            break;
                        case "L"://DropDown
                            DropDownList ddlFlex = new DropDownList();
                            ddlFlex.ID = "ddlFlex" + i;
                            ddlFlex.CssClass = "form-control";
                            DataTable dt = CorporateProfile.FillDropDown(Convert.ToString(row["flexSqlQuery"]));

                            if (dt != null && dt.Rows.Count > 0)
                            {
                                ddlFlex.DataSource = dt;
                                ddlFlex.DataTextField = dt.Columns[1].ColumnName;
                                ddlFlex.DataValueField = dt.Columns[0].ColumnName;
                                ddlFlex.DataBind();
                            }
                            ddlFlex.Items.Insert(0, new ListItem("-- Select--", "-1"));
                            if (row["Detail_FlexData"] != DBNull.Value)
                            {
                                try
                                {
                                    ddlFlex.SelectedValue = Convert.ToString(row["Detail_FlexData"]);
                                }
                                catch { }
                            }
                            divFlexControl.Controls.Add(ddlFlex);
                            divFlexControl.Controls.Add(lblError);
                            break;
                    }
                    HtmlGenericControl divClear = new HtmlGenericControl("Div");
                    divClear.Attributes.Add("class", "clearfix");
                    tc.Controls.Add(divFlexControl);
                    tr.Cells.Add(tc);
                    tblFlexFields.Controls.Add(tr);
                    //tblFlexFields.Controls.Add(divlblFlex1); //Flex Label Binding
                    // tblFlexFields.Controls.Add(divFlexControl);//Flex Control Binding
                    //tblFlexFields.Controls.Add(divClear);
                    i++;
                }
            }
        }
        catch { }
    }

    //void CreateFlexFields(DataTable dtFlex)
    //{
    //    try
    //    {
    //        if (dtFlex != null)
    //        {
    //            hdnFlexCount.Value = dtFlex.Rows.Count.ToString();
    //            int i = 0;
    //            foreach (DataRow row in dtFlex.Rows)
    //            {
    //                //HtmlGenericControl divlblFlex1 = new HtmlGenericControl("Div");
    //                HtmlTableRow tr = new HtmlTableRow();
    //                HtmlTableCell tc = new HtmlTableCell();
    //                //divlblFlex1.Attributes.Add("class", "col-md-3");

    //                HiddenField hdnFlexId = new HiddenField();                    
    //                hdnFlexId.ID = "hdnFlexId" + i;
    //                hdnFlexId.Value = row["flexId"].ToString();
    //                //divlblFlex1.Controls.Add(hdnFlexId);
    //                tc.CO

    //                HiddenField hdnFlexControl = new HiddenField();
    //                hdnFlexControl.ID = "hdnFlexControl" + i;
    //                hdnFlexControl.Value = Convert.ToString(row["flexControl"]);
    //                divlblFlex1.Controls.Add(hdnFlexControl);

    //                HiddenField hdnFlexMandatory = new HiddenField();

    //                hdnFlexMandatory.ID = "hdnFlexMandatory" + i;
    //                hdnFlexMandatory.Value = Convert.ToString(row["flexMandatoryStatus"]);
    //                divlblFlex1.Controls.Add(hdnFlexMandatory);

    //                HiddenField hdnFlexLabel = new HiddenField();
    //                hdnFlexLabel.ID = "hdnFlexLabel" + i;
    //                hdnFlexLabel.Value = Convert.ToString(row["flexLabel"]);
    //                divlblFlex1.Controls.Add(hdnFlexLabel);

    //                Label lblFlex1 = new Label();
    //                lblFlex1.ID = "lblFlex" + i;
    //                if (Convert.ToString(row["flexMandatoryStatus"]) == "N")
    //                {
    //                    //lblFlex1.Text = "<strong>" + Convert.ToString(row["flexLabel"]) + ":</strong>";
    //                    lblFlex1.Text =  Convert.ToString(row["flexLabel"]);
    //                }
    //                else
    //                {
    //                    lblFlex1.Text = Convert.ToString(row["flexLabel"]) + ":<span class='red_span'>*</span>";
    //                }

    //                divlblFlex1.Controls.Add(lblFlex1);

    //                HtmlGenericControl divFlexControl = new HtmlGenericControl("Div");
    //                divFlexControl.Attributes.Add("class", "col-md-3");

    //                Label lblError = new Label();
    //                lblError.ID = "lblError" + i;
    //                lblError.Font.Bold = true;
    //                lblError.CssClass = "red_span";

    //                HtmlTableCell tableCell1 = new HtmlTableCell();
    //                switch (row["flexControl"].ToString())
    //                {
    //                    case "T": //TextBox
    //                        TextBox txtFlex = new TextBox();
    //                        txtFlex.ID = "txtFlex" + i;
    //                        txtFlex.CssClass = "form-control";
    //                        if (row["flexDataType"].ToString() == "N") //N means Numeric
    //                        {
    //                            txtFlex.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'1');");
    //                        }
    //                        if (row["Detail_FlexData"] != DBNull.Value)
    //                        {
    //                            txtFlex.Text = Convert.ToString(row["Detail_FlexData"]);
    //                        }
    //                        divFlexControl.Controls.Add(txtFlex);
    //                        divFlexControl.Controls.Add(lblError);
    //                        break;
    //                    case "D"://Date

    //                        DropDownList ddlDay = new DropDownList();
    //                        ddlDay.ID = "ddlDay" + i;
    //                        ddlDay.Width = new Unit(30, UnitType.Percentage);
    //                        ddlDay.CssClass = "form-control pull-left ";
    //                        BindDates(ddlDay);

    //                        DropDownList ddlMonth = new DropDownList();
    //                        ddlMonth.ID = "ddlMonth" + i;
    //                        ddlMonth.Width = new Unit(30, UnitType.Percentage);
    //                        ddlMonth.CssClass = "form-control pull-left ";
    //                        BindMonths(ddlMonth);

    //                        DropDownList ddlYear = new DropDownList();
    //                        ddlYear.ID = "ddlYear" + i;
    //                        ddlYear.Width = new Unit(40, UnitType.Percentage);
    //                        ddlYear.CssClass = "form-control pull-left ";
    //                        BindYears(ddlYear);

    //                        DateTime date;
    //                        if (row["Detail_FlexData"] != DBNull.Value)
    //                        {
    //                            try
    //                            {
    //                                date = Convert.ToDateTime(row["Detail_FlexData"]);
    //                                ddlDay.SelectedValue = date.Day.ToString();
    //                                ddlMonth.SelectedValue = date.Month.ToString();
    //                                ddlYear.SelectedValue = date.Year.ToString();
    //                            }
    //                            catch { }
    //                        }

    //                        divFlexControl.Controls.Add(ddlDay);
    //                        divFlexControl.Controls.Add(ddlMonth);
    //                        divFlexControl.Controls.Add(ddlYear);
    //                        divFlexControl.Controls.Add(lblError);
    //                        break;
    //                    case "L"://DropDown
    //                        DropDownList ddlFlex = new DropDownList();
    //                        ddlFlex.ID = "ddlFlex" + i;
    //                        ddlFlex.CssClass = "form-control";
    //                        DataTable dt = CorporateProfile.FillDropDown(Convert.ToString(row["flexSqlQuery"]));

    //                        if (dt != null && dt.Rows.Count > 0)
    //                        {
    //                            ddlFlex.DataSource = dt;
    //                            ddlFlex.DataTextField = dt.Columns[1].ColumnName;
    //                            ddlFlex.DataValueField = dt.Columns[0].ColumnName;
    //                            ddlFlex.DataBind();
    //                        }
    //                        ddlFlex.Items.Insert(0, new ListItem("-- Select--", "-1"));
    //                        if (row["Detail_FlexData"] != DBNull.Value)
    //                        {
    //                            try
    //                            {
    //                                ddlFlex.SelectedValue = Convert.ToString(row["Detail_FlexData"]);
    //                            }
    //                            catch { }
    //                        }
    //                        divFlexControl.Controls.Add(ddlFlex);
    //                        divFlexControl.Controls.Add(lblError);
    //                        break;
    //                }
    //                HtmlGenericControl divClear = new HtmlGenericControl("Div");
    //                divClear.Attributes.Add("class", "clearfix");
    //                tblFlexFields.Controls.Add(divlblFlex1); //Flex Label Binding
    //                tblFlexFields.Controls.Add(divFlexControl);//Flex Control Binding
    //                tblFlexFields.Controls.Add(divClear);
    //                i++;
    //            }
    //        }
    //    }
    //    catch { }
    //}

    void BindDates(DropDownList ddlDay)
    {
        ddlDay.Items.Add(new ListItem("Day", "-1"));
        for (int i = 1; i <= 31; i++)
        {
            ddlDay.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }

    void BindMonths(DropDownList ddlMonth)
    {
        ddlMonth.Items.Add(new ListItem("Month", "-1"));
        string[] months = new string[] { "Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec" };
        for (int i = 1; i <= months.Length; i++)
        {
            ddlMonth.Items.Add(new ListItem(months[i - 1], i.ToString()));
        }
    }

    void BindYears(DropDownList ddlYear)
    {
        ddlYear.Items.Add(new ListItem("Year", "-1"));
        for (int i = 1930; i <= DateTime.Now.Year; i++)
        {
            ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
        }
    }


    protected void gvFlexDetails_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {

            if (e.Row.RowType == DataControlRowType.DataRow)
            {

                HiddenField IThdfFlexId = (HiddenField)e.Row.FindControl("IThdfFlexId");
                HiddenField IThdfFlexControl = (HiddenField)e.Row.FindControl("IThdfFlexControl");
                HiddenField IThdfFlexMandatoryYN = (HiddenField)e.Row.FindControl("IThdfFlexMandatoryYN");
                HiddenField IThdfFlexQry = (HiddenField)e.Row.FindControl("IThdfFlexQry");
                HiddenField IThdfDetailID = (HiddenField)e.Row.FindControl("IThdfDetailID");
                HiddenField IThdfFlexDataType = (HiddenField)e.Row.FindControl("IThdfFlexDataType");
                HiddenField IThdfFlexValid = (HiddenField)e.Row.FindControl("IThdfFlexvalid");
                HiddenField IThdfFlexGDSprefix = (HiddenField)e.Row.FindControl("IThdfFlexGDSprefix");
                Label ITlblLabel = (Label)e.Row.FindControl("ITlblLabel");

                TextBox ITtxtFlexData = (TextBox)e.Row.FindControl("ITtxtFlexData");
                DropDownList ITddlFlexData = (DropDownList)e.Row.FindControl("ITddlFlexData");

                DropDownList ITddlDay = (DropDownList)e.Row.FindControl("ITddlDay");
                DropDownList ITddlMonth = (DropDownList)e.Row.FindControl("ITddlMonth");
                DropDownList ITddlYear = (DropDownList)e.Row.FindControl("ITddlYear");

                HiddenField IThdfFlexData = (HiddenField)e.Row.FindControl("IThdfFlexData");
                //Table ITtblFlexDate = (Table)e.Row.FindControl("ITtblFlexDate");

                System.Web.UI.HtmlControls.HtmlTable ITtblFlexDate = (System.Web.UI.HtmlControls.HtmlTable)e.Row.FindControl("ITtblFlexDate");

                if (IThdfFlexMandatoryYN.Value == "Y")
                    ITlblLabel.Text = ITlblLabel.Text + ":<span class='red_span'>*</span>";


                //BindTax(ITddlTaxCode);
                if (Utility.ToLong(IThdfFlexId.Value) > 0)
                {
                    switch (IThdfFlexControl.Value)
                    {
                        case "T": //TextBox

                            ITddlFlexData.Visible = false;
                            ITtblFlexDate.Visible = false;
                            ITtxtFlexData.CssClass = "form-control";
                            if (IThdfFlexDataType.Value == "N") //N means Numeric
                            {
                                ITtxtFlexData.Attributes.Add("onkeypress", "return restrictNumeric(this.id,'1');");
                            }
                            if (!string.IsNullOrEmpty(IThdfFlexData.Value))
                            {
                                ITtxtFlexData.Text = Convert.ToString(IThdfFlexData.Value);
                            }
                            break;
                        case "D"://Date
                            ITtxtFlexData.Visible = false;
                            ITddlFlexData.Visible = false;

                            //ITddlDay.Width = new Unit(30, UnitType.Percentage);
                            ITddlDay.CssClass = "form-control pull-left ";
                            BindDates(ITddlDay);

                            //ITddlMonth.Width = new Unit(30, UnitType.Percentage);
                            ITddlMonth.CssClass = "form-control pull-left ";
                            BindMonths(ITddlMonth);

                            //ITddlYear.Width = new Unit(40, UnitType.Percentage);
                            ITddlYear.CssClass = "form-control pull-left ";
                            BindYears(ITddlYear);

                            DateTime date;
                            if (!string.IsNullOrEmpty(IThdfFlexData.Value))
                            {
                                try
                                {
                                    date = Convert.ToDateTime(IThdfFlexData.Value);
                                    ITddlDay.SelectedValue = date.Day.ToString();
                                    ITddlMonth.SelectedValue = date.Month.ToString();
                                    ITddlYear.SelectedValue = date.Year.ToString();
                                }
                                catch { }
                            }
                            break;
                        case "L"://DropDown
                            ITtxtFlexData.Visible = false;
                            ITtblFlexDate.Visible = false;

                            ITddlFlexData.CssClass = "form-control";
                            if (!string.IsNullOrEmpty(IThdfFlexValid.Value) && IThdfFlexValid.Value!="0")
                            {
                                GridViewRow Dependentrow= gvFlexDetails.Rows[0]; 
                                foreach (GridViewRow row in gvFlexDetails.Rows)
                                {
                                   HiddenField FlexID= (HiddenField) row.FindControl("IThdfFlexId");
                                    if (FlexID.Value == IThdfFlexValid.Value)
                                    {
                                        Dependentrow = row;
                                    }
                                       
                                   
                                }
                                DropDownList dependentddlFlexData = (DropDownList) Dependentrow.FindControl("ITddlFlexData");
                                dependentddlFlexData.Attributes.Add("onchange", "BindDependencyddl('"+ dependentddlFlexData.ClientID+ "','"+ ITddlFlexData .ClientID + "','"+ IThdfFlexQry .Value+ "');");
                                ITddlFlexData.Attributes.Add("onchange", "ValueDependencyddl('" + ITddlFlexData.ClientID + "',"+ IThdfFlexId .Value+ ");");

                            }
                            else
                            {
                                DataTable dt = new DataTable();
                                if (!string.IsNullOrEmpty(IThdfFlexQry.Value))
                                    dt = CorporateProfile.FillDropDown(Convert.ToString(IThdfFlexQry.Value));

                                if (dt != null && dt.Rows.Count > 0)
                                {
                                    ITddlFlexData.DataSource = dt;
                                    ITddlFlexData.DataTextField = dt.Columns[1].ColumnName;
                                    ITddlFlexData.DataValueField = dt.Columns[0].ColumnName;
                                    ITddlFlexData.DataBind();
                                }
                                ITddlFlexData.Items.Insert(0, new ListItem("-- Select--", "-1"));
                                if (!string.IsNullOrEmpty(IThdfFlexData.Value))
                                {
                                    try
                                    {
                                        ITddlFlexData.SelectedValue = Convert.ToString(IThdfFlexData.Value);
                                    }
                                    catch { }

                                }
                            }
                            
                            break;
                    }

                }
            }
        }
        catch { throw; }
    }
    [WebMethod]
    [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
    public static Dictionary<string, string> LoadDependencyddl(string Query)
    {
        Dictionary<string, string> list = new Dictionary<string, string>();
        DataTable dtDependency = CorporateProfile.FillDropDown(Query);
        if (dtDependency != null)
        {
            foreach (DataRow data in dtDependency.Rows)
            {

                list.Add(Convert.ToString(data[0]), Convert.ToString(data[1]));
            }
        }
        return list;
    }
    private void BindFlexGrid(DataTable dtFlex)
    {
        try
        {

            CommonGrid grid = new CommonGrid();
            //ViewState["gvTax"] = dtTax;
            grid.BindGrid(gvFlexDetails, dtFlex);

        }
        catch(Exception ex) { Audit.Add(EventType.Exception, Severity.High, 1, "Corporate Profile page BindFlexGrid " + ex.ToString(), "0"); }
    }

    private void BindPolicies(DataTable dtPolicy)
    {
        try
        {
            chkPolicies.DataSource = dtPolicy;
            chkPolicies.DataTextField = "PolicyName";
            chkPolicies.DataValueField = "PolicyId";
            chkPolicies.DataBind();


        }
        catch { }
    }

    //private void BindUserRoles(DataTable dtRole)
    //{
    //    try
    //    {

    //        CommonGrid grid = new CommonGrid();
    //        //ViewState["gvTax"] = dtTax;
    //        grid.BindGrid(gvUserRoleDetails, dtRole);

    //    }
    //    catch { }
    //}
    //protected void HTchkSelectAll_CheckedChanged(object sender, EventArgs e)
    //{
    //    try
    //    {
    //        GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
    //        CheckBox chkSelectAll = (CheckBox)gvHdrRow.FindControl("HTchkSelectAll");


    //        foreach (GridViewRow gvRow in gvUserRoleDetails.Rows)
    //        {
    //            CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
    //            chkSelect.Checked = chkSelectAll.Checked;
    //            long funcId = Utility.ToLong(gvUserRoleDetails.DataKeys[gvRow.RowIndex].Value);
    //            // SetRowStatus(funcId, chkSelect.Checked);

    //        }
    //    }
    //    catch { throw; }
    //}

    private void Edit(long id, int agentId)
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            CorporateProfile editCorp = new CorporateProfile(id, agentId);
            CurrentObject = editCorp;
            string rootFolder = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["ProfileImage"]);
            profileImage.SavePath = rootFolder;
            DataTable dtDetails = editCorp.DtProfileDetails;
            DataTable dtFlex = editCorp.DtProfileFlex;
            DataTable dtPolicies = editCorp.DtProfilePolicy;
            //DataTable dtRoles = editCorp.DtUserRoles;
            BindFlexGrid(dtFlex);
            BindPolicies(dtPolicies);
            //BindUserRoles(dtRoles);
            foreach (DataRow dr in dtPolicies.Rows)
            {
                if (Utility.ToString(dr["checked"]) == "true")
                {
                    for (int i = 0; i < chkPolicies.Items.Count; i++)
                    {
                        if (Utility.ToInteger(chkPolicies.Items[i].Value) == Utility.ToInteger(dr["PolicyId"]))
                        {
                            chkPolicies.Items[i].Selected = true;
                        }
                    }
                }
            }
            hdfMode.Value = "1";  //edit mode
            ddlAgent.SelectedValue = Utility.ToString(agentId);

            BindLocation();
            BindGrade();
            BindSetupValues(ddlDesignation, "DG");
            BindSetupValues(ddlDivision, "DV");
            BindSetupValues(ddlCostCentre, "CS");

            SetddlValue(ddlProfileType, editCorp.ProfileType); // ddlProfileType.SelectedValue = Utility.ToString(editCorp.ProfileType);
            SetddlValue(ddlTitle, editCorp.Title); //ddlTitle.SelectedValue = Utility.ToString(editCorp.Title);
            txtSurname.Text = Utility.ToString(editCorp.SurName);
            txtname.Text = Utility.ToString(editCorp.Name);
            SetddlValue(ddlDesignation, editCorp.Designation); //ddlDesignation.SelectedValue = Utility.ToString(editCorp.Designation);
            //         corpProfile.ImagePath = Utility.ToString("");
            //corpProfile.ImageType = Utility.ToString("");

            if (editCorp.ImagePath != string.Empty)
            {
                profileImage.DefaultImageUrl = "~/images/common/Preview.png";
                //lnkView.Visible = true;
                //lnkView.Text = "View Image";
                // hdfProfileImage.Value = rootFolder + "" + editCorp.ImagePath;
                profileImage.FileExtension = null;
                hdfImgPath.Value = rootFolder + "" + editCorp.ImagePath;
                string imgPath = Server.MapPath("~/" + rootFolder + "/") + editCorp.ImagePath;
                Utility.StartupScript(this.Page, "ShowProfilePic();", "ImgShow");
                //profileImage.FileExtension = null;
            }
            txtEmpId.Text = Utility.ToString(editCorp.EmployeeId);
            SetddlValue(ddlDivision, Convert.ToString(editCorp.Division)); //ddlDivision.SelectedValue = Utility.ToString(editCorp.Division);
            SetddlValue(ddlCostCentre, Convert.ToString(editCorp.CostCentre)); //ddlCostCentre.SelectedValue = Utility.ToString(editCorp.CostCentre);
            SetddlValue(ddlGrade, editCorp.Grade); //ddlGrade.SelectedValue = Utility.ToString(editCorp.Grade);
            string[] splitter = { "-" };
            if (!string.IsNullOrEmpty(editCorp.Telephone))
            {
                string[] telephone = editCorp.Telephone.Split(splitter, 2, StringSplitOptions.RemoveEmptyEntries);
                if (telephone.Length == 2)
                {
                    txtTelPhone.Text = telephone[1];
                    txtPhoneCountryCode.Text = telephone[0];
                }
                else txtTelPhone.Text = editCorp.Telephone;
            }

            if (!string.IsNullOrEmpty(editCorp.Mobilephone))
            {
                string[] mobilePhone = editCorp.Mobilephone.Split(splitter, 2, StringSplitOptions.RemoveEmptyEntries);
                if (mobilePhone.Length == 2)
                {
                    txtMobileNo.Text = mobilePhone[1];
                    txtMobileCoutryCode.Text = mobilePhone[0];
                }
                else txtMobileNo.Text = editCorp.Mobilephone;
            }
            //txtTelPhone.Text = Utility.ToString(editCorp.Telephone);
            //txtMobileNo.Text = Utility.ToString(editCorp.Mobilephone);
            txtFax.Text = Utility.ToString(editCorp.Fax);
            txtEmail.Text = Utility.ToString(editCorp.Email);
            txtAddress1.Text = Utility.ToString(editCorp.Address1);
            txtAddress2.Text = Utility.ToString(editCorp.Address2);
            SetddlValue(ddlNationality, editCorp.NationalityCode); //ddlNationality.SelectedValue = Utility.ToString(editCorp.NationalityCode);
            txtPassportNo.Text = Utility.ToString(editCorp.PassportNo);
            txtDOI.Text = editCorp.DateOfIssue.ToString("dd/MM/yyyy");
            try
            {
                txtDOE.Text = editCorp.DateOfExpiry.ToString("dd/MM/yyyy");  //Utility.ToString(editCorp.DateOfExpiry);
            }
            catch { }
            try { ddlcountryOfIssue.SelectedValue = Utility.ToString(editCorp.PassportCOI); } catch { }
            txtDOB.Text = editCorp.DateOfBirth.ToString("dd/MM/yyyy"); // Utility.ToString(editCorp.DateOfBirth);
            //ddlPlaceOfBirth.SelectedValue = Utility.ToString(editCorp.PlaceOfBirth);
            SetddlValue(ddlPlaceOfBirth, editCorp.PlaceOfBirth);
            //rdbMale.Checked = true;
            if (!string.IsNullOrEmpty(editCorp.Gender))
            {
                ddlGender.SelectedValue = Utility.ToString(editCorp.Gender);
            }
            else
            {
                ddlGender.SelectedValue = string.Empty;
            }
            txtSeatPref.Text = Utility.ToString(editCorp.SeatPreference);
            txtMealRequest.Text = Utility.ToString(editCorp.MealRequest);
            txtRemarks.Text = Utility.ToString(editCorp.HotelRemarks);
            txtOtherRemarks.Text = Utility.ToString(editCorp.OtherRemarks);
            string displayText = string.Empty;
            string displayValue = string.Empty;
            foreach (DataRow dr in dtDetails.Rows)
            {
                displayText = (Utility.ToString(dr["DetailType"]) == "CC" ? GenericStatic.DecryptData(Utility.ToString(dr["DisplayText"])) :
                    Utility.ToString(dr["DisplayText"])) + "--" + Utility.ToString(dr["Value"]) + "--" + Utility.ToString(dr["DisplayCardType"]);
                displayValue = Utility.ToString(dr["DetailId"]) + "||" + Utility.ToString(dr["DisplayValue"]) + "||" + Utility.ToString(dr["Value"]);

                if (Utility.ToString(dr["DetailType"]) == "FF")
                    lstFF.Items.Add(new ListItem(displayText, displayValue));
                else if (Utility.ToString(dr["DetailType"]) == "HM")
                    lstHM.Items.Add(new ListItem(displayText, displayValue));
                else if (Utility.ToString(dr["DetailType"]) == "CC")
                    lstCC.Items.Add(new ListItem(displayText, displayValue));
                else if (Utility.ToString(dr["DetailType"]) == "RT")
                {
                    hdfRTDetailId.Value = Utility.ToString(dr["DetailId"]);
                    SetddlValue(ddlRoomType, Utility.ToString(dr["DisplayValue"])); //ddlRoomType.SelectedValue = Utility.ToString(dr["DisplayValue"]);
                    txtRemarks.Text = Utility.ToString(dr["Value"]);
                }
            }
            txtLoginName.Enabled = txtPassword.Enabled = txtConfirmPassword.Enabled = false;
            SetddlValue(ddlLocation, editCorp.LocationId > 0 ? Utility.ToString(editCorp.LocationId) : ddlLocation.SelectedValue); //ddlLocation.SelectedValue = editCorp.LocationId > 0 ? Utility.ToString(editCorp.LocationId) : ddlLocation.SelectedValue;
            chkIsActive.Checked = editCorp.Status == "A";
            txtLoginName.Text = txtPassword.Text = txtConfirmPassword.Text = string.Empty;
            //ddlMemberType.SelectedValue = editCorp.MemberType.ToString();
            dcJoiningDate.Value = editCorp.DateOfJoining;

            //Somasekhar -30/03/2018 : for Mertial status 
            ddlMartialStatus.SelectedValue = Utility.ToString(editCorp.MartialStatus);
            //PHANI
            txtMiddleName.Text = Utility.ToString(editCorp.MiddleName);
            txtGSTNumber.Text = Utility.ToString(editCorp.GstNumber);
            txtGSTName.Text = Utility.ToString(editCorp.GstName);
            txtGSTEmail.Text = Utility.ToString(editCorp.GstEmail);
            txtGSTPhone.Text = Utility.ToString(editCorp.GstPhone);
            txtGSTAddress.Text = Utility.ToString(editCorp.GstAddress);
            //dcTerminationdate.Value = editCorp.DateOfTermination;
            txtGDSProfilePNR.Text = Utility.ToString(editCorp.GdsProfilePNR);
            SetddlValue(ddlResidency, Utility.ToString(editCorp.Residency)); //ddlResidency.SelectedValue = Utility.ToString(editCorp.Residency);
            txtExecAssistance.Text = Utility.ToString(editCorp.ExecAssistance);
            SetddlValue(ddlDeligateSupervisor, Utility.ToString(editCorp.DeligateSupervisor));
            SetddlValue(ddlDomestic, Utility.ToString(editCorp.DomesticEligibility));  //ddlDomestic.SelectedValue =Utility.ToString( editCorp.DomesticEligibility);
            SetddlValue(ddlinternational, Utility.ToString(editCorp.IntlEligibility)); //ddlinternational.SelectedValue = Utility.ToString(editCorp.IntlEligibility);
            txtbatch.Text = Utility.ToString(editCorp.Batch);
            chkAffidavit.Checked = editCorp.Affidivit;
            chkIsTravellerCard.Checked = editCorp.IsCardAllowedTravel;
            chkIsExpenseCard.Checked = editCorp.IsCardAllowedExpense;
            ddlExpenseCard.SelectedValue = Utility.ToString(editCorp.ExpenseCardID);
            ddlTravellerCard.SelectedValue = Utility.ToString(editCorp.TravelCardID);
            ddlApproverType.SelectedValue = Utility.ToString(editCorp.ApproverType);
            if (editCorp.CorpCostCenters != null && editCorp.CorpCostCenters.Count > 0)
            {
                if (chkCostCenters.Items != null && chkCostCenters.Items.Count > 0)
                {
                    foreach (ListItem li in chkCostCenters.Items)
                    {
                        if (editCorp.CorpCostCenters.Exists(x => x.costcenterid == Convert.ToInt32(li.Value)))
                            li.Selected = true;
                    }
                }
            }
            //Somasekhar -30/03/2018 : for Nationality Details Tab
            if (editCorp.ProfileVisaDetailsList != null && editCorp.ProfileVisaDetailsList.Count > 0)
            {
                if (editCorp.ProfileVisaDetailsList.Find(w => w.Type == "E") != null)
                {
                    txtNationalIDNo.Text = editCorp.ProfileVisaDetailsList.Find(w => w.Type == "E").VisaNumber;
                    dcNationalIDExpDate.Value = editCorp.ProfileVisaDetailsList.Find(w => w.Type == "E").VisaExpDate;
                }

            }
            //Lokesh-June2,2017 :Load Ticket approvers and Expense approvers for the selected profile

            //Approvers List
            hdnApproversList.Value = string.Empty;
            if (editCorp.ProfileApproversList != null && editCorp.ProfileApproversList.Count > 0)
            {               
                hdnApproversList.Value = JsonConvert.SerializeObject(editCorp.ProfileApproversList);
            }

            hdnVisaDetails.Value = string.Empty;
            if (editCorp.ProfileVisaDetailsList != null && editCorp.ProfileVisaDetailsList.Count > 0)
            {
                // .FindAll(w=>w.Type!="E") condition Added for Getting VisaDetails only -- Somasekhar on 30/03/2018
                foreach (CorpProfileVisaDetails pad in editCorp.ProfileVisaDetailsList.FindAll(w => w.Type != "E"))
                {
                    //Main :Row Information Capturing.
                    //Record Id (^)
                    //Visa Country#
                    //Visa No#
                    //Issue Date#
                    //Expiry Date#
                    //Type
                    string rowInfoTA = string.Empty;
                    rowInfoTA = Convert.ToString(pad.VisaId);
                    rowInfoTA += "^";
                    rowInfoTA += Convert.ToString(pad.VisaCountry) + "#";
                    rowInfoTA += Convert.ToString(pad.VisaNumber) + "#";
                    rowInfoTA += pad.VisaIssueDate.ToString("dd/MM/yyyy").Replace('/', '-') + "#";
                    rowInfoTA += pad.VisaExpDate.ToString("dd/MM/yyyy").Replace('/', '-') + "#";
                    rowInfoTA += Convert.ToString(pad.Visaplaceofissue) + "#";
                    rowInfoTA += "VD"; //VISA DETAILS
                    if (string.IsNullOrEmpty(hdnVisaDetails.Value))
                    {
                        hdnVisaDetails.Value = rowInfoTA;
                    }
                    else
                    {
                        hdnVisaDetails.Value += "|" + rowInfoTA;
                    }
                }
            }
            if (editCorp.ProfileContactList != null && editCorp.ProfileContactList.Count > 0)
            {
                foreach (CropProfileContactDetails pad in editCorp.ProfileContactList)
                {
                    txtContactccemail.Text = Convert.ToString(pad.Ccmail);
                    txtcontactCity.Text = Convert.ToString(pad.City);
                    txtcontactState.Text = Convert.ToString(pad.State);
                    txtcontactstreet.Text = Convert.ToString(pad.Street);
                    txtContactPhone.Text = Convert.ToString(pad.Phone);
                    txtcontactpincode.Text = Convert.ToString(pad.Pincode);
                    txtcontactstreet.Text = Convert.ToString(pad.Street);
                    ddlcontactcountry.SelectedValue = Convert.ToString(pad.Country);
                    if (pad.Emailnotification == true)
                    {
                        rbncontactEmailNotification.Checked = true;
                    }
                    else
                    {
                        rbncontactEmailNotification.Checked = false;
                    }

                }
            }
            if (editCorp.ProfileGDSSettings != null && editCorp.ProfileGDSSettings.Count > 0)
            {
                foreach (CropProfileGDSSettings pad in editCorp.ProfileGDSSettings)
                {
                    txtGDS.Text = Convert.ToString(pad.GDS);
                    txtGDScorporateSSR.Text = Convert.ToString(pad.CorporateSSR);
                    txtGDSDescription.Text = Convert.ToString(pad.Description);
                    txtGDSextraCommand.Text = Convert.ToString(pad.ExtraCommand);
                    txtGDSOSI.Text = Convert.ToString(pad.OSI);
                    txtGDSOwnerPCC.Text = Convert.ToString(pad.OwnerPCC);
                    txtGDSQueueNo.Text = Convert.ToString(pad.QueueNo);
                    txtGDSRemraks.Text = Convert.ToString(pad.Remraks);
                }
            }
        }
        catch(Exception ex)
        { throw ex; }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            BindSearch();
        }
        catch (Exception ex)
        {
            //Label lblMasterError = (Label)this.Master.FindControl("lblError");
            //lblMasterError.Visible = true;
            //lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void BindSearch()
    {
        try
        {
            //// LoginInfo loginfo = Settings.LoginInfo;
            //// int agentid = 0;
            //// if (Settings.LoginInfo.AgentId > 1) agentid = Settings.LoginInfo.AgentId;
            ////if (Settings.LoginInfo.MemberType == MemberType.ADMIN)
            ////{
            ////    int agentid = Settings.LoginInfo.AgentId;//Pull The profiles based on the agency Id
            ////}
            //// DataTable dt = CorporateProfile.GetList(agentid, 0, ListStatus.Long);

            //Note1Display the Corp Profiles in the search grid based on the Selected Agency Id
            //Note2:If the logged in user member type is of type "ADMIN" or "SUPER" then bind all the profiles.
            //Note3: Display the corp profile details based on search button inside gridview.
            int agentid = Utility.ToInteger(ddlAgent.SelectedItem.Value) > 0 ? Utility.ToInteger(ddlAgent.SelectedItem.Value) : 0;
            long profileID = Settings.LoginInfo.CorporateProfileId;
            if (!string.IsNullOrEmpty(txtgSurname.Text) || (!string.IsNullOrEmpty(txtGname.Text)) || (!string.IsNullOrEmpty(txtGEmpid.Text))||(!string.IsNullOrEmpty(txtGEmail.Text)))
            {
                DataTable data = CorporateProfile.GetList(agentid, profileID, ListStatus.Long, txtgSurname.Text, txtGname.Text, txtGEmpid.Text, txtGEmail.Text);
                gvSearch.DataSource = data;
                gvSearch.DataBind();
                CleargridSearch();
            }
            else
            {

                DataTable dt = CorporateProfile.GetList(agentid, 0, ListStatus.Long);
                SearchList = dt;
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvSearch, dt);
            }
            
        }
        catch { throw; }
    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, SearchList.Copy());
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            long profileId = Utility.ToLong(gvSearch.SelectedValue);
            DataRow dr = SearchList.Rows.Find(profileId);
            int agentId = Utility.ToInteger(dr["AgentId"]);
            Edit(profileId, agentId);
            hdnProfileId.Value = Convert.ToString(profileId);
            this.Master.HideSearch();
            //Lokesh-2Jun,2017 : During edit mode for particular profile if there are any, need to show all the ticket approvers and expense approvers on the page.
            Utility.StartupScript(this.Page, "appendAllApproversList();", "SCRIPT");
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[CORP_PROFILE_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ProfileId"] };
            Session[CORP_PROFILE_SEARCH_SESSION] = value;
        }
    }

    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns ={{ "HTtxtSurName", "SurName" },{"HTtxtName", "Name" }, 
            { "HTtxtEmpId", "EmployeeId" },{ "HTtxtPhone", "Telephone" },{ "HTtxtMobPhone", "Mobilephone" }};
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void RenameImage(string activityId, string imagePath)
    {
        try
        {
            string source = imagePath.Substring(0, imagePath.LastIndexOf("\\") + 1);
            string oldFile = Path.GetFileName(imagePath);
            string fileExtension = Path.GetExtension(imagePath);
            string dFilePath = string.Empty;
            string newfile = activityId + "" + fileExtension;
            if (System.IO.File.Exists(source + newfile)) System.IO.File.Delete(source + newfile);
            System.IO.File.Move(source + oldFile, source + newfile);
            if (System.IO.File.Exists(source + oldFile)) System.IO.File.Delete(source + oldFile);

        }
        catch { }

    }

    //private void BindRoomType()
    //{
    //    try
    //    {
    //        //int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
    //        DataTable dt = Room.GetList(ListStatus.Short, RecordStatus.Activated);
    //        ddlRoomType.DataSource = dt;
    //        ddlRoomType.DataTextField = "naionality_code";
    //        ddlRoomType.DataValueField = "nationality_name";
    //        ddlRoomType.DataBind();
    //        ddlRoomType.Items.Insert(0, new ListItem("--Select Nationality --", "0"));

    //    }
    //    catch { }
    //}

    /// <summary>
    /// This method returns all the approvers list.
    /// </summary>
    /// <returns></returns>
    private void GetApproversList(CorporateProfile cp)
    {
        try
        {
            List<CorpProfileApproval> corpProfileApprovers = hdfMode.Value == "0"?new List<CorpProfileApproval>(): cp.ProfileApproversList;           
            var approvers = JsonConvert.DeserializeObject<List<CorpProfileApproval>>(hdnApproversList.Value);
            if(approvers != null && approvers.Count > 0)
            {
                foreach (var approver in approvers)
                {
                    if (!corpProfileApprovers.Exists(x => x.Id == approver.Id && x.Type == approver.Type) && approver.Status=="A")
                    {
                        approver.Id = 0;
                        approver.CreatedBy = (int)Settings.LoginInfo.UserID;
                        corpProfileApprovers.Add(approver);
                    }
                    else
                    {
                        var existApprover = corpProfileApprovers.Where(x => x.Id == approver.Id).FirstOrDefault();
                        if (existApprover != null)
                        {
                            existApprover.Type = approver.Type;
                            existApprover.Hierarchy = approver.Hierarchy;
                            existApprover.ApproverId = approver.ApproverId;
                            existApprover.Status = approver.Status;
                            existApprover.ModifiedBy = (int)Settings.LoginInfo.UserID;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindCountry(DropDownList ddlCountry)
    {
        try
        {
            SortedList Countries = CT.Core.Country.GetCountryList();
            ddlCountry.DataSource = Countries;
            ddlCountry.DataTextField = "key";
            ddlCountry.DataValueField = "value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("Select", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    /// <summary>
    /// This method returns all the Visa Details List
    /// </summary>
    /// <returns></returns>
    private void GetVisaDetailsList(CorporateProfile cp)
    {

        try
        {
            IFormatProvider dateFormat1 = new System.Globalization.CultureInfo("en-GB");
            if (hdfMode.Value == "0")  //New Profile Approvers insertion
            {
                List<CorpProfileVisaDetails> ProfileVisaDetailsList = new List<CorpProfileVisaDetails>();
                //Ticket Approvers
                if (!string.IsNullOrEmpty(hdnVisaDetails.Value) && hdnVisaDetails.Value.Length > 0)
                {
                    //Main :Row Information Capturing.
                    //Record Id (^)
                    //Visa Country#
                    //Visa No#
                    //Issue Date#
                    //Expiry Date#
                    //Type

                    string[] selTicketApprovers = hdnVisaDetails.Value.Split('|');
                    if (selTicketApprovers.Length > 0)
                    {
                        foreach (string app in selTicketApprovers)
                        {
                            CorpProfileVisaDetails pa = new CorpProfileVisaDetails();
                            int id;
                            int.TryParse(app.Split('^')[0], out id); //returns the id.
                            pa.VisaId = id;
                            pa.VisaCountry = Convert.ToString(app.Split('^')[1].Split('#')[0]);
                            pa.VisaNumber = Convert.ToString(app.Split('^')[1].Split('#')[1]);
                            pa.VisaIssueDate = Convert.ToDateTime(app.Split('^')[1].Split('#')[2], dateFormat1);
                            pa.VisaExpDate = Convert.ToDateTime(app.Split('^')[1].Split('#')[3], dateFormat1);
                            pa.Visaplaceofissue= Convert.ToString(app.Split('^')[1].Split('#')[4]);
                            pa.Status = "A";
                            pa.Type = "V";
                            pa.CreatedBy = (int)Settings.LoginInfo.UserID;
                            ProfileVisaDetailsList.Add(pa);
                        }
                    }
                }
                cp.ProfileVisaDetailsList = ProfileVisaDetailsList;

                #region // For newly Inserted Nationality Details -- Add by somasekhar on 30/03/2018 
                if (!string.IsNullOrEmpty(txtNationalIDNo.Text))
                {
                    CorpProfileVisaDetails pa = new CorpProfileVisaDetails();
                    //int id;
                    //int.TryParse(app.Split('^')[0], out id); //returns the id.
                    pa.VisaId = -1;
                   // pa.VisaCountry = "";
                    pa.VisaNumber = txtNationalIDNo.Text ;
                   // pa.VisaIssueDate = Convert.ToDateTime( DBNull.Value);
                    pa.VisaExpDate = Convert.ToDateTime(dcNationalIDExpDate.Value, dateFormat1);
                    pa.Status = "A";
                    pa.Type = "E";
                    pa.CreatedBy = (int)Settings.LoginInfo.UserID;
                    ProfileVisaDetailsList.Add(pa);

                    cp.ProfileVisaDetailsList = ProfileVisaDetailsList;
                }
                #endregion

            }
            else if (hdfMode.Value == "1")//Edit mode
            {
                List<CorpProfileVisaDetails> existingProfileVisaDetails = null;
                List<CorpProfileVisaDetails> newProfileVisaDetails = new List<CorpProfileVisaDetails>(); //This is for new visa  details records insertion
                                                                                                         // editCorp.ProfileVisaDetailsList.Find(w => w.Type == "E").VisaNumber


                if (cp.ProfileVisaDetailsList != null && cp.ProfileVisaDetailsList.Count > 0)
                {
                    existingProfileVisaDetails = cp.ProfileVisaDetailsList;
                }
                if (existingProfileVisaDetails != null && existingProfileVisaDetails.Count > 0)
                {
                    if (!string.IsNullOrEmpty(hdnDelVD.Value))
                    {
                        string[] delApprovers = hdnDelVD.Value.Split('|');
                        if (delApprovers.Length > 0)
                        {
                            foreach (string delApp in delApprovers)
                            {
                                foreach (CorpProfileVisaDetails epa in existingProfileVisaDetails)
                                {
                                    int id;
                                    int.TryParse(delApp.Split('^')[0], out id);
                                    if (id == epa.VisaId)
                                    {
                                        epa.CreatedBy = (int)Settings.LoginInfo.UserID;
                                        epa.Status = "D";
                                    }
                                }
                            }
                        }
                    }

                    #region Visa Details
                    if (!string.IsNullOrEmpty(hdnVisaDetails.Value) && hdnVisaDetails.Value.Length > 0)
                    {
                        string[] selTicketApprovers = hdnVisaDetails.Value.Split('|');
                        if (selTicketApprovers.Length > 0)
                        {
                            foreach (string selTA in selTicketApprovers)
                            {
                                int id;
                                int.TryParse(selTA.Split('^')[0], out id);
                                if (id < 0)
                                {

                                    CorpProfileVisaDetails pa = new CorpProfileVisaDetails();
                                    pa.VisaId = id;
                                    pa.VisaCountry = Convert.ToString(selTA.Split('^')[1].Split('#')[0]);
                                    pa.VisaNumber = Convert.ToString(selTA.Split('^')[1].Split('#')[1]);
                                    pa.VisaIssueDate = Convert.ToDateTime(selTA.Split('^')[1].Split('#')[2], dateFormat1);
                                    pa.VisaExpDate = Convert.ToDateTime(selTA.Split('^')[1].Split('#')[3], dateFormat1);
                                    pa.Visaplaceofissue = Convert.ToString(selTA.Split('^')[1].Split('#')[4]);
                                    pa.Status = "A";
                                    pa.Type = "V";
                                    pa.CreatedBy = (int)Settings.LoginInfo.UserID;

                                    newProfileVisaDetails.Add(pa);
                                }
                                else
                                {
                                    foreach (CorpProfileVisaDetails ped in existingProfileVisaDetails)
                                    {
                                        if (ped.VisaId == id)
                                        {
                                            ped.VisaId = id;
                                            ped.VisaCountry = Convert.ToString(selTA.Split('^')[1].Split('#')[0]);
                                            ped.VisaNumber = Convert.ToString(selTA.Split('^')[1].Split('#')[1]);
                                            ped.VisaIssueDate = Convert.ToDateTime(selTA.Split('^')[1].Split('#')[2], dateFormat1);
                                            ped.VisaExpDate = Convert.ToDateTime(selTA.Split('^')[1].Split('#')[3], dateFormat1);
                                            ped.Visaplaceofissue = Convert.ToString(selTA.Split('^')[1].Split('#')[4]);
                                            ped.Status = "A";
                                            ped.Type = "V";
                                            ped.CreatedBy = (int)Settings.LoginInfo.UserID;
                                        }
                                    }

                                }

                            }
                        }
                    }
                    #endregion

                    #region  Nationality Details
                    //For newly Inserted Nationality Details in Edit Mode -- Add by somasekhar on 31/03/2018 

                    if (cp.ProfileVisaDetailsList != null && cp.ProfileVisaDetailsList.FindAll(w => w.Type == "E").Count > 0)
                    {
                        CorpProfileVisaDetails pa = new CorpProfileVisaDetails();

                        //int.TryParse(app.Split('^')[0], out id); //returns the id.
                        pa.VisaId = cp.ProfileVisaDetailsList.Find(w => w.Type == "E").VisaId;
                        // pa.VisaCountry = "";
                        pa.VisaNumber = txtNationalIDNo.Text;
                        // pa.VisaIssueDate = Convert.ToDateTime( DBNull.Value);
                        pa.VisaExpDate = Convert.ToDateTime(dcNationalIDExpDate.Value, dateFormat1);
                        pa.Status = "A";
                        pa.Type = "E";
                        pa.CreatedBy = (int)Settings.LoginInfo.UserID;
                        cp.ProfileVisaDetailsList.Add(pa);

                        existingProfileVisaDetails = cp.ProfileVisaDetailsList;
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(txtNationalIDNo.Text))
                        {

                            CorpProfileVisaDetails pa = new CorpProfileVisaDetails();
                            //int id;
                            //int.TryParse(app.Split('^')[0], out id); //returns the id.
                            pa.VisaId = -1;
                            // pa.VisaCountry = "";
                            pa.VisaNumber = txtNationalIDNo.Text;
                            // pa.VisaIssueDate = Convert.ToDateTime( DBNull.Value);
                            pa.VisaExpDate = Convert.ToDateTime(dcNationalIDExpDate.Value, dateFormat1);
                            pa.Status = "A";
                            pa.Type = "E";
                            pa.CreatedBy = (int)Settings.LoginInfo.UserID;
                            newProfileVisaDetails.Add(pa);
                        }

                    }
                    #endregion
                }
                else// While retrieving if there are no profile approvers this else block will be executed
                {

                    //Ticket Approvers
                    if (!string.IsNullOrEmpty(hdnVisaDetails.Value) && hdnVisaDetails.Value.Length > 0)
                    {
                        //Main :Row Information Capturing.
                        //Record Id (^)
                        //Visa Country#
                        //Visa No#
                        //Issue Date#
                        //Expiry Date#
                        //Type

                        string[] selTicketApprovers = hdnVisaDetails.Value.Split('|');
                        if (selTicketApprovers.Length > 0)
                        {
                            foreach (string app in selTicketApprovers)
                            {
                                CorpProfileVisaDetails pa = new CorpProfileVisaDetails();
                                int id;
                                int.TryParse(app.Split('^')[0], out id); //returns the id.
                                pa.VisaId = id;
                                pa.VisaCountry = Convert.ToString(app.Split('^')[1].Split('#')[0]);
                                pa.VisaNumber = Convert.ToString(app.Split('^')[1].Split('#')[1]);
                                pa.VisaIssueDate = Convert.ToDateTime(app.Split('^')[1].Split('#')[2], dateFormat1);
                                pa.VisaExpDate = Convert.ToDateTime(app.Split('^')[1].Split('#')[3], dateFormat1);
                                pa.Visaplaceofissue = Convert.ToString(app.Split('^')[1].Split('#')[4]);
                                pa.Status = "A";
                                pa.Type = "V";
                                pa.CreatedBy = (int)Settings.LoginInfo.UserID;
                                newProfileVisaDetails.Add(pa);
                            }
                        }
                    }
                }



                if (existingProfileVisaDetails != null && existingProfileVisaDetails.Count > 0)
                {
                    if (newProfileVisaDetails != null && newProfileVisaDetails.Count > 0)
                    {
                        existingProfileVisaDetails.AddRange(newProfileVisaDetails);
                    }
                    cp.ProfileVisaDetailsList = existingProfileVisaDetails;
                }
                else
                {
                    if (newProfileVisaDetails != null && newProfileVisaDetails.Count > 0)
                    {
                        cp.ProfileVisaDetailsList = newProfileVisaDetails;
                    }
                }

            }
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }
    private void GetContactDetailsList(CorporateProfile cp)
    {
        try
        {
            List<CropProfileContactDetails> ProfileContactDetailsList = new List<CropProfileContactDetails>();
            CropProfileContactDetails pa = new CropProfileContactDetails();
            if (hdfMode.Value == "0")  
            {

                            pa.ContId = -1;
                            pa.Street = Convert.ToString(txtcontactstreet.Text.Trim());
                            pa.State = Convert.ToString(txtcontactState.Text.Trim());
                            pa.City = Convert.ToString(txtcontactCity.Text.Trim());
                            pa.Country = Convert.ToString(ddlcontactcountry.SelectedValue);
                            pa.Phone = Convert.ToString(txtContactPhone.Text.Trim());
                            pa.Ccmail = Convert.ToString(txtContactccemail.Text.Trim());
                            pa.Pincode= Convert.ToString(txtcontactpincode.Text.Trim());
                            pa.CreatedBy = (int)Settings.LoginInfo.UserID;
                if (rbncontactEmailNotification.Checked == true)
                            {
                                pa.Emailnotification = true;
                            }
                            pa.Type = "PA";
                            pa.CreatedBy = (int)Settings.LoginInfo.UserID;
                            ProfileContactDetailsList.Add(pa);
                cp.ProfileContactList = ProfileContactDetailsList;


            }
            else if (hdfMode.Value == "1")//Edit mode
            {
                if (cp.ProfileContactList != null && cp.ProfileContactList.Count > 0)
                {
                    pa.ContId = cp.ProfileContactList[0].ContId;
                }
                else
                {
                    pa.ContId = -1;
                }
                pa.Street = Convert.ToString(txtcontactstreet.Text.Trim());
                pa.State = Convert.ToString(txtcontactState.Text.Trim());
                pa.City = Convert.ToString(txtcontactCity.Text.Trim());
                pa.Country = Convert.ToString(ddlcontactcountry.SelectedValue);
                pa.Phone = Convert.ToString(txtContactPhone.Text.Trim());
                pa.Ccmail = Convert.ToString(txtContactccemail.Text.Trim());
                pa.Pincode = Convert.ToString(txtcontactpincode.Text.Trim());
                if (rbncontactEmailNotification.Checked == true)
                {
                    pa.Emailnotification = true;
                }
                pa.Type = "PA";
                pa.CreatedBy = (int)Settings.LoginInfo.UserID;
                ProfileContactDetailsList.Add(pa);
                cp.ProfileContactList = ProfileContactDetailsList;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void GetGDSSettingsList(CorporateProfile cp)
    {
        try
        {
            List<CropProfileGDSSettings> ProfileGDSSettingsList = new List<CropProfileGDSSettings>();
            CropProfileGDSSettings pa = new CropProfileGDSSettings();
            if (hdfMode.Value == "0")
            {

                pa.SettingsId = -1;
                pa.GDS = Convert.ToString(txtGDS.Text.Trim());
                pa.Description = Convert.ToString(txtGDSDescription.Text.Trim());
                pa.OSI = Convert.ToString(txtGDSOSI.Text.Trim());
                pa.OwnerPCC = Convert.ToString(txtGDSOwnerPCC.Text.Trim());
                pa.ExtraCommand = Convert.ToString(txtGDSextraCommand   .Text.Trim());
                pa.CorporateSSR = Convert.ToString(txtGDScorporateSSR.Text.Trim());
                pa.Remraks = Convert.ToString(txtGDSRemraks.Text.Trim());
                pa.CreatedBy = (int)Settings.LoginInfo.UserID;
                ProfileGDSSettingsList.Add(pa);
                cp.ProfileGDSSettings = ProfileGDSSettingsList;


            }
            else if (hdfMode.Value == "1")//Edit mode
            {
                if (cp.ProfileGDSSettings != null && cp.ProfileGDSSettings.Count > 0)
                {
                    pa.SettingsId = cp.ProfileGDSSettings[0].SettingsId;
                }
                else
                {
                    pa.SettingsId = -1;
                }
                    pa.GDS = Convert.ToString(txtGDS.Text.Trim());
                    pa.Description = Convert.ToString(txtGDSDescription.Text.Trim());
                    pa.OSI = Convert.ToString(txtGDSOSI.Text.Trim());
                    pa.OwnerPCC = Convert.ToString(txtGDSOwnerPCC.Text.Trim());
                    pa.ExtraCommand = Convert.ToString(txtGDSextraCommand.Text.Trim());
                    pa.CorporateSSR = Convert.ToString(txtGDScorporateSSR.Text.Trim());
                    pa.Remraks = Convert.ToString(txtGDSRemraks.Text.Trim());
                    pa.CreatedBy = (int)Settings.LoginInfo.UserID;
                    ProfileGDSSettingsList.Add(pa);
                    cp.ProfileGDSSettings = ProfileGDSSettingsList;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    public void SetddlValue(DropDownList ddl, string val)
    {
        ddl.SelectedValue = ddl.Items.FindByValue(val) != null ? val : ddl.SelectedValue;
    }
    #region for search based on EmpCode -- Somasekhar on 31/03/2018
    protected void btnEmpQuerySearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (!string.IsNullOrEmpty(txtEmpQuerySearch.Text))
            {
                BindQuerySearch();
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void BindQuerySearch()
    {
        try
        {
            int agentid = Settings.LoginInfo.AgentId;//Pull The profiles based on the agency Id         
            long profileId = 0;
            int profileAgentId = 0;
            DataTable dt= CorporateProfile.GetProfileIdbasedOnEmpID(txtEmpQuerySearch.Text.Trim());
            Clear();
            if (dt.Rows.Count > 0)
            {
                profileId = Utility.ToLong(dt.Rows[0]["ProfileId"]);
                profileAgentId = Utility.ToInteger(dt.Rows[0]["AgentId"]);
                Edit(profileId, profileAgentId);
                hdnProfileId.Value = Convert.ToString(profileId);
                Utility.StartupScript(this.Page, "appendAllApproversList();", "SCRIPT");
            }
        }
        catch { throw; }
    }
    private void BindCorpCardDetails()
    {
        try
        {
            DataTable dt = CorporateProfile.GetCorpCardDetailsByAgent(Settings.LoginInfo.AgentId);
            if (dt != null && dt.Rows.Count > 0)
            {
                ddlTravellerCard.DataSource = dt;
                ddlTravellerCard.DataValueField = "CM_ID";
                ddlTravellerCard.DataTextField = "CM_Name";
                ddlTravellerCard.DataBind();
                ddlTravellerCard.Items.Insert(0, new ListItem("Select Travel Card", "0"));

                ddlExpenseCard.DataSource = dt;
                ddlExpenseCard.DataValueField = "CM_ID";
                ddlExpenseCard.DataTextField = "CM_Name";
                ddlExpenseCard.DataBind();
               ddlExpenseCard.Items.Insert(0, new ListItem("Select Expense Card", "0"));

               
            }
        }
        catch (Exception ex) {
            throw ex;
        }
    }

    #endregion

    #region Profile File Upload -- Somasekhar on 30/03/2018
    public string UploadDirectory = string.Empty;//"~/UploadControl/UploadImages/"; 
    //Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["ProfileImage"]); 
    protected void UploadControl_FileUploadComplete(object sender, FileUploadCompleteEventArgs e)
    {
        try
        {
            e.CallbackData = SavePostedFile(e.UploadedFile);
        }
        catch (Exception ex)
        {
          
        }
    }
    protected string SavePostedFile(UploadedFile uploadedFile)
    {
        string rootFolder = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["ProfileImage"]);
      
        try
        {
            if (!uploadedFile.IsValid)
                return string.Empty;

            profileImage.SavePath = rootFolder;
            string profileImg = Convert.ToString(DateTime.Now.ToString("dd-MM-yyyy hh:mm:ss")).Replace(":", "");
            profileImg = profileImg.Replace("-", "");
            profileImg = profileImg.Replace(" ", "");
            profileImage.FileName = profileImg;
            string AgentImgPath = Server.MapPath("~/" + rootFolder + "/") + profileImg;

            profileImage.FileName = AgentImgPath;
            profileImage.FileExtension = Path.GetExtension(uploadedFile.FileName);

            UploadDirectory = Server.MapPath("~/" + rootFolder);

            if (!Directory.Exists(UploadDirectory))
            {
                Directory.CreateDirectory(UploadDirectory);
            }

            string genFileName = CombinePath(Path.ChangeExtension(profileImage.FileName, profileImage.FileExtension));

            using (System.Drawing.Image original = System.Drawing.Image.FromStream(uploadedFile.FileContent))
            using (System.Drawing.Image thumbnail = new ImageThumbnailCreator(original).CreateImageThumbnail(new Size(350, 350)))
                ImageUtils.SaveToJpeg((Bitmap)thumbnail, genFileName);
 
            Session["profileImagePath"] = genFileName;
           
            return rootFolder + Path.GetFileName(genFileName);
        }
       catch (Exception ex)
        { 
            throw ex;
        } 
     
    }
    protected string CombinePath(string fileName)
    {
        return Path.Combine(UploadDirectory, fileName);
    }
    #endregion
    //Added by Anji on 04/11/2019.
    protected void btnGSearch_Click(object sender, EventArgs e)
    {
        try
        {

            BindSearch();
        }
        catch(Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);

        }

    }
}
