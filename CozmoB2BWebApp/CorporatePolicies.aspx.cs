﻿using System;
using System.Collections.Generic;
using System.Web.UI.WebControls;

using System.Data;
using CT.TicketReceipt.BusinessLayer;
using CT.Corporate;
using CT.Core;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using System.Linq;
using Newtonsoft.Json;


public partial class CorporatePoliciesUI : CT.Core.ParentPage
{

    private string POLICY_SEARCH_SESSION = "_PolicySearchList";
    private string currency;

    protected void Page_Load(object sender, EventArgs e)
    {

        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                this.currency = Settings.LoginInfo.Currency;
                if (!IsPostBack)
                {
                    IntialiseControls();
                }

            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateProfile page " + ex.Message, "0");
        }

    }

    private void IntialiseControls()
    {
        try
        {
            bindAgentsList();
            bindCountryList();
            bindAirLines();
            bindCities();
            Clear();

            //For Expense Tab

            ListItem item = new ListItem();
            item.Text = Settings.LoginInfo.Currency;
            item.Value = Settings.LoginInfo.Currency;

            ddlCapExpenseCurrency.Items.Add(item);
            ddlCurrencyCapAllowance.Items.Add(item);

            bindExpenseTypes();

            //For Approval tab
            ddlPreTripCurrency.Items.Add(item);
            ddlRelaunchPriceChange.Items.Add(item);

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    private void bindCities()
    {
        try
        {
            DataTable dtCities = CityMaster.GetAllCities(ListStatus.Short, RecordStatus.Activated);
            DataView view = dtCities.DefaultView;
            view.Sort = "cityName ASC";
            dtCities = view.ToTable();
            ddlExceptionCity.DataSource = dtCities;
            ddlExceptionCity.DataTextField = "cityName";
            ddlExceptionCity.DataValueField = "cityCode";
            ddlExceptionCity.AppendDataBoundItems = true;
            ddlExceptionCity.DataBind();

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void bindAirLines()
    {
        try
        {
            DataTable dtAirlines = CT.Core.Airline.GetAllAirlines();
            DataView view = dtAirlines.DefaultView;
            view.Sort = "airlineName ASC";
            dtAirlines = view.ToTable();

            ddlAirline.DataSource = dtAirlines;
            ddlAirline.DataTextField = "airlineName";
            ddlAirline.DataValueField = "airlineCode";
            ddlAirline.AppendDataBoundItems = true;
            ddlAirline.DataBind();

            ddlPrefAirline.DataSource = dtAirlines;
            ddlPrefAirline.DataTextField = "airlineName";
            ddlPrefAirline.DataValueField = "airlineCode";
            ddlPrefAirline.AppendDataBoundItems = true;
            ddlPrefAirline.DataBind();


            ddlAvoidAirline.DataSource = dtAirlines;
            ddlAvoidAirline.DataTextField = "airlineName";
            ddlAvoidAirline.DataValueField = "airlineCode";
            ddlAvoidAirline.AppendDataBoundItems = true;
            ddlAvoidAirline.DataBind();





        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {

            Clear();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateProfile page " + ex.Message, "0");

        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {

            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateProfile page " + ex.Message, "0");

        }
        finally
        {
            Clear();
            Utility.StartupScript(this.Page, "Clear();", "SCRIPT");
        }
    }

    private void Clear()
    {
        try
        {
            txtRateCode.Text = "";
            txtPolicyName.Text = "";
            hdnFromDate.Value = "";
            hdnDestinations.Value = "";
            hdnAirlineNegotiated.Value = "";
            hdnSecurityRules.Value = "";
            hdnAirlinePrefered.Value = "";
            hdnAirlineAvoid.Value = "";
            hdnAirFareShopping.Value = "";
            hdnDefaultCabin.Value = "";
            hdnCabinTypeException.Value = "";
            //ddlAgents.SelectedIndex = 0;
            ddlCountry.SelectedIndex = 0;
            ddlCity.SelectedIndex = 0;
            ddlAirline.SelectedIndex = 0;
            ddlAvoidAirline.SelectedIndex = 0;
            ddlPrefAirline.SelectedIndex = 0;
            ddlCabinType.SelectedIndex = 0;
            ddlExceptionCabinType.SelectedIndex = 0;
            ddlExceptionCity.SelectedIndex = 0;
            ddlCabinTypeRule2.SelectedValue = "0";
            lblSuccessMessage.Text = string.Empty;
            txtFromDate.Value = DateTime.Now;
            hdfMode.Value = "0";
            hdfEMId.Value = "0";
            btnClear.Text = "Clear";
            btnSave.Text = "Save";
            hdnDelDestinationsToAvoid.Value = "";
            hdnDelAirlinesNego.Value = "";
            hdnDelPreferAirline.Value = "";
            hdnDelAvoidAirlines.Value = "";
            hdnDelDefaultCabin.Value = "";
            hdnDelCabinTypeException.Value = "";
            ddlExceptionFlightType.SelectedValue = "-1";
            txtBookingAmount.Text = string.Empty;
            txtBookingThreshold.Text = string.Empty;
            ddlBookingThreshold.SelectedValue = "F";
            txtTransitTime.Text = string.Empty;
            chkRestrictBooking.Checked = false;
            chkAutoTicketing.Checked = false;
            chkCompleteBooking.Checked = false;
            if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
            {
                ddlAgents.Enabled = true;
            }
            else
            {
                ddlAgents.Enabled = false;
            }
            try
            {
                ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            catch { }

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void bindAgentsList()
    {
        try
        {
            DataTable dtAgents = CT.TicketReceipt.BusinessLayer.AgentMaster.GetList(1, "ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            DataView view = dtAgents.DefaultView;
            view.Sort = "agent_Name ASC";
            dtAgents = view.ToTable();
            ddlAgents.DataSource = dtAgents;
            ddlAgents.DataTextField = "agent_Name";
            ddlAgents.DataValueField = "agent_Id";
            ddlAgents.AppendDataBoundItems = true;
            ddlAgents.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    private void bindCountryList()
    {
        try
        {
            DataTable dtCountries = CountryMaster.GetList(ListStatus.Short, RecordStatus.Activated);

            ddlCountry.DataSource = dtCountries;
            ddlCountry.DataValueField = "COUNTRY_CODE";
            ddlCountry.DataTextField = "COUNTRY_NAME";
            ddlCountry.AppendDataBoundItems = true;
            ddlCountry.DataBind();

            //Expense Tab
            ddlCountryCapExpense.DataSource = dtCountries;
            ddlCountryCapExpense.DataValueField = "COUNTRY_CODE";
            ddlCountryCapExpense.DataTextField = "COUNTRY_NAME";
            ddlCountryCapExpense.AppendDataBoundItems = true;
            ddlCountryCapExpense.DataBind();


            //Allowance Tab
            ddlCountryCapAllowance.DataSource = dtCountries;
            ddlCountryCapAllowance.DataValueField = "COUNTRY_CODE";
            ddlCountryCapAllowance.DataTextField = "COUNTRY_NAME";
            ddlCountryCapAllowance.AppendDataBoundItems = true;
            ddlCountryCapAllowance.DataBind();


        }
        catch (Exception ex)
        {
            throw ex;
        }

    }

    private void Save()
    {
        try
        {

            if (hdfMode.Value == "0")  //New Policy Details insertion 
            {

                PolicyMaster pm = new PolicyMaster();
                pm.CreatedBy = (int)Settings.LoginInfo.UserID;

                #region Policy Header
                pm.AgentId = Utility.ToInteger(ddlAgents.SelectedItem.Value);
                pm.Status = "A";
                pm.PolicyCode = string.Empty;
                pm.PolicyName = txtPolicyName.Text;
                List<DateTime> DatesToAvoid = new List<DateTime>();
                if (!string.IsNullOrEmpty(hdnFromDate.Value))
                {
                    string[] selFromDates = hdnFromDate.Value.Split('|');
                    if (selFromDates.Length > 0)
                    {
                        foreach (string date in selFromDates)
                        {

                            DateTime edate = DateTime.ParseExact(date.Replace('-', '/'), "dd/MM/yyyy", null);
                            DatesToAvoid.Add(Utility.ToDate(edate));
                        }
                        pm.DatesToAvoidList = DatesToAvoid;
                    }
                }
                #endregion

                #region Policy Details
                List<PolicyDetails> policyDetails = new List<PolicyDetails>();
                //Destinations to avoid.
                if (!string.IsNullOrEmpty(hdnDestinations.Value))
                {
                    string[] selDestinations = hdnDestinations.Value.Split('|');
                    if (selDestinations.Length > 0)
                    {
                        foreach (string selDest in selDestinations)
                        {
                            PolicyDetails pd = new PolicyDetails();
                            int id;
                            int.TryParse(selDest.Split('^')[0], out id); //returns the id.
                            pd.Id = id;
                            pd.Category = (Category)Category.DESTINATIONTOAVOID;
                            pd.ProductId = 1;
                            pd.Status = "A";
                            pd.CreatedBy = (int)Settings.LoginInfo.UserID;

                            //Eg1: -1^AF-BST-AvoidConnection:Yes
                            //Eg2: -1^AF-BST-AvoidConnection:No
                            if (selDest.Split('^')[1].Split('-').Length >= 3)
                            {
                                pd.FilterValue2 = selDest.Split('^')[1].Split('-')[0]; // Country Value is saved --used in edit mode.
                                pd.FilterType = selDest.Split('^')[1].Split('-')[1];  //City Value
                                pd.FilterValue1 = selDest.Split('^')[1].Split('-')[2].Split(':')[1]; //AvoidConnection: Yes or No
                            }
                            policyDetails.Add(pd);
                        }
                    }
                }

                //AIRLINES WITH NEGOTIATED FARES

                if (!string.IsNullOrEmpty(hdnAirlineNegotiated.Value))
                {
                    string[] selAirlinesNegotiated = hdnAirlineNegotiated.Value.Split('|');
                    if (selAirlinesNegotiated.Length > 0)
                    {
                        foreach (string selAirline in selAirlinesNegotiated)
                        {
                            PolicyDetails pd = new PolicyDetails();
                            pd.Category = (Category)Category.NEGOTIATEDFARES;
                            int id;
                            int.TryParse(selAirline.Split('^')[0], out id);
                            pd.Id = id;
                            pd.ProductId = 1;
                            pd.Status = "A";
                            pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            if (selAirline.Split('^')[1].Split('-').Length >= 2)
                            {
                                pd.FilterType = selAirline.Split('^')[1].Split('-')[0];
                                pd.FilterValue1 = selAirline.Split('^')[1].Split('-')[1];
                            }
                            policyDetails.Add(pd);
                        }
                    }
                }
                //SECURITY RULES
                if (!string.IsNullOrEmpty(hdnSecurityRules.Value))
                {
                    PolicyDetails pd = new PolicyDetails();
                    pd.Category = (Category)Category.SECURITYRULES;
                    pd.Id = -1;
                    pd.ProductId = 1;
                    pd.Status = "A";
                    pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                    pd.FilterType = hdnSecurityRules.Value.Split('|')[0];
                    pd.FilterValue1 = hdnSecurityRules.Value.Split('|')[1];
                    policyDetails.Add(pd);
                }

                //PREFERRED AIRLINES
                if (!string.IsNullOrEmpty(hdnAirlinePrefered.Value))
                {
                    string[] preferredAirlines = hdnAirlinePrefered.Value.Split('|');
                    if (preferredAirlines.Length > 0)
                    {
                        foreach (string prefAirline in preferredAirlines)
                        {
                            PolicyDetails pd = new PolicyDetails();
                            pd.Category = (Category)Category.PREFERREDAIRLINES;
                            int id;
                            int.TryParse(prefAirline.Split('^')[0], out id);
                            pd.Id = id;
                            pd.ProductId = 1;
                            pd.Status = "A";
                            pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            pd.FilterType = prefAirline.Split('^')[1];
                            policyDetails.Add(pd);
                        }
                    }
                }

                //AIRLINES TO AVOID
                if (!string.IsNullOrEmpty(hdnAirlineAvoid.Value))
                {

                    string[] avoidAirlines = hdnAirlineAvoid.Value.Split('|');
                    if (avoidAirlines.Length > 0)
                    {
                        foreach (string avoidAirline in avoidAirlines)
                        {
                            PolicyDetails pd = new PolicyDetails();
                            pd.Category = (Category)Category.AIRLINESTOAVOID;
                            int id;
                            int.TryParse(avoidAirline.Split('^')[0], out id);

                            pd.Id = id;
                            pd.ProductId = 1;
                            pd.Status = "A";
                            pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            pd.FilterType = avoidAirline.Split('^')[1];
                            policyDetails.Add(pd);
                        }
                    }
                }

                //AIR FARE SHOPPING
                if (!string.IsNullOrEmpty(hdnAirFareShopping.Value))
                {
                    PolicyDetails pd = new PolicyDetails();
                    pd.Category = (Category)Category.AIRFARESHOPPING;
                    pd.Id = -1;
                    pd.ProductId = 1;
                    pd.Status = "A";
                    pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                    pd.FilterType = hdnAirFareShopping.Value;
                    policyDetails.Add(pd);
                }

                //Default Cabin
                if (!string.IsNullOrEmpty(hdnDefaultCabin.Value))
                {
                    PolicyDetails pd = new PolicyDetails();
                    pd.Category = (Category)Category.DEFAULTCABIN;
                    pd.Id = -1;
                    pd.ProductId = 1;
                    pd.Status = "A";
                    pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                    pd.FilterType = hdnDefaultCabin.Value;
                    pd.FilterValue3 = "4"; //Default cabin
                    policyDetails.Add(pd);
                }

                //CABIN TYPE RULES

                if (!string.IsNullOrEmpty(hdnCabinTypeException.Value))
                {
                    string[] cabinTypeRules = hdnCabinTypeException.Value.Split('|');
                    if (cabinTypeRules.Length > 0)
                    {
                        foreach (string cabinTypeRule in cabinTypeRules)
                        {
                            PolicyDetails pd = new PolicyDetails();
                            pd.Category = (Category)Category.EXCEPTIONCABIN;
                            int id;
                            int.TryParse(cabinTypeRule.Split('^')[0], out id);

                            pd.Id = id;
                            pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            pd.ProductId = 1;
                            pd.Status = "A";
                            if (cabinTypeRule.Split('^')[1].Split('-').Length >= 2)
                            {
                                pd.FilterType = cabinTypeRule.Split('^')[1].Split('-')[0];
                                pd.FilterValue1 = cabinTypeRule.Split('^')[1].Split('-')[1];

                                if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("DESTINATION"))
                                {
                                    pd.FilterValue3 = "1";
                                }
                                else if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("JOURNYTIME"))
                                {
                                    pd.FilterValue3 = "2";
                                }
                                else if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("FLIGHTDURATION"))
                                {
                                    pd.FilterValue3 = "3";
                                }
                                else if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("NOSEATS"))
                                {
                                    pd.FilterValue3 = "5";
                                }

                                if (cabinTypeRule.Split('^')[1].Split('-').Length > 2)
                                {
                                    pd.FilterValue2 = cabinTypeRule.Split('^')[1].Split('-')[2];
                                    pd.FilterValue4 = cabinTypeRule.Split('^')[1].Split('-')[3];
                                }
                            }
                            policyDetails.Add(pd);

                        }
                    }
                }


                #region ApprovalTab

                //Lokesh : 31May2017 Approval Tab.
                //Case -0: PRE-TRIP APPROVAL  --  NoApproval
                if (preTripNoApproval.Checked)
                {
                    PolicyDetails pd = new PolicyDetails();
                    pd.Category = (Category)Category.PRETRIPAPPROVAL;
                    pd.Id = -1;
                    pd.ProductId = 1;
                    pd.Status = "A";
                    pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                    pd.FilterType = "NOAPPROVAL";
                    policyDetails.Add(pd);
                }

                //Case -1: PRE-TRIP APPROVAL  --  AlwaysRequired
                if (preTripAlways.Checked)
                {
                    PolicyDetails pd = new PolicyDetails();
                    pd.Category = (Category)Category.PRETRIPAPPROVAL;
                    pd.Id = -1;
                    pd.ProductId = 1;
                    pd.Status = "A";
                    pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                    pd.FilterType = "ALWAYS";
                    policyDetails.Add(pd);
                }

                //Case -2: PRE-TRIP APPROVAL  -- if trip cost exceeds
                if (preTripCostExceeds.Checked)
                {
                    PolicyDetails pd = new PolicyDetails();
                    pd.Category = (Category)Category.PRETRIPAPPROVAL;
                    pd.Id = -1;
                    pd.ProductId = 1;
                    pd.Status = "A";
                    pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                    pd.FilterType = "COSTEXCEEDS";
                    pd.FilterValue1 = ddlPreTripCurrency.SelectedItem.Value;
                    pd.FilterValue2 = txtPreTripAmount.Text;
                    policyDetails.Add(pd);
                }


                //Case -3: RELAUNCH APPROVAL PROCESS  -- if price change exceeds
                if (chkRelaunchPriceChange.Checked)
                {
                    PolicyDetails pd = new PolicyDetails();
                    pd.Category = (Category)Category.RELAUNCHAPPROVAL;
                    pd.Id = -1;
                    pd.ProductId = 1;
                    pd.Status = "A";
                    pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                    pd.FilterType = "COSTEXCEEDS";
                    pd.FilterValue1 = ddlRelaunchPriceChange.SelectedItem.Value;
                    pd.FilterValue2 = txtRelaunchPriceAmount.Text;
                    policyDetails.Add(pd);
                }


                //Case -4: RELAUNCH APPROVAL PROCESS  --  itinerary change
                if (chkRelaunchItineraryChange.Checked)
                {
                    PolicyDetails pd = new PolicyDetails();
                    pd.Category = (Category)Category.RELAUNCHAPPROVAL;
                    pd.Id = -1;
                    pd.ProductId = 1;
                    pd.Status = "A";
                    pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                    pd.FilterType = "CHANGEITINERARY";
                    policyDetails.Add(pd);
                }

                //Case -5: PRE-VISA APPROVAL  --  NoApproval
                if (preVisaNoApproval.Checked)
                {
                    PolicyDetails pd = new PolicyDetails();
                    pd.Category = (Category)Category.PREVISAAPPROVAL;
                    pd.Id = -1;
                    pd.ProductId = 1;
                    pd.Status = "A";
                    pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                    pd.FilterType = "NOAPPROVAL";
                    policyDetails.Add(pd);
                }

                //Case -6: PRE-VISA APPROVAL  --  AlwaysRequired
                if (preVisaAlways.Checked)
                {
                    PolicyDetails pd = new PolicyDetails();
                    pd.Category = (Category)Category.PREVISAAPPROVAL;
                    pd.Id = -1;
                    pd.ProductId = 1;
                    pd.Status = "A";
                    pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                    pd.FilterType = "ALWAYS";
                    policyDetails.Add(pd);
                }

                //Auto Ticketing
                PolicyDetails policyDetail = BindPolicyDetails((int)Category.AUTOTICKETING);                
                policyDetail.FilterType = "NOAPPROVAL";
                policyDetail.FilterValue1 = chkAutoTicketing.Checked ? "Y" : "N";
                policyDetails.Add(policyDetail);
                //COMPLETEBOOKING
                policyDetail = BindPolicyDetails((int)Category.COMPLETEBOOKING);
                policyDetail.FilterType = "NOAPPROVAL";
                policyDetail.FilterValue1 = chkCompleteBooking.Checked ? "Y" : "N";
                policyDetails.Add(policyDetail);
                //RESTRICTBOOKING
                policyDetail = BindPolicyDetails((int)Category.RESTRICTBOOKING);
                policyDetail.FilterType = "NOTHING";
                policyDetail.FilterValue1 = chkRestrictBooking.Checked ? "Y" : "N";
                policyDetails.Add(policyDetail);
                //AVOID TRANSITTIME
                policyDetail = BindPolicyDetails((int)Category.AVOIDTRANSITTIME);
                policyDetail.FilterType = "AVOIDTRANSITTIME";
                policyDetail.FilterValue1 = string.IsNullOrEmpty(txtTransitTime.Text) ? "0" : txtTransitTime.Text.Trim();
                policyDetails.Add(policyDetail);                
                //TOTALBOOKINGAMOUNT
                policyDetail = BindPolicyDetails((int)Category.TOTALBOOKINGAMOUNT);
                policyDetail.FilterType = "TOTALBOOKINGAMOUNT";
                policyDetail.FilterValue1 = string.IsNullOrEmpty(txtBookingAmount.Text) ? "0" : txtBookingAmount.Text.Trim();
                policyDetails.Add(policyDetail);
                //BOOKINGTHRESHOLD
                policyDetail = BindPolicyDetails((int)Category.BOOKINGTHRESHOLD);
                policyDetail.FilterType = "BOOKINGTHRESHOLD";
                policyDetail.FilterValue1 = string.IsNullOrEmpty(txtBookingThreshold.Text) ? "0" : txtBookingThreshold.Text.Trim();
                policyDetail.FilterValue3 = ddlBookingThreshold.SelectedValue;
                policyDetails.Add(policyDetail);

                #endregion


                #endregion

                #region Policy Expense Details List

                List<PolicyExpenseDetails> policyExpenseDetails = new List<PolicyExpenseDetails>();

                #region ExpenseList
                if (!string.IsNullOrEmpty(hdnCapExpense.Value) && hdnCapExpense.Value.Length > 0)
                {

                    string[] selExpenses = hdnCapExpense.Value.Split('|');
                    if (selExpenses.Length > 0)
                    {
                        //RowInfo
                        //{recordId^Country-City-ExpenseType-Currency-Amount-DailyActuals-TravelDays-Type}
                        foreach (string selExp in selExpenses)
                        {
                            PolicyExpenseDetails ped = new PolicyExpenseDetails();
                            int id;
                            int.TryParse(selExp.Split('^')[0], out id); //returns the id.
                            ped.ExpId = id;
                            ped.CountryCode = Convert.ToString(selExp.Split('^')[1].Split('-')[0]);
                            ped.CityCode = Convert.ToString(selExp.Split('^')[1].Split('-')[1]);
                            ped.SetupId = Convert.ToInt32(selExp.Split('^')[1].Split('-')[2]);
                            ped.Currency = Convert.ToString(selExp.Split('^')[1].Split('-')[3]);
                            ped.Amount = Convert.ToDecimal(selExp.Split('^')[1].Split('-')[4]);
                            if (Convert.ToString(selExp.Split('^')[1].Split('-')[6]) == "True")
                            {
                                ped.IncudeTravelDays = true;
                            }
                            else
                            {
                                ped.IncudeTravelDays = false;
                            }
                            ped.Type = Convert.ToString(selExp.Split('^')[1].Split('-')[7]);
                            ped.Status = "A";
                            ped.CreatedBy = (int)Settings.LoginInfo.UserID;
                            ped.DailyActuals = ddlExpDailyActuals.SelectedValue;
                            policyExpenseDetails.Add(ped);
                        }
                    }
                }
                #endregion

                #region AllowanceList
                if (!string.IsNullOrEmpty(hdnCapAllowance.Value) && hdnCapAllowance.Value.Length > 0)
                {

                    string[] selAllowances = hdnCapAllowance.Value.Split('|');
                    if (selAllowances.Length > 0)
                    {
                        //RowInfo
                        //{recordId^Country-City-Currency-Amount-TravelDays-Type}}
                        foreach (string selAllowance in selAllowances)
                        {
                            PolicyExpenseDetails ped = new PolicyExpenseDetails();
                            int id;
                            int.TryParse(selAllowance.Split('^')[0], out id); //returns the id.
                            ped.ExpId = id;
                            ped.CountryCode = Convert.ToString(selAllowance.Split('^')[1].Split('-')[0]);
                            ped.CityCode = Convert.ToString(selAllowance.Split('^')[1].Split('-')[1]);
                            ped.Currency = Convert.ToString(selAllowance.Split('^')[1].Split('-')[2]);
                            ped.Amount = Convert.ToDecimal(selAllowance.Split('^')[1].Split('-')[3]);
                            if (Convert.ToString(selAllowance.Split('^')[1].Split('-')[4]) == "True")
                            {
                                ped.IncudeTravelDays = true;
                            }
                            else
                            {
                                ped.IncudeTravelDays = false;
                            }
                            ped.Type = Convert.ToString(selAllowance.Split('^')[1].Split('-')[5]);
                            ped.Status = "A";
                            ped.CreatedBy = (int)Settings.LoginInfo.UserID;
                            ped.DailyActuals = ddlAllowanceDailyActuals.SelectedValue;
                            policyExpenseDetails.Add(ped);
                        }
                    }
                }
                #endregion

                #endregion

                pm.PolicyDetailsList = policyDetails;
                pm.PolicyExpenseDetailsList = policyExpenseDetails;

                pm.Save();
                Clear();
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = "Details Saved Successfully !";
            }
            else if (hdfMode.Value == "1")
            {

                PolicyMaster pm = new PolicyMaster(Utility.ToLong(hdfEMId.Value));
                pm.ModifiedBy = (int)Settings.LoginInfo.UserID;

                #region Policy Header Information
                pm.AgentId = Utility.ToInteger(ddlAgents.SelectedItem.Value);
                pm.Status = "A";
                pm.PolicyCode = string.Empty;
                pm.PolicyName = txtPolicyName.Text;

                List<DateTime> DatesToAvoid = new List<DateTime>();
                if (!string.IsNullOrEmpty(hdnFromDate.Value))
                {
                    string[] selFromDates = hdnFromDate.Value.Split('|');
                    if (selFromDates.Length > 0)
                    {
                        foreach (string date in selFromDates)
                        {

                            DateTime edate = DateTime.ParseExact(date.Replace('-', '/'), "dd/MM/yyyy", null);
                            DatesToAvoid.Add(Utility.ToDate(edate));
                        }
                        pm.DatesToAvoidList = DatesToAvoid;
                    }
                }
                else
                {
                    pm.DatesToAvoidList = null;

                }
                #endregion

                List<PolicyDetails> existingPolicyDetails = null;
                List<PolicyDetails> newpolicyDetails = new List<PolicyDetails>(); //This is for new policy details records insertion


                if (pm.PolicyDetailsList != null && pm.PolicyDetailsList.Count > 0)
                {
                    existingPolicyDetails = pm.PolicyDetailsList;
                }

                // While retrieving if there are existing policy details then this if block will be executed.
                if (existingPolicyDetails != null && existingPolicyDetails.Count > 0)
                {

                    #region DESTINATIONS TO AVOID
                    //Destinations to avoid
                    //Delete -- Destinations to avoid
                    if (!string.IsNullOrEmpty(hdnDelDestinationsToAvoid.Value))
                    {
                        string[] delDestinations = hdnDelDestinationsToAvoid.Value.Split('|');
                        if (delDestinations.Length > 0)
                        {
                            foreach (string delDest in delDestinations)
                            {
                                foreach (PolicyDetails epd in existingPolicyDetails)
                                {
                                    if (epd.Category == Category.DESTINATIONTOAVOID)
                                    {
                                        int id;
                                        int.TryParse(delDest.Split('^')[0], out id);
                                        if (id == epd.Id)
                                        {
                                            epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                            epd.Status = "D";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    //Update and insert -- Destinations to avoid
                    if (!string.IsNullOrEmpty(hdnDestinations.Value))
                    {
                        string[] selDestinations = hdnDestinations.Value.Split('|');
                        if (selDestinations.Length > 0)
                        {
                            foreach (string selDest in selDestinations)
                            {
                                int id;
                                int.TryParse(selDest.Split('^')[0], out id);
                                foreach (PolicyDetails epd in existingPolicyDetails)
                                {
                                    if (epd.Category == Category.DESTINATIONTOAVOID)
                                    {

                                        if (id == epd.Id) //Destination Policy Details updated
                                        {
                                            epd.ModifiedBy = (int)Settings.LoginInfo.UserID;

                                            //Eg1: -1^AF-BST-AvoidConnection:Yes
                                            //Eg2: -1^AF-BST-AvoidConnection:No
                                            if (selDest.Split('^')[1].Split('-').Length >= 3)
                                            {
                                                epd.FilterValue2 = selDest.Split('^')[1].Split('-')[0]; // Country Value is saved --used in edit mode.
                                                epd.FilterType = selDest.Split('^')[1].Split('-')[1];  //City Value
                                                epd.FilterValue1 = selDest.Split('^')[1].Split('-')[2].Split(':')[1];
                                            }

                                            //epd.FilterType = selDest.Split('^')[1].Split('-')[0] + "-" + selDest.Split('-')[1]; // Country - City
                                            //epd.FilterValue1 = selDest.Split('^')[1].Split('-')[2].Split(':')[1]; //AvoidConnection: Yes or No
                                        }
                                    } // Update -- Destinations to avoid
                                }

                                //New -- Destinations to avoid
                                if (id == -1)
                                {
                                    PolicyDetails npd = new PolicyDetails();
                                    npd.Id = id;
                                    npd.Category = (Category)Category.DESTINATIONTOAVOID;
                                    npd.ProductId = 1;
                                    npd.Status = "A";
                                    npd.CreatedBy = (int)Settings.LoginInfo.UserID;

                                    //Eg1: -1^AF-BST-AvoidConnection:Yes
                                    //Eg2: -1^AF-BST-AvoidConnection:No
                                    if (selDest.Split('^')[1].Split('-').Length >= 3)
                                    {
                                        npd.FilterValue2 = selDest.Split('^')[1].Split('-')[0]; // Country Value is saved --used in edit mode.
                                        npd.FilterType = selDest.Split('^')[1].Split('-')[1];  //City Value
                                        npd.FilterValue1 = selDest.Split('^')[1].Split('-')[2].Split(':')[1];
                                    }

                                    //npd.FilterType = selDest.Split('^')[1].Split('-')[0] + "-" + selDest.Split('^')[1].Split('-')[1]; // Country - City
                                    //npd.FilterValue1 = selDest.Split('^')[1].Split('-')[2].Split(':')[1]; //AvoidConnection: Yes or No
                                    newpolicyDetails.Add(npd);
                                }

                            }
                        }
                    }
                    #endregion

                    #region AIRLINES WITH NEGOTIATED FARES

                    if (!string.IsNullOrEmpty(hdnDelAirlinesNego.Value))
                    {
                        string[] delAirlinesNego = hdnDelAirlinesNego.Value.Split('|');
                        if (delAirlinesNego.Length > 0)
                        {
                            foreach (string delAirlineNego in delAirlinesNego)
                            {
                                foreach (PolicyDetails epd in existingPolicyDetails)
                                {
                                    if (epd.Category == Category.NEGOTIATEDFARES)
                                    {
                                        int id;
                                        int.TryParse(delAirlineNego.Split('^')[0], out id);
                                        if (id == epd.Id)
                                        {
                                            epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                            epd.Status = "D";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(hdnAirlineNegotiated.Value))
                    {
                        string[] selAirlinesNegotiated = hdnAirlineNegotiated.Value.Split('|');
                        if (selAirlinesNegotiated.Length > 0)
                        {
                            foreach (string selAirline in selAirlinesNegotiated)
                            {
                                int id;
                                int.TryParse(selAirline.Split('^')[0], out id);
                                foreach (PolicyDetails epd in existingPolicyDetails)
                                {
                                    if (epd.Category == Category.NEGOTIATEDFARES)
                                    {
                                        if (id == epd.Id) //Destination Policy Details updated
                                        {
                                            epd.ModifiedBy = (int)Settings.LoginInfo.UserID;

                                            if (selAirline.Split('^')[1].Split('-').Length >= 2)
                                            {
                                                epd.FilterType = selAirline.Split('^')[1].Split('-')[0];
                                                epd.FilterValue1 = selAirline.Split('^')[1].Split('-')[1];
                                            }
                                        }
                                    }
                                }
                                if (id == -1)
                                {
                                    PolicyDetails npd = new PolicyDetails();
                                    npd.Category = (Category)Category.NEGOTIATEDFARES;
                                    npd.Id = id;
                                    npd.ProductId = 1;
                                    npd.Status = "A";
                                    npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                                    if (selAirline.Split('^')[1].Split('-').Length >= 2)
                                    {
                                        npd.FilterType = selAirline.Split('^')[1].Split('-')[0];
                                        npd.FilterValue1 = selAirline.Split('^')[1].Split('-')[1];
                                    }
                                    newpolicyDetails.Add(npd);

                                }
                            }
                        }
                    }
                    #endregion

                    #region SECURITY RULES
                    if (!string.IsNullOrEmpty(hdnSecurityRules.Value))
                    {
                        if (hdnSecurityRules.Value.Contains("^"))
                        {
                            int id;
                            int.TryParse(hdnSecurityRules.Value.Split('^')[0], out id);
                            foreach (PolicyDetails epd in existingPolicyDetails)
                            {
                                if (epd.Category == Category.SECURITYRULES)
                                {
                                    if (id == epd.Id) //Destination Policy Details updated
                                    {
                                        epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                        if (hdnSecurityRules.Value.Split('^')[1].Split('|').Length >= 2)
                                        {
                                            epd.FilterType = hdnSecurityRules.Value.Split('^')[1].Split('|')[0];
                                            epd.FilterValue1 = hdnSecurityRules.Value.Split('^')[1].Split('|')[1];
                                        }

                                    }
                                }
                            }
                        }
                        else
                        {


                            PolicyDetails npd = new PolicyDetails();
                            npd.Category = (Category)Category.SECURITYRULES;
                            npd.Id = -1;
                            npd.ProductId = 1;
                            npd.Status = "A";
                            npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            if (hdnSecurityRules.Value.Split('|').Length >= 2)
                            {
                                npd.FilterType = hdnSecurityRules.Value.Split('|')[0];
                                npd.FilterValue1 = hdnSecurityRules.Value.Split('|')[1];
                            }
                            newpolicyDetails.Add(npd);

                        }
                    }
                    #endregion

                    #region PREFERRED AIRLINES
                    if (!string.IsNullOrEmpty(hdnDelPreferAirline.Value))
                    {
                        string[] delPreferAirlines = hdnDelPreferAirline.Value.Split('|');
                        if (delPreferAirlines.Length > 0)
                        {
                            foreach (string delPreferAirline in delPreferAirlines)
                            {
                                foreach (PolicyDetails epd in existingPolicyDetails)
                                {
                                    if (epd.Category == Category.PREFERREDAIRLINES)
                                    {
                                        int id;
                                        int.TryParse(delPreferAirline.Split('^')[0], out id);
                                        if (id == epd.Id)
                                        {
                                            epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                            epd.Status = "D";
                                        }
                                    }
                                }
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(hdnAirlinePrefered.Value))
                    {
                        string[] preferredAirlines = hdnAirlinePrefered.Value.Split('|');
                        if (preferredAirlines.Length > 0)
                        {
                            foreach (string prefAirline in preferredAirlines)
                            {
                                int id;
                                int.TryParse(prefAirline.Split('^')[0], out id);
                                foreach (PolicyDetails epd in existingPolicyDetails)
                                {
                                    if (epd.Category == Category.PREFERREDAIRLINES)
                                    {
                                        if (id == epd.Id) //Destination Policy Details updated
                                        {
                                            epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                            epd.FilterType = prefAirline.Split('^')[1];

                                        }
                                    }
                                }
                                if (id == -1)
                                {
                                    PolicyDetails npd = new PolicyDetails();
                                    npd.Category = (Category)Category.PREFERREDAIRLINES;
                                    npd.Id = -1;
                                    npd.ProductId = 1;
                                    npd.Status = "A";
                                    npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                                    npd.FilterType = prefAirline.Split('^')[1];
                                    newpolicyDetails.Add(npd);
                                }
                            }
                        }
                    }
                    #endregion

                    #region AIRLINES TO AVOID

                    if (!string.IsNullOrEmpty(hdnDelAvoidAirlines.Value))
                    {
                        string[] delAvoidAirlines = hdnDelAvoidAirlines.Value.Split('|');
                        if (delAvoidAirlines.Length > 0)
                        {
                            foreach (string delAvoidAirline in delAvoidAirlines)
                            {
                                foreach (PolicyDetails epd in existingPolicyDetails)
                                {
                                    if (epd.Category == Category.AIRLINESTOAVOID)
                                    {
                                        int id;
                                        int.TryParse(delAvoidAirline.Split('^')[0], out id);
                                        if (id == epd.Id)
                                        {
                                            epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                            epd.Status = "D";
                                        }
                                    }
                                }
                            }
                        }
                    }


                    if (!string.IsNullOrEmpty(hdnAirlineAvoid.Value))
                    {

                        string[] avoidAirlines = hdnAirlineAvoid.Value.Split('|');
                        if (avoidAirlines.Length > 0)
                        {
                            foreach (string avoidAirline in avoidAirlines)
                            {
                                int id;
                                int.TryParse(avoidAirline.Split('^')[0], out id);
                                foreach (PolicyDetails epd in existingPolicyDetails)
                                {
                                    if (epd.Category == Category.AIRLINESTOAVOID)
                                    {
                                        if (id == epd.Id)
                                        {
                                            epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                            epd.FilterType = avoidAirline.Split('^')[1];

                                        }
                                    }
                                }
                                if (id == -1)
                                {
                                    PolicyDetails npd = new PolicyDetails();
                                    npd.Category = (Category)Category.AIRLINESTOAVOID;
                                    npd.Id = id;
                                    npd.ProductId = 1;
                                    npd.Status = "A";
                                    npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                                    npd.FilterType = avoidAirline.Split('^')[1];
                                    newpolicyDetails.Add(npd);
                                }
                            }
                        }
                    }
                    #endregion

                    #region AIR FARE SHOPPING
                    if (!string.IsNullOrEmpty(hdnAirFareShopping.Value))
                    {
                        if (hdnAirFareShopping.Value.Contains("^"))
                        {
                            int id;
                            int.TryParse(hdnAirFareShopping.Value.Split('^')[0], out id);
                            foreach (PolicyDetails epd in existingPolicyDetails)
                            {
                                if (epd.Category == Category.AIRFARESHOPPING)
                                {
                                    if (id == epd.Id)
                                    {
                                        epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                        epd.FilterType = hdnAirFareShopping.Value.Split('^')[1];
                                    }
                                }
                            }
                        }
                        else
                        {

                            PolicyDetails npd = new PolicyDetails();
                            npd.Category = (Category)Category.AIRFARESHOPPING;
                            npd.Id = -1;
                            npd.ProductId = 1;
                            npd.Status = "A";
                            npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            npd.FilterType = hdnAirFareShopping.Value;
                            newpolicyDetails.Add(npd);

                        }
                    }
                    #endregion

                    #region DEFAULT CABIN TYPE
                    if (!string.IsNullOrEmpty(hdnDefaultCabin.Value))
                    {
                        if (hdnDefaultCabin.Value.Contains("^"))
                        {
                            foreach (PolicyDetails epd in existingPolicyDetails)
                            {
                                if (epd.Category == Category.DEFAULTCABIN)
                                {
                                    int id;
                                    int.TryParse(hdnDefaultCabin.Value.Split('^')[0], out id);
                                    if (id == epd.Id)
                                    {
                                        epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                        epd.FilterType = hdnDefaultCabin.Value.Split('^')[1];
                                    }
                                }
                            }
                        }
                        else
                        {
                            //updating DEFAULTCABIN by phani
                            var list = existingPolicyDetails.Where(m => m.Category == Category.DEFAULTCABIN).SingleOrDefault();
                            if (list != null)
                            {
                                list.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                list.FilterType = hdnDefaultCabin.Value;
                            }
                            else
                            {
                                PolicyDetails npd = new PolicyDetails();
                                npd.Category = (Category)Category.DEFAULTCABIN;
                                npd.Id = -1;
                                npd.ProductId = 1;
                                npd.Status = "A";
                                npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                                npd.FilterType = hdnDefaultCabin.Value;
                                npd.FilterValue3 = "4"; //Default Cabin Priority order     
                                newpolicyDetails.Add(npd);
                            }


                        }
                    }
                    #endregion

                    #region CABIN TYPE RULES
                    if (!string.IsNullOrEmpty(hdnDelCabinTypeException.Value))
                    {
                        string[] delCabinTypeExceptions = hdnDelCabinTypeException.Value.Split('|');
                        if (delCabinTypeExceptions.Length > 0)
                        {
                            foreach (string delCabinTypeException in delCabinTypeExceptions)
                            {
                                foreach (PolicyDetails epd in existingPolicyDetails)
                                {
                                    if (epd.Category == Category.EXCEPTIONCABIN)
                                    {
                                        int id;
                                        int.TryParse(delCabinTypeException.Split('^')[0], out id);
                                        if (id == epd.Id)
                                        {
                                            epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                            epd.Status = "D";
                                        }
                                    }
                                }
                            }
                        }
                    }


                    if (!string.IsNullOrEmpty(hdnCabinTypeException.Value))
                    {
                        string[] cabinTypeRules = hdnCabinTypeException.Value.Split('|');
                        if (cabinTypeRules.Length > 0)
                        {
                            foreach (string cabinTypeRule in cabinTypeRules)
                            {
                                int id;
                                int.TryParse(cabinTypeRule.Split('^')[0], out id);
                                foreach (PolicyDetails epd in existingPolicyDetails)
                                {
                                    if (epd.Category == Category.EXCEPTIONCABIN)
                                    {
                                        if (id == epd.Id)
                                        {
                                            epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                            if (cabinTypeRule.Split('^')[1].Split('-').Length >= 2)
                                            {
                                                epd.FilterType = cabinTypeRule.Split('^')[1].Split('-')[0];
                                                epd.FilterValue1 = cabinTypeRule.Split('^')[1].Split('-')[1];

                                                if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("DESTINATION"))
                                                {
                                                    epd.FilterValue3 = "1";
                                                }
                                                else if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("JOURNYTIME"))
                                                {
                                                    epd.FilterValue3 = "2";
                                                }
                                                else if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("FLIGHTDURATION"))
                                                {
                                                    epd.FilterValue3 = "3";
                                                }
                                                else if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("NOSEATS"))
                                                {
                                                    epd.FilterValue3 = "5";
                                                }
                                                if (cabinTypeRule.Split('^')[1].Split('-').Length > 2)
                                                {
                                                    epd.FilterValue2 = cabinTypeRule.Split('^')[1].Split('-')[2];
                                                    epd.FilterValue4 = cabinTypeRule.Split('^')[1].Split('-')[3];
                                                }
                                            }
                                        }
                                    }
                                }
                                if (id == -1)
                                {
                                    PolicyDetails npd = new PolicyDetails();
                                    npd.Category = (Category)Category.EXCEPTIONCABIN;
                                    npd.Id = -1;
                                    npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                                    npd.ProductId = 1;
                                    npd.Status = "A";
                                    if (cabinTypeRule.Split('^')[1].Split('-').Length >= 2)
                                    {
                                        npd.FilterType = cabinTypeRule.Split('^')[1].Split('-')[0];
                                        npd.FilterValue1 = cabinTypeRule.Split('^')[1].Split('-')[1];

                                        if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("DESTINATION"))
                                        {
                                            npd.FilterValue3 = "1";
                                        }
                                        else if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("JOURNYTIME"))
                                        {
                                            npd.FilterValue3 = "2";
                                        }
                                        else if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("FLIGHTDURATION"))
                                        {
                                            npd.FilterValue3 = "3";
                                        }
                                        else if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("NOSEATS"))
                                        {
                                            npd.FilterValue3 = "5";
                                        }
                                        if (cabinTypeRule.Split('^')[1].Split('-').Length > 2)
                                        {
                                            npd.FilterValue2 = cabinTypeRule.Split('^')[1].Split('-')[2];
                                            npd.FilterValue4 = cabinTypeRule.Split('^')[1].Split('-')[3];
                                        }
                                    }
                                    newpolicyDetails.Add(npd);
                                }

                            }
                        }
                    }

                    #endregion

                    #region ApprovalTab

                    int preTripN = 0;//PreTrip --NOAPPROVAL
                    int preTripA = 0; //PreTrip --Always
                    int preTripCE = 0;//PreTrip -- COSTEXCEEDS
                    int preLaunchCE = 0;//PreLaunch -- COSTEXCEEDS
                    int preLaunchIC = 0;//PreLaunch -- Itinerary change
                    int preVisaN = 0;//PreVisa --NOAPPROVAL
                    int preVisaA = 0; //PreVisa --Always


                    foreach (PolicyDetails epd in existingPolicyDetails)
                    {
                        if (epd.Category == Category.PRETRIPAPPROVAL && epd.FilterType == "NOAPPROVAL")
                        {
                            preTripN++;
                            //Case -1: PRE-TRIP APPROVAL  --  NOAPPROVAL
                            if (preTripNoApproval.Checked)
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "A";
                            }
                            else
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "D";

                            }
                        }
                        if (epd.Category == Category.PRETRIPAPPROVAL && epd.FilterType == "ALWAYS")
                        {
                            preTripA++;
                            //Case -1: PRE-TRIP APPROVAL  --  AlwaysRequired
                            if (preTripAlways.Checked)
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "A";
                            }
                            else
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "D";

                            }
                        }
                        if (epd.Category == Category.PRETRIPAPPROVAL && epd.FilterType == "COSTEXCEEDS")
                        {
                            preTripCE++;
                            //Case -2: PRE-TRIP APPROVAL  -- if trip cost exceeds
                            if (preTripCostExceeds.Checked)
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "A";
                                epd.FilterValue1 = ddlPreTripCurrency.SelectedItem.Value;
                                epd.FilterValue2 = txtPreTripAmount.Text;
                            }
                            else
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "D";
                            }
                        }
                        if (epd.Category == Category.RELAUNCHAPPROVAL && epd.FilterType == "COSTEXCEEDS")
                        {
                            preLaunchCE++;

                            //Case -3: RELAUNCH APPROVAL PROCESS  -- if price change exceeds
                            if (chkRelaunchPriceChange.Checked)
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "A";
                                epd.FilterValue1 = ddlRelaunchPriceChange.SelectedItem.Value;
                                epd.FilterValue2 = txtRelaunchPriceAmount.Text;
                            }
                            else
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "D";
                            }
                        }
                        if (epd.Category == Category.RELAUNCHAPPROVAL && epd.FilterType == "CHANGEITINERARY")
                        {
                            preLaunchIC++;
                            //Case -4: RELAUNCH APPROVAL PROCESS  --  itinerary change
                            if (chkRelaunchItineraryChange.Checked)
                            {

                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "A";
                            }
                            else
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "D";
                            }

                        }
                        if (epd.Category == Category.PREVISAAPPROVAL && epd.FilterType == "NOAPPROVAL")
                        {
                            preVisaN++;
                            //Case -5: PREVISAAPPROVAL  --  NOAPPROVAL
                            if (preVisaNoApproval.Checked)
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "A";
                            }
                            else
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "D";

                            }
                        }
                        if (epd.Category == Category.PREVISAAPPROVAL && epd.FilterType == "ALWAYS")
                        {
                            preVisaA++;
                            //Case -6: PREVISAAPPROVAL  --  AlwaysRequired
                            if (preVisaAlways.Checked)
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "A";
                            }
                            else
                            {
                                epd.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                epd.Status = "D";

                            }
                        }                         
                    }

                    if (preTripN == 0)
                    {
                        //Case -0: PRE-TRIP APPROVAL  --  NoApproval
                        if (preTripNoApproval.Checked)
                        {
                            PolicyDetails pd = new PolicyDetails();
                            pd.Category = (Category)Category.PRETRIPAPPROVAL;
                            pd.Id = -1;
                            pd.ProductId = 1;
                            pd.Status = "A";
                            pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            pd.FilterType = "NOAPPROVAL";
                            newpolicyDetails.Add(pd);
                        }
                    }
                    if (preTripA == 0)
                    {
                        //Case -1: PRE-TRIP APPROVAL  --  AlwaysRequired
                        if (preTripAlways.Checked)
                        {
                            PolicyDetails pd = new PolicyDetails();
                            pd.Category = (Category)Category.PRETRIPAPPROVAL;
                            pd.Id = -1;
                            pd.ProductId = 1;
                            pd.Status = "A";
                            pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            pd.FilterType = "ALWAYS";
                            newpolicyDetails.Add(pd);
                        }
                    }
                    if (preTripCE == 0)
                    {
                        //Case -2: PRE-TRIP APPROVAL  -- if trip cost exceeds
                        if (preTripCostExceeds.Checked)
                        {
                            PolicyDetails pd = new PolicyDetails();
                            pd.Category = (Category)Category.PRETRIPAPPROVAL;
                            pd.Id = -1;
                            pd.ProductId = 1;
                            pd.Status = "A";
                            pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            pd.FilterType = "COSTEXCEEDS";
                            pd.FilterValue1 = ddlPreTripCurrency.SelectedItem.Value;
                            pd.FilterValue2 = txtPreTripAmount.Text;
                            newpolicyDetails.Add(pd);
                        }
                    }
                    if (preLaunchCE == 0)
                    {
                        //Case -3: RELAUNCH APPROVAL PROCESS  -- if price change exceeds
                        if (chkRelaunchPriceChange.Checked)
                        {
                            PolicyDetails pd = new PolicyDetails();
                            pd.Category = (Category)Category.RELAUNCHAPPROVAL;
                            pd.Id = -1;
                            pd.ProductId = 1;
                            pd.Status = "A";
                            pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            pd.FilterType = "COSTEXCEEDS";
                            pd.FilterValue1 = ddlRelaunchPriceChange.SelectedItem.Value;
                            pd.FilterValue2 = txtRelaunchPriceAmount.Text;
                            newpolicyDetails.Add(pd);
                        }

                    }
                    if (preLaunchIC == 0)
                    {
                        //Case -4: RELAUNCH APPROVAL PROCESS  --  itinerary change
                        if (chkRelaunchItineraryChange.Checked)
                        {
                            PolicyDetails pd = new PolicyDetails();
                            pd.Category = (Category)Category.RELAUNCHAPPROVAL;
                            pd.Id = -1;
                            pd.ProductId = 1;
                            pd.Status = "A";
                            pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            pd.FilterType = "CHANGEITINERARY";
                            newpolicyDetails.Add(pd);
                        }
                    }
                    if (preVisaN == 0)
                    {
                        //Case -5: PRE-VISA APPROVAL  --  NoApproval
                        if (preVisaNoApproval.Checked)
                        {
                            PolicyDetails pd = new PolicyDetails();
                            pd.Category = (Category)Category.PREVISAAPPROVAL;
                            pd.Id = -1;
                            pd.ProductId = 1;
                            pd.Status = "A";
                            pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            pd.FilterType = "NOAPPROVAL";
                            newpolicyDetails.Add(pd);
                        }
                    }
                    if (preVisaA == 0)
                    {
                        //Case -6: PRE-VISA APPROVAL  --  AlwaysRequired
                        if (preVisaAlways.Checked)
                        {
                            PolicyDetails pd = new PolicyDetails();
                            pd.Category = (Category)Category.PREVISAAPPROVAL;
                            pd.Id = -1;
                            pd.ProductId = 1;
                            pd.Status = "A";
                            pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                            pd.FilterType = "ALWAYS";
                            newpolicyDetails.Add(pd);
                        }
                    }                   
                    #endregion
                }
                else // While retrieving if there are no policy details then this else block will be executed.
                {
                    #region DESTINATIONS TO AVOID
                    if (!string.IsNullOrEmpty(hdnDestinations.Value))
                    {
                        string[] selDestinations = hdnDestinations.Value.Split('|');
                        if (selDestinations.Length > 0)
                        {
                            foreach (string selDest in selDestinations)
                            {
                                int id;
                                int.TryParse(selDest.Split('^')[0], out id);
                                PolicyDetails npd = new PolicyDetails();
                                npd.Id = id;
                                npd.Category = (Category)Category.DESTINATIONTOAVOID;
                                npd.ProductId = 1;
                                npd.Status = "A";
                                npd.CreatedBy = (int)Settings.LoginInfo.UserID;

                                //Eg1: -1^AF-BST-AvoidConnection:Yes
                                //Eg2: -1^AF-BST-AvoidConnection:No

                                if (selDest.Split('^')[1].Split('-').Length >= 3)
                                {
                                    npd.FilterValue2 = selDest.Split('^')[1].Split('-')[0]; // Country Value is saved --used in edit mode.
                                    npd.FilterType = selDest.Split('^')[1].Split('-')[1];  //City Value
                                    npd.FilterValue1 = selDest.Split('^')[1].Split('-')[2].Split(':')[1];
                                }

                                // npd.FilterType = selDest.Split('^')[1].Split('-')[0] + "-" + selDest.Split('^')[1].Split('-')[1]; // Country - City
                                // npd.FilterValue1 = selDest.Split('^')[1].Split('-')[2].Split(':')[1]; //AvoidConnection: Yes or No
                                newpolicyDetails.Add(npd);
                            }
                        }
                    }

                    #endregion

                    #region AIRLINES WITH NEGOTIATED FARES
                    if (!string.IsNullOrEmpty(hdnAirlineNegotiated.Value))
                    {
                        string[] selAirlinesNegotiated = hdnAirlineNegotiated.Value.Split('|');
                        if (selAirlinesNegotiated.Length > 0)
                        {
                            foreach (string selAirline in selAirlinesNegotiated)
                            {
                                int id;
                                int.TryParse(selAirline.Split('^')[0], out id);
                                PolicyDetails npd = new PolicyDetails();
                                npd.Category = (Category)Category.NEGOTIATEDFARES;
                                npd.Id = id;
                                npd.ProductId = 1;
                                npd.Status = "A";
                                npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                                npd.FilterType = selAirline.Split('^')[1].Split('-')[0];
                                npd.FilterValue1 = selAirline.Split('^')[1].Split('-')[1];
                                newpolicyDetails.Add(npd);
                            }
                        }
                    }
                    #endregion

                    #region SECURITY RULES
                    if (!string.IsNullOrEmpty(hdnSecurityRules.Value))
                    {
                        PolicyDetails npd = new PolicyDetails();
                        npd.Category = (Category)Category.SECURITYRULES;
                        npd.Id = -1;
                        npd.ProductId = 1;
                        npd.Status = "A";
                        npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                        if (hdnSecurityRules.Value.Split('|').Length >= 2)
                        {
                            npd.FilterType = hdnSecurityRules.Value.Split('|')[0];
                            npd.FilterValue1 = hdnSecurityRules.Value.Split('|')[1];
                        }
                        newpolicyDetails.Add(npd);
                    }
                    #endregion

                    #region PREFERRED AIRLINES
                    if (!string.IsNullOrEmpty(hdnAirlinePrefered.Value))
                    {
                        string[] preferredAirlines = hdnAirlinePrefered.Value.Split('|');
                        if (preferredAirlines.Length > 0)
                        {
                            foreach (string prefAirline in preferredAirlines)
                            {
                                PolicyDetails npd = new PolicyDetails();
                                npd.Category = (Category)Category.PREFERREDAIRLINES;
                                npd.Id = -1;
                                npd.ProductId = 1;
                                npd.Status = "A";
                                npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                                npd.FilterType = prefAirline.Split('^')[1];
                                newpolicyDetails.Add(npd);
                            }
                        }
                    }
                    #endregion

                    #region AIRLINES TO AVOID

                    if (!string.IsNullOrEmpty(hdnAirlineAvoid.Value))
                    {

                        string[] avoidAirlines = hdnAirlineAvoid.Value.Split('|');
                        if (avoidAirlines.Length > 0)
                        {
                            foreach (string avoidAirline in avoidAirlines)
                            {
                                PolicyDetails npd = new PolicyDetails();
                                npd.Category = (Category)Category.AIRLINESTOAVOID;
                                npd.Id = -1;
                                npd.ProductId = 1;
                                npd.Status = "A";
                                npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                                npd.FilterType = avoidAirline.Split('^')[1];
                                newpolicyDetails.Add(npd);
                            }
                        }
                    }
                    #endregion

                    #region AIR FARE SHOPPING
                    if (!string.IsNullOrEmpty(hdnAirFareShopping.Value))
                    {
                        PolicyDetails npd = new PolicyDetails();
                        npd.Category = (Category)Category.AIRFARESHOPPING;
                        npd.Id = -1;
                        npd.ProductId = 1;
                        npd.Status = "A";
                        npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                        npd.FilterType = hdnAirFareShopping.Value;
                        newpolicyDetails.Add(npd);
                    }
                    #endregion

                    #region DEFAULT CABIN TYPE
                    if (!string.IsNullOrEmpty(hdnDefaultCabin.Value))
                    {
                        PolicyDetails npd = new PolicyDetails();
                        npd.Category = (Category)Category.DEFAULTCABIN;
                        npd.Id = -1;
                        npd.ProductId = 1;
                        npd.Status = "A";
                        npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                        npd.FilterType = hdnDefaultCabin.Value;
                        npd.FilterValue3 = "4";
                        newpolicyDetails.Add(npd);
                    }
                    #endregion

                    #region CABIN TYPE RULES

                    if (!string.IsNullOrEmpty(hdnCabinTypeException.Value))
                    {
                        string[] cabinTypeRules = hdnCabinTypeException.Value.Split('|');
                        if (cabinTypeRules.Length > 0)
                        {
                            foreach (string cabinTypeRule in cabinTypeRules)
                            {
                                PolicyDetails npd = new PolicyDetails();
                                npd.Category = (Category)Category.EXCEPTIONCABIN;
                                npd.Id = -1;
                                npd.CreatedBy = (int)Settings.LoginInfo.UserID;
                                npd.ProductId = 1;
                                npd.Status = "A";
                                if (cabinTypeRule.Split('^')[1].Split('-').Length >= 2)
                                {
                                    npd.FilterType = cabinTypeRule.Split('^')[1].Split('-')[0];
                                    npd.FilterValue1 = cabinTypeRule.Split('^')[1].Split('-')[1];

                                    if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("DESTINATION"))
                                    {
                                        npd.FilterValue3 = "1";
                                    }
                                    else if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("JOURNYTIME"))
                                    {
                                        npd.FilterValue3 = "2";
                                    }
                                    else if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("FLIGHTDURATION"))
                                    {
                                        npd.FilterValue3 = "3";
                                    }
                                    else if (cabinTypeRule.Split('^')[1].Split('-')[1].Contains("NOSEATS"))
                                    {
                                        npd.FilterValue3 = "5";
                                    }

                                    if (cabinTypeRule.Split('^')[1].Split('-').Length == 3)
                                    {
                                        npd.FilterValue2 = cabinTypeRule.Split('^')[1].Split('-')[2];
                                    }
                                }
                                newpolicyDetails.Add(npd);

                            }
                        }
                    }

                    #endregion

                    #region ApprovalTab

                    //Lokesh : 31May2017 Approval Tab.

                    //Case -1: PRE-TRIP APPROVAL  --  NOAPPROVAL
                    if (preTripNoApproval.Checked)
                    {
                        PolicyDetails pd = new PolicyDetails();
                        pd.Category = (Category)Category.PRETRIPAPPROVAL;
                        pd.Id = -1;
                        pd.ProductId = 1;
                        pd.Status = "A";
                        pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                        pd.FilterType = "NOAPPROVAL";
                        newpolicyDetails.Add(pd);
                    }
                    //Case -1: PRE-TRIP APPROVAL  --  AlwaysRequired

                    if (preTripAlways.Checked)
                    {
                        PolicyDetails pd = new PolicyDetails();
                        pd.Category = (Category)Category.PRETRIPAPPROVAL;
                        pd.Id = -1;
                        pd.ProductId = 1;
                        pd.Status = "A";
                        pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                        pd.FilterType = "ALWAYS";
                        newpolicyDetails.Add(pd);
                    }

                    //Case -2: PRE-TRIP APPROVAL  -- if trip cost exceeds
                    if (preTripCostExceeds.Checked)
                    {
                        PolicyDetails pd = new PolicyDetails();
                        pd.Category = (Category)Category.PRETRIPAPPROVAL;
                        pd.Id = -1;
                        pd.ProductId = 1;
                        pd.Status = "A";
                        pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                        pd.FilterType = "COSTEXCEEDS";
                        pd.FilterValue1 = ddlPreTripCurrency.SelectedItem.Value;
                        pd.FilterValue2 = txtPreTripAmount.Text;
                        newpolicyDetails.Add(pd);
                    }


                    //Case -3: RELAUNCH APPROVAL PROCESS  -- if price change exceeds
                    if (chkRelaunchPriceChange.Checked)
                    {
                        PolicyDetails pd = new PolicyDetails();
                        pd.Category = (Category)Category.RELAUNCHAPPROVAL;
                        pd.Id = -1;
                        pd.ProductId = 1;
                        pd.Status = "A";
                        pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                        pd.FilterType = "COSTEXCEEDS";
                        pd.FilterValue1 = ddlRelaunchPriceChange.SelectedItem.Value;
                        pd.FilterValue2 = txtRelaunchPriceAmount.Text;
                        newpolicyDetails.Add(pd);
                    }


                    //Case -4: RELAUNCH APPROVAL PROCESS  --  itinerary change
                    if (chkRelaunchItineraryChange.Checked)
                    {
                        PolicyDetails pd = new PolicyDetails();
                        pd.Category = (Category)Category.RELAUNCHAPPROVAL;
                        pd.Id = -1;
                        pd.ProductId = 1;
                        pd.Status = "A";
                        pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                        pd.FilterType = "CHANGEITINERARY";
                        newpolicyDetails.Add(pd);
                    }

                    //Case -5: PRE-VISA APPROVAL  --  NOAPPROVAL
                    if (preVisaNoApproval.Checked)
                    {
                        PolicyDetails pd = new PolicyDetails();
                        pd.Category = (Category)Category.PREVISAAPPROVAL;
                        pd.Id = -1;
                        pd.ProductId = 1;
                        pd.Status = "A";
                        pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                        pd.FilterType = "NOAPPROVAL";
                        newpolicyDetails.Add(pd);
                    }
                    //Case -6: PRE-TRIP APPROVAL  --  AlwaysRequired

                    if (preVisaAlways.Checked)
                    {
                        PolicyDetails pd = new PolicyDetails();
                        pd.Category = (Category)Category.PREVISAAPPROVAL;
                        pd.Id = -1;
                        pd.ProductId = 1;
                        pd.Status = "A";
                        pd.CreatedBy = (int)Settings.LoginInfo.UserID;
                        pd.FilterType = "ALWAYS";
                        newpolicyDetails.Add(pd);
                    }


                    #endregion                    
                }

                if (existingPolicyDetails != null && existingPolicyDetails.Count > 0)
                {
                    if (newpolicyDetails != null && newpolicyDetails.Count > 0)
                    {
                        existingPolicyDetails.AddRange(newpolicyDetails);
                    }
                    pm.PolicyDetailsList = existingPolicyDetails;
                }
                else
                {
                    if (newpolicyDetails != null && newpolicyDetails.Count > 0)
                    {
                        pm.PolicyDetailsList = newpolicyDetails;
                    }
                }

                if (pm.PolicyDetailsList != null && pm.PolicyDetailsList.Count>0)
                {
                    (from policy in pm.PolicyDetailsList
                     select policy).ToList().ForEach((x) =>
                     {
                         if (x.Category == Category.AUTOTICKETING && x.FilterType.ToUpper() == "NOAPPROVAL")
                             x.FilterValue1 = chkAutoTicketing.Checked ? "Y" : "N";
                         else if (x.Category == Category.COMPLETEBOOKING && x.FilterType.ToUpper() == "NOAPPROVAL")
                             x.FilterValue1 = chkCompleteBooking.Checked ? "Y" : "N";
                         else if (x.Category == Category.RESTRICTBOOKING && x.FilterType.ToUpper() == "NOTHING")
                             x.FilterValue1 = chkRestrictBooking.Checked ? "Y" : "N";
                         else if (x.Category == Category.AVOIDTRANSITTIME && x.FilterType == "AVOIDTRANSITTIME")
                             x.FilterValue1 = string.IsNullOrEmpty(txtTransitTime.Text) ? "0" : txtTransitTime.Text.Trim();
                         else if (x.Category == Category.TOTALBOOKINGAMOUNT && x.FilterType == "TOTALBOOKINGAMOUNT")
                             x.FilterValue1 = string.IsNullOrEmpty(txtBookingAmount.Text) ? "0" : txtBookingAmount.Text.Trim();
                         else if (x.Category == Category.BOOKINGTHRESHOLD && x.FilterType == "BOOKINGTHRESHOLD")
                         {
                             x.FilterValue1 = string.IsNullOrEmpty(txtBookingThreshold.Text) ? "0" : txtBookingThreshold.Text.Trim();
                             x.FilterValue3 = ddlBookingThreshold.SelectedValue;
                         }

                         x.ModifiedBy = (int)Settings.LoginInfo.UserID;
                     });

                    PolicyDetails policyDetail = null;
                    //Auto Ticketing.
                    policyDetail = pm.PolicyDetailsList.Where(x => x.Category == Category.AUTOTICKETING && x.FilterType.ToUpper() == "NOAPPROVAL").FirstOrDefault();
                    if (policyDetail == null) 
                    {
                        policyDetail = BindPolicyDetails((int)Category.AUTOTICKETING);
                        policyDetail.FilterType = "NOAPPROVAL";
                        policyDetail.FilterValue1 = chkAutoTicketing.Checked ? "Y" : "N";
                        pm.PolicyDetailsList.Add(policyDetail);
                    }
                    //COMPLETEBOOKING
                    policyDetail = pm.PolicyDetailsList.Where(x => x.Category == Category.COMPLETEBOOKING && x.FilterType.ToUpper() == "NOAPPROVAL").FirstOrDefault();
                    if (policyDetail == null) 
                    {
                        policyDetail = BindPolicyDetails((int)Category.COMPLETEBOOKING);
                        policyDetail.FilterType = "NOAPPROVAL";
                        policyDetail.FilterValue1 = chkCompleteBooking.Checked ? "Y" : "N";
                        pm.PolicyDetailsList.Add(policyDetail);
                    }
                    //RESTRICTBOOKING
                    policyDetail = pm.PolicyDetailsList.Where(x => x.Category == Category.RESTRICTBOOKING && x.FilterType.ToUpper() == "NOTHING").FirstOrDefault();
                    if (policyDetail == null) 
                    {
                        policyDetail = BindPolicyDetails((int)Category.RESTRICTBOOKING);
                        policyDetail.FilterType = "NOTHING";
                        policyDetail.FilterValue1 = chkCompleteBooking.Checked ? "Y" : "N";
                        pm.PolicyDetailsList.Add(policyDetail);
                    }
                    //AVOIDTRANSITTIME
                    policyDetail = pm.PolicyDetailsList.Where(x => x.Category == Category.AVOIDTRANSITTIME && x.FilterType.ToUpper() == "AVOIDTRANSITTIME").FirstOrDefault();
                    if (policyDetail == null)
                    {
                        policyDetail = BindPolicyDetails((int)Category.AVOIDTRANSITTIME);
                        policyDetail.FilterType = "AVOIDTRANSITTIME";
                        policyDetail.FilterValue1 = string.IsNullOrEmpty(txtTransitTime.Text) ? "0" : txtTransitTime.Text.Trim();
                        pm.PolicyDetailsList.Add(policyDetail);
                    }
                    //TOTALBOOKINGAMOUNT
                    policyDetail = pm.PolicyDetailsList.Where(x => x.Category == Category.TOTALBOOKINGAMOUNT && x.FilterType.ToUpper() == "TOTALBOOKINGAMOUNT").FirstOrDefault();
                    if (policyDetail == null)
                    {
                        policyDetail = BindPolicyDetails((int)Category.TOTALBOOKINGAMOUNT);
                        policyDetail.FilterType = "TOTALBOOKINGAMOUNT";
                        policyDetail.FilterValue1 = string.IsNullOrEmpty(txtBookingAmount.Text) ? "0" : txtBookingAmount.Text.Trim();
                        pm.PolicyDetailsList.Add(policyDetail);
                    }
                    //BOOKINGTHRESHOLD
                    policyDetail = pm.PolicyDetailsList.Where(x => x.Category == Category.BOOKINGTHRESHOLD && x.FilterType.ToUpper() == "BOOKINGTHRESHOLD").FirstOrDefault();
                    if (policyDetail == null)
                    {
                        policyDetail = BindPolicyDetails((int)Category.BOOKINGTHRESHOLD);
                        policyDetail.FilterType = "BOOKINGTHRESHOLD";
                        policyDetail.FilterValue1 = string.IsNullOrEmpty(txtBookingThreshold.Text) ? "0" : txtBookingThreshold.Text.Trim();
                        policyDetail.FilterValue3 = ddlBookingThreshold.SelectedValue;
                        pm.PolicyDetailsList.Add(policyDetail);
                    }
                }

                //Expense Tab Details Updation

                List<PolicyExpenseDetails> existingPolicyExpenseDetails = null;
                //This is for new policy expense details records insertion
                List<PolicyExpenseDetails> newpolicyExpenseDetails = new List<PolicyExpenseDetails>();


                if (pm.PolicyExpenseDetailsList != null && pm.PolicyExpenseDetailsList.Count > 0)
                {
                    existingPolicyExpenseDetails = pm.PolicyExpenseDetailsList;
                }
                if (existingPolicyExpenseDetails != null && existingPolicyExpenseDetails.Count > 0)
                {
                    if (!string.IsNullOrEmpty(hdnDelCapExpenseAllowance.Value))
                    {
                        string[] delCapExpense = hdnDelCapExpenseAllowance.Value.Split('|');
                        if (delCapExpense.Length > 0)
                        {
                            foreach (string delCapExp in delCapExpense)
                            {
                                foreach (PolicyExpenseDetails eped in existingPolicyExpenseDetails)
                                {

                                    int id;
                                    int.TryParse(delCapExp.Split('^')[0], out id);
                                    if (id == eped.ExpId)
                                    {
                                        eped.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                        eped.Status = "D";
                                    }
                                }
                            }
                        }
                    }

                    #region ExpenseList
                    if (!string.IsNullOrEmpty(hdnCapExpense.Value) && hdnCapExpense.Value.Length > 0)
                    {

                        string[] selExpenses = hdnCapExpense.Value.Split('|');
                        if (selExpenses.Length > 0)
                        {
                            //RowInfo
                            //{recordId^Country-City-ExpenseType-Currency-Amount-DailyActuals-TravelDays-Type}
                            foreach (string selExp in selExpenses)
                            {
                                int id;
                                int.TryParse(selExp.Split('^')[0], out id);
                                if (id < 0)
                                {
                                    PolicyExpenseDetails ped = new PolicyExpenseDetails();
                                    ped.ExpId = id;
                                    ped.CountryCode = Convert.ToString(selExp.Split('^')[1].Split('-')[0]);
                                    ped.CityCode = Convert.ToString(selExp.Split('^')[1].Split('-')[1]);
                                    ped.SetupId = Convert.ToInt32(selExp.Split('^')[1].Split('-')[2]);
                                    ped.Currency = Convert.ToString(selExp.Split('^')[1].Split('-')[3]);
                                    ped.Amount = Convert.ToDecimal(selExp.Split('^')[1].Split('-')[4]);
                                    if (Convert.ToString(selExp.Split('^')[1].Split('-')[6]) == "True")
                                    {
                                        ped.IncudeTravelDays = true;
                                    }
                                    else
                                    {
                                        ped.IncudeTravelDays = false;
                                    }
                                    ped.Type = Convert.ToString(selExp.Split('^')[1].Split('-')[7]);
                                    ped.Status = "A";
                                    ped.CreatedBy = (int)Settings.LoginInfo.UserID;
                                    ped.DailyActuals = ddlExpDailyActuals.SelectedValue;
                                    newpolicyExpenseDetails.Add(ped);
                                }
                                else
                                {

                                    foreach (PolicyExpenseDetails ped in existingPolicyExpenseDetails)
                                    {
                                        if (ped.ExpId == id)
                                        {
                                            ped.ExpId = id;
                                            ped.CountryCode = Convert.ToString(selExp.Split('^')[1].Split('-')[0]);
                                            ped.CityCode = Convert.ToString(selExp.Split('^')[1].Split('-')[1]);
                                            ped.SetupId = Convert.ToInt32(selExp.Split('^')[1].Split('-')[2]);
                                            ped.Currency = Convert.ToString(selExp.Split('^')[1].Split('-')[3]);
                                            ped.Amount = Convert.ToDecimal(selExp.Split('^')[1].Split('-')[4]);
                                            if (Convert.ToString(selExp.Split('^')[1].Split('-')[6]) == "True")
                                            {
                                                ped.IncudeTravelDays = true;
                                            }
                                            else
                                            {
                                                ped.IncudeTravelDays = false;
                                            }
                                            ped.Type = Convert.ToString(selExp.Split('^')[1].Split('-')[7]);
                                            ped.Status = "A";
                                            ped.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                            ped.DailyActuals = ddlExpDailyActuals.SelectedValue;
                                        }
                                    }

                                }

                            }
                        }
                    }
                    #endregion

                    #region AllowanceList
                    if (!string.IsNullOrEmpty(hdnCapAllowance.Value) && hdnCapAllowance.Value.Length > 0)
                    {

                        string[] selAllowances = hdnCapAllowance.Value.Split('|');
                        if (selAllowances.Length > 0)
                        {
                            //RowInfo
                            //{recordId^Country-City-Currency-Amount-TravelDays-Type}}
                            foreach (string selAllowance in selAllowances)
                            {
                                int id;
                                int.TryParse(selAllowance.Split('^')[0], out id);
                                if (id < 0)
                                {
                                    PolicyExpenseDetails ped = new PolicyExpenseDetails();
                                    //returns the id.
                                    ped.ExpId = id;
                                    ped.CountryCode = Convert.ToString(selAllowance.Split('^')[1].Split('-')[0]);
                                    ped.CityCode = Convert.ToString(selAllowance.Split('^')[1].Split('-')[1]);
                                    ped.Currency = Convert.ToString(selAllowance.Split('^')[1].Split('-')[2]);
                                    ped.Amount = Convert.ToDecimal(selAllowance.Split('^')[1].Split('-')[3]);
                                    if (Convert.ToString(selAllowance.Split('^')[1].Split('-')[4]) == "True")
                                    {
                                        ped.IncudeTravelDays = true;
                                    }
                                    else
                                    {
                                        ped.IncudeTravelDays = false;
                                    }
                                    ped.Type = Convert.ToString(selAllowance.Split('^')[1].Split('-')[5]);
                                    ped.Status = "A";
                                    ped.CreatedBy = (int)Settings.LoginInfo.UserID;
                                    ped.DailyActuals = ddlAllowanceDailyActuals.SelectedValue;
                                    newpolicyExpenseDetails.Add(ped);
                                }
                                else
                                {
                                    foreach (PolicyExpenseDetails ped in existingPolicyExpenseDetails)
                                    {
                                        if (ped.ExpId == id)
                                        {
                                            ped.ExpId = id;
                                            ped.CountryCode = Convert.ToString(selAllowance.Split('^')[1].Split('-')[0]);
                                            ped.CityCode = Convert.ToString(selAllowance.Split('^')[1].Split('-')[1]);
                                            ped.Currency = Convert.ToString(selAllowance.Split('^')[1].Split('-')[2]);
                                            ped.Amount = Convert.ToDecimal(selAllowance.Split('^')[1].Split('-')[3]);
                                            if (Convert.ToString(selAllowance.Split('^')[1].Split('-')[4]) == "True")
                                            {
                                                ped.IncudeTravelDays = true;
                                            }
                                            else
                                            {
                                                ped.IncudeTravelDays = false;
                                            }
                                            ped.Type = Convert.ToString(selAllowance.Split('^')[1].Split('-')[5]);
                                            ped.Status = "A";
                                            ped.ModifiedBy = (int)Settings.LoginInfo.UserID;
                                            ped.DailyActuals = ddlAllowanceDailyActuals.SelectedValue;
                                        }
                                    }
                                }
                            }
                        }
                    }
                    #endregion

                }
                else // While retrieving if there are no policy expense details then this else block will be executed.
                {

                    #region ExpenseList
                    if (!string.IsNullOrEmpty(hdnCapExpense.Value) && hdnCapExpense.Value.Length > 0)
                    {

                        string[] selExpenses = hdnCapExpense.Value.Split('|');
                        if (selExpenses.Length > 0)
                        {
                            //RowInfo
                            //{recordId^Country-City-ExpenseType-Currency-Amount-DailyActuals-TravelDays-Type}
                            foreach (string selExp in selExpenses)
                            {
                                PolicyExpenseDetails ped = new PolicyExpenseDetails();
                                int id;
                                int.TryParse(selExp.Split('^')[0], out id); //returns the id.
                                ped.ExpId = id;
                                ped.CountryCode = Convert.ToString(selExp.Split('^')[1].Split('-')[0]);
                                ped.CityCode = Convert.ToString(selExp.Split('^')[1].Split('-')[1]);
                                ped.SetupId = Convert.ToInt32(selExp.Split('^')[1].Split('-')[2]);
                                ped.Currency = Convert.ToString(selExp.Split('^')[1].Split('-')[3]);
                                ped.Amount = Convert.ToDecimal(selExp.Split('^')[1].Split('-')[4]);
                                if (Convert.ToString(selExp.Split('^')[1].Split('-')[6]) == "True")
                                {
                                    ped.IncudeTravelDays = true;
                                }
                                else
                                {
                                    ped.IncudeTravelDays = false;
                                }
                                ped.Type = Convert.ToString(selExp.Split('^')[1].Split('-')[7]);
                                ped.Status = "A";
                                ped.CreatedBy = (int)Settings.LoginInfo.UserID;
                                ped.DailyActuals = ddlExpDailyActuals.SelectedValue;
                                newpolicyExpenseDetails.Add(ped);
                            }
                        }
                    }
                    #endregion

                    #region AllowanceList
                    if (!string.IsNullOrEmpty(hdnCapAllowance.Value) && hdnCapAllowance.Value.Length > 0)
                    {

                        string[] selAllowances = hdnCapAllowance.Value.Split('|');
                        if (selAllowances.Length > 0)
                        {
                            //RowInfo
                            //{recordId^Country-City-Currency-Amount-TravelDays-Type}}
                            foreach (string selAllowance in selAllowances)
                            {
                                PolicyExpenseDetails ped = new PolicyExpenseDetails();
                                int id;
                                int.TryParse(selAllowance.Split('^')[0], out id); //returns the id.
                                ped.ExpId = id;
                                ped.CountryCode = Convert.ToString(selAllowance.Split('^')[1].Split('-')[0]);
                                ped.CityCode = Convert.ToString(selAllowance.Split('^')[1].Split('-')[1]);
                                ped.Currency = Convert.ToString(selAllowance.Split('^')[1].Split('-')[2]);
                                ped.Amount = Convert.ToDecimal(selAllowance.Split('^')[1].Split('-')[3]);
                                if (Convert.ToString(selAllowance.Split('^')[1].Split('-')[4]) == "True")
                                {
                                    ped.IncudeTravelDays = true;
                                }
                                else
                                {
                                    ped.IncudeTravelDays = false;
                                }
                                ped.Type = Convert.ToString(selAllowance.Split('^')[1].Split('-')[5]);
                                ped.Status = "A";
                                ped.CreatedBy = (int)Settings.LoginInfo.UserID;
                                ped.DailyActuals = ddlAllowanceDailyActuals.SelectedValue;
                                newpolicyExpenseDetails.Add(ped);
                            }
                        }
                    }
                    #endregion
                }

                if (existingPolicyExpenseDetails != null && existingPolicyExpenseDetails.Count > 0)
                {
                    if (newpolicyExpenseDetails != null && newpolicyExpenseDetails.Count > 0)
                    {
                        existingPolicyExpenseDetails.AddRange(newpolicyExpenseDetails);
                    }
                    pm.PolicyExpenseDetailsList = existingPolicyExpenseDetails;
                }
                else
                {
                    if (newpolicyExpenseDetails != null && newpolicyExpenseDetails.Count > 0)
                    {
                        pm.PolicyExpenseDetailsList = newpolicyExpenseDetails;
                    }
                }

                pm.Save();
                Clear();
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = "Details updated Successfully !";
            }
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = "Error Occurred !" + ex.Message.ToString();
            Clear();
            throw ex;

        }
    }

    private void Edit(long id)
    {
        try
        {
            Clear();

            PolicyMaster pm = new PolicyMaster(id);
            hdfEMId.Value = Utility.ToString(id);

            //1.AGENT INFORMATION
            ddlAgents.SelectedValue = Utility.ToString(pm.AgentId);

            //2.POLICY NAME
            txtPolicyName.Text = pm.PolicyName;

            //3.TRAVEL DATES TO AVOID
            if (pm.DatesToAvoidList != null && pm.DatesToAvoidList.Count > 0)
            {
                foreach (DateTime date in pm.DatesToAvoidList)
                {
                    if (string.IsNullOrEmpty(hdnFromDate.Value))
                    {
                        hdnFromDate.Value = Convert.ToString(date.ToString("dd/MM/yyyy")).Replace('/', '-');
                    }
                    else
                    {
                        hdnFromDate.Value = hdnFromDate.Value + "|" + Convert.ToString(date.ToString("dd/MM/yyyy")).Replace('/', '-');
                    }
                }
            }
            if (pm.PolicyDetailsList != null && pm.PolicyDetailsList.Count > 0)
            {
                List<PolicyDetails> policyDetails = pm.PolicyDetailsList;
                foreach (PolicyDetails pd in pm.PolicyDetailsList)
                {
                    //4.DESTINATIONS TO AVOID
                    if (pd.Category == Category.DESTINATIONTOAVOID)
                    {
                        if (string.IsNullOrEmpty(hdnDestinations.Value))
                        {
                            hdnDestinations.Value = pd.Id + "^" + Convert.ToString(pd.FilterValue2) + "-" + Convert.ToString(pd.FilterType) + "- Avoid Connection:" + Convert.ToString(pd.FilterValue1);
                        }
                        else
                        {
                            hdnDestinations.Value = hdnDestinations.Value + "|" + pd.Id + "^" + Convert.ToString(pd.FilterValue2) + "-" + Convert.ToString(pd.FilterType) + "- Avoid Connection:" + Convert.ToString(pd.FilterValue1);
                        }
                    }
                    //5.AIRLINES WITH NEGOTIATED FARES

                    if (pd.Category == Category.NEGOTIATEDFARES)
                    {
                        if (string.IsNullOrEmpty(hdnAirlineNegotiated.Value))
                        {
                            hdnAirlineNegotiated.Value = pd.Id + "^" + Convert.ToString(pd.FilterType) + "-" + Convert.ToString(pd.FilterValue1);
                        }
                        else
                        {
                            hdnAirlineNegotiated.Value = hdnAirlineNegotiated.Value + "|" + pd.Id + "^" + Convert.ToString(pd.FilterType) + "-" + Convert.ToString(pd.FilterValue1);
                        }
                    }

                    //6.SECURITY RULES
                    if (pd.Category == Category.SECURITYRULES)
                    {
                        hdnSecurityRules.Value = pd.Id + "^" + pd.FilterType + "|" + pd.FilterValue1;
                    }

                    //7.PREFERRED AIRLINES

                    if (pd.Category == Category.PREFERREDAIRLINES)
                    {
                        if (string.IsNullOrEmpty(hdnAirlinePrefered.Value))
                        {
                            hdnAirlinePrefered.Value = pd.Id + "^" + Convert.ToString(pd.FilterType);
                        }
                        else
                        {
                            hdnAirlinePrefered.Value = hdnAirlinePrefered.Value + "|" + pd.Id + "^" + Convert.ToString(pd.FilterType);
                        }
                    }
                    //8.AIRLINES TO AVOID
                    if (pd.Category == Category.AIRLINESTOAVOID)
                    {
                        if (string.IsNullOrEmpty(hdnAirlineAvoid.Value))
                        {
                            hdnAirlineAvoid.Value = pd.Id + "^" + Convert.ToString(pd.FilterType);
                        }
                        else
                        {
                            hdnAirlineAvoid.Value = hdnAirlineAvoid.Value + "|" + pd.Id + "^" + Convert.ToString(pd.FilterType);
                        }
                    }

                    //9.AIR FARE SHOPPING
                    if (pd.Category == Category.AIRFARESHOPPING)
                    {
                        if (string.IsNullOrEmpty(hdnAirFareShopping.Value))
                        {
                            hdnAirFareShopping.Value = pd.Id + "^" + Convert.ToString(pd.FilterType);
                        }
                    }

                    //10. Default Cabin Type
                    if (pd.Category == Category.DEFAULTCABIN)
                    {
                        if (string.IsNullOrEmpty(hdnDefaultCabin.Value))
                        {
                            hdnDefaultCabin.Value = pd.Id + "^" + Utility.ToString(pd.FilterType);
                            if (Utility.ToString(pd.FilterType) == "Business")
                            {

                                ddlCabinType.SelectedIndex = 1;
                            }
                            else if (Utility.ToString(pd.FilterType) == "Economy")
                            {

                                ddlCabinType.SelectedIndex = 2;
                            }
                            else if (Utility.ToString(pd.FilterType) == "First")
                            {

                                ddlCabinType.SelectedIndex = 3;
                            }
                            else if (Utility.ToString(pd.FilterType) == "PremiumEconomy")
                            {

                                ddlCabinType.SelectedIndex = 4;
                            }
                            else
                            {
                                ddlCabinType.SelectedIndex = 0;
                            }
                        }
                    }

                    //11.CABIN TYPE RULES
                    if (pd.Category == Category.EXCEPTIONCABIN)
                    {
                        hdnCabinTypeException.Value += (string.IsNullOrEmpty(hdnCabinTypeException.Value) ? string.Empty : "|") + +pd.Id + "^" +
                        Convert.ToString(pd.FilterType) + "-" + Convert.ToString(pd.FilterValue1) + "-" +
                        Convert.ToString(pd.FilterValue2) + "-" + Convert.ToString(pd.FilterValue4);
                    }

                    //Lokesh : 1June2017 Load ApprovalTab  Details if there are any.

                    if (pd.Category == Category.PRETRIPAPPROVAL)
                    {
                        //PRETRIPAPPROVAL --AlWAYS
                        if (pd.FilterType == "NOAPPROVAL")
                        {
                            preTripNoApproval.Checked = true;
                        }
                        else if (pd.FilterType == "ALWAYS")
                        {
                            preTripAlways.Checked = true;
                        }
                        //PRETRIPAPPROVAL --COSTEXCEEDS
                        else if (pd.FilterType == "COSTEXCEEDS")
                        {
                            preTripCostExceeds.Checked = true;
                            ddlPreTripCurrency.SelectedValue = pd.FilterValue1;
                            txtPreTripAmount.Text = pd.FilterValue2;
                        }
                    }
                    if (pd.Category == Category.RELAUNCHAPPROVAL)
                    {
                        //RELAUNCHAPPROVAL --  COSTEXCEEDS
                        if (pd.FilterType == "COSTEXCEEDS")
                        {
                            chkRelaunchPriceChange.Checked = true;
                            ddlRelaunchPriceChange.SelectedValue = pd.FilterValue1;
                            txtRelaunchPriceAmount.Text = pd.FilterValue2;
                        }
                        //RELAUNCHAPPROVAL --  CHANGEITINERARY
                        else if (pd.FilterType == "CHANGEITINERARY")
                        {
                            chkRelaunchItineraryChange.Checked = true;
                        }

                    }

                    if (pd.Category == Category.PREVISAAPPROVAL)
                    {
                        //PREVISAAPPROVAL --NOAPPROVAL
                        if (pd.FilterType == "NOAPPROVAL")
                        {
                            preVisaNoApproval.Checked = true;
                        }
                        else if (pd.FilterType == "ALWAYS")
                        {
                            preVisaAlways.Checked = true;
                        }

                    }

                    if (pd.Category == Category.AUTOTICKETING)
                    {
                        chkAutoTicketing.Checked = pd.FilterValue1 == "Y" ? true : false;
                    }
                    if (pd.Category == Category.COMPLETEBOOKING)
                    {
                        chkCompleteBooking.Checked = pd.FilterValue1 == "Y" ? true : false;
                    }
                    if (pd.Category == Category.RESTRICTBOOKING)
                    {
                        chkRestrictBooking.Checked = pd.FilterValue1 == "Y" ? true : false;
                    }
                    if (pd.Category == Category.AVOIDTRANSITTIME)
                    {
                        txtTransitTime.Text = pd.FilterValue1.Trim();
                    }
                    if (pd.Category == Category.TOTALBOOKINGAMOUNT)
                    {
                        txtBookingAmount.Text = pd.FilterValue1.Trim();
                    }
                    if (pd.Category == Category.BOOKINGTHRESHOLD)
                    {
                        txtBookingThreshold.Text = pd.FilterValue1.Trim();
                        ddlBookingThreshold.SelectedValue = string.IsNullOrEmpty(pd.FilterValue3) ? "F" : pd.FilterValue3.Trim();
                    }
                }
            }
            //ExpensBOOKINGTHRESHOLD 	eDetails

            if (pm.PolicyExpenseDetailsList != null && pm.PolicyExpenseDetailsList.Count > 0)
            {
                foreach (PolicyExpenseDetails ped in pm.PolicyExpenseDetailsList)
                {
                    if (ped.Type == "E")
                    {
                        //RowInfo -- ExpenseType
                        //{recordId^Country-City-ExpenseType-Currency-Amount-DailyActuals-TravelDays-Type}
                        string rowInfoE = string.Empty;
                        string rowInfoCE = string.Empty;
                        rowInfoE = Convert.ToString(ped.ExpId);
                        rowInfoE += "^";
                        rowInfoE += ped.CountryCode + "-";
                        rowInfoE += ped.CityCode + "-";
                        rowInfoE += ped.SetupId + "-";
                        rowInfoE += ped.Currency + "-";
                        rowInfoE += ped.Amount + "-";
                        rowInfoE += "Yes" + "-";
                        rowInfoE += ped.IncudeTravelDays + "-";
                        rowInfoE += ped.Type;


                        rowInfoCE = ped.CountryCode + "-";
                        rowInfoCE += ped.CityCode + "-";
                        rowInfoCE += ped.SetupId;

                        if (string.IsNullOrEmpty(hdnCapCityExpense.Value))
                        {
                            hdnCapCityExpense.Value = rowInfoCE;
                        }
                        else
                        {
                            hdnCapCityExpense.Value += "|" + rowInfoCE;
                        }

                        if (string.IsNullOrEmpty(hdnCapExpense.Value))
                        {
                            hdnCapExpense.Value = rowInfoE;
                        }
                        else
                        {
                            hdnCapExpense.Value += "|" + rowInfoE;
                        }

                    }
                    else if (ped.Type == "A")
                    {
                        ////RowInfo -- AllowanceType
                        //{recordId^Country-City-Currency-Amount-TravelDays-Type}}

                        string rowInfoA = string.Empty;
                        string rowInfoCA = string.Empty;
                        rowInfoA = Convert.ToString(ped.ExpId);
                        rowInfoA += "^";
                        rowInfoA += ped.CountryCode + "-";
                        rowInfoA += ped.CityCode + "-";
                        rowInfoA += ped.Currency + "-";
                        rowInfoA += ped.Amount + "-";
                        rowInfoA += ped.IncudeTravelDays + "-";
                        rowInfoA += ped.Type;


                        rowInfoCA = ped.CountryCode + "-";
                        rowInfoCA += ped.CityCode;

                        //this hidden field is used to avoid the same country city combinations.
                        if (string.IsNullOrEmpty(hdnCapCityAllowance.Value))
                        {
                            hdnCapCityAllowance.Value = rowInfoCA;
                        }
                        else
                        {
                            hdnCapCityAllowance.Value += "|" + rowInfoCA;
                        }

                        //this hidden field is used to capture all the allowances that are saved for that particular policy id.
                        if (string.IsNullOrEmpty(hdnCapAllowance.Value))
                        {
                            hdnCapAllowance.Value = rowInfoA;
                        }
                        else
                        {
                            hdnCapAllowance.Value += "|" + rowInfoA;
                        }
                    }
                }
            }
            btnClear.Text = "Cancel";
            btnSave.Text = "Update";
            hdfMode.Value = "1";
            Utility.StartupScript(this.Page, "appendAllPolicyDetails();", "SCRIPT");
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            long vsId = Utility.ToLong(gvSearch.SelectedValue);
            Edit(vsId);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateProfile page " + ex.Message, "0");
        }
    }

    protected void FilterSearch_Click(object sender, EventArgs e)
    {
        try
        {

            string[,] textboxesNColumns = { { "HTtxtCode", "PolicyCode" }, { "HTtxtName", "PolicyName" } };
            CommonGrid g = new CommonGrid();
            g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateProfile page " + ex.Message, "0");
        }
    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            FilterSearch_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateProfile page " + ex.Message, "0");
        }

    }

    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[POLICY_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["PolicyId"] };
            Session[POLICY_SEARCH_SESSION] = value;
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {

            this.Master.ShowSearch("Search");
            bindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateProfile page " + ex.Message, "0");
        }
    }

    private void bindSearch()
    {
        try
        {
            LoginInfo loginfo = Settings.LoginInfo;
            DataTable dt = PolicyMaster.GetAllCorporatePolicies('A');

            SearchList = dt;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvSearch, dt);
        }
        catch { throw; }
    }

    private void bindExpenseTypes()
    {
        try
        {
            ddlCapExpenseType.DataSource = PolicyExpenseDetails.GetAllExpenseTypes('A', "ET");
            ddlCapExpenseType.DataValueField = "SetupId";
            ddlCapExpenseType.DataTextField = "Name";
            ddlCapExpenseType.AppendDataBoundItems = true;
            ddlCapExpenseType.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private PolicyDetails BindPolicyDetails(int category)
    {
        PolicyDetails policyDetail = new PolicyDetails();
        policyDetail.Category = (Category)category;
        policyDetail.Id = -1;
        policyDetail.ProductId = 1;
        policyDetail.Status = "A";
        policyDetail.CreatedBy = (int)Settings.LoginInfo.UserID;
        return policyDetail;
    }
}
