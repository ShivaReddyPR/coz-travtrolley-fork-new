﻿<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="CorporateProfileSetupUI" Title="" Codebehind="CorporateProfileSetup.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<asp:HiddenField  id="hdfTranxId" runat="server" Value="0"/>
<asp:HiddenField  id="hdfTab" runat="server" Value=""/>
 <script type="text/javascript" src="Scripts/responsive-tabs.js"></script>
 <div class="container-fluid CorpTrvl-page">
   <div class="row">
     <div class="col-xs-12">
       <h2><asp:Label Text="" id="lblHeaderSetup" runat="server"></asp:Label></h2>
     </div>
   </div>
    <div class="row">
        <div class="col-md-4 col-lg-3">
               <div class="form-group">
                  <label for="">Client</label>
                        <%--<input type="text" class="form-control" id="" placeholder="Division">--%>
                     <asp:DropDownList ID="ddlAgent" runat="server" class="form-control" AutoPostBack="true" OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged" ></asp:DropDownList>
               </div>
        </div>
        <div class="col-md-4 col-lg-3">
                <div class="form-group">
                    <asp:Label  runat="server" id="lblSetupName" Text="Setup"></asp:Label>
                       <%--<input type="text" class="form-control" id="" placeholder="Division">--%>
                <asp:DropDownList ID="ddlSetup" runat="server" class="form-control"    >
                <asp:ListItem Text="Division" Value="DV"></asp:ListItem>
                <asp:ListItem Text="Grade" Value="GD"></asp:ListItem>
                <asp:ListItem Text="Designation" Value="DG"></asp:ListItem>                
                <asp:ListItem Text="Department" Value="DP"></asp:ListItem>
                <asp:ListItem Text="Cost Centre" Value="CS"></asp:ListItem>
                <asp:ListItem Text="Label" Value="LB"></asp:ListItem>
                <asp:ListItem Text="ExpenseType" Value="ET"></asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
   </div>
   <div class="row">
      <div class="col-xs-12">
        
        
        <div class="CorpTrvl-tabbed-panel">

            <!-- Nav tabs -->
            <ul class="nav nav-tabs responsive" role="tablist">
              <li role="presentation" class="active"><a href="#all-divisions" onclick='ClearFields()' aria-controls="all-divisions" role="tab" data-toggle="tab">
              <asp:Label ID="lblAllSetups"  runat="server" Text="ALL SETUPS" ></asp:Label> 
              </a></li>
              <li role="presentation"><a  href="#add-divisions" aria-controls="add-divisions" class="tab-title-with-icon" role="tab" data-toggle="tab">
              <asp:Label ID="lblAddSetups" runat="server" Text="ADD SETUPS" ></asp:Label></a></li>
            </ul>

            <!-- Tab panes -->
            <div class="tab-content responsive">
              <div role="tabpanel" class="tab-pane active" id="all-divisions">

                <div class="row">
                   <div class="col-xs-12">

                       <div class="table-responsive">
                            <asp:GridView ID="gvSetupDetails" runat="server" AllowPaging="true" DataKeyNames="SetupId"
                                EmptyDataText="No SETUP DETAILS!" AutoGenerateColumns="false" PageSize="500" GridLines="none"
                                CssClass="table table-bordered" CellPadding="4" CellSpacing="0"
                                OnRowDeleting="gvSetupDetails_RowDeleting" 
                                ShowFooter="false">
                                <HeaderStyle BackColor="Gray" HorizontalAlign="Left"></HeaderStyle>
                                <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
                                <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
                                <Columns>
                                    <asp:TemplateField>
                                        <ItemStyle HorizontalAlign="left" />
                                        <HeaderTemplate>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:Label ID="ITlblSno" runat="server" Text='<%# Container.DataItemIndex+1 %>' CssClass=""
                                                ToolTip='<%# Container.DataItemIndex+1 %>' Width="20px"></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle Width="70px" />
                                        <HeaderTemplate>
                                           <label  class="label">
                                                 Code</label>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="left" />
                                        <ItemTemplate >
                                            <asp:HiddenField ID="IThdfSetupId" runat="server" Value='<%# Eval("SetupId") %>'>
                                            </asp:HiddenField>
                                            <asp:HiddenField ID="IThdfAgentId" runat="server" Value='<%# Eval("AgentId") %>'>
                                            </asp:HiddenField>
                                            <asp:HiddenField id="IThdfCode" runat="server" value='<%# Eval("Code") %>'></asp:HiddenField>                                           
                                            <asp:Label  ID="ITlblCode" runat="server" Width="120px"  CssClass=""  Text='<%# Eval("Code") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle HorizontalAlign="left" />
                                        <HeaderTemplate>                                           
                                           <label  class="label">
                                                Description</label>
                                        </HeaderTemplate>
                                        <ItemStyle HorizontalAlign="left" />
                                        <ItemTemplate>
                                           <asp:Label ID="ITlblDescription" runat="server" Width="110px"  CssClass=""  Text='<%# Eval("Name") %>' ></asp:Label>
                                        </ItemTemplate>
                                    </asp:TemplateField>
                                    <asp:TemplateField>
                                        <HeaderStyle  />
                                         <HeaderTemplate>
                                           <label  class="label">
                                                 Actions</label>
                                        </HeaderTemplate>
                                        <ItemTemplate>
                                            <asp:LinkButton id="ITbtnEdit" CssClass="control-btn edit label-primary" runat="server" OnClientClick="ShowTab('A');" OnClick="ITbtnEdit_Click"><span class="glyphicon glyphicon-pencil"></span> Edit</asp:LinkButton>
                                            <asp:LinkButton  ID="ITbtnDelete" CssClass="control-btn delete label-danger" runat="server" 
                                                CausesValidation="False" CommandName="Delete"><span class="glyphicon glyphicon-trash"></span> Delete</asp:LinkButton>
                                        </ItemTemplate>
                                        <%--<FooterStyle HorizontalAlign="left" />
                                        <FooterTemplate>
                                            <asp:LinkButton ID="lnkInsert" runat="server" Text="Add" CommandName="Add" 
                                                Width="50px" OnClientClick="return ValidateTaxDetails();"></asp:LinkButton>
                                        </FooterTemplate>--%>
                                    </asp:TemplateField>
                                </Columns>
                            </asp:GridView>

                       </div>
                     
                   </div>
                  
                </div>
       

       



              </div>       
           
              <div role="tabpanel" class="tab-pane" id="add-divisions">
                  
                  
                  <div class="row">
                      <div class="col-xs-12 col-sm-6 col-md-3">
                         <div class="form-group">
                            <label for="">Code</label>
                            <asp:TextBox id="txtCode" MaxLength="15" class="form-control" runat="server" placeholder="Code"> </asp:TextBox>                            
                        </div>
                     </div>  
                      <div class="col-xs-12 col-sm-6 col-md-3">
                         <div class="form-group">
                            <label for="">Description</label>
                            <asp:TextBox id="txtDescription" class="form-control" runat="server" placeholder="Description"> </asp:TextBox>                            
                        </div>
                     </div>     
                  </div>
                 <div class="row top-buffer">
                    <div class="col-xs-12">
                       <%--<button type="button" class="btn btn-primary">ADD</button>
                       <button type="button" class="btn btn-default ">CANCEL</button>--%>
                       <asp:Button CssClass="btn btn-primary" runat="server" Text="ADD" ID="btnAdd" OnClientClick="return Save();" OnClick="btnAdd_Click" />
                   <asp:Button CssClass="btn btn-default" runat="server" Text="CLEAR" ID="btnClear"  OnClick="btnClear_Click"/>
                   <%--<asp:Button CssClass="btn btn-default" runat="server" Text="Search" ID="btnSearch" OnClick="btnSearch_Click"/>--%>
                    </div>
                </div>
              </div>     
            </div>
            
           
                

       </div>

        
        
        </div>
   </div>

 </div> 
 
 <script type="text/javascript">
   (function($) {
  fakewaffle.responsiveTabs(['xs', 'sm', 'md']);
 
  
  })(jQuery);
 

//  $(function() {
//         var tabName = $("[id*=hdfTab]").val() != "" ? $("[id*=hdfTab]").val() : "add-divisions";
//         alert(tabName);
//         $('.CorpTrvl-tabbed-panel a[href="#' + tabName + '"]').tab('show');
//         $(".CorpTrvl-tabbed-panel a").click(function() {
//             $("[id*=hdfTab]").val($(this).attr("href").replace("#", ""));
//         });
//     });







//  $(document).ready(function() {
//      alert(getElement('hdfTab').value);
//      
//      if (getElement('hdfTab').value != 'A')
//          $('#myTabs a[href="#all-divisions"]').tab('show')
//      else
//          $('#myTabs a[href="#add-divisions"]').tab('show')

//  });
//  
  function Save()
  {
    var msg='';
    //alert('hi');
    if (getElement('txtCode').value =='') msg += '\n Code Cannot be blank !';
    if (getElement('txtDescription').value == '') msg += '\n Description can not be blank !';
    
    if (msg != '') {
        alert(msg);
        return false;
    }
    return true;
  }
  
    function ShowTab() {
    
        $('.CorpTrvl-tabbed-panel a[href="#add-divisions"]').tab('show')
        jQuery("#txtCode").focus(function () {
            $(this).val("");
        });

       
    
    }
    function ClearFields()
    {
        document.getElementById("<%=txtCode.ClientID%>").value = '';
        document.getElementById("<%=txtDescription.ClientID%>").value = '';
         document.getElementById("<%=btnAdd.ClientID%>").value = 'ADD';
    }
        
    
</script>
</asp:Content>

