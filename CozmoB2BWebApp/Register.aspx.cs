﻿using System;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System.Web.UI;
using System.Web.Services;
using System.Web;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

public partial class Register : CT.Core.ParentPage
{
    protected string strFromMail = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["fromEmail"]);
    //protected string strMailBody = Convert.ToString(System.Configuration.ConfigurationManager.AppSettings["registerMailContent"]);
    

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblSuccessMsg.Text = string.Empty;
            if (!IsPostBack)
            {
                Session["RegisterFiles"] = null;
                InitializePageControls();
                lblSuccessMsg.Text = string.Empty;
            }
            lblError.Text = string.Empty;


        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message.ToString();
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);

        }
    }
    private void InitializePageControls()
    {
        BindCountry();
        BindCurrency();
        Clear();
    }

    private void Clear()
    {
        try
        {
            txtAgency.Text = string.Empty;
            txtAddress.Text = string.Empty;
            txtEmail.Text = string.Empty;
            txtNameOfOwners.Text = string.Empty;
            txtNatureOfBussiness.Text = string.Empty;
            txtPostBoxNo.Text = string.Empty;
            txtTelephone.Text = string.Empty;
            txtTradeLicenseNo.Text = string.Empty;
            txtWebsite.Text = string.Empty;
            ddlCountry.SelectedIndex = -1;
            ddlCurrency.SelectedIndex = -1;

            dcLicExpDate.Clear();
        }
        catch { throw; }
    }
    

    private void Save()
    {
        try
        {
            List<string> FilesUploaded = new List<string>();

            if (IsValidCaptchaResponse())
            {
            AgentRegistration agentReg = new AgentRegistration();
                agentReg.AgencyName = txtAgency.Text.Trim();
                agentReg.Address = txtAddress.Text.Trim();
                agentReg.PostBoxno = txtPostBoxNo.Text.Trim();
                agentReg.BusinessNature = txtNatureOfBussiness.Text.Trim();
                agentReg.OwnerName = txtNameOfOwners.Text.Trim();
                agentReg.Phoneno = txtTelephone.Text.Trim();
                agentReg.Email = txtEmail.Text.Trim();
                agentReg.Website = txtWebsite.Text.Trim();
                agentReg.TradeLicenseNo = txtTradeLicenseNo.Text;
                agentReg.ExpiryDate = dcLicExpDate.Value;
                agentReg.country = Utility.ToInteger(ddlCountry.SelectedItem.Value);
                agentReg.currency = ddlCurrency.SelectedItem.Text;
                agentReg.agentReceipts = JsonConvert.DeserializeObject<List<AgentReceipts>>(hdnfileUploads.Value);
                agentReg.Save();

            //To save uploaded receipts in web server
            
            string spath = ConfigurationManager.AppSettings["RegisterReceiptsFilePath"];
            string status = "No files found to upload";

            try
            {
                if (HttpContext.Current.Session["RegisterFiles"] == null)
                {
                    lblError.Visible = true;
                    lblError.Text = status;
                }


                List<HttpPostedFile> files = HttpContext.Current.Session["RegisterFiles"] as List<HttpPostedFile>;

                if (files == null || files.Count == 0)
                {
                    lblError.Visible = true;
                    lblError.Text = status;
                }


                var tempFileNames = agentReg.agentReceipts.ToDictionary(item => item.doc_Id,item => item.doc_name);

                if (tempFileNames != null && tempFileNames.Count < 0)
                {
                    lblError.Visible = true;
                    lblError.Text = status;
                }


                foreach (var key in tempFileNames)
                {
                    foreach (HttpPostedFile file in files)
                    {
                        if (key.Value == file.FileName)
                        {
                            if (System.IO.File.Exists(spath + key.Key + "_" + key.Value.ToString()))
                            {
                                System.IO.File.Delete(spath + key.Key + "_" + key.Value.ToString());
                            }
                            FilesUploaded.Add(spath + key.Key + "_" + key.Value.ToString());
                            file.SaveAs(spath + key.Key + "_" + key.Value.ToString());
                            files.Remove(file);
                            break;
                        }
                    }
                }

                
                HttpContext.Current.Session["RegisterFiles"] = null;

                //status = "Success";

            }
            catch (Exception ex)
            {
                lblError.Visible = true;
                lblError.Text = ex.Message.ToString();
                Utility.WriteLog(ex, this.Title);
                Utility.Alert(this.Page, ex.Message);
            }


            string replyTo = ConfigurationManager.AppSettings["replyTo"];
                string filePath = ConfigurationManager.AppSettings["AgentRegistration"].ToString();

                List<string> toArray = new List<string>();
                toArray.Add(txtEmail.Text.Trim());
                StreamReader sr = new StreamReader(filePath);
                string finalString = string.Empty;

                while (!sr.EndOfStream)
                {
                    string line = sr.ReadLine();
                    finalString += line;
                }
                string companyName = "Cozmo travel";
                if (HttpContext.Current.Request.Url.Host.ToLower().Contains("ibyta.com"))
                {
                    companyName = "ibyta";
                    replyTo="support@ibyta.com";

                    
                }
               
                toArray.Add(replyTo);
                Hashtable hashtable = new Hashtable();
                hashtable.Add("agentName", txtAgency.Text.Trim());
                hashtable.Add("address", txtAddress.Text.Trim());
                hashtable.Add("PostboxNumber", txtPostBoxNo.Text.Trim());
                hashtable.Add("BusinessNature", txtNatureOfBussiness.Text.Trim());
                hashtable.Add("Phoneno", txtTelephone.Text.Trim());
                hashtable.Add("Email", txtEmail.Text.Trim());
                hashtable.Add("Website", txtWebsite.Text.Trim());
                hashtable.Add("TradeLicenseNo", txtTradeLicenseNo.Text.Trim());
                hashtable.Add("ExpiryDate", dcLicExpDate.Value);
                hashtable.Add("Country", ddlCountry.SelectedItem.Text);
                hashtable.Add("CompanyName", companyName);
                hashtable.Add("Currency", ddlCurrency.SelectedItem.Text);

            string[] attachments = FilesUploaded.ToArray();


            try
            {   
                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], replyTo, toArray, "Agent Register", finalString, hashtable,attachments);
            }
            catch(Exception ex) { }
            Clear();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = "Record Saved Successfully " + agentReg.OwnerName;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    /// <summary>
    /// reCaptcha server side validation
    /// </summary>
    /// <returns></returns>
    private bool IsValidCaptchaResponse()
    {
        bool validated = false;
        try
        {
            //HttpWebRequest request = WebRequest.Create("https://www.google.com/recaptcha/api/siteverify?secret=6LeLXAEVAAAAAFhlencqHAkBtMqCYIuIxqXQvgMl&response=" + hdnreCaptchaResponse.Value) as HttpWebRequest;
            HttpWebRequest request = WebRequest.Create("https://www.google.com/recaptcha/api/siteverify?secret=6LelXqUZAAAAAAs_lnxdZ9FZrk3lPVbuGY9Mfgkb&response=" + hdnreCaptchaResponse.Value) as HttpWebRequest;
            

            HttpWebResponse response = request.GetResponse() as HttpWebResponse;

            using (StreamReader readStream = new StreamReader(response.GetResponseStream()))
            {
                string jsonResponse = readStream.ReadToEnd();

                if (!string.IsNullOrEmpty(jsonResponse))
                {
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    var data = js.Deserialize<dynamic>(jsonResponse);// Deserialize Json

                    validated = Convert.ToBoolean((data as Dictionary<string, object>)["success"]);
                }
            }
        }
        catch(Exception ex)
        {
            Console.WriteLine(ex.ToString());
        }

        return validated;
    }

    protected void btnRegister_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message.ToString();
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Response.Redirect("login.aspx");
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = ex.Message.ToString();
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }

    }
    //for Bind Country List 
    private void BindCountry()
    {
        try
        {
            ddlCountry.DataSource = CountryMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            ddlCountry.DataValueField = "COUNTRY_ID";
            ddlCountry.DataTextField = "COUNTRY_NAME";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--select country--", "-1"));


        }
        catch { throw; }
    }

    //for Bind Currency List 
    private void BindCurrency()
    {
        try
        {
            ddlCurrency.DataSource = CurrencyMaster.GetList(ListStatus.Short, RecordStatus.Activated);
            ddlCurrency.DataValueField = "CURRENCY_ID";
            ddlCurrency.DataTextField = "CURRENCY_CODE";
            ddlCurrency.DataBind();
            ddlCurrency.SelectedIndex = 0;
        }
        catch { throw; }
    }

}
