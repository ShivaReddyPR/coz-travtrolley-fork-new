﻿using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using CT.Core;
using CT.Configuration;

namespace CozmoB2BWebApp
{
    public partial class TransferVoucher : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.AgentId);
                hdnBehalfLocation.Value = JsonConvert.SerializeObject(0);
                //LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.LocationID);
                //request.LoginCountryCode = locationMaster.CountryCode != null ? locationMaster.CountryCode : "";
            }
            else
            {
                hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentID);
                hdnBehalfLocation.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentLocation);
                //LocationMaster locationMaster = new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation);
                //request.LoginCountryCode = locationMaster.CountryCode != null ? locationMaster.CountryCode : "";
            }
            hdnUserId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.UserID);            
        }

        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object SendVoucherMail(string Content,string MailId)
        {
            try
            {
                EmailParams clsParams = new EmailParams();
                clsParams.FromEmail = ConfigurationSystem.Email["fromEmail"];
                clsParams.ToEmail = Convert.ToString(MailId);
                clsParams.CCEmail = Convert.ToString(MailId);
                clsParams.Subject = "Transfer Voucher";

                System.Text.StringBuilder emailBodySB = new System.Text.StringBuilder();
                emailBodySB.Append("<!DOCTYPE html>" +
                    "<html lang = \"en\" ><body tyle=\"-moz-box-sizing:border-box;-ms-text-size-adjust:100%;-webkit-box-sizing:border-box;-webkit-text-size-adjust:100%;Margin:0;background:#f3f3f3!important;box-sizing:border-box;color:#0a0a0a;font-family:'Open Sans',Arial,sans-serif;font-size:13px;font-weight:400;line-height:1.3;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important\">" + Content + "</body></html>");
                //string sLogoPath = "https://travtrolley.com/Uploads/AgentLogos/" + new AgentMaster(itinerary.AgentId).ImgFileName;

                clsParams.EmailBody = emailBodySB.ToString();

                Email.Send(clsParams);
                var response = new
                {
                    status = true,
                    message = "Email send successfully"
                };
                return JsonConvert.SerializeObject(response);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.TransferSearch, Severity.High, 0, "Exception returned from Voucher email sending : " + ex.Message + " | " + DateTime.Now, "");
                var response = new
                {
                    status = false,
                    message = "Operation failed"
                };
                return JsonConvert.SerializeObject(response);
            }
        }            
        
    }
}
