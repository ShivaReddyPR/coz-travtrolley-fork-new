﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.Web.UI.Controls;
using CT.TicketReceipt.Common;
using System.Collections.Generic;
using CT.GlobalVisa;
using CT.AccountingEngine;


public partial class CorporateGlobalVisaApprovalsQueueUI : CT.Core.ParentPage
{
    protected int userCorpProfileId;
    protected int userId;
    protected string _approvalStatus;
    protected string approverNames = string.Empty;
    protected GVVisaSale detail;
    protected int approverId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                if (!Page.IsPostBack)
                {
                    userId = (int)Settings.LoginInfo.UserID;
                    IntialiseControls();
                }
            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateGlobalVisaApprovalsQueueUI) Page Load Event Error: " + ex.Message, "0");
        }
    }
    private void IntialiseControls()
    {
        try
        {
            dcReimFromDate.Value = DateTime.Now;
            dcReimToDate.Value = DateTime.Now;

            int user_corp_profile_id = CorporateProfile.GetCorpProfileId((int)Settings.LoginInfo.UserID);
            if (user_corp_profile_id > 0)
            {
                this.userCorpProfileId = user_corp_profile_id;
            }
            if (user_corp_profile_id < 0)
            {
                user_corp_profile_id = 0;
            }

            bindSearch(DateTime.Now.AddMonths(-1), DateTime.Now, userCorpProfileId, 0);
            bindEmployeesListByGrade();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void bindSearch(DateTime fromDate, DateTime toDate, int profileId, int selEmpProfileId)
    {
        try
        {
            DataTable dt = CorporateGlobalVisaDetails.GetGVApprovalDetailsList(profileId, fromDate, toDate, selEmpProfileId);
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvVisaSales, dt);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void gvVisaSales_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvVisaSales.PageIndex = e.NewPageIndex;
            gvVisaSales.EditIndex = -1;
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected string IDDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            try
            {
                if (ddlEmployee.SelectedValue == "0")
                {
                    bindSearch(Convert.ToDateTime(dcReimFromDate.Value), Convert.ToDateTime(dcReimToDate.Value), (int)Settings.LoginInfo.CorporateProfileId, 0);
                }
                else
                {
                    bindSearch(Convert.ToDateTime(dcReimFromDate.Value), Convert.ToDateTime(dcReimToDate.Value), (int)Settings.LoginInfo.CorporateProfileId, Convert.ToInt32(ddlEmployee.SelectedValue));
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "CorporateGlobalVisaApprovalsQueueUI btnSearchApprovals Error: " + ex.Message, "0");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateGlobalVisaApprovalsQueueUI) btnSearchApprovals Error: " + ex.ToString(), "0");
        }
    }
    private void bindEmployeesListByGrade()
    {
        DataTable dt = CorporateProfile.GetCorpProfilesListByApproverType((int)Settings.LoginInfo.CorporateProfileId, "V");
        if (dt != null && dt.Rows.Count > 0)
        {
            ddlEmployee.DataSource = dt;
            ddlEmployee.DataTextField = "Name";
            ddlEmployee.DataValueField = "ProfileId";
            ddlEmployee.DataBind();
            ddlEmployee.Items.Insert(0, new ListItem("--Select Employee--", "0"));
        }
    }

    protected void btnApprove_Click(object sender, EventArgs e)
    {
        try
        {
            updateStatus("A");
            IntialiseControls();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateGlobalVisaApprovalsQueueUI)btnApprove_Click Event .Error:" + ex.ToString(), "0");
        }
    }


    protected void btnReject_Click(object sender, EventArgs e)
    {
        try
        {
            updateStatus("R");
            IntialiseControls();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateExpenseApprovalsPage)btnReject_Click Event .Error:" + ex.ToString(), "0");

        }
    }
    private void updateStatus(string approverStatus)
    {
        isTrackingEmpty();
        try
        {
            foreach (GridViewRow gvRow in gvVisaSales.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                HiddenField hdfExpId = (HiddenField)gvRow.FindControl("IThdfEXPDETAIL_ID");
                HiddenField hdfExpDetailId = (HiddenField)gvRow.FindControl("hdfExpDetailId");

                if (chkSelect.Checked)
                {
                    detail = new GVVisaSale(Convert.ToInt32(hdfExpDetailId.Value));
                    AgentMaster agency = new AgentMaster(detail.AgentId);
                    if (detail != null && (agency.UpdateBalance(0) >= detail.TotVisaFee))//Check Agent Balance
                    {
                        CorporateGlobalVisaDetails.UpdateApproverStatus(Convert.ToInt32(hdfExpId.Value), approverStatus, (int)Settings.LoginInfo.UserID);


                        if (approverStatus == "A")//Approved --  GV_T_VISA_SALES_MASTER -- header Table.
                            //if all the approvers approved only then we will deduct the corresponding agent balance and update the ledger..
                        {
                            try
                            {

                                detail = new GVVisaSale(Convert.ToInt32(hdfExpDetailId.Value));
                                if (detail.ApprovedStatus == "A")//
                                {
                                    agency = new AgentMaster(detail.AgentId);
                                    agency.UpdateBalance(-detail.TotVisaFee);
                                    //Saving in Ledger
                                    LedgerTransaction ledgerTxn = new LedgerTransaction();
                                    NarrationBuilder objNarration = new NarrationBuilder();
                                    ledgerTxn.LedgerId = Settings.LoginInfo.AgentId;
                                    ledgerTxn.Amount = -detail.TotVisaFee;
                                    objNarration.HotelConfirmationNo = detail.DocNumber;
                                    objNarration.TravelDate = detail.TTTravelDate.ToShortDateString();
                                    ledgerTxn.ReferenceType = ReferenceType.GlobalVisaBooked;
                                    objNarration.Remarks = "Global Visa Booking Charges";
                                    ledgerTxn.Narration = objNarration;
                                    ledgerTxn.IsLCC = true;
                                    ledgerTxn.ReferenceId = Utility.ToInteger(detail.TransactionId);
                                    ledgerTxn.Notes = "";
                                    ledgerTxn.Date = DateTime.UtcNow;
                                    ledgerTxn.CreatedBy = (int)Settings.LoginInfo.UserID;
                                    ledgerTxn.TransType = "B2B";
                                    ledgerTxn.Save();
                                }

                                SendEmail("E", approverStatus, Convert.ToInt32(hdfExpDetailId.Value));
                                SendEmail("A", approverStatus, Convert.ToInt32(hdfExpDetailId.Value));
                            }
                            catch { }
                        }
                        else//Rejected -- So no need to trigger any email to the next level approver
                        {
                            try
                            {
                                SendEmail("E", approverStatus, Convert.ToInt32(hdfExpDetailId.Value));
                            }
                            catch { }

                        }
                    }
                    else
                    {
                        Utility.StartupScript(this.Page, "alert('No enough Funds available')", "SCRIPT");
                    }
                }
            }
        }

        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void isTrackingEmpty()
    {
        try
        {
            bool _selected = false;
            foreach (GridViewRow gvRow in gvVisaSales.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");


                if (chkSelect.Checked)
                {
                    _selected = true;
                    return;
                }

            }
            string strMsg = "Please Select atleast one Item ! ";
            if (!_selected)
                Utility.Alert(this.Page, strMsg);
        }
        catch { throw; }
    }
    protected void ITchkSelect_CheckedChanged(object sender, EventArgs e)
    {
        try
        {
            GridViewRow gvHdrRow = (GridViewRow)((CheckBox)sender).NamingContainer;
            CheckBox chkSelectAll = (CheckBox)gvHdrRow.FindControl("HTchkSelectAll");
            foreach (GridViewRow gvRow in gvVisaSales.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                chkSelect.Checked = chkSelectAll.Checked;
            }
        }
        catch (Exception ex)
        {

            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void SendEmail(string toEmpOrApp, string approverStatus, int expDetailId)
    {
        detail = new GVVisaSale(expDetailId);
        if (detail != null && detail.ProfileApproversList != null)
        {
            int user_corp_profile_id = CorporateProfile.GetCorpProfileId((int)Settings.LoginInfo.UserID);

            try
            {
                if (approverStatus == "A")
                {
                    _approvalStatus = "Approved";
                }
                else
                {
                    _approvalStatus = "Rejected";
                }
                List<string> toArray = new System.Collections.Generic.List<string>();
                if (toEmpOrApp == "A" && approverStatus == "A")//To the approver
                {
                    int hLevel = Convert.ToInt32(detail.ProfileApproversList.Where(i => i.ApproverId == user_corp_profile_id).Select(i => i.Hierarchy).FirstOrDefault());
                    List<int> listOfAppId = detail.ProfileApproversList.Where(i => i.Hierarchy == (hLevel + 1)).Select(i => i.ApproverId).ToList();
                    if (listOfAppId != null && listOfAppId.Count > 0)
                    {
                        foreach (int appId in listOfAppId)
                        {
                            approverNames = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverName).FirstOrDefault();
                            approverId = appId;
                            string appEmail = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverEmail).FirstOrDefault();
                            if (!string.IsNullOrEmpty(appEmail))
                            {
                                toArray.Add(appEmail);
                                System.IO.StringWriter sw = new System.IO.StringWriter();
                                HtmlTextWriter htw = new HtmlTextWriter(sw);
                                EmailDivApprover.RenderControl(htw);
                                string myPageHTML = string.Empty;
                                myPageHTML = sw.ToString();
                                myPageHTML = myPageHTML.Replace("none", "block");
                                string subject = "Visa Approval Request";
                                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                                toArray.Clear();
                                approverNames = string.Empty;
                            }
                        }
                    }
                }
                else //To the employee
                {
                    approverNames = detail.ProfileApproversList.Where(i => i.ApproverId == user_corp_profile_id).Select(i => i.ApproverName).FirstOrDefault();
                    string empEmail = detail.ProfileApproversList.Where(i => i.ApproverId == user_corp_profile_id).Select(i => i.ProfileEmail).FirstOrDefault();
                    if (!string.IsNullOrEmpty(empEmail))
                    {
                        toArray.Add(empEmail);
                    }
                    System.IO.StringWriter sw = new System.IO.StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    EmailDivEmployee.RenderControl(htw);
                    string myPageHTML = string.Empty;
                    myPageHTML = sw.ToString();
                    myPageHTML = myPageHTML.Replace("none", "block");
                    string subject = "Visa Status Change Notification";
                    if (toArray != null && toArray.Count > 0)
                    {
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                    }
                    toArray.Clear();
                    approverNames = string.Empty;
                }
            }

            catch (Exception ex)
            {
                Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateGlobalVisaApprovalsQueue.aspx)Failed to Send Email For Employee and Approvers: Reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

    }

    protected void gvVisaSales_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow)
            {
                DataRowView drv = (DataRowView)(e.Row.DataItem);
                Label lblStatus = (Label)e.Row.FindControl("ITlblVisaStatus");
                if (Convert.ToString(drv.Row["FVS_APPROVE_STATUS"]) == "N")
                {
                    lblStatus.Text = "Not Approved";
                }
                else if (Convert.ToString(drv.Row["FVS_APPROVE_STATUS"]) == "Y")
                {
                    lblStatus.Text = "In Process";
                }
                else if (Convert.ToString(drv.Row["FVS_APPROVE_STATUS"]) == "A")
                {
                    lblStatus.Text = "Approved";
                }
                else if (Convert.ToString(drv.Row["FVS_APPROVE_STATUS"]) == "R")
                {
                    lblStatus.Text = "Rejected";
                }
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "CorporateGlobalVisaDetailsQueueUI : gvHeroV_RowDataBound event error.Reason: " + ex.ToString(), "0");
        }
    }


}
