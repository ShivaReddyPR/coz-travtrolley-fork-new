﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using System.Linq;

public partial class InsuranceListGUI : CT.Core.ParentPage// System.Web.UI.Page
{
    private string INSURANCE = "_insurance";
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
            scriptManager.RegisterPostBackControl(this.btnExport);
            this.Master.PageRole = true;
            lblSuccessMsg.Text = string.Empty;
            hdfParam.Value = "1";
            Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
            if (!IsPostBack)
            {
                ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                InitializePageControls();
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void InitializePageControls()
    {
        try
        {
            dcFromDate.Value = Utility.ToDate(DateTime.Now.Date);
            dcToDate.Value = Utility.ToDate(DateTime.Now.Date.ToString("MMM-dd-yyyy 23:59"));
            BindAgent();
            if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
            {
                ddlLocation.Enabled = true;
            }
            else
            {
                ddlLocation.Enabled = false;
            }
            ddlLocation.DataSource = LocationMaster.GetList(Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
            ddlLocation.DataTextField = "location_name";
            ddlLocation.DataValueField = "location_id";
            ddlLocation.DataBind();
            ddlLocation.SelectedIndex = -1;
            ddlLocation.SelectedValue = Convert.ToString(Settings.LoginInfo.LocationID);
            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgent.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgent.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            BindB2BAgent(Convert.ToInt32(ddlAgent.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
            BindGrid();

        }
        catch
        { throw; }

    }
    #endregion
    void BindLocation(int agentId, string type)
    {
        try
        {
            DataTable dtLocations = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, type);

            ddlLocation.Items.Clear();
            ddlLocation.DataSource = dtLocations;
            ddlLocation.DataTextField = "location_name";
            ddlLocation.DataValueField = "location_id";
            ddlLocation.DataBind();

            ListItem item = new ListItem("All", "-1");
            ddlLocation.Items.Insert(0, item);
            hdfParam.Value = "0";
        }
        catch { throw ; }
    }

    private void BindAgent()
    {
        try
        {
            ddlAgent.DataSource = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated); // AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("-- All --", "0"));

            ddlAgent.SelectedIndex = -1;
            if (Settings.LoginInfo.AgentId > 0)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
        }
        catch { throw; }
    }

    

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
            if (agentId < 0) agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);

            BindB2BAgent(agentId);
            BindB2B2BAgent(agentId);
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "BASE";
            }
            BindLocation(Utility.ToInteger(ddlAgent.SelectedItem.Value), type);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    # region Session
    private DataTable insuranceList
    {
        get
        {
            return (DataTable)Session[INSURANCE];
        }
        set
        {
            if (value == null)
                Session.Remove(INSURANCE);
            else
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["PlanId"] };
                Session[INSURANCE] = value;
            }
        }
    }


    # endregion


    private void BindGrid()
    {
        try
        {
            DateTime fromDate = Utility.ToDate(dcFromDate.Value);
            DateTime toDate = Utility.ToDate(dcToDate.Value);
            long location = Utility.ToLong(ddlLocation.SelectedValue);
            int agent = Utility.ToInteger(ddlAgent.SelectedValue);
            int acctStatus = Utility.ToInteger(ddlAcctStatus.SelectedItem.Value);
            string accountedStatus = (acctStatus == 1) ? accountedStatus = "Y" : (acctStatus == 0) ? accountedStatus = "N" : string.Empty;
            //Chandan Sharma

            string agentType = string.Empty;
            if (agent == 0)
            {
                agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty;// null Means binding in list all BOOKINGS
                }
            }
            if (agent > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agent > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agent = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                }
            }


            #region B2C purpose
            string transType = string.Empty;
            if (Settings.LoginInfo.TransType == "B2B")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2B";
            }
            else if (Settings.LoginInfo.TransType == "B2C")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2C";
            }
            else
            {
                ddlTransType.Visible = true;
                lblTransType.Visible = true;
                if (ddlTransType.SelectedItem.Value == "-1")
                {
                    transType = null;
                }
                else
                {
                    transType = ddlTransType.SelectedItem.Value;
                }
            }
            #endregion

            //End here


            //insuranceList = AirlineInfo.InsuranceGetListTemp(fromDate, toDate, agent, location, Settings.LoginInfo.MemberType.ToString(), acctStatus, transType, agentType);
            insuranceList = CT.BookingEngine.InsuranceQueue.InsuranceGetList(fromDate, toDate, agent, location, Settings.LoginInfo.MemberType.ToString(), accountedStatus, transType, agentType);
            Session["InsuranceList"] = insuranceList;
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvInsuranceList, insuranceList);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void Filter_Click(object sender, EventArgs e)
    {
        try
        {
            string[,] textboxesNColumns ={ { "HTtxtPlanType", "PlanTitle" }, {"HTtxtPlanCode", "InsPlanCode"},{ "HTtxtIsuueDate", "PolicyPurchasedDate" }, 
                                         { "HTtxtDepatureDate", "DepartureDate" }, { "HTtxtArrivalDate", "ReturnDate" }, {"HTtxtTotalDays","TotalDays"},
                                         {"HTtxtFromCity","FromCity"} , {"HTtxtToCity","ToCity"}, {"HTtxtAirlinePNR","PNR"},{"HTtxCertificateNo","PolicyNo"},
                                         {"HTtxtPaxType","PaxCount"}, {"HTtxtCurrency","Currency"}, {"HTtxtNetAmount","NetAmount"},{"HTtxtMarkup","Markup"},
                                            {"HTtxtDiscount","Discount"},{"HTtxtTotalAmount","TotalAmount"},{"HTtxtChargeType","PremiumChargeType"},
                                         {"HTtxtPassenger","PassengerName"},{"HTtxtProduct","Product"},{"HTtxtAgent","agent_name"},
                                            {"HTtxtLocation","location_name"},{"HTtxtUser","USER_FULL_NAME"},{"HTtxtAcctStatus","AccountedStatus"},{"HTtxtStatus","status"},{"HTtxtInvoiceNumber","Invoice_no"},{"HTtxtMessage","Message"}};

            CommonGrid g = new CommonGrid();
            insuranceList = (DataTable)Session["InsuranceList"];
            g.FilterGridView(gvInsuranceList, insuranceList.Copy(), textboxesNColumns);
            insuranceList = ((DataTable)gvInsuranceList.DataSource).Copy();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void gvInsuranceList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            selectedItem();
            gvInsuranceList.PageIndex = e.NewPageIndex;
            gvInsuranceList.EditIndex = -1;
            Filter_Click(null, null);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }


    protected void btnUpdateStatus_Click(object sender, EventArgs e)
    {
        try
        {
            lblSuccessMsg.Visible = false;
            lblSuccessMsg.Text = null;
            selectedItem();
            isTrackingEmpty();

            var insDatas =  from insData in insuranceList.AsEnumerable()
                            where insData.Field<string>("isAccounted") == "Y"
                            select new
                            {
                                isAccounted = insData.Field<string>("isAccounted"),
                                InsuranceId = insData.Field<int>("InsuranceId")
                            };
            foreach (var item in insDatas.Distinct())
            {
                if (item != null)
                {
                    bool selected = Utility.ToString(item.isAccounted) == "Y" ? true : false;
                    long InsId = Utility.ToLong(item.InsuranceId);
                    bool isUpdated = Utility.ToString(item.isAccounted) == "U" ? true : false;
                    if (selected && !isUpdated)
                    {
                        CT.BookingEngine.InsuranceQueue.UpdateInsAcctStatus(InsId);
                        lblSuccessMsg.Visible = true;
                        lblSuccessMsg.Text = "Updated Successfully";
                    }
                }
            }

            BindGrid();
            
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.StartupScript(this.Page, "alert('" + ex.Message + "');", "Err");
        }

    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            //string script = "window.open('ExportExcelInsuranceList.aspx?','','width=0,height=0,toolbar=yes,location=no,directories=no,status=no,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            //Utility.StartupScript(this.Page, script, "Excel");
            string attachment = "attachment; filename=InsuranceList.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dgInsuranceList.AllowPaging = false;
            dgInsuranceList.DataSource = insuranceList;
            dgInsuranceList.DataBind();
            dgInsuranceList.RenderControl(htw);
            Response.Write(sw.ToString());
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);

        }
        finally
        {
            Response.End();
        }
    }

    #region Date Format
    protected string CTDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string CTDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:mm");
        }
    }
    protected string CTCurrencyFormat(object currency, object decimalPoint)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + decimalPoint);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + decimalPoint);
        }
    }

    #endregion

    private void selectedItem()
    {
        try
        {
            foreach (System.Data.DataColumn col in insuranceList.Columns) col.ReadOnly = false;
            foreach (GridViewRow gvRow in gvInsuranceList.Rows)
            {
                CheckBox chkSelect = (CheckBox)gvRow.FindControl("ITchkSelect");
                HiddenField hdfInsDetId = (HiddenField)gvRow.FindControl("IThdfInsDetId");

                foreach (DataRow dr in insuranceList.Rows)
                {
                    if (dr != null)
                    {
                        dr.BeginEdit();
                        if (Utility.ToString(dr["PlanId"]) == hdfInsDetId.Value)
                        {
                            if (Utility.ToString(dr["isAccounted"]) != "U") dr["isAccounted"] = chkSelect.Checked ? "Y" : "N";
                        }
                        dr.EndEdit();
                    }

                }

            }

        }
        catch { throw; }
    }
    private void isTrackingEmpty()
    {
        try
        {
            bool _selected = false;
            foreach (DataRow dr in insuranceList.Rows)
            {
                if (dr != null)
                {
                    bool isUpdated = Utility.ToString(dr["isAccounted"]) == "U" ? true : false; ;
                    if (!isUpdated && Utility.ToString(dr["isAccounted"]) == "Y")
                    {

                        _selected = true;
                        return;
                    }
                }
            }
            string strMsg = "Please Select atleast one Item ! ";
            if (!_selected)
            {
                Utility.Alert(this.Page, strMsg);
                throw new Exception(strMsg);
            }

        }
        catch { throw; }
    }

    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            string type = string.Empty;
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            if (agentId == 0)
            {
                if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
                {
                    type = "AGENT";// AGENT Means Based On the AGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
                else
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
            }
            else
            {
                if (agentId == -1)
                {
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    if (agentId == 0)
                    {
                        type = "BASE";// BASE Means binding in Location Dropdown all BASEAGENT AND AGENTS Locations
                    }
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    type = "BASEB2B";// BASEB2B Means binding in Location Dropdown all BASEAGENT ,AGENTS AND B2B Locations
                }
            }
            BindLocation(agentId, type);

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void ddlB2B2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            string type = string.Empty;
            if (agentId == 0)
            {
                type = "B2B2B";// B2B2B Means Based On the B2B binding in Location DropDown All B2B2B Locations
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                }
            }
            else if (agentId == -1)
            {
                agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                if (agentId == 0)
                {
                    type = "B2B";// B2B Means Based On the BASEAGENT binding in Location Dropdown All B2B Locations
                    agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
                //if (agentId == -1)
                //{
                //    type = "BASE";
                //}
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = string.Empty;
                    }
                }
            }
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        type = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in Location DropDown All B2B AND B2B2B Locations
                        agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                    }
                }
            }
            BindLocation(agentId, type);
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    //Binding B2B2B Agents
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Binding B2B Agents
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    
}

