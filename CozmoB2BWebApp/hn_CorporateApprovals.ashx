﻿<%@ WebHandler Language="C#" Class="hn_CorporateApprovals" %>

using System;
using System.Web;
using CT.Core;
using CT.Corporate;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.Collections;



public class hn_CorporateApprovals : IHttpHandler
{
    protected string _approvalStatus;
    protected string approverNames = string.Empty;
    protected CorporateProfileExpenseDetails detail;

    public void ProcessRequest(HttpContext context)
    {
        try
        {
            string appStatus = string.Empty;
            int approverId = 0;
            int expDetailId = 0;

            if (context.Request.QueryString["status"] != null)
            {

                appStatus = HttpContext.Current.Server.UrlDecode(context.Request.QueryString["status"]);
            }
            if (context.Request.QueryString["approverId"] != null)
            {

                approverId = Convert.ToInt32(HttpContext.Current.Server.UrlDecode(context.Request.QueryString["approverId"]));
            }
            if (context.Request.QueryString["expDetailId"] != null)
            {

                expDetailId = Convert.ToInt32(HttpContext.Current.Server.UrlDecode(context.Request.QueryString["expDetailId"]));
            }

            if (!string.IsNullOrEmpty(appStatus) && approverId > 0 && expDetailId > 0)
            {
                if (appStatus == "A")
                {
                    CorporateProfileExpenseDetails.UpdateRefundStatusByMail(expDetailId, approverId, appStatus ,string.Empty);

                    try
                    {
                        SendEmail("E", appStatus, expDetailId, approverId);
                        SendEmail("A", appStatus, expDetailId, approverId);
                    }
                    catch { }
                    context.Response.Redirect("CorporateEmailStatusNotification.aspx?status=" + appStatus);
                }
                else{
                    context.Response.Redirect("CorporateEmailStatusNotification.aspx?status=" + appStatus + "&approverId=" + approverId + "&expDetailId=" + expDetailId);
                }
            }





        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), "0");
        }
    }

    public bool IsReusable
    {
        get
        {
            return false;
        }
    }




    protected string getAppHtmlText(CorporateProfileExpenseDetails detail, string status, int approverId)
    {
        string apphtml = string.Empty;
        apphtml += @"<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Transitional//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd'>
        <html xmlns='http://www.w3.org/1999/xhtml' lang='en' xml:lang='en' style='background: #f3f3f3!important'>
        <head>
            <meta http-equiv='Content-Type' content='text/html; charset=utf-8'>
            <meta name='viewport' content='width=device-width'>
            <title>Expense Approval Request</title>
            <style>
                @media only screen
                {
                    html
                    {
                        min-height: 100%;
                        background: #f3f3f3;
                    }
                }
                @media only screen and (max-width:596px)
                {
                    table.body img
                    {
                        width: auto;
                        height: auto;
                    }
                    table.body center
                    {
                        min-width: 0 !important;
                    }
                    table.body .container
                    {
                        width: 95% !important;
                    }
                    table.body .columns
                    {
                        height: auto !important;
                        -moz-box-sizing: border-box;
                        -webkit-box-sizing: border-box;
                        box-sizing: border-box;
                        padding-left: 16px !important;
                        padding-right: 16px !important;
                    }
                    table.body .columns .columns
                    {
                        padding-left: 0 !important;
                        padding-right: 0 !important;
                    }
                    th.small-6
                    {
                        display: inline-block !important;
                        width: 50% !important;
                    }
                    th.small-12
                    {
                        display: inline-block !important;
                        width: 100% !important;
                    }
                    .columns th.small-12
                    {
                        display: block !important;
                        width: 100% !important;
                    }
                }
            </style>
        </head>
        <body style='-moz-box-sizing: border-box; -ms-text-size-adjust: 100%; -webkit-box-sizing: border-box;
            -webkit-text-size-adjust: 100%; margin: 0; background: #f3f3f3!important; box-sizing: border-box;
            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
            line-height: 1.3; margin: 0; min-width: 100%; padding: 0; text-align: left; width: 100%!important'>
            <span class='preheader' style='color: #f3f3f3; display: none!important; font-size: 1px;
                line-height: 1px; max-height: 0; max-width: 0; mso-hide: all!important; opacity: 0;
                overflow: hidden; visibility: hidden'></span>
            <table class='body' style='margin: 0; background: #f3f3f3!important; border-collapse: collapse;
                border-spacing: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                font-weight: 400; height: 100%; line-height: 1.3; margin: 0; padding: 0; text-align: left;
                vertical-align: top; width: 100%'>
                <tr style='padding: 0; text-align: left; vertical-align: top'>
                    <td class='center' align='center' valign='top' style='-moz-hyphens: auto; -webkit-hyphens: auto;
                        margin: 0; border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                        font-size: 16px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                        padding: 0; text-align: left; vertical-align: top; word-wrap: break-word'>
                        <center data-parsed='' style='min-width: 580px; width: 100%'>
                            <table style='margin: 0 auto; background: #fefefe; border-collapse: collapse; border-spacing: 0;
                                float: none; margin: 0 auto; margin-top: 20px; padding: 10px; text-align: center;
                                vertical-align: top; width: 580px' align='center' class='container float-center'>
                                <tbody>
                                    <tr style='padding: 0; text-align: left; vertical-align: top'>
                                        <td style='-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                                            hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top;
                                            word-wrap: break-word'>
                                            <table class='spacer' style='border-collapse: collapse; border-spacing: 0; padding: 0;
                                                text-align: left; vertical-align: top; width: 100%'>
                                                <tbody>
                                                    <tr style='padding: 0; text-align: left; vertical-align: top'>
                                                        <td height='16px' style='-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                                                            hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                            text-align: left; vertical-align: top; word-wrap: break-word'>
                                                            &#xA0;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class='row' style='border-collapse: collapse; border-spacing: 0; display: table;
                                                padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%'>
                                                <tbody>
                                                    <tr style='padding: 0; text-align: left; vertical-align: top'>
                                                        <th class='small-12 large-12 columns first last' style='margin: 0 auto; color: #0a0a0a;
                                                            font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 1.3;
                                                            margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px;
                                                            text-align: left; width: 564px'>
                                                            <table style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                vertical-align: top; width: 100%'>
                                                                <tr style='padding: 0; text-align: left; vertical-align: top'>
                                                                    <th style='margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left'>
                                                                        <img src='http://www.travtrolley.com/images/logo.jpg' style='-ms-interpolation-mode: bicubic;
                                                                            clear: both; display: block; max-width: 100%; outline: 0; text-decoration: none;
                                                                            width: auto'>
                                                                    </th>
                                                                    <th class='expander' style='margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                        font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important;
                                                                        text-align: left; visibility: hidden; width: 0'>
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            <table class='spacer' style='border-collapse: collapse; border-spacing: 0; padding: 0;
                                                text-align: left; vertical-align: top; width: 100%'>
                                                <tbody>
                                                    <tr style='padding: 0; text-align: left; vertical-align: top'>
                                                        <td height='16px' style='-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                            color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400;
                                                            hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                            text-align: left; vertical-align: top; word-wrap: break-word'>
                                                            &#xA0;
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                            
                                            <table class='row' style='border-collapse: collapse; border-spacing: 0; display: table;
                                                padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%'>
                                                <tbody>
                                                    <tr style='padding: 0; text-align: left; vertical-align: top'>
                                                        <th class='small-12 large-12 columns first last' style='margin: 0 auto; color: #0a0a0a;
                                                            font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 1.3;
                                                            margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 16px; padding-right: 16px;
                                                            text-align: left; width: 564px'>
                                                            <table style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                vertical-align: top; width: 100%'>
                                                                <tr style='padding: 0; text-align: left; vertical-align: top'>
                                                                    <th style='margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                        font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left'>
                                                                        <h1 style='margin: 0; margin-bottom: 10px; color: inherit; font-family: Helvetica,Arial,sans-serif;
                                                                            font-size: 20px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                            padding: 0; text-align: left; word-wrap: normal'> ";

        apphtml += @"Dear" + " " + approverNames;


        apphtml += @"                                                         
              </h1>
                                                                        <p style='margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                            font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                            padding: 0; text-align: left'>
         Find below expense claim details for Employee ID :";

        apphtml += detail.EmpId;
        apphtml += @"Employee Name :" + detail.EmpName;


        apphtml += @"</p>
                                                                        <table class='spacer' style='border-collapse: collapse; border-spacing: 0; padding: 0;
                                                                            text-align: left; vertical-align: top; width: 100%'>
                                                                            <tbody>
                                                                                <tr style='padding: 0; text-align: left; vertical-align: top'>
                                                                                    <td height='30px' style='-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400;
                                                                                        hyphens: auto; line-height: 30px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                                                        text-align: left; vertical-align: top; word-wrap: break-word'>
                                                                                        &#xA0;
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        
                                                                        
                                                                        
                                                                        
                                                                        <table class='approver-table' style='background: #FFF; border: 1px solid #167F92;
                                                                            border-collapse: collapse; border-radius: 10px; border-spacing: 0; color: #024457;
                                                                            font-size: 11px; margin: 1em 0; padding: 0; text-align: left; vertical-align: top;
                                                                            width: 100%'>
                                                                            <tr style='background-color: #EAF3F3; border: 1px solid #D9E4E6; padding: 0; text-align: left;
                                                                                vertical-align: top'>
                                                                                <th style='margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left'>
                                                                                    Date
                                                                                </th>
                                                                                <th style='margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left'>
                                                                                    Expense Ref
                                                                                </th>
                                                                                <th style='margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left'>
                                                                                    Expense Category
                                                                                </th>
                                                                                <th style='margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left'>
                                                                                    Expense Type
                                                                                </th>
                                                                                <th style='margin: 0; background-color: #1c498a; border: 1px solid #476794; color: #FFF;
                                                                                    font-family: Helvetica,Arial,sans-serif; font-size: 11px; font-weight: 400; line-height: 1.3;
                                                                                    margin: 0; padding: 7px 7px; text-align: left'>
                                                                                    Amount
                                                                                </th>
                                                                                
                                                                                <th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Action</th>
                                                                            </tr>
   
                                                                            
                                                                            
                                                                       
                                                                            <tr style='border: 1px solid #D9E4E6; padding: 0; text-align: left; vertical-align: top'>
                                                                                <td style='-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word'>";

        apphtml += Convert.ToString(detail.Date).Split(' ')[0];

        apphtml += @"</td>
                                                                                <td style='-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word'>";

        apphtml += detail.DocNo;
        apphtml += @"</td>
                                                                                <td style='-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word'>";

        apphtml += detail.ExpCategoryText;

        apphtml += @"</td>
                                                                                <td style='-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word'>";

        apphtml += detail.ExpTypeText;
        apphtml += @" </td>
                                                                                <td style='-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border: 1px solid #476794;
                                                                                    border-collapse: collapse!important; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                    font-size: 11px; font-weight: 400; hyphens: auto; line-height: 1.3; margin: 0;
                                                                                    padding: 7px 7px; text-align: left; vertical-align: top; word-wrap: break-word'>";
        apphtml += detail.Currency + " " + detail.Amount;
        apphtml += @"</td>
    
                                                                    
                                                                                
    <td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        apphtml += @"     
    <a href='";
        apphtml += ConfigurationManager.AppSettings["CORPORATE_ROOT_URL"];
        apphtml += @"
/hn_CorporateApprovals.ashx?status=";
        apphtml += HttpContext.Current.Server.UrlEncode("A");
        apphtml += @"&approverId=";
        apphtml += HttpContext.Current.Server.UrlEncode(Convert.ToString(approverId));
        apphtml += @"&expDetailId=";
        apphtml += HttpContext.Current.Server.UrlEncode(Convert.ToString(detail.ExpDetailId));
        apphtml += @"'style='Margin:0;color:green;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:underline'>Approve</a> | <a href='";
        apphtml += ConfigurationManager.AppSettings["CORPORATE_ROOT_URL"];
        apphtml += @"
        /hn_CorporateApprovals.ashx?status=";
        apphtml += HttpContext.Current.Server.UrlEncode("R");
        apphtml += @"&approverId=";
        apphtml += HttpContext.Current.Server.UrlEncode(Convert.ToString(approverId));
        apphtml += @"&expDetailId=";
        apphtml += HttpContext.Current.Server.UrlEncode(Convert.ToString(detail.ExpDetailId));
        apphtml += @"'style='Margin:0;color:red;font-family:Helvetica,Arial,sans-serif;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;text-decoration:underline'>Reject</a>  
    </td>                                                                          
      </tr>
     </table>
                                                                        <table class='spacer' style='border-collapse: collapse; border-spacing: 0; padding: 0;
                                                                            text-align: left; vertical-align: top; width: 100%'>
                                                                            <tbody>
                                                                                <tr style='padding: 0; text-align: left; vertical-align: top'>
                                                                                    <td height='30px' style='-moz-hyphens: auto; -webkit-hyphens: auto; margin: 0; border-collapse: collapse!important;
                                                                                        color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 30px; font-weight: 400;
                                                                                        hyphens: auto; line-height: 30px; margin: 0; mso-line-height-rule: exactly; padding: 0;
                                                                                        text-align: left; vertical-align: top; word-wrap: break-word'>
                                                                                        &#xA0;
                                                                                    </td>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                        
                                                                        <table class='row' style='border-collapse: collapse; border-spacing: 0; display: table;
                                                                            padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%'>
                                                                            <tbody>
                                                                                <tr style='padding: 0; text-align: left; vertical-align: top'>
                                                                                    <th class='small-12 large-12 columns first last' style='margin: 0 auto; color: #0a0a0a;
                                                                                        font-family: Helvetica,Arial,sans-serif; font-size: 16px; font-weight: 400; line-height: 1.3;
                                                                                        margin: 0 auto; padding: 0; padding-bottom: 16px; padding-left: 0!important;
                                                                                        padding-right: 0!important; text-align: left; width: 100%'>
                                                                                        <table style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left;
                                                                                            vertical-align: top; width: 100%'>
                                                                                            <tr style='padding: 0; text-align: left; vertical-align: top'>
                                                                                                <th style='margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif; font-size: 16px;
                                                                                                    font-weight: 400; line-height: 1.3; margin: 0; padding: 0; text-align: left'>
                                                                                                    <p style='margin: 0; margin-bottom: 10px; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                        font-size: 14px; font-weight: 400; line-height: 1.3; margin: 0; margin-bottom: 10px;
                                                                                                        padding: 0; text-align: left'>
                                                                                                        Regards<br><strong>";

        apphtml += detail.AgencyName;
        apphtml += @"</strong></p>
                                                                                                </th>
                                                                                                <th class='expander' style='margin: 0; color: #0a0a0a; font-family: Helvetica,Arial,sans-serif;
                                                                                                    font-size: 16px; font-weight: 400; line-height: 1.3; margin: 0; padding: 0!important;
                                                                                                    text-align: left; visibility: hidden; width: 0'>
                                                                                                </th>
                                                                                            </tr>
                                                                                        </table>
                                                                                    </th>
                                                                                </tr>
                                                                            </tbody>
                                                                        </table>
                                                                       
                                                                    </th>
                                                                </tr>
                                                            </table>
                                                        </th>
                                                    </tr>
                                                </tbody>
                                            </table>
                                           
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </center>
                    </td>
                </tr>
            </table>
            <!-- prevent Gmail on iOS font size manipulation -->
            <div style='display: none; white-space: nowrap; font: 15px courier; line-height: 0'>
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;</div>
        </body>
        </html>";
        return apphtml;
    }

    protected string getEmpHtmlText(CorporateProfileExpenseDetails detail, string status)
    {
        string empHtml = string.Empty;
        empHtml += @"<!DOCTYPE HTML PUBLIC '-//W3C//DTD HTML 4.01 Transitional//EN'><html><head><META http-equiv='Content-Type' content='text/html; charset=utf-8'></head><body><div style='Margin:0;background:#f3f3f3!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;min-width:100%;padding:0;text-align:left;width:100%!important'><span style='color:#f3f3f3;display:none!important;font-size:1px;line-height:1px;max-height:0;max-width:0;overflow:hidden'></span><table style='Margin:0;background:#f3f3f3!important;border-collapse:collapse;border-spacing:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;height:100%;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;width:100%'><tr style='padding:0;text-align:left;vertical-align:top'><td align='center' valign='top' style='Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'><center style='min-width:580px;width:100%'><table style='Margin:0 auto;background:#fefefe;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;margin-top:20px;padding:10px;text-align:center;vertical-align:top;width:580px' align='center'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><td style='Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'><table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><td height='16px' style='Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:16px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'> </td></tr></tbody></table>";

        empHtml += @"<table style='border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><th style='Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px'><table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tr style='padding:0;text-align:left;vertical-align:top'><th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'><img src='http://www.travtrolley.com/images/logo.jpg' style='clear:both;display:block;max-width:100%;outline:0;text-decoration:none;width:auto'></th><th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;width:0'></th></tr></table></th></tr></tbody></table>";

        empHtml += @"<table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><td height='16px' style='Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:16px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'> </td></tr></tbody></table><table style='border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><th style='Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:16px;padding-right:16px;text-align:left;width:564px'><table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tr style='padding:0;text-align:left;vertical-align:top'><th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'><h1 style='Margin:0;Margin-bottom:10px;color:inherit;font-family:Helvetica,Arial,sans-serif;font-size:20px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left;word-wrap:normal'>";
        empHtml += @"Dear" + " " + detail.EmpName;

        empHtml += @"</h1><p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'>Below expenses are";

        empHtml += status + "by" + " " + approverNames + "</p>";

        empHtml += @"<table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><td height='30px' style='Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:30px;font-weight:400;line-height:30px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'> </td></tr></tbody></table><table style='background:#fff;border:1px solid #167f92;border-collapse:collapse;border-radius:10px;border-spacing:0;color:#024457;font-size:11px;margin:1em 0;padding:0;text-align:left;vertical-align:top;width:100%'><tr style='background-color:#eaf3f3;border:1px solid #d9e4e6;padding:0;text-align:left;vertical-align:top'><th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Date</th><th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Expense Ref</th><th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Expense Category</th><th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Expense Type</th><th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Amount</th><th style='Margin:0;background-color:#1c498a;border:1px solid #476794;color:#fff;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left'>Status</th></tr>";

        empHtml += @"<tr style='border:1px solid #d9e4e6;padding:0;text-align:left;vertical-align:top'>";

        //Date      
        empHtml += @"<td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        empHtml += Convert.ToString(detail.Date).Split(' ')[0];
        empHtml += @"</td>";

        //Expense Ref
        empHtml += @"<td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        empHtml += detail.DocNo;
        empHtml += @"</td>";

        //Expense Category
        empHtml += @"<td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        empHtml += detail.ExpCategoryText;
        empHtml += @"</td>";

        //Expense Type
        empHtml += @"<td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        empHtml += detail.ExpTypeText;
        empHtml += @"</td>";

        //Amount
        empHtml += @"<td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        empHtml += detail.Currency + detail.Amount;
        empHtml += @"</td>";

        //Status --Approved or Rejected.
        empHtml += @"<td style='Margin:0;border:1px solid #476794;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:11px;font-weight:400;line-height:1.3;margin:0;padding:7px 7px;text-align:left;vertical-align:top;word-wrap:break-word'>";
        empHtml += status;
        empHtml += @"</td>";
        empHtml += @"</tr></tbody></table>";

        //Footer Template  

        empHtml += @"<table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><td height='30px' style='Margin:0;border-collapse:collapse!important;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:30px;font-weight:400;line-height:30px;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word'> </td></tr></tbody></table><table style='border-collapse:collapse;border-spacing:0;display:table;padding:0;position:relative;text-align:left;vertical-align:top;width:100%'><tbody><tr style='padding:0;text-align:left;vertical-align:top'><th style='Margin:0 auto;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0 auto;padding:0;padding-bottom:16px;padding-left:0!important;padding-right:0!important;text-align:left;width:100%'><table style='border-collapse:collapse;border-spacing:0;padding:0;text-align:left;vertical-align:top;width:100%'><tr style='padding:0;text-align:left;vertical-align:top'><th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0;text-align:left'><p style='Margin:0;Margin-bottom:10px;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:14px;font-weight:400;line-height:1.3;margin:0;margin-bottom:10px;padding:0;text-align:left'>Regards<br><strong> ";

        empHtml += detail.AgencyName;

        empHtml += @"</strong></p></th><th style='Margin:0;color:#0a0a0a;font-family:Helvetica,Arial,sans-serif;font-size:16px;font-weight:400;line-height:1.3;margin:0;padding:0!important;text-align:left;width:0'></th></tr></table></th></tr></tbody></table></th></tr></table></th></tr></tbody></table></td></tr></tbody></table></center></td></tr></table><div style='display:none;white-space:nowrap;font:15px courier;line-height:0'></div></div></body></html>";
        return empHtml;

    }


    protected void SendEmail(string toEmpOrApp, string approverStatus, int expDetailId, int approverId)
    {
        detail = new CorporateProfileExpenseDetails(expDetailId);
        detail.ExpDetailId = expDetailId;
        try
        {
            if (approverStatus == "A")
            {
                _approvalStatus = "Approved";
            }
            else
            {
                _approvalStatus = "Rejected";
            }
            List<string> toArray = new System.Collections.Generic.List<string>();
            if (toEmpOrApp == "A" && approverStatus == "A")//To the approver
            {
                int hLevel = Convert.ToInt32(detail.ApprovalHierarachy);
                List<int> listOfAppId = detail.ProfileApproversList.Where(i => i.Hierarchy == (hLevel + 1)).Select(i => i.ApproverId).ToList();
                if (listOfAppId != null && listOfAppId.Count > 0)
                {
                    foreach (int appId in listOfAppId)
                    {
                        approverNames = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverName).FirstOrDefault();
                        string appEmail = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverEmail).FirstOrDefault();
                        if (!string.IsNullOrEmpty(appEmail))
                        {
                            toArray.Add(appEmail);
                            string appHtml = getAppHtmlText(detail, _approvalStatus, appId);
                            string subject = "Expense Approval Request";
                            if (toArray != null && toArray.Count > 0)
                            {
                                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, appHtml, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                                toArray.Clear();
                            }
                            approverNames = string.Empty;
                        }
                    }
                }



            }

            else //To The Employee
            {
                approverNames = detail.ProfileApproversList.Where(i => i.ApproverId == approverId).Select(i => i.ApproverName).FirstOrDefault();
                if (!string.IsNullOrEmpty(detail.EmpEmail))
                {
                    toArray.Add(detail.EmpEmail);
                }
                string empHTML = getEmpHtmlText(detail, _approvalStatus);
                string subject = "Expense Status Change Notification";
                if (toArray != null && toArray.Count > 0)
                {
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, empHTML, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                }
                approverNames = string.Empty;
            }


        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, 0, "(hn_CorporateApprovals.aspx)Failed to Send Email For Employee and Approvers: Reason - " + ex.ToString(), "");
        }

    }


}