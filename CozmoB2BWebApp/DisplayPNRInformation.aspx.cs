﻿using CCA.Util;
using CT.AccountingEngine;
using CT.BookingEngine;
using CT.BookingEngine.GDS;
using CT.Configuration;
using CT.Core;
using CT.Corporate;
using CT.MetaSearchEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Text.RegularExpressions;
using System.Transactions;
using System.Web;
using System.Web.UI.WebControls;

public partial class DisplayPNRInformationGUI : CT.Core.ParentPage// System.Web.UI.Page
{
    protected FlightItinerary itinerary = new FlightItinerary();
    protected FlightItinerary clsFLITDB = new FlightItinerary();
    protected string bookingSource = string.Empty;
    protected Ticket[] ticket;
    protected string fareBasisCodeOB = string.Empty;
    protected string fareBasisCodeIB = string.Empty;
    protected List<string> ticketNumbers = new List<string>();
    protected bool fromPendingQueue = false;
    protected AgentMaster agency;
    protected bool noCredit = false;
    protected bool overrideCreditLimit = false;
    protected LocationMaster location = new LocationMaster();
    protected bool onBehalf;
    protected bool isLCC = false;
    protected decimal amountToCompare = 0;
    protected Fare[] fareBreakDown;
    protected decimal[] otherCharges;
    protected decimal totalPublished;
    protected decimal handlingCharge;
    protected decimal addHandlingCharge;
    protected decimal additionalTxnFee;
    protected decimal tdsHc;
    protected decimal tdsAddHandlingCharge;
    protected decimal reverseHandlingCharge;
    protected decimal serviceTax;
    protected decimal totalAgentprice;
    protected decimal totalTransFee, discount;
    protected bool BookingSaved = false;
    protected string ShowMessage = string.Empty;
    protected bool saveAllowed = true;
    protected string errorMessage = string.Empty;
    protected bool isHold = false;
    protected bool savePnr = false;
    protected BookingDetail booking = new BookingDetail();
    private const int TimeLimitToComplete = 7;
    protected string ShowErrorMessage = string.Empty;
    protected bool SaveError = false;
    protected bool hasInfant = false;
    protected bool modalPopShow = false;
    protected List<Ticket> ticketList = new List<Ticket>();

    Dictionary<string, string> sFeeTypeList = new Dictionary<string, string>();
    Dictionary<string, string> sFeeValueList = new Dictionary<string, string>();

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (Settings.LoginInfo != null)
            {
                if (!Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                }
                else
                {
                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                }

                if (agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Card)
                {
                    ddlPaymentType.Enabled = true;
                }
                else if (agency.PaymentMode == CT.TicketReceipt.BusinessLayer.PaymentMode.Credit)
                {
                    ddlPaymentType.SelectedIndex = 0;
                    ddlPaymentType.Enabled = false;
                }

                if (!IsPostBack)
                {
                    if (Session["itinerary"] != null)
                    {
                        Session["itinerary"] = null;
                    }
                    if (Session["FlightItinerary"] != null)
                    {
                        Session["FlightItinerary"] = null;
                    }
                    if (Request["source"] != null && Request["source"] != string.Empty)
                    {
                        bookingSource = Convert.ToString(Request["source"]);
                    }
                    //if (Settings.LoginInfo.AgentId > 0)
                    //{
                    //    onBehalf = false;
                    //    //agency = new AgentMaster(Settings.LoginInfo.AgentId);
                    //}
                    if (Session["FlightItinerary"] != null)
                    {
                        itinerary = Session["FlightItinerary"] as FlightItinerary;
                        Session["itinerary"] = itinerary;
                    }
                    else
                    {
                        GetFlightItinerary();
                    }
                }
                else
                {
                    itinerary = (FlightItinerary)Session["itinerary"];
                    ticket = (Ticket[])Session["ticket"];
                    ticketNumbers = (List<string>)Session["ticketNumber"];
                }


                if (Request.QueryString["ErrorPG"] != null)//Error returned from Payment Gateway
                {
                    Utility.Alert(this, Request.QueryString["ErrorPG"]);
                }
                else if (Request.Form["encResp"] != null)//Payment returned from CCAvenue
                {
                    string orderId = string.Empty;
                    string paymentId = string.Empty;

                    string workingKey = ConfigurationManager.AppSettings["CCA_Encrypt_Key"];
                    CCACrypto ccaCrypto = new CCACrypto();
                    string encResponse = ccaCrypto.Decrypt(Request.Form["encResp"], workingKey);
                    Audit.Add(EventType.Book, Severity.Low, 1, "(Import PNR)CCAvenue response data : " + encResponse, Request["REMOTE_ADDR"]);
                    NameValueCollection Params = new NameValueCollection();
                    string[] segments = encResponse.Split('&');
                    foreach (string seg in segments)
                    {
                        string[] parts = seg.Split('=');
                        if (parts.Length > 0)
                        {
                            string Key = parts[0].Trim();
                            string Value = parts[1].Trim();
                            Params.Add(Key, Value);
                        }
                    }

                    if (Params["order_status"] == "Success")
                    {
                        orderId = Params["order_id"];
                        //paymentId = Params["payment_id"];
                        paymentId = Params["tracking_id"];

                        UseCredit_Click();

                        int paymentInformationId = 0;

                        if (Session["PaymentInformationId"] != null)
                        {
                            paymentInformationId = Convert.ToInt32(Session["PaymentInformationId"]);
                        }

                        try
                        {
                            CreditCardPaymentInformation creditCard = new CreditCardPaymentInformation();
                            creditCard.PaymentId = paymentId;
                            creditCard.PaymentInformationId = paymentInformationId;
                            creditCard.PaymentStatus = 1; //Success
                            creditCard.ReferenceId = itinerary.BookingId;
                            creditCard.Remarks = ShowMessage;
                            creditCard.UpdatePaymentDetails();
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, ex.ToString(), Request["REMOTE_ADDR"]);
                        }
                        finally
                        {
                            Session["PaymentInformationId"] = null;//clear the credit card id stored in session                            
                            Session["FlightItinerary"] = null;//Clear Itinerary
                        }
                    }
                    else
                    {
                        errorMessage = "Payment declined due to technical error or due to invalid details";
                    }
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }


        }
        catch (Exception ex)
        {
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void GetFlightItinerary()
    {
        string displayErrormsg = string.Empty;
        try
        {
          
            int flightId = FlightItinerary.GetFlightId(Request["pnrNo"]);
            if (flightId <= 0)
            {
                if (Session["itinerary"] == null)
                {
                    int agencyId = 0;
                    if (bookingSource == "UAPI")
                    {
                        SourceDetails agentDetails = default(SourceDetails);
                        CT.BookingEngine.GDS.UAPI uapi = new UAPI();
                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["UA"];
                            uapi.agentBaseCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                            uapi.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                            agencyId = Settings.LoginInfo.OnBehalfAgentID;
                        }
                        else
                        {
                            agentDetails = Settings.LoginInfo.AgentSourceCredentials["UA"];
                            uapi.agentBaseCurrency = Settings.LoginInfo.Currency;
                            uapi.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                            agencyId = Settings.LoginInfo.AgentId;
                        }
                        //CT.BookingEngine.GDS.UAPI.UserName = agentDetails.UserID;
                        //CT.BookingEngine.GDS.UAPI.Password = agentDetails.Password;
                        //CT.BookingEngine.GDS.UAPI.TargetBranch = agentDetails.HAP;

                        CT.BookingEngine.GDS.UAPICredentials credentials = new CT.BookingEngine.GDS.UAPICredentials();
                        credentials.UserName = agentDetails.UserID;
                        credentials.Password = agentDetails.Password;
                        credentials.TargetBranch = agentDetails.HAP;
                        uapi.SessionId = Guid.NewGuid().ToString();
                        uapi.AppUserId = (Settings.LoginInfo != null ? Settings.LoginInfo.UserID.ToString() : "");
                        uapi.EndpointURL = agentDetails.EndPoint;
                        agency = new AgentMaster(agencyId);
                        agency.UpdateBalance(0);
                        if (agency.CurrentBalance > 0)
                        {
                            UAPIdll.Universal46.UniversalRecordImportRsp URimportResponse = new UAPIdll.Universal46.UniversalRecordImportRsp();
                            itinerary = uapi.RetrieveItinerary(Request["pnrNo"], out ticket, credentials, ref URimportResponse);
                            itinerary.Passenger[0].CorpProfileId = Request["prfid"] != null && Request["prfid"] != string.Empty ? Request["prfid"] : string.Empty;

                            if (!string.IsNullOrEmpty(itinerary.Passenger[0].CorpProfileId))
                                GetFlexData(URimportResponse);

                            Session["FlightItinerary"] = itinerary;
                        }
                        else
                        {
                            Response.Redirect("DisplayPNR.aspx?pnrNo=" + Request["pnrNo"] + "&sr=" + Request["source"] + "&error=Insufficient Agent Balance");
                        }

                        /**********************************************************************************************
                         *                      Retrieving Location details for OnBehalf Agency
                         * ********************************************************************************************/
                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            string agentType = string.Empty;
                            switch (agency.AgentType)
                            {
                                case 1:
                                    agentType = "BASE";
                                    break;
                                case 2:
                                    agentType = "Agent";
                                    break;
                                case 3:
                                    agentType = "B2B";
                                    break;
                                case 4:
                                    agentType = "B2B2B";
                                    break;
                            }

                            DataTable dtLocations = LocationMaster.GetList((int)agency.ID, ListStatus.Short, RecordStatus.Activated, agentType);

                            /****************************************End retrieving Location details***********************************************/

                            /*********************************************************************************************
                            *                       Assigning Location Id of the agency to the Itinerary
                            * *******************************************************************************************/
                            if (dtLocations != null && dtLocations.Rows.Count > 0)
                            {
                                if (dtLocations.Rows[0]["LOCATION_ID"] != DBNull.Value)
                                {
                                    itinerary.LocationId = Convert.ToInt32(dtLocations.Rows[0]["LOCATION_ID"]);
                                }
                            }
                        }
                        /*****************************End Location Id assigning***************************************/

                        //int flightId = FlightItinerary.GetFlightId(Request["pnrNo"]);
                        //if (flightId > 0)
                        //{
                        //    FlightItinerary urItinerary = new FlightItinerary(flightId);
                        //    //FlightItinerary urItinerary = new FlightItinerary(FlightItinerary.GetFlightId(Request["pnrNo"]));
                        //    string uRecord = urItinerary.UniversalRecord;
                        //    if (!string.IsNullOrEmpty(uRecord))
                        //    {
                        //        itinerary = UAPI.RetrieveItinerary(uRecord, out ticket);
                        //    }
                        //}
                        //else
                        //{
                        //    itinerary = UAPI.RetrieveItinerary(Request["pnrNo"], out ticket);
                        //}
                    }
                    else if (bookingSource == "G9")
                    {
                        AirArabia api = new AirArabia();
                        itinerary = api.RetrieveItinerary(Request["pnrNo"]);
                        ticket = new Ticket[0];
                        Session["ticketNumber"] = ticketNumbers;
                    }

                    /*if (bookingSource == "1P")
                    {
                        //For world span
                        // itinerary = Worldspan.RetrieveItinerary(Request["pnrNo"], out ticket); 
                    }
                    else if (bookingSource == "SG")
                    {
                        bool modeSpicejet = (ConfigurationSystem.SpiceJetConfig["Mode"] == "SpiceJet");
                        if (modeSpicejet)
                        {
                            //SpiceJetAPI sp = new SpiceJetAPI();
                            //itinerary = sp.RetrieveItinerary(Request["pnrNo"], ref fareBasisCodeOB, ref fareBasisCodeIB);
                            //ticket = new Ticket[0];
                        }
                        else
                        {
                            //Navitaire navi = new Navitaire("0S");
                            //itinerary = navi.RetrieveItinerary(Request["pnrNo"], ref fareBasisCodeOB, ref fareBasisCodeIB);
                            //ticket = new Ticket[0];
                        }
                        // For Spice Jet
                    }
                    else if (bookingSource == "6E")
                    {
                        // For Spice Jet
                        //Navitaire navi = new Navitaire("6E");
                        //itinerary = navi.RetrieveItinerary(Request["pnrNo"], ref fareBasisCodeOB, ref fareBasisCodeIB);
                        //ticket = new Ticket[0];
                    }
                    else if (bookingSource == "I7")
                    {
                        // For Paramount
                        //itinerary = ParamountApi.RetrieveItinerary(Request["pnrNo"], ref fareCode, ref ticketNumbers);
                        //itinerary.FareRules = ParamountApi.GetFareRuleScreenScrape(itinerary.Origin, itinerary.Destination, fareCode);
                        //ticket = new Ticket[0];
                        //Session["ticketNumber"] = ticketNumbers;
                    }
                    else if (bookingSource == "DN")
                    {
                        // For Air Deccan
                        //AirDeccanApi airDeccan = new AirDeccanApi();
                        //itinerary = airDeccan.RetrieveItinerary(Request["pnrNo"], ref fareBasisCodeOB);
                        //itinerary.FareRules = AirDeccanApi.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB);
                        //ticket = new Ticket[0];
                        //Session["ticketNumber"] = ticketNumbers;
                    }
                    else if (bookingSource == "1A")
                    {
                        //itinerary = Amadeus.RetrieveItinerary(Request["pnrNo"], out ticket);
                    }
                    else if (bookingSource == "9H")
                    {
                        // For MDLR
                        //itinerary = Mdlr.RetrieveItinerary(Request["pnrNo"], ref fareCode, ref ticketNumbers);
                        //itinerary.FareRules = Mdlr.GetFareRule(itinerary.Origin, itinerary.Destination, fareCode);
                        //ticket = new Ticket[0];
                        //Session["ticketNumber"] = ticketNumbers;
                    }
                    else if (bookingSource == "G8")
                    {
                        // For Go Air
                        //GoAirApi airDeccan = new GoAirApi();
                        //itinerary = airDeccan.RetrieveItinerary(Request["pnrNo"], ref fareBasisCodeOB);
                        //itinerary.FareRules = GoAirApi.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB);
                        //ticket = new Ticket[0];
                    }
                    else if (bookingSource == "1G")
                    {
                        //itinerary = GalileoApi.RetrieveItinerary(Request["pnrNo"], out ticket);
                        itinerary = UAPI.RetrieveItinerary(Request["pnrNo"], out ticket);


                    }
                    else if (bookingSource == "UAPI")
                    {
                        int flightId = FlightItinerary.GetFlightId(Request["pnrNo"]);
                        if (flightId > 0)
                        {
                            FlightItinerary urItinerary = new FlightItinerary(flightId);
                            //FlightItinerary urItinerary = new FlightItinerary(FlightItinerary.GetFlightId(Request["pnrNo"]));
                            string uRecord = urItinerary.UniversalRecord;
                            if (!string.IsNullOrEmpty(uRecord))
                            {
                                itinerary = UAPI.RetrieveItinerary(uRecord, out ticket);
                            }
                        }
                        else
                        {
                            itinerary = UAPI.RetrieveItinerary(Request["pnrNo"], out ticket);
                        }
                    }
                    else if (bookingSource == "ZS")
                    {
                        // For SAMA
                        //itinerary = Sama.RetrieveItinerary(Request["pnrNo"]);
                        //ticket = new Ticket[0];
                    }
                    else if (bookingSource == "IX")
                    {
                        //HermesApi api = new HermesApi();
                        //itinerary = api.RetrieveItinerary(Request["pnrNo"], ref ticketNumbers);
                        //ticket = new Ticket[0];
                        //Session["ticketNumber"] = ticketNumbers;
                    }
                    else if (bookingSource == "G9")
                    {
                        AirArabia api = new AirArabia();
                        itinerary = api.RetrieveItinerary(Request["pnrNo"]);
                        ticket = new Ticket[0];
                        Session["ticketNumber"] = ticketNumbers;
                    }*/
                    //else
                    //{
                    //    Response.Redirect("DisplayPNR.aspx", true);
                    //}
                }
                else
                {
                    itinerary = (FlightItinerary)Session["itinerary"];
                    ticket = (Ticket[])Session["ticket"];
                    ticketNumbers = (List<string>)Session["ticketNumber"];
                }
                if (fromPendingQueue)
                {
                    itinerary.BookingMode = BookingMode.Auto;
                }
                else
                {
                    itinerary.BookingMode = BookingMode.Import;
                }
                itinerary.AgencyId = Convert.ToInt32(agency.ID);
                if (!Settings.LoginInfo.IsOnBehalfOfAgent)//For Self
                {
                    itinerary.LocationId = Convert.ToInt32(Settings.LoginInfo.LocationID);
                }
                else//for On behalf of
                {
                    itinerary.LocationId = Settings.LoginInfo.OnBehalfAgentLocation;
                }
                itinerary.PaymentMode = ModeOfPayment.Cash;

                location = Settings.LoginInfo.IsOnBehalfOfAgent ? new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation) : new LocationMaster(Settings.LoginInfo.LocationID);

                //Calculate Markup
                DataTable dtAgentMarkup = new DataTable();
                if (agency.ID > 0)
                {
                    List<FlightInfo> flightInfos = new List<FlightInfo>();
                    flightInfos.AddRange(itinerary.Segments);
                    bool isReturnFlight = flightInfos.Exists(delegate (FlightInfo fi) { return fi.Group == 1; });
                    bool isAirlineLCC = Airline.IsAirlineLCC(itinerary.Segments[0].Airline);
                    bool isInternational = flightInfos.Exists(delegate (FlightInfo fi) { return fi.Origin.CountryCode != fi.Destination.CountryCode; });

                    //Retrieve Import PNR Markup
                    dtAgentMarkup = UpdateMarkup.GetAllMarkup((int)agency.ID, 1, "I");
                    if (dtAgentMarkup != null && dtAgentMarkup.Rows.Count > 0 && (bookingSource == "UAPI" || bookingSource == "G9"))
                    {
                        string source = string.Empty;
                        if (bookingSource == "UAPI")
                        {
                            source = SearchResult.GetFlightBookingSource(BookingSource.UAPI);
                        }
                        else if (bookingSource == "G9")
                        {
                            source = SearchResult.GetFlightBookingSource(BookingSource.AirArabia);
                        }
                        dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND FlightType='" + (isInternational ? "INT" : "DOM") + "' AND JourneyType='" + (isReturnFlight ? "RET" : "ONW") + "' AND CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "'";

                        if (dtAgentMarkup.DefaultView.Count == 0)//FlightType not found FlightType=All, JourneyType=ONW,RET, CarrierType=GDS,LCC
                        {
                            dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (isReturnFlight ? "RET" : "ONW") + "') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "')";
                        }
                        if (dtAgentMarkup.DefaultView.Count == 0)//FlightType, JourneyType not found FlightType=All, JourneyType=All, CarrierType=GDS,LCC
                        {
                            dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (isReturnFlight ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "')";
                        }
                        if (dtAgentMarkup.DefaultView.Count == 0)//CarrierType, JourneyType not found FlightType=INT,DOM, JourneyType=All, CarrierType=All
                        {
                            dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "') AND (JourneyType='" + (isReturnFlight ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                        }
                        if (dtAgentMarkup.DefaultView.Count == 0)//FlightType, CarrierType not found FlightType=All, JourneyType=ONW,RET, CarrierType=All
                        {
                            dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (isReturnFlight ? "RET" : "ONW") + "') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                        }

                        if (dtAgentMarkup.DefaultView.Count == 0)//FlightType, JourneyType, CarrierType not found FlightType=All, JourneyType=All, CarrierType=All
                        {
                            dtAgentMarkup.DefaultView.RowFilter = "TransType='B2B' AND SourceId=" + source + " AND ProductId=1 AND (FlightType='" + (isInternational ? "INT" : "DOM") + "' OR FlightType='All') AND (JourneyType='" + (isReturnFlight ? "RET" : "ONW") + "' OR JourneyType='All') AND (CarrierType='" + (isAirlineLCC ? "LCC" : "GDS") + "' OR CarrierType='All')";
                        }
                    }
                }
                decimal discount = 0;
                itinerary.Passenger.ToList().ForEach(pax =>
                {
                    //1.Markup  Calculation.

                    //When the source is TBO Air need to consider the below components from the price object for Markup.
                    //1.AdditionalTxnFee
                    //2.OtherCharges
                    //3.SServiceFee
                    //4.TransactionFee

                    if (dtAgentMarkup != null && dtAgentMarkup.DefaultView.Count > 0)
                    {
                        //1.Markup  Calculation
                        decimal agtMarkUp = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Markup"]);
                        //Case-1: If markup type is percentage.
                        if (Convert.ToString(dtAgentMarkup.DefaultView[0]["MarkupType"]) == "P")
                        {
                            pax.Price.MarkupType = "P";
                            pax.Price.MarkupValue = agtMarkUp;
                            if (agency.AgentAirMarkupType == "BF") //Base Fare
                            {
                                pax.Price.Markup = (pax.Price.PublishedFare * agtMarkUp) / 100;
                            }
                            else if (agency.AgentAirMarkupType == "TX") //Tax Fare
                            {
                                if (bookingSource == "TA")//For TBO Air
                                {
                                    pax.Price.Markup = (Convert.ToDecimal(pax.Price.Tax + pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee) * agtMarkUp) / 100;
                                }
                                else
                                {
                                    pax.Price.Markup = (Convert.ToDecimal(pax.Price.Tax) * agtMarkUp) / 100;
                                }
                            }
                            else if (agency.AgentAirMarkupType == "TF") //BaseFare + Tax Fare
                            {
                                if (bookingSource == "TA")//For TBO Air
                                {
                                    pax.Price.Markup = (Convert.ToDecimal(pax.Price.PublishedFare + pax.Price.Tax + pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee) * agtMarkUp) / 100;
                                }
                                else
                                {
                                    pax.Price.Markup = (Convert.ToDecimal(pax.Price.PublishedFare + pax.Price.Tax) * agtMarkUp) / 100;
                                }
                            }
                        }
                        //Case-2: If markup type is fixed.
                        else
                        {
                            pax.Price.Markup = agtMarkUp;
                            pax.Price.MarkupType = "F";
                            pax.Price.MarkupValue = agtMarkUp;
                        }

                        //2.Discount Calculation
                        discount = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["Discount"]);
                        //Case-1: If Discount type is percentage.
                        if (Convert.ToString(dtAgentMarkup.DefaultView[0]["DiscountType"]) == "P")
                        {
                            pax.Price.DiscountType = "P";
                            pax.Price.DiscountValue = discount;
                            if (agency.AgentAirMarkupType == "BF") // Base Fare
                            {
                                pax.Price.Discount = (pax.Price.PublishedFare * discount) / 100;
                            }
                            else if (agency.AgentAirMarkupType == "TX")// Tax
                            {
                                if (bookingSource == "TA")//For TBO Air
                                {
                                    pax.Price.Discount = (Convert.ToDecimal(pax.Price.Tax + pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee) * discount) / 100;
                                }
                                else
                                {
                                    pax.Price.Discount = (Convert.ToDecimal(pax.Price.Tax) * discount) / 100;
                                }
                            }
                            else if (agency.AgentAirMarkupType == "TF") // Total Fare = BaseFare + Tax
                            {
                                if (bookingSource == "TA")//For TBO Air
                                {
                                    pax.Price.Discount = (Convert.ToDecimal(pax.Price.PublishedFare + pax.Price.Tax + pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee) * discount) / 100;
                                }
                                else
                                {
                                    pax.Price.Discount = (Convert.ToDecimal(pax.Price.PublishedFare + pax.Price.Tax) * discount) / 100;
                                }
                            }
                        }
                        //Case-2: If Discount type is fixed.
                        else
                        {
                            pax.Price.Discount = discount;
                            pax.Price.DiscountType = "F";
                            pax.Price.DiscountValue = discount;
                        }

                        //3.Handling Fee calculation For Self and On behalf 
                        if (location.CountryCode == "IN")
                        {
                            decimal handlingFeeValue = 0m, handlingFeeAmount = 0m;
                            if (dtAgentMarkup.DefaultView[0]["HandlingFeeValue"] != DBNull.Value)
                            {
                                handlingFeeValue = Convert.ToDecimal(dtAgentMarkup.DefaultView[0]["HandlingFeeValue"]);
                            }

                            pax.Price.HandlingFeeType = Convert.ToString(dtAgentMarkup.DefaultView[0]["HandlingFeeType"]);
                            pax.Price.HandlingFeeValue = handlingFeeValue;

                            //No need to consider the below components for handling fee calculation
                            //1.Discount
                            //2.B2CMarkup
                            decimal totalFare = (decimal)pax.Price.PublishedFare + pax.Price.Tax + pax.Price.Markup;
                            if (bookingSource == "TA")//TBOAir
                            {
                                totalFare = (decimal)pax.Price.PublishedFare + pax.Price.Tax + pax.Price.Markup + pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                            }
                            if (pax.Price.HandlingFeeType == "P")
                            {
                                handlingFeeAmount = (totalFare * handlingFeeValue / 100);
                            }
                            else//Fixed
                            {
                                handlingFeeAmount = handlingFeeValue;
                            }
                            pax.Price.HandlingFeeAmount = handlingFeeAmount;
                        }                        
                    }
                    //4. Gst/Vat calculation for self and on behalf.
                    decimal vatAmount = 0m;
                    
                    if (location.CountryCode == "IN")
                    {
                        List<GSTTaxDetail> gstTaxList = new List<GSTTaxDetail>();
                        decimal gstAmount = 0;

                        gstAmount = GSTTaxDetail.LoadGSTValues(ref gstTaxList, pax.Price.Markup - pax.Price.DynamicMarkup, (location.ID));

                        pax.Price.OutputVATAmount = gstAmount;
                        pax.Price.GSTDetailList = gstTaxList;
                        vatAmount += Convert.ToDecimal(gstAmount.ToString("N" + agency.DecimalValue));
                    }
                    else
                    {
                        decimal tax = 0;
                        Airport destinationAirport = itinerary.Segments.Last(s => s.Group == 0).Destination;
                        Airport originAirport = itinerary.Segments[0].Origin;
                        string countryCode = string.Empty, supplierCountryCode = string.Empty, originCountryCode = string.Empty, destinationCountryCode = string.Empty, destinationType = string.Empty;
                        if (originAirport != null)
                        {
                            originCountryCode = originAirport.CountryCode;
                        }
                        if (destinationAirport != null)
                        {
                            destinationCountryCode = destinationAirport.CountryCode;
                        }
                        if (originCountryCode == "AE" || originCountryCode == "SA")
                        {
                            countryCode = originCountryCode;
                        }
                        else if (destinationCountryCode == "AE" || destinationCountryCode == "SA")
                        {
                            countryCode = destinationCountryCode;
                        }
                        if (originCountryCode == destinationCountryCode)
                        {
                            destinationType = "Domestic";
                        }
                        else
                        {
                            destinationType = "International";
                        }
                        Dictionary<string, string> ActiveSources = AgentMaster.GetActiveSourcesByProduct((int)ProductType.Flight);
                        supplierCountryCode = ActiveSources.ContainsKey(bookingSource) ? ActiveSources[bookingSource] : string.Empty;
                        PriceTaxDetails taxDetails = null;

                        taxDetails = PriceTaxDetails.GetVATCharges(countryCode, location.CountryCode, supplierCountryCode, (int)ProductType.Flight, Module.Ticket.ToString(), destinationType);                        
                        
                        pax.Price.TaxDetails = taxDetails;
                        if (taxDetails != null && taxDetails.InputVAT != null)
                        {
                            taxDetails.InputVAT.CalculateVatAmount(pax.Price.BaseFare + pax.Price.Tax, ref vatAmount, Settings.LoginInfo.DecimalValue);
                            pax.Price.InputVATAmount = vatAmount;
                            if (!taxDetails.InputVAT.CostIncluded)
                            {
                                tax = vatAmount;
                            }
                        }
                        if (pax.Price.TaxDetails != null && pax.Price.TaxDetails.OutputVAT != null)
                        {
                            OutputVATDetail outVat = itinerary.Passenger[0].Price.TaxDetails.OutputVAT;
                            decimal markup = 0m, paxtotalFare = 0m;

                            paxtotalFare += Convert.ToDecimal(pax.Price.BaggageCharge.ToString("N" + agency.DecimalValue)) +
                            Convert.ToDecimal(pax.Price.PublishedFare.ToString("N" + agency.DecimalValue)) -
                            Convert.ToDecimal(pax.Price.Discount.ToString("N" + agency.DecimalValue)) +
                            Convert.ToDecimal(pax.Price.Tax.ToString("N" + agency.DecimalValue)) +
                            Convert.ToDecimal(pax.Price.OtherCharges.ToString("N" + agency.DecimalValue)) +
                            Convert.ToDecimal(pax.Price.MealCharge.ToString("N" + agency.DecimalValue)) +
                            Convert.ToDecimal(pax.Price.SeatPrice.ToString("N" + agency.DecimalValue)) +
                            Convert.ToDecimal(pax.Price.Markup.ToString("N" + agency.DecimalValue));
                            markup += Convert.ToDecimal(pax.Price.Markup.ToString("N" + agency.DecimalValue));

                            if (agency.AgentType == (int)AgentType.Agent)
                            {
                                markup += Convert.ToDecimal(pax.Price.AsvAmount.ToString("N" + agency.DecimalValue));
                                paxtotalFare += Convert.ToDecimal(pax.Price.AsvAmount.ToString("N" + agency.DecimalValue));
                            }
                            //Output vat must not be calculated including HandlingFee so deduct it from TotalFare
                            vatAmount = outVat.CalculateVatAmount((decimal)paxtotalFare, markup, agency.DecimalValue);
                            pax.Price.OutputVATAmount = Convert.ToDecimal(vatAmount.ToString("N" + agency.DecimalValue));
                        }
                    }

                });
            }
            else
            {
                agency = new AgentMaster(Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId);
                agency.UpdateBalance(0);
                if (agency.CurrentBalance == 0)
                {
                    Response.Redirect("DisplayPNR.aspx?pnrNo=" + Request["pnrNo"] + "&sr=" + Request["source"] + "&error=Insufficient Agent Balance", false);
                    return;
                }

                clsFLITDB = new FlightItinerary(flightId);
                itinerary = new FlightItinerary(flightId);
                AgentMaster bookingAgent = new AgentMaster(clsFLITDB.AgencyId);
                BookingDetail clsBookingDetail = new BookingDetail(clsFLITDB.BookingId);
                if (clsFLITDB.AgencyId != agency.ID)
                {
                    Response.Redirect("DisplayPNR.aspx?pnrNo=" + Request["pnrNo"] + "&sr=" + Request["source"] + "&error=PNR (" + clsFLITDB.PNR + ") already exists with another Agent: " + bookingAgent.Name, false);
                    return;
                }
                else if (clsBookingDetail.Status != BookingStatus.Hold && clsBookingDetail.Status != BookingStatus.InProgress
                    && clsBookingDetail.Status != BookingStatus.Ready || clsBookingDetail.Status == BookingStatus.Ticketed)
                {
                    Response.Redirect("DisplayPNR.aspx?pnrNo=" + Request["pnrNo"] + "&sr=" + Request["source"] + "&error=PNR (" + clsFLITDB.PNR + ") already exists with booking status : " + clsBookingDetail.Status.ToString(), false);
                    return;
                }
                

                if (Session["itinerary"] == null)
                {
                    //bookingSource = bookingSource == "UAPI" ? "UA" : bookingSource;
                    MetaSearchEngine clsMSE = new MetaSearchEngine();
                    clsMSE.SettingsLoginInfo = Settings.LoginInfo;

                    clsMSE.SessionId = (Session["sessionId"] == null) ? Guid.NewGuid().ToString() : Convert.ToString(Session["sessionId"]);

                    UAPIdll.Universal46.UniversalRecordImportRsp clsURIRsp = new UAPIdll.Universal46.UniversalRecordImportRsp();
                    clsMSE.RetrievePNR(itinerary, (BookingSource)Enum.Parse(typeof(BookingSource), bookingSource),
                        ref ticket, HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"], ref clsURIRsp);
                    itinerary.Passenger[0].CorpProfileId = Request["prfid"] != null && Request["prfid"] != string.Empty ? Request["prfid"] : string.Empty;

                    if (!string.IsNullOrEmpty(itinerary.Passenger[0].CorpProfileId))
                        GetFlexData(clsURIRsp);
                    Session["itinerary"] = itinerary;

                    location = Settings.LoginInfo.IsOnBehalfOfAgent ? new LocationMaster(Settings.LoginInfo.OnBehalfAgentLocation) : new LocationMaster(Settings.LoginInfo.LocationID);
                }
                else
                {
                    itinerary = (FlightItinerary)Session["itinerary"];
                    ticket = (Ticket[])Session["ticket"];
                    ticketNumbers = (List<string>)Session["ticketNumber"];
                }

                itinerary.BookingMode = fromPendingQueue ? BookingMode.Auto : BookingMode.Import;
            }
        }
        catch (Exception ex)
        {
            displayErrormsg = ex.Message;
            Audit.Add(EventType.Book, Severity.High, 1, "(Import PNR)Failed to import PNR. Reason: " + ex.Message, Request["REMOTE_ADDR"]);
            displayErrormsg = "Failed to import PNR. Reason: " + (ex.Message.Length > 112 ? ex.Message.Substring(0, 111) : ex.Message);
        }
        if (itinerary.Segments == null || itinerary.Segments.Length <= 0 || (itinerary.FlightId > 0 && Session["itinerary"] == null))
        {
            // Response.Redirect("DisplayPNR.aspx?pnrNo=" + Request["pnrNo"] + "&sr=" + Request["source"] + "&error=PNRcancelled");
            Response.Redirect("DisplayPNR.aspx?pnrNo=" + Request["pnrNo"] + "&sr=" + Request["source"] + "&error="+ displayErrormsg);
        }
        if (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource == BookingSource.HermesAirLine || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp)
        {
            isLCC = true;
        }
        if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
        {
            fromPendingQueue = true;
        }
        if (isLCC)
        {
            // amountToCompare = AccountingEngine.Ledger.IsAgentCapable(agency.AgencyId, AccountingEngine.AccountType.LCC); //sai
        }
        else
        {
            // amountToCompare = AccountingEngine.Ledger.IsAgentCapable(agency.AgencyId, AccountingEngine.AccountType.NonLCC); //sai
        }
        decimal totalPrice = 0;
        for (int k = 0; k < itinerary.Passenger.Length; k++)
        {
            totalPrice += itinerary.Passenger[k].Price.GetAgentPrice();
            totalPrice += (itinerary.Passenger[k].Price.Markup - itinerary.Passenger[k].Price.Discount);
        }

        if (amountToCompare < totalPrice)
        {
            noCredit = true;
        }
        //overrideCreditLimit = Role.IsAllowedTask(loggedMember.RoleId, (int)CT.Core1.Task.OverrideCreditLimit); //TODO Ziyad
        itinerary.TravelDate = itinerary.Segments[0].DepartureTime;
        // itinerary.IsDomestic = Utility.IsDomestic(itinerary.Segments, "" + ConfigurationSystem.LocaleConfig["CountryCode"] + ""); //TODO Ziyad
        //Check if more than one segment and round trip - which one is destination
        // Redirect to display PNR page and show modal pop to get input form user
        if (itinerary.Segments != null && itinerary.Segments.Length > 2 && Request["dest"] == null)
        {
            modalPopShow = true;
        }
        else if (Request["dest"] != null && Request["dest"].Length > 0)
        {
            itinerary.Destination = Convert.ToString(Request["dest"]);
        }
        else
        {
            if (itinerary.Segments.Length == 2 && (itinerary.Segments[0].Origin.CityCode == itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode))
            {
                itinerary.Destination = itinerary.Segments[0].Destination.CityCode;
            }
            else
            {
                itinerary.Destination = itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode;
            }
        }

        //if (!modalPopShow)
        //{

        //Price Calculation according to agency

        for (int i = 0; i < itinerary.Passenger.Length; i++)
        {
            if (i == 0)
            {
                itinerary.Passenger[i].IsLeadPax = true;
            }
            itinerary.Passenger[i].Nationality = itinerary.Passenger[i].Country;
            if (itinerary.Passenger[i].Title == null || itinerary.Passenger[i].Title.Length == 0)
            {
                Match titleMatch = Regex.Match(itinerary.Passenger[i].FirstName, ConfigurationSystem.AllowedTitles, RegexOptions.IgnoreCase);
                if (titleMatch.Success)
                {
                    itinerary.Passenger[i].Title = titleMatch.Value.Replace('.', ' ').Trim();
                    itinerary.Passenger[i].FirstName = itinerary.Passenger[i].FirstName.Substring(0, titleMatch.Index).Trim();
                }
            }
            if (itinerary.Passenger[i].Price == null)
            {
                itinerary.Passenger[i].Price = new PriceAccounts();
            }

            //if (itinerary.Passenger[i].Type == PassengerType.Infant)
            //{
            //    hasInfant = true;
            //}
        }
        //// adding created By
        for (int i = 0; i < itinerary.Segments.Length; i++)
        {
            if (isLCC)
            {
                itinerary.Segments[i].FlightStatus = FlightStatus.Confirmed;
                //TODO: Check whether this status is needed for LCCs or not
                itinerary.Segments[i].Status = "HK";
            }
            if (itinerary.Segments[i] != null)
            {
                itinerary.Segments[i].CreatedBy = (int)Settings.LoginInfo.UserID;
                itinerary.Segments[i].ETicketEligible = true;
            }
            //if (hasInfant)
            //{
            //    itinerary.Segments[i].ETicketEligible = false;
            //}
        }

        dlSaleSummaryDetails.DataSource = new List<FlightItinerary>() { itinerary };
        dlSaleSummaryDetails.DataBind();


        Session["itinerary"] = itinerary;
        Session["ticket"] = ticket;

        if (itinerary.Passenger[0].Price.PublishedFare + itinerary.Passenger[0].Price.Tax == new decimal(0))
        {
            BookingSaved = true;
            ShowMessage = "No Pricing Information <br> Please store the fare information in the pnr and then retrieve";
        }

        CheckForSaveAllowed();
    }
    private void CheckForSaveAllowed()
    {
        if (ticket.Length > itinerary.Passenger.Length)
        {
            Audit.Add(EventType.DisplayPNR, Severity.Normal, (int)Settings.LoginInfo.UserID, "Itinerary can not be saved as there exist multiple ticket for a passenger. PNR = " + itinerary.PNR, Request.ServerVariables["REMOTE_ADDR"]);
            saveAllowed = false;
            errorMessage = "Itinerary can not be saved as there exist multiple ticket for a passenger. Please contact Help Desk.";
        }
    }
    protected void UseCredit_Click()
    {
        //System.Threading.Thread.Sleep(5000);
        BookingResponse bookingResponse = BookItinerary(BookingStatus.Ready);
        BookingSaved = true;
        ShowMessage = "PNR Information has been Saved";
        savePnr = true;
        //expire session
        ClearBookingSession();
        if (booking.BookingId != 0)
        {
            AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);
            Settings.LoginInfo.AgentBalance = agent.UpdateBalance(0);
            Label lblAgentBalance = (Label)Master.FindControl("lblAgentBalance");
            lblAgentBalance.Text = CT.TicketReceipt.Common.Formatter.ToCurrency(Settings.LoginInfo.AgentBalance);

            Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + booking.BookingId+ "&pageFrom=importPNR", false);
        }
    }

    protected void PayLater_Click()
    {
        isHold = true;
        BookingResponse bookingResponse = BookItinerary(BookingStatus.Hold);
        BookingSaved = true;
        ShowMessage = "PNR Information has been Saved. Your booking is on Hold.";
        //expire 

        ClearBookingSession();
        if (booking.BookingId != 0)
        {
            Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + booking.BookingId, false);
        }
    }

    private void ClearBookingSession()
    {
        Session.Remove("itinerary");
        Session.Remove("ticket");
        Session.Remove("ticketNumber");
    }
    private BookingResponse BookItinerary(BookingStatus status)
    {
        BookingResponse bookingResponse = new BookingResponse();
        try
        {
            itinerary = (FlightItinerary)Session["itinerary"];

            if (!string.IsNullOrEmpty(hdnPaxFlexInfo.Value))
                itinerary.Passenger[0].FlexDetailsList = JsonConvert.DeserializeObject<List<FlightFlexDetails>>(hdnPaxFlexInfo.Value);
            // Creating booking detail 
            bool saveQueueForPending = false;
            bool generateInvoice = false;
            bool updateFailedBookingStatus = false;
            CT.Core.Queue queue = new CT.Core.Queue();
            booking.AgencyId = itinerary.AgencyId;
            booking.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
            booking.Status = status;
            // Getting itinerary from session and setting some properties

            if (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource == BookingSource.HermesAirLine || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp)
            {
                isLCC = true;
            }

            /**********************************Payment Mode************************************/
            //DataTable dtCharges = AgentCreditCardCharges.GetList(itinerary.AgencyId, 1);
            DataTable dtCharges = AgentCreditCardCharges.GetList();
            decimal charges = 0;
            //Assign the Payment mode 
            if (ddlPaymentType.SelectedValue == "Card")
            {
                itinerary.PaymentMode = ModeOfPayment.CreditCard;
                DataRow[] rows = dtCharges.Select("Productid IN ('" + 1 + "') AND Agentid IN ('" + agency.ID + "') AND PGSource IN ('" + (int)CT.AccountingEngine.PaymentGatewaySource.CCAvenue + "')");

                if (rows != null && rows.Length > 0)
                {
                    charges = Convert.ToDecimal(rows[0]["PGCharge"]);
                }
            }
            else
            {
                itinerary.PaymentMode = ModeOfPayment.Credit;
            }
            /**********************************************************************************/

            BookingHistory bh = new BookingHistory();
            bh.EventCategory = EventCategory.Booking;
            bh.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
            if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
            {
                fromPendingQueue = true;
                bh.Remarks = "Booking created";
            }
            else
            {
                bh.Remarks = "Booking created via import pnr";
            }
            ticket = (Ticket[])Session["ticket"];
            itinerary.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
            itinerary.IsOurBooking = false;
            itinerary.Origin = (string)itinerary.Segments[0].Origin.CityCode;
            if (Request["fareBasisCodeOB"] != null)
            {
                fareBasisCodeOB = Request["fareBasisCodeOB"].ToString();
            }
            if (string.IsNullOrEmpty(itinerary.Destination))
            {
                if (Request["dest"] != null && Request["dest"].Length > 0)
                {
                    itinerary.Destination = (string)Request["dest"];
                }
                else
                {
                    itinerary.Destination = itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode;
                }
            }
            // Sending itinerary for booking -- TO DO: check

            // TO DO: Fare Rules
            List<FareRule> fareRule = new List<FareRule>();
            if (itinerary.FlightBookingSource == BookingSource.WorldSpan)
            {
                // fareRule = Worldspan.GetFareRuleList(itinerary.FareRules);
            }
            else if (itinerary.FlightBookingSource == BookingSource.Galileo)
            {
                // fareRule = GalileoApi.GetFareRuleList(itinerary.FareRules);
            }
            else if (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp)
            {
                // fareRule = SpiceJetAPI.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB);
            }
            else if (itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.IndigoCorp)
            {
                // fareRule = Navitaire.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB, "6E");
            }
            else if (itinerary.FlightBookingSource == BookingSource.Paramount)
            {
                fareRule = itinerary.FareRules;
                for (int i = 0; i < fareRule.Count; i++)
                {
                    fareRule[i].Origin = itinerary.Segments[0].Origin.CityCode;
                    fareRule[i].Destination = itinerary.Destination;
                }
            }
            else if (itinerary.FlightBookingSource == BookingSource.AirDeccan)
            {
                // fareRule = AirDeccanApi.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB);
            }
            else if (itinerary.FlightBookingSource == BookingSource.Amadeus)
            {
                // fareRule = itinerary.FareRules;
            }
            else if (itinerary.FlightBookingSource == BookingSource.Mdlr)
            {
                fareRule = itinerary.FareRules;
                for (int i = 0; i < fareRule.Count; i++)
                {
                    fareRule[i].Origin = itinerary.Segments[0].Origin.CityCode;
                    fareRule[i].Destination = itinerary.Destination;
                }
            }
            else if (itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp)
            {
                // fareRule = GoAirApi.GetFareRule(itinerary.Origin, itinerary.Destination, fareBasisCodeOB);
            }
            else if (itinerary.FlightBookingSource == BookingSource.Sama)
            {
                //  fareRule = Sama.GetFareRule(itinerary.FareRules);
            }
            else if (itinerary.FlightBookingSource == BookingSource.HermesAirLine)
            {
                fareRule = itinerary.FareRules;
            }

            itinerary.FareRules = fareRule;

            decimal totalSellingPrice = 0;
            itinerary.Passenger.ToList().ForEach(pax =>
            {
                totalSellingPrice += (pax.Price.PublishedFare + pax.Price.Tax + pax.Price.Markup + pax.Price.OtherCharges + pax.Price.BaggageCharge + pax.Price.HandlingFeeAmount + pax.Price.MealCharge + pax.Price.SeatPrice - pax.Price.Discount + pax.Price.OutputVATAmount);
                pax.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
            });
            decimal agentCurrentBalance = agency.UpdateBalance(0);
            totalSellingPrice = Math.Round(totalSellingPrice, agency.DecimalValue);
            //try
            {
                if (agentCurrentBalance < totalSellingPrice)
                {
                    throw new ArgumentException(string.Format("Insufficient Credit Balance! Agent Current Balance :{0} Booking Amount :{1} Please contact Admin", agentCurrentBalance, totalSellingPrice));
                }
            }
            //catch (ArgumentException excep)
            //{
            //    BookingSaved = true;
            //    ShowErrorMessage = excep.Message;
            //    SaveError = true;
            //    booking.BookingId = 0;
            //}
            queue.QueueTypeId = (int)QueueType.Booking;
            queue.StatusId = (int)QueueStatus.Assigned;
            queue.AssignedTo = Convert.ToInt32(Session["memberId"]);
            queue.AssignedBy = Convert.ToInt32(Session["memberId"]);
            queue.AssignedDate = DateTime.Now.ToUniversalTime();
            queue.CompletionDate = DateTime.Now.ToUniversalTime().AddDays(TimeLimitToComplete);
            queue.CreatedBy = Convert.ToInt32(Session["memberId"]);
            // Saving the booked itinerary to database
            using (TransactionScope updateTransaction = new TransactionScope())
            {
                if (booking.Status == BookingStatus.Ready)
                {
                    // Agency.AlterCredit((-Convert.ToDouble(totalSellingPrice)), agency.AgencyId, !isAgent);
                }
                //We are also saving this bookingid in queue table
                //We are assigning same member to assignedTo,assignBy,Createdby
                //We take 7 as time limit of completion
                try
                {
                    int flightId = FlightItinerary.GetFlightId(itinerary.PNR);
                    //if (!(fromPendingQueue && (!isLCC || flightId > 0)))
                    if (flightId == 0)
                    {
                        saveQueueForPending = true;
                        booking.Save(itinerary, true);
                        // Utility.UpdateAirlinePNR(itinerary); // this utility is different sai
                    }
                    else
                    {
                        saveQueueForPending = false;
                        booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(flightId));
                        // Utility.UpdateAirlinePNR(itinerary); //sai
                        itinerary = itinerary.FlightId == 0 ? new FlightItinerary(flightId) : itinerary;
                        if (hdnPriceChange.Value == "Y")
                            itinerary.Passenger.ToList().ForEach(x => { x.Price.Save(); });
                        if (hdnSegChange.Value == "Y")
                            itinerary.Segments.ToList().ForEach(x => { x.Save(); });
                        if (itinerary.Passenger[0].FlexDetailsList != null && itinerary.Passenger[0].FlexDetailsList.Count() > 0)
                            itinerary.Passenger[0].FlexDetailsList.ForEach(x => { x.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID); x.PaxId = itinerary.Passenger[0].PaxId; x.Save(); });
                    }
                    if (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.HermesAirLine || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp)
                    {
                        SaveTicketInfo();
                        booking.SetBookingStatus(BookingStatus.Ticketed, Utility.ToInteger(Settings.LoginInfo.UserID));
                        generateInvoice = true;
                    }
                    else
                    {
                        if (ticket.Length > 0)
                        {
                            SaveTicketInfo();
                            if (ticket.Length == itinerary.Passenger.Length)
                            {
                                booking.SetBookingStatus(BookingStatus.Ticketed, Utility.ToInteger(Settings.LoginInfo.UserID));
                            }
                            else
                            {
                                booking.Lock(BookingStatus.InProgress, Utility.ToInteger(Settings.LoginInfo.UserID));
                            }
                        }
                        else
                        {
                            bh.BookingId = booking.BookingId;
                            bh.Remarks = "(Booking User IP: " + (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"]) + ")" + bh.Remarks;
                            bh.Save();
                        }
                    }
                    queue.ItemId = booking.BookingId;
                    
                    {
                        if (flightId == 0)
                            queue.Save();
                        try
                        {
                            if (booking.Status == BookingStatus.Ticketed)
                            {
                                Invoice invoice = new Invoice();
                                int invoiceNumber = Invoice.isInvoiceGenerated(Ticket.GetTicketList(itinerary.FlightId)[0].TicketId, ProductType.Flight);

                                if (invoiceNumber > 0)
                                {
                                    invoice.Load(invoiceNumber);
                                }
                                else
                                {
                                    invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.FlightId, "", (int)Settings.LoginInfo.UserID, ProductType.Flight, 1);
                                    if (invoiceNumber > 0)
                                    {
                                        invoice.Load(invoiceNumber);
                                        invoice.Status = InvoiceStatus.Paid;
                                        invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                                        invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                                        invoice.UpdateInvoice();
                                    }
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, Request.ServerVariables["REMOTE_ADDR"]);
                            try
                            {
                                string message = "Invoicing is Failed for the following PNR:(" + itinerary.PNR + ").Reason: " + ex.ToString() + "\nAgencyId is " + itinerary.AgencyId.ToString() + "\n Logged in Member is " + Settings.LoginInfo.FirstName + "(" + (int)Settings.LoginInfo.UserID + ", " + Settings.LoginInfo.LoginName + ")\n";
                                Email.Send(CT.Configuration.ConfigurationSystem.Email["fromEmail"], CT.Configuration.ConfigurationSystem.Email["ErrorNotificationMailingId"], "Invoice Failed in Import PNR Page .", message);
                            }
                            catch { }
                        }
                    }
                }
                catch (ArgumentException excep)
                {
                    BookingSaved = true;
                    ShowErrorMessage = excep.Message;
                    SaveError = true;
                    booking.BookingId = 0;
                    throw excep;
                }
                catch (BookingEngineException excep)
                {
                    if (excep.Message.IndexOf("Ticket for this passenger is already created") >= 0)
                    {
                        updateFailedBookingStatus = true;
                    }
                    BookingSaved = true;
                    ShowErrorMessage = excep.Message;
                    SaveError = true;
                    booking.BookingId = 0;
                    throw excep;
                }

                
                if (!SaveError)
                {                   

                    if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
                        FailedBooking.UpdateStatus(itinerary.PNR, Status.Saved, fromPendingQueue ? "Ticket is Saved" : "Booking is Saved");
                    updateTransaction.Complete();
                }



            }



           
            ///This is the case when the ticket is saved in DB created and 
            ///still we are trying to save from failed booking queue
            ///In this case update the status in failed booking queue
            if (updateFailedBookingStatus)
            {
                if (FailedBooking.CheckPNR(itinerary.PNR))
                {
                    FailedBooking.UpdateStatus(itinerary.PNR, CT.BookingEngine.Status.Saved, "Booking is Saved");
                    int flightid = FlightItinerary.GetFlightId(itinerary.PNR);
                    if (flightid > 0)
                    {
                        int bookingid = BookingDetail.GetBookingIdByFlightId(flightid);
                        Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + bookingid);
                    }
                }
            }
            if (!SaveError && generateInvoice)
            {
                if (saveQueueForPending || fromPendingQueue)
                {
                    using (TransactionScope accountTransaction = new TransactionScope())
                    {
                        //int invoiceNumber =  AccountUtility.RaiseInvoice(itinerary, string.Empty, Utility.ToInteger(Settings.LoginInfo.UserID)); //TODO Ziyad
                        if (itinerary.PaymentMode == ModeOfPayment.CreditCard && !(itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.HermesAirLine || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp))
                        {
                            // MetaSearchEngine.MakeCCPaymentEntry(booking, itinerary, loggedMember, invoiceNumber); //ziyad
                        }
                        accountTransaction.Complete();
                    }
                    Dictionary<int, decimal> agencyLccBal = (Dictionary<int, decimal>)CT.MemCache.Cache.Get("AgencyLccBal");
                    if (agencyLccBal != null)
                    {
                        agencyLccBal.Remove(Convert.ToInt32(agency.ID));
                        CT.MemCache.Cache.Replace("AgencyLccBal", (object)agencyLccBal);
                    }
                }
            }
            bookingResponse.BookingId = booking.BookingId;
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.DisplayPNR, Severity.High, (int)Settings.LoginInfo.UserID, "(ImportPNR)Failed to import PNR details due to error : " + ex.ToString(), Request["REMOTE_ADDR"]);
            bookingResponse.Error = ex.Message;
            throw ex;
        }
        return bookingResponse;
    }

    private void SaveTicketInfo()
    {
        //Utility.UpdateAirlinePNR(itinerary);
        FlightItinerary.GetMemberPrefServiceFee(Convert.ToInt32(agency.ID), ref sFeeTypeList, ref sFeeValueList);

        CT.Core.Airline air = new CT.Core.Airline();
        air.Load(itinerary.ValidatingAirlineCode);
        if (itinerary.FlightBookingSource == BookingSource.WorldSpan || itinerary.FlightBookingSource == BookingSource.Amadeus || itinerary.FlightBookingSource == BookingSource.UAPI)
        {
            for (int i = 0; i < ticket.Length; i++)
            {
                if (Request["txtcreateddate"] != null)
                {
                    string issueDate = Request["txtcreateddate"] + " " + DateTime.Now.ToString("HH:mm:ss");
                    DateTime issue = DateTime.ParseExact(issueDate, "dd/MM/yyyy HH:mm:ss", null);
                    ticket[i].IssueDate = Util.ISTToUTC(issue);
                }
                if (ticket[i].PaxKey == itinerary.Passenger[i].PaxKey)
                {
                    ticket[i].SupplierRemmitance = new SupplierRemmitance();
                    ticket[i].SupplierRemmitance.CommissionType = air.CommissionType;
                    SaveTicket(i, i);
                }
                else
                {
                    for (int j = 0; j < itinerary.Passenger.Length; j++)
                    {
                        if (ticket[i].PaxKey == itinerary.Passenger[j].PaxKey)
                        {
                            ticket[j].ValidatingAriline = itinerary.ValidatingAirline;
                            ticket[j].TaxBreakup = new List<KeyValuePair<string, decimal>>();
                            ticket[j].TaxBreakup.Add(new KeyValuePair<string, decimal>("TotalTax", itinerary.Passenger[j].Price.Tax));

                            ticket[i].SupplierRemmitance = new SupplierRemmitance();
                            ticket[i].SupplierRemmitance.CommissionType = air.CommissionType;

                            SaveTicket(i, j);
                            break;
                        }
                    }
                }
            }
        }
        else if (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.HermesAirLine || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp)
        {
            ticket = new Ticket[itinerary.Passenger.Length];
            for (int j = 0; j < itinerary.Passenger.Length; j++)
            {
                ticket[j] = new Ticket();
                if (itinerary.FlightBookingSource != BookingSource.HermesAirLine && itinerary.FlightBookingSource != BookingSource.Paramount && itinerary.FlightBookingSource != BookingSource.Mdlr)
                {
                    ticket[j].TicketNumber = itinerary.PNR;
                }
                if (Request["txtcreateddate"] != null)
                {
                    string issueDate = Request["txtcreateddate"] + " " + DateTime.Now.ToString("HH:mm:ss");
                    DateTime issue = DateTime.ParseExact(issueDate, "dd/MM/yyyy HH:mm:ss", null);
                    ticket[j].IssueDate = Util.ISTToUTC(issue);
                }
                else
                {
                    ticket[j].IssueDate = DateTime.UtcNow;
                }
                ticket[j].ValidatingAriline = itinerary.ValidatingAirline;
                ticket[j].TaxBreakup = new List<KeyValuePair<string, decimal>>();
                ticket[j].TaxBreakup.Add(new KeyValuePair<string, decimal>("TotalTax", itinerary.Passenger[j].Price.Tax));

                ticket[j].SupplierRemmitance = new SupplierRemmitance();
                ticket[j].SupplierRemmitance.CommissionType = air.CommissionType;
                SaveTicket(j, j);
            }
        }
    }

    private void SaveTicket(int ticketIndex, int paxIndex)
    {
        UserPreference preference = new UserPreference();

        int i = ticketIndex;
        int j = paxIndex;
        string corporateCode = string.Empty;
        string tourCode = string.Empty;
        string endorsement = string.Empty;
        string remarks = string.Empty;
        if (Request["corporateCode"] != null)
        {
            corporateCode = Request["corporateCode"].ToString();
        }
        if (Request["tourCode"] != null)
        {
            tourCode = Request["tourCode"].ToString();
        }
        if (Request["endorsement"] != null)
        {
            endorsement = Request["endorsement"].ToString();
        }
        if (Request["remarks"] != null)
        {
            remarks = Request["remarks"].ToString();
        }

        ticket[i].Status = "OK";
        ticket[i].CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
        ticket[i].PaxId = itinerary.Passenger[j].PaxId;
        ticket[i].PaxType = itinerary.Passenger[j].Type;
        string fareRule = string.Empty;
        List<string> airlineNameRemarksList = new List<string>();
        List<string> airlineNameFareRuleList = new List<string>();
        foreach (FlightInfo segment in itinerary.Segments)
        {
            string airlineCode = segment.Airline;
            Airline airline = new Airline();
            airline.Load(airlineCode);
            if (!airlineNameRemarksList.Contains(airline.AirlineName) && airline.Remarks.Length != 0)
            {
                airlineNameRemarksList.Add(airline.AirlineName);
            }
            if (!airlineNameFareRuleList.Contains(airline.AirlineName) && airline.FareRule.Length != 0)
            {
                airlineNameFareRuleList.Add(airline.AirlineName);
                fareRule += "<span style=\"font-weight:bold\">" + airline.AirlineName + "</span>" + "&nbsp;:&nbsp;" + airline.FareRule + "<br />";
            }
            if (segment.Airline == itinerary.ValidatingAirlineCode)
            {
                ticket[i].SupplierId = airline.ConsolidatorId;
            }
        }
        ticket[i].Remarks = remarks;
        ticket[i].FareRule = fareRule;


        if (itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.HermesAirLine)
        {
            ticket[i].TicketNumber = ticketNumbers[i];
        }
        ticket[i].Title = itinerary.Passenger[j].Title;
        ticket[i].PaxFirstName = itinerary.Passenger[j].FirstName;
        ticket[i].PaxLastName = itinerary.Passenger[j].LastName;

        ticket[i].FlightId = itinerary.FlightId;
        int paxIndexPricing = i;

        ticket[i].Price = AccountingEngine.GetPrice(itinerary, booking.AgencyId, paxIndexPricing, 0);

        ticket[i].Price.PriceId = itinerary.Passenger[j].Price.PriceId;
        ticket[i].ETicket = true;
        if (ticket[i].IssueInExchange == null)
        {
            ticket[i].IssueInExchange = string.Empty;
        }
        ticket[i].TourCode = tourCode;
        if (ticket[i].OriginalIssue == null)
        {
            ticket[i].OriginalIssue = string.Empty;
        }
        ticket[i].Endorsement = endorsement;
        ticket[i].CorporateCode = corporateCode;
        if (ticket[i].FareCalculation == null)
        {
            ticket[i].FareCalculation = string.Empty;
        }
        //int agencyPrimaryMemberId = Member.GetPrimaryMemberId(booking.AgencyId); //TODO Ziyad
        string sfType = string.Empty;
        decimal sfValue = 0;
        //if (!itinerary.CheckDomestic("" + Technology.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""))
        //if (!itinerary.IsDomestic)
        if (true)
        {
            if (sFeeTypeList.ContainsKey("INTL") && sFeeValueList.ContainsKey("INTL"))
            {
                sfType = sFeeTypeList["INTL"];
                sfValue = Convert.ToDecimal(sFeeValueList["INTL"]);
            }
        }
        else if (sFeeTypeList.ContainsKey(itinerary.ValidatingAirlineCode) && sFeeValueList.ContainsKey(itinerary.ValidatingAirlineCode))
        {
            sfType = sFeeTypeList[itinerary.ValidatingAirlineCode];
            sfValue = Convert.ToDecimal(sFeeValueList[itinerary.ValidatingAirlineCode]);
        }

        if (sfType == "FIXED")
        {
            ticket[i].ServiceFee = sfValue;
        }
        else if (sfType == "PERCENTAGE")
        {
            decimal totalfare = ticket[i].Price.PublishedFare + ticket[i].Price.Tax + ticket[i].Price.OtherCharges + ticket[i].Price.AdditionalTxnFee + ticket[i].Price.TransactionFee;
            ticket[i].ServiceFee = totalfare * sfValue / 100;
        }
        else
        {
            ticket[i].ServiceFee = 0;
        }
        //preference = preference.GetPreference(agencyPrimaryMemberId, UserPreference.ServiceFee, UserPreference.ServiceFee); // this block TODO Ziyad
        //if (preference.Value != null)
        //{
        //    if (preference.Value == UserPreference.ServiceFeeInTax)
        //    {
        //        ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowInTax;
        //    }
        //    else if (preference.Value == UserPreference.ServiceFeeShow)
        //    {
        //        ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowSeparately;
        //    }
        //}
        //else
        //{
        //    ticket[i].ShowServiceFee = ServiceFeeDisplay.ShowInTax;
        //}

        ticket[i].SupplierRemmitance.PublishedFare = itinerary.Passenger[j].Price.PublishedFare;
        ticket[i].SupplierRemmitance.AccpriceType = itinerary.Passenger[j].Price.AccPriceType;
        ticket[i].SupplierRemmitance.NetFare = itinerary.Passenger[j].Price.NetFare;
        ticket[i].SupplierRemmitance.CessTax = 0;
        ticket[i].SupplierRemmitance.Currency = itinerary.Passenger[j].Price.Currency;
        ticket[i].SupplierRemmitance.CurrencyCode = itinerary.Passenger[j].Price.CurrencyCode;
        ticket[i].SupplierRemmitance.Markup = itinerary.Passenger[j].Price.Markup;
        ticket[i].SupplierRemmitance.OtherCharges = itinerary.Passenger[j].Price.OtherCharges;
        ticket[i].SupplierRemmitance.OurCommission = itinerary.Passenger[j].Price.OurCommission;
        ticket[i].SupplierRemmitance.OurPlb = itinerary.Passenger[j].Price.OurPLB;
        ticket[i].SupplierRemmitance.RateOfExchange = itinerary.Passenger[j].Price.RateOfExchange;
        ticket[i].SupplierRemmitance.ServiceTax = itinerary.Passenger[j].Price.SeviceTax;
        ticket[i].SupplierRemmitance.Tax = itinerary.Passenger[j].Price.Tax;
        ticket[i].SupplierRemmitance.TdsCommission = 0;
        ticket[i].SupplierRemmitance.TdsPLB = 0;
        ticket[i].SupplierRemmitance.CreatedBy = ticket[j].CreatedBy;
        ticket[i].SupplierRemmitance.CreatedOn = DateTime.UtcNow;
        //ticket[i].SupplierRemmitance.CommissionType = air.CommissionType;
        ticket[i].ValidatingAriline = itinerary.ValidatingAirline;
        if (ticket[i].SupplierId > 0)
        {
            ticket[i].SupplierRemmitance.SupplierCode = Supplier.GetSupplierCodeById(ticket[i].SupplierId);
        }
        ticket[i].Save();
        ticket[i].SupplierRemmitance.TicketId = ticket[i].TicketId;
        ticket[i].SupplierRemmitance.Save();

        DateTime issueDate = new DateTime();
        if (Request["txtcreateddate"] != null)
        {
            issueDate = DateTime.ParseExact(Request["txtcreateddate"], "dd/MM/yyyy", null);
        }
        BookingHistory bh = new BookingHistory();
        bh.BookingId = booking.BookingId;
        bh.EventCategory = EventCategory.Ticketing;

        if (itinerary.PNR != null && itinerary.PNR.Length > 0 && FailedBooking.CheckPNR(itinerary.PNR))
        {
            if (issueDate.Date == DateTime.Now.Date)
            {
                bh.Remarks = "Ticket Created";
            }
            else
            {
                bh.Remarks = "Ticket created and the invoice date is changed";
            }
        }
        else
        {
            if (issueDate.Date == DateTime.Now.Date)
            {
                bh.Remarks = "Booking and ticket created via import pnr";
            }
            else
            {
                bh.Remarks = "Booking and ticket created via import pnr and the invoice date is changed";
            }
        }
        bh.Remarks = "(Booking User IP: " + (Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? Request.ServerVariables["REMOTE_ADDR"]) + ")" + bh.Remarks;
        bh.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
        bh.Save();
        if (itinerary.FlightBookingSource == BookingSource.UAPI || itinerary.FlightBookingSource == BookingSource.WorldSpan || itinerary.FlightBookingSource == BookingSource.Amadeus || itinerary.FlightBookingSource == BookingSource.Galileo)
        {
            for (int f = 0; f < itinerary.Segments.Length; f++)
            {
                for (int p = 0; p < ticket[i].PtcDetail.Count; p++)
                {
                    if (ticket[i].PtcDetail[p].FlightKey == itinerary.Segments[f].FlightKey)
                    {
                        ticket[i].PtcDetail[p].SegmentId = itinerary.Segments[f].SegmentId;
                        ticket[i].PtcDetail[p].PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
                        ticket[i].PtcDetail[p].Save();
                        break;
                    }
                }
            }
        }
        else if (itinerary.FlightBookingSource == BookingSource.SpiceJet || itinerary.FlightBookingSource == BookingSource.Indigo || itinerary.FlightBookingSource == BookingSource.Paramount || itinerary.FlightBookingSource == BookingSource.AirDeccan || itinerary.FlightBookingSource == BookingSource.Mdlr || itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp || itinerary.FlightBookingSource == BookingSource.Sama || itinerary.FlightBookingSource == BookingSource.HermesAirLine || itinerary.FlightBookingSource == BookingSource.SpiceJetCorp || itinerary.FlightBookingSource == BookingSource.IndigoCorp)
        {
            ticket[i].PtcDetail = new List<SegmentPTCDetail>();
            for (int f = 0; f < itinerary.Segments.Length; f++)
            {
                string baggage = ConfigurationSystem.SpiceJetConfig["Baggage"];
                ticket[i].PtcDetail.Add(new SegmentPTCDetail());
                ticket[i].PtcDetail[f].SegmentId = itinerary.Segments[f].SegmentId;
                if (itinerary.FlightBookingSource == BookingSource.Paramount)
                {
                    baggage = ConfigurationSystem.ParamountConfig["baggage"];
                }
                if (itinerary.FlightBookingSource == BookingSource.AirDeccan)
                {
                    baggage = ConfigurationSystem.AirDeccanConfig["Baggage"];
                }
                if (itinerary.FlightBookingSource == BookingSource.Mdlr)
                {
                    baggage = ConfigurationSystem.MdlrConfig["baggage"];
                }
                if (itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp)
                {
                    baggage = ConfigurationSystem.GoAirConfig["Baggage"];
                }
                if (itinerary.FlightBookingSource == BookingSource.Sama)
                {
                    baggage = ConfigurationSystem.SamaConfig["baggage"];
                }
                if (itinerary.FlightBookingSource == BookingSource.HermesAirLine)
                {
                    baggage = ConfigurationSystem.HermesConfig["Baggage"];
                }
                // Picking LCC baggage information from config
                ticket[i].PtcDetail[f].Baggage = baggage;
                ticket[i].PtcDetail[f].PaxType = FlightPassenger.GetPTC(ticket[i].PaxType);
                ticket[i].PtcDetail[f].FareBasis = itinerary.FareRules[0].FareBasisCode;
                ticket[i].PtcDetail[f].Save();
            }
        }
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlPaymentType.SelectedValue == "Card")
            {
                Response.Redirect("PaymentProcessing.aspx?paymentGateway=CCAvenue&redirect_url=DisplayPNRInformation.aspx", true);
            }
            UseCredit_Click();
            Session["itinerary"] = null;
        }
        catch (Exception ex)
        {
            errorMessage = ex.Message.ToString();
            Audit.Add(EventType.PNRRefresh, Severity.High, 1, "Failed to Save Booking for Import PNR. " + ex.ToString(), Request["REMOTE_ADDR"]);
        }

    }
    protected void btnHold_Click(object sender, EventArgs e)
    {
        try
        {
            PayLater_Click();
            Session["itinerary"] = null;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void dlSaleSummaryDetails_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            FlightItinerary obj = e.Item.DataItem as FlightItinerary;

            if (obj != null)
            {                
                Label lblBaseFare = e.Item.FindControl("lblBaseFare") as Label;
                Label lblTax = e.Item.FindControl("lblTax") as Label;
                Label lblServiceFee = e.Item.FindControl("lblServiceFee") as Label;
                Label lblTotal = e.Item.FindControl("lblTotal") as Label;
                Label lblBaggage = e.Item.FindControl("lblBaggage") as Label;
                Label lblMeal = e.Item.FindControl("lblMeal") as Label;
                Label lblSeat = e.Item.FindControl("lblSeat") as Label;
                Label lblVAT = e.Item.FindControl("lblVAT") as Label;
                
                //FlightPassenger pax;
                decimal Tax = 0;
                decimal BaseFare = 0;
                decimal AsvAmount = 0;
                decimal baggage = 0, outputVAT = 0m, meal = 0, seatprice = 0;
                bool isBasefare = false;
                foreach (FlightPassenger pax in itinerary.Passenger)
                {

                    if (obj.FareType.ToUpper() == "PUB")
                    {
                        BaseFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;                        
                    }
                    else
                    {
                        BaseFare += pax.Price.NetFare;
                        
                    }
                    Tax += pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup;
                    discount += pax.Price.Discount;
                    if (itinerary.FlightBookingSource == BookingSource.TBOAir)
                    {
                        Tax += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                    }
                    if (itinerary.FlightBookingSource == BookingSource.OffLine)
                    {
                        Tax -= (pax.Price.AdditionalTxnFee + pax.Price.TransactionFee);
                    }
                    AsvAmount += pax.Price.AsvAmount;
                    if (pax.Price.AsvElement == "BF")
                    {
                        isBasefare = true;
                    }
                    else
                    {
                        isBasefare = false;
                    }
                    baggage += pax.Price.BaggageCharge;
                    meal += pax.Price.MealCharge;
                    seatprice += pax.Price.SeatPrice;
                    outputVAT += (pax.Price.OutputVATAmount);

                    lblServiceFee.Text = pax.Price.SeviceTax.ToString("N" + agency.DecimalValue);
                }
                if (isBasefare)
                {
                    BaseFare += AsvAmount;
                }
                else
                {
                    Tax += AsvAmount;
                }
               
                Tax = Math.Round(Tax, agency.DecimalValue);
                lblBaseFare.Text = BaseFare.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                lblTax.Text = (Tax).ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                lblBaggage.Text = baggage.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                lblMeal.Text = meal.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                lblSeat.Text = seatprice.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                discount = Math.Round(discount, agency.DecimalValue);
                lblVAT.Text = outputVAT.ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                BaseFare = Convert.ToDecimal(BaseFare.ToString("N" + agency.DecimalValue));
                outputVAT = Math.Round(outputVAT, agency.DecimalValue);
                
                if (itinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam (Total price ceiling for TBO Source)
                {
                    lblTotal.Text = Math.Ceiling(BaseFare + Tax + baggage + outputVAT - discount).ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                   
                }
                else
                {
                   
                    lblTotal.Text = (BaseFare + Tax + baggage + meal + seatprice + outputVAT - discount).ToString("N" + agency.DecimalValue) + " " + agency.AgentCurrency;
                }
            }
        }
    }

    /// <summary>
    /// To Load flex info for corporate agents
    /// </summary>
    /// <param name="URimportResponse"></param>
    protected void GetFlexData(UAPIdll.Universal46.UniversalRecordImportRsp URimportResponse)
    {
        try
        {
            CorporateProfile corpProfile = new CorporateProfile(Convert.ToInt32(itinerary.Passenger[0].CorpProfileId), 
                Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId);
            if (corpProfile.DtProfileFlex != null && corpProfile.DtProfileFlex.Select("ISNULL(FlexProductId, '1') = '1'").Count() > 0)
            {
                corpProfile.DtProfileFlex = corpProfile.DtProfileFlex.Select("ISNULL(FlexProductId, '1') = '1'").CopyToDataTable();
                
                //Loading default Flex Fields for Import PNR
                DataTable dtDefaultFlexFields= CorporateProfile.GetDefaultFlexFields((int)agency.ID, 1);
                var liDFlex = new List<FlightFlexDetails>();
                if (dtDefaultFlexFields != null && dtDefaultFlexFields.Rows.Count > 0)
                {
                    corpProfile.DtProfileFlex.Merge(dtDefaultFlexFields);

                    /* Enable flight policy to get flight policy related default flex */
                    if (!string.IsNullOrEmpty(itinerary.Passenger[0].CorpProfileId) && itinerary.Passenger[0].CorpProfileId != "0")
                    {
                        itinerary.FlightPolicy = itinerary.FlightPolicy == null ? new FlightPolicy() : itinerary.FlightPolicy;
                        itinerary.FlightPolicy.TravelReasonId = Request["trid"] != null && Request["trid"] != string.Empty ? Convert.ToInt32(Request["trid"]) : 0;
                    }

                    /* Get default flex data based on itinerary object */
                    MetaSearchEngine clsMSE = new MetaSearchEngine();
                    clsMSE.SettingsLoginInfo = Settings.LoginInfo;
                    liDFlex = clsMSE.GetCommonDIInfo(itinerary);
                    liDFlex = liDFlex == null ? new List<FlightFlexDetails>() : liDFlex;

                    itinerary.FlightPolicy = null;
                }

                if (URimportResponse != null && URimportResponse.UniversalRecord != null && URimportResponse.UniversalRecord.AccountingRemark != null)
                {
                    URimportResponse.UniversalRecord.AccountingRemark = URimportResponse.UniversalRecord.AccountingRemark.
                        Where(x => x.Category.ToUpper() == "FT").ToArray();

                    for (int i = 0; i < corpProfile.DtProfileFlex.Rows.Count; i++)
                    {
                        string sGDSPrfx = Convert.ToString(corpProfile.DtProfileFlex.Rows[i]["FlexGDSprefix"]).ToUpper();

                        string data = URimportResponse.UniversalRecord.AccountingRemark.
                            Where(x => !string.IsNullOrEmpty(x.RemarkData) && x.RemarkData.ToUpper().StartsWith(sGDSPrfx)).
                            Select(y => y.RemarkData).FirstOrDefault();
                        
                        data = string.IsNullOrEmpty(data) ? liDFlex.Where(x => x.FlexGDSprefix.ToUpper() == sGDSPrfx).Select(y => y.FlexData).FirstOrDefault() : data;
                        corpProfile.DtProfileFlex.Rows[i]["Detail_FlexData"] = string.IsNullOrEmpty(data) ? corpProfile.DtProfileFlex.Rows[i]["Detail_FlexData"] : data.Replace("//", "@").Replace("--", "_").Replace(sGDSPrfx, string.Empty);
                    }
                }
                hdnFlexInfo.Value = JsonConvert.SerializeObject(corpProfile.DtProfileFlex);
                hdnPaxFlexInfo.Value = JsonConvert.SerializeObject(Enumerable.Repeat(new FlightFlexDetails(), corpProfile.DtProfileFlex.Rows.Count).ToList());
                hdnFlexConfig.Value = Convert.ToString(ConfigurationManager.AppSettings["TRAVEL_REASON_FLEX_PAIR"]);
                hdnTravelReason.Value = Request["trid"] != null && Request["trid"] != string.Empty ? Request["trid"] : string.Empty; 
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.DisplayPNR, Severity.Normal, (int)Settings.LoginInfo.UserID, "(DisplayPNRInformation.aspx)Failed to get flex info for PNR = " + itinerary.PNR, Request.ServerVariables["REMOTE_ADDR"]);
        }
    }
}
