﻿using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Services;

namespace CozmoB2BWebApp
{
    public partial class ExpUploadReceipt : ParentPage
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
                Session["ExpenseReceiptFiles"] = null;
        }

        /// <summary>
        /// To save uploaded receipts in web server 
        /// </summary>
        /// <param name="response"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string saveReceiptFiles(string response, string spath)
        {
            string status = "No files found to upload";
            try
            {
                if (HttpContext.Current.Session["ExpenseReceiptFiles"] == null)
                    return status;

                List<HttpPostedFile> files = HttpContext.Current.Session["ExpenseReceiptFiles"] as List<HttpPostedFile>;

                if (files == null || files.Count == 0)
                    return status;

                var tempFileNames = Newtonsoft.Json.JsonConvert.DeserializeObject<Hashtable>(response);

                if (tempFileNames != null && tempFileNames.Count < 0)
                    return status;

                foreach (string key in tempFileNames.Keys)
                {
                    foreach (HttpPostedFile file in files)
                    {
                        if (tempFileNames[key].ToString() == file.FileName)
                        {
                            if (System.IO.File.Exists(spath + key + "_" + tempFileNames[key].ToString()))
                            {
                                System.IO.File.Delete(spath + key + "_" + tempFileNames[key].ToString());
                            }
                            file.SaveAs(spath + key + "_" + tempFileNames[key].ToString());
                            files.Remove(file);
                            break;
                        }
                    }
                }

                HttpContext.Current.Session["ExpenseReceiptFiles"] = null;

                status = "Success";

            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 0, "Expense Receipt upload failed. Error:" + ex.ToString(), "");
                status = "Failed to upload files, please contact admin";
            }
            return status;
        }
    }
}
