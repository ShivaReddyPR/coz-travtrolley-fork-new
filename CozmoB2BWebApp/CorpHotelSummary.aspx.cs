﻿using CT.BookingEngine;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Data;
using System.Linq;

namespace CozmoB2BWebApp
{
    public partial class CorpHotelSummary : ParentPage
    {
        public string sTripstatus = string.Empty;
        public string sTravelReason = string.Empty;
        public string sViolationReason = string.Empty;
        public HotelItinerary clsHI = new HotelItinerary();
        public AgentMaster clsAM;
        public CorporateProfileTripDetails clsCPTDtls;
        public double doDeadLine;
        public string approvalText = "";
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Settings.LoginInfo == null)
                {
                    Response.Redirect("AbandonSession.aspx");
                    return;
                }

                var pageParams = GenericStatic.GetSetPageParams("CorpHotelSummary", "", "get").Split('|');
                
                //if (Request.QueryString["ConfNo"] == null && Request.QueryString["HotelId"] == null)
                    if (pageParams[0] == "" && pageParams[1] == "")
                    throw new Exception("Hotel booking id required");

                //int iHotelId = !string.IsNullOrEmpty(Request.QueryString["ConfNo"]) ? HotelItinerary.GetHotelId(Request.QueryString["ConfNo"]) : Convert.ToInt32(Request.QueryString["HotelId"]);
                int iHotelId = !string.IsNullOrEmpty(pageParams[0]) ? HotelItinerary.GetHotelId(pageParams[0]) : Convert.ToInt32(pageParams[1]);

                /* Get corporate trip status */
                sTripstatus = FlightPolicy.GetTripApprovalStatus(iHotelId,2);
                approvalText = sTripstatus == "P" ? "Awaiting for approval" : (sTripstatus == "A" ? "Approved" : "Rejected");

                /* Get hotel itinerary details */
                clsHI.Load(iHotelId);
                HotelRoom clsRoom = new HotelRoom();
                clsHI.Roomtype = clsRoom.Load(iHotelId);

                /* Get corporate profile trip details */
                clsCPTDtls = new CorporateProfileTripDetails(iHotelId);

                /* Get corporate policy details and travel reason */
                clsHI.HotelPolicy = new FlightPolicy();
                clsHI.HotelPolicy.Flightid = iHotelId; clsHI.HotelPolicy.ProductId = 2;
                clsHI.HotelPolicy.GetPolicyByFlightId();

                /* Get Travel reason and policy reson description */
                sTravelReason = clsHI.HotelPolicy.TravelReasonId > 0 ? new CorporateTravelReason(clsHI.HotelPolicy.TravelReasonId).Description : string.Empty;
                sViolationReason = clsHI.HotelPolicy.PolicyReasonId > 0 ? new CorporateTravelReason(clsHI.HotelPolicy.PolicyReasonId).Description : string.Empty;

                /* Get agent id from session and load agent related configs for hotel product */
                int iAgentId = Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentID : Settings.LoginInfo.AgentId;
                clsAM = new AgentMaster(iAgentId);

                AgentAppConfig clsAppConfig = new AgentAppConfig();
                clsAppConfig.AgentID = iAgentId;
                clsAppConfig.ProductID = 2;
                var liAppConfig = clsAppConfig.GetConfigData();

                /* Check for approval dead line info from agent product config data */
                string sDeadLine = liAppConfig != null && liAppConfig.Where(x => x.AppKey.ToUpper() == "CORPAPPROVALDEADLINE").Count() == 0 ? string.Empty :
                    liAppConfig.Where(x => x.AppKey.ToUpper() == "CORPAPPROVALDEADLINE").Select(y => y.AppValue).FirstOrDefault();
                doDeadLine = string.IsNullOrEmpty(sDeadLine) ? 5 : Convert.ToDouble(sDeadLine);

                /* Send email based on config data and session info availability */

                var pageParamsSE = GenericStatic.GetSetPageParams("CorpHotelSummary", "", "get").Split('|');

                //if (Request.QueryString["SE"] != null && !string.IsNullOrEmpty(Request.QueryString["SE"]))
                if (pageParamsSE[0] != "" && !string.IsNullOrEmpty(pageParamsSE[0]))
                {
                    try
                    {
                        DataTable dt = HotelItinerary.GetCorpTripEmail(clsHI.ConfirmationNo);
                        if (dt != null && dt.Rows.Count > 0)
                        {
                            EmailParams clsParams = new EmailParams();
                            clsParams.FromEmail = CT.Configuration.ConfigurationSystem.Email["fromEmail"];
                            clsParams.ToEmail = Convert.ToString(dt.Rows[0]["EmailTo"]);
                            clsParams.CCEmail = Convert.ToString(dt.Rows[0]["CCEmail"]);
                            clsParams.Subject = Convert.ToString(dt.Rows[0]["EmailSub"]);
                            clsParams.EmailBody = Convert.ToString(dt.Rows[0]["EmailBody"]).Replace("@RoomInfo", Convert.ToString(dt.Rows[0]["RoomInfo"]))
                                .Replace("@FlexInfo", Convert.ToString(dt.Rows[0]["FlexInfo"]));

                            Email.Send(clsParams);
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, 1, "(CorpHotelSummary) Failed to send email : " + ex.ToString(), "0");
                    }
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "(CorpHotelSummary) Page Load Event Error: " + ex.ToString(), "0");
                Response.Redirect("ErrorPage.aspx?Err=Failed to load trip details, please contact cozmo support team", false);
            }
        }
    }
}
