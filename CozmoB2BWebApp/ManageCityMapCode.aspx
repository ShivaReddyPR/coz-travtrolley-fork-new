<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="ManageCityMapCodeGUI" Title="Manage City Code" Codebehind="ManageCityMapCode.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<div class=" margin-top-10 border1gray ">
<script type="text/javascript">
    function ValidateParam() {
        if (document.getElementById('<%=txtCityName.ClientID %>').value == "") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Please Enter City Name";
            return false;
        }
        if (document.getElementById('<%=txtCityName.ClientID %>').value.length < 3) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Please Enter First 3 Characters";
            return false;
        }
        document.getElementById('<%=hdnSearchClickCount.ClientID %>').value = '1';
    }

    var message;
    function ReadValues() {
        
        document.getElementById('<%=hdnValue.ClientID %>').value = "0";
        var table = document.getElementById('<%=tblSave.ClientID %>');
        var rowLength = table.rows.length;
        var preRow = parseInt(0);
        
        for (var i = 0; i < rowLength - 1; i++) {
            var Cells = table.rows.item(i).cells;
            
            for (j = 0; j < Cells.length; j++) {
                var text = document.getElementById('ctl00_cphTransaction_txt_' + i + '_' + j);
                if (document.getElementById('<%=hdnValue.ClientID %>').value == "0") {
                    document.getElementById('<%=hdnValue.ClientID %>').value = text.value;    
                    document.getElementById('ctl00_cphTransaction_lbl_' + i + '_' + j).innerHTML="";                
                    if(j>2 && text.value.length > 0){
                    if(!SearchCityCode(Cells[j].innerHTML, text.value))
                    {
                      text.select();
                      text.focus();
                      document.getElementById('ctl00_cphTransaction_lbl_' + i + '_' + j).innerHTML=message;
                      return ;
                    }
                    }
                }
                else {
                    if (j != (Cells.length-1)) {
                        if (i == preRow) {
                            document.getElementById('<%=hdnValue.ClientID %>').value = document.getElementById('<%=hdnValue.ClientID %>').value + ',' + text.value;
                            
                        }
                        else {
                            document.getElementById('<%=hdnValue.ClientID %>').value = document.getElementById('<%=hdnValue.ClientID %>').value + '|' + text.value;
                            preRow = i;
                            
                        }
                        document.getElementById('ctl00_cphTransaction_lbl_' + i + '_' + j).innerHTML="";     
                         if(j>2 && text.value.length > 0){
                    if(!SearchCityCode(Cells[j].innerHTML, text.value))
                    {
                     text.select();
                     text.focus();
                      document.getElementById('ctl00_cphTransaction_lbl_' + i + '_' + j).innerHTML=message;
                      return;
                    }
                    }
                    }
                }
            }
        }        
    }

    function Add() {
    
        document.getElementById('<%=hdnValue.ClientID %>').value = "0";
        var table = document.getElementById('<%=tblSave.ClientID %>');
        var rowLength = table.rows.length;
        var preRow = parseInt(0);
        for (var i = 0; i < rowLength - 1; i++) {
            var Cells = table.rows.item(i).cells;
            for (j = 0; j < Cells.length; j++) {
                var text = document.getElementById('ctl00_cphTransaction_txt_' + i + '_' + j);
                if (document.getElementById('<%=hdnValue.ClientID %>').value == "0") {
                    document.getElementById('<%=hdnValue.ClientID %>').value = text.value;
                }
                else {
                    if (j != (Cells.length-1)) {
                        if (i == preRow) {
                            document.getElementById('<%=hdnValue.ClientID %>').value = document.getElementById('<%=hdnValue.ClientID %>').value + ',' + text.value;
                        }
                        else {
                            document.getElementById('<%=hdnValue.ClientID %>').value = document.getElementById('<%=hdnValue.ClientID %>').value + '|' + text.value;
                            preRow = i;
                        }
                    }
                }
            }
        }
    }
    function Validate() {
        var conunt = document.getElementById('<%=hdnAddCount.ClientID %>').value;
        for (var i = 0; i < parseInt(conunt); i++) {
            if (document.getElementById('ctl00_cphTransaction_txt_' + i + '_0') != null) {
                if (document.getElementById('ctl00_cphTransaction_txt_' + i + '_0').value == "") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter Destination ";
                    return false;
                }
                if (document.getElementById('ctl00_cphTransaction_txt_' + i + '_1').value == "") {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter  CountryCode ";
                    return false;
                }
                if (document.getElementById('ctl00_cphTransaction_txt_' + i + '_1').value.length > 5) {
                    document.getElementById('errMess').style.display = "block";
                    document.getElementById('errorMessage').innerHTML = "Please Enter CountryCode 5 Characters only";
                    return false;
                }
            }

        }
        var table = document.getElementById('<%=tblSave.ClientID %>');
        var rowLength = table.rows.length;
        for (var i = 0; i < rowLength - 1; i++) {
            var Cells = table.rows.item(i).cells;
            for (j = 0; j < Cells.length; j++) {
             if (j != (Cells.length-1)) {
              document.getElementById('ctl00_cphTransaction_lbl_' + i + '_' + j).innerHTML="";
              var text = document.getElementById('ctl00_cphTransaction_txt_' + i + '_' + j);
         if(j>2 && text.value.length > 0){
                    if(!SearchCityCode(Cells[j].innerHTML, text.value))
                    {
                     text.select();
                     text.focus();
                      document.getElementById('ctl00_cphTransaction_lbl_' + i + '_' + j).innerHTML=message;
                      return false;;
                    }
                    }
                    }
                    }
                    }
    }



         var cityCodes;
         <%if(!IsPostBack){ %>
       cityCodes = <%=LoadCityCodes() %>;
        <%}%>

</script>


    <div>

<div class="ns-h3">Update Hotel City Code</div>


<div>   <asp:HiddenField ID="hdnRowIds" runat="server" Value="" />
    <asp:HiddenField ID="hdnColumnIds" runat="server" Value="" />
    <asp:HiddenField ID="hdnSearchClickCount" runat="server" Value="0" />
    <asp:HiddenField ID="hdnAddCount" runat="server" Value="0" />
    <asp:HiddenField ID="hdnValue" runat="server"  Value="0"/></div>
    
    
     <div class="paramcon bg_white pad_10" title="Param" id="divParam">   
     
     
     <div> <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ForeColor="Green"></asp:Label></div>
         <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-2"> <asp:Label ID="lblCityName" runat="server" Text="City Name:"></asp:Label>
    
    <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage">
            </div>
            </div></div>
    <div class="col-md-2 marbot_20"> <asp:TextBox ID="txtCityName" runat="server" CssClass="form-control"></asp:TextBox></div>
  
  
  
    <div class="col-md-8"> 
    

    
      <label class=" pull-left margin-right-5">  <asp:Button runat="server" ID="btnSearch" Text="Search"  OnClientClick="return ValidateParam();" CssClass="btn but_b" OnClick="btnSearch_Click" /> </label>
    
      <label class="pull-left margin-right-5">  <asp:Button runat="server" ID="btnUpdate" Text="Update"  CssClass="btn but_b" OnClick="btnUpdate_Click" />
     </label>
     
    <label class="pull-left margin-right-5">  <asp:Button runat="server" ID="btnAdd" Text="Add" CssClass="btn but_b" OnClick="btnAdd_Click" OnClientClick="Add();" />   </label>
    
      <label class=" pull-left margin-right-5">  <asp:Button runat="server" ID="btnSave" Text="Save" CssClass="btn but_b" OnClick="btnSave_Click" OnClientClick="return Validate();"/> </label>
      
      <label class="pull-left margin-right-5">  <asp:Button runat="server" ID="btnClear" Text="Back" CssClass="btn but_b" OnClick="btnClear_Click" Visible="false"/>
    
    </div>



    <div class="clearfix"></div>
    </div>
    
    
    <div class="clearfix"> </div>
    
     </div>
    
    
   
    
    
    
    </div>
    
    </div>
    
    
    
     <div class="martop_10"> 
     <div style=" max-height:400px; overflow:auto" >
                 <table id="tblManage" runat="server" width="100%"></table>
                 <table id="tblSave" runat="server" width="100%"></table>
                            </div>   
                             <asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError" ForeColor="Red"></asp:Label>
    
    
    </div>
    
    <script type="text/javascript">
       

        function SearchCityCode(source, city) {
            for (var i = 0; i < cityCodes.length; i++) {
                if (cityCodes[i][source].toLowerCase() == city.toLowerCase()) {
                    message = 'CODE EXITS FOR ' + cityCodes[i]['Destination'];
//                    alert(city + ' already exist ' + cityCodes[i]['Destination']);
                    return false;
                }
            }
            return true;
        }
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

