﻿using CT.TicketReceipt.Common;
using CT.Core;
using CT.GlobalVisa;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Web.UI.WebControls;

public partial class corpGvEnqRequestGUI : CT.Core.ParentPage
{
    protected GVEnquiryRequest enqrequest = new GVEnquiryRequest();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;

            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", false);
            }
            if (!IsPostBack)
            {
                InitialiseControls();
                Session["GVEnqSession"] = null;
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    #region Private Methods
    private void InitialiseControls()
    {
        try
        {
            BindCountry();
            BindNationality();
            BindResidense();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindCountry()
    {
        try
        {
            ddlCountry.DataSource = Country.GetCountryList();
            ddlCountry.DataTextField = "Key";
            ddlCountry.DataValueField = "Value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select--", "-1"));
            ddlVisaType.Items.Insert(0, new ListItem("--select--", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindNationality()
    {
        try
        {
            ddlNationality.DataSource = Country.GetNationalityList();
            ddlNationality.DataValueField = "Value";
            ddlNationality.DataTextField = "Key";
            ddlNationality.DataBind();
            ddlNationality.Items.Insert(0, new ListItem("--Select--", "-1"));
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }

    private void BindResidense()
    {
        try
        {
            ddlResidense.DataSource = Country.GetCountryList();            
            ddlResidense.DataTextField = "Key";
            ddlResidense.DataValueField = "Value";
            ddlResidense.DataBind();
            ddlResidense.Items.Insert(0, new ListItem("--Select--", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindVisaType(string countryCode)
    {
        try
        {
            ddlVisaType.DataSource = CT.GlobasVisa.VisaTypeMaster.GetCountryByVisaType(countryCode);
            ddlVisaType.DataTextField = "visa_type_name";
            ddlVisaType.DataValueField = "visa_type_id";
            ddlVisaType.DataBind();
            ddlVisaType.Items.Insert(0, new ListItem("--Select--", "-1"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion
    #region Protected metods
    protected void ddlCountry_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            BindVisaType(ddlCountry.SelectedItem.Value);
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    #endregion
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            if (ddlCountry.SelectedIndex > 0)
            {
                enqrequest.TravelToCountryCode = ddlCountry.SelectedValue;
                enqrequest.TravelToCountryName = ddlCountry.SelectedItem.Text;
            }
            if (ddlVisaType.SelectedIndex > 0)
            {
                enqrequest.VisaTypeId = Utility.ToInteger(ddlVisaType.SelectedValue);
                enqrequest.VisaTypeName = ddlVisaType.SelectedItem.Text;
            }
            if (ddlNationality.SelectedIndex > 0)
            {
                enqrequest.NationalityCode = ddlNationality.SelectedValue;
                enqrequest.NationalityName = ddlNationality.SelectedItem.Text;
            }
            if (ddlResidense.SelectedIndex > 0)
            {
                enqrequest.ResidenseCode = ddlResidense.SelectedValue;
                enqrequest.ResidenseName = ddlResidense.SelectedItem.Text;
            }
            enqrequest.AdultCount =Utility.ToInteger(ddlAdults.SelectedValue);
            if(ddlChilds.SelectedIndex>0)
            {
              enqrequest.ChildCount= Utility.ToInteger(ddlChilds.SelectedValue);
            }
            if (ddlInfants.SelectedIndex > 0)
            {
                enqrequest.InfantCount = Utility.ToInteger(ddlInfants.SelectedValue);
            }
            Session["GVEnqSession"] = enqrequest;
            Response.Redirect("corpGvEnqDetails.aspx", false);       
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
}
