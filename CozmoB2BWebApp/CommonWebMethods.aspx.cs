﻿using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using System;
using System.Data;
using System.Linq;
using System.Web.Services;

namespace CozmoB2BWebApp
{
    public partial class CommonWebMethods : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// To set/get the page parameters using session instead of query string params
        /// </summary>
        /// <param name="sessionKey"></param>
        /// <param name="sessionData"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
        public static string GetSetPageParams(string sessionKey, string sessionData, string action)
        {
            return CT.Core.GenericStatic.GetSetPageParams(sessionKey, sessionData, action);
        }

        [System.Web.Services.WebMethod]
        public static string GetAgentBalance()
        {
            string balance = string.Empty;

            try
            {
                if (Settings.LoginInfo != null)
                {
                    int agentId = 0;
                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        agentId = Settings.LoginInfo.OnBehalfAgentID;
                    else
                        agentId = Settings.LoginInfo.AgentId;

                    AgentMaster agent = new AgentMaster(agentId);
                    AgentBalance objAB = new AgentBalance();
                    //Cr.Balance : INR 95,17,89,541.90 Booking on behalf of INDTEST IND
                    if (Settings.LoginInfo.IsOnBehalfOfAgent)
                    {
                        objAB.Location = agent.City;
                        objAB.Name = "Booking on behalf of " + agent.Name;
                        objAB.Balance = "Cr.Balance : " + agent.AgentCurrency + " " + agent.UpdateBalance(0).ToString("N" + agent.DecimalValue);
                    }
                    else
                        objAB.Balance = agent.UpdateBalance(0).ToString("N" + agent.DecimalValue);
                    balance = Newtonsoft.Json.JsonConvert.SerializeObject(objAB);
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Failed to get agency balance. Reason: " + ex.ToString(), "");
            }

            return balance;
        }

        [WebMethod]
        public static string GetCorporateDropDownValues(int agentId, string valueType)
        {
            System.Text.StringBuilder dropDownValues = new System.Text.StringBuilder();
            if (valueType.ToUpper() != "E")
            {
                DataTable dtTravelReasons = TravelUtility.GetTravelReasonList("S", agentId, ListStatus.Short);
                dtTravelReasons.AsEnumerable().ToList().ForEach(r =>
                {
                    dropDownValues.Append(string.IsNullOrEmpty(dropDownValues.ToString()) ? r.ItemArray[0].ToString() + "#" + r.ItemArray[1].ToString() : "," + r.ItemArray[0].ToString() + "#" + r.ItemArray[1].ToString());
                });

            }
            else
            {
                int profileId = Settings.LoginInfo.CorporateProfileId > 0 ? Settings.LoginInfo.CorporateProfileId : -1;
                DataTable dtProfiles = TravelUtility.GetProfileList(profileId, agentId, ListStatus.Short);
                dtProfiles.AsEnumerable().ToList().ForEach(r =>
                {
                    dropDownValues.Append(string.IsNullOrEmpty(dropDownValues.ToString()) ? r.ItemArray[0].ToString() + "#" + r.ItemArray[1].ToString() : "," + r.ItemArray[0].ToString() + "#" + r.ItemArray[1].ToString());
                });
            }

            return dropDownValues.ToString();
        }


        [WebMethod]
        public static string GetTimeIntervals(int agentId)
        {
            System.Text.StringBuilder timeIntervals = new System.Text.StringBuilder();

            AgentAppConfig appConfig = new AgentAppConfig();
            appConfig.AgentID = agentId;
            var listConfigs = appConfig.GetConfigData();
            var showTimings = listConfigs.Exists(x => x.AppKey.ToLower() == "showtimeintervals" && x.AppValue.ToLower() == "true");

            //If ShowTimeIntervals is set to TRUE in BKE_APP_CONFIG for the login agent then clear existing values and add half hour time intervals in 24 hour format
            if (showTimings)
            {
                for (int h = 0; h < 24; h++)
                {
                    if (h > 0)
                    {
                        int hour = h, prevHour = h - 1;
                        string prevHourFormat = string.Empty, hourFormat = string.Empty;

                        if (prevHour.ToString().Length == 1)
                            prevHourFormat = "0" + prevHour;
                        else
                            prevHourFormat = prevHour.ToString();

                        if (hour.ToString().Length == 1)
                            hourFormat = "0" + hour;
                        else
                            hourFormat = hour.ToString();

                        timeIntervals.Append(string.IsNullOrEmpty(timeIntervals.ToString()) ? prevHourFormat + ":30 - " + hourFormat + ":00|" + prevHourFormat + ":30 - " + hourFormat + ":00" : "," + prevHourFormat + ":30 - " + hourFormat + ":00|" + prevHourFormat + ":30 - " + hourFormat + ":00");
                        timeIntervals.Append("," + hourFormat + ":00 - " + hourFormat + ":30|" + hourFormat + ":00 - " + hourFormat + ":30");
                        
                    }
                    else
                    {
                        timeIntervals.Append(string.IsNullOrEmpty(timeIntervals.ToString()) ? "00:00 - 00:30|00:00 - 00:30" : ",00:00 - 00:30|00:00 - 00:30");
                        
                    }
                }

                timeIntervals.Append(",23:30 - 00:00|23:30 - 00:00");
                
            }
            else
            {
                timeIntervals.Append("Morning|Morning");
                timeIntervals.Append(",AfterNoon|AfterNoon");
                timeIntervals.Append(",Evening|Evening");
                timeIntervals.Append(",Night|Night");
            }

            return timeIntervals.ToString();
        }

        [WebMethod]
        public static bool IsRoutingEnabledForAgent(int agentId)
        {
            bool isEnabled = false;

            isEnabled = AgentMaster.FindSearchType(agentId, true);

            return isEnabled;
        }
    }

    [Serializable]
    public class AgentBalance
    {
        public string Name { get; set; }
        public string Location { get; set; }
        public string Balance { get; set; }
    }
}
