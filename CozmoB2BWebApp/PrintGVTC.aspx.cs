using System;
using CT.TicketReceipt.Common;
using CT.GlobalVisa;

public partial class PrintOBVisaTCUI : System.Web.UI.Page
{
    protected GVVisaSale tranxVS;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            //LoginInfo loginInfo = Settings.LoginInfo;
            //if (loginInfo == null)
            //{
            //    Response.Redirect("SessionExpired.aspx");
            //    // Response.Redirect(string.Format("ErrorPage.aspx?Err={0}", GetGlobalResourceObject("ErrorMessages", "INVALID_USER")));
            //}
            string strQuery = Request.QueryString["vsId"];
            if (!string.IsNullOrEmpty(strQuery))
            {
                long vsId = Utility.ToLong(strQuery);
                ShowData(vsId);
                ViewState["view_fpax_id"] = "0";
            }

        }
    }

    private void ShowData(long vsId)
    {
        try
        {
            tranxVS = new GVVisaSale(vsId);
            lblDocumentsDate.Text = DateFormat(tranxVS.DocDate);
        }
        catch { }
    }

    #region Date Format
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:MM");
        }
    }
    protected string DateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }


    #endregion
       
}
