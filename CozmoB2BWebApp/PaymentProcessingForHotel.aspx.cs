using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Threading;
using System.Text;
using CT.TicketReceipt.BusinessLayer;
using CT.AccountingEngine;
using CT.Core;
using CT.Configuration;
using CT.BookingEngine;
public partial class PaymentProcessingForHotel : System.Web.UI.Page
{
    protected string siteName = string.Empty;
    protected decimal totalAmount = 0;
    protected string ipAddr;
    protected string orderId = string.Empty;
    protected string paymentGatewaySite;
    protected string trackId;
    protected string paymentGateway = string.Empty;
    protected AgentMaster agency;
    protected int decimalPoint = 2;
    private class VPCStringComparer : IComparer
    {
        public int Compare(Object a, Object b)
        {
            if (a == b) return 0;
            if (a == null) return -1;
            if (b == null) return 1;

            // Ensure we have string to compare
            string sa = a as string;
            string sb = b as string;

            // Get the CompareInfo object to use for comparing
            System.Globalization.CompareInfo myComparer = System.Globalization.CompareInfo.GetCompareInfo("en-US");
            if (sa != null && sb != null)
            {
                // Compare using an Ordinal Comparison.
                return myComparer.Compare(sa, sb, System.Globalization.CompareOptions.Ordinal);
            }
            throw new ArgumentException("a and b should be strings.");
        }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            AuthorizationCheck();
            siteName = ExtractSiteName(Request["HTTP_HOST"]);
            //siteName = "oxi.tektravels.com";
            ipAddr = Request.ServerVariables["REMOTE_ADDR"];
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                decimalPoint = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
            }
            else
            {
                decimalPoint = Settings.LoginInfo.DecimalValue;
                agency = new AgentMaster(Settings.LoginInfo.AgentId);
            }
            decimal charges=0;
            decimal total = 0;
            string orderNumber = string.Empty;
            if (Request["paymentGateway"] != null)
            {
                HotelItinerary itinerary = Session["hItinerary"] as HotelItinerary;
                //calucating Total Amount
                decimal outVatAmount = 0;
                foreach (HotelRoom room in itinerary.Roomtype)
                {
                    ///room.Price.pri
                    if (room.Price.AccPriceType == PriceType.NetFare)
                    {
                        total += (room.Price.NetFare + room.Price.Markup );
                        outVatAmount += room.Price.OutputVATAmount;
                    }
                }
                totalAmount = Math.Ceiling(total) + Math.Ceiling(outVatAmount);
                //orderNumber genarting
                //orderNo=HL-PAXFIRSTNAME
                if (itinerary.Roomtype[0].PassenegerInfo[0].Firstname.Length > 10)
                {
                    orderNumber = "HL-" + "" + itinerary.Roomtype[0].PassenegerInfo[0].Firstname.Substring(0, 10);
                }
                else
                {
                    orderNumber = "HL-" + "" + itinerary.Roomtype[0].PassenegerInfo[0].Firstname;
                }
                //Loading Cradcharges

                try
                {
                    DataTable dtChrges = AgentCreditCardCharges.GetList();
                    DataView dv = dtChrges.DefaultView;
                    dv.RowFilter = "Productid IN('" + 2 + "') AND Agentid IN('" + agency.ID + "') AND PGSource IN('" + (int)CT.AccountingEngine.PaymentGatewaySource.CCAvenue + "')";
                    dtChrges = dv.ToTable();
                    if (dtChrges != null && dtChrges.Rows.Count > 0)
                    {
                        charges = Convert.ToDecimal(dtChrges.Rows[0]["PGCharge"]);
                    }
                }
                catch { }
                paymentGateway = Request["paymentGateway"];
            }
            else
            {
                Response.Redirect("HTLBookingConfirm.aspx?ErrorPG=True");
            }
            orderId = new Random().Next(1, 2147483647).ToString();
           
            //SavePendingHotelBooking();
            if (paymentGateway == "CozmoPg")//for cozmo payment Gate Way
            {
                trackId = orderId;
                Session["trackIdHotel"] = trackId;
                CozmoPaymentGateway();
            }
            else
            {
                //Genartaing orderNumber
                if (!string.IsNullOrEmpty(orderNumber))
                {
                    //orderNo=HL-PAXFIRSTNAME-random num
                    orderId = orderNumber + "-" + orderId;
                    if (orderId.Length > 30)
                    {
                        orderId = orderId.Substring(0, 30);
                    }
                }
                orderId.Replace(" ", "-");
                decimal ccCharges = (totalAmount * charges / 100);
                //CreditCard paymentdetails saving 
                CreditCardPaymentInformation payment = new CreditCardPaymentInformation();
                payment.Amount = totalAmount;
                payment.IPAddress = ipAddr;
                payment.Charges = ccCharges;
                payment.TrackId = orderId;
                payment.ReferenceId = 0;
                payment.PaymentId = "0";
                payment.PaymentGateway = CT.AccountingEngine.PaymentGatewaySource.CCAvenue;
                payment.ReferenceType = CT.AccountingEngine.ReferenceIdType.Payment;
                payment.TransType = "B2B";
                payment.Remarks = "In Progress";
                payment.PaymentStatus = 0;
                payment.Save();
                //Updating creditcard purpose useinmg session
                Session["PaymentInformationId"] = payment.PaymentInformationId;

                totalAmount += ccCharges;

                paymentGatewaySite = ConfigurationManager.AppSettings["CCAvenue_Url"];
                if (Convert.ToBoolean(ConfigurationManager.AppSettings["TestMode"]))
                {
                    totalAmount = 1;
                }
            }
        }
        catch (ThreadAbortException)
        {
            //Do nothing when thread aborted
        }
        catch (Exception ex)
        {
            ConfigurationSystem con = new ConfigurationSystem();
            Hashtable hostPort = con.GetHostPort();
            Email.Send(hostPort["fromEmail"].ToString(), hostPort["ErrorNotificationMailingId"].ToString(), siteName + " Error", ex.Message + "\n StackTrace:" + ex.StackTrace + "\n InnerExcepstion: " + ex.InnerException + "\n source: " + ex.Source + "\n data: " + ex.Data);
            Audit.Add(EventType.CCAvenue, Severity.High, 0, "Error message :" + ex.Message + ex.StackTrace, ipAddr);
            Response.Redirect("HTLBookingConfirm.aspx?ErrorPG=True");
        }
    }

    //for cozmo payment gate way implementation.
    private void CozmoPaymentGateway()
    {
        string itineraryDetail1 = string.Empty;

        if (Convert.ToBoolean(ConfigurationManager.AppSettings["TestMode"]))
        {
            totalAmount = 100;
        }
        else
        {
            totalAmount = totalAmount * 100;
        }

        string amount = Math.Round(totalAmount).ToString();

        //itineraryDetail1 = "HotelBooking for Hotel:" + bRequest.HotelName + "|Pax:" + bRequest.Guest[0].FirstName + bRequest.Guest[0].LastName + "|CheckIn:" + request.CheckInDate + "|CheckOut:" + request.CheckOutDate;
            //itineraryDetail1 += "|OutboundFlight:" + resOut.Segment[0].Airline.AirlineCode + "-" + resOut.Segment[0].FlightNumber;
            // itineraryDetail1 += " Arr Time :" + resOut.Segment[0].ArrTime.ToString("ddMMMyyyy hh:mm");
        itineraryDetail1 = "Hotel Booking";
            
        
        string SECURE_SECRET = ConfigurationManager.AppSettings["CozmoPg_SECURE_SECRET"];
        //try
        //{  
        System.Collections.SortedList transactionData = new System.Collections.SortedList(new VPCStringComparer());
        string queryString = ConfigurationManager.AppSettings["CozmoPg_queryString"];
        transactionData.Add("vpc_Version", "1");
        transactionData.Add("vpc_Command", ConfigurationManager.AppSettings["CozmoPg_Command"]);
        transactionData.Add("vpc_AccessCode", ConfigurationManager.AppSettings["CozmoPg_AccessCode"]);
        transactionData.Add("vpc_MerchTxnRef", orderId);//user
        transactionData.Add("vpc_Merchant", ConfigurationManager.AppSettings["CozmoPg_Merchant"]);
        //transactionData.Add("vpc_Merchant", DateTime.Now.ToString("ddMMyyyyhhmmss"));
        transactionData.Add("vpc_OrderInfo", itineraryDetail1);//user
        //amount = "100";        
        transactionData.Add("vpc_Amount", amount);
       // transactionData.Add("vpc_ReturnURL",Request.Url.Scheme+ "://" + siteName + "/ReviewHotelBooking.aspx");

        transactionData.Add("vpc_ReturnURL", Request.Url.Scheme+"://" + siteName + "/" + ConfigurationManager.AppSettings["RootFolder"].ToString() + "/HotelReview.aspx");
        transactionData.Add("vpc_Locale", ConfigurationManager.AppSettings["CozmoPg_Locale"]);
        transactionData.Add("AgainLink", Request.Url.Scheme+"://" + siteName + "/HotelReview.aspx");
        transactionData.Add("Title", "ASP.NET C# VPC 3-Party");
        string rawHashData = SECURE_SECRET;
        string seperator = "?";
        foreach (System.Collections.DictionaryEntry item in transactionData)
        {
            queryString += seperator + System.Web.HttpUtility.UrlEncode(item.Key.ToString()) + "=" + System.Web.HttpUtility.UrlEncode(item.Value.ToString());
            seperator = "&";

            if (SECURE_SECRET.Length > 0)
            {
                rawHashData += item.Value.ToString();
            }
        }
        string signature = "";
        if (SECURE_SECRET.Length > 0)
        {
            // create the signature and add it to the query string
            signature = CreateMD5Signature(rawHashData);
            queryString += seperator + "vpc_SecureHash=" + signature;
        }

        Response.Redirect(queryString);
    }



    private string CreateMD5Signature(string RawData)
    {
        System.Security.Cryptography.MD5 hasher = System.Security.Cryptography.MD5CryptoServiceProvider.Create();
        byte[] HashValue = hasher.ComputeHash(Encoding.ASCII.GetBytes(RawData));

        string strHex = "";
        foreach (byte b in HashValue)
        {
            strHex += b.ToString("x2");
        }
        return strHex.ToUpper();
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
    }

    //private string GetGuestResponseXml(WSGuest[] guest)
    //{
    //    MemoryStream stream = null;
    //    TextWriter writer = null;
    //    try
    //    {
    //        stream = new MemoryStream(); // read xml in memory

    //        writer = new StreamWriter(stream, Encoding.Unicode);
    //        // get serialise object
    //        XmlRootAttribute root = new XmlRootAttribute("Guest");
    //        XmlSerializer serializer = new XmlSerializer(typeof(WSGuest[]), root);

    //        serializer.Serialize(writer, guest); // read object
    //        int count = (int)stream.Length; // saves object in memory stream

    //        byte[] arr = new byte[count];
    //        stream.Seek(0, SeekOrigin.Begin);
    //        // copy stream contents in byte array

    //        stream.Read(arr, 0, count);
    //        UnicodeEncoding utf = new UnicodeEncoding(); // convert byte array to string

    //        return utf.GetString(arr).Trim();
    //    }
    //    catch
    //    {
    //        return string.Empty;
    //    }
    //    finally
    //    {
    //        if (stream != null) stream.Close();
    //        if (writer != null) writer.Close();
    //    }
    //}
    public static string ExtractSiteName(string remoteHost)
    {
        string siteName;
        if (remoteHost.Contains("www."))
        {
            siteName = remoteHost.Remove(0, 4);
        }
        else
        {
            siteName = remoteHost;
        }
        //siteName = "ndtv.tektravels.com";
        //siteName = "cozmotravel.tektravels.com";
        return siteName;
    }
    //private void SavePendingHotelBooking()
    //{
    //    WSAddPendingHotelBookingRequest pbRequest = new WSAddPendingHotelBookingRequest();
    //    WSAddPendingHotelBookingResponse pbResponse = new WSAddPendingHotelBookingResponse();
    //    pbRequest.HotelIndex = result.Index;
    //    pbRequest.SessionId = sessionId;
    //    pbRequest.Address1 = result.HotelInfo.HotelAddress;
    //    pbRequest.Address2 = result.HotelInfo.HotelLocation;
    //    int adultCount = 0, childCount = 0;
    //    for (int i = 0; i < result.RoomGuestDetails.Length; i++)
    //    {
    //        adultCount += result.RoomGuestDetails[i].NoOfAdults;
    //        childCount += result.RoomGuestDetails[i].NoOfChild;
    //    }
    //    pbRequest.AdultCount = adultCount;
    //    pbRequest.ChildCount = childCount;
    //    pbRequest.Country = request.CountryName;
    //    pbRequest.BookingStatus = WLHotelBookingStatus.Pending;
    //    pbRequest.CheckInDate = result.CheckInDate;
    //    pbRequest.CheckOutDate = result.CheckOutDate;
    //    pbRequest.CityCode = result.CityCode;
    //    pbRequest.CityRef = request.CityReference;
    //    if (cust != null && cust.UserName != null)
    //    {
    //        pbRequest.Email = cust.UserName;
    //    }
    //    else
    //    {
    //        pbRequest.Email = string.Empty;
    //    }
    //    pbRequest.HotelCode = result.HotelInfo.HotelCode;
    //    pbRequest.HotelName = result.HotelInfo.HotelName;
    //    pbRequest.HotelQueueId = 0;
    //    pbRequest.IPAddress = ipAddr;
    //    pbRequest.IsDomestic = request.IsDomestic;
    //    pbRequest.NumberOfRooms = result.NoOfRooms;
    //    pbRequest.OrderId = orderId;
    //    pbRequest.PassengerInfo = GetGuestResponseXml(bRequest.Guest);
    //    pbRequest.PaymentAmount = totalAmount;
    //    pbRequest.PaymentId = "";
    //    pbRequest.PaymentStatus = PaymentStatus.InProcess;

    //    pbRequest.PaySource = WSPaymentGatewaySource.CozmoPG;
    //    if (request.IsDomestic)
    //    {
    //        for (int i = 0; i < result.RoomDetails.Length; i++)
    //        {
    //            if (result.RoomDetails[i].RoomTypeCode.Equals(roomTypeCodeList[0]) && result.RoomDetails[i].RatePlanCode.Equals(ratePlanCodeList[0]))
    //            {
    //                for (int j = 0; j < request.NoOfRooms; j++)
    //                {
    //                    if (j == 0)
    //                    {
    //                        pbRequest.RoomName = result.RoomDetails[i].RoomTypeName;
    //                    }
    //                    else
    //                    {
    //                        pbRequest.RoomName += "|" + result.RoomDetails[i].RoomTypeName;
    //                    }
    //                }
    //                break;
    //            }
    //        }
    //    }
    //    else
    //    {
    //        for (int i = 0; i < roomTypeCodeList.Length; i++)
    //        {
    //            for (int j = 0; j < result.RoomDetails.Length; j++)
    //            {
    //                if (result.RoomDetails[j].RoomTypeCode.Equals(roomTypeCodeList[i]) && result.RoomDetails[j].RatePlanCode.Equals(ratePlanCodeList[i]))
    //                {
    //                    if (i == 0)
    //                    {
    //                        pbRequest.RoomName = result.RoomDetails[i].RoomTypeName;
    //                        break;
    //                    }
    //                    else
    //                    {
    //                        pbRequest.RoomName += "|" + result.RoomDetails[i].RoomTypeName;
    //                        break;
    //                    }
    //                }
    //            }
    //        }
    //    }
    //    pbRequest.Phone = bRequest.Guest[0].Phoneno;
    //    pbRequest.Remarks = "penging hotel queue entry, before going to payment gateway.";
    //    pbRequest.StarRating = (TekHotelService.WLHotelRating)Enum.Parse(typeof(TekHotelService.WLHotelRating), result.HotelInfo.Rating.ToString());
    //    pbResponse = hotelApi.AddPendingHotelBookingDetail(pbRequest);
    //    Session["pendingHotelBookingRequest"] = pbRequest;
    //    Session["hotelQueueId"] = pbResponse.HotelQueueId;
    //}

}
