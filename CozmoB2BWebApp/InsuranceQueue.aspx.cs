﻿using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.BookingEngine.Insurance;
using CT.Core;
using ReligareInsurance;

public partial class InsuranceQueueGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    public string InsuranceType;
    PagedDataSource pagedData = new PagedDataSource();
    bool isShowAll = false;
    protected string pagingEnable = "style='display:block'";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    isShowAll = true;
                    InitializeControls();

                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch(Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "Insurance Queue page Load", "0");
        }
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "autoCompInitFlightSearch();", "script");
    }
    private void InitializeControls()
    {
        try
        {
            BindAgency();
            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgency.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgency.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgency.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgency.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            BindB2BAgent(Convert.ToInt32(ddlAgency.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
            Clear();
        }

        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindAgency()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);// AGENT Means binding in Agency DropDown only BASEAGENT AND AGENTS
            ddlAgency.DataSource = dtAgents;
            ddlAgency.DataTextField = "Agent_Name";
            ddlAgency.DataValueField = "agent_id";
            ddlAgency.DataBind();
            ddlAgency.Items.Insert(0, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void ddlAgency_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
            if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
            BindB2BAgent(agentId);
            BindB2B2BAgent(agentId);
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "Agency selected Event", "0");
        }
    }
    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }

        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "ddlB2BAgent selected Event", "0");

        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            GetInsuredList();
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "button search Event", "0");
        }
    }


    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "button clear Event", "0");
        }
    }

    private void GetInsuredList()
    {
        try
        {
            DateTime departureDate, returnDate, purchaseDateFrom, purchaseDateTo;
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (txtDeptDate.Text.Trim().Length > 0)
            {
                departureDate = Convert.ToDateTime(txtDeptDate.Text.Trim(), dateFormat);
            }
            else
            {
                departureDate = Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"));
            }
            if (txtArrDate.Text.Trim().Length > 0)
            {
                returnDate = Convert.ToDateTime(txtArrDate.Text.Trim(), dateFormat);
            }
            else
            {
                returnDate = Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"));
            }
            if (txtPurchaseDateFrom.Text.Trim().Length > 0)
            {
                purchaseDateFrom = Convert.ToDateTime(txtPurchaseDateFrom.Text.Trim(), dateFormat);
            }
            else
            {
                purchaseDateFrom = Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy"));
            }
            if (txtPurchaseDateTo.Text.Trim().Length > 0)
            {
                purchaseDateTo = Convert.ToDateTime(txtPurchaseDateTo.Text.Trim()+" 11:59:59 PM", dateFormat);
            }
            else
            {
                purchaseDateTo = Convert.ToDateTime(DateTime.MinValue.ToString("dd/MM/yyyy 11:59:59 PM"));
            }
            string policyNo = txtPolicyNo.Text.Trim();
            string pnrNo = string.Empty;
            if (txtPnrNo.Text.Trim().Length > 0)
            {
                pnrNo = txtPnrNo.Text.Trim().ToString();
            }
             string origin = GetCityCode(Origin.Text.Trim());
            string destination = GetCityCode(Destination.Text.Trim());
            int agencyId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
            string agentType = "";
            //Add Search Logic here according to TransactionType 19/08/2015 by chandan Sharma
            if (agencyId == 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
                }
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = string.Empty;// null Means binding in list all BOOKINGS
                }
            }
            if (agencyId > 0 && ddlB2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlAgency.SelectedItem.Value) > 1)
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                    }
                    else
                    {
                        agencyId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                    }
                }
                else
                {
                    if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                    }
                    agencyId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }

            }
            if (agencyId > 0 && ddlB2B2BAgent.SelectedIndex > 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
                }
                else
                {
                    agencyId = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
                }
            }
            if (Convert.ToInt32(ddlAgency.SelectedItem.Value) != 0)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                    {
                        agentType = "B2B2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                        agencyId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
                    }
                }
            }
            string transType = string.Empty;
            if (Settings.LoginInfo.TransType == "B2B")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2B";
            }
            else if (Settings.LoginInfo.TransType == "B2C")
            {
                ddlTransType.Visible = false;
                lblTransType.Visible = false;
                transType = "B2C";
            }
            else
            {
                ddlTransType.Visible = true;
                lblTransType.Visible = true;
                if (ddlTransType.SelectedItem.Value == "-1")
                {
                    transType = null;
                }
                else
                {
                    transType = ddlTransType.SelectedItem.Value;
                }
            }
            // Added By HAri Malla 25-01-2019
            DataSet dsqueue = new DataSet();
            if (ddlInsType.SelectedValue == "RELIGARE")
            {
                DataTable InsQueue = ReligareHeader.GetHeaderQueue(departureDate, returnDate, purchaseDateFrom, purchaseDateTo, policyNo, agencyId, pnrNo, origin, destination, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.LocationID, Settings.LoginInfo.UserID, agentType, transType);
                System.Data.DataColumn newColumn = new System.Data.DataColumn("InsType", typeof(System.String));
                newColumn.DefaultValue = "Religare";
                InsQueue.Columns.Add(newColumn);
                dsqueue.Tables.Add(InsQueue.Copy());
            }
            else if (ddlInsType.SelectedValue == "TUNES")
            {
                DataTable InsQueue = InsuranceQueue.GetInsuranceQueue(departureDate, returnDate, purchaseDateFrom, purchaseDateTo, policyNo, agencyId, pnrNo, origin, destination, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.LocationID, Settings.LoginInfo.UserID, agentType, transType);
                System.Data.DataColumn newColumn = new System.Data.DataColumn("InsType", typeof(System.String));
                newColumn.DefaultValue = "Tunes";
                InsQueue.Columns.Add(newColumn);
                dsqueue.Tables.Add(InsQueue);
            }
            //else ALL
            //{
            //    DataTable InsQueue = ReligareHeader.GetHeaderQueue(departureDate, returnDate, purchaseDateFrom, purchaseDateTo, policyNo, agencyId, pnrNo, origin, destination, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.LocationID, Settings.LoginInfo.UserID, agentType, transType);
            //   // DataTable InsQueue = ReligareHeader.GetHeaderQueue(purchaseDateFrom, purchaseDateTo, transType, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.LocationID, agentType, Settings.LoginInfo.UserID, policyNo, agencyId);
            //    System.Data.DataColumn newColumn = new System.Data.DataColumn("InsType", typeof(System.String));
            //    newColumn.DefaultValue = "Religare";
            //    InsQueue.Columns.Add(newColumn);
            //    dsqueue.Tables.Add(InsQueue.Copy());
            //    DataTable InsQueue1 = InsuranceQueue.GetInsuranceQueue(departureDate, returnDate, purchaseDateFrom, purchaseDateTo, policyNo, agencyId, pnrNo, origin, destination, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.LocationID, Settings.LoginInfo.UserID, agentType, transType);
            //    newColumn = new System.Data.DataColumn("InsType", typeof(System.String));
            //    newColumn.DefaultValue = "Tunes";
            //    InsQueue1.Columns.Add(newColumn);
            //    dsqueue.Tables.Add(InsQueue1.Copy());
            //}

            Session["InsuranceQueue"] = dsqueue;
            dlInsQueue.DataSource = dsqueue;
            dlInsQueue.DataBind();
            CurrentPage = 0;
            doPaging();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void Clear()
    {
        txtDeptDate.Text = string.Empty;
        txtArrDate.Text = string.Empty;
        txtPurchaseDateFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        txtPurchaseDateTo.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
        //txtPNRno.Text = string.Empty;
        txtPolicyNo.Text = string.Empty;
        Origin.Text = string.Empty;
        Destination.Text = string.Empty;
        ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
        isShowAll = true;
        Session["InsuranceQueue"] = null;
        GetInsuredList();
    }

    private string GetCityCode(string searchCity)
    {
        string cityCode = "";

        cityCode = searchCity.Split(',')[0];

        if (cityCode.Contains(" "))
        {
            cityCode = cityCode.Split(' ')[0].Split(')')[0].Replace("(", "");
        }
        else
        {
            cityCode = cityCode.Split(')')[0].Replace("(", "");
        }
        return cityCode;
    }

    #region Paging
    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }

    void doPaging()
    {
        // Changed by Hari malla for Religare product.
        DataSet ds = (DataSet)Session["InsuranceQueue"];
        DataTable dt = new DataTable();
        dt = ds.Tables[0].Copy();
        if (ds.Tables.Count > 1)
        {
            dt.Merge(ds.Tables[1], true, MissingSchemaAction.Ignore);
        }
        pagedData.DataSource = dt.DefaultView;
        pagedData.AllowPaging = true;
        pagedData.PageSize = 5;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        dlInsQueue.DataSource = pagedData;
        dlInsQueue.DataBind();
        if (dt.Rows.Count <= 0)
        {
            pagingEnable = "style='display:none'";
        }
    }
    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }
    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }
    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }
    #endregion
    protected void dlInsQueue_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                
                Label lblInsType = e.Item.FindControl("lblInsType") as Label; //Added by Hari MAlla on 25-01-2019
                string bookingeurl  = lblInsType.Text.ToUpper() == "RELIGARE" ? "viewbookingforReligareInsurance.aspx" : "ViewBookingForInsurance.aspx";
                string invoiceurl = lblInsType.Text.ToUpper() == "RELIGARE" ? "ReligareInsuranceInvoice.aspx" : "InsuranceInvoice.aspx";

                Label lblViewBooking = e.Item.FindControl("lblViewBooking") as Label;
                Label lblViewInvoice = e.Item.FindControl("lblViewInvoice") as Label;
                HtmlTable tblChangeRequest = e.Item.FindControl("tblChangeRequest") as HtmlTable;
                Button btnRequest = e.Item.FindControl("btnRequest") as Button;
                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                Label lblTotal = e.Item.FindControl("lblTotal") as Label;
                LinkButton lnkPayment = e.Item.FindControl("lnkPayment") as LinkButton; //added by chandan on 15062016
                Button btnContine = e.Item.FindControl("btnContine") as Button;
                HtmlGenericControl divPlans = e.Item.FindControl("divPlans") as HtmlGenericControl;
                DataRowView view = e.Item.DataItem as DataRowView;
                //checklist
                CheckBoxList chkPlans = e.Item.FindControl("chkPlans") as CheckBoxList;

                decimal totalAmount = 0;
                totalAmount = Convert.ToDecimal(view["TotalAmount"]);
                int agentId = Convert.ToInt32(view["AgencyId"]);
                AgentMaster agent = new AgentMaster(agentId);
                if(lblInsType.Text=="Tunes")
                    lblTotal.Text = view["CurrencyCode"].ToString() + " " + totalAmount.ToString("N" + agent.DecimalValue);
                else
                    lblTotal.Text = agent.AgentCurrency + " " + totalAmount.ToString("N" + agent.DecimalValue);

                int id = Convert.ToInt32(view["Ins_Id"]);
                bool ChangeRequest = true;
                try
                {
                    //Loading All plans and binding Checkbox list control
                    DataTable dtPlans = null;
                    dtPlans = lblInsType.Text.ToUpper() == "RELIGARE"?  ReligareHeader.GetInsurancePlans(id) : InsuranceQueue.GetInsurancePlans(id);                  
                    if (dtPlans != null && dtPlans.Rows.Count > 0)
                    {
                        //binding Plans Showing purpose
                        HtmlGenericControl divPlanName = new HtmlGenericControl();
                        divPlanName.Attributes.Add("class", "col-md-6 col-xs-6");
                        divPlanName.InnerHtml = "<div ><span><b>Plan Title</b></span></Div>";

                        HtmlGenericControl divPlanTitle = new HtmlGenericControl();
                        divPlanTitle.Attributes.Add("class", "col-md-3 col-xs-6");
                        divPlanTitle.InnerHtml = "<div><span><b>Policy No</b></span></Div>";

                        HtmlGenericControl divPlanStatus = new HtmlGenericControl();
                        divPlanStatus.Attributes.Add("class", "col-md-3 col-xs-6");
                        divPlanStatus.InnerHtml = "<div><span><b>Plan Status</b></span></Div>";

                        divPlans.Controls.Add(divPlanName);
                        divPlans.Controls.Add(divPlanTitle);
                        divPlans.Controls.Add(divPlanStatus);
                        foreach (DataRow dr in dtPlans.Rows)
                        {
                            //Plan Title
                            HtmlGenericControl divPlanTitleValue = new HtmlGenericControl();
                            divPlanTitleValue.Attributes.Add("class", "col-md-6 col-xs-6");
                            Label lblPlanTitle = new Label();
                            lblPlanTitle.Text = dr["PlanTitle"].ToString();
                            divPlanTitleValue.Controls.Add(lblPlanTitle);

                            //Plan Name
                            HtmlGenericControl divPlanNameValue = new HtmlGenericControl();
                            divPlanNameValue.Attributes.Add("class", "col-md-3 col-xs-6");
                            Label lblPlanName = new Label();
                            lblPlanName.Text = dr["PolicyNo"].ToString();
                            divPlanNameValue.Controls.Add(lblPlanName);

                            //Plan Status
                            HtmlGenericControl divPlanStatusValue = new HtmlGenericControl();
                            divPlanStatusValue.Attributes.Add("class", "col-md-3 col-xs-6");
                            Label lblPlanStatus = new Label();
                            lblPlanStatus.Text = dr["ProposalState"].ToString();
                            divPlanStatusValue.Controls.Add(lblPlanStatus);

                            HtmlGenericControl divClear = new HtmlGenericControl("Div");
                            divClear.Attributes.Add("class", "clearfix");

                            divPlans.Controls.Add(divPlanTitleValue);
                            divPlans.Controls.Add(divPlanNameValue);
                            divPlans.Controls.Add(divPlanStatusValue);
                            divPlans.Controls.Add(divClear);

                        }

                        int selectedPlans = 0;
                        //bind plans cancelation purpose
                        foreach (DataRow dr in dtPlans.Rows)
                        {
                            ListItem item = new ListItem();
                            item.Text = dr["PolicyNo"].ToString();
                            item.Value = dr["PlanId"].ToString();
                            if (dr["BookingStatus"].ToString() != "1")
                            {
                                item.Selected = true;
                                item.Enabled = false;
                                selectedPlans++;
                            }
                            chkPlans.Items.Add(item);
                        }
                        if (selectedPlans == dtPlans.Rows.Count) //No need to show changeRequest
                        {
                            ChangeRequest = false;
                        }
                    }
                }
                catch { }
                IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
                DateTime travelDate = Convert.ToDateTime(view["DepartureDate"]);
                
                DateTime cancelDate = Convert.ToDateTime(travelDate.AddDays(-Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["INS_CANCEL_DATE"])));
                string state = view["ProposalState"].ToString();
                //if (state == "CONFIRMED" || state == "PARTIALCANCELLED")
                {
                    lblViewBooking.Text = "<input id='Open-" + e.Item.ItemIndex + "' type='button' class='button' value='Open' onclick=\"ViewBooking(" + id + ",'" + bookingeurl + "')\"  />";
                    lblStatus.ForeColor = System.Drawing.Color.LimeGreen;
                }
                btnContine.OnClientClick = "return PlansValidation('" + e.Item.ItemIndex + "');";
                //if (travelDate > cancelDate && travelDate < Convert.ToDateTime(DateTime.Now.ToString("dd/MM/yyyy")) && state != "PROPOSAL" && state != "CANCELLED")
                //if (travelDate > cancelDate && travelDate > DateTime.Now && cancelDate < DateTime.Now && state != "PROPOSAL" && state != "CANCELLED")
                if (travelDate.CompareTo(Convert.ToDateTime(DateTime.Now, dateFormat).Date) > 0 && state != "CANCELLED" && ChangeRequest)
                    {
                        tblChangeRequest.Visible = true;
                        btnRequest.OnClientClick = "return CancelInsPlan('" + e.Item.ItemIndex + "');";
                    }
                
                else if (DateTime.Now < Convert.ToDateTime(cancelDate) && state != "CANCELLED" && ChangeRequest)
                {
                    tblChangeRequest.Visible = true;
                    btnRequest.OnClientClick = "return CancelInsPlan('" + e.Item.ItemIndex + "');";
                }
                lblStatus.Text = state;
                if (state == "CANCELREQUEST")
                {
                    lblStatus.Text = "REQUESTED FOR CANCEL";
                    lblStatus.ForeColor = System.Drawing.Color.Gray;
                    //lblViewBooking.Text = "<input id='Open-" + e.Item.ItemIndex + "' type='button' class='button' value='Open' onclick=\"ViewBooking(" + id + ")\"  />";
                }
                if (lblStatus.Text == "CANCELLED")
                {
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
                lblViewInvoice.Text = "<input id='ViewInvoice-" + e.Item.ItemIndex + "' type='button' class='button' value='View Invoice' onclick=\"ViewInvoice(" + id + ",'"+ invoiceurl + "')\"  />";
                string TransType = view["TransType"].ToString();

                if (TransType == "B2C")
                {
                    lnkPayment.Visible = true;
                    lnkPayment.OnClientClick = "return ViewPaymentInfo('" + e.Item.ItemIndex + "','" + Convert.ToInt32(view["Ins_Id"]) + "');";
                }
                //CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, "ziyad", "0");
            }
        }
        catch (Exception ex)
        {
            //throw ex;
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "dlInsQueue_ItemDataBound ", "0");
        }
    }
    protected void dlInsQueue_ItemCommand(object source, DataListCommandEventArgs e)
    {
        if (e.CommandName == "InsChangeRequest")
        {
            if (e.CommandArgument.ToString().Length > 0)
            {
                string insType = (e.Item.FindControl("lblInsType") as Label).Text.Trim().ToUpper();
                int IPHId = Convert.ToInt32(e.CommandArgument);
                DropDownList ddlChangeRequestType = e.Item.FindControl("ddlChangeRequestType") as DropDownList;
                TextBox txtRemarks = e.Item.FindControl("txtRemarks") as TextBox;
                CheckBoxList chkPlans = e.Item.FindControl("chkPlans") as CheckBoxList;
                ServiceRequest serviceRequest = new ServiceRequest();
                int requestTypeId = 3;
                //Added by hari malla on 25-01-2019 for Religare Product
                try
                {
                    int selectedCount = 0;
                    string planTitle = string.Empty;
                    string policyNo = string.Empty;
                    bool updateHeaderStatus = true;                   
                    InsuranceHeader header = null;
                    ReligareHeader religareHeader = null;
                    if (insType.ToUpper() == "RELIGARE")
                    {
                        religareHeader = new ReligareHeader();
                        religareHeader.Load(IPHId);
                    }
                    else
                    {
                        header = new InsuranceHeader();
                        header.RetrieveConfirmedPlan(IPHId);
                    }
                    if (religareHeader != null)
                    {
                        AgentMaster Agency = new AgentMaster(religareHeader.Agent_id);
                        serviceRequest.BookingId = religareHeader.Header_id;
                        serviceRequest.ReferenceId = Convert.ToInt32(religareHeader.Produ_id);
                        serviceRequest.ProductType = ProductType.Insurance;
                        serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                        serviceRequest.Data = txtRemarks.Text;
                        serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;
                        serviceRequest.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                        serviceRequest.AgencyId = Settings.LoginInfo.AgentId;
                        serviceRequest.PaxName = religareHeader.ReligarePassengers[0].FirstName + religareHeader.ReligarePassengers[0].LastName;
                        serviceRequest.Pnr = religareHeader.Policy_no;
                        serviceRequest.StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["INS_CANCEL_DATE"])));
                        serviceRequest.SupplierName = "Religare";
                        serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                        serviceRequest.ReferenceNumber = religareHeader.Policy_no;
                        serviceRequest.AgencyId = Settings.LoginInfo.AgentId; //Modified by brahmam       //Every request must fall in Admin Queue, so assign Admin ID
                        serviceRequest.ItemTypeId = InvoiceItemTypeId.InsuranceBooking;
                        serviceRequest.DocName = "";
                        serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
                        serviceRequest.LastModifiedOn = DateTime.Now;
                        serviceRequest.CreatedOn = DateTime.Now;
                        //Added by shiva
                        serviceRequest.AgencyTypeId = CT.Core.Agencytype.Cash;
                        serviceRequest.RequestSourceId = RequestSource.BookingAPI;
                        serviceRequest.Save();

                        //Updating Religare status
                        religareHeader.Status = (int)InsuranceBookingStatus.CancelRequest;
                        religareHeader.updateStatus();

                        //Sending email.
                        Hashtable table = new Hashtable(3);
                        table.Add("agentName", Agency.Name);
                        table.Add("policyName", religareHeader.ProductName);
                        table.Add("policyNo", religareHeader.Policy_no);
                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        UserMaster bookedUser = new UserMaster(religareHeader.Created_by);
                        AgentMaster bookedAgency = new AgentMaster(religareHeader.Agent_id);
                        toArray.Add(bookedUser.Email);
                        toArray.Add(bookedAgency.Email1);
                        toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
                        if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["INS_CANCEL_MAIL"]))
                        {
                            string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["INS_CANCEL_MAIL"]).Split(';');
                            foreach (string cnMail in cancelMails)
                            {
                                toArray.Add(cnMail);
                            }
                        }
                        string message = ConfigurationManager.AppSettings["INSURANCE_CHANGE_REQUEST"]; //"Your request for cancelling Insurance plan <b>" + plan.Title + " </b> is under process. Policy No:(" + plan.PolicyNo + ")";
                        try
                        {
                            CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Insurance) Request for cancellation. Policy No:(" + religareHeader.Policy_no + ")", message, table);
                        }
                        catch
                        {
                        }
                    }
                    if (header != null)
                    {
                        AgentMaster Agency = new AgentMaster(header.AgentId);
                        //commented by hari malla testing Tunes Product.
                        if (header != null )/* && chkPlans.SelectedItem != null*/ 
                        {
                            foreach (ListItem chkPlan in chkPlans.Items)
                            {
                                if (chkPlan.Selected && chkPlan.Enabled)
                                {
                                    for (int i = 0; i < header.InsPlans.Count; i++)
                                    {
                                        if (chkPlan.Value.ToString() == header.InsPlans[i].PlanId.ToString())
                                        {
                                            selectedCount++;
                                            serviceRequest.BookingId = 0;
                                            serviceRequest.ReferenceId = Convert.ToInt32(header.InsPlans[i].PlanId);
                                            serviceRequest.ProductType = ProductType.Insurance;
                                            serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                                            serviceRequest.Data = txtRemarks.Text;
                                            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;
                                            serviceRequest.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                                            serviceRequest.AgencyId = Settings.LoginInfo.AgentId;
                                            //serviceRequest.IsDomestic = false;
                                            serviceRequest.PaxName = header.InsPassenger[0].FirstName + header.InsPassenger[0].LastName;
                                            serviceRequest.Pnr = header.InsPlans[i].PolicyNo;
                                            //serviceRequest.Source = plan.BookingSource;
                                            serviceRequest.StartDate = Convert.ToDateTime(DateTime.Now.AddDays(-Convert.ToDouble(System.Configuration.ConfigurationManager.AppSettings["INS_CANCEL_DATE"])));
                                            serviceRequest.SupplierName = "Tune Protect";
                                            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                                            serviceRequest.ReferenceNumber = header.InsPlans[i].PolicyNo;
                                            //serviceRequest.PriceId = itineary.Roomtype[0].PriceId;
                                            //serviceRequest.AgencyId = (int)agency.ID;
                                            serviceRequest.AgencyId = Settings.LoginInfo.AgentId; //Modified by brahmam       //Every request must fall in Admin Queue, so assign Admin ID
                                            serviceRequest.ItemTypeId = InvoiceItemTypeId.InsuranceBooking;
                                            serviceRequest.DocName = "";
                                            serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
                                            serviceRequest.LastModifiedOn = DateTime.Now;
                                            serviceRequest.CreatedOn = DateTime.Now;
                                            //Added by shiva
                                            serviceRequest.AgencyTypeId = CT.Core.Agencytype.Cash;
                                            serviceRequest.RequestSourceId = RequestSource.BookingAPI;

                                        serviceRequest.Save();

                                            if (!string.IsNullOrEmpty(planTitle))
                                            {
                                                planTitle = planTitle + "," + header.InsPlans[i].PlanTitle;
                                            }
                                            else
                                            {
                                                planTitle = header.InsPlans[i].PlanTitle;
                                            }
                                            if (!string.IsNullOrEmpty(policyNo))
                                            {
                                                policyNo = policyNo + "," + header.InsPlans[i].PolicyNo;
                                            }
                                            else
                                            {
                                                policyNo = header.InsPlans[i].PolicyNo;
                                            }
                                            //Updating status
                                            InsurancePlan.Update(header.InsPlans[i].PlanId, (int)InsuranceBookingStatus.CancelRequest, ZeusInsB2B.Zeus.ProposalStatus.PROPOSAL.ToString(), false);
                                        }
                                    }
                                }
                                else if (chkPlan.Selected)
                                {
                                    updateHeaderStatus = false;
                                    selectedCount++;
                                }
                            }
                            if (updateHeaderStatus) //First time only changing status
                            {
                                InsuranceHeader.UpdateInsuranceHeaderStatus(header.Id, (int)InsuranceBookingStatus.CancelRequest);
                            }
                            //else
                            //{
                            //    InsuranceHeader.UpdateInsuranceHeaderStatus(header.Id, (int)InsuranceBookingStatus.PartialCancelled);
                            //}
                            //Sending email.
                            Hashtable table = new Hashtable(3);
                            table.Add("agentName", Agency.Name);
                            table.Add("policyName", planTitle);
                            table.Add("policyNo", policyNo);
                            UserMaster bookedUser = new UserMaster(header.CreatedBy);
                            AgentMaster bookedAgency = new AgentMaster(header.AgentId);
                            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                            toArray.Add(bookedUser.Email);
                            toArray.Add(bookedAgency.Email1);
                            toArray.Add(ConfigurationManager.AppSettings["MAIL_COPY_RECIPIENTS"]);
                            if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["INS_CANCEL_MAIL"]))
                            {
                                string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["INS_CANCEL_MAIL"]).Split(';');
                                foreach (string cnMail in cancelMails)
                                {
                                    toArray.Add(cnMail);
                                }
                            }
                            string message = ConfigurationManager.AppSettings["INSURANCE_CHANGE_REQUEST"]; //"Your request for cancelling Insurance plan <b>" + plan.Title + " </b> is under process. Policy No:(" + plan.PolicyNo + ")";
                            try
                            {
                                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Insurance) Request for cancellation. Policy No:(" + policyNo + ")", message, table);
                            }
                            catch
                            {
                            }
                        }
                    }
                    Response.Redirect("INSURANCEQUEUE.ASPX", false);
                }
                catch (Exception ex)
                {
                    Audit.Add(EventType.HotelBook, Severity.High, 1, "Exception in the Hotel Insurance Request. Message:" + ex.Message, "");
                }
            }
        }
    }
}
