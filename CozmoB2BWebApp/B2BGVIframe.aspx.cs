﻿using CT.TicketReceipt.BusinessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class B2BGVIframe :CT.Core.ParentPage
    {
        protected int b2bagentId;
        protected long userId;
        protected string userName;
        protected string product =string.Empty;
        protected string password;

        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                this.Master.PageRole = true;
                DataTable dtAgentInfo = AgentMaster.GetVMSB2BAgentInfo(Settings.LoginInfo.AgentId, "GVUSER");
                if (dtAgentInfo != null && dtAgentInfo.Rows.Count > 0 && !Settings.LoginInfo.AgentBlock) //Checking Booking block or Not Added Brahmam
                {
                    foreach (DataRow dr in dtAgentInfo.Rows)
                    {
                        if (dr["user_login_name"] != DBNull.Value)
                        {
                            userName = Convert.ToString(dr["user_login_name"]);
                        }
                        b2bagentId = Settings.LoginInfo.AgentId;
                        userId = Settings.LoginInfo.UserID;
                        if (Request["product"] != null)
                        {
                            product = Convert.ToString(Request["product"]);
                        }
                        if (dr["user_password"] != DBNull.Value)
                        {
                            password = Encrypt(Convert.ToString(dr["user_password"])); //need to pass encrypted password to the page.
                        }
                        break;
                    }
                    if (!string.IsNullOrEmpty(Request.QueryString["Type"].ToString()))
                        product = Convert.ToString(Request.QueryString["Type"]).ToUpper();
                }
            }
            catch
            {

            }

        }


        private string Encrypt(string clearText)
        {
            string EncryptionKey = "MAKV2SPBNI99212";
            byte[] clearBytes = Encoding.Unicode.GetBytes(clearText);
            using (Aes encryptor = Aes.Create())
            {
                Rfc2898DeriveBytes pdb = new Rfc2898DeriveBytes(EncryptionKey, new byte[] { 0x49, 0x76, 0x61, 0x6e, 0x20, 0x4d, 0x65, 0x64, 0x76, 0x65, 0x64, 0x65, 0x76 });
                encryptor.Key = pdb.GetBytes(32);
                encryptor.IV = pdb.GetBytes(16);
                using (MemoryStream ms = new MemoryStream())
                {
                    using (CryptoStream cs = new CryptoStream(ms, encryptor.CreateEncryptor(), CryptoStreamMode.Write))
                    {
                        cs.Write(clearBytes, 0, clearBytes.Length);
                        cs.Close();
                    }
                    clearText = Convert.ToBase64String(ms.ToArray());

                }
            }
            return clearText;
        }
    }
}
