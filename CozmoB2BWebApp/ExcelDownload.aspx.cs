﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;

namespace CozmoB2BWebApp
{
    public partial class ExcelDownload : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Settings.LoginInfo != null)
            {
                DownloadExcel();
            }
        }
        protected void DownloadExcel()
        {
            if (Session["FlightRequest"] != null && Session["sessionId"] != null)
            {
                SearchResult[] result = new SearchResult[0];
                string[] ids = Request.QueryString["ids"].Split(new char[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
                result = new SearchResult[ids.Length];
                for (int i = 0; i < ids.Length; i++)
                {
                    int resultId = Convert.ToInt32(ids[i]);
                    result[i] = Basket.FlightBookingSession[Session["sessionId"].ToString()].Result[resultId - 1];
                }
                SearchRequest request = Session["FlightRequest"] as SearchRequest;
                AgentMaster agency = new AgentMaster();
                int decimalValue = 0; string currency = string.Empty;
                decimal markup = Convert.ToDecimal(Request["markup"]);
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    decimalValue = Settings.LoginInfo.OnBehalfAgentDecimalValue;
                    currency = Settings.LoginInfo.OnBehalfAgentCurrency;
                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                }
                else
                {
                    decimalValue = Settings.LoginInfo.DecimalValue;
                    currency = Settings.LoginInfo.Currency;
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                }

                bool routing = false, bindHeading = false, onwardHeading=false,returnHeading=false;
                

                if(Request["routing"] != null)
                {
                    routing = Convert.ToBoolean(Request["routing"]);
                }

                if (agency.RequiredItinerary)
                {
                    StringBuilder html = new StringBuilder();
                    for (int i = 0; i < result.Length; i++)
                    {
                        decimal agentMarkup = 0m, serviceTax = 0m, otherCharges = 0m, discount = 0m;

                        foreach (Fare fare in result[i].FareBreakdown)
                        {
                            agentMarkup += fare.AgentMarkup;
                            discount += fare.AgentDiscount;
                        }
                        otherCharges = result[i].Price.OtherCharges;
                        if (Settings.LoginInfo.LocationCountryCode == "IN")
                        {
                            serviceTax = (agentMarkup + markup) * 18 / 100;
                        }

                        if (routing)
                        {
                            if (result[i].Flights[0].Any(x => x.Group == 0) && !onwardHeading)
                            {
                                html.AppendLine("<table style='font-family: Arial,sans-serif;font-size:8pt;border:0;border-bottom: 1px solid #999;border-right: 1px solid #999;border-top: 0;border-left: 0;width:100%;text-align:center;' cellspacing='0' cellpadding='3'>");
                                html.AppendLine("<tr><td colspan='20' style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong>" + (result[i].Flights[0].Any(x => x.Group == 0) ? "Onward Itineraries" : "Return Itineraries") + "</strong></td>");
                                html.AppendLine("</tr></table>");
                            }
                            if (result[i].Flights[0].Any(x => x.Group == 1) && !returnHeading)
                            {
                                html.AppendLine("<table style='font-family: Arial,sans-serif;font-size:8pt;border:0;border-bottom: 1px solid #999;border-right: 1px solid #999;border-top: 0;border-left: 0;width:100%;text-align:center;' cellspacing='0' cellpadding='3'>");
                                html.AppendLine("<tr><td colspan='20' style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong>" + (result[i].Flights[0].Any(x => x.Group == 0) ? "Onward Itineraries" : "Return Itineraries") + "</strong></td>");
                                html.AppendLine("</tr></table>");
                            }
                        }
                        

                        html.AppendLine("<table style='font-family: Arial,sans-serif;font-size:8pt;border:0;border-bottom: 1px solid #999;border-right: 1px solid #999;border-top: 0;border-left: 0;width:100%;text-align:center;' cellspacing='0' cellpadding='3'>");
                        if (!bindHeading && !routing)
                        {
                            AddRowHeadings(html);
                            bindHeading = true;                            
                        }
                        else if(routing)
                        {
                            if (result[i].Flights[0].Any(x => x.Group == 0) && !onwardHeading)
                            {
                                AddRowHeadings(html);
                                onwardHeading = true;
                            }
                            else if(result[i].Flights[0].Any(x => x.Group == 1) && !returnHeading)
                            {
                                AddRowHeadings(html);
                                returnHeading = true;
                            }
                        }

                        int segCount = result[i].Flights[0].Length;
                        if (result[i].Flights.Length > 1)
                        {
                            segCount += result[i].Flights[1].Length;
                        }
                        int fc = 0, bc = 0;
                        for (int j = 0; j < result[i].Flights.Length; j++)
                        {
                            for (int k = 0; k < result[i].Flights[j].Length; k++)
                            {
                                Airline airline = new Airline();
                                FlightInfo seg = result[i].Flights[j][k];
                                airline.Load(seg.Airline);

                                if (j == 0)
                                {
                                    bc = j + k;
                                }
                                else
                                {
                                    bc++;
                                }
                                string baggage = string.Empty;
                                //if (!string.IsNullOrEmpty(result[i].BaggageIncludedInFare))
                                //{
                                //    if (result[i].BaggageIncludedInFare.Split(',')[bc].ToLower().Contains("kg"))
                                //        baggage = result[i].BaggageIncludedInFare.Split(',')[bc];
                                //    else
                                //        baggage = (result[i].BaggageIncludedInFare.Split(',')[bc].Split(' ').Length > 1 ? result[i].BaggageIncludedInFare.Split(',')[bc] : result[i].BaggageIncludedInFare.Split(',')[bc] + " KG");
                                //}
                                html.AppendLine("<tr>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + airline.AirlineName + "</td>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + seg.FlightNumber + "</td>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + (string.IsNullOrEmpty(seg.CabinClass) ? (request.Segments[0].flightCabinClass == CabinClass.All ? "Economy" : request.Segments[0].flightCabinClass.ToString()) : (seg.CabinClass.Length > 2 ? seg.CabinClass : "Economy")) + "</td>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + seg.Origin.CityName + ", " + seg.Origin.CountryName + "</td>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + seg.DepartureTime.ToString("dd MMM yyyy") + "</td>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + seg.DepartureTime.ToString("HH:mm tt") + "</td>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + seg.DepTerminal + "</td>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + seg.Destination.CityName + ", " + seg.Destination.CountryName + "</td>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + seg.ArrivalTime.ToString("dd MMM yyyy") + "</td>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + seg.ArrivalTime.ToString("HH:mm tt") + "</td>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + seg.ArrTerminal + "</td>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + seg.Duration.ToString().Split(':')[0] + " Hrs " + seg.Duration.ToString().Split(':')[1] + " Mins" + "</td>");
                                html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>Available</td>");
                                //html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + baggage + "</td>");
                                if (result[i].ResultBookingSource == BookingSource.Indigo || result[i].ResultBookingSource == BookingSource.SpiceJet)
                                {
                                    html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + (string.IsNullOrEmpty(seg.SegmentFareType) ? "" : seg.SegmentFareType) + "</td>");
                                }
                                else
                                {
                                    html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'>" + (!string.IsNullOrEmpty(result[i].FareType) && result[i].FareType.Split(',').Length > bc ? result[i].FareType.Split(',')[bc] : (string.IsNullOrEmpty(seg.SegmentFareType) ? "Published" : seg.SegmentFareType)) + "</td>");
                                }
                                if (fc == 0)
                                {
                                    html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'  rowspan = '" + segCount + "'>" + (result[i].BaseFare).ToString("N" + decimalValue) + "</td>");
                                    html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'  rowspan = '" + segCount + "'>" + (result[i].Tax + (double)(otherCharges + agentMarkup - discount)).ToString("N" + decimalValue) + "</td>");
                                    html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'  rowspan = '" + segCount + "'>" + (result[i].BaseFare + result[i].Tax + (double)(otherCharges + agentMarkup - discount)).ToString("N" + decimalValue) + "</td>");
                                    html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'  rowspan = '" + segCount + "'>" + serviceTax.ToString("N" + decimalValue) + "</td>");
                                    if (routing)
                                    {
                                        //Show only Download Markup for Onward Itinerary
                                        if (result[i].Flights[0].Any(x => x.Group == 0))
                                            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'  rowspan = '" + segCount + "'>" + markup.ToString("N" + decimalValue) + "</td>");
                                        else
                                            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'  rowspan = '" + segCount + "'>" + Convert.ToDecimal(0).ToString("N" + decimalValue) + "</td>");
                                    }
                                    else
                                        html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'  rowspan = '" + segCount + "'>" + markup.ToString("N" + decimalValue) + "</td>");

                                    if (routing)
                                    {
                                        if (result[i].Flights[0].Any(x => x.Group == 0))
                                            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'  rowspan = '" + segCount + "'>" + currency + " " + (result[i].ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling((result[i].TotalFare + (double)(serviceTax + markup))).ToString("N" + Settings.LoginInfo.DecimalValue) : (result[i].TotalFare + (double)(serviceTax + markup)).ToString("N" + decimalValue)) + "</td>");
                                        else
                                            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'  rowspan = '" + segCount + "'>" + currency + " " + (result[i].ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling((result[i].TotalFare + (double)(serviceTax))).ToString("N" + Settings.LoginInfo.DecimalValue) : (result[i].TotalFare + (double)(serviceTax)).ToString("N" + decimalValue)) + "</td>");
                                    }
                                    else
                                        html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;text-align:center;'  rowspan = '" + segCount + "'>" + currency + " " + (result[i].ResultBookingSource == BookingSource.TBOAir ? Math.Ceiling((result[i].TotalFare + (double)(serviceTax + markup))).ToString("N" + Settings.LoginInfo.DecimalValue) : (result[i].TotalFare + (double)(serviceTax + markup)).ToString("N" + decimalValue)) + "</td>");
                                }
                                fc++;
                                html.AppendLine("</tr>");
                            }
                        }
                        html.AppendLine("</table>");
                    }
                    
                    Response.AppendHeader("content-disposition", "attachment;filename=Itineraries-" + DateTime.Now.ToString("yyyyMMdd_hhmmss") + ".xls");
                    Response.Cache.SetCacheability(System.Web.HttpCacheability.NoCache);
                    Response.ContentType = "application/vnd.openxmlformats-officedocument.spreadsheet.sheet";// "application/vnd.ms-excel";
                    Response.Write(html.ToString());
                    Response.End();
                }
            }
        }

        protected void AddRowHeadings(StringBuilder html)
        {
            //html.AppendLine("<tr><td style = 'font-size:11px;text-align: center;border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;' colspan='20'><strong>Itinerary " + (i + 1) + " of " + result.Length + " selected</strong></td></tr>");
            html.AppendLine("<tr><td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Airline </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Flight Number </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Class </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> From Airport </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Departure Date </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Departure Time </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Departure Terminal </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> To Airport </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Arrival Date </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Arrival Time </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Arrival Terminal </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Flying Hours </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Status </strong></td>");
            //html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Baggage </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Fare Type </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Basic Fare </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Tax </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Nett Fare </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Management Fee </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> GST on Management Fee </strong></td>");
            html.AppendLine("<td style = 'border-top: 1px solid #999;border-left: 1px solid #999;border-right: 0;border-bottom: 0;background-color: #B9D6E4;text-align:center;'><strong> Total Fare </strong></td></tr>");

        }
    }
}
