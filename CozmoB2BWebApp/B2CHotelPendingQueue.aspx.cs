﻿using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using System;
using System.Data;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class B2CHotelPendingQueue : CT.Core.ParentPage
    {
        DataTable dataTable = null;
        protected void Page_Load(object sender, EventArgs e)
        {

            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    try
                    {
                        Bind_paymentSources();
                        Bind_BookingStatus();
                        Bind_Agents();
                        BindValues();
                        bindGrid();
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed Binding the Hotel pending queue grid and payment sources :" + ex.ToString(), Request["REMOTE_ADDR"]);
                    }
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx", false);
            }
        }
        
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            bindGrid();
        }

        protected void GridPendingQueue_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                bindGrid();
                GridPendingQueue.PageIndex = e.NewPageIndex;
                GridPendingQueue.DataBind();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to GridView Paging :" + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }
        #region private methods
        private void bindGrid()
        {
            try
            {
                DateTime fromDate = DateTime.MinValue, toDate = DateTime.MaxValue;
                IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
                if (txtFromDate.Text != string.Empty)
                {
                    try
                    {
                        fromDate = Convert.ToDateTime(txtFromDate.Text, provider);
                    }
                    catch { }
                }
                else
                {
                    fromDate = DateTime.Now;
                }

                if (txtToDate.Text != string.Empty)
                {
                    try
                    {
                        toDate = Convert.ToDateTime(Convert.ToDateTime(txtToDate.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
                    }
                    catch { }
                }
                else
                {
                    toDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
                }
                dataTable = TicketQueue.Get_HotelApiPendingQueueDetails(ddlpaymentSource.SelectedValue, txtPaymentId.Text, txtOrderId.Text, txtHotelName.Text, fromDate, toDate, ddlbookingStatus.SelectedValue, ddlAgents.SelectedValue);
                if (dataTable != null)
                {
                    dataTable = apiQueueStatus(dataTable);
                    GridPendingQueue.DataSource = dataTable;
                    GridPendingQueue.DataBind();
                }
                BindValues();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "failed to Binding the Hotel Api Pending Queue Details in Grid :" + ex.ToString(), Request["REMOTE_ADDR"]);
                throw ex;
            }
        }
        private DataTable apiQueueStatus(DataTable dataTable)
        {
            try
            {
                int i = 0;
                foreach (DataRow drOutput in dataTable.Rows)
                {
                    int paymentGatewayId = Convert.ToInt32(drOutput["paymentGatewaySourceId"]);
                    int paymentstatus = Convert.ToInt32(drOutput["PaymentStatus"]);
                    int bookingStatus = Convert.ToInt32(drOutput["bookingStatus"]);
                    drOutput["PaymentGateway"] = Enum.GetName(typeof(CT.AccountingEngine.PaymentGatewaySource), paymentGatewayId);
                    drOutput["Booking_status"] = Enum.GetName(typeof(BookingStatus), bookingStatus);
                    drOutput["payment_status"] = Enum.GetName(typeof(CT.AccountingEngine.PaymentStatus), paymentstatus);
                    i++;
                }
                return dataTable;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "failed to Changing the hotel Enum Statuses :" + ex.ToString(), Request["REMOTE_ADDR"]);
                throw ex;
            }
        }
        private void Bind_paymentSources()
        {
            try
            {
                Array StatusArray = Enum.GetValues(typeof(CT.AccountingEngine.PaymentGatewaySource));
                ddlpaymentSource.Items.Clear();
                foreach (CT.AccountingEngine.PaymentGatewaySource status in StatusArray)
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(CT.AccountingEngine.PaymentGatewaySource), status), ((int)status).ToString());
                    ddlpaymentSource.Items.Add(item);
                }
                ddlpaymentSource.Items.Insert(0, new ListItem("ALL", "0"));
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "failed to Binding the Hotel  payment Sources :" + ex.ToString(), Request["REMOTE_ADDR"]);
                throw ex;
            }
        }
        private void Bind_BookingStatus()
        {
            try
            {
                Array StatusArray = Enum.GetValues(typeof(CT.BookingEngine.BookingStatus));
                ddlbookingStatus.Items.Clear();
                foreach (CT.AccountingEngine.PaymentGatewaySource status in StatusArray)
                {
                    ListItem item = new ListItem(Enum.GetName(typeof(CT.BookingEngine.BookingStatus), status), ((int)status).ToString());
                    ddlbookingStatus.Items.Add(item);
                }
                ddlbookingStatus.Items.Insert(0, new ListItem("ALL", "0"));
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "failed to Binding the Hotel booking Sources :" + ex.ToString(), Request["REMOTE_ADDR"]);
                throw ex;
            }
        }
        private void Bind_Agents()
        {
            try
            {
                dataTable = AgentMaster.GetB2CAgentList();
                if (dataTable != null && dataTable.Rows.Count > 0)
                {
                    ddlAgents.DataTextField = "agent_name";
                    ddlAgents.DataValueField = "agent_id";
                    ddlAgents.DataSource = dataTable;
                    ddlAgents.DataBind();
                    ddlAgents.Items.Insert(0, new ListItem("ALL", "0"));
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "failed to Binding B2C Agents :" + ex.ToString(), Request["REMOTE_ADDR"]);
                throw ex;
            }
        }
        private void BindValues()
        {
            txtFromDate.Text = string.IsNullOrEmpty(txtFromDate.Text) ? DateTime.Now.ToString("dd/MMM/yyyy") : txtFromDate.Text;
            txtToDate.Text = string.IsNullOrEmpty(txtToDate.Text) ? DateTime.Now.ToString("dd/MMM/yyyy") : txtToDate.Text;            
        }
        
        #endregion
    }
}