﻿using System;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.BookingEngine.Insurance;
using CT.TicketReceipt.BusinessLayer;
using System.Linq;
public partial class ViewBookingForInsuranceGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected InsuranceHeader InsuranceHdr;
    protected int insId =0;
    protected CT.TicketReceipt.BusinessLayer.AgentMaster agent;
    protected string DepartureCity = string.Empty;
    protected string ArrivalCity = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    if (Request.QueryString["bookingId"] != null)
                    {
                        insId = Convert.ToInt32(Request.QueryString["bookingId"]);
                        InsuranceHdr = new InsuranceHeader();
                        InsuranceHdr.RetrieveConfirmedPlan(insId);
                        agent = new CT.TicketReceipt.BusinessLayer.AgentMaster(InsuranceHdr.AgentId);
                        DepartureCity = CT.BookingEngine.Util.GetCityName(InsuranceHdr.DepartureStationCode);
                        ArrivalCity = CT.BookingEngine.Util.GetCityName(InsuranceHdr.ArrivalStationCode);
                        //BindVoucher(InsuranceHdr);
                    }
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "ViewBookingForInsurance", "0");
        }
    }
    void BindVoucher(InsuranceHeader header)
    {
        try
        {

            if (header.InsPlans != null)
            {
                for (int n = 0; n < header.InsPassenger.Count; n++)
                {
                    HtmlGenericControl divPaxNameValue = new HtmlGenericControl();
                    divPaxNameValue.Attributes.Add("class", "col-md-3 col-xs-6");
                    Label lblPaxName = new Label();
                    lblPaxName.Text ="Pax Name:<b>"+ header.InsPassenger[n].FirstName + " " + header.InsPassenger[n].LastName+"</b>";
                    divPaxNameValue.Controls.Add(lblPaxName);

                    HtmlGenericControl divPaxTypeValue = new HtmlGenericControl();
                    divPaxTypeValue.Attributes.Add("class", "col-md-3 col-xs-6");
                    Label lblPaxType = new Label();
                    lblPaxType.Text = "Pax Type:<b>" + header.InsPassenger[n].PaxType + "</b>";
                    divPaxTypeValue.Controls.Add(lblPaxType);

                    HtmlGenericControl divPaxGenderValue = new HtmlGenericControl();
                    divPaxGenderValue.Attributes.Add("class", "col-md-3 col-xs-6");
                    Label lblPaxGender = new Label();
                    lblPaxGender.Text = "Pax Gender:<b>" + header.InsPassenger[n].Gender.ToString() + "</b>";
                    divPaxGenderValue.Controls.Add(lblPaxGender);

                    HtmlGenericControl divPaxDOBValue = new HtmlGenericControl();
                    divPaxDOBValue.Attributes.Add("class", "col-md-3 col-xs-6");
                    Label lblPaxDOB = new Label();
                    lblPaxDOB.Text = "Pax DOB:<b>" + header.InsPassenger[n].DOB.ToString() + "</b>";
                    divPaxDOBValue.Controls.Add(lblPaxDOB);

                    HtmlGenericControl divPaxEmailValue = new HtmlGenericControl();
                    divPaxEmailValue.Attributes.Add("class", "col-md-3 col-xs-6");
                    Label lblPaxEmail = new Label();
                    lblPaxEmail.Text = "Pax Email:<b>" + header.InsPassenger[n].Email + "</b>";
                    divPaxEmailValue.Controls.Add(lblPaxEmail);

                    HtmlGenericControl divPaxCityValue = new HtmlGenericControl();
                    divPaxCityValue.Attributes.Add("class", "col-md-3 col-xs-6");
                    Label lblPaxCity = new Label();
                    lblPaxCity.Text = "Pax City:<b>" + header.InsPassenger[n].City + "</b>";
                    divPaxCityValue.Controls.Add(lblPaxCity);

                    HtmlGenericControl divPaxCountyValue = new HtmlGenericControl();
                    divPaxCountyValue.Attributes.Add("class", "col-md-3 col-xs-6");
                    Label lblPaxCounty = new Label();
                    lblPaxCounty.Text = "Pax Country:<b>" + header.InsPassenger[n].City + "</b>";
                    divPaxCountyValue.Controls.Add(lblPaxCounty);

                    HtmlGenericControl divPaxPhoneValue = new HtmlGenericControl();
                    divPaxPhoneValue.Attributes.Add("class", "col-md-3 col-xs-6");
                    Label lblPaxPhone = new Label();
                    lblPaxPhone.Text = "Pax Phone:<b>" + header.InsPassenger[n].PhoneNumber + "</b>";
                    divPaxPhoneValue.Controls.Add(lblPaxPhone);

                    HtmlGenericControl divPaxDocuTypeValue = new HtmlGenericControl();
                    divPaxDocuTypeValue.Attributes.Add("class", "col-md-3 col-xs-6");
                    divPaxDocuTypeValue.Attributes.Add("style", " margin-bottom: 10px;");
                    Label lblPaxDocuType = new Label();
                    lblPaxDocuType.Text = "Pax Doc Type:<b>" + header.InsPassenger[n].DocumentType.ToString() + "</b>";
                    divPaxDocuTypeValue.Controls.Add(lblPaxDocuType);

                    HtmlGenericControl divPaxDocuNoValue = new HtmlGenericControl();
                    divPaxDocuNoValue.Attributes.Add("class", "col-md-3 col-xs-6");
                    divPaxDocuNoValue.Attributes.Add("style", " margin-bottom: 10px;");
                    Label lblPaxDocuNo = new Label();
                    lblPaxDocuNo.Text = "Pax Doc No:<b>" + header.InsPassenger[n].DocumentNo + "</b>";
                    divPaxDocuNoValue.Controls.Add(lblPaxDocuNo);


                    HtmlGenericControl divClearPax = new HtmlGenericControl("Div");
                    divClearPax.Attributes.Add("class", "clearfix");
                    divPlan.Controls.Add(divPaxNameValue);
                    divPlan.Controls.Add(divPaxTypeValue);
                    divPlan.Controls.Add(divPaxGenderValue);
                    divPlan.Controls.Add(divPaxDOBValue);
                    divPlan.Controls.Add(divPaxEmailValue);
                    divPlan.Controls.Add(divPaxCityValue);
                    divPlan.Controls.Add(divPaxCountyValue);
                    divPlan.Controls.Add(divPaxPhoneValue);
                    divPlan.Controls.Add(divPaxDocuTypeValue);
                    divPlan.Controls.Add(divPaxDocuNoValue);
                    divPlan.Controls.Add(divClearPax);
                    
                    for (int k = 0; k < header.InsPlans.Count; k++)
                    {

                        HtmlGenericControl divlblcertificationValue = new HtmlGenericControl("Div"); // For Adult City
                        divlblcertificationValue.Attributes.Add("class", "col-md-3 col-xs-6");
                        Label lblCertification = new Label();
                        lblCertification.Text = header.InsPlans[k].PolicyNo;
                        divlblcertificationValue.Controls.Add(lblCertification);

                        HtmlGenericControl divlblPlanNameValue = new HtmlGenericControl("Div"); // For Adult City
                        divlblPlanNameValue.Attributes.Add("class", "col-md-3 col-xs-6");
                        Label lblPlanName = new Label();
                        lblPlanName.Text = header.InsPlans[k].InsPlanCode;
                        divlblPlanNameValue.Controls.Add(lblPlanName);


                        //policyNo
                        HtmlGenericControl divPolicyNo = new HtmlGenericControl("Div"); // For Adult City
                        divPolicyNo.Attributes.Add("class", "col-md-3 col-xs-6");
                        Label lblPolicyNo = new Label();
                        if (header.InsPassenger[n].PolicyNo.Split('|').Length >= k)
                        {

                            lblPolicyNo.Text = header.InsPassenger[n].PolicyNo.Split('|')[k];

                        }
                        divPolicyNo.Controls.Add(lblPolicyNo);

                        //certificate Link
                        HtmlGenericControl divCertificate = new HtmlGenericControl("Div"); // For Adult City
                        divCertificate.Attributes.Add("class", "col-md-3 col-xs-6");
                        Label lblCerticate = new Label();
                        if (header.InsPassenger[n].PolicyUrlLink.Split('|').Length >= k)
                        {
                            lblCerticate.Text = "<a target='_blank' href='" + header.InsPassenger[n].PolicyUrlLink.Split('|')[k] + "' >View Certificate</a>";
                            divCertificate.Controls.Add(lblCerticate);
                        }
                        HtmlGenericControl divClear = new HtmlGenericControl("Div");
                        divClear.Attributes.Add("class", "clearfix");
                        divPlan.Controls.Add(divlblcertificationValue);
                        divPlan.Controls.Add(divlblPlanNameValue);
                        divPlan.Controls.Add(divPolicyNo);
                        divPlan.Controls.Add(divCertificate);
                        divPlan.Controls.Add(divClear);
                    }

                }
            }
        }
        catch { throw; }


    }
}
