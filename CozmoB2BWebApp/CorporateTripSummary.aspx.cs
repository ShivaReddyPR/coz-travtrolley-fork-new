﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;

public partial class CorporateTripSummaryUI : CT.Core.ParentPage
{
    protected List<FlightItinerary> listFlightItinerary = new List<FlightItinerary>();
    protected string _approvalStatus;
    protected string approverNames = string.Empty;
    protected int approverId = 0;

    protected BookingDetail booking;
    protected decimal AirFare = 0, Taxes = 0, Baggage = 0, MarkUp = 0, Discount = 0, AsvAmount = 0,
        SeatPref = 0, MealPrice = 0;
    protected AgentMaster agency;
    //protected Airline airline;
    protected string status = string.Empty;
    protected string agentImageSource = string.Empty;
    protected CorporateProfileTripDetails detail;


    /// <summary>
    /// Corporate booking Params
    /// </summary>
    protected bool isBookingSuccess = false;
    protected List<Ticket> ticketList;
    protected FlightItinerary flightItinerary;
    protected string inBaggage = "", outBaggage = "";
    protected CT.Core.Airline airline;
    protected string AirlineLogoPath = ConfigurationManager.AppSettings["AirlineLogoPath"];
    protected decimal charges = 0;
    protected int emailCounter = 1;//Checking for Agent details display in Email body
    protected AutoResetEvent[] BookingEvents;
    protected int agencyId = 0;

    protected string empEmail = string.Empty;
    protected string empId = string.Empty;
    protected string empName = string.Empty;
    protected string emailTo = string.Empty;

    List<AgentAppConfig> appConfigs = new List<AgentAppConfig>();
    protected double deadline = 5;//Default 5 Hours
    protected List<FlightPolicy> flightPolicies = new List<FlightPolicy>();
    protected string userLoginName = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Settings.LoginInfo != null)
            {
                if (Settings.LoginInfo.IsOnBehalfOfAgent)
                {
                    agency = new AgentMaster(Settings.LoginInfo.OnBehalfAgentID);
                    agencyId = Settings.LoginInfo.OnBehalfAgentID;

                }
                else
                {
                    agency = new AgentMaster(Settings.LoginInfo.AgentId);
                    agencyId = Settings.LoginInfo.AgentId;

                }

                if (Request.QueryString["flightId"] != null)
                {
                    //added product Id as param to fetch the status.


                    int flightId = Convert.ToInt32(Request.QueryString["flightId"]);
                    //Session["selFlightId"] = Request.QueryString["flightId"];


                    //flightItinerary = new FlightItinerary(Convert.ToInt32(Request.QueryString["flightId"]));
                    //listFlightItinerary = FlightItinerary.GetCorpItineraries(Convert.ToInt32(Request.QueryString["flightId"]));
                    //listFlightItinerary.Add(flightItinerary);
                    GetItineraries();
                    int approvers = 1; string appStatus = string.Empty;
                    foreach (FlightItinerary itinerary in listFlightItinerary)
                    {
                        CorporateProfileTripDetails tripDetails = new CorporateProfileTripDetails(itinerary.FlightId);
                        List<CorpProfileApproval> profileApprovals = CorpProfileApproval.RemoveDuplicatesIterative(tripDetails.ProfileApproversList);
                        approvers = profileApprovals.Count;
                        profileApprovals.ForEach(x => appStatus += appStatus.Length > 1 ? "," + x.ApprovalStatus : x.ApprovalStatus);
                    }
                    if (appStatus.Split(',').ToList().Any(x => x == "Awaiting Approval"))
                        status = "Pending";
                    else if (appStatus.Split(',').ToList().FindAll(x => x == "Approved").Count >= approvers)
                        status = "Approved";
                    else if (appStatus.Split(',').ToList().FindAll(x => x == "Rejected").Count >= approvers)
                        status = "Rejected";
                    else
                        status = "Pending";

                    detail = new CorporateProfileTripDetails(flightId);
                    

                    BuidItinerary();
                    AgentAppConfig appConfig = new AgentAppConfig();
                    appConfig.AgentID = agencyId;
                    appConfig.ProductID = 1;
                    appConfigs = appConfig.GetConfigData();

                    if (appConfigs.Count > 0)
                    {
                        AgentAppConfig config = appConfigs.Find(ac => ac.AppKey == "CorpApprovalDeadline");
                        if (config != null)
                        {
                            deadline = Convert.ToDouble(config.AppValue);
                        }
                    }
                    /****************Additional Bookings Start*******************/
                    //if (Session["Result"] != null && Session["FlightRequest"] != null && Session["TripId"] != null && Session["CriteriaItineraries"] != null && Session["sessionId"] != null && Session["CorpBookingResponses"] != null)
                    if (Session["Result"] != null && Session["FlightRequest"] != null && Session["TripId"] != null && Session["sessionId"] != null && Session["CorpBookingResponses"] != null) // Removed CriteriaItineraries as not using any more
                    {
                        /******Start:Send EMAIL To  Corporate Employee and to the approvers********/
                        try
                        {
                            Thread CorpThread = new Thread(new ParameterizedThreadStart(DoCorporateBookings));
                            CorpThread.Start(Settings.LoginInfo);
                            CorpThread.Join();//wait for the optional booking's to happen before sending email's
                            listFlightItinerary.Clear();
                            //Retrieve all the itineraries from DB since we are checking for the flightId > 0
                            GetItineraries();

                            // Checking falg in APP_CONFIG to send approval Email
                            AgentAppConfig clsAppCnf = new AgentAppConfig();
                            clsAppCnf.AgentID = (int)agency.ID;
                            //checking app key value for location email if not exist then sending emp, approvers email.
                            string isLocationEmail = clsAppCnf.GetConfigData().Where(x => x.AppKey.ToUpper() == "FLIGHTTRIPSUMMARY_LOCATION_EMAIL" && x.ProductID == 1).Select(y => y.AppValue).FirstOrDefault();
                            string isApprovalMail = clsAppCnf.GetConfigData().Where(x => x.AppKey.ToUpper() == "CORP_TRNX_APRVL_MAIL").Select(y => y.AppValue).FirstOrDefault();
                            string emailHtmlEmployee = string.Empty, emailHtmlApproval = string.Empty, emailHtmlLocation = string.Empty, header = string.Empty, footer = "</div>";
                            string subjectEmployee = string.Empty, subjectApproval = string.Empty, flightSegments = string.Empty, flexFields = string.Empty;
                            List<string> mailAddresses = new List<string>();
                            List<string> approvalMailAddresses = new List<string>();
                            int counter = 0;

                            //Send the trip summary to the booker/employee
                            if (!string.IsNullOrEmpty(agency.Email1))
                                mailAddresses.Add(agency.Email1);

                            long locationId = Settings.LoginInfo.IsOnBehalfOfAgent ? Settings.LoginInfo.OnBehalfAgentLocation : Settings.LoginInfo.LocationID;

                            LocationMaster locationMaster = new LocationMaster(locationId);
                            if (!string.IsNullOrEmpty(locationMaster.LocationEmail) && locationMaster.LocationEmail.Contains("@"))
                            {
                                if (locationMaster.LocationEmail.Contains(","))
                                {
                                    string[] emails = locationMaster.LocationEmail.Split(',');
                                    for (int i = 0; i < emails.Length; i++)
                                    {
                                        mailAddresses.Add(emails[i]);
                                    }
                                }
                                else
                                    mailAddresses.Add(locationMaster.LocationEmail);
                            }

                            //Send the trip summary to the booker if booker and employee email is different
                            if (!string.IsNullOrEmpty(agency.Email1) && !string.IsNullOrEmpty(detail.EmpEmail) && detail.EmpEmail != agency.Email1)
                                mailAddresses.Add(detail.EmpEmail);

                            //Get the approver email id's
                            List<int> listOfAppId = detail.ProfileApproversList.Where(i => i.Hierarchy == 1).Select(i => i.ApproverId).ToList();
                            if (listOfAppId != null)
                            {
                                listOfAppId.ForEach(appId =>
                                {
                                    approverId = appId;
                                    string appEmail = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverEmail).FirstOrDefault();
                                    if (!string.IsNullOrEmpty(appEmail))
                                        approvalMailAddresses.Add(appEmail);
                                });
                            }
                            flightItinerary = listFlightItinerary[0];

                            if (flightItinerary.Passenger[0].FlexDetailsList != null && flightItinerary.Passenger[0].FlexDetailsList.Count > 0)
                            {
                                //Add heading Reporting Fields
                                flexFields += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                                flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                flexFields += "<td colspan=\"1\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                flexFields += "<table class=\"main-header\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                                flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                flexFields += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                flexFields += "<strong style=\"font-weight: 600\">Reporting Fields:</strong>";
                                flexFields += "</td></tr></tbody></table></td></tr></tbody></table>";
                            }
                            listFlightItinerary.ForEach(x =>
                            {
                                string approvalStatus = FlightPolicy.GetTripApprovalStatus(x.FlightId, 1);
                                FlightPolicy policy = new FlightPolicy();
                                policy.Flightid = x.FlightId;
                                policy.GetPolicyByFlightId();
                                if (counter == 0)
                                    header = "<div style='background: #f8f8f8;font-size: 16px;padding: 5px;border: 2px solid #60c231;'><div style='-webkit-box-flex:0;flex:0 0 100%;max-width:100%'><div style='-webkit-box-flex:0;flex:0 0 41.66667%;'><span >Employee Selected</span>&nbsp; PNR : " + x.PNR + "</div><div style='-webkit-box-flex:0;flex:0 0 58.33333%;'>Approval Status : <strong style='font-weight: 600'>" + (approvalStatus == "P" ? "Awaiting Approval ⌛" : "Approved ") + "</strong><span>Policy Compliance : " + (policy.IsUnderPolicy ? "Inside Policy" : "Outside Policy") + "</span></div></div>";
                                else
                                    header = "<br/><div style='background: #f8f8f8;font-size: 16px;padding: 5px;'><div style='-webkit-box-flex:0;flex:0 0 100%;max-width:100%'><div style='-webkit-box-flex:0;flex:0 0 41.66667%;'><span >Option " + counter + "</span>&nbsp; PNR : " + x.PNR + "</div><div style='-webkit-box-flex:0;flex:0 0 58.33333%;'>Approval Status : <strong style='font-weight: 600'>" + (approvalStatus == "P" ? "Awaiting Approval ⌛" : "Approved ") + "</strong><span>Policy Compliance : " + (policy.IsUnderPolicy ? "Inside Policy" : "Outside Policy") + "</span></div></div>";
                                string segments = string.Empty;//Combine all flight details in flightSegments string to send for Employee or Location email, Approver's email
                                flightItinerary = x;
                                if (!string.IsNullOrEmpty(isLocationEmail) && isLocationEmail.ToUpper() == "TRUE")
                                    emailHtmlLocation += SendEmail("L", Settings.LoginInfo.CorporateProfileId, (int)agency.ID, x.FlightId, header, footer, ref flightSegments);
                                else if (string.IsNullOrEmpty(isApprovalMail) || isApprovalMail.ToUpper() == "TRUE")// If not configured OR value is True, then fire Email
                                {
                                    emailHtmlEmployee += SendEmail("E", Settings.LoginInfo.CorporateProfileId, (int)agency.ID, x.FlightId, header, footer, ref flightSegments);
                                    emailHtmlApproval += SendEmail("A", Settings.LoginInfo.CorporateProfileId, (int)agency.ID, x.FlightId, header, footer, ref segments);//To the approver with hierarchy level one               
                                }
                                if (counter == 0)
                                {
                                    //show all flex fields for first booking only
                                    if (flightItinerary.Passenger[0].FlexDetailsList != null && flightItinerary.Passenger[0].FlexDetailsList.Count > 0)
                                    {
                                        flexFields += "<table class=\"bottom-table-style\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\"><tbody>";
                                        flightItinerary.Passenger[0].FlexDetailsList.ForEach(flex =>
                                        {
                                            flexFields += "<tr class=\"table-cnt-inner\" style=\"padding: 0; text-align: left; vertical-align: top\">";
                                            flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flex.FlexLabel}:</td>";
                                            flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flex.FlexData}</td></tr>";
                                        });
                                        flexFields += "</tbody></table>";
                                    }
                                }

                                counter++;
                            });
                            
                            //Add the PNR of the first booking as shown in the screen
                            if (!String.IsNullOrEmpty(flightItinerary.PNR))
                            {
                                flexFields += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                                flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                flexFields += "<td colspan=\"1\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                flexFields += "<table class=\"main-header\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                                flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                flexFields += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                flexFields += "<strong style=\"font-weight: 600\">PNR:</strong>";
                                flexFields += "</td></tr></tbody></table></td></tr></tbody></table>";
                                flexFields += "<table class=\"bottom-table-style\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                                flexFields += "<tr class=\"table-cnt-inner\" style=\"padding: 0; text-align: left; vertical-align: top\">";
                                flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\"> PNR:</td>";
                                flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flightItinerary.PNR}</td></tr>";

                                if (!flightItinerary.IsLCC && flightItinerary.AirLocatorCode != null)
                                {
                                    flexFields += $"<tr><td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">GDS PNR:</td>";
                                    flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flightItinerary.AirLocatorCode}</td></tr>";
                                }
                                flexFields += "</table>";
                            }
                            
                            flexFields = flexFields.Replace("< ", "<").Replace(" >", ">");

                            //Replace the html place holder for flight details with flightSegments html
                            emailHtmlApproval = emailHtmlApproval.Replace("%flightSegments%", flightSegments).Replace("%flexFields%", flexFields);
                            emailHtmlEmployee = emailHtmlEmployee.Replace("%flightSegments%", flightSegments).Replace("%flexFields%", flexFields);
                            emailHtmlLocation = emailHtmlLocation.Replace("%flightSegments%", flightSegments).Replace("%flexFields%", flexFields);

                            subjectApproval = DateTime.Now.ToString("dd-MMM-yyyy") + " Trip Approval Request – " + flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName + " – " + flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName;
                            subjectEmployee = DateTime.Now.ToString("dd-MMM-yyyy") + " Trip Request – " + flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName + " – " + flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName;

                            //if emails to be send for the location only then we will send emails to the location email id's only not for employee and approver
                            if (!string.IsNullOrEmpty(isLocationEmail) && isLocationEmail.ToUpper() == "TRUE" && mailAddresses.Count > 0)
                            {
                                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], mailAddresses.Distinct().ToList(), subjectEmployee, emailHtmlLocation, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                            }
                            else
                            {
                                //Since Employee and location same email content we will send email at once
                                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], mailAddresses.Distinct().ToList(), subjectEmployee, emailHtmlEmployee, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                                //For approver email content will be different so we will send seperately
                                if (approvalMailAddresses.Count > 0 && !string.IsNullOrEmpty(emailHtmlApproval))
                                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], approvalMailAddresses.Distinct().ToList(), subjectApproval, emailHtmlApproval, new Hashtable(), ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                            }
                        }
                        catch (Exception ex) { Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "Failed to Send Optional booking email's for Corporate Trip Id: " + Request["TripId"] + ". Reason: " + ex.ToString(), ""); }
                        /******End:Send EMAIL To  Corporate Employee and to the approvers********/
                    }
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateTripSummaryUI) Page Load Event Error: " + ex.ToString(), "0");
        }

    }

    /// <summary>
    /// 
    /// </summary>
    private void GetItineraries()
    {
        if (Request["TripId"] != null)
        {
            DataTable dtBookings = CorporateProfileExpenseDetails.GetCorporateBookings(Request.QueryString["TripId"]);
            if (dtBookings != null && dtBookings.Rows.Count > 0)
            {
                foreach (DataRow dr in dtBookings.Rows)
                {
                    if (dr["flightId"] != DBNull.Value)
                    {
                        FlightItinerary flightItinerary = new FlightItinerary(Convert.ToInt32(dr["flightId"]));
                        
                        listFlightItinerary.Add(flightItinerary);
                    }

                }
            }
            //Get the policy compliance of the approved trip to display if approved or display first trip policy if rejected
            listFlightItinerary.ForEach(x=> 
            {                
                string approvalStatus = FlightPolicy.GetTripApprovalStatus(x.FlightId, 1);
                if (approvalStatus == "A")
                    flightItinerary = x;                
                else
                    flightItinerary = listFlightItinerary[0];
            });
        }
    }

    public void BuidItinerary()
    {
        AirFare = 0;
        Taxes = 0;
        MealPrice = SeatPref = Baggage = 0;
        MarkUp = 0;
        Discount = 0;
        AsvAmount = 0;
        int flightId = Convert.ToInt32(Request["FlightId"]);
        if (flightId > 0)
        {
            airline = new Airline();
            flightItinerary = new FlightItinerary(flightId);
            booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(flightId));
            booking.AgencyId = flightItinerary.AgencyId;
            agency = new AgentMaster(flightItinerary.AgencyId);
            for (int i = 0; i < flightItinerary.Passenger.Length; i++)
            {
                FlightPassenger pax = flightItinerary.Passenger[i];
                AirFare += pax.Price.PublishedFare;
                Taxes += pax.Price.Tax + pax.Price.Markup;
                if (flightItinerary.FlightBookingSource == BookingSource.TBOAir)
                {
                    Taxes += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
                }
                Baggage += pax.Price.BaggageCharge;
                MealPrice += pax.Price.MealCharge;
                MarkUp += pax.Price.Markup;
                Discount += pax.Price.Discount;
                AsvAmount += pax.Price.AsvAmount;
                SeatPref += pax.Price.SeatPrice;
            }
            if (flightItinerary.Passenger[0].Price.AsvElement == "BF")
            {
                AirFare += AsvAmount;
            }
            else if (flightItinerary.Passenger[0].Price.AsvElement == "TF")
            {
                Taxes += AsvAmount;
            }
        }
    }

    //protected void SendEmail(string toEmpOrApp, string approverStatus, int profileId, int agentId,int flightId)
    protected string SendEmail(string toEmpOrApp, int profileId, int agentId, int flightId,  string header,string footer, ref string flightSegments)
    {
        string emailHtml = string.Empty;
        
        emailTo = toEmpOrApp;
        string approverStatus = string.Empty;
        //int user_corp_profile_id = profileId;        
        string serverPath = Request.Url.Scheme + "://ctb2bstage.cozmotravel.com";
        string logoPath = "";

        if (agentId > 1)
        {
            if (!string.IsNullOrEmpty(new AgentMaster(agentId).ImgFileName))
            {
                logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + new AgentMaster(agentId).ImgFileName;
                agentImageSource = logoPath;
            }
        }
        else
        {
            agentImageSource = serverPath + "/images/logo.jpg";
        }
        //Audit.Add(EventType.Email, Severity.High, 1, "===========3===============", "");
        if (flightId > 0)
        {
            //Audit.Add(EventType.Email, Severity.High, 1, "===========4===============", "");
            detail = new CorporateProfileTripDetails(flightId);
            //Retrieve the current trip status before sending email for employee and approver
            //If in case it is auto approved no need to send email to approver and also show the current trip status in email
            //added product Id as param to fetch the status. 
            approverStatus = FlightPolicy.GetTripApprovalStatus(flightId,(int)ProductType.Flight);
            try
            {
                if (approverStatus == "A")
                {
                    _approvalStatus = "Approved";
                }
                else if (approverStatus == "P")
                {
                    _approvalStatus = "Awaiting Approval";
                }
                else
                {
                    _approvalStatus = "Rejected";
                }
                
                if (toEmpOrApp == "A" && approverStatus == "P")//To the approver, Send email to approver only if the trip status is P (Awaiting Approval)
                {
                    // Audit.Add(EventType.Email, Severity.High, 1, "===========6===============", "");
                    empId = detail.EmpId;
                    empName = detail.EmpName;

                    List<int> listOfAppId = detail.ProfileApproversList.Where(i => i.Hierarchy == 1).Select(i => i.ApproverId).ToList();
                    if (listOfAppId != null && listOfAppId.Count > 0)
                    {
                        foreach (int appId in listOfAppId)
                        {
                            approverNames = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverName).FirstOrDefault();
                            approverId = appId;
                            // Audit.Add(EventType.Email, Severity.High, 1, "===========7===============", "");
                            string appEmail = detail.ProfileApproversList.Where(i => i.ApproverId == appId).Select(i => i.ApproverEmail).FirstOrDefault();
                            if (!string.IsNullOrEmpty(appEmail))
                            {
                                
                                Hashtable hashtable = new Hashtable();
                                string subject = string.Empty;
                                if (header.Contains("Employee"))
                                {
                                    StreamReader reader = new StreamReader(Server.MapPath("~/CorporateEmail-2-Approver.html"));
                                    emailHtml = reader.ReadToEnd();
                                    reader.Close();                                    

                                    FlightPolicy flightPolicy = new FlightPolicy();
                                    List<FlightPolicy> flightPolicies = new List<FlightPolicy>();

                                    flightPolicy.Flightid = flightItinerary.FlightId;
                                    flightPolicy.GetPolicyByFlightId();
                                    flightPolicies.Add(flightPolicy);
                                    subject = DateTime.Now.ToString("dd-MMM-yyyy") + " Trip Approval Request – " + flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName + " – " + flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName;

                                    //string url = Request.IsSecureConnection ? "https://" : "http://" + Request["HTTP_HOST"];
                                    string url = Request.Url.Scheme + "://" + Request["HTTP_HOST"];

                                    //Get the login name of first approver in order approve or reject from email
                                    userLoginName = UserMaster.GetLoginNameForCorpProfile(appId);
                                    
                                    hashtable.Add("requestDate", DateTime.Now.ToString("dd-MMM-yyyy"));
                                    hashtable.Add("employee", flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName);
                                    hashtable.Add("departureCityName", flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName);
                                    hashtable.Add("tripReference", flightItinerary.TripId);
                                    hashtable.Add("rootUrl", url);
                                    hashtable.Add("approverId", approverId);
                                    hashtable.Add("expDetailId", flightId);
                                    hashtable.Add("employeeID", detail.EmpId);
                                    hashtable.Add("appLoginName", GenericStatic.EncryptData(userLoginName));//Assign for approver validation
                                    hashtable.Add("approvalStatus", _approvalStatus);

                                    double deadline = 5;//Default 5 Hours
                                    if (appConfigs.Count > 0)
                                    {
                                        AgentAppConfig config = appConfigs.Find(ac => ac.AppKey == "CorpApprovalDeadline");
                                        if (config != null)                                        
                                            deadline = Convert.ToDouble(config.AppValue);                                        
                                    }
                                    hashtable.Add("approvalDeadline", DateTime.Now.AddHours(Convert.ToDouble(deadline)).ToString("dd-MMM-yyyy hh:mm:ss tt"));//TODO: insert into app config table and assign

                                    if (flightPolicies.Count > 0)
                                    {
                                        CorporateTravelReason travelReason = new CorporateTravelReason(flightPolicies[0].TravelReasonId);
                                        CorporateTravelReason policyReason = new CorporateTravelReason();
                                        if (flightPolicies[0].PolicyReasonId > 0)                                        
                                            policyReason = new CorporateTravelReason(flightPolicies[0].PolicyReasonId);                                        

                                        hashtable.Add("travelReason", travelReason.Description);
                                        hashtable.Add("policyCompliance", flightPolicies[0].IsUnderPolicy ? "Inside Policy" : "Outside Policy - " + flightPolicies[0].PolicyBreakingRules);//Inside or outside policy with policy breaking rule
                                        hashtable.Add("bookingReason", !string.IsNullOrEmpty(policyReason.Description) && !flightPolicies[0].IsUnderPolicy ? "<strong style=\"font - weight: 600\">Violation Reason:</ strong > " + policyReason.Description : "");
                                    }

                                    string approvers = string.Empty;
                                    List<CorpProfileApproval> approvals = CorpProfileApproval.RemoveDuplicatesIterative(detail.ProfileApproversList);

                                    for (int i = 0; i < approvals.Count; i++)
                                    {
                                        if (!string.IsNullOrEmpty(approvals[i].ApproverEmail) && approvals[i].ApproverId != detail.ProfileId)
                                        {
                                            string approverEmail = approvals[i].ApproverEmail;
                                            string approvalStatus = approvals[i].ApprovalStatus;
                                            approvers += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                            approvers += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                            approvers += $"<strong style =\"font-weight: 600\">Approver{i + 1}</strong>";
                                            approvers += "</td>";
                                            approvers += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                            approvers += $"{approverEmail}" + "&nbsp;&nbsp;&nbsp;&nbsp; <span style=\"color: green\">Status: <strong>" + $"{approvalStatus}</strong></span>";
                                            approvers += "</td>";
                                            approvers += "</tr>";
                                        }
                                    }
                                    
                                    hashtable.Add("approvers", approvers);
                                    string fallBackApprover = string.Empty;
                                    try
                                    {
                                        List<CorpProfileApproval> approvalCount = CorpProfileApproval.RemoveDuplicatesIterative(detail.ProfileApproversList);
                                        List<CorpProfileApproval> fallBackApprovers = detail.ProfileApproversList.Except(approvalCount).ToList();
                                        fallBackApprovers = (from r in fallBackApprovers orderby r.Hierarchy select r).ToList();
                                        int counter = 0;
                                        foreach (CorpProfileApproval corpProfile in fallBackApprovers)
                                        {
                                            if (!string.IsNullOrEmpty(corpProfile.ApproverEmail))
                                            {
                                                fallBackApprover += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                                fallBackApprover += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                                fallBackApprover += "<strong style =\"font-weight: 600\">Fall Back Approver " + corpProfile.Hierarchy + " - " + (counter + 1) + "</strong>";
                                                fallBackApprover += "</td><td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                                fallBackApprover += $"{corpProfile.ApproverEmail}" + "&nbsp;&nbsp;&nbsp;&nbsp; <span style=\"color: green\">Status: <strong>" + $"{corpProfile.ApprovalStatus}</strong></span>";
                                                fallBackApprover += "</td></tr>";
                                                counter++;
                                            }
                                        }

                                    }
                                    catch { }
                                    finally { hashtable.Add("fallBackApprover", fallBackApprover); }
                                }

                                #region Flight Segments

                                int segments = flightItinerary.Segments.Length;
                                List<string> meals = new List<string>();
                                List<decimal> mealPrices = new List<decimal>();
                                List<string> baggages = new List<string>();
                                List<decimal> baggagePrices = new List<decimal>();
                                List<string> seats = new List<string>();
                                List<decimal> seatPrices = new List<decimal>();

                                flightItinerary.Passenger.ToList().ForEach(p =>
                                {
                                    string meal = string.Empty, seat = string.Empty, bag = string.Empty;
                                    for (int i = 0; i < segments; i++)
                                    {
                                        meal += meal.Length > 0 ? ", No Meal Booked" : "No Meal Booked";
                                        seat += seat.Length > 0 ? ", No Seat Booked" : "No Seat Booked";
                                        bag += bag.Length > 0 ? ", No Bag" : "No Bag";
                                    }
                                    meals.Add(string.IsNullOrEmpty(p.MealDesc) ? meal : p.MealDesc);
                                    mealPrices.Add(p.Price.MealCharge);
                                    baggages.Add(string.IsNullOrEmpty(p.BaggageCode) ? bag : p.BaggageCode);
                                    baggagePrices.Add(p.Price.BaggageCharge);
                                    seats.Add(string.IsNullOrEmpty(p.Seat.Code) ? seat : p.Seat.Code);
                                    seatPrices.Add(p.Price.SeatPrice);
                                });


                                string airlineName = string.Empty, airlineCode = string.Empty, flightNumber = string.Empty, departureCityName = string.Empty, departureAirportName = string.Empty, departureTerminal = string.Empty;
                                string departureDate = string.Empty, departureWeekDay = string.Empty, departureTime = string.Empty;
                                string arrivalCityName = string.Empty, arrivalDate = string.Empty, arrivalWeekDay = string.Empty, arrivalTime = string.Empty,
                                    arrivalAirportName = string.Empty, arrivalTerminal = string.Empty, Stops = string.Empty, Duration = string.Empty;
                                string Currency = flightItinerary.Passenger[0].Price.Currency;
                                decimal bookingAmount = 0;
                                int decimalPoint = 2;
                                
                                bookingAmount = flightItinerary.Passenger.ToList().Sum(p => p.Price.PublishedFare + p.Price.Tax + p.Price.Markup +
                                p.Price.BaggageCharge + p.Price.OtherCharges + p.Price.HandlingFeeAmount + p.Price.OutputVATAmount +
                                p.Price.MealCharge + p.Price.SeatPrice + p.Price.AsvAmount - p.Price.Discount);
                                
                                bookingAmount = flightItinerary.FlightBookingSource == BookingSource.TBOAir ? Math.Ceiling(bookingAmount) : bookingAmount;
                                decimalPoint = flightItinerary.Passenger[0].Price.DecimalPoint;

                                flightSegments += header;
                                //flightSegments += "< table class='spacer' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%'><tbody><tr style = 'padding: 0; text-align: left; vertical-align: top' >< td height='10px' style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word'>&#xA0;</td></tr></tbody></table>";
                                flightSegments += "<table class='flight-table' style='border-bottom: 1px solid #e4e4e4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%'>";
                                flightSegments += "< tr style = 'padding: 0; text-align: left; vertical-align: top' >";
                                flightSegments += "< th style='Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Airline</th>";
                                flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Departure</th>";
                                flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Arrival</th>";
                                flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Stops</th>";
                                flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Duration</th>";
                                flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Price</th>";
                                flightSegments += "</tr>";

                                
                                for (int i = 0; i < segments; i++)
                                {

                                    FlightInfo flight = flightItinerary.Segments[i];
                                    Airline airline = new Airline();
                                    airline.Load(flight.Airline);
                                    airlineName = airline.AirlineName; airlineCode = flight.Airline; flightNumber = flight.FlightNumber;
                                    departureCityName = flight.Origin.CityName; departureAirportName = flight.Origin.AirportName;
                                    departureTerminal = flight.DepTerminal; departureDate = flight.DepartureTime.ToString("dd MMM yyyy");
                                    departureWeekDay = flight.DepartureTime.DayOfWeek.ToString().Substring(0, 3);
                                    departureTime = flight.DepartureTime.ToString("hh:mm tt");
                                    arrivalCityName = flight.Destination.CityName; arrivalDate = flight.ArrivalTime.ToString("dd MMM yyyy");
                                    arrivalWeekDay = flight.ArrivalTime.DayOfWeek.ToString().Substring(0, 3);
                                    arrivalTime = flight.ArrivalTime.ToString("hh:mm tt");
                                    arrivalAirportName = flight.Destination.CityName; arrivalTerminal = flight.ArrTerminal;

                                    Stops = flight.Stops == 0 ? "Non Stop" : flight.Stops == 1 ? "One Stop" : "Two Stops";
                                    Duration = flight.Duration.ToString();


                                    flightSegments += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    flightSegments += $"< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">{airlineName}<br />{airlineCode} {flightNumber} </td>";
                                    flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    flightSegments += $"< strong >{departureCityName}</ strong >< br />{departureDate}< br /> ({departureWeekDay}),{departureTime},< br /> Airport:{departureAirportName},< br /> Terminal: {departureTerminal}</ td >";
                                    flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    flightSegments += $"< strong >{ arrivalCityName}</ strong >< br />{ arrivalDate}< br /> ({ arrivalWeekDay}),{ arrivalTime},< br /> Airport:{ arrivalAirportName}, Terminal: { arrivalTerminal}</ td >";
                                    flightSegments += $"< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\"> {Stops} </td>";
                                    flightSegments += $"< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\"> {Duration} </td>";
                                    if (i == 0)
                                    {
                                        flightSegments += "<td rowspan =\"" + (flightItinerary.Segments.Length + 5).ToString() + "\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; border-left: 1px solid #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: center; vertical-align: middle; word-wrap: break-word\">";
                                        flightSegments += $"< strong >{Currency}  {bookingAmount.ToString("N" + decimalPoint)}</ strong ></ td >";
                                    }
                                    flightSegments += "</tr>";
                                    flightSegments += "<tr>";
                                    flightSegments += "<td colspan =\"5\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    flightSegments += "<table class=\"table-main-content\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                                    flightSegments += "<tbody>";
                                    flightSegments += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    if (mealPrices.Sum() > 0)
                                    {
                                        for (int j = 0; j < meals.Count; j++)
                                        {
                                            flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                            flightSegments += $"< strong style =\"font-weight: 600\">Meal:</strong>{meals[j]} [{Currency} {mealPrices[j].ToString("N" + decimalPoint)}]</td>";
                                        }
                                    }
                                    flightSegments += "</tr> ";
                                    flightSegments += "< tr style =\"padding: 0; text-align: left; vertical-align: top\">";

                                    if (seatPrices.Sum() > 0)
                                    {
                                        for (int k = 0; k < seats.Count; k++)
                                        {
                                            //Displaying seat segment wise in email.
                                            flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                            flightSegments += "< strong style =\"font-weight: 600\">Seat:</strong>" + seats[k] + $"[{Currency} {seatPrices[k].ToString("N" + decimalPoint)}]</td>";
                                        }
                                    }
                                    flightSegments += "</tr> ";
                                    flightSegments += "<tr style =\"padding: 0; text-align: left; vertical-align: top\">";
                                    if (baggagePrices.Sum() > 0)
                                    {
                                        for (int l = 0; l < baggages.Count; l++)
                                        {
                                            //Displaying baggage as segment wise in email.
                                            flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                            flightSegments += "< strong style =\"font-weight: 600\">Additional Baggage</strong>" + baggages[l] + $"[{Currency} {baggagePrices[l].ToString("N" + decimalPoint)}]</td>";
                                        }
                                    }
                                    flightSegments += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    flightSegments += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    flightSegments += "<table class=\"spacer\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%\"><tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\"><td height=\"2px\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 2px; font-weight: normal; hyphens: auto; line-height: 2px; margin: 0; mso-line-height-rule: exactly; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">&#xA0;</td></tr></tbody></table>";
                                    flightSegments += "</td></tr><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    flightSegments += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    flightSegments += "<table class=\"spacer\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%\"><tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\"><td height=\"10px\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">&#xA0;</td></tr></tbody></table>";
                                    flightSegments += "</td></tr></tbody></table></td></tr>";
                                }

                                flightSegments += "</table>";
                                flightSegments += "< table class='spacer' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%'><tbody><tr style = 'padding: 0; text-align: left; vertical-align: top' >< td height='10px' style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word'>&#xA0;</td></tr></tbody></table>";
                                flightSegments += "< table class='table-main-content' width='100%' cellspacing='0' cellpadding='0' border='0' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top'>";
                                flightSegments += "< tbody>";
                                flightSegments += "< tr style = 'padding: 0; text-align: left; vertical-align: top' >";
                                flightSegments += "< td style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word'>";
                                flightSegments += "< table class='table-main-content' width='100%' cellspacing='0' cellpadding='0' border='0' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: bottom'>";
                                flightSegments += "<tr style = 'padding: 0; text-align: left; vertical-align: bottom' >";
                                flightSegments += "< td style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: bottom; word-wrap: break-word' valign='bottom'>";
                                flightSegments += "< strong style = 'font-weight: 600' >Total:</strong>";
                                flightSegments += "<strong style = 'font-size: 14px; font-weight: 600' >  " + Currency + " " + bookingAmount + "</ strong >";
                                flightSegments += "</ td ></ tr ></ tbody ></ table ></ td ></ tr ></ tbody ></ table >";
                                flightSegments += footer;

                                flightSegments = flightSegments.Replace("< ", "<").Replace(" >", ">");

                                #endregion

                                hashtable.Add("Company", new AgentMaster(flightItinerary.AgencyId).Name);
                                
                                //if (header.Contains("Employee"))
                                //{                                    
                                //    if (flightItinerary.Passenger[0].FlexDetailsList != null && flightItinerary.Passenger[0].FlexDetailsList.Count > 0)
                                //    {                                        
                                //        flexFields += "<table class=\"bottom-table-style\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\"><tbody>";
                                //        foreach (FlightFlexDetails flex in flightItinerary.Passenger[0].FlexDetailsList)
                                //        {
                                //            flexFields += "<tr class=\"table-cnt-inner\" style=\"padding: 0; text-align: left; vertical-align: top\">";
                                //            flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flex.FlexLabel}:</td>";
                                //            flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flex.FlexData}</td></tr>";
                                //        }
                                //        flexFields += "</tbody></table>";
                                //    }
                                //}

                                ////Adding PNR to Email

                                ////if (flightItinerary != null)
                                //{
                                //    flexFields += "<table width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                                //    flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                //    flexFields += "<td colspan=\"1\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 13px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                //    flexFields += "<table class=\"main-header\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"4\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                                //    flexFields += "<tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                //    flexFields += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #e4e4e4; border: 1px solid #d0cfcf; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                //    flexFields += "<strong style=\"font-weight: 600\">PNR:</strong>";
                                //    flexFields += "</td></tr></tbody></table></td></tr></tbody></table>";
                                //    flexFields += "<table class=\"bottom-table-style\" width=\"100%\" border=\"0\" cellspacing=\"0\" cellpadding=\"2\" style=\"background-color: #fff; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";

                                //    flexFields += "<tr class=\"table-cnt-inner\" style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    
                                //    flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\"> PNR:</td>";
                                //    flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flightItinerary.PNR}</td></tr>";

                                //    if(flightItinerary.IsLCC && flightItinerary.AirLocatorCode != null)
                                //    {
                                //         flexFields += $"<tr><td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">GDS PNR:</td>";
                                //         flexFields += $"<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #fff; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 6px 8px; text-align: left; vertical-align: top; word-wrap: break-word\">{flightItinerary.AirLocatorCode}</td></tr>";

                                //    }
                                    
                                //    flexFields += "</table>";
                                //}
                                //hashtable.Add("flexFields", flexFields);
                                hashtable.Add("heading", header);
                                hashtable.Add("footer", footer);

                                // Audit.Add(EventType.Email, Severity.High, 1, "===========9===============", "");
                                //CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, emailHtml, hashtable, ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                                foreach (DictionaryEntry de in hashtable)
                                {
                                    string keyString = "%" + de.Key.ToString() + "%";
                                    if (de.Value != null)
                                    {
                                        emailHtml = emailHtml.Replace(keyString, de.Value.ToString());
                                    }
                                    else
                                    {
                                        emailHtml = emailHtml.Replace(keyString, " ");
                                    }
                                }
                                //toArray.Clear();
                                approverNames = string.Empty;
                                // Audit.Add(EventType.Email, Severity.High, 1, "===========10===============", "");
                            }
                        }
                    }
                }
                else if (toEmpOrApp == "E" || toEmpOrApp == "L") //To the employee
                {
                    
                    // Audit.Add(EventType.Email, Severity.High, 1, "===========12===============", "");
                    empId = detail.EmpId;
                    empName = detail.EmpName;
                    // System.IO.StringWriter sw = new System.IO.StringWriter();
                    // HtmlTextWriter htw = new HtmlTextWriter(sw);
                    //// Audit.Add(EventType.Email, Severity.High, 1, "===========13===============", "");
                    // EmailDivEmployee.RenderControl(htw);
                    // string myPageHTML = string.Empty;
                    // myPageHTML = sw.ToString();
                    // myPageHTML = myPageHTML.Replace("none", "block");
                    // string subject = "Trip Approval Request";

                    Hashtable hashtable = new Hashtable();
                    string subject = string.Empty;

                    if (header.Contains("Employee"))
                    {
                        StreamReader reader = new StreamReader(Server.MapPath("~/CorporateEmail-1-Traveller.html"));
                        emailHtml = reader.ReadToEnd();
                        reader.Close();


                        FlightPolicy flightPolicy = new FlightPolicy();

                        List<FlightPolicy> flightPolicies = new List<FlightPolicy>();

                        flightPolicy.Flightid = flightItinerary.FlightId;
                        flightPolicy.GetPolicyByFlightId();
                        flightPolicies.Add(flightPolicy);
                        subject = DateTime.Now.ToString("dd-MMM-yyyy") + " Trip Request – " + flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName + " – " + flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName;


                        AgentAppConfig appConfig = new AgentAppConfig();
                        appConfig.AgentID = (int)agency.ID;
                        appConfig.ProductID = 1;
                        List<AgentAppConfig> appConfigs = appConfig.GetConfigData();


                        hashtable.Add("requestDate", DateTime.Now.ToString("dd-MMM-yyyy"));
                        hashtable.Add("employee", flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName);
                        hashtable.Add("departureCityName", flightItinerary.Segments.ToList().FindLast(f => f.Group == 0).Destination.CityName);
                        hashtable.Add("tripReference", flightItinerary.TripId);
                        hashtable.Add("employeeID", detail.EmpId);
                        hashtable.Add("approvalStatus", _approvalStatus);

                        double deadline = 5;//Default 5 Hours
                        if (appConfigs.Count > 0)
                        {
                            AgentAppConfig config = appConfigs.Find(ac => ac.AppKey == "CorpApprovalDeadline");
                            if (config != null)
                            {
                                deadline = Convert.ToDouble(config.AppValue);
                            }
                        }
                        hashtable.Add("approvalDeadline", DateTime.Now.AddHours(Convert.ToDouble(deadline)).ToString("dd-MMM-yyyy hh:mm:ss tt"));//TODO: insert into app config table and assign

                        if (flightPolicies.Count > 0)
                        {
                            CorporateTravelReason travelReason = new CorporateTravelReason(flightPolicies[0].TravelReasonId);
                            CorporateTravelReason policyReason = new CorporateTravelReason();
                            if (flightPolicies[0].PolicyReasonId > 0)
                            {
                                policyReason = new CorporateTravelReason(flightPolicies[0].PolicyReasonId);
                            }

                            hashtable.Add("travelReason", travelReason.Description);
                            hashtable.Add("policyCompliance", flightPolicies[0].IsUnderPolicy ? "Inside Policy" : "Outside Policy - " + flightPolicies[0].PolicyBreakingRules);//Inside or outside policy with policy breaking rule
                            hashtable.Add("bookingReason", !string.IsNullOrEmpty(policyReason.Description) && !flightPolicies[0].IsUnderPolicy ? "<strong style=\"font - weight: 600\">Violation Reason:</ strong > " + policyReason.Description : "");
                        }

                        string approvers = string.Empty;
                        List<CorpProfileApproval> approvals = CorpProfileApproval.RemoveDuplicatesIterative(detail.ProfileApproversList);
                        //if (approvalsCount > 0)
                        {
                            for (int i = 0; i < approvals.Count; i++)
                            {
                                if (!string.IsNullOrEmpty(approvals[i].ApproverEmail) && approvals[i].ApproverId != detail.ProfileId)
                                {
                                    string approverEmail = approvals[i].ApproverEmail;
                                    string approvalStatus = approvals[i].ApprovalStatus;
                                    approvers += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    approvers += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    approvers += $"<strong style =\"font-weight: 600\">Approver{i + 1}</strong>";
                                    approvers += "</td>";
                                    approvers += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    approvers += $"{approverEmail}" + "&nbsp;&nbsp;&nbsp;&nbsp; <span style=\"color: green\">Status: <strong>" + $"{approvalStatus}</strong></span>";
                                    approvers += "</td>";
                                    approvers += "</tr>";
                                }
                            }
                        }
                        hashtable.Add("approvers", approvers);
                        string fallBackApprover = string.Empty;
                        try
                        {
                            List<CorpProfileApproval> approvalCount = CorpProfileApproval.RemoveDuplicatesIterative(detail.ProfileApproversList);
                            List<CorpProfileApproval> fallBackApprovers = detail.ProfileApproversList.Except(approvalCount).ToList();
                            fallBackApprovers = (from r in fallBackApprovers orderby r.Hierarchy select r).ToList();
                            int counter = 0;
                            foreach (CorpProfileApproval corpProfile in fallBackApprovers)
                            {
                                if (!string.IsNullOrEmpty(corpProfile.ApproverEmail))
                                {
                                    fallBackApprover += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                                    fallBackApprover += "<td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    fallBackApprover += "<strong style =\"font-weight: 600\">Fall Back Approver " + corpProfile.Hierarchy + " - " + (counter + 1) + "</strong>";
                                    fallBackApprover += "</td><td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                    fallBackApprover += $"{corpProfile.ApproverEmail}" + "&nbsp;&nbsp;&nbsp;&nbsp; <span style=\"color: green\">Status: <strong>" + $"{corpProfile.ApprovalStatus}</strong></span>";
                                    fallBackApprover += "</td></tr>";
                                    counter++;
                                }
                            }

                        }
                        catch { }
                        finally { hashtable.Add("fallBackApprover", fallBackApprover); }
                    }

                    #region Flight Segments

                    int segments = flightItinerary.Segments.Length;
                    List<string> meals = new List<string>();
                    List<decimal> mealPrices = new List<decimal>();
                    List<string> baggages = new List<string>();
                    List<decimal> baggagePrices = new List<decimal>();
                    List<string> seats = new List<string>();
                    List<decimal> seatPrices = new List<decimal>();

                    flightItinerary.Passenger.ToList().ForEach(p =>
                    {
                        string meal = string.Empty, seat = string.Empty, bag = string.Empty;
                        for (int i = 0; i < segments; i++)
                        {
                            meal += meal.Length > 0 ? ", No Meal Booked" : "No Meal Booked";
                            seat += seat.Length > 0 ? ", No Seat Booked" : "No Seat Booked";
                            bag += bag.Length > 0 ? ", No Bag" : "No Bag";
                        }
                        meals.Add(string.IsNullOrEmpty(p.MealDesc) ? meal : p.MealDesc);
                        mealPrices.Add(p.Price.MealCharge);
                        baggages.Add(string.IsNullOrEmpty(p.BaggageCode) ? bag : p.BaggageCode);
                        baggagePrices.Add(p.Price.BaggageCharge);
                        seats.Add(string.IsNullOrEmpty(p.Seat.Code) ? seat : p.Seat.Code);
                        seatPrices.Add(p.Price.SeatPrice);
                    });


                    string airlineName = string.Empty, airlineCode = string.Empty, flightNumber = string.Empty, departureCityName = string.Empty, departureAirportName = string.Empty, departureTerminal = string.Empty;
                    string departureDate = string.Empty, departureWeekDay = string.Empty, departureTime = string.Empty;
                    string arrivalCityName = string.Empty, arrivalDate = string.Empty, arrivalWeekDay = string.Empty, arrivalTime = string.Empty,
                        arrivalAirportName = string.Empty, arrivalTerminal = string.Empty, Stops = string.Empty, Duration = string.Empty;
                    string Currency = flightItinerary.Passenger[0].Price.Currency;
                    decimal bookingAmount = 0;
                    int decimalPoint = 2;

                    bookingAmount = flightItinerary.Passenger.ToList().Sum(p => p.Price.PublishedFare + p.Price.Tax + p.Price.Markup +
                    p.Price.BaggageCharge + p.Price.OtherCharges + p.Price.HandlingFeeAmount + p.Price.OutputVATAmount +
                    p.Price.MealCharge + p.Price.SeatPrice + p.Price.AsvAmount - p.Price.Discount);

                    bookingAmount = flightItinerary.FlightBookingSource == BookingSource.TBOAir ? Math.Ceiling(bookingAmount) : bookingAmount;
                    decimalPoint = flightItinerary.Passenger[0].Price.DecimalPoint;

                    flightSegments += header;
                    flightSegments += "< table class='spacer' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%'><tbody><tr style = 'padding: 0; text-align: left; vertical-align: top' >< td height='10px' style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word'>&#xA0;</td></tr></tbody></table>";
                    flightSegments += "<table class='flight-table' style='border-bottom: 1px solid #e4e4e4; border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%'>";
                    flightSegments += "< tr style = 'padding: 0; text-align: left; vertical-align: top' >";
                    flightSegments += "< th style='Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Airline</th>";
                    flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Departure</th>";
                    flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Arrival</th>";
                    flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Stops</th>";
                    flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Duration</th>";
                    flightSegments += "< th style = 'Margin: 0; background-color: #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left'>Price</th>";
                    flightSegments += "</tr>";


                    for (int i = 0; i < segments; i++)
                    {

                        FlightInfo flight = flightItinerary.Segments[i];
                        Airline airline = new Airline();
                        airline.Load(flight.Airline);
                        airlineName = airline.AirlineName; airlineCode = flight.Airline; flightNumber = flight.FlightNumber;
                        departureCityName = flight.Origin.CityName; departureAirportName = flight.Origin.AirportName;
                        departureTerminal = flight.DepTerminal; departureDate = flight.DepartureTime.ToString("dd MMM yyyy");
                        departureWeekDay = flight.DepartureTime.DayOfWeek.ToString().Substring(0, 3);
                        departureTime = flight.DepartureTime.ToString("hh:mm tt");
                        arrivalCityName = flight.Destination.CityName; arrivalDate = flight.ArrivalTime.ToString("dd MMM yyyy");
                        arrivalWeekDay = flight.ArrivalTime.DayOfWeek.ToString().Substring(0, 3);
                        arrivalTime = flight.ArrivalTime.ToString("hh:mm tt");
                        arrivalAirportName = flight.Destination.CityName; arrivalTerminal = flight.ArrTerminal;

                        Stops = flight.Stops == 0 ? "Non Stop" : flight.Stops == 1 ? "One Stop" : "Two Stops";
                        Duration = flight.Duration.ToString();


                        flightSegments += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                        flightSegments += $"< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">{airlineName}<br />{airlineCode} {flightNumber} </td>";
                        flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                        flightSegments += $"< strong >{departureCityName}</ strong >< br />{departureDate}< br /> ({departureWeekDay}),{departureTime},< br /> Airport:{departureAirportName},< br /> Terminal: {departureTerminal}</ td >";
                        flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                        flightSegments += $"< strong >{ arrivalCityName}</ strong >< br />{ arrivalDate}< br /> ({ arrivalWeekDay}),{ arrivalTime},< br /> Airport:{ arrivalAirportName}, Terminal: { arrivalTerminal}</ td >";
                        flightSegments += $"< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\"> {Stops} </td>";
                        flightSegments += $"< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: left; vertical-align: top; word-wrap: break-word\"> {Duration} </td>";
                        if (i == 0)
                        {
                            flightSegments += "<td rowspan =\"" + (flightItinerary.Segments.Length + 5).ToString() + "\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; background-color: #f7f7f7; border-collapse: collapse !important; border-left: 1px solid #e4e4e4; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 5px 5px; text-align: center; vertical-align: middle; word-wrap: break-word\">";
                            flightSegments += $"< strong >{Currency}  {bookingAmount.ToString("N" + decimalPoint)}</ strong ></ td >";
                        }
                        flightSegments += "</tr>";
                        flightSegments += "<tr>";
                        flightSegments += "<td colspan =\"5\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                        flightSegments += "<table class=\"table-main-content\" width=\"100%\" cellspacing=\"0\" cellpadding=\"0\" border=\"0\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top\">";
                        flightSegments += "<tbody>";
                        flightSegments += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                        if (mealPrices.Sum() > 0)
                        {
                            for (int j = 0; j < meals.Count; j++)
                            {
                                flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                flightSegments += $"< strong style =\"font-weight: 600\">Meal:</strong>{meals[j]} [{Currency} {mealPrices[j].ToString("N" + decimalPoint)}]</td>";
                            }
                        }
                        flightSegments += "</tr> ";
                        flightSegments += "< tr style =\"padding: 0; text-align: left; vertical-align: top\">";

                        if (seatPrices.Sum() > 0)
                        {
                            for (int k = 0; k < seats.Count; k++)
                            {
                                //Displaying seat segment wise in email.
                                flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                flightSegments += "< strong style =\"font-weight: 600\">Seat:</strong>" + seats[k] + $"[{Currency} {seatPrices[k].ToString("N" + decimalPoint)}]</td>";
                            }
                        }
                        flightSegments += "</tr> ";
                        flightSegments += "<tr style =\"padding: 0; text-align: left; vertical-align: top\">";
                        if (baggagePrices.Sum() > 0)
                        {
                            for (int l = 0; l < baggages.Count; l++)
                            {
                                //Displaying baggage as segment wise in email.
                                flightSegments += "< td style =\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                                flightSegments += "< strong style =\"font-weight: 600\">Additional Baggage</strong>" + baggages[l] + $"[{Currency} {baggagePrices[l].ToString("N" + decimalPoint)}]</td>";
                            }
                        }
                        flightSegments += "<tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                        flightSegments += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                        flightSegments += "<table class=\"spacer\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%\"><tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\"><td height=\"2px\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 2px; font-weight: normal; hyphens: auto; line-height: 2px; margin: 0; mso-line-height-rule: exactly; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">&#xA0;</td></tr></tbody></table>";
                        flightSegments += "</td></tr><tr style=\"padding: 0; text-align: left; vertical-align: top\">";
                        flightSegments += "<td style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">";
                        flightSegments += "<table class=\"spacer\" style=\"border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%\"><tbody><tr style=\"padding: 0; text-align: left; vertical-align: top\"><td height=\"10px\" style=\"-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word\">&#xA0;</td></tr></tbody></table>";
                        flightSegments += "</td></tr></tbody></table></td></tr>";
                    }

                    flightSegments += "</table>";
                    flightSegments += "< table class='spacer' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%'><tbody><tr style = 'padding: 0; text-align: left; vertical-align: top' >< td height='10px' style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 10px; font-weight: normal; hyphens: auto; line-height: 10px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word'>&#xA0;</td></tr></tbody></table>";
                    flightSegments += "< table class='table-main-content' width='100%' cellspacing='0' cellpadding='0' border='0' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top'>";
                    flightSegments += "< tbody>";
                    flightSegments += "< tr style = 'padding: 0; text-align: left; vertical-align: top' >";
                    flightSegments += "< td style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: top; word-wrap: break-word'>";
                    flightSegments += "< table class='table-main-content' width='100%' cellspacing='0' cellpadding='0' border='0' style='border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: bottom'>";
                    flightSegments += "<tr style = 'padding: 0; text-align: left; vertical-align: bottom' >";
                    flightSegments += "< td style='-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #0a0a0a; font-family: 'Open Sans', Arial, sans-serif; font-size: 12px; font-weight: normal; hyphens: auto; line-height: 1.3; margin: 0; padding: 2px 7px; text-align: left; vertical-align: bottom; word-wrap: break-word' valign='bottom'>";
                    flightSegments += "< strong style = 'font-weight: 600' >Total:</strong>";
                    flightSegments += "<strong style = 'font-size: 14px; font-weight: 600' >  " + Currency + " " + bookingAmount + "</ strong >";
                    flightSegments += "</ td ></ tr ></ tbody ></ table ></ td ></ tr ></ tbody ></ table >";
                    flightSegments += footer;

                    flightSegments = flightSegments.Replace("< ", "<").Replace(" >", ">");
                   
                    #endregion

                    hashtable.Add("Company", new AgentMaster(flightItinerary.AgencyId).Name);



                    //hashtable.Add("flexFields", flexFields);
                    hashtable.Add("heading", header);
                    hashtable.Add("footer", footer);

                    // Audit.Add(EventType.Email, Severity.High, 1, "===========14===============", "");
                    //if (toArray != null && toArray.Count > 0)
                    {
                        //CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, emailHtml, hashtable, ConfigurationManager.AppSettings["CORPORATE_BCC_MAIL"]);
                        foreach (DictionaryEntry de in hashtable)
                        {
                            string keyString = "%" + de.Key.ToString() + "%";
                            if (de.Value != null)
                            {
                                emailHtml = emailHtml.Replace(keyString, de.Value.ToString());
                            }
                            else
                            {
                                emailHtml = emailHtml.Replace(keyString, " ");
                            }
                        }

                    }
                    // Audit.Add(EventType.Email, Severity.High, 1, "===========15===============", "");
                    //toArray.Clear();
                    approverNames = string.Empty;
                    //  Audit.Add(EventType.Email, Severity.High, 1, "===========16===============", "");
                }
            }
            catch (Exception ex)
            {
                throw ex;
                //Audit.Add(EventType.Email, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateTripAnalysisUI.aspx)Failed to Send Email For Employee and Approvers: Reason - " + ex.ToString(), Request["REMOTE_ADDR"]);
            }            
        }
        return emailHtml;
    }

    #region Corporate Bookings Block
    /// <summary>
    /// Initiates the optional/additional bookings for corporate profile
    /// </summary>
    protected void DoCorporateBookings(object LoginInfo)
    {
        try
        {
            LoginInfo loginInfo = LoginInfo as LoginInfo;
            if (Session["Result"] != null && Session["FlightRequest"] != null && Session["TripId"] != null && Session["CriteriaItineraries"] != null && Session["sessionId"] != null && Session["CorpBookingResponses"] != null)
            {
                SearchRequest request = null;

                request = Session["FlightRequest"] as SearchRequest;

                FlightItinerary itinerary = null;
                //HOLD remaining TWO criteria results for Corporate profile
                //TODO: Ziya
                if (loginInfo.IsCorporate == "Y" && request.CorporateTravelProfileId != 0 && request.AppliedPolicy)
                {
                    BookingResponse corpBookingResponse = new BookingResponse();
                    Dictionary<FlightItinerary, BookingResponse> BookingResponses = Session["CorpBookingResponses"] as Dictionary<FlightItinerary, BookingResponse>;

                    foreach (KeyValuePair<FlightItinerary, BookingResponse> pair in BookingResponses)
                    {
                        itinerary = pair.Key;
                        break;
                    }
                    agency = new AgentMaster(loginInfo.AgentId);
                    agencyId = loginInfo.AgentId;

                    CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine(Session["sessionId"].ToString());

                    if (Session["CriteriaItineraries"] != null)
                    {
                        List<FlightItinerary> CriteriaItineries = Session["CriteriaItineraries"] as List<FlightItinerary>;
                        //Audit.Add(EventType.Exception, Severity.High, 0, "(CorpBooking)Total number of CriteriaItineraries Found " + (CriteriaItineries.Count) + " : " + flightItinerary.FlightBookingSource.ToString(), "");
                        //Dictionary<string, WaitCallback> listOfThreads = new Dictionary<string, WaitCallback>();
                        ////Book other optional bookings simultaneously

                        //listOfThreads.Add("0", new WaitCallback(MakeCorporateBookings));
                        //listOfThreads.Add("1", new WaitCallback(MakeCorporateBookings));
                        //List<AutoResetEvent> Events = new List<AutoResetEvent>();
                        int i = 0;
                        foreach (FlightItinerary fi in CriteriaItineries)
                        {
                            FlightItinerary Itinerary = fi;
                            //Audit.Add(EventType.Exception, Severity.High, 0, "(CorpBooking)Additional Booking Source " + (i + 1) + " : " + Itinerary.FlightBookingSource.ToString(), "");
                            if (!Itinerary.IsLCC)
                            {
                                if (Itinerary.FlightBookingSource == BookingSource.AirArabia)
                                {
                                    //CalculateBaggageFare(Itinerary, i);
                                }
                                Itinerary.TripId = Session["TripId"].ToString();//Set the trip id for remaining flights
                                //Audit.Add(EventType.Book, Severity.Normal, (int)loginInfo.UserID, "(CorpBooking)Addtional Booking " + (i + 1) + " trip Id assigned : " + Itinerary.TripId, "");
                                if (Itinerary.FlightBookingSource != BookingSource.AirArabia)
                                {
                                    //List<object> Objects = new List<object>();
                                    //Objects.Add(Itinerary);//Add Itinerary
                                    //Objects.Add(mse);//Add MSE
                                    //Objects.Add(i);//Add ThreadPool Index
                                    //Objects.Add(BookingResponses);                                    
                                    //Objects.Add(loginInfo);
                                    //ThreadPool.QueueUserWorkItem(listOfThreads[i.ToString()], Objects);//Pass Object collection as Paramter to MakeCorporateBookings(...)                                    
                                    //Audit.Add(EventType.Book, Severity.Normal, (int)loginInfo.UserID, "(CorpBooking)Addtional Booking " + (i + 1) + " thread initiated", "");
                                    //AutoResetEvent aevent = new AutoResetEvent(false);
                                    //Events.Add(aevent);
                                    mse.SettingsLoginInfo = loginInfo;
                                    if (itinerary.FlightPolicy != null)
                                        Itinerary.FlightPolicy.SelectedTrip = false;
                                    
                                    corpBookingResponse = mse.Book(ref Itinerary, agencyId, (Itinerary.IsLCC || Request.QueryString["ticketing"] != null && Request.QueryString["ticketing"] == "Yes" ? BookingStatus.Ticketed : BookingStatus.Hold), new UserMaster(Itinerary.CreatedBy), true, "");
                                    BookingResponses.Add(Itinerary, corpBookingResponse);
                                    listFlightItinerary.Add(Itinerary);
                                }
                                else
                                {
                                    corpBookingResponse = mse.Book(ref Itinerary, agencyId, (Itinerary.IsLCC ? BookingStatus.Ticketed : BookingStatus.Hold), new UserMaster(Itinerary.CreatedBy), true, "");
                                    BookingResponses.Add(Itinerary, corpBookingResponse);
                                    listFlightItinerary.Add(Itinerary);
                                }
                                i++;//increment counter only when result is not LCC
                            }
                        }






                        //try
                        //{
                        //    if (Events.Count > 0)
                        //    {
                        //        BookingEvents = Events.ToArray();
                        //    }
                        //    if (i != 0 && Events.Count > 0)
                        //    {
                        //        //If any thread takes more than 2 minutes stop the thread
                        //        if (!WaitHandle.WaitAll(BookingEvents, new TimeSpan(0, 1, 0), true))
                        //        {
                        //            //TODO: audit which thread is timed out                
                        //        }
                        //    }
                        //}
                        //catch { }
                    }


                    isBookingSuccess = true;//to send HOLD emails

                    string EmailBody = string.Empty, subject = string.Empty;

                    //Send Corporate Emails for successful bookings
                    foreach (KeyValuePair<FlightItinerary, BookingResponse> pair in BookingResponses)
                    {
                        //If booking is success send email
                        if (pair.Value.Status == BookingResponseStatus.Successful)
                        {
                            //Combine all 3 email bodies in one
                            EmailBody += SendFlightTicketEmail(pair.Key, loginInfo);
                            try
                            {
                                if (pair.Key.FareRules[0].FareRuleDetail != null && itinerary.FlightBookingSource == BookingSource.UAPI)
                                {
                                    //Append the Penalties Fare Rule to the Email Body
                                    EmailBody += "<br/><hr/>Fare Rules :" + pair.Key.FareRules[0].FareRuleDetail.Substring(0, pair.Key.FareRules[0].FareRuleDetail.IndexOf("/p><p") + 3) + "<hr/>";
                                }
                            }
                            catch { }
                            //Combine PNR's
                            if (subject.Length > 0)
                            {
                                subject += "," + pair.Key.PNR;
                            }
                            else
                            {
                                subject = pair.Key.PNR;
                            }

                            
                            emailCounter++;//Increment the counter for hiding the Agent details in Email body
                        }
                        else
                        {
                            //TODO: Failure email
                        }
                    }

                    /*   try
                       {
                           //List<string> toArray = new List<string>();
                           //toArray.Add(itinerary.Passenger[0].Email);
                           //if (!string.IsNullOrEmpty(loginInfo.AgentEmail))
                           //{
                           //    toArray.Add(loginInfo.AgentEmail);
                           //}
                           //if (!string.IsNullOrEmpty(loginInfo.Email))
                           //{
                           //    toArray.Add(loginInfo.Email);
                           //}

                           //Send Corporate email
                           // CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Booking Confirmation - " + subject, EmailBody, new Hashtable());
                           if (listItinerary != null && listItinerary.Count > 0)
                           {
                               SendEmail("A", "P", loginInfo.CorporateProfileId, loginInfo.AgentId);//To the approver with hierarchy level one
                               if (Session["selFlightId"] != null)
                               {
                                   Itinerary = new Itinerary(Convert.ToInt32(Session["selFlightId"]));
                                   SendEmail("E", "P", loginInfo.CorporateProfileId, loginInfo.AgentId); //To the employee

                               }
                           }
                       }
                       catch (Exception ex)
                       {
                           Audit.Add(EventType.Email, Severity.High, 1, "(CorpBooking)Failed to Send Corporate Emails. Reason : " + ex.ToString(), "");
                       }
                       finally
                       {


                           //try
                           //{

                           //    //if (Request.QueryString["flightId"] != null)
                           //    //{
                           //    //    Response.Redirect("CorporateTripSummary.aspx?flightId=" + Request.QueryString["flightId"] + "&status=P");
                           //    //}
                           //}

                           //catch { }


                           Session["CorpBookingResponses"] = null;
                           Session["CriteriaItineraries"] = null;
                           Session["FlightItinerary"] = null;
                           Session["TripId"] = null;
                           Session["selFlightId"] = null;
                           Session.Remove("CriteriaResults");



                       }*/
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Book, Severity.High, 1, "(CorpBooking)Failed to book Cheapest & Shortest Flights. Reason : " + ex.ToString(), "");
        }
        finally
        {

            Session["CorpBookingResponses"] = null;
            Session["CriteriaItineraries"] = null;
            Session["FlightItinerary"] = null;
            Session["TripId"] = null;
            //Session["selFlightId"] = null;
            Session.Remove("CriteriaResults");
        }


    }

    /// <summary>
    /// Retrieves Baggage fare for Air Arabia
    /// </summary>
    /// <param name="itinerary"></param>
    /// <param name="index"></param>
    protected void CalculateBaggageFare(FlightItinerary itinerary, int index)
    {
        try
        {
            SearchResult result = (Session["CriteriaResults"] as List<SearchResult>)[index];
            string sessionId = Session["sessionId"].ToString();
            SearchRequest request = Session["FlightRequest"] as SearchRequest;
            if (result.ResultBookingSource == BookingSource.AirArabia)
            {
                //Hold booking is not allowed for G9

                string[] RPH = new string[result.Flights.Length];
                List<string> rph = new List<string>();
                for (int i = 0; i < itinerary.Segments.Length; i++)
                {
                    //for (int j = 0; j < result.Flights[i].Length; j++)
                    {
                        //RPH[i] = result.Flights[i][j].FareInfoKey.ToString() + "|" + result.Flights[i][j].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + result.Flights[i][j].Airline + result.Flights[i][j].FlightNumber;
                        rph.Add(itinerary.Segments[i].UapiDepartureTime.ToString() + "|" + itinerary.Segments[i].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + itinerary.Segments[i].Airline + itinerary.Segments[i].FlightNumber);
                        //rph.Add(result.Flights[i][j].UapiDepartureTime.ToString());
                    }
                }
                RPH = rph.ToArray();
                int arraySize = 0;

                //if (itinerary.Segments.Length <= 1)
                //{
                //    arraySize = (itinerary.Passenger.Length - request.InfantCount) ;
                //}
                //else
                {
                    arraySize = (itinerary.Passenger.Length - request.InfantCount) * itinerary.Segments.Length;
                }

                string[] paxBaggages = new string[arraySize];
                int count = 0;


                for (int i = 0; i < (itinerary.Passenger.Length - request.InfantCount); i++)
                {
                    if (itinerary.Passenger[i].Type != PassengerType.Infant)
                    {
                        string travelerRPH = "";
                        if (itinerary.Passenger[i].Type == PassengerType.Adult || itinerary.Passenger[i].Type == PassengerType.Senior)
                        {
                            travelerRPH = "A" + (i + 1);
                        }
                        else if (itinerary.Passenger[i].Type == PassengerType.Child)
                        {
                            travelerRPH = "C" + (i + 1);
                        }

                        string bagCode = itinerary.Passenger[i].BaggageCode;
                        for (int k = 0; k < itinerary.Segments.Length; k++)
                        {
                            paxBaggages[count] = bagCode.Split(',')[k].Trim() + "|" + travelerRPH + "|" + itinerary.Segments[k].UapiDepartureTime.ToString() + "|" + itinerary.Segments[k].DepartureTime.ToString("yyyy-MM-dd'T'HH:mm:ss") + "|" + itinerary.Segments[k].Airline + itinerary.Segments[k].FlightNumber;
                            count++;
                        }
                    }
                }

                CT.BookingEngine.GDS.AirArabia aaObj = new CT.BookingEngine.GDS.AirArabia();
                //if (Settings.LoginInfo.IsOnBehalfOfAgent)
                //{
                //    aaObj.UserName = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].UserID;
                //    aaObj.Password = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].Password;
                //    aaObj.Code = Settings.LoginInfo.OnBehalfAgentSourceCredentials["G9"].HAP;
                //    aaObj.AgentCurrency = Settings.LoginInfo.OnBehalfAgentCurrency;
                //    aaObj.ExchangeRates = Settings.LoginInfo.OnBehalfAgentExchangeRates;
                //}
                //else
                //{
                //    aaObj.UserName = Settings.LoginInfo.AgentSourceCredentials["G9"].UserID;
                //    aaObj.Password = Settings.LoginInfo.AgentSourceCredentials["G9"].Password;
                //    aaObj.Code = Settings.LoginInfo.AgentSourceCredentials["G9"].HAP;
                //    aaObj.AgentCurrency = Settings.LoginInfo.Currency;
                //    aaObj.ExchangeRates = Settings.LoginInfo.AgentExchangeRates;
                //}
                //CT.BookingEngine.GDS.AirArabia.SessionData data = (CT.BookingEngine.GDS.AirArabia.SessionData)Basket.BookingSession[sessionId][result.Airline];
                string fareXml = "";
                System.Net.CookieContainer cookie = new System.Net.CookieContainer();
                //aaObj.AppUserId = Settings.LoginInfo.UserID.ToString();
                aaObj.SessionID = Session["sessionId"].ToString();
                Dictionary<string, decimal> PaxBagFares = new Dictionary<string, decimal>();
                //Modified by Lokesh on 4-April-2018
                //This is for Only G9 Air Source
                //Need to pass the state code(mandatory) and taxRegNo(Not mandatory) only if the customer is travelling from India.
                //string[] baggageFare = aaObj.GetBaggageFare(ref result, request, ref data, ref RPH, ref fareXml, sessionId, ref cookie, ref paxBaggages, itinerary.Passenger[0].GSTStateCode, itinerary.Passenger[0].GSTTaxRegNo, ref PaxBagFares);
                //data.SelectedResultInfo = fareXml;
                // TodO ziya: update itineray according to last price quote
                // foreach (FlightPassenger pax in itinerary.Passenger)
                //  {
                //     for (int i = 0; i < result.FareBreakdown.Length; i++)
                //     {
                //         if (pax.Type == result.FareBreakdown[i].PassengerType && result.FareBreakdown[i].SupplierFare > 0)
                //        {
                //            pax.Price.SupplierPrice = (decimal)result.FareBreakdown[i].SupplierFare / result.FareBreakdown[i].PassengerCount;
                //        }
                //     }
                //  }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorpBooking)Failed to get Baggage price for G9. Reason " + ex.ToString(), "");
        }
    }

    /// <summary>
    /// Makes bookings for optional results in case of Corporate profile
    /// </summary>
    protected void MakeCorporateBookings(object collection)
    {
        FlightItinerary itinerary = null;
        int eventIndex = 0;
        try
        {
            List<object> BookingObjects = collection as List<object>;
            itinerary = BookingObjects[0] as FlightItinerary;

            CT.MetaSearchEngine.MetaSearchEngine mse = BookingObjects[1] as CT.MetaSearchEngine.MetaSearchEngine;
            eventIndex = Convert.ToInt32(BookingObjects[2]);
            mse.SettingsLoginInfo = BookingObjects[4] as LoginInfo;
            //Audit.Add(EventType.Book, Severity.Normal, (int)mse.SettingsLoginInfo.UserID, "(CorpBooking)Addtional Booking " + (eventIndex + 1) + " before going to MetaSearchEngine", "");
            BookingResponse corpBookingResponse = mse.Book(ref itinerary, agencyId, (itinerary.IsLCC ? BookingStatus.Ticketed : BookingStatus.Hold), new UserMaster(itinerary.CreatedBy), true, "10.200.44.26");
            // Audit.Add(EventType.Book, Severity.Normal, (int)mse.SettingsLoginInfo.UserID, "(CorpBooking)Addtional Booking " + (eventIndex + 1) + " after returning from MetaSearchEngine. Booking Status : " + corpBookingResponse.Status.ToString(), "");
            Dictionary<FlightItinerary, BookingResponse> BookingResponses = BookingObjects[3] as Dictionary<FlightItinerary, BookingResponse>;
            //Add the booked response along with Itinerary for sending emails
            BookingResponses.Add(itinerary, corpBookingResponse);

            BookingEvents[eventIndex].Set();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorpBooking)Failed to Book Corporate booking - Source :" + itinerary.FlightBookingSource + ". Reason : " + ex.ToString(), "");
            BookingEvents[eventIndex].Set();
        }
    }

    /// <summary>
    /// Bind the Booking information to the email div id='EmailDiv'
    /// </summary>
    /// <param name="FlightId"></param>
    protected void BindTicketInfo(int FlightId)
    {
        try
        {
            if (FlightId > 0)
            {
                flightItinerary = new FlightItinerary(FlightId);
                agency = new AgentMaster(flightItinerary.AgencyId);
                booking = new BookingDetail(BookingDetail.GetBookingIdByFlightId(FlightId));
                if (booking.Status == BookingStatus.Ticketed)
                {
                    ticketList = Ticket.GetTicketList(FlightId);
                }
            }
            if (flightItinerary.FlightBookingSource == BookingSource.AirArabia || flightItinerary.FlightBookingSource == BookingSource.FlyDubai)
            {
                foreach (FlightPassenger pax in flightItinerary.Passenger)
                {
                    if (inBaggage.Length > 0)
                    {
                        inBaggage += ", " + pax.BaggageCode.Split(',')[0];
                    }
                    else
                    {
                        inBaggage = pax.BaggageCode.Split(',')[0];
                    }
                    if (pax.BaggageCode.Split(',').Length > 1)
                    {
                        if (outBaggage.Length > 0)
                        {
                            outBaggage += ", " + pax.BaggageCode.Split(',')[1];
                        }
                        else
                        {
                            outBaggage = pax.BaggageCode.Split(',')[1];
                        }
                    }
                }
            }
            airline = new Airline();
            airline.Load(flightItinerary.ValidatingAirlineCode);

            if (airline.LogoFile.Trim().Length != 0)
            {
                string[] fileExtension = airline.LogoFile.Split('.');
                AirlineLogoPath = AirlineLogoPath + flightItinerary.ValidatingAirlineCode + "." + fileExtension[fileExtension.Length - 1];
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    /// <summary>
    /// Method to send Corporate booking email
    /// </summary>
    /// <param name="itinerary"></param>
    /// <returns></returns>
    protected string SendFlightTicketEmail(FlightItinerary itinerary, LoginInfo loginInfo)
    {
        string myPageHTML = string.Empty;
        try
        {
            BindTicketInfo(itinerary.FlightId);
            string logoPath = "";
            int agentId = loginInfo.AgentId;
            string serverPath = "http://ctb2bstage.cozmotravel.com";
            if (agentId > 1)
            {
                logoPath = serverPath + "/" + ConfigurationManager.AppSettings["AgentImage"] + new AgentMaster(agentId).ImgFileName;
                imgLogo.ImageUrl = logoPath;
            }
            else
            {
                imgLogo.ImageUrl = serverPath + "/images/logo.jpg";
            }
            System.IO.StringWriter sw = new System.IO.StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            EmailDiv.RenderControl(htw);
            myPageHTML = sw.ToString();
            //myPageHTML = myPageHTML.Replace("\"", "/\"");
            myPageHTML = myPageHTML.Replace("none", "block");

            //System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
            //toArray.Add(itinerary.Passenger[0].Email);

            string subject = "Ticket Confirmation - " + itinerary.PNR;

            //if (ViewState["MailSent"] == null)//If booking in not corporate then send email
            {
                //CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, subject, myPageHTML, new Hashtable());
                //ViewState["MailSent"] = true;
            }
        }

        catch (Exception ex)
        {
            Audit.Add(EventType.Email, Severity.High, (int)loginInfo.UserID, "(CorpBooking)Failed to Send Flight Corporate Booking Email PNR " + itinerary.PNR + ": reason - " + ex.ToString(), "");
        }
        return myPageHTML;
    }


    //public void UpdateSelectedTrip(int flightId)
    //{
    //    try
    //    {
    //        if (flightId > 0)
    //        {
    //            CorporateProfileTripDetails.UpdateSelectedTrip(flightId);
    //        }
    //    }
    //    catch { throw; }
    //}


    #endregion

}
