﻿using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using ReligareInsurance;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class viewbookingforReligareInsurance : CT.Core.ParentPage// System.Web.UI.Page
    {
        protected ReligareHeader religareHeader;
        protected int insId = 0;
        protected CT.TicketReceipt.BusinessLayer.AgentMaster agent;        
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Settings.LoginInfo != null)
                {
                    if (!IsPostBack)
                    {
                        if (Request.QueryString["bookingId"] != null)
                        {
                            insId = Convert.ToInt32(Request.QueryString["bookingId"]);
                            religareHeader = new ReligareHeader();
                            religareHeader.Load(insId);
                            hdnpolicyNo.Value = religareHeader.Policy_no;
                            agent = new CT.TicketReceipt.BusinessLayer.AgentMaster(religareHeader.Agent_id);                              
                         }
                    }
                }
                else
                {
                    Response.Redirect("AbandonSession.aspx");
                }
            }
            catch (Exception ex)
            {
                CT.Core.Audit.Add(CT.Core.EventType.Exception, CT.Core.Severity.High, (int)Settings.LoginInfo.UserID, ex.ToString() + "ViewBookingForInsurance", "0");
            }
        }
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string GetPolicyPDFData(string policyNo)
        {
            string pdfData = string.Empty;
            try
            {
                ReligarePolicyPurchase purchase = new ReligarePolicyPurchase();
                var pdfResponse = purchase.GetPolicyPdf(Convert.ToInt64(policyNo));
                string response = pdfResponse.ToString();
                string[] StreamData = response.Replace("StreamData", "$").Split('$');
                if (StreamData.Length > 1)
                {
                    pdfData = StreamData[1].Replace("&gt;", "").Replace("&lt;/", "").Trim();
                }
                return pdfData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Exception occured while calling the Religare pdf service." + ex.ToString(), "");
            }
            return pdfData;
        }
    }
}
