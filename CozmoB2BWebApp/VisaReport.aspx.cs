﻿#region NameSpace
using System;
using System.Data;
using System.Web.UI;
using CT.TicketReceipt.Common;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using System.IO;
using System.Web.UI.WebControls;
using Visa;
#endregion

public partial class VisaReportGUI : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null)
            {
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.btnExport);
                hdfParam.Value = "1";
                Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");

                if (!IsPostBack)
                {
                    txtFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                    hdfParam.Value = "0";
                    InitialiseControls();
                    LoadQueue();
                    
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "VisaReport Page_Load Event Error Raised:" + ex.ToString(), "");
        }
    }
    private void InitialiseControls()
    {
        try
        {
            BindVisaStatus();
            BindAgents();
            BindVisaTypes();
            BindUser(Settings.LoginInfo.AgentId);
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindAgents()
    {
        try
        {
            DataTable dtAgents = CT.TicketReceipt.BusinessLayer.AgentMaster.GetList(1, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            
            ddlAirAgents.DataSource = dtAgents;
            ddlAirAgents.DataTextField = "agent_name";
            ddlAirAgents.DataValueField = "agent_id";
            ddlAirAgents.DataBind();
            ddlAirAgents.Items.Insert(0,new ListItem("--Select Agent--","0"));

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindVisaTypes()
    {
        try
        {
            DataTable dtVisaTypes =VisaQueue.GetVisaTypeItems("A");


            ddlVisaType.DataSource = dtVisaTypes;
            ddlVisaType.DataTextField = "visaType";
            ddlVisaType.DataValueField = "visaTypeId";
            ddlVisaType.DataBind();
            ddlVisaType.Items.Insert(0, new ListItem("--Select Visa Type--", "0"));

        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            LoadQueue();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "VisaReport btnSearch_Click Event Error Raised:" + ex.ToString(), "");
        }

    }

    private DataTable GetVisaReportList()
    {
        DataTable VisaRecords = new DataTable(); ;

        try
        {

            DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
            int agentId = 0;
            int visatypeId = 0;
            int UserId = 0;
            int VisaStatusId = 0;

            IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
            if (txtFromDate.Text != string.Empty)
            {
                try
                {
                    startDate = Convert.ToDateTime(txtFromDate.Text, provider);
                }
                catch { }
            }
            else
            {
                startDate = DateTime.Now;
            }

            if (txtToDate.Text != string.Empty)
            {
                try
                {
                    endDate = Convert.ToDateTime(Convert.ToDateTime(txtToDate.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
                }
                catch { }
            }
            else
            {
                endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
            }

            if ((Convert.ToInt32(ddlAirAgents.SelectedItem.Value)) != 0)
            {
                agentId = Convert.ToInt32(ddlAirAgents.SelectedItem.Value);
            }

            if ((Convert.ToInt32(ddlVisaType.SelectedItem.Value)) != 0)
            {
                visatypeId = Convert.ToInt32(ddlVisaType.SelectedItem.Value);
            }
            if (ddlVisaStatus.SelectedValue != "Select")
            {
               VisaStatusId = (int)(VisaStatus)Enum.Parse(typeof(VisaStatus), ddlVisaStatus.SelectedValue);
            }

            if (ddlUser.SelectedValue != "Select User")
            {
                UserId = Convert.ToInt32(ddlUser.SelectedValue);
            }
            VisaRecords = VisaQueue.GetVisaReportItems(startDate, endDate, agentId, visatypeId, VisaStatusId, UserId);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return VisaRecords;

    }

    private void LoadQueue()
    {
        try
        {
            DataTable dtrecords = GetVisaReportList();
            if (dtrecords != null)
            {
                gvVisaReport.DataSource = dtrecords;
                gvVisaReport.DataBind();
                if (dtrecords.Rows.Count > 0)
                {
                    btnExport.Visible = true;
                }
                else
                {
                    btnExport.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            string attachment = "attachment; filename=VisaReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dgVisaReportList.AllowPaging = false;
            dgVisaReportList.DataSource = GetVisaReportList();
            dgVisaReportList.DataBind();
            dgVisaReportList.RenderControl(htw);
            Response.Write(sw.ToString());
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "VisaReport btnExport_Click Event Error Raised:" + ex.ToString(), "");

        }
        finally
        {
            Response.End();
        }
    }
    #region added status and user
    private void BindVisaStatus()
    {
        try
        {
            ddlVisaStatus.DataSource = Enum.GetNames(typeof(VisaStatus));
            ddlVisaStatus.DataBind();
            ddlVisaStatus.Items.Insert(0, new ListItem("-Select Visa Status-", "Select"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindUser(int agentid)
    {
        try
        {
            if (agentid == 1)
            {
                DataTable dtusers = UserMaster.GetList(agentid, ListStatus.Short, RecordStatus.Activated);
                DataRow[] drresult = dtusers.Select("user_location_id=" + 782);// live cozmo online location
                DataTable dtFilteredUsers = new DataTable();
                dtFilteredUsers.Columns.Add("USER_FULL_NAME");
                dtFilteredUsers.Columns.Add("USER_ID");
                foreach (DataRow dr in drresult)
                {
                    DataRow dataRow = dtFilteredUsers.NewRow();
                    dataRow["USER_ID"] = dr[0];
                    dataRow["USER_FULL_NAME"] = dr[1];
                    dtFilteredUsers.Rows.Add(dataRow);
                }
                ddlUser.DataSource = dtFilteredUsers;
                ddlUser.DataTextField = "USER_FULL_NAME";
                ddlUser.DataValueField = "USER_ID";
                ddlUser.DataBind();
                ddlUser.Items.Insert(0, new ListItem("--Select User--", "Select User"));
            }
        }
        catch
        {
            throw;
        }
    }
    protected void gvVisaReport_OnPageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvVisaReport.PageIndex = e.NewPageIndex;
            LoadQueue();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "VisaReport gvVisaReport_OnPageIndexChanging Event Error Raised:" + ex.ToString(), "");

        }
    }
    #endregion
}

