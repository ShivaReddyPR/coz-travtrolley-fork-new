﻿using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using CT.Core;
using CT.BookingEngine;
using System.IO;

public partial class FixedDepartureVoucherGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected Activity activity = null;
    protected CT.TicketReceipt.BusinessLayer.AgentMaster agent;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Load Voucher for Queue
            if (Request.QueryString["ID"] != null)
            {
                activity = new Activity();
                activity.GetActivityForQueue(Convert.ToInt64(Request.QueryString["ID"]));
                int agentId = Convert.ToInt32(activity.TransactionHeader.Rows[0]["AgencyId"]);
                agent = new CT.TicketReceipt.BusinessLayer.AgentMaster(agentId);
                BindVoucher(activity);
            }

            string serverPath = "";
            string logoPath = "";
            if (Request.Url.Port > 0)
            {
                serverPath = Request.Url.Scheme+"://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }
            else
            {
                serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }
            //to show agent logo
            if (agent.ID > 1)
            {
                logoPath = serverPath + ConfigurationManager.AppSettings["AgentImage"] + agent.ImgFileName;
                imgHeaderLogo.ImageUrl = logoPath;
            }
            else
            {
                imgHeaderLogo.ImageUrl = serverPath + "images/logo.jpg";
            }

            if (Session["FixedDeparture"] != null)
            {
                //activity = Session["Activity"] as Activity;
                if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId > 0)
                {
                    if (Convert.ToInt32(activity.TransactionHeader.Rows[0]["PaymentStatus"].ToString()) == 0)
                    {
                        string myPageHTML = "";
                        StringWriter sw = new StringWriter();
                        HtmlTextWriter htw = new HtmlTextWriter(sw);
                        printableArea.RenderControl(htw);
                        myPageHTML = sw.ToString();

                        string EmailText = "<table style='width:100%;'><tr><td>Dear&nbsp;&nbsp;" + activity.TransactionDetail.Rows[0]["FirstName"].ToString() + "</td></tr>"
                                                + "<tr><td></td></tr>"
                                                + "<tr><td></td></tr>"
                                                + "<tr><td colspan='2'>Thank you for booking your Holiday with us  </td></tr>"
                                                + "<tr><td></td></tr>"
                                                + "<tr><td></td></tr>"
                                                + "<tr><td colspan='2'>We look forward to welcoming you </td></tr>"
                                                + "<tr><td></td></tr>"
                                                + "<tr><td></td></tr>"
                                                + "<tr><td>Please find the details of your booking</td></tr>"
                                                + "<tr><td></td></tr>"
                                                + "<tr><td></td></tr></table>";

                        string EmailText1 = "<table style='width:100%;'><tr><td></td></tr>"
                                                + "<tr><td></td></tr>"
                                               + "<tr><td colspan='2'>Thank you for choosing Cozmo Travel. </td></tr>"
                                                + "<tr><td colspan='2'>Holidays Specialist Team</td></tr></table>";

                        myPageHTML = EmailText + "</br>" + myPageHTML + "</br>" + EmailText1;
                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        toArray.Add(activity.TransactionDetail.Rows[0]["Email"].ToString());
                        toArray.Add(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentEmail);
                        toArray.Add(ConfigurationManager.AppSettings["holidaySupport"].ToString());
                        //Email.Send(ConfigurationManager.AppSettings["fromEmail"], customer.UserName, new System.Collections.Generic.List<string>(), "Activity Confirmation - " + activity.TransactionHeader.Rows[0]["TripId"].ToString(), "<p>Dear " + customer.FirstName + ",</p><p>Your Activity Booking is Confirmed.</p><p>Greetings,</br> Cozmo Travel</p>", new Hashtable());
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "FixedDeparture Confirmation - " + activity.TransactionHeader.Rows[0]["TripId"].ToString(), myPageHTML, new Hashtable());
                    }
                }
                if (!string.IsNullOrEmpty(activity.SupplierEmail))
                {
                    if (Convert.ToInt32(activity.TransactionHeader.Rows[0]["PaymentStatus"].ToString()) == 0)
                    {
                        string myPageHTML = "<p>Dear " + activity.SupplierName + ",</p><p> FixedDeparture Booking is Confirmed.</p><p>Greetings,</br> Cozmo Travel</p>";
                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        toArray.Add(activity.SupplierEmail);
                        toArray.Add(ConfigurationManager.AppSettings["holidaySupport"].ToString());
                        //Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Activity(" + activity.TransactionHeader.Rows[0]["activityName"].ToString() + ")Confirmation - " + activity.TransactionHeader.Rows[0]["TripId"].ToString(), myPageHTML, new Hashtable());
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "FixedDeparture Confirmation - " + activity.TransactionHeader.Rows[0]["TripId"].ToString(), myPageHTML, new Hashtable());
                    }
                    else
                    {
                        System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                        toArray.Add(activity.TransactionDetail.Rows[0]["Email"].ToString());
                        string EmailText = "<table style='width:100%;'><tr><td>Dear&nbsp;&nbsp;" + activity.TransactionDetail.Rows[0]["FirstName"].ToString() + "</td></tr>"
                                            + "<tr><td></td></tr>"
                                            + "<tr><td></td></tr>"
                                            + "<tr><td colspan='2'>This is a reminder for you to make the balance payment which applies to this booking.</br> It is important to note the cancellation policy shown and any charges</br> which will apply if this booking is cancelled. </td></tr>"
                                            + "<tr><td></td></tr>"
                                            + "<tr><td></td></tr>"
                                            + "<tr><td colspan='2'>&nbsp;&nbsp;&nbsp;&nbsp;Below are the booking details</td></tr>"
                                            + "<tr><td></td></tr>"
                                            + "<tr><td></td></tr>"
                                            + "<tr><td>Trip Id:</td><td>" + activity.TransactionHeader.Rows[0]["TripId"].ToString() + "</td></tr>"
                                            + "<tr><td>Departure Date:</td><td>" + Convert.ToDateTime(activity.TransactionHeader.Rows[0]["Booking"]).ToString("dd-MMM-yyyy") + "</td></tr>"
                                            + "<tr><td>Lead Name:</td><td>" + activity.TransactionDetail.Rows[0]["FirstName"].ToString() + "" + activity.TransactionDetail.Rows[0]["LastName"].ToString() + "</td></tr>"
                                            + "<tr><td>Total Passengers:</td><td>Adult: " + activity.TransactionHeader.Rows[0]["Adult"].ToString() + " Child: " + activity.TransactionHeader.Rows[0]["Child"].ToString() + " Infant: " + activity.TransactionHeader.Rows[0]["Infant"].ToString() + "</td></tr>"
                                            + "<tr><td>Room Type:</td><td>" + activity.TransactionDetail.Rows[0]["RoomType"].ToString() + "</td></tr>"
                                            + "<tr><td></td></tr>"
                                            + "<tr><td></td></tr>"
                                            + "<tr><td colspan='2'>Thank you for choosing Cozmo Travel. </td></tr>"
                                            + "<tr><td colspan='2'>Holidays Specialist Team</td></tr></table>";
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEMail"], ConfigurationManager.AppSettings["holidaySupport"], toArray, "Payment reminder", EmailText, new Hashtable());
                    }
                }
                Session.Remove("FixedDeparture");
            }

        }
        catch (Exception ex)
        {
            Session.Remove("FixedDeparture");
            Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="activity"></param>
    void BindVoucher(Activity activity)
    {
        try
        {
            lblActivityName.Text = activity.Name;
            lblBookingDate.Text = Convert.ToDateTime(activity.TransactionHeader.Rows[0]["CreatedDate"]).ToString("dd MMM yyyy");
            lblTransactionDate.Text = Convert.ToDateTime(activity.TransactionHeader.Rows[0]["Booking"]).ToString("dd MMM yyyy");
            lblCity.Text = activity.City;
            lblCountry.Text = activity.Country;
            //lblDuration.Text = activity.DurationHours.ToString();
            lblLocation.Text = activity.City + ", " + activity.Country;
            lblTotal.Text = agent.AgentCurrency + " " + Convert.ToDecimal(activity.TransactionHeader.Rows[0]["TotalPrice"]).ToString("N" + agent.DecimalValue);

            lblDropPoint.Text = activity.DropoffLocation;
            lblPickupPoint.Text = activity.PickupLocation;
            lblPickupTime.Text = activity.PickupDate.ToString("hh:mm tt");
            //lblAdultCount.Text = activity.TransactionHeader.Rows[0]["Adult"].ToString();
            //lblChildCount.Text = activity.TransactionHeader.Rows[0]["Child"].ToString();
            //lblInfantCount.Text = activity.TransactionHeader.Rows[0]["Infant"].ToString();
            lblBookingRef.Text = activity.TransactionHeader.Rows[0]["TripId"].ToString();
            lblPassengers.Text = (Convert.ToInt32(activity.TransactionHeader.Rows[0]["Adult"]) + Convert.ToInt32(activity.TransactionHeader.Rows[0]["Child"]) + Convert.ToInt32(activity.TransactionHeader.Rows[0]["Infant"])).ToString();
            //Audit.Add(EventType.Exception, Severity.High, 1, "tra" + activity.TransactionDetail.Rows.Count.ToString(), "0");
            DataTable dtPassengers = activity.TransactionDetail;

            for (int i = 0; i < dtPassengers.Rows.Count; i++)
            {
                if (i == 0)//Lead Passenger
                {
                    DataRow row = dtPassengers.Rows[i];
                    lblLeadPassenger.Text = row["FirstName"].ToString() + " " + row["LastName"].ToString();
                    lblGuestName.Text = lblLeadPassenger.Text;
                    lblNationality.Text = row["Nationality"].ToString();
                    lblPhone.Text = row["phone"].ToString();
                }
                //lblPassengers.Text = dtPassengers.Rows.Count.ToString();
            }
            //Audit.Add(EventType.Exception, Severity.High, 1, "6: ", "0");
        }
        catch { throw; }
    }
}
