﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using System.Collections;
using System.Configuration;
using CT.Core;

public partial class FixedDepartureQueueGUI :CT.Core.ParentPage// System.Web.UI.Page
{
     protected DataTable ActivityList;
    PagedDataSource pagedData = new PagedDataSource();
    bool isShowAll = false;
    protected int AgentId;
    protected AgentMaster agency;
    #region Pageload
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                    isShowAll = true;
                    InitializeControls();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "pageLoad", "0");
        }
    }

    private void InitializeControls()
    {
        BindAgent();
        Clear();
    }
    #endregion

    #region BindEvents
    private void BindAgent()
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            ddlAgency.DataSource = dtAgents;
            ddlAgency.DataTextField = "Agent_Name";
            ddlAgency.DataValueField = "agent_id";
            ddlAgency.DataBind();
            ddlAgency.Items.Insert(0, new ListItem("--Select Agency--", "0"));

            ddlAgency.SelectedIndex = -1;
            if (Settings.LoginInfo.AgentId > 0)
            {
                ddlAgency.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }

            if (Settings.LoginInfo.AgentId > 1)
            {
                ddlAgency.Enabled = false;
            }
            else
            {
                ddlAgency.Enabled = true;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void BindActivityQueue()
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            string StartFromDate = string.Empty;
            if (!isShowAll)
                StartFromDate = (txtFrom.Text);
            else
                StartFromDate = DateTime.MinValue.ToString();
            //StartFromDate = DateTime.MinValue.AddYears(2010).ToString("dd/MM/yyyy");

            DateTime fromDate = Convert.ToDateTime(StartFromDate, dateFormat);
            string StartToDate = (txtTo.Text);
            DateTime toDate = Convert.ToDateTime(StartToDate, dateFormat);
            toDate = Convert.ToDateTime(toDate.ToString("MM/dd/yyyy 23:59:59"));
            //toDate = Convert.ToDateTime(toDate.ToString("dd/MM/yyyy 23:59"));
            string tripId = txtTripId.Text;
            string paxName = txtPaxName.Text;
            string status=string.Empty;
            if (ddlStatus.SelectedIndex > 0)
            {
                if (ddlStatus.SelectedItem.Value == "I")
                {
                    status = "C";
                }
                else
                {
                    status = Convert.ToString(ddlStatus.SelectedItem.Value);
                }
            }
            //string paymentId = txtPaymentId.Text;
            int agencyId = Convert.ToInt32(ddlAgency.SelectedItem.Value);
            ActivityList = Activity.GetActivityBookingQueue(fromDate, toDate, tripId, paxName, 0, agencyId, Settings.LoginInfo.MemberType.ToString(), Settings.LoginInfo.LocationID, Settings.LoginInfo.UserID, "Y", string.Empty, status, "B2B");
            DataRow[] rows = new DataRow[ActivityList.Rows.Count];
            if (ddlStatus.SelectedItem.Text == "Confirmed")
            {
                rows = ActivityList.Select("PaymentStatus='0'");
            }
            else if (ddlStatus.SelectedItem.Text == "InProgress")
            {
                rows = ActivityList.Select("PaymentStatus='1'");
            }
            DataTable dt = new DataTable();
            if (ddlStatus.SelectedItem.Value == "C" || ddlStatus.SelectedItem.Value == "I")
            {
                dt = ActivityList.Clone();
                foreach (DataRow row in rows)
                {
                    dt.ImportRow(row);
                }
            }
            else
            {
                dt = ActivityList;
            }
            Session["FixedDepartureQueue"] = dt;
            dlActivityQueue.DataSource = dt;
            dlActivityQueue.DataBind();
            CurrentPage = 0;
            doPaging();
            AgentId = Settings.LoginInfo.AgentId;
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    #endregion

    protected void dlActivityQueue_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                HtmlTable tblOpen = e.Item.FindControl("tblOpen") as HtmlTable;
                HtmlTable tblContinue = e.Item.FindControl("tblContinue") as HtmlTable;
                HiddenField hdnQuoted = e.Item.FindControl("hdnQuoted") as HiddenField;
                Button btnCancel = e.Item.FindControl("btnCancel") as Button;
                Label lblStatus = e.Item.FindControl("lblStatus") as Label;
                Label lblStatusValue = e.Item.FindControl("lblStatusValue") as Label;
                Label lblQuotedStatus = e.Item.FindControl("lblQuotedStatus") as Label;
                HtmlTable tblChangeRequest = e.Item.FindControl("tblChangeRequest") as HtmlTable;
                Button btnRequest = e.Item.FindControl("btnRequest") as Button;
                HiddenField hdnStatusValue = e.Item.FindControl("hdnStatusValue") as HiddenField;
                Label lblStatusText = e.Item.FindControl("lblStatusText") as Label;
                Button btnInvoice = e.Item.FindControl("btnInvoice") as Button;
                Button btnCreditNote = e.Item.FindControl("btnCreditNote") as Button;
                int id = Convert.ToInt32(((DataRowView)e.Item.DataItem).Row.ItemArray[0]);
                int agentId = Convert.ToInt32(((DataRowView)e.Item.DataItem).Row.ItemArray[10]);
                btnInvoice.OnClientClick = "return ViewInvoice('" + id + "','" + agentId + "');";
                btnCreditNote.OnClientClick = "return ViewCreditNote('" + id + "','" + agentId + "');";


                if (hdnQuoted.Value.Trim() == "Q")  //Here Q means Quoted And Status is Active
                {
                    tblContinue.Visible = true;
                    btnCancel.Visible = true;
                    lblQuotedStatus.Visible = true;
                    lblQuotedStatus.Text = "Active";
                    lblStatus.Visible = true;
                    lblStatusText.Visible = true;
                    lblStatus.Text = "QUOTED";
                    lblStatus.ForeColor = System.Drawing.Color.LimeGreen;
                }
                else if (hdnQuoted.Value.Trim() == "C") //Here C Means Confirmed
                {
                    tblOpen.Visible = true;
                    lblStatus.Visible = true;
                    lblStatusValue.Visible = true;
                    btnRequest.OnClientClick = "return CancelInsPlan('" + e.Item.ItemIndex + "');";
                    lblStatus.Visible = true;
                    lblStatusText.Visible = true;
                    if (hdnStatusValue.Value == "0")
                    {
                        lblStatus.Text = "CONFIRMED";
                        lblStatus.ForeColor = System.Drawing.Color.LimeGreen;
                        tblChangeRequest.Visible = true;
                    }
                    else
                    {
                        lblStatus.Text = "InProgress";
                        lblStatus.ForeColor = System.Drawing.Color.Red;
                        btnInvoice.Visible = false;
                        tblChangeRequest.Visible = false;
                    }
                }
                else if (hdnQuoted.Value.Trim() == "X")  //Here X means Quoted And Status Is DeActive
                {
                    tblContinue.Visible = false;
                    lblQuotedStatus.Visible = true;
                    lblStatusText.Visible = true;
                    lblQuotedStatus.Text = "DeActive";
                    lblStatus.Visible = true;

                    lblStatus.Text = "QUOTED";
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
                else if (hdnQuoted.Value.Trim() == "Y") //Here Y Means Requested For Cancel
                {
                    tblOpen.Visible = true;
                    lblStatus.Visible = true;
                    lblStatusText.Visible = true;
                    lblStatusValue.Visible = true;
                    lblStatus.Text = "REQUESTED FOR CANCEL";
                    lblStatus.ForeColor = System.Drawing.Color.Gray;
                }
                else if (hdnQuoted.Value.Trim() == "Z")  // Here Z means Cancelled
                {
                    tblOpen.Visible = true;
                    lblStatus.Visible = true;
                    lblStatus.Text = "CANCELLED";
                    btnInvoice.Visible = false;
                    btnCreditNote.Visible = true;
                    lblStatus.ForeColor = System.Drawing.Color.Red;
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, ex.Message, "0");
        }
    }

    protected void dlActivityQueue_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Continue")  //Here Continue Means Quoted Time directly going to passengerList page 
            {
                if (e.CommandArgument.ToString().Length > 0)
                {
                    Activity activity = new Activity();
                    int fixId = Convert.ToInt32(e.CommandArgument);
                    activity.GetActivityForQueue(fixId);
                    Session["FixedDeparture"] = activity;
                    Response.Redirect("FixedDeparturePassengerList.aspx", false);
                }
            }
            if (e.CommandName == "Cancel") //Here Cancel Means Quoted directly cancel
            {
                if (e.CommandArgument.ToString().Length > 0)
                {
                    long fixId = Convert.ToInt32(e.CommandArgument);
                    Activity.UpdateStatusForTransactionHeader(fixId, "X");  //Here X means InActive
                    BindActivityQueue();
                }
            }
            if (e.CommandName == "FdChangeRequest") //Here FdChangeRequest means directly cancelltion
            {
                if (e.CommandArgument.ToString().Length > 0)
                {
                    long fixId = Convert.ToInt32(e.CommandArgument);
                    DataSet ds = Activity.GetActivityQueueDetails(fixId);
                    DropDownList ddlChangeRequestType = e.Item.FindControl("ddlChangeRequestType") as DropDownList;
                    TextBox txtRemarks = e.Item.FindControl("txtRemarks") as TextBox;
                    ServiceRequest serviceRequest = new ServiceRequest();
                    int requestTypeId = 3;
                    try
                    {
                        if (ds.Tables.Count > 0)
                        {
                            AgentMaster Agency = new AgentMaster(Convert.ToInt32(ds.Tables[0].Rows[0]["AgencyId"]));
                            serviceRequest.BookingId = 0;
                            serviceRequest.ReferenceId = Convert.ToInt32(fixId);
                            serviceRequest.ProductType = ProductType.FixedDeparture;
                            serviceRequest.RequestType = (RequestType)Enum.Parse(typeof(RequestType), requestTypeId.ToString());
                            serviceRequest.Data = txtRemarks.Text;
                            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Assigned;
                            serviceRequest.CreatedBy = Convert.ToInt32(Settings.LoginInfo.UserID);
                            serviceRequest.AgencyId = Settings.LoginInfo.AgentId;
                            serviceRequest.PaxName = ds.Tables[1].Rows[0]["FirstName"].ToString() + " " + ds.Tables[1].Rows[0]["LastName"].ToString();
                            serviceRequest.Pnr = ds.Tables[0].Rows[0]["TripId"].ToString();
                            
                            serviceRequest.StartDate = Convert.ToDateTime(DateTime.Now);
                            serviceRequest.SupplierName = "Cozmo Travel";
                            serviceRequest.ServiceRequestStatusId = ServiceRequestStatus.Unassigned;
                            serviceRequest.ReferenceNumber = ds.Tables[0].Rows[0]["TripId"].ToString();
                            serviceRequest.AgencyId = Settings.LoginInfo.AgentId; //Modified by brahmam       //Every request must fall in Admin Queue, so assign Admin ID
                            serviceRequest.ItemTypeId = InvoiceItemTypeId.FixedDepartureBooking;
                            serviceRequest.DocName = "";
                            serviceRequest.LastModifiedBy = serviceRequest.CreatedBy;
                            serviceRequest.LastModifiedOn = DateTime.Now;
                            serviceRequest.CreatedOn = DateTime.Now;
                            serviceRequest.AgencyTypeId = CT.Core.Agencytype.Cash;
                            serviceRequest.RequestSourceId = RequestSource.BookingAPI;
                            serviceRequest.Save();

                            //Sending email.
                            Hashtable table = new Hashtable(3);
                            table.Add("agentName", Agency.Name);
                            table.Add("FixedDepartureName",ds.Tables[0].Rows[0]["activityName"].ToString());
                            table.Add("TripId", ds.Tables[0].Rows[0]["TripId"].ToString());
                            //Audit.Add(EventType.Visa, Severity.High, (int)Settings.LoginInfo.UserID, "zi 4", "0");
                            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                            UserMaster bookedUser = new UserMaster(Convert.ToInt32(ds.Tables[0].Rows[0]["CreatedBy"].ToString()));
                            toArray.Add(bookedUser.Email);
                            toArray.Add(Agency.Email1);
                            string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["FIXED_CANCEL_MAIL"]).Split(';');
                            foreach (string cnMail in cancelMails)
                            {
                                toArray.Add(cnMail);
                            }

                            //Updating status
                            Activity.UpdateStatusForTransactionHeader(fixId, "Y"); //Here Y means ChangeRequest
                            string message = ConfigurationManager.AppSettings["FIXED_DEPARTURE_CHANGE_REQUEST"]; 
                            try
                            {
                                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(FixedDeparture) Request for cancellation. TripId:(" + ds.Tables[0].Rows[0]["TripId"].ToString() + ")", message, table);
                            }
                            catch
                            {
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Visa, Severity.High,(int) Settings.LoginInfo.UserID, ex.Message, "0");
                    }
                    Response.Redirect("FixedDepartureQueue.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "itemCommand", "0");
        }
    }

    #region Button Click Events
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindActivityQueue();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "searchEvent", "0");
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "ClearEvent", "0");
        }
    }
    #endregion

    #region Private Methods
    private void Clear()
    {
        try
        {
            txtFrom.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtTo.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
            txtTripId.Text = string.Empty;
            txtPaxName.Text = string.Empty;
            ddlStatus.SelectedIndex = 0;
            //txtPrice.Text = "0.00";
            //txtPaymentId.Text = string.Empty;
            isShowAll = true;
            //ddlAgency.SelectedIndex = 0;
            Session["FixedDepartureQueue"] = null;
            BindActivityQueue();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected string CTCurrencyFormat(object currency, object AgentId)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            agency = new AgentMaster(Convert.ToInt32(AgentId));
            return agency.AgentCurrency + " " + Convert.ToDecimal(0).ToString("N" + agency.DecimalValue);
        }
        else
        {
            agency = new AgentMaster(Convert.ToInt32(AgentId));
            return agency.AgentCurrency + " " + Convert.ToDecimal(currency).ToString("N" + agency.DecimalValue);
        }
    }
    //protected string CTCurrencyName(object AgentId)
    //{
    //    if (string.IsNullOrEmpty(AgentId.ToString()) || AgentId == DBNull.Value)
    //    {
    //        agency=new AgentMaster(Convert.ToInt32(AgentId));
    //        return agency.AgentCurrency;
    //    }
    //    else
    //    {
    //        agency = new AgentMaster(Convert.ToInt32(AgentId));
    //        return agency.AgentCurrency;
    //    }
    //}
    #endregion

    #region Paging
    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }

    void doPaging()
    {
        DataTable dt = (DataTable)Session["FixedDepartureQueue"];
        pagedData.DataSource = dt.DefaultView;
        pagedData.AllowPaging = true;
        pagedData.PageSize = 5;
        Session["count"] = pagedData.PageCount - 1;
        pagedData.CurrentPageIndex = CurrentPage;
        btnPrev.Visible = (!pagedData.IsFirstPage);
        btnFirst.Visible = (!pagedData.IsFirstPage);
        btnNext.Visible = (!pagedData.IsLastPage);
        btnLast.Visible = (!pagedData.IsLastPage);
        lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
        DataView dView = (DataView)pagedData.DataSource;
        DataTable dTable;
        dTable = (DataTable)dView.Table;
        dlActivityQueue.DataSource = pagedData;
        dlActivityQueue.DataBind();
    }
    protected void btnPrev_Click(object sender, EventArgs e)
    {
        try
        {
            CurrentPage--;
            doPaging();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "", "0");
        }
    }
    protected void btnNext_Click(object sender, EventArgs e)
    {
        try
        {
            CurrentPage++;
            doPaging();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "", "0");
        }
    }
    protected void btnFirst_Click(object sender, EventArgs e)
    {
        try
        {
            CurrentPage = 0;
            doPaging();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "", "0");
        }
    }
    protected void btnLast_Click(object sender, EventArgs e)
    {
        try
        {
            CurrentPage = (int)Session["count"];
            doPaging();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message + "", "0");
        }
    }
    #endregion
}
