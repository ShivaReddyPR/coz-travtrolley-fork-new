﻿<%@ Page Title="" Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" CodeBehind="ReligarePaymentConfirmation.aspx.cs" Inherits="CozmoB2BWebApp.ReligarePaymentConfirmation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="server">
   
   <div class="body_container"> 
    <div class="" style="margin-top: 10px;">
        <table class="table">
            <tr>
                <td>
                    <h5>Quotation
                    </h5>
                </td>
                <td></td>
                <td>
                    <asp:Label ID="lblagentBal" Style="color: red" runat="server" Text="0.00" /></td>
                <td style="text-align: right">
                    <h5>
                        <span>Premium :</span>
                        <asp:Label ID="lblPremium" Style="color: Red" runat="server" Text="0.00" /><span>&nbsp;</span>
                    </h5>
                </td>
            </tr>
            <tr>
                <td><span>Start Date:-</span>&nbsp;<asp:Label ID="lblStartDate" runat="server" Text="0.00" />
                </td>
                <td><span>End Date:-</span>&nbsp;<asp:Label ID="lblEndDate" runat="server" Text="0.00" />
                </td>
                <td><span>Travelling To:-</span>&nbsp;<asp:Label ID="lblTravellingTo" runat="server" Text="0.00" />
                </td>
                <td><span>PED:-</span>&nbsp;<asp:Label ID="lblPED" runat="server" Text="0.00" />
                    <asp:Button ID="btnEdit" runat="server" Text="Edit Quotation" class="btn btn-primary active" OnClick="btnEdit_Click" Style="float: right; border-radius: 5px; margin-right: 10px;" />
                    <%--<input type="button" value="Edit" style="align-content:flex-end" />--%></td>
            </tr>
        </table>
    </div>
    <div class="" style="margin-top: 10px;">
        <table id="tblPaxPrice" cellspacing="0" border="0" style="width: 100%; border-collapse: collapse;">
            <tbody>
                <tr>
                    <td>
                        <div class="bg_white">
                            <table class="table" width="100%">
                                <tbody>
                                    <tr>
                                        <td width="55%" class="gray-smlheading">
                                            <strong>Products</strong>
                                        </td>
                                        <td class="gray-smlheading" width="35%" align="right"><strong>Price</strong></td>
                                        <td width="10%" class="gray-smlheading" align="right"></td>
                                    </tr>
                                    <tr>
                                        <td>Total Premium Amount                                                                    
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblBaseFare" runat="server" Text="0.00" />
                                        </td>
                                        <td></td>
                                    </tr>

                                    <tr style="display: none;">
                                        <td>Tax   </td>
                                        <td align="right">
                                            <asp:Label ID="lblTax" runat="server" Text="0.00" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr style="display: none;">
                                        <td>Mark up                                                                    
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblMarkup" runat="server" Text="0.00" />
                                        </td>
                                        <td></td>
                                    </tr>

                                    <tr>
                                        <td>Total GST                                                                    
                                        </td>
                                        <td align="right">
                                            <asp:Label ID="lblVATAmount" runat="server" Text="0.00" />
                                        </td>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <strong class="spnred">Grand Total</strong>
                                        </td>
                                        <td align="right">
                                            <strong>
                                                <asp:Label ID="lblMarkupTotal" runat="server" Text="0.00" />  
                                        </td>
                                        <td></td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    <div>
        <table width="100%" border="0" cellspacing="0" cellpadding="0">
            <tbody>
                <tr>
                    <td>
                        <div class="col-md-6 pad_left0" style="height: 315px;">
                            <div class="ns-h3">
                                <strong>Passenger Details</strong>
                                <asp:LinkButton ID="lbtnPaxEdit" runat="server" Text="Edit Pax Details" Style="margin-right: 30px; color: white; float: right;" OnClick="lbtnPaxEdit_Click" />
                            </div>
                            <div class="bg_white bor_gray pad_10 payment_confirm_pax">
                                <table id="ctl00_cphTransaction_dlPassengers" class="table table-striped" cellspacing="0" border="0" style="width: 100%; border-collapse: collapse;">
                                    <tbody>
                                        <tr>
                                            <td>
                                                <asp:Table ID="tblPassengerDetails" runat="server" Style="width: 100%;">
                                                </asp:Table>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <div class="col-md-6 pad_right0">
                            <div class="ns-h3"><strong>Payment Details</strong> </div>
                            <div class="modal fade in farerule-modal-style" data-backdrop="static" id="FareRuleBlock" tabindex="-1" role="dialog" aria-labelledby="FareDiffLabel">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close mt-0" style="color: #fff; opacity: .8;" onclick="FareRuleHide()"><span aria-hidden="true">×</span></button>
                                            <h4 class="modal-title" id="FareRuleHead"><span id="FareRuleHeadTitle"></span></h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="bg_white" id="FareRuleBody" style="padding: 10px; width: 100%; font-size: 13px; height: 92%; overflow: auto;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="bg_white bor_gray pad_10">
                                <div>
                                    <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                        <tbody>
                                            <tr>
                                                <td>
                                                    <div style="padding: 0px 10px 0px 10px">

                                                        <ul style="margin-left: 10px;">
                                                            <li>Policy changes may incur penalties and/or increased fares.</li>
                                                            <li>Policy is nontransferable and name changes are not allowed.</li>
                                                            <!--<li>Read an overview of all the<a href="#"> rules &amp; restrictions</a> applicable to 
                                                                        this fare.</li>-->
                                                           <%-- <li>Read the complete<a href="Javascript:FareRule(1,'c589f151-8e7f-44e6-95f0-760e57a6d6e1')"> penalty rules for changes and cancellations</a>
                                                                applicable to this fare.</li>--%>
                                                        </ul>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10px">
                                                    <hr class="b_bot_1 my-1">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <table style="margin-left: 10px;" width="100%" border="0" cellspacing="0" cellpadding="0">
                                                        <tbody>
                                                            <tr>
                                                                <td width="4%">
                                                                    <span style="font-size: 11px">
                                                                        <asp:CheckBox ID="chkRules" runat="server" Checked="true" />
                                                                    </span>
                                                                </td>
                                                                <td width="96%">
                                                                    <strong>I have read and accept the rules &amp; restrictions.</strong><%--<span id="spanrules" style="color:red"></span>--%>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td colspan="2" style="width: 100%">
                                                                    <div id="rules" style="color: Red; display: none; width: 100%">
                                                                        Please check rules and restrictions to continue.
                                                                    </div>
                                                                </td>
                                                            </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10px">
                                                    <hr class="b_bot_1  my-1">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>

                                                    <div class="col-sm-3">
                                                        <span>Payment Type:</span>
                                                        <asp:DropDownList ID="ddlPaymentType" runat="server" class="form-control">
                                                            <%-- <asp:ListItem Value="-1">-- Select --</asp:ListItem>--%>
                                                            <asp:ListItem Value="Credit">On Account</asp:ListItem>
                                                            <%--<asp:ListItem Value="Card">Credit Card</asp:ListItem>--%>
                                                        </asp:DropDownList>
                                                    </div>
                                                    <div id="divAgentPG" style="display: none; margin-bottom: 10px">
                                                        Payment Gateways
                                                                <table id="ctl00_cphTransaction_rblAgentPG" onchange=" return FillCharges()" border="0">
                                                                    <tbody>
                                                                        <tr>
                                                                            <td>
                                                                                <input id="ctl00_cphTransaction_rblAgentPG_0" type="radio" name="ctl00$cphTransaction$rblAgentPG" value="3.0000" checked="checked">
                                                                                <label for="ctl00_cphTransaction_rblAgentPG_0">SafexPay</label></td>
                                                                        </tr>
                                                                    </tbody>
                                                                </table>
                                                        <div id="pgValidation" style="color: red; display: none; width: 100%;">
                                                            Please select one of the payment gateways.
                                                        </div>
                                                    </div>
                                                    <div style="display: none; color: Red; font-weight: bold;" id="divCardCharge">
                                                        Credit Card Charges
                                                                <label id="lblCardCharges" style="color: Red; font-weight: bold;">
                                                                </label>
                                                        % applicable on Total Amount
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td height="10px">
                                                    <hr class="b_bot_1  my-1">
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>

                                                    <div style="padding: 5px 0px 10px 0px">
                                                        <table width="100%">
                                                            <tbody>
                                                                <tr class="nA-h4">
                                                                    <td align="right">Available Balance :
                                                                    </td>
                                                                    <td align="right" style="font-weight: bold; padding-right: 10px">
                                                                        <asp:Label ID="lblAgentBalance" runat="server" Text="0.00" />
                                                                    </td>
                                                                </tr>
                                                                <tr class="nA-h4">
                                                                    <td align="right">Amount to be Booked :
                                                                    </td>
                                                                    <td align="right" style="font-weight: bold; padding-right: 10px">
                                                                        <asp:Label ID="lblTxnAmount" runat="server" Text="0.00" />
                                                                    </td>
                                                                </tr>
                                                                <tr>
                                                                    <td align="left" colspan="2">
                                                                        <asp:Label ID="lblBalanceError" Style="color: Red;" runat="server" />
                                                                    </td>
                                                                </tr>
                                                            </tbody>
                                                        </table>
                                                    </div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td>
                                                    <span id="lblMessage" runat="server" style="color: Red;"></span>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="right">
                                                    <label style="padding-right: 10px">
                                                        <asp:Button ID="btnPayment" runat="server" CssClass="btn btn-info btn-primary" Text="Confirm Payment" OnClientClick="javascript:return PaymentValidations();" Style="float: right; border-radius: 5px; margin-right: 10px;" OnClick="btnPayment_Click" /><%--OnClick="btnPayment_Click"--%>
                                                    </label>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>

                            </div>
                        </div>
                    </td>
                </tr>
            </tbody>
        </table>
    </div>
    
</div>
 
 
    <style type="text/css">
        .ui-accordion .ui-accordion-content {
            height: auto !important;
        }   
      
        .modal {
            position: fixed;
            top: 0;
            left: 0;
            background-color: black;
            z-index: 99;
            opacity: 0.8;
            filter: alpha(opacity=20);
            -moz-opacity: 0.8;
            min-height: 100%;
            width: 100%;
        }
    </style>
    <script type="text/javascript">
        function PaymentValidations() {
            var isvalid = true;
            var ddlPaymentType = $("#<%=ddlPaymentType.ClientID%>").val();
                if (ddlPaymentType == "-1") {
                    $("#s2id_ctl00_cphTransaction_ddlPaymentType").focus();
                    $("#s2id_ctl00_cphTransaction_ddlPaymentType").css("border-color", "Red");
                    isvalid = false;
                }
                else { $("#s2id_ctl00_cphTransaction_ddlPaymentType").css("border-color", ""); }

                var isChecked = $("#ctl00_cphTransaction_chkRules").is(":checked");
                if (!(isChecked)) {
                    isvalid = false;
                    $('#rules').show();
                } else $('#rules').hide(); 
                var lblAgentBalance = parseInt($("#ctl00_cphTransaction_lblAgentBalance").val());//document.getElementById('lblAgentBalance').val();
                var paymentAmount = parseInt($("#ctl00_cphTransaction_lblMarkupTotal").val());
                lblAgentBalance = 100;
                if (lblAgentBalance > 0) {
                    if (paymentAmount > lblAgentBalance) {
                        isvalid = false;
                        $("#<%=lblMessage.ClientID%>").val('Insufficiant Balance.Please Contact Admin.');
                }
            } 
            //if (isvalid)
               // ShowProgress();
            return isvalid;
        }
        //function ShowProgress() {
        //    setTimeout(function () {
        //        var modal = $('<div />');
        //        modal.addClass("modal");
        //        $('body').append(modal);
        //        var loading = $(".loading");
        //        loading.show();
        //        $(".transparentCover").show();
        //        var top = Math.max($(window).height() / 2 - loading[0].offsetHeight / 2, 0);
        //        var left = Math.max($(window).width() / 2 - loading[0].offsetWidth / 2, 0);
        //        loading.css({ top: top, left: left });
        //    }, 200);

        //}
        //function HideProgress() {
        //    $(".loading").hide();
        //    $(".transparentCover").hide();
        //}

    </script>
</asp:Content>
