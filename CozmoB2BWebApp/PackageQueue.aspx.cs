﻿using System;
using System.Data;
using System.Configuration;
using System.Web.UI.WebControls;
using System.Collections;
using CT.Core;

using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;

public partial class PackageQueueGUI : CT.Core.ParentPage
{
    PagedDataSource adsource;
    int pos;
    protected int recordsPerPage = 0;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
            AuthorizationCheck();
            if (!IsPostBack)
            {
                hdfParam.Value = "0";
                BindAgent();
                txtFromDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                txtToDate.Text = DateTime.Now.Date.ToString("dd/MM/yyyy");
                this.ViewState["vs"] = 0;//For Paging List Items
                pos = (int)this.ViewState["vs"];
                databind();


            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from (PackageQueue Page):" + ex.ToString(), "");
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            //LoadQueue();
            this.ViewState["vs"] = 0;
            databind();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from (PackageQueue Page):" + ex.ToString(), "");
        }

    }
    protected void dlBookingQueue_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
            {
                DataRowView drv = (DataRowView)(e.Item.DataItem);
                int tranxId = int.Parse(drv.Row["TranxHeaderId"].ToString());
                Label lblViewBooking = e.Item.FindControl("lblViewPackageBooking") as Label;
                Label lblViewInvoice = e.Item.FindControl("lblViewPackageInvoice") as Label;
                Button btnRequest = e.Item.FindControl("btnRequest") as Button;
                DropDownList ddlChangeRequestType = e.Item.FindControl("ddlBooking") as DropDownList;
                TextBox txtRemarks = e.Item.FindControl("txtRemarks") as TextBox;
                Label lblRequestStatus = e.Item.FindControl("lblRequestStatus") as Label;
                Label lblRequestChange = e.Item.FindControl("lblRequestChange") as Label;
                Label lblSupplierRef = e.Item.FindControl("lblSupplierRefTxt") as Label;

                Label lblTotalPackagePrice = e.Item.FindControl("lblSupplierPrice") as Label;
                Label lblTotalPackageSupplierPrice = e.Item.FindControl("lblTotalPackageSupplierPrice") as Label;

                Label lblPromoCode = e.Item.FindControl("lblPromoCode") as Label;
                Label lblPromoCodeValue = e.Item.FindControl("lblPromoCodeValue") as Label;

                Label lblTotalPrice = e.Item.FindControl("lblTotalPrice") as Label;
                LinkButton lnkPayment = e.Item.FindControl("lnkPayment") as LinkButton;


                if (Settings.LoginInfo.AgentId == 1)
                {
                    lblTotalPackagePrice.Visible = true;
                    lblTotalPackageSupplierPrice.Visible = true;
                }
                else
                {
                    lblTotalPackagePrice.Visible = false;
                    lblTotalPackageSupplierPrice.Visible = false;
                }

                if (drv.Row["Promo_Code"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["Promo_Code"])))
                {
                    lblPromoCode.Visible = true;
                    lblPromoCodeValue.Visible = true;
                }
                else
                {
                    lblPromoCode.Visible = false;
                    lblPromoCodeValue.Visible = false;
                }
                //Verify if any promo applied diplay the price after deducting the discount value.
                if (drv.Row["Promo_Discount_Value"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["Promo_Discount_Value"])))
                {
                    decimal promodiscountValue = Math.Ceiling(Convert.ToDecimal(drv.Row["Promo_Discount_Value"]));
                    string[] priceDet = Convert.ToString(drv.Row["Sale_Price"]).Split(' ');
                    decimal totalPriceWithoutDiscount = 0;
                    if (priceDet.Length > 1)
                    {
                        totalPriceWithoutDiscount = Convert.ToDecimal(priceDet[1]);
                        decimal totalPriceAfterPromo = (totalPriceWithoutDiscount - promodiscountValue);
                        lblTotalPrice.Text = Convert.ToString(priceDet[0] + ' ' + totalPriceAfterPromo);
                    }
                    else
                    {
                        totalPriceWithoutDiscount = Convert.ToDecimal(priceDet[0]);
                        decimal totalPriceAfterPromo = (totalPriceWithoutDiscount - promodiscountValue);
                        lblTotalPrice.Text = Convert.ToString(totalPriceAfterPromo);
                    }
                }
                else
                {
                    lblTotalPrice.Text = Convert.ToString(drv.Row["Total_Price"]);
                }

                lblViewBooking.Text = "<input id='Open-" + e.Item.ItemIndex + "' type='button' class='button' value='Open' onclick=\"ViewBookingforPackage(" + tranxId + ")\"  />";
                lblViewInvoice.Text = "<input id='View-" + e.Item.ItemIndex + "' type='button' class='button' value='View Invoice' onclick=\"ViewInvoiceforPackage(" + tranxId + ")\"  />";

                if (drv.Row["Supplier_Ref"] != DBNull.Value && !string.IsNullOrEmpty(Convert.ToString(drv.Row["Supplier_Ref"])))
                {
                    lblSupplierRef.Visible = true;
                }
                else
                {
                    lblSupplierRef.Visible = false;
                }
                if (drv.Row["BookingStatus"].ToString().ToLower() == "vouchered")
                {
                    btnRequest.OnClientClick = "return CancelAmendBooking('" + e.Item.ItemIndex + "');";
                    btnRequest.Visible = true;
                    ddlChangeRequestType.Visible = true;
                    txtRemarks.Visible = true;
                    lblRequestStatus.Visible = false;
                    lblRequestChange.Visible = true;
                }
                else if (drv.Row["BookingStatus"].ToString().ToLower() == "request change")
                {
                    btnRequest.Visible = false;
                    ddlChangeRequestType.Visible = false;
                    lblRequestStatus.Visible = true;
                    lblRequestStatus.Text = "Requested for Cancellation";
                    txtRemarks.Visible = false;
                    lblRequestChange.Visible = false;
                }
                else
                {
                    btnRequest.Visible = false;
                    ddlChangeRequestType.Visible = false;
                    lblRequestStatus.Visible = false;
                    txtRemarks.Visible = false;
                    lblRequestChange.Visible = false;
                }

                if (drv["OrderId"] != DBNull.Value && !string.IsNullOrEmpty(drv["OrderId"].ToString()) && drv["PromoTranxId"] != DBNull.Value && !string.IsNullOrEmpty(drv["PromoTranxId"].ToString()))
                {
                    if (drv["PaymentStatus"] != DBNull.Value && !string.IsNullOrEmpty(drv["PaymentStatus"].ToString()) && Convert.ToInt32(drv["PaymentStatus"]) == 1)//Show Payment Link When Payment is success.
                    {
                        lnkPayment.Visible = true;
                        lnkPayment.OnClientClick = "return ViewPaymentInfo('" + e.Item.ItemIndex + "','" + drv["OrderId"].ToString() + "','" + drv["PromoTranxId"].ToString() + "');";
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from (PackageQueue Page):" + ex.ToString(), "");
        }
    }
    protected void dlBookingQueue_ItemCommand(object source, DataListCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "SubmitRequest")
            {
                if (e.CommandArgument.ToString().Length > 0)
                {
                    Label leadPaxEmail = e.Item.FindControl("lblLeadPaxEmail") as Label;
                    Label leadPaxName = e.Item.FindControl("lblLeadPaxName") as Label;
                    DropDownList reqChange = e.Item.FindControl("ddlBooking") as DropDownList;
                    TextBox txtRemarks = e.Item.FindControl("txtRemarks") as TextBox;
                    string selReqChange = reqChange.SelectedValue;

                    if (selReqChange.ToLower() == "cancel booking")
                    {
                        //Update tbl_Package_Transaction_Header bookingStatus Column to 'R'
                        //R ---Request Change
                        CT.Core.Queue.UpdatePkgTranxBookingStatus(Convert.ToInt32(e.CommandArgument), txtRemarks.Text, "R");
                        //  LoadQueue();
                        databind();

                        //Send a mail
                        //Get  Package_Transaction_Header details from tbl_Package_Transaction_Header by tranx id
                        DataTable dtDetails = CT.Core.Queue.GetPackageHeaderDetailsByTranxId(Convert.ToInt32(e.CommandArgument));
                        if (dtDetails != null && dtDetails.Rows.Count > 0)
                        {
                            AgentMaster agency = new AgentMaster(Convert.ToInt32(dtDetails.Rows[0]["AgencyId"]));
                            Hashtable table = new Hashtable();
                            table.Add("agentName", agency.Name);
                            table.Add("leadPaxName", !string.IsNullOrEmpty(leadPaxName.Text) ? leadPaxName.Text : "Customer");
                            table.Add("pkgName", Convert.ToString(dtDetails.Rows[0]["packageName"]));
                            table.Add("confirmationNo", Convert.ToString(dtDetails.Rows[0]["TripId"]));

                            System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                            UserMaster bookedBy = new UserMaster(Convert.ToInt32(dtDetails.Rows[0]["CreatedBy"]));
                            AgentMaster bookedAgency = new AgentMaster(Convert.ToInt32(dtDetails.Rows[0]["AgencyId"]));
                            toArray.Add(bookedBy.Email);
                            toArray.Add(bookedAgency.Email1);
                            string[] cancelMails = Convert.ToString(ConfigurationManager.AppSettings["CANCEL_REQUEST_MAIL"]).Split(';');
                            foreach (string cnMail in cancelMails)
                            {
                                toArray.Add(cnMail);
                            }
                            if (!string.IsNullOrEmpty(leadPaxEmail.Text))
                            {
                                toArray.Add(leadPaxEmail.Text);
                            }
                            string message = ConfigurationManager.AppSettings["PKG_CANCEL_REQUEST"];
                            try
                            {
                                CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["replyTo"], toArray, "(Package)Request for cancellation. Confirmation No:(" + Convert.ToString(dtDetails.Rows[0]["TripId"]) + ")", message, table, ConfigurationManager.AppSettings["BCCEmail"]);
                            }
                            catch { }
                        }
                    }

                }
            }

        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from (PackageQueue Page):" + ex.ToString(), "");
        }
    }
    protected void btnfirst_Click(object sender, EventArgs e)
    {
        pos = 0;
        databind();
    }

    protected void btnprevious_Click(object sender, EventArgs e)
    {
        pos = (int)this.ViewState["vs"];
        pos -= 1;
        this.ViewState["vs"] = pos;
        databind();
    }

    protected void btnnext_Click(object sender, EventArgs e)
    {
        pos = (int)this.ViewState["vs"];
        pos += 1;
        this.ViewState["vs"] = pos;
        databind();
    }

    protected void btnlast_Click(object sender, EventArgs e)
    {

        pos = adsource.PageCount - 1;
        databind();
    }
    #region Private Methods 
    private void BindAgent()
    {

        try
        {

            DataTable dtAgents = CT.TicketReceipt.BusinessLayer.AgentMaster.GetList(1, "ALL", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            DataView view = dtAgents.DefaultView;
            view.Sort = "agent_Name ASC";
            dtAgents = view.ToTable();
            ddlAgents.DataSource = dtAgents;
            ddlAgents.DataTextField = "agent_Name";
            ddlAgents.DataValueField = "agent_Id";
            ddlAgents.AppendDataBoundItems = true;
            ddlAgents.DataBind();
            ddlAgents.Items.Insert(0, new ListItem("--All--", "0"));
            ddlAgents.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);

            if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
            {
                ddlAgents.Enabled = true;
            }
            else
            {
                ddlAgents.Enabled = false;
            }
        }

        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }

    }
    private DataTable GetPackagesQueueData(DateTime fromDate, DateTime toDate, string agencyId)
    {
        DataTable dtItems = null;
        try
        {
            dtItems = CT.Core.Queue.GetPackageQueueRecords(fromDate, toDate, agencyId);
        }
        catch (Exception ex)
        {
            throw ex;
        }
        return dtItems;
    }

    #endregion
    public void databind()
    {


        DateTime startDate = DateTime.MinValue, endDate = DateTime.MaxValue;
        IFormatProvider provider = new System.Globalization.CultureInfo("en-GB");
        string agencyId = string.Empty;
        //string pnrNo = string.Empty;

        if (ddlAgents.SelectedItem.Value != "0")
        {
            agencyId = Convert.ToString(ddlAgents.SelectedItem.Value);
        }
        //if (txtPNR.Text.Length > 0)
        //{
        //    pnrNo = txtPNR.Text;
        //}

        if (txtFromDate.Text != string.Empty)
        {
            startDate = Convert.ToDateTime(txtFromDate.Text, provider);
        }
        else
        {
            startDate = DateTime.Now;
        }
        if (txtToDate.Text != string.Empty)
        {
            endDate = Convert.ToDateTime(Convert.ToDateTime(txtToDate.Text, provider).ToString("dd/MM/yyyy 23:59"), provider);
        }
        else
        {
            endDate = Convert.ToDateTime(DateTime.Now.Date.ToString("dd/MM/yyyy 23:59"), provider);
        }

        DataTable dt = GetPackagesQueueData(startDate, endDate, agencyId);
        DataView dv = dt.DefaultView;
        dv.Sort = "TranxHeaderId desc";
        DataTable sortedDT = dv.ToTable();



        adsource = new PagedDataSource();
        DataSet dsPkgQueues = new DataSet("PackagesQueues");
        dsPkgQueues.Tables.Add(sortedDT);
        if (dt.Rows.Count > 10)
        {
            Paging.Visible = true;
        }
        else
        {
            Paging.Visible = false;
        }
        adsource.DataSource = dsPkgQueues.Tables[0].DefaultView;
        adsource.PageSize = 10;
        recordsPerPage = 10;
        adsource.AllowPaging = true;
        adsource.CurrentPageIndex = pos;
        btnfirst.Enabled = !adsource.IsFirstPage;
        btnprevious.Enabled = !adsource.IsFirstPage;
        btnlast.Enabled = !adsource.IsLastPage;
        btnnext.Enabled = !adsource.IsLastPage;
        dlBookingQueue.DataSource = adsource;
        dlBookingQueue.DataBind();
        hdfParam.Value = "1";
    }
}
