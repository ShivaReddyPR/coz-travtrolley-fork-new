﻿using System;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Configuration;
using System.Collections.Generic;
using System.Globalization;
public partial class FleetResults : CT.Core.ParentPage
{
    protected FleetRequest request = new FleetRequest();
    protected FleetSearchResult[] totalResultList = new FleetSearchResult[0];
    protected int decimalValue = 2;
    protected string errorMessage = string.Empty;
    protected bool ErrorFlag = false;
    PagedDataSource pagedData = new PagedDataSource();
    protected FleetSearchResult[] filteredResultList;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (Session["FleetRequest"] == null || Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            else
            {
                Page.Title = "Fleet Search Results";
                request = (FleetRequest)Session["FleetRequest"];

                
                if (!IsPostBack)
                {
                    CurrentPage = 0;
                    SearchFleetResults();
                    doPaging();

                    List<CT.BookingEngine.FleetCity> cityList = new List<CT.BookingEngine.FleetCity>();
                    List<CT.BookingEngine.FleetLocation> locationList = new List<CT.BookingEngine.FleetLocation>();
                    if (Session["CityList"] == null && Session["LoactionList"] == null)
                    {
                        Sayara.SayaraApi sayarRestApi = new Sayara.SayaraApi();
                        //Loading Citi and Location list
                        cityList = sayarRestApi.GetCityList(ref locationList);
                        if (cityList != null && cityList.Count > 0 && locationList != null && locationList.Count > 0)
                        {
                            //storing all cities and locations in session
                            Session["CityList"] = cityList;
                            Session["LoactionList"] = locationList;
                        }
                    }
                    else
                    {
                        cityList = Session["CityList"] as List<CT.BookingEngine.FleetCity>;
                    }

                    //binding pickup and dropup cities
                    foreach (CT.BookingEngine.FleetCity city in cityList)
                    {
                        ListItem PickupCity = new ListItem(city.CityName, city.CityCode);
                        ListItem ReturnCity = new ListItem(city.CityName, city.CityCode);
                        ddlPickupCity.Items.Add(PickupCity);
                        ddlReturnCity.Items.Add(ReturnCity);
                    }

                    
                }
                if (hdnModify.Value == "0") //Checking Modify Search
                {
                    ddlPickupCity.SelectedValue = request.FromCityCode;
                    BindPickUpCity();
                    ddlPickupLocation.SelectedValue = request.FromLocationCode;
                    CheckIn.Text = request.FromDateTime.ToString("dd/MM/yyyy");
                    CheckOut.Text = request.ToDateTime.ToString("dd/MM/yyyy");
                    if (request.ReturnStatus == "Y") //checking return
                    {
                        chkRetunLocation.Checked = true;
                        ddlReturnCity.SelectedValue = request.ToCityCode;
                        BindReturnCity();
                        ddlReturnLocation.SelectedValue = request.ToLocationCode;
                    }
                    if (!string.IsNullOrEmpty(request.CarType))
                    {
                        ddlCarType.SelectedValue = request.CarType;
                    }
                }

                
            }
            CT.TicketReceipt.Common.Utility.StartupScript(this.Page, "selectReturnLocation();", "Script");
           

        }
        catch (BookingEngineException ex)
        {
            string url = Request.ServerVariables["HTTP_REFERER"].ToString();
            errorMessage = "Error: There is some error. Please " + "<a href=\"" + url + "\">" + "Retry" + "</a>.";
            ErrorFlag = true;
            throw ex;
        }
        catch (Exception ex)
        {
            errorMessage = "Error: " + ex.Message;
            ErrorFlag = true;
            Audit.Add(EventType.FleetSearch, Severity.High, (int)Settings.LoginInfo.UserID, "Fleet Result page load exception:" + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    protected void SearchFleetResults()
    {
        try
        {
            if (Session["searchResult"] == null)
            {
                //loading resonse
                CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
                mse.SettingsLoginInfo = Settings.LoginInfo;
                totalResultList = mse.GetFleetResults(request, Convert.ToInt32(Settings.LoginInfo.AgentId));
                Session["cSessionID"] = mse.SessionId;
            }
            else
            {
                totalResultList = Session["searchResult"] as FleetSearchResult[];
            }
            //Index id newly assign
            for (int i = 0; i < totalResultList.Length; i++)
            {
                totalResultList[i].ID = i;
            }
            if (totalResultList.Length > 0)
            {
                Session["searchResult"] = totalResultList;
                filteredResultList = totalResultList;
                Session["filteredResultList"] = filteredResultList;
            }
        }
        catch { }
    }
    #region paging
    public int CurrentPage
    {
        get
        {
            if (ViewState["_currentPage"] == null)
            {
                return 0;
            }
            else
            {
                return (int)ViewState["_currentPage"];
            }
        }

        set
        {
            ViewState["_currentPage"] = value;
        }
    }
    //paging
    void doPaging()
    {
        try
        {
            if (filteredResultList != null)
            {
                pagedData.DataSource = filteredResultList;
            }
            else
            {
                filteredResultList = (FleetSearchResult[])Session["filteredResultList"];
                pagedData.DataSource = filteredResultList;
            }
            pagedData.AllowPaging = true;
            pagedData.PageSize = 5; //Default page size
            Session["count"] = pagedData.PageCount - 1;
            pagedData.CurrentPageIndex = CurrentPage;
            btnPrev.Visible = (!pagedData.IsFirstPage);
            btnFirst.Visible = (!pagedData.IsFirstPage);
            btnNext.Visible = (!pagedData.IsLastPage);
            btnLast.Visible = (!pagedData.IsLastPage);
            lblCurrentPage.Text = "Page: " + (CurrentPage + 1).ToString() + " of " + pagedData.PageCount.ToString();
            if (pagedData.Count > 0)
            {
                dlFleet.DataSource = pagedData;
                dlFleet.DataBind();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnPrev_Click(object sender, EventArgs e)
    {
        CurrentPage--;
        doPaging();
    }

    protected void btnNext_Click(object sender, EventArgs e)
    {
        CurrentPage++;
        doPaging();
    }


    protected void btnFirst_Click(object sender, EventArgs e)
    {
        CurrentPage = 0;
        doPaging();
    }

    protected void btnLast_Click(object sender, EventArgs e)
    {
        CurrentPage = (int)Session["count"];
        doPaging();
    }
    #endregion
    //clear All filters
    protected void lnkClear_Click(object sender, EventArgs e)
    {
        try
        {
            CurrentPage = 0;
            totalResultList = (FleetSearchResult[])Session["searchResult"];
            filteredResultList = totalResultList;
            Session["filteredResultList"] = filteredResultList;
            txtFleetName.Text = string.Empty;
            ddlPriceSort.SelectedIndex = 0;
            doPaging();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.FleetSearch, Severity.High, 0, "Clear method FleetResults Page Error message :" + ex.ToString() + ex.StackTrace, "");
        }
    }
    //Filter Desc and FleetName
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            List<FleetSearchResult> filteredResults = new List<FleetSearchResult>();
            totalResultList = (FleetSearchResult[])Session["searchResult"];
            string filterByFleetName = txtFleetName.Text.Trim();
            string priceSortBy = ddlPriceSort.SelectedItem.Value;
            if (totalResultList.Length > 0)
            {
                for (int i = 0; i < totalResultList.Length; i++)
                {
                    FleetSearchResult result = totalResultList[i];
                    //FleetName Filtering
                    if (filterByFleetName.Length > 0) //checking fleetName length
                    {
                        if (totalResultList[i].FleetName.ToLower().Contains(filterByFleetName.Trim().ToLower()))
                        {
                            filteredResults.Add(totalResultList[i]);
                        }
                    }
                    else
                    {
                        filteredResults.Add(totalResultList[i]);
                    }
                }
            }
            filteredResultList = filteredResults.ToArray();
            if (filteredResults.Count > 0)
            {
                //Price Sorting
                Array.Sort(filteredResultList, delegate(FleetSearchResult h1, FleetSearchResult h2) { return h1.TotalPrice.CompareTo(h2.TotalPrice); });
                if (priceSortBy == "1")
                {
                    Array.Sort(filteredResultList, delegate(FleetSearchResult h1, FleetSearchResult h2) { return h2.TotalPrice.CompareTo(h1.TotalPrice); });
                    //Array.Reverse(filteredResultList);
                }
            }
            //saving filter results in session
            Session["filteredResultList"] = filteredResultList;
            filteredResults.Clear();
            CurrentPage = 0;
            doPaging();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.FleetSearch, Severity.High, 0, "search event method in FleetResult Page Error message :" + ex.ToString() + ex.StackTrace, "");
        }
    }
    //binding all search results
    protected void dlFleet_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
            FleetSearchResult resultObj = e.Item.DataItem as FleetSearchResult;
            Image imgLogo = e.Item.FindControl("imgLogo") as Image;
            Image imgLogoLarge = e.Item.FindControl("imgLogoLarge") as Image;
            Label lblSeat = e.Item.FindControl("lblSeat") as Label;
            Label lblEngine = e.Item.FindControl("lblEngine") as Label;
            Label lblBaggage = e.Item.FindControl("lblBaggage") as Label;
            Label lblTitle = e.Item.FindControl("lblTitle") as Label;
            Label lblCarAc = e.Item.FindControl("lblCarAc") as Label;
            Label lblCurrency = e.Item.FindControl("lblCurrency") as Label;
            Label lblDays = e.Item.FindControl("lblDays") as Label;
            Label lblPrice = e.Item.FindControl("lblPrice") as Label;
            Label lblStrikeOut = e.Item.FindControl("lblStrikeOut") as Label;
            Label lblStrikeCurrency = e.Item.FindControl("lblStrikeCurrency") as Label;
            Button btnRent = e.Item.FindControl("btnRentNow") as Button;

            if (resultObj != null)
            {
                btnRent.PostBackUrl = "FleetGuestDetails.aspx?Id=" + resultObj.ID;

                string imagePath = ConfigurationSystem.SAYARAConfig["FleetLogoPath"];
                if (System.IO.File.Exists(Server.MapPath(imagePath + resultObj.SRNO + ".jpg")))
                {
                    imgLogo.ImageUrl = imagePath + resultObj.SRNO + ".jpg";
                    imgLogoLarge.ImageUrl = imagePath + resultObj.SRNO + ".jpg";
                }
                else
                {
                    imgLogo.ImageUrl = imagePath + "car-demo-image.jpg";
                    imgLogoLarge.ImageUrl = imagePath + "car-demo-image.jpg";
                }
                //Binding All Specs
                lblTitle.Text = resultObj.FleetName;
                if (resultObj.FleetSpecs != null)
                {
                    foreach (FleetSpecifications spec in resultObj.FleetSpecs)
                    {
                        if (resultObj.SRNO == spec.SrNo)
                        {
                            switch (spec.SpecName)
                            {
                                case "SEAT":
                                    lblSeat.Text = spec.SpecDetails;
                                    break;
                                case "ENG":
                                    lblEngine.Text = spec.SpecDetails;
                                    break;
                                case "AC":
                                    lblCarAc.Text = spec.SpecDetails;
                                    break;
                                case "LUGGAGE":
                                    lblBaggage.Text = spec.SpecDetails;
                                    break;
                            }
                        }
                    }
                }
                int days = (int)Math.Ceiling(request.ToDateTime.Subtract(request.FromDateTime).TotalDays);
                lblDays.Text = resultObj.Days.ToString() + " day(s)";
                lblCurrency.Text = resultObj.PriceInfo.Currency;
                //Calucating fleetAmount Based on Rent Type
                switch (resultObj.RentType)
                {
                    case RentType.Daily:
                        lblPrice.Text = Math.Round(resultObj.PriceInfo.NetFare + resultObj.PriceInfo.Markup - resultObj.PriceInfo.Discount, resultObj.PriceInfo.DecimalPoint).ToString("N" + resultObj.PriceInfo.DecimalPoint);
                        break;
                    case RentType.Weekly:
                        lblPrice.Text = Math.Round(resultObj.PriceInfo.NetFare + resultObj.PriceInfo.Markup - resultObj.PriceInfo.Discount, resultObj.PriceInfo.DecimalPoint).ToString("N"+ resultObj.PriceInfo.DecimalPoint);
                        lblStrikeOut.Visible = true;
                        lblStrikeCurrency.Visible = true;
                        lblStrikeCurrency.Text = resultObj.PriceInfo.Currency;
                        lblStrikeOut.Text = Math.Round(resultObj.PriceInfo.OtherCharges,resultObj.PriceInfo.DecimalPoint).ToString("N"+ resultObj.PriceInfo.DecimalPoint);
                        break;
                    case RentType.Monthly:
                        lblPrice.Text = Math.Round(resultObj.PriceInfo.NetFare + resultObj.PriceInfo.Markup - resultObj.PriceInfo.Discount, resultObj.PriceInfo.DecimalPoint).ToString("N" + resultObj.PriceInfo.DecimalPoint);
                        lblStrikeOut.Visible = true;
                        lblStrikeCurrency.Visible = true;
                        lblStrikeCurrency.Text = resultObj.PriceInfo.Currency;
                        lblStrikeOut.Text = Math.Round(resultObj.PriceInfo.OtherCharges,resultObj.PriceInfo.DecimalPoint).ToString("N"+ resultObj.PriceInfo.DecimalPoint);
                        break;
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.FleetSearch, Severity.High, 0, "Fllet Search Result Binding Page Error message :" + ex.ToString() + ex.StackTrace, "");
        }
    }
    //modify Search
    protected void btnModifySearch_Click(object sender,EventArgs e)
    {
        try
        {
            Clear(); //Clear All Sessions
            //creating fleetRequest
            FleetRequest fleetReq = new FleetRequest();
            string fromDatetime = CheckIn.Text.Trim() + " " + ddlPickuptime.SelectedItem.Value;
            string toDatetime = CheckOut.Text.Trim() + " " + ddlReturntime.SelectedItem.Value;
            fleetReq.FromCity = ddlPickupCity.SelectedItem.Text;
            fleetReq.FromCityCode = ddlPickupCity.SelectedItem.Value;
            fleetReq.FromLocation = ddlPickupLocation.SelectedItem.Text;
            fleetReq.FromLocationCode = ddlPickupLocation.SelectedItem.Value;

            IFormatProvider format = new CultureInfo("en-GB", true);
            fleetReq.FromDateTime = DateTime.Parse(fromDatetime, format);
            fleetReq.ToDateTime = Convert.ToDateTime(toDatetime, format);
            //selected diffrent city
            if (chkRetunLocation.Checked) //Checking Return Or not
            {
                fleetReq.ToCity = ddlReturnCity.SelectedItem.Text;
                fleetReq.ToCityCode = ddlReturnCity.SelectedItem.Value;
                fleetReq.ToLocation = ddlReturnLocation.SelectedItem.Text;
                fleetReq.ToLocationCode = ddlReturnLocation.SelectedItem.Value;
                fleetReq.ReturnStatus = "Y";
            }
            else
            {

                fleetReq.ReturnStatus = "N";
            }
            if (ddlCarType.SelectedIndex > 0)
            {
                fleetReq.CarType = ddlCarType.SelectedItem.Value;
            }
            else
            {
                fleetReq.CarType = string.Empty;
            }
            Session["FleetRequest"] = fleetReq;
            //navigating again same page
            Response.Redirect("FleetResults.aspx", false);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.FleetSearch, Severity.High, 0, "FleetSearch page " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
    //Clear all sessions
    private void Clear()
    {
        Session["filteredResultList"] = null;
        Session["searchResult"] = null;
        Session["fleetItinerary"] = null;
        Session["FleetRequest"] = null;
    }


    private void BindPickUpCity()
    {
        try
        {
            List<CT.BookingEngine.FleetCity> cityList = new List<CT.BookingEngine.FleetCity>();
            List<CT.BookingEngine.FleetLocation> locationList = new List<CT.BookingEngine.FleetLocation>();
            if (Session["LoactionList"] == null)
            {
                //Loading Citi and Location list
                Sayara.SayaraApi sayarRestApi = new Sayara.SayaraApi();
                cityList = sayarRestApi.GetCityList(ref locationList);
                if (cityList != null && cityList.Count > 0 && locationList != null && locationList.Count > 0)
                {
                    Session["CityList"] = cityList;
                    Session["LoactionList"] = locationList;
                }
            }
            else
            {
                locationList = Session["LoactionList"] as List<CT.BookingEngine.FleetLocation>;
            }
            ddlPickupLocation.Items.Clear();
            ddlPickupLocation.Items.Add(new ListItem("Select Location", "0"));
            //binding pickupLocation based on pickup city
            if (locationList != null)
            {
                foreach (FleetLocation location in locationList)
                {
                    if (ddlPickupCity.SelectedItem.Value.ToUpper() == location.CityCode.ToUpper())
                    {
                        ListItem pickupLocation = new ListItem(location.LocationName, location.LocationCode);
                        ddlPickupLocation.Items.Add(pickupLocation);
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, 0, "Exception Loading PickupLocation In FleetSearch Page. Message:" + ex.ToString(), Request["REMOTE_ADDR"]);

        }
    }

    private void BindReturnCity()
    {
        try
        {
            List<CT.BookingEngine.FleetCity> cityList = new List<CT.BookingEngine.FleetCity>();
            List<CT.BookingEngine.FleetLocation> locationList = new List<CT.BookingEngine.FleetLocation>();
            if (Session["LoactionList"] == null)
            {
                //Loading City and Location list
                Sayara.SayaraApi sayarRestApi = new Sayara.SayaraApi();
                cityList = sayarRestApi.GetCityList(ref locationList);
                if (cityList != null && cityList.Count > 0 && locationList != null && locationList.Count > 0)
                {
                    Session["CityList"] = cityList;
                    Session["LoactionList"] = locationList;
                }
            }
            else
            {
                locationList = Session["LoactionList"] as List<CT.BookingEngine.FleetLocation>;
            }
            ddlReturnLocation.Items.Clear();
            ddlReturnLocation.Items.Add(new ListItem("Select Location", "0"));
            //binding drop location baed on dropcity
            if (locationList != null)
            {
                foreach (FleetLocation location in locationList)
                {
                    if (ddlReturnCity.SelectedItem.Value.ToUpper() == location.CityCode.ToUpper())
                    {
                        ListItem returnLocation = new ListItem(location.LocationName, location.LocationCode);
                        ddlReturnLocation.Items.Add(returnLocation);
                    }
                }
            }
        }


        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, 0, "Exception Loading returnLocation In FleetSearch Page. Message:" + ex.ToString(), Request["REMOTE_ADDR"]);

        }
    }

    protected void ddlPickupCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            doPaging(); //Paging
            BindPickUpCity(); //binding pickupcity
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, 0, "Exception Loading PickupLocation In FleetSearch Page. Message:" + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    protected void ddlReturnCity_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            doPaging();//Paging
            BindReturnCity();//binding dropcity
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Search, Severity.High, 0, "Exception Loading returnLocation In FleetSearch Page. Message:" + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
    
}
