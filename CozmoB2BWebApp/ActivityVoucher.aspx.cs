﻿using System;
using System.Data;
using System.Collections;
using System.Configuration;
using System.Web.UI;
using CT.Core;
using CT.BookingEngine;
using System.IO;

public partial class ActivityVoucherGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    protected Activity activity = null;
    protected CT.TicketReceipt.BusinessLayer.AgentMaster agent;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            //Load Voucher for Queue
            if (Request.QueryString["ID"] != null)
            {
                activity = new Activity();
                activity.GetActivityForQueue(Convert.ToInt64(Request.QueryString["ID"]));
                int agentId = Convert.ToInt32(activity.TransactionHeader.Rows[0]["AgencyId"]);
                agent = new CT.TicketReceipt.BusinessLayer.AgentMaster(agentId);
                BindVoucher(activity);
            }
           
            string serverPath = "";
            string logoPath = "";
            if (Request.Url.Port > 0)
            {
                //serverPath = "http://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
                serverPath = Request.Url.Scheme + "://" + Request.Url.Host + ":" + Request.Url.Port + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }
            else
            {
                serverPath = Request.Url.Scheme + "://" + Request.Url.Host + Request.Url.AbsolutePath.Replace(Request.AppRelativeCurrentExecutionFilePath.Replace("~/", ""), "");
            }
            //to show agent logo
            if (agent.ID > 1)
            {


                logoPath = serverPath + ConfigurationManager.AppSettings["AgentImage"] + agent.ImgFileName;
                imgHeaderLogo.ImageUrl = logoPath;
            }
            else
            {
                imgHeaderLogo.ImageUrl = serverPath + "images/logo.jpg";
            } 

            if (Session["Activity"] != null)
            {
                //activity = Session["Activity"] as Activity;
                if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId != null)
                {
                    string myPageHTML = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    printableArea.RenderControl(htw);
                    myPageHTML = sw.ToString();
                    
                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                    toArray.Add(activity.TransactionDetail.Rows[0]["Email"].ToString());
                    toArray.Add(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentEmail);
                    toArray.Add(ConfigurationManager.AppSettings["holidaySupport"].ToString());
                    //Email.Send(ConfigurationManager.AppSettings["fromEmail"], customer.UserName, new System.Collections.Generic.List<string>(), "Activity Confirmation - " + activity.TransactionHeader.Rows[0]["TripId"].ToString(), "<p>Dear " + customer.FirstName + ",</p><p>Your Activity Booking is Confirmed.</p><p>Greetings,</br> Cozmo Travel</p>", new Hashtable());
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Activity Confirmation - " + activity.TransactionHeader.Rows[0]["TripId"].ToString(), myPageHTML, new Hashtable());
                }
                if (!string.IsNullOrEmpty(activity.SupplierEmail))
                {
                    string myPageHTML = "<p>Dear " + activity.SupplierName + ",</p><p> Activity Booking is Confirmed.</p><p>Greetings,</br> Cozmo Travel</p>";
                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                    toArray.Add(activity.SupplierEmail);
                    toArray.Add(ConfigurationManager.AppSettings["holidaySupport"].ToString());
                    //Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Activity(" + activity.TransactionHeader.Rows[0]["activityName"].ToString() + ")Confirmation - " + activity.TransactionHeader.Rows[0]["TripId"].ToString(), myPageHTML, new Hashtable());
                    CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Activity Confirmation - " + activity.TransactionHeader.Rows[0]["TripId"].ToString(), myPageHTML, new Hashtable());
                }
                Session.Remove("Activity");
            }
            
        }
        catch (Exception ex)
        {
            Session.Remove("Activity");
            Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
        }
    }

    /// <summary>
    /// 
    /// </summary>
    /// <param name="activity"></param>
    void BindVoucher(Activity activity)
    {
        try
        {
            lblActivityName.Text = activity.Name;
            lblBookingDate.Text = Convert.ToDateTime(activity.TransactionHeader.Rows[0]["CreatedDate"]).ToString("dd MMM yyyy");
            lblTransactionDate.Text = Convert.ToDateTime(activity.TransactionHeader.Rows[0]["Booking"]).ToString("dd MMM yyyy");
            lblCity.Text = activity.City;
            lblCountry.Text = activity.Country;
            lblDuration.Text = activity.DurationHours.ToString();
            lblLocation.Text = activity.City + ", " + activity.Country;
            lblTotal.Text = agent.AgentCurrency+" "+ Convert.ToDecimal(activity.TransactionHeader.Rows[0]["TotalPrice"]).ToString("N"+agent.DecimalValue);

            lblDropPoint.Text = activity.DropoffLocation;
            lblPickupPoint.Text = activity.PickupLocation;
            lblPickupTime.Text = activity.PickupDate.ToString("hh:mm tt");
            lblAdultCount.Text = activity.TransactionHeader.Rows[0]["Adult"].ToString();
            lblChildCount.Text = activity.TransactionHeader.Rows[0]["Child"].ToString();
            lblInfantCount.Text = activity.TransactionHeader.Rows[0]["Infant"].ToString();
            lblBookingRef.Text = activity.TransactionHeader.Rows[0]["TripId"].ToString();
            lblPassengers.Text = (Convert.ToInt32(activity.TransactionHeader.Rows[0]["Adult"]) + Convert.ToInt32(activity.TransactionHeader.Rows[0]["Child"]) + Convert.ToInt32(activity.TransactionHeader.Rows[0]["Infant"])).ToString();
            DataTable dtPassengers = activity.TransactionDetail;
            
            for (int i = 0; i < dtPassengers.Rows.Count; i++)
            {
                if (i == 0)//Lead Passenger
                {
                    DataRow row = dtPassengers.Rows[i];
                    lblLeadPassenger.Text = row["FirstName"].ToString() + " " + row["LastName"].ToString();
                    lblGuestName.Text = lblLeadPassenger.Text;
                    lblNationality.Text = row["Nationality"].ToString();
                    lblPhone.Text = row["phone"].ToString();
                }
                //lblPassengers.Text = dtPassengers.Rows.Count.ToString();
            }
        }
        catch { throw; }
    }
}

