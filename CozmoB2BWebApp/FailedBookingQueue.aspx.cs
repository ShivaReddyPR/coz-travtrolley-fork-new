using System;
using System.Collections.Generic;
using System.Web.UI;
using CT.BookingEngine;
using CT.Configuration;
using CT.TicketReceipt.BusinessLayer;

public partial class FailedBookingQueue :CT.Core.ParentPage// System.Web.UI.Page
{
    protected int recordsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["FailedBookingQueueRecordsPerPage"]);
    protected int noOfPages;
    protected int pageNo;
    protected int AdminId = 1;
    protected List<FailedBooking> fblist;
    protected string show = string.Empty;
    protected int rowCount;
    protected int lower;
    protected int upper;
    protected int j = 0;
    protected string errorMessage = string.Empty;
    protected int productTypeId = 0;

    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true; 
        AuthorizationCheck();
        Page.Title = "Failed Booking Queue";
        //if (!IsPostBack)
        //{
        if (Request.QueryString["Source"] != null && Request.QueryString["Source"] != string.Empty)
        {
            if (Request.QueryString["Source"] == ProductType.Flight.ToString())
            {
                productTypeId = (int)ProductType.Flight;
            }
            else if (Request.QueryString["Source"] == ProductType.Hotel.ToString())
            {
                productTypeId = (int)ProductType.Hotel;
                lblSearch.Text = "Doc No:";
                lblPNR.Text = "Doc No";
            }
        }
        //}
        if (PageNoString.Value != null)
        {
            pageNo = Convert.ToInt32(PageNoString.Value);
        }
        else
        {
            pageNo = 1;
        }
        lower = (pageNo * recordsPerPage) - (recordsPerPage - 1);
        upper = pageNo * recordsPerPage;
        if (Request["postback"] == "remove")
        {
            int flightId = FlightItinerary.GetFlightId(Request["pnr"]);
            if (flightId > 0)
            {
                FlightItinerary itinerary = new FlightItinerary(flightId);
                List<Ticket> ticketList = Ticket.GetTicketList(flightId);
                if (ticketList.Count == itinerary.Passenger.Length)
                {
                    FailedBooking.UpdateStatus(Request["pnr"], CT.BookingEngine.Status.Removed, Request["remarks"]);
                }
                else
                {
                    errorMessage = "This Booking can't be removed, as tickets are not saved for it. Please create ticket manually and try again";
                }
            }
            else
            {
                errorMessage = "This Booking can't be removed, as tickets are not saved for it. Please create ticket manually and try again";
            }
        }
        if (Request["saveAgain"] == "SaveAgain")
        {
            CT.MetaSearchEngine.MetaSearchEngine mse = new CT.MetaSearchEngine.MetaSearchEngine();
            string pnr = Request["pnr"];
            FailedBooking fb = new FailedBooking();
            fb = FailedBooking.Load(pnr);
            string fbItinerary = fb.ItineraryXML;
            HotelItinerary itinerary = new HotelItinerary();
            itinerary = FailedBooking.GenerateItinerary(fbItinerary);
            mse.SaveHotelBookingFromFailed(itinerary);

            //Invoice genarated   added by brahmam
            int invoiceNumber = Invoice.isInvoiceGenerated(itinerary.Roomtype[0].RoomId, ProductType.Hotel);
            Invoice invoice = new Invoice();
            if (invoiceNumber > 0)
            {
                invoice.Load(invoiceNumber);
            }
            else
            {
                invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.HotelId, string.Empty, itinerary.CreatedBy, ProductType.Hotel, itinerary.Roomtype[0].Price.RateOfExchange);
                invoice.Load(invoiceNumber);
                invoice.Status = InvoiceStatus.Paid;
                invoice.CreatedBy = itinerary.CreatedBy;
                invoice.LastModifiedBy = itinerary.CreatedBy;
                invoice.UpdateInvoice();
            }
            //Agent balance deduct
            decimal total = 0;
            foreach (HotelRoom room in itinerary.Roomtype)
            {
                ///room.Price.pri
                if (room.Price.AccPriceType == PriceType.NetFare)
                {
                    total += (room.Price.NetFare + room.Price.Markup + room.Price.Tax);
                }
                else
                {
                    total += (room.Price.PublishedFare + room.Price.Tax);
                }
                
            }
            //No need to deduct from agent balance as it is addl markup only. We are not adding markup to total
            //so no need to deduct addl markup from total now. commented by shiva on 28 Aug 2014 4:30 PM
            //total = Math.Ceiling(total) - Convert.ToDecimal(Session["Markup"]);
            total = Math.Ceiling(total);

            //Update agent balance, reduce the hotel booking amount from the balance.
            AgentMaster agency = new AgentMaster(itinerary.AgencyId);
            agency.CreatedBy = Settings.LoginInfo.UserID;
            agency.UpdateBalance(-total);

            if (itinerary.AgencyId == Settings.LoginInfo.AgentId)
            {
                Settings.LoginInfo.AgentBalance -= total;
            }
            FailedBooking.UpdateStatus(pnr, CT.BookingEngine.Status.Saved, "Hotel Booking is Saved");
        }
        if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId == 1)
        {
            if (Request["inputPNRsearch"] != null && Request["inputPNRsearch"] != string.Empty)
            {
                FailedBooking temp = FailedBooking.Load(Request["inputPNRsearch"].Trim());
                if (temp.PNR != null)
                {
                    fblist = new List<FailedBooking>();
                    fblist.Add(temp);
                }
            }
            else
            {
                fblist = FailedBooking.GetAllFailedBooking(lower, upper,productTypeId);
                rowCount = FailedBooking.GetCount(productTypeId);
            }
        }
        else
        {
            if (Request["inputPNRsearch"] != null && Request["inputPNRsearch"] != string.Empty)
            {
                FailedBooking temp = FailedBooking.Load(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId, Request["inputPNRsearch"].Trim());
                if (temp.PNR != null)
                {
                    fblist = new List<FailedBooking>();
                    fblist.Add(temp);
                }
            }
            else
            {
                fblist = FailedBooking.GetAllFailedBooking(Convert.ToInt32(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId), lower, upper,productTypeId);
                rowCount = FailedBooking.GetCount(Convert.ToInt32(CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.AgentId),productTypeId);
            }
        }
        string url = "FailedBookingQueue.aspx?";
        if ((rowCount % recordsPerPage) != 0)
        {
            noOfPages = (rowCount / recordsPerPage) + 1;
        }
        else
        {
            noOfPages = (rowCount / recordsPerPage);
        }
        if (rowCount > recordsPerPage)
        {
            show = CT.MetaSearchEngine.MetaSearchEngine.PagingJavascript(noOfPages, url, pageNo);
        }
        
    }

    private void AuthorizationCheck()
    {
        if (CT.TicketReceipt.BusinessLayer.Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx", true);
        }

    }
}
