﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="OfflineEmailApproval.aspx.cs" Inherits="CozmoB2BWebApp.OfflineEmailApproval" %>

<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>

    <link href="build/css/main.min.css?V1" rel="stylesheet" type="text/css" />
    <link href="css/override.css" rel="stylesheet" />
    <link href="css/bootstrap.min.css" rel="stylesheet" />
    <style>
        @media print{.receipt-wrapper{color:#000!important}}
        @media print{.receipt-visitor-info th{color:#000!important}}
        @media print{.receipt-wrapper{font-family:Arial,Helvetica,sans-serif;width:100%}.receipt-visitor-info th{color:#fff;text-shadow:0 0 0 #cfffcc}}
        @media print and (-webkit-min-device-pixel-ratio:0){.receipt-visitor-info th{color:#fff;-webkit-print-color-adjust:exact}}
    </style>

    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.6.2/jquery.min.js"></script>
    <script type="text/javascript" src="Scripts/Common/Common.js" ></script>
    <script type="text/javascript">

        var mt = ''; var apiAgentInfo = mt; var selOffId = 0; var authError = '';
        var repAction = mt; var approverId = 0; var prodId = 0;

        $(document).ready(function() {

            authError = '<%=authError%>';
            approverId = '<%=Request.QueryString["DIRPPA"] == null ? "0" : Request.QueryString["DIRPPA"]%>';

            /* Check login user authentication error */
            if (!IsEmpty(authError)) {

                alert(authError);
                return;
            }
            
            /* Prepare agent and login info for expense web api request call and assign to global variable */
            GetAgentInfo();
            
            /* Check the session info and revert if expired */
            if (IsEmpty(apiAgentInfo) || approverId == 0) {

                alert('User authentication failed.');
                return;
            }

            /* Check query string to see to approve/reject transactions */
            selOffId = '<%=Request.QueryString["DIFFO"] == null ? "0" : Request.QueryString["DIFFO"]%>';
            repAction = '<%=Request.QueryString["TATS"] == null ? string.Empty : Request.QueryString["TATS"]%>';
            prodId = '<%=Request.QueryString["DIP"] == null ? "0" : Request.QueryString["DIP"]%>';

            /* Check the offline ref no */
            if (selOffId == 0) {

                alert('Invalid transaction reference no.');
                return;
            }

            /* To show comments section for reject status */
            if (repAction == 'R') {
                                
                $('#divComments').show();
                return false;
            }                

            /* Update approval/reject status */
            UpdateOfflineStatus();      
        });

        /* To Update approval/reject status */
        function UpdateOfflineStatus() {
                        
            if (repAction != 'A' && IsEmpty($('#txtComments').val())) {

                alert('Please enter comments.');
                return;
            }

            var webAppHost = '<%=Request.Url.Scheme + "://" + Request["HTTP_HOST"]%>';
            document.getElementById('ctl00_upProgress').style.display = 'block';

            setTimeout(function () {
                
                var data = AjaxCall("OfflineBookingQueue.aspx/SaveApprovalStatus", "{'status':'" + repAction + "','transid':'" + selOffId
                    + "' ,'remarks':'" + $('#txtComments').val() + "','productId':'" + prodId + "','approver':'" + approverId
                    + "','hostName':'" + webAppHost + "'}");

                if (data == "S")                                         
                    alert('Status updated Successfully');
                else if (!IsEmpty(data))                 
                    alert(data);

                document.getElementById('ctl00_upProgress').style.display = 'none';

            }, 100);           
        }

        /* To get agent and login info */
        function GetAgentInfo() {

            try {

                var loginInfo = '<%=Settings.LoginInfo == null%>';

                if (loginInfo == true)
                    return apiAgentInfo;

                var agentId = '<%=Settings.LoginInfo.AgentId%>';
                var loginUser = '<%=Settings.LoginInfo.UserID%>';
                var loginUserCorpProfile = '<%=Settings.LoginInfo.CorporateProfileId%>';
                var behalfLocation = '<%=Settings.LoginInfo.OnBehalfAgentLocation%>';
                var ipAddress = '<%=HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]%>';
                apiAgentInfo = BindAgentInfo(agentId, behalfLocation, loginUser, loginUserCorpProfile, ipAddress);
            }
            catch (excp) {
                var exception = excp;
            }
            return apiAgentInfo;
        }

    </script>
</head>
<body>
    <form id="form1">
        <div id="divComments" style="display:none;">
            <label>Comments</label>
            <textarea class="form-control" style="width:500px;margin-bottom:20px;" id="txtComments" placeholder="Enter Comments"></textarea>
            <a id="lnkSubmit" class="btn btn-danger" onclick="return UpdateOfflineStatus();">Reject</a>
        </div>
        <div id="ctl00_upProgress" style="display:none">                        
            <div class="lds-loader-wrap">
                <div class="lds-loader-inner"><div class="lds-roller"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div></div>
            </div>
        </div>
    </form>
</body>
</html>
