﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Web.UI.WebControls;
using CT.BookingEngine.Insurance;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using System.Linq;

public partial class InsuranceConfirmation :CT.Core.ParentPage// System.Web.UI.Page
{
    protected InsuranceResponse selectedPlans;
    protected InsuranceHeader header;
    protected InsuranceRequest InsRequest;
    protected string imagePath;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            lblBalanceEtrror.Text = string.Empty;
            if(Session["InsRequest"]!=null)
                InsRequest = Session["InsRequest"] as InsuranceRequest;
            imagePath += ConfigurationManager.AppSettings["RootFolder"];
            if (!IsPostBack)
            {
                if (Session["SelectedPlans"] != null && Session["Header"] != null)
                {
                    selectedPlans = Session["SelectedPlans"] as InsuranceResponse;
                    header = Session["Header"] as InsuranceHeader;
                    //lblDescription.Text = header.Description;

                    //lblChargeType.Text = plan.PremiumChargeType.ToString();
                    int totalPax = header.Adults + header.Childs + header.Infants;
                    List<InsurancePlan> planList = new List<InsurancePlan>();
                    //OTA plans Loading
                    decimal TotalAmount = 0;
                    header.OutputVATAmount = 0;
                    if (selectedPlans != null && selectedPlans.OTAPlans != null)
                    {
                        foreach (InsurancePlanDetails planDetails in selectedPlans.OTAPlans)
                        {
                            InsurancePlan insPlan = new InsurancePlan();
                            insPlan = LoadPlanDetails(planDetails);
                            TotalAmount = insPlan.NetAmount + insPlan.Markup+ insPlan.InputVATAmount + insPlan.OutputVATAmount - insPlan.Discount;
                            header.OutputVATAmount += insPlan.OutputVATAmount;
                            planList.Add(insPlan);
                        }
                    }
                    //Loading UPSell plans
                    if (selectedPlans != null && selectedPlans.UpsellPlans != null)
                    {                       
                        foreach (InsurancePlanDetails planDetails in selectedPlans.UpsellPlans)
                        {
                            InsurancePlan insPlan = new InsurancePlan();
                            insPlan = LoadPlanDetails(planDetails);
                            TotalAmount += insPlan.NetAmount + insPlan.InputVATAmount+ insPlan.Markup +insPlan.OutputVATAmount - insPlan.Discount;
                            header.OutputVATAmount += insPlan.OutputVATAmount;
                            planList.Add(insPlan);
                        }
                    }
                    header.InsPlans = planList;                   
                    header.SessionId = selectedPlans.SessionId;
                    header.TotalAmount = TotalAmount;
                    lblPaxCount.Text = (header.Adults + header.Childs + header.Infants).ToString();
                    lblQualPax.Text = lblPaxCount.Text;
                    lblVATamount.Text = Settings.LoginInfo.Currency + " " + (header.OutputVATAmount).ToString("N" + Settings.LoginInfo.DecimalValue);
                    lblAmount.Text = Settings.LoginInfo.Currency + " " + (header.TotalAmount).ToString("N" + Settings.LoginInfo.DecimalValue);

                    dlPaxList.DataSource = header.InsPassenger;
                    dlPaxList.DataBind();

                    Session["Header"] = header;

                    lblAgentBalance.Text = Settings.LoginInfo.Currency + " " + Settings.LoginInfo.AgentBalance.ToString("N" + Settings.LoginInfo.DecimalValue);

                    lblAmountPaid.Text = Settings.LoginInfo.Currency + " " + header.TotalAmount.ToString("N" + Settings.LoginInfo.DecimalValue);
                }
                else
                {
                    Audit.Add(EventType.Exception, Severity.High, 1, "Insurance confirmation page session exparied", Request["REMOTE_ADDR"]);
                    Response.Redirect("Insurance.aspx", false);
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "InsuranceConfirmation page load: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
    //Loading selected plan details 
    private InsurancePlan LoadPlanDetails(InsurancePlanDetails planDetails)
    {
        InsurancePlan insplan = new InsurancePlan();
        insplan.PlanTitle = planDetails.Title;
        insplan.PlanDescription = planDetails.Description;
        insplan.ItineraryID = "";
        insplan.PolicyNo = "";
        insplan.PolicyPurchasedDate = DateTime.Now;
        insplan.SourceAmount = planDetails.SourceAmount;
        insplan.SourceCurrency=planDetails.SourceCurrency;
        insplan.NetAmount=planDetails.NetFare;
        insplan.Markup=planDetails.Markup;
        insplan.MarkupType=planDetails.MarkupType;
        insplan.MarkupValue=planDetails.MarkupValue;
        insplan.Discount=planDetails.Discount;
        insplan.DiscountType=planDetails.DiscountType;
        insplan.DiscountValue=planDetails.DiscountValue;
        insplan.Currency=planDetails.CurrencyCode;

        // Output VAT Calculation.
        decimal outPutVAT = 0m,totalAmount = 0m;
        LocationMaster location = new LocationMaster(Settings.LoginInfo.LocationID);
        insplan.TaxDetail = planDetails.TaxDetail;
        insplan.InputVATAmount = planDetails.InputVATAmount;
        totalAmount = insplan.NetAmount + insplan.Markup - insplan.Discount;       
        if (planDetails.TaxDetail != null && planDetails.TaxDetail.OutputVAT != null && planDetails.TaxDetail.OutputVAT.Applied)
        {           
            outPutVAT = planDetails.TaxDetail.OutputVAT.CalculateVatAmount(totalAmount, insplan.Markup , Settings.LoginInfo.DecimalValue);
        }       
        insplan.OutputVATAmount = outPutVAT;

        if (planDetails.PlanPriceBreakDown != null && planDetails.PlanPriceBreakDown.Count > 0)
        {
            insplan.PremiumAmount = Convert.ToDecimal(planDetails.PlanPriceBreakDown[0].PremiumAmount) * planDetails.ROE;
        }
        if (planDetails.PlanType == "OTA")
        {
            insplan.PremiumChargeType = planDetails.PremiumChargeType.ToString();
        }
        else
        {
            insplan.PremiumChargeType = planDetails.UPsellPremiumChargeType.ToString();
        }
        insplan.InsPlanCode = planDetails.Code;
        insplan.SSRFeeCode = planDetails.SSRFeeCode;
        insplan.ROE = planDetails.ROE;
        return insplan;
    }
    protected void btnPayment_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["Header"] != null)
            {
                header = Session["Header"] as InsuranceHeader;
                
                int paxCnt = 0;
                foreach (InsurancePassenger pax in header.InsPassenger)
                {
                    if (paxCnt == 0)
                    {
                        header.ContactPersonName = pax.FirstName + pax.LastName;
                        header.EmailAddress = pax.Email;
                    }
                    break;
                }
                AgentMaster agent = new AgentMaster(Settings.LoginInfo.AgentId);

                header.PaymentMode = agent.PaymentMode == PaymentMode.Credit_Limit ? ModeOfPayment.CreditLimit : ModeOfPayment.Credit;

                decimal currentAgentBalance = agent.UpdateBalance(0);
                if (currentAgentBalance > Convert.ToDecimal(header.TotalAmount))
                {
                    MetaSearchEngine mse = new MetaSearchEngine();
                    mse.SettingsLoginInfo = Settings.LoginInfo;
                    mse.ConfirmPolicyPurchase(ref header);
                    if (header.Error == "0")
                    {
                        //Invoice 
                        try
                        {
                            Invoice invoice = new Invoice();
                            int invoiceNumber = CT.BookingEngine.Invoice.isInvoiceGenerated(header.Id, CT.BookingEngine.ProductType.Insurance);

                            if (invoiceNumber > 0)
                            {
                                invoice.Load(invoiceNumber);
                            }
                            else
                            {
                                invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(header.Id, "", (int)Settings.LoginInfo.UserID, CT.BookingEngine.ProductType.Insurance, 1);
                                if (invoiceNumber > 0)
                                {
                                    invoice.Load(invoiceNumber);
                                    invoice.Status = CT.BookingEngine.InvoiceStatus.Paid;
                                    invoice.CreatedBy = (int)Settings.LoginInfo.UserID;
                                    invoice.LastModifiedBy = (int)Settings.LoginInfo.UserID;
                                    invoice.UpdateInvoice();
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, Request.ServerVariables["REMOTE_ADDR"]);
                        }

                        //Deduct the premium amount from the Agent Balance and display vouhcer

                        agent.CreatedBy = Settings.LoginInfo.UserID;
                        currentAgentBalance = agent.UpdateBalance(-(decimal)(header.TotalAmount));
                        //Settings.LoginInfo.AgentBalance -= (decimal)plan.TotalPremiumAmount;
                        Settings.LoginInfo.AgentBalance = currentAgentBalance;
                        Label lblAgentBalance = (Label)Master.FindControl("lblAgentBalance");
                        lblAgentBalance.Text = CT.TicketReceipt.Common.Formatter.ToCurrency(currentAgentBalance);
                        
                        Response.Redirect("InsuranceVoucher.aspx?InsId=" + header.Id, false);
                    }
                    else
                    {
                        lblError.Text = header.Error;
                        MultiView1.ActiveViewIndex = 1;
                        Session["SelectedPlans"] = null;
                        Session["Header"] = null;
                    }
                }
                else
                {
                    lblBalanceEtrror.Text = "Insufficient Credit Balance ! please contact Admin";
                    MultiView1.ActiveViewIndex=0;
                }
            }
            else
            {
                Response.Redirect("Insurance.aspx",true);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "Failed to confirm Insurance: " + ex.ToString(), Request["REMOTE_ADDR"]);
            Session["SelectedPlans"] = null;
            Session["Header"] = null;
        }
    }
}
