<%@ Page Language="C#" MasterPageFile="~/TransactionVisaTitle.master" AutoEventWireup="true" Inherits="TermsMasterUI" Title="Terms Master" Codebehind="GVTermsMaster.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionVisaTitle.master" %>
<%@ Register Src="~/DateControl.ascx" TagName="DateControl" TagPrefix="dc" %>
<%@ Register Assembly="CT.TicketReceipt.Web.UI.Controls" Namespace="CT.TicketReceipt.Web.UI.Controls"
    TagPrefix="cc1" %>
<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">

    <%--<asp:UpdatePanel ID="upnlLocation" runat="server" UpdateMode="conditional">
   <contenttemplate> --%>
    <script type="text/javascript" src="ckeditor/ckeditor.js" >
        </script>
    <div class="body_container">


        <center>
   
    <input type="hidden" id="hdnDescription" value="" runat="server" />
    
     <table cellpadding="0" cellspacing="0" border="0">
                <tr>
                    <td style="height:10px">
                    </td>
                </tr>
                
                <tr class="trpad paramcon">
                   
                     <td height="24px" align="right"><asp:Label ID="lblHeader" runat="server" Text="Header"></asp:Label><span style="color:red">*:</span></td>
                     <td align="left" colspan="3"><asp:TextBox ID="txtHeader" CssClass="inputEnabled form-control" runat="server" Width="370px"></asp:TextBox></td>
               </tr>     
             <tr class="trpad paramcon">
                    
                    <td height="24px"  align="right"><asp:Label ID="lblCountry" runat="server" Text="Country"></asp:Label><span style="color:red">*:</span></td>
                    <td align="left"><asp:DropDownList  ID="ddlCountry"  CssClass="inputDdlEnabled form-control" runat="server" Width="154px"  AutoPostBack="true" OnSelectedIndexChanged="ddlCountry_SelectedIndexChanged"></asp:DropDownList></td>
                    
                    <td align="right"><asp:Label ID="lblNationality" runat="server" Text="Nationality"></asp:Label><span style="color:red">*:</span></td>
                    <td align="left"><asp:DropDownList  ID="ddlNationality"  CssClass="inputDdlEnabled form-control" runat="server" Width="154px" ></asp:DropDownList></td>               
                                                         
                </tr>
                
              
                <tr class="trpad paramcon">
                   
                    <td height="24px" align="right"><asp:Label ID="lblVisaType" runat="server" Text="Visa Type"> </asp:Label><span style="color:red">*:</span></td>
                    <td align="left"><asp:DropDownList ID="ddlVisaType"  CssClass="inputDdlEnabled form-control" runat="server" Width="154px" > </asp:DropDownList></td> 
                   
                    <td align="right"><asp:Label ID="lblResidence" runat="server" Text="Residence"></asp:Label><span style="color:red">*:</span></td>
                    <td align="left"><asp:DropDownList  ID="ddlResidence"  CssClass="inputDdlEnabled form-control" runat="server" Width="154px" ></asp:DropDownList></td>                
                   
                </tr>
               <tr class="trpad paramcon">
                    <td align="right" valign="top"><asp:Label ID="lblDescription" runat="server" Text="Description"></asp:Label><span style="color:red">*:</span></td>
                   <%--<td align="left" colspan="3"><textarea id="txtDescription" runat="server" name="txtDescription" rows="10" columns="80"></textarea></td>--%>
                    <%--<td align="left" colspan="3"><asp:TextBox  ID="txtDescription" TextMode="multiLine" Height="100px" CssClass="inputEnabled form-control" runat="server" Width="370px"></asp:TextBox></td>--%>
                   <td align="left" colspan="3">
                       <CKEditor:CKEditorControl ID="txtDescription" name="txtDescription"  runat="server"  FilebrowserImageUploadUrl ="/TicketReceipt/Upload.ashx" FilebrowserUploadUrl="/TicketReceipt/Upload.ashx">
                       </CKEditor:CKEditorControl>
                   </td>
                </tr>
               
                
                
              
                <tr>
                    <td colspan="4" align="right" >
                        <asp:Button ID="btnSave" runat="server" Text="Save" CssClass="btn but_b" OnClientClick="return Save();" OnClick="btnSave_Click"  />
                        <asp:Button ID="btnCancel" runat="server" Text="Clear" CssClass="btn but_b" OnClick="btnCancel_Click"  />
                        <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="btn but_b" OnClick="btnSearch_Click"  />
                    </td>                                                     
                </tr>             
                <tr>
                    <td colspan="4" align="left" style="height:20px">
                        <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ></asp:Label>
                        <asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError"></asp:Label>  
                    </td>
                </tr>
                </table>
    </center>

    </div>



    <%--</contenttemplate>
   </asp:UpdatePanel>--%>

    <script type="text/javascript">
        function countryChange() {
            for (var instance in CKEDITOR.instances) {
                var data = CKEDITOR.instances[instance].getData();
            }
            document.getElementById('ctl00_cphTransaction_hdnDescription').value = data;
            return true;
        }


        function Save() {

            //var dd=GetDateObject('ctl00_cphTransaction_dcPaxPspIssueDate');

            // alert(document.getElementById('ctl00_cphTransaction_dcSubmissionFrom_Time').value);

            //var dd=GetDateObject('ctl00_cphTransaction_dcSubmissionFrom');
            // alert(GetDateObject('ctl00_cphTransaction_dcSubmissionFrom'));
            //alert(Document.('ctl00_cphTransaction_dcSubmissionFrom_Date_Time').value)


            if (getElement('txtHeader').value == '') addMessage('Header cannot be blank!', '');
            if (CKEDITOR != undefined) {
                for (var instance in CKEDITOR.instances)
                    //var data = CKEDITOR.instances[instance].getData();
                    var data = CKEDITOR.instances[instance].document.getBody().getText();
                if (data == '') {
                    addMessage('Description cannot be blank!', '');
                }
                else {
                    document.getElementById('ctl00_cphTransaction_txtDescription').value = data;
                }
            }
            if (getElement('ddlCountry').selectedIndex <= 0) addMessage('Please Select Country from the List!', '');
            if (getElement('ddlNationality').selectedIndex <= 0) addMessage('Please Select Nationality from the List!', '');
            if (getElement('ddlVisaType').selectedIndex <= 0) addMessage('Please Select VisaType from the List!', '');
            if (getElement('ddlResidence').selectedIndex <= 0) addMessage('Please Select Residence from the List!', '');
            
           
            if (getMessage() != '') {
                alert(getMessage()); clearMessage(); return false;
            }
           // return true;
        }

        //$(document).ready(function () {
        //    CKEDITOR.replace('ctl00_cphTransaction_txtDescription');
        //   });

        function startScript() {
            var instance = CKEDITOR.instances['ctl00_cphTransaction_txtDescription'];
            if (instance) {
                CKEDITOR.remove(instance);
            }
            CKEDITOR.replace('ctl00_cphTransaction_txtDescription');
            
                document.getElementById('ctl00_cphTransaction_txtDescription').value = document.getElementById('ctl00_cphTransaction_hdnDescription').value;
            
        }
    </script>
</asp:Content>

<asp:Content ID="cntSearch" ContentPlaceHolderID="cphSearch" runat="Server">
    <asp:GridView ID="gvSearch" Width="100%" runat="server" AllowPaging="true" DataKeyNames="TERMS_ID"
        EmptyDataText="No Terms List!" AutoGenerateColumns="false" PageSize="10" GridLines="none" CssClass="grdTable"
        OnSelectedIndexChanged="gvSearch_SelectedIndexChanged" CellPadding="4" CellSpacing="0"
        OnPageIndexChanging="gvSearch_PageIndexChanging">

        <HeaderStyle CssClass="gvHeader" HorizontalAlign="left"></HeaderStyle>
        <RowStyle CssClass="gvDtlRow" HorizontalAlign="left" />
        <AlternatingRowStyle CssClass="gvDtlAlternateRow" />
        <Columns>
            <asp:CommandField ButtonType="Link" SelectText="<img src='Images/grid/wg_edit.gif' style='border:none' />" ControlStyle-CssClass="label" ShowSelectButton="True" />

            <asp:TemplateField>
                <ItemStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtHeader" Width="100px" HeaderText="Header" CssClass="inputEnabled form-control" OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblHeader" runat="server" Text='<%# Eval("terms_header") %>' CssClass="grdof" ToolTip='<%# Eval("terms_header") %>' Width="70px"></asp:Label>
                    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>--%>
                </ItemTemplate>

            </asp:TemplateField>

            <asp:TemplateField>
                <ItemStyle HorizontalAlign="left" />
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtDescription" Width="100px" HeaderText="Description" CssClass="inputEnabled form-control" OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="ITlblDescription" runat="server" Text='<%# Eval("terms_description") %>' CssClass="grdof" ToolTip='<%# Eval("terms_description") %>' Width="70px"></asp:Label>
                    <%--<asp:HiddenField id="IThdfVSId" runat="server" Value='<%# Bind("vs_id") %>'></asp:HiddenField>--%>
                </ItemTemplate>

            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtCountry" Width="150px" CssClass="inputEnabled form-control" HeaderText="Country" OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <%-- <ItemStyle HorizontalAlign="left" />--%>
                <ItemTemplate>
                    <asp:Label ID="ITlblCountry" runat="server" Text='<%# Eval("terms_country_name") %>' CssClass="grdof" ToolTip='<%# Eval("terms_country_name") %>' Width="120px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtNationality" Width="150px" CssClass="inputEnabled form-control" HeaderText="Nationality" OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <%-- <ItemStyle HorizontalAlign="left" />--%>
                <ItemTemplate>
                    <asp:Label ID="ITlblNationality" runat="server" Text='<%# Eval("terms_nationality_name") %>' CssClass="grdof" ToolTip='<%# Eval("terms_nationality_name") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTxtVisaType" Width="100px" CssClass="inputEnabled form-control" HeaderText="Visa Type" OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITlblCode" runat="server" Text='<%# Eval("terms_visa_type_name") %>' CssClass="grdof" ToolTip='<%# Eval("terms_visa_type_name") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>

            <asp:TemplateField>
                <HeaderTemplate>
                    <cc1:Filter ID="HTtxtResidence" Width="100px" CssClass="inputEnabled form-control" HeaderText="Residence" OnClick="FilterSearch_Click" runat="server" />
                </HeaderTemplate>
                <ItemStyle HorizontalAlign="left" />
                <ItemTemplate>
                    <asp:Label ID="ITlblName" runat="server" Text='<%# Eval("terms_residence_name") %>' CssClass="grdof" ToolTip='<%# Eval("terms_residence_name") %>' Width="150px"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>



        </Columns>
    </asp:GridView>

</asp:Content>

