﻿using System;
using System.Data;
using System.Collections;
using System.Web.UI.WebControls;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.GlobasVisa;
using CT.TicketReceipt.Web.UI.Controls;

public partial class GVVisaTypeMasterGUI : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            lblSuccessMsg.Text = string.Empty;

            AuthorizationCheck();
            if (!IsPostBack)
            {
                IntialiseControls();

            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    #region Private methods
    private void IntialiseControls()
    {
        try
        {
            BindCountry();
          //  BindAgent();
        }
        catch
        {
            throw;
        }
    }
    private void AuthorizationCheck()
    {
        try
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch
        {
            throw;
        }

    }
    private void BindCountry()
    {
        try
        {
            SortedList Countries = Country.GetCountryList();
            ddlCountry.DataSource = Countries;
            ddlCountry.DataTextField = "key";
            ddlCountry.DataValueField = "value";
            ddlCountry.DataBind();
            ddlCountry.Items.Insert(0, new ListItem("--Select Country--", "0"));
        }
        catch
        {
            throw;
        }
    }
    //private void BindAgent()
    //{
    //    try
    //    {
    //        ddlAgent.DataSource = AgentMaster.GetList(Settings.LoginInfo.CompanyID, Convert.ToString(Settings.LoginInfo.AgentType), Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
    //        ddlAgent.DataTextField = "Agent_Name";
    //        ddlAgent.DataValueField = "agent_id";
    //        ddlAgent.DataBind();
    //        ddlAgent.Items.Insert(0, new ListItem("--Select Agent--", "0"));
    //    }
    //    catch
    //    {
    //        throw;
    //    }
    //}
    private void Clear()
    {
        try
        {
            txtVisaTypeCode.Text = string.Empty;
            txtVisaTypeName.Text = string.Empty;
            ddlCountry.SelectedIndex = 0;
           // ddlAgent.SelectedIndex = 0;
            hdfVisaTypeId.Value = string.Empty;
        }
        catch
        {
            throw;
        }
    }
    private void BindSearch()
    {
        try
        {
            DataTable dt = VisaTypeMaster.GetList(ListStatus.Long, RecordStatus.All);
            if ( ddlCountry.SelectedIndex!= 0)
            {
                    DataRow[] drresult = dt.Select("COUNTRYNAME ='" + ddlCountry.SelectedItem.Text + "'");
                if (drresult.Length > 0)
                {
                    DataTable result = drresult.CopyToDataTable();
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvSearch, result);
                }
            }
            else
            {
                if (dt != null && dt.Rows.Count > 0)
                {
                    CommonGrid cg = new CommonGrid();
                    cg.BindGrid(gvSearch, dt);
                }
            }

        }
        catch
        {
            throw;
        }
    }
    private void Save()
    {
        try
        {
            VisaTypeMaster visaMaster = new VisaTypeMaster();
            if (Utility.ToInteger(hdfVisaTypeId.Value) > 0)
            {
                visaMaster.Id = Utility.ToLong(hdfVisaTypeId.Value);
            }
            else
            {
                visaMaster.Id = -1;
            }
            visaMaster.Code = txtVisaTypeCode.Text.Trim();
            visaMaster.Name = txtVisaTypeName.Text.Trim();
            visaMaster.CountryId = ddlCountry.SelectedValue;
            //visaMaster.AgentId =Utility.ToInteger(ddlAgent.SelectedValue);
            visaMaster.Status = "A";
            visaMaster.CreatedBy = Settings.LoginInfo.UserID;
            visaMaster.Save();
            lblSuccessMsg.Text=Formatter.ToMessage("Successfully",txtVisaTypeName.Text,hdfVisaTypeId.Value=="0"?CT.TicketReceipt.Common.Action.Saved:CT.TicketReceipt.Common.Action.Updated);
            Clear();
        }
        catch
        {
            throw;
        }
    }
    private void Edit(long visatypeid)
    {
        try
        {
            VisaTypeMaster vtmaster = new VisaTypeMaster(visatypeid);
            hdfVisaTypeId.Value =Utility.ToString(vtmaster.Id);
            txtVisaTypeCode.Text = vtmaster.Code;
            txtVisaTypeName.Text = vtmaster.Name;
            ddlCountry.SelectedValue = vtmaster.CountryId;
           // ddlAgent.SelectedValue = Utility.ToString(vtmaster.AgentId);
            
        }
        catch
        {

        }
    }
    #endregion
    #region ButtonEvents
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            BindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }  
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }
    #endregion
    #region GridEvents
    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            BindSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }

    }
    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            Clear();
            long visatypeid = Utility.ToInteger(gvSearch.SelectedValue);
            Edit(visatypeid);
            this.Master.HideSearch();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    #endregion
}
