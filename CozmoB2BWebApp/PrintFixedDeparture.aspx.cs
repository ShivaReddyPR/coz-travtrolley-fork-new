﻿using System;
using CT.Core;
using CT.BookingEngine;
using CT.Configuration;
using CT.TicketReceipt.BusinessLayer;

public partial class PrintFixedDepartureGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    #region variables
    protected Activity activity;
    protected string imageServerPath;
    protected decimal minPrice = 0;
    #endregion
    protected void Page_Load(object sender, EventArgs e)
    {
        imageServerPath = Convert.ToString(ConfigurationSystem.B2CSettingsConfig["hotelDealImagePath"]);

        if (Session["Activity"] == null)
        {
            Response.Redirect("ActivityResults.aspx");
        }
        else
        {
            try
            {
                activity = Session["Activity"] as Activity;

                minPrice = Convert.ToDecimal(activity.PriceDetails.Rows[0]["Total"]);
                dlRoomRates.DataSource = activity.PriceDetails;
                dlRoomRates.DataBind();
                dlFDItinerary.DataSource = activity.FixedItineraryDetails;
                dlFDItinerary.DataBind();
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }
        }
    }
    protected string CTCurrencyFormat(object currency)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + Settings.LoginInfo.DecimalValue);
        }
    }
}
