<%@ Page Language="C#" AutoEventWireup="true"
    Inherits="StaticPackagesGUI" MasterPageFile="~/TransactionBE.master" Title="Static Pakages" Codebehind="StaticPackages.aspx.cs" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
            
      



<div>
                          
                           <asp:LinkButton ID="btnFirst" CssClass="fcol_blue" runat="server" OnClick="btnFirst_Click">First</asp:LinkButton>
                            <asp:LinkButton CssClass="fcol_blue" ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" />
                            <asp:Label CssClass="fcol_blue" ID="lblCurrentPage" runat="server"></asp:Label>
                            <asp:LinkButton CssClass="fcol_blue" ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" />
                            <asp:LinkButton  CssClass="fcol_blue" ID="btnLast" runat="server" OnClick="btnLast_Click">Last</asp:LinkButton>
                          
                          </div>

<div class="ns-h3"> Search packages</div>


       <asp:DataList ID="dlPackages" runat="server" CellPadding="4" class="hotel_mblock"
            OnItemDataBound="dlPackages_ItemDataBound" DataKeyField="dealId" Width="100%" onitemcommand="dlPackages_ItemCommand">
            <ItemTemplate>
            
            
            <div class="marbot_10 bg_white pad_10 bor_gray"> 
            
            <div class="col-md-2">
            
            
  <asp:Image ID="imglogo" runat="server" Width="146px" Height="109" AlternateText='<%#Eval("DealName") %>' />
           
           
            <%--below dummy picture using for demo purpose firoz 11 july 2016--%>  
          <%--<img Width="100%" Height="109" src="<%=Request.Url.Scheme %>://cozmotravel.com/Images/PackageImages/168/24032016140149149_L.jpg"> --%>
           
             
             
             </div>
            <div class="col-md-8"> 
            <div> <b> <a class="fcol_blue" href="PackageDetails.aspx?packageId=<%#Eval("DealId") %>&packageName=<%#Eval("DealName") %>-<%#Eval("Nights") %>-Nights--Days">
                                                <%#Eval("DealName") %>
                                            </a></b> </div>
<div class="packagedays">
                                            <%#Eval("Nights")%>
                                            nights &
                                            <asp:Label ID="lblDays" runat="server" class="packagedays"></asp:Label>
                                            days</div>
<div class="package-description">
                                            <asp:Label ID="lblDescription" runat="server"></asp:Label></div>
                                            

                                            
            
            
            </div>
            <div class="col-md-2"> 
            
       <table width="100%" border="0" cellpadding="0" cellspacing="0">
                                <tbody>
                                    <tr>
                                        <td>
                                            <p class="lowest_price">
                                                Starting From</p>
                                            <h2 style=" font-size:18px" class="price">
                                                <b><%=CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.Currency %>
                                                    <%#Convert.ToDecimal(Eval("price")).ToString("N" + CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.DecimalValue)%>
                                                </b>
                                            </h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <span class="graycol">per person</span>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            &nbsp;
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="button">
                                            </label>
                                            <asp:Button ID="brnView" CssClass="but but_b" runat="server" Text="View Details" CommandName = "View" />
                                            <%--<a href="PackageDetails.aspx?packageId=<%#Eval("DealId") %>&packageName=<%#Eval("DealName") %>-<%#Eval("Nights") %>-Nights--Days">
                                                <img src="images/view_details_butt.gif" width="113" height="50" border="0" /></a>--%>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>     
            
            
            </div>
            
            
            <div class="clearfix"> </div>
            </div>
            
            
            
                
                
            </ItemTemplate>
        </asp:DataList>            


    
        <asp:Label ID="lblMessage" runat="server"></asp:Label>


</asp:Content>
