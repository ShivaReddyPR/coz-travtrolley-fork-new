﻿<%@ Page Title="UpdateHandling" Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="UpdateHandlingFee" Codebehind="UpdateHandlingFee.aspx.cs" %>

<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" runat="Server">
    <style type="text/css">
        .chkBoxList td {
            width: 160px;
        }
    </style>
    <script type="text/javascript">

        function validation() {

            if (document.getElementById('<%=ddlAgent.ClientID %>').selectedIndex <= 0) {
                document.getElementById('<%= errMess.ClientID %>').style.display = "block";
                document.getElementById('<%= errorMessage.ClientID %>').innerHTML = "Please select Agent!";
                return false;
            }


            var chklist = document.getElementById('<%= chkProduct.ClientID %>');
            var chkListinputs = chklist.getElementsByTagName("input");
            var count = 0;
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {
                    if (i < 2 || i == 4) {
                        if (document.getElementById('ctl00_cphTransaction_ddlSource_' + i).selectedIndex <= 0) {
                            document.getElementById('<%= errMess.ClientID %>').style.display = "block";
                        document.getElementById('<%= errorMessage.ClientID %>').innerHTML = "Please select Source!";
                            return false;
                        }
                    }
                    count = count + 1;
                }
            }
            if (count == 0) {
                document.getElementById('<%= errMess.ClientID %>').style.display = "block";
            document.getElementById('<%= errorMessage.ClientID %>').innerHTML = "Atleast one Product should be selected !";
                return false;
            }
            return true;
        }


        function Check(id) {
            var val = document.getElementById(id).value;
            if (val == '0.0000') {
                document.getElementById(id).value = '';
            }
        }

        function Set(id) {
            var val = document.getElementById(id).value;
            if (val == '' || val == '0.0000') {
                document.getElementById(id).value = '0.0000';
            }
        }
       function SetValue() {
          
            var chklist = document.getElementById('<%= chkProduct.ClientID %>');
            var chkListinputs = chklist.getElementsByTagName("input");
            var HandlingFee = "";
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked) {

                    HandlingFee = parseFloat(document.getElementById('ctl00_cphTransaction_txtHandlingFee_' + i).value);
                    if (isNaN(HandlingFee)) {
                        HandlingFee = 0;
                    }
                   
                    document.getElementById('ctl00_cphTransaction_txtHandlingFee_' + i).value = HandlingFee.toFixed(4);
                }
            }
        }


    </script>


    <div class="">

        <div class="ns-h3">Update Markup</div>
        <asp:HiddenField ID="hdnCount" runat="server" />

        <div class="paramcon bg_white bor_gray pad_10">


            <div class="col-md-12 padding-0 marbot_10">
                <div class="col-md-1 col-xs-2 marbot_10">
                    <asp:Label ID="lblAgent" runat="server" Text="Agent:" Font-Bold="true"></asp:Label>
                </div>


                <div class="col-md-2 col-xs-10 marbot_10">
                    <asp:DropDownList CssClass="form-control" ID="ddlAgent" runat="server" Style="display: block;" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlAgent_SelectedIndexChanged">
                    </asp:DropDownList>
                    <div id="errMess" runat="server" class="error_module" style="display: none;">
                        <div id="errorMessage" runat="server" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
                        </div>
                    </div>


                </div>


                <div class="col-md-6 col-xs-12">
                    <asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ForeColor="Green"></asp:Label>
                    <asp:Button ID="btnUpdate" CssClass="btn but_b" runat="server" Text="Update" OnClick="btnUpdate_Click"
                        OnClientClick="return validation();" />



                    <asp:Button ID="btnClear" runat="server" Text="Clear" CssClass="btn but_b" OnClick="btnClear_Click" />
                </div>



                <div class="clearfix"></div>
            </div>

            <div>

                <table cellpadding="0" width="100%" cellspacing="0" border="0">

                    <tr>
                        <td width="160px">
                            <div class=" table-responsive">
                                <asp:CheckBoxList ID="chkProduct" runat="server" Style="width: 100%; display: block" CssClass="chkBoxList"
                                    RepeatDirection="Horizontal" AutoPostBack="true" OnSelectedIndexChanged="chkProduct_SelectedIndexChanged">
                                    <asp:ListItem Text="Flight" Value="1"></asp:ListItem>
                                    <%--Insurance Added by phani 13/02/2019--%>
                                     <asp:ListItem Text="Insurance" Value="5"></asp:ListItem>
                                         
                                </asp:CheckBoxList>
                            </div>
                            <%--AutoPostBack="true" OnSelectedIndexChanged="chkProduct_OnSelectedIndexChanged"  onclick="javascript:agentValidate()""--%>
                        </td>
                    </tr>

                    <tr>
                        <td>
                            <div class=" table-responsive">
                                <table class="tblmarkup" id="tblMarkup" runat="server" style="width: 100%; height: 250px;">
                                </table>
                        </td>
                    </tr>
                </table>

            </div>

        </div>
        <div>

            <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click">First</asp:LinkButton>
            <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" />
            <asp:Label ID="lblCurrentPage" runat="server"></asp:Label>
            <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" />
            <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click">Last</asp:LinkButton>

        </div>


    </div>

    <div>

        <div id="Div1" runat="server" class="bg_white margin-top-10 table-responsive" style="width: 100%; overflow: auto; color: #000000;">
            <asp:DataList ID="dlMarkup" runat="server" AutoGenerateColumns="false" GridLines="Both"
                BorderWidth="0" CellPadding="0" CellSpacing="0" Width="100%" ForeColor="" ShowHeader="true"
                DataKeyField="MRId">
                <HeaderStyle BackColor="#333333" Font-Bold="true" ForeColor="White" HorizontalAlign="Left" />
                <HeaderTemplate>
                    <table width="100%" class="tblpax">
                        <tr height="20px">
                            <td width="140px" class="heading">Product Type
                            </td>
                            <td width="140px" class="heading">Agent Name
                            </td>
                            <td width="140px" class="heading">Source
                            </td>
                            <td width="60px" class="heading">Flight
                            </td>
                            <td width="60px" class="heading">Journey
                            </td>
                            <td width="60px" class="heading">Carrier
                            </td>
                           <%-- <td width="80px" class="heading">AgentMarkup
                            </td>
                            <td width="80px" class="heading">Our Commission
                            </td>
                            <td width="80px" class="heading">Markup
                            </td>

                            <td width="100px" class="heading">MarkupType
                            </td>
                            <td width="80px" class="heading">Discount
                            </td>
                            <td class="heading">DiscountType
                            </td>--%>
                            <td width="100px" class="heading">Handling Fee Type
                            </td>
                            <td width="100px" class="heading">Handling Fee
                            </td>
                        </tr>
                    </table>
                </HeaderTemplate>
                <ItemTemplate>
                    <table width="100%" class="tblpax">
                        <tr height="20px">
                            <td width="140px">
                                <%#Eval("productType")%>
                            </td>
                            <td width="140px" align="left">
                                <%#Eval("agent_name")%>
                            </td>
                            <td width="140px" align="left">
                                <%#Eval("SourceId")%>
                            </td>
                            <td width="80px" align="left" style="text-align: left">
                                <%#Eval("FlightType")%>
                            </td>
                            <td width="80px" align="left" style="text-align: left">
                                <%#Eval("JourneyType")%>
                            </td>
                            <td width="80px" align="left" style="text-align: left">
                                <%#Eval("CarrierType")%>
                            </td>
                           <%-- <td width="80px" align="left">
                                <%#Eval("AgentMarkup")%>
                            </td>
                            <td width="80px" align="left">
                                <%#Eval("OurCommission")%>
                            </td>
                            <td width="80px" align="left">
                                <%#Eval("Markup")%>
                            </td>
                            <td width="100px" align="left">
                                <%#Eval("MarkupTypeName")%>
                            </td>
                            <td width="80px" align="left">
                                <%#Eval("Discount")%>
                            </td>
                            <td width="100px" align="left">
                                <%#Eval("DiscountTypeName")%>
                            </td>--%>
                            <td style="text-align: left; width: 60px">
                                <%#Eval("HandlingFeeType")%>
                            </td>
                            <td style="text-align: left; width: 60px">
                                <%#Eval("HandlingFeeValue")%>
                            </td>
                        </tr>
                    </table>
                </ItemTemplate>
            </asp:DataList>


            <asp:Label ID="lblMessage" runat="server"></asp:Label>
        </div>
    </div>


</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" runat="Server">
</asp:Content>

