﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.Web.UI.Controls;
using System.Collections.Generic;
using CT.TicketReceipt.Common;

public partial class CorporateTripDetailsQueueUI : CT.Core.ParentPage
{
    protected int userId;
    protected int userCorpProfileId;
    private string REIM_SEARCH_SESSION_APPROVED = "_ReimSearchListApproved";
    private string REIM_SEARCH_SESSION_PENDING = "_ReimSearchListPending";
    private string REIM_SEARCH_SESSION_REJECTED = "_ReimSearchListRejected";

    private string REIM_SEARCH_SESSION_LTAPPROVED = "_ReimSearchListLTApproved";
    private string REIM_SEARCH_SESSION_LTPENDING = "_ReimSearchListLTPending";
    private string REIM_SEARCH_SESSION_LTREJECTED = "_ReimSearchListLTRejected";

    //Added for Hotel Tab
    private string REIM_SEARCH_SESSION_HTAPPROVED = "_ReimSearchListHTApproved";
    private string REIM_SEARCH_SESSION_HTPENDING = "_ReimSearchListHTPending";
    private string REIM_SEARCH_SESSION_HTREJECTED = "_ReimSearchListHTRejected";


    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            this.Master.PageRole = true;
            if (Settings.LoginInfo != null) //Authorisation Check -- if success
            {
                if (!Page.IsPostBack)
                {
                    userId = (int)Settings.LoginInfo.UserID;
                    IntialiseControls();
                }
                if(hdnBindData.Value=="true") //Only Tab click event only this will be true and calls the BindCorporateQueueData()
                {
                    BindCorporateQueueData();
                }
            }
            else//Authorisation Check -- if failed
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 1, "(CorporateTripDetailsQueue) Page Load Event Error: " + ex.Message, "0");
        }
    }
    private void IntialiseControls()
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            //dcReimFromDate.Value = DateTime.Now.AddMonths(-1);
            dcReimFromDate.Value = DateTime.Now;
            dcReimToDate.Value = DateTime.Now;
            dcLocalTripFromDate.Value = DateTime.Now;
            dcLocalTripToDate.Value = DateTime.Now;
            dcHotelFromDate.Value = DateTime.Now;//.AddMonths(-1);  //initializing current date for Hotel Tab
            dcHotelToDate.Value = DateTime.Now; //initializing current date for Hotel Tab

            int user_corp_profile_id = CorporateProfile.GetCorpProfileId((int)Settings.LoginInfo.UserID);
            if (user_corp_profile_id > 0)
            {
                this.userCorpProfileId = user_corp_profile_id;
            }

            bindEmployeesList();
            //Added product Type as parameter for bind data based on the tab selection
            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), Convert.ToInt32(ddlEmployee.SelectedItem.Value), "A",Convert.ToInt32(hdnProductType.Value));//Approved
            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), Convert.ToInt32(ddlEmployee.SelectedItem.Value), "P", Convert.ToInt32(hdnProductType.Value));//Pending
            bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), Convert.ToInt32(ddlEmployee.SelectedItem.Value), "R", Convert.ToInt32(hdnProductType.Value));//Rejected

            bindLocalTripSearch(Convert.ToDateTime(dcLocalTripFromDate.Value, dateFormat), Convert.ToDateTime(dcLocalTripToDate.Value, dateFormat), userId, "A");//Approved
            bindLocalTripSearch(Convert.ToDateTime(dcLocalTripFromDate.Value, dateFormat), Convert.ToDateTime(dcLocalTripToDate.Value, dateFormat), userId, "P");//Pending
            bindLocalTripSearch(Convert.ToDateTime(dcLocalTripFromDate.Value, dateFormat), Convert.ToDateTime(dcLocalTripToDate.Value, dateFormat), userId, "R");//Rejected



        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void bindEmployeesList()
    {
        DataTable dtEmployees = CorporateProfile.GetCorpProfilesList((int)Settings.LoginInfo.AgentId);

        ddlEmployee.DataSource = dtEmployees;
        ddlEmployee.DataTextField = "Name";
        ddlEmployee.DataValueField = "ProfileId";
        ddlEmployee.DataBind();
        ddlEmployee.Items.Insert(0, new ListItem("-Select Employee-", "0"));

        ddlLocalTripEmployees.DataSource = dtEmployees;
        ddlLocalTripEmployees.DataTextField = "Name";
        ddlLocalTripEmployees.DataValueField = "ProfileId";
        ddlLocalTripEmployees.DataBind();
        ddlLocalTripEmployees.Items.Insert(0, new ListItem("-Select Employee-", "0"));

        //Binding employees to dropdown for Hotel Tab
        ddlHtlEmployees.DataSource = dtEmployees;
        ddlHtlEmployees.DataTextField = "Name";
        ddlHtlEmployees.DataValueField = "ProfileId";
        ddlHtlEmployees.DataBind();
        ddlHtlEmployees.Items.Insert(0, new ListItem("-Select Employee-", "0"));

        //CorporateProfile cp = new CorporateProfile(Settings.LoginInfo.CorporateProfileId, (int)Settings.LoginInfo.AgentId);

        ///TRAVEL CORDINATOR
        //if (cp != null && !string.IsNullOrEmpty(cp.ProfileType) && cp.ProfileType == "TRAVELCORDINATOR")
        if (Settings.LoginInfo.MemberType==MemberType.SUPER || Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.TRAVELCORDINATOR)
        {
            ddlEmployee.Enabled = true;
            ddlLocalTripEmployees.Enabled = true;
            ddlEmployee.SelectedValue = "0";
            ddlLocalTripEmployees.SelectedValue = "0";
            ddlHtlEmployees.Enabled = true;
            ddlHtlEmployees.SelectedValue = "0";
        }
        else // if profile Type is not Travel Corordinator then auto seleting the employee in drop down and disabling control.
        {
            ddlEmployee.Enabled = false;
            ddlEmployee.SelectedValue = Convert.ToString(Settings.LoginInfo.CorporateProfileId);
            ddlLocalTripEmployees.Enabled = false;
            ddlLocalTripEmployees.SelectedValue = Convert.ToString(Settings.LoginInfo.CorporateProfileId);
            ddlHtlEmployees.Enabled = false;
            ddlHtlEmployees.SelectedValue = Convert.ToString(Settings.LoginInfo.CorporateProfileId);
        }

    }

    private void bindSearch(DateTime fromDate, DateTime toDate, int profileId, string approvalStatus,int productId)
    {
        try
        {
            
            if (approvalStatus == "A") //Approved
            {
                DataTable dtApproved = CorporateProfileExpenseDetails.GetTripDetailsQueueList(profileId, fromDate, toDate, approvalStatus,(int)Settings.LoginInfo.AgentId, productId);
                if (productId == 1) //based on product type binding data to gridview 
                {
                    SearchListApproved = dtApproved;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvApproved, dtApproved);
                }
                else
                {
                    SearchListHTApproved = dtApproved;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvHTApproved, dtApproved);
                }
            }
            if (approvalStatus == "P") //Pending
            {
                DataTable dtPending = CorporateProfileExpenseDetails.GetTripDetailsQueueList(profileId, fromDate, toDate, approvalStatus, (int)Settings.LoginInfo.AgentId, productId);
                if (productId == 1) //based on product type binding data to gridview 
                {
                    SearchListPending = dtPending;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvPending, dtPending);
                }
                else
                {
                    SearchListHTPending = dtPending;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvHTPending, dtPending);
                }
            }
            if (approvalStatus == "R") //Rejected
            {
                DataTable dtRejected = CorporateProfileExpenseDetails.GetTripDetailsQueueList(profileId, fromDate, toDate, approvalStatus,(int)Settings.LoginInfo.AgentId, productId);
                if (productId == 1)//based on product type binding data to gridview 
                {
                    SearchListRejected = dtRejected;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvRejected, dtRejected);
                }
                else
                {
                    SearchListHTRejected = dtRejected;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvHTRejected, dtRejected);
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    private void bindLocalTripSearch(DateTime fromDate, DateTime toDate, int userId, string approvalStatus)
    {
        try
        {
            if (approvalStatus == "A") //Approved
            {
                DataTable dtApproved = CorporateProfileTripDetails.GetLocalTripDetailsQueueList(userId, fromDate, toDate, approvalStatus);
                if (dtApproved != null && dtApproved.Rows.Count > 0)
                {
                    SearchListLTApproved = dtApproved;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvLTApproved, dtApproved);
                }
            }
            if (approvalStatus == "P") //Pending
            {
                DataTable dtPending = CorporateProfileTripDetails.GetLocalTripDetailsQueueList(userId, fromDate, toDate, approvalStatus);
                if (dtPending != null && dtPending.Rows.Count > 0)
                {
                    SearchListLTPending = dtPending;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvLTPending, dtPending);
                }
            }
            if (approvalStatus == "R") //Rejected
            {
                DataTable dtRejected = CorporateProfileTripDetails.GetLocalTripDetailsQueueList(userId, fromDate, toDate, approvalStatus);
                if (dtRejected != null && dtRejected.Rows.Count > 0)
                {
                    SearchListLTRejected = dtRejected;
                    CommonGrid g = new CommonGrid();
                    g.BindGrid(gvLTRejected, dtRejected);
                }
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }


    private DataTable SearchListApproved
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_APPROVED];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ROWID"] };
            Session[REIM_SEARCH_SESSION_APPROVED] = value;
        }
    }

    private DataTable SearchListPending
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_PENDING];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ROWID"] };
            Session[REIM_SEARCH_SESSION_PENDING] = value;
        }
    }

    private DataTable SearchListRejected
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_REJECTED];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ROWID"] };
            Session[REIM_SEARCH_SESSION_REJECTED] = value;
        }
    }

    private DataTable SearchListLTApproved
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_LTAPPROVED];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["TRIP_ID"] };
            Session[REIM_SEARCH_SESSION_APPROVED] = value;
        }
    }

    private DataTable SearchListLTPending
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_LTPENDING];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["TRIP_ID"] };
            Session[REIM_SEARCH_SESSION_PENDING] = value;
        }
    }

    private DataTable SearchListLTRejected
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_LTREJECTED];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["TRIP_ID"] };
            Session[REIM_SEARCH_SESSION_REJECTED] = value;
        }
    }

    // Added Properties for Hotel Tab to bind the data 
    private DataTable SearchListHTApproved
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_HTAPPROVED];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ROWID"] };
            Session[REIM_SEARCH_SESSION_HTAPPROVED] = value;
        }
    }

    private DataTable SearchListHTPending
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_HTPENDING];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ROWID"] };
            Session[REIM_SEARCH_SESSION_HTPENDING] = value;
        }
    }

    private DataTable SearchListHTRejected
    {
        get
        {

            return (DataTable)Session[REIM_SEARCH_SESSION_HTREJECTED];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["ROWID"] };
            Session[REIM_SEARCH_SESSION_HTREJECTED] = value;
        }
    }


    protected void FilterSearch1_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={
         {"HTtxtEMP_ID", "EMP_ID"}
        ,{"HTtxtEMP_NAME", "EMP_NAME"}
        ,{"HTtxtTRIP_ID", "UNIQUE_TRIP_ID"}
        ,{"HTtxtBOOKING_DATE", "BOOKING_DATE"}
        ,{"HTtxtTRAVEL_DATE", "TRAVEL_DATE"}
        ,{"HTtxtRETURN_DATE", "RETURN_DATE"}
        ,{"HTtxtAIRLINE_CODE", "AIRLINE_CODE"}
        ,{"HTtxtROUTE", "ROUTE"}
        ,{"HTtxtREASON_OF_TRAVEL", "REASON_OF_TRAVEL"}
        ,{"HTtxtTOTAL_FARE", "TOTAL_FARE" }
        ,};
        CommonGrid g = new CommonGrid();
        g.FilterGridView(gvPending, SearchListPending.Copy(), textboxesNColumns);
    }

    protected void FilterSearch2_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={

            {"HTtxtEMP_ID", "EMP_ID"}
            ,{"HTtxtEMP_NAME", "EMP_NAME"}
            ,{"HTtxtTRIP_ID", "UNIQUE_TRIP_ID"}
            ,{"HTtxtBOOKING_DATE", "BOOKING_DATE"}
            ,{"HTtxtTRAVEL_DATE", "TRAVEL_DATE"}
            ,{"HTtxtRETURN_DATE", "RETURN_DATE"}
            ,{"HTtxtAIRLINE_CODE", "AIRLINE_CODE"}
            ,{"HTtxtROUTE", "ROUTE"}
            ,{"HTtxtREASON_OF_TRAVEL", "REASON_OF_TRAVEL"}
            ,{"HTtxtTOTAL_FARE", "TOTAL_FARE" }
             ,
             };
        CommonGrid g = new CommonGrid();

        g.FilterGridView(gvApproved, SearchListApproved.Copy(), textboxesNColumns);


    }

    protected void FilterSearch3_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={

             {"HTtxtEMP_ID", "EMP_ID"}
            ,{"HTtxtEMP_NAME", "EMP_NAME"}
            ,{"HTtxtTRIP_ID", "UNIQUE_TRIP_ID"}
            ,{"HTtxtBOOKING_DATE", "BOOKING_DATE"}
            ,{"HTtxtTRAVEL_DATE", "TRAVEL_DATE"}
            ,{"HTtxtRETURN_DATE", "RETURN_DATE"}
            ,{"HTtxtAIRLINE_CODE", "AIRLINE_CODE"}
            ,{"HTtxtROUTE", "ROUTE"}
            ,{"HTtxtREASON_OF_TRAVEL", "REASON_OF_TRAVEL"}
            ,{"HTtxtTOTAL_FARE", "TOTAL_FARE"}
                                        ,
                                         };
        CommonGrid g = new CommonGrid();

        g.FilterGridView(gvRejected, SearchListRejected.Copy(), textboxesNColumns);
    }

    //Added for filter events for Hotel Tab
    protected void HotelFilterSearch1_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={
         {"HTtxtEmpId", "EmpId"}
        ,{"HTtxtEmpName", "EmpName"}
        ,{"HTtxtRefenceNo", "Reference_No"}
        ,{"HTtxtBookingDate", "BookingDate"}
        ,{"HTtxtCheckinDate", "CheckinDate"}
        ,{"HTtxtCheckoutDate", "CheckoutDate"}
        ,{"HTtxtHotelName", "HotelName"}
        ,{"HTtxtCity", "City"}
        ,{"HTtxtReasonOfTravel", "ReasonOfTravel"}
        ,{"HTtxtPrice", "Price" }
        ,};
        CommonGrid g = new CommonGrid();
        g.FilterGridView(gvHTPending, SearchListHTPending.Copy(), textboxesNColumns);
    }

    protected void HotelFilterSearch2_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={
         {"HTtxtEmpId", "EmpId"}
        ,{"HTtxtEmpName", "EmpName"}
        ,{"HTtxtReferenceNo", "Reference_No"}
        ,{"HTtxtBookingDate", "BookingDate"}
        ,{"HTtxtChekckinDate", "CheckinDate"}
        ,{"HTtxCheckoutDate", "CheckoutDate"}
        ,{"HTtxthotelName", "HotelName"}
        ,{"HTtxtCity", "City"}
        ,{"HTtxtReasonOfTravel", "ReasonOfTravel"}
        ,{"HTtxtPrice", "Price" }
        ,};
        CommonGrid g = new CommonGrid();
        g.FilterGridView(gvHTApproved, SearchListHTApproved.Copy(), textboxesNColumns);
    }

    protected void HotelFilterSearch3_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={
         {"HTtxtEmpId", "EmpId"}
        ,{"HTtxtEmpName", "EmpName"}
        ,{"HTtxtRefereneNo", "Reference_No"}
        ,{"HTtxtBookingDate", "BookingDate"}
        ,{"HTtxtCheckinDate", "CheckinDate"}
        ,{"HTtxtCheckoutDate", "CheckoutDate"}
        ,{"HTtxtHotelName", "HotelName"}
        ,{"HTtxtCity", "City"}
        ,{"HTtxtReasonofTravel", "ReasonOfTravel"}
        ,{"HTtxtPrice", "Price" }
        ,};
        CommonGrid g = new CommonGrid();
        g.FilterGridView(gvHTRejected, SearchListHTRejected.Copy(), textboxesNColumns);
    }

    protected void gvApproved_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvApproved.PageIndex = e.NewPageIndex;
            gvApproved.EditIndex = -1;
            FilterSearch2_Click(null, null);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateTripDetailsQueue)gvApproved__PageIndexChanging method .Error:" + ex.ToString(), "0");

        }
    }

    protected void gvPending_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvPending.PageIndex = e.NewPageIndex;
            gvPending.EditIndex = -1;
            FilterSearch1_Click(null, null);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateTripDetailsQueue)gvPending_PageIndexChanging method .Error:" + ex.ToString(), "0");

        }

    }

    protected void gvRejected_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvRejected.PageIndex = e.NewPageIndex;
            gvRejected.EditIndex = -1;
            FilterSearch3_Click(null, null);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateTripDetailsQueue)gvRejected_PageIndexChanging method .Error:" + ex.ToString(), "0");

        }

    }

    protected void FilterSearchLTApproved_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={

                                         {"HTtxtEMP_ID", "EMP_ID"}
,{"HTtxtEMP_NAME", "EMP_NAME"}
,{"HTtxtTRIP_ID", "TRIP_ID"}
,{"HTtxtBOOKING_DATE", "BOOKING_DATE"}
,{"HTtxtTRAVEL_DATE", "TRAVEL_DATE"}
,{"HTtxtAIRLINE_CODE", "AIRLINE_CODE"}
,{"HTtxtROUTE", "ROUTE"}
,{"HTtxtREASON_OF_TRAVEL", "REASON_OF_TRAVEL"}
,{"HTtxtTOTAL_FARE", "TOTAL_FARE" }


,



                                         
                                         };
        CommonGrid g = new CommonGrid();
        g.FilterGridView(gvPending, SearchListPending.Copy(), textboxesNColumns);

    }

    protected void FilterSearchLTPending_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={

                                         {"HTtxtEMP_ID", "EMP_ID"}
,{"HTtxtEMP_NAME", "EMP_NAME"}
,{"HTtxtTRIP_ID", "TRIP_ID"}
,{"HTtxtBOOKING_DATE", "BOOKING_DATE"}
,{"HTtxtTRAVEL_DATE", "TRAVEL_DATE"}
,{"HTtxtAIRLINE_CODE", "AIRLINE_CODE"}
,{"HTtxtROUTE", "ROUTE"}
,{"HTtxtREASON_OF_TRAVEL", "REASON_OF_TRAVEL"}
,{"HTtxtTOTAL_FARE", "TOTAL_FARE" }

                                        ,
                                         };
        CommonGrid g = new CommonGrid();

        g.FilterGridView(gvApproved, SearchListApproved.Copy(), textboxesNColumns);


    }

    protected void FilterSearchLTRejected_Click(object sender, EventArgs e)
    {
        string[,] textboxesNColumns ={

                                         {"HTtxtEMP_ID", "EMP_ID"}
,{"HTtxtEMP_NAME", "EMP_NAME"}
,{"HTtxtTRIP_ID", "TRIP_ID"}
,{"HTtxtBOOKING_DATE", "BOOKING_DATE"}
,{"HTtxtTRAVEL_DATE", "TRAVEL_DATE"}
,{"HTtxtAIRLINE_CODE", "AIRLINE_CODE"}
,{"HTtxtROUTE", "ROUTE"}
,{"HTtxtREASON_OF_TRAVEL", "REASON_OF_TRAVEL"}
,{"HTtxtTOTAL_FARE", "TOTAL_FARE" }
                                        ,
                                         };
        CommonGrid g = new CommonGrid();

        g.FilterGridView(gvRejected, SearchListRejected.Copy(), textboxesNColumns);


    }

    protected void gvLTApproved_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvLTApproved.PageIndex = e.NewPageIndex;
            gvLTApproved.EditIndex = -1;
            FilterSearchLTApproved_Click(null, null);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateTripDetailsQueue)gvLTApproved_PageIndexChanging .Error:" + ex.ToString(), "0");

        }
    }

    protected void gvLTPending_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvLTPending.PageIndex = e.NewPageIndex;
            gvLTPending.EditIndex = -1;
            FilterSearchLTPending_Click(null, null);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateTripDetailsQueue)gvLTPending_PageIndexChanging method .Error:" + ex.ToString(), "0");

        }

    }

    protected void gvLTRejected_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvLTRejected.PageIndex = e.NewPageIndex;
            gvLTRejected.EditIndex = -1;
            FilterSearchLTRejected_Click(null, null);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Airline, Severity.High, 1, "(CorporateTripDetailsQueue)gvLTRejected_PageIndexChanging method .Error:" + ex.ToString(), "0");

        }

    }


    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (Convert.ToInt32(ddlEmployee.SelectedItem.Value) > 0)
            {
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), Convert.ToInt32(ddlEmployee.SelectedItem.Value), "A", Convert.ToInt32(hdnProductType.Value));//Approved
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), Convert.ToInt32(ddlEmployee.SelectedItem.Value), "P", Convert.ToInt32(hdnProductType.Value));//Pending
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), Convert.ToInt32(ddlEmployee.SelectedItem.Value), "R", Convert.ToInt32(hdnProductType.Value));//Rejected
            }
            else
            {
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), 0, "A", Convert.ToInt32(hdnProductType.Value));//Approved
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), 0, "P", Convert.ToInt32(hdnProductType.Value));//Pending
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), 0, "R", Convert.ToInt32(hdnProductType.Value));//Rejected
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateTripDetailsQueue) btnSearchApprovals Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }

    protected void btnLocalTripSearch_Click(object sender, EventArgs e)
    {
        
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            bindLocalTripSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), (int)Settings.LoginInfo.UserID, "A");//Approved
            bindLocalTripSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), (int)Settings.LoginInfo.UserID, "P");//Pending
            bindLocalTripSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), (int)Settings.LoginInfo.UserID, "R");//Rejected
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateTripDetailsQueue)Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }



    /// <summary>
    /// Return the ceiled value for TBOAir otherwise rounded value
    /// </summary>
    /// <param name="price"></param>
    /// <param name="source"></param>
    /// <returns></returns>
    protected Decimal GetRoundedAmount(decimal price, string source)
    {
        decimal total = 0m;
        if (source.Contains(","))
        {
            if (source == "21,21")
                total = Math.Ceiling(price);
            else
                total = Math.Round(price, Settings.LoginInfo.DecimalValue);
        }
        else if (source == "21")
        {
            total = Math.Ceiling(price);
        }
        else
        {
            total = Math.Round(price, Settings.LoginInfo.DecimalValue);
        }

        return total;
    }

    protected void Item_Bound(Object sender, GridViewRowEventArgs e)
    {
       
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            HyperLink hlink = e.Row.FindControl("hlinkDetails") as HyperLink;
            hlink.NavigateUrl = string.Format("~/CorporateTripSummary.aspx?flightId={0}&empId={1}&empName={2}&status=P&type={3}&TripId={4}", DataBinder.Eval(e.Row.DataItem, "TRIP_ID"), DataBinder.Eval(e.Row.DataItem, "EMP_ID"), DataBinder.Eval(e.Row.DataItem, "EMP_NAME"), DataBinder.Eval(e.Row.DataItem, "BOOKING_TYPE"), DataBinder.Eval(e.Row.DataItem, "UNIQUE_TRIP_ID"));
            if (DataBinder.Eval(e.Row.DataItem, "BOOKING_TYPE").ToString() == "Normal")
            {
                hlink.NavigateUrl = string.Format("~/CorporateTripSummary.aspx?flightId={0}&empId={1}&empName={2}&status=P&TripId={3}", DataBinder.Eval(e.Row.DataItem, "TRIP_ID"), DataBinder.Eval(e.Row.DataItem, "EMP_ID"), DataBinder.Eval(e.Row.DataItem, "EMP_NAME"), DataBinder.Eval(e.Row.DataItem, "UNIQUE_TRIP_ID"));
            }
        }
    }

    //Added for Hotel Tab Search click event
    protected void btnHotelSearch_Click(object sender, EventArgs e)
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            if (Convert.ToInt32(ddlHtlEmployees.SelectedItem.Value) > 0)
            {
                bindSearch(Convert.ToDateTime(dcHotelFromDate.Value, dateFormat), Convert.ToDateTime(dcHotelToDate.Value, dateFormat), Convert.ToInt32(ddlHtlEmployees.SelectedItem.Value), "A", Convert.ToInt32(hdnProductType.Value));//Approved
                bindSearch(Convert.ToDateTime(dcHotelFromDate.Value, dateFormat), Convert.ToDateTime(dcHotelToDate.Value, dateFormat), Convert.ToInt32(ddlHtlEmployees.SelectedItem.Value), "P", Convert.ToInt32(hdnProductType.Value));//Pending
                bindSearch(Convert.ToDateTime(dcHotelFromDate.Value, dateFormat), Convert.ToDateTime(dcHotelToDate.Value, dateFormat), Convert.ToInt32(ddlHtlEmployees.SelectedItem.Value), "R", Convert.ToInt32(hdnProductType.Value));//Rejected
            }
            else
            {
                bindSearch(Convert.ToDateTime(dcHotelFromDate.Value, dateFormat), Convert.ToDateTime(dcHotelToDate.Value, dateFormat), 0, "A", Convert.ToInt32(hdnProductType.Value));//Approved
                bindSearch(Convert.ToDateTime(dcHotelFromDate.Value, dateFormat), Convert.ToDateTime(dcHotelToDate.Value, dateFormat), 0, "P", Convert.ToInt32(hdnProductType.Value));//Pending
                bindSearch(Convert.ToDateTime(dcHotelFromDate.Value, dateFormat), Convert.ToDateTime(dcHotelToDate.Value, dateFormat), 0, "R", Convert.ToInt32(hdnProductType.Value));//Rejected
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateTripDetailsQueue) btnHotelSearch_Click() Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
    }
   
    private void BindCorporateQueueData()  //Added for bind the data only in Tab click event
    {
        try
        {
            IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
            dcHotelFromDate.Value = DateTime.Now;
            dcHotelToDate.Value = DateTime.Now;
            dcReimFromDate.Value = DateTime.Now;
            dcReimToDate.Value = DateTime.Now;
            //Based on the product Type bind the data
            if (Convert.ToInt32(hdnProductType.Value) == 1)
            {
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), Convert.ToInt32(ddlEmployee.SelectedItem.Value), "A", Convert.ToInt32(hdnProductType.Value));//Approved
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), Convert.ToInt32(ddlEmployee.SelectedItem.Value), "P", Convert.ToInt32(hdnProductType.Value));//Pending
                bindSearch(Convert.ToDateTime(dcReimFromDate.Value, dateFormat), Convert.ToDateTime(dcReimToDate.Value, dateFormat), Convert.ToInt32(ddlEmployee.SelectedItem.Value), "R", Convert.ToInt32(hdnProductType.Value));//Rejected
            }
            else if (Convert.ToInt32(hdnProductType.Value) == 2)
            {
                bindSearch(Convert.ToDateTime(dcHotelFromDate.Value, dateFormat), Convert.ToDateTime(dcHotelToDate.Value, dateFormat), Convert.ToInt32(ddlHtlEmployees.SelectedItem.Value), "A", Convert.ToInt32(hdnProductType.Value));//Approved
                bindSearch(Convert.ToDateTime(dcHotelFromDate.Value, dateFormat), Convert.ToDateTime(dcHotelToDate.Value, dateFormat), Convert.ToInt32(ddlHtlEmployees.SelectedItem.Value), "P", Convert.ToInt32(hdnProductType.Value));//Pending
                bindSearch(Convert.ToDateTime(dcHotelFromDate.Value, dateFormat), Convert.ToDateTime(dcHotelToDate.Value, dateFormat), Convert.ToInt32(ddlHtlEmployees.SelectedItem.Value), "R", Convert.ToInt32(hdnProductType.Value));//Rejected
            }
            
        }
        catch(Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CorporateTripDetailsQueue) BindCorporateQueueData() Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        finally
        {
            hdnBindData.Value = "false"; //after binding data making hdnBindData to false for should not call this method in other events.
        }
    }

    protected void Page_PreRender(object sender, EventArgs e) //before rendering page setting Tab selection.
    {
        Utility.StartupScript(this.Page, "SetActiveTab();", "SetActiveTab");
    }

    //Events for PageIndexChanging for Hotel Tab Gridview controls 
    protected void gvHTPending_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvHTPending.PageIndex = e.NewPageIndex;
        gvHTPending.EditIndex = -1;
        HotelFilterSearch1_Click(null, null);

    }

    protected void gvHTRejected_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        gvHTRejected.PageIndex = e.NewPageIndex;
        gvHTRejected.EditIndex = -1;
        HotelFilterSearch3_Click(null, null);
    }

    protected void gvHTApproved_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvHTApproved.PageIndex = e.NewPageIndex;
        gvHTApproved.EditIndex = -1;
        HotelFilterSearch2_Click(null, null);
    }
    //Rounding the price to agent decimal value
    protected Decimal GetPriceRoundOff(object price)
    {
        decimal total = 0m;
        if (price!=DBNull.Value)
        {
            total = Math.Round(Convert.ToDecimal(price), Settings.LoginInfo.DecimalValue);
        }
        else
        {
            total = 0;
        }
        return total;
    }
}
