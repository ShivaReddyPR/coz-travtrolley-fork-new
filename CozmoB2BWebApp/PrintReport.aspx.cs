using System;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;

public partial class PrintReport : System.Web.UI.Page
{
    bool isDecimalLocation = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                LoginInfo loginInfo = Settings.LoginInfo;
                if (loginInfo == null)
                {
                    Response.Redirect("SessionExpired.aspx");
                    // Response.Redirect(string.Format("ErrorPage.aspx?Err={0}", GetGlobalResourceObject("ErrorMessages", "INVALID_USER")));
                }

                if (!string.IsNullOrEmpty(Request.QueryString["receiptId"]))
                {
                    long recieptId = Utility.ToLong(Request.QueryString["receiptId"]);
                    ShowData(recieptId);
                }

            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
        }
    }

    private void ShowData(long receiptId)
    {
        try
        {
            string modeDescription = string.Empty;
            PaymentMode mode = PaymentMode.Cash;
            CT.TicketReceipt.BusinessLayer.ReceiptMaster receipt = new  CT.TicketReceipt.BusinessLayer.ReceiptMaster(receiptId);
	  // hdfDecimalNo.Value = Utility.ToString(receipt.LocationDecimals);
            //if (receipt.HandlingType == "P")
            //{
            //    lblHeading.Text =lblCopyHeading.Text= "Payment";
            //}
            switch (receipt.SettlementMode)
            {
                case "1":
                    mode = PaymentMode.Cash;
                    modeDescription = string.Format("{0}:{1}", PaymentMode.Cash,  getFormattedAmnt(receipt.CashMode.BaseAmount));
                    break;
                case "2":
                    mode = PaymentMode.Credit;
                    modeDescription = string.Format("{0}:{1}", PaymentMode.Credit, getFormattedAmnt(receipt.CreditMode.BaseAmount));
                    break;
                case "3":
                    mode = PaymentMode.Card;
                    modeDescription = string.Format("{0}:{1}", PaymentMode.Card, getFormattedAmnt(receipt.CardMode.BaseAmount));
                    break;
                case "4":
                    mode = PaymentMode.Cash_Credit;
                    modeDescription = string.Format("{0}:{1},{2}:{3}", PaymentMode.Cash, getFormattedAmnt(receipt.CashMode.BaseAmount), PaymentMode.Credit, Formatter.ToCurrency(Utility.ToLong(receipt.CreditMode.BaseAmount)));
                    break;
                case "5":
                        mode = PaymentMode.Cash_Card;
                    modeDescription = string.Format("{0}:{1},{2}:{3}", PaymentMode.Cash, getFormattedAmnt(receipt.CashMode.BaseAmount), PaymentMode.Card, getFormattedAmnt(receipt.CardMode.BaseAmount));
                    break;
                case "6":
                    mode = PaymentMode.Cash_Credit_Card;
                    modeDescription = string.Format("{0}:{1},{2}:{3},{4}:{5}", PaymentMode.Cash, getFormattedAmnt(receipt.CashMode.BaseAmount), PaymentMode.Credit, getFormattedAmnt(receipt.CreditMode.BaseAmount), PaymentMode.Card, getFormattedAmnt(receipt.CardMode.BaseAmount));
                    break;
                case "7":
                    mode = PaymentMode.Employee;
                    modeDescription = string.Format("{0}:{1}", PaymentMode.Employee, getFormattedAmnt(receipt.EmployeeMode.BaseAmount));
                    break;
                case "8":
                    mode = PaymentMode.Others;
                    modeDescription = string.Format("{0}:{1}", PaymentMode.Others, getFormattedAmnt(receipt.OthersMode.BaseAmount));
                    break;
            }
            lblReceiptNoValue.Text = lblCopyReceiptNoValue.Text = receipt.DocNumber;
            lblDateValue.Text = lblCopyDateValue.Text = IDDateTimeFormat(receipt.DocDate);
           
            lblPaxNameValue.Text = lblCopyPaxNameValue.Text = receipt.PaxName;
            lblTotalFareValue.Text = lblCopyTotalFareValue.Text = getFormattedAmnt(receipt.BaseToCollect ) + " " + (receipt.CurrencyCode);
            lblCollectionModeValue.Text = lblCopyCollectionModeValue.Text = modeDescription;
            lblTickedByValue.Text = lblCopyTicketedByValue.Text=receipt.CreatedByName;
            lblRemarksValue.Text =lblCopyRemarksValue.Text = receipt.Remarks;
            lblContactValue.Text = receipt.PaxMobileNo;
            lblLocationAds.Text = lblLocationAdsCopy.Text = Settings.LoginInfo.LocationAddress;
            lblLocationTerms.Text = lblLocationTermsCopy.Text = Settings.LoginInfo.LocationTerms;
            lblStaffIdValue.Text = lblCopyStaffIdValue.Text = receipt.StaffIdName;
            lblCompanyValue.Text = lblCopyCompanyValue.Text = receipt.CompanyName;
        }
        catch { throw; }
    }

    private string getFormattedAmnt(decimal amount)
    {
        try
        {            
            string formatedAmnt = Formatter.ToCurrency("0");
            formatedAmnt = Formatter.ToCurrency(Utility.ToDecimal(amount));//,Utility.ToInteger(hdfDecimalNo.Value));
             
            return formatedAmnt;
        }
        catch { throw; }
    }
    //private void setDecimals(long rcptLocationId)
    //{
    //    try
    //    {
    //        long locationId = rcptLocationId;
    //        string[] locIds = hdfDecimalLocations.Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
    //        string[] locAndDecimals = hdfDecimals.Value.Split(new[] { ',' }, StringSplitOptions.RemoveEmptyEntries);
    //        if (locIds.Length > 0)
    //        {
    //            foreach (string locId in locIds)
    //            {
    //                if (locationId == Utility.ToLong(locId))
    //                {
    //                    isDecimalLocation = true;
    //                    foreach (string locDecimals in locAndDecimals)
    //                    {
    //                        string[] decimalValues = locDecimals.Split(new[] { '-' }, StringSplitOptions.RemoveEmptyEntries);
    //                        if (locationId == Utility.ToLong(decimalValues[0])) hdfDecimalNo.Value = Utility.ToString(decimalValues[1]);
    //                    }
    //                    break;
    //                }
    //            }

    //        }
    //    }
    //    catch
    //    { throw; }

    //}
    #region Date Format
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:MM");
        }
    }
    #endregion
   
}
