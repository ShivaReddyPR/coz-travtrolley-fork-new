﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using System.IO;
using System.Collections.Generic;
using ReligareInsurance;
using CT.Core;

namespace CozmoB2BWebApp
{
    public partial class ReligareProductDetailsMaster : CT.Core.ParentPage
    {
         //string ReligareProductDetails_SESSION = "_ReligareProdcutdetails";
       

        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {

                BindProductTypeId();//Bindgin the dropdown values
            }

        }
        #region Binding the productType table values
        void BindProductTypeId()
        {
            try
            {
                ddlProductTypeId.DataSource = ReligareProductDetails.GetList();
                ddlProductTypeId.DataValueField = "PRODUCTID";
                ddlProductTypeId.DataTextField = "PRODUCTNAME";
                ddlProductTypeId.DataBind();
                ddlProductTypeId.Items.Insert(0, new ListItem("Select Product", "-1"));

            }
            catch(Exception ex) { throw ex; }
        }
        #endregion
        #region Methods
       

         void Save()
        {
            try
            {
                ReligareProductDetails productDetails=new ReligareProductDetails();
                if (Utility.ToInteger(hdfEMId.Value) > 0)
                {
                    productDetails.ID = Utility.ToLong(hdfEMId.Value);
                }
                else
                {
                    productDetails.ID = -1;
                }

                
                productDetails.PRODUCT_TYPE_ID = Utility.ToInteger(ddlProductTypeId.SelectedItem.Value);
                productDetails.PRODUCTID = txtproductId.Text;
                productDetails.PRODUCTNAME = txtProductName.Text;
                productDetails.CREATED_BY = Settings.LoginInfo.UserID;
                productDetails.Trip_Type = ddlTripType.SelectedItem.Value;
                productDetails.Save();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("Details Successfully", "", hdfEMId.Value == "0" ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);
                btnSave.Text = "Save";
                Clear();
            }
            catch(Exception ex) { throw ex; }
        }
         void Clear()
        {
            
            txtproductId.Text = string.Empty;
            txtProductName.Text = string.Empty;
            ddlProductTypeId.SelectedIndex = -1;
            ddlTripType.SelectedIndex = 0;
            
        }
       
         void bindSearch()
        {

            try
            {

                DataTable dt = ReligareProductDetails.GetAdditionalServiceMasterDetails();
                if (dt.Rows.Count > 0)
                {
                    gvSearch.DataSource = dt;
                    gvSearch.DataBind();
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }
         void Edit(long id)
        {
            try
            {
                ReligareProductDetails productDetails = new ReligareProductDetails(id);
                hdfEMId.Value = Utility.ToString(productDetails.ID);
                ddlProductTypeId.SelectedValue = Convert.ToString(productDetails.PRODUCT_TYPE_ID);
                txtproductId.Text = productDetails.PRODUCTID;
                txtProductName.Text = productDetails.PRODUCTNAME;
                ddlTripType.SelectedValue = productDetails.Trip_Type.Trim();
                btnSave.Text = "Update";
                btnClear.Text = "Cancel";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Button Events
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareProductDetailsMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);

            }

        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.ShowSearch("Search");
                bindSearch();
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareProductDetailsMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);


            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareProductDetailsMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        #endregion
        #region Gridview Events
        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                long detailsid = Utility.ToInteger(gvSearch.SelectedValue);
                Edit(detailsid);
                this.Master.HideSearch();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareProductDetailsMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);

            }
        }
        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                gvSearch.EditIndex = -1;
                bindSearch();
                
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareProductDetailsMaster.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);

            }
        }
        #endregion


    }
}
