﻿using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class CreateTransferInvoice : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            if (!Settings.LoginInfo.IsOnBehalfOfAgent)
            {
                hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.AgentId);
                hdnBehalfLocation.Value = JsonConvert.SerializeObject(0);                
            }
            else
            {
                hdnAgentId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentID);
                hdnBehalfLocation.Value = JsonConvert.SerializeObject(Settings.LoginInfo.OnBehalfAgentLocation);                
            }
            hdnUserId.Value = JsonConvert.SerializeObject(Settings.LoginInfo.UserID);
        }
    }
}