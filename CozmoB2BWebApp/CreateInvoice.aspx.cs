using System;
using System.Data;
using System.Web.UI;
using CT.BookingEngine;
using CT.Core;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;
using System.Linq;

public partial class CreateInvoiceGUI :CT.Core.ParentPage// System.Web.UI.Page
{

    protected List<Ticket> ticketList;
    protected Invoice invoice = new Invoice();
    protected AgentMaster agency;
    protected string agencyAddress;
    protected RegCity agencyCity = new RegCity();
    protected UserMaster loggedMember = new UserMaster();
    protected int lineItemCount;
    protected int flightId;
    protected int agencyId;
    protected int cityId = 0;
    protected string remarks = string.Empty;
    protected string pnr;
    protected string routing = string.Empty;
    protected string bookingClass = string.Empty;
    protected string airlineCode = string.Empty;
    protected DateTime traveldate = new DateTime();
    protected string plbString = string.Empty;
    protected Dictionary<string, DataRow[]> segment = new Dictionary<string, DataRow[]>();
    protected DataTable segmentTable;
    protected FlightItinerary itinerary;
    /// <summary>
    /// Flag to indicate if the agency type is 'Service'
    /// </summary>
    protected bool isServiceAgency;
    protected string invoiceModified = string.Empty;
    protected bool isAnyFareZero = false;
    protected bool paymentDoneAgainstInvoice = false;
    protected int bookingId;
    protected string supplierName = string.Empty;
    protected LocationMaster location;
    protected AgentMaster parentAgent;
    protected LocationMaster parentLocation;
    protected void Page_Load(object sender, EventArgs e)
    {
        Page.Title = "Invoice";
        AuthorizationCheck();
        loggedMember = new UserMaster(Convert.ToInt32(Settings.LoginInfo.UserID));
        int invoiceNumber = 0;
        //checking if the page is reached with proper data
        if (Request["agencyId"] != null)
        {
            //agencyId = Convert.ToInt32(Request["agencyId"]);
            // agency = new AgentMaster(agencyId);
        }
        else
        {
            throw new ArgumentException("All the required values are not available in Request");
        }
        //string logoPath = "";
        //if (agency.ID > 1)
        //{
        //    logoPath = ConfigurationManager.AppSettings["AgentImage"] + agency.ImgFileName;
        //    imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + logoPath;
        //}
        //else
        //{
        //    imgLogo.ImageUrl = System.Configuration.ConfigurationManager.AppSettings["RootFolder"] + "images/logo.jpg";
        //} 
        if (Request["flightId"] != null && Request["agencyId"] != null)
        {
            flightId = Convert.ToInt32(Request["flightId"]);
            //agencyId = Convert.ToInt32(Request["agencyId"]);
        }
        else
        {
            invoiceNumber = Convert.ToInt32(Request["invoiceNumber"]);
            flightId = FlightItinerary.GetFlightIdByInvoiceNumber(invoiceNumber);
        }
        itinerary = new FlightItinerary(flightId);
        pnr = itinerary.PNR;
        bookingId = itinerary.BookingId;
        // code added for handling sector in case of conjunction tickets.
        agency = new AgentMaster(itinerary.AgencyId);
        FlightInfo[] allSegments = FlightInfo.GetSegments(flightId);
        segmentTable = new DataTable();
        DataColumn column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "source"; segmentTable.Columns.Add(column);
        column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "destination"; segmentTable.Columns.Add(column);
        column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "flightNumber"; segmentTable.Columns.Add(column);
        column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "status"; segmentTable.Columns.Add(column);
        column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "bookingClass"; segmentTable.Columns.Add(column);
        column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "AirLine"; segmentTable.Columns.Add(column);
        column = new DataColumn(); column.DataType = Type.GetType("System.String"); column.ColumnName = "conjunctionNo"; segmentTable.Columns.Add(column);


        List<string> conjunctionlist = new List<string>();
        conjunctionlist.Add("C0");

        for (int i = 0; i < allSegments.Length; i++)
        {
            if (allSegments[i].ConjunctionNo != null)
            {
                if (!conjunctionlist.Contains(allSegments[i].ConjunctionNo.ToString()))
                {
                    conjunctionlist.Add(allSegments[i].ConjunctionNo.ToString());
                }
            }

            DataRow row = segmentTable.NewRow();

            row["source"] = allSegments[i].Origin.AirportCode;
            row["destination"] = allSegments[i].Destination.AirportCode;
            row["flightNumber"] = allSegments[i].FlightNumber;
            row["status"] = allSegments[i].Status;
            row["bookingClass"] = allSegments[i].BookingClass;
            row["AirLine"] = allSegments[i].Airline;
            row["conjunctionNo"] = allSegments[i].ConjunctionNo;
            segmentTable.Rows.Add(row);
        }

        for (int j = 0; j < conjunctionlist.Count; j++)
        {
            if (conjunctionlist[j] == "C0")
            {
                segment[conjunctionlist[j]] = segmentTable.Select("conjunctionNo is null");
            }
            else
            {
                segment.Add(conjunctionlist[j], segmentTable.Select("conjunctionNo='" + conjunctionlist[j].ToString() + "'"));
            }
        }

        //DataRow[] row1 = segment["C0"];
        //string SectorString = row1[0]["source"].ToString();
        //for (int i = 0; i < row1.Length; i++)
        //{
        //    SectorString = SectorString + "-" + row1[i]["destination"];
        //}


        //if (Request["routing"] != null)
        //{
        //    routing = Request["routing"];
        //}
        //else
        //{
        //    FlightInfo[] segment = FlightInfo.GetSegments(flightId);
        //    routing = segment[0].Origin.CityCode;
        //    for (int i = 0; i < segment.Length; i++)
        //    {
        //        routing += "-" + segment[i].Destination.CityCode;
        //    }
        //}

        List<string> trips = new List<string>();
        foreach (FlightInfo segment in itinerary.Segments)
        {
            bookingClass += segment.BookingClass; //bookingClass + row[k]["bookingClass"].ToString();
            if (!trips.Contains(segment.Origin.CityCode))
            {
                trips.Add(segment.Origin.CityCode);
            }
            if (!trips.Contains(segment.Destination.CityCode))
            {
                trips.Add(segment.Destination.CityCode);
            }
        }

        bool isReturn = false;

        if (itinerary.Origin == itinerary.Segments[itinerary.Segments.Length - 1].Destination.CityCode)
        {
            isReturn = true;
        }

        foreach (string city in trips)
        {
            if (routing.Length > 0)
            {
                routing += "-" + city;
            }
            else
            {
                routing = city;
            }
        }
        if (isReturn)
        {
            routing += "-" + trips[0];
        }


        if (Request["remarks"] != null)
        {
            remarks = Request["remarks"];
        }

        if (Request["invoiceModified"] != null)
        {
            invoiceModified = Request["invoiceModified"];
        }

        DataTable dtCities = CityMaster.GetList(agency.Country, ListStatus.Short, RecordStatus.Activated);
        DataRow[] cities = dtCities.Select("City_Name like '%" + agency.City + "%'");

        if (cities != null && cities.Length > 0)
        {
            cityId = Convert.ToInt32(cities[0]["city_id"]);
        }
        // Reading agency information.
        //agency = new AgentMaster(agencyId);
        isServiceAgency = true;// agency.AgencyTypeId == Convert.ToInt32(ConfigurationSystem.Core["ServiceAgencyTypeId"]);
        if (agency.City.Length == 0)
        {
            agencyCity.CityName = agency.City;
        }
        else
        {

            agencyCity = RegCity.GetCity(cityId);
        }
        location = new LocationMaster(itinerary.LocationId);
        string agentType = string.Empty;
        int agentId = itinerary.AgencyId;
        switch(agency.AgentType)
        {
            case 1:
                agentType = "BASE";
                break;
            case 2:
                agentType = "Agent";
                break;
            case 3:
                agentType = "B2B";
                agentId = agency.AgentParantId;
                break;
            case 4:
                agentType = "B2B2B";
                agentId = agency.AgentParantId;
                break;
        }
        //Getting parent location, agent from the booking location
        DataTable dtAgentLocations = LocationMaster.GetList(agentId, ListStatus.Long, RecordStatus.Activated, "BASE");

        if(dtAgentLocations != null)
        {
            DataRow[] parentLocs = dtAgentLocations.Select("location_code='" + location.ParentCode + "'");
            
            if(parentLocs != null && parentLocs.Length > 0 && itinerary.AgencyId > 1)
            {
                parentLocation = new LocationMaster(Convert.ToInt64(parentLocs[0]["location_id"]));
                parentAgent = new AgentMaster(Convert.ToInt64(parentLocs[0]["location_agent_id"]));
            }
            else
            {
                parentLocation = location;
                parentAgent = agency;
            }
        }
        
        // Formatting agency address for display.
        agencyAddress = agency.Address;
        agencyAddress.Trim();
        if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
        {
            agencyAddress += ",";
        }
        if (agency.Address != null && agency.Address.Length > 0)
        {
            agencyAddress += agency.Address;
        }
        agencyAddress.Trim();
        if (agencyAddress.Length > 0 && agencyAddress.Substring(agencyAddress.Length - 1) != ",")
        {
            agencyAddress += ",";
        }
        // Getting ticket list from DB
        ticketList = Ticket.GetTicketList(flightId);
        foreach (Ticket ticket in ticketList)
        {
            if (ticket.Price.PublishedFare == 0)
            {
                isAnyFareZero = true;
                break;
            }
        }
        traveldate = itinerary.TravelDate;

        // Generating invoice.
        if (ticketList.Count > 0)
        {
            if (invoiceNumber == 0)
            {
                invoiceNumber = Invoice.isInvoiceGenerated(ticketList[0].TicketId);
            }
        }
        if (invoiceNumber > 0)
        {
            invoice = new Invoice();
            invoice.Load(invoiceNumber);
            supplierName = Invoice.GetSupplierByInvoiceNumber(invoiceNumber);
        }
        else
        {
            invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(flightId, string.Empty, (int)loggedMember.ID, ProductType.Flight, 1);
            invoice.Load(invoiceNumber);
        }
        paymentDoneAgainstInvoice = Invoice.IsPaymentDoneAgainstInvoice(invoiceNumber);

        //Fare calculations;
        decimal Tax = 0;
        decimal BaseFare = 0;
        decimal AsvAmount = 0;
        decimal baggage = 0;
        decimal markup = 0, outputVAT = 0, inputVAT = 0, IGST = 0, CGST = 0, SGST = 0;
        int CGSTPer = 0, IGSTPer = 0, SGSTPer = 0;
        //bool isBasefare = false;
        foreach (FlightPassenger pax in itinerary.Passenger)
        {

            if (ticketList[0].Price.AccPriceType == PriceType.PublishedFare)
            {
                BaseFare += pax.Price.PublishedFare + pax.Price.HandlingFeeAmount;
                //lblTotalForAdult.Text = (pax.Price.PublishedFare + pax.Price.SeviceTax + pax.Price.Tax).ToString("0.000");
                //lblTotal.Text = lblTotalForAdult.Text;
            }
            else
            {
                BaseFare += pax.Price.NetFare;
                //lblTotalForAdult.Text = (pax.Price.NetFare + pax.Price.Markup + pax.Price.SeviceTax + pax.Price.Tax).ToString("0.000");
                //lblTotal.Text = lblTotalForAdult.Text;
            }
            if (Settings.LoginInfo.AgentId > 1)
            {
                Tax += pax.Price.Tax + pax.Price.Markup + pax.Price.B2CMarkup - pax.Price.Discount;// add only B2C markup by chandan on  13062016
            }
            //else
            //{
            //    Tax += pax.Price.Tax - pax.Price.Discount;
            //    markup += pax.Price.Markup + pax.Price.B2CMarkup;
            //}
            if (location.CountryCode == "IN") //If Booking Login Country IN is there Need to load GST
            {
                List<GSTTaxDetail> taxDetList = GSTTaxDetail.LoadGSTDetailsByPriceId(pax.Price.PriceId);
                if (taxDetList != null && taxDetList.Count > 0)
                {
                    CGST += taxDetList.Where(p => p.TaxCode == "CGST").Sum(p => p.TaxAmount);
                    SGST += taxDetList.Where(p => p.TaxCode == "SGST").Sum(p => p.TaxAmount);
                    IGST += taxDetList.Where(p => p.TaxCode == "IGST").Sum(p => p.TaxAmount);
                    GSTTaxDetail objTaxDet = taxDetList.Where(p => p.TaxCode == "CGST").FirstOrDefault();
                    if(objTaxDet !=null)
                    {
                        CGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                    }
                    objTaxDet = taxDetList.Where(p => p.TaxCode == "SGST").FirstOrDefault();
                    if (objTaxDet != null)
                    {
                        SGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                    }
                    objTaxDet = taxDetList.Where(p => p.TaxCode == "IGST").FirstOrDefault();
                    if (objTaxDet != null)
                    {
                        IGSTPer = Convert.ToInt32(objTaxDet.TaxValue);
                    }
                }
            }
            else
            {
                outputVAT += pax.Price.OutputVATAmount;
                inputVAT += pax.Price.InputVATAmount;
            }
            if (itinerary.FlightBookingSource == BookingSource.TBOAir)
            {
                Tax += pax.Price.AdditionalTxnFee + pax.Price.OtherCharges + pax.Price.SServiceFee + pax.Price.TransactionFee;
            }
            AsvAmount += pax.Price.AsvAmount;
            //if (pax.Price.AsvElement == "BF")
            //{
            //    isBasefare = true;
            //}
            //else
            //{
            //    isBasefare = false;
            //}
            baggage += pax.Price.BaggageCharge;
            //lblServiceFee.Text = pax.Price.SeviceTax.ToString("0.000");
        }
        if (location.CountryCode != "IN")
        {
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                BaseFare -= inputVAT;
            }
            else
            {
                inputVAT = 0;
            }
        }
        //if (isBasefare)
        //{
        //    BaseFare += AsvAmount;
        //}
        //else
        //{
        //    Tax += AsvAmount;
        //}
        if (Settings.LoginInfo.AgentId > 1)
        {
            lblMarkup.Visible = false;
            lblMarkupVal.Visible = false;
            lblGross.Text = agency.AgentCurrency + " " + (BaseFare + Tax + baggage).ToString("N" + agency.DecimalValue);

            if (itinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam(Total price Ceiling for TBO Source)
            {
                lblNetAmount.Text = agency.AgentCurrency + " " + Math.Ceiling(BaseFare + Tax + baggage + AsvAmount + outputVAT+inputVAT+ CGST+SGST+IGST).ToString("N" + agency.DecimalValue);
            }
            else
            {
                lblNetAmount.Text = agency.AgentCurrency + " " + (BaseFare + Tax + baggage + AsvAmount + outputVAT+inputVAT + CGST + SGST + IGST).ToString("N" + agency.DecimalValue);
            }
        }
        else
        {
            lblMarkup.Visible = true;
            lblMarkupVal.Visible = true;
            lblMarkupVal.Text = agency.AgentCurrency + " " + markup.ToString("N" + agency.DecimalValue);
            lblGross.Text = agency.AgentCurrency + " " + (BaseFare + Tax + baggage + markup).ToString("N" + agency.DecimalValue);
          
                if (itinerary.FlightBookingSource == BookingSource.TBOAir) //Added by brahmam(Total price Ceiling for TBO Source)
                {
                    lblNetAmount.Text = agency.AgentCurrency + " " + Math.Ceiling(BaseFare + Tax + baggage + AsvAmount + markup + outputVAT+inputVAT + CGST + SGST + IGST).ToString("N" + agency.DecimalValue);
                }
                else
                {
                    lblNetAmount.Text = agency.AgentCurrency + " " + (BaseFare + Tax + baggage + AsvAmount + markup + outputVAT+inputVAT + CGST + SGST + IGST).ToString("N" + agency.DecimalValue);
                }           
           
        }
       
        
            lblBaseFare.Text = agency.AgentCurrency + " " + BaseFare.ToString("N" + agency.DecimalValue);
        
        
        lblTax.Text = agency.AgentCurrency + " " + Tax.ToString("N" + agency.DecimalValue);
        lblBaggage.Text = agency.AgentCurrency + " " + baggage.ToString("N" + agency.DecimalValue);
        //lblGross.Text = agency.AgentCurrency + " " + (BaseFare + Tax + baggage).ToString("N" + agency.DecimalValue);
        lblAddMarkup.Text = agency.AgentCurrency + " " + AsvAmount.ToString("N" + agency.DecimalValue);
        //lblNetAmount.Text = agency.AgentCurrency + " " + (BaseFare + Tax + baggage + AsvAmount).ToString("N" + agency.DecimalValue);
        if (location.CountryCode == "IN")
        {
            if (IGST > 0)
            {
                lblIGSTValue.Visible = true;
                lblIGST.Visible = true;
                lblIGST.Text = "IGST (" + IGSTPer + "%)";
                lblIGSTValue.Text = agency.AgentCurrency + " " + IGST.ToString("N" + agency.DecimalValue);
            }
            if (CGST > 0)
            {
                lblCGST.Visible = true;
                lblCGSTValue.Visible = true;
                lblCGST.Text = "CGST (" + CGSTPer + "%)";
                lblCGSTValue.Text = agency.AgentCurrency + " " + CGST.ToString("N" + agency.DecimalValue);
            }
            if (SGST > 0)
            {
                lblSGST.Visible = true;
                lblSGSTValue.Visible = true;
                lblSGST.Text = "SGST (" + SGSTPer + "%)";
                lblSGSTValue.Text = agency.AgentCurrency + " " + SGST.ToString("N" + agency.DecimalValue);
            }
        }
        else
        {
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent || Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                lblinputVAT.Visible = true;
                lblinpVAT.Visible = true;
                lblinpVAT.Text = "In VAT:";
                lbloutVAT.Visible = true;
                lbloutVAT.Text = "Out VAT:";
                lblinputVAT.Text = agency.AgentCurrency + " " + inputVAT.ToString("N" + agency.DecimalValue);
                lblVAT.Text = agency.AgentCurrency + " " + outputVAT.ToString("N" + agency.DecimalValue);

            }
            else
            {
                lbloutVAT.Visible = true;
                lbloutVAT.Text = "VAT:";
                lblVAT.Text = agency.AgentCurrency + " " + outputVAT.ToString("N" + agency.DecimalValue);
            }
        }
        
        lblNetReceivable.Text = lblNetAmount.Text;
        lblTotal.Text = lblNetAmount.Text;

    }

    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {
            Response.Redirect("AbandonSession.aspx");
        }
        //if (Session["roleId"] == null)
        //{
        //    String values = "?errMessage=Login Required to access " + Page.Title + " page.";
        //    Response.Redirect("Default.aspx" + values, true);
        //}
        //else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.ViewInvoice))
        //{
        //    String values = "?errMessage=You are not authorised to access " + Page.Title + " page.";
        //    Response.Redirect("Default.aspx" + values, true);
        //}
    }


    protected string GetSegments()
    {
        string segments = string.Empty;
        
        foreach(FlightInfo seg in itinerary.Segments)
        {
            if(string.IsNullOrEmpty(segments))
            {                
                segments = seg.Origin.AirportCode + "-" + seg.Destination.AirportCode;
            }
            else
            {
                segments += "<br/>" + seg.Origin.AirportCode + "-" + seg.Destination.AirportCode;
            }
        }

        return segments;
    }
   
    /// <summary>
    /// Method to Show sector info for all pax.
    /// </summary>
    /// <returns>string of sector names in ORIGIN-DEST format</returns>
    protected string GetFlightNumbers()
    {
        string flightNumbers = string.Empty;
        if (itinerary != null && itinerary.Segments != null)
        {
            foreach (FlightInfo seg in itinerary.Segments)
            {
                if (string.IsNullOrEmpty(flightNumbers))
                {
                    flightNumbers = seg.Airline + "-" + seg.FlightNumber;
                }
                else
                {
                    flightNumbers += "<br/>" + seg.Airline + "-" + seg.FlightNumber;
                }
            }
        }

        return flightNumbers;
    }


    protected string GetTravelDates()
    {
        string travelDates = string.Empty;
        foreach (FlightInfo seg in itinerary.Segments)
        {
            if (string.IsNullOrEmpty(travelDates))
            {
                travelDates = seg.DepartureTime.ToString("dd/MM/yyyy");
            }
            else
            {
                travelDates += "<br/>" + seg.DepartureTime.ToString("dd/MM/yyyy");
            }
        }

        return travelDates;
    }

    protected decimal GetYQTax(int index)
    {
        decimal yqTax = 0m;
        List<KeyValuePair<string,decimal>> taxBreakups = ticketList[index].TaxBreakup;

        yqTax = taxBreakups.FirstOrDefault(s => (s.Key == "YQ" || s.Key == "YQTax")).Value;       

        return yqTax;
    }


    protected decimal GetYRTax(int index)
    {
        decimal yrTax = 0m;
        List<KeyValuePair<string, decimal>> taxBreakups = ticketList[index].TaxBreakup;

        yrTax = taxBreakups.FirstOrDefault(s => (s.Key == "YR")).Value;

        return yrTax;
    }

    protected decimal GetK3Tax(int index)
    {
        decimal k3Tax = 0m;
        List<KeyValuePair<string, decimal>> taxBreakups = ticketList[index].TaxBreakup;

        if (string.IsNullOrEmpty(itinerary.RoutingTripId))
        {
            k3Tax = taxBreakups.FirstOrDefault(s => s.Key.Contains("GST")).Value;
        }
        else
        {
            if (itinerary.FlightBookingSource == BookingSource.TBOAir)
            {
                k3Tax = taxBreakups.Where(x => x.Key.Contains("K3")).Sum(x => x.Value);
            }
            else if (itinerary.FlightBookingSource == BookingSource.GoAir || itinerary.FlightBookingSource == BookingSource.GoAirCorp)
            {
                k3Tax = taxBreakups.Where(x => x.Key.Contains("ST") || x.Key.Contains("IT") || x.Key.Contains("CT")).Sum(x => x.Value);

            }
            else if (itinerary.FlightBookingSource == BookingSource.UAPI)
            {
                k3Tax = taxBreakups.Where(x => x.Key.Contains("K3")).Sum(x => x.Value);
                
            }
            else
            {
                k3Tax = taxBreakups.Where(x => x.Key.Contains("GST")).Sum(x => x.Value);
            }
            
        }

        return k3Tax;
    }

    protected decimal GetK7Tax(int index)
    {
        decimal k7Tax = 0m;
        List<KeyValuePair<string, decimal>> taxBreakups = ticketList[index].TaxBreakup;

        k7Tax = taxBreakups.FirstOrDefault(s => s.Key == "K7").Value;

        return k7Tax;
    }

    protected string GetStateName(int stateId,string countryCode)
    {
        string stateName = string.Empty;
        try
        {
            if (!string.IsNullOrEmpty(countryCode))
            {
                DataTable dtRegions = LocationMaster.GetTourRegionList(ListStatus.Short, RecordStatus.Activated, "S", countryCode);

                if (dtRegions != null && dtRegions.Rows.Count > 0)
                {
                    DataRow[] region = dtRegions.Select("rgn_id=" + stateId);

                    if (region != null && region.Length > 0)
                    {
                        stateName = region[0]["rgn_name"].ToString();
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, "(CreateInvoice Page)Failed to get States list for CountryCode: " + countryCode + ". Reason: " + ex.ToString(), Request["REMOTE_ADDR"]);
        }
        return stateName;
    }

    #region ConverWordToNumber method

    protected String ConvertToWords(String numb)
    {
        String val = "", wholeNo = numb, points = "", andStr = "", pointStr = "";
        String endStr = "Only";
        try
        {
            int decimalPlace = numb.IndexOf(".");
            if (decimalPlace > 0)
            {
                wholeNo = numb.Substring(0, decimalPlace);
                points = numb.Substring(decimalPlace + 1);
                if (Convert.ToInt32(points) > 0)
                {
                    andStr = "and";// just to separate whole numbers from points/cents  
                    if (itinerary.Passenger[0].Price.Currency == "INR")
                    {
                        endStr = "Paisa " + endStr;//Cents   
                    }
                    else
                    {
                        endStr = "Fills " + endStr;//Cents   
                    }
                    pointStr = ConvertDecimals(points);
                }
            }
            val = String.Format("{0} {1}{2} {3}", ConvertWholeNumber(wholeNo).Trim(), andStr, pointStr, endStr);
        }
        catch { }
        return val;
    }

    private String ConvertWholeNumber(String Number)
    {
        string word = "";
        try
        {
            bool beginsZero = false;//tests for 0XX 
            bool isDone = false;//test if already translated 
            double dblAmt = (Convert.ToDouble(Number));
            //if ((dblAmt > 0) && number.StartsWith("0")) 
            if (dblAmt > 0)
            {//test for zero or digit zero in a nuemric 
                beginsZero = Number.StartsWith("0");

                int numDigits = Number.Length;
                int pos = 0;//store digit grouping 
                String place = "";//digit grouping name:hundres,thousand,etc... 
                switch (numDigits)
                {
                    case 1://ones' range 

                        word = ones(Number);
                        isDone = true;
                        break;
                    case 2://tens' range 
                        word = tens(Number);
                        isDone = true;
                        break;
                    case 3://hundreds' range 
                        pos = (numDigits % 3) + 1;
                        place = " Hundred ";
                        break;
                    case 4://thousands' range 
                    case 5:
                    case 6:
                        pos = (numDigits % 4) + 1;
                        place = " Thousand ";
                        break;
                    case 7://millions' range 
                    case 8:
                    case 9:
                        pos = (numDigits % 7) + 1;
                        place = " Million ";
                        break;
                    case 10://Billions's range 
                    case 11:
                    case 12:

                        pos = (numDigits % 10) + 1;
                        place = " Billion ";
                        break;
                    //add extra case options for anything above Billion... 
                    default:
                        isDone = true;
                        break;
                }
                if (!isDone)
                {//if transalation is not done, continue...(Recursion comes in now!!) 
                    if (Number.Substring(0, pos) != "0" && Number.Substring(pos) != "0")
                    {
                        try
                        {
                            word = ConvertWholeNumber(Number.Substring(0, pos)) + place + ConvertWholeNumber(Number.Substring(pos));
                        }
                        catch { }
                    }
                    else
                    {
                        word = ConvertWholeNumber(Number.Substring(0, pos)) + ConvertWholeNumber(Number.Substring(pos));
                    }

                    //check for trailing zeros 
                    //if (beginsZero) word = " and " + word.Trim(); 
                }
                //ignore digit grouping names 
                if (word.Trim().Equals(place.Trim())) word = "";
            }
        }
        catch { }
        return word.Trim();
    }

    private String tens(String Number)
    {
        int _Number = Convert.ToInt32(Number);
        String name = null;
        switch (_Number)
        {
            case 10:
                name = "Ten";
                break;
            case 11:
                name = "Eleven";
                break;
            case 12:
                name = "Twelve";
                break;
            case 13:
                name = "Thirteen";
                break;
            case 14:
                name = "Fourteen";
                break;
            case 15:
                name = "Fifteen";
                break;
            case 16:
                name = "Sixteen";
                break;
            case 17:
                name = "Seventeen";
                break;
            case 18:
                name = "Eighteen";
                break;
            case 19:
                name = "Nineteen";
                break;
            case 20:
                name = "Twenty";
                break;
            case 30:
                name = "Thirty";
                break;
            case 40:
                name = "Fourty";
                break;
            case 50:
                name = "Fifty";
                break;
            case 60:
                name = "Sixty";
                break;
            case 70:
                name = "Seventy";
                break;
            case 80:
                name = "Eighty";
                break;
            case 90:
                name = "Ninety";
                break;
            default:
                if (_Number > 0)
                {
                    name = tens(Number.Substring(0, 1) + "0") + " " + ones(Number.Substring(1));
                }
                break;
        }
        return name;
    }

    private String ones(String Number)
    {
        int _Number = Convert.ToInt32(Number);
        String name = "";
        switch (_Number)
        {

            case 1:
                name = "One";
                break;
            case 2:
                name = "Two";
                break;
            case 3:
                name = "Three";
                break;
            case 4:
                name = "Four";
                break;
            case 5:
                name = "Five";
                break;
            case 6:
                name = "Six";
                break;
            case 7:
                name = "Seven";
                break;
            case 8:
                name = "Eight";
                break;
            case 9:
                name = "Nine";
                break;
        }
        return name;
    }

    private String ConvertDecimals(String number)
    {
        String cd = "", digit = "", engOne = "";
        for (int i = 0; i < number.Length; i++)
        {
            digit = number[i].ToString();
            if (digit.Equals("0"))
            {
                engOne = "Zero";
            }
            else
            {
                engOne = ones(digit);
            }
            cd += " " + engOne;
        }
        return cd;
    }

    #endregion
}
