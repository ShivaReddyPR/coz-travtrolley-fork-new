﻿using CT.CMS;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Script.Services;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class TransferSearch : CT.Core.ParentPage
    {       
        protected int agencyId;
        protected string userType;
        DataTable dtTransferSource;
        public B2BCMSDetails clsCMSDet;
        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            if (Settings.LoginInfo == null)
            {
                Response.Redirect("AbandonSession.aspx", true);
            }
            if (!IsPostBack)
            {
                agencyId = Settings.LoginInfo.AgentId;
                hdnAgentId.Value =JsonConvert.SerializeObject(Settings.LoginInfo.AgentId);
                agentLocation.Value = JsonConvert.SerializeObject(Settings.LoginInfo.LocationID);
                userType = Settings.LoginInfo.MemberType.ToString();
                hdnAgentType.Value = ((int)Settings.LoginInfo.AgentType).ToString();
                GetCMSInfo();
                int id = 0;
                HtmlTableRow row = new HtmlTableRow();

                HtmlTableCell cell1 = new HtmlTableCell();
                CheckBox chkSource1 = new CheckBox();
                chkSource1.ID = "chkSource" + id.ToString();
                chkSource1.Text = "All";
                chkSource1.Checked = true;
                chkSource1.Attributes.Add("onclick", "selectOrUnselectSupplier()");
                cell1.Controls.Add(chkSource1);

                row.Cells.Add(cell1);
                id++;
                dtTransferSource = AgentMaster.GetAgentSources(Settings.LoginInfo.AgentId, 9, false);
                foreach (DataRow dr in dtTransferSource.Rows)
                {
                    HtmlTableCell cell = new HtmlTableCell();
                    CheckBox chkSource = new CheckBox();
                    chkSource.ID = "chkSource" + id.ToString();
                    chkSource.Text = Convert.ToString(dr["Name"]);
                    chkSource.InputAttributes.Add("value", dr["Name"].ToString());
                    chkSource.Attributes.Add("onclick", "setCheck('ctl00_cphTransaction_" + chkSource.ID + "')");
                    chkSource.Checked = true;
                    cell.Controls.Add(chkSource);
                    row.Cells.Add(cell);
                }
                tblSources.Rows.Add(row);
            }
            if (hdnSearchSubmit.Value == "search")
            {
                Response.Redirect("TransferResults.aspx");
            }
            TokenGeneration();
        }
        void GetCMSInfo()
        {
            try
            {
                clsCMSDet = B2BCMSDetails.GetCMSData(Settings.LoginInfo.AgentId, Convert.ToInt32(Settings.LoginInfo.AgentType));
                clsCMSDet = clsCMSDet == null ? new B2BCMSDetails() : clsCMSDet;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.HotelSearch, Severity.Normal, Convert.ToInt32(Settings.LoginInfo.UserID), "Failed to get GetCMSInfo. Error: " + ex.ToString(), Request["REMOTE_ADDR"]);
            }

        }       
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object LoadB2BAgents(int agentId)
        {
            DataTable dtAgents = AgentMaster.GetList(1, string.Empty, agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            return JsonConvert.SerializeObject(dtAgents);
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object LoadLocation(int agentId)
        {
            DataTable dtLocations = CT.TicketReceipt.BusinessLayer.LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
            return JsonConvert.SerializeObject(dtLocations);
        }
        [WebMethod]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json, UseHttpGet = false)]
        public static object GetAllAirline()
        {
            DataTable dtAirline = Airline.GetAllAirlines();
            return JsonConvert.SerializeObject(dtAirline);
        }

        public void TokenGeneration()
        {
            HttpCookie aCookie = Request.Cookies["Session_Token"];
            string baseAddress = ConfigurationManager.AppSettings["TransferWebApiUrl"];
            using (var client = new HttpClient())
            {
                var form = new Dictionary<string, string>
               {
                   {"grant_type", "password"},
                   {"UserName", "cozmo"},
                   {"Password", "cozmo"},
               };
                var tokenResponse = client.PostAsync(baseAddress + "/token", new FormUrlEncodedContent(form)).Result;
                //var token = tokenResponse.Content.ReadAsStringAsync().Result;  
                var token = tokenResponse.Content.ReadAsStringAsync().Result;
                Token ApiResponseObj = JsonConvert.DeserializeObject<Token>(token);
                string resp = string.Empty;
                HttpCookie tokenCookie = new HttpCookie("Session_Token");
                DateTime now = DateTime.Now;
                // Set the cookie value.
                tokenCookie.Value = ApiResponseObj.AccessToken;
                // Set the cookie expiration date.
                tokenCookie.Expires = now.AddMinutes(ApiResponseObj.ExpiresIn);
                Response.SetCookie(tokenCookie);
            }
        }
        
    }
    public class Token
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
        [JsonProperty("token_type")]
        public string TokenType { get; set; }
        [JsonProperty("expires_in")]
        public int ExpiresIn { get; set; }
        [JsonProperty("refresh_token")]
        public string RefreshToken { get; set; }
        [JsonProperty("error")]
        public string Error { get; set; }
        [JsonProperty("error_description")]
        public string ErrorDescription { get; set; }
    }
}
