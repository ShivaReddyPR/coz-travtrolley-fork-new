<%@ Page Language="C#" AutoEventWireup="true" Inherits="PrintAgentPaymentReceipt" Codebehind="PrintAgentPaymentReceipt.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" >
<head runat="server">
<meta http-equiv="X-UA-Compatible" content="IE=EmulateIE7" />
    <title>Print Agent Payment Receipt</title>
    <link type="text/css" href="Styles/CTStyle.css" rel="stylesheet" />
    <link type="text/css" href="Styles/default.css" rel="stylesheet" />
</head>
<script type="text/javascript" language="javascript">
//    function print()
//    {
//        document.getElementById('Print').style.display="none";
//	    window.print();	 
//	    setTimeout('showButtons()',100000);
//    }
//    function showButtons()
//    {
//        document.getElementById('Print').style.display="block";	    
//    }
 function printPage()
    {
        document.getElementById('btnPrint').style.display="none";
	    window.print();	 
	    //alert('2');
	    setTimeout('showButtons()',1000);
	   // alert('3');
    }
    function showButtons()
    {
        document.getElementById('btnPrint').style.display="block";	    
    }
    </script>
    
        
              <!-- Bootstrap Core CSS -->
    


<link href="css/bootstrap.min.css" rel="stylesheet">

    <link href="css/override.css" rel="stylesheet">



<body>

    <form id="form1" runat="server">
 <div class="col-md-12"> 

 
 
  <div style="margin-bottom:10px; border-bottom: solid 1px #ccc" class="bg_white paramcon pad_10">
    
              <div class="col-md-12 padding-0">                                      
   
 <div class="col-md-4"> <div id="divPrintButton" style="text-align:left">
    <input id="btnPrint" onclick="return printPage();" class="button" type="button" value="Print Receipt" />
    </div></div>
    <div class="col-md-4"> <center> <img src="images/logo.gif" height="50px" width="150px" alt="" /></center> </div>
    <div class="col-md-4"><asp:Label id="lblPrintTimeValue" CssClass="rptLabelNewValue" runat="server" style="font-size:9px"></asp:Label><asp:Label ID="lblPrintTime" CssClass="rptLabelNew" runat="server" style="font-size:8px" Text="Print Date&Tme:"></asp:Label> </div>


<div class="clearfix"></div>
    </div>
    
    
    
        <div class="col-md-12 padding0"> 
    
     <div class="col-md-4">  <label class="rptLabelNew" style="font-style:italic; font-size:10px;font-weight:lighter">Agent Copy</label></div>
 
 
 <div class="col-md-4"> <center> <asp:Label ID="lblReceiptHeader" CssClass="rptLabelNew" style="text-decoration:underline;font-size:20px" runat="server" ></asp:Label></center></div> <div class="col-md-4">  </div>

<div class="clearfix"></div>

</div>


    
    <div class="col-md-12  marbot_10">          
    <table  width="100%" cellpadding="0" cellspacing="0"  border="1" >
         <%--       <tr>
                    <td style="width:340px"></td>
                    <td  style="width:320px"></td>
                    <td style="width:50px"></td>
                    <td  style="width:100px"></td>
                    <td style="width:150px"></td>
                    
                </tr>--%>
                
                
              
                
                
                <tr>
                    <td valign="top" align="left"><asp:Label ID="lblReceived" CssClass="rptLabelNew" runat="server" Text="Received With thanks from : "></asp:Label></td>
                    <td   align="left"><asp:Label ID="lblReceivedValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                    <td valign="top" align="right"colspan="2"><asp:Label ID="lblReceiptNo" CssClass="rptLabelNew" runat="server" Text="Receipt No : "></asp:Label></td>
                    <td   align="left"><asp:Label ID="lblReceiptNoValue" CssClass="rptLabelNewValue" style="font-weight:bold" runat="server" ></asp:Label></td>
                </tr>
                
                
               <tr>
                <td></td>
                    <td></td>
                    <td  valign="top" align="right"colspan="2"><asp:Label ID="lblDate" CssClass="rptLabelNew" runat="server" Text="Date : "></asp:Label></td>
                    <td  align="left"><asp:Label ID="lblDateValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                </tr>
                
                
                
                 <tr>
                    <td></td>
                        <td></td>
                    <td  valign="top" align="right"colspan="2"><asp:Label ID="lblMode" CssClass="rptLabelNew" runat="server" Text="Mode : "></asp:Label></td>
                    <td  align="left"><asp:Label ID="lblModeValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                </tr>
                
                 <tr id="trBank" runat="server">
                    <td colspan="2"></td>
                    <td  valign="top" align="right" colspan="2"><asp:Label ID="lblBank" CssClass="rptLabelNew" runat="server" Text="Bank : "></asp:Label></td>
                    <td   align="left"><asp:Label ID="lblBankValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                </tr>
                <tr>
                    <td  valign="top" align="left"><asp:Label ID="lblCurrency" CssClass="rptLabelNew" runat="server" Text="Currency : "></asp:Label></td>
                    <td  align="left"><asp:Label ID="lblCurrencyValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                </tr>
             
                <tr>
                    <td  valign="top" align="left"><asp:Label ID="lblAmountFigure" CssClass="rptLabelNew" runat="server" Text="Amount in Figure : "></asp:Label></td>
                    <td  align="left" colspan="4"><asp:Label ID="lblAmountFigureValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                </tr>
                
                 <tr>
                    <td  valign="top" align="left"><asp:Label ID="lblAmountWord" CssClass="rptLabelNew" runat="server" Text="Amount in Words : "></asp:Label></td>
                    <td  align="left" colspan="4"><asp:Label ID="lblAmountWordValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                </tr>
                
                 <tr>
                    <td align="left" valign="top" ><asp:Label ID="lblRemarks" CssClass="rptLabelNew"  runat="server" Text="In Settlement Of:"></asp:Label></td>
                    <td align="left"  colspan="4"><asp:TextBox ID="lblRemarksValue"  runat="server" TextMode="MultiLine"  ReadOnly="true" CssClass="rptLabelNewValue" style="overflow:hidden;border:0px solid white;font-size:10px" Height="50px" Width="100%"></asp:TextBox></td>
                </tr>
       
         
            </table>
    </div>
    <div style="border:0.2pt solid #000; padding:5px; overflow:hidden;">

          <table width="90%" border="0">
            <tr>
                <td align="left"><asp:Label  ID="lblCreatedBy" Text="Created By :" runat="server"  CssClass="rptLabelNew"  ></asp:Label>
               <asp:Label   ID="lblCreatedValue"  runat="server" CssClass="rptLabelNewValue" Width="150px" ></asp:Label></td>    
               <td><asp:Label  ID="lblApprovedBy" Text="Approved By :" runat="server"  CssClass="rptLabelNew"  ></asp:Label></td>
            </tr>    
          </table> 
        </div>
    <div class="clearfix"></div>
    
   </div> 
   
   
   
   
     <div class="bg_white paramcon pad_10 ">
    
    
    
              <div class="col-md-12 padding-0 marbot_10">                                      
    <div class="col-md-4"> <label class="rptLabelNew" style="font-style:italic;font-size:10px;font-weight:lighter">Accounts Copy</label></div>
    <div class="col-md-4"> <center> <img src="images/logo.gif" height="50px" alt="" /></center> </div>




   
 <div class="col-md-4"> <asp:Label ID="lblCopyPrintTime" CssClass="rptLabelNew" runat="server" style="font-size:8px" Text="Print Date&Tme:"></asp:Label><asp:Label id="lblCopyPrintTimeValue" CssClass="rptLabelNewValue" runat="server" style="font-size:9px"></asp:Label></div>


              <div class="col-md-12 marbot_10">  <center> <asp:Label ID="lblCopyReceiptHeader" CssClass="rptLabelNew" style="text-decoration:underline;font-size:20px" runat="server" ></asp:Label></center>   </div>


<div class="clearfix"></div>
    </div>




              <div class="col-md-12 padding-0 marbot_10">     
              
       
              
              
           <div>   <table  width="100%" cellpadding="0" cellspacing="0" class="rptTable"  border="1" >
      <%--          <tr>
                    <td style="width:340px"></td>
                    <td  style="width:320px"></td>
                    <td style="width:50px"></td>
                    <td  style="width:100px"></td>
                    <td style="width:150px"></td>
                    
                    
                </tr>--%>
              
                
                
                <tr>
                    <td valign="top" align="left"><asp:Label ID="lblCopyReceived" CssClass="rptLabelNew" runat="server" Text="Received With thanks from : "></asp:Label></td>
                    <td   align="left"><asp:Label ID="lblCopyReceivedValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                    <td valign="top" align="right"colspan="2"><asp:Label ID="lblCopyReceiptNo" CssClass="rptLabelNew" runat="server" Text="Receipt No : "></asp:Label></td>
                    <td   align="left"><asp:Label ID="lblCopyReceiptNoValue" CssClass="rptLabelNewValue" style="font-weight:bold" runat="server" ></asp:Label></td>
                </tr>
               <tr>
                    <td></td><td></td>
                    <td  valign="top" align="right"colspan="2"><asp:Label ID="lblCopyDate" CssClass="rptLabelNew" runat="server" Text="Date : "></asp:Label></td>
                    <td  align="left"><asp:Label ID="lblCopyDateValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                </tr>
                
                 <tr>
                    <td></td><td></td>
                    <td  valign="top" align="right"colspan="2"><asp:Label ID="lblCopyMode" CssClass="rptLabelNew" runat="server" Text="Mode : "></asp:Label></td>
                    <td  align="left"><asp:Label ID="lblCopyModeValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                </tr>
                
                 <tr id="trCopyBank" runat="server">
                   <td></td><td></td>
                    <td  valign="top" align="right" colspan="2"><asp:Label ID="lblCopyBank" CssClass="rptLabelNew" runat="server" Text="Bank : "></asp:Label></td>
                    <td   align="left"><asp:Label ID="lblCopyBankValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                </tr>
                <tr>
                    <td  valign="top" align="left"><asp:Label ID="lblCopyCurrency" CssClass="rptLabelNew" runat="server" Text="Currency : "></asp:Label></td>
                    <td  align="left"><asp:Label ID="lblCopyCurrencyValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                </tr>
             
                <tr>
                    <td  valign="top" align="left"><asp:Label ID="lblCopyAmountFigure" CssClass="rptLabelNew" runat="server" Text="Amount in Figure : "></asp:Label></td>
                    <td  align="left" colspan="4"><asp:Label ID="lblCopyAmountFigureValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                </tr>
                
                 <tr>
                    <td  valign="top" align="left"><asp:Label ID="lblCopyAmountWord" CssClass="rptLabelNew" runat="server" Text="Amount in Words : "></asp:Label></td>
                    <td  align="left" colspan="4"><asp:Label ID="lblCopyAmountWordValue" CssClass="rptLabelNewValue" runat="server" ></asp:Label></td>
                </tr>
                
                 <tr>
                    <td align="left" valign="top" ><asp:Label ID="lblCopyRemarks" CssClass="rptLabelNew"  runat="server" Text="In Settlement Of:"></asp:Label></td>
                    <td align="left"  colspan="4"><asp:TextBox ID="lblCopyRemarksValue"  runat="server" TextMode="MultiLine"  ReadOnly="true" CssClass="rptLabelNewValue" style="overflow:hidden;border:0px solid white;font-size:10px" Height="50px" Width="100%"></asp:TextBox></td>
                </tr>
       
         
            </table>  </div>  
 

              
              <div class="clearfix"></div>
              
              
              </div>
              
                <div class="clearfix"></div>
              </div>
      
      
 
       
        <div class="col-md-12  marbot_10">   
 <table width="100%" border="1">
            <tr>
                <td height="30" align="left"><asp:Label  ID="lblCopyCreatedBy" Text="Created By :" runat="server"  CssClass="rptLabelNew"  ></asp:Label>
               <asp:Label   ID="lblCopyCreatedValue"  runat="server" CssClass="rptLabelNewValue" Width="100%" ></asp:Label></td>    
                <td><asp:Label  ID="lblCopyApprovedBy" Text="Approved By :" runat="server"  CssClass="rptLabelNew"  ></asp:Label></td>
            </tr>  
              
          </table></div>  
          
          
      </div>


    </form>
</body>
</html>
