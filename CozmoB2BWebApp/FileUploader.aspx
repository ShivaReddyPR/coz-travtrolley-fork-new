   <%@ Page Language="C#" AutoEventWireup="true" Inherits="FileUploader" Codebehind="FileUploader.aspx.cs" %>
<%@ Register Src="~/DocumentManager.ascx"  TagPrefix="CT" TagName="DocumentManager" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "<%=Request.Url.Scheme%>://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="<%=Request.Url.Scheme%>://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
    <link href="styles/CTStyle.css" rel="stylesheet" type="text/css" />
    <link href="Styles/Default.css" rel="stylesheet" type="text/css" />
</head>
<body>
    <form id="form1" runat="server">
    <div class="upld" style="border:none;background-color:white">
    <table    border="0"  cellpadding="0" cellspacing="0">
    
    <tr class="uploadHeader" style="height:20px">
        <td  style="height: 20px"><asp:Label ID="lblTitle"   runat="server" Text="Upload Document:"></asp:Label> </td>
        <td align="center"  style="height: 20px">
           </td>
    </tr>
    <tr>
    <td><asp:Label ID="Label1" CssClass="label"  runat="server" Text="File Name:"></asp:Label> <asp:FileUpload ID="fuDocument" runat="server" CssClass="inputEnabled" Width="200px"  /></td>
    <td  rowspan="4"  >
        <fieldset><legend class="label">Preview</legend>
        <asp:Image ID="imgPreview" runat="server" Width="80px" Height="80px"  ImageUrl="~/Images/Common/no_preview.jpg" />
        </fieldset>
     </td>
    </tr>
    <tr>
        <td>&nbsp;</td>
       
    </tr>
    <tr>
    <td align="right">
   <asp:Button ID="butPreview" CssClass="buttonUpload" runat="server" Text="Upload" OnClick="butPreview_Click" OnClientClick="return validateFile('U')"/>
   </td>
        <%--<asp:Button ID="butUpload" CssClass="button" runat="server" Visible="false" Text="Upload" OnClick="butUpload_Click"  /></td>--%>
    </tr>
    <tr>
        <td >
        <asp:Label ID="lblError" CssClass="label" style="color:#FF0000; font-weight:bold" runat="server" Text=""></asp:Label> </td>
    </tr>
    <asp:HiddenField runat="server" id="hdfFileType" Value="1" />
    <asp:HiddenField runat="server" id="hdfUploaded" Value="0" />
    <asp:HiddenField runat="server" id="hdfFileName" Value="" />
    </table>
    </div>
     
 <script type="text/javascript">
     function validateFile(source)
    {
        if(document.getElementById('hdfFileName').value=='' && document.getElementById('fuDocument').value=='')
        {
            alert('Please select file to Upload !');
            return false;
        }
        if (source == "U")  //Only Validate, while Upload button click.
        {
            if (document.getElementById('fuDocument').value == '') {
                alert('Please select file to Upload !');
                return false;
            }
        }
        
        return true;
    }
    function isUploaded()
    {
    
    var val=document.getElementById('hdfUploaded').value;
     if(validateFile()) 
     {
      if(val=='0') 
        {alert('Please Upload the Document !');return false;}
      else return true;
     }
     else 
        return false;
    }
    document.body.bgColor="white";
    
 </script>
</form>
</body>
</html>

