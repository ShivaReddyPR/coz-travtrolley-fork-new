using System;
using CT.BookingEngine;
using CT.AccountingEngine;
using CT.Core;
using System.Data.SqlClient;
using System.Collections.Generic;
using CT.ReadingEngine;
using CT.TicketReceipt.BusinessLayer;


public partial class BookingAjax :System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //TODO: Authorization Check
        int loggedMemberId = Convert.ToInt32(Session["memberId"]);
        string voided = string.Empty;
        string voidedString = string.Empty;
        if (Request["isCancelTicket"] != null && Convert.ToBoolean(Request["isCancelTicket"]))
        {
            int ticketId = Convert.ToInt32(Request["ticketId"]);
            string remarks = string.Empty;
            if (Request["isRefundTicket"] == null || Request["isCancelTicket"] == string.Empty)
            {
                bool isVoided = Convert.ToBoolean(Request["isVoided"]);
                if (isVoided)
                {
                    voided = "Voided";
                    voidedString = "Ticket voided";
                }
                else
                {
                    voided = "Cancelled";
                    voidedString = "Ticket Refunded";
                }
            }
            else
            {
                voidedString = Request["index"];
                voided = Request["RequestType"].ToString() + "ed";
            }
            decimal adminFee = Convert.ToDecimal(Request["adminFee"]);
            decimal supplierFee = Convert.ToDecimal(Request["supplierFee"]);

            Ticket ticket = new Ticket();
            ticket.Load(ticketId);
            if (ticket.Status == "Cancelled" || ticket.Status == "Voided" || ticket.Status == "Refunded")
            {
                Response.Write("Already Refunded");
                Response.End();
            }
            if (Request["isRefundTicket"] == null || Request["isCancelTicket"] == string.Empty)
            {
                remarks = Request["remarks"];
            }
            else
            {
                remarks = "Refunded for Ticket No -" + ticket.TicketNumber;
            }
            bool isRefundTicket = false;
            if (Request["isRefundTicket"] == null || Request["isCancelTicket"] == string.Empty)
            {
                isRefundTicket = true;
            }
            //remarks = remarks;
            //TODO
            //Utility util = new Utility();
            //util.CancelTicket(ticket, adminFee, supplierFee, loggedMemberId, remarks, voided, voidedString, Convert.ToBoolean(Request["isCancelTicket"]), Convert.ToBoolean(Request["direct"]), Convert.ToInt32(Request["requestId"]), isRefundTicket);
            Response.Write(voidedString);
        }

        else if (Request["isCancelOffTicket"] != null && Convert.ToBoolean(Request["isCancelOffTicket"]))
        {
            //int ticketId = Convert.ToInt32(Request["ticketId"]);
            int offBookingId = Convert.ToInt32(Request["OffbookingId"]);
            string ticketNo = Convert.ToString(Request["ticketNo"]);
            string remarks = string.Empty;
            if (Request["isRefundOffTicket"] == null || Request["isCancelOffTicket"] == string.Empty)
            {
                bool isVoided = Convert.ToBoolean(Request["isVoided"]);
                if (isVoided)
                {
                    voided = "Voided";
                    voidedString = "Ticket voided";
                }
                else
                {
                    voided = "Cancelled";
                    voidedString = "Ticket cancelled";
                }
            }
            else
            {
                voidedString = Request["index"];
                voided = Request["RequestType"].ToString() + "ed";
            }
            decimal adminFee = Convert.ToDecimal(Request["adminFee"]);
            decimal supplierFee = Convert.ToDecimal(Request["supplierFee"]);
            //TODO
            OfflineBooking offBooking = new OfflineBooking();
            offBooking.Load(offBookingId);

            if (offBooking.Status == "Cancelled" || offBooking.Status == "Voided" || offBooking.Status == "Refunded")
            {
                Response.Write("Already Refunded");
                Response.End();

            }
            if (Request["isRefundOffTicket"] == null || Request["isCancelOffTicket"] == string.Empty)
            {
                remarks = Request["remarks"];
            }
            else
            {
                remarks = "Refunded for Ticket No -" + ticketNo;
            }

            int agencyId = offBooking.AgencyId;
            AgentMaster agency = new AgentMaster(agencyId);
            PriceAccounts price = new PriceAccounts();
            price.Load(offBooking.PriceId);

            //decimal money = ((int)Agencytype.Service == Agency.GetAgencyTypeId(agencyId)) ? price.GetServiceAgentPrice() - (adminFee + supplierFee + price.OtherCharges) : price.GetAgentPrice() - (adminFee + supplierFee + price.OtherCharges);
            decimal money = ((int)Agencytype.Service == agency.AgentType) ? price.GetServiceAgentPrice() - (adminFee + supplierFee + price.OtherCharges) : price.GetAgentPrice() - (adminFee + supplierFee + price.OtherCharges);
            bool isLCC = offBooking.IsLCC;

            PaymentDetails pd = new PaymentDetails();
            pd.AgencyId = agencyId;
            pd.PaymentDate = DateTime.Now.ToUniversalTime();
            pd.Remarks = "Credit Note as Ticket Refund for Offline Booking of Ticket No:" + offBooking.TicketNo;
            if (agency.AgentType == (int)Agencytype.Cash || agency.AgentType == (int)Agencytype.Service)
            {
                pd.IsLCC = true;
            }
            else
            {
                pd.IsLCC = isLCC;
            }

            pd.Amount = money;
            pd.RemainingAmount = money;
            pd.OurBankDetailId = 1;
            pd.ModeOfPayment = CT.AccountingEngine.PaymentMode.Credit;
            pd.Invoices = new List<InvoiceCollection>();
            pd.Status = PaymentStatus.Accepted;
            pd.CreatedBy = loggedMemberId;

            CancellationCharges cancellationCharge = new CancellationCharges();
            cancellationCharge.AdminFee = adminFee;
            cancellationCharge.SupplierFee = supplierFee;
            cancellationCharge.ReferenceId = offBooking.OfflineBookingId;
            cancellationCharge.ProductType = ProductType.Flight;
            cancellationCharge.ItemTypeId = InvoiceItemTypeId.Offline;

            BookingHistory bh = new BookingHistory();
            bh.BookingId = Convert.ToInt32(Request["offBookingId"]);
            bh.EventCategory = EventCategory.Refund;
            bh.Remarks = "Refunded for OfflineBookingId -" + offBooking.OfflineBookingId;
            bh.CreatedBy = loggedMemberId;

            //Ledger ledger = new Ledger();
            //ledger.Load(agencyId);
            //LedgerTransaction ledgerTxn = new LedgerTransaction();
            //ledgerTxn.LedgerId = ledger.LedgerId;

            //ledgerTxn.Amount = (agency.AgencyTypeId == (int)Agencytype.Service) ? price.GetServiceAgentPrice() - price.OtherCharges : price.GetAgentPrice() - price.OtherCharges;
            ////ledgerTxn.Narration = remarks;
            //ledgerTxn.ReferenceId = pd.PaymentDetailId;
            //ledgerTxn.ReferenceType = ReferenceType.OfflineRefund;
            //if (agency.AgencyTypeId == (int)Agencytype.Cash || agency.AgencyTypeId == (int)Agencytype.Service)
            //{
            //    ledgerTxn.IsLCC = true;
            //}
            //else
            //{
            //    ledgerTxn.IsLCC = isLCC;
            //}

            //if (Request["isRefundOffTicket"] == null || Request["isCancelOffTicket"] == string.Empty)
            //{
            //    ledgerTxn.Notes = Request["remarks"];
            //    remarks = "Refunded for Ticket No -" + offBooking.TicketNo;
            //}
            //else
            //{
            //    ledgerTxn.Notes = "Ticket Refunded";
            //}
            //ledgerTxn.Notes = "Ticket Refunded";
            //ledgerTxn.Date = DateTime.UtcNow;
            //ledgerTxn.CreatedBy = loggedMemberId;
            //NarrationBuilder objNarrationCancelationCharge = new NarrationBuilder();
            //LedgerTransaction ledgerTxnCancelCharge = new LedgerTransaction();
            //ledgerTxnCancelCharge.LedgerId = ledger.LedgerId;
            //ledgerTxnCancelCharge.Amount = -(supplierFee + adminFee);

            //ledgerTxnCancelCharge.ReferenceId = pd.PaymentDetailId;
            //ledgerTxnCancelCharge.ReferenceType = ReferenceType.OfflineCancellationCharge;
            //if (agency.AgencyTypeId == (int)Agencytype.Cash || agency.AgencyTypeId == (int)Agencytype.Service)
            //{
            //    ledgerTxnCancelCharge.IsLCC = true;
            //}
            //else
            //{
            //    ledgerTxnCancelCharge.IsLCC = isLCC;
            //}

            //ledgerTxnCancelCharge.Notes = "";
            //ledgerTxnCancelCharge.Date = DateTime.UtcNow;
            //ledgerTxnCancelCharge.CreatedBy = loggedMemberId;
            //pd.ModeOfPayment = PaymentMode.CreditNote;

            using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
            {
                try
                {
                    #region Enter in Payment Table
                    //pd.Save();

                    //if (voided == "Cancelled" || voided == "Refunded")
                    //{
                    //objNarrationCancelationCharge.DocNo = AccountUtility.RaiseOffLineCreditNote(offBookingId, adminFee + supplierFee, offBooking.Price, offBooking.AgencyId, offBooking.IsLCC, offBooking.IsDomestic, remarks, loggedMemberId, pd.PaymentDetailId, ProductType.Flight, BookingStatus.Cancelled);
                    //}
                    //else
                    //{
                    //    objNarrationCancelationCharge.DocNo = AccountUtility.RaiseOffLineCreditNote(offBookingId, adminFee + supplierFee, offBooking.Price, offBooking.AgencyId, offBooking.IsLCC, offBooking.IsDomestic, remarks, loggedMemberId, pd.PaymentDetailId, ProductType.Flight,BookingStatus.Void);
                    //}
                    #endregion
                    #region
                    bh.Save();
                    #endregion
                    #region Entry in Cancellation Charge Table

                    cancellationCharge.PaymentDetailId = 1;//Temp//pd.PaymentDetailId;
                    cancellationCharge.Save();

                    #endregion

                    #region Two New Entries in LedgerTransaction One for ticket refund one for charge
                    //objNarrationCancelationCharge.Remarks = remarks;
                    //ledgerTxn.ReferenceId = pd.PaymentDetailId;
                    //ledgerTxn.Narration = objNarrationCancelationCharge;
                    //ledgerTxn.Save();
                    //ledgerTxnCancelCharge.ReferenceId = pd.PaymentDetailId;
                    //objNarrationCancelationCharge.TicketNo = ticketNo;
                    //objNarrationCancelationCharge.Remarks = "Cancellation Charges";
                    //objNarrationCancelationCharge.Remarks = remarks;
                    //ledgerTxnCancelCharge.Narration = objNarrationCancelationCharge;
                    //ledgerTxnCancelCharge.Save();
                    #endregion

                    #region Update Ledger's Current Balance
                    //if (!isLCC && agency.AgencyTypeId != (int)Agencytype.Cash && agency.AgencyTypeId != (int)Agencytype.Service)
                    //{
                    //    Ledger.UpdateCurrentBalance(agencyId, money, loggedMemberId);
                    //}
                    //else
                    //{
                    //    Agency.UpdateLCCBalance(agencyId, money, loggedMemberId);
                    //}
                    #endregion

                    #region Change Tikcet status to Voided/Cancelled
                    if (voided != string.Empty)
                    {
                        offBooking.SetTicketStatus(offBooking.OfflineBookingId, voided);
                    }
                    #endregion

                    #region Update Queue Status
                    //Update Queue Status, Change Request  status
                    if (Request["isRefundOffTicket"] != null && Convert.ToBoolean(Request["isCancelOffTicket"]))
                    {
                        ServiceRequest serviceRequest = new ServiceRequest();
                        //Update Queue Status
                        CT.Core.Queue.SetStatus(QueueType.Request, Convert.ToInt32(Request["requestId"]), QueueStatus.Completed, loggedMemberId, 0, "Completed");
                        serviceRequest.UpdateServiceRequestAssignment(Convert.ToInt32(Request["requestId"]), (int)ServiceRequestStatus.Completed, loggedMemberId, (int)ServiceRequestStatus.Completed, null);
                    }
                    #endregion
                    updateTransaction.Complete();
                    Response.Write(voidedString);
                }
                catch (SqlException)
                {
                    //System.Data.SqlClient.SqlConnection.ClearPool(new SqlConnection(Technology.Data.ConncetionStringUsingModule.Technology));
                }
            }
            //Utility.AgencyLccBal.Remove(agencyId);
        }


        else if (Request["isManualEntry"] != null && Convert.ToBoolean(Request["isManualEntry"]))
        {
            try
            {
                int agencyId = Convert.ToInt32(Request["agencyId"]);
                bool isLCC = Convert.ToBoolean(Request["isLCC"]);
                string remarks = Request["remarks"];
                decimal amount = Convert.ToDecimal(Request["amount"]);

                //PaymentDetails pd = null;
                //if (Convert.ToBoolean(Request["isDebit"]))
                //{
                //    amount = -amount;
                //}
                //else
                //{
                //    pd = new PaymentDetails();
                //    pd.AgencyId = agencyId;
                //    pd.PaymentDetailId = 0;
                //    pd.PaymentDate = DateTime.Now.ToUniversalTime();

                //    pd.IsLCC = (bool)isLCC;
                //    pd.Invoices = new List<InvoiceCollection>();
                //    pd.Amount = amount;
                //    pd.RemainingAmount = amount;
                //    pd.OurBankDetailId = 0;
                //    pd.ReferenceNo = 0;
                //    pd.ModeOfPayment = PaymentMode.ManualCredit;
                //    pd.Status = PaymentStatus.Accepted;
                //    pd.Remarks = "Manual Credit Entry.";
                //    pd.CreatedBy = loggedMemberId;
                //}

                //Ledger ledger = new Ledger();
                //ledger.Load(agencyId);
                //LedgerTransaction ledgerTxn = new LedgerTransaction();
                //ledgerTxn.LedgerId = ledger.LedgerId;
                //ledgerTxn.Amount = amount;
                //NarrationBuilder objNarration = new NarrationBuilder();
                //objNarration.Remarks = remarks;
                //ledgerTxn.Narration = objNarration;
                //ledgerTxn.ReferenceType = ReferenceType.ManualEntry;
                //ledgerTxn.IsLCC = isLCC;
                //ledgerTxn.Notes = "Manual Entry :- " + AccountsDetail.GetMaxDocumentSequenceNumber("JV");
                //ledgerTxn.Date = DateTime.UtcNow;
                //ledgerTxn.CreatedBy = loggedMemberId;
                //using (System.Transactions.TransactionScope updateTransaction =
                //new System.Transactions.TransactionScope())
                //{

                //    if (!Convert.ToBoolean(Request["isDebit"]))
                //    {
                //        pd.Save();
                //        ledgerTxn.ReferenceId = pd.PaymentDetailId;
                //    }

                //    ledgerTxn.Save();
                //    if (!isLCC)
                //    {
                //        Ledger.UpdateCurrentBalance(agencyId, amount, loggedMemberId);
                //    }
                //    else
                //    {
                //        Agency.UpdateLCCBalance(agencyId, amount, loggedMemberId);
                //    }
                //    updateTransaction.Complete();
                //}
                //Utility.AgencyLccBal.Remove(agencyId);
            }

            catch (Exception ex)
            {
                Audit.Add(EventType.Account, Severity.High, loggedMemberId, ex.Message, "");
            }

        }
        else if (Request["isOneDayCredit"] != null && Convert.ToBoolean(Request["isOneDayCredit"]))
        {
            try
            {
                int agencyId = Convert.ToInt32(Request["agencyId"]);
                decimal amount = Convert.ToDecimal(Request["amount"]);
                DateTime oneDayCreditdate = Convert.ToDateTime(Request["oneDayCreditDate"]);
                DateTime date = DateTime.Today;
                if (oneDayCreditdate.Date == DateTime.Today.Date)
                {
                    date = DateTime.Now;
                }
                else
                {
                    date = new DateTime(oneDayCreditdate.Year, oneDayCreditdate.Month, oneDayCreditdate.Day, 0, 0, 0);
                }
                //Ledger.UpdateOneDayCredit(agencyId, amount, date, loggedMemberId);
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Account, Severity.High, loggedMemberId, ex.Message, "");
            }
        }
        else if (Request["RefundedAmount"] != null && Convert.ToBoolean(Request["RefundedAmount"]))
        {
            int ticketId = Convert.ToInt32(Request["ticketId"]);
            CancellationCharges cancellationCharge = new CancellationCharges();
            Ticket ticket = new Ticket();
            ticket.Load(ticketId);
            cancellationCharge.Load(ticketId, ProductType.Flight);
            Response.Write(cancellationCharge.AdminFee.ToString() + "," + cancellationCharge.SupplierFee.ToString() + "," + ticket.Status);
        }
        else if (Request["isBookingHistory"] != null && Convert.ToBoolean(Request["isBookingHistory"]) && Convert.ToBoolean(Request["ForTrain"] == "true"))
        {
            List<BookingHistory> listBookingHistory = new List<BookingHistory>();
            listBookingHistory = BookingHistory.GetBookingHistoryForProductId(ProductType.Train, Convert.ToInt32(Request["bookingId"]));
            string bookingHistoryBlock = string.Empty;
            if (listBookingHistory.Count <= 0)
            {
                bookingHistoryBlock += "<div class=\"fleft\"  style=\"width:620px; height:150px;overflow:auto;\">";
                bookingHistoryBlock += "<div class=\"fleft center full-width margin-bottom-5\" style=\"text-align:center;\">";
                bookingHistoryBlock += "<span class=\"fleft\" style=\"width:610px;text-align:center;\">No History is Maintained for this Booking</span></div></div>";

            }
            else
            {
                int height = 5;
                string membName = "";
                int remarksHeight = 18;
                int membNameHeight = 18;
                bookingHistoryBlock += "<div class=\"fleft\" id=\"BHContainerInner\"style=\"width:535px;background-color:#efefef;border: 1px solid #082b53;padding-top:5px;padding-bottom:3px\">";
                for (int i = 0; i < listBookingHistory.Count; i++)
                {
                    remarksHeight = (((listBookingHistory[i].Remarks.Length * 7) / 280) + 1) * 18;
                    membName = new UserMaster(listBookingHistory[i].CreatedBy).FirstName;
                    membNameHeight = (((membName.Length * 7) / 100) + 1) * 18;
                    height += remarksHeight >= membNameHeight ? remarksHeight : membNameHeight;
                    bookingHistoryBlock += "<div class=\"fleft margin-bottom-5\">";
                    bookingHistoryBlock += "<span class=\"fleft width-70 margin-right-3\">" + Util.UTCToIST(listBookingHistory[i].CreatedOn).ToString("dd/MM/yyyy") + "</span>";
                    bookingHistoryBlock += "<span class=\"fleft width-60 margin-right-3\">" + Util.UTCToIST(listBookingHistory[i].CreatedOn).ToString("HHmm") + "hrs</span>";
                    bookingHistoryBlock += "<span class=\"fleft width-280 margin-right-3\">" + listBookingHistory[i].Remarks + "</span>";
                    bookingHistoryBlock += "<span class=\"fleft width-100px \">" + new UserMaster(listBookingHistory[i].CreatedBy).FirstName + "</span>";
                    bookingHistoryBlock += "</div>";
                }
                height += 3;
                bookingHistoryBlock += "<input type=\"hidden\" id=\"height\" value=\"" + height + "\" \\>";
                bookingHistoryBlock += "</div>";
            }

            Response.Write(bookingHistoryBlock);
        }
        #region Booking History
        else if (Request["isBookingHistory"] != null && Convert.ToBoolean(Request["isBookingHistory"]))
        {
            List<BookingHistory> listBookingHistory = new List<BookingHistory>();
            listBookingHistory = BookingHistory.GetBookingHistory(Convert.ToInt32(Request["bookingId"]));
            string bookingHistoryBlock = string.Empty;
            bookingHistoryBlock += "<div class=\"fleft bold font-16 margin-bottom-5\" style=\"width:620px; color:rgb(49,71,119); Background:rgb(190,202,228);\">";
            bookingHistoryBlock += "<span class=\"fleft padding-bottom-5 padding-top-5 padding-left-5\">Booking History</span>";
            bookingHistoryBlock += "<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" onclick=\"Element.hide('BookingHistory'); return false;\">Close</a></label></div>";
            if (listBookingHistory.Count <= 0)
            {
                bookingHistoryBlock += "<div class=\"fleft\"  style=\"width:620px; height:150px;overflow:auto;\">";
            }
            for (int i = 0; i < listBookingHistory.Count; i++)
            {
                bookingHistoryBlock += "<div class=\"fleft contract-right-top-heading-width padding-5 light-gray-bg margin-bottom-5\">";
                bookingHistoryBlock += "<span class=\"fleft width-95 margin-right-5\">" + Util.UTCToIST(listBookingHistory[i].CreatedOn).ToString("dd/MM/yyyy") + "</span>";
                bookingHistoryBlock += "<span class=\"fleft width-60 margin-right-5\">" + Util.UTCToIST(listBookingHistory[i].CreatedOn).ToString("HHmm") + "hrs</span>";
                bookingHistoryBlock += "<span class=\"fleft width-95 margin-right-5\">" + listBookingHistory[i].EventCategory.ToString() + "</span>";
                bookingHistoryBlock += "<span class=\"fleft width-210 margin-right-5\">" + listBookingHistory[i].Remarks + "</span>";
                bookingHistoryBlock += "<span class=\"fleft width-100px \">" + new UserMaster(listBookingHistory[i].CreatedBy).FirstName + "</span>";
                bookingHistoryBlock += "</div>";
            }

            if (listBookingHistory.Count <= 0)
            {
                bookingHistoryBlock += "<div class=\"fleft center full-width margin-bottom-5\" style=\"text-align:center;\">";
                bookingHistoryBlock += "<span class=\"fleft\" style=\"width:610px;text-align:center;\">No History is Maintained for this Booking</span>";
            }
            bookingHistoryBlock += "</div>";
            Response.Write(bookingHistoryBlock);
        }


        else if (Request["isVisaQueueHistory"] != null)
        {
            List<Visa.VisaQueue> lstVisaQueue = new List<Visa.VisaQueue>();
            lstVisaQueue = Visa.VisaQueue.GetVisaQueueHistory(Convert.ToInt32(Request["visaId"]));
            string bookingHistoryBlock = string.Empty;
            bookingHistoryBlock += "<div class=\"fleft bold font-16 margin-bottom-5\" style=\"width:1000px; color:rgb(49,71,119); Background:rgb(190,202,228);\">";
            bookingHistoryBlock += "<span class=\"fleft padding-bottom-5 padding-top-5 padding-left-5\">Queue History</span>";
            bookingHistoryBlock += "<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" onclick=\"document.getElementById('QueueHistory').style.display='none'\">Close</a></label></div>";
            if (lstVisaQueue.Count <= 0)
            {
                bookingHistoryBlock += "<div class=\"fleft\"  style=\"width:700px; height:150px;overflow:auto;\">";
            }

            bookingHistoryBlock += "<div style='width:690px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">";
            bookingHistoryBlock += "<span class=\"fleft width-90 margin-right-5\"><b>Date</b></span>";
            bookingHistoryBlock += "<span class=\"fleft width-60 margin-right-5\"><b>Time</b></span>";
            bookingHistoryBlock += "<span class=\"fleft width-90 margin-right-5\"><b>Status</b></span>";
            // bookingHistoryBlock += "<span class=\"fleft width-60 margin-right-5\"><b>Deposit Fee</b></span>";
            //  bookingHistoryBlock += "<span class=\"fleft width-70 margin-right-5\"><b>Refundable Fee</b></span>";
            bookingHistoryBlock += "<span class=\"fleft width-350 margin-right-5\"><b>Remarks</b></span>";
            bookingHistoryBlock += "<span class=\"fleft width-200px \"><b>User</b></span>";
            bookingHistoryBlock += "</div>";

            for (int i = 0; i < lstVisaQueue.Count; i++)
            {
                bookingHistoryBlock += "<div style='width:690px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">";
                bookingHistoryBlock += "<span class=\"fleft width-90  margin-right-5\">" + Util.UTCToIST(lstVisaQueue[i].UpdatedOn).ToString("dd/MM/yyyy") + "</span>";
                bookingHistoryBlock += "<span class=\"fleft width-60 margin-right-5\">" + Util.UTCToIST(lstVisaQueue[i].UpdatedOn).ToString("HH:mm") + "hrs</span>";
                bookingHistoryBlock += "<span class=\"fleft width-90 margin-right-5\">" + (Visa.VisaStatus)Enum.Parse(typeof(Visa.VisaStatus), lstVisaQueue[i].VisaStatusId.ToString()) + "</span>";
                // bookingHistoryBlock += "<span class=\"fleft width-60 margin-right-5\">" + lstVisaQueue[i].VisaDepositFee.ToString("#,##0.00") + "</span>";
                //  bookingHistoryBlock += "<span class=\"fleft width-60 margin-right-5\">" + lstVisaQueue[i].VisaRefundableFee.ToString("#,##0.00") + "</span>";
                bookingHistoryBlock += "<span class=\"fleft width-350 margin-right-5\">" + lstVisaQueue[i].VisaRemarks + "</span>";
                bookingHistoryBlock += "<span class=\"fleft width-50 \">" + new UserMaster(lstVisaQueue[i].UpdatedBy).FirstName + "</span>";
                bookingHistoryBlock += "</div>";
            }

            if (lstVisaQueue.Count <= 0)
            {
                bookingHistoryBlock += "<div class=\"fleft center full-width margin-bottom-5\" style=\"text-align:center;\">";
                bookingHistoryBlock += "<span class=\"fleft\" style=\"width:700px;text-align:center;\">No History is Maintained for this Queue</span>";
            }
            bookingHistoryBlock += "</div>";
            Response.Write(bookingHistoryBlock);
        }

        //else if (Request["isPacakgeQueueHistory"] != null)
        //{
        //    List<Holidaypackagequeue> lstPackageQueue = new List<Holidaypackagequeue>();
        //    lstPackageQueue = Holidaypackagequeue.GetPackageQueueHistory(Convert.ToInt32(Request["packageQueueId"]));
        //    string bookingHistoryBlock = string.Empty;
        //    bookingHistoryBlock += "<div class=\"fleft booking-history-top\">";
        //    bookingHistoryBlock += "<span class=\"fleft\">Queue History</span>";
        //    //bookingHistoryBlock += "<img class=\"hand margin-right-5\" alt=\"close\" src=\"Images/close.gif\" onclick=\"hideDiv('" + Request["packageQueueId"].ToString() + "')\" />";
        //    //bookingHistoryBlock += "<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" onclick=\"Element.hide('QueueHistory'); return false;\">Close</a></label></div>";
        //    bookingHistoryBlock += "<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" onclick=\"Element.hide('history-" + Request["packageQueueId"].ToString() + "'); return false;\">Close</a></label></div>";
        //    if (lstPackageQueue.Count <= 0)
        //    {
        //        bookingHistoryBlock += "<div class=\"fleft\"  style=\"width:700px; height:150px;overflow:auto;\">";
        //    }

        //    bookingHistoryBlock += "<div style='width:690px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">";
        //    bookingHistoryBlock += "<span class=\"fleft width-90 margin-right-5\"><b>Date</b></span>";
        //    bookingHistoryBlock += "<span class=\"fleft width-60 margin-right-5\"><b>Time</b></span>";
        //    bookingHistoryBlock += "<span class=\"fleft width-90 margin-right-5\"><b>Status</b></span>";
        //    bookingHistoryBlock += "<span class=\"fleft width-350 margin-right-5\"><b>Remarks</b></span>";
        //    bookingHistoryBlock += "<span class=\"fleft width-200px \"><b>User</b></span>";
        //    bookingHistoryBlock += "</div>";

        //    for (int i = 0; i < lstPackageQueue.Count; i++)
        //    {
        //        bookingHistoryBlock += "<div style='width:690px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">";
        //        bookingHistoryBlock += "<span class=\"fleft width-90  margin-right-5\">" + Utility.UTCtoISTtimeZoneConverter(lstPackageQueue[i].LastModifiedOn.ToString()).ToString("dd/MM/yyyy") + "</span>";
        //        bookingHistoryBlock += "<span class=\"fleft width-60 margin-right-5\">" + Utility.UTCtoISTtimeZoneConverter(lstPackageQueue[i].LastModifiedOn.ToString()).ToString("HH:mm") + "hrs</span>";
        //        bookingHistoryBlock += "<span class=\"fleft width-90 margin-right-5\">" + (PackageStatus)Enum.Parse(typeof(PackageStatus), lstPackageQueue[i].PacakgeStatus.ToString()) + "</span>";
        //        bookingHistoryBlock += "<span class=\"fleft width-350 margin-right-5\">" + lstPackageQueue[i].HistoryRemarks + "</span>";
        //        bookingHistoryBlock += "<span class=\"fleft width-200px \">" + Member.GetMemberName(lstPackageQueue[i].LastModifiedBy) + "</span>";
        //        bookingHistoryBlock += "</div>";
        //    }

        //    if (lstPackageQueue.Count <= 0)
        //    {
        //        bookingHistoryBlock += "<div class=\"fleft center full-width margin-bottom-5\" style=\"text-align:center;\">";
        //        bookingHistoryBlock += "<span class=\"fleft\" style=\"width:700px;text-align:center;\">No History is Maintained for this Queue</span>";
        //    }
        //    bookingHistoryBlock += "</div>";
        //    Response.Write(bookingHistoryBlock);
        //}

        //else if (Request["isPacakgepaymentInfo"] != null)
        //{
        //    Holidaypackagequeue PackageQueue = new Holidaypackagequeue();
        //    PackageQueue.Load(Convert.ToInt32(Request["packageQueueId"]));
        //    string bookingHistoryBlock = string.Empty;
        //    bookingHistoryBlock += "<div class=\"fleft booking-history-top\">";
        //    bookingHistoryBlock += "<span class=\"fleft\">Payment Information</span>";
        //    //bookingHistoryBlock += "<img class=\"hand margin-right-5\" alt=\"close\" src=\"Images/close.gif\" onclick=\"hideDiv('" + Request["packageQueueId"].ToString() + "')\" />";
        //    //bookingHistoryBlock += "<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" onclick=\"Element.hide('QueueHistory'); return false;\">Close</a></label></div>";
        //    bookingHistoryBlock += "<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" onclick=\"Element.hide('PaymentInfo-" + Request["packageQueueId"].ToString() + "'); return false;\">Close</a></label></div>";
            

        //    bookingHistoryBlock += "<div style='width:690px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">";
        //    bookingHistoryBlock += "<span class=\"fleft width-90 margin-right-5\"><b>Order Id</b></span>";
        //    bookingHistoryBlock += "<span class=\"fleft width-200 margin-right-5\"><b>Payment Id</b></span>";
        //    bookingHistoryBlock += "<span class=\"fleft width-90 margin-right-5\"><b>Amount</b></span>";
        //    bookingHistoryBlock += "<span class=\"fleft width-60 margin-right-5\"><b>Status</b></span>";
        //    bookingHistoryBlock += "<span class=\"fleft width-60px \"><b>Payment Sources</b></span>";
        //    bookingHistoryBlock += "</div>";

           
        //        bookingHistoryBlock += "<div style='width:690px;' class=\"fleft  padding-5 light-gray-bg margin-bottom-5\">";
        //        bookingHistoryBlock += "<span class=\"fleft width-90  margin-right-5\">" + PackageQueue.OrderId + "</span>";
        //        bookingHistoryBlock += "<span class=\"fleft width-200 margin-right-5\">" + PackageQueue.PaymentId + "</span>";
        //        bookingHistoryBlock += "<span class=\"fleft width-90 margin-right-5\">" + PackageQueue.HotelPrice.ToString("#0.00") + "</span>";
        //        bookingHistoryBlock += "<span class=\"fleft width-60 margin-right-5\">" + (PackagePaymentStatus)PackageQueue.PaymentStatus + "</span>";
        //        bookingHistoryBlock += "<span class=\"fleft width-200px \">" + "CozmoPG" + "</span>";
        //        bookingHistoryBlock += "</div>";
            

           
        //    bookingHistoryBlock += "</div>";
        //    Response.Write(bookingHistoryBlock);
        //}




        //else if (Request["isReadPackagePaxProfile"] != null)
        //{
        //    List<Packagepaxdetails> lstPackageQueue = new List<Packagepaxdetails>();
        //    lstPackageQueue = Packagepaxdetails.GetPackagePaxDetails(Convert.ToInt32(Request["packageQueueId"]));
        //    string paxQueueBlock = string.Empty;
        //    paxQueueBlock += "<div class=\"fleft booking-history-top\">";
        //    paxQueueBlock += "<span class=\"fleft\">Pax Details</span>";
        //    //bookingHistoryBlock += "<img class=\"hand margin-right-5\" alt=\"close\" src=\"Images/close.gif\" onclick=\"hideDiv('" + Request["packageQueueId"].ToString() + "')\" />";
        //    //bookingHistoryBlock += "<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" onclick=\"Element.hide('QueueHistory'); return false;\">Close</a></label></div>";
        //    paxQueueBlock += "<label class=\"fright font-12 padding-top-5 padding-right-5\"><a href=\"#\" onclick=\"Element.hide('PaxProfile-" + Request["packageQueueId"].ToString() + "'); return false;\">Close</a></label></div>";
        //    if (lstPackageQueue.Count <= 0)
        //    {
        //        paxQueueBlock += "<div class=\"fleft\" >";
        //    }

        //        paxQueueBlock += "<div class=\"fleft light-gray-bg margin-bottom-5\">";
        //        paxQueueBlock += "<span class=\"pax-name padding-5\"><b>Name</b></span>";
        //        paxQueueBlock += "<span class=\"pax-type padding-5\"><b>Pax Type</b></span>";
        //        paxQueueBlock += "<span class=\"pax-sex padding-5\"><b>Gender</b></span>";
        //        paxQueueBlock += "<span class=\"pax-address padding-5\"><b>Contact Details</b></span>";
        //        paxQueueBlock += "</div>";
        //        paxQueueBlock += "<div class=\"left-col-wd\">";
        //    for (int i = 0; i < lstPackageQueue.Count; i++)
        //    {
        //        paxQueueBlock += "<div class=\"fleft light-gray-bg margin-bottom-5\">";
        //        paxQueueBlock += "<span class=\"pax-name\">" + lstPackageQueue[i].FirstName + " " + lstPackageQueue[i].LastName.ToString() + "</span>";
        //        paxQueueBlock += "<span class=\"pax-type\">" + lstPackageQueue[i].PType + "</span>";
        //        paxQueueBlock += "<span class=\"pax-sex\">" + lstPackageQueue[i].Sex + "</span>";
                
        //        //paxQueueBlock += "<span class=\"fleft width-90  margin-right-5\">" + Utility.UTCtoISTtimeZoneConverter(lstPackageQueue[i].LastModifiedOn.ToString()).ToString("dd/MM/yyyy") + "</span>";
        //        paxQueueBlock += "</div>";
        //    }
        //        paxQueueBlock += "</div>";
        //        paxQueueBlock += "<div class=\"light-gray-bg margin-bottom-5 right-col-add\">";
        //        paxQueueBlock += "<span><label>Address:</label><em>" + lstPackageQueue[0].Address.Line1 +",";
        //        if (lstPackageQueue[0].Address.Line2 != "")
        //        {
        //        paxQueueBlock += "<br />"+ lstPackageQueue[0].Address.Line2 + " ," + "<br />";
        //        }
        //        paxQueueBlock += lstPackageQueue[0].Address.CityName + "," + "<br />";
        //        paxQueueBlock += Country.GetCountry(lstPackageQueue[0].Address.CountryCode).CountryName + " ," + "<br /></em></span>";
        //        paxQueueBlock += "<span><label>Ph:</label><em>" + lstPackageQueue[0].Address.Phone + "</em></span>";
        //        paxQueueBlock += "<span><label>Email:</label><em>" + lstPackageQueue[0].Address.Email + "</em></span>";
        //        paxQueueBlock += "</div>";
        //    if (lstPackageQueue.Count <= 0)
        //    {
        //        paxQueueBlock += "<div class=\"fleft center full-width margin-bottom-5\" style=\"text-align:center;\">";
        //        paxQueueBlock += "<span class=\"fleft\" style=\"width:700px;text-align:center;\">No History is Maintained for this Queue</span>";
        //    }
        //    paxQueueBlock += "</div>";
        //    Response.Write(paxQueueBlock);
        //}



        #endregion
    }

}
