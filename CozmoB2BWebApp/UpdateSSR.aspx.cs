using System;
using CT.BookingEngine;
using CT.MetaSearchEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;


public partial class UpdateSSR :CT.Core.ParentPage// System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //TODO: how does this authorization handls the post variables in case of redirection.
        AuthorizationCheck();
        string pnr = string.Empty;
        string result = string.Empty;
        string message = string.Empty;
        string url = string.Empty;
        int memberId = (int)Settings.LoginInfo.UserID;

        //if (Session["roleId"] == null || Session["memberId"] == null)
        //{
        //    message = "Your login Session seems expired. Re-Login to continue.";
        //}
        //else
        //{
        #region Old code kept for reference temporarily.
        //pnr = Request["pnr"];
        //int bookingId = Convert.ToInt32(Request["bookingId"]);
        //BookingDetail booking = new BookingDetail(bookingId);
        //Product[] products = BookingDetail.GetProductsLine(bookingId);
        //FlightItinerary itinerary = new FlightItinerary();
        //for (int i = 0; i < products.Length; i++)
        //{
        //    if (products[i].ProductTypeId == (int)ProductType.Flight)
        //    {
        //        itinerary = new FlightItinerary(products[i].ProductId);
        //        break;
        //    }
        //}
        //List<KeyValuePair<string, SSR>> ssrList = new List<KeyValuePair<string, SSR>>();

        //if (itinerary.FlightBookingSource == BookingSource.Amadeus || itinerary.FlightBookingSource == BookingSource.WorldSpan)
        //{
        //    if (itinerary.FlightBookingSource == BookingSource.Amadeus)
        //    {
        //        ssrList = Amadeus.GenerateSSR(itinerary);
        //    }
        //    else if (itinerary.FlightBookingSource == BookingSource.WorldSpan)
        //    {
        //        ssrList = Worldspan.GenerateSSRPaxList(itinerary);
        //    }
        //    //SaveSSR(booking.BookingId, itinerary.PNR, Convert.ToString(itinerary.FlightBookingSource), ssrList);
        //}
        //string bookingSource = Request["bookingSource"];
        //List<string> templist = new List<string>();
        //try
        //{
        //    MetaSearchEngine.SaveSSR(itinerary);
        //    url = Request["redirectUrl"];
        //}
        //catch (BookingEngineException ex)
        //{
        //    url = Request["redirectUrl"] + "&message=" + ex.Message;
        //    Audit.Add(EventType.UpdateSSR, Severity.High, memberId, "Update SSR failed: " + Utility.GetExceptionInformation(ex, string.Empty), Request.ServerVariables["REMOTE_ADDR"]);
        //}
        #endregion
        int flightId = 0;
        url = Request.QueryString["redirectUrl"];
        if (Request.QueryString["flightId"] != null)
        {
            flightId = Convert.ToInt32(Request.QueryString["flightId"]);
            try
            {
                MetaSearchEngine.UpdateSSR(flightId);
            }
            catch (BookingEngineException ex)
            {
                message = "Update SSR failed.";
                Audit.Add(EventType.UpdateSSR, Severity.High, memberId, "Update SSR failed: " + Util.GetExceptionInformation(ex, string.Empty), Request.ServerVariables["REMOTE_ADDR"]);
            }
        }
        else
        {
            message = "Update SSR needs flight id which was not supplied.";
        }

        
        //MetaSearchEngine.UpdateSSR(
        if (message.Length > 0)
        {
            url += "&message=" + message;
        }
        Response.Redirect(url, true);
        //}
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {            
            Response.Redirect("AbandonSession.aspx", true);
        }
        //else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.MakeFlightBookings))
        //{
        //    String values = "?errMessage=You are not authorised to update SSR.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
    }
}
