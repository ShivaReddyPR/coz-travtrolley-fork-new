using System;
using System.Collections.Generic;
using System.Transactions;
using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;

public partial class ManualTicket :CT.Core.ParentPage// System.Web.UI.Page
{
    protected FlightItinerary itinerary;
    protected BookingDetail booking;
    protected AgentMaster agency;
    protected UserMaster agent;
    protected int bookingId = 0;
    protected int paxId;
    protected int paxIndex;
    protected UserMaster loggedInMember = new UserMaster();
    protected bool editMode = false;
    protected bool ETicketMode = false;

    // variables used for edit mode.
    protected Ticket ticket = new Ticket();
    protected Ticket[] eTicket = new Ticket[0];
    protected string editIssueInExchange = string.Empty;
    protected string editTicketNumber = string.Empty;
    protected string editConjunctionNumber = string.Empty;
    protected string editTicketDesignator = string.Empty;
    protected string editFOP = string.Empty;
    protected string editTourCode = string.Empty;
    protected string editOriginalIssue = string.Empty;
    protected string editValidatingAirline = string.Empty;
    protected string editEndorsement = string.Empty;
    protected string editFareCalculation = string.Empty;
    protected string editStatus = string.Empty;
    protected bool eTicketMode = false;
    protected string errorMessage = string.Empty;
    protected bool errorEticket = false;
    protected string ipAddr = string.Empty;
    UserPreference preference = new UserPreference();
    public Dictionary<int, string> supplierList;

    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            AuthorizationCheck();   // Checks for login session.
            loggedInMember = new UserMaster(Settings.LoginInfo.UserID);
            ipAddr = Request.ServerVariables["REMOTE_ADDR"];
            supplierList = Supplier.loadSupplier();
            if (Request.QueryString["bookingId"] != null && Request.QueryString["paxId"] != null)
            {
                bookingId = Convert.ToInt32(Request.QueryString["bookingId"]);
                paxId = Convert.ToInt32(Request.QueryString["paxId"]);
            }

            if (Request["editMode"] != null && Request["ticketId"] != null && Request["editMode"].ToLower() == "true")
            {
                editMode = true;
            }

        if (Request["eticketMode"] != null && Request["eticketMode"] == "true")
        {
            ETicketMode = true;
        }

        Product[] products = BookingDetail.GetProductsLine(bookingId);
        booking = new BookingDetail(bookingId);

        agency = new AgentMaster(booking.AgencyId);
        agent = new UserMaster(booking.CreatedBy);
        for (int i = 0; i < products.Length; i++)
        {
            if (products[i].ProductTypeId == (int)ProductType.Flight)
            {
                itinerary = new FlightItinerary(products[i].ProductId);
                break;
            }
        }

        for (int i = 0; i < itinerary.Passenger.Length; i++)
        {
            if (itinerary.Passenger[i].PaxId == paxId)
            {
                paxIndex = i;
                break;
            }
        }

        if (Request["postBack"] != null)
        {
            //TODO: Read from request variables when needed.
            PriceAccounts tempPrice = itinerary.Passenger[paxIndex].Price;
            //TODO: Remove this part when correction is made in passenger class for accomodating currency in Price object.
            tempPrice.Currency = Request["baseCurrency"];

            ticket.TicketId = Convert.ToInt32(Request["ticketId"]);
            ticket.PaxFirstName = Request["firstName"];
            ticket.PaxLastName = Request["lastName"];
            ticket.Title = Request["title"];
            ticket.IssueDate = DateTime.Parse(Request["issueDate"]);
            ticket.IssueInExchange = Request["issueInExchange"];
            ticket.TicketNumber = Request["ticketNumber"];
            ticket.ConjunctionNumber = Request["conjunctionNo"];
            ticket.TicketDesignator = Request["ticketDesignator"];
            ticket.Price = tempPrice;
            ticket.FOP = Request["fop"];
            ticket.TourCode = Request["tourCode"];
            ticket.OriginalIssue = Request["originalIssue"];
            ticket.ValidatingAriline = Request["validatingAirlineCode"];
            ticket.Endorsement = Request["endorsement"];
            ticket.FareCalculation = Request["fareCalculation"];
            ticket.FlightId = itinerary.FlightId;
            ticket.PaxId = itinerary.Passenger[paxIndex].PaxId;
            ticket.PaxType = itinerary.Passenger[paxIndex].Type;
            if (Request["ticketStatus"] == null)
            {
                ticket.Status = "OK";
            }
            else
            {
                ticket.Status = Request["ticketStatus"];
            }
            ticket.ETicket = Convert.ToBoolean(Request["eTicketMode"]);
            ticket.CreatedBy = (int)loggedInMember.ID;
            string remarks = string.Empty;
            string fareRule = string.Empty;
            List<string> airlineNameRemarksList = new List<string>();
            List<string> airlineNameFareRuleList = new List<string>();
            foreach (FlightInfo segment in itinerary.Segments)
            {
                string airlineCode = segment.Airline;
                Airline airline = new Airline();
                airline.Load(airlineCode);
                if (!airlineNameRemarksList.Contains(airline.AirlineName) && airline.Remarks.Length != 0)
                {
                    airlineNameRemarksList.Add(airline.AirlineName);
                    remarks += "<span style=\"font-weight:bold\">" + airline.AirlineName + "</span>" + "&nbsp;:&nbsp;" + airline.Remarks + "<br />";
                }
                if (!airlineNameFareRuleList.Contains(airline.AirlineName) && airline.FareRule.Length != 0)
                {
                    airlineNameFareRuleList.Add(airline.AirlineName);
                    fareRule += "<span style=\"font-weight:bold\">" + airline.AirlineName + "</span>" + "&nbsp;:&nbsp;" + airline.FareRule + "<br />";
                }
                int supplierid=Convert.ToInt32(Request["SupplierList"]);
                if (supplierid != 0)
                {
                    ticket.SupplierId = supplierid;
                }
                else
                {
                    ticket.SupplierId = airline.ConsolidatorId;
                }
            }
            ticket.Remarks = remarks;
            ticket.FareRule = fareRule;
            ticket.TaxBreakup = new List<KeyValuePair<string, decimal>>();
            int taxCount = Convert.ToInt32(Request["taxRowCount"]);
            string tempTaxType;
            string tempTax;
            decimal tempTaxValue;
            for (int i = 0; i < taxCount; i++)
            {
                tempTaxType = Request["taxType" + i];
                tempTax = Request["taxValue" + i];
                if (tempTaxType.Length != 0 && tempTax.Length != 0)
                {
                    tempTaxValue = Convert.ToDecimal(Request["taxValue" + i]);
                    ticket.TaxBreakup.Add(new KeyValuePair<string, decimal>(tempTaxType, tempTaxValue));
                }
            }

            int agencyPrimaryMemberId = (int)Settings.LoginInfo.UserID;//UserMaster.GetPrimaryMemberId(booking.AgencyId);

            //Airline service fee for Email Itinerary
            Dictionary<string, string> sFeeTypeList = new Dictionary<string, string>();
            Dictionary<string, string> sFeeValueList = new Dictionary<string, string>();
            FlightItinerary.GetMemberPrefServiceFee(booking.AgencyId, ref sFeeTypeList, ref sFeeValueList);
            string sfType = string.Empty;
            decimal sfValue = 0;
            if (!itinerary.CheckDomestic("" + CT.Configuration.ConfigurationSystem.LocaleConfig["CountryCode"] + ""))
            {
                if (sFeeTypeList.ContainsKey("INTL") && sFeeValueList.ContainsKey("INTL"))
                {
                    sfType = sFeeTypeList["INTL"];
                    sfValue = Convert.ToDecimal(sFeeValueList["INTL"]);
                }
            }
            else if (sFeeTypeList.ContainsKey(itinerary.ValidatingAirlineCode) && sFeeValueList.ContainsKey(itinerary.ValidatingAirlineCode))
            {
                sfType = sFeeTypeList[itinerary.ValidatingAirlineCode];
                sfValue = Convert.ToDecimal(sFeeValueList[itinerary.ValidatingAirlineCode]);
            }

            if (sfType == "FIXED")
            {
                ticket.ServiceFee = sfValue;
            }
            else if (sfType == "PERCENTAGE")
            {
                decimal totalfare = ticket.Price.PublishedFare + ticket.Price.Tax + ticket.Price.OtherCharges + ticket.Price.AdditionalTxnFee + ticket.Price.TransactionFee;
                ticket.ServiceFee = totalfare * sfValue / 100;
            }
            else
            {
                ticket.ServiceFee = 0;
            }
            preference = preference.GetPreference(agencyPrimaryMemberId, UserPreference.ServiceFee, UserPreference.ServiceFee);
            if (preference.Value != null)
            {
                if (preference.Value == UserPreference.ServiceFeeInTax)
                {
                    ticket.ShowServiceFee = ServiceFeeDisplay.ShowInTax;
                }
                else if (preference.Value == UserPreference.ServiceFeeShow)
                {
                    ticket.ShowServiceFee = ServiceFeeDisplay.ShowSeparately;
                }
            }
            else
            {
                ticket.ShowServiceFee = ServiceFeeDisplay.ShowInTax;
            }
            string error = string.Empty;

            bool ticketingComplete = false;
            bool saveSuccessful = false;
            ticket.PtcDetail = new List<SegmentPTCDetail>();

            itinerary.TravelDate = DateTime.Parse(Request["date" + 0]);
            itinerary.Origin = itinerary.Segments[0].Origin.AirportCode;
            itinerary.Destination = itinerary.Segments[itinerary.Segments.Length-1].Destination.AirportCode;
            itinerary.LastModifiedBy = (int)loggedInMember.ID;
            itinerary.LastModifiedOn = DateTime.Now;

            for (int i = 0; i < itinerary.Segments.Length; i++)
            {

                itinerary.Segments[i].StopOver = (Request["xo" + i] == "O");
                itinerary.Segments[i].Origin.AirportCode = Request["Org" + i];
                itinerary.Segments[i].Destination.AirportCode = Request["des" + i];
                itinerary.Segments[i].Airline = Request["airline" + i];
                itinerary.Segments[i].FlightNumber = Request["flightNumber" + i];
                itinerary.Segments[i].BookingClass = Request["class" + i];
                itinerary.Segments[i].DepartureTime = DateTime.Parse(Request["date" + i]);
                itinerary.LastModifiedBy = (int)loggedInMember.ID;
                itinerary.Segments[i].Status = Request["status" + i];

                ticket.PtcDetail.Add(new SegmentPTCDetail());
                ticket.PtcDetail[i].PaxType = FlightPassenger.GetPTC(ticket.PaxType);
                ticket.PtcDetail[i].FareBasis = Request["fareBasis" + i];
                ticket.PtcDetail[i].NVA = Request["nva" + i];
                ticket.PtcDetail[i].NVB = Request["nvb" + i];
                ticket.PtcDetail[i].Baggage = Request["allow" + i];
                ticket.PtcDetail[i].SegmentId = itinerary.Segments[i].SegmentId;
            }

            Airline air = new Airline();
            air.Load(itinerary.ValidatingAirlineCode);
            string supplierCode = string.Empty;

            if (ticket.SupplierId > 0)
            {
                supplierCode = Supplier.GetSupplierCodeById(ticket.SupplierId);
            }
            if (editMode)
            {
                ticket.SupplierRemmitance = SupplierRemmitance.LoadSupplierRemmitance(ticket.TicketId);
                ticket.SupplierRemmitance.SupplierCode = supplierCode;
            }
            else
            {
                ticket.SupplierRemmitance = new SupplierRemmitance();
                ticket.SupplierRemmitance.PublishedFare = ticket.Price.PublishedFare;
                ticket.SupplierRemmitance.AccpriceType = ticket.Price.AccPriceType;
                ticket.SupplierRemmitance.NetFare = ticket.Price.NetFare;
                ticket.SupplierRemmitance.CessTax = 0;
                ticket.SupplierRemmitance.Currency = ticket.Price.Currency;
                ticket.SupplierRemmitance.CurrencyCode = ticket.Price.CurrencyCode;
                ticket.SupplierRemmitance.Markup = ticket.Price.Markup;
                ticket.SupplierRemmitance.OtherCharges = ticket.Price.OtherCharges;
                ticket.SupplierRemmitance.OurCommission = ticket.Price.OurCommission;
                ticket.SupplierRemmitance.OurPlb = ticket.Price.OurPLB;
                ticket.SupplierRemmitance.RateOfExchange = ticket.Price.RateOfExchange;
                ticket.SupplierRemmitance.ServiceTax = ticket.Price.SeviceTax;
                ticket.SupplierRemmitance.SupplierCode = supplierCode;

                ticket.SupplierRemmitance.Tax = ticket.Price.Tax;
                ticket.SupplierRemmitance.TdsCommission = 0;
                ticket.SupplierRemmitance.TdsPLB = 0;
                ticket.SupplierRemmitance.CreatedBy = ticket.CreatedBy;
                ticket.SupplierRemmitance.CreatedOn = DateTime.UtcNow;
                ticket.SupplierRemmitance.CommissionType = air.CommissionType;
            }
            try
            {
                using (TransactionScope saveTicketScope = new TransactionScope())
                {
                    FlightItinerary.RefreshItinerarySelectedData(itinerary);
                    ticket.Save();
                    ticket.SupplierRemmitance.TicketId = ticket.TicketId;
                    ticket.SupplierRemmitance.Save();
                    
                    for (int i = 0; i < itinerary.Segments.Length; i++)
                    {
                        itinerary.Segments[i].Save();
                        ticket.PtcDetail[i].Save();
                    }
                    BookingHistory bh = new BookingHistory();
                    bh.BookingId = booking.BookingId;
                    bh.EventCategory = EventCategory.Ticketing;
                    if (editMode)
                    {
                        bh.Remarks = "Ticket No- " + ticket.TicketNumber.ToString() + " is updated manually.";
                    }
                    else
                    {
                        bh.Remarks = "Manual Ticket is generated " + ticket.TicketNumber.ToString();
                    }

                        bh.CreatedBy = (int)loggedInMember.ID;
                        bh.Save();
                        if (FlightItinerary.IsTicketed(itinerary) && !editMode)
                        {
                            if (booking.Status == BookingStatus.Released)
                            {
                                Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + bookingId.ToString() + "&message=This booking has been cancelled, So this action cannot be performed.", true);
                            }
                            else if (booking.Status != BookingStatus.InProgress && booking.Status != BookingStatus.OutsidePurchase)
                            {
                                Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + bookingId.ToString() + "&message=This ticket cannot be saved.", true);
                            }
                            else
                            {
                                ticketingComplete = true;
                                int invoiceNumber = CT.AccountingEngine.AccountUtility.RaiseInvoice(itinerary.ProductId, "", (int)loggedInMember.ID, itinerary.ProductType, 1);
                                if (itinerary.PaymentMode == ModeOfPayment.CreditCard)
                                {
                                    //MetaSearchEngine.MakeCCPaymentEntry(booking, itinerary, loggedInMember, invoiceNumber);
                                }
                                //else if (agency.AgencyTypeId == 1 || agency.AgencyTypeId == (int)Agencytype.Service)
                                //{
                                //    Invoice.UpdateInvoiceStatus(invoiceNumber, InvoiceStatus.Paid, (int)loggedInMember.ID);
                                //}
                                booking.Unlock(BookingStatus.Ticketed, (int)loggedInMember.ID);
                                BookingHistory bookingHistory = new BookingHistory();
                                bookingHistory.BookingId = booking.BookingId;
                                bookingHistory.EventCategory = EventCategory.Payment;
                                bookingHistory.Remarks = "Invoice generated";
                                bookingHistory.CreatedBy = (int)loggedInMember.ID;
                                bookingHistory.Save();
                            }
                        }
                        saveSuccessful = true;
                        saveTicketScope.Complete();
                    }
                    if (saveSuccessful)
                    {
                        if (ticketingComplete)
                        {
                            //send SMS
                            BookingUtility.AgencyLccBal.Remove(booking.AgencyId);
                            decimal ticketAmount = 0;
                            PriceAccounts price = ticket.Price;

                            if (price.NetFare > 0)
                            {
                                ticketAmount = price.NetFare + price.Tax + price.Markup;
                            }
                            else
                            {
                                ticketAmount = price.PublishedFare + price.Tax;
                            }


                            //Updating Agent Balance
                            Settings.LoginInfo.AgentBalance -= ticketAmount;
                            AgentMaster agent1 = new AgentMaster(Settings.LoginInfo.AgentId);
                            agent1.CreatedBy= Settings.LoginInfo.UserID;
                            agent1.UpdateBalance(-ticketAmount);
                            //if (SMS.IsSMSSubscribed(booking.AgencyId, SMSEvent.ETicketGeneration))
                            //{
                            //    Hashtable hTable = new Hashtable();
                            //    hTable.Add("AgencyId", booking.AgencyId);
                            //    hTable.Add("PNR", itinerary.PNR);
                            //    hTable.Add("EventType", SMSEvent.ETicketGeneration);
                            //    SMS.SendSMSThread(hTable);
                            //}
                            Response.Redirect("ViewBookingForTicket.aspx?bookingId=" + bookingId, false);
                        }
                        else
                        {
                            Response.Redirect("CreateTicket.aspx?bookingId=" + bookingId, false);
                        }
                    }
                    else
                    {
                        Audit.Add(EventType.Ticketing, Severity.Normal, (int)loggedInMember.ID, "Manual Ticket Errro: Could not save the Ticket. pnr=" + itinerary.PNR, ipAddr);
                        Response.Redirect("CreateTicket.aspx?bookingId=" + bookingId.ToString() + "&errorMessage=Could not save the Ticket. Please try again.", true);
                    }
                }
                catch (BookingEngineException ex)
                {
                    Audit.Add(EventType.Ticketing, Severity.Normal, (int)loggedInMember.ID, Util.GetExceptionInformation((Exception)ex, errorMessage), ipAddr);
                    Response.Redirect("CreateTicket.aspx?bookingId=" + bookingId.ToString() + "&errorMessage=Could not save the Ticket. Please try again.", true);
                }
                catch (TimeoutException ex)
                {
                    Audit.Add(EventType.Ticketing, Severity.Normal, (int)loggedInMember.ID, Util.GetExceptionInformation((Exception)ex, errorMessage), ipAddr);
                    Response.Redirect("CreateTicket.aspx?bookingId=" + bookingId.ToString() + "&errorMessage=Could not save the Ticket. Please try again.", true);
                }
                catch (TransactionException ex)
                {
                    Audit.Add(EventType.Ticketing, Severity.Normal, (int)loggedInMember.ID, Util.GetExceptionInformation((Exception)ex, errorMessage), ipAddr);
                    Response.Redirect("CreateTicket.aspx?bookingId=" + bookingId.ToString() + "&errorMessage=Could not save the Ticket. Please try again.", true);
                }

        }

        if (editMode)
        {
            ticket.Load(Convert.ToInt32(Request["ticketId"]));
            editIssueInExchange = ticket.IssueInExchange;
            editTicketNumber = ticket.TicketNumber;
            editConjunctionNumber = ticket.ConjunctionNumber;
            editTicketDesignator = ticket.TicketDesignator;
            editFOP = ticket.FOP;
            editTourCode = ticket.TourCode;
            editOriginalIssue = ticket.OriginalIssue;
            editValidatingAirline = ticket.ValidatingAriline;
            editEndorsement = ticket.Endorsement;
            editFareCalculation = ticket.FareCalculation;
        }
        else
        {
            ticket.PtcDetail = SegmentPTCDetail.GetSegmentPTCDetail(itinerary.FlightId);
        }

        // if - E Ticket mode
        if (ETicketMode)
        {
            try
            {
                //if (itinerary.FlightBookingSource == BookingSource.WorldSpan)
                //{
                //    eTicket = Worldspan.RetrieveETicketInfo(Request["pnrNo"]);
                //}
                //else if (itinerary.FlightBookingSource == BookingSource.Amadeus)
                //{
                //    Amadeus.RetrieveItinerary(Request["pnrNo"], out eTicket);
                //}
                //else if (itinerary.FlightBookingSource == BookingSource.Galileo)
                //{
                //    GalileoApi.RetrieveItinerary(Request["pnrNo"], out eTicket);
                //}
                 if (itinerary.FlightBookingSource == BookingSource.UAPI)
                {

                    FlightItinerary urItinerary = new FlightItinerary(FlightItinerary.GetFlightId(Request["pnrNo"]));
                    string uRecord = urItinerary.UniversalRecord;
                    if (!string.IsNullOrEmpty(uRecord))
                    {
                        SourceDetails agentDetails = new SourceDetails();
                        if (Settings.LoginInfo.IsOnBehalfOfAgent)
                        {
                            agentDetails = Settings.LoginInfo.OnBehalfAgentSourceCredentials["UA"];
                        }
                        else
                        {
                            agentDetails = Settings.LoginInfo.AgentSourceCredentials["UA"];
                        }
                        CT.BookingEngine.GDS.UAPICredentials credentials = new CT.BookingEngine.GDS.UAPICredentials();
                        credentials.UserName = agentDetails.UserID;
                        credentials.Password = agentDetails.Password;
                        credentials.TargetBranch = agentDetails.HAP;

                        //itinerary = UAPI.RetrieveItinerary(uRecord, out eTicket, credentials);
                    }
                }
            }
            catch (BookingEngineException excep)
            {
                errorMessage = "Error: " + excep.Message;
                errorEticket = true;
                //break;
            }

            // TO DO - error E Tiocketing info not available

            if (eTicket.Length <= 0)
            {
                errorMessage = "E Ticket information does not exist for PNR No: " + Request["pnrNo"] + "<br> Please try Manual Ticketing ";
                errorEticket = true;
            }

            if (errorEticket == false)
            {
                //TODO: Read from request variables when needed.
                PriceAccounts tempPrice = itinerary.Passenger[paxIndex].Price;
                //TODO: Remove this part when correction is made in passenger class for accomodating currency in Price object.
                tempPrice.Currency = "" + CT.Configuration.ConfigurationSystem.LocaleConfig["CurrencyCode"] + ""; // TO DO: Change currency
                //TODO: paxIndex does not guarantee for index of same pax in eTicket as well.
                int eticketIndex = -1;
                for (int i = 0; i < eTicket.Length; i++)
                {
                    if (eTicket[i].PaxKey == itinerary.Passenger[paxIndex].PaxKey)
                    {
                        eticketIndex = i;
                    }
                }
                eTicket[eticketIndex].Price = tempPrice;
                eTicket[eticketIndex].CreatedBy = (int)loggedInMember.ID;

                //ticket.TicketId = Convert.ToInt32(Request["ticketId"]); // TODO:generated after ticket is generated
                eTicket[eticketIndex].PaxId = itinerary.Passenger[paxIndex].PaxId;

                    editIssueInExchange = eTicket[eticketIndex].IssueInExchange;
                    editTicketNumber = eTicket[eticketIndex].TicketNumber;
                    editConjunctionNumber = eTicket[eticketIndex].ConjunctionNumber;
                    editTicketDesignator = eTicket[eticketIndex].TicketDesignator;
                    editFOP = eTicket[eticketIndex].FOP;
                    editTourCode = eTicket[eticketIndex].TourCode;
                    editOriginalIssue = eTicket[eticketIndex].OriginalIssue;
                    editValidatingAirline = eTicket[eticketIndex].ValidatingAriline;
                    editEndorsement = eTicket[eticketIndex].Endorsement;
                    editFareCalculation = eTicket[eticketIndex].FareCalculation;
                    eTicketMode = true;
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, (int)Settings.LoginInfo.UserID, ex.Message, "");
        }
    }
    private void AuthorizationCheck()
    {
        if (Settings.LoginInfo == null)
        {            
            Response.Redirect("AbandonSession.aspx", true);
        }
        //else if (!Role.IsAllowedTask((int)Session["roleId"], (int)Task.IssueFlightTickets))
        //{
        //    String values = "?errMessage=You are not authorised to access " + Page.Title + " page.";
        //    values += "&requestUri=" + Request.Url.ToString();
        //    Response.Redirect("Default.aspx" + values, true);
        //}
    }
}
