<%@ Page Language="C#" AutoEventWireup="true" Inherits="PaymentProcessing" Codebehind="PaymentProcessing.aspx.cs" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<%@ Import Namespace="CT.TicketReceipt.BusinessLayer" %>
<%@ Import Namespace="CT.Configuration" %>
<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Cozmo Payment Gateway</title>
    <link rel="Stylesheet" href="css/override.css" />
    <script src="https://code.jquery.com/jquery-3.4.1.min.js" type="text/javascript"></script>
</head>
<body>
    <%if (paymentGateway == "CCAvenue")
        { %>
<form id="nonseamless" method="post" name="redirect" action="<%=paymentGatewaySite %>">  
<% try
   {
if (paymentGateway == "CCAvenue")
  {
      Session["ResultIndex"] = Request.QueryString["id"];
      string ccaRequest;
      CCA.Util.CCACrypto ccaCrypto = new CCA.Util.CCACrypto();
      string workingKey;
      workingKey = ConfigurationManager.AppSettings["CCA_Encrypt_Key"];
      string redirectUrl = Request.Url.Scheme+"://" + siteName + "/";
      //siteName = "localhost:3093";
      string strAccessCode = ConfigurationManager.AppSettings["CCA_Access_Code"];
      //string rootFolder = ConfigurationManager.AppSettings["RootFolder"].ToString();
      string preCurrency = string.Empty;
      if (ConfigurationManager.AppSettings["TestMode"].ToString() == "True")
      {
          preCurrency = "USD";
         // siteName = "localhost:29589";          
      }
      else if (agency != null && !string.IsNullOrEmpty(agency.AgentCurrency))
      {
          preCurrency = agency.AgentCurrency;
      }
      else
      {
          preCurrency = "AED";
      }

      if (!string.IsNullOrEmpty(ConfigurationManager.AppSettings["RootFolder"]))
      {
          redirectUrl += ConfigurationManager.AppSettings["RootFolder"] + "/";
      }

            if (Request.QueryString["redirect_url"] == null)
            {
                if (Session["PaxPageValues"] != null)
                {
                    redirectUrl += "/ViewSwitcher.aspx?id=" + pageParams[0] + "&uid=" + Settings.LoginInfo.UserID+ "&sid=" + Session["sessionId"].ToString() + "&type=C";
                }
                else
                {
                    redirectUrl += "/ViewSwitcher.aspx?id=" + pageParams[0] + "&uid=" + Settings.LoginInfo.UserID+ "&sid=" + Session["sessionId"].ToString() + "&type=N";
                }
            }
            else
            {
                redirectUrl += "/ViewSwitcher.aspx?id=" + pageParams[0] + "&uid=" + Settings.LoginInfo.UserID+ "&sid=" + Session["sessionId"].ToString() + "&type=N";
            }

            ccaRequest = "merchant_id=" + ConfigurationManager.AppSettings["CCAvenueMerchantId"] + "&order_id=" + orderId + "&amount=" + totalAmount + "&currency=" + preCurrency + "&redirect_url=" + redirectUrl + "&cancel_url=" + redirectUrl + "&language=EN&merchant_param1="+Settings.LoginInfo.UserID+"&merchant_param2="+Session["sessionId"].ToString()+"&merchant_param3="+(Session["PaxPageValues"] != null ? "C" : "N");
            string strEncRequest = ccaCrypto.Encrypt(ccaRequest, workingKey);
     %>
        <input type="hidden" name="Merchant_Id" value="<%=ConfigurationManager.AppSettings["CCAvenueMerchantId"]%>" />
	    <input type="hidden" name="Amount" value="<%=totalAmount%>" />
	    <input type="hidden" name="Order_Id" value="<%=orderId%>" />
	    <input type="hidden" name="Redirect_Url" value="<%=redirectUrl %>"/>
        <input type="hidden" id="encRequest" name="encRequest" value="<%=strEncRequest%>"/>
        <input type="hidden" name="access_code" id="access_code" value="<%=strAccessCode%>"/>          	    
    <input type="hidden" name="merchant_param1" value="<%=Settings.LoginInfo.UserID %>"/>
    <input type="hidden" name="merchant_param2" value="<%=Session["sessionId"].ToString() %>"/>
    <input type="hidden" name="merchant_param3" value="<%=(Session["PaxPageValues"] != null ? "C" : "N") %>"/>
    <script type="text/javascript">
        document.getElementById('nonseamless').submit();
    </script>
<%}
    }
    catch (Exception ex)
    {        
        if (Request.QueryString["redirect_url"] == null)
        {
            if (Session["PaxPageValues"] != null)
            {
                Response.Redirect("PaymentConfirmationBySegments.aspx?ErrorPG=" + ex.Message);
            }
            else
            {
                Response.Redirect("PaymentConfirmation.aspx?ErrorPG=" + ex.Message);
            }
        }
        else
        {
            Response.Redirect(Request.QueryString["redirect_url"] + "?ErrorPG=" + ex.Message);
        }
    }%>
    </form>
    <%}
    else if (paymentGateway == "SafexPay")
    {

        if (data != null && data.Count > 0)
        {%>
        <form id="PostForm" method="post" name="PostForm" action="<%=paymentGatewaySite%>">
            <% foreach (string key in data)
                {%>
            <input type="hidden" name='<%=key %>' value='<%=data[key]%>' />
            <%} %>


            <script type="text/javascript">
                document.PostForm.submit();
            </script>
            </form>
       
    <%}
            else
            {

                if (Request.QueryString["redirect_url"] == null)
                {
                    if (Session["PaxPageValues"] != null)
                    {
                        Response.Redirect("PaymentConfirmationBySegments.aspx?ErrorPG=" + "Payment values are not available");
                    }
                    else
                    {
                        Response.Redirect("PaymentConfirmation.aspx?ErrorPG=" + "Payment values are not available");
                    }
                }
                else
                {
                    Response.Redirect(Request.QueryString["redirect_url"] + "?ErrorPG=" + "Payment values are not available");
                }

            }
        }%>
        <%--else if (paymentGateway == "RazorPay")
        {
            string url = Request.IsSecureConnection ? "https://" : "http://";
            url += Request["HTTP_HOST"] + "/ViewSwitcher.aspx?id=" + pageParams[0] + "&uid=" + Settings.LoginInfo.UserID+ "&sid=" + Session["sessionId"].ToString() + (Session["PaxPageValues"] != null ? "&type=C" : "&type=N");
            if (Settings.LoginInfo.IsOnBehalfOfAgent)
                url += "&obaid=" + Settings.LoginInfo.OnBehalfAgentID;
            
    
    <button id="rzp-button1">Pay</button>
<script src="https://checkout.razorpay.com/v1/checkout.js"></script>
<script>
var options = {
    "key": "<%=ConfigurationSystem.RazorPayConfig["Key"] %>", // Enter the Key ID generated from the Dashboard
    "amount": "<%=totalAmount * 100 %>", // Amount is in currency subunits. Default currency is INR. Hence, 50000 refers to 50000 paise
    "currency": "INR",
    "name": "TravTrolley",
    "description": "Flight Transaction",
    "image": "https://ctb2b.cozmotravel.com/Upload/CMS/2/LOGO_2.png",
    "order_id": "<%=trackId %>", //
    "callback_url": "<%=url %>",
    "redirect": true,
    "prefill": {
        "name": "<%=itinerary.Passenger[0].FirstName %>",
        "email": "<%=itinerary.Passenger[0].Email %>",
        "contact": "<%=itinerary.Passenger[0].CellPhone.Replace("+", "").Replace("-", "") %>"
    },
    "notes": {
        "address": "<%=itinerary.Passenger[0].AddressLine1 %>"
    },
    "theme": {
        "color": "#3e7db7"
    }
};
var rzp1 = new Razorpay(options);
document.getElementById('rzp-button1').onclick = function(e){
    rzp1.open();
    e.preventDefault();
    }
    document.getElementById('rzp-button1').click();
</script>
        <%} %>--%>

</body>
</html>
