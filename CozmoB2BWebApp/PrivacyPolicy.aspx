﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Transaction.master" AutoEventWireup="true" Inherits="PrivacyPolicy" Codebehind="PrivacyPolicy.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
    <!--body container starts-->



<style> 
 
 .body_container { line-height:24px; font-size:14px; }

 </style>

<!-- Half Page Image Background Carousel Header -->


    
<!-- Half Page Image Background Carousel Header ends -->



    <div class="body_container">


        <h1>Privacy Policy</h1>
        <div class=" col-md-12">
            



            <p>
                <strong>General</strong><br />

                This privacy policy applies to all services provided by COZMO TRAVEL and its subsidiaries, who shall be collectively referred to as “we” or “us” herein this notice. 
  <br />
                <br />
                As a customer, you are in charge of your travel-planning. We know you want to remain in charge of your personal data as well. We value your trust and make it a high priority to ensure the security and confidentiality of the personal data you provide to us. It is a responsibility we take very seriously.
                <br />
                <br />

                We strive to secure your data and ensure it is not used for any unlawful activities and/or purposes other than those you have given permission for. We do so by enforcing data privacy and protection in our design phase. This concept is endorsed by the leadership group and the entire team. We recommend you review this Notice and become familiar with it.
                <br />
                <br />

                By providing personal data to us, you agree that this Notice will apply to how we handle your personal data. Furthermore, you consent to us collecting, processing and disclosing your personal data where the lawful basis for the same is contractual necessity. Under any other circumstance, we shall collect your consent before collecting, processing or sharing your data. If you do not agree with any part of this Notice, you must not provide your personal data to us.
                <br />
                <br />


                If you do not provide us with your personal data, or if you withdraw a consent that you have given under this Notice, this may affect our ability to provide services to you or negatively impact the services we can provide to you.
                <br />
                <br />

                With respect to General Data Protection Regulation 2016/679 (“GDPR”), we have taken the relevant steps to ensure compliance.
                <br />



            </p>


            <p>

                <div><strong>What data do we collect and how is it used</strong></div>



                In order to process the service requested and to ensure that your travel arrangements meet your requirements we need to use the information you provide. We collect data whenever you avail a service from us.<br />
                <br />

                We take full responsibility for ensuring that proper security measures are in place to protect your information. Under GDPR, the lawful basis for our processing activities fall under contractual necessity or legitimate interest. In situations where you share information regarding health concerns, the lawful basis is vital interests. Your consent, our processing activities and the data involved is clearly documented.
                <br />
                <br />


                We predominantly process the following types of personal data about you:<br />


                <ul>

                    <li>Contact information (such as name, telephone number, email address, residential/mailing address); </li>
                    <li>Passport details </li>
                    <li>Loyalty program / frequent flyer details </li>
                    <li>Information about your dietary requirements and health issues </li>
                    <li>Other details relevant to your travel arrangements or to the relevant travel service provider(s)  </li>
                    <li>Information from our website such as device model, IP address, the type of browser being used, usage pattern through cookies and browser settings, query logs and product usage logs. </li>

                </ul>



            </p>



            <p>
                The purposes for which we collect personal data include:
                <br />




                <div><strong>Service Fulfillment:</strong></div>

                <div>
                    <ul>
                        <li>To fulfill the services requested  </li>
                        <li>To send you requested information about the Service(s) </li>
                        <li>To respond to customer service requests, questions and concerns; </li>
                        <li>For identification of fraud or error, regulatory reporting and compliance;   </li>
                        <li>For internal accounting and administration;</li>
                        <li>To comply with our legal obligations and any applicable customs/immigration requirements</li>
                        <li>For other purposes as authorized or required by law </li>

                    </ul>
                </div>



                <div><strong>Marketing: </strong>
                    <br />
                </div>

                <div>
                    <ul>
                        <li>For servicing our relationship with you and maintaining our brands to service you better</li>
                        <li>For presenting options on our website, we think may interest you </li>
                        <li>To facilitate your participation in loyalty programs; </li>
                        <li>Use your personal data for customer engagement and to send electronic marketing materials to You including e-newsletters, email, SMS, social media channels etc. if you have opted-in to receive them. </li>
                    </ul>
                </div>



                <div><strong>Analytics:</strong></div>

                <div>We collect certain personal data of yours when you visit our websites. We use such data to</div>


                <div>
                    <ul>

                        <li>Assess needs of your business to determine or suggest suitable Service(s) (e.g.: redirect to corresponding domain as per customer location)</li>
                        <li>For analytical purposes based on use of our websites</li>
                        <li>For research and analysis in relation to our business and services, including trends and preferences in sales and travel destinations </li>
                        <li>Involve you in market research, evaluate customer satisfaction and seek feedback regarding our relationship and the service we have provided</li>

                    </ul>
                </div>


            </p>


            <p>



                <strong>With whom is your personal data shared</strong>
                <br />
                We do not and will not sell, rent out or trade your personal data. We must pass the information on to our partners such as airlines, hotels, transport companies immigration departments, government authorities etc. depending on the service provided to you. Our partners are professional and reputed firms that are leaders in their respective domains. Where data is shared, we take necessary steps to ensure confidentiality by implementing measures such as data pseudonymizing,wherever feasible. The information may also be provided to security or credit checking companies, public authorities such as customs/immigration if required by them, or as required by law.


            </p>




            <p>

                <strong>What are cookies and do we use them</strong>
                <br />

                A cookie is a text-only string of information that a website transfers to the cookie file of the browser on your computer's hard disk so that the website can remember who you are.<br />

                The website uses cookies for the following general purposes:<br />


                <ul>


                    <li>To help recognize your browser as a previous visitor and save and remember any preferences that may have been set while your browser was visiting our site.</li>
                    <li>To help us customize the content and advertisements provided to you on this website and on other sites across the Internet. </li>



                </ul>



                We cannot accept responsibility for any issues concerning your data collected by websites that are not directly under our control. Please also be aware that individual organizations operate their own policies regarding the use and sale of personal data and the use of cookies. If you have a concern regarding the way your personal data will be used on other sites then you are advised to read the relevant privacy statement.





            </p>



            <p>



                <strong>How to control and delete cookies</strong>
                <br />
                If you would like to restrict or block the cookies which are set by us, or any other website, you can do this through your browser settings. You can opt-in or opt-out of our cookies by selecting the appropriate options on our cookie pop-up that appears on the website. For information on how to delete or restrict cookies on your mobile phone refer to your handset manual. Please be aware that restricting cookies may impact the functionality of our website.



            </p>




            <p>

                <strong>How we protect your information </strong>
                <br />
                We want you to feel confident about working with us to plan and purchase your travel, so we are committed to protecting the information we collect. We have detailed policies and procedures in place to ensure confidentiality and rightful use of the data. We have implemented appropriate administrative, technical, and physical security procedures. We review our information collection, storage and processing practices periodically.We have physical security measures and access control systems to guard against unauthorized access. We restrict access to personal data to thosewho requireaccess for fulfillment of service. Access permissions are subject to strict contractual confidentiality obligations

            </p>




            <p>
                <strong>Data Retention</strong>
                <br />
                We retain your data for the duration of your association with us as the data that we retain is necessary for completion of our services and improving your experience as a customer. You can request the erasure of your data once you wish to end your association with us. At the point, your personal data will be removed from our internal servers and storage
            </p>



            <p>

                <div><strong>Data Breaches</strong></div>


                <div>Under GDPR, we liable to report a significant data breach to the concerned parties within 72 hours of discovery. We ensure that security measures are in place in order to ensure that a data breach is identified and the necessary mitigation actions are implemented immediately. We have defined policies and standard operating procedures with respect to data breach identification, mitigation and reporting.</div>



            </p>




            <p>


                <strong>Children’s Privacy</strong>
                <br />
                Should a child whom we know to be under 16 send personal data to us, we will use that information only to respond directly to that child to inform him or her that we must have parental consent or consent from a guardian in order to fulfill their request

            </p>



            <p>

                <div><strong>External Links</strong></div>


                <div>If any part of our website links you to other websites, those websites do not operate under our Privacy Policy. We recommend you examine the privacy statements posted on those websites to understand their procedures for collecting, using, and disclosing personal data.</div>

            </p>


            <p>

                <div><strong>Third Parties</strong></div>


                <div>
                    We do not intend our third-party service providers to use your personal data for their own purposes. We only permit them to process your personal data for specified purposes in accordance to our contractual terms. 
We do not collect or store any of your credit card information. All payments you make for the purchase of our services using your credit card are securely processed by the concerned banks/payment gateway.
                </div>


            </p>






            <p>




                <strong>Your rights in relation to the personal data we collect</strong>
                <br />
                As Data Subjects, under GDPR, you are entitled to:<br />


                <ul>


                    <li>Access i.e. to update, rectify, and erase your personal data held by us</li>
                    <li>Restrict us from processing or profiling any of your personal data, </li>
                    <li>Request to get your personal data in a machine-readable language </li>
                    <li>A responsible point of contact within the company i.e. a Data Protection Officer for any queries regarding your data and privacy</li>
                    <li>Withdraw consent for any processing activities you have consented to before</li>


                </ul>



                You can request any of these featuresand any other queries regarding your data by emailing us at legal@cozmotravel.com. We try to respond to all legitimate requests within one month. Occasionally it may take us longer than a month if your request is particularly complex or you have made a number of requests.<br />
                <br />

                We reserve the right to deny you access for any reason permitted under applicable laws. Such exemptions may include national security, corporate finance and confidential references.<br />
                <br />


                Pertaining to your right to be forgotten i.e. erasure of your personal data, we will remove your personal data from our storage sources. However, logs of transactions between you and the services we provide will be retained for internal audits and similar purposes. 
                <br />
                <br />

                You must always provide accurate information and you agree to update it whenever necessary.  You also agree that, in the absence of any update, we can assume that the information submitted to us is correct.<br />
                <br />

                In any of the situations listed above, we may request that you prove your identity by providing us with a copy of a valid means of identification for us to comply with our security obligations and to prevent unauthorized disclosure of personal data.














            </p>







            <p>
                <div><strong>Hiring Disclaimer:</strong>   </div>

                <div>We are aware of some individuals and organizations who are not associated with Cozmo Travel, sending emails or otherwise contacting candidates to make fraudulent offers of employment with Cozmo Travel. These people and organizations may request personal data or money from you in order to process the application. We would strongly advise you not to provide your bank account or credit card details as part of an employment application. </div>

                <br />



                <div>
                    We would like to advise all candidates applying for positions with Cozmo Travel to be aware of fraudulent emails and websites and to notify you of the following:
                    <br />
                    <br />
                </div>


                <strong>Cozmo Travel only advertises for job vacancies through the following websites:</strong>
                <br />

                <div>
                    <ul>


                        <li><a style="color: #14aeea" target="_blank" href="<%=Request.Url.Scheme%>://cozmotravel.com/career.aspx"><%=Request.Url.Scheme%>://cozmotravel.com/career.aspx</a> </li>
                        <li><a style="color: #14aeea" target="_blank" href="www.linkedin.com">www.linkedin.com</a></li>
                        <li><a style="color: #14aeea" href="www.gulftalent.com">www.gulftalent.com</a></li>
                        <li><a style="color: #14aeea" href=" <%=Request.Url.Scheme%>://www.naukrigulf.com"><%=Request.Url.Scheme%>://www.naukrigulf.com</a></li>

                        <li><a style="color: #14aeea" href=" <%=Request.Url.Scheme%>://www.naukri.com"><%=Request.Url.Scheme%>://www.naukri.com</a></li>



                    </ul>
                </div>





                <div>Our recruitment department will never email you from personal email accounts such as Hotmail, Yahoo or Gmail. Cozmo Travel email addresses always end in "@cozmotravel.com" or @cozmotravelworld.com. Cozmo Travel never charges fees for recruitment or visa processing or recruitment deposit. Cozmo Travel will never make a job offer without meeting a candidate face to face or via video conference / Skype. If you are asked for money as part of any Cozmo Travel recruitment, please do not pay and report it to <a style="color: #14aeea" target="_blank" href="mailto:legal@cozmotravel.com">legal@cozmotravel.com </a>with all accompanying e-mails and documents.</div>









            </p>



            <p>


                <div><strong>Changes to this Privacy Policy </strong></div>

                By visiting this website, you are accepting the practices described herein. Please note that we review our Privacy Policy from time to time, therefore, you may wish to periodically review this page to make sure that you have the current version.
                <br />



                Thank you for using the services of Cozmo Travel and its subsidiaries !
            </p>

            <div class="clearfix"> </div>
        </div>

        <div class="clearfix"> </div>


    </div>
   

<!--body container ends-->
</asp:Content>

