﻿using CT.BookingEngine.Insurance;
using CT.Core;
using CT.TicketReceipt.BusinessLayer;
using ReligareInsurance;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class ReligareInsuranceVocher : CT.Core.ParentPage
    {

        protected string imagePath = "";
        protected ReligareHeader header;
        protected string logoPath = string.Empty;
        protected AgentMaster agent;
        protected void Page_Load(object sender, EventArgs e)
        {
            try
            {
                if (Settings.LoginInfo != null)
                {
                    imagePath += ConfigurationManager.AppSettings["RootFolder"];
                    if (Request.QueryString["InsId"] != null)
                    {
                        header = new ReligareHeader();
                        header.Load(Convert.ToInt32(Request.QueryString["InsId"]));
                        if (header.Agent_id > 0)
                        {
                            agent = new AgentMaster(header.Agent_id);
                            logoPath = "http://ctb2bstage.cozmotravel.com/" + ConfigurationManager.AppSettings["AgentImage"] + agent.ImgFileName;
                            imgHeaderLogo.ImageUrl = logoPath;
                        }
                        SendInsuranceVoucherMail();
                        Session["quotation"] = null;
                        Session["religareHeader"] = null;
                    }
                    else
                    {
                        Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in Insurance Voucher :Query string header id is empty ", Request["REMOTE_ADDR"]);
                        Response.Redirect("ReligareInsurance.aspx");
                    }
                }
                else
                {
                    Response.Redirect("AbandonSession.aspx");
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Error occured in Insurance Voucher : " + ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }


        private void SendInsuranceVoucherMail()
        {
            try
            {
                if (ViewState["MailSent"] == null)
                {
                    string myPageHTML = "";
                    StringWriter sw = new StringWriter();
                    HtmlTextWriter htw = new HtmlTextWriter(sw);
                    printableArea.RenderControl(htw);
                    myPageHTML = sw.ToString();//.Replace(@"<td>Policy Link</td>", "").Replace(@"View Certificate", "");

                    string regex = "<td id=\"tdPolicy.*td>";
                    myPageHTML = Regex.Replace(myPageHTML, regex, "").ToString();
                    regex = "<td id=\"tdViewLink.*td>";
                    myPageHTML = Regex.Replace(myPageHTML, regex, "").ToString();

                    System.Collections.Generic.List<string> toArray = new System.Collections.Generic.List<string>();
                    toArray.Add(header.ReligarePassengers[0].Email);
                    string policyNo = header.Policy_no;
                    hdnpolicyNo.Value = header.Policy_no;
                    AgentMaster agency = new AgentMaster(header.Agent_id);
                    string bccEmails = string.Empty;
                    if (!string.IsNullOrEmpty(agency.Email1))
                    {
                        bccEmails = agency.Email1;
                    }
                    if (!string.IsNullOrEmpty(agency.Email2))
                    {
                        bccEmails = bccEmails + "," + agency.Email2;
                    }
                    try
                    {
                        CT.Core.Email.Send(ConfigurationManager.AppSettings["fromEmail"], ConfigurationManager.AppSettings["fromEmail"], toArray, "Insurance Confirmation - " + policyNo, myPageHTML, new Hashtable(), bccEmails);
                    }
                    catch { }
                    ViewState["MailSent"] = true;
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, (int)CT.TicketReceipt.BusinessLayer.Settings.LoginInfo.UserID, ex.ToString(), Request["REMOTE_ADDR"]);
            }
        }

        //Added By Hari
        [System.Web.Script.Services.ScriptMethod()]
        [System.Web.Services.WebMethod]
        public static string GetPolicyPDFData(string policyNo)
        {
            string pdfData = string.Empty;
            try
            {
                ReligarePolicyPurchase purchase = new ReligarePolicyPurchase();
                var pdfResponse = purchase.GetPolicyPdf(Convert.ToInt64(policyNo));
                string response = pdfResponse.ToString();
                string[] StreamData = response.Replace("StreamData", "$").Split('$');
                if (StreamData.Length > 1)
                {
                    pdfData = StreamData[1].Replace("&gt;", "").Replace("&lt;/", "").Trim();
                }
                return pdfData;
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, "Exception occured while calling the Religare pdf service." + ex.ToString(), "");
            }
            return pdfData;
        }
    }
}
