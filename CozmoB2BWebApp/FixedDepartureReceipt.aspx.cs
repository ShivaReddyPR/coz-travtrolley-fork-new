﻿using System;
using System.Data;
using CT.BookingEngine;

public partial class FixedDepartureReceipt :CT.Core.ParentPage// System.Web.UI.Page
{
    protected Activity activity = null;
    protected CT.TicketReceipt.BusinessLayer.AgentMaster agent;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Request.QueryString["ID"] != null)
        {
            activity = new Activity();
            activity.GetActivityForQueue(Convert.ToInt64(Request.QueryString["ID"]));
            int agentId = Convert.ToInt32(activity.TransactionHeader.Rows[0]["AgencyId"]);
            agent = new CT.TicketReceipt.BusinessLayer.AgentMaster(agentId);
            BindReceipt(activity);
        }
    }

    void BindReceipt(Activity activity)
    {        
        lblTourName.Text = activity.Name;
        lblBookingDate.Text = Convert.ToDateTime(activity.TransactionHeader.Rows[0]["Booking"]).ToString("dd MMM yyyy");
        lblDepartureDate.Text = lblBookingDate.Text;

        decimal amount = 0;
            //balance=0;
        foreach(DataRow dr in activity.FixedDepartureDetails.Rows)
        {
            amount += Convert.ToDecimal(dr["Amount"]);            
        }
        
        lblPackage.Text = agent.AgentCurrency + " " + Convert.ToDecimal(activity.TransactionHeader.Rows[0]["TotalPrice"]).ToString("N" + agent.DecimalValue);
        lblAmountPaid.Text = agent.AgentCurrency + " " + amount.ToString("N" + agent.DecimalValue);
        lblBalance.Text = agent.AgentCurrency + " " + (Convert.ToDecimal(activity.TransactionHeader.Rows[0]["TotalPrice"]) - amount).ToString("N" + agent.DecimalValue);
        lblNextPaymentDate.Text = Convert.ToDateTime(activity.FixedDepartureDetails.Rows[0]["NextPaymentDate"]).ToString("dd MMM yyyy");
        lblLeadPax.Text = activity.TransactionDetail.Rows[0]["FirstName"].ToString() + " " + activity.TransactionDetail.Rows[0]["LastName"].ToString();

        lblFirstPaymentDate.Text ="First Payment Date : "+ lblBookingDate.Text;

        if (activity.FixedDepartureDetails.Rows.Count > 1)
        {
            lblSecondPaymentDate.Text = "Second Payment Date : "+Convert.ToDateTime(activity.FixedDepartureDetails.Rows[1]["NextPaymentDate"]).ToString("dd MMM yyyy");
        }

        lblFileNo.Text = activity.TransactionHeader.Rows[0]["TripId"].ToString();
        lblPaxCount.Text = "AD: " + Convert.ToInt32(activity.TransactionHeader.Rows[0]["Adult"]) + " CH: " + Convert.ToInt32(activity.TransactionHeader.Rows[0]["Child"]);
    }
}
