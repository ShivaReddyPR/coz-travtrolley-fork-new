﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Web.UI.HtmlControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using System.IO;
using System.Collections.Generic;
using ReligareInsurance;
using CT.Core;

namespace CozmoB2BWebApp
{
    public partial class ReligareSumInsuredDetails :CT.Core.ParentPage
    {



        protected void Page_Load(object sender, EventArgs e)
        {
            this.Master.PageRole = true;
            if (!IsPostBack)
            {
                
                IntialiseControls();// Dropdown binded methods
                ddlTerm.Visible = false;
            }

        }
        #region Dropdown Binded methods
        #region Binding CT_T_Religare_ProductType table
        void BindProductTypeId()
        {
            try
            {
                DdlProductTypeName.DataSource = ReligareProductDetails.GetList();
                DdlProductTypeName.DataValueField = "PRODUCTID";
                DdlProductTypeName.DataTextField = "PRODUCTNAME";
                DdlProductTypeName.DataBind();
                DdlProductTypeName.Items.Insert(0, new ListItem("Select Product Type", "-1"));

            }
            catch (Exception ex)
            { throw ex; }
        }

        #endregion
        #region Binding CT_T_Religare_ProductDetails table
        void BindProductDetailsId(int product_type_id)
        {
            try
            {
                DdlProdcutDetailsId.DataSource = SumInsured.GetList(product_type_id); ;
                DdlProdcutDetailsId.DataValueField = "PRODUCT_ID";
                DdlProdcutDetailsId.DataTextField = "PRODUCT_NAME";
                DdlProdcutDetailsId.DataBind();
                DdlProdcutDetailsId.Items.Insert(0, new ListItem("Select Product Name", "-1"));


            }
            catch (Exception ex)
            { throw ex; }
        }
        #endregion
        #region SumInsuredId in CT_T_Religare_Sum_Insured_Master
        void BindSumInsuredId(int productTypeid, int productid)
        {
            try
            {
                DdlSumInsuredId.DataSource = InsuredDetails.GetListProductTypeDetailsId(productTypeid, productid);
                DdlSumInsuredId.DataValueField = "SUM_INSURED_ID";
                DdlSumInsuredId.DataTextField = "RANGE_DESCRIPTION";
                DdlSumInsuredId.DataBind();
                DdlSumInsuredId.Items.Insert(0, new ListItem("Select Range Description", "-1"));

            }
            catch (Exception ex) { throw ex; }
        }
        #endregion
        #region Loads the methods in dropdown
        void IntialiseControls()
        {
            BindProductTypeId();
            BindProductDetailsId(Utility.ToInteger(DdlProductTypeName.SelectedItem.Value));
            BindSumInsuredId(Utility.ToInteger(DdlProductTypeName.SelectedItem.Value), Utility.ToInteger(DdlProdcutDetailsId.SelectedItem.Value));
            
        }
        #endregion
        #endregion
        #region Methods
        void bindSearch()
        {

            try
            {

                DataTable dt = InsuredDetails.GetAdditionalServiceMasterDetails();
                if (dt.Rows.Count > 0)
                {
                    gvSearch.DataSource = dt;
                    gvSearch.DataBind();
                }
            }

            catch (Exception ex)
            {
                throw ex;
            }

        }

        void Clear()
        {
            txtAgeBand.Text = string.Empty;
            txtMemberInAgeBand.Text = string.Empty;
            txtPlan.Text = string.Empty;
            txtPremium.Text = string.Empty;
            txtPremiumRoundOff.Text = string.Empty;
            txtPremiumRoundOfWihtGst.Text = string.Empty;
            txtPremiumWithGst.Text = string.Empty;
            txtSumInsured.Text = string.Empty;
            txtTerm.Text = string.Empty;
            txtTripType.Text = string.Empty;
            ddlTerm.SelectedIndex = 0;
            DdlSumInsuredId.SelectedIndex = -1;
            DdlProductTypeName.SelectedIndex = -1;
            DdlProdcutDetailsId.SelectedIndex = -1;
            rbPed.Checked = false;
            rbPed1.Checked = false;
        }

        void Save()
        {
            try
            {
                InsuredDetails Idetails = new InsuredDetails();
                if (Utility.ToInteger(hdfMeid.Value) > 0)
                {
                    Idetails.ID = Utility.ToLong(hdfMeid.Value);
                }
                else
                {
                    Idetails.ID = -1;
                }

                Idetails.PRODUCT_TYPE_ID = Utility.ToInteger(DdlProductTypeName.SelectedItem.Value);
                Idetails.PRODUCT__ID = Utility.ToInteger(DdlProdcutDetailsId.SelectedItem.Value);
                Idetails.SUM_INSURED_ID = Utility.ToInteger(DdlSumInsuredId.SelectedItem.Value);
                Idetails.AGE_BAND = txtAgeBand.Text;
                Idetails.PLAN = txtPlan.Text;
                //Idetails.PED =Utility.ToInteger( rbPed.Text);
                Idetails.PED = rbPed.Checked ? 1 : 0;
                Idetails.SUM_INSURED = Convert.ToInt16(txtSumInsured.Text);
                Idetails.TRIP_TYPE = txtTripType.Text;
                Idetails.MEMBER_IN_AGE_BAND = int.Parse(txtMemberInAgeBand.Text);
                if (DdlProductTypeName.SelectedValue != "2")
                {
                    
                    Idetails.TERM = int.Parse(txtTerm.Text);

                }
                else
                {
                    Idetails.TERM = Utility.ToInteger(ddlTerm.SelectedItem.Value);
                }


                Idetails.PREMIUM = Convert.ToDecimal(txtPremium.Text);
                Idetails.PREMIUN_ROUNDOFF = Convert.ToDecimal(txtPremiumRoundOff.Text);
                Idetails.PREMIUM_WITH_GST = Convert.ToDecimal(txtPremiumWithGst.Text);
                Idetails.PREMIUM_ROUNDOFF_WITH_GST = Convert.ToDecimal(txtPremiumRoundOfWihtGst.Text);
                Idetails.CREATED_BY = Settings.LoginInfo.UserID;
                Idetails.Save();
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("Details Successfully", "", hdfMeid.Value == "0" ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated);
                btnSave.Text = "Save";
                Clear();

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        void Edit(long id)
        {
            try
            {
                InsuredDetails Idetails = new InsuredDetails(id);
                hdfMeid.Value = Utility.ToString(Idetails.ID);
                DdlProductTypeName.SelectedValue = Utility.ToString(Idetails.PRODUCT_TYPE_ID);
                BindProductDetailsId(Idetails.PRODUCT_TYPE_ID);
                DdlProdcutDetailsId.SelectedValue = Convert.ToString(Idetails.PRODUCT__ID);
                BindSumInsuredId(Idetails.PRODUCT_TYPE_ID, Idetails.PRODUCT__ID);
                DdlSumInsuredId.SelectedValue = Utility.ToString(Idetails.SUM_INSURED_ID);
                txtAgeBand.Text = Idetails.AGE_BAND;
                txtPlan.Text = Idetails.PLAN;
                if (Idetails.PED == 1)
                    rbPed.Checked = true;
                else if (Idetails.PED == 0)
                    rbPed1.Checked = true;
                //rbPed.Text = Utility.ToString(Idetails.PED);
                txtSumInsured.Text = Utility.ToString(Idetails.SUM_INSURED);
                txtTripType.Text = Idetails.TRIP_TYPE;
                txtMemberInAgeBand.Text = Utility.ToString(Idetails.MEMBER_IN_AGE_BAND);
                if (Idetails.PRODUCT_TYPE_ID != 2)
                {
                    txtTerm.Text = Utility.ToString(Idetails.TERM);
                }
                else
                {
                    ddlTerm.Visible = true;
                    txtTerm.Visible = false;
                    ddlTerm.SelectedValue = Utility.ToString(Idetails.TERM);
                    
                }
                txtTerm.Text = Utility.ToString(Idetails.TERM);
                txtPremium.Text = Utility.ToString(Idetails.PREMIUM);
                txtPremiumRoundOff.Text = Utility.ToString(Idetails.PREMIUN_ROUNDOFF);
                txtPremiumWithGst.Text = Utility.ToString(Idetails.PREMIUM_WITH_GST);
                txtPremiumRoundOfWihtGst.Text = Utility.ToString(Idetails.PREMIUM_ROUNDOFF_WITH_GST);
                btnSave.Text = "Update";
                btnClear.Text = "Cancel";

            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        #endregion
        #region Dropdown clicking events
        protected void DdlProductTypeName_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindProductDetailsId(Utility.ToInteger(DdlProductTypeName.SelectedItem.Value));
                if (DdlProductTypeName.SelectedValue == "2")
                {
                    ddlTerm.Visible = true;
                    txtTerm.Visible = false;
                }
                else
                {
                    ddlTerm.Visible = false;
                    txtTerm.Visible = true;

                }
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredDetails.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        protected void DdlProdcutDetailsId_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                BindSumInsuredId(Utility.ToInteger(DdlProductTypeName.SelectedItem.Value), Utility.ToInteger(DdlProdcutDetailsId.SelectedItem.Value));
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredDetails.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }

        #endregion
        #region Button_clicking events
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                

                Save();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredDetails.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.ShowSearch("Search");
                bindSearch();
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredDetails.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredDetails.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }

        }
        #endregion
        #region GridView events
        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                long SumInsuredDetailsid = Utility.ToInteger(gvSearch.SelectedValue);
                Edit(SumInsuredDetailsid);
                this.Master.HideSearch();

            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredDetails.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                gvSearch.EditIndex = -1;
                bindSearch();
            }
            catch (Exception ex)
            {
                //Audit.Add(EventType.Exception, Severity.High, 1, "(ReligareSumInsuredDetails.aspx Page)Error while saving.Reason:" + ex.ToString(), "0");
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
            }
        }
        #endregion


    }
}
