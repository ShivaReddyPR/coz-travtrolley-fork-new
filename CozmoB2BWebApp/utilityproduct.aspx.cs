﻿using CT.BookingEngine;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{
    public partial class utilityproduct : CT.Core.ParentPage
    {
        private int ProductId;
        private int bind;
        private string txcode = string.Empty;
        DataTable parentdt = new DataTable();
        private string AGENTDATALIST_SESSION = "_AgentDataList";
        private string AGENTDATALIST_SEARCH_SESSION = "_AgentDataSearchList";

        protected void Page_Load(object sender, EventArgs e)
        {

           
            if(Settings.LoginInfo != null)
            {
                if (!IsPostBack)
                {
                   
                    DataTable dt = new DataTable();
                    dt.Columns.AddRange(new DataColumn[5] {
         new DataColumn("LST_ID",typeof(int)), new DataColumn("LST_VALUE"), new DataColumn("LST_DESCP"),
                        new DataColumn("LST_SEQ"), new DataColumn("LST_STATUS") });
                    Session["MasteData"] = dt;

                        BindAgent();
                   
                    if (Request.QueryString["Source"] != null && Request.QueryString["Source"] != string.Empty)
                    {
                        if (Request.QueryString["Source"] == "Hotel")
                        {
                            ProductId = (int)ProductType.Hotel;
                        }
                        else if (Request.QueryString["Source"] == "Flight")
                        {
                            ProductId = (int)ProductType.Flight;
                        }
                        else if (Request.QueryString["Source"] == "Packages")
                        {
                            ProductId = (int)ProductType.Packages;
                        }
                        else if (Request.QueryString["Source"] == "Activity")
                        {
                            ProductId = (int)ProductType.Activity;
                        }
                        else if (Request.QueryString["Source"] == "Insurance")
                        {
                            ProductId = (int)ProductType.Insurance;
                        }
                        else if (Request.QueryString["Source"] == "Sightseeing")
                        {
                            ProductId = (int)ProductType.SightSeeing;
                        }
                        else if (Request.QueryString["Source"] == "Sightseeing")
                        {
                            ProductId = (int)ProductType.SightSeeing;
                        }
                        else if (Request.QueryString["Source"] == "Transfers")
                        {
                            ProductId = (int)ProductType.Transfers;
                        }
                        else if (Request.QueryString["Source"] == "FixedDeparture")
                        {
                            ProductId = (int)ProductType.FixedDeparture;
                        }
                        else if (Request.QueryString["Source"] == "Visa")
                        {
                            ProductId = (int)ProductType.Visa;
                        }
                        else if (Request.QueryString["Source"] == "Car")
                        {
                            ProductId = (int)ProductType.Car;
                        }
                        else if (Request.QueryString["Source"] == "ItineraryAddService")
                        {
                            ProductId = (int)ProductType.ItineraryAddService;
                        }
                        else if (Request.QueryString["Source"] == "BaggageInsurance")
                        {
                            ProductId = (int)ProductType.BaggageInsurance;
                        }
                        else
                        {
                            ProductId = 0;
                        }
                        
                    }
                    //this.BindGrid();

                }
                bind = Utility.ToInteger(ddlAgent.SelectedItem.Value);
                txcode = Utility.ToString(txtCode.Text);


            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }

        }

        #region Methods.
        /// <summary>
        /// This method is Binding Agentdetails.
        /// </summary>
        private void BindAgent()
        {
            try
            {
                int agentId = Settings.LoginInfo.AgentId;
                if (agentId <= 1) agentId = 0;
                ddlAgent.DataSource = AgentMaster.GetList(1, Settings.LoginInfo.AgentType.ToString(), agentId, ListStatus.Short, RecordStatus.Activated);
                ddlAgent.DataValueField = "agent_id";
                ddlAgent.DataTextField = "agent_name";
                ddlAgent.DataBind();
            }
            catch { throw; }
        }
        /// <summary>
        /// This method when updating database recoreds current id details.
        /// </summary>
        private utilityproductListData CurrentObject
        {
            get
            {
                return (utilityproductListData)Session[AGENTDATALIST_SESSION];
            }
            set
            {
                if (value == null)
                {
                    Session.Remove(AGENTDATALIST_SESSION);
                }
                else
                {
                    Session[AGENTDATALIST_SESSION] = value;
                }

            }
        }
        /// <summary>
        /// This method is saveing and updaing the database table.
        /// </summary>
  
        private void Save()
        {
            try
            {
                utilityproductListData agentDataList;
                if (CurrentObject == null)
                {
                    agentDataList = new utilityproductListData();
                    foreach (GridViewRow row in gridData.Rows)
                    {
                        agentDataList.Agetn_ID = Utility.ToInteger(ddlAgent.SelectedItem.Value);
                        agentDataList.CODE = txtCode.Text;
                        agentDataList.VALUE = row.Cells[1].Text;
                        agentDataList.DESCRIPTION = row.Cells[2].Text;
                        agentDataList.SEQUENCE = Utility.ToInteger(row.Cells[3].Text);
                        //agentDataList.STATUS = Utility.ToBoolean(row.Cells[4].Text);
                        if (Utility.ToBoolean(row.Cells[4].Text == "IsActive"))
                        {
                            agentDataList.STATUS = true;
                        }
                        agentDataList.CREATED_BY = Settings.LoginInfo.UserID;
                        agentDataList.Save(ProductId);
                    }
                }
                else
                {
                    agentDataList = CurrentObject;
                    foreach (GridViewRow row in gridparent.Rows)
                    {
                        agentDataList.Agetn_ID = Utility.ToInteger(ddlAgent.SelectedItem.Value);
                        agentDataList.CODE = txtCode.Text;
                        agentDataList.ID =Utility.ToInteger (row.Cells[1].Text);
                        agentDataList.VALUE = row.Cells[2].Text;
                        agentDataList.DESCRIPTION = row.Cells[3].Text;
                        agentDataList.SEQUENCE = Utility.ToInteger(row.Cells[4].Text);
                        //agentDataList.STATUS = Utility.ToBoolean(row.Cells[5].Text);
                        if (Utility.ToBoolean(row.Cells[5].Text == "IsActive"))
                        {
                            agentDataList.STATUS = true;
                        }
                        agentDataList.CREATED_BY = Settings.LoginInfo.UserID;
                        agentDataList.Save(ProductId);
                    }

                }
               
                gridData.Visible = false;
                lblSuccessMsg.Visible = true;
                lblSuccessMsg.Text = Formatter.ToMessage("Details Sucessfully", "", (CurrentObject == null ? CT.TicketReceipt.Common.Action.Saved : CT.TicketReceipt.Common.Action.Updated));
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// This method is retriving header details.
        /// </summary>
        /// <param name="id"></param>
        private void Edit(int id)
        {
            try
            {
                utilityproductListData agentMasterList = new utilityproductListData(id);
                CurrentObject = agentMasterList;
                ddlAgent.SelectedValue = Utility.ToString(agentMasterList.Agetn_ID);
                txtCode.Text = agentMasterList.CODE;
                BindParentgridDetails(agentMasterList.Agetn_ID, agentMasterList.CODE);
                btnSave.Text = "Update";
                btnClear.Text = "Cancel";
                ddlAgent.Enabled = false;
                txtCode.Enabled = false;
            }
            catch(Exception ex)
            {
                throw ex;
            }

        }
        /// <summary>
        /// This method is clearing the all controls.
        /// </summary>
        private void Clear()
        {
            try
            {
                ddlAgent.SelectedIndex = -1;
                txtCode.Text = string.Empty;
                txtValue.Text = string.Empty;
                txtDescription.Text = string.Empty;
                txtSequence.Text = string.Empty;
                lblSuccessMsg.Visible = false;
                lblSuccessMsg.Text = string.Empty;
                btnSave.Text = "Save";
                btnClear.Text = "Clear";
                ddlAgent.Enabled = true;
                txtCode.Enabled = true;
                gridData.Visible = false;
                chkStatus.Checked = false;
                //Session["MasteData"] = null;
                //Session["Data"] = null;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// This method is clearing parent details controls.
        /// </summary>
        private void ClearParentDetails()
        {
            try
            {
                txtValue.Text = string.Empty;
                txtDescription.Text = string.Empty;
                txtSequence.Text = string.Empty;
                chkStatus.Checked = false;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
        /// <summary>
        /// This method is using filtering the records.
        /// </summary>
        private DataTable SearchList
        {
            get
            {
                return (DataTable)Session[AGENTDATALIST_SEARCH_SESSION];
            }
            set
            {
                value.PrimaryKey = new DataColumn[] { value.Columns["LST_ID"] };
                Session[AGENTDATALIST_SEARCH_SESSION] = value;
            }
        }
        #endregion
        #region Binding gridviews.
        /// <summary>
        /// This method is binding Agentid,code details.
        /// </summary>
        private void bindSearch()
        {
            try
            {
                LoginInfo loginfo = Settings.LoginInfo;
                DataTable dt = utilityproductListData.GetList(Convert.ToInt32(ddlAgent.SelectedItem.Value), ProductId);
                SearchList = dt;
                CommonGrid g = new CommonGrid();
                g.BindGrid(gvSearch, dt);
            }
            catch { throw; }
        }
        /// <summary>
        /// This method is  parent details Binding.
        /// </summary>
        protected void BindGrid()
        {
            try
            {
                DataTable dt = (DataTable)Session["MasteData"];
                gridData.DataSource = dt;
                gridData.DataBind();
            }
            catch(Exception ex)
            {
                throw ex;
            }


        }
        /// <summary>
        /// agentid and code based on parent details displaying in gridview
        /// </summary>
        /// <param name="agentid"></param>
        /// <param name="code"></param>
        private void BindParentgridDetails(int agentid,string code)
        {
           
            try
            {
                if (parentdt.Rows.Count > 0)
                {
                    gridparent.DataSource = parentdt;
                    gridparent.DataBind();
                    gridData.Visible = false;
                }
                else
                {
                    parentdt = utilityproductListData.GetParentTableDetails(agentid, code);
                    Session["Data"] = parentdt;
                    gridparent.DataSource = parentdt;
                    gridparent.DataBind();
                    gridData.Visible = false;
                }
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }
        #endregion

        #region Button Clicking events.
        /// <summary>
        /// This button click events saveing and updateing in database table.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSave_Click(object sender, EventArgs e)
        {
            try
            {
                Save();
                gridData.Visible = false;
                gridparent.Visible = false;
            }
            catch(Exception ex)
            {
                Label lblErrorMaster = (Label)this.FindControl("lblError");
                lblErrorMaster.Visible = true;
                lblErrorMaster.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                throw ex;
            }
        }
        /// <summary>
        /// This button click events clearing all controls.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClear_Click(object sender, EventArgs e)
        {
            try
            {
                Clear();
               
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                //lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                throw ex;
            }

        }
        /// <summary>
        /// This button click events display databse records in gridview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnSearch_Click(object sender, EventArgs e)
        {
            try
            {
                this.Master.ShowSearch("Search");
                bindSearch();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                //lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                throw ex;
            }

        }
        /// <summary>
        /// This button events deleteing row in gridview.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnDelete_Click(object sender, EventArgs e)
        {
            try
            {
                //refrence of data table
                DataTable dt = (DataTable)Session["MasteData"];
                int RowId = Convert.ToInt32(hidGrid.Value);
                RowId++;
                DataRow dr = dt.Select("LST_ID=" + RowId).FirstOrDefault();


                if (dr != null)
                {
                    dt.Rows.Remove(dr);
                    Session["MasteData"] = dt;

                    this.BindGrid();
                    ClearParentDetails();

                }
                btnAdd.Text = "Add";

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                //lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                throw ex;
            }

        }
        /// <summary>
        /// This button clearing the parent controls details.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnClear1_Click(object sender, EventArgs e)
        {
            try
            {
                ClearParentDetails();

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                //lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                throw ex;
            }
        }
        /// <summary>
        /// This button is adding parent details in gridview without saveing database.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        protected void btnAdd_Click(object sender, EventArgs e)
        {
            try
            {
                lblSuccessMsg.Text = string.Empty;
                DataTable dt = new DataTable();
                if (btnAdd.Text == "Add")
                {
                    dt = (DataTable)Session["MasteData"];
                    int rowcount = dt.Rows.Count;
                    rowcount++;
                    bool exists = dt.Select().ToList().Exists(row => row["LST_VALUE"].ToString().ToUpper() == txtValue.Text);
                    if (exists == true)
                    {
                        lblSuccessMsg.Text = "Value alreay exists";
                    }
                    else
                    {

                        dt.Rows.Add(rowcount, txtValue.Text.Trim(), txtDescription.Text.Trim(), txtSequence.Text.Trim(), chkStatus.Checked ?true:false);
                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["LST_STATUS"].ToString() == "True")
                                row.SetField("LST_STATUS", "IsActive");
                            else
                                row.SetField("LST_STATUS", "DeActive");
                        }
                        Session["MasteData"] = dt;
                        this.BindGrid();
                        ClearParentDetails();
                    }
                }
                //This code is parent details updateing without database data.
                else  if (btnAdd.Text == "UpdateGrid")
                {
                    dt = (DataTable)Session["MasteData"];

                    int RowId = Convert.ToInt32(hidGrid.Value);
                    RowId++;
                    DataRow dr = dt.Select("LST_ID=" + RowId).FirstOrDefault();
                    dr[1] = Utility.ToString(txtValue.Text);
                    dr[2] = Utility.ToString(txtDescription.Text);
                    dr[3] = Utility.ToString(txtSequence.Text);
                    dr[4] = Utility.ToString(chkStatus.Checked ?true : false);
                    foreach (DataRow row in dt.Rows)
                    {
                        if (row["LST_STATUS"].ToString() == "True")
                            row.SetField("LST_STATUS", "IsActive");
                        else
                            row.SetField("LST_STATUS", "DeActive");
                    }
                    
                    Session["MasteData"] = dt;
                    this.BindGrid();
                    ClearParentDetails();
                    btnAdd.Text = "Add";

                }
                //This code is parent details updateing database records in gridview.
                else
                {
                    parentdt = (DataTable)Session["Data"];
                    int RowId = Convert.ToInt32(hidparent.Value);
                    DataRow dr = parentdt.Select("LST_ID=" + RowId).FirstOrDefault();
                    dr[1] = Utility.ToString(txtValue.Text);
                    dr[2] = Utility.ToString(txtDescription.Text);
                    dr[3] = Utility.ToString(txtSequence.Text);
                    dr[4] = Utility.ToString(chkStatus.Checked ? true : false);
                    foreach (DataRow parent in parentdt.Rows)
                    {
                        if (parent["LST_STATUS"].ToString() == "True")
                            parent.SetField("LST_STATUS", "IsActive");
                        else
                            parent.SetField("LST_STATUS", "DeActive");
                    }
                    Session["Data"] = parentdt;
                    this.BindParentgridDetails(bind, txcode);
                    ClearParentDetails();
                }

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                //lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                throw ex;
            }
        }

        #endregion
        #region gridview Events.
        protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                Clear();
                int id = Utility.ToInteger(gvSearch.SelectedValue);
                Edit(id);
                this.Master.HideSearch();

            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                //lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                throw ex;
            }

        }
        

        protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gvSearch.PageIndex = e.NewPageIndex;
                gvSearch.EditIndex = -1;
                FilterSearch_Click(null, null);

            }
            catch(Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                //lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                throw ex;
            }

        }
        protected void gridData_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                GridViewRow gridView = gridData.SelectedRow;
                hidGrid.Value = gridData.SelectedRow.RowIndex.ToString();
                txtValue.Text = gridView.Cells[1].Text;
                txtDescription.Text = gridView.Cells[2].Text;
                txtSequence.Text = gridView.Cells[3].Text;
                //chkStatus.Checked = Utility.ToBoolean(gridView.Cells[4].Text);
                if (gridView.Cells[4].Text == "IsActive")
                {
                    chkStatus.Checked=true;
                }
                btnAdd.Text = "UpdateGrid";

            }
            catch (Exception ex)
            {
                throw ex;
            }

        }
        protected void gridparent_SelectedIndexChanged(object sender, EventArgs e)
        {
            int id = Utility.ToInteger(gridparent.SelectedValue);
            utilityproductListData agentDataList = new utilityproductListData(id);
            hidparent.Value = Utility.ToString(id);
            txtValue.Text = agentDataList.VALUE;
            txtDescription.Text = agentDataList.DESCRIPTION;
            txtSequence.Text = Utility.ToString(agentDataList.SEQUENCE);
            if (agentDataList.STATUS == true)
            {
                chkStatus.Checked = true;
            }
            //chkStatus.Checked = Utility.ToBoolean(agentDataList.STATUS);
            btnAdd.Text = "Update";
            btnDelete.Visible = false;

        }


        #endregion
        #region FilterSearch event.
        protected void FilterSearch_Click(object sender, EventArgs e)
        {
            try
            {
                string[,] textboxesNColumns ={{ "HTtxtAgent", "agent_name" },{"HTtxtCode", "LST_CODE" },
                { "HTtxtValue", "LST_VALUE" }};
                CommonGrid g = new CommonGrid();
                g.FilterGridView(gvSearch, SearchList.Copy(), textboxesNColumns);

            }
            catch(Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                //lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
               
            }
           
        }
        protected void gridData_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gridData.PageIndex = e.NewPageIndex;
                this.BindGrid();
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                //lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);

            }
        }
        protected void gridparent_PageIndexChanging(object sender, GridViewPageEventArgs e)
        {
            try
            {
                gridparent.PageIndex = e.NewPageIndex;
                BindParentgridDetails(bind, txcode);
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                //lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);

            }

        }
        #endregion
        #region WebMethod.
        /// <summary>
        /// This web method is agentid and code already exists in databse is showing alert message.
        /// </summary>
        /// <param name="code"></param>
        /// <param name="agentid"></param>
        /// <returns></returns>
        [WebMethod(EnableSession = true)]
       public static string GetCode(string code, string agentid)
        {
            try
            {
                string value = utilityproductListData.GetExistingcode(code,Convert.ToInt32(agentid));

                return value;
            }
            catch(Exception ex)
            {
                throw ex;
            }
        }




        #endregion

        
    }
}