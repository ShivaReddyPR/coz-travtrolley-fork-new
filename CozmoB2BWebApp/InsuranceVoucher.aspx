﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="InsuranceVoucher" Title="Insurance Voucher" Codebehind="InsuranceVoucher.aspx.cs" %>

<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<link href="css/steps.css" rel="stylesheet" type="text/css" />
 <script type="text/javascript">
     function viewVoucher() {
         window.open('printInsuranceVoucher.aspx?InsId=<%= header.Id %>', 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes');
         return false;
     }
     
    </script>
    <div class="wizard-steps">
        <div class="completed-step">
            <a href="#step-one"><span>1</span>Select Policy</a></div>
        <div class="completed-step">
            <a href="#step-two"><span>2</span> PAX Details</a></div>
        <div class="completed-step">
            <a href="#"><span>3</span> Secure Payments</a></div>
        <div class="active-step">
            <a href="#"><span>4</span> Confirmation</a></div>
        <div class="clear">
        </div>
    </div>
      <br />

       <div style="text-align:right">
        <asp:LinkButton ID="lnkVoucher" CssClass="fcol_blue" OnClientClick="return viewVoucher();" runat="server" Text="Show Voucher"></asp:LinkButton>
        
        </div>
    <div id="printableArea" runat="server">
    
    
    
    
            
            
        
           
           
         <div class="col-md-12"> <asp:Image ID="imgHeaderLogo" runat="server" Width="162"  Height="51px" AlternateText="AgentLogo" ImageUrl="" /></div>
            
             
   
    <div class="font_med marbot_20">  <center>  <strong> Insurance Voucher</strong></center></div>
                
                
         
            
           
            
          
         
         
         <div class="marbot_20">
      <div class="col-md-6 col-xs-12">Your Journey : <asp:Label ID="lblSector" runat="server" Text="" Font-Bold="true"></asp:Label></div>
                
      <div class="col-md-6 col-xs-12">Departure Date : <asp:Label ID="lblJourneyDates" runat="server" Text="" Font-Bold="true"></asp:Label></div>
                
                <div class="clearfix"></div>
            </div>
            
            
         
         
         
              
              <div class="panel-group" id="accordion3">

              
       <% if (header != null && header.InsPassenger != null && header.InsPassenger.Count > 0)
                     {
                         for (int i = 0; i < header.InsPassenger.Count; i++)
                         {%>
   
  <div class="panel panel-default">
   

    <div class="panel-heading">
      <a href="#collapseOne<%=i %>" data-parent="#accordion3" data-toggle="collapse">
     <h4 class="panel-title">
       
          <span class="glyphicon pull-right font_bold glyphicon-minus"></span>
          <%=header.InsPassenger[i].FirstName + " " + header.InsPassenger[i].LastName%>
        
      </h4>
      </a>
    </div>
   
   
    <div id="collapseOne<%=i %>" class="collapse in">
      
      
       <table width="100%" border="1" cellpadding="0" cellspacing="0">
       <tr class="font_bold">
       <td>Plan Title</td>
       
       <td>Plan Code</td>
       
       <td>Policy No</td>
       
        <td>Policy Link</td>
        
       </tr>
       
       
       <%if (header.InsPlans != null && header.InsPlans.Count > 0)
         {
             for (int k = 0; k < header.InsPlans.Count; k++)
             {%>
               
       <tr>
      
       <td><%=header.InsPlans[k].PlanTitle %></td>
       
       <td><%=header.InsPlans[k].InsPlanCode %></td>
       
       <% if (!string.IsNullOrEmpty(header.InsPlans[k].PolicyNo))
           {   string [] policyNo = header.InsPassenger[i].PolicyNo.Split('|');
               for (int m=0; m<policyNo.Length; m++)
               {
                   if (policyNo[m].Contains(header.InsPlans[k].PolicyNo))
                   {%>
                   <td><%=policyNo[m]%></td>
                  <% }
               }
         } %>
        <% if (!string.IsNullOrEmpty(header.InsPlans[k].PolicyNo) && !string.IsNullOrEmpty(header.InsPassenger[i].PolicyUrlLink))
            {
                string[] policyUrl = header.InsPassenger[i].PolicyUrlLink.Split('|');
                for (int n = 0; n < policyUrl.Length; n++)
                {
                    if (policyUrl[n].Contains(header.InsPlans[k].PolicyNo))
                    {%>
                 <td><a target='_blank' href='<%=policyUrl[n]%>'>View Certificate</a></td>
                    <%}
                }
           } %>
       </tr>
      
         
       <%}
         } %>
          </table>
    </div>




       
  </div>

  <%}
                     }%>
   </div>
         
            
            <div class="col-md-12 padding-0 marbot_10">
                <div id="divPlan" runat="server">
                </div>
            </div>
            <div class="col-md-12 marbot_10">
                <label style="color: Red">
                    Note:</label>This voucher is only for cozmo internal reference
            </div>
        </div>
    
    
    
    <script>


        $('.collapse').on('shown.bs.collapse', function() {
            $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
        }).on('hidden.bs.collapse', function() {
            $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
        });

</script> 
</asp:Content>
 

