﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using System.Collections.Generic;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using CT.Roster;

public partial class ROSTranxHeaderGUI : CT.Core.ParentPage
{
    private string ROS_LIST = "_TranxROSList";
    private string ROS_LIST_DEFAULT = "_TranxROSList_Default";
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        if (!IsPostBack)
        {
            InitializePageControls();
            hdfHomeDriverTime.Value = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["ROS_HOME_DRIVER_TIME"]);
            hdfAirportDriverTime.Value  = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["ROS_AIRPORT_DRIVER_TIME"]);
        }
    }

    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
            
        
        }
        catch{throw;}
    }
    public void Clear()
    {
        try
        {
            ddlStaff.SelectedIndex = 0;
            dcFromDate.Clear();
            dcToDate.Clear();
            SetGridRow(0);
            btnLoad.Enabled = true;
            btnLoad.BackColor = System.Drawing.Color.Empty;            

        }
        catch { throw; }
    
    }
    protected void btnLoad_Click(object sender, EventArgs e)
    {
        try
        {
            hdfLoadStatus.Value = "1";//loaed from ddl
            SetGridRow(Utility.ToInteger(ddlStaff.SelectedItem.Value));

            DataRow dr;
            long serial = Utility.ToLong(ROSList.Compute("MAX(rm_id)", ""));
            DateTime fromDate = dcFromDate.Value;
            DateTime toDate = dcToDate.Value;
            int days = ((toDate - fromDate).Days+1)*2;
            ROSList.Clear();
            DateTime dateChange= dcFromDate.Value;
            for (int i = 1; i <= days; i++)
            {
               dr = ROSList.NewRow();
                dr["rm_id"] = i;
                dr["RM_DOC_NUMBER"] = DefaultROSList.Rows[0]["RM_DOC_NUMBER"];
                dr["RM_EMPID"] = DefaultROSList.Rows[0]["RM_EMPID"];
                dr["RM_FLIGHTNO"] = DefaultROSList.Rows[0]["RM_FLIGHTNO"];
                dr["RM_FROM_SECTOR"] = DefaultROSList.Rows[0]["RM_FROM_SECTOR"];
                dr["RM_TO_SECTOR"] = DefaultROSList.Rows[0]["RM_TO_SECTOR"];
                dr["RM_SIGN_IN"] = DefaultROSList.Rows[0]["RM_SIGN_IN"];
                dr["RM_SIGN_OUT"] = DefaultROSList.Rows[0]["RM_SIGN_OUT"];
                dr["RM_DUTY_HRS"] = DefaultROSList.Rows[0]["RM_DUTY_HRS"];
               // dr["RM_FROM_LOC_ID"] = DefaultROSList.Rows[0]["RM_FROM_LOC_ID"];
                //dr["RM_FROM_LOC_TYPE"] = DefaultROSList.Rows[0]["RM_FROM_LOC_TYPE"];
                //dr["RM_FROM_LOC_MAP"] = DefaultROSList.Rows[0]["RM_FROM_LOC_MAP"];
                //dr["RM_TO_LOC_ID"] = DefaultROSList.Rows[0]["RM_TO_LOC_ID"];
                //dr["RM_TO_LOC_TYPE"] = DefaultROSList.Rows[0]["RM_TO_LOC_TYPE"];
               // dr["RM_TO_LOC_MAP"] = DefaultROSList.Rows[0]["RM_TO_LOC_MAP"];
                
//                dr["RM_DATE"] = DefaultROSList.Rows[0]["RM_DATE"];
                dr["RM_STATUS"] = DefaultROSList.Rows[0]["RM_STATUS"];
                dr["RM_TRANX_STATUS"] = DefaultROSList.Rows[0]["RM_TRANX_STATUS"];
                dr["RM_CREATED_BY"] = DefaultROSList.Rows[0]["RM_CREATED_BY"];
               // dr["locationFromDetails"] = DefaultROSList.Rows[0]["locationFromDetails"];
               // dr["locationToDetails"] = DefaultROSList.Rows[0]["locationToDetails"];
                dr["STAFF_ID"] = DefaultROSList.Rows[0]["STAFF_ID"];
               
                if (i % 2 == 0)
                {
                    dr["FROMLOC"] = DefaultROSList.Rows[0]["TOLOC"];
                    dr["TOLOC"] = DefaultROSList.Rows[0]["FROMLOC"];
                    dr["RM_DIRECTION"] = "A";
                    dr["RM_FROM_LOC_TYPE"] ="A";
                    dr["RM_TO_LOC_TYPE"] = "H";
                    dr["RM_FROM_LOC_ID"] = DefaultROSList.Rows[0]["RM_TO_LOC_ID"];
                    dr["RM_TO_LOC_ID"] = DefaultROSList.Rows[0]["RM_FROM_LOC_ID"];
                    dr["locationFromDetails"] = DefaultROSList.Rows[0]["locationToDetails"];
                    dr["locationToDetails"] = DefaultROSList.Rows[0]["locationFromDetails"];
                    dr["RM_FROM_LOC_MAP"] = DefaultROSList.Rows[0]["RM_TO_LOC_MAP"];
                    dr["RM_TO_LOC_MAP"] = DefaultROSList.Rows[0]["RM_FROM_LOC_MAP"];

                }
                else
                {
                    dr["FROMLOC"] = DefaultROSList.Rows[0]["FROMLOC"];
                    dr["TOLOC"] = DefaultROSList.Rows[0]["TOLOC"];
                    dr["RM_DIRECTION"] = "H";
                    dr["RM_FROM_LOC_TYPE"] = "H";
                    dr["RM_TO_LOC_TYPE"] = "A";
                    dr["RM_FROM_LOC_ID"] = DefaultROSList.Rows[0]["RM_FROM_LOC_ID"];
                    dr["RM_TO_LOC_ID"] = DefaultROSList.Rows[0]["RM_TO_LOC_ID"];
                    dr["locationFromDetails"] = DefaultROSList.Rows[0]["locationFromDetails"];
                    dr["locationToDetails"] = DefaultROSList.Rows[0]["locationToDetails"];
                    dr["RM_FROM_LOC_MAP"] = DefaultROSList.Rows[0]["RM_FROM_LOC_MAP"];
                    dr["RM_TO_LOC_MAP"] = DefaultROSList.Rows[0]["RM_TO_LOC_MAP"];
                    if (i > 1) dateChange = dateChange.AddDays(1);
                }

                //if (i > 1) dr["RM_DATE"] = dcFromDate.Value.Add(TimeSpan.FromDays(1));
                dr["RM_DATE"] = dateChange;

                //SetVisaDetails(dr, gvRow, "FT");

                ROSList.Rows.Add(dr);
            }
            BindGrid();
            setRowId(serial);
            btnLoad.BackColor = System.Drawing.Color.Gray;
            btnLoad.Enabled = false;
            

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            Utility.Alert(this.Page, ex.Message);
        }
    }

    private void InitializePageControls()
    {
        try
        {
            BindStaffList();
            SetGridRow(0);

        }
        catch { throw; }


    }

    private void SetGridRow(int empId)
    {
        ROSTranxHeader rosHdr;
        if(empId ==0)
             rosHdr = new ROSTranxHeader();
        else
             rosHdr = new ROSTranxHeader(empId);


        DataSet ds = new DataSet();   
                DataTable ros = ds.Tables.Add("ROS");
               
                ros.Columns.AddRange(new DataColumn[] { 
                new DataColumn("RM_ID", typeof(long)), 
                new DataColumn("RM_DOC_NUMBER", typeof(string)),
                new DataColumn("RM_EMPID", typeof(int)),
                new DataColumn("RM_FLIGHTNO", typeof(string)),
                new DataColumn("RM_FROM_SECTOR", typeof(string)),
                new DataColumn("RM_TO_SECTOR", typeof(string)),
                new DataColumn("RM_SIGN_IN", typeof(DateTime)),
                new DataColumn("RM_SIGN_OUT", typeof(DateTime)),
                new DataColumn("RM_DUTY_HRS", typeof(string)),
                new DataColumn("RM_FROM_LOC_ID", typeof(int)),
                new DataColumn("RM_FROM_LOC_TYPE", typeof(string)),
                new DataColumn("RM_FROM_LOC_MAP", typeof(string)),
                new DataColumn("RM_TO_LOC_ID", typeof(int)),
                new DataColumn("RM_TO_LOC_TYPE", typeof(string)),
                new DataColumn("RM_TO_LOC_MAP", typeof(string)),
                new DataColumn("RM_DATE", typeof(DateTime)),
                new DataColumn("RM_STATUS", typeof(string)),
                new DataColumn("RM_TRANX_STATUS", typeof(string)),
                new DataColumn("RM_CREATED_BY", typeof(Int32)),
                new DataColumn("locationFromDetails", typeof(string)),
                new DataColumn("locationToDetails", typeof(string)),
                new DataColumn("STAFF_ID", typeof(string)),
                new DataColumn("FROMLOC", typeof(string)),
                new DataColumn("TOLOC", typeof(string)),
                new DataColumn("RM_DIRECTION", typeof(string))
                });

                DataRow row = ros.Rows.Add(rosHdr.Rm_id, rosHdr.Rm_doc_number, rosHdr.Rm_empid, rosHdr.Rm_flightno, rosHdr.Rm_from_sector,
                    rosHdr.Rm_to_sector, rosHdr.Rm_sign_in, rosHdr.Rm_sign_out, rosHdr.Rm_duty_hrs, rosHdr.Rm_from_loc_id, rosHdr.Rm_from_loc_type,
                    rosHdr.Rm_from_loc_map, rosHdr.Rm_to_loc_id, rosHdr.Rm_to_loc_type, rosHdr.Rm_to_loc_map, DateTime.MinValue , rosHdr.Rm_status,
                    rosHdr.Rm_tranx_status,rosHdr.Rm_created_by, rosHdr.Rm_From_Loc_Details, rosHdr.Rm_TO_Loc_Details,rosHdr.Rm_staff_id,
                    "Home","Airport","H");

                DefaultROSList = ds.Tables[0].Copy();

                ROSList = ds.Tables[0];              
                BindGrid();
                setRowId(0);
    }

    

    protected void gvROSDetails_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
        }catch{}
    }

    protected void gvROSDetails_RowDataBound(object sender, GridViewRowEventArgs e)
      {
          try
          {

              if (e.Row.RowType == DataControlRowType.DataRow)
              {
                  DropDownList ddlDirecrtion = (DropDownList)e.Row.FindControl("ITddlDirection");
                  DropDownList ddlEmpName = (DropDownList)e.Row.FindControl("ITddlEmpName");
                  HiddenField hdfDirection = (HiddenField)e.Row.FindControl("IThdfDirection");
                  HiddenField hdfEmployeeId = (HiddenField)e.Row.FindControl("IThdfEmpId");
                  DateControl dcSignIn = (DateControl)e.Row.FindControl("ITdcSignIn");
                  DateControl dcSignOut = (DateControl)e.Row.FindControl("ITdcSignOut");                  
                  HiddenField hdfPaxId = (HiddenField)e.Row.FindControl("IThdfPaxId");
                   DateControl dcDate = (DateControl)e.Row.FindControl("ITdcDate"); 
                  //Label lblStaffId = (Label)e.Row.FindControl("ITlblStaffId");

                  ddlDirecrtion.SelectedValue = hdfDirection.Value;
                  BindStaffList(ddlEmpName);
                  if (Utility.ToInteger(hdfEmployeeId.Value) > 0)
                  {                      
                      ddlEmpName.SelectedValue = hdfEmployeeId.Value;
                      //if (hdfLoadStatus.Value == "1")
                      //    ddlEmpName.Enabled = false;
                  }

                  if (dcSignIn.Value == DateTime.MinValue) dcSignIn.Clear();
                  if (dcSignOut.Value == DateTime.MinValue) dcSignOut.Clear();
                  if (dcDate.Value == DateTime.MinValue) dcDate.Clear();
              }
          }
          catch { }
      }

    protected void gvROSDetails_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
                if (e.CommandName == "Insert")
            {
                updateRecord();

                DataRow dr = ROSList.NewRow();
                DataTable dtDeleted = ROSList.GetChanges(DataRowState.Deleted);
                long deletedSerial = 0;
                if (dtDeleted != null)
                {
                    dtDeleted.RejectChanges();
                    deletedSerial = Utility.ToLong(dtDeleted.Compute("MAX(rm_id)", ""));
                }
                long rmId = Utility.ToLong(ROSList.Compute("MAX(rm_id)", ""));
                string direction = ROSList.Rows.Find(rmId)["RM_DIRECTION"].ToString();
                long serial = Utility.ToLong(ROSList.Rows.Count)+1;
                rmId = rmId + 1;
                //if (serial >= 0) serial = -1;


                dr["rm_id"] = rmId;
                dr["RM_DOC_NUMBER"]=DefaultROSList.Rows[0]["RM_DOC_NUMBER"];
                dr["RM_EMPID"] = DefaultROSList.Rows[0]["RM_EMPID"];
                dr["RM_FLIGHTNO"] = DefaultROSList.Rows[0]["RM_FLIGHTNO"];
                dr["RM_FROM_SECTOR"] = DefaultROSList.Rows[0]["RM_FROM_SECTOR"];
                dr["RM_TO_SECTOR"] = DefaultROSList.Rows[0]["RM_TO_SECTOR"];
                dr["RM_SIGN_IN"] = DefaultROSList.Rows[0]["RM_SIGN_IN"];
                dr["RM_SIGN_OUT"] = DefaultROSList.Rows[0]["RM_SIGN_OUT"];
                dr["RM_DUTY_HRS"] = DefaultROSList.Rows[0]["RM_DUTY_HRS"];
                dr["RM_FROM_LOC_ID"] = DefaultROSList.Rows[0]["RM_FROM_LOC_ID"];
                dr["RM_FROM_LOC_TYPE"] = DefaultROSList.Rows[0]["RM_FROM_LOC_TYPE"];
                dr["RM_FROM_LOC_MAP"] = DefaultROSList.Rows[0]["RM_FROM_LOC_MAP"];
                dr["RM_TO_LOC_ID"] = DefaultROSList.Rows[0]["RM_TO_LOC_ID"];
                dr["RM_TO_LOC_TYPE"] = DefaultROSList.Rows[0]["RM_TO_LOC_TYPE"];
                dr["RM_TO_LOC_MAP"] = DefaultROSList.Rows[0]["RM_TO_LOC_MAP"];
                dr["RM_DATE"] = DefaultROSList.Rows[0]["RM_DATE"];
                dr["RM_STATUS"] = DefaultROSList.Rows[0]["RM_STATUS"];
                dr["RM_TRANX_STATUS"] = DefaultROSList.Rows[0]["RM_TRANX_STATUS"];
                dr["RM_CREATED_BY"] = DefaultROSList.Rows[0]["RM_CREATED_BY"];
                dr["locationFromDetails"] = DefaultROSList.Rows[0]["locationFromDetails"];
                dr["locationToDetails"] = DefaultROSList.Rows[0]["locationToDetails"];
                dr["STAFF_ID"] = DefaultROSList.Rows[0]["STAFF_ID"];
                if (direction=="A")
                {
                    dr["FROMLOC"] = DefaultROSList.Rows[0]["FROMLOC"];
                    dr["TOLOC"] = DefaultROSList.Rows[0]["TOLOC"];
                    dr["RM_DIRECTION"] = "H";
                    
                }
                else
                {
                    dr["FROMLOC"] = DefaultROSList.Rows[0]["TOLOC"];
                    dr["TOLOC"] = DefaultROSList.Rows[0]["FROMLOC"];
                    dr["RM_DIRECTION"] = "A";
                }
                //SetVisaDetails(dr, gvRow, "FT");

                ROSList.Rows.Add(dr);
                BindGrid();
                setRowId(serial);





                // lnkSettlement_Click(null, null);

                //FilterSearchGrid();
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            //Utility.StartupScript(this.Page, "ShowMessageDialog('gvVisaDetails_RowCommand','" + ex.Message + "','Information')", "gvVisaDetails_RowCommand");
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void gvROSDetails_RowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        try
        {
            updateRecord();

            GridViewRow gvRow = gvROSDetails.Rows[e.RowIndex];
            long serial = Utility.ToLong(gvROSDetails.DataKeys[e.RowIndex].Value);

            
            DataRow dr = ROSList.Rows.Find(serial);

            ROSList.Rows.Find(serial).Delete();
            BindGrid();
            
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    private DataTable ROSList
    {
        get
        {
            return (DataTable)Session[ROS_LIST];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["rm_id"] };
            Session[ROS_LIST] = value;
        }
    }

    private DataTable DefaultROSList
    {
        get
        {
            return (DataTable)Session[ROS_LIST_DEFAULT];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["rm_id"] };
            Session[ROS_LIST_DEFAULT] = value;
        }
    }

    private void BindGrid()
    {
        try
        {
            CommonGrid grid = new CommonGrid();
            if(ROSList.Rows.Count >0)
            grid.BindGrid(gvROSDetails, ROSList);
            else
            grid.BindGrid(gvROSDetails, DefaultROSList);
        }
        catch { }
    }

    private void BindStaffList(DropDownList staffList)
    {
        try
        {
            staffList.DataSource = ROSEmployeeMaster.GetROSEmpMasterList(RecordStatus.Activated,ListStatus.Short, "Crew");
            staffList.DataValueField = "emp_Id";
            staffList.DataTextField = "emp_Name";
            staffList.DataBind();
            staffList.Items.Insert(0, new ListItem("--Select Employee--", "0"));
            staffList.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }

    private void BindStaffList()
    {
        try
        {
            ddlStaff.DataSource = ROSEmployeeMaster.GetROSEmpMasterList(RecordStatus.Activated,ListStatus.Short, "Crew");
            ddlStaff.DataValueField = "emp_Id";
            ddlStaff.DataTextField = "emp_Name";
            ddlStaff.DataBind();
            ddlStaff.Items.Insert(0, new ListItem("--Select Employee--", "0"));
            ddlStaff.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }


    private void BindLocation(string type)
    {
        try
        {
            ddlNewLocation.DataSource = ROSTranxHeader.GetLocationTypewiseList(type);
            ddlNewLocation.DataValueField = "loc_id";
            ddlNewLocation.DataTextField = "loc_name";
            ddlNewLocation.DataBind();
            ddlNewLocation.Items.Insert(0, new ListItem("--Select Location--", "0"));
            ddlNewLocation.SelectedIndex = 0;
            //Utility.StartupScript(this.Page, "setExchRate(" + ddlCurrency.SelectedItem.Value + ")", "setExchRate");
        }
        catch { throw; }
    }
    private void setRowId(long slNo)
    {
        try
        {
            //DropDownList ddlStaffList = null;          

           // ddlStaffList = (DropDownList)gvROSDetails.Rows[Utility.ToInteger(slNo)].FindControl("ITddlEmpName");
            //BindStaffList(ddlStaffList);
            //ddlNationality = (DropDownList)gvVisaDetails.Rows[gvVisaDetails.EditIndex].FindControl("EITddlPaxNationality"); 

            //if (gvVisaDetails.EditIndex > -1)
            //{
            //    hdfDetailRowId.Value = gvVisaDetails.Rows[gvVisaDetails.EditIndex].ClientID;
            //    ddlNationality = (DropDownList)gvVisaDetails.Rows[gvVisaDetails.EditIndex].FindControl("EITddlPaxNationality");
            //    BindNationality(ddlNationality);
            //    ddlPaxType = (DropDownList)gvVisaDetails.Rows[gvVisaDetails.EditIndex].FindControl("EITddlPaxType");
            //    BindPaxType(ddlPaxType);
            //    if (hdfVisaActivity.Value == "VISANO") ((TextBox)gvVisaDetails.Rows[gvVisaDetails.EditIndex].FindControl("EITtxtVisaEntryNo")).Attributes.Remove("disabled");
            //    else ((TextBox)gvVisaDetails.Rows[gvVisaDetails.EditIndex].FindControl("EITtxtVisaEntryNo")).Attributes.Add("disabled", "true");
            //}
            //else
            //{
            //    hdfDetailRowId.Value = gvVisaDetails.FooterRow.ClientID;
            //    ddlNationality = (DropDownList)gvVisaDetails.FooterRow.FindControl("FTddlPaxNationality");
            //    BindNationality(ddlNationality);
            //    ddlPaxType = (DropDownList)gvVisaDetails.FooterRow.FindControl("FTddlPaxType");
            //    BindPaxType(ddlPaxType);
            //    if (hdfVisaActivity.Value == "VISANO") ((TextBox)gvVisaDetails.FooterRow.FindControl("FTtxtVisaEntryNo")).Attributes.Remove("disabled");
            //    else ((TextBox)gvVisaDetails.FooterRow.FindControl("FTtxtVisaEntryNo")).Attributes.Add("disabled", "true");
            //}

        }
        catch { throw; }
    }

    private void updateRecord()
    {

        try
        {
            foreach (GridViewRow gvRow in gvROSDetails.Rows)
            {
                HiddenField hdfRMId = (HiddenField)gvRow.FindControl("IThdfRMid");
                HiddenField hdfDirection = (HiddenField)gvRow.FindControl("IThdfDirection");
                HiddenField hdfEmpId = (HiddenField)gvRow.FindControl("IThdfEmpId");
                HiddenField hdfFromLocId = (HiddenField)gvRow.FindControl("IThdfFromLocId");
                HiddenField hdfFromLocDetails = (HiddenField)gvRow.FindControl("IThdfFromLocDetails");
                HiddenField hdfFromLocType = (HiddenField)gvRow.FindControl("IThdfFromLocType");
                HiddenField hdfFromLocMap = (HiddenField)gvRow.FindControl("IThdfToLocMap");
                HiddenField hdfToLocId = (HiddenField)gvRow.FindControl("IThdfToLocId");
                HiddenField hdfToLocDetails = (HiddenField)gvRow.FindControl("IThdfToLocDetails");
                HiddenField hdfToLocType = (HiddenField)gvRow.FindControl("IThdfToLocType");
                HiddenField hdfToLocMap = (HiddenField)gvRow.FindControl("IThdfToLocMap");

                TextBox txtFlightNo = (TextBox)gvRow.FindControl("ITtxtFlightNo");
                TextBox txtFromSector = (TextBox)gvRow.FindControl("ITtxtFromSector");
                TextBox txtToSector = (TextBox)gvRow.FindControl("ITtxtToSector");
                DropDownList ddlDirection = (DropDownList)gvRow.FindControl("ITddlDirection");
                DropDownList ddlEmployee = (DropDownList)gvRow.FindControl("ITddlEmpName");
                Label lblStaffId = (Label)gvRow.FindControl("ITlblStaffId");
                TextBox txtDutyHours = (TextBox)gvRow.FindControl("ITtxtDutyHours");
                DateControl dcSignIn = (DateControl)gvRow.FindControl("ITdcSignIn");
                DateControl dcSignOut = (DateControl)gvRow.FindControl("ITdcSignOut");
                DateControl dcDate=(DateControl)gvRow.FindControl("ITdcDate");
                TextBox lblFrom = (TextBox)gvRow.FindControl("ITlblFrom");
                TextBox lblTo = (TextBox)gvRow.FindControl("ITlblTo");

               
                int cntRows = ROSList.Rows.Count;
                foreach (DataRow dr in ROSList.Rows)
                {
                    if (dr != null)
                    {
                        dr.BeginEdit();
                        if (Utility.ToString(dr["rm_id"]) == hdfRMId.Value)
                        {

                            dr["RM_DOC_NUMBER"] = "";
                            dr["RM_EMPID"] = ddlEmployee.SelectedItem.Value; ;
                            dr["RM_FLIGHTNO"] = txtFlightNo.Text.Trim();
                            dr["RM_FROM_SECTOR"] = txtFromSector.Text.Trim();
                            dr["RM_TO_SECTOR"] = txtToSector.Text.Trim();
                            dr["RM_SIGN_IN"] = dcSignIn.Value;
                            dr["RM_SIGN_OUT"] = dcSignOut.Value;
                            dr["RM_DUTY_HRS"] = txtDutyHours.Text.Trim();
                            dr["RM_FROM_LOC_ID"] = hdfFromLocId.Value;
                            dr["RM_FROM_LOC_TYPE"] = hdfFromLocType.Value;
                            dr["RM_FROM_LOC_MAP"] = hdfFromLocMap.Value;
                            dr["RM_TO_LOC_ID"] = hdfToLocId.Value;
                            dr["RM_TO_LOC_TYPE"] = hdfToLocType.Value;
                            dr["RM_TO_LOC_MAP"] = hdfToLocMap.Value;

                            dr["RM_DATE"] = dcDate.Value;
                            dr["RM_STATUS"] = "A";
                            dr["RM_TRANX_STATUS"] = "P";
                            dr["RM_CREATED_BY"] = Settings.LoginInfo.UserID;
                            dr["locationFromDetails"] = hdfFromLocDetails.Value ;
                            dr["locationToDetails"] = hdfToLocDetails.Value;

                            dr["FROMLOC"] = lblFrom.Text.Trim();
                            dr["TOLOC"] = lblTo.Text.Trim();
                            dr["RM_DIRECTION"] = ddlDirection.SelectedItem.Value;//lblFrom.Text.Trim()=="Home"?"H":"A";
                            dr["STAFF_ID"] = lblStaffId.Text.Trim();
                        }
                        dr.EndEdit();
                    }

                    //if (dr == ROSList.Rows[ROSList.Rows.Count - 1])
                    //{

                    //    hdflastDirection.Value = Utility.ToString(dr["RM_DIRECTION"]);
                    //}
                }
            }




        }
        catch { throw; }
    
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            //If Settlement mode is 'Card' then validate card details first and redirect to payment gateway.
            //If Success then call Save(), otherwise show the error message.

            {
                Save();
                

            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
            //StartupScript(this.Page, "alert('" + ex.Message + "');", "Err");
        }
    }

    private void Save()
    {
        string interfaceErrMsg = string.Empty;
        
        try
        {
            updateRecord();
            String docNumber = DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss").Replace("/","");
            docNumber = docNumber.Replace(":", "");
            docNumber = docNumber.Replace(" ", "");
            docNumber = "ROS/" + docNumber;
            List<ROSTranxHeader> rosList = new List<ROSTranxHeader>();
            foreach (DataRow dr in ROSList.Rows)
            {
                ROSTranxHeader rosHdr = new ROSTranxHeader();
                rosHdr.Rm_id=-1;
                rosHdr.Rm_doc_number = docNumber;
                rosHdr.Rm_empid =Utility.ToInteger(dr["RM_EMPID"]) ;
                rosHdr.Rm_flightno=Utility.ToString(dr["RM_FLIGHTNO"]) ; 
                rosHdr.Rm_from_sector=Utility.ToString(dr["RM_FROM_SECTOR"]) ;
                rosHdr.Rm_to_sector=Utility.ToString(dr["RM_TO_SECTOR"]) ;
                rosHdr.Rm_sign_in=Utility.ToDate(dr["RM_SIGN_IN"]) ;
                rosHdr.Rm_sign_out=Utility.ToDate(dr["RM_SIGN_OUT"]) ;
                rosHdr.Rm_duty_hrs=Utility.ToString(dr["RM_DUTY_HRS"]) ;
                rosHdr.Rm_from_loc_id=Utility.ToInteger(dr["RM_FROM_LOC_ID"]) ;
                rosHdr.Rm_from_loc_type=Utility.ToString(dr["RM_FROM_LOC_TYPE"]) ;
                rosHdr.Rm_from_loc_map=Utility.ToString(dr["RM_FROM_LOC_MAP"]) ;
                rosHdr.Rm_to_loc_id=Utility.ToInteger(dr["RM_TO_LOC_ID"]) ;
                rosHdr.Rm_to_loc_type=Utility.ToString(dr["RM_TO_LOC_TYPE"]) ;
                rosHdr.Rm_to_loc_map=Utility.ToString(dr["RM_TO_LOC_MAP"]) ;
                rosHdr.Rm_date=Utility.ToDate(dr["RM_DATE"]) ;
                rosHdr.Rm_status=Utility.ToString(dr["RM_STATUS"]) ;
                rosHdr.Rm_tranx_status=Utility.ToString(dr["RM_TRANX_STATUS"]) ;
                rosHdr.Rm_created_by=Utility.ToInteger(dr["RM_CREATED_BY"]) ;
                rosHdr.Rm_From_Loc_Details=Utility.ToString(dr["locationFromDetails"]) ;
                rosHdr.Rm_TO_Loc_Details=Utility.ToString(dr["locationToDetails"]) ;
                rosHdr.Rm_staff_id=Utility.ToString(dr["STAFF_ID"]) ;               
                rosList.Add(rosHdr);
            }

            ROSTranxHeader.SaveROSListDetails(rosList);

            rosList.Clear();
            //BindGrid();
            CommonGrid grid = new CommonGrid();
            grid.BindGrid(gvROSDetails, DefaultROSList);
            Utility.Alert(this.Page, "Roster " + docNumber +" Details are saved successfully !");

        }
        catch
        {
            throw;
        }
        
    }

   protected void ITddlEmpName_SelectedIndexChanged(object sender, EventArgs e)   
     {
        try
        {
            DropDownList ddl = (DropDownList)sender;
            GridViewRow row = (GridViewRow)ddl.NamingContainer;

            DropDownList ddlStaff = (DropDownList)row.FindControl("ITddlEmpName");
            DropDownList ddlDirection = (DropDownList)row.FindControl("ITddlDirection");
            Label lblStaffId = (Label)row.FindControl("ITlblStaffId");
            HiddenField hdfFromLocId = (HiddenField)row.FindControl("IThdfFromLocId");
            HiddenField hdfFromLocDetails = (HiddenField)row.FindControl("IThdfFromLocDetails");
            HiddenField hdfFromLocType = (HiddenField)row.FindControl("IThdfFromLocType");                        
            HiddenField hdfFromLocMap = (HiddenField)row.FindControl("IThdfFromLocMap");
            HiddenField hdfFromLocName = (HiddenField)row.FindControl("IThdfFromLocName");

            HiddenField hdfToLocId = (HiddenField)row.FindControl("IThdfToLocId");
            HiddenField hdfToLocDetails = (HiddenField)row.FindControl("IThdfToLocDetails");
            HiddenField hdfToLocType = (HiddenField)row.FindControl("IThdfToLocType");
            HiddenField hdfToLocMap = (HiddenField)row.FindControl("IThdfToLocMap");
            HiddenField hdfToLocName = (HiddenField)row.FindControl("IThdfToLocName");

            int empId = Utility.ToInteger(ddlStaff.SelectedItem.Value);

            DataTable dtEmpLoc = ROSTranxHeader.GetEmpLocationDetails(empId);

            lblStaffId.Text = Utility.ToString(dtEmpLoc.Rows[0]["staff_id"]);
            if (ddlDirection.SelectedItem.Value == "H")
            {
                hdfFromLocId.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationFrom"]);
                hdfFromLocDetails.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationFromDetails"]);
                hdfFromLocType.Value = "H";// Utility.ToString(dtEmpLoc.Rows[0]["locationFrom"]);
                hdfToLocType.Value = "A";// Utility.ToString(dtEmpLoc.Rows[0]["locationFrom"]);
                hdfFromLocMap.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationFromMap"]);
                hdfFromLocName.Value = Utility.ToString(dtEmpLoc.Rows[0]["FROMLOC"]);

                hdfToLocId.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationTo"]);
                hdfToLocDetails.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationToDetails"]);
                hdfToLocMap.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationToMap"]);
                hdfToLocName.Value = Utility.ToString(dtEmpLoc.Rows[0]["TOLOC"]);
            }
            else
            {
                hdfFromLocId.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationTo"]);
                hdfFromLocDetails.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationToDetails"]);
                hdfFromLocType.Value = "A";// Utility.ToString(dtEmpLoc.Rows[0]["locationFrom"]);
                hdfToLocType.Value = "H";// Utility.ToString(dtEmpLoc.Rows[0]["locationFrom"]);
                hdfFromLocMap.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationToMap"]);
                hdfFromLocName.Value = Utility.ToString(dtEmpLoc.Rows[0]["TOLOC"]);

                hdfToLocId.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationFrom"]);
                hdfToLocDetails.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationFromDetails"]);
                hdfToLocMap.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationFromMap"]);
                hdfToLocDetails.Value = Utility.ToString(dtEmpLoc.Rows[0]["FROMLOC"]);
            
            }
            //hdfToLocType.Value = Utility.ToString(dtEmpLoc.Rows[0]["locationFrom"]);

        }
        catch { }
    }
        //}
    private void SetLocationDetails()
    {
        try
        {
            //ddlStaff = (DropDownList)gvROSDetails.Rows[gvROSDetails.].FindControl(id + "Country");


        }
        catch{}
    
    }
    protected void ITlnkEditFrom_Click(object sender, EventArgs e)
    {
        try
        {


            string script ="document.getElementById('divEditLocation').style.display = 'block';";
            Utility.StartupScript(this.Page, script, "setExchRate;");
            
            GridViewRow gvRow = (GridViewRow)(((LinkButton)sender).NamingContainer);
            DropDownList ddlDirection = (DropDownList)gvRow.FindControl("ITddlDirection");
            string type = ddlDirection.SelectedItem.Value;
            BindLocation(type);
            hdfFromOrTo.Value = "F"; //from location
            
            //divEditLocation.Visible = true;
        }
        catch { }
    }
    protected void ITlnkViewFrom_Click(object sender, EventArgs e)
    {
        try
        { }
        catch { }
    }

    protected void ITlnkEditTo_Click(object sender, EventArgs e)
    {
        try
        {
            string script = "document.getElementById('divEditLocation').style.display = 'block';";
            Utility.StartupScript(this.Page, script, "setExchRate;");

            GridViewRow gvRow = (GridViewRow)(((LinkButton)sender).NamingContainer);
            DropDownList ddlDirection = (DropDownList)gvRow.FindControl("ITddlDirection");
            string type = ddlDirection.SelectedItem.Value;
            if (type == "H") type = "A";
            else type = "H";
            BindLocation(type);

            hdfFromOrTo.Value = "T"; //to location
       
        }
        catch { }
    }
    protected void ITlnkViewTo_Click(object sender, EventArgs e)
    {
        try
        { }
        catch { }
    }
}

