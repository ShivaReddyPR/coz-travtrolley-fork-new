﻿using System;
using System.Collections.Generic;
////using System.Linq;
using System.Web;
using System.Web.Configuration;
using System.IO;
using System.Text;
using System.Xml;
using System.Xml.XPath;
using System.Xml.Xsl;
using System.Data;
using System.Data.SqlClient;
using CT.TicketReceipt.BusinessLayer;

/// <summary>
/// Summary description for NlevelMenu
/// </summary>
public class MenuList
{
    string ErrorMsg;

    public MenuList()
    {
        //Read Connection String From web.config
        //ConnectionString = WebConfigurationManager.ConnectionStrings["AspSpoorFishConnectionString"].ConnectionString;
        //ConnectionString = DBGateway.conn
    }


    public string ExecuteXSLTransformation()
    {
        string HtmlTags,XsltPath;
        MemoryStream DataStream = default(MemoryStream);
        StreamReader streamReader = default(StreamReader);

        try
        {
            //Path of XSLT file
            XsltPath = HttpContext.Current.Server.MapPath("XsltFormatFolder/XsltTransformer.xslt");

            //Encode all Xml format string to bytes
            byte[] bytes = Encoding.ASCII.GetBytes(GenerateXmlFormat());

            DataStream = new MemoryStream(bytes);

            //Create Xmlreader from memory stream

            XmlReader reader = XmlReader.Create(DataStream);

            // Load the XML 
            XPathDocument document = new XPathDocument(reader);
            XslCompiledTransform XsltFormat = new XslCompiledTransform(true);
            //XsltFormat.e

            // Load the style sheet.
            XsltFormat.Load(XsltPath);

            DataStream = new MemoryStream();

            XmlTextWriter writer = new XmlTextWriter(DataStream, Encoding.ASCII);

            
            //Apply transformation from xml format to html format and save it in xmltextwriter
            XsltFormat.Transform(document, writer);

            streamReader = new StreamReader(DataStream);
            
            DataStream.Position = 0;

            HtmlTags = streamReader.ReadToEnd();

            return HtmlTags;
        }
        catch (Exception ex)
        {
            ErrorMsg = ex.Message;
            return ErrorMsg;
        }
        finally
        {
            //Release the resources 
            streamReader.Close();
            DataStream.Close();
        }
    }
    public string GenerateXmlFormat()
    {
        // string SqlCommand;
        //DataSet DbMenu = MenuMaster.GetList(Settings.LoginInfo.UserID);
        DataSet DbMenu = Settings.MenuList;
        

        return DbMenu.GetXml();
    }



    //public string GenerateXmlFormat()
    //{
    //    string SqlCommand;
    //    DataSet DbMenu;
    //    DataRelation relation;
    //    string ConnectionString = System.Configuration.ConfigurationManager.AppSettings["dbSetting"];

    //    using (SqlConnection conn = new SqlConnection(ConnectionString))
    //    {

    //        SqlCommand = "Select MenuID, Name,Url, ParentID from MenuTable";

    //        DbMenu = new DataSet();

    //        SqlDataAdapter Adapter = new SqlDataAdapter(SqlCommand, conn);

    //        Adapter.Fill(DbMenu);

    //        Adapter.Dispose();
    //    }

    //    DbMenu.DataSetName = "Menus";

    //    DbMenu.Tables[0].TableName = "Menu";

    //    //create Relation Parent and Child
    //    relation = new DataRelation("ParentChild", DbMenu.Tables["Menu"].Columns["MenuID"], DbMenu.Tables["Menu"].Columns["ParentID"], true);


    //    relation.Nested = true;

    //    DbMenu.Relations.Add(relation);

    //    return DbMenu.GetXml();
    //}
}