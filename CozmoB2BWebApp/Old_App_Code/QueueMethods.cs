using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using CT.BookingEngine;

/// <summary>
/// Summary description for QueueMethods
/// </summary>
public class QueueMethods
{
    public QueueMethods()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    public static string GetAllAirlineString(FlightItinerary flightItinerary)
    {
        string allAirline = string.Empty;
        for (int j = 0; j < flightItinerary.Segments.Length; j++)
        {
            if (allAirline.IndexOf(flightItinerary.Segments[j].Airline) >= 0)
            {
                continue;
            }
            if (allAirline.Length == 0)
            {
                allAirline = flightItinerary.Segments[j].Airline;
            }
            else
            {
                allAirline = allAirline + "," + flightItinerary.Segments[j].Airline;
            }
        }
        return allAirline;
    }
    public static string GetAllAirportString(FlightItinerary flightItinerary)
    {
        string allAirport = string.Empty;
        for (int j = 0; j < flightItinerary.Segments.Length; j++)
        {
            if (allAirport.IndexOf(flightItinerary.Segments[j].Origin.AirportCode) >= 0)
            {
                continue;
            }
            if (allAirport.Length == 0)
            {
                allAirport = flightItinerary.Segments[j].Origin.AirportCode;
            }
            else
            {
                allAirport = allAirport + "," + flightItinerary.Segments[j].Origin.AirportCode;
            }
        }
        return allAirport;
    }
    public static string GetLeadPax(FlightItinerary flightItinerary)
    {
        string leadPaxName = string.Empty;
        for (int j = 0; j < flightItinerary.Passenger.Length; j++)
        {
            if (flightItinerary.Passenger[j].IsLeadPax)
            {
                leadPaxName = flightItinerary.Passenger[j].FirstName + " " + flightItinerary.Passenger[j].LastName;
            }

        }
        if (leadPaxName.Length == 0)
        {
            leadPaxName = flightItinerary.Passenger[0].FirstName + " " + flightItinerary.Passenger[0].LastName;
        }
        return leadPaxName;
    }
    /// <summary>
    /// Gets the Agent price for a passenger.
    /// </summary>
    /// <param name="flightItinerary">FlightItinerary with pax detail</param>
    /// <returns>Decimal price that agent would need to see</returns>
    public static decimal GetTotalPrice(FlightItinerary flightItinerary)
    {
        decimal price = 0;
        for (int i = 0; i < flightItinerary.Passenger.Length; i++)
        {
            price = flightItinerary.Passenger[i].Price.GetAgentPrice();
        }
        return price;
    }
    /// <summary>
    /// Get Itinerary String of all segment 
    /// </summary>
    /// <param name="flightItinerary"></param>
    /// <returns></returns>
    public static string GetItineraryString(FlightItinerary flightItinerary)
    {
        string itenaryString = string.Empty;
        if (flightItinerary.Segments.Length > 0)
        {
            itenaryString = flightItinerary.Segments[0].Origin.AirportCode;
            for (int k = 0; k < flightItinerary.Segments.Length; k++)
            {
                itenaryString = itenaryString + "-" + flightItinerary.Segments[k].Destination.AirportCode;
            }
        }
        return itenaryString;
    }
    /// <summary>
    /// Get Booking Class of flightItinerary
    /// </summary>
    /// <param name="flightItinerary"> FlightIntenary</param>
    /// <returns></returns>
    public static string GetBookingClassString(FlightItinerary flightItinerary)
    {
        string bookingClassString = string.Empty;
        if (flightItinerary.Segments.Length > 0)
        {
            bookingClassString = flightItinerary.Segments[0].BookingClass;
            for (int k = 1; k < flightItinerary.Segments.Length; k++)
            {
                bookingClassString = bookingClassString + "/" + flightItinerary.Segments[k].BookingClass;
            }
        }
        return bookingClassString;
    }
    public static string GetPaxTypeString(FlightItinerary flightItinerary)
    {
        #region Set PAX type string
        string adt = string.Empty;
        int numAdt = 0;
        int numCNN = 0;
        int numINF = 0;
        int numSNN = 0;
        string adult = string.Empty;
        string child = string.Empty;
        string infant = string.Empty;
        string seniorCitizen = string.Empty;
        for (int j = 0; j < flightItinerary.Passenger.Length; j++)
        {
            if (flightItinerary.Passenger[j].Type == PassengerType.Adult)
            {
                numAdt++;
            }
            if (flightItinerary.Passenger[j].Type == PassengerType.Child)
            {
                numCNN++;
            }
            if (flightItinerary.Passenger[j].Type == PassengerType.Infant)
            {
                numINF++;
            }
            if (flightItinerary.Passenger[j].Type == PassengerType.Senior)
            {
                numSNN++;
            }
        }
        if (numAdt > 0)
        {
            adt = numAdt.ToString() + " " + "ADT";
        }
        if (numCNN > 0)
        {
            if (adt.Length > 0)
            {
                adt = adt + " | " + numCNN.ToString() + " " + "CNN";
            }
            else
            {
                adt = numCNN.ToString() + " " + "CNN";
            }
        }
        if (numINF > 0)
        {
            if (adt.Length > 0)
            {
                adt = adt + " | " + numINF.ToString() + " " + "INF";
            }
            else
            {
                adt = numINF.ToString() + " " + "INF";
            }
        }
        if (numSNN > 0)
        {
            if (adt.Length > 0)
            {
                adt = adt + " | " + numSNN.ToString() + " " + "SNN";
            }
            else
            {
                adt = numSNN.ToString() + " " + "SNN";
            }
        }
        #endregion
        return adt;
    }

}
