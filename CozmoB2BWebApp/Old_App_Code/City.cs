using System;
using System.Collections.Generic;
using System.Text;
using System.Collections;
using CT.TicketReceipt.DataAccessLayer;
using System.Data;
using System.Data.SqlClient;
using CT.Core;



    /// <summary>
    /// Summary description for City
    /// </summary>
    public class City
    {
        /// <summary>
        /// Private variables
        /// </summary>

        private string cityCode;
        private string cityName;
        private string countryName;
        private int index;
        private string stateProvince;
        /// <summary>
        /// Public Properties
        /// </summary>
        public string CityCode
        {
            get { return cityCode; }
            set { cityCode = value; }

        }
        public string CityName
        {
            get { return cityName; }
            set { cityName = value; }
        }
        public string CountryName
        {
            get { return countryName; }
            set { countryName = value; }
        }
        public int Index
        {
            get { return index; }
            set { index = value; }
        }
        public string StateProvince
        {
            get { return stateProvince; }
            set { stateProvince = value; }
        }

        public static SortedList GetCities(string countryCode)
        {
            SortedList Cities = new SortedList();

            try
            {
                
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@countryCode", countryCode);
                
                DataTable dtCities = DBGateway.FillDataTableSP("usp_GetCityByCountryCode", paramList);

                foreach (DataRow dr in dtCities.Rows)
                {
                    Cities.Add(dr["CityName"].ToString(), dr["CityCode"].ToString());
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }


            return Cities;
        }
        public static SortedList GetCitiesUnique(string countryCode)
        {
            SortedList Cities = new SortedList();

            try
            {

                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@countryCode", countryCode);

                DataTable dtCities = DBGateway.FillDataTableSP("usp_GetCityByCountryCode", paramList);

                foreach (DataRow dr in dtCities.Rows)
                {
                    Cities.Add(dr["CityCode"].ToString(), dr["CityName"].ToString());
                }
            }
            catch (Exception ex)
            {
                Audit.Add(EventType.Exception, Severity.High, 1, ex.Message, "0");
            }


            return Cities;
        }


        public static DataTable GetCityCode(string cityName, string countryName)
        {
            string[] code = new string[2];
            try
            {
                
                SqlParameter[] paramList = new SqlParameter[2];
                paramList[0] = new SqlParameter("@cityName", cityName);
                paramList[1] = new SqlParameter("@countryName", countryName);
                DataTable dtCodes = DBGateway.FillDataTableSP("usp_GetCityCountryCodesByNames", paramList);
                return dtCodes;
            }
            catch (Exception ex)
            {
                throw ex;
            }

        }

        public static string GetCountryCodeFromCityCode(string cityCode)
        {
            string countryCode = "";

            try
            {
                SqlParameter[] paramList = new SqlParameter[1];
                paramList[0] = new SqlParameter("@cityCode", cityCode);
                DataTable dt = DBGateway.FillDataTableSP("usp_GetCountryCodeFromCityCode", paramList);

                if (dt != null && dt.Rows.Count > 0)
                {
                    countryCode = dt.Rows[0]["CountryCode"].ToString();
                }
            }
            catch (Exception ex)
            {
                throw ex;
            }
            return countryCode;
        }



    }

