﻿using CT.Core;
using CT.TicketReceipt.DataAccessLayer;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for IRQApplicantDetails
/// </summary>
public class IRQApplicantDetails
{
    #region Private variables
    private long _visaId;
    private string _paxType;
    private string _applicantName;
    private DateTime _dateOfBirth;
    private string _passportNo;
    private DateTime _passportExpiry;
    private string _nationality;
    private string _document1Path;
    private string _document2Path;
    private string _document3Path;
    private string _document4Path;
    private string _document5Path;
    private string _visaStatus;
    private int _createdBy;
    #endregion

    #region Public Properties
    public long VisaId
    {
        get { return _visaId; }
        set { _visaId = value; }
    }
    public string PaxType
    {
        get { return _paxType; }
        set { _paxType = value; }
    }
    public string ApplicantName
    {
        get { return _applicantName; }
        set { _applicantName = value; }
    }
    public DateTime DateOfBirth
    {
        get { return _dateOfBirth; }
        set { _dateOfBirth = value; }
    }
    public string PassportNo
    {
        get { return _passportNo; }
        set { _passportNo = value; }
    }
    public DateTime PassportExpiry
    {
        get { return _passportExpiry; }
        set { _passportExpiry = value; }
    }
    public string Nationality
    {
        get { return _nationality; }
        set { _nationality = value; }
    }
    public string Document1Path
    {
        get { return _document1Path; }
        set { _document1Path = value; }
    }
    public string Document2Path
    {
        get { return _document2Path; }
        set { _document2Path = value; }
    }
    public string Document3Path
    {
        get { return _document3Path; }
        set { _document3Path = value; }
    }
    public string Document4Path
    {
        get { return _document4Path; }
        set { _document4Path = value; }
    }
    public string Document5Path
    {
        get { return _document5Path; }
        set { _document5Path = value; }
    }
    public string VisaStatus
    {
        get { return _visaStatus; }
        set { _visaStatus = value; }
    }
    public int CreatedBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }
    
    #endregion

    public IRQApplicantDetails()
    {
        //
        // TODO: Add constructor logic here
        //
    }

    public static DataTable GetVisaTypes()
    {
        DataTable dt = new DataTable();
        try
        {
            SqlParameter[] paramList = new SqlParameter[0];
            dt = DBGateway.FillDataTableSP("IRQ_GetVisaTypesList", paramList);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
        }
        return dt;
    }

    public static void Save(List<IRQApplicantDetails> lstApplicantDet, IRQHeader headerDet)
    {
        try
        {
            if (lstApplicantDet != null && lstApplicantDet.Count > 0)
            {
                using (System.Transactions.TransactionScope updateTransaction = new System.Transactions.TransactionScope())
                {

                    IRQHeader header = new IRQHeader();
                    header = headerDet;

                    if (header.VisaId <= 0)
                    {
                        header.Save();

                        if (header.VisaId > 0)
                        {
                            string imageName = string.Empty;
                            foreach (IRQApplicantDetails irqApplicant in lstApplicantDet)
                            {
                                imageName = header.AgentId + "_" + header.VisaId + "_" + irqApplicant.PassportNo + "_";
                                SqlParameter[] paramList = new SqlParameter[16];
                                paramList[0] = new SqlParameter("@VisaId", header.VisaId);
                                paramList[1] = new SqlParameter("@PaxType", irqApplicant.PaxType);
                                paramList[2] = new SqlParameter("@ApplicantName", irqApplicant.ApplicantName);
                                paramList[3] = new SqlParameter("@DOB", irqApplicant.DateOfBirth);
                                paramList[4] = new SqlParameter("@PassportNo", irqApplicant.PassportNo);
                                paramList[5] = new SqlParameter("@PassportExpiry", irqApplicant.PassportExpiry);
                                paramList[6] = new SqlParameter("@Nationality", irqApplicant.Nationality);
                                paramList[7] = new SqlParameter("@VisaStatus", irqApplicant.VisaStatus);
                                //Here Document 1,2,3 are mandatory and 4 is optional and 5 is not using yet
                                paramList[8] = new SqlParameter("@Doc1Path", imageName + "1");
                                paramList[9] = new SqlParameter("@Doc2Path", imageName + "2");
                                paramList[10] = new SqlParameter("@Doc3Path", imageName + "3");
                                //if image url is not present then need to send DBNull
                                if (irqApplicant.Document4Path != "N/A") 
                                {
                                    paramList[11] = new SqlParameter("@Doc4Path", imageName + "4");
                                }
                                else {
                                    paramList[11] = new SqlParameter("@Doc4Path", DBNull.Value);
                                }
                                if (irqApplicant.Document5Path != "N/A")
                                {
                                    paramList[12] = new SqlParameter("@Doc5Path", imageName + "5");
                                }
                                else {
                                    paramList[12] = new SqlParameter("@Doc5Path", DBNull.Value);
                                }
                                paramList[13] = new SqlParameter("@CreatedBy", irqApplicant.CreatedBy);

                                SqlParameter paramMsgType = new SqlParameter("@MsgType", SqlDbType.NVarChar);
                                paramMsgType.Size = 1;
                                paramMsgType.Direction = ParameterDirection.Output;
                                paramList[14] = paramMsgType;

                                SqlParameter paramMsg = new SqlParameter("@ErrMsg", SqlDbType.NVarChar);
                                paramMsg.Size = 1000;
                                paramMsg.Direction = ParameterDirection.Output;
                                paramList[15] = paramMsg;
                                //paramList[0] = new SqlParameter("", irqApplicant.VisaId);
                                //paramList[0] = new SqlParameter("", irqApplicant.VisaId);
                                DBGateway.ExecuteNonQuerySP("IRQ_SaveApplicant", paramList);

                                if (Convert.ToString(paramMsgType.Value) == "E")
                                {
                                    throw new Exception(Convert.ToString(paramMsg));
                                }
                            }

                            updateTransaction.Complete();
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw new Exception(ex.Message);
        }
    }
    public static void UpdateVisaStatus(long applicantId, string visaStatus, int modifiedBy)
    {
        try
        {
            SqlParameter[] paramList = new SqlParameter[3];
            paramList[0] = new SqlParameter("@ApplicantId", applicantId);
            paramList[1] = new SqlParameter("@VisaStatus", visaStatus);
            paramList[2] = new SqlParameter("@ModifiedBy", modifiedBy);
            DBGateway.ExecuteNonQuery("IRQ_UpdateVisaStatus", paramList);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
    }
}

public class IRQHeader
{
    #region private Variables
    private int _visaId;
    private string _docNumber;
    private int _visaTypeId;
    private int _adultCount;
    private int _childCount;
    private int _infantCount;
    private int _agentId;
    private int _locationId;
    private decimal _price;
    private int _createdBy;
    #endregion

    #region Public Properties
    public int VisaId
    {
        get { return _visaId; }
        set { _visaId = value; }
    }
    public string DocNumber
    {
        get { return _docNumber; }
        set { _docNumber = value; }
    }
    public int VisaTypeId
    {
        get { return _visaTypeId; }
        set { _visaTypeId = value; }
    }
    public int AdultCount
    {
        get { return _adultCount; }
        set { _adultCount = value; }
    }
    public int ChildCount
    {
        get { return _childCount; }
        set { _childCount = value; }
    }
    public int InfantCount
    {
        get { return _infantCount; }
        set { _infantCount = value; }
    }
    public int AgentId
    {
        get { return _agentId; }
        set { _agentId = value; }
    }
    public int LocationId
    {
        get { return _locationId; }
        set { _locationId = value; }
    }
    public decimal Price
    {
        get { return _price; }
        set { _price = value; }
    }
    public int CreatedBy
    {
        get { return _createdBy; }
        set { _createdBy = value; }
    }
    #endregion

    public int Save()
    {
        VisaId = 0;
        try
        {
            SqlParameter[] paramList = new SqlParameter[9];
            //paramList[0] = new SqlParameter("@DocNumber", DocNumber);
            paramList[0] = new SqlParameter("@VisaTypeId", VisaTypeId);
            paramList[1] = new SqlParameter("@AdultCount", AdultCount);
            paramList[2] = new SqlParameter("@ChildCount", ChildCount);
            paramList[3] = new SqlParameter("@InfantCount", InfantCount);
            paramList[4] = new SqlParameter("@AgentId", AgentId);
            paramList[5] = new SqlParameter("@LocationId", LocationId);
            paramList[6] = new SqlParameter("@Price", Price);
            paramList[7] = new SqlParameter("@CreatedBy", CreatedBy);
            SqlParameter paramVisaId = new SqlParameter("@VisaId", SqlDbType.Int);
            paramVisaId.Direction = ParameterDirection.Output;
            paramList[8] = paramVisaId;
            //SqlParameter paramDocNo = new SqlParameter("@DocNumber", SqlDbType.NVarChar);
            //paramDocNo.Size = 50;
            //paramDocNo.Direction = ParameterDirection.Output;
            //paramList[9] = paramDocNo;

            DBGateway.ExecuteNonQuerySP("IRQ_SaveHeaderDetails", paramList);
            if (Convert.ToInt32(paramVisaId.Value) > 0)
            {
                VisaId = Convert.ToInt32(paramVisaId.Value);
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw new Exception(ex.Message);
        }
        return VisaId;
    }

    public DataTable GetVisaList(DateTime fromDate, DateTime toDate, string docNo, string status, int agentId, int visaType)
    {
        DataTable visaDetails = new DataTable();
        try
        {
            SqlParameter[] paramList = new SqlParameter[6];
            paramList[0] = new SqlParameter("@fromDate", fromDate);
            paramList[1] = new SqlParameter("@toDate", toDate);
            if (!string.IsNullOrEmpty(docNo))
            {
                paramList[2] = new SqlParameter("@docNo", docNo);
            }
            else
            {
                paramList[2] = new SqlParameter("@docNo", DBNull.Value);
            }
            if (!string.IsNullOrEmpty(status) && status != "-1")
            {
                paramList[3] = new SqlParameter("@visaStatus", status);
            }
            else {
                paramList[3] = new SqlParameter("@visaStatus", DBNull.Value);
            }
            if (agentId > 0)
            {
                paramList[4] = new SqlParameter("@visaAgentId", agentId);
            }
            else
            {
                paramList[4] = new SqlParameter("@visaAgentId", DBNull.Value);
            }
            if (visaType > 0)
            {
                paramList[5] = new SqlParameter("@visaType", visaType);
            }
            else {
                paramList[5] = new SqlParameter("@visaType", DBNull.Value);
            }
            visaDetails = DBGateway.FillDataTableSP("IRQ_GetVisaDetails", paramList);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Visa, Severity.High, 0, ex.ToString(), "0");
            throw;
        }
        return visaDetails;
    }
}