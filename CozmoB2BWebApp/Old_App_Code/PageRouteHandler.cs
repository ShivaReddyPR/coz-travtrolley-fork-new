﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.Routing;
using System.Web.Compilation;


public class UserRouteHandler : IRouteHandler
{
    public UserRouteHandler(string virtualPath)
    {
        _virtualPath = virtualPath;
    }

    public IHttpHandler GetHttpHandler(RequestContext requestContext)
    {
        var display = BuildManager.CreateInstanceFromVirtualPath(
                      _virtualPath, typeof(Page)) as IUserDisplay;
        display.UniqueName =
          requestContext.RouteData.Values["name"] as string;
        return display;
    }

    string _virtualPath;
}
public interface IUserDisplay : IHttpHandler
{
    string UniqueName { get; set; }
}
