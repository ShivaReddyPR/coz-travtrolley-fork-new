using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Collections.Generic;
using System.Data.SqlClient;
using CT.Core;
/// <summary>
/// Summary description for AirlineInfo
/// </summary>
public class AirlineInfo
{
    public AirlineInfo()
    {
        //
        // TODO: Add constructor logic here
        //
    }
    private string airlineCode;
    private string airlineName;
    private string departAirportName;
    private int numOfFlights;
    private string duration;
    private bool isStopOver;
    private int totalAmount;
    private DateTime dateTime;

    public string AirlineCode
    {
        get
        {
            return airlineCode;
        }
        set
        {
            airlineCode = value;
        }
    }
    public string AirlineName
    {
        get
        {
            return airlineName;
        }
        set
        {
            airlineName = value;
        }
    }
    public string DepartAirportName
    {
        get
        {
            return departAirportName;
        }
        set
        {
            departAirportName = value;
        }
    }
    public int NumOfFlights
    {
        get
        {
            return numOfFlights;
        }
        set
        {
            numOfFlights= value;
        }
    }
    public string Duration    
    {
        get
        {
            return duration;
        }
        set
        {
            duration = value;
        }
    }
    public bool IsStopOver
    {
        get
        {
            return isStopOver;
        }
        set
        {
            isStopOver = value;
        }
    }
    public int TotalAmount
    {
        get
        {
            return totalAmount;
        }
        set
        {
            totalAmount= value;
        }
    }

    public static List<string> GetAllAirlines()
    {
        List<string> Airlines = new List<string>();

        try
        {
            SqlParameter[] paramList = new SqlParameter[0];
            DataTable dtAirlines = CT.TicketReceipt.DataAccessLayer.DBGateway.FillDataTableSP("usp_GetAllAirlines", paramList);

            foreach (DataRow row in dtAirlines.Rows)
            {
                Airlines.Add(row["airlineCode"].ToString() + "|" + row["airlineName"].ToString());
            }
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.GoAirSearch, Severity.High, 0, ex.Message, "0");
        }

        return Airlines;
    }
    //public static DataTable InsuranceGetListTemp(DateTime fromDate, DateTime toDate, int agentId, long locationId, string memberType, int acctStatus, string transType, string agentType)
    //{
    //    SqlDataReader data = null;
    //    DataTable dt = new DataTable();
    //    using (SqlConnection connection = CT.TicketReceipt.DataAccessLayer.DBGateway.GetConnection())
    //    {
    //        SqlParameter[] paramList;
    //        try
    //        {
    //            paramList = new SqlParameter[7];
    //            paramList[0] = new SqlParameter("@P_FROM_DATE", fromDate);
    //            paramList[1] = new SqlParameter("@P_TO_DATE", toDate);
    //            if (agentId > 0) paramList[2] = new SqlParameter("@P_AGENT_ID", agentId);
    //            if (locationId > 0) paramList[3] = new SqlParameter("@P_LOCATION_ID", locationId);
    //            //if (userId > 0) paramList[4] = new SqlParameter("@P_USER_ID", userId);
    //            //paramList[5] = new SqlParameter("@P_USER_TYPE", memberType);
    //            if (acctStatus >= 0) paramList[4] = new SqlParameter("@P_IsAccounted", acctStatus);
    //            if (!string.IsNullOrEmpty(transType)) paramList[5] = new SqlParameter("@P_TransType", transType);
    //            paramList[6] = new SqlParameter("@P_AgentType", agentType);
    //            data = CT.TicketReceipt.DataAccessLayer.DBGateway.ExecuteReaderSP("usp_insurance_acct_report_getlist", paramList, connection);
    //            if (data != null)
    //            {
    //                dt.Load(data);
    //            }
    //            connection.Close();
    //        }
    //        catch
    //        {
    //            throw;
    //        }
    //    }
    //    return dt;
    //}
}
