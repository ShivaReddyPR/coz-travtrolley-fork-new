using System;
using System.Collections.Generic;
using System.Text;

namespace Technology.MetaSearchEngine
{
    internal static class SPNames
    {
        public const string GetSourceByOriginDest = "usp_GetSourceByOriginDest";
        public const string GetHoldPnrToRelease = "usp_GetHoldPnrToRelease";
    }
}
