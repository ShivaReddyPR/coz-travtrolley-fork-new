using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.IO;
using CT.TicketReceipt.BusinessLayer;

/// <summary>
/// Summary description for UploadDocument
/// </summary>
public class UploadDocument
{
    public string FileName;
    public string SavePath;
    public string ServerFileName;
    public byte[] Content = null;
    //public static void saveFile(System.Web.UI.WebControls.FileUpload fileUpload)
    //{
    //    //CreateCacheDirectory();
    //    string dir = string.Format("{0}\\{1}", HttpContext.Current.Server.MapPath(getTempDirName), getSubDirName);
    //    fileUpload.SaveAs(dir +"\\" + fileUpload.FileName);
    //}
    public System.Web.UI.WebControls.FileUpload FileUploder;
    public static string CreateImage(object byteArray, string fileName)
    {
        try
        {
            if (byteArray is DBNull || byteArray == null)
            {
                return null;
            }
            byte[] bytes = (byte[])byteArray;
            CreateCacheDirectory();
            string imgPath = string.Format("{0}\\{1}\\{2}", HttpContext.Current.Server.MapPath(getTempDirName), getSubDirName, fileName);
            FileStream fs = new FileStream(imgPath, FileMode.Create);
            fs.Write(bytes, 0, bytes.Length);
            fs.Flush();
            fs.Close();
            string imgUrl = string.Format("{0}/{1}/{2}", getTempDirName, getSubDirName, fileName);
            return imgUrl;
        }
        catch { throw; }
    }

    public static void ClearCache()
    {
        try
        {
            string dir = string.Format("{0}\\{1}", HttpContext.Current.Server.MapPath(getTempDirName), getSubDirName);
            // if (Directory.Exists(dir)) Directory.Delete(dir,true);
        }
        catch { throw; }
    }
    private static string CreateCacheDirectory()
    {

        try
        {
            string temp = HttpContext.Current.Server.MapPath(getTempDirName);
            if (!Directory.Exists(temp)) Directory.CreateDirectory(temp);
            temp = string.Format("{0}\\{1}", temp, getSubDirName);
            if (!Directory.Exists(temp)) Directory.CreateDirectory(temp);
            return temp;
        }
        catch { throw; }

    }

    private static string getTempDirName
    {
        get
        { return "~/Temp"; }
    }

    private static string getSubDirName
    {
        get { return string.Format("{0:0000000000}", HttpContext.Current.Session.SessionID); }
        //get { return "ziya"; }

    }

}

public class DocumentViewer
{
    public static DocumentInfo CurrentDocumentInfo
    {
        get { return (DocumentInfo)HttpContext.Current.Session["_CurrentDocumentInfo"]; }
        set
        {
            HttpContext.Current.Session["_CurrentDocumentInfo"] = value;
        }
    }
}
