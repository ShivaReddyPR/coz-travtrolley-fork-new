﻿using DevExpress.Web;
using System;
using System.Collections.Generic;
using System.Linq;

public class Customprovider :PhysicalFileSystemProvider
{
      string InitialFolder;
    public Customprovider(string initialfolder) : base("~\\DocumentsUpload") { InitialFolder = initialfolder; }

    public override IEnumerable<FileManagerFolder> GetFolders(FileManagerFolder rootFolder)
    {
        try
        {      
        IEnumerable<FileManagerFolder> folders = base.GetFolders(rootFolder);
        return folders.Where(f => f.FullName.Contains(InitialFolder));
        }
        catch(Exception ex)
        {
            throw ex;
        }
    }
}
