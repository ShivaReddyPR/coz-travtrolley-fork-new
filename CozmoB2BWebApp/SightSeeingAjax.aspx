﻿<%@ Page Language="C#" AutoEventWireup="true" Inherits="SightSeeingAjax" Codebehind="SightSeeingAjax.aspx.cs" %>
<%@ Import Namespace="System.Collections.Generic" %>

<script type="text/javascript">
    document.write('<style>.noscript { display: none; }</style>');
		</script>
<%if (ErrorFlag != true)
  { %>
  <%if (Request["Action"].ToString() == "getSightSeeingDetail")
    {%>
    
    
    
    <div> 

<div class="window_title"> <%=staticInfo.ItemName%> </div>
    
 
<div class="container_iframe">
          <ul class="tabs">
              
              <li  id="lisig_overview"><a href="javaScript:ShowDiv('sig_overview','overview')">Description</a></li>
              
              <li  id="lisig_inclusions" ><a href="javaScript:ShowDiv('sig_inclusions','inclusions')">Inclusions & Exclusions</a></li>
              <li id="lisig_operation" ><a href="javaScript:ShowDiv('sig_operation','operation')">Tour Operation</a></li>
              <li id="lisig_note" ><a href="javaScript:ShowDiv('sig_note','note')">Note</a></li>
              <li id="lisig_terms" ><a href="javaScript:ShowDiv('sig_terms','terms')">Terms & Conditions</a></li>
            </ul>
          </div>
 <div class="hotel_inner-sc">
          
          <div style=" padding:4px 11px 16px 11px; display:none"  id="sig_overview">
          <div >
            <%if (staticInfo.Description != null)
              { %>            
              <p class="fleft">
                <%=staticInfo.Description%>
              </p>
              </br>
              <%} %> 
              <h3>Tour Information</h3>
               <%if (staticInfo.TourInfo != null)
                 { %>    
              <p class="fleft">
                <%=staticInfo.TourInfo%>
              </p>
               <%} %>     
               </div>
          </div>
          <div style=" padding:4px 11px 0px 11px;  display:none" id="sig_inclusions" >
          <div >
            <%if (staticInfo.Includes != null)
              { %>   
               <h4>Inclusions</h4>
              <p class="fleft">
                <%=staticInfo.Includes%>
              </p>
              <br/>
               <%} %> 
              <%if (staticInfo.Excludes != null)
              { %> 
               <br/>
             
              <p class="fleft">
                 
                <b>Exclusions</b> 
              <br/>
                <%=staticInfo.Excludes%>
              </p>
               <%} %>   
               </div>
 
          </div> 
         
          <div style=" padding:4px 11px 0px 11px;  display:none" id="sig_operation" >
          <div >
            <%if (staticInfo.TourOperationInfo != null && staticInfo.TourOperationInfo.Count > 0)
              { %>            
             <table class="table" border="1" width="100%">
                                           <tr style="background:#c7dfec; font-weight:bold">
                                               <td>Language</td>
                                               <td>Commentry</td>
                                               <td>From Date</td>
                                               <td>To Date</td>
                                               <td>Frequency</td>
                                               <%if (staticInfo.TourOperationInfo[0].DepTime != null && staticInfo.TourOperationInfo[0].DepTime.Length > 0)
                                                 { %>
                                               <td>Departure Time</td>
                                               <td>Departure Name</td>
                                               <%}
                                                 else if (staticInfo.TourOperationInfo[0].FromTime != null && staticInfo.TourOperationInfo[0].FromTime.Length > 0)
                                                 { %>
                                               <td>From</td>
                                               <td>To</td>
                                               <td>Every</td>
                                               <%} %>
                                            </tr>
                                            <%foreach (CT.BookingEngine.TourOperations tourInfo in staticInfo.TourOperationInfo)
                                              { %>
                                            <tr>
                                               <td><%=tourInfo.Language%></td>
                                               <td><%=tourInfo.Commentary%></td>
                                               <td><%=tourInfo.FromDate.ToString("dd/MM/yy")%></td>
                                               <td><%=tourInfo.ToDate.ToString("dd/MM/yy")%></td>
                                               <td><%=tourInfo.Frequency%></td>
                                                <%if (staticInfo.TourOperationInfo[0].DepTime != null && staticInfo.TourOperationInfo[0].DepTime.Length > 0)
                                                  { %>
                                               <td><%=tourInfo.DepTime%></td>
                                               <td><%=tourInfo.DepName%></td>
                                                <%}
                                                  else if (staticInfo.TourOperationInfo[0].FromTime != null && staticInfo.TourOperationInfo[0].FromTime.Length > 0)
                                                  { %>
                                               <td><%=tourInfo.FromTime%></td>
                                                <td><%=tourInfo.ToTime%></td>
                                               <td><%=tourInfo.Interval%></td>
                                                <%} %>
                                              </tr>
                                            <%} %>
                                            <tr>
                                              <td colspan="8">A - Arabic
                                                             C - Cantonese
                                                              D - Danish
                                                             E - English
                                                            F - French
                                                            G - German
                                                            H - Hebrew
                                                            I - Italian
                                          J - Japanese
                                              K - Korean
                                             M - Mandarin
                                             P - Portuguese
                                             R - Russian
                                             S - Spanish 
                                            </tr>
                                           
                                           
                                           </table> 
               <%} %>     
               </div>
 
          </div>  
          <div style=" padding:4px 11px 0px 11px;  display:none" id="sig_note" >
          <div >
          <table>
            <%if (staticInfo.Notes != null)
              { %> 
              <tr>
                  <td>
                   <%--<h3>Notes</h3>--%>
                  </td>
              </tr>
              <p class="fleft">
                <%=staticInfo.Notes%>
              </p>
               <%} %>  
               <%if (staticInfo.DeparturePoint != null)
                 { %> 
               <tr><td>
               <h3>Pickup Location and Pcikup Time</h3></td></tr>
               <tr><td>
                          
              <%string[] aData = staticInfo.DeparturePoint.Split(',');
                  foreach (string depoint in aData)
                  {%>
                   <li><%=depoint%></li>
                    <%}
                     %>
                </td>
               </tr> 
               <%} %>
              </table>
               </div>
              <%-- <%=staticInfo.departurePoint%>--%>
              </div>
          <div style=" padding:4px 11px 0px 11px;  display:none" id="sig_terms" >
          <div >
          <table>
            <%if (chargeResponse != null && chargeResponse.Count > 1)
                  { %>
                      <tr>
                <td>
               <h3> Cancellation Policy</h3>
                </td>
                </tr>
                <%  
                  foreach(KeyValuePair<string,string> cancelData in chargeResponse)
                  {
                      if (cancelData.Key == "CancelPolicy")
                      {
                          string[] cancel = cancelData.Value.Split('|');
                          foreach (string cancelMsg in cancel)
                          {
                      %>
           
                   <tr>
                  <td>
                  <p class="fleft"> <%=cancelMsg%></p>
                  </td>
                   </tr>
                  <%}
                      }
                        } %>
                <%} %>

                <%if (staticInfo.Notes != null)
                 { %> 
               <tr><td>
               <h3>Notes</h3></td></tr>
               <tr><td>
                          
              <p class="fleft">
                <%=staticInfo.Notes%>
              </p>
               
              </td>
               </tr> 
               <%} %>
                <%if (selectedResult.AdditionalInformationList != null && selectedResult.AdditionalInformationList.Length > 0)
                      { %>  
               <tr>
               <td>
               <h3>Additional Information</h3></td> </tr>
              
                     
              <% foreach (string addition in selectedResult.AdditionalInformationList)
                 {%>  
              <tr>
              <p>
              <td>
                <%=addition%>
                </td>
              </p>
              </tr>
              <%} %>
               
               
               <%} %> 
               </table>
               </div>
 
          </div>
</div>
   
    </div>
    
    <%}
    else if (Request["Action"].ToString() == "getCancellationDetail")
    { %>
    
    <%if( (chargeResponse != null && chargeResponse.Count > 1 ) || selectedResult.CancellationCharges!=null)
            {%>

    <div>
        <%--<div class="popup_h">--%>
            <%--<div class="popup_left">
            </div>--%>
            <div class="showMsgHeading">
                <span class="hotel_name">Cancellation and Charges</span>
           <%-- </div>--%>
            <%--<div class="popup_right">
            </div>--%>
            <div class="clear">
            </div>
        </div>
        <div style=" height:400px;" class="hotel_inner-sc">
            <div style="padding: 4px 11px 16px 11px; display: block" id="Div1">
                <div>
                <table>
                 <%if ((chargeResponse != null && chargeResponse.Count > 1) )
            { %>
                   <tr>
                <td>
               <h3> Cancellation Policy</h3>
                </td>
                </tr>
                <%foreach (KeyValuePair<string, string> cancelData in chargeResponse)
            {
                if (cancelData.Key == "CancelPolicy")
                {
                    string[] cancel = cancelData.Value.Split('|');
                    foreach (string cancelMsg in cancel)
                    {
                        %>
                   <tr>
                  <td>
                  <p class="fleft"> <%=cancelMsg%></p>
                  </td>
                   </tr>
                  <%}
                }
            } %>
                <%} else if ( selectedResult.CancellationCharges!=null) { 
                    %>
                     <tr>
                <td>
               <h4> Cancellation Policy</h4>
                </td>
                </tr>
                    <% foreach (string cancelData in selectedResult.CancellationCharges)
                    {
                        %>
                   <tr>
                  <td>
                  <p class="fleft"> <%=cancelData%></p>
                  </td>
                   </tr>
                  <%} %>
                     <%}
                    %>
                </table>
                </div>
            </div>
        </div>
    </div>

      <%}%>
  
  <%}
  }
      else
  {%>
 
        <div style="text-align:justify; float:left;">
  
        <div class="hotel_details" >
        <div style="height:30px; background-color:#1c498a; width:100%;">
                            <span style="color:#ffffff;font-weight:bold;font-size:18px;position:absolute;">Session Expired!</span><br />
                            </div>
                            <div style="text-align:center; padding-top:40px;">
         <h4 style="color:Red; font-weight:bold"><%=errorMessage %></h4>
         </div>
   </div>
   </div>
  <%} %>
