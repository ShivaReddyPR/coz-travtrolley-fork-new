using System;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.GlobalVisa;
using System.Collections.Generic;

public partial class PrintGVSalesReceiptUI : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            if (!IsPostBack)
            {
                //LoginInfo loginInfo = Settings.LoginInfo;
                //if (loginInfo == null)
                //{
                //    Response.Redirect("SessionExpired.aspx");
                //    // Response.Redirect(string.Format("ErrorPage.aspx?Err={0}", GetGlobalResourceObject("ErrorMessages", "INVALID_USER")));
                //}
                string strQuery = Request.QueryString["vsId"];
                if (!string.IsNullOrEmpty(strQuery))
                {
                    long vsId = Utility.ToLong(strQuery);
                    ShowData(vsId);
                    ViewState["view_fpax_id"] = "0";
                }
                    
            }
        }
        catch (Exception ex)
        {
            Utility.WriteLog(ex, this.Title);
        }
    }

    private void ShowData(long vsId)
    {
        try
        {
            GVVisaSale tranxVS = new GVVisaSale(vsId);
            lblReceiptNoValue.Text = lblCopyReceiptNoValue.Text = tranxVS.DocNumber;
            lblDateValue.Text = lblCopyDateValue.Text = IDDateTimeFormat(tranxVS.DocDate);
            //lblDocumentsDate.Text = DateFormat(tranxVS.DocDate);
            lblVisaTypeValue.Text = lblCopyVisaTypeValue.Text = tranxVS.SelVisaType;
            lblSubmittedForValue.Text = lblCopySubmittedForValue.Text = tranxVS.SelCountry;
            lblAdultsValue.Text = lblCopyAdultsValue.Text  =Utility.ToString(tranxVS.Adults);
            lblChildValue.Text = lblCopyChildValue.Text = Utility.ToString(tranxVS.Children);
            lblInfantValue.Text = lblCopyInfantValue.Text = Utility.ToString(tranxVS.Infants);
            //lblPrincipalApplicantValue.Text = lblCopyPrincipalApplicantValue.Text = Utility.ToString(tranxVS.Infants);
            lblNationalityValue.Text = lblCopyNationalityValue.Text = tranxVS.SelNationality;
            lblVisaFeeValue.Text = lblCopyVisaFeeValue.Text = Utility.ToString(tranxVS.TotVisaFee);//splitting the visa fee from Svc Tax
            //lblConsultantValue.Text = lblCopyConsultantValue.Text = tranxVS.CreatedByName;
            //lblSubmitted.Text= tranxVS.CountryName;
            lblDocketNoValue.Text = lblCopyDocketNoValue.Text = tranxVS.DocNumber;
            string addFees = string.Empty;
            //if (tranxVS.UrgentStatus == "Y") addFees = "Urgent Visa";

            //if (tranxVS.Add1FeeStatus == "Y")
            //{ 
            //    if(string.IsNullOrEmpty(addFees)) addFees = "Additional Fee 1";
            //    else addFees = addFees + ',' + "Additional Fee 1";             
            //}
            //if (tranxVS.Add2FeeStatus == "Y")
            //{
            //    if(string.IsNullOrEmpty(addFees))  addFees = "Additional Fee 2";
            //    else addFees = addFees + ',' + "Additional Fee 2";
            //}

            //if (!string.IsNullOrEmpty(addFees )) trAddFee.Visible = trCopyAddFee.Visible = true;
            //lblAddFeeValue.Text = lblCopyAddFeeValue.Text = addFees;

            //string addServices = string.Empty;
            //DataTable dtAddSvc = tranxVS.AddSvcDetails;
            //foreach (DataRow dr in dtAddSvc.Rows)   
            //{
            //    if (string.IsNullOrEmpty(addServices)) addServices = Utility.ToString(dr["adsvc_name"]);
            //    else addServices = addServices + ',' + Utility.ToString(dr["adsvc_name"]);
            
            //}
            //if(!string.IsNullOrEmpty(addServices)) trAddSvc.Visible = trCopyAddSvc.Visible = true;
            //lblAddServicesValue.Text = lblCopyAddServicesValue.Text = addServices;
            //int totalNo=tranxVS.Adults+tranxVS.Children+tranxVS.Infants;
            //lblTotalNo.Text = Utility.ToString(totalNo);
            List<GVPassenger> passList = tranxVS.PassengerList;
            if(passList != null && passList.Count>0)
            {
                //lblDocApplicant.Text = Utility.ToString(dt.Rows[0]["FPAX_NAME"]);
                //lblPrincipalApplicantValue.Text = lblCopyPrincipalApplicantValue.Text = Utility.ToString(dt.Rows[0]["FPAX_NAME"]);
                //lblPassportNoValue.Text = lblCopyPassportNoValue.Text = Utility.ToString(dt.Rows[0]["FPAX_PASSPORT_NO"]);
                //lblPassportNoValue.Text = lblCopyPassportNoValue.Text = Utility.ToString(dr["pax_visitor_passport_no"]) + " , " + lblPassportNoValue.Text;
                //lblNationalityValue.Text = lblCopyNationalityValue.Text = Utility.ToString(dr["pax_visitor_nationality_name"]) + " , " + lblNationalityValue.Text;
                dlPaxDetails.DataSource  = passList;
                dlPaxDetails.DataBind();
                dlCopyPaxDetails.DataSource = passList;
                dlCopyPaxDetails.DataBind();
            }

            //PaymentMode mode = PaymentMode.Cash;
            //string modeDescription = string.Empty;
            //switch (tranxVS.SettlementMode)
            //{
            //    case "1":
            //        modeDescription = string.Format("{0}:{1}", mode.ToString(), Formatter.ToCurrency(tranxVS.CashMode.BaseAmount));
            //        break;
            //    case "2":
            //        modeDescription = string.Format("{0}:{1}", PaymentMode.Credit.ToString(), Formatter.ToCurrency(tranxVS.CreditMode.BaseAmount));
            //        break;
            //    case "3":
            //        modeDescription = string.Format("{0}:{1}", PaymentMode.Card.ToString(), Formatter.ToCurrency(tranxVS.CardMode.BaseAmount));
            //        break;
            //    case "4":
            //        modeDescription = string.Format("{0}:{1},{2}:{3}", PaymentMode.Cash.ToString(), Formatter.ToCurrency(tranxVS.CashMode.BaseAmount), PaymentMode.Card.ToString(), Formatter.ToCurrency(tranxVS.CreditMode.BaseAmount));
            //        break;
            //    case "5":
            //        modeDescription = string.Format("{0}:{1},{2}:{3}", PaymentMode.Cash.ToString(), Formatter.ToCurrency(tranxVS.CashMode.BaseAmount), PaymentMode.Card.ToString(), Formatter.ToCurrency(tranxVS.CardMode.BaseAmount));
            //        //mode = PaymentMode.Cash_Card;
            //        break;
            //    case "6":
            //        modeDescription = string.Format("{0}:{1},{2}:{3},{4}:{5}", PaymentMode.Cash.ToString(), Formatter.ToCurrency(tranxVS.CashMode.BaseAmount), PaymentMode.Credit.ToString(), Formatter.ToCurrency(tranxVS.CreditMode.BaseAmount), PaymentMode.Card.ToString(), Formatter.ToCurrency(tranxVS.CardMode.BaseAmount));
            //        //mode = PaymentMode.Cash_Credit_Card;
            //        break;
            //    case "7":
            //        modeDescription = string.Format("{0}:{1}", PaymentMode.Employee.ToString(), Formatter.ToCurrency(tranxVS.EmployeeMode.BaseAmount));
            //        //mode = PaymentMode.Employee;
            //        break;
            //    case "8":
            //        modeDescription = string.Format("{0}:{1}", PaymentMode.Others.ToString(), Formatter.ToCurrency(tranxVS.OthersMode.BaseAmount));
            //        //mode = PaymentMode.Others;
            //        break;
            //}
            lblCollectionModeValue.Text = lblCopyCollectionModeValue.Text = string.Format("{0}:{1}", tranxVS.SettlementMode, Formatter.ToCurrency(tranxVS.TotVisaFee)) + " (" + tranxVS.CurrencyCode + ")";
            //lblLocationTerms.Text = tranxVS.LocationTerms;
            //lblLocationAds.Text = tranxVS.LocationAdds;
            //lblSvcTaxValue.Text = lblCopySvcTaxValue.Text = Formatter.ToCurrency(tranxVS.SVCTaxAmount);
            //dlPaxDocuments.DataSource = tranxVS.PaxDocDetails;
            //dlPaxDocuments.DataBind();

            string script = "window.open('PrintGVTC.aspx?vsId=" + Utility.ToString(vsId) + "','','width=600,height=600,toolbar=yes,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');";
            Utility.StartupScript(this.Page, script, "PrintGVSalesReceipt");


        }
        catch { throw; }
    }
    protected void dlPaxDocuments_ItemDataBound(object sender, DataListItemEventArgs e)
    {
        try
        {
        //    if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        //    {
        //        Label lblPaxName = (Label)e.Item.FindControl("ITlblPaxName");
        //        HiddenField hdfPaxId = (HiddenField)e.Item.FindControl("IThdfPaxId");

        //        if (hdfPaxId.Value == Utility.ToString(ViewState["view_fpax_id"])) lblPaxName.Text = string.Empty;
        //        else
        //        {
        //            lblPaxName.Text = lblPaxName.Text = lblPaxName.Text +":-";
        //            lblPaxName.Width = Unit.Pixel(150);
        //        }
        //        ViewState["view_fpax_id"] = hdfPaxId.Value;
        //    }
        }
        catch{throw;}


    }
    #region Date Format
    protected string IDDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy HH:MM");
        }
    }
    protected string DateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }


    #endregion
   
}



