﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;

public partial class DealEntryGUI :CT.Core.ParentPage// System.Web.UI.Page
{
    private string AGENT_NOTICE_SESSION = "_agentNotice";
    private string AGENT_NOTICE_SEARCH_SESSION = "_agentNoticeSearch";
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (!IsPostBack)
            {
                InitializeControls();
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void InitializeControls()
    {
        try
        {
            BingAgents();
        }
        catch
        {
            throw;
        }
    }
    private AgentNotice CurrentObject
    {
        get
        {
            return (AgentNotice)Session[AGENT_NOTICE_SESSION];
        }
        set
        {
            if (value == null)
            {
                Session.Remove(AGENT_NOTICE_SESSION);
            }
            else
            {
                Session[AGENT_NOTICE_SESSION] = value;
            }
        }
    }
    private DataTable SearchList
    {
        get
        {
            return (DataTable)Session[AGENT_NOTICE_SEARCH_SESSION];
        }
        set
        {
            value.PrimaryKey = new DataColumn[] { value.Columns["noticeId"] };
            Session[AGENT_NOTICE_SEARCH_SESSION] = value;
        }
    }
    private void BingAgents()
    {
        try
        {
            chkAgents.DataSource = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
            chkAgents.DataValueField = "AGENT_ID";
            chkAgents.DataTextField = "AGENT_NAME";
            chkAgents.DataBind();
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void btnSave_Click(object sender, EventArgs e)
    {
        try
        {
            Save();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void btnCancel_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }
    private void Save()
    {

        try
        {
            foreach (ListItem item in chkAgents.Items)
            {
                if (item.Selected)
                {
                    AgentNotice objNotice;
                    if (CurrentObject == null)
                    {
                        objNotice = new AgentNotice();
                    }
                    else
                    {
                        objNotice = CurrentObject;
                    }
                    objNotice.AgencyId = Convert.ToInt32(item.Value);
                    objNotice.Subject = Convert.ToString(txtSubject.Text);
                    objNotice.Message = Convert.ToString(txtMessage.Text);
                    if (rbtnReadStatus.SelectedItem.Text == "Yes")
                    {
                        objNotice.ReadStatus = rbtnReadStatus.SelectedItem.Value;
                    }
                    else
                    {
                        objNotice.ReadStatus = rbtnReadStatus.SelectedItem.Value;
                    }
                    objNotice.Status = Settings.ACTIVE;
                    objNotice.CreatedBy = Settings.LoginInfo.UserID;
                    objNotice.Save();
                    lblSuccessMsg.Visible = true;
                    if (CurrentObject == null)
                    {
                        lblSuccessMsg.Text = "Notice Saved successfully";
                    }
                    else
                    {
                        lblSuccessMsg.Text = "Notice Updated Successfully";
                    }
                }
            }
            Clear();
        }

        catch
        { throw; }
    }
    private void Clear()
    {
        try
        {
            txtSubject.Text = string.Empty;
            txtMessage.Text = string.Empty;

            foreach (ListItem item in chkAgents.Items)
            {
                item.Selected = false;
                item.Enabled = true;
            }

            rbtnReadStatus.SelectedIndex = 0;
            btnSave.Text = "Save";
            CurrentObject = null;
        }
        catch { throw; }

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            this.Master.ShowSearch("Search");
            Clear();
            bindSearch();
        }
        catch { throw; }
    }

    private void bindSearch()
    {
        try
        {
            DataTable dt = AgentNotice.GetList(ListStatus.Long, RecordStatus.All);
            SearchList = dt;
            gvSearch.DataSource = SearchList;
            gvSearch.DataBind();
        }
        catch { throw; }
    }

    protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvSearch.PageIndex = e.NewPageIndex;
            gvSearch.EditIndex = -1;
            gvSearch.DataSource = SearchList;
            gvSearch.DataBind();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }
    }

    protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            long msgId = Convert.ToInt64(gvSearch.SelectedValue);
            Edit(msgId);
            this.Master.HideSearch();

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
        }

    }
    private void Edit(long id)
    {
        try
        {
            AgentNotice objNotice = new AgentNotice(id);
            CurrentObject = objNotice;
            foreach (ListItem item in chkAgents.Items)
            {
                if (item.Value == objNotice.AgencyId.ToString())
                {
                    item.Selected = true;
                }
                item.Enabled = false;
            }

            txtSubject.Text = Convert.ToString(objNotice.Subject);
            txtMessage.Text = Convert.ToString(objNotice.Message);
            rbtnReadStatus.SelectedValue = Convert.ToString(objNotice.ReadStatus);
            btnSave.Text = "Update";
        }
        catch
        {
            throw;
        }
    }
}

