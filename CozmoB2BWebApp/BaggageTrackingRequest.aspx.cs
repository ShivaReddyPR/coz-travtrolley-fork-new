﻿using CT.BookingEngine;
using CT.Core;
using CT.TicketReceipt.Common;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.Script.Services;
using CT.TicketReceipt.BusinessLayer;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace CozmoB2BWebApp
{    
    public partial class BaggageTrackingRequest : CT.Core.ParentPage
    {
        protected string trackNumber = string.Empty;
        protected void Page_Load(object sender, EventArgs e)
        {
            if(Settings.LoginInfo==null)
            {
                Response.Redirect("AbandonSession.aspx",false);
            }   
        }

        protected void btnsubmit_Click(object sender, EventArgs e)
        {
            string rootFolder = Utility.ToString(System.Configuration.ConfigurationManager.AppSettings["BaggageUploads"]);

            BaggagePassengerDetails baggagePassengerDetails = new BaggagePassengerDetails();
            baggagePassengerDetails.EmailID = txtemail.Text;
            baggagePassengerDetails.PhoneNumber = txtmobileNo.Text;
            baggagePassengerDetails.PNRNumber = txtpnr.Text;
            baggagePassengerDetails.UnDelivaredBags = Convert.ToInt32(ddlbags.SelectedValue);

            DataTable dt = getBaggageDetails(txtpnr.Text, txtpaxname.Text, txtmobileNo.Text, txtemail.Text);
            if(dt.Rows.Count==0)
            {
                HttpContext.Current.Session["BaggageTrackFiles"] = null;
                string str = Request.Url.AbsoluteUri;
                string script = "alert('No record(s) found. '); window.location='" + str + "'";
                ScriptManager.RegisterStartupScript(this, GetType(),
                "ServerControlScript", script, true);
            }
            if (dt != null && dt.Rows.Count > 0)
            {
                List<HttpPostedFile> files = new List<HttpPostedFile>();
                if (Session["BaggageTrackFiles"] != null)
                {
                    try
                    {
                        string[] pirFiles;
                        string[] boardpassFiles;

                        files = Session["BaggageTrackFiles"] as List<HttpPostedFile>;
                        string UploadDirectory = Path.Combine(rootFolder, dt.Rows[0]["PAX_ID"].ToString());
                        if (!Directory.Exists(UploadDirectory))
                        {
                            Directory.CreateDirectory(UploadDirectory);
                        }
                        if (!Directory.Exists(Path.Combine(UploadDirectory, "PIR Copy(s)")))
                        {
                            Directory.CreateDirectory(Path.Combine(UploadDirectory, "PIR Copy(s)"));
                        }
                        if (!Directory.Exists(Path.Combine(UploadDirectory, "BoardingPass Copy(s)")))
                        {
                            Directory.CreateDirectory(Path.Combine(UploadDirectory, "BoardingPass Copy(s)"));
                        }

                        pirFiles = dzPIRFiles.Value.Split('|');
                        boardpassFiles = dzBoardPassFiles.Value.Split('|');

                        foreach (HttpPostedFile file in files)
                        {
                            if (pirFiles.Contains(file.FileName))
                            {
                                string filePath = Path.Combine(UploadDirectory + @"\PIR Copy(s)", file.FileName);
                                file.SaveAs(filePath);
                            }
                            if (boardpassFiles.Contains(file.FileName))
                            {
                                string filePath = Path.Combine(UploadDirectory + @"\BoardingPass Copy(s)", file.FileName);
                                file.SaveAs(filePath);
                            }
                        }
                        try
                        {
                            trackNumber = baggagePassengerDetails.UpdatePassengerSupportDocByPaxId(Convert.ToInt64(dt.Rows[0]["PAX_ID"].ToString()), true, true, Convert.ToInt32(ddlbags.SelectedValue));
                            if(!string.IsNullOrEmpty(trackNumber))
                            {
                                resltmsg.Style.Add("display", "block");
                                Utility.StartupScript(this.Page, "showpopup()", "showpopup");

                            }
                        }
                        catch (Exception ex)
                        {
                            Audit.Add(EventType.Exception, Severity.High, 1, "(BaggageTrackFiles)Failed to Update Baggage support docs. Reason: " + ex.ToString(), Request["REMOTE_ADDR"]);
                        }
                        Session["BaggageTrackFiles"] = null;
                        ClearFields();
                    }
                    catch (Exception ex)
                    {
                        Audit.Add(EventType.Exception, Severity.High, 1, "(BaggageTrackFiles)Failed to Upload BaggageClaimDoc Docs. Reason: " + ex.ToString(), Request["REMOTE_ADDR"]);
                    }
                    finally
                    {
                        Session["BaggageTrackFiles"] = null;
                    }
                }
            }
            else
            {
                HttpContext.Current.Session["BaggageTrackFiles"] = null;
            }

        }

        [ScriptMethod()]
        [WebMethod]
        public static bool getPassengerDetails(string paxPnr, string paxName, string mobeleNo, string email)
        {
            bool flag = false; 
            DataTable dt = getBaggageDetails(paxPnr, paxName, mobeleNo, email);
            if (dt.Rows.Count == 1)
            {
                flag = true;
            }
            return flag;
        }

        [ScriptMethod()]
        [WebMethod]
        public static void RemoveFile(string response)
        {
            List<HttpPostedFile> files = new List<HttpPostedFile>();
            if (HttpContext.Current.Session["BaggageTrackFiles"] != null)
            {
                files = HttpContext.Current.Session["BaggageTrackFiles"] as List<HttpPostedFile>;
                foreach (var file in files)
                {
                    if (file.FileName == response)
                    {
                        files.Remove(file);
                        break;
                    }

                }
                HttpContext.Current.Session["BaggageTrackFiles"] = files;
            }
        }

        private static DataTable getBaggageDetails(string paxPnr, string paxName, string mobeleNo, string email)
        {
            
                string passengerName = string.Empty;
                passengerName = Regex.Replace(paxName, @"\s+", "");
                BaggagePassengerDetails baggagePassengerDetails = new BaggagePassengerDetails();
                baggagePassengerDetails.PNRNumber = paxPnr;
                baggagePassengerDetails.PassengerName = passengerName;
                baggagePassengerDetails.PhoneNumber = mobeleNo;
                baggagePassengerDetails.EmailID = email;
                DataTable dt = baggagePassengerDetails.GetpassengerDetails();
                return dt;

        }

        private void ClearFields()
        {
            txtpnr.Text = string.Empty;
            txtpaxname.Text = string.Empty;
            txtmobileNo.Text = string.Empty;
            txtemail.Text = string.Empty;
            ddlbags.SelectedIndex = -1;
            dzPIRFiles.Value = string.Empty;
            dzBoardPassFiles.Value = string.Empty;
            HttpContext.Current.Session["BaggageTrackFiles"] = null;
        }
    }
}
