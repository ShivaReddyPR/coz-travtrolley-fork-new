﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="RateOfExchangeGUI" Title="Rate Of Exchange" Codebehind="RateOfExchange.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">
<script type="text/javascript">
    function ValidateParam() {
        document.getElementById('<%=lblSuccessMsg.ClientID %>').innerHTML = '';
        if (document.getElementById('<%=ddlCurrency.ClientID %>').selectedIndex <= 0) {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "Please select an Currency!";
            return false;
        }
    }
    function Check(id) {
        var val = document.getElementById(id).value;
        if (val == '0.000') {
            document.getElementById(id).value = '';
        }
    }
    function Set(id) {
        var val = document.getElementById(id).value;
        if (val == '' || val == '0.000') {
            document.getElementById(id).value = '0.000';
        }
    }
    function isNumber(evt) {
        evt = (evt) ? evt : window.event;
        var charCode = (evt.which) ? evt.which : evt.keyCode;
        if (charCode > 31 && (charCode < 46 || charCode > 57)) {
            return false;
        }
        return true;
    }
</script>
<table>
<tr><td><asp:Label runat="server" ID="lblSuccessMsg" CssClass="lblSuccess" ForeColor="Green"></asp:Label></td></tr>
</table>
 <div>

<div class="ns-h3">Update Rate Of Exchange</div>




<div> 

  <div class="paramcon bg_white bor_gray marbot_10 pad_10" title="Param" id="divParam">       
              
            
                 <div class="col-md-12 padding-0">                                      
    <div class="col-md-2"><asp:Label ID="lblBaseCurrency" runat="server" Text="Base Currency:" Font-Bold="true"></asp:Label> </div>
    
    <div class="col-md-2"> <asp:DropDownList ID="ddlCurrency"  CssClass="inputDdlEnabled form-control"  runat="server" OnSelectedIndexChanged="ddlCurrency_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>
    
    </div>
    
    
    <div class="col-md-2 marxs_top5"><asp:Button runat="server" ID="btnUpdate" Text="Update" CssClass="btn but_b" OnClick="btnUpdate_Click" OnClientClick="return ValidateParam();"/> </div>


    <div class="clearfix"></div>
    </div>
           
           
           <div>   <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
            </div></div>
             
             
               <div class="clearfix"> </div>
             </div>
             
             

</div>
    
    
    <div> 
    <div> <asp:Label runat="server" ID="lblErrorMsg" CssClass="lblError" ForeColor="Red"></asp:Label></div>
    <div style=" max-height:400px; overflow:auto" class="bg_white">
                 <table id="tblRateExchange" runat="server" width="100%"></table>
                            </div>
    
    
    </div>    
    </div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

