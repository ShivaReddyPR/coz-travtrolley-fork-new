﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.Core;
using CT.Corporate;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.Web.UI.Controls;
using Newtonsoft.Json;

namespace CozmoB2BWebApp
{
	public partial class CorpProfile : CT.Core.ParentPage
	{
		private string CORP_PROFILE_SESSION = "_CorporateProfileSession";
		private string CORP_PROFILE_SEARCH_SESSION = "_CorporateProfileSearchList";

		protected void Page_Load(object sender, EventArgs e)
		{
            try
            {
				this.Master.PageRole = true;
				if (Settings.LoginInfo != null)
                {
                    //Check if the user has access to this page (role) or not
                    if (Request.Url.AbsolutePath != "/ErrorPage.aspx" &&
                    Settings.roleFunctionList.Where(x => ("/" + x).ToUpper().Contains(Request.Url.AbsolutePath.ToUpper())).Count() == 0)
                        Response.Redirect("ErrorPage.aspx?sourcePage=Access Denied", false);
                    if (!IsPostBack)
                    {

                        ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                        DataTable dt = CorporateProfile.LoadControls(Settings.LoginInfo.AgentId);
                        LoadControls(dt);
                        if (Settings.LoginInfo.CorporateProfileId > 0)
                        {
                            Edit(Settings.LoginInfo.CorporateProfileId, Settings.LoginInfo.AgentId);
                        }
                        else
                        {
                            InitializePageControls();
                        }
                    }
                }
                else
                {
                    Response.Redirect("AbandonSession.aspx");
                }
            }
            catch (Exception ex)
            {
                Label lblMasterError = (Label)this.Master.FindControl("lblError");
                lblMasterError.Visible = true;
                lblMasterError.Text = ex.Message;
                Utility.WriteLog(ex, this.Title);
                Utility.Alert(this.Page, ex.Message);
            }

		}
		#region Methods
		private void InitializePageControls()
		{
			try
			{

				
				BindApproversList();

				BindAgent();
				BindLocation();
				BindSetupValues(ddlDesignation, "DG");
				BindSetupValues(ddlDivision, "DV");
				BindSetupValues(ddlCostCentre, "CS");
				BindSetupValues(ddlAddCostCenter, "CS");
				BindNationality();
				BindProfileType();
				BindGrade();
				BindCountry(ddlcountryOfIssue);
				BindCountry(ddlPlaceOfBirth);
				BindCountry(ddlVisaIssueCntry);
				BindCountry(ddlvisaplaceofissue);
				BindCountry(ddlResidency);
				BindDelegateSupervisors();
				Clear();

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		private void LoadControls(DataTable dt)
		{
			try
			{
				foreach(DataRow dr in dt.Rows)
				{
					#region General Info Controls
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Client")
						divClient.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Is Active")
						divActive.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Title")
						divTitle.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Surname/Last Name")
						DivSurname.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Given Name/First Name")
						divGivenName.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Middle Name")
						divMiddleName.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Batch#")
						DivBatch.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Designation")
						divDestignation.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Date Of Joining")
						divDateofJoining.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Employee ID")
						DivEmploid.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Division")
						divDivision.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Cost Centre")
						divCostCenter.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Telephone")
						divTelephone.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Mobile")
						divMobile.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "ZIP CODE")
						divZipCode.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Email")
						divEmail.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Grade")
						divGrade.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Profile Type")
						divProfileType.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Approval Type")
						divApproveType.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Generate Affidavit")
						divAffidavit.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Address")
						divAddress.Style.Add("display", "block");
					#endregion
					#region Personal Info Controls
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Nationality")
						divNationality.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Passport #")
						divPassport.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Date of Issue")
						divDateOfIssue.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Valid Till")
						divValidTill.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Country of Issue")
						divCountryOfIssue.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Residency")
						divResidency.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "ExecAssistance")
						divExcesAssistance.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Deligate Supervisor")
						divDeligateSP.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Date of Birth")
						divDateofBirth.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Place of Birth")
						divPlaceofBirth.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Gender")
						divGender.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Marital Status")
						divMartial.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "VISA DETAILS")
						divVisadetailsDisplay.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Visa Country")
						divVisaCountry.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Visa No.")
						divVisano.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Issue Date")
						divIssueDate.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Expiry Date")
						divExpiryDate.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Place of Issue")
						divPlaceOfissue.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "NATIONAL ID DETAILS")
						divNationalDeta.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "National ID Number")
						divNationalityId.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Expiry Date")
						divExpiryDateN.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "TAX DETAILS")
						divTaxDetailDisplay.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Number")
						divNumber.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Email")
						divEmailTax.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Name")
						divName.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Phone")
						divPhone.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Address")
						divAddressTax.Style.Add("display", "block");
					#endregion
					#region Travel Info controls
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Domestic Eligibility")
						divTravelDomestic.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "International Eligibility")
						divTravelInternal.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Seat Preference")
						divTravelSeat.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Meal Request")
						divTravelMeal.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "GDSProfilePNR")
						divTravelGds.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "GDS SETTINGS")
						DivGdsSettindsDisplay.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "GDS")
						DivTravelSettingsGds.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Description")
						divTravelDescription.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "OwnerPCC")
						divTravelOwnerPcc.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Queue No.")
						divTravelQueNO.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "extraCommand")
						divTravelExtra.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "corporateSSR")
						divTravelCorporateSsr.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "OSI")
						divTravelOsi.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Remarks")
						divTravelRemarks.Style.Add("display", "block");
					#endregion
					#region Hotel Info Controls
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Room Type")
						divHotelRoomtype.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "Remarks/Comments")
						divhotelRemarks.Style.Add("display", "block");
					#endregion
					#region Add Details Info Controls
					if (Convert.ToString(dr["cntl_name"]).Trim() == "COST CENTER")
						divAddCostCenter.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "EMPLOYEE ID")
						divAddEmpId.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "ERP COMPANY")
						divAddErpCompany.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "TRIP DESCRIPTION")
						divAddTrip.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "LOCATION")
						divAddLocation.Style.Add("display", "block");
					if (Convert.ToString(dr["cntl_name"]).Trim() == "APPROVER NAME")
						divAddAproveName.Style.Add("display", "block");
                    if (Convert.ToString(dr["cntl_name"]).Trim() == "DELIGATE SUPERVISOR")
                        divDeligateSP.Style.Add("display", "block");
                    #endregion
                }
				#region Serach Button display
				if (Settings.LoginInfo.MemberType==MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.TRAVELCORDINATOR)
					divbtnSearch.Style.Add("display", "block");
				#endregion

			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		private void BindDelegateSupervisors()
		{
			try
			{
				DataTable dt = CorporateProfile.GetCorpProfilesList(Settings.LoginInfo.AgentId);
                DataView dv = dt.DefaultView;
                dv.RowFilter = "ProfileId not in (" + Settings.LoginInfo.CorporateProfileId + ")";
                ddlDeligateSupervisor.DataSource = dt;
				ddlDeligateSupervisor.DataTextField = "Name";
				ddlDeligateSupervisor.DataValueField = "ProfileId";
				ddlDeligateSupervisor.DataBind();
				ddlDeligateSupervisor.Items.Insert(0, new ListItem("--Select Supervisor --", "0"));
			}
			catch { throw; }
		}
		private void BindLocation()
		{
			try
			{
                ddlLocation.ClearSelection();
                ddlLocation.Items.Clear();
                ddlLocation.Items.Insert(0, new ListItem("--Select Location --", "0"));
                ddlLocation.SelectedValue = "0";
                ddlLocation.SelectedIndex = 0;
                int agentId =Utility.ToInteger(ddlAgent.SelectedIndex) > 0 ? Utility.ToInteger(ddlAgent.SelectedItem.Value) : Settings.LoginInfo.AgentId;
				DataTable dt = LocationMaster.GetList(agentId, ListStatus.Short, RecordStatus.Activated, string.Empty);
				ddlLocation.DataSource = dt;
				ddlLocation.DataTextField = "Location_name";
				ddlLocation.DataValueField = "location_id";
				ddlLocation.DataBind();				
				ddlLocation.SelectedValue = Convert.ToString(Settings.LoginInfo.LocationID);

			}
			catch(Exception ex) { throw ex; }
		}
		private void BindCountry(DropDownList ddlCountry)
		{
			try
			{
				SortedList Countries = CT.Core.Country.GetCountryList();
				ddlCountry.DataSource = Countries;
				ddlCountry.DataTextField = "key";
				ddlCountry.DataValueField = "value";
				ddlCountry.DataBind();
				ddlCountry.Items.Insert(0, new ListItem("Select", "-1"));
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}
		private void BindNationality()
		{
			try
			{
				int agentId = Utility.ToInteger(ddlAgent.SelectedIndex) > 0 ? Utility.ToInteger(ddlAgent.SelectedItem.Value) : Settings.LoginInfo.AgentId;
				ddlNationality.DataSource = CT.Core.Country.GetNationalityList();
				ddlNationality.DataTextField = "Key";
				ddlNationality.DataValueField = "Value";
				ddlNationality.DataBind();
				ddlNationality.Items.Insert(0, new ListItem("--Select Nationality --", "0"));

			}
			catch { throw; }
		}
		private void BindSearch()
		{
			try
			{
				int agentid = Utility.ToInteger(ddlAgent.SelectedIndex) > 0 ? Utility.ToInteger(ddlAgent.SelectedItem.Value) : Settings.LoginInfo.AgentId;
				if (!string.IsNullOrEmpty(txtgSurname.Text) || (!string.IsNullOrEmpty(txtGname.Text)) || (!string.IsNullOrEmpty(txtGEmpid.Text)) || (!string.IsNullOrEmpty(txtGEmail.Text)))
				{
					DataTable data = CorporateProfile.GetList(agentid, 0, ListStatus.Long, txtgSurname.Text, txtGname.Text, txtGEmpid.Text, txtGEmail.Text);
					gvSearch.DataSource = data;
					gvSearch.DataBind();
					CleargridSearch();
				}
				else
				{

					DataTable dt = CorporateProfile.GetList(agentid, 0, ListStatus.Long);
					SearchList = dt;
					CommonGrid g = new CommonGrid();
					g.BindGrid(gvSearch, dt);
				}

			}
			catch { throw; }
		}
		private void CleargridSearch()
		{
			try
			{
				txtgSurname.Text = string.Empty;
				txtGname.Text = string.Empty;
				txtGEmail.Text = string.Empty;
				txtGEmpid.Text = string.Empty;

			}
			catch { throw; }
		}
		private DataTable SearchList
		{
			get
			{
				return (DataTable)Session[CORP_PROFILE_SEARCH_SESSION];
			}
			set
			{
				value.PrimaryKey = new DataColumn[] { value.Columns["ProfileId"] };
				Session[CORP_PROFILE_SEARCH_SESSION] = value;
			}
		}

		private void BindApproversList()
		{

			try
			{
				//M-Manager Type Profile
				DataTable dtApprovers = CorporateProfile.GetProfilesListByGrade("A", "M", Settings.LoginInfo.AgentId);
				ddlAppName.DataSource = dtApprovers;
				ddlAppName.DataTextField = "Name";
				ddlAppName.DataValueField = "ProfileId";
				ddlAppName.DataBind();
				ddlAppName.Items.Insert(0, new ListItem("--Select ApproveType --", "0"));
				hdnApprovers.Value = JsonConvert.SerializeObject(dtApprovers);
			}
			catch { throw; }
		}
		private void BindAgent()
		{
			try
			{
				DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, string.Empty, Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);
				ddlAgent.DataSource = dtAgents;
				ddlAgent.DataTextField = "Agent_Name";
				ddlAgent.DataValueField = "agent_id";
				ddlAgent.DataBind();
				ddlAgent.Items.Insert(0, new ListItem("--Select Client--", "0"));
				ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
				if (Settings.LoginInfo.MemberType == MemberType.ADMIN || Settings.LoginInfo.MemberType == MemberType.SUPER)
				{
					ddlAgent.Enabled = true;
				}
				else
				{
					ddlAgent.Enabled = false;
				}
			}
			catch (Exception ex)
			{
				CT.Core.Audit.Add(EventType.Exception, Severity.High, 1, "Corporate Profile page " + ex.Message, "0");
			}
		}
		private void BindProfileType()
		{
			try
			{
				DataSet ds = UserMaster.GetMemberTypeList("PROFILE_TYPE");
				ddlProfileType.DataSource = ds;
				ddlProfileType.DataTextField = "FIELD_TEXT";
				ddlProfileType.DataValueField = "FIELD_VALUE";
				ddlProfileType.DataBind();
				ddlProfileType.Items.Insert(0, new ListItem("--Select Profile Type --", "0"));

			}
			catch { }
		}
		private void BindGrade()
		{
			try
			{
				int agentId = 0; //Utility.ToInteger(ddlAgent.SelectedItem.Value);
				if (ddlAgent.SelectedIndex > 0)
				agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
				else
				agentId = Settings.LoginInfo.AgentId;
				
				DataTable dt = CorporateProfileSetup.GetList(agentId, "GD", ListStatus.Short);
				ddlGrade.DataSource = dt;
				ddlGrade.DataTextField = "Name";
				ddlGrade.DataValueField = "Code";
				ddlGrade.DataBind();
				ddlGrade.Items.Insert(0, new ListItem("--Select Profile Grade --", "0"));

			}
			catch { throw; }
		}
		private void BindCostCenters()
		{
			try
			{
				int agentId = 0; //Utility.ToInteger(ddlAgent.SelectedItem.Value);
				if (ddlAgent.SelectedIndex > 0)
					agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
				else
					agentId = Settings.LoginInfo.AgentId;
				DataTable dt = CorporateProfileSetup.GetList(agentId, "CS", ListStatus.Short);
				foreach (DataRow dr in dt.Rows)
				{
					chkCostCenters.Items.Add(new ListItem() { Value = dr["SetupId"].ToString(), Text = dr["Name"].ToString() });
				}
			}
			catch { throw; }
		}
		private void BindSetupValues(DropDownList ddl, string type) //Designations,Divisions,CostCenter
		{
			try
			{
				int agentId = 0; //Utility.ToInteger(ddlAgent.SelectedItem.Value);
				if (ddlAgent.SelectedIndex > 0)
					agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
				else
					agentId = Settings.LoginInfo.AgentId;
				DataTable dt = CorporateProfileSetup.GetList(agentId, type, ListStatus.Short);
				ddl.DataSource = dt;
				ddl.DataTextField = "Name";
				ddl.DataValueField = "SetupId";
				ddl.DataBind();
				string textDefault = string.Empty;
				if (type == "DG") textDefault = "--Select Designation --";
				else if (type == "DV") textDefault = "--Select Division --";
				else if (type == "CS") textDefault = "--Select Cost Centre --";

				ddl.Items.Insert(0, new ListItem(textDefault, "0"));

			}
			catch { throw; }
		}
		private void Update()
		{
			try
			{
				CorporateProfile corpProfile;
				if (CurrentObject == null)
				{
					corpProfile = new CorporateProfile();

				}
				else
				{
					corpProfile = CurrentObject;
				}

				corpProfile.AgentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
				corpProfile.ProfileType = Utility.ToString(ddlProfileType.SelectedItem.Value);
				corpProfile.Title = Utility.ToString(ddlTitle.SelectedItem.Value);//TODO
				corpProfile.SurName = Utility.ToString(txtSurname.Text.Trim());
				corpProfile.Name = Utility.ToString(txtname.Text.Trim());
				corpProfile.Designation = Utility.ToString(ddlDesignation.SelectedItem.Value);

				string strProfileImagePath = Session["profileImagePath"] as string;
				if (!string.IsNullOrEmpty(strProfileImagePath))
				{
					string[] fileExtension = strProfileImagePath.Split('.');

					corpProfile.ImagePath = strProfileImagePath.Substring(strProfileImagePath.Substring(0, strProfileImagePath.LastIndexOf("\\") + 1).Length, strProfileImagePath.Length - (strProfileImagePath.Substring(0, strProfileImagePath.LastIndexOf("\\") + 1).Length));

					corpProfile.ImageType = "." + fileExtension[1];
				}	
				corpProfile.EmployeeId = Utility.ToString(txtEmpId.Text.Trim());
				corpProfile.Division = Utility.ToInteger(ddlDivision.SelectedItem.Value);
				corpProfile.CostCentre = Utility.ToInteger(ddlCostCentre.SelectedItem.Value);
				corpProfile.Grade = Utility.ToString(ddlGrade.SelectedItem.Value);
				corpProfile.Telephone = txtPhoneCountryCode.Text.Trim() + (string.IsNullOrEmpty(txtTelPhone.Text.Trim()) ? string.Empty : "-" + txtTelPhone.Text.Trim());
				corpProfile.Mobilephone = txtMobileCoutryCode.Text.Trim() + (string.IsNullOrEmpty(txtMobileNo.Text.Trim()) ? string.Empty : "-" + txtMobileNo.Text.Trim());
				corpProfile.Fax = Utility.ToString(txtFax.Text.Trim());
				corpProfile.Email = Utility.ToString(txtEmail.Text.Trim());
				corpProfile.Address1 = Utility.ToString(txtAddress2.Text.Trim());
				corpProfile.NationalityCode = Utility.ToString(ddlNationality.SelectedItem.Value);
				corpProfile.PassportNo = Utility.ToString(txtPassportNo.Text.Trim());

				IFormatProvider dateFormat1 = new System.Globalization.CultureInfo("en-GB");
				corpProfile.DateOfIssue = Convert.ToDateTime(txtDOI.Text, dateFormat1);
				corpProfile.DateOfExpiry = Convert.ToDateTime(txtDOE.Text, dateFormat1);
				corpProfile.DateOfBirth = Convert.ToDateTime(txtDOB.Text, dateFormat1);

				corpProfile.PassportCOI = Utility.ToString(ddlcountryOfIssue.SelectedItem.Value);
				corpProfile.PlaceOfIssue = Utility.ToString(ddlcountryOfIssue.SelectedItem.Value);
			
				corpProfile.PlaceOfBirth = Utility.ToString(ddlPlaceOfBirth.SelectedItem.Value);
				corpProfile.Gender = Utility.ToString(ddlGender.SelectedItem.Value); //rdbMale.Checked ? "M" : "F";           
				corpProfile.SeatPreference = Utility.ToString(txtSeatPref.Text.Trim());
				corpProfile.MealRequest = Utility.ToString(txtMealRequest.Text.Trim());
				corpProfile.HotelRemarks = Utility.ToString(txtRemarks.Text.Trim());
                corpProfile.OtherRemarks = string.Empty;
				corpProfile.Status = Settings.ACTIVE;
				corpProfile.CreatedBy = Utility.ToInteger(Settings.LoginInfo.UserID);
				
				if (ddlLocation.SelectedIndex > 0)
					corpProfile.LocationId = Utility.ToInteger(ddlLocation.SelectedItem.Value);
				

				if (ddlProfileType.SelectedIndex > 0)
					corpProfile.MemberType = ddlProfileType.SelectedValue == MemberType.SUPERVISOR.ToString() ? MemberType.SUPERVISOR : MemberType.TRAVELCORDINATOR;
				
				corpProfile.MartialStatus = Utility.ToString(ddlMartialStatus.SelectedItem.Value);
				
				corpProfile.MiddleName = Utility.ToString(txtMiddleName.Text.Trim());
				corpProfile.GstNumber = Utility.ToString(txtGSTNumber.Text.Trim());
				corpProfile.GstName = Utility.ToString(txtGSTName.Text.Trim());
				corpProfile.GstAddress = Utility.ToString(txtGSTAddress.Text.Trim());
				corpProfile.GstEmail = Utility.ToString(txtGSTEmail.Text.Trim());
				corpProfile.GstPhone = Utility.ToString(txtGSTPhone.Text.Trim());
				corpProfile.ExecAssistance = Utility.ToString(txtExecAssistance.Text.Trim());
				corpProfile.DeligateSupervisor = Convert.ToInt64(ddlDeligateSupervisor.SelectedValue);
				corpProfile.GdsProfilePNR = Utility.ToString(txtGDSProfilePNR.Text.Trim());
				corpProfile.DomesticEligibility = Utility.ToBoolean(ddlDomestic.SelectedValue);
				corpProfile.IntlEligibility = Utility.ToBoolean(ddlinternational.SelectedValue);
				corpProfile.Batch = Utility.ToString(txtEmpId.Text.Trim());
				corpProfile.ApproverType = Utility.ToString(ddlApproverType.SelectedValue);
				corpProfile.Affidivit = Utility.ToBoolean(chkAffidavit.Checked);

				//Bookers Cost Centers
				List<CorpCostCenter> corpCostCenters = corpProfile.CorpCostCenters;
				corpCostCenters = corpCostCenters == null ? new List<CorpCostCenter>() : corpCostCenters;
				if (chkCostCenters.Items != null && chkCostCenters.Items.Count > 0)
				{
					foreach (ListItem li in chkCostCenters.Items)
					{
						CorpCostCenter corpCostCenter = null;
						bool isExistsCostCenter = corpCostCenters.Exists(x => x.costcenterid == Convert.ToInt32(li.Value));
						if (li.Selected || isExistsCostCenter)
						{
							corpCostCenter = corpCostCenters.Find(x => x.costcenterid == Convert.ToInt32(li.Value));
							if (corpCostCenter == null)
							{
								corpCostCenter = new CorpCostCenter();
							}
							corpCostCenter.costcenterid = Convert.ToInt32(li.Value);
							corpCostCenter.profileid = corpProfile.ProfileId;
							corpCostCenter.productid = (int)CT.BookingEngine.ProductType.Flight;
							corpCostCenter.createdby = (int)Settings.LoginInfo.UserID;
							corpCostCenter.createdon = DateTime.Now;
							corpCostCenter.status = li.Selected ? true : false;
							if (!isExistsCostCenter)
								corpCostCenters.Add(corpCostCenter);
						}
					}
				}
				corpProfile.CorpCostCenters = corpCostCenters;

				DataSet ds = new DataSet();
				DataTable ProDetails = corpProfile.DtProfileDetails;
				DataColumn[] keyColumns = new DataColumn[1];
				keyColumns[0] = ProDetails.Columns["DetailId"];
				ProDetails.PrimaryKey = keyColumns;

				DataTable profileFlex = corpProfile.DtProfileFlex;

				string[] splitter = { "&&" };
				string[] spliterValue = { "||" };
				string[] spliterText = { "--" };
				//int recStatus = 0;

				//RoomType
				if (Convert.ToInt32(hdfRTDetailId.Value) > 0)
				{
					long serial = Convert.ToInt32(hdfRTDetailId.Value);
					DataRow dr = ProDetails.Rows.Find(serial);
					dr.BeginEdit();
					dr["DisplayValue"] = ddlRoomType.SelectedItem.Value;
					dr["DisplayText"] = ddlRoomType.SelectedItem.Text;
					dr["Value"] = txtRemarks.Text.Trim();
					dr.EndEdit();
				}
				else
				{
					long serial = Utility.ToLong(ProDetails.Compute("MAX(DetailId)", ""));
					serial = serial + 1;
					ProDetails.Rows.Add(serial, -1, "RT", ddlRoomType.SelectedItem.Value, ddlRoomType.SelectedItem.Text, txtRemarks.Text.Trim(), null, "A", Settings.LoginInfo.AgentId);
				}

				corpProfile.DtProfileDetails = ProDetails;
				
				corpProfile.DtProfileFlex = profileFlex;
				corpProfile.Status = chkIsActive.Checked ? Settings.ACTIVE : Settings.DELETED;
				GetApproversList(corpProfile);
				corpProfile.DateOfJoining = Convert.ToDateTime(dcJoiningDate.Value, dateFormat1);
				GetVisaDetailsList(corpProfile);
				
				GetGDSSettingsList(corpProfile);
				corpProfile.Save();
				string profileId = Utility.ToString(corpProfile.ProfileIdRet);
				Session["CorpProfileId"] = profileId;
				// if (!string.IsNullOrEmpty(profileImage.FileExtension))
				
				string name = txtSurname.Text.Trim() + " " + txtname.Text.Trim();
				string modeVal = hdfMode.Value;
				Clear();
				string message = string.Empty;
				if (modeVal == "0")
					message = "Corporate Profile for " + name + " is created !";
				else if (modeVal == "1")
					message = "Corporate Profile for " + name + " is updated !";

				Utility.Alert(this.Page, message);
			}
			catch
			{
				throw;
			}

		}
		private void Edit(long id, int agentId)
		{
			try
			{
				IFormatProvider dateFormat = new System.Globalization.CultureInfo("en-GB");
				CorporateProfile editCorp = new CorporateProfile(id, agentId);
				CurrentObject = editCorp;
				DataTable dtDetails = editCorp.DtProfileDetails;
				DataTable dtFlex = editCorp.DtProfileFlex;
				DataTable dtPolicies = editCorp.DtProfilePolicy;
				hdfMode.Value = "1";  //edit mode
				ddlAgent.SelectedValue = Utility.ToString(agentId);

				BindLocation();
				BindGrade();
				BindSetupValues(ddlDesignation, "DG");
				BindSetupValues(ddlDivision, "DV");
				BindSetupValues(ddlCostCentre, "CS");
				BindSetupValues(ddlAddCostCenter, "CS");
				BindCountry(ddlcountryOfIssue);
				BindCountry(ddlPlaceOfBirth);
				BindCountry(ddlVisaIssueCntry);
				BindApproversList();
				BindProfileType();
				BindAgent();
				BindDelegateSupervisors();
				BindCountry(ddlResidency);
				if(!string.IsNullOrEmpty(editCorp.ProfileType))
				SetddlValue(ddlProfileType, editCorp.ProfileType);
				if (!string.IsNullOrEmpty(editCorp.Title))
					SetddlValue(ddlTitle, editCorp.Title); 
				txtSurname.Text = Utility.ToString(editCorp.SurName);
				txtname.Text = Utility.ToString(editCorp.Name);
				if(!string.IsNullOrEmpty(editCorp.Designation))
				SetddlValue(ddlDesignation, editCorp.Designation); 
				txtEmpId.Text = Utility.ToString(editCorp.EmployeeId);
				txtAddEmpId.Text= Utility.ToString(editCorp.EmployeeId);
				if(editCorp.Division>0)
				SetddlValue(ddlDivision, Convert.ToString(editCorp.Division));
				if(editCorp.CostCentre>0)
				SetddlValue(ddlCostCentre, Convert.ToString(editCorp.CostCentre)); 
				if(!String.IsNullOrEmpty(editCorp.Grade))
				SetddlValue(ddlGrade, editCorp.Grade); 
				if(editCorp.CostCentre>0)
				SetddlValue(ddlAddCostCenter, Convert.ToString(editCorp.CostCentre));
				
				string[] splitter = { "-" };
				if (!string.IsNullOrEmpty(editCorp.Telephone))
				{
					string[] telephone = editCorp.Telephone.Split(splitter, 2, StringSplitOptions.RemoveEmptyEntries);
					if (telephone.Length == 2)
					{
						txtTelPhone.Text = telephone[1];
						txtPhoneCountryCode.Text = telephone[0];
					}
					else txtTelPhone.Text = editCorp.Telephone;
				}

				if (!string.IsNullOrEmpty(editCorp.Mobilephone))
				{
					string[] mobilePhone = editCorp.Mobilephone.Split(splitter, 2, StringSplitOptions.RemoveEmptyEntries);
					if (mobilePhone.Length == 2)
					{
						txtMobileNo.Text = mobilePhone[1];
						txtMobileCoutryCode.Text = mobilePhone[0];
					}
					else txtMobileNo.Text = editCorp.Mobilephone;
				}
				
				txtFax.Text = Utility.ToString(editCorp.Fax);
				txtEmail.Text = Utility.ToString(editCorp.Email);
				txtAddress2.Text = Utility.ToString(editCorp.Address1);
				BindNationality();
				if(!String.IsNullOrEmpty(editCorp.NationalityCode))
				SetddlValue(ddlNationality, editCorp.NationalityCode); 
				txtPassportNo.Text = Utility.ToString(editCorp.PassportNo);
				txtDOI.Text = editCorp.DateOfIssue.ToString("dd/MM/yyyy");
				try
				{
					txtDOE.Text = editCorp.DateOfExpiry.ToString("dd/MM/yyyy");  
				}
				catch { }
				try { ddlcountryOfIssue.SelectedValue = Utility.ToString(editCorp.PassportCOI); } catch { }
				txtDOB.Text = editCorp.DateOfBirth.ToString("dd/MM/yyyy"); 
				if(!string.IsNullOrEmpty(editCorp.PlaceOfBirth))
				SetddlValue(ddlPlaceOfBirth, editCorp.PlaceOfBirth);
				//rdbMale.Checked = true;
				if (!string.IsNullOrEmpty(editCorp.Gender))
				{
					ddlGender.SelectedValue = Utility.ToString(editCorp.Gender);
				}
				else
				{
					ddlGender.SelectedValue = string.Empty;
				}
				txtSeatPref.Text = Utility.ToString(editCorp.SeatPreference);
				txtMealRequest.Text = Utility.ToString(editCorp.MealRequest);
				txtRemarks.Text = Utility.ToString(editCorp.HotelRemarks);
				string displayText = string.Empty;
				string displayValue = string.Empty;
				if(editCorp.LocationId > 0)
				SetddlValue(ddlLocation, editCorp.LocationId > 0 ? Utility.ToString(editCorp.LocationId) : ddlLocation.SelectedValue); 
				chkIsActive.Checked = editCorp.Status == "A";
				dcJoiningDate.Value = editCorp.DateOfJoining;

				
				ddlMartialStatus.SelectedValue = Utility.ToString(editCorp.MartialStatus);
				
				txtMiddleName.Text = Utility.ToString(editCorp.MiddleName);
				txtGSTNumber.Text = Utility.ToString(editCorp.GstNumber);
				txtGSTName.Text = Utility.ToString(editCorp.GstName);
				txtGSTEmail.Text = Utility.ToString(editCorp.GstEmail);
				txtGSTPhone.Text = Utility.ToString(editCorp.GstPhone);
				txtGSTAddress.Text = Utility.ToString(editCorp.GstAddress);
				txtGDSProfilePNR.Text = Utility.ToString(editCorp.GdsProfilePNR);
				if(!string.IsNullOrEmpty(editCorp.Residency))
				SetddlValue(ddlResidency, Utility.ToString(editCorp.Residency)); 
				txtExecAssistance.Text = Utility.ToString(editCorp.ExecAssistance);
				if(editCorp.DeligateSupervisor>0)
				SetddlValue(ddlDeligateSupervisor, Utility.ToString(editCorp.DeligateSupervisor));
				SetddlValue(ddlDomestic, Utility.ToString(editCorp.DomesticEligibility));  
				SetddlValue(ddlinternational, Utility.ToString(editCorp.IntlEligibility)); 
				txtbatch.Text = Utility.ToString(editCorp.EmployeeId);
				chkAffidavit.Checked = editCorp.Affidivit;
				if(!string.IsNullOrEmpty(editCorp.ApproverType))
				ddlApproverType.SelectedValue = Utility.ToString(editCorp.ApproverType);
				if (editCorp.CorpCostCenters != null && editCorp.CorpCostCenters.Count > 0)
				{
					if (chkCostCenters.Items != null && chkCostCenters.Items.Count > 0)
					{
						foreach (ListItem li in chkCostCenters.Items)
						{
							if (editCorp.CorpCostCenters.Exists(x => x.costcenterid == Convert.ToInt32(li.Value)))
								li.Selected = true;
						}
					}
				}
				
				if (editCorp.ProfileVisaDetailsList != null && editCorp.ProfileVisaDetailsList.Count > 0)
				{
					if (editCorp.ProfileVisaDetailsList.Find(w => w.Type == "E") != null)
					{
						txtNationalIDNo.Text = editCorp.ProfileVisaDetailsList.Find(w => w.Type == "E").VisaNumber;
						dcNationalIDExpDate.Value = editCorp.ProfileVisaDetailsList.Find(w => w.Type == "E").VisaExpDate;
					}

				}
				

				//Approvers List
				hdnApproversList.Value = string.Empty;
				if (editCorp.ProfileApproversList != null && editCorp.ProfileApproversList.Count > 0)
				{
					hdnApproversList.Value = JsonConvert.SerializeObject(editCorp.ProfileApproversList);
					ddlAppName.SelectedValue = Utility.ToString(editCorp.ProfileApproversList[0].ApproverId);
				}
				
				
				hdnVisaDetails.Value = string.Empty;
				if (editCorp.ProfileVisaDetailsList != null && editCorp.ProfileVisaDetailsList.Count > 0)
				{
					// .FindAll(w=>w.Type!="E") condition Added for Getting VisaDetails only -- Somasekhar on 30/03/2018
					foreach (CorpProfileVisaDetails pad in editCorp.ProfileVisaDetailsList.FindAll(w => w.Type != "E"))
					{
						//Main :Row Information Capturing.
						//Record Id (^)
						//Visa Country#
						//Visa No#
						//Issue Date#
						//Expiry Date#
						//Type
						string rowInfoTA = string.Empty;
						rowInfoTA = Convert.ToString(pad.VisaId);
						rowInfoTA += "^";
						rowInfoTA += Convert.ToString(pad.VisaCountry) + "#";
						rowInfoTA += Convert.ToString(pad.VisaNumber) + "#";
						rowInfoTA += pad.VisaIssueDate.ToString("dd/MM/yyyy").Replace('/', '-') + "#";
						rowInfoTA += pad.VisaExpDate.ToString("dd/MM/yyyy").Replace('/', '-') + "#";
						rowInfoTA += Convert.ToString(pad.Visaplaceofissue) + "#";
						rowInfoTA += "VD"; //VISA DETAILS
						if (string.IsNullOrEmpty(hdnVisaDetails.Value))
						{
							hdnVisaDetails.Value = rowInfoTA;
						}
						else
						{
							hdnVisaDetails.Value += "|" + rowInfoTA;
						}
					}
				}
				
				if (editCorp.ProfileGDSSettings != null && editCorp.ProfileGDSSettings.Count > 0)
				{
					foreach (CropProfileGDSSettings pad in editCorp.ProfileGDSSettings)
					{
						txtGDS.Text = Convert.ToString(pad.GDS);
						txtGDScorporateSSR.Text = Convert.ToString(pad.CorporateSSR);
						txtGDSDescription.Text = Convert.ToString(pad.Description);
						txtGDSextraCommand.Text = Convert.ToString(pad.ExtraCommand);
						txtGDSOSI.Text = Convert.ToString(pad.OSI);
						txtGDSOwnerPCC.Text = Convert.ToString(pad.OwnerPCC);
						txtGDSQueueNo.Text = Convert.ToString(pad.QueueNo);
						txtGDSRemraks.Text = Convert.ToString(pad.Remraks);
					}
				}
			}
			catch (Exception ex)
			{ throw ex; }
		}
		public void SetddlValue(DropDownList ddl, string val)
		{
			ddl.SelectedValue = ddl.Items.FindByValue(val) != null ? val : ddl.SelectedValue;
		}
		private CorporateProfile CurrentObject
		{
			get
			{
				return (CorporateProfile)Session[CORP_PROFILE_SESSION];
			}
			set
			{
				if (value == null)
				{
					Session.Remove(CORP_PROFILE_SESSION);
				}
				else
				{
					Session[CORP_PROFILE_SESSION] = value;
				}

			}

		}
		private void GetVisaDetailsList(CorporateProfile cp)
		{

			try
			{
				IFormatProvider dateFormat1 = new System.Globalization.CultureInfo("en-GB");
				 if (hdfMode.Value == "1")//Edit mode
				{
					List<CorpProfileVisaDetails> existingProfileVisaDetails = null;
					List<CorpProfileVisaDetails> newProfileVisaDetails = new List<CorpProfileVisaDetails>(); //This is for new visa  details records insertion
																											 // editCorp.ProfileVisaDetailsList.Find(w => w.Type == "E").VisaNumber


					if (cp.ProfileVisaDetailsList != null && cp.ProfileVisaDetailsList.Count > 0)
					{
						existingProfileVisaDetails = cp.ProfileVisaDetailsList;
					}
					if (existingProfileVisaDetails != null && existingProfileVisaDetails.Count > 0)
					{
						if (!string.IsNullOrEmpty(hdnDelVD.Value))
						{
							string[] delApprovers = hdnDelVD.Value.Split('|');
							if (delApprovers.Length > 0)
							{
								foreach (string delApp in delApprovers)
								{
									foreach (CorpProfileVisaDetails epa in existingProfileVisaDetails)
									{
										int id;
										int.TryParse(delApp.Split('^')[0], out id);
										if (id == epa.VisaId)
										{
											epa.CreatedBy = (int)Settings.LoginInfo.UserID;
											epa.Status = "D";
										}
									}
								}
							}
						}

						#region Visa Details
						if (!string.IsNullOrEmpty(hdnVisaDetails.Value) && hdnVisaDetails.Value.Length > 0)
						{
							string[] selTicketApprovers = hdnVisaDetails.Value.Split('|');
							if (selTicketApprovers.Length > 0)
							{
								foreach (string selTA in selTicketApprovers)
								{
									int id;
									int.TryParse(selTA.Split('^')[0], out id);
									if (id < 0)
									{

										CorpProfileVisaDetails pa = new CorpProfileVisaDetails();
										pa.VisaId = id;
										pa.VisaCountry = Convert.ToString(selTA.Split('^')[1].Split('#')[0]);
										pa.VisaNumber = Convert.ToString(selTA.Split('^')[1].Split('#')[1]);
										pa.VisaIssueDate = Convert.ToDateTime(selTA.Split('^')[1].Split('#')[2], dateFormat1);
										pa.VisaExpDate = Convert.ToDateTime(selTA.Split('^')[1].Split('#')[3], dateFormat1);
										pa.Visaplaceofissue = Convert.ToString(selTA.Split('^')[1].Split('#')[4]);
										pa.Status = "A";
										pa.Type = "V";
										pa.CreatedBy = (int)Settings.LoginInfo.UserID;

										newProfileVisaDetails.Add(pa);
									}
									else
									{
										foreach (CorpProfileVisaDetails ped in existingProfileVisaDetails)
										{
											if (ped.VisaId == id)
											{
												ped.VisaId = id;
												ped.VisaCountry = Convert.ToString(selTA.Split('^')[1].Split('#')[0]);
												ped.VisaNumber = Convert.ToString(selTA.Split('^')[1].Split('#')[1]);
												ped.VisaIssueDate = Convert.ToDateTime(selTA.Split('^')[1].Split('#')[2], dateFormat1);
												ped.VisaExpDate = Convert.ToDateTime(selTA.Split('^')[1].Split('#')[3], dateFormat1);
												ped.Visaplaceofissue = Convert.ToString(selTA.Split('^')[1].Split('#')[4]);
												ped.Status = "A";
												ped.Type = "V";
												ped.CreatedBy = (int)Settings.LoginInfo.UserID;
											}
										}

									}

								}
							}
						}
						#endregion

						#region  Nationality Details
						//For newly Inserted Nationality Details in Edit Mode -- Add by somasekhar on 31/03/2018 

						if (cp.ProfileVisaDetailsList != null && cp.ProfileVisaDetailsList.FindAll(w => w.Type == "E").Count > 0)
						{
							CorpProfileVisaDetails pa = new CorpProfileVisaDetails();

							//int.TryParse(app.Split('^')[0], out id); //returns the id.
							pa.VisaId = cp.ProfileVisaDetailsList.Find(w => w.Type == "E").VisaId;
							// pa.VisaCountry = "";
							pa.VisaNumber = txtNationalIDNo.Text;
							// pa.VisaIssueDate = Convert.ToDateTime( DBNull.Value);
							pa.VisaExpDate = Convert.ToDateTime(dcNationalIDExpDate.Value, dateFormat1);
							pa.Status = "A";
							pa.Type = "E";
							pa.CreatedBy = (int)Settings.LoginInfo.UserID;
							cp.ProfileVisaDetailsList.Add(pa);

							existingProfileVisaDetails = cp.ProfileVisaDetailsList;
						}
						else
						{
							if (!string.IsNullOrEmpty(txtNationalIDNo.Text))
							{

								CorpProfileVisaDetails pa = new CorpProfileVisaDetails();
								//int id;
								//int.TryParse(app.Split('^')[0], out id); //returns the id.
								pa.VisaId = -1;
								// pa.VisaCountry = "";
								pa.VisaNumber = txtNationalIDNo.Text;
								// pa.VisaIssueDate = Convert.ToDateTime( DBNull.Value);
								pa.VisaExpDate = Convert.ToDateTime(dcNationalIDExpDate.Value, dateFormat1);
								pa.Status = "A";
								pa.Type = "E";
								pa.CreatedBy = (int)Settings.LoginInfo.UserID;
								newProfileVisaDetails.Add(pa);
							}

						}
						#endregion
					}
					else// While retrieving if there are no profile approvers this else block will be executed
					{

						//Ticket Approvers
						if (!string.IsNullOrEmpty(hdnVisaDetails.Value) && hdnVisaDetails.Value.Length > 0)
						{
							//Main :Row Information Capturing.
							//Record Id (^)
							//Visa Country#
							//Visa No#
							//Issue Date#
							//Expiry Date#
							//Type

							string[] selTicketApprovers = hdnVisaDetails.Value.Split('|');
							if (selTicketApprovers.Length > 0)
							{
								foreach (string app in selTicketApprovers)
								{
									CorpProfileVisaDetails pa = new CorpProfileVisaDetails();
									int id;
									int.TryParse(app.Split('^')[0], out id); //returns the id.
									pa.VisaId = id;
									pa.VisaCountry = Convert.ToString(app.Split('^')[1].Split('#')[0]);
									pa.VisaNumber = Convert.ToString(app.Split('^')[1].Split('#')[1]);
									pa.VisaIssueDate = Convert.ToDateTime(app.Split('^')[1].Split('#')[2], dateFormat1);
									pa.VisaExpDate = Convert.ToDateTime(app.Split('^')[1].Split('#')[3], dateFormat1);
									pa.Visaplaceofissue = Convert.ToString(app.Split('^')[1].Split('#')[4]);
									pa.Status = "A";
									pa.Type = "V";
									pa.CreatedBy = (int)Settings.LoginInfo.UserID;
									newProfileVisaDetails.Add(pa);
								}
							}
						}
					}



					if (existingProfileVisaDetails != null && existingProfileVisaDetails.Count > 0)
					{
						if (newProfileVisaDetails != null && newProfileVisaDetails.Count > 0)
						{
							existingProfileVisaDetails.AddRange(newProfileVisaDetails);
						}
						cp.ProfileVisaDetailsList = existingProfileVisaDetails;
					}
					else
					{
						if (newProfileVisaDetails != null && newProfileVisaDetails.Count > 0)
						{
							cp.ProfileVisaDetailsList = newProfileVisaDetails;
						}
					}

				}
			}
			catch (Exception ex)
			{
				throw ex;
			}

		}
		private void GetApproversList(CorporateProfile cp)
		{
			try
			{
				List<CorpProfileApproval> corpProfileApprovers = hdfMode.Value == "0" ? new List<CorpProfileApproval>() : cp.ProfileApproversList;
				var approvers = JsonConvert.DeserializeObject<List<CorpProfileApproval>>(hdnApproversList.Value);
				if (approvers != null && approvers.Count > 0)
				{

					foreach (var approver in approvers)
					{
						if (!corpProfileApprovers.Exists(x => x.Id == approver.Id && x.Type == approver.Type) && approver.Status == "A")
						{
							approver.Id = 0;
							approver.CreatedBy = (int)Settings.LoginInfo.UserID;
							corpProfileApprovers.Add(approver);
							cp.ProfileApproversList.Add(approver);
						}
						else
						{
							var existApprover = corpProfileApprovers.Where(x => x.Id == approver.Id).FirstOrDefault();
							if (existApprover != null)
							{
								existApprover.Type = approver.Type;
								existApprover.Hierarchy = approver.Hierarchy;
								existApprover.ApproverId = approver.ApproverId;
								existApprover.Status = approver.Status;
								existApprover.JourneyType = approver.JourneyType;
								existApprover.ApprovalType = approver.ApprovalType;
								existApprover.Amount = approver.Amount;
								existApprover.ModifiedBy = (int)Settings.LoginInfo.UserID;
								//cp.ProfileApproversList.Add(existApprover);
							}
						}
					}
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		private void GetGDSSettingsList(CorporateProfile cp)
		{
			try
			{
				List<CropProfileGDSSettings> ProfileGDSSettingsList = new List<CropProfileGDSSettings>();
				CropProfileGDSSettings pa = new CropProfileGDSSettings();
				 if (hdfMode.Value == "1")//Edit mode
				{
					if (cp.ProfileGDSSettings != null && cp.ProfileGDSSettings.Count > 0)
					{
						pa.SettingsId = cp.ProfileGDSSettings[0].SettingsId;
					}
					else
					{
						pa.SettingsId = -1;
					}
					pa.GDS = Convert.ToString(txtGDS.Text.Trim());
					pa.Description = Convert.ToString(txtGDSDescription.Text.Trim());
					pa.OSI = Convert.ToString(txtGDSOSI.Text.Trim());
					pa.OwnerPCC = Convert.ToString(txtGDSOwnerPCC.Text.Trim());
					pa.ExtraCommand = Convert.ToString(txtGDSextraCommand.Text.Trim());
					pa.CorporateSSR = Convert.ToString(txtGDScorporateSSR.Text.Trim());
					pa.Remraks = Convert.ToString(txtGDSRemraks.Text.Trim());
					pa.CreatedBy = (int)Settings.LoginInfo.UserID;
					ProfileGDSSettingsList.Add(pa);
					cp.ProfileGDSSettings = ProfileGDSSettingsList;
				}
			}
			catch (Exception ex)
			{
				throw ex;
			}
		}
		private void Clear()
		{
			try
			{
				int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
				if (agentId == 0) agentId = 1;
				CorporateProfile corpProfile = new CorporateProfile(-1, agentId);

				DataTable dtFlex = corpProfile.DtProfileFlex;

				DataTable dtPolicies = corpProfile.DtProfilePolicy;
				txtbatch.Text = string.Empty;
				ddlProfileType.SelectedIndex = 0;
				ddlTitle.SelectedIndex = 0;
				txtSurname.Text = string.Empty;
				txtname.Text = string.Empty;
				ddlDesignation.SelectedIndex = 0;
				txtEmpId.Text = string.Empty;
				ddlDivision.SelectedIndex = 0;
				ddlCostCentre.SelectedIndex = 0;
				ddlGrade.SelectedIndex = 0;
				txtTelPhone.Text = string.Empty;
				txtMobileNo.Text = string.Empty;
				txtFax.Text = string.Empty;
				txtEmail.Text = string.Empty;
				txtAddress2.Text = string.Empty;
				ddlNationality.SelectedIndex = 0;
				txtPassportNo.Text = string.Empty;
				txtDOI.Text = string.Empty;
				txtDOE.Text = string.Empty;
				ddlcountryOfIssue.SelectedIndex = 0;
				txtDOB.Text = string.Empty; ;
				ddlPlaceOfBirth.SelectedIndex = 0;
				//rdbMale.Checked = true;
				ddlGender.SelectedIndex = -1;
				txtSeatPref.Text = string.Empty;
				txtMealRequest.Text = string.Empty;
				txtRemarks.Text = string.Empty;
				ddlRoomType.SelectedIndex = 0;
				txtMobileCoutryCode.Text = string.Empty;
				txtPhoneCountryCode.Text = string.Empty;
				ddlAddCostCenter.SelectedIndex = 0;
				ddlLocation.SelectedIndex = 0;
				ddlAppName.SelectedIndex = 0;
				txtAddEmpId.Text = string.Empty;
				txtAddErpCompany.Text = string.Empty;
				txtAddTrip.Text = string.Empty;
				if (Settings.LoginInfo.AgentType != AgentType.BaseAgent)
				{
					//ddlLocation.SelectedValue = Utility.ToString(Settings.LoginInfo.LocationID);
					ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
				}
				if (Utility.ToInteger(Settings.LoginInfo.AgentId) > 1)
					ddlLocation.SelectedValue = Utility.ToString(Settings.LoginInfo.LocationID);

				hdfRTDetailId.Value = "0";


				
				hdfMode.Value = "0";
				CurrentObject = null;
				
				hdnApproversList.Value = string.Empty;
				hdnVisaDetails.Value = string.Empty;
				hdnDelVD.Value = string.Empty;
				//hdnProfileId.Value = string.Empty;
				dcJoiningDate.Value = DateTime.Now;
				txtMiddleName.Text = string.Empty;
				//dcTerminationdate.Value = DateTime.Now;
				ddlResidency.SelectedIndex = 0;
				ddlDeligateSupervisor.SelectedValue = "0";
				txtExecAssistance.Text = string.Empty;
				ddlvisaplaceofissue.SelectedIndex = 0;
				txtGSTNumber.Text = string.Empty;
				txtGSTName.Text = string.Empty;
				txtGSTEmail.Text = string.Empty;
				txtGSTAddress.Text = string.Empty;
				txtGSTPhone.Text = string.Empty;
				ddlDomestic.SelectedIndex = 0;
				ddlinternational.SelectedIndex = 0;
				txtGDSProfilePNR.Text = string.Empty;
				txtGDS.Text = string.Empty;
				txtGDScorporateSSR.Text = string.Empty;
				txtGDSDescription.Text = string.Empty;
				txtGDSextraCommand.Text = string.Empty;
				txtGDSOSI.Text = string.Empty;
				txtGDSOwnerPCC.Text = string.Empty;
				txtGDSQueueNo.Text = string.Empty;
				txtGDSRemraks.Text = string.Empty;
				
				ddlApproverType.SelectedValue = "N";
				
				chkAffidavit.Checked = false;
				

			}
			catch { throw; }

		}
		#endregion
		#region dropDown Click Event
		protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				int agentId = Utility.ToInteger(ddlAgent.SelectedItem.Value);
				BindLocation();
				BindGrade();
				BindSetupValues(ddlDesignation, "DG");
				BindSetupValues(ddlDivision, "DV");
				BindSetupValues(ddlCostCentre, "CS");
				BindSetupValues(ddlAddCostCenter, "CS");
				Clear();
				Utility.StartupScript(this.Page, "appendAllApproversList();", "SCRIPT");
			}
			catch (Exception ex)
			{
				Label lblMasterError = (Label)this.Master.FindControl("lblError");
				lblMasterError.Visible = true;
				lblMasterError.Text = ex.Message;
				Utility.WriteLog(ex, this.Title);
				//Utility.StartupScript(this.Page, "ShowMessageDialog('btnSave_Click','"+ex.Message+"','Information')", "btnSave_Click"); 
				Utility.Alert(this.Page, ex.Message);
			}

		}
		#endregion
		#region Button click Events
		protected void btnGSearch_Click(object sender, EventArgs e)
		{
			try
			{

				BindSearch();
			}
			catch (Exception ex)
			{
				Utility.WriteLog(ex, this.Title);
				Utility.Alert(this.Page, ex.Message);

			}


		}

		

		protected void btnClear_Click(object sender, EventArgs e)
		{
			try
			{
				Clear();

			}
			catch (Exception ex)
			{
				Label lblMasterError = (Label)this.Master.FindControl("lblError");
				lblMasterError.Visible = true;
				lblMasterError.Text = ex.Message;
				Utility.WriteLog(ex, this.Title);
				//Utility.StartupScript(this.Page, "ShowMessageDialog('btnSave_Click','"+ex.Message+"','Information')", "btnSave_Click"); 
				Utility.Alert(this.Page, ex.Message);
			}

		}
		protected void btnUpdate_Click(object sender, EventArgs e)
		{
			try
			{
				Update();

			}
			catch (Exception ex)
			{
				Label lblMasterError = (Label)this.Master.FindControl("lblError");
				lblMasterError.Visible = true;
				lblMasterError.Text = ex.Message;
				Utility.WriteLog(ex, this.Title);

				Utility.Alert(this.Page, ex.Message);
			}

		}
		protected void btnSearch_Click(object sender, EventArgs e)
		{
			try
			{
				this.Master.ShowSearch("Search");
				BindSearch();
			}
			catch (Exception ex)
			{
				//Label lblMasterError = (Label)this.Master.FindControl("lblError");
				//lblMasterError.Visible = true;
				//lblMasterError.Text = ex.Message;
				Utility.WriteLog(ex, this.Title);
				Utility.Alert(this.Page, ex.Message);
			}

		}
		#endregion
		#region Gridview click events
		protected void gvSearch_SelectedIndexChanged(object sender, EventArgs e)
		{
			try
			{
				Clear();
				long profileId = Utility.ToLong(gvSearch.SelectedValue);
				DataRow dr = SearchList.Rows.Find(profileId);
				int agentId = Utility.ToInteger(dr["AgentId"]);
				Edit(profileId, agentId);
				this.Master.HideSearch();
			}
			catch (Exception ex)
			{
				Label lblMasterError = (Label)this.Master.FindControl("lblError");
				lblMasterError.Visible = true;
				lblMasterError.Text = ex.Message;
				Utility.WriteLog(ex, this.Title);
			}

		}

		protected void gvSearch_PageIndexChanging(object sender, GridViewPageEventArgs e)
		{
			try
			{
				gvSearch.PageIndex = e.NewPageIndex;
				gvSearch.EditIndex = -1;
				CommonGrid g = new CommonGrid();
				g.BindGrid(gvSearch, SearchList.Copy());
			}
			catch (Exception ex)
			{
				Label lblMasterError = (Label)this.Master.FindControl("lblError");
				lblMasterError.Visible = true;
				lblMasterError.Text = ex.Message;
				Utility.WriteLog(ex, this.Title);
			}
		}
		#endregion

		
	}
}
