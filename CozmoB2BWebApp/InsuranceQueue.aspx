﻿<%@ Page Language="C#" MasterPageFile="~/TransactionBE.master" AutoEventWireup="true" Inherits="InsuranceQueueGUI" Title="Insurance Queue" Codebehind="InsuranceQueue.aspx.cs" %>
<%@ MasterType VirtualPath="~/TransactionBE.master" %>
<asp:Content ID="Content1" ContentPlaceHolderID="cphTransaction" Runat="Server">

<%--<link href="css-holiday/main-style.css" rel="stylesheet" type="text/css" />--%>



    <script type="text/javascript" src="yui/build/yahoo/yahoo-min.js"></script>
    <script type="text/javascript" src="yui/build/event/event-min.js"></script>
    <script type="text/javascript" src="yui/build/dom/dom-min.js"></script>
    <script type="text/javascript" src="yui/build/calendar/calendar.js"></script>
    <script type="text/javascript" src="yui/build/animation/animation-min.js"></script>
    <script type="text/javascript" src="yui/build/autocomplete/autocomplete-min.js"></script>
    <script type="text/javascript" src="yui/build/dragdrop/dragdrop-min.js"></script>
    <script type="text/javascript" src="Scripts/jsBE/Search.js"></script>
    <script src="yui/build/yahoo-dom-event/yahoo-dom-event.js" type="text/javascript"></script>
    <script src="yui/build/container/container-min.js" type="text/javascript"></script>
<%--    <link rel="stylesheet" href="yui/build/container/assets/container.css" type="text/css" />--%>
    <script type="text/javascript" src="Scripts\jsBE\organictabs.jquery.js"></script>
    <script type="text/javascript" src="ash.js"></script>
     <link rel="stylesheet" href="css/style.css">
    <link href="yui/build/calendar/assets/calendar.css" rel="stylesheet" type="text/css" />
     <link href="css/style.css" rel="stylesheet" type="text/css" /> <!--Added by chandan on  13062016 -->
     
     <link href="css/main-style.css" rel="stylesheet" type="text/css" />
    <div style="padding-top: 10px; position: relative">
        <div id="errMess" class="error_module" style="display: none;">
            <div id="errorMessage" style="float: left; color: Red;" class="padding-5 yellow-back width-100 center margin-top-5">
            </div>
        </div>
        <div class="clear" style="margin-left: 25px">
            <div id="container1" style="position: absolute; top: 0px; right: 2%; display: none;
                z-index: 9999;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="container2" style="position: absolute; top: 0px; left: 20%; display: none;
                z-index: 9999;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="container3" style="position: absolute; top: 0px; left: 2%; display: none;
                z-index: 9999;">
            </div>
        </div>
        <div class="clear" style="margin-left: 30px">
            <div id="container4" style="position: absolute; top: 0px; left: 46%; display: none;
                z-index: 9999;">
            </div>
        </div>
        <div class="bg_white bor_gray padding-5 paramcon">
            <div class="col-md-12 padding-0 margin-bottom-10">
              <div class="col-md-2">
                    <asp:Label ID="lblPurchaseDateFrom" Text="Purchase From Dt:" runat="server"></asp:Label></div>
                <div class="col-md-2">
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtPurchaseDateFrom" runat="server" CssClass="form-control" Width="110px"></asp:TextBox>
                            </td>
                            <td>
                                <a href="javascript:void(null)" onclick="showCalendar3()">
                                    <img id="Img2" src="images/call-cozmo.png" alt="Pick Date" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblPurDtTo" Text="Purchase To Dt:" runat="server"></asp:Label></div>
                <div class="col-md-2">
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtPurchaseDateTo" runat="server" CssClass="form-control" Width="110px"></asp:TextBox>
                            </td>
                            <td>
                                <a href="javascript:void(null)" onclick="showCalendar4()">
                                    <img id="Img3" src="images/call-cozmo.png" alt="Pick Date" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
               
               <div class="col-md-2">
                    <asp:Label ID="lblDeptDate" Text="Departure Date:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtDeptDate" runat="server" Width="110px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td>
                                <a href="javascript:void(null)" onclick="showCalendar1()">
                                    <img id="dateLink1" src="images/call-cozmo.png" alt="Pick Date" />
                                </a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 margin-bottom-10">
                <div class="col-md-2">
                    <asp:Label ID="lblArrDate" Text="Arrival Date:" runat="server"></asp:Label></div>
                <div class="col-md-2">
                    <table>
                        <tr>
                            <td>
                                <asp:TextBox ID="txtArrDate" runat="server" Width="110px" CssClass="form-control"></asp:TextBox>
                            </td>
                            <td>
                                <a href="javascript:void(null)" onclick="showCalendar2()">
                                    <img id="Img1" src="images/call-cozmo.png" alt="Pick Date" /></a>
                            </td>
                        </tr>
                    </table>
                </div>
                <div class="col-md-2">
                    Agency:</div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlAgency" runat="server" CssClass="inputDdlEnabled form-control"
                        OnSelectedIndexChanged="ddlAgency_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblB2BAgent" Text="B2BAgent:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlB2BAgent" runat="server" CssClass="inputDdlEnabled form-control"
                        OnSelectedIndexChanged="ddlB2BAgent_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 margin-bottom-10">
                <div class="col-md-2">
                    <asp:Label ID="lblB2B2BAgent" Text="B2B2BAgent:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlB2B2BAgent" runat="server" CssClass="inputDdlEnabled form-control">
                    </asp:DropDownList>
                </div>
                <div class="col-md-2">
                    <asp:Label ID="lblPolicyNo" Text="Certificate No:" runat="server"></asp:Label></div>
                <div class="col-md-2">
                    <asp:TextBox ID="txtPolicyNo" CssClass="form-control" runat="server"></asp:TextBox>
                </div>
               <%-- <div class="col-md-2">
                    <asp:Label ID="lblPNRno" Text="PNR No:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:TextBox CssClass="form-control" ID="txtPNRno" runat="server"></asp:TextBox></div>--%>
             
                       <div class="col-md-2">
                    <asp:Label ID="lblTransType" runat="server" Text="TransType:" Visible="false"></asp:Label></div>
                <div class="col-md-2">
                    <asp:DropDownList CssClass="form-control" ID="ddlTransType" runat="server" Visible="true">
                        <asp:ListItem Selected="True" Value="-1" Text="--All--"></asp:ListItem>
                        <asp:ListItem Value="B2B" Text="B2B"></asp:ListItem>
                        <asp:ListItem Value="B2C" Text="B2C"></asp:ListItem>
                    </asp:DropDownList>
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 margin-bottom-10">
             <div class="col-md-2">
                    <asp:Label ID="Label1" Text="Insurance Type" runat="server"></asp:Label></div>
                <div class="col-md-2">
                    <asp:DropDownList ID="ddlInsType" runat="server"  CssClass="inputDdlEnabled form-control"  >                          
                        <asp:ListItem Value="TUNES" Selected="True">TUNES</asp:ListItem>
                        <asp:ListItem Value="RELIGARE">RELIGARE</asp:ListItem>
                    </asp:DropDownList>
                </div>
           
             <div class="col-md-2">
                    <asp:Label ID="lblOrigin" Text="Origin:" runat="server"></asp:Label></div>
                <div class="col-md-2">
                    <asp:TextBox ID="Origin" CssClass="form-control" runat="server"></asp:TextBox>
                    <div class="clear">
                        <div style="width:95%; line-height:30px; color:#000;" id="statescontainer">
                        </div>
                    </div>
                    <div id="multipleCity1">
                    </div>
                </div>
              <div class="col-md-2">
                    <asp:Label ID="lblDestination" Text="Destination:" runat="server"></asp:Label>
                </div>
                <div class="col-md-2">
                    <asp:TextBox ID="Destination" CssClass="form-control" runat="server"></asp:TextBox>
                    <div id="statesshadow2">
                        <div style="width:95%; line-height:30px; color:#000;" id="statescontainer2">
                        </div>
                    </div>
                    <div id="multipleCity2">
                    </div>
                </div>
                
                 
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 margin-bottom-10">
                <div class="col-md-2">
                    <asp:Label ID="lblPnr" Text="PNR No:" runat="server"></asp:Label></div>
                <div class="col-md-2">
                    <asp:TextBox ID="txtPnrNo" CssClass="form-control" runat="server" MaxLength="25"></asp:TextBox>
                </div>
                <div class="clearfix">
                </div>
            </div>
            <div class="col-md-12 padding-0 margin-bottom-10">
                <label class=" f_R padding-left-5">
                    <asp:Button runat="server" ID="btnSearch" Text="Search" OnClick="btnSearch_Click"
                        CssClass="but but_b" OnClientClick="return Validate();" /></label>
                <label class=" f_R padding-left-5">
                    <asp:Button runat="server" ID="btnClear" Text="Clear" OnClick="btnClear_Click" CssClass="but but_b" /></label>
            </div>
            <div class="clear">
            </div>
        </div>
       
       
        <div style="padding-bottom: 10px;" width="100%">
            <div <%=pagingEnable %>>
                Page:
                <asp:LinkButton ID="btnFirst" runat="server" OnClick="btnFirst_Click" Text="First" ></asp:LinkButton>
                <asp:LinkButton ID="btnPrev" Text="Prev" OnClick="btnPrev_Click" runat="server" />
                <asp:Label ID="lblCurrentPage" runat="server" CssClass=""></asp:Label>
                <asp:LinkButton ID="btnNext" Text="Next" OnClick="btnNext_Click" runat="server" />
                <asp:LinkButton ID="btnLast" runat="server" OnClick="btnLast_Click" Text="Last"></asp:LinkButton>
            </div>
            <div class="clear">
            </div>
        </div>
        <div>
            <asp:DataList ID="dlInsQueue" runat="server" CellPadding="4" DataKeyField="Ins_Id"
                Width="100%" OnItemDataBound="dlInsQueue_ItemDataBound" OnItemCommand="dlInsQueue_ItemCommand">
                <ItemTemplate>
                    <div style="display: none" id="DisplayPlans<%#Container.ItemIndex %>" class="div-TravelAgent">
                        <span style="position: absolute; right: 0px; top: 0px; cursor: pointer"><a href="#"
                            onclick="hide('DisplayPlans<%#Container.ItemIndex %>');">
                            <img src="images/close1.png" /></a></span>
                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                                <td>
                                    <asp:CheckBoxList ID="chkPlans" runat="server">
                                    </asp:CheckBoxList>
                                </td>
                            </tr>
                            <tr>
                            <td>
                             <b id="errorPlans" style="color: Red" runat="server"></b>
                            </td>
                            
                            </tr>
                            <tr>
                                <td>
                                 <span id="spinloader<%#Container.ItemIndex %>" style="display:none;"><img src="images/ajax-loader.gif" alt="loader" /></span>
                                    <asp:Button ID="btnContine" runat="server" CommandName="InsChangeRequest" CommandArgument='<%#Eval("Ins_Id") %>'
                                        Text="Continue" CssClass="but but_b"/>
                                </td>
                            </tr>
                        </table>
                    </div>
                    <div class="bg_white bor_gray pad_10 marbot_10" id="Result">
                        <div>
                            
                            
                            
                            
                            
                            <table class="table table-bordered"> 
                            
                            
                            <tr> 
                            <td>Trip Id: <strong><%# Eval("AgetRefNO")%>(<%# Eval("agent_name")%>) </strong> </td>
                             <td>Status:<strong><asp:Label ID="lblStatus" runat="server"></asp:Label></strong> </td> 
                             
                             <td><table>
                                 <tr>
                                     <td style="width:50%"> PNR: <strong><%# Eval("PNR")%></strong></td>
                                      <td style="width:50%">Insurance Type :<strong><asp:Label ID="lblInsType" runat="server" Text='<%# Eval("InsType") %>'></asp:Label></strong></td>
                                     </tr>
                                 </table>
                                  </tr>
                             </td>
                            </tr>  <tr> 
                            <td>Destination: <strong><%# Eval("OriginCity")%> - <%# Eval("DestinationCity")%> </strong></td> 
                            <td>Departure Date: <strong><%# Convert.ToDateTime(Eval("DepartureDate")).ToString("dd-MMM-yyyy HH:MM")%></strong> </td> 
                            <td> Return Date: <strong><%# Convert.ToDateTime(Eval("ReturnDate")).ToString("dd-MMM-yyyy HH:MM")%> </strong></td>
                            </tr>
                            
                            
                            
                           
                            
                            
                            
                            
                                 <tr> 
                            <td>Purchase Date: <strong><%# Convert.ToDateTime(Eval("createdON")).ToString("dd-MMM-yyyy HH:MM")%> </strong></td> 
                            
                            <td>Booked By Location: <strong><%# Eval("location_name")%> </strong> </td> 
                            
                            <td>Booked By User: <strong><%# Eval("USER_FULLNAME")%> </strong> </td>
                            </tr>
                            
          
                            <tr> 
                            
                            <td colspan="2"> <strong> Amount: </strong></td> <td> <asp:Label CssClass="fcol_red" ID="lblTotal" runat="server"></asp:Label> </td>
                            
                            
                            </tr>
                            
                            
                            
                            </table>
                            
                            
                            
                            
                           
                            
                            
                            
                            
                            <table width="99%">
                                <tr>
                                    <td>
                                        <asp:Label ID="lblRequestStatus" BackColor="#9AF1A2" ForeColor="Black" runat="server"
                                            Font-Italic="True" Font-Names="Arial" Font-Size="10pt"></asp:Label>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        
                                       <div class="col-md-12 padding-0 marbot_10">
                                       
                                       
                                           <div id="accordion" class="panel-group">
                                               <div class="panel panel-default">
                                                   <div class="panel-heading">
                                                       <h4 class="panel-title">
   <a href="#collapseOne<%#Container.ItemIndex %>" data-parent="#accordion" data-toggle="collapse" class="accordion-toggle btn-block" aria-expanded="false"> <span class="glyphicon pull-right font_bold glyphicon-plus"> </span>View Plan Details
                                                               
                                                           </a>
                                                       </h4>
                                                   </div>
                                                   <div class="panel-collapse collapse" id="collapseOne<%#Container.ItemIndex %>" aria-expanded="false"
                                                       style="height: 0px;">
                                                       <div>
                                                           <div runat="server" id="divPlans">
                                                           </div>
                                                       </div>
                                                   </div>
                                               </div>
                                           </div>
                                            
                                            <script>


                                                $('.collapse').on('shown.bs.collapse', function() {
                                                    $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
                                                }).on('hidden.bs.collapse', function() {
                                                    $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
                                                });

</script> 
                                            
                                         <div class="clearfix">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="4">
                                       



                                       <div class="row"> 
                                       <div class="col-md-8"> 
                                       
                                       
                                        <table style="margin-top: 10px" id="tblChangeRequest" visible="false" width="100%"
                                            border='0' cellspacing='0' cellpadding='2' runat="server">
                                            <tr>
                                                <td>
                                                    <div class="padding-0 paramcon">
                                                     
                                                        <div class="col-md-4">


                                                        <table> 
                                                        <tr> 
                                                        
                                                        <td> Request Change :</td>
                                                        
                                                        
                                                       <td> <asp:DropDownList width="100px" CssClass="form-control" ID="ddlChangeRequestType" runat="server">
                                                                <asp:ListItem Selected="True" Text="Select" Value="Select"></asp:ListItem>
                                                                <asp:ListItem Text="Cancel Request" Value="Cancel Request"></asp:ListItem>
                                                            </asp:DropDownList>
                                                            
                                                            </td>

                                                        </tr>
                                                        
                                                        
                                                        
                                                        </table>


                                                            
                                                        </div>
                                                        <div class="col-md-6">
                                                            <asp:TextBox ID="txtRemarks" Width="100%" TextMode="MultiLine" Rows="2" Text="Enter Remarks here"
                                                                runat="server" Height="80px" onfocus="this.style.color='#000000'; if( this.value=='Enter Remarks here' ) { this.value=''; }"
                                                                onblur="this.style.color='#000000'; if( this.value=='' ) { this.value='Enter Remarks here'; }"></asp:TextBox>
                                                        </div>
                                                        <div class="col-md-2">
                                                            <b id="errRemarks" style="color: Red" runat="server"></b>
                                                            <asp:Button ID="btnRequest" runat="server" 
                                                                Text="Submit Request" CssClass="but but_b" CommandName="InsChangeRequest" CommandArgument='<%#Eval("Ins_Id") %>' /><%-- CommandName="InsChangeRequest" CommandArgument='<%#Eval("Ins_Id") %>'--%>
                                                        </div>
                                                       
                                                        <div class="clearfix">
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        </table>
                                       
                                       
                                       </div>










                                        <div class="col-md-4 text-right"> 
                                        
                                       
                                                            <label class="margin-left-5">
                                                                <asp:Label ID="lblViewInvoice" runat="server" Text=""></asp:Label>
                                                            </label>
                                                            <label class="margin-left-5">
                                                                <asp:Label ID="lblViewBooking" runat="server" Text=""></asp:Label>
                                                            </label>
                                                        </div>
                                      
                                      
                                    
                                         
                                            </div>     
                                                 
                                                 
                                                 
                                                 
                                               
                                      
                                      
                                   
                                      
                                     
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left" colspan="2" style="padding-left: 20px">
                                        <div style="position: relative">
                                            <asp:LinkButton ID="lnkPayment" runat="server" Visible="false" Text="Payment Information"
                                                href="#"></asp:LinkButton>
                                            <div id="PaymentInfo-<%#Container.ItemIndex %>" class="visa_PaymentInfo_pop" style="position: absolute;
                                                display: none; bottom: -70px; left: 234px; height: auto!important; z-index: 9999;">
                                            </div>
                                        </div>
                                    </td>
                                </tr>
                            </table>
                        </div>
                        <div class="clearfix">
                        </div>
                    </div>
                </ItemTemplate>
            </asp:DataList>
            <div class="clear">
            </div>
        </div>
        
        
         
    </div>
    
    <script type="text/javascript">
        var cal1;
        var cal2;
        var cal3;
        var cal4;
        function init() {

            //    showReturn();
            var today = new Date();
            // For making dual Calendar use CalendarGroup  for single Month use Calendar     
            cal1 = new YAHOO.widget.Calendar("cal1", "container1");
//            cal1.cfg.setProperty("minDate", (today.getMonth() + 1) + "/" + today.getDate() + "/" + today.getFullYear());
            cal1.cfg.setProperty("title", "Select Departure date");
            cal1.cfg.setProperty("close", true);
            cal1.selectEvent.subscribe(setDate1);
            cal1.render();

            cal2 = new YAHOO.widget.Calendar("cal2", "container2");
            cal2.cfg.setProperty("title", "Select Arrival date");
            cal2.selectEvent.subscribe(setDate2);
            cal2.cfg.setProperty("close", true);
            cal2.render();

            cal3 = new YAHOO.widget.Calendar("cal3", "container3");
            cal3.cfg.setProperty("title", "Purchase From Date");
            cal3.selectEvent.subscribe(setDate3);
            cal3.cfg.setProperty("close", true);
            cal3.render();

            cal4 = new YAHOO.widget.Calendar("cal4", "container4");
            cal4.cfg.setProperty("title", "Purchase To Date");
            cal4.selectEvent.subscribe(setDate4);
            cal4.cfg.setProperty("close", true);
            cal4.render();
        }
        function showCalendar1() {
            $('container4').context.styleSheets[0].display = "none";
            $('container3').context.styleSheets[0].display = "none";
            $('container2').context.styleSheets[0].display = "none";
            $('container1').context.styleSheets[0].display = "block";
            cal1.show();
            cal2.hide();
            cal3.hide();
            cal4.hide();
            init();
        }
//        var departureDate = new Date();
        function showCalendar2() {
            $('container1').context.styleSheets[0].display = "none";
            $('container3').context.styleSheets[0].display = "none";
            $('container4').context.styleSheets[0].display = "none";
            cal1.hide();
            cal3.hide();
            cal4.hide();
            cal2.show();
            init();
//            // setting Calender2 min date acoording to calendar1 selected date
//            var date1 = document.getElementById('<%= txtDeptDate.ClientID%>').value;
//            //var date1=new Date(tempDate.getDate()+1);

//            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
//                var depDateArray = date1.split('/');

//                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);

//                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
//                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
//                cal2.render();
//            }
            document.getElementById('container2').style.display = "block";
        }

        function showCalendar3() {
            $('container1').context.styleSheets[0].display = "none";
            $('container2').context.styleSheets[0].display = "none";
            $('container4').context.styleSheets[0].display = "none";
            cal1.hide();
            cal2.hide();
            cal4.hide();
            cal3.show();
            init();
            //            // setting Calender2 min date acoording to calendar1 selected date
            //            var date1 = document.getElementById('<%= txtDeptDate.ClientID%>').value;
            //            //var date1=new Date(tempDate.getDate()+1);

            //            if (date1.length != 0 && date1 != "DD/MM/YYYY") {
            //                var depDateArray = date1.split('/');

            //                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate() + 1);

            //                cal2.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
            //                cal2.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
            //                cal2.render();
            //            }
            document.getElementById('container3').style.display = "block";
        }
        var departureDate = new Date();
        function showCalendar4() {
            $('container1').context.styleSheets[0].display = "none";
            $('container2').context.styleSheets[0].display = "none";
            $('container3').context.styleSheets[0].display = "none";
            cal1.hide();
            cal2.hide();
            cal3.hide();
            cal4.show();
            init();
            var date3 = document.getElementById('<%= txtPurchaseDateFrom.ClientID%>').value;
            //var date1=new Date(tempDate.getDate()+1);

            if (date3.length != 0 && date3 != "DD/MM/YYYY") {
                var depDateArray = date3.split('/');

                var arrMinDate = new Date(departureDate.getFullYear(), departureDate.getMonth(), departureDate.getDate());

                cal4.cfg.setProperty("minDate", (arrMinDate.getMonth() + 1) + "/" + arrMinDate.getDate() + "/" + arrMinDate.getFullYear());
                cal4.cfg.setProperty("pageDate", depDateArray[1] + "/" + depDateArray[2]);
                cal4.render();
            }
            document.getElementById('container4').style.display = "block";
        }
        
        function setDate1() {
            var date1 = cal1.getSelectedDates()[0];

//            $('IShimFrame').context.styleSheets[0].display = "none";
//            this.today = new Date();
//            var thisMonth = this.today.getMonth();
//            var thisDay = this.today.getDate();
//            var thisYear = this.today.getFullYear();

//            var todaydate = new Date(thisYear, thisMonth, thisDay);
//            var depdate = new Date(date1.getFullYear(), date1.getMonth(), date1.getDate());
//            var difference = (depdate.getTime() - todaydate.getTime());

//            if (difference < 0) {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "Date selected should be greater than or equal to today's date ";
//                return false;
//            }
//            departureDate = cal1.getSelectedDates()[0];
//            document.getElementById('errMess').style.display = "none";
//            document.getElementById('errorMessage').innerHTML = "";
            //			
            var month = date1.getMonth() + 1;
            var day = date1.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%= txtDeptDate.ClientID %>').value = day + "/" + (month) + "/" + date1.getFullYear();

            //cal2.pages[0].minDate = YAHOO.widget.DateMath.add(date1, YAHOO.widget.DateMath.DAY, 0);
            //cal2.render();

            cal1.hide();

        }
        function setDate2() {
//            var date1 = document.getElementById('<%=txtDeptDate.ClientID %>').value;
//            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
//                return false;
//            }

            var date2 = cal2.getSelectedDates()[0];

//            var depDateArray = date1.split('/');

//            // checking if date1 is valid		    
//            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
//                return false;
//            }
//            document.getElementById('errMess').style.display = "none";
//            document.getElementById('errorMessage').innerHTML = "";

//            // Note: Date()	for javascript take months from 0 to 11
//            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
//            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
//            var difference = returndate.getTime() - depdate.getTime();

//            if (difference < 1) {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
//                return false;
//            }
//            if (difference == 0) {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
//                return false;
//            }
//            document.getElementById('errMess').style.display = "none";
//            document.getElementById('errorMessage').innerHTML = "";

            var month = date2.getMonth() + 1;
            var day = date2.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtArrDate.ClientID %>').value = day + "/" + month + "/" + date2.getFullYear();
            cal2.hide();
        }

        function setDate3() {
//            var date1 = document.getElementById('<%=txtPurchaseDateFrom.ClientID %>').value;
//            if (date1.length == 0 || date1 == "DD/MM/YYYY") {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "First select checkin date.";
//                return false;
//            }

            var date3 = cal3.getSelectedDates()[0];

//            var depDateArray = date1.split('/');

//            // checking if date1 is valid		    
//            if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = " Invalid Departure Date";
//                return false;
//            }
//            document.getElementById('errMess').style.display = "none";
//            document.getElementById('errorMessage').innerHTML = "";

//            // Note: Date()	for javascript take months from 0 to 11
//            var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
//            var returndate = new Date(date2.getFullYear(), date2.getMonth(), date2.getDate());
//            var difference = returndate.getTime() - depdate.getTime();

//            if (difference < 1) {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "Date of CheckOut should be greater than  or equal to date of checkin (" + date1 + ")";
//                return false;
//            }
//            if (difference == 0) {
//                document.getElementById('errMess').style.display = "block";
//                document.getElementById('errorMessage').innerHTML = "Date of CheckIn and CheckOut Could not be same";
//                return false;
            //            }
            departureDate = cal3.getSelectedDates()[0];
//            document.getElementById('errMess').style.display = "none";
//            document.getElementById('errorMessage').innerHTML = "";

            var month = date3.getMonth() + 1;
            var day = date3.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtPurchaseDateFrom.ClientID %>').value = day + "/" + month + "/" + date3.getFullYear();
            cal3.hide();
        }

        function setDate4() {
            var date4 = cal4.getSelectedDates()[0];
            
            var date3 = document.getElementById('<%=txtPurchaseDateFrom.ClientID %>').value;
            if (date3.length == 0 || date3 == "DD/MM/YYYY") {
            document.getElementById('errMess').style.display = "block";
            document.getElementById('errorMessage').innerHTML = "First select Purchase from date.";
                return false;
            }

                       var depDateArray = date3.split('/');

                        // checking if date1 is valid		    
                        if (!CheckValidDate(depDateArray[0], depDateArray[1], depDateArray[2])) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = " Invalid Purchase From Date";
                            return false;
                        }
                        document.getElementById('errMess').style.display = "none";
                        document.getElementById('errorMessage').innerHTML = "";

                        // Note: Date()	for javascript take months from 0 to 11
                        var depdate = new Date(depDateArray[2], depDateArray[1] - 1, depDateArray[0]);
                        var returndate = new Date(date4.getFullYear(), date4.getMonth(), date4.getDate());
                        var difference = returndate.getTime() - depdate.getTime();

                        if (difference < 0) {
                            document.getElementById('errMess').style.display = "block";
                            document.getElementById('errorMessage').innerHTML = "Purchase To date should be greater than Purchase from date (" + date3 + ")";
                            return false;
                        }
//                        if (difference == 0) {
//                            document.getElementById('errMess').style.display = "block";
//                            document.getElementById('errorMessage').innerHTML = "Purchase from date and to date Could not be same";
//                            return false;
//                        }
                        document.getElementById('errMess').style.display = "none";
                        document.getElementById('errorMessage').innerHTML = "";
                        
            var month = date4.getMonth() + 1;
            var day = date4.getDate();

            if (month.toString().length == 1) {
                month = "0" + month;
            }

            if (day.toString().length == 1) {
                day = "0" + day;
            }

            document.getElementById('<%=txtPurchaseDateTo.ClientID %>').value = day + "/" + month + "/" + date4.getFullYear();
            cal4.hide();
        }

        YAHOO.util.Event.addListener(window, "load", init);
        //YAHOO.util.Event.addListener(this, "click", init);
        
    </script>
    <script type="text/javascript">


        function hide(id) {
            document.getElementById(id).style.display = "none";
        }
    
        function Validate() {
            if (document.getElementById('<%=txtPurchaseDateFrom.ClientID %>').value != '' && document.getElementById('<%=txtPurchaseDateTo.ClientID %>').value == '') {
                alert("Please Select Purchase To Date!");
                return false;
            }
            return true;
        }
        function CancelInsPlan(index) {
            var val = document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_txtRemarks').value;
            if (document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_ddlChangeRequestType').value == "Select") {
                document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_errRemarks').innerHTML = "Please Select Request type!";
                return false;
            }
            else if (val.length <= 0 || val == "Enter Remarks here") {
            document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_errRemarks').innerHTML = "Enter remarks";
                return false;
            }
            else {
                document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_errRemarks').innerHTML = "";
                document.getElementById('DisplayPlans' + index).style.display = 'block';
                return false;
            }
        }
        function PlansValidation(index) {
            var chklist = document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_chkPlans');
            var chkListinputs = chklist.getElementsByTagName("input");
            var count = 0;
            for (var i = 0; i < chkListinputs.length; i++) {
                if (chkListinputs[i].checked && !chkListinputs[i].disabled) {
                    count = count + 1;
                }
            }
            if (count == 0) {
                document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_errorPlans').innerHTML = "Please select at least one plan";
                return false;
            }
            document.getElementById('ctl00_cphTransaction_dlInsQueue_ctl0' + index + '_btnContine').style.display = "none";
            document.getElementById('spinloader' + index).style.display = "block";
            return true;
        }
        
        
        function ViewBooking(insID,bookingUrl) {
            var finalurl = bookingUrl+"?bookingId=" + insID;
            window.location = finalurl;
        }
        function ViewInvoice(insHdrId, insurl) {
             window.open(insurl + "?insHdrId=" + insHdrId, 'Voucher', 'width=900,height=600,status=yes,menubar=no,scrollbars=yes,resizable=yes,left=50,top=50');
        }

//invoke Payment information through Ajax
        var Ajax;

        if (window.XMLHttpRequest) {
            Ajax = new window.XMLHttpRequest();
        }
        else {
            Ajax = new ActiveXObject("Microsoft.XMLHTTP");
        }

        function ViewPaymentInfo(id, bookingId) {
            pblockId = id;
            var url = "PaymentInfoAjax";
            var paramList = 'isInsurancePaymentInfo=true';
            paramList += '&planId=' + bookingId;
            paramList += '&blockId=' + pblockId;
            document.getElementById('PaymentInfo-' + pblockId).style.display = "block";
            document.getElementById('PaymentInfo-' + pblockId).innerHTML = "Loading...";

            Ajax.onreadystatechange = ShowPaymentInfoPopUp;
            Ajax.open('POST', url);
            Ajax.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded');
            Ajax.send(paramList);
        }

        function ShowPaymentInfoPopUp() {
            if (Ajax.readyState == 4) {
                if (Ajax.status == 200) {
                    if (Ajax.responseText.length > 0) {
                        var numberOfRecord = 5;
                        for (var i = 0; i < numberOfRecord; i++) {
                            if (document.getElementById('PaymentInfo-' + i) != null)
                                document.getElementById('PaymentInfo-' + i).style.display = "none";

                        }
                        document.getElementById('PaymentInfo-' + pblockId).style.display = "block";
                        document.getElementById('PaymentInfo-' + pblockId).innerHTML = Ajax.responseText;

                    }
                }
            }
        }
        
    </script>
    
          
          
    

          
    
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="cphSearch" Runat="Server">
</asp:Content>

