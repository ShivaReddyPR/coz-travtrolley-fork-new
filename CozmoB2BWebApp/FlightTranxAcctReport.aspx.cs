﻿using System;
using System.Data;
using System.Web.UI;
using System.Web.UI.WebControls;
using CT.TicketReceipt.Common;
using CT.TicketReceipt.BusinessLayer;
using CT.TicketReceipt.Web.UI.Controls;
using CT.BookingEngine;
using System.IO;
using CT.Core;

public partial class FlightTranxAcctReportGUI : CT.Core.ParentPage
{
    #region Page Load
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        try
        {
            if (Settings.LoginInfo != null)
            {
                ScriptManager scriptManager = ScriptManager.GetCurrent(this.Page);
                scriptManager.RegisterPostBackControl(this.btnExport);
                //this.Master.PageRole = true;
                lblSuccessMsg.Text = string.Empty;
                hdfParam.Value = "1";
                Utility.StartupScript(this.Page, "ShowHide('divParam');", "ShowHide");
                if (!IsPostBack)
                {
                    ((Label)this.Master.FindControl("lblMainTitle")).Text = this.Title;
                    InitializePageControls();
                }
            }
            else
            {
                Response.Redirect("AbandonSession.aspx");
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    private void InitializePageControls()
    {
        try
        {
            dcFromDate.Value = Utility.ToDate(DateTime.Now.Date);
            dcToDate.Value = Utility.ToDate(DateTime.Now.Date.ToString("MMM-dd-yyyy 23:59"));
            Array Sources = Enum.GetValues(typeof(BookingSource));
            foreach (BookingSource source in Sources)
            {
                //if (source == BookingSource.AirArabia || source == BookingSource.UAPI)
                //{
                    ListItem item = new ListItem(Enum.GetName(typeof(BookingSource), source), ((int)source).ToString());
                    ddlSource.Items.Add(item);
                //}
            }
            BindAgent();
            int b2bAgentId;
            int b2b2bAgentId;
            if (Settings.LoginInfo.AgentType == AgentType.BaseAgent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
            }
            else if (Settings.LoginInfo.AgentType == AgentType.Agent)
            {
                ddlAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B)
            {
                ddlAgent.Enabled = false;
                b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2BAgent.Enabled = false;
            }
            else if (Settings.LoginInfo.AgentType == AgentType.B2B2B)
            {
                ddlAgent.Enabled = false;
                ddlB2BAgent.Enabled = false;
                b2b2bAgentId = AgentMaster.GetParentId(Convert.ToInt32(Settings.LoginInfo.AgentId));
                b2bAgentId = AgentMaster.GetParentId(b2b2bAgentId);
                ddlAgent.SelectedValue = Convert.ToString(b2bAgentId);
                ddlB2BAgent.SelectedValue = Convert.ToString(b2b2bAgentId);
                ddlB2B2BAgent.SelectedValue = Convert.ToString(Settings.LoginInfo.AgentId);
                ddlB2B2BAgent.Enabled = false;
            }
            BindB2BAgent(Convert.ToInt32(ddlAgent.SelectedItem.Value));
            BindB2B2BAgent(Convert.ToInt32(ddlB2BAgent.SelectedItem.Value));
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) < 0)
            {
                ddlB2B2BAgent.Enabled = false;
            }
            BindGrid();
        }
        catch
        { throw; }
    }
    #endregion
    #region private methods
    private void BindAgent()
    {
        try
        {
            ddlAgent.DataSource = AgentMaster.GetList(1, "AGENT", Settings.LoginInfo.AgentId, ListStatus.Short, RecordStatus.Activated);//TODO
            ddlAgent.DataValueField = "agent_id";
            ddlAgent.DataTextField = "agent_name";
            ddlAgent.DataBind();
            ddlAgent.Items.Insert(0, new ListItem("-- All --", "0"));
            ddlAgent.SelectedValue = Utility.ToString(Settings.LoginInfo.AgentId);
        }
        catch { throw; }
    }
    //Binding B2B Agents
    private void BindB2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B Means binding in Agency DropDown only B2B Agents
            ddlB2BAgent.DataSource = dtAgents;
            ddlB2BAgent.DataTextField = "Agent_Name";
            ddlB2BAgent.DataValueField = "agent_id";
            ddlB2BAgent.DataBind();
            ddlB2BAgent.Items.Insert(0, new ListItem("-- Select B2BAgent --", "-1"));
            ddlB2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }

    //Binding B2B2B Agents
    private void BindB2B2BAgent(int agentId)
    {
        try
        {
            DataTable dtAgents = AgentMaster.GetList(Settings.LoginInfo.CompanyID, "B2B2B", agentId, ListStatus.Short, RecordStatus.Activated);// B2B2B Means binding in Agency DropDown only B2B2B Agents
            ddlB2B2BAgent.DataSource = dtAgents;
            ddlB2B2BAgent.DataTextField = "Agent_Name";
            ddlB2B2BAgent.DataValueField = "agent_id";
            ddlB2B2BAgent.DataBind();
            ddlB2B2BAgent.Items.Insert(0, new ListItem("-- Select B2B2BAgent --", "-1"));
            ddlB2B2BAgent.Items.Insert(1, new ListItem("--All--", "0"));
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    private void BindGrid()
    {
        try
        {
            CommonGrid g = new CommonGrid();
            g.BindGrid(gvFlightInvReport, GetData());
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, 0, "Exception from Flight Report:" + ex.ToString(), "");
        }
    }
    private DataTable GetData()
    {
        DateTime fromDate = Utility.ToDate(dcFromDate.Value);
        DateTime toDate = Utility.ToDate(dcToDate.Value);
        int agent = Utility.ToInteger(ddlAgent.SelectedValue);
        int source = Utility.ToInteger(ddlSource.SelectedValue);
        string PNRNO = txtPNR.Text;
        string agentType = string.Empty;
        if (agent == 0)
        {
            agentType = "BASE";// BASE Means binding in list all BASEAGENT AND AGENTS BOOKINGS
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
            {
                agentType = "BASEB2B";// BASEB2B Means binding in list all BASEAGENT ,AGENTS AND B2B BOOKINGS
            }
            if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
            {
                agentType = string.Empty;// null Means binding in list all BOOKINGS
            }
        }
        if (agent > 0 && ddlB2BAgent.SelectedIndex > 0)
        {
            if (Convert.ToInt32(ddlAgent.SelectedItem.Value) > 1)
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "AGENT";// AGENT Means Based On the AGENT binding in list All B2B Bookings
                }
                else
                {
                    agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
                }
            }
            else
            {
                if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2B";// B2B Means Based On the BASEAGENT binding in list All B2B Bookings
                }
                 agent = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            }

        }
        if (agent > 0 && ddlB2B2BAgent.SelectedIndex > 0)
        {
            if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
            {
                agentType = "B2B2B";// B2B2B Means Based On the B2B binding in list All B2B2B Bookings
            }
            else
            {
                agent = Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value);
            }
        }
        if (Convert.ToInt32(ddlAgent.SelectedItem.Value) != 0)
        {
            if (Convert.ToInt32(ddlB2BAgent.SelectedItem.Value) == 0)
            {
                if (Convert.ToInt32(ddlB2B2BAgent.SelectedItem.Value) == 0)
                {
                    agentType = "B2BB2B2B";// B2BB2B2B Means Based On the AGENT OR BASEAGENT binding in list All B2B AND B2B2B Bookings
                    agent = Convert.ToInt32(ddlAgent.SelectedItem.Value);
                }
            }
        }
        #region B2C purpose
        string transType = string.Empty;
        if (Settings.LoginInfo.TransType == "B2B")
        {
            ddlTransType.Visible = false;
            lblTransType.Visible = false;
            transType = "B2B";
        }
        else if (Settings.LoginInfo.TransType == "B2C")
        {
            ddlTransType.Visible = false;
            lblTransType.Visible = false;
            transType = "B2C";
        }
        else
        {
            ddlTransType.Visible = true;
            lblTransType.Visible = true;
            if (ddlTransType.SelectedItem.Value == "-1")
            {
                transType = null;
            }
            else
            {
                transType = ddlTransType.SelectedItem.Value;
            }
        }
        #endregion
        DataTable FlightAcctReport = ProductReport.FlightTranxReportGetList(fromDate, toDate, agent, source, PNRNO, agentType, transType);
        return FlightAcctReport;
    }

    #endregion
    #region ButtonEvents
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {
            BindGrid();
        }
        catch { throw; }
    }
    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {            
            string attachment = "attachment; filename=FlightAcctReport.xls";
            Response.ClearContent();
            Response.AddHeader("content-disposition", attachment);
            Response.ContentType = "application/vnd.ms-excel";
            StringWriter sw = new StringWriter();
            HtmlTextWriter htw = new HtmlTextWriter(sw);
            dgFlightAcctReportList.AllowPaging = false;
            dgFlightAcctReportList.DataSource = GetData();
            dgFlightAcctReportList.DataBind();
            dgFlightAcctReportList.RenderControl(htw);
            Response.Write(sw.ToString());
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
        finally
        {
            Response.End();
        }
    }
    #endregion
    protected void ddlAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlAgent.SelectedItem.Value);
            if (agentId < 0) agentId = Settings.LoginInfo.AgentId;
            BindB2BAgent(agentId);
            BindB2B2BAgent(agentId);
            hdfParam.Value = "0";
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void ddlB2BAgent_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            int agentId = Convert.ToInt32(ddlB2BAgent.SelectedItem.Value);
            if (agentId >= 0)
            {
                BindB2B2BAgent(agentId);
                ddlB2B2BAgent.Enabled = true;
            }
            else
            {
                ddlB2B2BAgent.SelectedIndex = 0;
                ddlB2B2BAgent.Enabled = false;
            }
            hdfParam.Value = "0";

        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    protected void gvFlightInvReport_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        try
        {
            gvFlightInvReport.PageIndex = e.NewPageIndex;
            gvFlightInvReport.EditIndex = -1;
            BindGrid();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    #region Date Format
    protected string CTDateFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string CTDateTimeFormat(object date)
    {
        if (Utility.ToDate("") == Utility.ToDate(date))
            return string.Empty;
        else
        {
            DateTime dt = Utility.ToDate(date);
            return dt.ToString("dd-MMM-yyyy");
        }
    }
    protected string CTCurrencyFormat(object currency, object decimalPoint)
    {
        if (string.IsNullOrEmpty(currency.ToString()) || currency == DBNull.Value)
        {
            return Convert.ToDecimal(0).ToString("N" + decimalPoint);
        }
        else
        {
            return Convert.ToDecimal(currency).ToString("N" + decimalPoint);
        }
    }
    #endregion
}
