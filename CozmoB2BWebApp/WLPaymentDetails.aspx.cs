﻿using System;
using System.Collections.Generic;
using CT.BookingEngine.WhiteLabel;
using CT.Configuration;

public partial class WLPaymentDetails :CT.Core.ParentPage
{
    protected PaymentInformation[] payments;
    protected string show = string.Empty;
    //protected int reportsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["IndiaTimesPaymentRecordsPerPage"]);
    protected int reportsPerPage = Convert.ToInt32(ConfigurationSystem.PagingConfig["noOfPaymentsPerPage"]);
   // protected int reportsPerPage = 1; for test pagging

    protected int totalRecordsCount;
    protected string orderByString = string.Empty;
    protected int pageNo;
    protected string whereString = string.Empty;
    protected int totalPageCount;
    protected FilterPaymentBy filterPaymentBy;
    protected string searchBox;
    protected string searchBy;
    protected void Page_Load(object sender, EventArgs e)
    {
        this.Master.PageRole = true;
        if (Request["page"] != null)
        {
            pageNo = Convert.ToInt16(Request["page"]);
        }
        else
        {
            pageNo = 1;
        }
        if (Request["postbackPayment"] == null || Request["postbackPayment"] != "true")
        {
            filterPaymentBy = FilterPaymentBy.NoFilter;
            searchBox = "";
            searchBy = "1";
        }
        else
        {
            if (Request["searchBy"] == "2")
            {
                filterPaymentBy = FilterPaymentBy.ChangeRequestId;
            }
            else if (Request["searchBy"] == "3")
            {
                filterPaymentBy = FilterPaymentBy.PaymentId;
            }
            else if (Request["searchBy"] == "4")
            {
                filterPaymentBy = FilterPaymentBy.CustomerEmailId;
            }
            else
            {
                filterPaymentBy = FilterPaymentBy.NoFilter;
            }
            searchBy = Request["searchBy"];
            searchBox = Request["searchBox"];
        }
        string whereString = string.Empty;
        whereString = PaymentInformation.GetWhereStringForPayment(filterPaymentBy, searchBox.ToUpper().Trim());
        string orderByString = string.Empty;
        orderByString = orderByString + " order By paymentInformationId desc";
        totalRecordsCount = PaymentInformation.GetTotalFilteredPaymentsCount(whereString, "");
        if (totalRecordsCount % reportsPerPage != 0)
        {
            totalPageCount = totalRecordsCount / reportsPerPage + 1;
        }
        else
        {
            totalPageCount = totalRecordsCount / reportsPerPage;
        }
        string url = "SubAgentBookings.aspx";
        show = CT.MetaSearchEngine.MetaSearchEngine.PagingJavascript(totalPageCount, url, pageNo);

        Dictionary<string, string> ec = new Dictionary<string, string>();
        ec.Add("\\", "\\\\");
        ec.Add("\"", "\\\"");
        ec.Add("\n", "\\n");
        ec.Add("\'", "\\'");
        ec.Add("\0", "\\0");
        ec.Add("\a", "\\a");
        ec.Add("\b", "\\b");
        ec.Add("\f", "\\f");
        ec.Add("\r", "\\r");
        ec.Add("\t", "\\t");
        ec.Add("\v", "\\v");
        ec.Add("\r\n", "\\r\\n");
        ec.Add("\\uxxxx", "\\\\uxxxx");
        ec.Add("\\x\n[\n][\n][\n]", "\\\\x\n[\n][\n][\n]");
        ec.Add("\\Uxxxxxxxx", "\\\\Uxxxxxxxx");

        payments = PaymentInformation.GetTotalFilteredPayments(pageNo, reportsPerPage, totalRecordsCount, whereString, orderByString);
        foreach (PaymentInformation payment in payments)
        {
            foreach (KeyValuePair<string, string> k in ec)
            {
                payment.Remarks = payment.Remarks.Replace(k.Key, k.Value);
            }
        }
    }
}
