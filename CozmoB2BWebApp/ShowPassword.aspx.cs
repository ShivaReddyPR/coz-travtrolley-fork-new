﻿using CT.TicketReceipt.Common;
using System;
using System.Web.UI.WebControls;
using System.Configuration;
using CT.TicketReceipt.BusinessLayer;
public partial class ShowPasswordGUI : CT.Core.ParentPage
{
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            long userId = Convert.ToInt64(ConfigurationManager.AppSettings["pwdUserId"]);
            if (Settings.LoginInfo.UserID == userId)
            {
                lblPassword.Text = string.Empty;
            }
            else
            {
                Response.Redirect("AbandonSession.aspx",false);
            }
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
    #region Private Methods
    private void Clear()
    {
        try
        {
            txtLoginName.Text = string.Empty;
            lblPassword.Text = string.Empty;
        }
        catch
        {
            throw;
        }
    }
    private void Search()
    {
        try
        {
            string pwd = UserMaster.GetPasswordByLoginName(txtLoginName.Text);
            if(!string.IsNullOrEmpty(pwd))
            {
                lblPassword.Text = "Password Is: " +  pwd;
            }
            else
            {
                lblPassword.Text = "LoginName Doesnot Exist!";
            }
        }
        catch
        {
            throw;
        }
    }
    #endregion
    #region Button Events
    protected void btnClear_Click(object sender, EventArgs e)
    {
        try
        {
            Clear();
        }
        catch (Exception ex)
        {

            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        try
        {          
            Search();
        }
        catch (Exception ex)
        {
            Label lblMasterError = (Label)this.Master.FindControl("lblError");
            lblMasterError.Visible = true;
            lblMasterError.Text = ex.Message;
            Utility.WriteLog(ex, this.Title);
        }
    }
   
    #endregion
}