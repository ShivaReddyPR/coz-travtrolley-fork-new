﻿using System;
using System.Data;
using System.Web.UI.WebControls;
using CT.TicketReceipt.BusinessLayer;
using CT.Core;
using CT.ActivityDeals;


public partial class FixDepListGUI : CT.Core.ParentPage
{

    //protected List<HolidayPackage> dealList;
    protected string activeCheck = string.Empty;
    protected string inactiveCheck = string.Empty;
    protected int activeCount = 0;
    protected string errorString = string.Empty;
    protected string whereString = string.Empty;
    protected string activeValue = string.Empty;
    protected string inactiveValue = string.Empty;

    protected AgentMaster agency = new AgentMaster();
    protected UserMaster member = new UserMaster();
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            this.Master.PageRole = true;
            DataTable ds = Activity.GetList("Y", -1);
            DataList1.DataSource = ds;
            DataList1.DataBind();
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "FixDepartureList" + ex.Message, "0");
        }

    }
    protected void DataList1_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {
            HiddenField hdfActivityId = (HiddenField)DataList1.SelectedItem.FindControl("IThdfActivityId");
            string activityId = hdfActivityId.Value;
            // code = "31";
            string url = string.Format("FixDepMaster.aspx?activity={0}", activityId);
            Response.Redirect(url, false);
        }
        catch (Exception ex)
        {
            Audit.Add(EventType.Exception, Severity.High, Convert.ToInt32(Settings.LoginInfo.UserID), "FixDepartureList"+ex.Message, "0");
        }
    }

}
